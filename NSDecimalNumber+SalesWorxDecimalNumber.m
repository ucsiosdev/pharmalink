//
//  NSDecimalNumber+SalesWorxDecimalNumber.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 1/30/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "NSDecimalNumber+SalesWorxDecimalNumber.h"

@implementation NSDecimalNumber (SalesWorxDecimalNumber)
+ (NSDecimalNumber *)ValidDecimalNumberWithString:(nullable NSString *)numberValue
{
    if([numberValue isEqualToString:@""])
    {
        return [NSDecimalNumber decimalNumberWithString:@"0"];
    }
    else
    {
        return [NSDecimalNumber decimalNumberWithString:numberValue];
    }
}


- (NSComparisonResult) compareWithDecimal:(NSDecimalNumber *)i{
    return [self compare:i];
}

- (BOOL) isEqualToDecimal:(nullable NSDecimalNumber *)i{
    return [self compareWithDecimal:i] == NSOrderedSame;
}

- (BOOL) isGraterThanDecimal:(nullable NSDecimalNumber *)i{
    return [self compareWithDecimal:i] == NSOrderedDescending;
}

- (BOOL) isGreaterThanOrEqualToDecimal:(nullable NSDecimalNumber *)i{
    return [self isGraterThanDecimal:i] || [self compareWithDecimal:i] == NSOrderedSame;
}

- (BOOL) isLessThanDecimal:(nullable NSDecimalNumber *)i{
    return [self compareWithDecimal:i] == NSOrderedAscending;
}

- (BOOL) isLessThanOrEqualToDecimal:(nullable NSDecimalNumber *)i{
    return [self isLessThanDecimal:i] || [self compareWithDecimal:i] == NSOrderedSame;
}
- (BOOL) isBetweenOREqualToFromDecimal:(nullable NSDecimalNumber *)i ToDescimal:(nullable NSDecimalNumber *)j{
    return ([self isLessThanOrEqualToDecimal:j]&&[self isGreaterThanOrEqualToDecimal:i]);
}
- (BOOL) isBetweenFromDecimal:(nullable NSDecimalNumber *)i ToDescimal:(nullable NSDecimalNumber *)j{
    return ([self isLessThanDecimal:j]&&[self isGraterThanDecimal:i]);
}
-(BOOL)isWholeNumber {
    
    NSDecimalNumberHandler *handler = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                             scale:0
                                                                                  raiseOnExactness:NO
                                                                                   raiseOnOverflow:NO
                                                                                  raiseOnUnderflow:NO
                                                                               raiseOnDivideByZero:NO];
    
    NSDecimalNumber *vOne = [NSDecimalNumber decimalNumberWithString:@"1"];
    
    NSDecimalNumber *result = [self decimalNumberByMultiplyingBy:vOne
                                                    withBehavior:handler];
    
    BOOL inputIsWholeNumber = [self isEqualToNumber:result];
    
    return inputIsWholeNumber;
}
@end
