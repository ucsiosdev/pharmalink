//
//  UIImageView+LocalImageLoading.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 1/29/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "UIImageView+LocalImageLoading.h"
#import "MBProgressHUD.h"
#import "SWDefaults.h"

@implementation UIImageView (LocalImageLoading)
- (void)setImageWithPath:(NSString *)path
{
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        // Background work
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.image=image;
        }];
    }];
}

- (void)setThumbnailImageFromPath:(NSString *)path /*Size:(CGSize *)size*/
{
    
//    NSCache * currentImageCache = [[NSCache alloc]init];
//    
//    UIImage *cachedImage = [currentImageCache objectForKey:[[path lastPathComponent] stringByAppendingString:@"~Thumbnail"]];
//
//    if (cachedImage)
//    {
//        self.image=cachedImage;
//        NSLog(@"loading image from cache");
//        
//    }
//    else
//    {
    
    
    //[MBProgressHUD showHUDAddedTo:self animated:YES];

    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        // Background work
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            if(data.length*1024*1024>=0.5)
//            {
//                NSData *dataForJPEGFile = UIImageJPEGRepresentation(image, 0.5);
//                self.image=[UIImage imageWithData:dataForJPEGFile];
//            }
//            else
//            {
//                 self.image=image;
//            }
            
           self.image= [self resizeImage:image newWidth:self.frame.size.width];
            
            
            
            
            
            
            //image compression
            
//            NSString *savedImagePath = [[SWDefaults applicationDocumentsDirectory]  stringByAppendingPathComponent:@"Testthumb.png"];
//            
//            NSData *imageData = UIImageJPEGRepresentation(self.image, 0.7);/*UIImagePNGRepresentation(_imgView.image);*/
//            
//            
//            [imageData writeToFile:savedImagePath atomically:NO];
//            

            
            
            
            
            
            
            
            //[MBProgressHUD hideAllHUDsForView:self animated:YES];
           
            NSLog(@"cached image path %@",[[path lastPathComponent] stringByAppendingString:@"~Thumbnail"]);
            
            
            

        }];
    }];
    
}

- (UIImage *)resizeImage:(UIImage*)image newWidth:(CGFloat)newWidth {
    
    CGFloat imageSizeRatio=image.size.height/image.size.width;
    
    CGSize newSize=CGSizeMake(newWidth, imageSizeRatio *newWidth);
    
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    
//    NSData *pngData = UIImagePNGRepresentation(newImage);
//    [UIImagePNGRepresentation(newImage) writeToFile:[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"Thumbnails"] stringByAppendingPathComponent:@"Testthumb.png"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    
    
   
    
    

    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end
