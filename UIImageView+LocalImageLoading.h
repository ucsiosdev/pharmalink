//
//  UIImageView+LocalImageLoading.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 1/29/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kThumbnailKey 


@interface UIImageView (LocalImageLoading)
- (void)setImageWithPath:(NSString *)path;
- (void)setThumbnailImageFromPath:(NSString *)path;
@end
