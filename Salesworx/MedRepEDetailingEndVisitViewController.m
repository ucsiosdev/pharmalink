//
//  MedRepEDetailingEndVisitViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepEDetailingEndVisitViewController.h"
#import "MedRepDefaults.h"
#import "MedRepEdetailTaskViewController.h"
#import "MedRepQueries.h"
#import "MedRepVisitsListViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepDefaults.h"
#import "SalesWorxImageBarButtonItem.h"
#import "MBProgressHUD.h"
#import "MedRepVisitsStartMultipleCallsViewController.h"
@interface MedRepEDetailingEndVisitViewController (){
    MedRepVisitsStartMultipleCallsViewController *multiCallsViewcontroller;
}
@end

@implementation MedRepEDetailingEndVisitViewController

@synthesize demonstratedProductsArray,samplesGivenArray,visitDetailsDict,locationLbl,doctorLbl,visitEndTimeLbl,visitStartTimeLbl,visitConclusionNotesTxtView,meetingAttendeesTxtView,visitAdditionalInfoDict,endVisitWithoutCall,visitDetails,selectedDoctorName,selectedMeetingAttendeesArray, isFromDashboard, selectedDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSDateFormatter *endTimeFormatter = [NSDateFormatter new] ;
//    [endTimeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [endTimeFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
//    NSString *callStartDate =  [endTimeFormatter stringFromDate:[NSDate date]];
//    [[NSUserDefaults standardUserDefaults]setObject:callStartDate forKey:@"EDetailing_End_Time"];
    
    
    appCtrl = [AppControl retrieveSingleton];
    wordsLimitForVisitConclusion =appCtrl.FM_VISIT_CONCLUSION_WORD_LIMIT;
    wordsLimitForNextVisitObjective =appCtrl.FM_NEXT_VISIT_OBJECTIVE_WORD_LIMIT;
    nextVisitObjectiveTitle = [MedRepQueries fetchDescriptionForAppCode:kNextVisitObjectiveTitleAppCode];
    if ([NSString isEmpty:[SWDefaults getValidStringValue:nextVisitObjectiveTitle]])
    {
        nextVisitObjectiveTitle = kNextVisitObjective;
    }
    nextVisitObjectiveTitleLabel.text =nextVisitObjectiveTitle;
    meetingAttendeesArray=[[NSMutableArray alloc]init];
    selectedMeetingAttendeesArray=[[NSMutableArray alloc]init];
    
    meetingAttendeesTextField.placeholder=NSLocalizedString(@"Meeting Attendees", nil);
    
    selectedMeetingAttendeesTblView.allowsMultipleSelectionDuringEditing = NO;
 //   visitConclusionNotesTxtView.contentInset = UIEdgeInsetsMake(8, 8, 8, 8);

    oldFrame=CGRectMake(0, 64, 1024, 704);
    
   
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:MedRepEndVisitTitle];

   // visitRating=@"5";

    
    NSLog(@"demonstrated products in end visit %@", [demonstratedProductsArray description]);
    
    NSLog(@"samples given in end visit %@", [samplesGivenArray description]);
    
    NSLog(@"visit details in end visit %@", [visitDetailsDict description]);
    
    NSLog(@"visit start time %@",[MedRepDefaults fetchVisitStartTime]);
    
    
    visitType=[visitDetailsDict valueForKey:@"Visit_Type"];
    
    //capture start visit end  time
    
    NSDate *visitEnddate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:visitEnddate];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    NSString* visitEndTimeStr=[[[NSString stringWithFormat:@"%ld",(long)hour] stringByAppendingString:@":"] stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)minute]];

    
    NSString* visitStartTimeStr=visitDetails.currentCallStartTime;
    
    NSLog(@"VISIT START TIME IS %@", visitStartTimeStr);
    
    if (visitDetailsDict.count>0) {
        
       // NSLog(@"visit details in start call %@", [visitDetailsDict description]);
        locationLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Location_Name"]];
       
//        
//        if ([[visitDetailsDict valueForKey:@"Doctor_Name"]length]==0) {
//            
//            doctorLbl.text=@"N/A";
//        }
//        else
//        {
//            
//        
//        doctorLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Doctor_Name"]];
//        
//        }
    }
    
    
    
    //to be done
    
    
    
//    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
//    [numberFormatter setPaddingCharacter:@"0"];
//    [numberFormatter setMinimumIntegerDigits:2];
//    
//    
//    NSString * hourString = [NSString stringWithFormat:@"%@",[numberFormatter stringFromNumber:[NSNumber numberWithInteger:hour]]];
//    
//    NSString * minuteString = [NSString stringWithFormat:@"%@",[numberFormatter stringFromNumber:[NSNumber numberWithInteger:minute]]];
//    
//    // NSLog(@"check hour and minute in start time %@:%@",hourString,minuteString );
//    
//
//    NSString* visitEndTimeStr=[[hourString stringByAppendingString:@":"] stringByAppendingString:minuteString];
    
    
    
    
    
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"hh:mm a"];
    [formatterTime setLocale:usLocaleq];
    NSString *callEndDate =  [formatterTime stringFromDate:[NSDate date]];
    
    visitEndTimeLbl.text=callEndDate;
    
    if (visitStartTimeStr.length>0) {
        
        visitStartTimeLbl.text=visitStartTimeStr;
    }
    
    if (visitEndTimeStr.length>0) {
        
        
        NSDate *visitStartdate = [NSDate date];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:visitStartdate];
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        

        
        
        NSLog(@"time is %d, %d", hour,minute);
        
       // visitEndTimeLbl.text=[NSString stringWithFormat:@"%d:%d",hour,minute];
        
    }
    
    
//    UIBarButtonItem* taskBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"T" style:UIBarButtonItemStylePlain target:self action:@selector(taskBarButtonTapped:)];
//    
//    UIBarButtonItem* notesBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"N" style:UIBarButtonItemStylePlain target:self action:@selector(notesBarButtonTapped:)];
//    
//    UIBarButtonItem* startVisitBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"EV" style:UIBarButtonItemStylePlain target:self action:@selector(endCallTapped)];
//    
//      UIBarButtonItem* taskBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"medRepTasks.png"] style:UIBarButtonItemStylePlain target:self action:@selector(taskBarButtonTapped:)];
//    
//    UIBarButtonItem* notesBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"medRepNotes.png"] style:UIBarButtonItemStylePlain target:self action:@selector(notesBarButtonTapped:)];
    
    endVisitBarButtonItem=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_EndVisit"] style:UIBarButtonItemStylePlain target:self action:@selector(endCallTapped)];
//    NSArray*barButtonArray=[[NSArray alloc]initWithObjects:taskBarButtonItem,notesBarButtonItem,startVisitBarButtonItem, nil];
    
    

    self.navigationItem.rightBarButtonItem=endVisitBarButtonItem;
    
    
   // self.navigationItem.rightBarButtonItems=barButtonArray;
    
    
    // Do any additional setup after loading the view from its nib.
    
    camelLabelsView.tickCount=10;
    camelLabelsView.ticksDistance=75;
    camelLabelsView.value=0;
    camelLabelsView.upFontName=@"WeblySleekUISemibold";
    camelLabelsView.upFontSize=20;
    camelLabelsView.upFontColor=kNavigationBarBackgroundColor;
    camelLabelsView.downFontName=@"WeblySleekUISemibold";
    camelLabelsView.downFontSize=20;
    camelLabelsView.downFontColor=[UIColor lightGrayColor];
    camelLabelsView.backgroundColor=[UIColor clearColor];
    
    
    descreteSliderView.tickStyle=1;
    descreteSliderView.tickSize = CGSizeMake(1, 8);
    descreteSliderView.tickCount=10;
    descreteSliderView.trackThickness=2;
    descreteSliderView.incrementValue=0;
    descreteSliderView.minimumValue=0;
    descreteSliderView.value=0;
    [descreteSliderView addTarget:self action:@selector(customSliderValueChanged) forControlEvents:UIControlEventValueChanged];
    
    descreteSliderView.tintColor=kNavigationBarBackgroundColor;
    descreteSliderView.ticksListener=camelLabelsView;
}

-(void)customSliderValueChanged {
    
    visitRating = [NSString stringWithFormat:@"%d",[[NSNumber numberWithFloat:descreteSliderView.value+1] intValue]];
    [visitDetailsDict setValue:visitRating forKey:@"Visit_Rating"];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingEndCall];
    }
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    if (self.isMovingToParentViewController) {

        pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(0, 0, signatureContainerView.frame.size.width, signatureContainerView.frame.size.height)];;
        [pjrSignView.layer setBorderWidth: 1.0];
        [pjrSignView.layer setCornerRadius:5.0f];
        [pjrSignView.layer setMasksToBounds:YES];
        [pjrSignView.layer setBorderColor:[[UIColor colorWithRed:(234.0/255.0) green:(237.0/255.0) blue:(242.0/255.0) alpha:1] CGColor]];
 
        NSString *path = [self dbGetImagesDocumentPath];
        path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[visitDetailsDict valueForKey:@"Planned_Visit_ID"]]];
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:path];
        
        isSignatureCaptured = [MedRepDefaults isSignatureCapturedForCurrentCall];
        if (img != nil && isSignatureCaptured) {
            [pjrSignView.previousSignature setFrame:pjrSignView.frame];
            pjrSignView.previousSignature.image = [[UIImage alloc] initWithContentsOfFile:path];

            [pjrSignView setUserInteractionEnabled:NO];
            [signatureClearButton setHidden:YES];
            [signatureSaveButton setUserInteractionEnabled:NO];
            [signatureSaveButton UpdateImage:[UIImage imageNamed:@"Signature_Saved"] WithAnimation:YES];
        } else {
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"New_SignaturePenIcon"];
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            pjrSignView.lblSignature.attributedText = attachmentString;
        }
        [signatureContainerView addSubview:pjrSignView];
    }
    //appCtrl.FM_SHOW_NEXT_VISIT_OBJECTIVE=KAppControlsYESCode; 736
    
    if ([appCtrl.FM_SHOW_NEXT_VISIT_OBJECTIVE isEqualToString:KAppControlsYESCode]) {
        xNextVisitObjectiveWidthConstraint.constant=359.0;
        xNextVisitObjectiveTrailingConstraint.constant=8.0;
        nextVisitObjectiveView.hidden=NO;
        visitConclusionNotesWidthConstraint.constant = 359.0;
        nextVisitObjectiveLeadingConstraint.constant = 8.0;
        nextVisitObjectiveTrailingConstraint.constant = 8.0;
        
        
    }
    else
    {
        xNextVisitObjectiveWidthConstraint.constant=0.0;
        xNextVisitObjectiveTrailingConstraint.constant=0.0;
        nextVisitObjectiveView.hidden=YES;
        nextVisitObjectiveWidthConstraint.constant = 0.0;
        nextVisitObjectiveLeadingConstraint.constant = 0.0;
        nextVisitObjectiveTrailingConstraint.constant = 8.0;
        visitConclusionNotesWidthConstraint.constant = 480;
        
    }
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    if (signatureClearButton.hidden == YES) {
        [MedRepDefaults setSignatureCapturedForCurrentCall:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    waitTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    visitStartTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    visitEndTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);

    
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingEndVisit];
    }
    
    [doctorLbl sizeToFit];
    appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];

    //check time spend after starting visit and before meeting doctor,(wait time)
    
    NSLog(@"WAIT TIME END TIMER IS %@", [[NSUserDefaults standardUserDefaults]objectForKey:@"waitTimer"]);
    
    
    NSLog(@"samples in end call %@", self.visitDetails.samplesArray);
    
    
    NSString* waitTimeStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"waitTimer"];
    
    if (waitTimeStr.length>0) {
        waitTimeLbl.text=waitTimeStr;

    }
    
    if (visitDetailsDict.count>0) {
        
        
        
        
        
        NSString* notesAvailable=[visitDetailsDict valueForKey:@"Notes_Available"];
        
        
        NSString* visitConclusionNotes=[visitDetailsDict valueForKey:@"Visit_Conclusion_Notes"];
        
        NSString* nextVisitObjective = [visitDetailsDict valueForKey:@"Next_Visit_Objective"];
        
        NSString* visitRatingStr=[visitDetailsDict valueForKey:@"Visit_Rating"];
        
        
        NSString* meetingAttendees =[SWDefaults getValidStringValue:[visitDetailsDict valueForKey:@"Meeting_Attendees"]];
        
        selectedMeetingAttendeesArray=[[meetingAttendees componentsSeparatedByString:@","] mutableCopy];

        [selectedMeetingAttendeesArray removeObject:@""];
        
        if (visitRatingStr.length>0) {
            visitRating = visitRatingStr;
        } else {
            visitRating = appCtrl.FM_DEFAULT_VISIT_RATING;
            [visitDetailsDict setValue:visitRating forKey:@"Visit_Rating"];
        }
        camelLabelsView.value = [visitRating floatValue]-1;
        descreteSliderView.value = [visitRating floatValue]-1;
        
        
        if (visitConclusionNotes.length>0) {
            
            visitConclusionNotesTxtView.text=visitConclusionNotes;
            
        }
        
        if (nextVisitObjective.length>0) {
            
            nextVisitObjectiveTextView.text=nextVisitObjective;
            
        }
        
        if (meetingAttendees.length>0) {
            
            meetingAttendeesTxtView.text=[NSString stringWithFormat:@"%@",meetingAttendees];
            
        }
        
        
        NSString* taskStatus=[visitDetailsDict valueForKey:@"TASK_STATUS"];
        
        if ([taskStatus isEqualToString:@"OVER_DUE"]) {
            
            [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
            
            
        }
        
        
        else if ([taskStatus isEqualToString:@"PENDING"])
            
        {
            [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
        }
        
        
        
        if ([notesAvailable isEqualToString:@"YES"]) {
            
            [notesStatusButton setBackgroundColor:KNOTESAVAILABLECOLOR];
            
        }
        
        else
        {
            [notesStatusButton setBackgroundColor:KNOTESUNAVAILABLECOLOR];
            
        }
    }
    
    
    if ([[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
        
        doctorLbl.text=[visitDetailsDict valueForKey:@"Pharmacy_Contact_Name"];
        
        doctorHeaderLbl.text=@"Contact Name";
    }
    else
    {
        
        
        doctorLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Doctor_Name"]];
    }
    





    if ([visitType isEqualToString:@"R"]) {
        
        self.roundTableMeetingLbl.hidden=NO;
        self.meetingAttendeesTxtView.hidden=NO;
        meetingAttendeesBackgroundView.hidden=NO;
        meetingAttendeesSaperator.hidden=NO;
        xMeetingAttendeesWidthConstraint.constant=359.0;
        xMeetingAttendeesTrainlingConstraint.constant=8.0;
        self.roundTableMeetingLbl.text=@"Meeting Attendees";
    }
    
    else
    {
        xMeetingAttendeesWidthConstraint.constant=0.0;
        xMeetingAttendeesTrainlingConstraint.constant=0.0;
        self.roundTableMeetingLbl.hidden=YES;
        self.meetingAttendeesTxtView.hidden=YES;
        meetingAttendeesBackgroundView.hidden=YES;
        meetingAttendeesSaperator.hidden=YES;
        
    }
    
    
    
    
    for (UIView* currentView in self.view.subviews) {
        
        if (currentView.tag==10 || currentView.tag==11) {
            
//            [currentView.layer setBorderWidth: 1.0];
//            [currentView.layer setCornerRadius:8.0f];
//            [currentView.layer setMasksToBounds:YES];
//            [currentView.layer setBorderColor:[[UIColor colorWithRed:(234.0/255.0) green:(237.0/255.0) blue:(242.0/255.0) alpha:1] CGColor]];
        }
        
    }
    
    
     faceTimeTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:YES];
    self.navigationController.navigationBar.topItem.title = @"";

    
    
    tasksButton.layer.borderWidth=1.0f;
    notesButton.layer.borderWidth=1.0f;
    tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    
    
    
    notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    notesButton.layer.shadowOffset = CGSizeMake(2, 2);
    notesButton.layer.shadowOpacity = 0.1;
    notesButton.layer.shadowRadius = 1.0;
    
    
    
    tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
    tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
    tasksButton.layer.shadowOpacity = 0.1;
    tasksButton.layer.shadowRadius = 1.0;
   
    
    
    for (UIView *currentView in self.view.subviews) {
        
        if ([currentView isKindOfClass:[UIView class]]) {
            
            
            
            if (currentView.tag==1 || currentView.tag==2|| currentView.tag==10 ||currentView.tag==11 ) {
                
                currentView.layer.shadowColor = [UIColor blackColor].CGColor;
                currentView.layer.shadowOffset = CGSizeMake(2, 2);
                currentView.layer.shadowOpacity = 0.1;
                currentView.layer.shadowRadius = 1.0;
                
            }
        }
    }
    

    visitStatisticsBackgroundView.layer.shadowColor = [UIColor blackColor].CGColor;
    visitStatisticsBackgroundView.layer.shadowOffset = CGSizeMake(2, 2);
    visitStatisticsBackgroundView.layer.shadowOpacity = 0.1;
    visitStatisticsBackgroundView.layer.shadowRadius = 1.0;

    
    visitRatingBackgroundView.layer.shadowColor = [UIColor blackColor].CGColor;
    visitRatingBackgroundView.layer.shadowOffset = CGSizeMake(2, 2);
    visitRatingBackgroundView.layer.shadowOpacity = 0.1;
    visitRatingBackgroundView.layer.shadowRadius = 1.0;
    
    meetingAttendeesBackgroundView.layer.shadowColor = [UIColor blackColor].CGColor;
    meetingAttendeesBackgroundView.layer.shadowOffset = CGSizeMake(2, 2);
    meetingAttendeesBackgroundView.layer.shadowOpacity = 0.1;
    meetingAttendeesBackgroundView.layer.shadowRadius = 1.0;
    
    
    //[self updateStatusforTasksandNotes];
    [self refreshTaskandNotesStatus:self.visitDetails];

    
}


-(void)startFaceTimeTimer
{
//    NSDate * currentTimer=[[NSUserDefaults standardUserDefaults]objectForKey:@"faceTimeTimer"];
//    NSDate *now = [NSDate date];
//    NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
//    
//    // NSLog(@"wait time counter ticking %@",[self stringFromTimeInterval:elapsedTimeDisplayingPoints]);
//
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"hh:mm a"];
    [formatterTime setLocale:usLocaleq];
    NSString *callEndDate =  [formatterTime stringFromDate:[NSDate date]];
    
    visitEndTimeLbl.text=callEndDate;
    
    
}

-(NSString*)fetchVisitEndTime

{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    // timeLabel.text = formattedDateString;**
    
    return formattedDateString;
    

}



- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

#pragma mark Navigation Bar button Actions

- (IBAction)taskButtonTapped:(id)sender {
    
    popOverController=nil;
    MedRepEdetailTaskViewController * edetailTaskVC=[[MedRepEdetailTaskViewController alloc]init];
    edetailTaskVC.visitDetails=self.visitDetails;
    
    edetailTaskVC.tasksArray=self.visitDetails.taskArray;
    edetailTaskVC.delegate=self;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(376, 600) animated:YES];
    edetailTaskVC.tasksPopOverController=popOverController;
    
    CGRect relativeFrame = [tasksButton convertRect:tasksButton.bounds toView:self.view];
    
    if ([SWDefaults isRTL]) {
        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else
    {
        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    }
}


- (IBAction)notesButtonTapped:(id)sender {
    popOverController=nil;
    MedRepEDetailNotesViewController * edetailTaskVC=[[MedRepEDetailNotesViewController alloc]init];
    edetailTaskVC.visitDetails=self.visitDetails;
    edetailTaskVC.delegate=self;
    edetailTaskVC.notesArray=self.visitDetails.notesArray;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(376, 350) animated:YES];
    edetailTaskVC.notesPopOverController=popOverController;
    CGRect relativeFrame = [notesButton convertRect:notesButton.bounds toView:self.view];
    [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}
-(void)closeVisitWithData
{
    NSDateFormatter *formatterTime = [NSDateFormatter new];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *callEndDate =  [formatterTime stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults]setObject:callEndDate forKey:@"Call_End_Date"];
    
    visitAdditionalInfoDict=[[NSMutableDictionary alloc]init];
    
    
    if (visitRating.length>0) {
        [visitAdditionalInfoDict setValue:visitRating forKey:@"Visit_Rating"];
        
    }
    //check here if visit has ended without call, without showing demo to doctor
    //if a visit is closed with out demo, we will have visit details and visit additinal info, update the query accordingly
    
    BOOL status=[self closeVisit];
    if (status==YES) {
        
        NSLog(@"VISIT DETAILS ADDED SUCCESSFULLY");
        
        
        [faceTimeTimer invalidate];
        faceTimeTimer=nil;
        
        NSLog(@"facetime timer invalidated? %@", faceTimeTimer);
        
        
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"faceTimeTimer"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"waitTimer"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"EDetailing_End_Time"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_Start_Date"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_End_Date"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"EDetailing_Start_Time"];

        
        
        
        if ([visitConclusionNotesTxtView isFirstResponder]) {
            
            [visitConclusionNotesTxtView resignFirstResponder];
        }
        
        /** Uilaertview replaced with ui alertcontroller*/
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"Success", nil)
                                      message:NSLocalizedString(@"Visit Completed Successfully", nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action){
                                        [MBProgressHUD showHUDAddedTo:appDelegate.window animated:YES];
                                        endVisitBarButtonItem.enabled=NO;
                                        [[NSUserDefaults standardUserDefaults]setValue:@"YES" forKey:@"MovingtoRootVC"];
                                        if ([self.selectedDelegate respondsToSelector:@selector(selectedIndexDelegate:)]) {
                                            [self.selectedDelegate selectedIndexDelegate:visitDetailsDict];
                                        }
                                        
                                        if ([appCtrl.FM_VISIT_WITH_PLANNED_DOCTOR_ONLY isEqualToString:KAppControlsYESCode]) {
                                            BOOL status=[MedRepQueries updateVisitStatus:visitDetails];
                                            if (status==YES) {
                                                [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:visitDetails];
                                                
                                                if ([appCtrl.FM_ENABLE_DISTRIBUTION_CHECK isEqualToString:KAppControlsYESCode] && [[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
                                                    
                                                    if (isFromDashboard) {
                                                        multiCallsViewcontroller = [self.navigationController.viewControllers objectAtIndex:2];
                                                    } else{
                                                        multiCallsViewcontroller = [self.navigationController.viewControllers objectAtIndex:1];
                                                    }
                                                    [self.navigationController popToViewController:multiCallsViewcontroller animated:YES];
                                                } else {
                                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                                }
                                            }
                                        } else {
                                            [[NSNotificationCenter defaultCenter]postNotificationName:@"CallDidFinishNotification" object:visitDetails];
                                            [self.navigationController popToViewController:multiCallsViewcontroller animated:YES];
                                        }
                                        [MBProgressHUD hideAllHUDsForView:appDelegate.window animated:YES];
                                        
                                    }];
        [alert addAction:yesAction];
        [self presentViewController:alert animated:YES completion:^{
            [visitDetailsDict setValue:@"Y" forKey:@"Visit_Status"];
            visitDetails.Accompanied_By=[visitDetailsDict valueForKey:@"Accompanies_By"];
            visitDetails.ActualPlannedVisitID=[visitDetailsDict valueForKey:@"ActualPlannedVisitID"];
            visitDetails.Actual_Visit_ID=[visitDetailsDict valueForKey:@"Actual_Visit_ID"];
            visitDetails.Address=[visitDetailsDict valueForKey:@"Address"];
            visitDetails.Class=[visitDetailsDict valueForKey:@"Class"];
            visitDetails.Contact_ID=[visitDetailsDict valueForKey:@"Contact_ID"];
            visitDetails.Created_At=[visitDetailsDict valueForKey:@"Created_At"];
            visitDetails.Created_By=[visitDetailsDict valueForKey:@"Created_By"];
            visitDetails.Demo_Plan_ID=[visitDetailsDict valueForKey:@"Demo_Plan_ID"];
            visitDetails.Doctor_Class=[visitDetailsDict valueForKey:@"Doctor_Class"];
            visitDetails.Doctor_ID=[visitDetailsDict valueForKey:@"Doctor_ID"];
            visitDetails.Doctor_Name=[visitDetailsDict valueForKey:@"Doctor_Name"];
            visitDetails.EMP_ID=[visitDetailsDict valueForKey:@"EMP_ID"];
            visitDetails.Last_Visited_At=[visitDetailsDict valueForKey:@"Last_Visited_At"];
            visitDetails.Latitude=[visitDetailsDict valueForKey:@"Latitude"];
            visitDetails.Location_ID=[visitDetailsDict valueForKey:@"Location_ID"];
            visitDetails.Location_Name=[visitDetailsDict valueForKey:@"Location_Name"];
            visitDetails.Longitude=[visitDetailsDict valueForKey:@"Longitude"];
            visitDetails.Notes_Available=[visitDetailsDict valueForKey:@"Notes_Available"];
            visitDetails.OTC=[visitDetailsDict valueForKey:@"OTC"];
            visitDetails.Objective=[visitDetailsDict valueForKey:@"Objective"];
            visitDetails.Pharmacy_Contact_Name=[visitDetailsDict valueForKey:@"Pharmacy_Contact_Name"];
            visitDetails.Planned_Visit_ID=[visitDetailsDict valueForKey:@"Planned_Visit_ID"];
            visitDetails.Selected_Index=[visitDetailsDict valueForKey:@"Selected_Index"];
            visitDetails.Specialization=[visitDetailsDict valueForKey:@"Specialization"];
            visitDetails.Tasks_Available=[visitDetailsDict valueForKey:@"Tasks_Available"];
            visitDetails.Trade_Channel=[visitDetailsDict valueForKey:@"Trade_Channel"];
            visitDetails.Visit_Date=[visitDetailsDict valueForKey:@"Visit_Date"];
            visitDetails.Visit_Rating=[visitDetailsDict valueForKey:@"Visit_Rating"];
            visitDetails.Visit_Status=[visitDetailsDict valueForKey:@"Visit_Status"];
            visitDetails.Visit_Type=[visitDetailsDict valueForKey:@"Visit_Type"];
            visitDetails.VisitEndTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            visitDetails.Location_Type=[visitDetailsDict valueForKey:@"Location_Type"];
            
            
            if ([appCtrl.FM_ENABLE_DISTRIBUTION_CHECK isEqualToString:KAppControlsYESCode] && [[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
                
                NSLog(@"view controller stack is %@",self.navigationController.viewControllers);
                if (isFromDashboard) {
                    multiCallsViewcontroller = [self.navigationController.viewControllers objectAtIndex:3];
                } else{
                    multiCallsViewcontroller = [self.navigationController.viewControllers objectAtIndex:2];
                }
                
            } else {
                if (isFromDashboard) {
                    multiCallsViewcontroller = [self.navigationController.viewControllers objectAtIndex:2];
                } else{
                    multiCallsViewcontroller = [self.navigationController.viewControllers objectAtIndex:1];
                }
            }
            
            NSLog(@"visit details before posting notification %@ %@", visitDetailsDict,visitDetails.Visit_Status);
        }];
        
    }
    else{
        UIAlertView* visitUnSuccessfull=[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Visit Could not be completed at this moment,would you like to try again" delegate:self cancelButtonTitle:(NSLocalizedString(KAlertYESButtonTitle, nil)) otherButtonTitles:(NSLocalizedString(KAlertNoButtonTitle, nil)), nil];
        visitUnSuccessfull.tag=200;
        [visitUnSuccessfull show];
    }
}

-(void)endCallTapped

{
    [self.view endEditing:YES];
    endVisitBarButtonItem.enabled=NO;
    if(visitRating == nil && [appCtrl.IS_FM_VISIT_RATING_MANDATORY isEqualToString:KAppControlsYESCode]){
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:@"Please select rating" withController:self];
    }else if(  visitConclusionNotesTxtView.text.trimString.length==0 && [appCtrl.IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY isEqualToString:KAppControlsYESCode]){
        //dont show alert
        [visitDetailsDict setObject:@"" forKey:@"Visit_Conclusion_Notes"];
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:@"Please enter visit conclusion" withController:self];
    }else if([NSString isEmpty:[visitDetailsDict valueForKey:@"Meeting_Attendees"]]==YES && [visitType isEqualToString:@"R"]){
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:@"Please enter meeting attendees" withController:self];
    }else if (nextVisitObjectiveTextView.text.trimString.length==0 && [appCtrl.FM_SHOW_NEXT_VISIT_OBJECTIVE isEqualToString:KAppControlsYESCode]){
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:[NSString stringWithFormat:@"please enter %@",nextVisitObjectiveTitle] withController:self];
    }else if ([appCtrl.FM_ENABLE_WORD_LIMIT isEqualToString:KAppControlsYESCode] &&
               ![NSString isEmpty:wordsLimitForVisitConclusion] &&
               ![self validateWordLengthforTextView:visitConclusionNotesTxtView]) {
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [visitConclusionNotesTxtView becomeFirstResponder];
                                           
                                       }];
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Visit Conclusion" andMessage:[NSString stringWithFormat:@"Please enter at least %@ words in visit conclusion notes",wordsLimitForVisitConclusion] andActions:actionsArray withController:self];
    }else if ([appCtrl.FM_SHOW_NEXT_VISIT_OBJECTIVE isEqualToString:KAppControlsYESCode] &&
              [appCtrl.FM_ENABLE_WORD_LIMIT isEqualToString:KAppControlsYESCode] &&
              ![NSString isEmpty:wordsLimitForNextVisitObjective] &&
              ![self validateWordLengthforTextView:nextVisitObjectiveTextView]){
        
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           
                                           [nextVisitObjectiveTextView becomeFirstResponder];
                                           
                                       }];
            
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:nextVisitObjectiveTitle andMessage:[NSString stringWithFormat:@"Please enter at least %@ words in %@",wordsLimitForNextVisitObjective,nextVisitObjectiveTitle] andActions:actionsArray withController:self];
    }else{
        [self closeVisitWithData];
    }
    endVisitBarButtonItem.enabled=YES;
}


-(BOOL)closeVisit
{
    [self.view endEditing:YES];
    
    BOOL status =NO;
    if (endVisitWithoutCall==YES) {
        // status=[MedRepQueries saveVisitDetailsWithOutCall:visitDetailsDict AdditionalInfo:visitAdditionalInfoDict];
        status=[MedRepQueries saveVisitDetailsWithOutCall:visitDetailsDict AdditionalInfo:visitAdditionalInfoDict andVisitDetails:self.visitDetails];
    }
    
    else
    {
        
        status=[MedRepQueries saveVisitDetails:visitDetailsDict withProductsDemonstrated:demonstratedProductsArray andSamplesGiven:samplesGivenArray withAdditionalInfo:visitAdditionalInfoDict andVisitDetails:self.visitDetails];
        
        
    }
    return status;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag==100) {
    }
    
    else if (alertView.tag==200)
    {
        
        if (buttonIndex==0) {

        BOOL status=[self closeVisit];
        
        if (status==NO) {
            
            [faceTimeTimer invalidate];
            faceTimeTimer=nil;
            
            UIAlertView * errorVisit=[[UIAlertView alloc]initWithTitle:@"Visit could not be completed" message:@"please try later" delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
            errorVisit.tag=300;
            [errorVisit show];
            
        }
        
        }
        
    }
    
    else if (alertView.tag==300)
    {
        
        
        //VisitDetails * visitDetails=[[VisitDetails alloc]init];
        
        [visitDetailsDict setValue:@"N" forKey:@"Visit_Status"];
        
        
        
        visitDetails.Accompanied_By=[visitDetailsDict valueForKey:@"Accompanies_By"];
        visitDetails.ActualPlannedVisitID=[visitDetailsDict valueForKey:@"ActualPlannedVisitID"];
        visitDetails.Actual_Visit_ID=[visitDetailsDict valueForKey:@"Actual_Visit_ID"];
        visitDetails.Address=[visitDetailsDict valueForKey:@"Address"];
        visitDetails.Class=[visitDetailsDict valueForKey:@"Class"];
        visitDetails.Contact_ID=[visitDetailsDict valueForKey:@"Contact_ID"];
        visitDetails.Created_At=[visitDetailsDict valueForKey:@"Created_At"];
        visitDetails.Created_By=[visitDetailsDict valueForKey:@"Created_By"];
        visitDetails.Demo_Plan_ID=[visitDetailsDict valueForKey:@"Demo_Plan_ID"];
        visitDetails.Doctor_Class=[visitDetailsDict valueForKey:@"Doctor_Class"];
        visitDetails.Doctor_ID=[visitDetailsDict valueForKey:@"Doctor_ID"];
        visitDetails.Doctor_Name=[visitDetailsDict valueForKey:@"Doctor_Name"];
        visitDetails.EMP_ID=[visitDetailsDict valueForKey:@"EMP_ID"];
        visitDetails.Last_Visited_At=[visitDetailsDict valueForKey:@"Last_Visited_At"];
        visitDetails.Latitude=[visitDetailsDict valueForKey:@"Latitude"];
        visitDetails.Location_ID=[visitDetailsDict valueForKey:@"Location_ID"];
        visitDetails.Location_Name=[visitDetailsDict valueForKey:@"Location_Name"];
        visitDetails.Longitude=[visitDetailsDict valueForKey:@"Longitude"];
        visitDetails.Notes_Available=[visitDetailsDict valueForKey:@"Notes_Available"];
        visitDetails.OTC=[visitDetailsDict valueForKey:@"OTC"];
        visitDetails.Objective=[visitDetailsDict valueForKey:@"Objective"];
        visitDetails.Pharmacy_Contact_Name=[visitDetailsDict valueForKey:@"Pharmacy_Contact_Name"];
        visitDetails.Planned_Visit_ID=[visitDetailsDict valueForKey:@"Planned_Visit_ID"];
        visitDetails.Selected_Index=[visitDetailsDict valueForKey:@"Selected_Index"];
        visitDetails.Specialization=[visitDetailsDict valueForKey:@"Specialization"];
        visitDetails.Tasks_Available=[visitDetailsDict valueForKey:@"Tasks_Available"];
        visitDetails.Trade_Channel=[visitDetailsDict valueForKey:@"Trade_Channel"];
        visitDetails.Visit_Date=[visitDetailsDict valueForKey:@"Visit_Date"];
        visitDetails.Visit_Rating=[visitDetailsDict valueForKey:@"Visit_Rating"];
        visitDetails.Visit_Status=[visitDetailsDict valueForKey:@"Visit_Status"];
        visitDetails.Visit_Status=[visitDetailsDict valueForKey:@"Visit_Type"];
        visitDetails.VisitEndTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        [self.navigationController popToRootViewControllerAnimated:YES];

    }
}

-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
    // [textField release] // uncomment if not using ARC
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

-(void)popViewPopTimer

{

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark UITextView Delegate Methods
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView==meetingAttendeesTxtView) {

       selectedMeetingAttendeesArray=[[NSMutableArray alloc]initWithArray:[textView.text componentsSeparatedByString:@","]];
        [selectedMeetingAttendeesArray removeObject:@""];

    }
    
}

-(BOOL)validateWordLengthforTextView:(MedRepTextView *)textView
{
    BOOL valid=NO;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+" options:0 error:&error];
    NSUInteger wordCount = [regex numberOfMatchesInString:textView.text
                                                  options:0
                                                    range:NSMakeRange(0, [textView.text length])];
    NSLog(@"word count is %lu",(unsigned long)wordCount);
    
    if (textView == visitConclusionNotesTxtView) {
        if (wordCount<[wordsLimitForVisitConclusion integerValue]) {
            valid=NO;
        }
        else
        {
            valid = YES;
        }
    } else if (textView == nextVisitObjectiveTextView) {
        if (wordCount<[wordsLimitForNextVisitObjective integerValue]) {
            valid=NO;
        }
        else
        {
            valid = YES;
        }
    }
    
    return valid;
}



- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
//    //in case if user taps on next visit objective without entering minimum 20 words in conclusion notes then disable editing in next visit objective
//    BOOL wordLimitValid= [self validateWordLengthforTextView:visitConclusionNotesTxtView.text];
//
//    BOOL wordLimitValidNextVisitObjective= [self validateWordLengthforTextView:nextVisitObjectiveTextView.text];
//
//    BOOL isNextVisitObjectiveEmpty = [NSString isEmpty:[NSString getValidStringValue:nextVisitObjectiveTextView.text]];
//
//    if (textView==nextVisitObjectiveTextView && [appCtrl.FM_ENABLE_WORD_LIMIT isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:wordsLimit] && [appCtrl.IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY isEqualToString:KAppControlsYESCode] && !wordLimitValid) {
//        UIAlertAction* okAction = [UIAlertAction
//                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//                                   }];
//        UIAlertController * alert=   [UIAlertController
//                                      alertControllerWithTitle:@"Visit Conclusion"
//                                      message:[NSString stringWithFormat:@"Minimum %@ words required in visit conculsion notes",wordsLimit]
//                                      preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:okAction];
//        [self presentViewController:alert animated:YES completion:^{
//            [visitConclusionNotesTxtView becomeFirstResponder];
//        }];
//
//        return NO;
//    }
//    else if (textView==visitConclusionNotesTxtView && [appCtrl.FM_ENABLE_WORD_LIMIT isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:wordsLimit] && [appCtrl.IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY isEqualToString:KAppControlsYESCode] && !isNextVisitObjectiveEmpty && !wordLimitValidNextVisitObjective)
//    {
//        UIAlertAction* okAction = [UIAlertAction
//                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//                                   }];
//        UIAlertController * alert=   [UIAlertController
//                                      alertControllerWithTitle:@"Next Visit Objective"
//                                      message:[NSString stringWithFormat:@"Minimum %@ words required in next visit objective",wordsLimit]
//                                      preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:okAction];
//        [self presentViewController:alert animated:YES completion:^{
//            [nextVisitObjectiveTextView becomeFirstResponder];
//        }];
//
//        return NO;
//    }
//
//    else
//    {
//        return YES;
//    }
    return YES;

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==visitConclusionNotesTxtView) {
        
//        if ([appCtrl.FM_ENABLE_WORD_LIMIT isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:wordsLimit] && [appCtrl.IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY isEqualToString:KAppControlsYESCode]) {
//            BOOL wordLimitValid= [self validateWordLengthforTextView:textView.text];
//        if (!wordLimitValid) {
//
//            UIAlertAction* okAction = [UIAlertAction
//                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action)
//                                       {
//
//
//                                       }];
//
//            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
//
//            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Visit Conclusion" andMessage:[NSString stringWithFormat:@"Minimum %@ words required in visit conculsion notes",wordsLimit] andActions:actionsArray withController:self];
//        }
//        else
//        {
//            [visitDetailsDict setObject:visitConclusionNotesTxtView.text forKey:@"Visit_Conclusion_Notes"];
//        }
//        }
//        else{
//            [visitDetailsDict setObject:visitConclusionNotesTxtView.text forKey:@"Visit_Conclusion_Notes"];
//
//        }
        [visitDetailsDict setObject:visitConclusionNotesTxtView.text forKey:@"Visit_Conclusion_Notes"];

    }
    else if (textView==nextVisitObjectiveTextView)
    {
        
//        if ([appCtrl.FM_ENABLE_WORD_LIMIT isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:wordsLimit]) {
//            BOOL wordLimitValid= [self validateWordLengthforTextView:nextVisitObjectiveTextView.text];
//            if (!wordLimitValid) {
//                
//                UIAlertAction* okAction = [UIAlertAction
//                                           actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
//                                           style:UIAlertActionStyleDefault
//                                           handler:^(UIAlertAction * action)
//                                           {
//                                               
//                                               
//                                               
//                                           }];
//                
//                NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
//                
//                [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Next Visit Objective" andMessage:[NSString stringWithFormat:@"Minimum %@ words required in next visit objective",wordsLimit] andActions:actionsArray withController:self];
//            }
//            else
//            {
//                [visitDetailsDict setObject:nextVisitObjectiveTextView.text forKey:@"Next_Visit_Objective"];
//            }
//        }
//        else{
//            [visitDetailsDict setObject:nextVisitObjectiveTextView.text forKey:@"Next_Visit_Objective"];
//            
//        }
        [visitDetailsDict setObject:nextVisitObjectiveTextView.text forKey:@"Next_Visit_Objective"];

    }
    
    else if (textView==meetingAttendeesTxtView)
    {
        [visitDetailsDict setObject:meetingAttendeesTxtView.text forKey:@"Meeting_Attendees"];
    }
    
    if (CGRectEqualToRect(self.view.frame, oldFrame)) {
        
        
    }
    
    else
    {
        [UIView animateWithDuration:0.25f animations:^{
            
            
            
            self.view.frame=oldFrame;
            
        }];
        
    }
}


#pragma mark UITextView Delegate Methods


- (void)textViewDidBeginEditing:(UITextView *)textView
{
   // textView.text=@"";
    
     if (textView==meetingAttendeesTxtView)
        
     {
        
         NSLog(@"FRAMES IN DID BEGIN VIEW FRAME %@ OLD FRAME %@", NSStringFromCGRect(self.view.frame),NSStringFromCGRect(oldFrame));
         
        if (CGRectEqualToRect(self.view.frame, oldFrame)) {
            
       
        
        [UIView animateWithDuration:0.25f animations:^{
            
            float viewYPosition=self.view.frame.origin.y-398;
            
            CGRect currentViewFrame=CGRectMake(self.view.frame.origin.x, viewYPosition, self.view.frame.size.width, self.view.frame.size.height);
            
            self.view.frame=currentViewFrame;
            
        }];
        }
        

                 // [self animateTextField: textView up: YES];

        

    }
    else{
        
        if (CGRectEqualToRect(self.view.frame, oldFrame)) {
            [UIView animateWithDuration:0.25f animations:^{
                
                float viewYPosition=self.view.frame.origin.y-50;
                
                CGRect currentViewFrame=CGRectMake(self.view.frame.origin.x, viewYPosition, self.view.frame.size.width, self.view.frame.size.height);
                
                self.view.frame=currentViewFrame;
                
            }];
        }
    }
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && ([text isEqualToString:@" "] || [text isEqualToString:@"\n"])) {
        return NO;
    }
    
    if (textView==visitConclusionNotesTxtView) {
        return textView.text.length + (text.length - range.length) <= VisitConclusionNotesTextFieldLimit;

    }
    else
    {
        return textView.text.length + (text.length - range.length) <= MeetingAttendeestextFieldLimit;

    }
}



#pragma mark UIKeyboard Notifications



- (void)keyboardWillHide:(NSNotification *)notification {
    
    // [self.versionView setFrame:versionViewFrame];
    [UIView animateWithDuration:0.25f animations:^{
        
       
        
        self.view.frame=oldFrame;
        
    }];
    
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    
    
   
    
   }


- (void) animateTextField: (UITextView*) textField up: (BOOL) up
{
    const int movementDistance = 110; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}



#pragma mark Task and Notes Status Update

-(void)updateStatusforTasksandNotes

{
    
    NSString* locationType=[visitDetailsDict valueForKey:@"Location_Type"];
    
    NSString* locationID=[visitDetailsDict valueForKey:@"Location_ID"];
    
    NSInteger  taskCount=0;
    
    NSInteger notesCount=0;
    
    if ([locationType isEqualToString:@"D"]) {
        
        NSString* doctorID=[visitDetailsDict valueForKey:@"Doctor_ID"];
        
        
        NSMutableArray* taskCount=[MedRepQueries fetchTasksCountforLocationID:locationID andDoctorID:doctorID];
        
        if (taskCount.count>0) {
            
            for (NSInteger i=0; i<taskCount.count; i++) {
                
                NSMutableDictionary * currentDict=[taskCount objectAtIndex:i];
                
                NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:taskDueDate];
                
                
                BOOL testDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                
                BOOL colorSet=NO;
                
                
                if (testDate==YES) {
                    
                    NSLog(@"RED RED TASKS OVER DUE");
                    [visitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                    
                    [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
                    
                    colorSet=YES;
                }
                
                else
                {
                    NSLog(@"YELLOW");
                    
                    if (colorSet==YES) {
                        
                    }
                    else
                    {
                        [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
                    }
                    
                    [visitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                    
                    
                }
                
            }
            
        }
        
        
        
        notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andDoctorID:doctorID];
        
        NSLog(@"task count : %@ notes count : %d",taskCount,notesCount);
        
        
        if (taskCount>0) {
            [visitDetailsDict setValue:@"YES" forKey:@"Tasks_Available"];
        }
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Tasks_Available"];
            
        }
        if (notesCount>0)
            
        {
            [visitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
            
        }
        
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
            
        }
        
        
    }
    
    else if ([locationType isEqualToString:@"P"])
        
    {
        NSString* contactID=[visitDetailsDict valueForKey:@"Contact_ID"];
        
        NSMutableArray* tasksArray=[MedRepQueries fetchTaskCountforLocationID:locationID andContactID:contactID];
        
        if (tasksArray.count>0) {
            
            for (NSInteger i=0; i<tasksArray.count; i++) {
                
                NSMutableDictionary * currentDict=[tasksArray objectAtIndex:i];
                
                NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:taskDueDate];
                
                
                BOOL testDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                
                if (testDate==YES) {
                    
                    NSLog(@"RED RED TASKS OVER DUE");
                    [visitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                    
                    [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
                    
                }
                
                else
                {
                    NSLog(@"YELLOW");
                    
                    [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
                    
                    [visitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                    
                    
                }
                
            }
            
        }
        
        
        else if (tasksArray.count==0)
            
        {
            [taskStatusButton setBackgroundColor:[UIColor whiteColor]];
            
        }
        
        notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andContactID:contactID];
        
        
        
        
        
        NSLog(@"task count : %d notes count : %d",taskCount,notesCount);
        
        
        
        
        if (notesCount>0)
            
        {
            [visitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
            
        }
        
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
            
        }
        
        
        
    }
    
    
    if (notesCount>0) {
        
        notesStatusButton.backgroundColor=KNOTESAVAILABLECOLOR;
    }
    else
    {
        notesStatusButton.backgroundColor=KNOTESUNAVAILABLECOLOR;
        
    }
    
    
    
    //update task objects
    
    NSLog(@"task array while updating color  %@", self.visitDetails.taskArray);
    
    BOOL isColorUpdated=NO;
    
    
    for (VisitTask * task in self.visitDetails.taskArray) {
        
        
        
        NSString* taskDueDate=task.Start_Time;
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [[NSDate alloc] init];
        // voila!
        dateFromString = [dateFormatter dateFromString:taskDueDate];
        
        
        BOOL testTaskDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
        
      
        if ([task.Status isEqualToString:@"Closed"] && isColorUpdated==NO) {
            
            [taskStatusButton setBackgroundColor:[UIColor whiteColor]];
            
        }
   
        else   if (testTaskDate==YES && isColorUpdated==NO) {
            
            NSLog(@"task status color updated to red");
            
            [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
            isColorUpdated=YES;
            
        }
        
        else if (isColorUpdated==YES)
            
        {
            
        }
        
        else
        {
            [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
            NSLog(@"task status color updated to green");
            
        }
        
        
    }
    
    
    //update notes based on custom objects
    
    notesCount=self.visitDetails.notesArray.count;
    
    if (notesCount>0) {
        
        notesStatusButton.backgroundColor=KNOTESAVAILABLECOLOR;
    }
    else
    {
        notesStatusButton.backgroundColor=KNOTESUNAVAILABLECOLOR;
        
    }
    

    
    
}


#pragma Mark Task popover controller delegate

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray
{
    self.visitDetails.taskArray=tasksArray;
    appDelegate.CurrentRunningMedRepVisitDetails=self.visitDetails;
    NSLog(@"task array in popover did close in start call %@", self.visitDetails.taskArray);
    [self refreshTaskandNotesStatus:self.visitDetails];
    
}

-(void)notesDidClose:(NSMutableArray*)updatedNotes{
    self.visitDetails.notesArray=updatedNotes;
    appDelegate.CurrentRunningMedRepVisitDetails=self.visitDetails;
    if (self.visitDetails.notesArray.count>0) {
        //[self updateStatusforTasksandNotes];
        [self refreshTaskandNotesStatus:self.visitDetails];
    }
}

-(void)refreshTaskandNotesStatus:(VisitDetails*)currentVisitDetails
{
    
    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodeForTaskandNotes:currentVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }
    
}


#pragma mark UITextfield Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        //remove current doctor reversing object enumartor to remove object while being enumerated in loop
        
        
        for (Doctors * selectedDoctor in [visitDetails.doctorsforLocationArray reverseObjectEnumerator]) {
            
            if ([selectedDoctor.Doctor_ID isEqual:visitDetails.selectedDoctor.Doctor_ID]) {
                
                [visitDetails.doctorsforLocationArray removeObject:selectedDoctor];
                
            }
            
        }
        
        [self presentPopoverfor:meetingAttendeesTextField withTitle:kDoctorTitle withContent:[visitDetails.doctorsforLocationArray valueForKey:@"Doctor_Name"]];
    }
    else
    {
        
        for (Contact * selectedContact in [visitDetails.locationContactsArray reverseObjectEnumerator]) {
            
            if ([selectedContact.Contact_ID isEqual:visitDetails.selectedContact.Contact_ID]) {
                
                [visitDetails.locationContactsArray removeObject:selectedContact];
                
            }
            
        }

        
        [self presentPopoverfor:meetingAttendeesTextField withTitle:kContactTitle withContent:[visitDetails.locationContactsArray valueForKey:@"Contact_Name"]];
    }
    
    popOverTitle=kContactTitle;
    return NO;
}


-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    
    
    MedRepMeetingAttendeesViewController * popOverVC=[[MedRepMeetingAttendeesViewController alloc]init];
    popOverVC.meetingAttendeesArray=contentArray;
    popOverVC.selectedMeetingAttendeesArray=selectedMeetingAttendeesArray;
    popOverVC.delegate=self;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    meetingAttendeesPopOverController=nil;
    meetingAttendeesPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    meetingAttendeesPopOverController.delegate=self;
    popOverVC.meetingAttendeesPopOverController=meetingAttendeesPopOverController;
   // popOverVC.titleKey=popOverString;
    if ([popOverString isEqualToString:kDoctorTitle]) {
        [meetingAttendeesPopOverController setPopoverContentSize:CGSizeMake(400, 273) animated:YES];
        
    }
    else{
        [meetingAttendeesPopOverController setPopoverContentSize:CGSizeMake(400, 273) animated:YES];
        
    }
    [meetingAttendeesPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}



-(void)selectedMeetingAttendees:(NSMutableArray*)selectedAttendees

{
    NSLog(@"selected attendees are %@",selectedAttendees);
    
//    for (NSInteger i=0; i<selectedAttendees.count; i++) {
//        
//        NSString* currentAttendee =[selectedAttendees objectAtIndex:i];
//        
//        if ([selectedMeetingAttendeesArray containsObject:currentAttendee]) {
//            
//        }
//        else
//        {
//            [selectedMeetingAttendeesArray addObject:currentAttendee];
//        }
//    }
//    
    NSString* meetingAttendeesString=[[NSString alloc]init];
    
    
    if (selectedAttendees.count>0) {
        
        selectedMeetingAttendeesArray=selectedAttendees;
      meetingAttendeesString=  [selectedAttendees componentsJoinedByString:@","];

        meetingAttendeesTxtView.text=meetingAttendeesString;
        
        [visitDetailsDict setObject:meetingAttendeesString forKey:@"Meeting_Attendees"];
    }
    else
    {
        selectedMeetingAttendeesArray=selectedAttendees;
        meetingAttendeesString=  [selectedAttendees componentsJoinedByString:@","];
        
        meetingAttendeesTxtView.text=meetingAttendeesString;
        
        [visitDetailsDict removeObjectForKey:@"Meeting_Attendees"];
    }
    
    NSLog(@"meeting attendees atring is %@", meetingAttendeesString);
    
    
    if (meetingAttendeesArray.count>0) {
        
        [selectedMeetingAttendeesTblView reloadData];
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (meetingAttendeesArray.count>0) {
        
        return  meetingAttendeesArray.count;
 
    }
    else
    {
        if (tableView.tag == 1) {
            return demonstratedProductsArray.count;
            
        }
        
        else if(tableView.tag == 2)
        {
            return samplesGivenArray.count;
        }
        
        else
        {
            return 0;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (tableView.tag == 2) {
        
        return UITableViewAutomaticDimension;
    }
    else
    {
        return 45;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    if (tableView.tag==1)
    {
        
        ProductMediaFile *currentMedia = [demonstratedProductsArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString getValidStringValue:currentMedia.Caption];
        cell.textLabel.font = MedRepElementDescriptionLabelFont;
        cell.textLabel.textColor = MedRepDescriptionLabelFontColor;
        return cell;
    }
    else if (tableView.tag == 2)
    {
        static NSString *CellIdentifier = @"samplesCell";
        
        SamplesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SamplesTableViewCell" owner:self options:nil];
            
            if([[AppControl retrieveSingleton].ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode]){
                cell=[bundleArray objectAtIndex:1];
            } else {
                cell=[bundleArray objectAtIndex:0];
            }
        }
        tableView.allowsMultipleSelectionDuringEditing = NO;
        
        SampleProductClass *tempProduct = [samplesGivenArray objectAtIndex:indexPath.row];
        cell.expiryLbl.text=tempProduct.DisplayExpiryDate == nil ? @"N/A" : tempProduct.DisplayExpiryDate;
        cell.lotNumberLbl.text=tempProduct.productLotNumber;
        cell.titleLbl.text= tempProduct.productName;
        cell.qtyLbl.text=tempProduct.QuantityEntered;
        
        if([[AppControl retrieveSingleton].ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode]){
            cell.itemTypeLbl.text = tempProduct.ItemType;
        }
        return cell;
    }
    else
    {
        cell.textLabel.font=MedRepSingleLineLabelFont;
        
        cell.textLabel.textColor=MedRepDescriptionLabelFontColor;
        
        cell.textLabel.text=[SWDefaults getValidStringValue:[meetingAttendeesArray objectAtIndex:indexPath.row]];
        
        return cell;
    }
    
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete

        [meetingAttendeesArray removeObjectAtIndex:indexPath.row];
        
        [selectedMeetingAttendeesTblView reloadData];

    }
}
#pragma mark Signature Delegate Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
    CGPoint viewPoint = [pjrSignView convertPoint:locationPoint fromView:self.view];
    if ([pjrSignView pointInside:viewPoint withEvent:event]) {
        
        isSignatureCaptured = YES;
        NSLog(@"SIGNATURE CAPTURED");
    }
}

- (IBAction)saveButtonTapped:(id)sender
{
    NSLog(@"save button tapped in signature");
    
    //if signature already captured display the signature

    BOOL touchesBegan=[pjrSignView touchesBegan];
    
    UIImage* signImage=[pjrSignView getSignatureImage];
    NSData *imageData = UIImagePNGRepresentation(signImage);

    
    
    if (touchesBegan == YES ) {
        NSString* imageName=[visitDetailsDict valueForKey:@"Planned_Visit_ID"];
        [self signatureSaveImage:signImage withName:[NSString stringWithFormat:@"%@.jpg",imageName] ];
        [MedRepDefaults setDoctorSignature:imageData];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSObject *object = [prefs objectForKey:@"doctorSignature"];
       
        [pjrSignView setUserInteractionEnabled:NO];
        [signatureClearButton setHidden:YES];
        [signatureSaveButton setUserInteractionEnabled:NO];
        [signatureSaveButton UpdateImage:[UIImage imageNamed:@"Signature_Saved"] WithAnimation:YES];
        
    }
    
    else
    {
        
        
        //signature made optional
        UIAlertView* signatureAlert=[[UIAlertView alloc]initWithTitle:@"Missing Signature" message:@"Please provide signature" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        // [signatureAlert show];
        
    }
}
- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}
- (NSString*) dbGetImagesDocumentPath
{
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}
- (IBAction)eraseButtonTapped:(id)sender
{
    isSignatureCaptured=NO;
    
    if ([pjrSignView.previousSignature superview] || [pjrSignView.previousSignature superview]){
        [pjrSignView.lblSignature removeFromSuperview];
        
        [MedRepDefaults clearDoctorSignature];
        [pjrSignView.previousSignature removeFromSuperview];
    }
    [MedRepDefaults clearDoctorSignature];
    [pjrSignView clearSignature];
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"New_SignaturePenIcon"];
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    pjrSignView.lblSignature.attributedText = attachmentString;
    [signatureContainerView addSubview:pjrSignView];
}


@end
