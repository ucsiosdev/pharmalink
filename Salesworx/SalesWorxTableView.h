//
//  SalesWorxTableView.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxTableView : UITableView
- (void)reloadDataWithCompletion:(void (^)(void))completionBlock;

@property (nonatomic) IBInspectable BOOL hideSeparator;


@property (nonatomic, copy) void (^reloadDataCompletionBlock)(void);
@end
