//
//  SWTextViewCell.m
//  SWFoundation
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWTextViewCell.h"
#import "SWFoundation.h"

@implementation SWTextViewCell

@synthesize textView;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:reuseIdentifier]) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if(self.textView)
        {
            self.textView=nil;
        }
        [self setTextView:[[UITextView alloc] initWithFrame:CGRectZero] ];
        [self.textView setBackgroundColor:[UIColor clearColor]];
        [self.textView setFont:LightFontOfSize(14.0f)];
        [self.textView setDelegate:self];
        [self.contentView addSubview:self.textView];
    }
    
    return self;
}



#pragma mark UITableViewCell+Responder override
- (void)resignResponder {
    if ([self.textView isFirstResponder]) {
        [self.textView resignFirstResponder];
    }
}

#pragma mark UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // get the bounding rectangle of the content view
    CGRect bounds = [self.contentView bounds];
    
    [textView setFrame:CGRectInset(bounds, 10, 0)];
}

#pragma mark UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(editableCell:didUpdateValue:forKey:)]) {
        [self.delegate editableCell:self didUpdateValue:self.textView.text forKey:self.key];
    }
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(editableCellDidStartUpdate:)]) {
        [self.delegate editableCellDidStartUpdate:self];
    }
}
@end
