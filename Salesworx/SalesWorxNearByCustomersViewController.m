//
//  SalesWorxNearByCustomersViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxNearByCustomersViewController.h"
#import "SalesWorxCustomerStatementViewController.h"

@interface SalesWorxNearByCustomersViewController ()

@end

@implementation SalesWorxNearByCustomersViewController
@synthesize nearByCustomersMapView;

-(void)viewDidLoad
{
    [nearByCustomersCollectionView registerClass:[SalesWorxNearByCustomersCollectionViewCell class] forCellWithReuseIdentifier:@"nearByCell"];
    
    selectedDate = [NSDate date];
    
    customersDataDicsArray = [[NSMutableArray alloc]init];
    customersDataDicsArray = [self fetchRouteCustomersForSelectedDate];
    
}
-(void)updateNearByCustomersData:(Customer*)selectedCustomerObject
{
    
    if([selectedCustomer.Customer_ID isEqualToString:selectedCustomerObject.Customer_ID] && [selectedCustomer.Ship_Site_Use_ID isEqualToString:selectedCustomerObject.Ship_Site_Use_ID]){
        
        NSLog(@"Page already loaded with selecetd customer");
        
    }else{
        
        [nearByCustomersMapView removeAnnotations:nearByCustomersMapView.annotations];
        
        selectedCustomer=selectedCustomerObject;
        nearByCustomersArray=[[NSMutableArray alloc]init];
        
        customersDataDicsArray = [[NSMutableArray alloc]init];
        customersDataDicsArray = [self fetchRouteCustomersForSelectedDate];
        
        NSString *tempLat = [SWDefaults getValidStringValue:selectedCustomer.Cust_Lat];
        NSString *tempLong = [SWDefaults getValidStringValue:selectedCustomer.Cust_Long];
        
        NSLog(@"\n\n::::: selected customer lati: %@ , longi: %@  ::::::::\n\n",tempLat,tempLong);
        
        
        if ([NSString isEmpty:tempLat] && [NSString isEmpty:tempLong] ){
            
            isSelectedCustomerRealLocAvaialble = NO;
        }
        
        else if ( tempLat.integerValue == 0 && tempLong.integerValue == 0 ){
            
            isSelectedCustomerRealLocAvaialble = NO;
        }
        
        else {
            
            isSelectedCustomerRealLocAvaialble = YES;
        }
        
        //if selected customer's coordinates valid then only calculate near by customer, else NO
        if (isSelectedCustomerRealLocAvaialble){
            nearByParentView.hidden = NO;
            
            [self updateMapData];
            
            [self fetchNearByCustomers];
            
        }
        else {
            nearByParentView.hidden = YES;
        }
        
    }
}

-(NSMutableArray *)fetchRouteCustomersForSelectedDate
{
    NSMutableArray *arrCustomers = [[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate] mutableCopy];
    
    if(arrCustomers.count>0){
        NSLog(@"\n\n ::::: Route Customers are %@  ::::::\n\n", [arrCustomers valueForKey:@"Customer_Name"]);
    }else{
        NSLog(@"\n\n :::::  No Route Customers for date %@  ::::::\n\n", selectedDate);
    }
    
    return arrCustomers;
}

-(void)updateDate:(NSDate*)newDate{
    
    selectedDate = newDate;
    
}

-(void)updateMapData{
    
    double selectedCustLat = [[SWDefaults getValidStringValue:selectedCustomer.Cust_Lat] doubleValue];
    double selectedCustLong = [[SWDefaults getValidStringValue:selectedCustomer.Cust_Long] doubleValue];
    
    
    CLLocationCoordinate2D selectedCustomerCoordinate= CLLocationCoordinate2DMake(selectedCustLat, selectedCustLong);
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = selectedCustomerCoordinate;
    Pin.title=[SWDefaults getValidStringValue:selectedCustomer.Customer_Name];
    
    [nearByCustomersMapView removeAnnotations:nearByCustomersMapView.annotations];
    [nearByCustomersMapView addAnnotation:Pin];
    
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (selectedCustomerCoordinate, 500, 500);
    [nearByCustomersMapView setRegion:regionCustom animated:YES];
}

-(void)fetchNearByCustomers
{
    if(self.parentViewController != nil){
        [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        double selectedCustLat = [[SWDefaults getValidStringValue:selectedCustomer.Cust_Lat] doubleValue];
        double selectedCustLong = [[SWDefaults getValidStringValue:selectedCustomer.Cust_Long] doubleValue];
        
        
        NSMutableArray * customerListArray=[[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
        if (customerListArray.count>0) {
            
            for (NSMutableDictionary *currentDict in customerListArray) {
                NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                Customer * currentCustomer=[[Customer alloc] init];
                currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
                currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
                currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
                currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
                currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
                currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
                currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
                currentCustomer.City=[currentCustDict valueForKey:@"City"];
                currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
                currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
                currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
                currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
                currentCustomer.Cust_Lat=[currentCustDict valueForKey:@"Cust_Lat"];
                currentCustomer.Cust_Long=[currentCustDict valueForKey:@"Cust_Long"];
                currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
                currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
                currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
                currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
                currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
                currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
                currentCustomer.Customer_Segment_ID=[currentCustDict valueForKey:@"Customer_Segment_ID"];
                currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
                currentCustomer.Dept=[currentCustDict valueForKey:@"Dept"];
                currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
                currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
                currentCustomer.Postal_Code=[currentCustDict valueForKey:@"Postal_Code"];
                currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
                currentCustomer.SalesRep_ID=[currentCustDict valueForKey:@"SalesRep_ID"];
                currentCustomer.Sales_District_ID=[currentCustDict valueForKey:@"Sales_District_ID"];
                currentCustomer.Ship_Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]];
                currentCustomer.Ship_Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]];
                currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
                currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
                currentCustomer.isDelegatedCustomer=[currentCustDict valueForKey:@"isDelegatedCustomer"];
                currentCustomer.Area=[currentCustDict valueForKey:@"Area"];
                
                currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:@"CUST_LOC_RNG"];
                if ([currentCustomer.Customer_Name isEqualToString:selectedCustomer.Customer_Name] && [currentCustomer.Customer_No isEqualToString:selectedCustomer.Customer_No]) {
                    continue;
                }
                
                
                /*
                 
                 NEW CHANGE REQUEST ON 2 JULY 2019
                 Remove the static value of a nearby customer and set as dynamic which is coming in flag name NEARBY_CUSTOMERS_DISTANCE_THRESHOLD. if flag not available/nil then set 1000 meters and  If a customer's latitude and longitude both are 0 then show marker on the map as the current user's location.
                 
                 */
                
                
                SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
                AppControl *appControl = [AppControl retrieveSingleton];
                
                NSString *tempLatCurrentCust = [SWDefaults getValidStringValue:currentCustomer.Cust_Lat];
                NSString *tempLongCurrentCust = [SWDefaults getValidStringValue:currentCustomer.Cust_Long];
                
                if ([NSString isEmpty:tempLatCurrentCust] && [NSString isEmpty:tempLongCurrentCust] ){
                    
                    continue ;
                    /*
                     currentCustomer.Cust_Lat = [NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude] ;
                     currentCustomer.Cust_Long = [NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude] ;
                     */
                    
                }
                else  if (tempLatCurrentCust.integerValue == 0 && tempLongCurrentCust.integerValue == 0 ){
                    
                    continue;
                    /*
                     currentCustomer.Cust_Lat = [NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude] ;
                     currentCustomer.Cust_Long = [NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude] ;
                     */
                }
                
                
                CLLocationCoordinate2D customerLocation= CLLocationCoordinate2DMake([currentCustomer.Cust_Lat doubleValue], [currentCustomer.Cust_Long doubleValue]);
                CLLocation *selectedCustomerLocation=[[CLLocation alloc]initWithLatitude:selectedCustLat longitude:selectedCustLong];
                
                //original code
                //BOOL isInLocation=[self location:selectedCustomerLocation isNearCoordinate:customerLocation withRadius:1000];
                
                NSInteger nearByLocationRadiusInt = 1000;
                
                if (appControl.NEARBY_CUSTOMERS_DISTANCE_THRESHOLD != nil && appControl.NEARBY_CUSTOMERS_DISTANCE_THRESHOLD.length>0){
                    nearByLocationRadiusInt = [appControl.NEARBY_CUSTOMERS_DISTANCE_THRESHOLD integerValue];
                }
                
                
                BOOL isInLocation=[self location:selectedCustomerLocation isNearCoordinate:customerLocation withRadius:nearByLocationRadiusInt];
                
                
                if (isInLocation) {
                    NSLog(@"\n\n :::::::   Customer %@ is in range with coordinate %f %f  ::::::\n\n", currentCustomer.Customer_Name,customerLocation.latitude,customerLocation.longitude );
                    
                    CLLocation *custLocation = [[CLLocation alloc] initWithLatitude:[currentCustomer.Cust_Lat doubleValue] longitude:[currentCustomer.Cust_Long doubleValue]];
                    
                    CLLocationDistance distance = [appDelegate.currentLocation distanceFromLocation:custLocation];
                    
                    currentCustomer.distanceFromCurrentLocation=[NSString stringWithFormat:@"%f",distance];
                    
                    
                    NSString *lastVisitedDateStr=[[NSString alloc] init];
                    lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:currentCustomer];
                    currentCustomer.lastVisitedOn=[NSString getValidStringValue:lastVisitedDateStr];
                    
                    NSMutableArray* outStandingArray=[self fetchTotalOutStandingforCustomer:currentCustomer.Customer_ID];
                    if (outStandingArray.count>0) {
                        NSMutableDictionary* outstandingDict=[outStandingArray objectAtIndex:0];
                        NSString* outStandingString=[NSString getValidStringValue:[outstandingDict valueForKey:@"Total"]];
                        currentCustomer.totalOutStanding=outStandingString;
                    }
                    
                    NSString *tempCustName = [SWDefaults getValidStringValue:currentCustomer.Customer_Name];
                    NSPredicate * matchTodayVisitPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_Name ==  \"%@\" and SELF.Customer_No ==  \"%@\"",tempCustName,currentCustomer.Customer_No]];
                    NSMutableArray * tempFilterData=[[customersDataDicsArray filteredArrayUsingPredicate:matchTodayVisitPredicate]mutableCopy];
                    
                    if (tempFilterData.count>0) {
                        currentCustomer.isTodayVisit = YES;
                        NSLog(@"\n\n ::::: Is customer today visit: YES  ::::\n\n");
                    }else {
                        currentCustomer.isTodayVisit = NO;
                        NSLog(@"\n\n ::::: Is customer today visit: NO  ::::\n\n");
                    }
                    
                    [nearByCustomersArray addObject:currentCustomer];
                }
                else {
                     NSLog(@"\n\n :::::::   Customer %@ is NOT in range with coordinate %f %f  ::::::\n\n", currentCustomer.Customer_Name,customerLocation.latitude,customerLocation.longitude );
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            for (Customer * tempCustomer in nearByCustomersArray) {
                
                CLLocationCoordinate2D tempCustomerLocation= CLLocationCoordinate2DMake([tempCustomer.Cust_Lat doubleValue], [tempCustomer.Cust_Long doubleValue]);
                
                MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
                Pin.coordinate = tempCustomerLocation;
                Pin.title = tempCustomer.Customer_Name;
                [nearByCustomersMapView addAnnotation:Pin];
            }
            
            
            
            [nearByCustomersCollectionView reloadData];
            if (nearByCustomersArray.count == 0) {
                nearByParentView.hidden = YES;
            } else {
                nearByParentView.hidden = NO;
            }
            
            
            if(self.parentViewController != nil){
                [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
            }else{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
        });
    });
    
    
    
    
}

-(NSMutableArray*)fetchTotalOutStandingforCustomer:(NSString*)selectedCustomerID
{
    NSString * outStandingQry=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"Select IFNULL(Total_Due,'N/A')  AS Total, IFNULL(Over_Due,'N/A') AS Over_Due, IFNULL(PDC_Due,'N/A') AS PDC_Due from TBL_Customer_Dues where Customer_ID = '%@'",selectedCustomerID]];
    
    NSLog(@"total outstanding qry %@", outStandingQry);
    
    NSMutableArray *temp=  [[[SWDatabaseManager retrieveManager] fetchDataForQuery:outStandingQry] mutableCopy];
    
    if (temp.count>0) {
        
        return temp;
    }
    else
    {
        return nil;
    }
}
- (BOOL)location:(CLLocation *)location isNearCoordinate:(CLLocationCoordinate2D)currentCoordinate withRadius:(CLLocationDistance)radius
{
    CLCircularRegion *circularRegion = [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius identifier:@"radiusCheck"];
    
    return [circularRegion containsCoordinate:currentCoordinate];
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKPinAnnotationView *mapPin = nil;
    if(annotation != map.userLocation)
    {
        static NSString *defaultPinID = @"defaultPin";
        mapPin = (MKPinAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if (mapPin == nil )
        {
            mapPin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                     reuseIdentifier:defaultPinID];
            mapPin.canShowCallout = YES;
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [infoButton addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            mapPin.rightCalloutAccessoryView = infoButton;
        }
        else
        {
            mapPin.annotation = annotation;
        }
        
        if([[annotation title] isEqualToString:selectedCustomer.Customer_Name])
        {
            mapPin.pinColor = MKPinAnnotationColorGreen;
        } else {
            mapPin.pinColor = MKPinAnnotationColorRed;
        }
    }
    return mapPin;
}

-(void)infoButtonTapped:(id)sender
{
    
}

- (void)mapView:(MKMapView *)currentMapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* custLatValue;
    NSString* custLongValue;
    
    if (currentMapView==nearByCustomersMapView) {
        
        if (currentMapView.selectedAnnotations.count>0) {
            MKPointAnnotation * selectedannotation= (MKPointAnnotation*)[currentMapView.selectedAnnotations objectAtIndex:0];
            custLatValue=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%f",selectedannotation.coordinate.latitude]];
            custLongValue=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%f",selectedannotation.coordinate.longitude]];
        }
        else{
            
        }
        NSLog(@"selected annotations are %@", currentMapView.selectedAnnotations);
    }
    else{
        if (![NSString isEmpty:selectedCustomer.Cust_Lat]) {
            custLatValue =[NSString stringWithFormat:@"%@",selectedCustomer.Cust_Lat];
            custLongValue=[NSString stringWithFormat:@"%@",selectedCustomer.Cust_Long];
        }
    }
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        NSString* googleMapsUrlString;
        
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication]openURL:googleMapsUrl];
            
        }
    }
    
    else
    {
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
        else
        {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICOllectionview methods

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(350, 170);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  nearByCustomersArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"nearByCell";
    SalesWorxNearByCustomersCollectionViewCell *cell = (SalesWorxNearByCustomersCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Customer *currentCustomer =  [nearByCustomersArray objectAtIndex:indexPath.row];
    
    cell.customerNameLbl.text=[NSString getValidStringValue:currentCustomer.Customer_Name];
    cell.customerNumberLbl.text=[NSString getValidStringValue:currentCustomer.Customer_No];
    cell.outStandingLbl.text=[[NSString getValidStringValue:currentCustomer.totalOutStanding] currencyString];
    
    NSString* distanceString=[NSString getValidStringValue:currentCustomer.distanceFromCurrentLocation];
    
    cell.distanceLbl.text=[NSString isEmpty:distanceString] || [distanceString integerValue]==0?@"":[NSString stringWithFormat:@"%ldm",(long)[distanceString integerValue]];
    cell.lastVisitedOnLbl.text=currentCustomer.lastVisitedOn;
    
    
    
    
    if (currentCustomer.isTodayVisit){
        cell.lblInRoute.hidden = NO;
        cell.lblInRoute.textColor = [UIColor whiteColor];
        cell.lblInRoute.layer.cornerRadius=6.0;
        cell.lblInRoute.clipsToBounds=YES;
    }
    else {
        cell.lblInRoute.hidden = YES;
    }
    
    
    return cell;
}


-(void)cellInfoButtonTapped:(id)sender
{
    UIButton* selectedButton=sender;
    
    Customer * selectedCustomer=[nearByCustomersArray objectAtIndex:selectedButton.tag];
    
    NSLog(@"selected customer is %@",selectedCustomer.Customer_Name);
}

-(void)updateCustomerLastVisitDetails:(Customer*)selectCust{
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SalesWorxCustomerStatementViewController *statementVC = [[SalesWorxCustomerStatementViewController alloc]init];
    statementVC.selectedCustomer = [nearByCustomersArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:statementVC animated:YES];
}

@end

