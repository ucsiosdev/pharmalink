




//sales order updated code




//
//  SalesOrderNewViewController.m
//  Salesworx
//
//  Created by Saad Ansari on 11/19/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SalesOrderNewViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TamplateNameViewController.h"

#define NUMERIC                 @"1234567890"

@interface SalesOrderNewViewController ()

@end

@implementation SalesOrderNewViewController
@synthesize parentViewController,customerDict,preOrdered,preOrderedItems,isOrderConfirmed,wareHouseDataSalesOrderArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

//- (void)dealloc
//{
//    oldSalesOrderVC.delegate = nil;
//    NSLog(@"Dealloc SalesOrderNewViewController.h %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title= @"Sales Order";
    
    
    
    if ([[[[SWDefaults appControl] objectAtIndex:0]valueForKey:@"ALLOW_SPECIAL_DISCOUNT"] isEqualToString:@"Y"]) {
        
        specialDiscountstaticLbl.hidden=NO;
        specialDiscountContentLbl.hidden=NO;
    }
    else
    {
        specialDiscountstaticLbl.hidden=YES;
        specialDiscountContentLbl.hidden=YES;
    }
    
    
   // longDescriptionButton.hidden=YES;

            [longDescriptionButton setBackgroundImage:[UIImage imageNamed:@"Old_ProductDescription_DisableIcon"] forState:UIControlStateNormal];
    
    
    //table view crash animation reported by client visit fixed
    productTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.view.backgroundColor = [UIColor whiteColor ];
    isPinchDone = NO;
    
    NSLog(@"product cat in SO %@",[SWDefaults productCategory]);
    
    if ([self.parentViewString isEqualToString:@"SO"])
    {
        
        oldSalesOrderVC = [[SalesOrderViewController alloc] initWithCustomer:self.customerDict andCategory:[SWDefaults productCategory]] ;
    }
    else if ([self.parentViewString isEqualToString:@"MO"]) {
        oldSalesOrderVC = [[SalesOrderViewController alloc] initWithCustomer:self.customerDict andOrders:self.preOrdered andManage:self.isOrderConfirmed];
        
        
       // oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
        
        NSLog(@"ware house data in old sales order %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
        
        //OLA!!
        if ([self.isOrderConfirmed isEqualToString:@"confirmed"])
        {
            [addButton setHidden:YES];
            [txtDefBonus setUserInteractionEnabled:NO];
            [txtDiscount setUserInteractionEnabled:NO];
            [txtfldWholesalePrice setUserInteractionEnabled:NO];
            [txtFOCQty setUserInteractionEnabled:NO];
            [txtNotes setUserInteractionEnabled:NO];
            [txtProductQty setUserInteractionEnabled:NO];
            [txtRecBonus setUserInteractionEnabled:NO];
            [focProductBtn setUserInteractionEnabled:NO];
        }
    }
    else if ([self.parentViewString isEqualToString:@"TO"]) {
        oldSalesOrderVC = [[SalesOrderViewController alloc] initWithCustomer:self.customerDict andTemplate:self.preOrdered andTemplateItem:self.preOrderedItems] ;
    }
    oldSalesOrderVC.view.frame = bottomOrderView.bounds;
    oldSalesOrderVC.view.clipsToBounds = YES;
    
    
    
    // moving ware house data to old sales order
    
    NSLog(@"data before moving %@", [wareHouseDataSalesOrderArray description]);
    oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
    
    NSLog(@"data after moving %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
    
    
    [bottomOrderView addSubview:oldSalesOrderVC.view];
    [oldSalesOrderVC didMoveToParentViewController:self];
    [self addChildViewController:oldSalesOrderVC];
    
    
        //fetching array of products query has been modified by harpal on 24.01.2016 as per aki requirement to add product description and sorting products based on ranking
    
    
    
    NSLog(@"app control flags in sales order enable prod rank  %@ : %@ ", [[SWDefaults appControl]valueForKey:@"ENABLE_PROD_RANK"],[[SWDefaults appControl]valueForKey:@"SHOW_PROD_LONG_DESC"]);
    
    
    enableProductRank= [NSString stringWithFormat:@"%@",[[[SWDefaults appControl] objectAtIndex:0]valueForKey:@"ENABLE_PROD_RANK"]];
    
    showProductLongDescription=[NSString stringWithFormat:@"%@",[[[SWDefaults appControl] objectAtIndex:0]valueForKey:@"SHOW_PROD_LONG_DESC"]];
    
    NSLog(@"test string flags %@ %@", enableProductRank,showProductLongDescription);
    
    NSLog(@"category in sales order %@",[SWDefaults productCategory]);
    
    if ([enableProductRank isEqualToString:@"Y"] || [showProductLongDescription isEqualToString:@"Y"]) {
        
        NSLog(@"PRODUCT DESC FLAG ENABLED");
        productArray=[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager]fetchProductswithRanking:[SWDefaults productCategory]]];
        
       // NSLog(@"refined product with ranking %@", [productArray description]);
        
        
    }
    
    else
    {
        longDescriptionButton.hidden=YES;
        
        productArray =[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:[SWDefaults productCategory]]];
       // NSLog(@"products array %@ and category %@", productArray,[SWDefaults productCategory]);

    }

    
    
    
    
    filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray ) {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Brand_Code"]];
        if ( theMutableArray == nil ) {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Brand_Code"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    
   // NSLog(@"section headings %@", [finalProductArray description]);
    
    _collapsedSections = [NSMutableSet new];
    
    bSearchIsOn = NO;
    [self borderView:bonusView];
    [self borderView:stockView];
    [self borderView:salesHistoryView];
    [self borderView:dragParentView];
    // [self borderView:dragButton];
    
    //changed to [customerDict stringForKey:@"Customer_Name"]
    //    NSLog(@"OLA!!******customer name %@",[customerDict stringForKey:@"Customer_Name"]);
    //    NSLog(@"OLA!!******customer name %@",[[SWDefaults customer] stringForKey:@"Customer_Name"]);
    lblCustomerLabel.text = [customerDict stringForKey:@"Customer_Name"];
    lblCustomerLabel.font = RegularFontOfSize(26);
    lblProductName.font = BoldSemiFontOfSize(16);
    lblProductCode.font =BoldSemiFontOfSize(12);
    lblProductBrand.font=BoldSemiFontOfSize(12);
    lblWholesalePrice.font=BoldSemiFontOfSize(12);
    txtfldWholesalePrice.font=BoldSemiFontOfSize(12);
    lblRetailPrice.font=BoldSemiFontOfSize(12);
    lblUOMCode.font=BoldSemiFontOfSize(12);
    lblAvlStock.font=BoldSemiFontOfSize(12);
    lblExpDate.font=BoldSemiFontOfSize(12);
    lblPriceLabel.font=BoldSemiFontOfSize(12);
    specialDiscountContentLbl.font=BoldSemiFontOfSize(12);
    defaultDiscountLbl.font=BoldSemiFontOfSize(12);
    
    
    
    //    txtProductQty.font=LightFontOfSize(14);
    //    txtDefBonus.font=LightFontOfSize(14);
    //    txtRecBonus.font=LightFontOfSize(14);
    //    txtDiscount.font=LightFontOfSize(14);
    //    txtFOCQty.font=LightFontOfSize(14);
    //    txtNotes.font=LightFontOfSize(14);
    
    lblCustomerLabel.textColor = UIColorFromRGB(0x4A5866);
    lblProductName.textColor = UIColorFromRGB(0x4687281);
    lblProductCode.textColor =UIColorFromRGB(0x4687281);
    lblProductBrand.textColor=UIColorFromRGB(0x4687281);
    lblWholesalePrice.textColor=UIColorFromRGB(0x4687281);
    txtfldWholesalePrice.textColor=UIColorFromRGB(0x4687281);
    lblRetailPrice.textColor=UIColorFromRGB(0x4687281);
    lblUOMCode.textColor=UIColorFromRGB(0x4687281);
    lblAvlStock.textColor=UIColorFromRGB(0x4687281);
    lblExpDate.textColor=UIColorFromRGB(0x4687281);
    lblPriceLabel.textColor=UIColorFromRGB(0x4687281);
    specialDiscountContentLbl.textColor=UIColorFromRGB(0x4687281);
    defaultDiscountLbl.textColor=UIColorFromRGB(0x4687281);
    
    
    txtProductQty.textColor=UIColorFromRGB(0x4687281);
    txtDefBonus.textColor=UIColorFromRGB(0x4687281);
    txtRecBonus.textColor=UIColorFromRGB(0x4687281);
    txtDiscount.textColor=UIColorFromRGB(0x4687281);
    txtFOCQty.textColor=UIColorFromRGB(0x4687281);
    txtNotes.textColor=UIColorFromRGB(0x4687281);
    txtNotes.textColor = [UIColor darkTextColor ];
    
    
    candySearchBar.backgroundColor = UIColorFromRGB(0xF6F7FB);
    
    lblTitleProductCode.textColor=UIColorFromRGB(0x4A5866);
    lblTitleWholesalePrice.textColor=UIColorFromRGB(0x4A5866);
    lblTitleRetailPrice.textColor=UIColorFromRGB(0x4A5866);
    lblTitleUOMCode.textColor=UIColorFromRGB(0x4A5866);
    lblTitleAvlStock.textColor=UIColorFromRGB(0x4A5866);
    lblTitleExpDate.textColor=UIColorFromRGB(0x4A5866);
    txtTitleProductQty.textColor=UIColorFromRGB(0x4A5866);
    txtTitleDefBonus.textColor=UIColorFromRGB(0x4A5866);
    txtTitleRecBonus.textColor=UIColorFromRGB(0x4A5866);
    txtTitleDiscount.textColor=UIColorFromRGB(0x4A5866);
    txtTitleFOCQty.textColor=UIColorFromRGB(0x4A5866);
    txtTitleFOCItem.textColor=UIColorFromRGB(0x4A5866);
    txtTitleNotes.textColor=UIColorFromRGB(0x4A5866);
    specialDiscountstaticLbl.textColor=UIColorFromRGB(0x4A5866);
    defaultDiscountTitleLbl.textColor=UIColorFromRGB(0x4A5866);
    
    
    lblTitleProductCode.font=LightFontOfSize(14);
    lblTitleWholesalePrice.font=LightFontOfSize(14);
    lblTitleRetailPrice.font=LightFontOfSize(14);
    lblTitleUOMCode.font=LightFontOfSize(14);
    lblTitleAvlStock.font=LightFontOfSize(14);
    lblTitleExpDate.font=LightFontOfSize(14);
    txtTitleProductQty.font=LightFontOfSize(14);
    txtTitleDefBonus.font=LightFontOfSize(14);
    txtTitleRecBonus.font=LightFontOfSize(14);
    txtTitleDiscount.font=LightFontOfSize(14);
    txtTitleFOCQty.font=LightFontOfSize(14);
    txtTitleFOCItem.font=LightFontOfSize(14);
    txtTitleNotes.font=LightFontOfSize(14);
    specialDiscountstaticLbl.font=LightFontOfSize(14);
    defaultDiscountTitleLbl.font=LightFontOfSize(14);
    
    
    lblSelectProduct.font=BoldSemiFontOfSize(20);
    
    dragParentView.backgroundColor = UIColorFromRGB(0x343F5F);
    focProductBtn.titleLabel.font =BoldSemiFontOfSize(14);
    
    [txtNotes.layer setCornerRadius:7.0f];
    [txtNotes.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [txtNotes.layer setBorderWidth:1.0f];
    
    //    [txtfldWholesalePrice setPlaceholder:[NSString stringWithFormat:@"(in %@)",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]]];
    [txtfldWholesalePrice setDelegate:self];
    [txtfldWholesalePrice setKeyboardType:UIKeyboardTypeDecimalPad];
    [txtfldWholesalePrice setHidden:YES];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeVisit:)] ];
    
}


#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (alertView.tag==1000000) {
        
        noProductSelectedAlert=nil;
    }
    
   else if (alertView.tag==100 || alertView.tag==101) {
        
    }
    
    else if (alertView.tag==1000)
        
    {
        
   
            
            
            if (buttonIndex==0) {
                //yes tapped
                [self.navigationController  popViewControllerAnimated:YES];
            }
            else
            {
                
            }
        
        
    }
    
    else{
        
        
        [self.navigationController  popViewControllerAnimated:YES];
        
    }
    
    
    
    //    switch (buttonIndex) {
    //        case 0: {
    //            [self.navigationController  popViewControllerAnimated:YES];
    //            break;
    //        }
    //        default:
    //            break;
    //    }
    
}

- (void)closeVisit:(id)sender {
    
    UIAlertView* closeAlert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert", nil) message:NSLocalizedString(@"Would you like to close this order?", nil) delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    
    closeAlert.tag=1000;
   [closeAlert show];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    NSLog(@"ware house data in sales order %@", [wareHouseDataSalesOrderArray description]);
    
    
    single = [Singleton retrieveSingleton];
    NSLog(@"singleton data %hhd %@", single.isManageOrder,self.isOrderConfirmed);
    
    
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
    {
        saveOrderBtn.hidden=YES;
        loadFromMslBtn.hidden=YES;
        saveTempleteBtn.hidden=YES;
    }
   
    else
    {
        saveOrderBtn.hidden=NO;
        loadFromMslBtn.hidden=NO;
        saveTempleteBtn.hidden=NO;
    }
    if ([[[[SWDefaults appControl] objectAtIndex:0]valueForKey:@"ENABLE_ITEM_DISCOUNT"] isEqualToString:@"Y"]) {
        
        txtDiscount.userInteractionEnabled=YES;
    }
    else
    {
        txtDiscount.userInteractionEnabled=NO;
    }
    
    
    if (!single.isDistributionChecked)
    {

    }
    else
    {
            [loadFromMslBtn setEnabled:NO];
    }

    
    
  
    OrderAdditionalInfoViewController * orderVC=[[OrderAdditionalInfoViewController alloc]init];
    
    orderVC.warehouseDataArray=wareHouseDataSalesOrderArray ;
    
    
    NSLog(@"ware house data being pushed to final destination %@", [orderVC.warehouseDataArray description]);
   
    //pushing from new sales order to old sales order
  
    
    
    self.navigationController.toolbarHidden = YES;
    oldSalesOrderVC.delegate = self;
    //txtfldWholesalePrice.delegate=self;
    
    
    
    if ([[[[SWDefaults appControl] objectAtIndex:0] valueForKey:@"ALLOW_SPECIAL_DISCOUNT"] isEqualToString:@"Y"]) {
        defaultDiscountTitleLbl.hidden=NO;
        defaultDiscountLbl.hidden=NO;
        }
        else
        {
            NSLog(@"hiding default discount title");
            defaultDiscountTitleLbl.hidden=YES;
            defaultDiscountLbl.hidden=YES;
        }
    
    
    AppControl *appControl = [AppControl retrieveSingleton];
    ENABLE_TEMPLATES = appControl.ENABLE_TEMPLATES;
    
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
    {
        
        
        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
        {
            //[self setupToolbar:[NSArray arrayWithObjects:saveTemplateButton,flexibleSpace, flexibleSpace, totalLabelButton, nil]];
            saveTempleteBtn.hidden=NO;
        }
        else
        {
            // [self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, flexibleSpace, totalLabelButton, nil]];
            saveTempleteBtn.hidden=YES;
            
        }
        loadFromMslBtn.hidden=YES;
        saveTempleteBtn.hidden = YES;//OLA!!
    }
    else
    {
        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
        {
            //[self setupToolbar:[NSArray arrayWithObjects:saveTemplateButton,flexibleSpace,flexibleSpace,totalLabelButton, saveButton, nil]];
            saveTempleteBtn.hidden=NO;
            
        }
        else
        {
            //[self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, flexibleSpace, totalLabelButton, saveButton, nil]];
            saveTempleteBtn.hidden=YES;
            
        }
    }
}


-(void)viewDidDisappear:(BOOL)animated
{
    oldSalesOrderVC.delegate = nil;
    txtfldWholesalePrice.delegate=nil;
    productStockViewController.delegate=nil;
}
-(void)borderView:(UIView *)view
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowPath = shadowPath.CGPath;
    [view.layer setBorderColor:UIColorFromRGB(0xE6E6E6).CGColor];
    [view.layer setBorderWidth:2.0f];
    
}

#pragma mark - UITableView

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            return 1;
            
        }
        else
        {
            return [finalProductArray count];
        }
    }
    else
    {
        return 1;
        
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return [filteredCandyArray count];
            
        } else {
            NSArray *tempDict =[productDictionary objectForKey:[finalProductArray objectAtIndex:section]];
            
            return [_collapsedSections containsObject:@(section)] ? [tempDict count] :0 ;
            
        }
    }
    else
    {
        return [productArray count];
        
    }
    
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.01f;
//}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return 0;
        }
        else
        {
            return 40.0;
        }
    }
    else
    {
        return 0;
    }
    
}



//CRASH FIXED
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//}



- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    if (tv.tag==1)
    {
        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            
            UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
            result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
            [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            result.backgroundColor=UIColorFromRGB(0xE0E5EC);
            [result setTitle:[[row objectAtIndex:0] stringForKey:@"Brand_Code"] forState:UIControlStateNormal];
            result.tag = s;
            [result.layer setBorderColor:[UIColor whiteColor].CGColor];
            [result.layer setBorderWidth:1.0f];
            result.titleLabel.font=BoldSemiFontOfSize(16);
            [result setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
            
            [headerView addSubview:result];
            
            return headerView;
        }
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.numberOfLines=2;
        cell.textLabel.font=RegularFontOfSize(5);
        cell.backgroundColor=UIColorFromRGB(0xF6F7FB);
        cell.textLabel.textColor=UIColorFromRGB(0x687281);
        cell.textLabel.textColor=[UIColor darkTextColor ];
        cell.textLabel.font  =  LightFontOfSize(14) ;
    }
    
    
    
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            NSDictionary * row = [filteredCandyArray objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
    }
    else
    {
        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [row stringForKey:@"Description"];
        if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
        {
            cell.textLabel.textColor = [UIColor redColor];
        }
        else
        {
            cell.textLabel.textColor=[UIColor darkTextColor ];
        }
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *produstDetail;

    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    if (tableView.tag==1)
    {
        
        txtProductQty.text=@"";
        txtDefBonus.text=@"";
        txtRecBonus.text=@"";
        txtDiscount.text=@"";
        txtFOCQty.text=@"";
        txtNotes.text=@"";
        [addButton setTitle:@"Add" forState:UIControlStateNormal];
        
        NSDictionary * row ;
        if (bSearchIsOn)
        {
            row = [filteredCandyArray objectAtIndex:indexPath.row];
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            row= [objectsForCountry objectAtIndex:indexPath.row];
        }
        
        
        //check product details based on flag to display long description for product
        
        
        
        if ([enableProductRank isEqualToString:@"Y"] || [showProductLongDescription isEqualToString:@"Y"]) {

            produstDetail=[[SWDatabaseManager retrieveManager] fetchProductDetailswithRanking:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
           
            
        

        }
        
        else
        {
           produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
        }
        
        
        
       // NSLog(@"selected product details are %@", produstDetail);

        
        NSString* specialDiscount=[[SWDatabaseManager retrieveManager] dbGetSpecialDiscount:[[produstDetail objectAtIndex:0] valueForKey:@"Inventory_Item_ID"]];
        
        NSLog(@"special discount for selected product code : %@ is %@",[row valueForKey:@"Item_Code"], specialDiscount);
        
         specialDiscountPercent=[specialDiscount doubleValue];
        
        specialDiscountContentLbl.text=[NSString stringWithFormat:@"%0.2f", specialDiscountPercent];
        
        
        mainProductDict=nil;
        
        
        mainProductDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];

        
        //fteching main product dict
        
        
        
        [mainProductDict setValue:[[produstDetail objectAtIndex:0]valueForKey:@"Discount"] forKey:@"Default_Discount"];
        defaultDiscountLbl.text=[NSString stringWithFormat:@"%0.2f",[[mainProductDict valueForKey:@"Default_Discount"] doubleValue]];
        

        
                Singleton *single = [Singleton retrieveSingleton];

        if([single.visitParentView isEqualToString:@"MO"] )
        {
           
 
        }
        
        else
        {
            
                        NSLog(@"default discount is being set here 681");

        }
        
        NSLog(@"check default discont %@",[mainProductDict valueForKey:@"Default_Discount"]);

        
        NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
        [mainProductDict setValue:itemID forKey:@"ItemID"];
        
        
        [mainProductDict setValue:specialDiscountContentLbl.text forKey:@"Special_Discount"];
        
        isDidSelectStock=YES;
        [stockBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
        [stockBtn setSelected:YES];
        
        
        //NSLog(@"OLA!!********* product %@",[produstDetail objectAtIndex:0]);
        
        if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
            [lblWholesalePrice setHidden:YES];
            [txtfldWholesalePrice setHidden:NO];
        }
        else
        {
            [lblWholesalePrice setHidden:NO];
            [txtfldWholesalePrice setHidden:YES];
        }
        // NSLog(@"Product %@",row);
        
        [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];
        
        
        
        [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
         
        
//        [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"]]];
    }
    else
    {
        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
        if (![row objectForKey:@"Guid"])
        {
            [row setValue:[NSString createGuid] forKey:@"Guid"];
        }
        if([fBonusProduct objectForKey:@"Qty"])
        {
            [row setValue:[fBonusProduct objectForKey:@"Qty"] forKey:@"Qty"];
        }
        fBonusProduct=[row mutableCopy];
        [focProductBtn setTitle:[row stringForKey:@"Description"] forState:UIControlStateNormal];
        [self buttonAction:focProductBtn];
        //[tableView reloadData];
    }
    // bonusView = productBonusViewController.view;
    
    
    
    
    if ([[mainProductDict valueForKey:@"Long_Description"]length]==0) {
        
        [longDescriptionButton setBackgroundImage:[UIImage imageNamed:@"Old_ProductDescription_DisableIcon"] forState:UIControlStateNormal];
        
       // longDescriptionButton.hidden=YES;
    }
    
    else
    {
        [longDescriptionButton setBackgroundImage:[UIImage imageNamed:@"Old_ProductDescriptionIcon"] forState:UIControlStateNormal];
        //longDescriptionButton.hidden=NO;
    }
    
    NSLog(@"product details with ranking in did select row are %@", produstDetail);
    
    

}

#pragma mark chil view delegate

-(void)productDeleted:(NSMutableDictionary*)selectedProduct
{
    //product deleted by swiping
    
    NSLog(@"main product dict after deleting product in delegate %@", mainProductDict);
    
    mainProductDict=nil;
    
    
    
    lblProductName.text = @"Product Name";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    
    specialDiscountContentLbl.text=@"";
    
    defaultDiscountLbl.text=@"";
    
    
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
    [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
    mainProductDict = nil;
    bonusProduct = nil;
    fBonusProduct = nil;
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
    
}





- (void)selectedProduct:(NSMutableDictionary *)product
{
    
    
    NSLog(@"product selected in MO %@", product);
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    mainProductDict=nil;
    mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
    
    
    NSLog(@"main product dict is delegate %@", mainProductDict);
    
    txtfldWholesalePrice.text = @"";
    
    
    
    
    if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
        [lblWholesalePrice setHidden:YES];
        [txtfldWholesalePrice setHidden:NO];
        

    }
    else
    {
        [lblWholesalePrice setHidden:NO];
        [txtfldWholesalePrice setHidden:YES];
    }
    
    
    if ([mainProductDict objectForKey:@"Bonus_Product"]) {
        bonusProduct=[mainProductDict objectForKey:@"Bonus_Product"];
    }
    
    if ([mainProductDict objectForKey:@"FOC_Product"]) {
        fBonusProduct=[mainProductDict objectForKey:@"FOC_Product"];
        
    }
    NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
    [mainProductDict setValue:itemID forKey:@"ItemID"];
    
    
    NSString* updatedWholesalePrice;
    
    single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])

    {
        
        
        NSLog(@"check is order confirmed %@", self.isOrderConfirmed);
        
        NSLog(@"check net price in confrimed order %@", [mainProductDict valueForKey:@"Unit_Selling_Price"]);
        
        updatedWholesalePrice= [mainProductDict valueForKey:@"Unit_Selling_Price"] ;

    }
  
    else
    {
    
   updatedWholesalePrice= [mainProductDict valueForKey:@"Net_Price"] ;
    }
                                                                             
    
   
    
    [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];
//    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"]]];
    
     [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
    
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    NSLog(@"quantity text is %@",txtProductQty.text);
    txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
    txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
    txtDiscount.text=[NSString stringWithFormat:@"%0.2f",[[mainProductDict stringForKey:@"Discount"] doubleValue]];
    NSLog(@"discount text set here 900");
    txtFOCQty.text=[fBonusProduct stringForKey:@"Qty"];
    txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
    
    
    NSString* primaryUOMString =[mainProductDict valueForKey:@"Primary_UOM_Code"];
   
    NSLog(@"main product dict in sales order new vc %@ ", mainProductDict);

    
    
    if (primaryUOMString) {
        
        lblUOMCode.text=primaryUOMString;
    }
    else
    {
        lblUOMCode.text=[mainProductDict valueForKey:@"UOM"];
    }
    
    
    if (txtfldWholesalePrice.isHidden==NO) {
        
        
        
        
         NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
        
        
        
        NSLog(@"updated wholase price case 2 %@", updatedWholesalePrice);
        
        
        [txtfldWholesalePrice setText: [NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,[updatedWholesalePrice doubleValue]]];
        
    }

    
    [addButton setTitle:@"Update" forState:UIControlStateNormal];
    
    
    
    if ([single.visitParentView isEqualToString:@"MO"]) {
        
        double tempActualPrice;
        
        NSString* uspStr=[NSString stringWithFormat:@"%@",[mainProductDict valueForKey:@"Unit_Selling_Price"]];
        
        
        if ([uspStr isEqualToString:@"0"]) {
            
            tempActualPrice=[[mainProductDict valueForKey:@"Net_Price"] doubleValue]*[[mainProductDict valueForKey:@"Qty"] doubleValue];
        }
        
        else
        {
            tempActualPrice=[[mainProductDict valueForKey:@"Unit_Selling_Price"] doubleValue]*[[mainProductDict valueForKey:@"Qty"] doubleValue];
        }
        
        
        
        
        
        
         [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),tempActualPrice ,NSLocalizedString(@"Discounted Price", nil),[[mainProductDict valueForKey:@"Discounted_Price"] doubleValue] ,NSLocalizedString(@"Discounted Amount", nil), [[mainProductDict valueForKey:@"Discounted_Amount"] doubleValue]]];
    }
    else
    {
        
        [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),[[mainProductDict valueForKey:@"Price"] doubleValue] ,NSLocalizedString(@"Discounted Price", nil),[[mainProductDict valueForKey:@"Discounted_Price"] doubleValue] ,NSLocalizedString(@"Discounted Amount", nil), [[mainProductDict valueForKey:@"Discounted_Amount"] doubleValue]]];
    }

    
    
    
    NSLog(@"price label set in 964 %@",mainProductDict);

    
    
    if([single.visitParentView isEqualToString:@"MO"] )
    {
        
        
      NSMutableArray *  productDetails = [[[SWDatabaseManager retrieveManager] dbGetProductDetail:[mainProductDict stringForKey:@"ItemID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"]] mutableCopy];
        
        NSLog(@"product details in MO %@", productDetails);
        
        defaultDiscountLbl.text=[NSString stringWithFormat:@"%0.2f", [[[productDetails objectAtIndex:0] valueForKey:@"Discount"] doubleValue]];
        
        
        [mainProductDict setValue:[[productDetails objectAtIndex:0] valueForKey:@"Discount"] forKey:@"Default_Discount"];
        
    }

    else
    {
        defaultDiscountLbl.text=[NSString stringWithFormat:@"%0.2f",[[mainProductDict valueForKey:@"Default_Discount"] doubleValue]];

    }
    NSLog(@"default discount is being set here 938");
    
    
    if([single.visitParentView isEqualToString:@"MO"] )
    {
       // txtDiscount.text=[NSString stringWithFormat:@"%@",[mainProductDict valueForKey:@"Discount"]];

    
    }
    
    else
    {
        txtDiscount.text=[NSString stringWithFormat:@"%0.2f",[[mainProductDict valueForKey:@"Applied_Discount"] doubleValue]];

        NSLog(@"discount text set here 978");

    }
    
    //do thing
}

- (void) dbGetSelectedProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    
    
    //OLA!! fetching lot info for selected item
    
    
    
    //fetching product detaisl with ranking
    
    NSArray *stockInfo;
    
    
    if ([enableProductRank isEqualToString:@"Y"] || [showProductLongDescription isEqualToString:@"Y"]) {
        
        stockInfo=[[SWDatabaseManager retrieveManager] fetchProductDetailswithRanking:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"]];
        
    }
    
    else
    {
       stockInfo = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"]];
    }
    

    
    
    
    
    NSLog(@"OLA!!******* stock info %@", stockInfo);
    
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Lot_Qty"] forKey:@"Lot_Qty"];
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Expiry_Date"] forKey:@"Expiry_Date"];
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"UOM"] forKey:@"UOM"];
  //  [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Primary_UOM_Code"] forKey:@"Primary_UOM_Code"];
    
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
                    [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            
            //checking the net price junk value
            
            
                   [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else
        {
            
            NSLog(@"no prices alert called 1095");

            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
        }
    }
    else
    {
        
        
        NSLog(@"no prices alert called 1103");

        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        
        //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    // [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Unit_List_Price"] currencyString]];
    // [lblRetailPrice setText:[[mainProductDict stringForKey:@"Unit_Selling_Price"] currencyString]];
    [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [txtfldWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
    [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
    [lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
    // [lblExpDate setText:[mainProductDict stringForKey:@"Expiry_Date"]];
    
    lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
}



- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    if([priceDetail count]!=0)
    {
        
        
        addButton.enabled=YES;
        txtProductQty.enabled=YES;
        txtDefBonus.enabled=YES;
        txtRecBonus.enabled=YES;
        if([isDiscountAppControl isEqualToString:@"Y"])//APP CONTROL
        {
               txtDiscount.enabled=YES;
        }
        else
        {
            txtDiscount.enabled=NO;
        }
        if ([self.isOrderConfirmed isEqualToString:@"confirmed"] || isFOCAllow ==NO)
        {
            txtFOCQty.enabled=NO;

        }
        else
        {
            txtFOCQty.enabled=YES;

        }
        
            
        focProductBtn.enabled=YES;
        txtNotes.userInteractionEnabled=YES;
        

        if ([[SWDefaults checkMultiUOM] isEqualToString:@"Y"]) {
            
            
        }
        
        
        
        
       
        
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            
           
             [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Unit_Selling_Price"];
        }
        else
        {
            
            NSLog(@"no prices alert called 1162");

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            
            
            //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
        }
    }
    else
    {
        
        NSLog(@"no prices alert called 1172");

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices available for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        
        addButton.enabled=NO;
        txtDiscount.enabled=NO;
        txtProductQty.enabled=NO;
        txtDefBonus.enabled=NO;
        txtRecBonus.enabled=NO;
        txtFOCQty.enabled=NO;
        focProductBtn.enabled=NO;
        txtNotes.userInteractionEnabled=NO;
        

        //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    // [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Unit_List_Price"] currencyString]];
    // [lblRetailPrice setText:[[mainProductDict stringForKey:@"Unit_Selling_Price"] currencyString]];
    [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    
    NSString* currencyCode=[[SWDefaults userProfile]valueForKey:@"Currency_Code"];
    
    
    
    [txtfldWholesalePrice setText:[ NSString stringWithFormat:@"%@%@",currencyCode,[mainProductDict stringForKey:@"Net_Price"]]];
    [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
   
  //  NSLog(@"main product dict while setting uom code is %@", mainProductDict);
    
    
    
    
    
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] )
    {
        NSString* specialDiscount=[[SWDatabaseManager retrieveManager] dbGetSpecialDiscount:[mainProductDict valueForKey:@"Inventory_Item_ID"]];
        
        
        specialDiscountPercent=[specialDiscount doubleValue];
        
        [mainProductDict setValue:[NSString stringWithFormat:@"%0.2f",specialDiscountPercent] forKey:@"Special_Discount"];
        
        [mainProductDict setValue:[mainProductDict valueForKey:@"Discount"] forKey:@"Default_Discount"];
        
        //to be done default discount
        
        
        
//        txtDiscount.text=[NSString stringWithFormat:@"%0.2f", [[mainProductDict valueForKey:@"Discount"] doubleValue]];
        
        NSLog(@"discount text set here 1149");


    }

    if (specialDiscountContentLbl.hidden==NO) {
        
        specialDiscountContentLbl.text=[NSString stringWithFormat:@"%@", [mainProductDict valueForKey:@"Special_Discount"]];
        
    }
    
    
    [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
    
    
    
    if ([[mainProductDict stringForKey:@"Lot_Qty"] floatValue]==0.0)
    {
        [lblAvlStock setText:@"N/A"];
    }
    else
    {
        [lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
    }
    
    if ([[mainProductDict stringForKey:@"Expiry_Date"] floatValue]==0.0)
    {
        lblExpDate.text = @"N/A";
    }
    else
    {
        lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
    }
    
    
   // NSLog(@"geniric price is %@", mainProductDict);
    
    
    
    
    //[lblExpDate setText:[mainProductDict stringForKey:@"Expiry_Date"]];
}
- (void)stockAllocated:(NSMutableDictionary *)p {
    mainProductDict = nil;
    mainProductDict=[NSMutableDictionary dictionaryWithDictionary:p];
}
-(void)sectionButtonTouchUpInside:(UIButton*)sender {
    [productTableView beginUpdates];
    int section = sender.tag;
    
    
    
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (!shouldCollapse) {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        int numOfRows = [productTableView numberOfRowsInSection:section];
        
        NSInteger tempcount=_collapsedSections.count;
        
        
        
        
       
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
       
        
        
        
        
    }
    else {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        NSInteger collapedSectionsCount=_collapsedSections.count;

        
        for (NSInteger i=0; i<collapedSectionsCount; i++) {
            
            NSArray *myArray = [_collapsedSections allObjects];
            
            NSInteger sectionVal=[[myArray objectAtIndex:i] integerValue];
            int numOfRows = [productTableView numberOfRowsInSection:sectionVal];
            NSArray* indexPaths = [self indexPathsForSection:sectionVal withNumberOfRows:numOfRows];
            [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [_collapsedSections removeObject:@(sectionVal)];
            
        }

        
        
        
        
        NSArray *tempDict =[productDictionary objectForKey:[finalProductArray objectAtIndex:section]];
        int numOfRows = [tempDict count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    
    [productTableView endUpdates];
    //[_tableView reloadData];
}

#pragma mark - UISearchBar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        //[searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0];
        bSearchIsOn = NO;
        [productTableView reloadData];
        [productTableView setScrollsToTop:YES];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar
{
    bSearchIsOn = YES;
    [candySearchBar resignFirstResponder];
    int len = [ [candySearchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length];
    if (len > 0)
    {
        NSString *searchText = candySearchBar.text;
        
        if ([searchText length] > 0)
        {
            [filteredCandyArray removeAllObjects];
            NSDictionary *element=[NSDictionary dictionary];
            for(element in productArray)
            {
                NSString *customerName = [element objectForKey:@"Description"];
                NSRange r = [customerName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (r.length > 0)
                {
                    [filteredCandyArray addObject:element];
                }
            }
            NSMutableDictionary *sectionSearch = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in filteredCandyArray)
            {
                NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionSearch allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in filteredCandyArray)
            {
                [[sectionSearch objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each isDualBonusAppControlsection array
            for (NSString *key in [sectionSearch allKeys])
            {
                [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
            }
            [productTableView reloadData];
        }
        
    }
    else
    {
        [ candySearchBar resignFirstResponder ];
    }
}

#pragma mark - Button Action
- (void)getProductServiceDiddbGetBonusInfo:(NSArray *)info{
    isBonusRowDeleted=NO;
    bonusInfo=nil;
    bonusInfo=[NSArray arrayWithArray:info];
    AppControl *appControl = [AppControl retrieveSingleton];
    isBonusAppControl = appControl.ALLOW_BONUS_CHANGE;
    isFOCAppControl = appControl.ENABLE_MANUAL_FOC;
    isDiscountAppControl = appControl.ALLOW_DISCOUNT;
    isDualBonusAppControl = appControl.ALLOW_DUAL_FOC;
    isFOCItemAppControl=appControl.ALLOW_MANUAL_FOC_ITEM_CHANGE;
    
    if([isFOCAppControl isEqualToString:@"Y"])
    {
        isFOCAllow=YES;
        checkFOC=YES;
    }
    else
    {
        if ([isDualBonusAppControl isEqualToString:@"N"])
        {
            isFOCAllow=NO;
            checkFOC=NO;
        }
    }
    
    if([isDiscountAppControl isEqualToString:@"Y"])
    {
        isDiscountAllow=YES;
        checkDiscount = YES;
    }
    else
    {
        isDiscountAllow=YES;
        checkDiscount = NO;
    }
    
    if([isFOCItemAppControl isEqualToString:@"Y"])
    {
        isFOCItemAllow=YES;
    }
    else
    {
        isFOCItemAllow=NO;
    }
    
    if([isDualBonusAppControl isEqualToString:@"Y"])
    {
        isDualBonusAllow=YES;
    }
    else
    {
        isDualBonusAllow=NO;
    }
    
    if (bonusInfo.count > 0)
    {
        isBonus = YES;
        if([isBonusAppControl isEqualToString:@"Y"] || [isBonusAppControl isEqualToString:@"I"] || [isBonusAppControl isEqualToString:@"D"])
        {
            isBonusAllow = YES;
            checkBonus = YES;
        }
        else
        {
            isBonusAllow = NO;
            checkBonus = NO;
        }
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        isBonus = NO;
        bonus=0;
        isBonusAllow = NO;
        checkBonus = NO;
    }
    bonusEdit = @"N";
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    if([mainProductDict objectForKey:@"FOC_Product"])
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"FOC_Product"]];
    }
    else
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:mainProductDict];
        [fBonusProduct removeObjectForKey:@"Qty"];
        [fBonusProduct removeObjectForKey:@"Bonus_Product"];
        [fBonusProduct removeObjectForKey:@"StockAllocation"];
    }
    
    if([mainProductDict objectForKey:@"Bonus_Product"])
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"Bonus_Product"]];
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[mainProductDict stringForKey:@"Item_Code"]]];
        [bonusProduct removeObjectForKey:@"Qty"];
        [bonusProduct removeObjectForKey:@"FOC_Product"];
        [bonusProduct removeObjectForKey:@"StockAllocation"];
    }
    [focProductBtn setTitle:[fBonusProduct stringForKey:@"Description"] forState:UIControlStateNormal];
    
    //focProductBtn.titleLabel.text = [fBonusProduct stringForKey:@"Description"];
    //[tableView reloadData];
    info=nil;
    if(isBonusAllow)//APP CONTROL
    {
        if (bonus<=0)
        {
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        else{
            [txtRecBonus setUserInteractionEnabled:YES];
        }
    }
    else
    {
        [txtRecBonus setUserInteractionEnabled:NO];
    }
}

- (void)calculateBonus {
    NSLog(@"calculating bonus ");
    
    
    int quantity = 0;
    bonus = 0;
    NSDictionary *tempBonus;
    BOOL isBonusGroup = YES;
    if ([[AppControl retrieveSingleton].ENABLE_BONUS_GROUPS isEqualToString:@"Y"])
    {
        NSString *query = [NSString stringWithFormat:@"Select A.* from TBL_BNS_Promotion AS A INNER JOIN TBL_Customer_Addl_Info As B ON B.Attrib_Value=A.BNS_Plan_ID Where B.Customer_ID = '%@' AND B.Site_Use_ID = '%@'",[customerDict stringForKey:@"Customer_ID"],[customerDict stringForKey:@"Site_Use_ID"]];
        if ([[SWDatabaseManager retrieveManager] fetchDataForQuery:query].count==0)
        {
            isBonusGroup = NO;
        }
        else
        {
            isBonusGroup = YES;
        }
    }
    else
    {
        isBonusGroup = YES;
    }
    
    if (isBonusGroup) {
        if ([mainProductDict objectForKey:@"Qty"])
        {
            quantity = [[mainProductDict objectForKey:@"Qty"] intValue];
            for(int i=0 ; i<[bonusInfo count] ; i++ )
            {
                NSDictionary *row = [bonusInfo objectAtIndex:i];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    
                    if(checkBonus)
                    {
                        isBonusAllow = YES;
                    }
                    if ([[AppControl retrieveSingleton].BONUS_MODE isEqualToString:@"DEFAULT"])
                    {
                        if(i == [bonusInfo count]-1)
                        {
                            if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                            {
                                int dividedValue = quantity / rangeStart ;
                                bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                                tempBonus=row;
                            }
                            
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                            {
                                int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                                bonus = floor(dividedValue) ;
                                tempBonus=row;
                                
                            }
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"POINT"])
                            {
                                bonus = [[row objectForKey:@"Get_Qty"] intValue];
                                tempBonus=row;
                                
                            }
                        }
                        else
                        {
                            bonus = [[row objectForKey:@"Get_Qty"] intValue];
                            tempBonus=row;
                        }
                        
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue] + floorf(([[mainProductDict objectForKey:@"Qty"] intValue] - rangeStart) *[[row objectForKey:@"Get_Add_Per"] floatValue]);
                        tempBonus=row;
                    }
                    break;
                }
                else
                {
                    if(checkBonus)
                    {
                        isBonusAllow = NO;
                    }
                }
            }
        }
        
        if (bonus > 0 && bonusInfo)
        {
            NSLog(@"given bonus is %d", bonus);
            
            if(![[tempBonus stringForKey:@"Get_Item"] isEqualToString:[bonusProduct stringForKey:@"Item_Code"]])
            {
                NSArray *temBonusArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:[NSString stringWithFormat:@"select * from TBL_Product where Item_Code ='%@'",[tempBonus stringForKey:@"Get_Item"]]];
                NSLog(@"bonus product before setting it to nill %@", bonusProduct);
                
                bonusProduct=nil;
                bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[temBonusArray objectAtIndex:0]];
                [bonusProduct removeObjectForKey:@"Qty"];
                [bonusProduct removeObjectForKey:@"FOC_Product"];
                [bonusProduct removeObjectForKey:@"StockAllocation"];
            }
            
            NSLog(@"setting bonus data %@", bonusProduct);
            
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Def_Qty"];
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Qty"];
            
            if(!isDualBonusAllow)
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct removeObjectForKey:@"Def_Qty"];
                
            }
            if(isDiscountAllow)
            {
                [mainProductDict setValue:@"0" forKey:@"Discount"];
            }
        }
        else
        {
            [bonusProduct removeObjectForKey:@"Qty"];
            [bonusProduct removeObjectForKey:@"Def_Qty"];
            
        }
        
    }
    
}

- (NSString*)stringByKeepingOnlyCharactersInString:(NSString *)string {
    #define ACCEPTABLE_CHARACTERS @"0123456789."
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];

    
    return filtered;
    
}
- (void)updatePrice{
    
    //NSLog(@"OLA!!******* mainProduct at begin updatePrice %@",mainProductDict);
    
    if ([mainProductDict stringForKey:@"Qty"].length > 0)
    {
        int qty = [[mainProductDict stringForKey:@"Qty"] intValue];
        if (qty > 0)
        {
            
            NSMutableString *wholesalePrice;
            
            
            if (txtfldWholesalePrice.text) {
                if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""]){
                    
                    
                    //unit price issue 1,000 to 1 fixed  by syed (Comma issue)
                    NSString * stringValue = txtfldWholesalePrice.text;
                    NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                    
                    
                    
                    // All the characters to remove
                    NSMutableCharacterSet *characterSet = [NSMutableCharacterSet letterCharacterSet];
                    [characterSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    // Build array of components using specified characters as separtors
                    NSArray *arrayOfComponents = [stringValue1 componentsSeparatedByCharactersInSet:characterSet];
                    
                    // Create string from the array components
                    NSString *strOutput = [arrayOfComponents componentsJoinedByString:@""];
                    
                    NSLog(@"New string: %@", strOutput);
//
                    
                    
                    
                    
                    
                    
                    
                    NSString *filtered = [self stringByKeepingOnlyCharactersInString:stringValue1];

                    
                    
                    
                    
                    
//                    NSString *newString= [self stringByKeepingOnlyCharactersInString:stringValue1];
                    
                    
                    NSLog(@"updated new string is %@", filtered);
                    
                    
                    wholesalePrice=[NSMutableString stringWithFormat:@"%@", filtered];
                    
                   // wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
                }
                else
                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
            }
            else
                wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
            
            
            NSLog(@"updated price in case 1 %@", [mainProductDict stringForKey:@"Net_Price"]);
            
            
            
            float totalPrice = (double)qty * ([wholesalePrice floatValue]);
            
            NSLog(@"total val %f",[wholesalePrice doubleValue]);
            //            NSLog(@"Price %f",[[mainProductDict stringForKey:@"Net_Price"] doubleValue]);
            //            NSLog(@"Price %f",(double)qty);
            //            NSLog(@"Price %f",totalPrice);
            if(bonus<=0 || !checkDiscount)
            {
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                
                if ([self.parentViewString isEqualToString:@"MO"]) {
                    
                    double tempActualPrice;
                    
                    NSString* uspStr=[NSString stringWithFormat:@"%@",[mainProductDict valueForKey:@"Unit_Selling_Price"]];
                    
                    
                    if ([uspStr isEqualToString:@"0"]) {
                        
                          tempActualPrice=[[mainProductDict valueForKey:@"Net_Price"] doubleValue]*[[mainProductDict valueForKey:@"Qty"] doubleValue];
                    }
                    
                    else
                    {
                         tempActualPrice=[[mainProductDict valueForKey:@"Unit_Selling_Price"] doubleValue]*[[mainProductDict valueForKey:@"Qty"] doubleValue];
                    }
                    
       
                    
                        
                                         
                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),tempActualPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                    NSLog(@"price label set in 1764 %@",[mainProductDict description]);

                    
                }
                

                else
                {
                
                
                
                
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                    
                [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                    NSLog(@"price label set in 1782");

            }
            }
            
            else
            {
//                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
//                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Discounted_Price"];
//                
//                //why si discount hardcoded as 0.0?
//                
//                [mainProductDict setValue:@"0.00" forKey:@"Discounted_Amount"];
//                

                
                
                
                
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                
                
                NSLog(@"check discount amount in main product dict %@", [mainProductDict valueForKey:@"Discounted_Amount"]);
                
                if ([single.visitParentView isEqualToString:@"MO"]) {
                    
                    
                    NSString* uspStr=[NSString stringWithFormat:@"%@",[mainProductDict valueForKey:@"Unit_Selling_Price"]];
                    
                   double tempActualPrice;
                    
                    
                    if ([uspStr isEqualToString:@"0"]) {
                        
                        tempActualPrice=[[mainProductDict valueForKey:@"Net_Price"] doubleValue]*[[mainProductDict valueForKey:@"Qty"] doubleValue];
                    }
                    
                    else
                    {
                        tempActualPrice=[[mainProductDict valueForKey:@"Unit_Selling_Price"] doubleValue]*[[mainProductDict valueForKey:@"Qty"] doubleValue];
                    }
                    

                    
                   
                    
                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),tempActualPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                    

                    
                }
                
                else
                {
                    
               
                
                
                [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                
                    
                }
                NSLog(@"price label set in 1815");

                
            }
        }
        else{
            [lblPriceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Item Price", nil), [mainProductDict currencyStringForKey:@"Price"]]];
            NSLog(@"price label set in 1823");

            
        }
        //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    }
    else
    {
        [lblPriceLabel setText:@""];
        //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
    
    //NSLog(@"OLA!!******* mainProduct at end updatePrice %@",mainProductDict);
    
    

    
    
    

}

-(void)saveAsTemplateAction:(id)sender
{
    NSLog(@"Save As Template");
}
-(IBAction)resetAction:(id)sender
{
    lblProductName.text = @"Product Name";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    specialDiscountContentLbl.text=@"";
    defaultDiscountLbl.text=@"";
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
    [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
    mainProductDict = nil;
    bonusProduct = nil;
    fBonusProduct = nil;
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
}



-(void)ResetData
{
}


-(IBAction)saveOrderAction:(id)sender
{
    NSLog(@"wholesale price is %@", txtfldWholesalePrice.text);
    
    if ([self.isOrderConfirmed isEqualToString:@"confirmed"]) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Confirmed orders cannot be edited." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    [txtProductQty resignFirstResponder];
    [txtDefBonus resignFirstResponder];
    [txtRecBonus resignFirstResponder];
    [txtDiscount resignFirstResponder];
    [txtFOCQty resignFirstResponder];
    [txtNotes resignFirstResponder];
    
    
    
    
    
    if (![txtProductQty.text isEqualToString:@""]) {
        
        [txtfldWholesalePrice resignFirstResponder];

        [self saveProductOrder];
        
        [oldSalesOrderVC productAdded:mainProductDict];
        mainProductDict=nil;
        fBonusProduct= nil;
        bonusProduct = nil;
        [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
        
        [productTableView deselectRowAtIndexPath:[productTableView indexPathForSelectedRow] animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please enter quantity." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil) message:@"Please enter quantity." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
    }
    
}

- (void)saveProductOrder {
    
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
    isSaveOrder = YES;
    int totalBonus = 0 ;
    [mainProductDict removeObjectForKey:@"FOC_Product"];
    [mainProductDict removeObjectForKey:@"Bonus_Product"];
    
    [mainProductDict setValue:bonusProduct forKey:@"Bonus_Product"];
    [mainProductDict setValue:fBonusProduct forKey:@"FOC_Product"];
    
    BOOL isBothBonus = NO;
    if([bonusProduct objectForKey:@"Qty"])
    {
        totalBonus = [[bonusProduct stringForKey:@"Qty"] intValue];
        if(!isDualBonusAllow)
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
        else if([fBonusProduct objectForKey:@"Qty"])
        {
            isBothBonus = YES;
            totalBonus = totalBonus + [[fBonusProduct stringForKey:@"Qty"] intValue];
        }
        else
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
    }
    else
    {
        [mainProductDict removeObjectForKey:@"Bonus_Product"];
    }
    
    if([fBonusProduct objectForKey:@"Qty"] && !isBothBonus)
    {
        totalBonus = [[fBonusProduct stringForKey:@"Qty"] intValue];
    }
    else if(!isBothBonus)
    {
        [mainProductDict removeObjectForKey:@"FOC_Product"];
    }
    
    
    [mainProductDict setValue:[NSString stringWithFormat:@"%d",totalBonus] forKey:@"Def_Bonus"];
    [mainProductDict setValue:[mainProductDict stringForKey:@"Discount"] forKey:@"DiscountPercent"];
    
    if(![mainProductDict objectForKey:@"StockAllocation"])
    {
        // [mainProductDict setValue:stockItems forKey:@"StockAllocation"];
    }
    else
    {
    }
    //NSLog(@"Product %@",mainProductDict);
    //[self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    //[self.navigationController  popViewControllerAnimated:YES];
    
    
    if(txtfldWholesalePrice.text)
    {
        
        NSMutableString *wholesalePrice;
        if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""])
        {
            
             NSLog(@"OLA!!***** WP 2 %@",txtfldWholesalePrice.text );
            
            //unit price fix 1,000 to 1 fixed by syed (comma issue)
            NSString * stringValue = txtfldWholesalePrice.text;
            NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            
            NSLog(@"wholesale price before string removal is %@", stringValue1);

            
            
            
            //this guy has used substringfrom index 3 if its AED11250.00 it shows 11245, if its $11250 it shows 250 so changing the logic here to check if string has alphabets then remove them so that it wont effect for currency signs like $
            
            
            
            NSRegularExpression *regex = [[NSRegularExpression alloc]
                                           initWithPattern:@"[a-zA-Z]" options:0 error:NULL];
            
            // Assuming you have some NSString `myString`.
            NSUInteger matches = [regex numberOfMatchesInString:stringValue1 options:0
                                                          range:NSMakeRange(0, [stringValue1 length])];
            
            if (matches > 0) {
                // `myString` contains at least one English letter.
                
                
            }
            else
            {
                //this string dones not have alphabets in prefix ex AED, AQ etc
                
                
                
            }
            
            
            
            //removing alphabets from price
            
//            
//            NSString *newString = [[stringValue1 componentsSeparatedByCharactersInSet:
//                                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
//                                   componentsJoinedByString:@""];
//            
//            
//            
//            
////            wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
//            
//
//            
//            NSLog(@"wholesale price after string removal is %@", wholesalePrice);
//            
            
            
            
            NSString *filtered = [self stringByKeepingOnlyCharactersInString:stringValue1];

            
            wholesalePrice=[NSMutableString stringWithFormat:@"%@", filtered];

            
            
            //NSString *wholesalePrice = [txtfldWholesalePrice.text substringFromIndex:3];
            [mainProductDict setObject:wholesalePrice forKey:@"Net_Price"];
        }
        
    }
    
    
    
    if ([[[[SWDefaults appControl] objectAtIndex:0] valueForKey:@"ALLOW_SPECIAL_DISCOUNT"] isEqualToString:@"Y"]) {
        
        NSLog(@"calculate discount  called from save product price");
        
        [self calculateDiscount];
        
       
        
    }
    else
    {
       
    }
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    lblProductName.text = @"Product Name";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    specialDiscountContentLbl.text=@"";
    defaultDiscountLbl.text=@"";
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
    
    
    
   
}


-(void)calculateDiscount
{
    
    
    NSLog(@"calculating special discount for %@", mainProductDict);
    
    //NSLog(@"bonus given is %d", bonus);
   
    //NSLog(@"foc product is %@", fBonusProduct);
    
    NSLog(@"bonus product in calculate discount %@", bonusProduct);
    
    
    //[self calculateBonus];

    //check if foc is given
    
    NSString * focQtyStr=[fBonusProduct objectForKey:@"Qty"];
    
    BOOL isFOCGiven=NO;
    
    if (focQtyStr == (id)[NSNull null] || focQtyStr.length == 0 )
    {
        
    }
    else
    {
        isFOCGiven=YES;
    }
    
    NSLog(@"price of product is %@",[mainProductDict valueForKey:@"Price"]);
    
    double usp =[[mainProductDict valueForKey:@"Net_Price"] doubleValue];
    
    double updatedPrice=[[mainProductDict valueForKey:@"Discounted_Price"] doubleValue];
    
    
    //fetch default discount
    NSMutableArray *  productDetails = [[[SWDatabaseManager retrieveManager] dbGetProductDetail:[mainProductDict stringForKey:@"ItemID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"]] mutableCopy];
    
   // NSLog(@"product details in discount calculation %@", productDetails);

    double defaultDiscount=0.0;
    
    if (productDetails>0) {
        
        //this is default discount
        defaultDiscount=[[[productDetails objectAtIndex:0] valueForKey:@"Discount"] doubleValue];
        
        NSLog(@"default discount in discount calculation %0.2f", defaultDiscount);
        
    }
    
    
    //double defaultDiscount=[[mainProductDict valueForKey:@"Default_Discount"] doubleValue];
    //
    NSInteger orderQty=[[mainProductDict valueForKey:@"Qty"] integerValue];
    
    double netPrice=orderQty*usp;
    
    NSString* allowDiscount=[[[SWDefaults appControl] objectAtIndex:0] valueForKey:@"ALLOW_DISCOUNT"];
   
    NSString* allowSpecialDiscount=[[[SWDefaults appControl] objectAtIndex:0] valueForKey:@"ALLOW_SPECIAL_DISCOUNT"];
    
    double pricewithSpecialDiscount=netPrice-netPrice*(specialDiscountPercent/100);
    
    double pricewithDefaultDiscount=netPrice-netPrice*(defaultDiscount/100);
    
    double consolidatedDiscount=specialDiscountPercent+defaultDiscount;
    
    double pricewithDefaultandSpecialDiscount=netPrice-netPrice*(consolidatedDiscount/100);
    
    
    BOOL applyDefaultDiscount=NO;
    
    BOOL applySpecialDiscount=NO;
    
    BOOL applyDefaultandSpecialDiscounts=NO;
    
    if ([allowDiscount isEqualToString:@"Y"]) {
        NSLog(@"entered discount percent is %0.2f", defaultDiscount);
    }
    else
    {
        defaultDiscount=0.0;
    }
    if ([allowSpecialDiscount isEqualToString:@"Y"]) {
        NSLog(@"allowed special discount is %0.2f",specialDiscountPercent);
    }
    else
    {
        specialDiscountPercent =0.0;
    }
    
    NSLog(@"check bonus and foc %d %hhd", bonus,isFOCGiven);
    
    if (specialDiscountPercent>0 && defaultDiscount>0) {
        if (bonus>0 || isFOCGiven==YES) {
            updatedPrice=pricewithSpecialDiscount;
            applySpecialDiscount=YES;
        }
        else
        {
            //as per client requirement on 22/3/2016 if DefaultDiscount>0 and SpecialDiscount>0 and NoBonus given appled discount should be Default discount+ special disocunt.
            
            updatedPrice=pricewithDefaultandSpecialDiscount;
            applyDefaultandSpecialDiscounts=YES;
        }
    }
    else if (specialDiscountPercent>0 && defaultDiscount<=0)
    {
            updatedPrice=pricewithSpecialDiscount;
            applySpecialDiscount=YES;
    }
    else if (specialDiscountPercent<=0&& defaultDiscount>0)
    {
        if (bonus>0 || isFOCGiven==YES) {
            updatedPrice=usp*orderQty;
        }
        else
        {
            updatedPrice=pricewithDefaultDiscount;
            applyDefaultDiscount=YES;
        }
    }
    else if (specialDiscountPercent==0&& defaultDiscount==0)
    {
        updatedPrice=usp*orderQty;
    }
    
    
    NSLog(@"updated price is %0.2f", updatedPrice);
    
    double discountedAmount= (usp*orderQty)-updatedPrice;
    
    NSLog(@"discounted amount in main product dict is  %@", [mainProductDict valueForKey:@"Discounted_Amount"]);
    
    NSLog(@"discounted amount in calculate discount is %0.2f",discountedAmount);
    
    [mainProductDict setValue:[NSString stringWithFormat:@"%0.2f",discountedAmount] forKey:@"Discounted_Amount"];
    
    [mainProductDict setValue:[NSString stringWithFormat:@"%0.2f",updatedPrice] forKey:@"Discounted_Price"];
    
    
    
    if (applyDefaultDiscount==YES) {
        
        [mainProductDict setValue:[NSString stringWithFormat:@"%@",[mainProductDict valueForKey:@"Default_Discount"]] forKey:@"Applied_Discount"];
        
        [mainProductDict setValue:[NSString stringWithFormat:@"%@",[mainProductDict valueForKey:@"Default_Discount"]] forKey:@"Discount"];
        
        
    }
    else if (applySpecialDiscount ==YES)
    {
        [mainProductDict setValue:[NSString stringWithFormat:@"%0.2f",specialDiscountPercent] forKey:@"Applied_Discount"];
        
        [mainProductDict setValue:[NSString stringWithFormat:@"%0.2f",specialDiscountPercent] forKey:@"Discount"];
        
    }
    
    else if (applyDefaultandSpecialDiscounts==YES)
    {
        
        double updatedDiscount = specialDiscountPercent+defaultDiscount;
        
        NSLog(@"both default and special discount applied %f", updatedDiscount);
        
        [mainProductDict setValue:[NSString stringWithFormat:@"%0.2f",updatedDiscount] forKey:@"Applied_Discount"];
        
        [mainProductDict setValue:[NSString stringWithFormat:@"%0.2f",updatedDiscount] forKey:@"Discount"];
        
    }
    else
    {
        [mainProductDict setValue:@"0" forKey:@"Applied_Discount"];
        
        [mainProductDict setValue:@"0" forKey:@"Discount"];
    }
    
    txtDiscount.text=[NSString stringWithFormat:@"%0.2f", [[mainProductDict valueForKey:@"Applied_Discount"] doubleValue]];
    
    NSLog(@"discount text set here 2235");

    
}

-(IBAction)buttonAction:(id)sender
{
    [self.view endEditing:YES];
    
    //    if (mainProductDict==nil) {
    //
    //
    //    ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
    //    [stockViewController setDelegate:self];
    //    [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
    //    [stockViewController setTarget:self];
    //    [stockViewController setAction:@selector(stockAllocated:)];
    //
    //    UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
    //    modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //    modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    //    [self presentViewController:modalViewNavController animated:YES completion:nil];
    //
    //    }
    
    
    
    int tag = ((UIButton *)sender).tag;
    switch (tag)
    {
        case 1:
            if (isPproductDropDownToggled)
            {
                isPproductDropDownToggled=NO;
                focProductTableView.hidden=YES;
                
                productDropDown.frame=CGRectMake(599, 133, 241, 340);
                
                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,0) withDuration:0.2];
            }
            else
            {
                isPproductDropDownToggled=YES;
                focProductTableView.hidden=NO;
                productDropDown.frame=CGRectMake(599, 133, 241, 340);

                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,383) withDuration:0.2];
            }
            break;
        case 11:
        {
            if (mainProductDict.count!=0)
            {
                //NSLog(@"Stock");
                //productStockViewController=nil;
                ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
                [stockViewController setDelegate:self];
                [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
                [stockViewController setTarget:self];
                [stockViewController setAction:@selector(stockAllocated:)];
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
                
                NSLog(@"please select product called 2276");
            }
        }
            break;
        case 12:
        {
            if (mainProductDict.count!=0)
            {
                ///NSLog(@"Bonus");
                //productBonusViewController=nil;
                ProductBonusViewController *bonusViewController = [[ProductBonusViewController alloc] initWithProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:bonusViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                NSLog(@"please select product called 2295");

                [alert show];
            }
            
        }
            break;
        case 13:
        {
            if (mainProductDict.count!=0)
            {
                //NSLog(@"History");
                //customerSalesVC=nil;
                CustomerSalesViewController *salesHistory = [[CustomerSalesViewController alloc] initWithCustomer:customerDict andProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:salesHistory];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                NSLog(@"please select product called 2318");

                [alert show];
            }
        }
            break;
        case 5:
            //Toggel
            break;
        default:
            break;
    }
}

- (IBAction)saveOrderTapped:(id)sender {
    
    [oldSalesOrderVC  saveOrderTapped];
    
}

- (IBAction)loadFromMSLTapped:(id)sender {
    
    [oldSalesOrderVC loadFromMSLTapped];
}

- (IBAction)saveTemplateTapped:(id)sender {
    
    
//Items
    
//    UIButton * senderBtn=sender;
//    
//    TamplateNameViewController *viewController = [[TamplateNameViewController alloc] initWithTemplateName] ;
//    [viewController setPreferredContentSize:CGSizeMake(600, self.view.bounds.size.height - 100)];
//    [viewController setTarget:self];
//    [viewController setAction:@selector(itemUpdated:)];
//    viewController.templateName = [[NSUserDefaults standardUserDefaults]valueForKey:@"Template_Name"];
//    NSLog(@"templatename is %@",viewController.templateName);
//    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController] ;
//    popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
//    [popoverController setDelegate:self];
//    [popoverController presentPopoverFromRect:senderBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
    [oldSalesOrderVC saveTemplateTapped:sender];
}
//- (void) itemUpdated:(NSString *)item {
//    NSCharacterSet *unacceptedInput = nil;
//    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
//    if ([[item componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1)
//    {
//        Singleton *single = [Singleton retrieveSingleton];
//        if (item)
//        {
//            [popoverController dismissPopoverAnimated:YES];
//            NSMutableArray *tempTemplate = [NSMutableArray array];
//            NSMutableDictionary *dataDict;
//            for (int i = 0;i < self.items.count; i++)
//            {
//                
//                if([[[self.items objectAtIndex:i] stringForKey:@"priceFlag"] isEqualToString:@"N"] || [[[self.items objectAtIndex:i] stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
//                {
//                    dataDict =[NSMutableDictionary dictionary] ;
//                    if(![[self.items objectAtIndex:i] objectForKey:@"ItemID"])
//                    {
//                        [dataDict setValue:[[self.items objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
//                    }
//                    else
//                    {
//                        [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"ItemID"] forKey:@"ItemID"];
//                    }
//                    [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"Qty"] forKey:@"Qty"];
//                    [tempTemplate addObject:dataDict];
//                }
//            }
//            
//            //salesSer.delegate=self;
//            if([item isEqualToString:name_TEMPLATES])
//            {
//                [[SWDatabaseManager retrieveManager] deleteTemplateOrderWithRef:[selectedOrder stringForKey:@"Order_Template_ID"]];
//                [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
//                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Template saved" message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                [alert show];
//                
//                //                [ILAlertView showWithTitle:@"Template saved"
//                //                                   message:nil
//                //                          closeButtonTitle:NSLocalizedString(@"OK", nil)
//                //                         secondButtonTitle:nil
//                //                       tappedButtonAtIndex:nil];
//            }
//            else
//            {
//                BOOL isTheObjectThere = [single.savedOrderTemplateArray containsObject:item];
//                if(isTheObjectThere)
//                {
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Template name already exist." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                    [alert show];
//                    //                    [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                    //                                       message:@"Template name already exist."
//                    //                              closeButtonTitle:NSLocalizedString(@"OK", nil)
//                    //                             secondButtonTitle:nil
//                    //                           tappedButtonAtIndex:nil];
//                }
//                else
//                {
//                    [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
//                    
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Template saved", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                    [alert show];
//                    //                    [ILAlertView showWithTitle:@"Template saved"
//                    //                                       message:nil
//                    //                              closeButtonTitle:NSLocalizedString(@"OK", nil)
//                    //                             secondButtonTitle:nil
//                    //                           tappedButtonAtIndex:nil];
//                }
//            }
//        }
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Template name cannot have special characters.Please correct name and try to save again" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//        
//        //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//        //                           message:@"Template name cannot have special characters.Please correct name and try to save again"
//        //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//        //                 secondButtonTitle:nil
//        //               tappedButtonAtIndex:nil];
//    }
//}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)textViewDidEndEditing:(UITextView *)textView{
    //if ([key isEqualToString:@"lineItemNotes"])
    if(textView==txtNotes)
    {
        [mainProductDict setValue:textView.text forKey:@"lineItemNotes"];
        txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
        
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    NSString* output = nil;
    
    
    
    
    
    //     if (textField.text!=tempStr) {
    //           NSLog(@"clear");
    //    //
    //           tempStr=nil;
    //          NSLog(@"text field after nill is %@", tempStr);
    //
    //
    //     }
    
   
    
   // NSLog(@"main product dic desc %@", [mainProductDict description]);
    
    
}





- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (mainProductDict.count==0)
    {
        
        if(!noProductSelectedAlert){

        noProductSelectedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        noProductSelectedAlert.tag=1000000;
        NSLog(@"please select product called 2504");

        [noProductSelectedAlert show];
        }
        isErrorSelectProduct = YES;
        [self performSelector:@selector(resignedTextField:) withObject:textField afterDelay:0.0];
        
    }
    
    if (textField==txtDiscount) {
        textField.text=@"";
    }
    
    return YES;
}
-(void)resignedTextField:(UITextField *)textField
{
    [textField resignFirstResponder];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //    NSLog(@"text fileds text in did end %@", tempStr);
    //
    //    if (textField.text!=tempStr) {
    //        NSLog(@"clear");
    //
    //        tempStr=nil;
    //        NSLog(@"text field after nill is %@", tempStr);
    //
    ////        lblProductName.text = @"Product Name";
    ////        lblProductCode.text =@"";
    ////        lblProductBrand.text=@"";
    ////        lblWholesalePrice.text=@"";
    ////        txtfldWholesalePrice.text = @"";
    ////        lblRetailPrice.text=@"";
    ////        lblUOMCode.text=@"";
    ////        lblAvlStock.text=@"";
    ////        lblExpDate.text=@"";
    ////        lblPriceLabel.text=@"";
    //
    //        txtProductQty.text=@"";
    //        txtDefBonus.text=@"";
    //        txtRecBonus.text=@"";
    //        txtDiscount.text=@"";
    //        txtFOCQty.text=@"";
    //        txtNotes.text=@"";
    //        [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
    //        mainProductDict = nil;
    //        bonusProduct = nil;
    //        fBonusProduct = nil;
    //        for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
    //            [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    //        }
    //        for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
    //            [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    //        }
    //        [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
    
    
    
    //[self ResetData];
    // }
    
    //    tempStr=nil;
    //
    //
    //    tempStr=textField.text;
    //    //.. temp=textField.text ;
    //
    //    NSLog(@"temp is %@", textField.text);
    
    
   // NSLog(@"main product dic desc %@", [mainProductDict description]);
    
    //deleting allocated text field data in stock info
    
    [mainProductDict removeObjectForKey:@"StockAllocation"];
    
    
    [textField resignFirstResponder];
    NSCharacterSet *unacceptedInput = nil;
    NSString *zeroString=@"0";
    
    // textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    if (textField==txtFOCQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            
            if ([textField.text length]!=0)

            {
                [fBonusProduct setValue:textField.text forKey:@"Qty"];
//                [fBonusProduct setValue:zeroString forKey:@"Qty"];

                [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
            }
            else
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
            }
            

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Bonus quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            
            [fBonusProduct removeObjectForKey:@"Qty"];
            [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
        }
        
        [self calculateDiscount];


    }
    if (textField==txtRecBonus)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            [bonusProduct setValue:textField.text forKey:@"Qty"];
            if([isBonusAppControl isEqualToString:@"Y"])
            {
                bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                bonusEdit = @"Y";
            }
            else if([isBonusAppControl isEqualToString:@"I"])
            {
                if([[bonusProduct stringForKey:@"Qty"] intValue] >= bonus)
                {
                    bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                    bonusEdit = @"Y";
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Bonus can increase only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                    [alert show];
                    [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                }
            }
            else if([isBonusAppControl isEqualToString:@"D"])
            {
                if([[bonusProduct stringForKey:@"Qty"] intValue] <= bonus)
                {
                    bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                    bonusEdit = @"Y";
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Bonus can decrease only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                    [alert show];
                    [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                }
            }
            
            if (bonus==0)
            {
                [bonusProduct removeObjectForKey:@"Qty"];
            }
            

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Bonus quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
//            bonus=0;
            [bonusProduct removeObjectForKey:@"Qty"];
            
        }
        
        NSLog(@"check bonus product qty before calculating discount %@", bonusProduct);
        
        [self calculateDiscount];

    }
    if (textField==txtDiscount)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            if ([textField.text intValue]>100) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Discount (%) should not be more than 100%" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                [alert show];
                [mainProductDict removeObjectForKey:@"Discount"];
            }
            else
            {
                [mainProductDict setValue:textField.text forKey:@"Discount"];
                
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Discount (%) value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
            [alert show];
            [mainProductDict removeObjectForKey:@"Discount"];
            
        }
    }
    if (textField==txtProductQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            
            NSLog(@"calculating discount from product qty");
            
            
            [mainProductDict setValue:textField.text forKey:@"Qty"];
            if (bonusInfo.count > 0 || ![[mainProductDict stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
            {
                if([bonusEdit isEqualToString:@"N"])
                {
                    [self calculateBonus];
                }
                else
                {
                    [self calculateBonus];
                    
                    bonusEdit = @"N";
                }
                
                [self calculateDiscount];

            }
        }
        else
        {
            if (!isErrorSelectProduct) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
            }
            isErrorSelectProduct=NO;
            
            [mainProductDict setValue:@"" forKey:@"Qty"];
        }
    }
    
    
    
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
    
    
    
    
    //Bonus
    if(![fBonusProduct objectForKey:@"Qty"]  || isDualBonusAllow)
    {
        txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
    }
    
    
    //OLA!!
    if(isBonusAllow)//APP CONTROL
    {
        if (bonus<=0){
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        else{
            [txtRecBonus setUserInteractionEnabled:YES];
        }
        
    }
    else
    {
        [txtRecBonus setUserInteractionEnabled:NO];
        
        
    }
    
    
    //MFOC
    if(![bonusProduct objectForKey:@"Qty"] || isDualBonusAllow)
    {
        [txtFOCQty setText:[fBonusProduct stringForKey:@"Qty"]];
    }
    //[((SWTextFieldCell *)cell).secondLabel setText:[fBonusProduct stringForKey:@"Description"]];
    
    if(isFOCAllow)//APP CONTROL
    {
        [txtFOCQty setUserInteractionEnabled:YES];
        focProductBtn.userInteractionEnabled=YES;
    }
    else
    {
        [txtFOCQty setUserInteractionEnabled:NO];
        focProductBtn.userInteractionEnabled=NO;
    }
    
    if(isFOCItemAllow)
    {
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        focProductBtn.userInteractionEnabled=YES;
    }
    else
    {
        focProductBtn.userInteractionEnabled=NO;
    }
    //Discount
    NSString *discountString = [mainProductDict stringForKey:@"Discount"];
    
    [txtDiscount setText: [NSString stringWithFormat:@"%0.2f",[[mainProductDict valueForKey:@"Applied_Discount"] doubleValue]]];
    NSLog(@"discount text set here 2870");

    
    
    
    //[((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Discount", nil)];
    
//    if([isDiscountAppControl isEqualToString:@"Y"])//APP CONTROL
//    {
//        [txtDiscount setUserInteractionEnabled:YES];
//    }
//    else
//    {
//        [txtDiscount setUserInteractionEnabled:NO];
//        //((SWTextFieldCell *)cell).label.textColor = [UIColor grayColor];
//    }
    
    
    
    NSLog(@"check item disocunt %@", [[[SWDefaults appControl] objectAtIndex:0]valueForKey:@"ENABLE_ITEM_DISCOUNT"]);
    
    
   

    
    //return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"OLA!!***** WP %@",txtfldWholesalePrice.text );
    
    [textField resignFirstResponder];
    return YES;
}


//restrictiong text view notes to 50 characters

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    else if([[textView text] length] > 49 )
    {
        return NO;
    }
    return YES;
    //return textView.text.length + (text.length - range.length) <= 140;
}


//restrict characters here



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //restricting length to 50
    
    
    
    
    
    NSRange spaceRange = [string rangeOfString:@" "];
    if (spaceRange.location != NSNotFound)
    {
    	UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have entered wrong input" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        error.tag=101;
        [error show];
        return NO;
    } //    /* for backspace */
    //    if([string length]==0){
    //        return YES;
    //    }
    //
    //    /*  limit to only numeric characters  */
    //
    //    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //    for (int i = 0; i < [string length]; i++) {
    //        unichar c = [string characterAtIndex:i];
    //        if ([myCharSet characterIsMember:c]) {
    //            return YES;
    //        }
    //    }
    //
    //    UIAlertView *specialCharacterAlert=[[UIAlertView alloc]initWithTitle:@"Special Character Entered" message:@"Please enter numeric value" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //    specialCharacterAlert.tag=100;
    //    [specialCharacterAlert show];
    //
    //
    //    return NO;
    
    
    
    
    if([textField.text hasPrefix:@"0"])
    {
        //
        //        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Allocated quantity cannot be Zero" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //        [alert show];
        //           output = [textField.text substringFromIndex:1];
        //           NSLog(@"out put %@", output);
        //
        textField.text=@"";
        //
        //
    }
    
    
    //OLA!
    if ([textField isEqual:txtRecBonus]) {
        if ([txtProductQty.text isEqualToString:@""] || [txtProductQty.text isEqual:[NSNull null]]) {
            return NO;
        }
    }
    
    if ([textField isEqual:txtProductQty]) {
        /*
         NSString *finalText = [txtProductQty.text stringByAppendingString:string];
         
         if ([finalText isEqualToString:@""]) {
         [txtRecBonus setText:@""];
         [txtDefBonus setText:@""];
         [txtRecBonus setUserInteractionEnabled:NO];
         
         }*/
        
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0) {
            // Game on: when you return YES from this, your field will be empty
            [txtRecBonus setText:@""];
            [txtDefBonus setText:@""];
            [txtRecBonus setUserInteractionEnabled:NO];
            
            // [bonusProduct setValue:@"0" forKey:@"Qty"];
            
        }
    }
    
    if ([textField isEqual:txtfldWholesalePrice]) {
        
        

//        NSString * tempRange=textField.text;
//        NSString* test;
//        
//        if ([tempRange hasPrefix:@"$"]) {
//            test=[tempRange stringByReplacingOccurrencesOfString:@"$" withString:@"USD "];
//            
//            
//            
//            
//        }
//    
        
        
        
        NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSLog(@"resulting string would be: %@", resultString);
        NSString *prefixString = [[SWDefaults userProfile]valueForKey:@"Currency_Code"];
        NSRange prefixStringRange = [resultString rangeOfString:prefixString];
        
        
//        if ([prefixString isEqualToString:@"USD"]) {
//            
//            
//            prefixString=@"USD";
//            prefixStringRange=[resultString rangeOfString:prefixString];
//            
//            
//            
//        }
        
        
        if (prefixStringRange.location == 0) {
            // prefix found at the beginning of result string
            
            NSString *allowedCharacters;
            if ([textField.text componentsSeparatedByString:@"."].count >= 2)//no decimal point yet, hence allow point
                allowedCharacters = @"01234567890,.";
            else
                allowedCharacters = @"0123456789.,";
            
            if ([[string stringByReplacingOccurrencesOfString:prefixString withString:@""] rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
            {
                // BasicAlert(@"", @"This field accepts only numeric entries.");
                
                return NO;
            }
        }
        else
            return NO;
    }
    else if ([textField isEqual:txtDiscount])
    {
        NSString *allowedCharacters = @"0123456789.";
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            
            return NO;
        }
        
    }
    
    
    else if (textField==txtProductQty|| textField==txtFOCQty)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:SALESORDER_QTY_ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (range.location==0 &&[string hasPrefix:@"0"] ) {
            return NO;
        }
        else
        {
        return [string isEqualToString:filtered];
        }
    }
    
    else if (textField==txtRecBonus)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:SALESORDER_ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (range.location==0 &&[string hasPrefix:@"0"] ) {
            return NO;
        }
        else
        {
            return [string isEqualToString:filtered];
        }
    }
    
    
    return YES;
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        
        if (!isPinchDone)
        {
            isPinchDone=YES;
            //        recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
            //        recognizer.scale = 1;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(251, 8, 773, 690) withDuration:0.5];
            
            //recognizer.view.frame = CGRectMake(258, 8, 756, 690);
        }
        else
        {
            isPinchDone=NO;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(258, 324, 756, 374) withDuration:0.5];
            
            recognizer.view.frame = CGRectMake(258, 324, 756, 310);
            // recognizer.scale = 1;
            
        }
    }
}

#pragma mark - ProductStockViewControllerDelegate methods

//OLA!
-(void)didTapSaveButton
{
    if (isStockToggled)
    {
        [productTableView setUserInteractionEnabled:YES];//enable product table selection when draggable view is hidden. OLA!
        
        isStockToggled=NO;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(1024, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(999, 0, 30, 605) withDuration:0.5];
    }
}


#pragma mark Info Popover Button Methods

- (IBAction)productInformationPopoverButtonTapped:(id)sender {
    
    
    //check if product is selected, checking main product dict
    
    

    
    NSLog(@"main product dict before presenting popover %@", [mainProductDict description]);
    
    if (mainProductDict.count>0) {
        
    
    //test
        
        
        NSArray *arrayWithTwoStrings = [[mainProductDict valueForKey:@"Long_Description"] componentsSeparatedByString:@"\n"];

        NSLog(@"TEST ARRAY %@", [arrayWithTwoStrings objectAtIndex:0]);
        
    
    
    swPopOverController = [[SWPopoverController alloc]init];

    swPopOverController.headerTitle=@"Product Description";
        swPopOverController.longDescription= [mainProductDict valueForKey:@"Long_Description"];
        
    
    swPopOverController.preferredContentSize=CGSizeMake(500, 200);
    
    //creater a new navigation controller first
    
    UINavigationController * productInfoNavController=[[UINavigationController alloc]initWithRootViewController:swPopOverController];
    

    
    
    
    
    UIButton* locationBtn=(UIButton*)sender;
    
    productDescriptionPopoverViewController=nil;
     
    
    //[customPopoverViewController.view addSubview:popoverView];
    
    if (productDescriptionPopoverViewController == nil) {
        //The color picker popover is not showing. Show it.
        productDescriptionPopoverViewController = [[UIPopoverController alloc] initWithContentViewController:productInfoNavController];
        
        [productDescriptionPopoverViewController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        productDescriptionPopoverViewController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [productDescriptionPopoverViewController dismissPopoverAnimated:YES];
        productDescriptionPopoverViewController = nil;
        productDescriptionPopoverViewController.delegate=nil;
    }
    
    }
    
    else
    {
        NSLog(@"product not selected");
    }
    

}


@end

