
//
//  SWCustomerDetailViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWCustomerDetailViewController.h"
#import "SWFoundation.h"
#import "SalesWorxPaymentCollectionViewController.h"
#import "CashFormView.h"
#import "SalesWorxAllSurveyViewController.h"


#define landSpace 500
#define portSpace 0
#define PieChartOriginX 50
#define PieChartOriginY 70
#import "AlSeerCustomerDashboardViewController.h"

@interface SWCustomerDetailViewController ()
- (void)setupToolbar;
- (void)rearrangeViews;

@end

@implementation SWCustomerDetailViewController



- (id)initWithCustomer:(NSDictionary *)row {
    self = [super init];
    if (self)
    {

        if ([row count]==0)
        {
            customer = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults customer]  ] ;
        }
        else
        {
            customer = [NSMutableDictionary dictionaryWithDictionary:row   ] ;
        }
        
        [self setTitle:NSLocalizedString(@"Customer Details", nil)];
        Singleton *single = [Singleton retrieveSingleton] ;
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
        CGFloat screenHeight = screenRect.size.height;
        single.mainScreenWidth = screenWidth;
        single.mainScreenHeight = screenHeight;
        
        //////////
     //   NSLocale* currentLoc = [NSLocale currentLocale];
        popoverOriented=YES;
        
        //[self.view setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
        
        AppControl *appControl = [AppControl retrieveSingleton];
        
        isOrderHistoryEnable = appControl.ENABLE_ORDER_HISTORY; //[SWDefaults appControl]count]
        isCollectionEnable = appControl.ENABLE_COLLECTION; //[SWDefaults appControl]count]
        
        if([isOrderHistoryEnable isEqualToString:@"Y"])//Enable order history
        {
            pages=4;
        }
        else{
            pages=3;
        }
        
        // [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Customers", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancel)] ];
        
        pageControl=[[UIPageControl alloc] initWithFrame:CGRectZero] ;
        [ pageControl setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin];
        
        [ pageControl setNumberOfPages:pages];
        [ pageControl setCurrentPage:0];
        
        detailScroller=[[UIScrollView alloc] initWithFrame:self.view.bounds] ;
        [detailScroller setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [detailScroller setPagingEnabled:YES];
        [detailScroller setDelegate:self];
        
        
        //[detailScroller setBackgroundColor:[UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.5f]];
        
        [self.view addSubview:detailScroller];
        [self.view addSubview: pageControl];
        
        // views for scroller
        customerDetailView=[[CustomerDetailView alloc] initWithCustomer:customer] ;
        [customerDetailView setFrame:CGRectMake(0, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
        [customerDetailView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        customerTargetView=[[CustomerTargetView alloc] initWithCustomer:customer] ;
        [customerTargetView setFrame:CGRectMake(detailScroller.bounds.size.width, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
        [customerTargetView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        customerOrderHistoryView=[[CustomerOrderHistoryView alloc] initWithCustomer:customer] ;
        [customerOrderHistoryView setFrame:CGRectMake(detailScroller.bounds.size.width * 2, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
        [customerOrderHistoryView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        // for price list tab
        customerPriceListView=[[CustomerPriceList alloc] initWithCustomer:customer] ;
        [  customerPriceListView setFrame:CGRectMake(detailScroller.bounds.size.width * 3, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
        [  customerPriceListView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        
        
               
        
        
        [detailScroller addSubview:customerDetailView];
        [detailScroller addSubview:customerTargetView];
        if([isOrderHistoryEnable isEqualToString:@"Y"]) //Enable_Order_History
        {
            [detailScroller addSubview:customerOrderHistoryView];
        }// for price list tab
        [detailScroller addSubview: customerPriceListView];
        
        
        UIBarButtonItem *displayActionBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(displayActions:)] ;
        
        
        
        UIBarButtonItem *displayMapBarButton  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_filter_geo"cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)];
        self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
        
        
        
        if([isOrderHistoryEnable isEqualToString:@"Y"])//Enable order history
        {
            segments = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"Details", nil), NSLocalizedString(@"Target", nil), NSLocalizedString(@"Order History", nil),NSLocalizedString(@"Price List", nil), nil]] ;
        }
        else{
            segments = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"Details", nil), NSLocalizedString(@"Target", nil),NSLocalizedString(@"Price List", nil), nil]] ;
        }
        
        [segments setFrame:CGRectMake(0, 0, 800, 30)];
        [segments setSelectedSegmentIndex:0];
        // [segments setSegmentedControlStyle:UISegmentedControlStyleBar];
        //[segments setTintColor:UIColorFromRGB(0x2A3949)];
        if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
            segments.tintColor = [UIColor whiteColor];}
        
        
        
        [segments setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               RegularFontOfSize(13.0f), UITextAttributeFont,
                                               nil] forState:UIControlStateNormal];
        [segments addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
        segments.segmentedControlStyle = UISegmentedControlStyleBar;
        
        [ pageControl setFrame:CGRectMake(self.view.bounds.size.width / 2 - 100, self.view.bounds.size.height - 30, 200, 20)];
        
        flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        
        lastSyncButton = [[UIBarButtonItem alloc] initWithCustomView:segments] ;
        [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, lastSyncButton, flexibleSpace, nil] animated:YES];

        //////////
    }
    return self;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return YES;
    
}



-(void)cancel
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
   }

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController setToolbarHidden:NO animated:YES];

    [detailScroller setContentSize:CGSizeMake(detailScroller.bounds.size.width * pages, 1.0)];

    [customerOrderHistoryView refreshOrderHistory];
    [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customer stringForKey:@"Customer_ID"]]];

    
   

    
    //[self rearrangeViews];
    
}
-(void)viewDidAppear:(BOOL)animated
{

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [actions dismissWithClickedButtonIndex:-1 animated:YES];
    actions = nil;
}

- (void)setupToolbar {
    
    
}

- (void)rearrangeViews {
    int i = 0;
    CGRect scrollerRect = detailScroller.bounds;
    for (UIView *view in detailScroller.subviews) {
        CGRect frame = view.frame;
        frame.origin.x = i * scrollerRect.size.width;
        [view setFrame:frame];
        i++;
    }
    
    [self scrollToPage: pageControl.currentPage];
}
- (void)displayMap:(id)sender
{
    mapView = [[MapView alloc] init] ;
    mapView.mapView.frame = CGRectMake(0, 0, 700, 600);
    [mapView setPreferredContentSize:CGSizeMake(700,600  )];
    
    place = [[Place alloc] init] ;
    place.latitude= [[customer objectForKey:@"Cust_Lat"] floatValue];
    place.longitude =  [[customer objectForKey:@"Cust_Long"] floatValue];
    place.name= [customer objectForKey:@"Customer_Name"];
    place.otherDetail = [customer objectForKey:@"City"];
    
    [mapView showCurrentLocationAddress:place];
    
    Annotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
    [mapView.mapView addAnnotation:Annotation];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapView] ;
    
    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    [locationFilterPopOver setDelegate:self];
    
    CGRect rect = self.view.frame;
    rect.size.width = rect.size.width;
    
    [locationFilterPopOver presentPopoverFromRect:rect inView:self.view permittedArrowDirections:NO animated:YES];
    
    popoverOriented=NO;
    
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [self applyMapViewMemoryHotFix];
    popoverOriented = YES;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
   //  [self applyMapViewMemoryHotFix];
    
    return YES;
    
}
- (void)scrollToPage:(NSInteger)page {
    CGFloat pageWidth = detailScroller.frame.size.width;
    CGFloat pageHeight = detailScroller.frame.size.height;
    CGRect scrollTarget = CGRectMake(page * pageWidth, 0, pageWidth, pageHeight);
    [detailScroller scrollRectToVisible:scrollTarget animated:YES];
    [ pageControl setCurrentPage:page];
}

- (void)segmentChanged:(id)segment {
    [self scrollToPage:segments.selectedSegmentIndex];
    [self setTitle:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"Customer", nil), [segments titleForSegmentAtIndex: pageControl.currentPage],NSLocalizedString(@"Information", nil)]];
    
    
    if(segments.selectedSegmentIndex == 0)
    {
        
    }
    else if(segments.selectedSegmentIndex == 1)
    {
        [customerTargetView loadTarget];
        
    }
    else if(segments.selectedSegmentIndex == 2)
    {
        if(customerOrderHistoryView.isToggled)
        {
            customerOrderHistoryView.orderItemView.alpha=0.0;
            customerOrderHistoryView.isToggled = NO;
        }
        [customerOrderHistoryView loadOrders];
        
    }
//    else if (segments.selectedSegmentIndex==4)
//    {
//       
//        
//        
//        customerDashboard=[[AlSeerCustomerDashboardViewController alloc]init];
//        customerDashboard.view.frame=CGRectMake(0, 0, 1024, 768);
//        [detailScroller addSubview:customerDashboard.view];
//        [self addChildViewController:customerDashboard];
//        [customerDashboard didMoveToParentViewController:self];
//        
//
//        
//        
////         customerDashboard=[[AlSeerCustomerDashboardViewController alloc]init];
////        
////        
////        [self.navigationController pushViewController:customerDashboard animated:NO];
//        
//        
//        
//    }
    
    else {
        [customerPriceListView loadprice];
        
    }
}



//crash reported on iOS 8 dated jan 7 2015
- (void)displayActions:(id)sender {
    if (actions == nil)
    {
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Collection", nil),NSLocalizedString(@"Survey", nil), nil]  ;
        }
        else{
//            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Survey", nil), nil]  ;
            
            
            //check this action sheet
            
            
            actions = [[UIActionSheet alloc]
                                     initWithTitle:@"Select Folder"
                                     delegate:self
                                     cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
            
            NSMutableArray* buttons=[[NSMutableArray alloc] initWithObjects:@"Start Visit",@"Survey", nil];
            
            //[actions addButtonWithTitle:@"Start Visit"];

            for (int nb=0; nb<2; nb++)
            {
                
                
                    [actions addButtonWithTitle:@"Start Visit"];

                }
            
            
            
            [actions addButtonWithTitle:@"Survey"];

            
        
            //actions.cancelButtonIndex = [actions addButtonWithTitle: @"Cancel"];
            
            
        }
        
    }
    [actions showFromBarButtonItem:sender animated:YES];
}


#pragma mark UIScrollview Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = detailScroller.frame.size.width;
    int page = ((int)scrollView.contentOffset.x +
                (int)pageWidth / 2) / (int)pageWidth;
    [ pageControl setCurrentPage:page];
    [segments setSelectedSegmentIndex:page];
    [self setTitle:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"Customer", nil), [segments titleForSegmentAtIndex: pageControl.currentPage],NSLocalizedString(@"Information", nil) ]];
    if(segments.selectedSegmentIndex == 0)
    {
        
    }
    else if(segments.selectedSegmentIndex == 1)
    {
        
        
        
        [customerTargetView loadTarget];
        
    }
    else if(segments.selectedSegmentIndex == 2)
    {
        if(customerOrderHistoryView.isToggled)
        {
            customerOrderHistoryView.orderItemView.alpha=0.0;
            customerOrderHistoryView.isToggled = NO;
        }
        [customerOrderHistoryView loadOrders];
        
    }
    else {
        [customerPriceListView loadprice];
        
    }
    
    
}


#pragma mark UIvheet Delegate
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
//    for (UIButton *btn in [actionSheet valueForKey:@"_buttons"])
//    {
//        [btn.titleLabel setFont:BoldSemiFontOfSize(20.0f)];
//    }
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [SWDefaults setCustomer:customer];
    if (buttonIndex == 0)
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.planDetailId = @"0";
        if([[customer stringForKey:@"Cash_Cust"]isEqualToString:@"Y"]){
            single.isCashCustomer = @"Y";
            CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:customer] ;
            [self.navigationController pushViewController:cashCustomer animated:YES];
            cashCustomer = nil;
        }
        else{
            single.isCashCustomer = @"N";
            [[[SWVisitManager alloc] init] startVisitWithCustomer:customer parent:self andChecker:@"D"];
        }
    }
    else if (buttonIndex == 1)
    {
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            SalesWorxPaymentCollectionViewController *collectionViewController = [[SalesWorxPaymentCollectionViewController alloc] initWithCustomer:customer] ;
            [self.navigationController pushViewController:collectionViewController animated:YES];
        }
        else
        {
//            SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
            SalesWorxAllSurveyViewController *objSurveyVC =[[SalesWorxAllSurveyViewController alloc] initWithNibName:@"SalesWorxAllSurveyViewController" bundle:nil];
            [self.navigationController pushViewController:objSurveyVC animated:YES];
        }
        
    }
    else if (buttonIndex == 2)
    {
//        SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
        SalesWorxAllSurveyViewController *objSurveyVC =[[SalesWorxAllSurveyViewController alloc] initWithNibName:@"SalesWorxAllSurveyViewController" bundle:nil];
        [self.navigationController pushViewController:objSurveyVC animated:YES];
       
        
    }
}

//- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
    
    [SWDefaults setCustomer:customer];
    customer = [SWDefaults validateAvailableBalance:orderAmmount];
    [self rearrangeViews];
    [customerDetailView.tableView reloadData];
    orderAmmount=nil;
    
}
- (void)applyMapViewMemoryHotFix{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    //[self applyMapViewMemoryHotFix];
    [mapView RemoveIT];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
