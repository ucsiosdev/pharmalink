//
//  ProductDetailViewController.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "AFImageRequestOperation.h"
#import "SWFoundation.h"
#define PieChartOriginX 50
#define PieChartOriginY 70
@interface ProductDetailViewController ()
- (void)setupToolbar;
- (void)rearrangeViews;
- (void)displayActions:(id)sender;

@end

@implementation ProductDetailViewController


- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    
    if (self) {
        [self setTitle:NSLocalizedString(@"Product Details", nil)];
        product = [NSMutableDictionary dictionaryWithDictionary:p ] ;

        Singleton *single = [Singleton retrieveSingleton] ;

        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
        CGFloat screenHeight = screenRect.size.height;
        
        single.mainScreenWidth = screenWidth;
        single.mainScreenHeight = screenHeight;
    }
    
    return self;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
       return YES;
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{

    [detailScroller setContentSize:CGSizeMake(detailScroller.bounds.size.width * 4, detailScroller.bounds.size.height)];
    [self rearrangeViews];
    
}



//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    NSLog(@"Product %@",product);
    //    NSLog(@"Profile %@",[SWDefaults userProfile]);

    
    
    [self.view setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    AppControl *appControl = [AppControl retrieveSingleton];
    
    isProductTargetEnable = appControl.SHOW_PRODUCT_TARGET;
    // if([[SWDefaults appControl] count]==0)//SHOW_PRODUCT_TARGET
    
    if([isProductTargetEnable isEqualToString:@"Y"])
    {
        pages =4;
    }
    else
    {
        pages = 3;
    }
    
    pageControl=[[UIPageControl alloc] initWithFrame:CGRectZero] ;
    [pageControl setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin];
    
    pageControl.pageIndicatorTintColor = [UIColor grayColor];
    pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0x4790D2);
    [pageControl setNumberOfPages:pages];
    [pageControl setCurrentPage:0];
    
    detailScroller=[[UIScrollView alloc] initWithFrame:self.view.bounds] ;
    [detailScroller setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [detailScroller setPagingEnabled:YES];
    [detailScroller setDelegate:self];
    CGSize scrollableSize = CGSizeMake(320, detailScroller.frame.size.height);
    [detailScroller setContentSize:scrollableSize];
    [self.view addSubview:detailScroller];
    [self.view addSubview:pageControl];
    
    // views for scroller
    productDetailView=[[ProductDetailView alloc] initWithProduct:product] ;
    [productDetailView setFrame:CGRectMake(0, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
    [productDetailView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    productStockView=[[ProductStockView alloc] initWithProduct:product] ;
    [productStockView setFrame:CGRectMake(0, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
    [productStockView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    productBonusView=[[ProductBonusView alloc] initWithProduct:product] ;
    [productBonusView setFrame:CGRectMake(0, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
    [productBonusView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    productTargetView=[[ProductTargetView alloc] initWithProduct:product] ;
    [productTargetView setFrame:CGRectMake(0, 0, detailScroller.bounds.size.width, detailScroller.bounds.size.height)];
    [productTargetView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [detailScroller addSubview:productDetailView];
    [detailScroller addSubview:productStockView];
    [detailScroller addSubview:productBonusView];
    if([isProductTargetEnable isEqualToString:@"Y"])//SHOW_PRODUCT_TARGET
    {
        [detailScroller addSubview:productTargetView];
    }
    if([isProductTargetEnable isEqualToString:@"Y"])//SHOW_PRODUCT_TARGET
    {
        segments = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"Details", nil),NSLocalizedString(@"Stock", nil), NSLocalizedString(@"Bonus", nil), NSLocalizedString(@"Target", nil), nil]] ;     }
    else
    {
        segments = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"Details", nil), NSLocalizedString(@"Stock", nil),NSLocalizedString(@"Bonus", nil), nil]] ;
    }
    
    [segments setFrame:CGRectMake(0, 0, 800, 30)];
    [segments setSelectedSegmentIndex:0];
    [segments setSegmentedControlStyle:UISegmentedControlStyleBar];
    //[segments setTintColor:UIColorFromRGB(0x2A3949)];
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        segments.tintColor = [UIColor whiteColor];}
    
    [segments setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                      RegularFontOfSize(13.0f), UITextAttributeFont,
                                      nil] forState:UIControlStateNormal];
    [segments addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    lastSyncButton = [[UIBarButtonItem alloc] initWithCustomView:segments] ;
    
    if ([[AppControl retrieveSingleton].ENABLE_PRODUCT_IMAGE isEqualToString:@"Y"]) {
        productAddArray = [NSArray arrayWithArray: [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Product_Addl_Info Where Inventory_Item_ID = '%@' AND Attrib_Name='PROD_IMG'",[product stringForKey:@"ItemID"]]]];
        if (productAddArray.count==0)
        {
            isProductImageAvailble =NO;
            
        }
        else
        {
            
            isProductImageAvailble =YES;
            NSDictionary *row = [productAddArray objectAtIndex:0];
//            NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsDir  = [documentPaths objectAtIndex:0];
            
            
            
            //iOS 8 support
            NSString *documentsDir;
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                documentsDir=[SWDefaults applicationDocumentsDirectory];
            }
            
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains
                (NSDocumentDirectory, NSUserDomainMask, YES);
                documentsDir = [paths objectAtIndex:0];
            }
            
            
            NSString  *pngfile = [documentsDir stringByAppendingPathComponent:[row stringForKey:@"Attrib_Value"]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:pngfile])
            {
                NSData *imageData = [NSData dataWithContentsOfFile:pngfile];
                UIImage *img = [UIImage imageWithData:imageData];
                productDetailView.productImage.image = img;
            }
            
            
        }
        [self getProductImageDetails];

    }
    
    
    
   

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Product Detail View"];
    //[Crittercism leaveBreadcrumb:@"<Product Detail View>"];
    [self rearrangeViews];

    
   
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[detailScroller setContentSize:CGSizeMake(detailScroller.bounds.size.width * pages, detailScroller.bounds.size.height)];
    CGSize scrollableSize = CGSizeMake(detailScroller.bounds.size.width * pages, detailScroller.frame.size.height);
    [detailScroller setContentSize:scrollableSize];
    
    [pageControl setFrame:CGRectMake(self.view.bounds.size.width / 2 - 100, self.view.bounds.size.height - 30, 200, 20)];
    [self setupToolbar];
//    [self performSelector:@selector(LaodDetail) withObject:nil afterDelay:1.01];
//    [self performSelector:@selector(LaodBonus) withObject:nil afterDelay:1.01];
//    [self performSelector:@selector(LaodStock) withObject:nil afterDelay:1.01];
//    [self performSelector:@selector(LaodTarget) withObject:nil afterDelay:1.01];
    
}
-(void)LaodDetail
{
    [productDetailView loadDetail];

}
-(void)LaodBonus
{
    [productBonusView loadBonus];

}
-(void)LaodStock
{
    [productStockView loadStock];

}
-(void)LaodTarget
{
    [productTargetView loadTarget];
    
    NSLog(@"test");
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [actions dismissWithClickedButtonIndex:-1 animated:YES];
}

- (void)setupToolbar
{

    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }
    [self.navigationController setToolbarHidden:NO animated:YES];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, lastSyncButton, flexibleSpace, nil] animated:YES];
}

- (void)rearrangeViews {
    int i = 0;
    CGRect scrollerRect = detailScroller.bounds;
    for (UIView *view in detailScroller.subviews) {
        CGRect frame = view.frame;
        frame.origin.x = i * scrollerRect.size.width;
        [view setFrame:frame];
        i++;
    }
    
    [self scrollToPage:pageControl.currentPage];
}

- (void)scrollToPage:(NSInteger)page {
    CGFloat pageWidth = detailScroller.frame.size.width;
    CGFloat pageHeight = detailScroller.frame.size.height;
    CGRect scrollTarget = CGRectMake(page * pageWidth, 0, pageWidth, pageHeight);
    [detailScroller scrollRectToVisible:scrollTarget animated:YES];
    [pageControl setCurrentPage:page];
}

- (void)segmentChanged:(id)segment {
    [self scrollToPage:segments.selectedSegmentIndex];
    [self setTitle:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"Product", nil), [segments titleForSegmentAtIndex:pageControl.currentPage],NSLocalizedString(@"Information", nil)]];
    if(segments.selectedSegmentIndex == 0)
    {
        [self LaodDetail];
    }
    else if(segments.selectedSegmentIndex == 1)
    {
        [self LaodStock];
        
    }
    else if(segments.selectedSegmentIndex == 2)
    {
        [self LaodBonus];
    }
    else
    {
        [self LaodTarget];
    }

}

- (void)displayActions:(id)sender {
    if (actions == nil) {
        actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Collection", nil), nil]  ;
    }
    
    [actions showFromBarButtonItem:sender animated:YES];
}

#pragma mark UIScrollview Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = detailScroller.frame.size.width;
    int page = ((int)scrollView.contentOffset.x +
                (int)pageWidth / 2) / (int)pageWidth;
    [pageControl setCurrentPage:page];
    [segments setSelectedSegmentIndex:page];
    [self setTitle:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"Product", nil), [segments titleForSegmentAtIndex:pageControl.currentPage],NSLocalizedString(@"Information", nil)]];
    if(segments.selectedSegmentIndex == 0)
    {
    }
    else if(segments.selectedSegmentIndex == 1)
    {
        [self LaodStock];
        
    }
    else if(segments.selectedSegmentIndex == 2)
    {
        [self LaodBonus];
    }
    else
    {
        [self LaodTarget];
    }
}

-(void)getProductImageDetails
{
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString  *serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
   // NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    NSString * avid = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];

    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    

    
    NSString *ClientVersion =[NSString stringWithFormat:@"%@(%@)", version,build];
    NSString *timeStampString = @"";
    
    if (isProductImageAvailble)
    {

        timeStampString = [[productAddArray objectAtIndex:0] stringForKey:@"Sync_Timestamp"];
    }
    else
    {

        timeStampString = @"";
    }
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@&ProcParams=SalesRepID&ProcValues=%@&ProcParams=InventoryItemID&ProcValues=%@&ProcParams=ItemOrgID&ProcValues=%@&ProcParams=LastSyncTimestamp&ProcValues=%@",[SWDefaults username],[[DataSyncManager sharedManager] sha1:[SWDefaults password]],[[DataSyncManager sharedManager]getDeviceID],ClientVersion,@"Get_ProductImageInfo",[testServerDict stringForKey:@"name"],@"sync_MC_ExecGetProductImageInfo",[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"],[product stringForKey:@"ItemID"],[product stringForKey:@"OrgID"],timeStampString];
    
    //NSLog(@"String %@",myRequestString);
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
	[urlRequest setHTTPBody:myRequestData];

    AFHTTPClient *request  = [[HttpClient sharedManager] initWithBaseURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ,id responseObject)
     {

         NSString *responseText=[operationQ responseString];
         id object = [responseText JSONValue];
         NSArray *array= (NSArray*)object;
         if (array.count==0 )
         {
             if (productAddArray.count!=0) {
                 NSDictionary *row = [productAddArray objectAtIndex:0];
                 
                 
                 
                 //iOS 8 support
                 NSString *documentsDir;
                 
                 if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                     documentsDir=[SWDefaults applicationDocumentsDirectory];
                 }
                 
                 else
                 {
                     NSArray *paths = NSSearchPathForDirectoriesInDomains
                     (NSDocumentDirectory, NSUserDomainMask, YES);
                     documentsDir = [paths objectAtIndex:0];
                 }

                 
//                 NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                 NSString *documentsDir  = [documentPaths objectAtIndex:0];
                 
                 
                 NSString  *pngfile = [documentsDir stringByAppendingPathComponent:[row stringForKey:@"Attrib_Value"]];
                 
                 if ([[NSFileManager defaultManager] fileExistsAtPath:pngfile])
                 {
                     NSData *imageData = [NSData dataWithContentsOfFile:pngfile];
                     UIImage *img = [UIImage imageWithData:imageData];
                     productDetailView.productImage.image = img;
                 }
                 else
                 {
                      [self downloadProductImageWithName:[[productAddArray objectAtIndex:0]stringForKey:@"Attrib_Value"] andType:[[productAddArray objectAtIndex:0]stringForKey:@"Attrib_Name"] ];
                 }
             }
         }
         else
         {

             NSString *responseText=[operationQ responseString];
             [[SWDatabaseManager retrieveManager] insertProductAddInfo:[[responseText JSONValue] objectAtIndex:0]];
             [self downloadProductImageWithName:[[[responseText JSONValue] objectAtIndex:0]stringForKey:@"Attrib_Value"] andType:[[[responseText JSONValue] objectAtIndex:0]stringForKey:@"Attrib_Name"] ];

         }

     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"error: %@", error.localizedDescription);
         
         
         if (productAddArray.count>0) {

             
         
         NSDictionary *row = [productAddArray objectAtIndex:0];
         
         
         
         //iOS 8 support
         NSString *documentsDir;
         
         if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
             documentsDir=[SWDefaults applicationDocumentsDirectory];
         }
         
         else
         {
             NSArray *paths = NSSearchPathForDirectoriesInDomains
             (NSDocumentDirectory, NSUserDomainMask, YES);
             documentsDir = [paths objectAtIndex:0];
         }

         
         
         
//         
//         NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//         NSString *documentsDir  = [documentPaths objectAtIndex:0];
         NSString  *pngfile = [documentsDir stringByAppendingPathComponent:[row stringForKey:@"Attrib_Value"]];
         
         if ([[NSFileManager defaultManager] fileExistsAtPath:pngfile])
         {
             NSData *imageData = [NSData dataWithContentsOfFile:pngfile];
             UIImage *img = [UIImage imageWithData:imageData];
             productDetailView.productImage.image = img;
         }
         }

     }];
    [operation start];
     
}
-(void)downloadProductImageWithName:(NSString *)fileName andType:(NSString *)fileType
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString  *serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];
    
    NSString *strUrl =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"File/%@?FileType=%@",fileName,fileType]];
    
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
   

    AFImageRequestOperation *operation = [AFImageRequestOperation imageRequestOperationWithRequest:request123
                                                                              imageProcessingBlock:nil
                                          
                                                                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                                                               
                                                                                               // Get dir
//                                                                                               NSString *documentsDirectory = nil;
//                                                                                               NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                                                                                               documentsDirectory = [paths objectAtIndex:0];
                                                                                               
                                                                                               
                                                                                               
                                                                                               //iOS 8 support
                                                                                               NSString *documentsDir;
                                                                                               
                                                                                               if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                                                                                                   documentsDir=[SWDefaults applicationDocumentsDirectory];
                                                                                               }
                                                                                               
                                                                                               else
                                                                                               {
                                                                                                   NSArray *paths = NSSearchPathForDirectoriesInDomains
                                                                                                   (NSDocumentDirectory, NSUserDomainMask, YES);
                                                                                                   documentsDir = [paths objectAtIndex:0];
                                                                                               }

                                                                                               
                                                                                               
                                                                                               
                                                                                               NSString *pathString = [NSString stringWithFormat:@"%@/%@",documentsDir, fileName];
                                                                                               
                                                                                               // Save Image
                                                                                               NSData *imageData = UIImageJPEGRepresentation(image, 90);
                                                                                               [imageData writeToFile:pathString atomically:YES];
                                                                                               
//                                                                                               NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                                                                                               NSString *documentsDir  = [documentPaths objectAtIndex:0];
                                                                                               NSString  *pngfile = [documentsDir stringByAppendingPathComponent:fileName];
                                                                                               
                                                                                               if ([[NSFileManager defaultManager] fileExistsAtPath:pngfile])
                                                                                               {
                                                                                                   NSData *imageData = [NSData dataWithContentsOfFile:pngfile];
                                                                                                   UIImage *img = [UIImage imageWithData:imageData];
                                                                                                   productDetailView.productImage.image = img;
                                                                                               }
                                                                                               
                                                                                           } 
                                                                                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                                                               NSLog(@"Test ...... %@", [error localizedDescription]);
                                                                                           }];
    
    [operation start];
    

}
#pragma mark UIActionSheet Delegate
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIButton *btn in [actionSheet valueForKey:@"_buttons"]) {
        [btn.titleLabel setFont:BoldSemiFontOfSize(20.0f)];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
