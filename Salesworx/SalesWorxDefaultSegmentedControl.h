//
//  SalesWorxDefaultSegmentedControl.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 8/1/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxDefaultSegmentedControl : UISegmentedControl
@property (nonatomic) IBInspectable BOOL EnableTitleRTLSupport;

@end
