//
//  MedRepVisitsListViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import <MapKit/MapKit.h>
#import "MedRepVisitsViewController.h"
#import "MedRepQueries.h"
#import "MedRepMenuTableViewCell.h"
#import <MedRepUpdatedDesignCell.h>
#import "MedRepVisitsFilterViewController.h"
#import "MedRepCustomClass.h"
#import "NSString+Additions.h"
#import "MedRepVisitsStartMultipleCallsViewController.h"
#import "SalesWorxTableView.h"
#import "MedRepPlannedVisitsMultipleCallsTableViewCell.h"
#import "SalesWorxTableViewHeaderView.h"
#import "MedRepDefaults.h"
#import "MedRepVisitsLocationMapPopUpViewController.h"
#import "MedRepVisitContactDetailsPopUpViewController.h"
#import "SalesWorxImageView.h"
#import "MedRepPanelTitle.h"
#import "SalesWorxDefaultLabel.h"
#import "SalesWorxImageBarButtonItem.h"
#import "AppControl.h"
#import "SalesWorxPresentationViewController.h"
#import "SalesWorxPresentationHomeViewController.h"

@interface MedRepVisitsListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate,StartVisitDelegate,UISearchBarDelegate,VisitsFilterDelegate,UITextFieldDelegate, CLLocationManagerDelegate,SalesWorxPresentationDelegate,UIPopoverPresentationControllerDelegate>

{
    UIPopoverController * createVisitPopOverController,* rescheduleVisitPopOverController,*calenderPopOverController;
    SalesWorxImageBarButtonItem* createVisitButton;
    SalesWorxImageBarButtonItem* editVisitButton;
    IBOutlet UIView *presentationPlaceHolderView;
    
    IBOutlet SalesWorxImageView *visitStatusImageView;
    IBOutlet MedRepPanelTitle *noVisitSelectedLbl;
    NSMutableArray* plannedVisitsArray;
    NSMutableArray* filteredPlannedVisitsArray;
    NSMutableArray* visitDetailsArray;
    
    UIDatePicker* datepicker;
    IBOutlet UIView *tableHeaderView;

    IBOutlet UIButton *visitStatusBtn;
    UIDatePicker* visitDatePicker;
    IBOutlet SalesWorxImageView *notesStatusImageView;

    IBOutlet SalesWorxImageView *taskStatusImageView;
    
    NSString* visitStatus;
    
    SalesWorxImageBarButtonItem* startVisitButton;
    
    NSString* doctorNameStr;
    
    NSInteger selectedDateValue
    ;
    UISearchBar *customSearchBar;
    
    CLLocationCoordinate2D  coord;
    BOOL visitCompleted;
    
    BOOL isSearching;
    
    BOOL calenderUsedtopickDate;
    
    NSString* futureVisitDate;
    
    VisitDetails * visitDetails;
    
    NSMutableArray * currentVisitTasksArray;
    
    IBOutlet UIButton *visitsFilterbtn;
    UIPopoverController* popOverController,*filterPopOverController;
    StringPopOverViewController_ReturnRow* locationPopOver;
    
    IBOutlet UIImageView *visitTypeImageView;
    IBOutlet UIButton *notesButton;
    
    IBOutlet UIButton *tasksButton;
    NSIndexPath *selectedVisitIndexPath;
    
    
    IBOutlet MedRepElementDescriptionLabel *demoPlanDescLbl;
    IBOutlet SalesWorxDropShadowView *tableContainerView;
    
    
    IBOutlet UIButton *pdfFileButton;
    IBOutlet UIButton *imageFileButton;
    IBOutlet UIButton *vedioFileButton;
    NSIndexPath *reschudeleSelectedIndexpath;
    IBOutlet UIImageView *LocationTypeImageView;
    IBOutlet SalesWorxDefaultLabel *LocationTypeLabel;
    
    NSMutableDictionary* previousFilteredParameters;
    
    NSMutableDictionary*   currentVisitDict;
    
    
    NSMutableArray* productPDFFilesArray;
    NSMutableArray* productImageFilesArray;

    NSMutableArray* productVideoFilesArray;
    
//    NSMutableDictionary* selectedVisitDetailsDict;
    
    
    NSInteger selectedIndexforDemoPlan;
    
    BOOL cellSelected;
    
    IBOutlet UIButton *taskStatusButton;
    IBOutlet UIButton *notesStatusButton;
    
    BOOL isFiltered;
    
    NSMutableArray* unfilteredVisitsArray;
    
    BOOL visitFinished;
    
    BOOL isFirstAppearance;
    
    
    NSInteger imagesCount;
    NSInteger VideosCount;
    NSInteger brochuresCount;

    
    IBOutlet SalesWorxTableView *VisitCallsTableView;
    IBOutlet SalesWorxTableViewHeaderView *VisitCallsTableHeaderView;
    IBOutlet UIImageView *NotesImageView;

    IBOutlet UIImageView *TaksImageView;
    IBOutlet UIView *notesandTaksButtonContainerView;
    IBOutlet UIView *NoVisitMessageView;
    
    IBOutlet SalesWorxDefaultLabel *LocaionMapLabel;
    IBOutlet UIButton *LocationContactButton;
    IBOutlet UIButton *LocationMapButton;
    NSMutableArray *selectedVisitCallsArray;
    
    
    IBOutlet UIView *medrepNoVisitDetailsView;
    IBOutlet MedRepElementDescriptionLabel *medrepNoVisitReasonLabel;
    IBOutlet MedRepElementDescriptionLabel *medrepNoVisitDateLabel;
    IBOutlet MedRepTextView *medrepNoVisitCommentsTextView;
    AppControl *appcontrol;
    NSMutableArray *MedrepNoVisitsReasonDBArray;
    NSMutableDictionary *lastVisitDetailsDict;
    IBOutlet UIButton *lastVisitDetailsButton;
    NSString *lastVisitNote;
}
- (IBAction)demoPlanPDFButtonTapped:(id)sender;
- (IBAction)demoPlanVideosButtonTapped:(id)sender;
- (IBAction)demoPlanImagesButtonTapped:(id)sender;
- (IBAction)visitsFilterButtontapped:(id)sender;
-(IBAction)LocationMapViewButtonTapped:(id)sender;
-(IBAction)ContcatDetailsButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *visitEndDateLbl;
@property (strong, nonatomic) IBOutlet UILabel *visitEndDateValueLbl;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBarVisit;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;

@property (strong, nonatomic) IBOutlet UILabel *doctorLbl;

@property (weak, nonatomic) IBOutlet UILabel *plannedVisitTimeLbl;

@property (strong, nonatomic) IBOutlet UILabel *demonstrationPlanLbl;

@property (strong, nonatomic) IBOutlet UILabel *visitTypeLbl;

@property (strong, nonatomic) IBOutlet UITextView *objectiveTxtView;

@property (strong, nonatomic) IBOutlet UILabel *classLbl;

@property (strong, nonatomic) IBOutlet UILabel *tradeChannelLbl;

@property (strong, nonatomic) IBOutlet UILabel *idLbl;

@property (strong, nonatomic) IBOutlet UILabel *otcRxLbl;

@property (strong, nonatomic) IBOutlet UITextView *addressTxtView;

@property (strong, nonatomic) IBOutlet UILabel *emirateLbl;

@property (strong, nonatomic) IBOutlet MKMapView *visitLocationMapView;

@property (strong, nonatomic) IBOutlet UILabel *contactNumberLbl;

@property (strong, nonatomic) IBOutlet UILabel *emailLbl;

@property (strong, nonatomic) IBOutlet UILabel *lastVisitedOnLbl;

@property (strong, nonatomic) IBOutlet UILabel *nextPlannedVisitLbl;

@property (strong, nonatomic) IBOutlet UITableView *visitsTblView;

@property(strong,nonatomic) NSMutableDictionary * startedVisitDict;

@property(strong,nonatomic) NSMutableDictionary *selectedVisitDetailsDict;

@property(strong,nonatomic) NSString* startedVisitIndexStr;

@property(nonatomic) NSInteger startedVisitIndex;

@property(strong,nonatomic) NSMutableDictionary* currentVisitDetailsDict;

- (IBAction)taskButtontapped:(id)sender;

- (IBAction)notesButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *tblBgView;

@property (strong, nonatomic) IBOutlet UILabel *visitDateLbl;

@property(nonatomic) BOOL isFromDashboard;


- (void)showVisitforSelectedDate:(id)sender;

- (IBAction)calenderBtnTapped:(id)sender;


-(void)loadVisitDetailswithIndex:(NSInteger)arrayIndex;




@property UIView *blurMask;
@property UIImageView *blurredBgImage;


-(void)fetchProductMediaFilesforDemoPlanID:(NSString*)demoPlanID;


@end
