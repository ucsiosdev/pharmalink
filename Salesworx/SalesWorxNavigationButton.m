//
//  SalesWorxNavigationButton.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/24/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxNavigationButton.h"
#import "MedRepDefaults.h"

@implementation SalesWorxNavigationButton

@synthesize EnableTitleRTLSupport,EnableImageRTLSupport;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/




-(void)awakeFromNib
{
    
    //if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) {
    //    self.imageView.image = [self.imageView.image imageFlippedForRightToLeftLayoutDirection];
    //}
    
    
    
    self.titleLabel.font=NavigationBarButtonItemFont;
    self.titleLabel.textColor=[UIColor whiteColor];
    //    self.textColor=[UIColor yellowColor];
    
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    

    
    if(EnableTitleRTLSupport)
    {
        [super setTitle:NSLocalizedString(self.titleLabel.text, nil) forState:UIControlStateNormal];
    }
    if(EnableImageRTLSupport)
    {
        if ([UIImageView userInterfaceLayoutDirectionForSemanticContentAttribute:self.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
            [super setImage:[UIImage imageWithCGImage:super.imageView.image.CGImage
                                                scale:1.0 orientation: UIImageOrientationUpMirrored] forState:UIControlStateNormal];
        }
        else
        {
            [super setImage:super.imageView.image forState:UIControlStateNormal];
        }
        
    }
    else
    {
        [super setImage:super.imageView.image forState:UIControlStateNormal];
    }
    
    
}


- (void)setTitle:(nullable NSString *)title forState:(UIControlState)state                     // default is nil. title is assumed to be single line
{
    if(EnableTitleRTLSupport)
    {
        [super setTitle:NSLocalizedString(title, nil) forState:state];
    }
    else
    {
        [super setTitle:title forState:state];
    }
}
- (void)setImage:(UIImage *)image forState:(UIControlState)state                     // default is nil. title is assumed to be single line
{
    if(EnableImageRTLSupport)
    {
        if ([UIImageView userInterfaceLayoutDirectionForSemanticContentAttribute:self.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
            [super setImage:[UIImage imageWithCGImage:image.CGImage
                                                scale:1.0 orientation: UIImageOrientationUpMirrored] forState:state];
        }
        else
        {
            [super setImage:image forState:state];
        }
        
    }
    else
    {
        [super setImage:image forState:state];
    }
    
}


@end
