//
//  SWCustomerDetailViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "CustomerDetailView.h"
#import "CustomerTargetView.h"
#import "CustomerOrderHistoryView.h"
#import "CustomerPriceList.h"
#import "MapView.h"

#import "AlSeerCustomerDashboardViewController.h"


@interface SWCustomerDetailViewController : SWViewController <UIScrollViewDelegate, UIActionSheetDelegate,UIPopoverControllerDelegate>
{
    NSMutableDictionary *customer;
    UIScrollView *detailScroller;
    UIPageControl *pageControl;
    CustomerDetailView *customerDetailView;
    CustomerTargetView *customerTargetView;
    CustomerOrderHistoryView *customerOrderHistoryView;
    CustomerPriceList *  customerPriceListView;
    UISegmentedControl *segments;
    UIActionSheet *actions;
    UIBarButtonItem *flexibleSpace ;
    UIBarButtonItem *lastSyncButton  ;
    AlSeerCustomerDashboardViewController* customerDashboard;
    
    UIPopoverController *locationFilterPopOver;
    
    MapView *mapView;
    
    NSString *isOrderHistoryEnable;
    
    NSString *isCollectionEnable;
    
    BOOL popoverOriented;
    
    int pages;
    
    Place *place;
    
    CSMapAnnotation *Annotation;
    
    SWLoadingView *loadingView;
}

- (id)initWithCustomer:(NSDictionary *)row;
- (void)displayActions:(id)sender;
@end