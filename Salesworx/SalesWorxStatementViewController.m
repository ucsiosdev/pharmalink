//
//  SalesWorxStatementViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/11/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxStatementViewController.h"
#import "SWPlatform.h"
#import "SalesWorxStatementHeaderTableViewCell.h"
#import "SalesWorxStatementTableViewCell.h"
#import "MedRepDefaults.h"
#import "SalesWorxCustomerStatementPDFTemplateView.h"
#import "AppControl.h"
#import "SalesWorxCustomerAgeingReportPDFTemplateView.h"
@interface SalesWorxStatementViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UIBarButtonItem* emailButton;
    UIBarButtonItem* closeButton;
    AppControl *appcontrol;
}
@end

@implementation SalesWorxStatementViewController
@synthesize statementScrollView,selectedCustomer;
- (id)initWithCustomer:(NSDictionary *)c
{
    self = [super init];
    if (self)
    {
        customer=c;
       // [self setTitle:NSLocalizedString(@"Statement", nil)];
        
       self.navigationItem.titleView=  [SWDefaults createNavigationBarTitleView:@"Statement"];
        
        _total = 0;
        _balance = 0;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appcontrol=[AppControl retrieveSingleton];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    [Flurry logEvent:@"Statement View"];

    NSLog(@"statement view called");
    
    balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, 25)] ;
    [balanceLabel setText:@""];
    [balanceLabel setTextAlignment:NSTextAlignmentCenter];
    [balanceLabel setBackgroundColor:[UIColor redColor]];
    [balanceLabel setTextColor:[UIColor whiteColor]];
    [balanceLabel setFont:BoldSemiFontOfSize(14.0f)];
    
    closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=closeButton;
    
    emailButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Email", nil) style:UIBarButtonItemStylePlain target:self action:@selector(emailButtonTapped)];
    [emailButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    if([appcontrol.ENABLE_CUSTOMER_STATEMENT_PDF_EMAIL isEqualToString:KAppControlsYESCode]){
        self.navigationItem.rightBarButtonItem=emailButton;
    }
    
    @try {
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        spinner.center = _statementTableView.center;
        [_statementTableView addSubview: spinner];
        [spinner startAnimating];
        [closeButton setEnabled:NO];

        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void)
                       {
                           
                           [self getSalesOrderServiceDidGetAllInvoices: [[SWDatabaseManager retrieveManager] dbGetInvoicesOfCustomer:[customer stringForKey:@"Customer_ID"] withSiteId:[customer stringForKey:@"Site_Use_ID"]andShipCustID:[customer stringForKey:@"Ship_Customer_ID"] withShipSiteId:[customer stringForKey:@"Ship_Site_Use_ID"] andIsStatement:YES]];
                           
                           dispatch_async(dispatch_get_main_queue(), ^(void){
                               /** disabling the uifields*/

                               [closeButton setEnabled:YES];
                               [_statementTableView reloadData];
                               if (invoices.count > 0)
                               {
                                   [self setupToolbar];
                               }
                               [spinner stopAnimating];
                           });
                       });
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //fixing the total label
    
    _totalAmtPaid = [[NSUserDefaults standardUserDefaults]doubleForKey:@"totalAmountPaid"];
    _total = _total-_totalAmtPaid;
    
    
    
}
-(void)emailButtonTapped{
    emailButton.title = @"Preparing PDF. Please Wait..";
    [self DisbaleNavigationBarActions];
    [self performSelector:@selector(GenerateStatementPDFDocument) withObject:nil afterDelay:0.5];
}
-(void)DisbaleNavigationBarActions{
    [emailButton setEnabled:NO];
    [closeButton setEnabled:NO];
}
-(void)EnableNavigationBarActions{
    [emailButton setEnabled:YES];
    [closeButton setEnabled:YES];
}
-(void)GenerateStatementPDFDocument{
    
    if (statementSegmentControl.selectedSegmentIndex == 0) {
        NSString *documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        // get the image path
        NSString *documentPathComponent=[NSString stringWithFormat:@"CustomerSatement-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]];
       // NSString *emailAddres = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:[selectedCustomer valueForKey:@"Customer_ID"]];
        NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
       
        
        // make sure the file is removed if it exists
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:filename]) {
        }
        
        NSInteger numberOfPages = (invoices.count%15) == 0 ? (invoices.count/15) : (invoices.count/15)+1;
        NSMutableData *pdfData = [NSMutableData data];
        UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
        
        for (NSInteger i=0; i< numberOfPages ; i ++) {
            SalesWorxCustomerStatementPDFTemplateView * PageTemplate =[[SalesWorxCustomerStatementPDFTemplateView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
            PageTemplate.selectedCustomer = customer;
            PageTemplate.arrCustomersStatement = invoices;
            PageTemplate.pageNumber =i+1;
            
            [PageTemplate UpdatePdfPgae];
            
            UIGraphicsBeginPDFPage();
            CGContextRef pdfContext = UIGraphicsGetCurrentContext();
            [PageTemplate.layer renderInContext:pdfContext];
        }
        
        UIGraphicsEndPDFContext();
        emailButton.title = NSLocalizedString(@"Email", nil);
        if ([MFMailComposeViewController canSendMail]){
            MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
            mailController.mailComposeDelegate = self;
            [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
            NSMutableArray * orgNameArary = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select  Value_1  from TBL_Custom_Info  where Info_Type='ORG_NAME'and Info_Key = (select Organization_ID from TBL_user )"];
             if(orgNameArary.count >0 ){
            [mailController setSubject:[NSString stringWithFormat:@"Customer Statement - %@ - %@",[selectedCustomer valueForKey:@"Customer_Name"],[[orgNameArary objectAtIndex:0] valueForKey:@"Value_1"]]];
             }
            [mailController setMessageBody:@"Hi,\n Please find the attached statement." isHTML:NO];
            
            if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
                
                if ([selectedCustomer valueForKey:@"Customer_ID"] != nil){
                
                 NSString *emailAddres = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:[selectedCustomer valueForKey:@"Customer_ID"]];
                 [mailController setToRecipients:@[emailAddres]];
               }
            }
            

            
           
            [mailController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"CustomerSatement-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]]];
            [self presentViewController:mailController animated:YES completion:nil];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
            [self EnableNavigationBarActions];
        }
    }
    else{
        NSString *documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        // get the image path
        NSString *documentPathComponent=[NSString stringWithFormat:@"CustomerAgeingReport-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]];
        NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        
        
        // make sure the file is removed if it exists
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:filename]) {
        }
        
        NSInteger numberOfPages = (customerDuesArray.count%15) == 0 ? (customerDuesArray.count/15) : (customerDuesArray.count/15)+1;
        NSMutableData *pdfData = [NSMutableData data];
        UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
        
        for (NSInteger i=0; i< numberOfPages ; i ++) {
            SalesWorxCustomerAgeingReportPDFTemplateView * PageTemplate =[[SalesWorxCustomerAgeingReportPDFTemplateView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
            PageTemplate.selectedCustomer = customer;
            PageTemplate.arrCustomersAgeingReport = customerDuesArray;
            PageTemplate.pageNumber =i+1;
            
            [PageTemplate UpdatePdfPgae];
            UIGraphicsBeginPDFPage();
            CGContextRef pdfContext = UIGraphicsGetCurrentContext();
            [PageTemplate.layer renderInContext:pdfContext];
        }
        
        UIGraphicsEndPDFContext();
        emailButton.title = NSLocalizedString(@"Email", nil);
        if ([MFMailComposeViewController canSendMail]){
            MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
            mailController.mailComposeDelegate = self;
            [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
            [mailController setSubject:[NSString stringWithFormat:@"Customer ageing report - %@",[selectedCustomer valueForKey:@"Customer_Name"]]];
            [mailController setMessageBody:@"Hi,\n Please find the attached report." isHTML:NO];
            [mailController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"CustomerAgeingReport-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]]];
            
            
            if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
                
                if ([selectedCustomer valueForKey:@"Customer_ID"] != nil) {
                   NSString *emailAddres = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:[selectedCustomer valueForKey:@"Customer_ID"]];
                   [mailController setToRecipients:@[emailAddres]];
              }
            }
            
            
            [self presentViewController:mailController animated:YES completion:nil];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
            [self EnableNavigationBarActions];
        }
    }
}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        [self EnableNavigationBarActions];
    }];
}
-(void)viewDidAppear:(BOOL)animated
{
    statementSegmentControl.sectionTitles = @[NSLocalizedString(@"Statement", nil), NSLocalizedString(@"Ageing Report", nil)];
    statementSegmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    statementSegmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    
    statementSegmentControl.selectionIndicatorBoxOpacity=0.0f;
    statementSegmentControl.verticalDividerEnabled=YES;
    statementSegmentControl.verticalDividerWidth=1.0f;
    statementSegmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    statementSegmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    statementSegmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    statementSegmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    statementSegmentControl.tag = 3;
    statementSegmentControl.selectedSegmentIndex = 0;
    
    [statementSegmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    __weak typeof(self) weakSelf = self;
    [statementSegmentControl setIndexChangeBlock:^(NSInteger index) {
        // [weakSelf.customersScrollView scrollRectToVisible:CGRectMake(statementSegmentControlWidth * index, 0, statementSegmentControlWidth, 200) animated:YES];
      //  [weakSelf.statementScrollView setContentOffset:CGPointMake(statementSegmentControl.frame.size.width * index, 0)];
        
        
    }];
//    if (statementSegmentControl.selectedSegmentIndex==0) {
//        
//        [statementScrollView scrollRectToVisible:CGRectMake(0, 0, statementSegmentControl.frame.size.width, 200) animated:NO];
//        
//    }
//    else if (statementSegmentControl.selectedSegmentIndex==1)
//    {
//        //[customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth, 0, SegmentControlWidth, 200) animated:NO];
//        [statementScrollView setContentOffset:CGPointMake(statementSegmentControl.frame.size.width, 0)];
//        
//        
//    }
//    else
//    {
//        // [customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth *2, 0, SegmentControlWidth, 200) animated:NO];
//        [statementScrollView setContentOffset:CGPointMake(statementSegmentControl.frame.size.width*2, 0)];
//        
//    }
//    
    
    //[statementScrollView setContentOffset:CGPointMake(statementSegmentControl.frame.size.width, 0)];

    
    customerDuesArray=[[SWDatabaseManager retrieveManager] fetchDuesForCustomer:selectedCustomer];
    
    if (customerDuesArray.count>0) {
        duesDict=[customerDuesArray objectAtIndex:0];
    }
    NSLog(@"customer dues array in statement %@",customerDuesArray);

}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
     NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    
    if (segmentedControl.selectedSegmentIndex==0) {
        [self.navigationController setToolbarHidden:NO animated:YES];
    }
    else{
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
    
    [_statementTableView reloadData];
    
    
}
-(void)closeTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIBarButtonItem *btn = [[_popOverController valueForKey:@"_delegate"]valueForKey:@"leftBtn"];
    btn.enabled = YES;
}
- (void)setupToolbar
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 550, 25)];
    [label setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Balance", nil), [[NSString stringWithFormat:@"%f",_total]currencyString]]];
    [label setTextAlignment:NSTextAlignmentRight];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor colorWithRed:(89.0/255.0) green:(114.0/255.0) blue:(153.0/255.0) alpha:1]];
    [label setFont:BoldSemiFontOfSize(18.0f)];
    UIBarButtonItem *lastSyncButton = [[UIBarButtonItem alloc]initWithCustomView:label];
    
    [self.navigationController setToolbarHidden:NO animated:YES];
    self.navigationController.toolbar.barTintColor = [UIColor colorWithRed:(240.0/255.0) green:(245.0/255.0) blue:(255.0/255.0) alpha:1];
    
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace,flexibleSpace, lastSyncButton, nil] animated:YES];
}
#pragma mark UITableView Data source Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (statementSegmentControl.selectedSegmentIndex==0) {
        return invoices.count;

    }
    else
    {
        if (customerDuesArray.count>0) {
            return 7;
        }
        else
        {
            return 0;
        }
    }
}

#pragma mark UITableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (statementSegmentControl.selectedSegmentIndex==0) {

    static NSString* identifier=@"Cell";
    SalesWorxStatementHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxStatementHeaderTableViewCell" owner:nil options:nil] firstObject];
    }
        [cell.lblDocumentNo setIsHeader:YES];
        [cell.lblInvoiceDate setIsHeader:YES];
        [cell.lblBalanceAmt setIsHeader:YES];
        [cell.lblPaidAmt setIsHeader:YES];
        
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        [cell.contentView.layer setBorderWidth: 1.0];
        [cell.contentView.layer setMasksToBounds:YES];
        [cell.contentView.layer setBorderColor:kUITableViewSaperatorColor.CGColor];
    
    return cell;
    }
    
    else
    {
        static NSString* identifier=@"duesCell";
        SalesWorxCustomerDuesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerDuesTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        NSLog(@"cell frame is %@",NSStringFromCGRect(tableView.frame));
        
        CGRect sepFrame = CGRectMake(0,43, tableView.frame.size.width, 1);
        UIView*  seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = kUITableViewSaperatorColor;
        [cell addSubview:seperatorView];

        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        cell.titleLbl.isHeader=YES;
        cell.descLbl.isHeader=YES;
        cell.titleLbl.text=@"Period";
        cell.descLbl.text=@"Outstanding";
        
        return cell;

    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];

    if (statementSegmentControl.selectedSegmentIndex==0) {

        
    static NSString* identifier=@"Cell";
    SalesWorxStatementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxStatementTableViewCell" owner:nil options:nil] firstObject];
    }
    
    cell.lblDocumentNo.text = [[invoices objectAtIndex:indexPath.row]objectForKey:@"Invoice_Ref_No"];
    cell.lblInvoiceDate.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[[invoices objectAtIndex:indexPath.row]valueForKey:@"InvDate"]];
    cell.lblBalanceAmount.text = [[invoices objectAtIndex:indexPath.row]currencyStringForKey:@"NetAmount"];
    cell.lblPaidAmount.text = [[invoices objectAtIndex:indexPath.row]currencyStringForKey:@"PaidAmt"];
    if ([[invoices objectAtIndex:indexPath.row] objectForKey:@"Paid"]) {
        cell.lblSettledAmount.text = [[invoices objectAtIndex:indexPath.row] currencyStringForKey:@"Paid"];
    } else {
        cell.lblSettledAmount.text = [NSString stringWithFormat:@"%@0.00",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
    }
    
    return cell;
    
    
    }
    else
    {
        
    duesDict=[customerDuesArray objectAtIndex:0];
        
            static NSString* identifier=@"duesCell";
            SalesWorxCustomerDuesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerDuesTableViewCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            NSMutableArray * prevoiusMonthsArray=[[NSMutableArray alloc]init];
            
            BOOL ageingReportDisplayFormatMonthly=[SWDefaults isAgeingReportDisplayFormatMonthly];
            if (ageingReportDisplayFormatMonthly) {
                prevoiusMonthsArray=[SWDefaults fetchPreviousMonthsforAgeingReport];
            }
            
            if(indexPath.row==0)
            {
                if (ageingReportDisplayFormatMonthly) {
                    [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:0]];
                }
                else{
                    [cell.titleLbl setText:NSLocalizedString(@"30 Days", nil)];
                }
                [cell.descLbl setText:[duesDict currencyStringForKey:@"M0_Due"]];
            }
            else  if(indexPath.row==1)
            {
                if (ageingReportDisplayFormatMonthly) {
                    [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:1]];
                }
                else{
                    [cell.titleLbl setText:NSLocalizedString(@"60 Days", nil)];
                }
                [cell.descLbl setText:[duesDict currencyStringForKey:@"M1_Due"]];
            }
            else  if(indexPath.row==2)
            {
                if (ageingReportDisplayFormatMonthly) {
                    [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:2]];
                }
                else{
                    [cell.titleLbl setText:NSLocalizedString(@"90 Days", nil)];
                }
                [cell.descLbl setText:[duesDict currencyStringForKey:@"M2_Due"]];
            }
            else  if(indexPath.row==3)
            {
                if (ageingReportDisplayFormatMonthly) {
                    [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:3]];
                }
                else{
                    [cell.titleLbl setText:NSLocalizedString(@"120 Days", nil)];
                }
                [cell.descLbl setText:[duesDict currencyStringForKey:@"M3_Due"]];
            }
            else  if(indexPath.row==4)
            {
                if (ageingReportDisplayFormatMonthly) {
                    [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:4]];
                }
                else{
                    [cell.titleLbl setText:NSLocalizedString(@"150 Days", nil)];
                }
                [cell.descLbl setText:[duesDict currencyStringForKey:@"M4_Due"]];
            }
            else if (indexPath.row==5)
            {
                if (ageingReportDisplayFormatMonthly) {
                    [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:5]];
                }
                else{
                    [cell.titleLbl setText:NSLocalizedString(@"180 Days", nil)];
                }
                [cell.descLbl setText:[duesDict currencyStringForKey:@"M5_Due"]];
                
            }
            else  if(indexPath.row==6)
            {
                [cell.titleLbl setText:NSLocalizedString(@"Prior Due", nil)];
                [cell.descLbl setText:[duesDict currencyStringForKey:@"Prior_Due"]];
            }
            
        
        return cell;
    
}



}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma Sales Order Service Delegate
- (void)getSalesOrderServiceDidGetAllInvoices:(NSArray *)_invoices
{
    invoices=[NSMutableArray arrayWithArray:_invoices];
    for (int i = 0; i < invoices.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [invoices objectAtIndex:i];
            double amount = [[row objectForKey:@"NetAmount"] doubleValue];
            
            _total = _total + amount;
        }
    }
    _invoices=nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
}


@end
