//
//  SWDatabaseManager.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/7/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SWQuery.h"
#import "SWSection.h"
#import "Singleton.h"
#import "CustomerShipAddressDetails.h"
#import <CoreLocation/CoreLocation.h>
#import "XMLWriter.h"
#import "SWPlatformDatabaseConstants.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "SWPlatform.h"
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"
#import <NSDictionary+Additions.h>
#import "NSObject+NullDictionary.h"
#import "JSONModel.h"
#define k30DaysDueTime @"M0_Due"
#define k60DaysDueTime @"M1_Due"
#define k90DaysDueTime @"M2_Due"
#define k120DaysDueTime @"M3_Due"
#define k150DaysDueTime @"M4_Due"

@class xswiViewController;

enum errorCodes {
	kDBNotExists,
	kDBFailAtOpen,
	kDBFailAtCreate,
	kDBErrorQuery,
	kDBFailAtClose
};


@interface SWDatabaseManager : NSObject <CLLocationManagerDelegate> {

//    NSMutableArray *resultsArray;
//    NSMutableDictionary *result;
    //SWSection *sectionItem;
   // NSArray *resultSet;
  //  NSString *columnName;
    BOOL databaseIsOpen;
    //
    
    //Product
    BOOL isFilter;

    //Dashboard
    //NSMutableArray *actualVisitArray;

    //Customer
   // NSMutableArray *sections;
    int currentCharacter;
    BOOL shouldStop;
    
    //Send Order
    NSMutableDictionary *mainDictionary;
    NSMutableDictionary *orderDictionary;
    NSMutableDictionary *itemDictionary ;
    NSMutableDictionary *lotDictionary ;
    NSMutableArray *orderArray1;
    NSMutableArray *itemArray;
    NSMutableArray *lotArray1;
    NSMutableArray *mainArray;
    //       id target;
    SEL action;
   // SWDatabaseManager *dataBase;
    //XMLWriter* xmlWriter;
    
    //Collection
    BOOL isCalled;
    
    //Route
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
    
    
    
    ////FOR VISIT MANAGER

}


@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

+ (SWDatabaseManager *)retrieveManager;
+ (void)destroyMySingleton;
- (void)executeNonQuery:(NSString *)sql ;
- (NSMutableArray *)fetchDataForQuery:(NSString *)sql;
-(BOOL)databaseExists;

//////SQLFunction//////
- (void)InsertdataCustomerResponse:(NSString*)strquery;
- (NSArray*)selectResponseType:(int)QuestionId;
- (NSArray*)selectQuestionBySurveyId:(int)SurveyId;
- (NSArray*)selectSurveyWithCustomerID:(NSString *)customerID andSiteUseID:(NSString *)siteID;
- (int) dbGetCustomerResponseCount;
- (int) dbGetAuditResponseCount;

//Communication module
- (int) dbGetIncomingMessageCount;
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_ID:(NSString *)Customer_ID SiteUse_ID:(NSString *)SiteUse_ID;
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_Name:(NSString *)Customer_Name;

//- (NSArray*)dbGetPlannedVisitDetails:(NSString*)strQuery;
- (NSArray*)dbGetUsersList;
- (NSArray*)dbGetFSRMessages;
- (NSArray*)dbGetFSRSentMessages;
-(void)updateReadMessagestatus:(NSString*)strQuery;
-(NSArray*)checkSurveyIsTaken:(NSString *)customerID andSiteUseID:(NSString *)siteID ansSurveyID:(NSString *)surveyID;
-(void)checkUnCompleteVisits;
-(void)updateEndTimeWithID:(NSString *)visitID;
-(NSString*)dbGetOrderCount;
-(NSString*)dbdbGetCollectionCount;
-(NSString*)dbGetReturnCount;
-(NSMutableDictionary*)dbGetPriceOrProductWithID:(NSString *)itemID;
-(void)deleteManageOrderWithRef:(NSString*)strQuery;
-(NSArray*)dbGetAppControl;
-(NSMutableDictionary *)dbGetBonusItemOfProduct:(NSString *)itemCode;
-(NSArray*)dbGetAllCategory;
-(NSArray*)dbGetBonusInfoOfProduct:(NSString *)product;
-(NSString*)dbGetOnOrderQuantityOfProduct:(NSString *)itemID;
-(void)deleteTemplateOrderWithRef:(NSString*)strQuery;
-(NSArray*)dbGetMultiCurrency;

//Reports Function
-(NSMutableArray*)dbGetDataForReport:(NSString*)sql;

//product Ser
- (NSArray *)dbGetProductCollection;
- (NSArray *)dbGetProductDetail:(NSString *)itemId organizationId:(NSString *)orgId;
- (NSArray *)dbGetStockInfo:(NSString *)itemId;
- (NSArray *)dbGetBonusInfo:(NSString *)itemId;
- (NSArray *)dbdbGetTargetInfo:(NSString *)itemId;
- (NSArray *)dbGetFilterByColumn:(NSString *)columnName;
- (NSArray *)dbGetCategoriesForCustomer:(NSDictionary *)customer;
- (NSArray *)dbGetProductsOfCategory:(NSDictionary *)category;
- (NSArray *)dbGetStockInfoForManage:(NSString *)itemId;
//- (NSMutableDictionary *)getProductsOfBonus:(NSString *)bname;
//- (NSArray *)checkGenericPriceOfProduct:(NSString *)productID;
- (NSArray *)checkGenericPriceOfProduct:(NSString *)productID :(NSString*)PriceListID;


- (NSMutableDictionary *)dbdbGetPriceListProduct:(NSString *)items;


//Dashboard
-(NSArray *)dbGetListofCustomer;
-(NSArray *)dbGetTotalPlannedVisits;
-(NSArray *)dbGetTotalCompletedVisits;
-(NSArray *)dbGetTotalOrder;
-(NSArray *)dbGetTotalReturn;
-(NSArray *)dbGetTotalCollection;
-(NSArray *)dbGetSaleTrend;
-(NSArray *)dbGetCustomerStatus:(NSString *)customer;
-(NSArray *)dbGetCustomerSales:(NSString *)customer;
-(NSArray *)dbGetCustomerOutstanding:(NSString *)customer;
-(NSArray *)dbGetCustomerOutOfRoute;
-(NSArray *)dbGgtDetailofCustomer:(NSString *)customerID andSite:(NSString *)siteID;


////////Session
-(NSString *)verifyLogin:(NSString *)login andPassword:(NSString *)password;
-(NSString *) decrypt:(NSString *)encryptedtext andKey:(NSString *)keytext;
-(NSString *) encrypt:(NSString *)plaintext andKey:(NSString *)keytext;
-(void)writeXML;
-(NSString*) digest:(NSString*)input;

//////Customer
- (NSArray *)dbGetCollectionFromReports;
- (NSArray *)dbGetCollection;
- (NSArray *)dbGetFilterByColumnCustomer:(NSString *)columnName;
- (NSArray *)dbGetTarget:(NSString *)customerCode;
- (NSArray *)dbGetRecentOrders:(NSString *)customerId;
- (NSArray *)dbGetPriceList:(NSString *)customerId;
- (NSArray *)dbGetPriceListGeneric:(Customer *)customer;
- (NSArray *)dbGetOrderAmountForAvl_Balance:(NSString *)customerId;
- (NSArray *)dbGetCashCutomerList;
- (NSArray *)dbGetCustomerProductSalesWithCustomerID:(NSString *)custID andProductId:(NSString *)productID;
- (void)cancel;

////////////////////Distribution/////////////////////
- (NSArray *)dbGetDistributionCollection;
- (NSArray *)checkCurrentVisitInDC:(NSString *)visitID;
- (NSString *)saveDistributionCheckWithCustomerInfo:(Customer *)customerInfo andDistChectItemInfo:(NSMutableArray *)fetchDistriButionCheckLocations;

///SalesOrder
- (NSString *)savePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID;
- (NSString *)saveOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID;
- (void)saveOrderHistoryWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderID:(NSString *)orderID andOrderRef:(NSString *)lastOrderReference;
- (void)deleteSalesPerformaOrderItems:(NSString *)refId;
- (NSString *)updatePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderRef:(NSString *)orderRef;
- (NSString *)saveReturnOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID;
- (void)saveTemplateWithName:(NSString *)name andOrderInfo:(NSMutableArray *)orderInfo;
- (void)saveDocAdditionalInfoWithDoc:(NSString *)docNumber andDocType:(NSString *)docType andAttName:(NSString *)attName andAttValue:(NSString *)attVale andCust_Att_5:(NSString *)cust_Att_5;
- (NSArray *)dbGetReturnsType;
-(NSString*)isItemRestrictedForReturn:(NSString*)inventoryItemID;

- (NSArray *)dbGetReasonCode;
- (NSArray *)dbGetTemplateOrder;
- (NSArray *)dbGetTemplateOrderItem:(NSString *)refId;
- (NSArray *)dbGetPerformaOrder:(NSString *)customerId withSiteId:(NSString *)siteId;
- (NSArray *)dbGetPerformaOrder;
- (NSArray *)dbGetConfirmOrder:(NSString *)customerId withSiteId:(NSString *)siteId;
- (NSArray *)dbGetSalesConfirmOrderItems:(NSString *)refId;
- (NSArray *)dbGetSalesPerformaOrderItems:(NSString *)refId;
- (NSArray *)dbGetSalesOrderItems:(NSString *)refId;
- (NSArray *)dbGetSalesOrderInvoice:(NSString *)refId;
- (NSArray *)dbGetInvoicesOfCustomer:(NSString *)customerId withSiteId:(NSString *)siteId andShipCustID:(NSString *)sCusId withShipSiteId:(NSString *)sSiteId andIsStatement:(BOOL)isStatement;
-(void)saveConfirmedOrder : (NSMutableDictionary *)order;


//SendOrder
- (void)dbGetConfirmOrder;
- (void)dbGetConfirmOrderItems;
- (void)dbGetConfirmOrderLots;
- (void)writeXMLString;
- (void)deleteSalesOrder;

////////Collection
//- (BOOL)saveCollection:(NSDictionary *)collectionInfo withCustomerInfo:(NSDictionary *)customerInfo andInvoices:(NSArray *)invoices;

//Route
- (NSArray *)dbGetCollectionFromDate:(NSDate *)date;
- (NSArray *)dbGetOpenVisiteFromCustomerID:(NSString *)customer_ID andSiteID:(NSString *)siteID;

- (void)saveVistWithCustomerInfo:(NSMutableDictionary *)customerInfo andVisitID:(NSString *)visitID;
- (void)saveVisitEndDateWithVisiteID:(NSString *)visitID;
- (NSArray *)dbGetDuesStaticWithCustomerID:(NSString *)customerID;
- (void)saveVisitStatusWithVisiteID:(NSString *)visitID;
- (void)insertProductAddInfo:(NSDictionary *)row;


- (BOOL)saveCollection:(CustomerPaymentClass *)customerPaymentDetails withCustomerInfo:(NSDictionary *)customerInfo :(UIImage *)signImage :(NSMutableArray *)arrCollectionImages;


-(NSMutableArray*)fetchCustomerswithOpenInvoices;

//modify response

-(NSArray*)ModifyResponseType:(NSString*)query;


-(NSMutableArray*)fetchAgencies;
-(NSMutableArray*)fetchUOMforItem:(NSString*)itemCode;
//- (NSArray *)checkGenericPriceOfProductwithUOM:(NSString *)productID UOM:(NSString*)SelectedUOM;
- (NSArray *)checkGenericPriceOfProductwithUOM:(NSString *)productID UOM:(NSString*)SelectedUOM :(NSString*)priceListID;

- (NSArray *)dbGetProductDetailwithUOM:(NSString *)itemId organizationId:(NSString *)orgId;

-(BOOL)saveDocAdditionalInfo:(NSString *)docNumber andDocType:(NSString *)docType andAttName:(NSString *)attName andAttValue:(NSString *)attVale andCust_Att_5:(NSString *)cust_Att_5;

+(NSString*)isCustomerBlocked:(NSString*)customerID;

-(NSArray*)fetchProductDetailswithRanking :(NSString *)itemId organizationId:(NSString *)orgId;
-(NSArray*)fetchProductswithRanking:(NSDictionary*)category;


+(NSMutableArray*)fetchAllCustomers;

-(NSArray*)fetchCategoriesForCustomer:(Customer*)customer;
-(NSMutableArray*)fetchTargetInformationForCustomer:(Customer*)selectedCustomer;
-(NSMutableArray*)fetchBrandsforCustomer:(Customer*)selectedCustomer;
-(NSMutableArray*)fetchTotalOutStandingforCustomer:(Customer*)selectedCustomer;
- (NSArray *)dbGetCustomerTotalSalesWithCustomerID:(NSString *)custID;

//dashboard
-(NSMutableArray*)fetchTargetInformationForCurrentMonth;

//products
-(NSMutableArray*)fetchProductsArray;
-(NSString*)fetchCreationDateforInvoice:(NSString*)erpRefNo;
-(NSString*)dbGetSpecialDiscount:(NSString*)productID;
-(NSMutableArray*)fetchProductTarget:(NSString*)agency;


-(NSArray *)dbGetCustomerOutOfRoute:(NSString *)date;
-(NSArray *)dbGetTotalPlannedVisits:(NSString *)date;
-(NSArray *)dbGetTotalCompletedVisits:(NSString *)date;
-(NSArray *)dbGetCustomerOutOfRouteCount:(NSString *)date;
//-(NSMutableArray*)fetchProductPriceList:(Products*)selectedProd;
-(NSMutableArray*)fetchProductPriceList:(Products*)selectedProd withCustomer:(Customer*)customerObject;
-(NSMutableArray*)fetchProductGenericPriceList:(Products*)selectedProduct;



-(NSMutableArray*)fetchBonusforSelectedProduct:(Products*)selectedProd;
-(NSMutableArray*)fetchStockwithOnOrderQty:(Products*)selectedProduct WareHouseId:(NSString *)OrgId;

-(NSMutableArray*)fetchUOMforProduct:(NSString*)itemCode;
-(NSMutableArray*)fetchProductImagesforSelectedProduct:(Products*)selectedProd;
-(NSString*)fetchConversionRateForSelectedUOM:(Products*)selectedProd;
-(NSMutableArray *)fetchDuesForCustomer:(Customer *)selectdCust;

//visit options

-(BOOL)saveVisitDetails:(SalesWorxVisit*)curentVisit;
-(SalesWorxVisit*)fetchRecentOrdersforCustomer:(SalesWorxVisit*)currentVisit;

//product categories
-(NSMutableArray*)fetchProductCategoriesforCustomer:(SalesWorxVisit*)currentVisit;

//order templete
-(NSMutableArray*)fetchOrderTempletes;
-(NSMutableArray*)fetchOrderTemplateLineItems:(SalesOrderTemplete*)selectedTemplate;
-(NSMutableArray*)fetchConventionalTemplate:(NSMutableArray*)templateObjectsArray;
-(NSMutableArray*)fetchOrderTempletesForCustomer:(Customer*)customer;
-(NSMutableArray*)fetchProductDetailsForInventoryItemId:(NSString*)inventory_Item_ID AndOrganization_Id:(NSString*)productOrganizationId;
//parent orders
-(NSMutableArray*)fetchParentOrdersforVisitID:(SalesWorxVisit*)currentVisit;

//product categories
- (NSArray *)fetchProductsforCategory:(ProductCategories *)category AndCustomer:(Customer *)customer andTransactionType:(NSString*)transactionTypeString;
- (NSArray *)fetchProductsforCategoryWithRanking:(ProductCategories *)category AndCustomer:(Customer *)customer andTransactionType:(NSString*)transactionTypeString;

-(NSArray *)dbGetAssignedLotsForOrderId:(NSString*)OrderId AndLineNumber:(NSString *)lineNumber;
-(NSArray *)dbGetAssignedLotsForProformaOrderId:(NSString*)OrderId AndLineNumber:(NSString *)lineNumber;
-(BOOL)DeleteProformaOrderWithId:(NSString *)orderId;
-(NSMutableArray *)DBGetRMALotTypes;
-(NSMutableArray *)DBGetTBLReasonCodesForPurpose:(NSString*)purpose;
-(NSString *)getLongDescriptionForInventoryItemId:(NSString*)InventoryItemId;
-(NSArray*)checkBrandAmbassadorSurveyTaken:(NSString*)surveyID;


-(NSMutableArray *)fetchProductWareHouseStock:(Products*)selectedProduct;
-(NSMutableArray *)fetchProductPOStock:(Products*)selectedProduct;
-(NSMutableArray*)fetchavAvailableProductPriceLists:(Products*)selectedProd;
-(NSMutableArray*)fetchavAvailableProductPriceListsDescription:(Products*)selectedProd;
//visit completion

-(NSMutableArray*)fetchOpenVisitsforOtherCustomers:(NSString*)currentCustomerID;
-(NSString*)fetchuserNamefromDB;


-(BOOL)closePlannedVisitwithVisitID:(NSString*)visitID;


-(BOOL)updateVisitEndDatewithAlert:(NSString*)visitID;
-(NSDictionary*)FetchTodayIncompletedPlannedVisitDetailsForCustomer:(Customer*)customer;
- (NSArray *)dbGetRecentOrdersForCustomer:(Customer *)customer;
-(NSInteger)dbGetOrderCountForVisit:(NSString *)visitId;
-(NSString*)dbGetOrdersValueForVisit:(NSString *)visitId;
-(NSMutableArray*)fetchRecommendedOrderTemplateLineItemsForcustomer:(Customer *)customerDetails andCategory:(ProductCategories *)category AndCurrentVisitDcItems:(NSMutableArray *)dcItems;
-(NSMutableArray*)fetchDCReOrderItemsForCustomer:(Customer *)customerDetails andCategory:(ProductCategories *)category AndCurrentVisitDcItems:(NSMutableArray *)dcItems;

//To Do
+(BOOL)InsertToDoList:(ToDo*)toDoArray;
-(NSMutableArray*)fetchTodoforCustomer:(Customer*)customer;
-(void)deleteOrdersAndReturnsAfterBackGroundSync:(NSString *)syncStr AndLastSyncTime:(NSString *)lastSync;

//ONSITE exception reason

-(BOOL)updateOnsiteExceptionReason:(SalesWorxVisit*)currentVisit;
-(NSString*)fetchCustomerLocationRange:(Customer*)selectedCustomer;
-(BOOL)isDistributionCheckAvailable;
-(void)InsertFSRNewMessages:(NSArray *)newMessages;

-(NSArray*)fetchSurveyforBrandAmbassador;
-(NSMutableArray*)fetchListValueOfBankNames;
-(NSMutableArray*)fetchTodoList;
- (NSArray *)dbGetSalesReOrderItems:(NSString *)refId;


//Dashboard methods
-(NSMutableArray*)fetchSalesTrendWithDocType:(NSString*)docType;
-(NSMutableArray*)fetchCustomerdataforPlannedVisits;
-(NSMutableArray*)fetchSalesTrendInformation;
-(double)fetchTotalTransactionValue:(NSString*)documentType frequency:(BOOL)isMonthly;

#pragma mark Communication v2 methods
-(BOOL)InsertV2MessagesIntoDb:(NSDictionary *)messageDetails;
- (NSArray*)dbGetV2FSRSentMessages;
- (NSArray*)dbGetV2FSRMessages;
-(BOOL)InsertV2ReplyMessagesIntoDb:(NSDictionary *)messageDetails;
- (BOOL)updateV2MessageReadStatus:(NSString*)messageId;
-(void)InsertV2FSRTBLMSGs:(NSArray *)newMessages;
-(void)InsertV2FSRTBLMSGReciepients:(NSArray *)newMessages;

#pragma mark Background stock sync methods
-(BOOL)updateStockfromStockSync:(NSMutableArray*)productStockArray;
-(NSString*)fetchLastStockSyncDate;

#pragma mark Reports
-(NSArray*)fetchSalesSummaryData;
-(NSArray*)fetchCustomerStatementData;
-(NSArray*)fetchBlockedCustomerData;
-(NSMutableArray*)fetchReviewDocumentsData;
-(NSMutableArray*)fetchPaymentSummaryData;
-(NSMutableArray*)fetchTargetVSAchievementReport:(NSString*)incentiveType year:(NSString *)year period:(int)period;
-(NSMutableArray*)fetchTotalReport:(NSString*)incentiveType year:(NSString *)year period:(int)period;

#pragma mark Visit Options Flags
-(NSMutableArray*)fetchVisitOptionsFlagsforCustomer:(NSString*)customerID;

#pragma mark Accompanied by Field Sales Visit
-(NSMutableArray*)fetchAccompaniedByForFieldSalesVisit;

/**DC*/
-(NSMutableArray*)fetchDistributionCheckItemsForcustomer;
-(NSMutableArray *)fetchDistriButionCheckLocations;
-(NSMutableArray*)fetchDCMinStockOfMSLItems;

/**UOM*/
-(NSMutableArray *)getPriceListAvailableUOMSForSalesOrderProduct:(Products *)product AndCustomer:(Customer *)customer;
-(NSMutableArray *)getUOMsForProduct:(Products *)product;
-(NSArray *)fetchCutomerDetailsForBarCode:(NSString *)barcode;

/*iBeacon*/
-(NSMutableArray*)fetchCustomerIDwithBeacon:(SalesWorxBeacon*)matchedBeacon;
-(NSMutableArray*)fetchBeaconDetailsforCustomers;
-(NSMutableArray*)fetchProductsDetails;

// collection target
-(NSMutableArray*)fetchCollectionTarget:(NSString *)Customer_No;


////***Merchandising**////
-(NSMutableArray*)fetchPlanogramData:(NSString*)brandCode;
-(NSMutableArray*)fetchBrandCodes;
-(NSMutableArray*)fetchMerchandisingMasterData;
-(NSMutableArray*)fetchAvailabilityDatawithBrandCode:(NSString*)brandCode;

-(NSMutableArray*)fetchAvailabilityDatawithBrandCode:(NSString*)brandCode andVisit:(SalesWorxVisit*)currentVisit;

-(BOOL)insertMerchandisingSessionData:(SalesWorxVisit*)currentVisit;
-(NSMutableArray*)fetchAppCodeValueforPOS:(NSString*)codeType;
-(NSMutableArray*)fetchMerchandisingProductsForSelectedBrand:(NSString*)selectedBrandCode withClassification:(SalesWorxVisit*)currentVisit;


///*Field Sales Visit Notes*//
-(NSString*)fetchFieldSalesVisitNotesForCustomer:(Customer*)selectedCustomer;
-(BOOL)saveVisitContactDetails:(SalesWorxVisit*)currentVisit;
-(NSMutableDictionary*)fetchFieldSalesVisitContactDetailsForCustomer:(Customer*)selectedCustomer;
-(NSMutableDictionary*)fetchFieldSalesVisitContactDetailsForCustomerinCurrentVisit:(SalesWorxVisit*)selectedVisit;

-(NSMutableArray*)fetchSurveys;
-(NSMutableArray*)fetchMedicinaSurveys:(NSString*)locationID;
-(NSInteger)fetchLimitofField:(NSString*)field in:(NSString*)tblName;


-(NSMutableDictionary*)fetchCoachSurveyStructure:(CoachSurveyType*)selectedSurvey;
-(NSMutableArray*)fetchCoachSurveyConfirmationData:(CoachSurveyType*)selectedSurvey;
-(BOOL)insertCoachDataSurvey:(NSMutableArray*)surveyArray withConfirmation:(NSMutableArray*)confirmationArray andSurveyType:(CoachSurveyType*)selectedSurveyType;

-(BOOL)surveyCompletedForToday;
-(BOOL)isCoachSurveyCompletedForToday:(NSString*)surveyID;


///*Field Marketing Stock Check*//
-(NSMutableArray*)fetchStockCheckItemsForCustomer;
-(NSMutableArray *)fetchStockCheckLocations;
- (BOOL)saveStockCheckWithVisitDetails:(NSMutableDictionary*)visitDetailsDict andDistChectItemInfo:(NSMutableArray*)fetchDistriButionCheckLocations andVisitDetails:(VisitDetails*)currentVisitDetails;

-(NSMutableArray*)fetchInvoicesforReportsPaymentReceipt:(NSString*)collection_ID;
-(NSString*)fetchBackEndURL;

- (BOOL)saveRouteRescheduleDetails:(RouteRescheduleDetails*)routeDetail;

- (NSMutableArray *)fetchProductsWithMedia:(NSString *)productID;
- (NSMutableArray *)fetchStockWithProductID:(NSString*)productID;
- (NSMutableArray *)fetchMediaFilesforProduct:(NSString*)productID;

-(NSString*)fetchEmailAddressViaCustomerID:(NSString*)customerID;
-(NSString*)fetchEmailAddressViaDoctorID:(NSString*)doctorID;
-(NSString*)fetchEmailAddressViaContactID:(NSString*)contactID;
@end
