//
//  SalesWorxBackgroundSyncManager.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBackgroundSyncManager.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"
#import "SWAppDelegate.h"
@implementation SalesWorxBackgroundSyncManager
-(void)postVisitDataToserver
{
    [self sendRequestForExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendTrxAndVisitData_V1" andProcDic:[self getProcedureDictionary]];
    
    
    [self sendRequestForExecMarketingWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendPHXVisitsAndOtherData" andProcDic:[self getMarketingModuleProcedureDictionary]];
}
-(NSMutableArray *)getProcedureParametersArray
{
    NSMutableArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"OrderList", nil];
    return procedureParametersArray;
}
-(NSDictionary *)getMarketingModuleProcedureDictionary
{
    
    if([SWDefaults lastVTRXBackGroundSyncDate]==nil)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
    }
    else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastVTRXBackGroundSyncDate]]] == NSOrderedAscending)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
        
    }
    else
    {
        lastSynctimeStr=[SWDefaults lastVTRXBackGroundSyncDate];
        
    }
    
    
    currentSyncStarttimeStr=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    
    /**Conatct List**/
    NSMutableArray *contactListArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetDoctorList, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *conatctListXmlString=[self prepareXMLStringForContactList:contactListArray];
    NSString  *conatctListXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)conatctListXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**Location List**/
    NSMutableArray *locationListArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetLocationList, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *locationContactMapArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetLocationConatctMapList,lastSynctimeStr,currentSyncStarttimeStr]]];
    NSDictionary *locationListData=[NSDictionary dictionaryWithObjectsAndKeys:locationListArray,@"locationListArray",locationContactMapArray,@"locationConatctMapArray", nil];
    NSString *locationListXmlString=[self prepareXMLStringForLocationList:locationListData];
    NSString  *locationListXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)locationListXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**Doctor List**/
    NSMutableArray *doctorListArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetVisitDoctorList, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *doctorLocationArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetDoctorLocationList,lastSynctimeStr,currentSyncStarttimeStr]]];
    
    NSDictionary *doctorListData=[NSDictionary dictionaryWithObjectsAndKeys:doctorListArray,@"doctorListArray",doctorLocationArray,@"docotorLocationArray", nil];
    NSString *doctorListXmlString=[self prepareXMLStringForDoctorList:doctorListData];
    NSString  *doctorListXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)doctorListXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**Actual List**/
    NSMutableArray *actualVisitsArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetActualVisits, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *actualVisitXmlString=[self prepareXMLStringForActualVisitList:actualVisitsArray];
    NSString  *actualVisitXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)actualVisitXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    
    /***Planned Visits*/
    NSMutableArray *plannedVistsArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetPlannedVisits, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *plannedVisitsXmlString=[self prepareXMLStringForPlannedVisits:plannedVistsArray];
    NSString  *plannedVisitsXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)plannedVisitsXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
   
    /**eDetailing Data**/
    NSMutableArray *eDetailingArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetEDetailing, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *eDetailingDataXmlString=[self prepareXMLStringForeDetailingData:eDetailingArray];
    NSString  *eDetailingDataXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)eDetailingDataXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**EmpNotesData**/
    NSMutableArray *empNotesArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetEmpNotes, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *empNotesDataXmlString=[self prepareXMLStringForeEmpNotes:empNotesArray];
    NSString  *empNotesDataXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)empNotesDataXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**EmpStockData**/
    NSMutableArray *empStockArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetEmpStock
, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *empStockDataXmlString=[self prepareXMLStringForempStockData:empStockArray];
    NSString  *empStockDataXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)empStockDataXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
   
    /**EmpExpenseData**/
    NSString *empExpenseDataXmlString=[self prepareXMLStringForempExpenseData:@""];
    NSString  *empExpenseDataXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)empExpenseDataXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**Issue Note List**/
    NSMutableArray *issueNotesArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetIssueNotes, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *issueNotesItemsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetIssueNotesLineItems, lastSynctimeStr,currentSyncStarttimeStr]]];
    
    
    NSDictionary *issueNotesData=[NSDictionary dictionaryWithObjectsAndKeys:issueNotesArray,@"issueNotesArray",issueNotesItemsarray,@"itemArray", nil];
    NSString *issueNoteListXmlString=[self prepareXMLStringForIssueNoteList:issueNotesData];
    NSString  *issueNoteListXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)issueNoteListXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**DC List**/
    NSString *DCListXmlString=[self prepareXMLStringForDCList:@""];
    NSString  *DCListXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)DCListXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**VisitAdditionalData**/
    NSMutableArray *visitAdditionalInfoArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetVisitAddlInfo, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *visitAdditionalDataXmlString=[self prepareXMLStringForVisitAdditionalInfo:visitAdditionalInfoArray];
    NSString  *visitAdditionalDataXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)visitAdditionalDataXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /**TaskList**/
    NSMutableArray *taskListArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetTaskList, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *taskListXmlString=[self prepareXMLStringForTaskList:taskListArray];
    NSString  *taskListXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)taskListXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    

    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"ContactList",@"LocationList",@"DoctorList",@"ActualVisitList",@"PlannedVisitList",@"EDetailingData",@"EmpNotesData",@"EmpStockData",@"EmpExpenseData",@"IssueNoteList",@"DCList",@"VisitAdditionalData",@"TaskList",nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:conatctListXMLPassingString,locationListXMLPassingString,doctorListXMLPassingString,actualVisitXMLPassingString,plannedVisitsXMLPassingString,eDetailingDataXMLPassingString,empNotesDataXMLPassingString,empStockDataXMLPassingString,empExpenseDataXMLPassingString,issueNoteListXMLPassingString,DCListXMLPassingString,visitAdditionalDataXMLPassingString,taskListXMLPassingString,nil];
    
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
    
    
}
-(NSDictionary *)getProcedureDictionary
{
    
    if([SWDefaults lastVTRXBackGroundSyncDate]==nil)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
    }
    else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastVTRXBackGroundSyncDate]]] == NSOrderedAscending)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
 
    }
    else
    {
        lastSynctimeStr=[SWDefaults lastVTRXBackGroundSyncDate];

    }
    
    
    currentSyncStarttimeStr=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];

    /*** Visits*/
    NSMutableArray *vistsArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetVisits, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *visitsXmlString=[self prepareXMLStringForVisits:vistsArray];
    NSString  *visitsXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)visitsXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    /*** Orders*/
    NSMutableArray *ordersArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrders, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *orderLineItemsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderLineItems, lastSynctimeStr,currentSyncStarttimeStr]]];

    NSMutableArray *orderLineItemLotsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderLots,lastSynctimeStr,currentSyncStarttimeStr]]];

    
    NSDictionary *ordersData=[NSDictionary dictionaryWithObjectsAndKeys:ordersArray,@"orderArray",orderLineItemsarray,@"itemArray",orderLineItemLotsarray,@"lotArray", nil];
    
    NSString *ordersXmlString=[self prepareXMLStringForOrders:ordersData];
    NSString  *ordersXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)ordersXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    
    
    /**** Returns **/

    
    
    NSMutableArray *returnsArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetReturns, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *oreturnsLineItemsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetReturnLiteItems, lastSynctimeStr,currentSyncStarttimeStr]]];
    
    NSMutableArray *returnsLineItemLotsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetReturnLots,lastSynctimeStr,currentSyncStarttimeStr]]];
    
    
    NSDictionary *returnsData=[NSDictionary dictionaryWithObjectsAndKeys:returnsArray,@"returnArray",oreturnsLineItemsarray,@"itemArray",returnsLineItemLotsarray,@"lotArray", nil];
    
    NSString *returnXmlString=[self prepareXMLStringForReturns:returnsData];
    NSString  *returnXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)returnXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    /** Order History*/
    NSMutableArray *orderHistoryArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderHistory, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *orderHistoryItemsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderHistoryLineItems, lastSynctimeStr,currentSyncStarttimeStr]]];
    

    NSDictionary *orderHistorysData=[NSDictionary dictionaryWithObjectsAndKeys:orderHistoryArray,@"orderHistoryArray",orderHistoryItemsarray,@"itemArray", nil];
    NSString *orderHistoryXmlString=[self prepareXMLStringForOrderHistory:orderHistorysData];
    NSString  *orderHistoryXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)orderHistoryXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    
    /** Order History*/
    NSMutableArray *CollectionsArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetCollections, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *CollectionItemsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetCollection_Invoices, lastSynctimeStr,currentSyncStarttimeStr]]];

    /** Collections Info*/
    NSDictionary *collectionData=[NSDictionary dictionaryWithObjectsAndKeys:CollectionsArray,@"collectionArray",CollectionItemsarray,@"itemArray", nil];
    NSString *collectionXmlString=[self prepareXMLStringCollections:collectionData];
    NSString  *collectionXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)collectionXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    
    

    
    /*** Visits*/
    NSArray *CollevctionsdocInfoArray=[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetCollectionsDOCINFO, lastSynctimeStr,currentSyncStarttimeStr]];
    NSArray *SalesordwrdocInfoArray=[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrdersDOCINFO, lastSynctimeStr,currentSyncStarttimeStr]];
    NSMutableArray *docInfoArray=[[NSMutableArray alloc]initWithArray:CollevctionsdocInfoArray];
    [docInfoArray addObjectsFromArray:SalesordwrdocInfoArray];
    
    
    NSString *docInfoXmlString=[self prepareXMLStringForDocAdditionalInfo:docInfoArray];
    NSString  *docInfoXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)docInfoXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    /***Distribution Check*/
    
    NSMutableArray *distributionCheckArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetDistributionCheck, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSLog(@"%@",distributionCheckArray);
    NSMutableArray *distributionCheckItems=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetDistributionCheckItems, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSLog(@"%@",distributionCheckItems);
    // NSDictionary *ordersData=[NSDictionary dictionaryWithObjectsAndKeys:ordersArray,@"orderArray",orderLineItemsarray,@"itemArray",orderLineItemLotsarray,@"lotArray", nil];
    
    NSDictionary *distributionCheckData=[NSDictionary dictionaryWithObjectsAndKeys:distributionCheckArray,@"DistributionCheckData",distributionCheckItems,@"DistributionCheckItem",nil];
    NSString *distributionXmlString=[self prepareXMLStringForDistribution:distributionCheckData];
    NSString  *visitsXMLPassingString1= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)distributionXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /***Distribution Check*/
    
    NSString *visitAdditionalXmlString=[self prepareXMLStringForVisitAdditionInfo:@""];
    NSString  *visitAdditionalInfoXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)visitAdditionalXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"OrderList",@"RMAList",@"OrderHistoryData",@"CollectionInfo",@"DocAdditionalData",@"VisitsData",@"VisitAdditionalData",@"DistributionCheckData", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:ordersXMLPassingString,returnXMLPassingString,orderHistoryXMLPassingString,collectionXMLPassingString,docInfoXMLPassingString,visitsXMLPassingString,visitAdditionalInfoXMLPassingString,visitsXMLPassingString1,nil];
    
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];

}
- (void)sendRequestForExecMarketingWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    NSLog(@"Sync request for marketing module %@",[NSDate date]);
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"ContactList",@"LocationList",@"DoctorList",@"ActualVisitList",@"PlannedVisitList",@"EDetailingData",@"EmpNotesData",@"EmpStockData",@"EmpExpenseData",@"IssueNoteList",@"DCList",@"VisitAdditionalData",@"TaskList", nil];
    for (NSInteger i=0; i<procedureParametersArray.count; i++) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];
        
    }
    
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRep_ID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    
    
    
    // NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Send_PHXVisitsAndOtherData",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    SWAppDelegate *appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"Sync response for marketing module %@",[NSDate date]);
         NSLog(@"background sync Response Marketing %@",resultArray);
         appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None
         ;
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             [[SWDatabaseManager retrieveManager]deleteOrdersAndReturnsAfterBackGroundSync:currentSyncStarttimeStr AndLastSyncTime:lastSynctimeStr];
             [SWDefaults setLastVTRXBackGroundSyncDate:currentSyncStarttimeStr];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@" background sync error Marketing %@",error);
         appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None
         ;
     }];
    
    //call start on your request operation
    [operation start];
}

- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    NSLog(@"Request sent at %@",[NSDate date]);
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"OrderList",@"RMAList",@"OrderHistoryData",@"CollectionInfo",@"DocAdditionalData",@"VisitsData",@"VisitAdditionalData",@"DistributionCheckData", nil];
    for (NSInteger i=0; i<procedureParametersArray.count; i++) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];

    }
    
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRep_ID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    
    
    
   // NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Send_TrxAndVisitData_V1",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    SWAppDelegate *appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"Sync response time %@",[NSDate date]);
         NSLog(@"background sync Response %@",resultArray);
         appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None
         ;
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             [[SWDatabaseManager retrieveManager]deleteOrdersAndReturnsAfterBackGroundSync:currentSyncStarttimeStr AndLastSyncTime:lastSynctimeStr];
             [SWDefaults setLastVTRXBackGroundSyncDate:currentSyncStarttimeStr];
        }
     }
      failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@" background sync error %@",error);
         appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None
         ;
     }];
    
    //call start on your request operation
    [operation start];
}

#pragma mark VisitAdditionalInfo methods
-(NSString *)prepareXMLStringForVisitAdditionInfo:(NSDictionary *)visitAdditionalInfo{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"VisitAdditionalInfo"];
    [xmlWriter writeEndElement];
    
     return   [xmlWriter toString];
}
#pragma mark DistributionCheck methods
-(NSString *)prepareXMLStringForDistribution:(NSDictionary *)distributionData{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *distributionArray=[distributionData valueForKey:@"DistributionCheckData"];
    NSMutableArray *distributionCheckItemArray=[distributionData valueForKey:@"DistributionCheckItem"];
    [xmlWriter writeStartElement:@"DistributionChecks"];
    if(!([distributionArray count]==0))
    {
        for (int o=0; o<[distributionArray count]; o++)
        {
            NSMutableDictionary * distributionCheckDictionary = [NSMutableDictionary dictionaryWithDictionary:[distributionArray objectAtIndex:o]];
            NSString *orderRef = [distributionCheckDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            [xmlWriter writeStartElement:@"DistributionCheck"];
            if (![[distributionCheckDictionary stringForKey:@"DistributionCheck_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"DistributionCheck_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"DistributionCheck_ID"]];
                [xmlWriter writeEndElement];
                if (![[distributionCheckDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Customer_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C2"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Customer_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Site_Use_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C3"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Site_Use_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"SalesRep_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C4"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"SalesRep_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Emp_Code"].length !=0){
                    [xmlWriter writeStartElement:@"C5"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Emp_Code"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Checked_On"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Checked_On"].length !=0){
                    [xmlWriter writeStartElement:@"C6"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Checked_On"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Visit_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C7"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Visit_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Status"].length !=0){
                    [xmlWriter writeStartElement:@"C8"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Status"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                    [xmlWriter writeStartElement:@"C9"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Custom_Attribute_1"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                    [xmlWriter writeStartElement:@"C10"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Custom_Attribute_2"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                    [xmlWriter writeStartElement:@"C11"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Custom_Attribute_3"]];
                    [xmlWriter writeEndElement];
                }
                
                if([distributionCheckItemArray count]!=0)
                {
                    [xmlWriter writeStartElement:@"LineItems"];
                    for (int i=0; i<[distributionCheckItemArray count]; i++)
                    {
                        
                        NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[distributionCheckItemArray objectAtIndex:i]];
                        NSString *itemLine = [itemDictionary stringForKey:@"Line_Number"];
                        if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                        {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"DistributionCheckLine_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"DistributionCheckLine_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"DistributionCheckLine_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"DistributionCheck_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"DistributionCheck_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"DistributionCheck_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Is_Available"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Is_Available"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Is_Available"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Qty"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Qty"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Qty"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Expiry_Dt"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Expiry_Dt"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Expiry_Dt"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Display_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Display_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Display_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Reorder"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Reorder"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Reorder"]];
                            [xmlWriter writeEndElement];
                        }
                    }
                        [xmlWriter writeEndElement];
                    }
                    
                    [xmlWriter writeEndElement];
                }
            }
                [xmlWriter writeEndElement];
        }
    }
    [xmlWriter writeEndElement];
    
    return   [xmlWriter toString];
}


#pragma mark SalesOrder methods
-(NSString *)prepareXMLStringForOrders:(NSDictionary *)ordersData{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *orderArray=[ordersData valueForKey:@"orderArray"];
    NSMutableArray *lotArray=[ordersData valueForKey:@"lotArray"];
    NSMutableArray *itemArray=[ordersData valueForKey:@"itemArray"];
    [xmlWriter writeStartElement:@"Orders"];

    if(!([orderArray count]==0))
    {
        
        for (int o=0; o<[orderArray count]; o++)
        {
            
            NSMutableDictionary * orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderArray objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            
            [xmlWriter writeStartElement:@"Order"];
            
            if (![[orderDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Creation_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Creation_Date"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Creation_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"System_Order_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"System_Order_Date"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"System_Order_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Shipping_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Shipping_Instructions"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Shipping_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Packing_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 ){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Packing_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Customer_PO_Number"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Customer_PO_Number"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Customer_PO_Number"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Request_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Request_Date"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Request_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Schedule_Ship_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Schedule_Ship_Date"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Schedule_Ship_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Status"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Signee_Name"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Signee_Name"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Signee_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Update_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Update_Date"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Update_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Discount_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Discount_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Discount_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Print_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Print_Status"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Print_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C33"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C34"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    NSString *itemLine = [itemDictionary stringForKey:@"Line_Number"];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Order_Quantity_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Order_Quantity_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Order_Quantity_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Display_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Display_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Display_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Ordered_Quantity"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Ordered_Quantity"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Ordered_Quantity"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Calc_Price_Flag"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Calc_Price_Flag"].length !=0){
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Calc_Price_Flag"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Unit_Selling_Price"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Unit_Selling_Price"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Unit_Selling_Price"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Def_Bonus"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Def_Bonus"].length !=0){
                            //NSLog(@"Def_Bonus");
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Def_Bonus"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Assortment_Plan_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Assortment_Plan_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C14"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Assortment_Plan_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Amount"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Amount"].length !=0){
                            [xmlWriter writeStartElement:@"C15"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Amount"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Percentage"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Percentage"].length !=0){
                            [xmlWriter writeStartElement:@"C16"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Percentage"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Level"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Level"].length !=0){
                            [xmlWriter writeStartElement:@"C17"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Level"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Code"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Code"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C18"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Code"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                            [xmlWriter writeStartElement:@"C19"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_4"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if([lotArray count]!=0)
                        {
                            [xmlWriter writeStartElement:@"Lots"];
                            for (int l=0 ; l<[lotArray count] ; l++)
                            {
                                
                                NSMutableDictionary * lotDictionary = [NSMutableDictionary dictionaryWithDictionary:[lotArray objectAtIndex:l]];
                                if([orderRef isEqualToString:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]] && [itemLine isEqualToString:[lotDictionary stringForKey:@"Line_Number"]]){
                                    [xmlWriter writeStartElement:@"Lot"];
                                    
                                    [xmlWriter writeStartElement:@"C1"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Row_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C2"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C3"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Line_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C4"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Lot_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C5"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Ordered_Quantity"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C6"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Is_FIFO"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C7"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Org_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeEndElement];//lot
                                }
                            }
                            [xmlWriter writeEndElement];//lots
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            
            [xmlWriter writeEndElement];//Orders
            
        }
    }
    [xmlWriter writeEndElement];//Order

    return   [xmlWriter toString];
    
}

#pragma mark Returns methods
-(NSString *)prepareXMLStringForReturns:(NSDictionary *)returnsData{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *orderArray=[returnsData valueForKey:@"returnArray"];
    NSMutableArray *lotArray=[returnsData valueForKey:@"lotArray"];
    NSMutableArray *itemArray=[returnsData valueForKey:@"itemArray"];
    [xmlWriter writeStartElement:@"Returns"];

    if(!([orderArray count]==0))
    {
        
        for (int o=0; o<[orderArray count]; o++)
        {
            
            NSMutableDictionary * orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderArray objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            
            [xmlWriter writeStartElement:@"RMA"];
            
            if (![[orderDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Creation_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Creation_Date"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Creation_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Internal_Notes"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Internal_Notes"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Internal_Notes"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Customer_Ref_No"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Customer_Ref_No"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Customer_Ref_No"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Order_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Status"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Customer_Signature"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Customer_Signature"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Customer_Signature"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Update_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Update_Date"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Update_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Signee_Name"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Signee_Name"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Signee_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Credit_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            
            
            if (![[orderDictionary stringForKey:@"Invoice_Ref_No"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Invoice_Ref_No"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Invoice_Ref_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Reason_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Reason_Code"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Reason_Code"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Print_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Print_Status"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Print_Status"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    NSString *itemLine = [itemDictionary stringForKey:@"Line_Number"];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Return_Quantity_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Return_Quantity_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Return_Quantity_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Display_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Display_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Display_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Returned_Quantity"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Returned_Quantity"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Returned_Quantity"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Unit_Selling_Price"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Unit_Selling_Price"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Unit_Selling_Price"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                  
                        if (![[itemDictionary stringForKey:@"VAT_Amount"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Amount"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Amount"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Percentage"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Percentage"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Percentage"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Level"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Level"].length !=0){
                            [xmlWriter writeStartElement:@"C14"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Level"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Code"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Code"].length !=0){
                            [xmlWriter writeStartElement:@"C15"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Code"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                            [xmlWriter writeStartElement:@"C16"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_4"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                            [xmlWriter writeStartElement:@"C17"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_5"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                            [xmlWriter writeStartElement:@"C18"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_6"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_7"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_7"].length !=0){
                            [xmlWriter writeStartElement:@"C19"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_7"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_8"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_8"].length !=0){
                            [xmlWriter writeStartElement:@"C20"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_8"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_9"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_9"].length !=0){
                            [xmlWriter writeStartElement:@"C21"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_9"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if([lotArray count]!=0)
                        {
                            [xmlWriter writeStartElement:@"Lots"];
                            for (int l=0 ; l<[lotArray count] ; l++)
                            {
                                
                                NSMutableDictionary * lotDictionary = [NSMutableDictionary dictionaryWithDictionary:[lotArray objectAtIndex:l]];
                                if([orderRef isEqualToString:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]] && [itemLine isEqualToString:[lotDictionary stringForKey:@"Line_Number"]]){
                                    [xmlWriter writeStartElement:@"Lot"];
                                    
                                    [xmlWriter writeStartElement:@"C1"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Row_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C2"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C3"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Line_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C4"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Lot_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C5"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Inventory_Item_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C6"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Returned_Quantity"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C7"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Expiry_Dt"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C8"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Lot_Type"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeEndElement];//lot
                                }
                            }
                            [xmlWriter writeEndElement];//lots
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            
            [xmlWriter writeEndElement];//RMA
            
        }
    }
    [xmlWriter writeEndElement];//Returns

    return   [xmlWriter toString];
    
}
#pragma mark Visits methods
-(NSString *)prepareXMLStringForVisits:(NSMutableArray *)VisitsArray{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"ActualVisits"];

    if(!([VisitsArray count]==0))
    {
        
        for (NSInteger v=0; v<[VisitsArray count]; v++)
        {
            
            NSMutableDictionary * visitDictionary = [NSMutableDictionary dictionaryWithDictionary:[VisitsArray objectAtIndex:v]];

            [xmlWriter writeStartElement:@"Visit"];
            
            if (![[visitDictionary stringForKey:@"Actual_Visit_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Actual_Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Actual_Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"FSR_Plan_Detail_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"FSR_Plan_Detail_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"FSR_Plan_Detail_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Site_Use_ID"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Site_Use_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Customer_Barcode"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Customer_Barcode"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Customer_Barcode"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Visit_Start_Date"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Visit_Start_Date"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Visit_Start_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Visit_End_Date"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Visit_End_Date"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Visit_End_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Text_Notes"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Text_Notes"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Text_Notes"]];
                [xmlWriter writeEndElement];
            }
//            if (![[visitDictionary stringForKey:@"Voice_Notes"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Voice_Notes"].length !=0){
//                [xmlWriter writeStartElement:@"C9"];
//                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Voice_Notes"]];
//                [xmlWriter writeEndElement];
//            }
            if (![[visitDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"SalesRep_ID"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"SalesRep_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Emp_Code"].length !=0  ){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];}
            if (![[visitDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Sync_Timestamp"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Sync_Timestamp"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Sync_Timestamp"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Scanned_Closing"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Scanned_Closing"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Scanned_Closing"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Odo_Reading"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Odo_Reading"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Odo_Reading"]];
                [xmlWriter writeEndElement];
            }

            if (![[visitDictionary stringForKey:@"Cash_Customer_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Cash_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Cash_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Cash_Site_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Cash_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Cash_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"CC_Name"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"CC_Name"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"CC_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"CC_TelNo"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"CC_TelNo"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"CC_TelNo"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Latitude"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Latitude"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Latitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Longitude"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Longitude"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Longitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//Visit
            
        }
    }
    [xmlWriter writeEndElement];//Visits

    return   [xmlWriter toString];
    
}

#pragma mark OrderHistory Methods
-(NSString *)prepareXMLStringForOrderHistory:(NSDictionary *)OrderHistoryData{
    //NSDictionary *orderHistorysData=[NSDictionary dictionaryWithObjectsAndKeys:orderHistoryArray,@"orderHistoryArray",orderHistoryItemsarray,@"itemArray", nil];

    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *orderHistoryArray=[OrderHistoryData valueForKey:@"orderHistoryArray"];
    NSMutableArray *itemArray=[OrderHistoryData valueForKey:@"itemArray"];
    [xmlWriter writeStartElement:@"SalesHistory"];

    if(!([orderHistoryArray count]==0))
    {
        
        for (int o=0; o<[orderHistoryArray count]; o++)
        {
            
            NSMutableDictionary * orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderHistoryArray objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            
            [xmlWriter writeStartElement:@"Trx"];
            
            if (![[orderDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Doc_Type"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Doc_Type"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Doc_Type"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Creation_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Creation_Date"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Creation_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"System_Order_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"System_Order_Date"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"System_Order_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Request_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Request_Date"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Request_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Schedule_Ship_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Schedule_Ship_Date"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Schedule_Ship_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Customer_ID"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Site_ID"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Inv_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"BO_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"BO_Status"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"BO_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Shipping_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Shipping_Instructions"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Shipping_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Packing_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Packing_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"ERP_Ref_No"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"ERP_Ref_No"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"ERP_Ref_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"ERP_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"ERP_Status"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"ERP_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Transaction_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Transaction_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Transaction_Amt"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Due_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Due_Date"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Due_Date"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Credit_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Sync_Timestamp"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Sync_Timestamp"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Sync_Timestamp"]];
                [xmlWriter writeEndElement];
            }

            if (![[orderDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Lot_No"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Lot_No"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Lot_No"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Expiry_Date"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Expiry_Date"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Expiry_Date"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Order_Quantity_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Order_Quantity_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Order_Quantity_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Ordered_Quantity"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Ordered_Quantity"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Ordered_Quantity"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Calc_Price_Flag"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Calc_Price_Flag"].length !=0){
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Calc_Price_Flag"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Unit_Selling_Price"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Unit_Selling_Price"].length !=0){
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Unit_Selling_Price"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Item_Value"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Item_Value"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Item_Value"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C14"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if (![[itemDictionary stringForKey:@"Assortment_Plan_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Assortment_Plan_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C15"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Assortment_Plan_ID"]];
                            [xmlWriter writeEndElement];
                        }
                       
                        if (![[itemDictionary stringForKey:@"VAT_Amount"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Amount"].length !=0){
                            [xmlWriter writeStartElement:@"C16"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Amount"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Percentage"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Percentage"].length !=0){
                            [xmlWriter writeStartElement:@"C17"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Percentage"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Level"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Level"].length !=0){
                            [xmlWriter writeStartElement:@"C18"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Level"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Code"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Code"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C19"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Code"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                            [xmlWriter writeStartElement:@"C20"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_4"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                            [xmlWriter writeStartElement:@"C21"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_5"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                            [xmlWriter writeStartElement:@"C22"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_6"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_7"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_7"].length !=0){
                            [xmlWriter writeStartElement:@"C23"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_7"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_8"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_8"].length !=0){
                            [xmlWriter writeStartElement:@"C24"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_8"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_9"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_9"].length !=0){
                            [xmlWriter writeStartElement:@"C25"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_9"]];
                            [xmlWriter writeEndElement];
                        }
                       
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            
            [xmlWriter writeEndElement];//TRX
            
        }
    }
    [xmlWriter writeEndElement];//SalesHistory

    return   [xmlWriter toString];
    
}
#pragma mark DOC Info
-(NSString *)prepareXMLStringForDocAdditionalInfo:(NSMutableArray *)DocsArray{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"DocAdditionalInfo"];

    if(!([DocsArray count]==0))
    {
        
        for (NSInteger v=0; v<[DocsArray count]; v++)
        {
            
            NSMutableDictionary * visitDictionary = [NSMutableDictionary dictionaryWithDictionary:[DocsArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"Info"];
            
            if (![[visitDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Doc_No"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Doc_No"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Doc_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Doc_Type"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Doc_Type"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Doc_Type"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Attrib_Name"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Attrib_Name"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Attrib_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Attrib_Value"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Attrib_Value"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Attrib_Value"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_5"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Sync_Timestamp"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Sync_Timestamp"].length !=0  ){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Sync_Timestamp"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//Visit
            
        }
    }
    [xmlWriter writeEndElement];//Visits

    return   [xmlWriter toString];
    
}
#pragma mark Collections Methods
-(NSString *)prepareXMLStringCollections:(NSDictionary *)CollectionsData{
    //NSDictionary *orderHistorysData=[NSDictionary dictionaryWithObjectsAndKeys:orderHistoryArray,@"orderHistoryArray",orderHistoryItemsarray,@"itemArray", nil];
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *orderHistoryArray=[CollectionsData valueForKey:@"collectionArray"];
    NSMutableArray *itemArray=[CollectionsData valueForKey:@"itemArray"];
    [xmlWriter writeStartElement:@"Collections"];

    if(!([orderHistoryArray count]==0))
    {
        
        for (int o=0; o<[orderHistoryArray count]; o++)
        {
            
            NSMutableDictionary * orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderHistoryArray objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Collection_ID"];
            
            [xmlWriter writeStartElement:@"Collection"];
            
            if (![[orderDictionary stringForKey:@"Collection_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Collection_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Collection_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Collection_Ref_No"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Collection_Ref_No"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Collection_Ref_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Collected_On"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Collected_On"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Collected_On"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Collected_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Collected_By"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Collected_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Collection_Type"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Collection_Type"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Collection_Type"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Site_Use_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Site_Use_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Amount"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Amount"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Amount"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Cheque_No"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Cheque_No"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Cheque_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Cheque_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Cheque_Date"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Cheque_Date"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Bank_Name"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Bank_Name"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Bank_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Bank_Branch"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Bank_Branch"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Bank_Branch"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Status"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Print_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Print_Status"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Print_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Sync_Timestamp"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Sync_Timestamp"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Sync_Timestamp"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Remarks"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Remarks"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Remarks"]];
                [xmlWriter writeEndElement];
            }
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"Invoices"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Collection_ID"]])
                    {
                        [xmlWriter writeStartElement:@"Invoice"];
                        if (![[itemDictionary stringForKey:@"Collection_Inv_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Collection_Inv_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Collection_Inv_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Collection_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Collection_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Collection_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Invoice_No"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Invoice_No"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Invoice_No"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Invoice_Date"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Invoice_Date"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Invoice_Date"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Amount"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Amount"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Amount"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"ERP_Status"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"ERP_Status"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"ERP_Status"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Sync_Timestamp"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Sync_Timestamp"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Sync_Timestamp"]];
                            [xmlWriter writeEndElement];
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            
            [xmlWriter writeEndElement];//Orders
            
        }
    }
    [xmlWriter writeEndElement];//Order

    return [xmlWriter toString];
    
}
#pragma Marketing Module
#pragma mark Conatct List methods
-(NSString *)prepareXMLStringForContactList:(NSMutableArray *)ContactListArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"Contacts"];
    if(!([ContactListArray count]==0))
    {
        for (NSInteger v=0; v<[ContactListArray count]; v++)
        {
            
            NSMutableDictionary * conatctListDictionary = [NSMutableDictionary dictionaryWithDictionary:[ContactListArray objectAtIndex:v]];
            [xmlWriter writeStartElement:@"Contact"];
            
            if (![[conatctListDictionary stringForKey:@"Contact_ID"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Contact_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Contact_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Contact_Name"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Contact_Name"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Contact_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Conatct_Code"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Conatct_Code"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Conatct_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Title"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Title"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Title"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Name_2"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Name_2"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Name_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Name_3"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Name_3"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Name_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Address_1"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Address_1"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Address_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Address_2"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Address_2"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Address_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Address_3"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Address_3"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Address_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"City"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"City"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"City"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"State"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"State"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"State"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Email"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Email"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Email"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Phone"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Phone"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Phone"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Postal_Code"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Postal_Code"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Postal_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Classification_1"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Classification_1"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Classification_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Classification_2"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Classification_2"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Classification_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Classification_3"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Classification_3"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Classification_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Classification_4"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Classification_4"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Classification_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Classification_5"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Classification_5"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Classification_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Custom_Attribute_7"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Custom_Attribute_7"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Custom_Attribute_7"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Is_Active"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Is_Active"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Is_Active"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Is_Deleted"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Is_Deleted"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Is_Deleted"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[conatctListDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [conatctListDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[conatctListDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
        }
        [xmlWriter writeEndElement];//contact
    }
    [xmlWriter writeEndElement];//Contacts


return   [xmlWriter toString];
}
#pragma mark Location List methods
-(NSString *)prepareXMLStringForLocationList:(NSDictionary *)LocationListArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"Locations"];
    NSMutableArray *issueNotesArray=[LocationListArray valueForKey:@"locationListArray"];
    NSMutableArray *itemArray=[LocationListArray valueForKey:@"locationConatctMapArray"];
    if(!([issueNotesArray count]==0))
    {
        
        for (int o=0; o<[issueNotesArray count]; o++)
        {
            
            NSMutableDictionary * issueNoteDictionary = [NSMutableDictionary dictionaryWithDictionary:[issueNotesArray objectAtIndex:o]];
            NSString *orderRef = [issueNoteDictionary stringForKey:@"Doc_No"];
            
            [xmlWriter writeStartElement:@"Location"];
            
            if (![[issueNoteDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Location_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Location_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Location_Name"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Location_Name"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Location_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Location_Type"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Location_Type"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Location_Type"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Location_Code"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Location_Code"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Location_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Name_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Name_2"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Name_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Name_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Name_3"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Name_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Address_1"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Address_1"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Address_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Address_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Address_2"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Address_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Address_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Address_3"].length !=0 ){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Address_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"City"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"City"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"City"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"State"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"State"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"State"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Email"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Email"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Email"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Phone"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Phone"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Phone"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Postal_Code"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Postal_Code"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Postal_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Latitude"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Latitude"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Latitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Longitude"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Longitude"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Longitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Classification_1"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_1"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Classification_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_2"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Classification_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_3"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_3"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[issueNoteDictionary stringForKey:@"Classification_4"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_4"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_4"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[issueNoteDictionary stringForKey:@"Classification_5"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_5"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_7"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_7"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_7"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Is_Active"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Is_Active"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Is_Active"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Is_Deleted"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Is_Deleted"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Is_Deleted"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C33"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C34"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Is_Approved"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Is_Approved"].length !=0){
                [xmlWriter writeStartElement:@"C35"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Is_Approved"]];
                [xmlWriter writeEndElement];
            }
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LocationContactMaps"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Doc_No"]])
                    {
                        [xmlWriter writeStartElement:@"LocationContactMap"];
                        if (![[itemDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Location_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Location_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Conatct_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Conatct_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Conatct_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Created_At"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Created_At"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Created_By"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Created_By"]];
                            [xmlWriter writeEndElement];
                        }
                        [xmlWriter writeEndElement];//LocationContactMap
                    }
                }
                [xmlWriter writeEndElement];//LocationContactMaps
            }
            
            [xmlWriter writeEndElement];//Location
        }
    }
    [xmlWriter writeEndElement];//Locations

    
    return   [xmlWriter toString];
}
#pragma mark Doctor List methods
-(NSString *)prepareXMLStringForDoctorList:(NSDictionary *)DoctorsListArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"Doctors"];
    NSMutableArray *issueNotesArray=[DoctorsListArray valueForKey:@"doctorListArray"];
    NSMutableArray *itemArray=[DoctorsListArray valueForKey:@"docotorLocationArray"];
    if(!([issueNotesArray count]==0))
    {
        
        for (int o=0; o<[issueNotesArray count]; o++)
        {
            
            NSMutableDictionary * issueNoteDictionary = [NSMutableDictionary dictionaryWithDictionary:[issueNotesArray objectAtIndex:o]];
            NSString *orderRef = [issueNoteDictionary stringForKey:@"Doc_No"];
            
            [xmlWriter writeStartElement:@"Doctor"];
            
            if (![[issueNoteDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Doctor_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Doctor_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Doctor_Name"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Doctor_Name"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Doctor_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Doctor_Code"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Doctor_Code"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Doctor_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Title"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Title"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Title"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Name_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Name_2"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Name_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Name_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Name_3"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Name_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Address_1"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Address_1"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Address_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Address_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Address_2"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Address_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Address_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Address_3"].length !=0 ){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Address_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"City"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"City"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"City"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"State"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"State"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"State"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Email"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Email"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Email"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Phone"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Phone"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Phone"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Postal_Code"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Postal_Code"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Postal_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Rating_Info"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Rating_Info"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Rating_Info"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Classification_1"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_1"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Classification_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_2"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Classification_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_3"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Classification_4"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_4"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_4"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[issueNoteDictionary stringForKey:@"Classification_5"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Classification_5"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Classification_5"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_7"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_7"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_7"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Is_Active"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Is_Active"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Is_Active"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Is_Deleted"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Is_Deleted"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Is_Deleted"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C33"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Is_Approved"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Is_Approved"].length !=0){
                [xmlWriter writeStartElement:@"C34"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Is_Approved"]];
                [xmlWriter writeEndElement];
            }
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"DoctorLocations"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Doc_No"]])
                    {
                        [xmlWriter writeStartElement:@"DoctorLocation"];
                        if (![[itemDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Doctor_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Doctor_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Location_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Location_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Created_At"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Created_At"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Created_By"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Created_By"]];
                            [xmlWriter writeEndElement];
                        }
                        [xmlWriter writeEndElement];//DoctorLocation
                    }
                }
                [xmlWriter writeEndElement];//DoctorLocations
            }
            [xmlWriter writeStartElement:@"DoctorPharmacyLocations"];
            [xmlWriter writeStartElement:@"DoctorPharmacyLocation"];

            [xmlWriter writeEndElement];//DoctorPharmacyLocation
            [xmlWriter writeEndElement];//DoctorPharmacyLocations

            [xmlWriter writeEndElement];//Doctor
        }
    }
    [xmlWriter writeEndElement];//Doctors
    return   [xmlWriter toString];
}
#pragma mark Actual Visit methods
-(NSString *)prepareXMLStringForActualVisitList:(NSMutableArray *)ActualVisitArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"ActualVisits"];
    if(!([ActualVisitArray count]==0))
    {
        for (NSInteger v=0; v<[ActualVisitArray count]; v++)
        {
            
            NSMutableDictionary * actualVisitDictionary = [NSMutableDictionary dictionaryWithDictionary:[ActualVisitArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"Visit"];
            
            if (![[actualVisitDictionary stringForKey:@"Actual_Visit_ID"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Actual_Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Actual_Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Emp_ID"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Emp_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Emp_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Location_ID"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Location_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Doctor_ID"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Doctor_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Contact_ID"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Contact_ID"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Contact_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Planned_Visit_ID"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Planned_Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Planned_Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Visit_Start_Date"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Visit_Start_Date"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Visit_Start_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Visit_End_Date"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Visit_End_Date"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Visit_End_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Latitude"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Latitude"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Latitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Longitude"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Longitude"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Longitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Notes"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Notes"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Notes"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Accompanied_By"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Accompanied_By"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Accompanied_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Call_Started_At"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Call_Started_At"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Call_Started_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[actualVisitDictionary stringForKey:@"Call_Ended_At"] isEqualToString:@"<null>"] && [actualVisitDictionary stringForKey:@"Call_Ended_At"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[actualVisitDictionary stringForKey:@"Call_Ended_At"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//visit
        }
    }
    [xmlWriter writeEndElement];//ActualVisits
    
    return   [xmlWriter toString];
}

#pragma mark Planned Visit methods
-(NSString *)prepareXMLStringForPlannedVisits:(NSMutableArray *)PlannedVisitsArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"PlannedVisits"];
    if(!([PlannedVisitsArray count]==0))
    {
        for (NSInteger v=0; v<[PlannedVisitsArray count]; v++)
        {
            
            NSMutableDictionary * plannedVisitDictionary = [NSMutableDictionary dictionaryWithDictionary:[PlannedVisitsArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"PlannedVisit"];
            
            if (![[plannedVisitDictionary stringForKey:@"Planned_Visit_ID"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Planned_Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Planned_Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Visit_Date"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Visit_Date"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Visit_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Emp_ID"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Emp_ID"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Emp_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Location_ID"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Location_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Doctor_ID"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Doctor_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Contact_ID"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Contact_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C6"];
                    [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Contact_ID"]];
                    [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Visit_Status"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Visit_Status"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Visit_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Comments"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Comments"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Comments"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Old_Visit_ID"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Old_Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Old_Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Demo_Plan_ID"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Demo_Plan_ID"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Demo_Plan_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Visit_Type"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Visit_Type"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Visit_Type"]];
                [xmlWriter writeEndElement];
            }
            if (![[plannedVisitDictionary stringForKey:@"Accompanied_By"] isEqualToString:@"<null>"] && [plannedVisitDictionary stringForKey:@"Accompanied_By"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[plannedVisitDictionary stringForKey:@"Accompanied_By"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement]; //PlannedVisit
        }
        
    }
    [xmlWriter writeEndElement];//PlannedVisits
    
    return   [xmlWriter toString];
}

#pragma mark eDetailing List methods
-(NSString *)prepareXMLStringForeDetailingData:(NSMutableArray *)eDetailingDataArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"EDetailing"];
    if(!([eDetailingDataArray count]==0))
    {
        for (NSInteger v=0; v<[eDetailingDataArray count]; v++)
        {
            
            NSMutableDictionary * eDetailingDictionary = [NSMutableDictionary dictionaryWithDictionary:[eDetailingDataArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"Info"];
            
            if (![[eDetailingDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Product_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Product_ID"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Product_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Media_File_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Media_File_ID"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Media_File_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Discussed_At"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Discussed_At"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Discussed_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Emp_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Emp_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Emp_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Location_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Location_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Doctor_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Doctor_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Conatct_ID"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Conatct_ID"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Conatct_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[eDetailingDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [eDetailingDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[eDetailingDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//Info
        }
    }
    
    [xmlWriter writeEndElement];//EDetailing
    
    return   [xmlWriter toString];
}
#pragma mark empNotes methods
-(NSString *)prepareXMLStringForeEmpNotes:(NSMutableArray *)empNotesArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"EmployeeNotes"];
    if(!([empNotesArray count]==0))
    {
        for (NSInteger v=0; v<[empNotesArray count]; v++)
        {
            
            NSMutableDictionary * empNoteDictionary = [NSMutableDictionary dictionaryWithDictionary:[empNotesArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"EmployeeNote"];
            
            if (![[empNoteDictionary stringForKey:@"Note_ID"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Note_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Note_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Title"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Title"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Title"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Description"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Description"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Description"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Category"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Category"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Category"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Emp_ID"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Emp_ID"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Emp_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Location_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Location_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Doctor_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Doctor_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Conatct_ID"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Conatct_ID"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Conatct_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Customer_Attribute_1"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Customer_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Customer_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Customer_Attribute_2"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Customer_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Customer_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Customer_Attribute_3"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Customer_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Customer_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Customer_Attribute_4"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Customer_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Customer_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[empNoteDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [empNoteDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[empNoteDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//EmployeeNote
        }
    }
    [xmlWriter writeEndElement];//EmployeeNotes
    
    return   [xmlWriter toString];
}
#pragma mark empStockData methods
-(NSString *)prepareXMLStringForempStockData:(NSMutableArray *)empStockDataArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"EmployeeStockInfo"];
    if(!([empStockDataArray count]==0))
    {
        for (NSInteger v=0; v<[empStockDataArray count]; v++)
        {
            
            NSMutableDictionary * empStockDataDictionary = [NSMutableDictionary dictionaryWithDictionary:[empStockDataArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"EmployeeStock"];
            
            if (![[empStockDataDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Product_ID"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Product_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Product_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Emp_ID"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Emp_ID"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Emp_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Lot_No"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Lot_No"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Lot_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Expiry_Date"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Expiry_Date"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Expiry_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Lot_Qty"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Lot_Qty"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Used_Qty"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Used_Qty"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Used_Qty"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[empStockDataDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [empStockDataDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[empStockDataDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
    
            [xmlWriter writeEndElement];//EmployeeStock
            
        }
    }
    [xmlWriter writeEndElement];////EmployeeStockInfo
    return   [xmlWriter toString];
}
#pragma mark empExpenseData methods
-(NSString *)prepareXMLStringForempExpenseData:(NSDictionary *)empExpenseDataArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"EmpExpenses"];
    [xmlWriter writeEndElement];
    
    return   [xmlWriter toString];
}
#pragma mark issueNoteList methods
-(NSString *)prepareXMLStringForIssueNoteList:(NSDictionary *)issueNoteListArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *issueNotesArray=[issueNoteListArray valueForKey:@"issueNotesArray"];
    NSMutableArray *itemArray=[issueNoteListArray valueForKey:@"itemArray"];
    [xmlWriter writeStartElement:@"IssueNotes"];
    
    if(!([issueNotesArray count]==0))
    {
        
        for (int o=0; o<[issueNotesArray count]; o++)
        {
            
            NSMutableDictionary * issueNoteDictionary = [NSMutableDictionary dictionaryWithDictionary:[issueNotesArray objectAtIndex:o]];
            NSString *orderRef = [issueNoteDictionary stringForKey:@"Doc_No"];
            
            [xmlWriter writeStartElement:@"IssueNote"];
            
            if (![[issueNoteDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Doc_No"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Doc_No"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Doc_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Doc_Type"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Doc_Type"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Doc_Type"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Emp_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Emp_ID"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Emp_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Location_ID"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Location_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Doctor_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Doctor_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Conatct_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Conatct_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Conatct_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Notes"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Notes"].length !=0 ){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Notes"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Signee_Name"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Signee_Name"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Signee_Name"]];
                [xmlWriter writeEndElement];}
            if (![[issueNoteDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Status"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Custom_Attribute_7"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Custom_Attribute_7"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Custom_Attribute_7"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[issueNoteDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[issueNoteDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[issueNoteDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [issueNoteDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[issueNoteDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
        if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Doc_No"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Doc_No"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Doc_No"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Doc_No"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Product_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Product_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Product_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Lot_No"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Lot_No"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Lot_No"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Issue_Qty"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Issue_Qty"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Issue_Qty"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Issue_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Issue_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Issue_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_4"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_5"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_6"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_7"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_7"].length !=0){
                            [xmlWriter writeStartElement:@"C14"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_7"]];
                            [xmlWriter writeEndElement];
                        }
                        
                            [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            
            [xmlWriter writeEndElement];//IssueNote
            
        }
    }
    [xmlWriter writeEndElement];
    return   [xmlWriter toString];
}
#pragma mark DCList methods
-(NSString *)prepareXMLStringForDCList:(NSDictionary *)DCListArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"DC"];
    [xmlWriter writeEndElement];
    
    return   [xmlWriter toString];
}
#pragma mark TaskList methods

-(NSString *)prepareXMLStringForTaskList:(NSMutableArray *)TaskListArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"Tasks"];
    if(!([TaskListArray count]==0))
    {
        for (NSInteger v=0; v<[TaskListArray count]; v++)
        {
            
            NSMutableDictionary * taskListDictionary = [NSMutableDictionary dictionaryWithDictionary:[TaskListArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"Info"];
            
            if (![[taskListDictionary stringForKey:@"Task_ID"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Task_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Task_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Title"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Title"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Title"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Description"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Description"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Description"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Category"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Category"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Category"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Priority"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Priority"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Priority"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Status"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Assigned_To"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Assigned_To"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Assigned_To"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Emp_ID"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Emp_ID"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Emp_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Location_ID"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Location_ID"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Location_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Doctor_ID"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Doctor_ID"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Doctor_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Contact_ID"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Contact_ID"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Contact_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Show_Reminder"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Show_Reminder"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Show_Reminder"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Show_Reminder"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Show_Reminder"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Show_Reminder"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Remind_At"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Remind_At"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Remind_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Is_Active"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Is_Active"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Is_Active"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Is_Deleted"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Is_Deleted"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Is_Deleted"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Last_Updated_At"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[taskListDictionary stringForKey:@"Source"] isEqualToString:@"<null>"] && [taskListDictionary stringForKey:@"Source"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[taskListDictionary stringForKey:@"Source"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//Info
        }
    }
    [xmlWriter writeEndElement];//Task
    return   [xmlWriter toString];
}

#pragma mark Visit methods
-(NSString *)prepareXMLStringForVisitAdditionalInfo:(NSMutableArray *)visitAdditionalArray{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"VisitAdditionalInfo"];
    if(!([visitAdditionalArray count]==0))
    {
        for (NSInteger v=0; v<[visitAdditionalArray count]; v++)
        {
            
            NSMutableDictionary * visitAdditionalDictionary = [NSMutableDictionary dictionaryWithDictionary:[visitAdditionalArray objectAtIndex:v]];
            
            [xmlWriter writeStartElement:@"Info"];
            
            if (![[visitAdditionalDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Attrib_Name"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Attrib_Name"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Attrib_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Attrib_Value"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Attrib_Value"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Attrib_Value"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Attrib_Value"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Attrib_Value"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Attrib_Value"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Created_At"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Created_At"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitAdditionalDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [visitAdditionalDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[visitAdditionalDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//Info
        }
    }
    [xmlWriter writeEndElement];//VisitAdditionalInfo
    
    return   [xmlWriter toString];
}

#pragma mark Route reschedule request

-(void)postRouteDataToserver:(NSMutableArray *)routesarray
{
    [self sendRequestForChangeVisit:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendVisitChangeRequest" andProcDic:[self getProcedureDictionaryForChangeVisits:routesarray]];
}

- (void)sendRequestForChangeVisit:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    NSString *strParams =[[NSString alloc] init];
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"ChangeRequestList"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:@"ChangeRequestList"]];
    
    
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Send_VisitChangeRequest",[testServerDict stringForKey:@"name"],procName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];

    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"background sync Response %@",resultArray);
         
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             NSString *lastSynctimeStr;
             
             if([SWDefaults lastRouteRescheduleBackGroundSyncDate]==nil) {
                 lastSynctimeStr = [SWDefaults lastFullSyncDate];
             }
             else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastRouteRescheduleBackGroundSyncDate]]] == NSOrderedAscending) {
                 lastSynctimeStr = [SWDefaults lastRouteRescheduleBackGroundSyncDate];
             } else {
                 lastSynctimeStr = [SWDefaults lastFullSyncDate];
             }
             
             NSString *currentSyncStarttimeStr = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
             NSMutableArray *routesarray = [[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetRescheduleRoutes, lastSynctimeStr,currentSyncStarttimeStr]]];
             if (routesarray.count > 0) {
                 for (int i = 0; i<routesarray.count; i++) {
                     NSMutableDictionary *dicResult = [routesarray objectAtIndex:i];
                     NSString *Visit_Change_ID = [NSString getValidStringValue:[dicResult valueForKey:@"Visit_Change_ID"]];
                     
                     [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_Visit_Change_Log set Proc_Status = 'Y' where Visit_Change_ID = '%@'", Visit_Change_ID]];
                 }
             }
             [SWDefaults setLastRouteRescheduleBackGroundSyncDate:currentSyncStarttimeStr];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"background sync error in route reschedule background sync %@",error);
     }];
    
    //call start on your request operation
    [operation start];
}

-(NSDictionary *)getProcedureDictionaryForChangeVisits:(NSMutableArray *)routesarray
{
    NSString *xmlInRoutesString=[self prepareXMLStringForRescheduleRoute:routesarray];
    NSString *XMLInRoutesPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)xmlInRoutesString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"ChangeRequestList", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:XMLInRoutesPassingString,nil];
    
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
}

-(NSString *)prepareXMLStringForRescheduleRoute:(NSMutableArray *)RoutesArray{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"VCL"];
    
    if([RoutesArray count]!=0)
    {
        for (int l=0 ; l<[RoutesArray count] ; l++)
        {
            NSMutableDictionary *RouteDictionary = [NSMutableDictionary dictionaryWithDictionary:[RoutesArray objectAtIndex:l]];
            
            [xmlWriter writeStartElement:@"CR"];
            
            if (![[RouteDictionary stringForKey:@"Visit_Change_ID"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Visit_Change_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Visit_Change_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"FSR_Plan_Detail_ID"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"FSR_Plan_Detail_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"FSR_Plan_Detail_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Visit_Date"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Visit_Date"].length !=0) {
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Visit_Date"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Customer_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Site_Use_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Site_Use_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"SalesRep_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"SalesRep_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Change_Type"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Change_Type"].length !=0) {
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Change_Type"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"New_Visit_Date"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"New_Visit_Date"].length !=0) {
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"New_Visit_Date"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Proc_Status"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Proc_Status"].length !=0) {
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Proc_Status"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Approval_Status"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Approval_Status"].length !=0) {
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Approval_Status"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Requested_At"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Requested_At"].length !=0) {
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Requested_At"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Requested_By"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Requested_By"].length !=0) {
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Requested_By"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Last_Updated_At"].length !=0) {
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Last_Updated_At"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Last_Updated_By"].length !=0) {
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Approval_Action_At"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Approval_Action_At"].length !=0) {
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Approval_Action_At"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Approval_Action_By"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Approval_Action_By"].length !=0) {
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Approval_Action_By"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Cancelled_At"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Cancelled_At"].length !=0) {
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Cancelled_At"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Cancelled_By"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Cancelled_By"].length !=0) {
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Cancelled_By"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Reason"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Reason"].length !=0) {
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Reason"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Comments"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Comments"].length !=0) {
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Comments"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[RouteDictionary stringForKey:@"Remarks"] isEqualToString:@"<null>"] && [RouteDictionary stringForKey:@"Remarks"].length !=0) {
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[RouteDictionary stringForKey:@"Remarks"]];
                [xmlWriter writeEndElement];
            }
            
            [xmlWriter writeEndElement];//Route
        }
    }
    [xmlWriter writeEndElement];//Routes
    
    return [xmlWriter toString];
}

#pragma mark Route reschedule Cancel request

-(void)postRouteCancelDataToserver:(NSMutableArray *)routesarray
{
    [self sendRequestForVisitCancellation:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendVisitChangeCancellation" andProcDic:[self getProcedureDictionaryForChangeVisits:routesarray]];
}

#pragma mark Send cancel request to server
- (void)sendRequestForVisitCancellation:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    NSString *strParams =[[NSString alloc] init];
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"ChangeRequestList"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:@"ChangeRequestList"]];
    
    
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Send_VisitChangeCancellation",[testServerDict stringForKey:@"name"],procName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"background sync Response %@",resultArray);
         
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             [SWDefaults setLastRouteRescheduleCancelBackGroundSyncDate:currentSyncStarttimeStr];
             
             for (int i = 0; i<resultArray.count; i++) {
                 NSMutableDictionary *dicResult = [resultArray objectAtIndex:i];
                 
                 NSString *Approval_Status = [NSString getValidStringValue:[dicResult valueForKey:@"Approval_Status"]];
                 NSString *Approval_Action_At = [NSString getValidStringValue:[dicResult valueForKey:@"Approval_Action_At"]];
                 NSString *Approval_Action_By = [NSString getValidStringValue:[dicResult valueForKey:@"Approval_Action_By"]];
                 NSString *Cancelled_At = [NSString getValidStringValue:[dicResult valueForKey:@"Cancelled_At"]];
                 NSString *Cancelled_By = [NSString getValidStringValue:[dicResult valueForKey:@"Cancelled_By"]];
                 NSString *Remarks = [NSString getValidStringValue:[dicResult valueForKey:@"Remarks"]];
                 NSString *Visit_Change_ID = [[NSString getValidStringValue:[dicResult valueForKey:@"Visit_Change_ID"]] uppercaseString];
                 
                 [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_Visit_Change_Log set Approval_Status = '%@', Approval_Action_At = '%@', Approval_Action_By = '%@', Cancelled_At = '%@', Cancelled_By = '%@', Remarks = '%@' where Visit_Change_ID = '%@'", Approval_Status, Approval_Action_At, Approval_Action_By, Cancelled_At, Cancelled_By, Remarks, Visit_Change_ID]];
             }
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@" background sync error  in route reschedule cancel background sync %@",error);
     }];
    
    //call start on your request operation
    [operation start];
}

#pragma mark sync actual visit data on push notification

-(void)syncVisitDataToserver
{
    [self sendActualVisitData:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSyncVisitData" andProcDic:[self getProcedureDictionaryForActualVisits]];
}

#pragma mark Send cancel request to server
- (void)sendActualVisitData:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];

    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];



    NSString *strParams =[[NSString alloc] init];
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"VisitList"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:@"VisitList"]];

    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRepID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    

    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];

    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Sync_VisitData",[testServerDict stringForKey:@"name"],procName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];


    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;

    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];

    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"background sync_MC_ExecSyncVisitData Response %@",resultArray);

         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"Delete from TBL_FSR_Planned_Visits"]];
             
             for (int i = 0; i<resultArray.count; i++) {
                 NSMutableDictionary *dicResult = [resultArray objectAtIndex:i];
                 NSString *FSR_Plan_Detail_ID = [NSString getValidStringValue:[dicResult valueForKey:@"FSR_Plan_Detail_ID"]];
                 NSString *Visit_Date = [NSString getValidStringValue:[dicResult valueForKey:@"Visit_Date"]];
                 NSString *Customer_ID = [NSString getValidStringValue:[dicResult valueForKey:@"Customer_ID"]];
                 NSString *Site_Use_ID = [NSString getValidStringValue:[dicResult valueForKey:@"Site_Use_ID"]];
                 NSString *Start_Time = [NSString getValidStringValue:[dicResult valueForKey:@"Start_Time"]];
                 NSString *End_Time = [NSString getValidStringValue:[dicResult valueForKey:@"End_Time"]];
                 NSString *SalesRep_ID = [NSString getValidStringValue:[dicResult valueForKey:@"SalesRep_ID"]];
                 NSString *Visit_Status = [NSString getValidStringValue:[dicResult valueForKey:@"Visit_Status"]];
                 NSString *Planned_Visit_ID = [[NSString getValidStringValue:[dicResult valueForKey:@"Planned_Visit_ID"]]uppercaseString];

                 [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"insert into TBL_FSR_Planned_Visits (Planned_Visit_ID, FSR_Plan_Detail_ID, Visit_Date, Customer_ID, Site_Use_ID, Start_Time, End_Time, SalesRep_ID, Visit_Status) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",Planned_Visit_ID, FSR_Plan_Detail_ID, Visit_Date, Customer_ID, Site_Use_ID, Start_Time, End_Time, SalesRep_ID, Visit_Status]];
             }
             [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRouteDataFromBackground" object:nil];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"background sync error in route reschedule sync_MC_ExecSyncVisitData %@",error);
     }];

    //call start on your request operation
    [operation start];
}

-(NSDictionary *)getProcedureDictionaryForActualVisits
{
    /*** Visits*/
    NSMutableArray *vistsArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbSyncActualVisits]]];
    
    NSString *xmlInRoutesString=[self prepareXMLStringForActualVisit:vistsArray];
    NSString *XMLInRoutesPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)xmlInRoutesString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"VisitList", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:XMLInRoutesPassingString, nil];

    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
}

-(NSString *)prepareXMLStringForActualVisit:(NSMutableArray *)vistsArray{

    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"AVL"];

    if([vistsArray count]!=0)
    {
        for (int l=0 ; l<[vistsArray count] ; l++)
        {
            NSMutableDictionary *visitDictionary = [NSMutableDictionary dictionaryWithDictionary:[vistsArray objectAtIndex:l]];

            [xmlWriter writeStartElement:@"V"];

            if (![[visitDictionary stringForKey:@"Actual_Visit_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Actual_Visit_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Actual_Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[visitDictionary stringForKey:@"FSR_Plan_Detail_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"FSR_Plan_Detail_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"FSR_Plan_Detail_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[visitDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Customer_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[visitDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Site_Use_ID"].length !=0) {
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Site_Use_ID"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[visitDictionary stringForKey:@"Visit_Start_Date"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Visit_Start_Date"].length !=0) {
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Visit_Start_Date"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[visitDictionary stringForKey:@"Visit_End_Date"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Visit_End_Date"].length !=0) {
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Visit_End_Date"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//actual visit
        }
    }
    [xmlWriter writeEndElement];//actual visits

    return [xmlWriter toString];
}


#pragma mark execute Visit Change Log data on push notification

-(void)executeVisitChangeLogDataToserver
{
    [self executeVisitChangeLogData:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecGetVisitChangeLog"];
}

#pragma mark Send cancel request to server
- (void)executeVisitChangeLogData:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    NSString *strParams =[[NSString alloc] init];

    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRepID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    
    
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_VisitChangeLog",[testServerDict stringForKey:@"name"],procName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"background sync_MC_ExecGetVisitChangeLog Response %@",resultArray);
         
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             for (int i = 0; i<resultArray.count; i++) {
                 NSMutableDictionary *dicResult = [resultArray objectAtIndex:i];
                 
                 NSString *Approval_Status = [NSString getValidStringValue:[dicResult valueForKey:@"Approval_Status"]];
                 NSString *Approval_Action_At = [NSString getValidStringValue:[dicResult valueForKey:@"Approval_Action_At"]];
                 NSString *Approval_Action_By = [NSString getValidStringValue:[dicResult valueForKey:@"Approval_Action_By"]];
                 NSString *Cancelled_At = [NSString getValidStringValue:[dicResult valueForKey:@"Cancelled_At"]];
                 NSString *Cancelled_By = [NSString getValidStringValue:[dicResult valueForKey:@"Cancelled_By"]];
                 NSString *Remarks = [NSString getValidStringValue:[dicResult valueForKey:@"Remarks"]];
                 NSString *Visit_Change_ID = [[NSString getValidStringValue:[dicResult valueForKey:@"Visit_Change_ID"]] uppercaseString];
                 
                 [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_Visit_Change_Log set Approval_Status = '%@', Approval_Action_At = '%@', Approval_Action_By = '%@', Cancelled_At = '%@', Cancelled_By = '%@', Remarks = '%@' where Visit_Change_ID = '%@'", Approval_Status, Approval_Action_At, Approval_Action_By, Cancelled_At, Cancelled_By, Remarks, Visit_Change_ID]];
             }
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"background sync error in route reschedule sync_MC_ExecGetVisitChangeLog %@",error);
     }];
    
    //call start on your request operation
    [operation start];
}


@end
