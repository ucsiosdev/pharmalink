//
//  SalesWorxiBeaconManager.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <KontaktSDK/KontaktSDK.h>
#import "SalesWorxCustomClass.h"

@interface SalesWorxiBeaconManager : NSObject<KTKBeaconManagerDelegate>

@property KTKBeaconManager *beaconManager;

+ (SalesWorxiBeaconManager*) retrieveSingleton;
@property(strong,nonatomic) SalesWorxBeacon * detectedBeacon;
@property(strong,nonatomic) SalesWorxBeacon * customerBeaconfromDB;
+ (void) destroyBeaconSingleton;

-(void)initilizeiBeaconManager:(SalesWorxBeacon*)currentBeacon;


@end
