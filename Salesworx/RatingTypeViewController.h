//
//  RatingTypeViewController.h
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYRateView.h"

@interface RatingTypeViewController : UIView <DYRateViewDelegate>
{
    
}
@property(nonatomic, strong) UILabel *rateLabel;

@end
