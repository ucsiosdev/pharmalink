//
//  SWSignatureCell.h
//  SWFoundation
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableCell.h"
#import "SWSignatureView.h"

@interface SWSignatureCell : EditableCell < SWSignatureViewDelegate, UITextFieldDelegate > {
    SWSignatureView *signatureView;
    UITextField *textField;
}

@property (nonatomic, strong) SWSignatureView *signatureView;
@property (nonatomic, strong) UITextField *textField;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end