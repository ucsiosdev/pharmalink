//
//  MedRepProductPDFViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepProductPDFViewController.h"
#import "MedRepProductsHomeCollectionViewCell.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "ReaderViewController.h"
#import "AppControl.h"
#import "MedRepQueries.h"

@interface MedRepProductPDFViewController ()<ReaderViewControllerDelegate>

@end

@implementation MedRepProductPDFViewController
@synthesize productImagesArray,currentImageIndex,selectedIndexPath,productPDFCollectionView,productDataArray,pdfWebView,currentVisit,isCurrentUserPharmacy,isCurrentUserDoctor;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchEmailID];
    
    viewedImages=[[NSMutableArray alloc]init];
    demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_PDF_DEFAULT_DEMO_TIME.integerValue;

    cancelButton.hidden=YES;
    
    if (productDataArray.count>0) {
        [productDataArray setValue:@"N" forKey:@"isSelected"];
    }

    if (selectedIndexPathArray.count>0) {
        
    }
    else{
        selectedIndexPathArray=[[NSMutableArray alloc]init];
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)fetchEmailID{
    
    emailIDForAutoInsert = @"";
    
    if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
        
        if (isCurrentUserDoctor){
            emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaDoctorID:self.doctor_ID];
        }else  if (isCurrentUserPharmacy){
            emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaContactID:self.contact_ID];
        }
        
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    if (productDataArray.count>0) {
        
        [productPDFCollectionView reloadData];
        
        if (selectedIndexPath) {
            
            [productPDFCollectionView selectItemAtIndexPath:selectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
            [self collectionView:productPDFCollectionView didSelectItemAtIndexPath:selectedIndexPath];
        }
    }
}

-(void)loadProductDetailsWithSelectedIndex:(NSInteger)selectedIndex
{
    productHeaderTitle.text=[NSString stringWithFormat:@"%@", [[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Name"]];
    
}

-(void)viewWillDisappear:(BOOL)animated{
}

-(void)viewWillAppear:(BOOL)animated
{
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingProductPDF];
    }
    
    pdfWebView.scrollView.delegate=self;
    
    [productPDFCollectionView registerClass:[MedRepProductsHomeCollectionViewCell class] forCellWithReuseIdentifier:@"productHomeCell"];
    
    
    
    selectedCellIndex=0;
    
    productPDFCollectionView.backgroundColor = [UIColor clearColor];
    productPDFCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
   // NSLog(@"product data array is %@", [productDataArray description]);
    
  
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [pdfWebView addGestureRecognizer:singleTap];
    
    
    //adding double tap gesture for uiwebview to see it on full screen
    
    UITapGestureRecognizer * doubleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleDoubleTaponWebView:)];
    doubleTap.numberOfTouchesRequired=2;
    [pdfWebView addGestureRecognizer:doubleTap];
    
    if ([[AppControl retrieveSingleton].DISABLE_EMAIL_PRODUCT_MEDIA isEqualToString:KAppControlsYESCode]){
        
        self.isViewing = YES;
    }else{
       self.isViewing = NO;
    }
    
    
    if (self.isViewing==YES) {
        
        [emailButton setHidden:YES];
    }
    
    else
    {
        [emailButton setHidden:NO];
        
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, 100)];
    [webView.scrollView setScrollEnabled:NO];
    [webView.scrollView setBounces:NO];
    
}

#pragma mark - UIGestureRecognizer methods

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        readerViewController.fileName = productHeaderTitle.text;
#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
        
        [self.navigationController pushViewController:readerViewController animated:YES];
        
#else // present in a modal view controller
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:readerViewController animated:YES completion:^{
            viewedTime=[NSDate date];
        }];
        
#endif // DEMO_VIEW_CONTROLLER_PUSH
    }
    else // Log an error so that we know that something went wrong
    {
        NSLog(@"%s [ReaderDocument withDocumentFilePath:'%@' password:'%@'] failed.", __FUNCTION__, filePath, phrase);
    }
}
#pragma mark - ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
 
    if(self.isFromProductsScreen)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        NSDate *currentTime = [NSDate date];
        NSTimeInterval viewedTime1 = [currentTime timeIntervalSinceDate:viewedTime];
        NSLog(@"viewed Time for PDF = %f", viewedTime1);
        long ti = lroundf(viewedTime1);
        int viewedTimeInterval = ti % 60;
        
        ProductMediaFile* selectedMediaFile=[productDataArray objectAtIndex:currentImageIndex];
        if (selectedMediaFile.viewedTime.length == 0) {
            selectedMediaFile.viewedTime=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%ld",(long)viewedTimeInterval]];
        } else {
            selectedMediaFile.viewedTime = [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%0.2f",(viewedTimeInterval + selectedMediaFile.viewedTime.doubleValue)]];
        }
        if (selectedMediaFile.discussedAt.length == 0) {
            selectedMediaFile.discussedAt = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        }
        [productDataArray replaceObjectAtIndex:currentImageIndex withObject:selectedMediaFile];
        
        //    selectedMediaFile.viewedTime=[NSString stringWithFormat:@"%ld",(long)viewedTime];
        //    [productDataArray replaceObjectAtIndex:currentImageIndex withObject:selectedMediaFile];
        
#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
        
        [self.navigationController popViewControllerAnimated:YES];
        
#else // dismiss the modal view controller
        
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
#endif // DEMO_VIEW_CONTROLLER_PUSH
}

-(void)handleDoubleTaponWebView:(UITapGestureRecognizer*)tapGesture
{
    NSLog(@"double tapped");
    
    NSLog(@"rect of view during double tap %@", NSStringFromCGRect(pdfWebView.frame));
    
    
}

- (IBAction)closeButtontapped:(id)sender {
    
    if(self.isFromProductsScreen)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
    if ([self.pdfViewedDelegate respondsToSelector:@selector(PDFViewedinFullScreenDemo:)]  ) {
        
        [self.pdfViewedDelegate PDFViewedinFullScreenDemo:viewedImages];
    }
    
    if ([self.pdfViewedDelegate respondsToSelector:@selector(fullScreenPDFDidCancel)]  ) {
        
        [self.pdfViewedDelegate fullScreenPDFDidCancel];
    }
    
        if ([self.pdfViewedDelegate respondsToSelector:@selector(setBorderOnLastViewedImage:)]) {
            [self.pdfViewedDelegate setBorderOnLastViewedImage:currentImageIndex];
        }
        
    NSLog(@"viewed images are %@", [viewedImages description]);
    [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (IBAction)emailButtonTapped:(id)sender {
    
    
    
    collectionViewHeightConstraint.constant=208;
    
    
    [productPDFCollectionView setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:2.0f animations:^{
        [productPDFCollectionView layoutIfNeeded];
    }];
    
    
    isEmailTapped=YES;
    cancelButton.hidden=NO;
    
    buttonTitle = [sender titleForState:UIControlStateNormal];
    
    if (buttonTitle.length>0 && sendEmail==YES ) {
        
        [self sendEmail];
    }
    else
    {
               
        UIAlertView * emailAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Email", nil) message:NSLocalizedString(@"Please select the media you would like to send as an Email, you can select multiple media at a time", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [emailAlert show];
        
    }
    
    
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    isEmailTapped=NO;
    cancelButton.hidden=YES;
    sendEmail=NO;
    for (NSMutableDictionary * currentDict in productDataArray) {
        NSLog(@"dict being removed is %@", currentDict);
    }
    
    [productDataArray setValue:@"N" forKey:@"isSelected"];
    
    [selectedIndexPathArray removeAllObjects];
    [productPDFCollectionView reloadData];
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];
    
}

#pragma mark UICOllectionview methods


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(147, 150);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  productDataArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    //    static NSString *cellIdentifier = @"productCell";
    static NSString *cellIdentifier = @"productHomeCell";
    
    NSString* mediaType = [[productDataArray  objectAtIndex:indexPath.row]valueForKey:@"Media_Type"];
    
    
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.selectedImgView.image=nil;
    
    cell.placeHolderImageView.hidden=YES;
    
    
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[productDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
    
    //  NSLog(@"file name is %@", fileName);
    
    // NSLog(@"media type %@", mediaType);
    
    
    if ([mediaType isEqualToString:@"Brochure"]) {
        
        //        UIImage* thumbImageforPdf=[MedRepDefaults generateThumbImageforPdf:fileName];
        //
        //        cell.productImageView.image=thumbImageforPdf;
        //cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        
        cell.captionLbl.text=[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        
        
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        
        cell.placeHolderImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumb"];
        
        
    }
    
    
    cell.backgroundColor=[UIColor clearColor];
    
    if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"N"]) {
        cell.selectedImgView.image=nil;
    }
    else
    {
        cell.selectedImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
    }

    
    
//    if ([[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"]==nil|| [[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"N"]) {
//        
//        
//        cell.selectedImgView.image=nil;
//        
//        
//    }
//    
//    
//    else
//    {
//        cell.selectedImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
//    }
    
    
    
    if (indexPath.row== selectedCellIndex) {
        
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        
    }
    
    else
    {
        cell.layer.borderWidth=0;
        
    }
    
    
    
    return cell;
    
    
}

-(void)loadSelectedCellDetails:(NSInteger)selectedIndex

{
    
    //load selected pdf
    
    
    if (selectedIndex) {
        
        NSString* filePath=[[productDataArray  objectAtIndex:selectedIndex]valueForKey:@"File_Name"];
        
        NSURL *targetURL = [NSURL fileURLWithPath:filePath];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [pdfWebView loadRequest:request];
        
    }
    
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    NSLog(@"current offset %@", NSStringFromCGPoint(imagesScrollView.contentOffset));
    
    selectedCellIndexPath=indexPath;
    currentImageIndex=indexPath.row;
    [demoTimer invalidate];
    demoTimer=nil;

    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
    filePath = [documentsDirectory stringByAppendingPathComponent:[[productDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
    
    
    if (isEmailTapped==YES) {
        
        
        MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        
        NSLog(@"selected cell status %@",[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"]);
        
        
        if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"Y"]) {
            [[productDataArray objectAtIndex:indexPath.row]setValue:@"N" forKey:@"isSelected"];
        }
        
        else
        {
            [[productDataArray objectAtIndex:indexPath.row]setValue:@"Y" forKey:@"isSelected"];
        }

        
//        if ([[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"]==nil || [[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"Y"]) {
//            
//            [[productDataArray objectAtIndex:indexPath.row]setValue:@"N" forKey:@"isSelected"];
//            
//        }
//        
//        else
//        {
//            
//           // [[productDataArray objectAtIndex:indexPath.row]removeObjectForKey:@"isSelected"];
//            
//            [[productDataArray objectAtIndex:indexPath.row]setValue:@"Y" forKey:@"isSelected"];
//
//        }
        
        
        
        if ([selectedIndexPathArray containsObject:indexPath]) {
            [selectedIndexPathArray removeObject:indexPath];
            
        }
        else
        {
            
            [selectedIndexPathArray addObject:indexPath];
            sendEmail=YES;
            
            [emailButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
            
            
        }
        selectedCellIndex=indexPath.row;
        [productPDFCollectionView reloadData];
        
        
    }
    
    else
    {
        [collectionView setAllowsMultipleSelection:NO];
        
        
        
        if (demoTimer) {
            
            demoTimer=nil;
        }
        
        //start the timer to check if images for displayed for more than 5 secs
        demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:NO];
        
        
        
        NSString*documentsDirectory;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        
        
        selectedCellIndex=indexPath.row;
        
        [productPDFCollectionView reloadData];
        
        
        
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[productDataArray  objectAtIndex:indexPath.row]valueForKey:@"File_Name"]];
        
        NSLog(@"file name in pdf is %@", fileName);
        NSURL *targetURL = [NSURL fileURLWithPath:fileName];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [pdfWebView loadRequest:request];
        
    }
    
    NSLog(@"selected indexpath array is %@", selectedIndexPathArray);
    
    if (selectedIndexPathArray.count==0) {
        
        [emailButton setTitle:@"Email" forState:UIControlStateNormal];
        cancelButton.hidden=YES;
        isEmailTapped=NO;
        sendEmail=NO;
    }
    
    if (demoTimer) {
        
        demoTimer=nil;
    }
    
    //start the timer to check if images for displayed for more than 5 secs
    demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:NO];
    
    
    
    
    
    [self loadProductDetailsWithSelectedIndex:selectedCellIndex];
    
    
}

-(void)startFaceTimeTimer

{
    if (![viewedImages containsObject:[productDataArray objectAtIndex:currentImageIndex]]) {
        [viewedImages addObject:[productDataArray objectAtIndex:currentImageIndex]];

    }
    
    NSLog(@"pdf viewed for %ld seconds", demoTimeInterval);

   // NSLog(@"viewed pdfs are %@", [viewedImages description]);
    
}

-(void)sendEmail

{
    if ([MFMailComposeViewController canSendMail])
    {
        if (selectedIndexPathArray.count>0) {
            
       // NSString *emailAddres = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:self.currentVisit.visitOptions.customer.Customer_ID];

        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        
        mailController.mailComposeDelegate = self;
        
        [mailController setSubject:@"MedRep"];
        
        [mailController setMessageBody:@"MedRep Document" isHTML:NO];
            
        [mailController setToRecipients:@[emailIDForAutoInsert]];
        
        [mailController.navigationBar setTintColor:[UIColor blackColor]];
            
        NSString *fileNameString;
        
        
        for (NSInteger i=0; i<selectedIndexPathArray.count; i++) {
            
            NSIndexPath * currentIndexPath=[selectedIndexPathArray objectAtIndex:i];
            
            NSString* pdfName= [NSString stringWithFormat:@"%@",[[productDataArray objectAtIndex:currentIndexPath.row]valueForKey:@"File_Name"] ];
            
            fileNameString=[[MedRepDefaults fetchDocumentsDirectory] stringByAppendingPathComponent:[[productDataArray  objectAtIndex:currentIndexPath.row]valueForKey:@"File_Name"]];
            
            
            NSData *currentImageData = [NSData dataWithContentsOfFile:fileNameString];
            
            [mailController addAttachmentData:currentImageData mimeType:@"application/pdf" fileName:pdfName];
            
        }
        
        [self presentViewController:mailController animated:YES completion:nil];
        }
        
        else{
           [SWDefaults showAlertAfterHidingKeyBoard:@"Email" andMessage:@"Please select the media you would like to send as an Email, you can select multiple media at a time" withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        
    }
    
}

#pragma mark Mail Composer Delegate


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
            
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            
            
            [emailButton setTitle:@"Email" forState:UIControlStateNormal];
            
            
            for (NSInteger i=0; i<selectedIndexPathArray.count; i++) {
                NSIndexPath * selectedIndexPathforEmail=[selectedIndexPathArray objectAtIndex:i];
                MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell*)[productPDFCollectionView cellForItemAtIndexPath:selectedIndexPathforEmail];
                //already seelected cell
               // [[cell.contentView.subviews lastObject] removeFromSuperview];
                NSLog(@"check subviews in mail delegate %@", [cell.contentView.subviews description]);
            }
            
           
            
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    sendEmail=NO;
    isEmailTapped=NO;
    cancelButton.hidden=YES;
    
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];

    [productDataArray setValue:@"N" forKey:@"isSelected"];
    
    [selectedIndexPathArray removeAllObjects];
    [productPDFCollectionView reloadData];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//#pragma mark UIScrollView Delegate methods
//
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//
//    if(scrollView.contentOffset.y <= 0.0){
//        NSLog(@"TOP REACHED");
//
//        collectionViewHeightConstraint.constant=208;
//
//
//        [productPDFCollectionView setNeedsUpdateConstraints];
//
//        [UIView animateWithDuration:2.0f animations:^{
//            [productPDFCollectionView layoutIfNeeded];
//        }];
//
//
//    }
//}// called when scroll view grinds to a halt
//
//
//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//
//{
//    collectionViewHeightConstraint.constant=0;
//    [productPDFCollectionView setNeedsUpdateConstraints];
//
//    [UIView animateWithDuration:2.0f animations:^{
//        [productPDFCollectionView layoutIfNeeded];
//    }];
//
//
//
//    /*
//    if(scrollView.contentOffset.y>=
//       (scrollView.contentSize.height - scrollView.frame.size.height)){
//        NSLog(@"BOTTOM REACHED");
//    }
//    if(scrollView.contentOffset.y <= 0.0){
//        NSLog(@"TOP REACHED");
//
//        collectionViewHeightConstraint.constant=208;
//
//
//        [productPDFCollectionView setNeedsUpdateConstraints];
//
//        [UIView animateWithDuration:0.25f animations:^{
//            [productPDFCollectionView layoutIfNeeded];
//        }];
//
//
//    }*/
//
//}
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//
//    /*
//    CGFloat requiredFullScreenheight=[UIScreen mainScreen].bounds.size.height-96;
//
//    CGSize fullScreenContentSize=CGSizeMake(pdfWebView.frame.size.width, requiredFullScreenheight);
//
//
//
//    [UIView animateWithDuration:.3f animations:^{
//        CGRect currentFrame = pdfWebView.frame;
//        currentFrame.size.height =requiredFullScreenheight;
//        pdfWebView.frame = currentFrame;
//
//    }];
//
//
//
//
//
//    NSLog(@"scrollview did scroll called");
//
//    NSLog(@"pdf webview frame is %@", NSStringFromCGRect(pdfWebView.frame));
//
//
//
//    NSLog(@"required fullscreen height is %0.4f", requiredFullScreenheight);
//
//    */
//  
//    
//
//    
//}

@end
