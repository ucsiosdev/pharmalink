//
//  SalesWorxCustomersFilterViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxCustomersFilterViewController.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "SalesWorxCustomerFilterChildViewController.h"
@interface SalesWorxCustomersFilterViewController ()

@end

@implementation SalesWorxCustomersFilterViewController
@synthesize customerListArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    // NSLog(@"customer list array in filter %@",[customerListArray description]);
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:@"Clear" style:UIBarButtonItemStylePlain target:self action:@selector(clearTapped)];
    
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    
    saveBtn.tintColor=[UIColor whiteColor];
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.rightBarButtonItem=saveBtn;
    //self.navigationItem.leftBarButtonItem=closeBtn;
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
}


-(void)clearTapped
{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)searchButtonTapped
{
    
}
-(IBAction)customerNameButtonTapped:(id)sender
{
//    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
//    filterDescVC.filterPopOverController=self.filterPopOverController;
//    filterDescVC.isCreateVisit=YES;
//    filterDescVC.selectedFilterDelegate=self;
//    filterDescVC.descTitle=@"Customer Number";
//    filterDescVC.filterDescArray=[customerListArray valueForKey:@"Customer_No"];
//    [self.navigationController pushViewController:filterDescVC animated:YES];
    

    SalesWorxCustomerFilterChildViewController *childFilterVC=[[SalesWorxCustomerFilterChildViewController alloc]init];
    childFilterVC.customerListArray=customerListArray;
    childFilterVC.searchElementName=@"customerNumber";
    childFilterVC.titleString=@"Customer Number";
    [self.navigationController pushViewController:childFilterVC animated:YES];
}

@end
