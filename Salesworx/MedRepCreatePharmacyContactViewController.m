//
//  MedRepCreatePharmacyContactViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepCreatePharmacyContactViewController.h"
#import "MedRepDefaults.h"
#import "NSString+Additions.h"
#import "MedRepQueries.h"

@interface MedRepCreatePharmacyContactViewController ()

@end

@implementation MedRepCreatePharmacyContactViewController


@synthesize contactNameTxtFld,locationDetailsDictionary,contactTitleTxtFld,contactTelephoneTxtFld,contactEmailTxtFld,contactDetailsDict,contactID,locationNameTxtFld,isFromMultiCalls,createContactPopOverController;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    contactDetailsDict =[[NSMutableDictionary alloc]init];
    
    contactID=[NSString createGuid];
    
    [contactDetailsDict setValue:contactID forKey:@"Contact_ID"];
    
    locationNameTxtFld.userInteractionEnabled=NO;
    locationNameTxtFld.text=[locationDetailsDictionary valueForKey:@"Location_Name"];
    
    
//    CGRect frame = CGRectMake(0, 0, 400, 44);
//    UILabel *label = [[UILabel alloc] initWithFrame:frame];
//    label.backgroundColor = [UIColor clearColor];
//    label.font = headerTitleFont;
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor whiteColor];
//    label.text = NSLocalizedString(@"Create Contact", nil);
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Create Contact", nil)];

    
    
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillDisappear:(BOOL)animated
{
 
    
}

-(void)cancelTapped
{
    [createContactPopOverController dismissPopoverAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.preferredContentSize=CGSizeMake(376, 600);
    if (self.navigationController){
        self.navigationController.preferredContentSize = self.preferredContentSize;
    }
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingCreateContact];
    }
    
    
    UIBarButtonItem* saveContactButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveContactButtonTapped)];
    
    [saveContactButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=saveContactButton;
    
    if (isFromMultiCalls==YES) {
        
        UIBarButtonItem* cancelButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
        
        [cancelButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem=cancelButton;
    }
//

    
    

}



-(void)saveContactButtonTapped

{
    
    NSString* misssingString;
    
    if (contactTitleTxtFld.text.length==0) {
        
        misssingString=@"Please enter Contact Title";

    }
    
    else if (contactNameTxtFld.text.length==0)
        
    {
        misssingString=@"Please enter Contact Name";
    }
        
    else if ((![MedRepDefaults validateEmail:contactEmailTxtFld.text]) && contactTelephoneTxtFld.text.length==0)

    {
        misssingString=@"Please enter valid Contact Email or Telephone";
    }
   
//    else if (contactTelephoneTxtFld.text.length==0)
//        
//    {
//        misssingString=@"Please enter Contact Telephone";
//    }
//    
//    else
//    {
//        
//    }
    
    if (misssingString.length>0) {
        
//        UIAlertView * missingAlert=[[UIAlertView alloc]initWithTitle:@"Missing Data" message:[NSString stringWithFormat:@"Please enter %@ and try again",misssingString] delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
//        [missingAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:NSLocalizedString(misssingString, nil) withController:self];
        
        
    }
    
    else
    {
       
        [self savePharmacyContact:contactDetailsDict];
    }
    
    NSLog(@"contact details before saving %@",contactDetailsDict);
    
}


-(void)savePharmacyContact:(NSMutableDictionary*)detailsDict

{
    NSLog(@"saving pharmacy data %@", detailsDict);
    
    
    PharmacyContact * contact=[[PharmacyContact alloc]init];
    contact.locationID=[locationDetailsDictionary valueForKey:@"Location_ID"];
    contact.contactID=[contactDetailsDict valueForKey:@"Contact_ID"];
    contact.contactTitle=[contactDetailsDict valueForKey:@"Contact_Title"];
    contact.contactName=[contactDetailsDict valueForKey:@"Contact_Name"];
    contact.contactEmail=[contactDetailsDict valueForKey:@"Contact_Email"];
    contact.contactTelephone=[contactDetailsDict valueForKey:@"Contact_Telephone"];

    BOOL success= [MedRepQueries createContactforPharmacy:contact];
    if (success==YES) {
        
        NSLog(@"contact created successfully");
        
        UIAlertView * missingAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString([NSString stringWithFormat:@"Contact created successfully"], nil) delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
        missingAlert.tag=100;
        [missingAlert show];
        
    }
    
}

#pragma mark UItextField Delegate Methods




- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==contactTitleTxtFld) {
        [contactDetailsDict setValue:textField.text forKey:@"Contact_Title"];
    }
    
    else if (textField ==contactNameTxtFld)
    {
        [contactDetailsDict setValue:textField.text forKey:@"Contact_Name"];
    }
    
    else if (textField ==contactEmailTxtFld)
    {
        
        NSString* emailstr=contactEmailTxtFld.text;
        if ([NSString isEmpty:emailstr]==NO) {
            
            BOOL emailValid=[MedRepDefaults validateEmail:contactEmailTxtFld.text];
            
            if (emailValid==YES) {
                [contactDetailsDict setValue:textField.text forKey:@"Contact_Email"];
                
            }
            else
            {

                [SWDefaults showAlertAfterHidingKeyBoard:@"Invalid Email" andMessage:@"Please enter valid Email Address" withController:self];
            }
        }
    }
    
    else if (textField ==contactTelephoneTxtFld){
        [contactDetailsDict setValue:textField.text forKey:@"Contact_Telephone"];

    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==contactTitleTxtFld) {
        [contactTitleTxtFld resignFirstResponder];
        [contactNameTxtFld becomeFirstResponder];
        [contactDetailsDict setValue:textField.text forKey:@"Contact_Title"];
    }
    
    else if (textField ==contactNameTxtFld)
    {
        [contactNameTxtFld resignFirstResponder];
        [contactEmailTxtFld becomeFirstResponder];
        [contactDetailsDict setValue:textField.text forKey:@"Contact_Name"];
    }
    
    else if (textField ==contactEmailTxtFld)
    {
        [contactEmailTxtFld resignFirstResponder];
        [contactTelephoneTxtFld becomeFirstResponder];
        
        if ([MedRepDefaults validateEmail:contactEmailTxtFld.text]) {
            [contactDetailsDict setValue:textField.text forKey:@"Contact_Email"];
        }
    }
    
    else if (textField ==contactTelephoneTxtFld){
        [contactTelephoneTxtFld resignFirstResponder];
        [contactDetailsDict setValue:textField.text forKey:@"Contact_Telephone"];
        
    }

    
    
    return YES;
}





    
    
    
    
 


#define VALID_CHARECTERS @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define TelephoneNumber @"0123456789"

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    
    if(textField==contactEmailTxtFld)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if (newString.length>ContactEmailAddressTextFieldLimit) {
            return NO;
        }
        else{
            return [string isEqualToString:filtered];
            
        }    }
    
  else  if(textField==self.contactTelephoneTxtFld)
        
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:TelephoneNumber] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if (newString.length>ContactTelephoneTextFieldLimit) {
            return NO;
        }
        else{
            return [string isEqualToString:filtered];
 
        }
        
        

    }
    
  else  if(textField==contactTitleTxtFld)
    {
        
        
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_WITHSPACE] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if (newString.length>ContactTitleTextFieldLimit) {
            return NO;
        }
        else{
            return [string isEqualToString:filtered];
            
        }
        
        
        
          }
    else  if(textField==contactNameTxtFld)
    {
        
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_WITHSPACE] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if (newString.length>ContactNameTextFieldLimit) {
            return NO;
        }
        else{
            return [string isEqualToString:filtered];
            
        }
        
        
        
        
        
    }
    else  if(textField==self.contactEmailTxtFld)
    {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if (newString.length>ContactEmailAddressTextFieldLimit) {
            return NO;
        }
        else{
            return [string isEqualToString:filtered];
            
        }
        
        
        
    }
   
    else
    {
        return YES;
    }
    
    
   
}


#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
//     if (alertView.tag==1000)
//    {
//        [contactEmailTxtFld becomeFirstResponder];
//    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag==100) {
        
        
        if ([self.delegate respondsToSelector:@selector(createdContactforPharmacy:)]) {
            
            [self.delegate createdContactforPharmacy:contactDetailsDict];
            
            if (isFromMultiCalls==YES) {
                [createContactPopOverController dismissPopoverAnimated:YES];
            }
            else
            {
            [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
