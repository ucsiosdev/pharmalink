//
//  MedRepButton.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDefaultButton.h"
IB_DESIGNABLE

@interface MedRepButton : SalesWorxDefaultButton

@property (nonatomic) IBInspectable BOOL ShadowColor;
@property (nonatomic) IBInspectable BOOL isClearButton;

@property (nonatomic) IBInspectable BOOL isTextField;


@end
