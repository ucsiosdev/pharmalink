//
//  SalesOrderNotesViewConrollerViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextView.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@protocol SalesOrderNotesViewConrollerViewControllerDelegate <NSObject>
-(void)SaveNotes:(NSString *)notes;
-(void)NotesCancelled;
@end


@interface SalesOrderNotesViewConrollerViewController : SalesWorxPresentControllerBaseViewController<UITextViewDelegate>
{
    IBOutlet MedRepTextView *notesTextView;
    IBOutlet UIButton *saveButton;
    IBOutlet UIButton *cancelButton;

}
-(IBAction)saveButtonTapped:(id)sender;
-(IBAction)cancelButtonTapped:(id)sender;

@property (strong,nonatomic) NSString *notesText;
@property (nonatomic) BOOL isReadOnly;
@property (nonatomic) BOOL *enableCancelButton;

@property (strong,nonatomic) id <SalesOrderNotesViewConrollerViewControllerDelegate>salesOrderNotesViewConrollerViewControllerDelegate;
@end
