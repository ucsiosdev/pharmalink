//
//  FCWGalleryCollectionDetailViewController.h
//  FalconCityofWonders
//
//  Created by Unique Computer Systems on 9/24/14.
//  Copyright (c) 2014 UCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "SalesWorxCustomClass.h"

@protocol LPOImagesGalleryCollectionDetailViewControllerDelegate <NSObject>


-(void)updateImagesArray:(NSMutableArray*)imagesArray;

@end
@interface LPOImagesGalleryCollectionDetailViewController : UIViewController<SwipeViewDelegate, SwipeViewDataSource,UICollectionViewDelegate>
{
    NSInteger imageIndex;
    IBOutlet UIButton *deleteButton;
    IBOutlet UIPageControl *imagePageControl;
    IBOutlet UIView *contentView;

    IBOutlet UILabel *titlelabel;
}
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
- (IBAction)shareImage:(id)sender ;
@property (nonatomic, strong) IBOutlet SwipeView *swipeView;
@property (nonatomic,strong)NSMutableString* imageURL;
@property (nonatomic,strong) IBOutlet UILabel* imageURLLabel;
@property (nonatomic, strong) IBOutlet UIImageView *swipeItemImage;
@property (nonatomic, strong) NSMutableArray *swipImagesUrls;
@property (nonatomic, strong) NSMutableArray *paymentImageObjectsArray;

@property (nonatomic, strong) NSString *selectedImageIndex;
@property (nonatomic, strong) NSString *isImageFromWebservices;

@property (nonatomic, strong) NSString *strTitle;

@property(nonatomic) BOOL isPaymentImage;

@property (nonatomic, weak) id<LPOImagesGalleryCollectionDetailViewControllerDelegate> delegate;
-(IBAction)DeleteButtonTapped:(id)sender;
-(IBAction)backButtonTapped:(id)sender;

@end
