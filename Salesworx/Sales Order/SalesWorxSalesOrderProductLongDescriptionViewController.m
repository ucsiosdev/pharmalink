//
//  SalesWorxSalesOrderProductLongDescriptionViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/23/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSalesOrderProductLongDescriptionViewController.h"

@interface SalesWorxSalesOrderProductLongDescriptionViewController ()

@end

@implementation SalesWorxSalesOrderProductLongDescriptionViewController
@synthesize Description;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    DescriptionTextView.text=Description.length==0?KNotApplicable:Description;
    
    [DescriptionTextView setEditable:NO];
    DescriptionTextView.textColor=SalesWorxReadOnlyTextViewTextColor;
    DescriptionTextView.backgroundColor=SalesWorxReadOnlyTextViewColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
