//
//  SalesOrderDescriptionLabel2.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderDescriptionLabel2.h"

@implementation SalesOrderDescriptionLabel2

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    self.font=kSWX_FONT_SEMI_BOLD(15);
    self.textColor=[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1];
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(super.text, nil);
    
}
-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
    
    
}


@end
