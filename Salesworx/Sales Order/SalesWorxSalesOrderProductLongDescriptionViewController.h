//
//  SalesWorxSalesOrderProductLongDescriptionViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/23/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextView.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@interface SalesWorxSalesOrderProductLongDescriptionViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet MedRepTextView *DescriptionTextView;
}
@property (strong,nonatomic) NSString *Description;

@end
