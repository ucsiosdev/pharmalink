//
//  SaleWorxSalesOrderVATChargesCalculationsManager.m
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 11/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SaleWorxVATChargesCalculationsManager.h"
#import "SWDatabaseManager.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
@implementation SaleWorxVATChargesCalculationsManager
- (id) init
{
    if (self = [super init]){
        appControl=[AppControl retrieveSingleton];
        VATChargesArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Inventory_Item_ID,Organization_ID,Customer_ID,Site_Use_ID,VAT_Value,VAT_Code from TBL_VAT_Rule Where Inventory_Item_ID IS NOT NULL AND Organization_ID IS NOT NULL AND Customer_ID IS NOT NULL AND Site_Use_ID IS NOT NULL AND VAT_Value  IS NOT NULL"];
    }
    return self;
}

-(SalesOrderLineItemVATCharge *)getVATChargeForProduct:(Products *)product AndCustomer:(Customer *)customer{
    
    SalesOrderLineItemVATCharge *vatChargeObj=[[SalesOrderLineItemVATCharge alloc]init];
    
    NSMutableArray *filteredTempChargesArray=[[NSMutableArray alloc] init];
    if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
        
        if([appControl.VAT_RULE_LEVEL isEqualToString:@"P"]){/** Product Level*/
            
            filteredTempChargesArray = [[VATChargesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Inventory_Item_ID=%ld AND Customer_ID=0 AND Site_Use_ID=0",product.Inventory_Item_ID.integerValue]] mutableCopy];
            if(filteredTempChargesArray.count==0){
                filteredTempChargesArray = [[VATChargesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Inventory_Item_ID=0 AND Customer_ID=%ld AND Site_Use_ID=%ld",customer.Ship_Customer_ID.integerValue,customer.Ship_Site_Use_ID.integerValue]] mutableCopy];
            }
            
        }else if ([appControl.VAT_RULE_LEVEL isEqualToString:@"C"]){/** Customer Level*/
            filteredTempChargesArray = [[VATChargesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Inventory_Item_ID=0 AND Customer_ID=%ld AND Site_Use_ID=%ld",customer.Ship_Customer_ID.integerValue,customer.Ship_Site_Use_ID.integerValue]] mutableCopy];
            if(filteredTempChargesArray.count==0){
                filteredTempChargesArray = [[VATChargesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Inventory_Item_ID=%ld AND Customer_ID=0 AND Site_Use_ID=0",product.Inventory_Item_ID.integerValue]] mutableCopy];
            }
        }
        
        if(filteredTempChargesArray.count==0){
            filteredTempChargesArray = [[VATChargesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Inventory_Item_ID=0 AND Customer_ID=0 AND Site_Use_ID=0"]] mutableCopy];
        }
    }
    if(filteredTempChargesArray.count!=0){
        
        vatChargeObj.VATCharge=[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.2f",[[[filteredTempChargesArray objectAtIndex:0] valueForKey:@"VAT_Value"]floatValue]]];
        vatChargeObj.VATRuleLevel=appControl.VAT_RULE_LEVEL;
        vatChargeObj.VATCode=[[filteredTempChargesArray objectAtIndex:0] valueForKey:@"VAT_Code"];
        vatChargeObj.VATLevel=appControl.VAT_LEVEL;
        return vatChargeObj;
    }
    /**If not found return zero*/
    return nil;
}

@end
