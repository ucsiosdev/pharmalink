//
//  SalesWorxSalesOrderViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/6/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSalesOrderViewController.h"
#import "SalesOrderNotesViewConrollerViewController.h"
#import <MBProgressHUD.h>
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
#import <AVFoundation/AVFoundation.h>
#import "SalesWorxAssortmentBonusManager.h"
#import "SalesWorxAssortmentBonusInfoViewController.h"
#import "SalesWorxSalesOrderItemDuplicateOHViewController.h"
#import "SaleWorxVATChargesCalculationsManager.h"

@interface SalesWorxSalesOrderViewController ()
{
    SaleWorxVATChargesCalculationsManager * VATChargesManager;
}
@end

@implementation SalesWorxSalesOrderViewController
@synthesize currentVisit,filterPopOverController;
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    proSearchType=ProductSerachNormal;
    AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
    VATChargesManager=[[SaleWorxVATChargesCalculationsManager alloc] init];
    /** this bools for to show alert messages for templates and confirm orders do not use for any other ptrpose*/
     showOneOrMoreProductPriceNotAvailable=NO;
     showOneOrMoreProductsNotAvailable=NO;
     showOneOrMoreManualFocProductsNotAvailable=NO;
     showOneOrMoreBonusProductsNotAvailable=NO;
     showOneOrMoreProductsDiscountChanged=NO;
     showOneOrMoreProductsBonusQtyChanged=NO;
    showOneOrMoreProductsBonusProductChanged=NO;
    showOneOrMoreProductsLotsareDiscarded=NO;
    /*end*/

    
    //self.title= @"";
    productsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    filteredProductsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    [self hideNoSelectionView];
    NoSelectionMessageLabel.text=KNOProductSelectedLabel;
    selectedProductIndexPathArray=[[NSMutableArray alloc]init];
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    
    
    selectedProductIndexpath=nil;
    // longDescriptionButton.hidden=YES;
    
    //fetching array of products query has been modified by harpal on 24.01.2016 as per aki requirement to add product description and sorting products based on ranking
    
    
    enableProductRank= [NSString stringWithFormat:@"%@",appControl.ENABLE_PROD_RANK];
    showProductLongDescription=[NSString stringWithFormat:@"%@",[[[SWDefaults appControl] objectAtIndex:0]valueForKey:@"SHOW_PROD_LONG_DESC"]];
    
    NSLog(@"test string flags %@ %@", enableProductRank,showProductLongDescription);
    [self clearAllProductLabelsAndTextFields];
    appControl = [AppControl retrieveSingleton];
    
    isUpdatingOrder=NO;
    allowSearchBarEditing=YES;
    [self updateAddToOrderButtonTitle];
    orderLineItemsArray=[[NSMutableArray alloc]init];
    OrdersTableView.allowsMultipleSelectionDuringEditing = NO;
    [self AddActivityIndicatorInProductSearchBar];
    
    
}
-(void)AddActivityIndicatorInProductSearchBar
{
    searchBarActivitySpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //set frame for activity indicator
    [searchBarActivitySpinner setFrame:CGRectMake(100, 200, 100, 100)];
    [productsTableView addSubview: searchBarActivitySpinner];
}
-(void)showActivityIndicatorInSearchBar
{
    [productsTableView setUserInteractionEnabled:NO];
    [productsFilterButton setUserInteractionEnabled:NO];
    [searchBarActivitySpinner startAnimating];
}
-(void)hideActivityIndicatorInSearchBar
{
    [productsTableView setUserInteractionEnabled:YES];
    [productsFilterButton setUserInteractionEnabled:YES];
    [searchBarActivitySpinner stopAnimating];
}
-(void)updateAddToOrderButtonTitle
{
    if(isUpdatingOrder)
    {
        [addToProductButton setTitle:KSalesOrderProductUpdateButtonTitle forState:UIControlStateNormal];
    }
    else
    {
        [addToProductButton setTitle:KSalesOrderProductAddButtonTitle forState:UIControlStateNormal];

    }
}

-(void)viewWillAppear:(BOOL)animated
{
    productsTableView.estimatedRowHeight=70;
    productsTableView.rowHeight=UITableViewAutomaticDimension;
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesSalesOrderScreenName];
    }

    methodStart=[NSDate date];
    
    showOrderTableViewNewItemAnimationColor=NO;
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;

    
    
    NSMutableAttributedString* salesOrderattributeString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Sales Order", nil),[NSString stringWithFormat:@"[ Type: %@, Category: %@, W/H: %@ ]",[[currentVisit.visitOptions.salesOrder.selectedOrderType stringByReplacingOccurrencesOfString:@"Order" withString:@""] trimString],currentVisit.visitOptions.salesOrder.selectedCategory.Category,currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID]]];
 
    [salesOrderattributeString addAttribute:NSForegroundColorAttributeName
                                value:[UIColor whiteColor]
                                range:NSMakeRange(0, salesOrderattributeString.length)];
    
    [salesOrderattributeString addAttribute:NSFontAttributeName
                                value:headerTitleFont
                                range:NSMakeRange(0, salesOrderattributeString.length)];
    NSMutableAttributedString* orderDetailsAttributedString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"[ Type: %@, Category: %@, W/H: %@ ]",[currentVisit.visitOptions.salesOrder.selectedOrderType stringByReplacingOccurrencesOfString:@"Order" withString:@""],currentVisit.visitOptions.salesOrder.selectedCategory.Category,currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID]];
    [orderDetailsAttributedString addAttribute:NSForegroundColorAttributeName
                                  value:[UIColor whiteColor]
                                  range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    [orderDetailsAttributedString addAttribute:NSFontAttributeName
                                  value:KOpenSans16
                                  range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    
//    [salesOrderattributeString appendAttributedString:orderDetailsAttributedString];
    
    label.attributedText=salesOrderattributeString;
    self.navigationItem.titleView = label;
    
    
    
    
    
    saveBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    saveBarButtonItem.tintColor=[UIColor whiteColor];
    [saveBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=saveBarButtonItem;
    closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeSalesOrderTapped)];
    closeVisitButton.tintColor=[UIColor whiteColor];
    [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=closeVisitButton;

    
    if(self.isMovingToParentViewController)
    {
        if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
        {
           // [self UpdateUIFiledsForFOCOrder];
            [OrderQuantityStringLabel setText:@"Req. Bonus:"];

        }
        
        if([appControl.SHOW_SALES_HISTORY isEqualToString:KAppControlsNOCode])
        {
            [productSalesHistoryInfoButton setHidden:YES];
            productSalesHistoryInfoButtonHeightContraint.constant=0;
            productSalesHistoryInfoButtonWidthContraint.constant=0;
            productSalesHistoryInfoButtontarilingMarginContraint.constant=0;
        }
    
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [saveBarButtonItem setEnabled:NO];
        
        if ([enableProductRank isEqualToString:KAppControlsYESCode] || [showProductLongDescription isEqualToString:KAppControlsYESCode]) {
        }else{
            productDescriptionInfoButton.hidden=YES;
           
        }
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        if ([enableProductRank isEqualToString:KAppControlsYESCode] || [showProductLongDescription isEqualToString:KAppControlsYESCode]) {
            productsArray=[[[SWDatabaseManager retrieveManager]fetchProductsforCategoryWithRanking:currentVisit.visitOptions.salesOrder.selectedCategory AndCustomer:currentVisit.visitOptions.customer andTransactionType:kSalesOrderTransaction] mutableCopy];
        }else{
            productsArray=[[[SWDatabaseManager retrieveManager]fetchProductsforCategory:currentVisit.visitOptions.salesOrder.selectedCategory AndCustomer:currentVisit.visitOptions.customer andTransactionType:kSalesOrderTransaction] mutableCopy];
        }
        
            if(!currentVisit.visitOptions.customer.isBonusAvailable){
                [productsArray setValue:@"0" forKey:@"bonus"];
             }
        
            currentVisit.visitOptions.salesOrder.AssortmentPlansArray=[[NSMutableArray alloc]init];
            if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] && currentVisit.visitOptions.customer.isBonusAvailable){
                SalesWorxAssortmentBonusManager *manager=[[SalesWorxAssortmentBonusManager alloc]init];
                currentVisit.visitOptions.salesOrder.AssortmentPlansArray=[manager FetchAssortmentBonusPlansForCategoryProducts:productsArray ForCustomer:currentVisit.visitOptions.customer];
            }

            
            if ([appControl.FILTER_FOC_LIST isEqualToString:KAppControlsYESCode]) {
                NSPredicate * focRestrictionPredicate=[NSPredicate predicateWithFormat:@"SELF.REMOVE_IN_FOC_LIST != 'Y'"];
                manualFocProductsArray=[[productsArray filteredArrayUsingPredicate:focRestrictionPredicate] mutableCopy] ;
            }
            else
            {
                manualFocProductsArray=productsArray;
            }
            filteredProductsArray=productsArray;
            
            
            productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsArray valueForKeyPath:@"@distinctUnionOfObjects.Brand_Code"]];
            productsBrandCodesArray= [[NSMutableArray alloc]initWithArray:[productsBrandCodesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
            
            
        /** removing invalid brandcodes*/
       // [productsBrandCodesArray removeObject:@""];
        /*filtering products by brand code*/
        for (NSString *productBrandCode in productsBrandCodesArray) {
//            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
//                                           @"Brand_Code=%@",productBrandCode];
//            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:brandCodePredicate]];
            [productsArrayGroupedByBrandCode addObject:[[NSMutableArray alloc]init]];
            
        }
            
        filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [saveBarButtonItem setEnabled:YES];
            
            
            
            if(productsBrandCodesArray.count==1)
            {
                [self ExpandFirstProductsSection];
            }
            else
            {
                ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
            }
            
            
            [productsTableView reloadData];
            [self hideProductDetailsView];
            [self hideProductOrderDetailsView];
            [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
            [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];
            [self showCustomerDetails];
            [self populateOrderLineItems];
            
            NSDate * methodFinish=[NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime in sales order = %f", executionTime);
        });
    });


    
    if ([appControl.ENABLE_ITEM_DISCOUNT isEqualToString:KAppControlsNOCode]) {
        [discountTextField setIsReadOnly:YES];
    }

    if ([appControl.ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsNOCode]) {
   
        productSpecialDiscountLabel.text=@"N/A";
    }
    
    
    if([appControl.ALLOW_BONUS_CHANGE isEqualToString:KAppControlsNOCode])
    {
        requestedBonusTextField.userInteractionEnabled=NO;
    }
    else
    {
        requestedBonusTextField.userInteractionEnabled=YES;
    }
    
        if([appControl.ENABLE_MANUAL_FOC isEqualToString:KAppControlsNOCode])
        {
            /*
            [FOCQtyTextField setHidden:YES];
            [FocQtyStringLabel setHidden:YES];
            [manualFOCTextField setHidden:YES];
            [manualFocStringLabel setHidden:YES];
             */
        }
        else
        {
            [FOCQtyTextField setHidden:NO];
            [FocQtyStringLabel setHidden:NO];
            [manualFOCTextField setHidden:NO];
            [manualFocStringLabel setHidden:NO];
            if([appControl.ALLOW_MANUAL_FOC_ITEM_CHANGE isEqualToString:KAppControlsYESCode])
            {
                [manualFOCTextField setIsReadOnly:NO];
                
            }
            else
            {
                [manualFOCTextField setDropdownImageView:nil];
                [manualFOCTextField setIsReadOnly:YES];
            }

        }
    
        [UOMTextField setIsReadOnly:YES];
    
    }

    OrdersTableView.delegate=self;
    OrdersTableView.dataSource=self;

    [self updateSalesOrderTotalAmount];
    
    
    
    if(self.isMovingToParentViewController){
        if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode]){
            xSearchBarTrailingspaceToFilterButton.constant=47;
            [barcodeScanButton setHidden:NO];
        }
        else{
            xSearchBarTrailingspaceToFilterButton.constant=8;
            [barcodeScanButton setHidden:YES];
        }
    }


 }

-(void)closeSalesOrderTapped
{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        
        NSString *alertTitle;
        NSString *alertMessage;
      if (currentVisit.visitOptions.salesOrder.isManagedOrder==YES) {
            if([currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KConfirmOrderStatusString])
            {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
            else if([currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KUnConfirmOrderStatusString])
            {
                if(isManageSalesOrderUpdated)
                {
                    alertTitle=@"Close Order";
                    alertMessage=@"Would you like to close the order? Unsaved changes will be discarded.";
                }
                else{
                    [self.navigationController popViewControllerAnimated:YES];
                    return;
                }

            }
        }
        else
        {
            alertTitle=@"Cancel order";
            alertMessage=@"Would you like to cancel the order?";

        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesAction = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self.navigationController popViewControllerAnimated:YES];
                                 
                             }];
        [alert addAction:yesAction];
        UIAlertAction* noAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        
                                    }];
        [alert addAction:noAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];

}


-(void)saveButtonTapped
{
    
    if(orderLineItemsArray.count>0)
    {
        for (SalesOrderLineItem *selectedOrder in orderLineItemsArray) {
            if ([selectedOrder.OrderProductQty doubleValue] == 0) {
                [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Order items quanity should be greater than zero" withController:nil];
                return;
            }
        }
        
        //NSArray* reversedArray = [[orderLineItemsArray reverseObjectEnumerator] allObjects];

       // orderLineItemsArray=[reversedArray mutableCopy];
        
        NSNumber* orderedItemsAmountValue = [orderLineItemsArray valueForKeyPath: @"@sum.OrderItemNetamount"];
        
        double remainingBalance=[currentVisit.visitOptions.customer.Avail_Bal doubleValue]-[orderedItemsAmountValue doubleValue];
        
        if(![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle] && [orderedItemsAmountValue doubleValue]==0)
        {
            [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Total order amount should be greater than zero" withController:nil];
        }
        else if([appControl.OVER_LIMIT isEqualToString:KAppControlsYESCode] ||
           ([appControl.OVER_LIMIT isEqualToString:KAppControlsNOCode] && remainingBalance>=0))
        {
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                
                BOOL isBonusPending = NO;
                int IndexOfPendingBonusItem = 0;
                for (int i = 0; i<orderLineItemsArray.count; i++) {
                    IndexOfPendingBonusItem = i;
                    SalesOrderLineItem *currentOrderItem = orderLineItemsArray[i];
                    if (currentOrderItem.bonusProduct == nil && currentOrderItem.bonusDetailsArray.count > 0 && currentOrderItem.proximityNotes.length == 0) {
                        isBonusPending = YES;
                        break;
                    }
                }
                
                if (isBonusPending) {
                    SalesOrderLineItem *currentOrderItem = orderLineItemsArray[IndexOfPendingBonusItem];
                    
                    SalesOrderBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesOrderBonusInfoViewController alloc]initWithNibName:@"SalesOrderBonusInfoViewController" bundle:[NSBundle mainBundle]];
                    salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
                    salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
                    [self getBonusDetails];
                    salesOrderBonusInfoViewController.bonusDelegate = self;
                    salesOrderBonusInfoViewController.isOrderQuantityRecommendationMandatory = YES;
                    salesOrderBonusInfoViewController.selectedOrderLineItemIndexForBonusRecommendation = IndexOfPendingBonusItem;
                    salesOrderBonusInfoViewController.productsArray=productsArray;
                    salesOrderBonusInfoViewController.OrderLineItem=currentOrderItem;
                    salesOrderBonusInfoViewController.bonusArray= currentOrderItem.bonusDetailsArray;
                    [self.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
                } else {
                    SalesworxSalesOrderConfirmationViewController *salesworxSalesOrderConfirmationViewController=[[SalesworxSalesOrderConfirmationViewController alloc]initWithNibName:@"SalesworxSalesOrderConfirmationViewController" bundle:[NSBundle mainBundle]];
                    //reversing order line items
                    NSArray* reversedArray = [[orderLineItemsArray reverseObjectEnumerator] allObjects];
                    currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[reversedArray mutableCopy];
                    currentVisit.visitOptions.salesOrder.AssortmentBonusProductsArray=AssortmentBonusProductsArray;
                    salesworxSalesOrderConfirmationViewController.currentVisit=currentVisit;
                    [self.navigationController pushViewController:salesworxSalesOrderConfirmationViewController animated:YES];
                }
            } else {
                SalesworxSalesOrderConfirmationViewController *salesworxSalesOrderConfirmationViewController=[[SalesworxSalesOrderConfirmationViewController alloc]initWithNibName:@"SalesworxSalesOrderConfirmationViewController" bundle:[NSBundle mainBundle]];
                //reversing order line items
                NSArray* reversedArray = [[orderLineItemsArray reverseObjectEnumerator] allObjects];
                currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[reversedArray mutableCopy];
                currentVisit.visitOptions.salesOrder.AssortmentBonusProductsArray=AssortmentBonusProductsArray;
                salesworxSalesOrderConfirmationViewController.currentVisit=currentVisit;
                [self.navigationController pushViewController:salesworxSalesOrderConfirmationViewController animated:YES];
            }
        }
        else
        {
            [self showAlertAfterHidingKeyBoard:@"Over Limit" andMessage:@"Available balance is less than the ordered amount" withController:nil];
            return;
        }

    }
    else
    {

        [self showAlertAfterHidingKeyBoard:@"No products" andMessage:@"Please add at least one product to proceed" withController:nil];
    }
    
 }
-(void)viewDidAppear:(BOOL)animated
{
    
        if(self.isMovingToParentViewController)
        {

                if([appControl.HIDE_DISCOUNT_FIELDS_AND_LABELS isEqualToString:KAppControlsYESCode])
                {
                    /** product details View*/
                    [defaultDiscountStringLabel removeFromSuperview];
                    [specialDiscountStringLabel removeFromSuperview];
                    [productDefaultDiscountLabel removeFromSuperview];
                    [productSpecialDiscountLabel removeFromSuperview];
                    
                    [productDetailsView removeConstraint:xDefaultBonusValueLabelTrailingMarginConstraint];
                    [bonusItemStringLabel removeConstraint:xBonusItemStringLabelLeadingMarginConstraint];
                    xBonusItemStringLabelLeadingMarginToViewConstraint.constant=8;
                    xDefaultBonusStringLabelLeadinggMarginConstraint.constant=8;
                    
                    NSLayoutConstraint *leading = [NSLayoutConstraint
                                                   constraintWithItem:defaultBonusItemStringLabel
                                                   attribute:NSLayoutAttributeLeading
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:productDetailsView
                                                   attribute:NSLayoutAttributeLeading
                                                   multiplier:1.0f
                                                   constant:100.f];
                    
                    [productDetailsView addConstraint:leading];

                    DCQtyLabel.hidden = YES;
                    productDCQtyLabel.hidden = YES;
                    [bonusItemStringLabel updateConstraintsIfNeeded];
                    [productDefaultBonusLabel updateConstraintsIfNeeded];
                    [productDetailsView updateConstraintsIfNeeded];
                    
                    
                    
                    /** order details View*/
                    if(![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
                    {
                        [discountTextField removeFromSuperview];
                        [DiscountStringLabel removeFromSuperview];
                        
                        xRequestedBonusTextfieldLeadingMargintoSellingPriceTextfield.constant=8;
                        [requestedBonusTextField updateConstraintsIfNeeded];
                        [productOrderDetailsView updateConstraintsIfNeeded];
                        [OrderTextfieldsClearButton updateConstraintsIfNeeded];

                    }

                        /***AmountLabels*/
                        [OrderItemDiscountAmountLabel setHidden:YES];
                        [OrderItemNetAmountLabel setHidden:YES];
                        [OrderProductNetAmountLabel setHidden:YES];
                        [OrderProductDiscountStringLabel setHidden:YES];
                        [ordersTotalAmountLbl removeConstraint:xOrderProductTotalAmountLabelTrailingMarginToDiscountLabelConstraint];
                        xOrderProductTotalAmountLabelTrailingMarginToOrderDetailsViewConstraint.constant=8;
                        [ordersTotalAmountLbl updateConstraintsIfNeeded];
                        [productOrderDetailsView updateConstraintsIfNeeded];

                    
                    /** orderTable*/
                    xOrderTableHeaderDiscountLabelWidthConstraint.constant=0;
                    xOrderTableHeaderDiscountLabelLeadingMarginConstraint.constant=0;
                    /** Hiding VAT Columns*/
                    if(![appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                        xOrderTableHeaderNetAmountLabelLeadingMarginConstraint.constant=0;
                        xOrderTableHeaderNetAmountLabelWidthConstraint.constant=0;
                        xOrderTableHeaderVATChargeLabelLeadingMarginConstraint.constant=0;
                        xOrderTableHeaderVATChargeLabelWidthConstraint.constant=0;
                        OrderTableHeaderTotalLabel.textAlignment = NSTextAlignmentRight;
                    }
                }
                else
                {
//                    [productDetailsView removeConstraint:xDefaultBonusStringLabelLeadinggMarginConstraint];
//                    [productDetailsView removeConstraint:xBonusItemStringLabelLeadingMarginToViewConstraint];
                    
                    [bonusItemStringLabel updateConstraintsIfNeeded];
                    [defaultBonusItemStringLabel updateConstraintsIfNeeded];
                    [defaultBonusItemStringLabel layoutIfNeeded];
                    [bonusItemStringLabel layoutIfNeeded];
                    [productDetailsView layoutIfNeeded];
                    
                    DCQtyLabel.hidden = NO;
                    productDCQtyLabel.hidden = NO;
                    
                    /** order details View*/
                    [requestedBonusTextField removeConstraint:xRequestedBonusTextfieldLeadingMargintoSellingPriceTextfield];
                    [requestedBonusTextField updateConstraintsIfNeeded];
                        /***AmountLabels*/
                        [productOrderDetailsView removeConstraint:xOrderProductTotalAmountLabelTrailingMarginToOrderDetailsViewConstraint];
                        [ordersTotalAmountLbl updateConstraintsIfNeeded];
                    [productOrderDetailsView updateConstraintsIfNeeded];
                    /** Hiding VAT Columns*/
                    if(![appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                        xOrderTableHeaderVATChargeLabelLeadingMarginConstraint.constant=0;
                        xOrderTableHeaderVATChargeLabelWidthConstraint.constant=0;
                    }
                }
            
            
        }

}
-(void)populateOrderLineItems
{
        if (currentVisit.visitOptions.salesOrder.isTemplateOrder==YES || [currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kRecommendedOrderPopOverTitle] || [currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kDCReOrderPopOverTitle]) {
            NSMutableArray *tempOrdrerLineItemsArray=currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray;
            OrdersTableView.userInteractionEnabled=NO;
            productsTableView.userInteractionEnabled=NO;
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                for (NSInteger i=0; i<tempOrdrerLineItemsArray.count; i++) {
                    selectedOrderLineItem=[[SalesOrderLineItem alloc]init];
                    SalesOrderLineItem *templineItem=[tempOrdrerLineItemsArray objectAtIndex:i];
                    
                    
                    NSPredicate *itemCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",templineItem.OrderProduct.Item_Code];
                    NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:itemCodeProductPredicate]];
                    
                    /* if Product Exist*/
                    if(tempProductsArray.count>0)
                    {
                        Products *pimaryUomProduct =[tempProductsArray objectAtIndex:0];
                        NSMutableArray *UomsArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:pimaryUomProduct];
                        pimaryUomProduct.ProductUOMArray=UomsArray;
                        NSPredicate *selectedUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",templineItem.OrderProduct.selectedUOM];
                        NSMutableArray *selectedUOMfiltredArray=[[UomsArray filteredArrayUsingPredicate:selectedUomPredicate] mutableCopy];

                            /** if UOM Exist*/
                            if(selectedUOMfiltredArray.count>0)
                            {
                                
                                Products *selectedUOMProduct=[self getUpdatedProductDetails:pimaryUomProduct ToSelectedUOM:[selectedUOMfiltredArray objectAtIndex:0]];
                                PriceList *priceList=[self fetchProductPriceListData:selectedUOMProduct];
                                if(priceList!=nil && [priceList.Unit_Selling_Price doubleValue]>0)
                                {
                                    selectedOrderLineItem.OrderProduct=selectedUOMProduct;
                                    selectedOrderLineItem.VATChargeObj=[VATChargesManager getVATChargeForProduct:selectedOrderLineItem.OrderProduct AndCustomer:currentVisit.visitOptions.customer];
                                    selectedOrderLineItem.OrderProductQty=templineItem.OrderProductQty;
                                    selectedOrderLineItem.OrderProduct.productPriceList=(PriceList*)priceList;
                                    selectedOrderLineItem.manualFocProduct=[self isTheProductRestrictedForManualFOC:selectedOrderLineItem.OrderProduct]?nil:selectedOrderLineItem.OrderProduct;
                                    selectedOrderLineItem.manualFOCQty=0.0;
                                    selectedOrderLineItem.productSelleingPrice=[selectedOrderLineItem.OrderProduct.productPriceList.Unit_Selling_Price doubleValue] ;
                                    
                                    if ([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
                                    {
                                        selectedOrderLineItem.productSelleingPrice=0.0 ;
                                    }
                                    /* first have to get special dicount and bonus details to caluclate bonus and discount*/
                                    [self getBonusDetails];
                                    [self getProductSpecailDisount];
                                    [self calculateBonus];
                                    [self calculateDiscount];
                                    
                                    selectedOrderLineItem.NotesStr=templineItem.NotesStr;
                                    [orderLineItemsArray addObject:selectedOrderLineItem];
                                }
                            }
                       }
                 }
                
                /** Assigning default assortment bonus*/
                if(orderLineItemsArray.count>0 &&
                   [appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
                   ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
                    /** creating temp salesorer */
                    SalesOrder *tempOrder=[[SalesOrder alloc]init];
                    tempOrder.SalesOrderLineItemsArray=orderLineItemsArray;
                    tempOrder.AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
                    tempOrder.AssortmentPlansArray=currentVisit.visitOptions.salesOrder.AssortmentPlansArray;
                    /*8 updating sales order details with default assortment bonus Qunatities*/
                    SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
                    tempOrder= [bnsManager AssignDefaultBonusPlansForSalesOrder:tempOrder];
                    orderLineItemsArray=tempOrder.SalesOrderLineItemsArray;
                    AssortmentBonusProductsArray=tempOrder.AssortmentBonusProductsArray;
                    
                }

                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self SortOrderLineItems];
                    OrdersTableView.userInteractionEnabled=YES;
                    productsTableView.userInteractionEnabled=YES;
                    [self DeselectTableViewsAndUpdate];
                    
                });
            });
            
            
        }
        else if (currentVisit.visitOptions.salesOrder.isManagedOrder==YES) {
            if([currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KConfirmOrderStatusString])
            {
                [self fetchConfirmOrderDetails];
            }
            else if([currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KUnConfirmOrderStatusString])
            {
                [self fetchSavedOrderDetails];
                
            }
        }
        else if ([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kReOrderPopOverTitle]) {
            [self fetchSavedOrderDetails];

        }
        if ([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]) {
            requestedBonusTextField.isReadOnly=YES;
            manualFOCTextField.isReadOnly=YES;
            FOCQtyTextField.isReadOnly=YES;
            productBonusItemLabel.text=KNotApplicable;
            productDefaultBonusLabel.text=KNotApplicable;
            sellingPriceTextField.text=[@"0" floatString];
            [discountTextField setIsReadOnly:YES];
            [sellingPriceTextField setIsReadOnly:YES];
        }

}
-(void)SortOrderLineItems{
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
       ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
        NSSortDescriptor *PlandIdSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"AppliedAssortMentPlanID"
                                                                              ascending:YES];
        NSSortDescriptor *showApliedAssortMentPlanBonusProductsSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"showApliedAssortMentPlanBonusProducts"
                                                                                                            ascending:YES];
        NSArray *sortDescriptors = @[PlandIdSortDescriptor,showApliedAssortMentPlanBonusProductsSortDescriptor];
        NSArray *sortedArray = [orderLineItemsArray sortedArrayUsingDescriptors:sortDescriptors];
        orderLineItemsArray=[[NSMutableArray alloc]initWithArray:sortedArray];
        

        currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=orderLineItemsArray;
    }
}
-(void)fetchConfirmOrderDetails
{
    OrdersTableView.userInteractionEnabled=NO;
    productsTableView.userInteractionEnabled=NO;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){

        NSMutableArray *tempOrdrerLineItemsArray=[[NSMutableArray alloc]initWithArray:[[SWDatabaseManager retrieveManager] dbGetSalesConfirmOrderItems:currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref]] ;
        
        /** fetching the main products array*/
        BOOL isFOCConfirmedOrder=[currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle];
        NSPredicate *mainProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@",isFOCConfirmedOrder?@"F":@"N"];/** for foc orders products are saving with calc flag as "F" in tbl_order*/
        NSMutableArray * mainProductsArray=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:mainProductsPredicate]];
        
        for (NSInteger i=0; i<mainProductsArray.count; i++) {
            /** fetching the main product Product object from category products Array*/
            NSPredicate *mainProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[[mainProductsArray objectAtIndex:i] valueForKey:@"Item_Code"]];
            NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:mainProductPredicate]];
            
            
                
            if(tempProductsArray.count>0 ) /**this product exist in the category*/
            {
                Products *tempProduct=[tempProductsArray objectAtIndex:0];
                
                tempProduct=[self FetchProductDetailsFromPrimaryUOMProduct:tempProduct ForUOM:[[mainProductsArray objectAtIndex:i] valueForKey:@"Order_Quantity_UOM"]];
                if(tempProduct!=nil)
                {
                
                selectedOrderLineItem=[[SalesOrderLineItem alloc]init];
                selectedOrderLineItem.OrderProduct=tempProduct;
                    
                    /** Order line Item VAT Charge*/
                if(![NSString isEmpty:[[mainProductsArray objectAtIndex:i] valueForKey:@"VAT_Level"]]){
                    SalesOrderLineItemVATCharge *vatChargeObj=[[SalesOrderLineItemVATCharge alloc]init];
                    vatChargeObj.VATCharge=[NSDecimalNumber ValidDecimalNumberWithString:[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"VAT_Percentage"]]];
                    vatChargeObj.VATLevel=[[mainProductsArray objectAtIndex:i] valueForKey:@"VAT_Level"];
                    vatChargeObj.VATCode=[[mainProductsArray objectAtIndex:i] valueForKey:@"VAT_Code"];
                    vatChargeObj.VATRuleLevel=appControl.VAT_RULE_LEVEL;
                    selectedOrderLineItem.VATChargeObj=vatChargeObj;
                }else{
                    selectedOrderLineItem.VATChargeObj=nil;
                }

                    
                    
                selectedOrderLineItem.OrderProductQty=[NSDecimalNumber ValidDecimalNumberWithString:[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Ordered_Quantity"]]];
                PriceList *priceList=[self fetchProductPriceListData:selectedOrderLineItem.OrderProduct];
                selectedOrderLineItem.OrderProduct.productPriceList=(PriceList*)priceList;
                selectedOrderLineItem.productSelleingPrice=[[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Unit_Selling_Price"]] doubleValue] ;
                /** set the sellingprice to zero for foc order*/
                if ([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
                {
                    selectedOrderLineItem.productSelleingPrice=0.0 ;
                }
                selectedOrderLineItem.manualFOCQty=0;
                /** fetching the manual products array*/
                NSPredicate *manualFocProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@ AND Custom_Attribute_1==[cd] %@",@"M",[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_1"]] ];
                NSMutableArray * filteredFocProductsArray=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:manualFocProductsPredicate]];
                
                if(filteredFocProductsArray.count>0 && !isFOCConfirmedOrder)
                {
                    NSPredicate *manualFocProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[SWDefaults getValidStringValue:[[filteredFocProductsArray objectAtIndex:0] valueForKey:@"Item_Code"]]];
                    NSMutableArray * tempFOCProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:manualFocProductPredicate]];
                    if(tempFOCProductsArray.count>0)
                    {
                        Products *tempFOCProduct=[tempFOCProductsArray objectAtIndex:0];
                        
                        tempFOCProduct=[self FetchProductDetailsFromPrimaryUOMProduct:tempFOCProduct ForUOM:[[mainProductsArray objectAtIndex:i] valueForKey:@"Order_Quantity_UOM"]];
                        if(tempFOCProduct!=nil)
                        {
                            selectedOrderLineItem.manualFocProduct=tempFOCProduct;
                            selectedOrderLineItem.manualFOCQty=[[SWDefaults getValidStringValue:[[filteredFocProductsArray objectAtIndex:0] valueForKey:@"Ordered_Quantity"]] doubleValue];
                        }
                        
                     }
                    else
                    {
                        selectedOrderLineItem.manualFocProduct=selectedOrderLineItem.OrderProduct;
                        selectedOrderLineItem.manualFOCQty=0;
                    }
                    
                }
                else
                {
                    selectedOrderLineItem.manualFocProduct=selectedOrderLineItem.OrderProduct;
                    selectedOrderLineItem.manualFOCQty=0;
                }
                selectedOrderLineItem.requestedBonusQty=0;
                [self getBonusDetails];
                [self getProductSpecailDisount];
                /** fetching the manual bonus array*/
                NSPredicate *BonusProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@ AND Custom_Attribute_1==[cd] %@ AND Assortment_Plan_ID==NULL",@"F",[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_1"]]];
                NSMutableArray * filteredBonusProductsArray=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:BonusProductsPredicate]];
                
                if(filteredBonusProductsArray.count>0 && !isFOCConfirmedOrder)
                {
                    NSPredicate *BonusProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[SWDefaults getValidStringValue:[[filteredBonusProductsArray objectAtIndex:0] valueForKey:@"Item_Code"]]];
                    NSMutableArray * tempBonusProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:BonusProductPredicate]];
                    if(tempBonusProductsArray.count>0)
                    {
                        Products *tempBonusProduct=[tempBonusProductsArray objectAtIndex:0];
                        
                        tempBonusProduct=[self FetchProductDetailsFromPrimaryUOMProduct:tempBonusProduct ForUOM:[[mainProductsArray objectAtIndex:i] valueForKey:@"Order_Quantity_UOM"]];
                        if(tempBonusProduct!=nil)
                        {
                            selectedOrderLineItem.bonusProduct=tempBonusProduct;
                            [self calculateBonus];
                            selectedOrderLineItem.requestedBonusQty=[[SWDefaults getValidStringValue:[[filteredBonusProductsArray objectAtIndex:0] valueForKey:@"Ordered_Quantity"]] doubleValue];
                        }
                    }
                    
                }
                else
                {
                    selectedOrderLineItem.bonusProduct=nil;
                    selectedOrderLineItem.requestedBonusQty=0;
                }
                /** calculating the orderlineitem amounts*/
                selectedOrderLineItem.appliedDiscount=[[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_3"]] doubleValue]*100;
                [self calculateItemAmounts];
                /* first have to get special dicount and bonus details to caluclate bonus and discount*/
                selectedOrderLineItem.NotesStr=[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_2"]];
                
                /** lots Assign*/
                 NSArray *lotsArray=[[SWDatabaseManager retrieveManager]dbGetAssignedLotsForOrderId:currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref AndLineNumber:[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Line_Number"]]];
                NSMutableArray *productAllocatedLotsarray=[[NSMutableArray alloc]init];
                if(lotsArray.count>0)/* lots assigned*/
                {
                    NSMutableArray * stockLotsarray=  [self fetchStockDataforItemID:selectedOrderLineItem.OrderProduct];
                    for (NSInteger j=0; j<lotsArray.count; j++) {
                        NSPredicate * lotNumberPredicate=[NSPredicate predicateWithFormat:@"SELF.Lot_No=%@",[SWDefaults getValidStringValue:[[lotsArray objectAtIndex:j] valueForKey:@"Lot_Number"]]];
                        NSArray *filteredlotsArray=  [stockLotsarray filteredArrayUsingPredicate:lotNumberPredicate];
                        if(filteredlotsArray.count>0)/** if lot exist*/
                        {
                            Stock *lot=[filteredlotsArray objectAtIndex:0];
                            SalesOrderAssignedLot *asignedLot=[[SalesOrderAssignedLot alloc]init];
                            asignedLot.lot=lot;
                            asignedLot.assignedQuantity=[NSDecimalNumber decimalNumberWithString:[SWDefaults getValidStringValue:[[lotsArray objectAtIndex:j] valueForKey:@"Ordered_Quantity"]]];
                            [productAllocatedLotsarray addObject:asignedLot];
                        }

                    }
                }
                selectedOrderLineItem.AssignedLotsArray=productAllocatedLotsarray;
                [orderLineItemsArray addObject:selectedOrderLineItem];
            }
            }
            currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=orderLineItemsArray;
        }
        
        /** Assigning Assortment bonus*/
        if(orderLineItemsArray.count>0 &&
           [appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
           ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
            /** fetching the Assort bonus Array*/
            NSPredicate *AsstBonusProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@ AND Assortment_Plan_ID!=NULL",@"F"];
            NSMutableArray * filteredAsstBonusProducts=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:AsstBonusProductsPredicate]];
            
            
            /** creating temp salesorer */
            SalesOrder *tempOrder=[[SalesOrder alloc]init];
            tempOrder.SalesOrderLineItemsArray=orderLineItemsArray;
            tempOrder.AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
            tempOrder.AssortmentPlansArray=currentVisit.visitOptions.salesOrder.AssortmentPlansArray;
            /* updating sales order details with default assortment bonus Qunatities*/
            SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
            tempOrder= [bnsManager UpdateConfirmesSalesOrderLineItems:tempOrder WithDBAssortmentBonusProductsArray:filteredAsstBonusProducts AndProductsArray:productsArray];
            orderLineItemsArray=tempOrder.SalesOrderLineItemsArray;
            AssortmentBonusProductsArray=tempOrder.AssortmentBonusProductsArray;
            
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            /** disabling the uifields*/
            [self SortOrderLineItems];
            OrdersTableView.userInteractionEnabled=YES;
            productsTableView.userInteractionEnabled=YES;
            [self updateSalesOrderTotalAmount];
            [self DeselectTableViewsAndUpdate];
            self.navigationItem.rightBarButtonItems=nil;
        });
    });
    
}

-(Products *)FetchProductDetailsFromPrimaryUOMProduct:(Products *)pimaryUomProduct ForUOM:(NSString *)DestUOM
{
    NSMutableArray *UomsArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:pimaryUomProduct];
    pimaryUomProduct.ProductUOMArray=UomsArray;
    NSPredicate *selectedUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",DestUOM];
    NSMutableArray *selectedUOMfiltredArray=[[UomsArray filteredArrayUsingPredicate:selectedUomPredicate] mutableCopy];
    
    /** if UOM Exist*/
    if(selectedUOMfiltredArray.count>0)
    {
        
        Products *selectedUOMProduct=[self getUpdatedProductDetails:pimaryUomProduct ToSelectedUOM:[selectedUOMfiltredArray objectAtIndex:0]];
        
        return selectedUOMProduct;
    }
    return nil;
}
#pragma mark
/** fetching saved/proforma orderDetails*/
-(void)fetchSavedOrderDetails{
    OrdersTableView.userInteractionEnabled=NO;
    productsTableView.userInteractionEnabled=NO;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){

        NSMutableArray *tempOrdrerLineItemsArray;
        if([self.currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kReOrderPopOverTitle])
        {
            
            tempOrdrerLineItemsArray=[[NSMutableArray alloc]initWithArray:[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*, B.* FROM TBL_Order_History_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@'  ORDER BY A.Line_Number ASC", self.currentVisit.visitOptions.salesOrder.reOrderDetails.Orig_Sys_Document_Ref]]];
            [tempOrdrerLineItemsArray setValue:@"" forKey:@"Custom_Attribute_2"];/* clearing the notes*/
        }
        else{
            tempOrdrerLineItemsArray=[[NSMutableArray alloc]initWithArray:[[SWDatabaseManager retrieveManager] dbGetSalesPerformaOrderItems:currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref]] ;
        }
        
        /** fetching the main products array*/
        NSPredicate *mainProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@",@"N"];
        NSMutableArray * mainProductsArray=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:mainProductsPredicate]];
        
        for (NSInteger i=0; i<mainProductsArray.count; i++) {
            /** fetching the main product Product object from category products Array*/
            NSPredicate *mainProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Item_Code"]]];
            NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:mainProductPredicate]];
            if(tempProductsArray.count>0) /**this product exist in the category*/
            {
                selectedOrderLineItem=[[SalesOrderLineItem alloc]init];
                Products *tempProduct=[tempProductsArray objectAtIndex:0];
                tempProduct=[self FetchProductDetailsFromPrimaryUOMProduct:tempProduct ForUOM:[[mainProductsArray objectAtIndex:i] valueForKey:@"Order_Quantity_UOM"]];
                selectedOrderLineItem.VATChargeObj=[VATChargesManager getVATChargeForProduct:tempProduct AndCustomer:currentVisit.visitOptions.customer];

                PriceList *priceList;
                if(tempProduct!=nil)
                {
	                   selectedOrderLineItem.OrderProduct=tempProduct;
                    
                    selectedOrderLineItem.OrderProductQty=[NSDecimalNumber ValidDecimalNumberWithString:[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Ordered_Quantity"]]];
                    priceList=[self fetchProductPriceListData:tempProduct];
                    
                }
                
                if(tempProduct==nil)
                {
                    showOneOrMoreProductsNotAvailable=YES;
                    
                }
                else if(priceList!=nil && [priceList.Unit_Selling_Price doubleValue]==0)
                {
                    showOneOrMoreProductPriceNotAvailable=YES;
                }
                else
                {
                    selectedOrderLineItem.OrderProduct.productPriceList=(PriceList*)priceList;
                    selectedOrderLineItem.productSelleingPrice=[selectedOrderLineItem.OrderProduct.isSellingPriceFieldEditable isEqualToString:KAppControlsYESCode]?[[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Unit_Selling_Price"]] doubleValue]:[priceList.Unit_Selling_Price doubleValue];
                    /** set the sellingprice to zero for foc order*/
                    if ([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
                    {
                        selectedOrderLineItem.productSelleingPrice=0.0 ;
                    }
                    selectedOrderLineItem.manualFOCQty=0;
                    /** fetching the manual products array*/
                    NSPredicate *manualFocProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@ AND Custom_Attribute_1==[cd] %@",@"M",[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_1"]] ];
                    NSMutableArray * filteredFocProductsArray=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:manualFocProductsPredicate]];
                    
                    if(filteredFocProductsArray.count>0)
                    {
                        NSPredicate *manualFocProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[SWDefaults getValidStringValue:[[filteredFocProductsArray objectAtIndex:0] valueForKey:@"Item_Code"]]];
                        NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:manualFocProductPredicate]];
                        if(tempProductsArray.count>0 && ![self isManualFOCDisabledForThisProduct:selectedOrderLineItem.OrderProduct])
                        {
                            selectedOrderLineItem.manualFocProduct=[self isTheProductRestrictedForManualFOC:[tempProductsArray objectAtIndex:0]]?nil:[tempProductsArray objectAtIndex:0];
                            selectedOrderLineItem.manualFOCQty=[self isTheProductRestrictedForManualFOC:[tempProductsArray objectAtIndex:0]]?0:[[SWDefaults getValidStringValue:[[filteredFocProductsArray objectAtIndex:0] valueForKey:@"Ordered_Quantity"]] doubleValue];
                        }
                        else
                        {
                            showOneOrMoreManualFocProductsNotAvailable=YES;
                            selectedOrderLineItem.manualFocProduct=[self isTheProductRestrictedForManualFOC:selectedOrderLineItem.OrderProduct]?nil:selectedOrderLineItem.OrderProduct;
                            selectedOrderLineItem.manualFOCQty=0;
                        }
                        
                    }
                    else
                    {
                        selectedOrderLineItem.manualFocProduct=[self isTheProductRestrictedForManualFOC:selectedOrderLineItem.OrderProduct]?nil:selectedOrderLineItem.OrderProduct;
                        selectedOrderLineItem.manualFOCQty=0;
                    }
                    
                    [self getBonusDetails];
                    [self getProductSpecailDisount];
                    /** fetching the manual bonus array*/
                    NSPredicate *BonusProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@ AND Custom_Attribute_1==[cd] %@ AND Assortment_Plan_ID==NULL",@"F",[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_1"]]];
                    NSMutableArray * filteredBonusProductsArray=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:BonusProductsPredicate]];
                    
                    if(filteredBonusProductsArray.count>0)
                    {
                        NSPredicate *BonusProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[SWDefaults getValidStringValue:[[filteredBonusProductsArray objectAtIndex:0] valueForKey:@"Item_Code"]]];
                        NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:BonusProductPredicate]];
                        if(tempProductsArray.count>0)
                        {
                            Products *tempBonusProduct=[self FetchProductDetailsFromPrimaryUOMProduct:[tempProductsArray objectAtIndex:0] ForUOM:[[filteredBonusProductsArray objectAtIndex:0] valueForKey:@"Order_Quantity_UOM"]];
                            [self calculateBonus];/*first assigning default bonus and bonus quantitys*/
                            if([self validateGivenBonusProduct:tempBonusProduct andDefaultBonusProduct:selectedOrderLineItem.bonusProduct])
                            {
                                selectedOrderLineItem.bonusProduct=[tempProductsArray objectAtIndex:0];
                                /** checking assigned bonus quantity valid or not*/
                                if([self validateGivenBonus:[[SWDefaults getValidStringValue:[[filteredBonusProductsArray objectAtIndex:0] valueForKey:@"Ordered_Quantity"]] doubleValue] andDefaultBonus:selectedOrderLineItem.defaultBonusQty])
                                {
                                    selectedOrderLineItem.requestedBonusQty=[[SWDefaults getValidStringValue:[[filteredBonusProductsArray objectAtIndex:0] valueForKey:@"Ordered_Quantity"]] doubleValue];
                                }
                                else
                                {
                                    selectedOrderLineItem.requestedBonusQty=selectedOrderLineItem.defaultBonusQty;/**assigning default bonus as requested bonus*/
                                    showOneOrMoreProductsBonusQtyChanged=YES;
                                }
                            }
                            else
                            {
                                showOneOrMoreProductsBonusProductChanged=YES;
                                [self calculateBonus];/*assigning the default bonusProduct and bonus quantitys*/
                            }
                            
                        }
                        else
                        {
                            showOneOrMoreBonusProductsNotAvailable=YES;
                            [self calculateBonus];/*first assigning default bonus and bonus quantitys*/
                            selectedOrderLineItem.requestedBonusQty=selectedOrderLineItem.defaultBonusQty;/**assigning default bonus as requested bonus*/
                            
                        }
                        
                    }
                    else
                    {
                        [self calculateBonus];/*assigning the default bonus and bonus quantitys*/
                    }
                    
                    
                    double discountAplliedWhileSavingTheOrder=[[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_3"]] doubleValue];
                    if([appControl.ENABLE_ITEM_DISCOUNT isEqualToString:KAppControlsYESCode])
                    {
                        selectedOrderLineItem.appliedDiscount=discountAplliedWhileSavingTheOrder*100;
                        
                    }
                    else
                    {
                        [self calculateDiscount];/*first assigning the discount as per current discounts*/
                        
                        /* checking is discounts are as per current discount rules or not*/
                        if(discountAplliedWhileSavingTheOrder*100==selectedOrderLineItem.appliedDiscount)
                        {
                            selectedOrderLineItem.appliedDiscount=discountAplliedWhileSavingTheOrder*100;
                            
                        }
                        else{
                            showOneOrMoreProductsDiscountChanged=YES;
                            selectedOrderLineItem.appliedDiscount=selectedOrderLineItem.appliedDiscount;
                        }
                    }
                    /** calculating the orderlineitem amounts*/
                    [self calculateItemAmounts];
                    
                    
                    /* first need to get special dicount and bonus details to caluclate bonus and discount*/
                    selectedOrderLineItem.NotesStr=[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Custom_Attribute_2"]];
                    
                    /** lots Assign*/
                    if(selectedOrderLineItem.OrderProduct.stock>0 && [selectedOrderLineItem.OrderProduct.stock doubleValue]>=selectedOrderLineItem.OrderProductQty.doubleValue)
                    {
                        NSArray *lotsArray=[[SWDatabaseManager retrieveManager]dbGetAssignedLotsForProformaOrderId:currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref AndLineNumber:[SWDefaults getValidStringValue:[[mainProductsArray objectAtIndex:i] valueForKey:@"Line_Number"]]];
                        NSMutableArray *productAllocatedLotsarray=[[NSMutableArray alloc]init];
                        if(lotsArray.count>0 && [appControl.ALLOW_LOT_SELECTION isEqualToString:KAppControlsNOCode])
                        {
                            selectedOrderLineItem.AssignedLotsArray=[[NSMutableArray alloc]init];
                            showOneOrMoreProductsLotsareDiscarded=YES;
                            
                        }
                        else if(lotsArray.count>0)/* lots assigned*/
                        {
                            NSMutableArray * stockLotsarray=  [self fetchStockDataforItemID:[tempProductsArray objectAtIndex:0]];
                            for (NSInteger j=0; j<lotsArray.count; j++) {
                                NSPredicate * lotNumberPredicate=[NSPredicate predicateWithFormat:@"SELF.Lot_No=%@",[SWDefaults getValidStringValue:[[lotsArray objectAtIndex:j] valueForKey:@"Lot_Number"]]];
                                
                                NSArray *filteredlotsArray=  [stockLotsarray filteredArrayUsingPredicate:lotNumberPredicate];
                                if(filteredlotsArray.count>0)/** if lot exist*/
                                {
                                    Stock *lot=[filteredlotsArray objectAtIndex:0];
                                    SalesOrderAssignedLot *asignedLot=[[SalesOrderAssignedLot alloc]init];
                                    asignedLot.lot=lot;
                                    asignedLot.assignedQuantity=[NSDecimalNumber ValidDecimalNumberWithString:[SWDefaults getValidStringValue:[[lotsArray objectAtIndex:j] valueForKey:@"Ordered_Quantity"]]];
                                    if([asignedLot.lot.lotQuantityNumber isLessThanDecimal:asignedLot.assignedQuantity])
                                    {
                                        showOneOrMoreProductsLotsareDiscarded=YES;
                                        productAllocatedLotsarray=[[NSMutableArray alloc]init];/** assigning empty lots array*/
                                        break;
                                    }
                                    [productAllocatedLotsarray addObject:asignedLot];
                                }
                                else
                                {
                                    productAllocatedLotsarray=[[NSMutableArray alloc]init];/** assigning empty lots array*/
                                    showOneOrMoreProductsLotsareDiscarded=YES;
                                }
                                
                            }
                        }
                        selectedOrderLineItem.AssignedLotsArray=productAllocatedLotsarray;
                    }
                    else
                    {
                        selectedOrderLineItem.AssignedLotsArray=[[NSMutableArray alloc]init];
                    }
                    /** lots Assign end*/
                    
                    [orderLineItemsArray addObject:selectedOrderLineItem];
                }
            }
            else
            {
                showOneOrMoreProductsNotAvailable=YES;
            }
            
        }
        
        
        if ([currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KUnConfirmOrderStatusString]) {
            //if loading from proforma order item will be added like the last added item is displayed on last but we are showing last added item on top in sales order so reversing the order
            //NSLog(@"proforma as received %@", [[orderLineItemsArray valueForKey:@"OrderProduct"] valueForKey:@"Description"]);
            NSArray* reversedArray = [[orderLineItemsArray reverseObjectEnumerator] allObjects];
            orderLineItemsArray=[reversedArray mutableCopy];
           // NSLog(@"proforma after reversed %@", [[orderLineItemsArray valueForKey:@"OrderProduct"] valueForKey:@"Description"]);

        }
        
        currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=orderLineItemsArray;

        
        /** Assigning Assortment bonus*/
        if(orderLineItemsArray.count>0 &&
           [appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
           ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
            /** fetching the Assort bonus Array*/
            NSPredicate *AsstBonusProductsPredicate=[NSPredicate predicateWithFormat:@"Calc_Price_Flag ==[cd] %@ AND Assortment_Plan_ID!=NULL",@"F"];
            NSMutableArray * filteredAsstBonusProducts=[[NSMutableArray alloc]initWithArray:[tempOrdrerLineItemsArray filteredArrayUsingPredicate:AsstBonusProductsPredicate]];
            
            
            /** creating temp salesorer */
            SalesOrder *tempOrder=[[SalesOrder alloc]init];
            tempOrder.SalesOrderLineItemsArray=orderLineItemsArray;
            tempOrder.AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
            tempOrder.AssortmentPlansArray=currentVisit.visitOptions.salesOrder.AssortmentPlansArray;
            /* updating sales order details with default assortment bonus Qunatities*/
            SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
            NSDictionary *tempUpdatedProformaOrderWithAsstBns = [bnsManager UpdateProformaSalesOrderLineItems:tempOrder WithDBAssortmentBonusProductsArray:filteredAsstBonusProducts AndProductsArray:productsArray];
            tempOrder= [tempUpdatedProformaOrderWithAsstBns valueForKey:@"SalesOrder"];
            
            if(!showOneOrMoreProductsBonusProductChanged){
                showOneOrMoreProductsBonusProductChanged=[[tempUpdatedProformaOrderWithAsstBns valueForKey:@"ShowOneOrMoreProductsAsstBonusProductChanged"] isEqualToString:KAppControlsYESCode]?YES:NO;
            }
            
            orderLineItemsArray=tempOrder.SalesOrderLineItemsArray;
            
            /**for a normal order we are inserting default bonus into def bounus field of tbl_order_line_Items in the case of re order we are fetching data from tbl_order_history_line_items which does not have def_bonus column, so re calculationg default bous and assigning to the object.**/
            
            
            for (NSInteger k=0; k<tempOrder.AssortmentBonusProductsArray.count; k++) {
                SalesworxAssortmentBonusProduct *asstBnsPro=[tempOrder.AssortmentBonusProductsArray objectAtIndex:k];
                SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
                
                asstBnsPro.PlanDefaultBnsQty=[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%f",[bnsManager FetchAvailableBonusQuantityForSalesOrder:tempOrder ForPlanId:asstBnsPro.PlanId]]];
                [tempOrder.AssortmentBonusProductsArray replaceObjectAtIndex:k withObject:asstBnsPro];
            }
            AssortmentBonusProductsArray=tempOrder.AssortmentBonusProductsArray;
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            /** disabling the uifields*/
            [self SortOrderLineItems];
            OrdersTableView.userInteractionEnabled=YES;
            productsTableView.userInteractionEnabled=YES;
            [self DeselectTableViewsAndUpdate];
            
            if(![[self prepareUnConfirmOrderDetailsChanesMessage]isEqualToString:@""])
            {
                if(![self.currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kReOrderPopOverTitle])
                {
                    isManageSalesOrderUpdated=YES;
                }
                [self showAlertAfterHidingKeyBoard:@"Sorry" andMessage:[self prepareUnConfirmOrderDetailsChanesMessage] withController:nil];
            }
            
            
        });
    });
    
}

-(NSString*)prepareUnConfirmOrderDetailsChanesMessage
{
    NSString *ProformaOrdresChangesAlertMessage=[[NSString alloc]init];
    NSMutableArray *alertMessagesArray=[[NSMutableArray alloc]init];
//    showOneOrMoreProductPriceNotAvailable=YES;
//    showOneOrMoreProductsNotAvailable=YES;
//    showOneOrMoreManualFocProductsNotAvailable=YES;
//    showOneOrMoreBonusProductsNotAvailable=YES;
//    showOneOrMoreProductsDiscountChanged=YES;
//    showOneOrMoreProductsBonusQtyChanged=YES;
//    showOneOrMoreProductsBonusProductChanged=YES;
    if(showOneOrMoreProductPriceNotAvailable)
    [alertMessagesArray addObject:@"One or more products are discarded due to unavailability of prices"];
    if(showOneOrMoreProductsNotAvailable)
    [alertMessagesArray addObject:@"One or more products are discarded due to unavailability of product details"];
    if(showOneOrMoreManualFocProductsNotAvailable)
    [alertMessagesArray addObject:@"One or more manual foc products discarded due to unavailability of product details"];
    if(showOneOrMoreBonusProductsNotAvailable)
    [alertMessagesArray addObject:@"One or more manual bonus products discarded due to unavailability of product details"];
    if(showOneOrMoreProductsDiscountChanged)
    [alertMessagesArray addObject:@"One or more products discounts changed as per current discounts"];
    if(showOneOrMoreProductsBonusProductChanged)
    [alertMessagesArray addObject:@"One or more bonus products changed to default bonus values"];
    if(showOneOrMoreProductsBonusQtyChanged)
    [alertMessagesArray addObject:@"One or more bonus products Quantity changed to default bonus values"];
    if(showOneOrMoreProductsLotsareDiscarded)
        [alertMessagesArray addObject:@"One or more products lots discarded due to lots/quantity unavailability"];



    
    if(alertMessagesArray.count>0)
    {
        ProformaOrdresChangesAlertMessage=[NSString stringWithFormat:@". %@",[alertMessagesArray componentsJoinedByString:@"\n\n . "]] ;
        return ProformaOrdresChangesAlertMessage;

    }
    else
    {
        return @"";
    }
}

-(BOOL)validateGivenBonusProduct:(Products*)tempBonusProduct andDefaultBonusProduct:(Products*)DefaultBonusProduct
{
    if(DefaultBonusProduct==nil)
        return NO;
    else if([tempBonusProduct.Item_Code isEqualToString:DefaultBonusProduct.Item_Code] && [tempBonusProduct.selectedUOM isEqualToString:DefaultBonusProduct.selectedUOM])
        return YES;
    else
        return NO;
}


-(BOOL)validateGivenBonus:(double)givenBonusQty andDefaultBonus:(double)DefaultBonus
{
    if([appControl.ALLOW_BONUS_CHANGE isEqualToString:KAppControlsYESCode])
    {
        return YES;
    }
    else if([appControl.ALLOW_BONUS_CHANGE isEqualToString:KAppControlsNOCode] && givenBonusQty==DefaultBonus)
    {
        return YES;

    }
    else if([appControl.ALLOW_BONUS_CHANGE isEqualToString:@"I"] && givenBonusQty>=DefaultBonus)
    {
        return YES;
    }
    else if([appControl.ALLOW_BONUS_CHANGE isEqualToString:@"D"] && givenBonusQty<=DefaultBonus)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(void)showCustomerDetails
{
    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    customerAvailableBalanceLabel.text=[currentVisit.visitOptions.customer.Avail_Bal currencyString];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);

    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerstatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerstatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==OrdersTableView) {
        if(section==0){
            return 0;
        }
        else{
            SalesOrderLineItem *orderLineItem=[orderLineItemsArray objectAtIndex:section];
            SalesOrderLineItem *previousOrderLineItem=[orderLineItemsArray objectAtIndex:section-1];
            
            if(orderLineItem.showApliedAssortMentPlanBonusProducts){
                return 0;
            }else if(previousOrderLineItem.AppliedAssortMentPlanID==NSNotFound || orderLineItem.AppliedAssortMentPlanID!=previousOrderLineItem.AppliedAssortMentPlanID){
                return 5.0f;
            }
        }
        return 0;
    }
    else if (tableView==productsTableView) {
        
        return 2.0f;
    }
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==OrdersTableView) {
        return 44;
    }
    if (tableView==productsTableView) {
        if(indexPath.row==0)
        {
            return 44.0;
        }
        return UITableViewAutomaticDimension;
    }
    return 0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==OrdersTableView) {

        if(section==0){
            return nil;
        }
        else{
            SalesOrderLineItem *orderLineItem=[orderLineItemsArray objectAtIndex:section];
            SalesOrderLineItem *previousOrderLineItem=[orderLineItemsArray objectAtIndex:section-1];

            if(orderLineItem.showApliedAssortMentPlanBonusProducts){
                return nil;
            }else if(previousOrderLineItem.AppliedAssortMentPlanID==NSNotFound || orderLineItem.AppliedAssortMentPlanID!=previousOrderLineItem.AppliedAssortMentPlanID){
                UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 5.0)];
                view.backgroundColor=UIViewBackGroundColor;
                return view;
            }
        }
        return nil;
    }
    else
    {
        return nil;
    }
    
}   // custom view for header. will be adjusted to default or specified header height



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==productsTableView) {
        return productsBrandCodesArray.count;
    }
    else if (tableView==OrdersTableView)
    {
            return orderLineItemsArray.count;
    }
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==productsTableView) {
        
        
        if(![ProductTableViewExpandedSectionsArray containsObject:[NSString stringWithFormat:@"%ld",(long)section]])
            return 1;
        else
            return [(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:section]count]+1;
    }
    else if (tableView==OrdersTableView)
    {
            SalesOrderLineItem *orderLineItem=[orderLineItemsArray objectAtIndex:section];
            NSInteger numberOfrows=0;
            if(orderLineItem.OrderProductQty>0)
                numberOfrows++;
            if(orderLineItem.requestedBonusQty>0)
                numberOfrows++;
            if(orderLineItem.manualFOCQty>0)
                numberOfrows++;
            if(orderLineItem.AppliedAssortMentPlanID!=NSNotFound && orderLineItem.showApliedAssortMentPlanBonusProducts)
            {
                NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)orderLineItem.AppliedAssortMentPlanID]];
                NSMutableArray *tempAssortMentBonusProductsArray=  [[AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
                numberOfrows=numberOfrows+tempAssortMentBonusProductsArray.count;
                NSLog(@"number of sections with assortment bonus %ld %ld",numberOfrows,tempAssortMentBonusProductsArray.count);
            }
                
            return numberOfrows;

    }
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView==productsTableView) {
        
        
        if(indexPath.row==0)
        {
            static NSString* identifier=@"BrandCodeCell";
            SalesWorxSalesOrderProductsTableViewBrandcodeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesOrderProductsTableViewBrandcodeCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.brandCodeLabel.text=[productsBrandCodesArray objectAtIndex:indexPath.section];
            cell.brandCodeLabel.font=MedRepTitleFont;
            if(![ProductTableViewExpandedSectionsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section ]])
            {
                [self updateBrandCodeCell:cell isExpanded:NO];
            }
            else
            {
                [self updateBrandCodeCell:cell isExpanded:YES];
            }
            return cell;
        }
        else
        {
            Products *currentProduct;
            NSMutableArray *arrProductsByBrandCode = [filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section];
            if (arrProductsByBrandCode.count > 0) {
                currentProduct = [arrProductsByBrandCode objectAtIndex:indexPath.row-1];
            }
            
            static NSString* identifier=@"updatedDesignCell";
            MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.cellStatusViewType=KCustomColorStatusView;
            if(![selectedProductIndexpath isEqual:indexPath] || selectedProductIndexpath==nil)
            {
                [cell setDeSelectedCellStatus];

            }
            else
            {
                [cell setSelectedCellStatus];
            }
            
            
            
            cell.titleLbl.text=[[NSString stringWithFormat:@"%@",currentProduct.Description] capitalizedString];
            cell.descLbl.text=[NSString stringWithFormat:@"%@",currentProduct.Item_Code];
            
            if(currentProduct.stock.doubleValue>0)
            {
                cell.statusView.backgroundColor=[UIColor greenColor];
                
            }
            else
            {
                cell.statusView.backgroundColor=[UIColor redColor];
                
            }
            
            return cell;

        }
        
    }
    else
    {
    
        static NSString* identifier=@"SalesOrderItemCell";
        SalesWorxSalesOrdersTableCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesOrdersTableCellTableViewCell" owner:nil options:nil] firstObject];

        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.btnProximityIndicator.hidden = YES;
        cell.btnProximityIndicator.tag = indexPath.section;
        [cell.btnProximityIndicator addTarget:self action:@selector(btnProximityIndicatorTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        SalesOrderLineItem *currentLineItem=[orderLineItemsArray objectAtIndex:indexPath.section];
        
        if(indexPath.row==0)
        {
            NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.OrderProduct.selectedUOM];

            cell.QunatutyLbl.text=[NSString stringWithFormat:@"%@ %@",currentLineItem.OrderProductQty,uomStr];
            cell.UnitPriceLbl.text=[[NSString
                                    stringWithFormat:@"%f",currentLineItem.productSelleingPrice] floatString];
            cell.ProductNameLbl.text=currentLineItem.OrderProduct.Description;
            cell.NetAmountLbl.text=[[NSString
                                     stringWithFormat:@"%@",currentLineItem.OrderItemNetamount] floatString];
            cell.ToatlLbl.text=[[NSString
                                 stringWithFormat:@"%@",currentLineItem.OrderItemTotalamount] floatString];
            cell.DiscountLbl.text=[[NSString
                                    stringWithFormat:@"%@",currentLineItem.OrderItemDiscountamount] floatString];

            cell.VATChargesLbl.text=currentLineItem.VATChargeObj!=nil?[[NSString
                                                                        stringWithFormat:@"%.2f",currentLineItem.VATChargeObj.VATCharge.floatValue]stringByAppendingString:@"%"]:@"0.00%" ;

            if(selectedOrderItemIndexpath!=nil && selectedOrderItemIndexpath.section==indexPath.section)
            {
                /*seclecting the present selected cell*/
                [cell.statusView setHidden:YES];
                cell.ProductNameLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.QunatutyLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.UnitPriceLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.ToatlLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.DiscountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.NetAmountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;

            }
            else
            {
                [cell.statusView setHidden:YES];
                cell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;

            }
            
            NSMutableArray *rightSwipeButtons=[[NSMutableArray alloc]init];
            if((currentVisit.visitOptions.salesOrder.isManagedOrder && [currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KConfirmOrderStatusString]))
            {
                if([appControl.ALLOW_LOT_SELECTION isEqualToString:KAppControlsYESCode] && currentLineItem.AssignedLotsArray.count>0)
                    [rightSwipeButtons addObject:[MGSwipeButton buttonWithTitle:KLineItemShowLots backgroundColor:[UIColor colorWithRed:(255.0/255.0) green:(156.0/255.0) blue:(88.0/255.0) alpha:1.0]]];
                if([appControl.ENABLE_LINE_ITEM_NOTES isEqualToString:KAppControlsYESCode] && currentLineItem.NotesStr.length>0)
                    [rightSwipeButtons addObject:[MGSwipeButton buttonWithTitle:KLineItemNotes backgroundColor:[UIColor lightGrayColor]]];
                cell.rightButtons = rightSwipeButtons;
                cell.rightSwipeSettings.transition = MGSwipeTransition3D;
                cell.delegate=self;

            }

            else
            {
                [rightSwipeButtons addObject:[MGSwipeButton buttonWithTitle:NSLocalizedString(@"Delete", nil) backgroundColor:[UIColor redColor]]];

                if(([appControl.ENABLE_LINE_ITEM_NOTES isEqualToString:KAppControlsYESCode] || [appControl.ALLOW_LOT_SELECTION isEqualToString:KAppControlsYESCode]) && ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
                    [rightSwipeButtons addObject:[MGSwipeButton buttonWithTitle:NSLocalizedString(@"More", nil) backgroundColor:[UIColor lightGrayColor]]];
                cell.rightButtons = rightSwipeButtons;
                cell.rightSwipeSettings.transition = MGSwipeTransition3D;
                cell.delegate=self;

            }
            
            if(indexPath.section==0 && showOrderTableViewNewItemAnimationColor)
            {
                cell.lineItemRowAnimationView.backgroundColor=KNewRowAnimationColor;
            }
            else
            {
                cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
            }
            cell.CellItemType=NormalProductTypeCell;
            

            //<NG 2019-01-29> Bonus alert modifications
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode] && currentLineItem.bonusDetailsArray.count > 0) {
                
                BOOL showProximityIndicator = NO;
                if ([appControl.BONUS_ALERT_THRESHOLD_TYPE isEqualToString:@"P"]) {
                    // Percentage type
                    
                    for (ProductBonusItem *objBonus in currentLineItem.bonusDetailsArray) {
                        float floatVal = (objBonus.Prom_Qty_From.floatValue * appControl.BONUS_ALERT_THRESHOLD.floatValue)/100;
                        int thresholdPercentage = ceil(floatVal);
                        int thresholdRange = objBonus.Prom_Qty_From.intValue - thresholdPercentage;
                        
                        if (currentLineItem.OrderProductQty.intValue >= thresholdRange && currentLineItem.OrderProductQty.intValue < objBonus.Prom_Qty_From.intValue) {
                            showProximityIndicator = YES;
                            break;
                        }
                    }
                } else {
                    // Value type
                    
                    for (ProductBonusItem *objBonus in currentLineItem.bonusDetailsArray) {
                        int thresholdRange = objBonus.Prom_Qty_From.intValue - appControl.BONUS_ALERT_THRESHOLD.intValue;

                        if (currentLineItem.OrderProductQty.intValue >= thresholdRange && currentLineItem.OrderProductQty.intValue < objBonus.Prom_Qty_From.intValue) {
                            showProximityIndicator = YES;
                            break;
                        }
                    }
                }
                
                if (showProximityIndicator) {
                    cell.btnProximityIndicator.hidden = NO;
                }
            }
        }
        else if(indexPath.row==1 && (currentLineItem.requestedBonusQty>0 || currentLineItem.manualFOCQty>0))
        {
            [cell.statusView setHidden:NO];
            cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];

            if(currentLineItem.manualFOCQty>0)
            {
                NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.manualFocProduct.selectedUOM];

                cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.manualFOCQty,uomStr];
                cell.ProductNameLbl.text=currentLineItem.manualFocProduct.Description;
                cell.CellItemType=ManualFOCTypeCell;

            }
            else
            {
                NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.bonusProduct.selectedUOM];

                cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.requestedBonusQty,uomStr];
                cell.ProductNameLbl.text=currentLineItem.bonusProduct.Description;
                cell.CellItemType=SimpleBonusTypeCell;

            }
            
            cell.UnitPriceLbl.text=[@"0.0" floatString];
            cell.NetAmountLbl.text=[@"0.0" floatString];
            cell.ToatlLbl.text=[@"0.0" floatString];
            cell.DiscountLbl.text=[@"0.0" floatString];
            cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
            
            cell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
            cell.rightButtons=[[NSArray alloc]init];
            cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
            cell.VATChargesLbl.text=@"0.00%" ;

        }
        else if(indexPath.row==2 && (currentLineItem.requestedBonusQty>0 && currentLineItem.manualFOCQty>0))
        {
            [cell.statusView setHidden:NO];

            NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.bonusProduct.selectedUOM];
            cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.requestedBonusQty,uomStr];
            cell.UnitPriceLbl.text=[@"0.0" floatString];
            cell.ProductNameLbl.text=currentLineItem.bonusProduct.Description;
            cell.NetAmountLbl.text=[@"0.0" floatString];
            cell.ToatlLbl.text=[@"0.0" floatString];
            cell.DiscountLbl.text=[@"0.0" floatString];
            cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
            
            cell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
            cell.rightButtons=[[NSArray alloc]init];
            cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
            cell.CellItemType=SimpleBonusTypeCell;
            cell.VATChargesLbl.text=@"0.00%" ;

        }
        else
        {
            NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)currentLineItem.AppliedAssortMentPlanID]];
            NSMutableArray *tempAssortMentBonusProductsArray=  [[AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];

            NSInteger bonusProductIndex=indexPath.row-(currentLineItem.requestedBonusQty>0?1:0)-(currentLineItem.manualFOCQty>0?1:0)-1/*main product*/;
            NSLog(@"bonus product index is %ld",(long)bonusProductIndex);
            
            if (tempAssortMentBonusProductsArray.count>bonusProductIndex)
            {
                SalesworxAssortmentBonusProduct *BnsPro= [tempAssortMentBonusProductsArray objectAtIndex:bonusProductIndex];
                NSString *uomStr=[NSString stringWithFormat:@"%@",BnsPro.product.primaryUOM];
                cell.QunatutyLbl.text=[NSString stringWithFormat:@"%@ %@",BnsPro.assignedQty,uomStr];
                cell.ProductNameLbl.text=BnsPro.product.Description;
            }
            else
            {
                cell.QunatutyLbl.text=KNotApplicable;
                cell.ProductNameLbl.text=KNotApplicable;
            }
            
            
            
            [cell.statusView setHidden:NO];
            
            cell.UnitPriceLbl.text=[@"0.0" floatString];
            cell.NetAmountLbl.text=[@"0.0" floatString];
            cell.ToatlLbl.text=[@"0.0" floatString];
            cell.DiscountLbl.text=[@"0.0" floatString];
            cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
            cell.VATChargesLbl.text=@"0.00%" ;

            cell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
            cell.rightButtons=[[NSArray alloc]init];
            cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
            cell.CellItemType=AssortMentBonusTypeCell;
        }
        return cell;

    }

}
    
-(void)btnProximityIndicatorTapped:(id)Sender
{
    [self.view endEditing:YES];
    
    UIButton *selectedButton = Sender;
    selectedOrderLineItem=[(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:selectedButton.tag] copy];
        
    SalesOrderBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesOrderBonusInfoViewController alloc]initWithNibName:@"SalesOrderBonusInfoViewController" bundle:[NSBundle mainBundle]];
    salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
    [self getBonusDetails];
    salesOrderBonusInfoViewController.bonusDelegate = self;
    salesOrderBonusInfoViewController.isOrderQuantityRecommended = YES;
    salesOrderBonusInfoViewController.selectedOrderLineItemIndexForBonusRecommendation = selectedButton.tag;
    salesOrderBonusInfoViewController.productsArray=productsArray;
    salesOrderBonusInfoViewController.OrderLineItem=selectedOrderLineItem;
    salesOrderBonusInfoViewController.bonusArray=selectedOrderLineItem.bonusDetailsArray;
    [self.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
}
    
-(void)updateOrderLineItem:(SalesOrderLineItem *)orderLineItem atIndex:(NSInteger)index
{
    [orderLineItemsArray replaceObjectAtIndex:index withObject:orderLineItem];
    [self tableView:OrdersTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
    orderQtyTextField.text = [NSString stringWithFormat:@"%@", orderLineItem.OrderProductQty];
    [self SalesOrderTextFieldEditingChanged:orderQtyTextField] ;
    [self addToOrderButtonTapped:self];
    [NSTimer scheduledTimerWithTimeInterval:0.1
                                     target:self
                                   selector:@selector(showOrderTableViewNewItemAddAnimation)
                                   userInfo:nil
                                    repeats:NO];
}
    
-(BOOL)isTheProductRestrictedForManualFOC:(Products *)manulaFOCProduct
{

    if([manulaFOCProduct.REMOVE_IN_FOC_LIST isEqualToString:KAppControlsYESCode] &&
       [appControl.FILTER_FOC_LIST isEqualToString:KAppControlsYESCode])
            return YES;
    
    return NO;
}

-(BOOL)isManualFOCDisabledForThisProduct:(Products *)product
{
    
    if([product.RESTRICTED isEqualToString: KAppControlsYESCode] && [appControl.DISABLE_FOC_FOR_RESTRICTED isEqualToString:KAppControlsYESCode])
    {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==productsTableView)
    {
        if(indexPath.row==0)
        {
            [self.view endEditing:YES];
            SalesWorxSalesOrderProductsTableViewBrandcodeCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            NSMutableArray *productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
            
            if(ProductTableViewExpandedSectionsArray.count>0)
            {
                NSInteger expnadedSection=[[ProductTableViewExpandedSectionsArray objectAtIndex:0]integerValue];
                cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:expnadedSection]];

                for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:expnadedSection]count]; i++) {
                    NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:expnadedSection];
                    [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                }
                [tableView beginUpdates];
                [ProductTableViewExpandedSectionsArray removeAllObjects];
                [tableView deleteRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                [self updateBrandCodeCell:cell isExpanded:NO];
                [tableView endUpdates];
                
                if(expnadedSection!=indexPath.section)
                {
                    cell = [tableView cellForRowAtIndexPath:indexPath];

                    productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
                    
                    [self AddProductObjectsToGroupByArrayAtIndex:indexPath.section];
                    for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section]count]; i++) {
                        NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:indexPath.section];
                        [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                    }
                    [tableView beginUpdates];
                    [ProductTableViewExpandedSectionsArray addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
                    [tableView insertRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self updateBrandCodeCell:cell isExpanded:YES];
                    [tableView endUpdates];
                    
                }
                
            }
            else
            {
                cell = [tableView cellForRowAtIndexPath:indexPath];

                productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
                [self AddProductObjectsToGroupByArrayAtIndex:indexPath.section];

                for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section]count]; i++) {
                    NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:indexPath.section];
                    [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                }
                [tableView beginUpdates];
                [ProductTableViewExpandedSectionsArray addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
                [tableView insertRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                [self updateBrandCodeCell:cell isExpanded:YES];
                [tableView endUpdates];
            }

        }
        else
        {
            tempSalesOrder=nil;
            [self.view endEditing:YES];
            [self showProductDetailsView];
            [self showProductOrderDetailsView];
            [self hideNoSelectionView];
            [self clearAllProductLabelsAndTextFields];
            [self updateTheProductTableViewSelectedCellIndexpath:indexPath];
            
            NSMutableArray *arrProductsByBrandCode = [filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section];
            
            if (arrProductsByBrandCode.count > 0) {
                selectedProduct=[(Products *)[arrProductsByBrandCode objectAtIndex:indexPath.row-1] copy];
            }
            
            selectedProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:selectedProduct];
               /* Price list for Primary UOM*/
           if([self fetchProductPriceListData:selectedProduct]!=nil)
           {
               selectedProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedProduct];
               productWholeSalePriceLabel.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString] ;
               productRetailPriceLabel.text=[selectedProduct.productPriceList.Unit_List_Price floatString];
           }
           else if([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode] && selectedProduct.ProductUOMArray.count>0 )
               // checking for other UOM price list availability
           {
               NSMutableArray *priceListAvailableUOMsArray=[[SWDatabaseManager retrieveManager] getPriceListAvailableUOMSForSalesOrderProduct:selectedProduct AndCustomer:currentVisit.visitOptions.customer];
               if (priceListAvailableUOMsArray.count==0)
               {
                   productWholeSalePriceLabel.text=KNotApplicable;
                   productRetailPriceLabel.text=KNotApplicable;
                   [self showProductOrderDetailsView];
                   [self hideProductOrderDetailsView];
                   [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
                   [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];
               }
               else
               {
                   selectedProduct=[self getUpdatedProductDetails:selectedProduct ToSelectedUOM:[priceListAvailableUOMsArray objectAtIndex:0]];
                   selectedProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedProduct];
                   productWholeSalePriceLabel.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString] ;
                   productRetailPriceLabel.text=[selectedProduct.productPriceList.Unit_List_Price floatString];

               }
               
           }
            else
            {
                productWholeSalePriceLabel.text=KNotApplicable;
                productRetailPriceLabel.text=KNotApplicable;
                [self showProductOrderDetailsView];
                [self hideProductOrderDetailsView];
                [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
                [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];

            }

            productNearestExpiryDateLabel.text=[selectedProduct.nearestExpiryDate isEqualToString:@""]?KNotApplicable:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:selectedProduct.nearestExpiryDate];
            productCodeLabel.text=selectedProduct.Item_Code;
            productNameLabel.text=selectedProduct.Description;
            NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Visit_ID,sum(B.Qty),A.Checked_On,B.Inventory_Item_ID FROM TBL_Distribution_Check A inner join TBL_Distribution_check_Items B on A.Distributioncheck_ID=B.Distributioncheck_ID where customer_ID in(Select Customer_ID from TBL_Customer where customer_No='%@') and B.Inventory_Item_ID='%@'and date(A.Checked_On)=date(datetime())",currentVisit.visitOptions.customer.Customer_No,selectedProduct.Inventory_Item_ID]];
            NSLog(@"%@",plannedVisitIDArray);
            //  selectedOrderLineItem.OrderProduct.Inventory_Item_ID
            //  NSString * documentPathComponent = [[orglogoNameArary objectAtIndex:0] valueForKey:@"Value_1"];
            if (plannedVisitIDArray.count==0) {
                productDCQtyLabel.text = @"NA";
            }
            else{
                //[NSString stringWithFormat:@"%@",[[arrlastStock  objectAtIndex:0] valueForKey:@"totalStock"]]
                NSString *newString = [NSString stringWithFormat:@"%@",[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Qty)"]];//[[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Qty)"] stringValue];
                if([newString isEqual:[NSNull null]] || [newString isEqualToString:@"<null>"]){
                    productDCQtyLabel.text = @"NA";
                }
                else{
                    productDCQtyLabel.text = newString;
                }
            }
            productAvailableStockLabel.text=[NSString stringWithFormat:@"%@ %@",selectedProduct.stock,selectedProduct.selectedUOM];
            productDefaultDiscountLabel.text=[[NSString stringWithFormat:@"%0.2f", [selectedProduct.Discount doubleValue]] stringByAppendingString:@"%"];
            
            selectedOrderLineItem=[[SalesOrderLineItem alloc]init];
            selectedOrderLineItem.OrderProduct=selectedProduct;
            selectedOrderLineItem.VATChargeObj=[VATChargesManager getVATChargeForProduct:selectedOrderLineItem.OrderProduct AndCustomer:currentVisit.visitOptions.customer];

            [self getProductSpecailDisount];
            
            if([selectedOrderLineItem.OrderProduct.specialDiscount doubleValue]>0.0)
                productSpecialDiscountLabel.text=[NSString stringWithFormat:@"%0.2f", [selectedOrderLineItem.OrderProduct.specialDiscount doubleValue]];
            else
                productSpecialDiscountLabel.text=@"N/A";
            
            [self getBonusDetails];
            
            if([appControl.ENABLE_ALERT_NO_BONUS isEqualToString:KAppControlsYESCode] && selectedOrderLineItem.bonusDetailsArray.count == 0){
                {
                    [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Bonus not available" withController:self];
                }
            }
            
            if(![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
            {
                sellingPriceTextField.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString];
                selectedOrderLineItem.productSelleingPrice=[selectedProduct.productPriceList.Unit_Selling_Price doubleValue];

            }
            else
            {
                sellingPriceTextField.text=[@"0" floatString];
                selectedOrderLineItem.productSelleingPrice=0.00;

            }
            
            /** For FOC orders manual foc not allowed*/
            if(![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
            {
                //check if the product is restricted for manual foc /** intially assigning selected product also as manual foc*/
                if([self isTheProductRestrictedForManualFOC:selectedProduct])
                {
                    selectedOrderLineItem.manualFocProduct=nil;
                }
                else
                {
                    manualFOCTextField.text=selectedProduct.Description;
                    selectedOrderLineItem.manualFocProduct=[self getManualFOCProductWithPrimaryUOM:selectedProduct];
                }
            }


            [self updateRequiredBonusField];

            /** foc order user cana not change price*/
            if(![selectedProduct.isSellingPriceFieldEditable isEqualToString:KAppControlsYESCode] || [currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
                [sellingPriceTextField setIsReadOnly:YES];
            else
                [sellingPriceTextField setIsReadOnly:NO];
            
            [self calculateDiscount];
            discountTextField.text=[NSString stringWithFormat:@"%0.2f",selectedOrderLineItem.appliedDiscount];
            isUpdatingOrder=NO;
            [self updateAddToOrderButtonTitle];
            [self updateFOCFields];
            
            /** Multi UOM*/
            [self updateUOMTextfield];
            
            selectedOrderItemIndexpath=nil;
            [OrdersTableView reloadData];
            
            if ([appControl.FS_ENABLE_PRODUCT_MEDIA isEqualToString:KAppControlsYESCode]) {
                NSMutableArray *selectedProductMediaArray = [[SWDatabaseManager retrieveManager] fetchMediaFilesforProduct:selectedProduct.ItemID];
                if (selectedProductMediaArray.count == 0) {
                    productMediaButton.hidden = YES;
                } else {
                    productMediaButton.hidden = NO;
                }
            }else{
                productMediaButton.hidden = YES;
            }
            
        }
    }
    else
    {
        tempSalesOrder=nil;
        SalesWorxSalesOrdersTableCellTableViewCell *tappedCell=(SalesWorxSalesOrdersTableCellTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        [addToProductButton setTitle:@"Update" forState:UIControlStateNormal];
        selectedOrderLineItem=[(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:indexPath.section] copy];
        [self showProductDetailsView];
        [self showProductOrderDetailsView];
        [self hideNoSelectionView];
        [self clearAllProductLabelsAndTextFields];
        [self updateTheOrderTableViewSelectedCellIndexpath:indexPath];
        selectedProduct=selectedOrderLineItem.OrderProduct;
        if([self fetchProductPriceListData:selectedOrderLineItem.OrderProduct]!=nil)
        {
            selectedOrderLineItem.OrderProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedOrderLineItem.OrderProduct];
            productWholeSalePriceLabel.text=[selectedOrderLineItem.OrderProduct.productPriceList.Unit_Selling_Price floatString];
            productRetailPriceLabel.text=[selectedOrderLineItem.OrderProduct.productPriceList.Unit_List_Price floatString];
            [self showProductDetailsView];
            [self showProductOrderDetailsView];
            [self hideNoSelectionView];
        }
        else
        {
            productWholeSalePriceLabel.text=KNotApplicable;
            productRetailPriceLabel.text=KNotApplicable;
            [self showProductOrderDetailsView];
            [self hideProductOrderDetailsView];
            [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
            [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];
        }
        productNearestExpiryDateLabel.text=[selectedOrderLineItem.OrderProduct.nearestExpiryDate isEqualToString:@""]?KNotApplicable:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:selectedProduct.nearestExpiryDate];
        productCodeLabel.text=selectedOrderLineItem.OrderProduct.Item_Code;
        productNameLabel.text=selectedOrderLineItem.OrderProduct.Description;
        productAvailableStockLabel.text=[NSString stringWithFormat:@"%@ %@",selectedOrderLineItem.OrderProduct.stock,selectedOrderLineItem.OrderProduct.selectedUOM];
        productDefaultDiscountLabel.text=[[NSString stringWithFormat:@"%0.2f", [selectedOrderLineItem.OrderProduct.Discount doubleValue]] stringByAppendingString:@"%"];
        [self getProductSpecailDisount];
        if([selectedOrderLineItem.OrderProduct.specialDiscount doubleValue]>0.0)
            productSpecialDiscountLabel.text=[NSString stringWithFormat:@"%0.2f", [selectedOrderLineItem.OrderProduct.specialDiscount doubleValue]];
        else
            productSpecialDiscountLabel.text=@"N/A";

        [self getBonusDetails];
        if(![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
        {
            sellingPriceTextField.text=[[NSString stringWithFormat:@"%f",selectedOrderLineItem.productSelleingPrice] floatString] ;
            discountTextField.text=[NSString stringWithFormat:@"%0.2f",selectedOrderLineItem.appliedDiscount] ;
        }
        else
        {
            sellingPriceTextField.text=[@"0" floatString];
            discountTextField.text=[NSString stringWithFormat:@"%0.2f",0.00];
        }
        selectedOrderLineItem.manualFocProduct=selectedOrderLineItem.manualFOCQty>0?selectedOrderLineItem.manualFocProduct:([self isTheProductRestrictedForManualFOC:selectedOrderLineItem.OrderProduct]?nil:[self getManualFOCProductWithPrimaryUOM:selectedOrderLineItem.OrderProduct]);
        
        [self updateBonusFields];
        [self updateFOCFields];
        [self updateRequiredBonusField];
        requestedBonusTextField.text=[NSString stringWithFormat:@"%0.0f",selectedOrderLineItem.requestedBonusQty];
        orderQtyTextField.text=[NSString stringWithFormat:@"%@",selectedOrderLineItem.OrderProductQty];
        [self updateProductAmountLabels];
        
        if(![selectedProduct.isSellingPriceFieldEditable isEqualToString:KAppControlsYESCode] || [currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
            [sellingPriceTextField setIsReadOnly:YES];
        else
            [sellingPriceTextField setIsReadOnly:NO];
        
        
        isUpdatingOrder=YES;
        [self updateAddToOrderButtonTitle];
        selectedOrderItemIndexpath=indexPath;
        selectedProductIndexpath=nil;
        
        /** Multi UOM*/
        selectedOrderLineItem.OrderProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:selectedOrderLineItem.OrderProduct];
        [self updateUOMTextfield];

        if(showOrderTableViewNewItemAnimationColor)
        {
            showOrderTableViewNewItemAnimationColor=NO;

            [OrdersTableView beginUpdates];
            [OrdersTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
            [OrdersTableView endUpdates];
        }
        
        [productsTableView reloadData];
        
        
        if(selectedOrderLineItem.AppliedAssortMentPlanID!=NSNotFound &&
           selectedOrderLineItem.showApliedAssortMentPlanBonusProducts &&
           tappedCell.CellItemType==AssortMentBonusTypeCell)
        {
    
            NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)selectedOrderLineItem.AppliedAssortMentPlanID]];
            NSMutableArray *tempAssortMentBonusProductsArray=  [[AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
    
          SalesOrderLineItem *  OrderLineItem=[selectedOrderLineItem copy];
            if(tempSalesOrder==nil){
                tempSalesOrder=[[SalesOrder alloc]init];
                tempSalesOrder.SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
                tempSalesOrder.SalesOrderLineItemsArray=[[orderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid!=%@",OrderLineItem.Guid]] mutableCopy];
                [tempSalesOrder.SalesOrderLineItemsArray addObject:OrderLineItem];
                tempSalesOrder.AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
                tempSalesOrder.AssortmentBonusProductsArray=[AssortmentBonusProductsArray mutableCopy];
                tempSalesOrder.AssortmentPlansArray=[[NSMutableArray alloc]init];
                tempSalesOrder.AssortmentPlansArray=currentVisit.visitOptions.salesOrder.AssortmentPlansArray;
            }
            
            [self PresentAssortmentBonusViewControllerWithPlanId:selectedOrderLineItem.AppliedAssortMentPlanID WithBonusQuantity:[[tempAssortMentBonusProductsArray valueForKeyPath:@"@sum.assignedQty"] doubleValue] AndAssignedBonusItemsArray:tempAssortMentBonusProductsArray AndSelectedOrderLineItem:[selectedOrderLineItem copy] AndTempSalesOrder:tempSalesOrder IsAllocationMandatory:NO];
        }
    }
    
    /* if confirm Order disbale all fields*/
    if(currentVisit.visitOptions.salesOrder.isManagedOrder && [currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KConfirmOrderStatusString])
    {
        [self DiableAllTexfields];
    }
    if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        [self UpdateUIFiledsForFOCOrder];
    }

}

-(void)getProductSpecailDisount
{
    if ([appControl.ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsYESCode])
    {
        NSString* specialDiscount;
        specialDiscount=[[SWDatabaseManager retrieveManager] dbGetSpecialDiscount:selectedOrderLineItem.OrderProduct.Inventory_Item_ID];
        selectedOrderLineItem.OrderProduct.specialDiscount=specialDiscount;
    }
    else
    {
        selectedOrderLineItem.OrderProduct.specialDiscount=@"0.0";

    }
}

-(void)getBonusDetails
{

    
    NSMutableArray *bonusDetailsArray=[[NSMutableArray alloc]init];

    /** checking availability of assortment bonus*/
    BOOL isAssortmentBonusPlanAvailable=NO;
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
        SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
        if([bnsManager IsAnyAssortmentBonusAvailableForTheProduct:[selectedOrderLineItem.OrderProduct copy] AssortmentPlans:currentVisit.visitOptions.salesOrder.AssortmentPlansArray])
            isAssortmentBonusPlanAvailable=YES;
    }
    

    if(currentVisit.visitOptions.customer.isBonusAvailable && !isAssortmentBonusPlanAvailable)
    {
        NSArray * bonusInfo=[[SWDatabaseManager retrieveManager] dbGetBonusInfo:selectedOrderLineItem.OrderProduct.Item_Code];
        for (NSInteger i=0; i<bonusInfo.count; i++) {
            NSMutableDictionary *bonusItemInfo=[bonusInfo objectAtIndex:i];
            ProductBonusItem * bonusItem=[[ProductBonusItem alloc]init];
            bonusItem.Description=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Description"]];
            bonusItem.Get_Add_Per=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Add_Per"]];
            bonusItem.Get_Item=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Item"]];
            bonusItem.Price_Break_Type_Code=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Price_Break_Type_Code"]];
            bonusItem.Get_Qty=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Qty"]];
            bonusItem.Prom_Qty_From=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Prom_Qty_From"]];
            bonusItem.Prom_Qty_To=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Prom_Qty_To"]];
            
            
            NSPredicate *bonusProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Item"]]];
            NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:bonusProductPredicate]];
            
            
            /** checking availability of product details*/
            if(tempProductsArray.count==1)
            {
                Products *product=[tempProductsArray objectAtIndex:0];
                product.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:product];
                
                /** checking availability of product UOM*/
                NSPredicate *getUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_UOM"]]];
                NSMutableArray *getUOMfiltredArray=[[product.ProductUOMArray filteredArrayUsingPredicate:getUomPredicate] mutableCopy];
                
                if(getUOMfiltredArray.count==1)
                {
                    product=[self getUpdatedProductDetails:product ToSelectedUOM:[getUOMfiltredArray objectAtIndex:0]];
                    bonusItem.bonusProduct=product;
                    [bonusDetailsArray addObject:bonusItem];
                }
            }
        }
    }
    selectedOrderLineItem.bonusDetailsArray=bonusDetailsArray;
    
}
-(void)updateBrandCodeCell:(SalesWorxSalesOrderProductsTableViewBrandcodeCell*)cell isExpanded:(BOOL)isExpanded
{
    if(!isExpanded)
    {
        cell.expandCollapseArrowImageView.image=[UIImage imageNamed:KSalesOrderProductTableViewDownArrowImageName];
        cell.backgroundColor=KSalesOrderProductTableViewBrandCodeCellCollapsedColor;
        cell.brandCodeLabel.textColor=KSalesOrderProductTableViewBrandCodeCellCollapsedTextColor;
    }
    else
    {
        cell.expandCollapseArrowImageView.image=[UIImage imageNamed:KSalesOrderProductTableViewUpArrowImageName];
        cell.backgroundColor=KSalesOrderProductTableViewBrandCodeCellExpandColor;
        cell.brandCodeLabel.textColor=KSalesOrderProductTableViewBrandCodeCellExpandTextColor;
    }

}


#pragma mark ProductDetailsMethods

-(IBAction)productDescriptionInfoButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    SalesWorxSalesOrderProductImageDescriptionViewController *objVC = [[SalesWorxSalesOrderProductImageDescriptionViewController alloc]initWithNibName:@"SalesWorxSalesOrderProductImageDescriptionViewController" bundle:[NSBundle mainBundle]];
    objVC.view.backgroundColor = [UIColor clearColor];
    objVC.modalPresentationStyle = UIModalPresentationCustom;
    objVC.selectedProduct = selectedOrderLineItem.OrderProduct;
    [self.navigationController presentViewController:objVC animated:NO completion:nil];
}
-(IBAction)productStockInfoButtonTapped:(id)sender
{
    [self.view endEditing:YES];

    SalesOrderStockInfoViewController *salesOrderStockInfoViewController=[[SalesOrderStockInfoViewController alloc]initWithNibName:@"SalesOrderStockInfoViewController" bundle:[NSBundle mainBundle]];
    salesOrderStockInfoViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderStockInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderStockInfoViewController.OrderLineItem=selectedOrderLineItem;
    salesOrderStockInfoViewController.wareHouseId=currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID;
    [self.navigationController presentViewController:salesOrderStockInfoViewController animated:NO completion:nil];
}
-(IBAction)productBonusInfoButtonTapped:(id)sender
{
    [self.view endEditing:YES];

    if(selectedOrderLineItem.OrderProduct.bonus>0)
    {
        NSMutableArray *tempProductAsstBonusPlans=[[NSMutableArray alloc]init];;
        if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
            NSMutableArray  *ProductAssortmentPlans=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B on A.Item_Code=B.Item_Code Where A.Item_Code='%@' and Is_Get_Item='N'",selectedOrderLineItem.OrderProduct.Item_Code]];
            if(ProductAssortmentPlans.count>0)
            {
                ProductAssortmentPlans= [ProductAssortmentPlans valueForKey:@"Assortment_Plan_ID"];
                
               tempProductAsstBonusPlans= [[currentVisit.visitOptions.salesOrder.AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId IN %@", ProductAssortmentPlans]] mutableCopy];
            }

        }
        if(tempProductAsstBonusPlans.count>0){
            SalesWorxAssortmentBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesWorxAssortmentBonusInfoViewController alloc]initWithNibName:@"SalesWorxAssortmentBonusInfoViewController" bundle:[NSBundle mainBundle]];
            salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
            salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
            salesOrderBonusInfoViewController.plan=[tempProductAsstBonusPlans objectAtIndex:0];
            [self.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
        }
        else{
            SalesOrderBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesOrderBonusInfoViewController alloc]initWithNibName:@"SalesOrderBonusInfoViewController" bundle:[NSBundle mainBundle]];
            salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
            salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
            [self getBonusDetails];
            salesOrderBonusInfoViewController.productsArray=productsArray;
            salesOrderBonusInfoViewController.OrderLineItem=selectedOrderLineItem;
            salesOrderBonusInfoViewController.bonusArray=selectedOrderLineItem.bonusDetailsArray;
            [self.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"Bonus not available" withController:self];
    }
    

}
-(IBAction)productSalesHistoryInfoButtonTapped:(id)sender
{
    [self.view endEditing:YES];

    SalesOrderSalesHistoryInfoViewController *salesOrderSalesHistoryInfoViewController=[[SalesOrderSalesHistoryInfoViewController alloc]initWithNibName:@"SalesOrderSalesHistoryInfoViewController" bundle:[NSBundle mainBundle]];
    salesOrderSalesHistoryInfoViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderSalesHistoryInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderSalesHistoryInfoViewController.OrderLineItem=selectedOrderLineItem;
    salesOrderSalesHistoryInfoViewController.customerVisit=currentVisit;
    [self.navigationController presentViewController:salesOrderSalesHistoryInfoViewController animated:NO completion:nil];

   
}
-(IBAction)addToOrderButtonTapped:(id)sender
{
    if ([appControl.ENABLE_ALERT_NO_BONUS isEqualToString:KAppControlsYESCode] && selectedOrderLineItem.bonusDetailsArray.count==0) {
        if(selectedOrderLineItem.OrderProductQty.doubleValue<=0){
            [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please enter quantity"  withController:nil];
            return;
        }
        [UIView animateWithDuration:0 animations: ^{
            [self.view endEditing:YES];
            [self CheckAndADDOrUpdateProductToOrderLineItems];
            [self mandatoryNotes];
            
            if(isUpdatingOrder){
                selectedOrderLineItem.isOHDuplicateItemEntryAlertConfirmedByUser=YES;
                selectedOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser=YES;
            }
            
        } completion: ^(BOOL finished) {
            // [self CheckAndADDOrUpdateProductToOrderLineItems];
            [self mandatoryNotes];
        }];
    }
    else{
            [UIView animateWithDuration:0 animations: ^{
            [self.view endEditing:YES];
            
            if(isUpdatingOrder){
                selectedOrderLineItem.isOHDuplicateItemEntryAlertConfirmedByUser=YES;
                selectedOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser=YES;
            }

        } completion: ^(BOOL finished) {
            [self CheckAndADDOrUpdateProductToOrderLineItems];
        }];
    }
}
-(void)mandatoryNotes{
    if(!isUpdatingOrder){
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:KTitleStrAlert
                                     message:@"Notes are Mandatory"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        SalesOrderNotesViewConrollerViewController *salesOrderNotesViewConrollerViewController=[[SalesOrderNotesViewConrollerViewController alloc]initWithNibName:@"SalesOrderNotesViewConrollerViewController" bundle:[NSBundle mainBundle]];
                                        salesOrderNotesViewConrollerViewController.view.backgroundColor = [UIColor clearColor];
                                        salesOrderNotesViewConrollerViewController.modalPresentationStyle = UIModalPresentationCustom;
                                        
                                        //                                    SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];
                                        //                                    salesOrderNotesViewConrollerViewController.notesText=swipedOrderItem.NotesStr;
                                        salesOrderNotesViewConrollerViewController.salesOrderNotesViewConrollerViewControllerDelegate=self;
                                        [self.navigationController presentViewController:salesOrderNotesViewConrollerViewController animated:NO completion:nil];
                                        
                                    }];
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(void)CheckAndADDOrUpdateProductToOrderLineItems
{
    
    /** alloted lots quantity sum*/
    NSNumber *quantityAlloted = [selectedOrderLineItem.AssignedLotsArray valueForKeyPath:@"@sum.assignedQuantity"];
    /** Duplicate Items Checking*/
    NSPredicate * duplicateItemPredicate=[NSPredicate predicateWithFormat:@"SELF.OrderProduct.selectedUOM=%@ && SELF.OrderProduct.Item_Code=%@",selectedOrderLineItem.OrderProduct.selectedUOM,selectedOrderLineItem.OrderProduct.Item_Code];
    NSArray *duplicateItemArray=  [orderLineItemsArray filteredArrayUsingPredicate:duplicateItemPredicate];
    
    NSLog(@"adding product to the order %@ \n qty %f",[SWDefaults getValidStringValue:selectedOrderLineItem.OrderProduct.Description],selectedOrderLineItem.OrderProductQty.doubleValue);
    
    
    
    NSMutableArray *duplicateSelecetdIteOrderHistoryArray=[[NSMutableArray alloc]init];
    if(appControl.OH_DUPLICATE_ITEM_CHECK_PERIOD.integerValue>0){
        NSString *duplicateOHItemsQuery=[NSString stringWithFormat:@"select A.Inventory_Item_ID,B.Creation_Date As OrderDate,A.Orig_Sys_Document_Ref AS OrderNumber,A.Ordered_Quantity AS OrderQuantity from TBL_Order_History_Line_Items AS A  Left JOIN  TBL_Order_History AS B ON A.Orig_Sys_Document_Ref=B.Orig_Sys_Document_Ref  where date(B.Creation_date)>date('now','-%@ days') AND B.Ship_To_Customer_ID='%@' AND B.Ship_To_Site_ID='%@' AND A.Inventory_Item_ID='%@' AND A.Order_Quantity_UOM='%@' AND A.Calc_Price_Flag='N' AND B.Doc_Type='I' AND B.Creation_Date IS NOT NULL  AND A.Ordered_Quantity IS NOT NULL",appControl.OH_DUPLICATE_ITEM_CHECK_PERIOD,currentVisit.visitOptions.customer.Ship_Customer_ID,currentVisit.visitOptions.customer.Ship_Site_Use_ID,selectedOrderLineItem.OrderProduct.Inventory_Item_ID,selectedOrderLineItem.OrderProduct.selectedUOM];
        NSLog(@"duplicateOHItemsQuery %@",duplicateOHItemsQuery);
        duplicateSelecetdIteOrderHistoryArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:duplicateOHItemsQuery];
    }
    
    
    if([selectedOrderLineItem.OrderProductQty isLessThanDecimal:[NSDecimalNumber decimalNumberWithString:@"0"]])
    {
        [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please enter quantity"  withController:nil];
    }
    else if([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode]&&
            ![selectedOrderLineItem.OrderProduct.primaryUOM isEqualToString:selectedOrderLineItem.OrderProduct.selectedUOM]&&
            ([SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[selectedOrderLineItem.OrderProduct copy]]<1)&&
            ![self isOrderItemWithValidDecimalOrderQuantity:selectedOrderLineItem]
            )
    {
        [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Please enter a valid quantity." withController:self];
        
    }
    else if((![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]) &&
            ([[sellingPriceTextField.text trimString]isEqualToString:@""] || sellingPriceTextField.text.doubleValue==0))
    {
        [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Selling price can not be zero" withController:self];
    }
    else if((selectedOrderLineItem.manualFOCQty>0 || FOCQtyTextField.text.length!=0) && (manualFOCTextField.text.length==0||
                                                                                         selectedOrderLineItem.manualFocProduct==nil))
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select the product to be given as FOC" withController:self];
    }
    else if(![self ValidateRequestedBonus])
    {
        NSLog(@"Can not proceed");
    }
    else if ([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]&&
             ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]&&
             [self IsAnyUpdateOrNewAsstBnsPlanAvailableThenShowBonusAllocation:[selectedOrderLineItem copy]]){
        
        NSLog(@"Assortment bonus pop up will be shown");
    }
    else if(isUpdatingOrder && [quantityAlloted doubleValue]>0 && ([quantityAlloted doubleValue]!=[orderQtyTextField.text doubleValue]))
    {
        [self showAlertAfterHidingKeyBoard:@"Lots allocation" andMessage:@"Please reallocate lots for updated order quantity" withController:nil];
        SalesOrderLotAssigningViewController *salesOrderLotAssigningViewController=[[SalesOrderLotAssigningViewController alloc]initWithNibName:@"SalesOrderLotAssigningViewController" bundle:[NSBundle mainBundle]];
        salesOrderLotAssigningViewController.salesOrderLotAssigningViewControllerDelegate=self;
        salesOrderLotAssigningViewController.view.backgroundColor = [UIColor clearColor];
        salesOrderLotAssigningViewController.modalPresentationStyle = UIModalPresentationCustom;
        salesOrderLotAssigningViewController.wareHouseId=currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID;
        salesOrderLotAssigningViewController.OrderLineItem=selectedOrderLineItem;
        salesOrderLotAssigningViewController.isReAllocationForUpdatedQuantity=YES;
        [self.navigationController presentViewController:salesOrderLotAssigningViewController animated:NO completion:nil];
    }
    else if(!isUpdatingOrder &&
            !selectedOrderLineItem.isOHDuplicateItemEntryAlertConfirmedByUser &&
            appControl.OH_DUPLICATE_ITEM_CHECK_PERIOD.integerValue!=0 &&
            duplicateSelecetdIteOrderHistoryArray.count>0 &&
            duplicateItemArray.count==0){
        /** show confirmation pop up on adding the duplicate order history item from user*/
        
        
        SalesWorxSalesOrderItemDuplicateOHViewController *oHController=[[SalesWorxSalesOrderItemDuplicateOHViewController alloc]init];
        oHController.oHDelegate=self;
        oHController.view.backgroundColor = [UIColor clearColor];
        oHController.modalPresentationStyle = UIModalPresentationCustom;
        oHController.productOrderHistoryArray=duplicateSelecetdIteOrderHistoryArray;
        [self.navigationController presentViewController:oHController animated:NO completion:nil];
        
//             UIAlertAction* noButtonAct = [UIAlertAction
//                                          actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
//                                          style:UIAlertActionStyleDefault
//                                          handler:^(UIAlertAction * action)
//                                          {
//                                              [self clearAllProductLabelsAndTextFields];
//                                              [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
//                                          }];
//            UIAlertAction* yesButtonAct = [UIAlertAction
//                                           actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
//                                           style:UIAlertActionStyleDefault
//                                           handler:^(UIAlertAction * action)
//                                           {
//                                               selectedOrderLineItem.isOHDuplicateItemEntryAlertConfirmedByUser=YES;
//                                               selectedOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser=YES;
//
//                                               [self addOrUpdateProduct];
//                                           }];
//            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Warning" andMessage:@"This item has been Ordered few days back.Would you like to order again?" andActions:[[NSMutableArray alloc] initWithObjects:noButtonAct,yesButtonAct, nil] withController:self];
        
    }
    else if( ([appControl.DUPLICATE_ITEM_ENTRY_CHECK isEqualToString:KAppControlsNOCode] ||
              [appControl.DUPLICATE_ITEM_ENTRY_CHECK isEqualToString:KAppControlsConfirmCode] )&&
              !isUpdatingOrder &&
              duplicateItemArray.count>0 &&
              !selectedOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser)
    {
        if([appControl.DUPLICATE_ITEM_ENTRY_CHECK isEqualToString:KAppControlsNOCode])
        {
            [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"This item already added to order" withController:nil];
        }
        else
        {
            if ([appControl.ENABLE_ALERT_NO_BONUS isEqualToString:KAppControlsYESCode] && selectedOrderLineItem.bonusDetailsArray.count==0)
            {
                UIAlertController * alert = [UIAlertController
                                          alertControllerWithTitle:kNoData
                                          message:@"This item has been already added to the order.Are you sure you want to add it again?"
                                          preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noButtonAct = [UIAlertAction
                                              actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  [self clearAllProductLabelsAndTextFields];
                                                  [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
                                                  // [alert dismissViewControllerAnimated:YES completion:nil];
                                              }];
                UIAlertAction* yesButtonAct = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   selectedOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser=YES;
                                                   [self addOrUpdateProduct];
                                                   [self mandatoryNotes];
                                                   // [alert dismissViewControllerAnimated:YES completion:nil];
                                               }];
                [alert addAction:noButtonAct];
                [alert addAction:yesButtonAct];
                // [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Warning" andMessage:@"This item has been already added to the order.Are you sure you want to add it again?" andActions:[[NSMutableArray alloc] initWithObjects:noButtonAct,yesButtonAct, nil] withController:self];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            else{
                [UIView animateWithDuration:0 animations: ^{
                    [self.view endEditing:YES];
                    
                } completion: ^(BOOL finished) {
                    //                    UIAlertController * alert=   [UIAlertController
                    //                                                  alertControllerWithTitle:@"Warning"
                    //                                                  message:@"This item has been already added to the order.Are you sure you want to add it again?"
                    //                                                  preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* noButtonAct = [UIAlertAction
                                                  actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action)
                                                  {
                                                      [self clearAllProductLabelsAndTextFields];
                                                      [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
                                                      // [alert dismissViewControllerAnimated:YES completion:nil];
                                                  }];
                    UIAlertAction* yesButtonAct = [UIAlertAction
                                                   actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                                                   {
                                                       selectedOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser=YES;
                                                       [self addOrUpdateProduct];
                                                       // [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
                    // [alert addAction:noButtonAct];
                    //[alert addAction:yesButtonAct];
                    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Warning" andMessage:@"This item has been already added to the order.Are you sure you want to add it again?" andActions:[[NSMutableArray alloc] initWithObjects:noButtonAct,yesButtonAct, nil] withController:self];
                    //[self presentViewController:alert animated:YES completion:nil];
                }];
                
            }
        }
    }
    
    else
    {
        [self addOrUpdateProduct];
    }
    
}
-(void)addOrUpdateProduct
{
    
    if(selectedOrderLineItem.OrderProductQty.doubleValue<=0){
        [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please enter quantity"  withController:nil];
        return;
    }
    
    isManageSalesOrderUpdated=YES;
    
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]&&
       ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
        [self AddOrUpdateAsstBonusPlanForOrderLineItems];
    }else{
        if(isUpdatingOrder){
            [orderLineItemsArray replaceObjectAtIndex:selectedOrderItemIndexpath.section withObject:selectedOrderLineItem];
        }
        else{
            [orderLineItemsArray insertObject:selectedOrderLineItem atIndex:0];
        }
    }
    

    [self updateSalesOrderTotalAmount];
    
    
    if(isUpdatingOrder){
        [self DeselectTableViewsAndUpdate];
    }
    else{
        [self clearAllProductLabelsAndTextFields];
        [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
        showOrderTableViewNewItemAnimationColor=NO;
        [OrdersTableView reloadData];
        [NSTimer scheduledTimerWithTimeInterval:0.1
                                         target:self
                                       selector:@selector(showOrderTableViewNewItemAddAnimation)
                                       userInfo:nil
                                        repeats:NO];
    }

}
-(void)showOrderTableViewNewItemAddAnimation
{
    [OrdersTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    SalesWorxSalesOrdersTableCellTableViewCell *Cell=[OrdersTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    Cell.lineItemRowAnimationView.layer.sublayers=nil;
    //create the initial and the final path.
    UIBezierPath *fromPath = [UIBezierPath bezierPathWithRect:CGRectMake(Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height, 1, Cell.lineItemRowAnimationView.bounds.size.height)];
    UIBezierPath *toPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height)];
    //create the shape layer that will be animated
    CAShapeLayer *fillingShape = [CAShapeLayer layer];
    fillingShape.path = fromPath.CGPath;
    fillingShape.fillColor = [UIColor colorWithRed:(30.0/255.0) green:(216.0/255.0) blue:(255.0/255.0) alpha:1.0].CGColor;
    //create the animation for the shape layer
    CABasicAnimation *animatedFill = [CABasicAnimation animationWithKeyPath:@"path"];
    animatedFill.duration = 1.0f;
    animatedFill.fromValue = (id)fromPath.CGPath;
    animatedFill.toValue = (id)toPath.CGPath;
    
    
    //create the initial and the final path.
    UIBezierPath *fromPath1 = [UIBezierPath bezierPathWithRect:CGRectMake(Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height, 1, Cell.lineItemRowAnimationView.bounds.size.height)];
    UIBezierPath *toPath1 = [UIBezierPath bezierPathWithRect:CGRectMake(Cell.lineItemRowAnimationView.bounds.size.width/2, 0,Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height)];
    //create the shape layer that will be animated
    CAShapeLayer *fillingShape1= [CAShapeLayer layer];
    fillingShape1.path = fromPath1.CGPath;
    fillingShape1.fillColor = [UIColor colorWithRed:(30.0/255.0) green:(216.0/255.0) blue:(255.0/255.0) alpha:1.0].CGColor;
    //create the animation for the shape layer
    CABasicAnimation *animatedFill1 = [CABasicAnimation animationWithKeyPath:@"path"];
    animatedFill1.duration = 1.0f;
    animatedFill1.fromValue = (id)fromPath1.CGPath;
    animatedFill1.toValue = (id)toPath1.CGPath;
    
    //using CATransaction like this we can set a completion block
    [CATransaction begin];
    
    [CATransaction setCompletionBlock:^{
        //when the animation ends, we want to set the proper color itself!
        Cell.lineItemRowAnimationView.backgroundColor = KNewRowAnimationColor;
        showOrderTableViewNewItemAnimationColor=YES;
       // [OrdersTableView reloadData];
    }];
    
    //add the animation to the shape layer, then add the shape layer as a sublayer on the view changing color
    [fillingShape addAnimation:animatedFill forKey:@"path"];
    [fillingShape1 addAnimation:animatedFill1 forKey:@"path"];

    [Cell.lineItemRowAnimationView.layer addSublayer:fillingShape];
    [Cell.lineItemRowAnimationView.layer addSublayer:fillingShape1];

    [CATransaction commit];
}
-(IBAction)clearButtonTapped:(id)sender
{
    [self.view endEditing:YES];

    if(selectedProductIndexpath!=nil)/** product tableview selected*/
    {
        [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
    }
    else
    {
    
        selectedOrderItemIndexpath=nil;
        [OrdersTableView reloadData];
        selectedProductIndexpath=nil;
        [productsTableView reloadData];
        
        selectedOrderLineItem=nil;
        selectedProduct=nil;
        [self hideProductDetailsView];
        [self hideProductOrderDetailsView];
        [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
        [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];

        [self updateSalesOrderTotalAmount];
    }
}


-(void)DeselectTableViewsAndUpdate
{
    selectedOrderItemIndexpath=nil;
    [OrdersTableView reloadData];
    selectedProductIndexpath=nil;
    [productsTableView reloadData];
    
    selectedOrderLineItem=nil;
    selectedProduct=nil;
    [self hideProductDetailsView];
    [self hideProductOrderDetailsView];
    [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
    [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];

    [self updateSalesOrderTotalAmount];
}

-(void)clearAllProductLabelsAndTextFields
{
    productAvailableStockLabel.text=@"N/A";
    productRetailPriceLabel.text=@"N/A";
    productWholeSalePriceLabel.text=@"N/A";
    productNearestExpiryDateLabel.text=@"N/A";
    productDefaultDiscountLabel.text=@"N/A";
    productSpecialDiscountLabel.text=@"N/A";
    productBonusItemLabel.text=@"N/A";
    productDefaultBonusLabel.text=@"N/A";
    
    orderQtyTextField.text=@"";
    UOMTextField.text=@"";
    sellingPriceTextField.text=@"";
    discountTextField.text=@"";
    requestedBonusTextField.text=@"";
    manualFOCTextField.text=@"";
    FOCQtyTextField.text=@"";
    
    OrderItemTotalAmountLabel.text=@"N/A";
    OrderItemDiscountAmountLabel.text=@"N/A";
    OrderItemNetAmountLabel.text=@"N/A";
}

-(id)fetchProductPriceListData:(Products*)Product
{
    NSMutableArray *  productPriceListArray=[[NSMutableArray alloc]init];
    PriceList* priceList=[[PriceList alloc ]init];
    productPriceListArray=[[SWDatabaseManager retrieveManager]fetchProductPriceList:Product withCustomer:currentVisit.visitOptions.customer];
    
    if (productPriceListArray.count>0) {
        
        NSLog(@"product price list response is %@", productPriceListArray);
        
        
        //now refine using a predicate and display the price based on selected uom
        NSPredicate * uomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM ==[cd] %@",Product.selectedUOM];
        
        NSMutableArray* filteredPredicateArray=[[productPriceListArray filteredArrayUsingPredicate:uomPredicate] mutableCopy];
        NSLog(@"filtered UOM data is %@", filteredPredicateArray);
        if (filteredPredicateArray.count>0) {
            priceList=[filteredPredicateArray objectAtIndex:0];
            return priceList;

        }
        
    }
    return nil;

}







#pragma mark LayOutConstarinsMethods
-(void)showProductDetailsView
{
    [productDetailsView setHidden:NO];
    productDetailsViewTopConstraiint.constant=KSalesOrderScreenProductDetailsViewTopMargin;
    productDetailsViewHeightConstraiint.constant=KSalesOrderScreenProductDetailsViewHeight;

}
-(void)hideProductDetailsView
{
    [productDetailsView setHidden:YES];
    productDetailsViewTopConstraiint.constant=KZero;
    productDetailsViewHeightConstraiint.constant=KZero;
}
-(void)showProductOrderDetailsView
{
    [productOrderDetailsView setHidden:NO];
    productOrderDetailsViewTopConstraiint.constant=KSalesOrderScreenProductOrderDetailsViewTopMargin;
  if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])    {
    productOrderDetailsViewHeightConstraiint.constant=KSalesOrderScreenProductFOCOrderDetailsViewHeight;

    }
    else
    {
        productOrderDetailsViewHeightConstraiint.constant=KSalesOrderScreenProductOrderDetailsViewHeight;

    }
}
-(void)hideProductOrderDetailsView
{
    [productOrderDetailsView setHidden:YES];
    productOrderDetailsViewTopConstraiint.constant=KZero;
    productOrderDetailsViewHeightConstraiint.constant=KZero;
}
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    NoSelectionTopMarginConstraint.constant=KZero;
}
-(void)showNoSelectionViewWithMessage:(NSString *)message WithHeight:(NSInteger)height
{
    [NoSelectionView setHidden:NO];
    NoSelectionMessageLabel.text=message;
    NoSelectionHeightConstraint.constant=height;
    NoSelectionTopMarginConstraint.constant=KSalesOrderScreenNoSelectionsViewTopMargin;
    
}
-(void)updateTheProductTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    /* deselecting the previous selected cell*/

    if(selectedProductIndexpath!=nil)
    {
        MedRepUpdatedDesignCell *previousSelectedCell=[productsTableView cellForRowAtIndexPath:selectedProductIndexpath];
        [previousSelectedCell setDeSelectedCellStatus];

        previousSelectedCell.cellDividerImg.hidden=NO;
    }

    /*seclecting the present selected cell*/
    MedRepUpdatedDesignCell *currentSelectedcell=[productsTableView cellForRowAtIndexPath:indexpath];
    [currentSelectedcell setSelectedCellStatus];

    currentSelectedcell.cellDividerImg.hidden=YES;

    selectedProductIndexpath=indexpath;
}
-(void)updateTheOrderTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    
    /* deselecting the previous selected cell*/
    
    if(selectedOrderItemIndexpath!=nil)
    {
        SalesWorxSalesOrdersTableCellTableViewCell *previousSelectedCell=[OrdersTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedOrderItemIndexpath.section]];
        previousSelectedCell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
    }
    
    /*seclecting the present selected cell*/
    SalesWorxSalesOrdersTableCellTableViewCell *currentSelectedcell=[OrdersTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexpath.section]];
    currentSelectedcell.ProductNameLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.QunatutyLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.UnitPriceLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.ToatlLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.DiscountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.NetAmountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;

    selectedOrderItemIndexpath=indexpath;
}


#pragma mark Filter Button Action



- (IBAction)filterButtonTapped:(id)sender {
    isSearching=NO;
    if (productsArray.count>0) {
        [productsSearchBar setShowsCancelButton:NO animated:YES];
         productsSearchBar.text=@"";
        [productsSearchBar resignFirstResponder];
        [self.view endEditing:YES];
        [self searchProductsContent:@""];
        selectedProductIndexpath=nil;
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        // Blurred with UIImage+ImageEffects
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        
        [self.view addSubview:blurredBgImage];
        
        UIButton* searchBtn=productsFilterButton;
        SalesWorxProductsFilterViewController * popOverVC=[[SalesWorxProductsFilterViewController alloc]init];
        popOverVC.delegate=self;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict=previousFilteredParameters;
            
        }
        
//        if(sender==productsFilterButton){
//            popOverVC.isFromSearchBar=NO;
//
//        }
//        else if(sender==productsSearchBar){
//            popOverVC.isFromSearchBar=YES;
//        }
        
        popOverVC.productsArray=productsArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 580) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
        
        CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];

        
        
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
    else{
        [self showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later"  withController:nil];
    }
    
}


-(void)filteredProducts:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    filteredProductsArray=filteredArray;
    [self updateProductTableViewOnFilter];
}
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    previousFilteredParameters=parametersDict;
}
-(void)productsFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    filteredProductsArray=productsArray;
    [self updateProductTableViewOnFilter];
    
}
-(void)productsFilterDidClose
{
    [blurredBgImage removeFromSuperview];
    
}

#pragma mark SearchBarDelegate Methods
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
 if([appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode])
 {
     
     
     if(!isSearchBarClearButtonTapped)
     {
         
         NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:@""];
         NSMutableArray *popOverProductsArray;
         if(predicateArray.count>0)
         {
             NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
             NSMutableArray *tempProductsArray=[productsArray mutableCopy];
             popOverProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
         }
         else{
             popOverProductsArray=productsArray;
         }
         
         SalesWorxProductNameSearchPopOverViewController * popOverVC=[[SalesWorxProductNameSearchPopOverViewController alloc]init];
         popOverVC.popOverContentArray=popOverProductsArray;
         popOverVC.salesWorxPopOverControllerDelegate=self;
         popOverVC.disableSearch=NO;
         popOverVC.popoverType=KVisitOptionsProductNameSearchPopOverTitle;
        // popOverVC.filterText=searchBar.text;
         popOverTitle=KVisitOptionsProductNameSearchPopOverTitle;
         
         UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
         FOCPopOverController=nil;
         FOCPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
         FOCPopOverController.delegate=self;
         popOverVC.popOverController=FOCPopOverController;
         popOverVC.titleKey=KVisitOptionsProductNameSearchPopOverTitle;
         [FOCPopOverController setPopoverContentSize:CGSizeMake(300, 500) animated:YES];
         [FOCPopOverController presentPopoverFromRect:searchBar.frame inView:searchBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
         [self.view resignFirstResponder];
     }
     //[self filterButtonTapped:searchBar];
     isSearchBarClearButtonTapped=NO;

     return NO;

 }
 else{
     return YES;
 }
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [productsSearchBar setShowsCancelButton:YES animated:YES];
    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode])
    {
        xSearchBarTrailingspaceToFilterButton.constant=8;
        [barcodeScanButton setHidden:YES];
    }
  
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [productsSearchBar setShowsCancelButton:NO animated:YES];

}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if([appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode])
    {
        if([searchText length] == 0) {
            // clearButton Tapped
            isSearchBarClearButtonTapped=YES;
        }
    }

    if([searchText length] != 0) {
        //    if([searchText length] >= 3) {
        isSearching = YES;
        selectedProductIndexpath=nil;
        
        [self searchProductsContent:searchBar.text];
    }
    else {
        isSearching = NO;
        if (productsArray.count>0) {
            [self searchProductsContent:@""];
        }
    }


}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [productsSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    [self.view endEditing:YES];
    if (productsArray.count>0) {
        [self searchProductsContent:@""];
    }
    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode])
    {
        xSearchBarTrailingspaceToFilterButton.constant=47;
        [barcodeScanButton setHidden:NO];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}
-(NSMutableArray *)getSearchFieldPredicateArray:(NSString *)searchString
{
    NSString* filter = @"%K CONTAINS[cd] %@";
    
    
    NSMutableArray *predicateArray=[[NSMutableArray alloc]init];
    if([[previousFilteredParameters valueForKey:@"Brand_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[previousFilteredParameters valueForKey:@"Brand_Code"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Description"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[previousFilteredParameters valueForKey:@"Description"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Item_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[previousFilteredParameters valueForKey:@"Item_Code"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Stock"] length]>0) {
        
        if ([[previousFilteredParameters valueForKey:@"Stock"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue == 0"]];
        }
    }
    if([[previousFilteredParameters valueForKey:@"Bonus"] length]>0) {
        
        if ([[previousFilteredParameters valueForKey:@"Bonus"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue == 0"]];
        }
    }
    
    NSString* promoItemStatus=[previousFilteredParameters valueForKey:@"Promo_Item"];
    
    if ([NSString isEmpty:promoItemStatus]==NO) {
        
        if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsYESCode]];
        }
        
        else if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kUnAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsNOCode]];
        }
        else
        {
            
        }
        
    }
    
    NSPredicate *BrandCodeSearchPredicate = [NSPredicate predicateWithFormat:filter, @"Brand_Code", searchString];
    NSPredicate *DescriptionSearchPredicate = [SWDefaults fetchMultipartSearchPredicate:searchString withKey:@"Description"];
    NSPredicate *BarCodeSearchPredicate = [NSPredicate predicateWithFormat:filter, @"ProductBarCode", searchString];

    NSMutableArray *searchBarPredicatesArray=[[NSMutableArray alloc]init];
    if(![searchString isEqualToString:@""])
    {
        [searchBarPredicatesArray addObject:BrandCodeSearchPredicate];
        [searchBarPredicatesArray addObject:DescriptionSearchPredicate];
        [searchBarPredicatesArray addObject:BarCodeSearchPredicate];

    }
    if(predicateArray.count>0 || searchBarPredicatesArray.count>0)
    {
        
        if(searchBarPredicatesArray.count>0)
        {
            NSPredicate *searchBarPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:searchBarPredicatesArray];
            [predicateArray addObject:searchBarPredicate];
        }
    }
    
    return predicateArray;
}

-(void)searchProductsContent:(NSString*)searchString
{
    if (searchString.length>=0 || searchString.length==0) {
        
     searchStartTime=[NSDate date];
    
      NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:searchString];
    if(predicateArray.count>0)
    {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        NSMutableArray *tempProductsArray=[productsArray mutableCopy];
        filteredProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
        NSLog(@"filtered Products count %lu", (unsigned long)filteredProductsArray.count);
        if (filteredProductsArray.count>0) {
            [self updateProductTableViewOnFilter];
        }
        else if (filteredProductsArray.count==0)
        {
            isSearching=YES;
            [self updateProductTableViewOnFilter];
        }
        
    }
    else
    {
        filteredProductsArray=productsArray;
        [self updateProductTableViewOnFilter];

    }
    
    if(!isUpdatingOrder)
    {
        selectedProductIndexpath=nil;
        [self hideProductDetailsView];
        [self hideProductOrderDetailsView];
        [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
        [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];
    }
    }
    else{
        NSLog(@"search string not greter than 3 characters");
        
    }
}


#pragma mark UIPopOver delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController == filterPopOverController) {
        
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)updateProductTableViewOnFilter
{
    productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsArray valueForKeyPath:@"@distinctUnionOfObjects.Brand_Code"]];
    productsBrandCodesArray= [[NSMutableArray alloc]initWithArray:[productsBrandCodesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];

    
    
    /** removing invalid brandcodes*/
    //[productsBrandCodesArray removeObject:@""];
    filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
    allowSearchBarEditing=NO;
    [self showActivityIndicatorInSearchBar];
    [productsTableView reloadData];

    
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        /*filtering products by brand code*/
//        for (NSString *productBrandCode in productsBrandCodesArray) {
//            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
//                                                @"Brand_Code=%@",productBrandCode];
//            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[filteredProductsArray filteredArrayUsingPredicate:brandCodePredicate]];
//            [filteredProductsArrayGroupedByBrandCode addObject:tempProductsArray];
//        }
        filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];

        dispatch_async(dispatch_get_main_queue(), ^(void){
            selectedProductIndexpath=nil;
            allowSearchBarEditing=YES;
            [self hideActivityIndicatorInSearchBar];
            if(productsBrandCodesArray.count==1)
            {
                [self ExpandFirstProductsSection];
            }
            else
            {
                ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];

            }
            
            [productsTableView reloadData];
            
               /** if only one product is there in table view , selecting the first product*/
                if(filteredProductsArray.count==1 && /*&& [appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode]*/ (proSearchType==ProductSearchByPopOver || proSearchType==ProductSearchByBarCode))
                {
                    [self tableView:productsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                    //[orderQtyTextField becomeFirstResponder];
                    proSearchType=ProductSerachNormal;
                }
         });
    });
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0); // called before text changes
{
    if(allowSearchBarEditing)
    return YES;
    else
        return NO;
        
}

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    
    
    [self.view endEditing:YES];

    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.popoverType=popOverString;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];

    popOverVC.titleKey=popOverString;
    if([popOverString isEqualToString:KSalesOrderUOMPopOverTitle])
    {
        popOverVC.preferredContentSize = CGSizeMake(250, 250);
        popOverVC.disableSearch=YES;
        
    }
    else{
        popOverVC.preferredContentSize = CGSizeMake(300, 300);
        popOverVC.disableSearch=NO;
    }
    popover.delegate = self;
    popOverVC.titleKey=popOverString;
    popover.sourceView = selectedTextField.rightView;
    popover.sourceRect =selectedTextField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{

        
    }];
    
    
    
//    [self.view endEditing:YES];
//    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
//    popOverVC.popOverContentArray=contentArray;
//    popOverVC.salesWorxPopOverControllerDelegate=self;
//    popOverVC.popoverType=popOverString;
//   
//    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
//    FOCPopOverController=nil;
//    FOCPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
//    FOCPopOverController.delegate=self;
//    popOverVC.popOverController=FOCPopOverController;
//    popOverVC.titleKey=popOverString;
//    
//    if([popOverString isEqualToString:KSalesOrderUOMPopOverTitle])
//    {
//        [FOCPopOverController setPopoverContentSize:CGSizeMake(250, 300) animated:YES];
//        popOverVC.disableSearch=YES;
//
//    }
//    else{
//        [FOCPopOverController setPopoverContentSize:CGSizeMake(300, 300) animated:YES];
//        popOverVC.disableSearch=NO;
//    }
//    [FOCPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma POPOVER DELEGATE METHODS
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath
{
    
}
-(void)didDismissPopOverController
{
    
}


-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
 
    if ([popOverTitle isEqualToString:KSalesOrderManualFocPopOverTitle]) {
        manualFOCTextField.text=[[manualFocProductsArray valueForKey:@"Description"]objectAtIndex:selectedIndexPath.row];
        selectedOrderLineItem.manualFocProduct=[manualFocProductsArray objectAtIndex:selectedIndexPath.row];/** its already primary uom product*/
        [FOCPopOverController dismissPopoverAnimated:YES];
        FOCPopOverController=nil;
    }
    else if ([popOverTitle isEqualToString:@"More"]) {
        [FOCPopOverController dismissPopoverAnimated:NO];
        FOCPopOverController=nil;
        if([[selectedLineItemMoreOptionsArray objectAtIndex:selectedIndexPath.row] isEqualToString:KLineItemNotes])
        {
            SalesOrderNotesViewConrollerViewController *salesOrderNotesViewConrollerViewController=[[SalesOrderNotesViewConrollerViewController alloc]initWithNibName:@"SalesOrderNotesViewConrollerViewController" bundle:[NSBundle mainBundle]];
            salesOrderNotesViewConrollerViewController.view.backgroundColor = [UIColor clearColor];
            salesOrderNotesViewConrollerViewController.modalPresentationStyle = UIModalPresentationCustom;
            salesOrderNotesViewConrollerViewController.enableCancelButton = YES;
            
            
            SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];
            salesOrderNotesViewConrollerViewController.notesText=swipedOrderItem.NotesStr;
            salesOrderNotesViewConrollerViewController.salesOrderNotesViewConrollerViewControllerDelegate=self;
            [self.navigationController presentViewController:salesOrderNotesViewConrollerViewController animated:NO completion:nil];

        }
        if([[selectedLineItemMoreOptionsArray objectAtIndex:selectedIndexPath.row] isEqualToString:KLineItemSelectLots])

        {
        
                SalesOrderLotAssigningViewController *salesOrderLotAssigningViewController=[[SalesOrderLotAssigningViewController alloc]initWithNibName:@"SalesOrderLotAssigningViewController" bundle:[NSBundle mainBundle]];
                salesOrderLotAssigningViewController.salesOrderLotAssigningViewControllerDelegate=self;
                salesOrderLotAssigningViewController.view.backgroundColor = [UIColor clearColor];
                salesOrderLotAssigningViewController.modalPresentationStyle = UIModalPresentationCustom;
                salesOrderLotAssigningViewController.wareHouseId=currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID;
                SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];
                salesOrderLotAssigningViewController.OrderLineItem=swipedOrderItem;
                [self.navigationController presentViewController:salesOrderLotAssigningViewController animated:NO completion:nil];
            
        }


    }
    else if ([popOverTitle isEqualToString:KSalesOrderUOMPopOverTitle]) {
        
        NSMutableArray *priceListAvailableUOMsArray=[[SWDatabaseManager retrieveManager] getPriceListAvailableUOMSForSalesOrderProduct:selectedOrderLineItem.OrderProduct AndCustomer:currentVisit.visitOptions.customer];
        [self updateProductAndOrderDetailsUIOnUOMSelection:[priceListAvailableUOMsArray objectAtIndex:selectedIndexPath.row]];

        
        [FOCPopOverController dismissPopoverAnimated:YES];
        FOCPopOverController=nil;
    }
}

-(void)didSelectProductNameSearchPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:@""];
    NSMutableArray *popOverProductsArray;
    if(predicateArray.count>0)
    {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        NSMutableArray *tempProductsArray=[productsArray mutableCopy];
        popOverProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    else{
        popOverProductsArray=productsArray;
    }
    
    productsSearchBar.text=[[popOverProductsArray valueForKey:@"Description"]objectAtIndex:selectedIndexPath.row];
    proSearchType=ProductSearchByPopOver;
    [self searchProductsContent:productsSearchBar.text];
}
#pragma mark UITextField Delegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==manualFOCTextField) {
        popOverTitle=KSalesOrderManualFocPopOverTitle;
        [self presentPopoverfor:manualFOCTextField withTitle:KSalesOrderManualFocPopOverTitle withContent:manualFocProductsArray];
        return NO;
    }
    else if (textField==FOCQtyTextField)
    {
        
        if([appControl.ALLOW_DUAL_FOC isEqualToString:KAppControlsYESCode])
        {
            return YES;
        }
        else if(selectedOrderLineItem.OrderProductQty.doubleValue==0 || [orderQtyTextField.text isEqualToString:@""])
        {
            FOCQtyTextField.text=@"";
            [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please enter order quantity"  withController:nil];
            return NO;
        }

        else if(selectedOrderLineItem.defaultBonusQty==0)
        {
            return YES;
        }
        else
        {
            [self showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Dual bonus not allowed."  withController:nil];
            return NO;
        }
    }
    else if (textField==requestedBonusTextField)
    {
        if(selectedOrderLineItem.OrderProductQty.doubleValue==0 || [orderQtyTextField.text isEqualToString:@""])
        {
            FOCQtyTextField.text=@"";
            [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please enter order quantity"  withController:nil];
            return NO;
        }

       else  if(selectedOrderLineItem.defaultBonusQty<=0)
        {
            [self showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Bonus not available"  withController:nil];
            return NO;
        }
    }
    else if (textField==UOMTextField) {
        popOverTitle=KSalesOrderUOMPopOverTitle;
        
        NSMutableArray *priceListAvailableUOMsArray=[[SWDatabaseManager retrieveManager] getPriceListAvailableUOMSForSalesOrderProduct:selectedProduct AndCustomer:currentVisit.visitOptions.customer];
        [self presentPopoverfor:UOMTextField withTitle:KSalesOrderUOMPopOverTitle withContent:[priceListAvailableUOMsArray valueForKey:@"Item_UOM"]];
        return NO;
    }
    return YES;
}

-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
     [textField resignFirstResponder];
    return NO;
}

- (IBAction)SalesOrderTextFieldEditingChanged:(id)sender
{
    if (sender==orderQtyTextField)
    {

            selectedOrderLineItem.OrderProductQty=[NSDecimalNumber ValidDecimalNumberWithString:orderQtyTextField.text];
            [self calculateBonus];
            [self calculateDiscount];
            [self updateProductAmountLabels];
            [self updateFOCFields];
            [self updateBonusFields];
            [self updateRequiredBonusField];
            selectedOrderLineItem.requestedBonusQty=selectedOrderLineItem.defaultBonusQty;
            requestedBonusTextField.text=[NSString stringWithFormat:@"%0.0f",selectedOrderLineItem.requestedBonusQty];
            discountTextField.text=[NSString stringWithFormat:@"%0.2f",selectedOrderLineItem.appliedDiscount];

    }
    else if (sender==FOCQtyTextField)
    {
        
        
         if(appControl.ALLOW_DUAL_FOC)
        {
            selectedOrderLineItem.manualFOCQty=[FOCQtyTextField.text doubleValue];
        }
        else if(selectedOrderLineItem.defaultBonusQty==0)
        {
            selectedOrderLineItem.manualFOCQty=[FOCQtyTextField.text doubleValue];
        }
        [self calculateDiscount];
        discountTextField.text=[NSString stringWithFormat:@"%0.2f",selectedOrderLineItem.appliedDiscount];
        
    }
    else if (sender==discountTextField)
    {
        selectedOrderLineItem.appliedDiscount=[discountTextField.text doubleValue];
        [self calculateItemAmounts];
        [self updateProductAmountLabels];
    }
    else if(sender==sellingPriceTextField)
    {
        if([sellingPriceTextField.text doubleValue]==0)
        {
            selectedOrderLineItem.productSelleingPrice=0;
        }
        else
        {
            selectedOrderLineItem.productSelleingPrice=[[sellingPriceTextField.text AmountStringWithOutComas] doubleValue];
        }
        selectedOrderLineItem.appliedDiscount=[discountTextField.text doubleValue];
        [self calculateItemAmounts];
        [self updateProductAmountLabels];
        
    }

}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(sellingPriceTextField)
    {
        sellingPriceTextField.text=[sellingPriceTextField.text AmountStringWithOutComas];

    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
   
     if(textField==sellingPriceTextField)
    {
        sellingPriceTextField.text=[[NSString stringWithFormat:@"%f",selectedOrderLineItem.productSelleingPrice] floatString];
    }
     else if (textField==discountTextField)
     {
         discountTextField.text=[NSString stringWithFormat:@"%0.2f",[discountTextField.text doubleValue]];
         selectedOrderLineItem.appliedDiscount=[discountTextField.text doubleValue];
         [self updateProductAmountLabels];
         
     }
     else  if (textField==requestedBonusTextField)
     {
        // [self ValidateRequestedBonus];
     }
    

}


-(BOOL)ValidateRequestedBonus
{
    BOOL isRequestedBonusValid=YES;
      if([appControl.ALLOW_BONUS_CHANGE isEqualToString:@"Y"])
    {
        selectedOrderLineItem.requestedBonusQty=[requestedBonusTextField.text doubleValue];
    }
    else if([appControl.ALLOW_BONUS_CHANGE isEqualToString:@"I"])
    {
        if(selectedOrderLineItem.defaultBonusQty<=[requestedBonusTextField.text doubleValue])
        {
            selectedOrderLineItem.requestedBonusQty=[requestedBonusTextField.text doubleValue];
            
        }
        else
        {
            [self showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Bonus can increase only." withController:nil];
            isRequestedBonusValid=NO;
        }
    }
    else if([appControl.ALLOW_BONUS_CHANGE isEqualToString:@"D"])
    {
        if(selectedOrderLineItem.defaultBonusQty>=[requestedBonusTextField.text doubleValue])
        {
            selectedOrderLineItem.requestedBonusQty=[requestedBonusTextField.text doubleValue];
            
        }
        else
        {
            [self showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Bonus can decrease only." withController:nil];
            isRequestedBonusValid=NO;
        }
    }
    requestedBonusTextField.text=[NSString stringWithFormat:@"%0.0f",selectedOrderLineItem.requestedBonusQty];

   
    if(!isRequestedBonusValid)
    {
        [self calculateDiscount];
        discountTextField.text=[NSString stringWithFormat:@"%0.2f",selectedOrderLineItem.appliedDiscount];
    }
    
    return isRequestedBonusValid;
}
-(void)updateRequiredBonusField
{
    
    if(selectedOrderLineItem.defaultBonusQty==0)
    {
        [requestedBonusTextField setIsReadOnly:YES];
    }
    else if([appControl.ALLOW_BONUS_CHANGE isEqualToString:KAppControlsYESCode]|| [appControl.ALLOW_BONUS_CHANGE isEqualToString:@"I"] || [appControl.ALLOW_BONUS_CHANGE isEqualToString:@"D"])
    {
        [requestedBonusTextField setIsReadOnly:NO];
    }
    else
    {
        [requestedBonusTextField setIsReadOnly:YES];
    }

}
-(void)updateBonusFields
{
    if(selectedOrderLineItem.bonusProduct!=nil)
    {
        productBonusItemLabel.text=selectedOrderLineItem.bonusProduct.Description;
        productDefaultBonusLabel.text=[NSString stringWithFormat:@"%0.0f",selectedOrderLineItem.defaultBonusQty];
    }
    else
    {
        productBonusItemLabel.text=KNotApplicable;
        productDefaultBonusLabel.text=KNotApplicable;
    }
    
    /** for foc orders  bouns not allowed*/
    if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        productBonusItemLabel.text=KNotApplicable;
        productDefaultBonusLabel.text=KNotApplicable;
    }
}
-(void)updateProductAmountLabels
{
    OrderItemTotalAmountLabel.text=[[[NSString alloc]initWithFormat:@"%@",selectedOrderLineItem.OrderItemTotalamount] currencyString];
    OrderItemDiscountAmountLabel.text=[[[NSString alloc]initWithFormat:@"%@",selectedOrderLineItem.OrderItemDiscountamount] currencyString];
    OrderItemNetAmountLabel.text=[[[NSString alloc]initWithFormat:@"%@",selectedOrderLineItem.OrderItemNetamount] currencyString];
}
-(void)updateFOCFields
{
    if(selectedOrderLineItem.manualFOCQty==0)
    {
        manualFOCTextField.text=selectedOrderLineItem.manualFocProduct.Description;
        FOCQtyTextField.text=@"";
    }
    else
    {
        manualFOCTextField.text=selectedOrderLineItem.manualFocProduct.Description;
        FOCQtyTextField.text=[NSString stringWithFormat:@"%0.0f",selectedOrderLineItem.manualFOCQty];
    }
    
    
    if((![appControl.ALLOW_MANUAL_FOC_ITEM_CHANGE isEqualToString:KAppControlsYESCode]) ||
       !([appControl.ENABLE_MANUAL_FOC isEqualToString:KAppControlsYESCode])
       )
    {
        [manualFOCTextField setIsReadOnly:YES];
    }
    else
    {
        [manualFOCTextField setIsReadOnly:NO];
    }

    
    if([appControl.ALLOW_DUAL_FOC isEqualToString:KAppControlsYESCode])
    {
        [FOCQtyTextField setIsReadOnly:NO];
    }
    else if([appControl.ENABLE_MANUAL_FOC isEqualToString:KAppControlsYESCode])
    {
        if(selectedOrderLineItem.defaultBonusQty==0)
        {
            [FOCQtyTextField setIsReadOnly:NO];
        }
        else
        {
            [FOCQtyTextField setIsReadOnly:YES];
            [manualFOCTextField setIsReadOnly:YES];
        }
    }
    else
    {
        [FOCQtyTextField setIsReadOnly:YES];
    }

 
    
    /** checking  manual FOC item level  -- appcontrol flag DISABLE_FOC_FOR_RESTRICTED // this is added for IDS*/
    if([self isManualFOCDisabledForThisProduct:[selectedOrderLineItem.OrderProduct copy]])
    {
        manualFOCTextField.text=@"";
        FOCQtyTextField.text=@"";
        [FOCQtyTextField setIsReadOnly:YES];
        [manualFOCTextField setIsReadOnly:YES];
    }
    
    if([appControl.ENABLE_MANUAL_FOC isEqualToString:KAppControlsNOCode]){
        manualFOCTextField.text=@"";
        FOCQtyTextField.text=@"";
        [FOCQtyTextField setIsReadOnly:YES];
        [manualFOCTextField setIsReadOnly:YES];
    }
    
    /** for foc orders  manual foc not allowed*/
    if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        manualFOCTextField.text=@"";
        FOCQtyTextField.text=@"";
        [FOCQtyTextField setIsReadOnly:YES];
        [manualFOCTextField setIsReadOnly:YES];
    }

}


- (void)calculateBonus {
    NSLog(@"calculating bonus ");
    
    if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        selectedOrderLineItem.bonusProduct=nil;
        selectedOrderLineItem.defaultBonusQty=0;
        selectedOrderLineItem.requestedBonusQty=0;
        return;
    }
    
    double bonus = 0;
    double quantity=selectedOrderLineItem.OrderProductQty.doubleValue*[SWDefaults getConversionRateWithRespectToPrimaryUOMCodeForProduct:[selectedOrderLineItem.OrderProduct copy]];
    
    /** checking availability of assortment bonus*/
    BOOL isAssortmentBonusPlanAvailable=NO;
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
        SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
        if([bnsManager IsAnyAssortmentBonusAvailableForTheProduct:[selectedOrderLineItem.OrderProduct copy] AssortmentPlans:currentVisit.visitOptions.salesOrder.AssortmentPlansArray])
            isAssortmentBonusPlanAvailable=YES;
    }
    
    /**Calculating simple bous*/
    ProductBonusItem *selectedBonusItem;
    if(currentVisit.visitOptions.customer.isBonusAvailable && !isAssortmentBonusPlanAvailable)
    {
            for(NSInteger i=0 ; i<[selectedOrderLineItem.bonusDetailsArray count] ; i++ )
            {
                ProductBonusItem *bonusItem = [selectedOrderLineItem.bonusDetailsArray objectAtIndex:i];
                double rangeStart = [bonusItem.Prom_Qty_From doubleValue];
                double rangeEnd = [bonusItem.Prom_Qty_To doubleValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
    
                    if ([appControl.BONUS_MODE isEqualToString:@"DEFAULT"])
                    {
                            if([bonusItem.Price_Break_Type_Code isEqualToString:@"RECURRING"])
                            {
                                double dividedValue = quantity / rangeStart ;
                                bonus = [bonusItem.Get_Qty integerValue] * floor(dividedValue) ;
                                selectedBonusItem=bonusItem;
                            }
                            
                            else if([bonusItem.Price_Break_Type_Code  isEqualToString:@"PERCENT"])
                            {
                                double dividedValue = [bonusItem.Get_Qty integerValue]* (quantity / rangeStart) ;
                                bonus = floor(dividedValue) ;
                                selectedBonusItem=bonusItem;
                                
                            }
                            else if([bonusItem.Price_Break_Type_Code  isEqualToString:@"POINT"])
                            {
                                bonus = [bonusItem.Get_Qty integerValue];
                                selectedBonusItem=bonusItem;
                                
                            }
                    }
                    else
                    {
                        bonus = [bonusItem.Get_Qty integerValue] + floorf(([bonusItem.Get_Qty integerValue] - rangeStart) *[bonusItem.Get_Add_Per  floatValue]);
                        selectedBonusItem=bonusItem;
                    }
                    break;
                }
            }
        }
        
        if (bonus > 0)
        {
                selectedOrderLineItem.bonusProduct=selectedBonusItem.bonusProduct;
                selectedOrderLineItem.defaultBonusQty=bonus;
                selectedOrderLineItem.requestedBonusQty=bonus;
                if([appControl.ALLOW_DUAL_FOC isEqualToString:KAppControlsNOCode])
                {
                    selectedOrderLineItem.manualFOCQty=0;
                }
        }
        else
        {
            selectedOrderLineItem.bonusProduct=nil;
            selectedOrderLineItem.defaultBonusQty=0;
            selectedOrderLineItem.requestedBonusQty=0;
        }
}
-(void)UpdateDiscountTextField
{
    [self calculateDiscount];
    if([appControl.ENABLE_ITEM_DISCOUNT isEqualToString:KAppControlsYESCode])
    {
        selectedOrderLineItem.appliedDiscount=[discountTextField.text doubleValue];
    }
    else
    {
        [self calculateDiscount];
    }

    [self calculateItemAmounts];

}
-(void)calculateDiscount
{
    //check if foc order
    if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        selectedOrderLineItem.appliedDiscount=0.00;
        selectedOrderLineItem.OrderItemDiscountamount=[NSDecimalNumber zero];
        selectedOrderLineItem.OrderItemTotalamount=[NSDecimalNumber zero];
        selectedOrderLineItem.OrderItemNetamount=[NSDecimalNumber zero];
        return;
    }
    
    //check for foc
    BOOL isFOCGiven=selectedOrderLineItem.manualFOCQty>0?YES:NO;
    
    // check for bonus
    BOOL isBonusGiven=NO;
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]&&
       ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
        SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
        if([bnsManager IsAnyAssortmentBonusAvailableForTheProduct:[selectedOrderLineItem.OrderProduct copy] AssortmentPlans:tempSalesOrder.AssortmentPlansArray]/*&&
           selectedOrderLineItem.manualFOCQty==0*/){
            isBonusGiven=YES;
        }
    }else{
            isBonusGiven=selectedOrderLineItem.defaultBonusQty>0?YES:NO;
    }
    
    
    double defaultDiscount=0.0;
    double specialDiscount=0.0;
    double appliedDiscount=0.0;

    if([appControl.ALLOW_DISCOUNT isEqualToString:KAppControlsYESCode])
    {
        defaultDiscount=[selectedOrderLineItem.OrderProduct.Discount doubleValue];
    }
    if([appControl.ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsYESCode])
    {
        specialDiscount=[selectedOrderLineItem.OrderProduct.specialDiscount doubleValue];
    }
    if(defaultDiscount<=0.0 && specialDiscount>=0.0)
    {
        appliedDiscount=specialDiscount;
    }
    else if(!(isFOCGiven || isBonusGiven) && specialDiscount<=0.0)
    {
        appliedDiscount=defaultDiscount;
    }
    else if((isFOCGiven || isBonusGiven)  && specialDiscount<=0.0)
    {
        appliedDiscount=0.0;
    }
    else if(defaultDiscount>0.0 && specialDiscount>0.0 && (isFOCGiven || isBonusGiven) )
    {
        appliedDiscount=specialDiscount;
    }
    else if(defaultDiscount>0.0 && specialDiscount>0.0 && !(isFOCGiven || isBonusGiven) )
    {
        appliedDiscount=specialDiscount+defaultDiscount;
    }

    selectedOrderLineItem.appliedDiscount=appliedDiscount;
    [self calculateItemAmounts];

}
-(void)calculateItemAmounts
{
   
    NSDecimalNumber  * discountDecimalNumber=[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",selectedOrderLineItem.appliedDiscount]];
    NSDecimalNumber  * productSelleingPriceDecimalNumber=[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",selectedOrderLineItem.productSelleingPrice]];

    
    selectedOrderLineItem.OrderItemDiscountamount=[[[selectedOrderLineItem.OrderProductQty decimalNumberByMultiplyingBy:discountDecimalNumber] decimalNumberByMultiplyingBy:productSelleingPriceDecimalNumber] decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"100"]];
    
    selectedOrderLineItem.OrderItemTotalamount=[selectedOrderLineItem.OrderProductQty decimalNumberByMultiplyingBy:productSelleingPriceDecimalNumber];

    if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode] &&
       selectedOrderLineItem.VATChargeObj.VATCharge!=nil){
        if([selectedOrderLineItem.VATChargeObj.VATLevel isEqualToString:@"NET"]){
            selectedOrderLineItem.OrderItemVATChargeamount=[[[selectedOrderLineItem.OrderItemTotalamount decimalNumberBySubtracting:selectedOrderLineItem.OrderItemDiscountamount] decimalNumberByMultiplyingBy:selectedOrderLineItem.VATChargeObj.VATCharge] decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"100"]];
        }else{
            selectedOrderLineItem.OrderItemVATChargeamount=[[selectedOrderLineItem.OrderItemTotalamount decimalNumberByMultiplyingBy:selectedOrderLineItem.VATChargeObj.VATCharge] decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"100"]];
        }
    }else{
        selectedOrderLineItem.OrderItemVATChargeamount=[NSDecimalNumber zero];
    }


    selectedOrderLineItem.OrderItemNetamount=[[selectedOrderLineItem.OrderItemTotalamount decimalNumberBySubtracting:selectedOrderLineItem.OrderItemDiscountamount] decimalNumberByAdding:selectedOrderLineItem.OrderItemVATChargeamount];

//    if(selectedOrderLineItem.appliedDiscount==100){
//        selectedOrderLineItem.OrderItemNetamount=[NSDecimalNumber zero];
//    }
//    else{
//        selectedOrderLineItem.OrderItemNetamount=[selectedOrderLineItem.OrderItemTotalamount decimalNumberBySubtracting:selectedOrderLineItem.OrderItemDiscountamount];
//    }
}
-(void)updateUOMTextfield
{
    UOMTextField.text=selectedOrderLineItem.OrderProduct.selectedUOM;

    if([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode])
    {
        [UOMTextField setIsReadOnly:NO];
        [UOMTextField setDropDownImage:@"Arrow_DropDown"];
    }
    else
    {
        [UOMTextField setIsReadOnly:YES];

    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if(textField==orderQtyTextField)
    {
        /* if selecetd uom is not primary uom allwoing decimal points in quantity textfield*/
        NSString *expression = ([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode]&&
                                ![selectedOrderLineItem.OrderProduct.primaryUOM isEqualToString:selectedOrderLineItem.OrderProduct.selectedUOM]&&
                                ([SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[selectedOrderLineItem.OrderProduct copy]]<1))?KDecimalQuantityRegExpression:KWholeNumberQuantityRegExpression;
        
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        
        if ([appControl.OVER_STOCK isEqualToString:KAppControlsNOCode]) {
            return (numberOfMatches != 0 &&[newString doubleValue]<=[selectedOrderLineItem.OrderProduct.stock doubleValue]  && newString.length<12);
        }
        return (numberOfMatches != 0 && newString.length<=KSalesOrderScreenOrderQuantityTextFieldMaximumDigitsLimit);
    }
    else if(textField==FOCQtyTextField)
    {
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KSalesOrderScreenFOCQuantityTextFieldMaximumDigitsLimit);
    }
    else if(textField==requestedBonusTextField)
    {
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KSalesOrderScreenBonusQuantityTextFieldMaximumDigitsLimit);
        
    }
    else if(textField==discountTextField)
    {
        NSString *expression =[NSString stringWithFormat:@"^[0-9]*((\\.|,)[0-9]{0,%0.0f})?$",2.0];
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KSalesOrderScreenDiscountPercantageMaximumDigitsLimit && newString.doubleValue<=100);
        
    }
    else if(textField==sellingPriceTextField)
    {
        NSString *expression =[NSString stringWithFormat:@"^[0-9]*((\\.|,)[0-9]{0,%0.0f})?$",[[[SWDefaults userProfile]valueForKey:@"Decimal_Digits"] doubleValue]];
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KSalesOrderScreenSellingPriceTextFieldMaximumDigitsLimit);
        
    }

    return YES;
}

-(void)confirmedOrderShowLots
{
    SalesOrderLotAssigningViewController *salesOrderLotAssigningViewController=[[SalesOrderLotAssigningViewController alloc]initWithNibName:@"SalesOrderLotAssigningViewController" bundle:[NSBundle mainBundle]];
    salesOrderLotAssigningViewController.salesOrderLotAssigningViewControllerDelegate=self;
    salesOrderLotAssigningViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderLotAssigningViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderLotAssigningViewController.wareHouseId=currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID;
    SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];
    salesOrderLotAssigningViewController.OrderLineItem=swipedOrderItem;
    salesOrderLotAssigningViewController.isLotsAssignmentReadOnly=YES;
    [self.navigationController presentViewController:salesOrderLotAssigningViewController animated:NO completion:nil];

}
-(void)confirmOrdershowNotes
{
    SalesOrderNotesViewConrollerViewController *salesOrderNotesViewConrollerViewController=[[SalesOrderNotesViewConrollerViewController alloc]initWithNibName:@"SalesOrderNotesViewConrollerViewController" bundle:[NSBundle mainBundle]];
    salesOrderNotesViewConrollerViewController.isReadOnly=YES;
    salesOrderNotesViewConrollerViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderNotesViewConrollerViewController.modalPresentationStyle = UIModalPresentationCustom;
    SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];
    salesOrderNotesViewConrollerViewController.notesText=swipedOrderItem.NotesStr;
    salesOrderNotesViewConrollerViewController.salesOrderNotesViewConrollerViewControllerDelegate=self;
    [self.navigationController presentViewController:salesOrderNotesViewConrollerViewController animated:NO completion:nil];
}
-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %ld, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (long)index, fromExpansion ? @"YES" : @"NO");
    
    if (direction == MGSwipeDirectionRightToLeft) {
        if(currentVisit.visitOptions.salesOrder.isManagedOrder && [currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KConfirmOrderStatusString])
        {
            swipedCellIndexpath=[OrdersTableView indexPathForCell:cell];
            SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];

            if(index==0)
            {
                if([appControl.ALLOW_LOT_SELECTION isEqualToString:KAppControlsYESCode] && swipedOrderItem.AssignedLotsArray.count>0)
                    [self confirmedOrderShowLots];
                else if([appControl.ENABLE_LINE_ITEM_NOTES isEqualToString:KAppControlsYESCode] && swipedOrderItem.NotesStr.length>0)
                    [self confirmOrdershowNotes];
            }
            else  if(index==1)
            {
                [self confirmOrdershowNotes];

            }
            
            
        }
        else
        {
            if(index==0)
            {
                //delete button
                NSIndexPath * path = [OrdersTableView indexPathForCell:cell];
                swipedCellIndexpath=[OrdersTableView indexPathForCell:cell];

                NSInteger appliedAsstBnsPlanId=NSNotFound;
                NSInteger availableBonusPlanId=NSNotFound;
                if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]&&
                   ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
                    appliedAsstBnsPlanId=[(SalesOrderLineItem *)[orderLineItemsArray objectAtIndex:path.section] AppliedAssortMentPlanID];
                    
                    /** Getting default bonus plan */
                     availableBonusPlanId=[[[SalesWorxAssortmentBonusManager alloc]init]FetchAvailableBonusPlanForProduct:[(SalesOrderLineItem *)[orderLineItemsArray objectAtIndex:path.section] OrderProduct] AssortmentPlans:currentVisit.visitOptions.salesOrder.AssortmentPlansArray];
                }
                
 
                
                if(appliedAsstBnsPlanId!=NSNotFound || availableBonusPlanId!=NSNotFound){
                    //  [self AlterBonusPlanIfRequired:appliedAsstBnsPlanId AndSelectedLineItem:nil];
                    tempSalesOrder=nil;
                    if(tempSalesOrder==nil){
                        tempSalesOrder=[[SalesOrder alloc]init];

                        tempSalesOrder.SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
                        tempSalesOrder.SalesOrderLineItemsArray=[orderLineItemsArray mutableCopy];
                        /** removing the deleting order line item*/
                        [tempSalesOrder.SalesOrderLineItemsArray removeObjectAtIndex:path.section];
                        
                        tempSalesOrder.AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
                        tempSalesOrder.AssortmentBonusProductsArray=[AssortmentBonusProductsArray mutableCopy];
                        tempSalesOrder.AssortmentPlansArray=[[NSMutableArray alloc]init];
                        tempSalesOrder.AssortmentPlansArray=currentVisit.visitOptions.salesOrder.AssortmentPlansArray;
                    }
                    
                    if(appliedAsstBnsPlanId!=NSNotFound){
                        [self AlterBonusPlanIfRequired:appliedAsstBnsPlanId AndSelectedLineItem:nil AndSalesOrder:tempSalesOrder];
                    }else if(availableBonusPlanId!=NSNotFound){
                        [self AlterBonusPlanIfRequired:availableBonusPlanId AndSelectedLineItem:nil AndSalesOrder:tempSalesOrder];
                    }
                
                }else{
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    SalesOrderLineItem *tempLineItem=[orderLineItemsArray objectAtIndex:path.section];
                    NSLog(@"removing object %@",tempLineItem.OrderProduct.Description);

                    [orderLineItemsArray removeObjectAtIndex:path.section];
                    [self DeleteOrderTableViewSectionAtIndexPath:path];
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                }
                
                return NO; //Don't auto hide to improve delete expansion animation
            }
            else  if(index==1)
            {
                popOverTitle=@"More";
                selectedLineItemMoreOptionsArray=[[NSMutableArray alloc]init];
                if([appControl.ALLOW_LOT_SELECTION isEqualToString:KAppControlsYESCode])
                    [selectedLineItemMoreOptionsArray addObject:KLineItemSelectLots];
                if([appControl.ENABLE_LINE_ITEM_NOTES isEqualToString:KAppControlsYESCode])
                    [selectedLineItemMoreOptionsArray addObject:KLineItemNotes];
                swipedCellIndexpath=[OrdersTableView indexPathForCell:cell];
                [self presentMoreButtonPopOver:[cell.rightButtons objectAtIndex:1] withTitle:popOverTitle withContent:selectedLineItemMoreOptionsArray inView:cell];
                return NO;
            }
        }
    }
    
    return YES;
}
-(void)DeleteOrderTableViewSectionAtIndexPath:(NSIndexPath *)path{
    
    [UIView beginAnimations:@"myAnimationId" context:nil];
    
    [UIView setAnimationDuration:0.5]; // Set duration here
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        NSLog(@"Complete!");
    }];
    
    
    [OrdersTableView beginUpdates];
    [OrdersTableView deleteSections:[NSIndexSet indexSetWithIndex:path.section]
                   withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [OrdersTableView endUpdates];
    
    [CATransaction commit];
    [UIView commitAnimations];
    
    if(selectedOrderItemIndexpath==nil)
    {
        [OrdersTableView reloadData];
    }
    else
    {
        [self DeselectTableViewsAndUpdate];
    }
    [self updateSalesOrderTotalAmount];

}
-(void)presentMoreButtonPopOver:(MGSwipeButton*)Button withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray inView:(MGSwipeTableCell*)cell
{
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    //popOverVC.disableSearch=NO;
    popOverVC.enableArabicTranslation=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    FOCPopOverController=nil;
    FOCPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    FOCPopOverController.delegate=self;
    popOverVC.popOverController=FOCPopOverController;
    popOverVC.titleKey=popOverString;
    [FOCPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [FOCPopOverController presentPopoverFromRect:Button.frame inView:Button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark SalesOrderNotesViewConrollerViewControllerDelegateMethods
-(void)SaveNotes:(NSString *)notes
{
    SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];
    swipedOrderItem.NotesStr=notes;
    [orderLineItemsArray replaceObjectAtIndex:swipedCellIndexpath.section withObject:swipedOrderItem];
    if(selectedOrderItemIndexpath!=nil &&  selectedOrderItemIndexpath.section==swipedCellIndexpath.section)
        selectedOrderLineItem.NotesStr=notes;
    
    [self CloseOrdersTableRightSideSwipeWithAnimation:YES];
}
-(void)NotesCancelled
{
    [self CloseOrdersTableRightSideSwipeWithAnimation:YES];
}

-(void)updateSalesOrderTotalAmount
{
    NSNumber* sum = [orderLineItemsArray valueForKeyPath: @"@sum.OrderItemNetamount"];
    ordersTotalAmountLbl.text=[[NSString stringWithFormat:@"%@",sum] currencyString];
}


#pragma mark SalesOrderLotAssigningViewControllerDelegateMethods
-(void)updateLotsAllcation:(NSMutableArray *)assignedLotsArray isReAllocationForUpdatedQuantity:(BOOL)status
{
    
    if(!status)
    {
        SalesOrderLineItem *swipedOrderItem= [(SalesOrderLineItem*)[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section] copy];        swipedOrderItem.AssignedLotsArray=assignedLotsArray;
        selectedOrderLineItem.AssignedLotsArray = assignedLotsArray;
        [orderLineItemsArray replaceObjectAtIndex:swipedCellIndexpath.section withObject:swipedOrderItem];
    }
    else
    {
        selectedOrderLineItem.AssignedLotsArray=assignedLotsArray;
        [self CheckAndADDOrUpdateProductToOrderLineItems];
    }
    [self CloseOrdersTableRightSideSwipeWithAnimation:YES];

    swipedCellIndexpath=nil;

}

-(void)lotsAllocationCancelled
{
    [self CloseOrdersTableRightSideSwipeWithAnimation:YES];
}
#pragma mark productLots
-(NSMutableArray *)fetchStockDataforItemID:(Products*)product
{
    NSInteger productStockValue=0;
    
    NSMutableArray *productStockArray=[[NSMutableArray alloc]init];
    NSMutableArray *tempProductStockArray=[[NSMutableArray alloc]init];
    
    
//    if([appControl.SHOW_ORG_STOCK_ONLY isEqualToString:KAppControlsYESCode])
//    {
//        tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:product WareHouseId:currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID];
//    }
//    else
//    {
//        tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:product WareHouseId:KNotApplicable];
//    }
    
    /**  stock will be allocated to selected  ware house only*/
    tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:product WareHouseId:currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID];
   // NSLog(@"stock array in fetchStockDataforItemID %@", tempProductStockArray);
    
    if (tempProductStockArray.count>0) {
        
        for (NSMutableDictionary * currentStockDict in tempProductStockArray) {
            
            Stock * productStock=[[Stock alloc]init];
            //NSLog(@"refined stock dict is %@", currentStockDict);
            productStock.Lot_No=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Lot_No"]];
            productStock.Lot_Qty=[NSString stringWithFormat:@"%0.0f",[[currentStockDict valueForKey:@"Lot_Qty"] doubleValue] *[SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[product copy]]];
           
            
            
            
            productStock.lotQuantityNumber=[SWDefaults getQuantityWithRespectToSelectedUOMCodeForProducrt:[product copy] PrimaryUOMQuanity:[NSDecimalNumber ValidDecimalNumberWithString:[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Lot_Qty"]]]];
            
            productStock.Org_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Org_ID"]];
            productStock.Stock_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Stock_ID"]];
            productStock.Expiry_Date=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Expiry_Date"]];
            productStock.On_Order_Qty=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"On_Order_Qty"]];
            
            productStock.warehouse=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Description"]];
            productStock.lotNumberWithExpiryDate=[NSString stringWithFormat:@"%@ ## %@ ",productStock.Lot_No,productStock.Expiry_Date];
            productStockValue= productStockValue+[productStock.Lot_Qty integerValue];
            [productStockArray addObject:productStock];
        }
    }
    return productStockArray;
}

-(void)CloseOrdersTableRightSideSwipeWithAnimation:(BOOL)Animated
{
    SalesWorxSalesOrdersTableCellTableViewCell *cell=[OrdersTableView cellForRowAtIndexPath:swipedCellIndexpath];
    [cell hideSwipeAnimated:Animated completion:^(BOOL finished) {
    }];
}

-(void)DiableAllTexfields
{
    [sellingPriceTextField setIsReadOnly:YES];
    [orderQtyTextField setIsReadOnly:YES];
    [UOMTextField setIsReadOnly:YES];
    [discountTextField setIsReadOnly:YES];
    [requestedBonusTextField setIsReadOnly:YES];
    [manualFOCTextField setIsReadOnly:YES];
    [FOCQtyTextField setIsReadOnly:YES];
    [addToProductButton setHidden:YES];
}

-(void)UpdateUIFiledsForFOCOrder
{
//    orderQtyTextField=FOCorderQtyTextField;
//    UOMTextField=FOCOrderUOMTextField;
//    OrderItemTotalAmountLabel=FOCOrderItemTotalAmountLabel;
//    OrderItemDiscountAmountLabel=FOCOrderItemDiscountAmountLabel;
//    OrderItemNetAmountLabel=FOCOrderItemNetAmountLabel;
//    addToProductButton=addToFOCProductButton;
    [manualFOCTextField setHidden:YES];
    [FOCQtyTextField setHidden:YES];
    [manualFocStringLabel setHidden:YES];
    [FocQtyStringLabel setHidden:YES];
    [requestedBonusTextField setHidden:YES];
    [requestedBonusTextField setHidden:YES];
    [sellingPriceTextField setIsReadOnly:YES];
    [discountTextField setHidden:YES];
    [DiscountStringLabel setHidden:YES];
    [RequestedBonusStringLabel setHidden:YES];
    
    AddToOrderButtonleftMargin.constant=0;
    AddToOrderButtonRightMargin.constant=50;
    AddtoOrderButtonBottomMargin.constant=8;
    manualFocStringLabelHeightConstrain.constant=0;
    FOCQtyTextFieldHeightConstraint.constant=0;
    ManualFocTextfieldHeightConstrain.constant=0;
    FOCQtyStringLabelHeightConstraint.constant=0;
    

}

-(void)ExpandFirstProductsSection{
    ProductTableViewExpandedSectionsArray =[[NSMutableArray alloc]init];
    [ProductTableViewExpandedSectionsArray addObject:@"0"];
    filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
    [self AddProductObjectsToGroupByArrayAtIndex:0];
}
-(void)AddProductObjectsToGroupByArrayAtIndex:(NSInteger)brandCodeLocation
{
    NSPredicate *brandCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[productsBrandCodesArray objectAtIndex:brandCodeLocation]];
    NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[filteredProductsArray filteredArrayUsingPredicate:brandCodeProductPredicate]];
    
    [filteredProductsArrayGroupedByBrandCode replaceObjectAtIndex:brandCodeLocation  withObject:tempProductsArray];
}

#pragma mark MultiUOM
-(Products *)getUpdatedProductDetails:(Products *)product ToSelectedUOM:(ProductUOM *)proUOM
{
    NSPredicate *ProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@ && SELF.OrgID==[cd] %@",product.Item_Code,product.OrgID];
    NSMutableArray *tempProducts= [[productsArray filteredArrayUsingPredicate:ProductPredicate] mutableCopy];
    
    if(tempProducts.count==1)
    {
        Products *pro=[(Products *)[tempProducts objectAtIndex:0] copy];
        NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
        NSMutableArray *UOMarray=[product.ProductUOMArray mutableCopy];
        NSMutableArray *filtredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
        
        if(filtredArray.count==1)
        {
            ProductUOM *primaryUOMObj=[filtredArray objectAtIndex:0];
            
            pro.stock=[NSString stringWithFormat:@"%ld",(long)[[NSString stringWithFormat:@"%f",([pro.stock doubleValue]*[primaryUOMObj.Conversion doubleValue])/[proUOM.Conversion doubleValue]] integerValue]];
            pro.selectedUOM=proUOM.Item_UOM;
            pro.ProductUOMArray=[product.ProductUOMArray mutableCopy];
            return  pro;
        }
        else
        {
            return nil;
        }

    }
    else
    {
       return  nil;
    }

}

-(void)updateProductAndOrderDetailsUIOnUOMSelection:(ProductUOM *)proUOM
{
    [self clearAllProductLabelsAndTextFields];
    //tempSalesOrder=nil;
    selectedProduct=[self getUpdatedProductDetails:selectedOrderLineItem.OrderProduct ToSelectedUOM:proUOM];
    
    
    /**/
    SalesOrderLineItem *tempSelectedOrderLineItem=[selectedOrderLineItem copy];
    
    selectedOrderLineItem=[[SalesOrderLineItem alloc]init];
    selectedOrderLineItem.OrderProduct=selectedProduct;
    selectedOrderLineItem.VATChargeObj=[VATChargesManager getVATChargeForProduct:selectedOrderLineItem.OrderProduct AndCustomer:currentVisit.visitOptions.customer];
    selectedOrderLineItem.Guid=tempSelectedOrderLineItem.Guid;
    selectedOrderLineItem.AppliedAssortMentPlanID=tempSelectedOrderLineItem.AppliedAssortMentPlanID;
    selectedOrderLineItem.showApliedAssortMentPlanBonusProducts=tempSelectedOrderLineItem.showApliedAssortMentPlanBonusProducts;

    if([self fetchProductPriceListData:selectedProduct]!=nil)
    {
        selectedProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedProduct];
        if([selectedProduct.productPriceList.Unit_Selling_Price doubleValue]==0)
        {
            productWholeSalePriceLabel.text=KNotApplicable;
            productRetailPriceLabel.text=KNotApplicable;
            [self showProductOrderDetailsView];
            [self hideProductOrderDetailsView];
            [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
            [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];
            
        }
        else
        {
            productWholeSalePriceLabel.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString] ;
            productRetailPriceLabel.text=[selectedProduct.productPriceList.Unit_List_Price floatString];
            [self showProductDetailsView];
            [self showProductOrderDetailsView];
            [self hideNoSelectionView];
        }
        
    }
    else
    {
        productWholeSalePriceLabel.text=KNotApplicable;
        productRetailPriceLabel.text=KNotApplicable;
        [self showProductOrderDetailsView];
        [self hideProductOrderDetailsView];
        [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
        [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];
        
    }
    productNearestExpiryDateLabel.text=[selectedProduct.nearestExpiryDate isEqualToString:@""]?KNotApplicable:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:selectedProduct.nearestExpiryDate];
    productCodeLabel.text=selectedProduct.Item_Code;
    productNameLabel.text=selectedProduct.Description;
    productAvailableStockLabel.text=[NSString stringWithFormat:@"%@ %@",selectedProduct.stock,selectedProduct.selectedUOM];
    productDefaultDiscountLabel.text=[[NSString stringWithFormat:@"%0.2f", [selectedProduct.Discount doubleValue]] stringByAppendingString:@"%"];
    

    [self getProductSpecailDisount];
    if([selectedOrderLineItem.OrderProduct.specialDiscount doubleValue]>0.0)
        productSpecialDiscountLabel.text=[NSString stringWithFormat:@"%0.2f", [selectedOrderLineItem.OrderProduct.specialDiscount doubleValue]];
    else
        productSpecialDiscountLabel.text=@"N/A";
    [self getBonusDetails];
    
    if(![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        sellingPriceTextField.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString];
        selectedOrderLineItem.productSelleingPrice=[selectedProduct.productPriceList.Unit_Selling_Price doubleValue];
        
    }
    else
    {
        sellingPriceTextField.text=[@"0" floatString];
        selectedOrderLineItem.productSelleingPrice=0.00;
        
    }
    
    /** For FOC orders manual foc not allowed*/
    if(![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        //check if the product is restricted for manual foc /** intially assigning selected product also as manual foc*/
        if([self isTheProductRestrictedForManualFOC:selectedProduct])
        {
            selectedOrderLineItem.manualFocProduct=nil;
        }
        else
        {
            manualFOCTextField.text=selectedProduct.Description;
            selectedOrderLineItem.manualFocProduct=[self getManualFOCProductWithPrimaryUOM:selectedProduct];
        }
    }
    
    
    
    [self updateRequiredBonusField];
    
    /** foc order user cana not change price*/
    if(![selectedProduct.isSellingPriceFieldEditable isEqualToString:KAppControlsYESCode] || [currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
        [sellingPriceTextField setIsReadOnly:YES];
    else
        [sellingPriceTextField setIsReadOnly:NO];
    
    [self calculateDiscount];
    discountTextField.text=[NSString stringWithFormat:@"%0.2f",selectedOrderLineItem.appliedDiscount];
    
    [self updateAddToOrderButtonTitle];
    [self updateFOCFields];
    
    /** Multi UOM*/
    selectedOrderLineItem.OrderProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:selectedOrderLineItem.OrderProduct];
    [self updateUOMTextfield];
    
}



-(Products *)getProductWithPrimaryUOMCode:(Products *)pro
{
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
    NSMutableArray *UOMarray=[pro.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    
    return [self getUpdatedProductDetails:pro ToSelectedUOM:primaryUOMObj];
}
-(Products *)getManualFOCProductWithPrimaryUOM:(Products *)pro
{
    Products *manualFocProduct=[pro copy];
    return [self getProductWithPrimaryUOMCode:manualFocProduct];
    
}


#pragma mark order_quantity_decimals
-(BOOL)isOrderItemWithValidDecimalOrderQuantity:(SalesOrderLineItem *)item
{
    
    
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",item.OrderProduct.primaryUOM];
    NSPredicate *selecetdUOMPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",item.OrderProduct.selectedUOM];
    
    NSMutableArray *UOMarray=[item.OrderProduct.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    NSMutableArray *selecetdUOMFilterArray=[[UOMarray filteredArrayUsingPredicate:selecetdUOMPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    ProductUOM *selectedUOMObj=[selecetdUOMFilterArray objectAtIndex:0];
    
    NSDecimalNumber *OrderProductQtyInPrimaryUOM= [[item.OrderProductQty decimalNumberByMultiplyingBy:[NSDecimalNumber ValidDecimalNumberWithString:selectedUOMObj.Conversion]]decimalNumberByDividingBy:[NSDecimalNumber ValidDecimalNumberWithString:primaryUOMObj.Conversion]];
    ;
    if([[NSString stringWithFormat:@"%0.2f",OrderProductQtyInPrimaryUOM.doubleValue] integerValue]==OrderProductQtyInPrimaryUOM.integerValue)
    {
        return YES;
    }
    else{
        return NO;
    }
    
    
    return YES;
}




#pragma mark Barcode_Scan
-(IBAction)barCodeScanButtonTapped:(id)sender
{

    [self.view endEditing:YES];
    SalesWorxBarCodeScannerManager *barcodeManager=[[SalesWorxBarCodeScannerManager alloc]init];
    barcodeManager.swxBCMdelegate=self;
    [barcodeManager ScanBarCode];
}

- (void)BarCodeScannerButtonBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];

}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    [reader dismissViewControllerAnimated:YES completion:^{
        
            NSLog(@"scanned bar code is %@", symbol.data);
            NSLog(@"symbol is %@", symbol.typeName);
        
      NSPredicate *barCodePredicate=[NSPredicate predicateWithFormat:@"SELF.ProductBarCode ==[cd] %@",symbol.data];
        if([[productsArray filteredArrayUsingPredicate:barCodePredicate] count]>0)
        {
            previousFilteredParameters=[[NSMutableDictionary alloc]init];
            productsSearchBar.text=symbol.data;
            proSearchType=ProductSearchByBarCode;
            [self searchProductsContent:productsSearchBar.text];
        }
        else
        {
           NSMutableArray *tempProductsArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Product where EANNO='%@'",symbol.data]];
            if(tempProductsArray.count>0)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Product with scanned barcode not available in selected warehouse/category" withController:self];

            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"No product found" withController:self];
            }
        }
    }];
}



#pragma mark SalesOrderAssortmentBonusAllocationViewControllerDelegate Methods
-(void)UpdateAssortmentBonusAllcation:(NSMutableArray *)bonusproductsArray WithPlan:(SalesworxAssortmentBonusPlan *)bnsPlan AndSelectedOrderItem:(SalesOrderLineItem *)OrderItem
{
    NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId != %ld",(long)bnsPlan.PlanId]];
    NSMutableArray *tempAssortMentBonusProductsArray=  [[AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
    [tempAssortMentBonusProductsArray addObjectsFromArray:bonusproductsArray];
    
    NSMutableArray *offerProductCodesArray= [bnsPlan.OfferProductsArray valueForKey:@"Item_Code"];
    
    BOOL IsShowApliedAssortMentPlanBonusProductsAssigned=NO;
    for (NSInteger i=0; i<tempSalesOrder.SalesOrderLineItemsArray.count; i++) {
        
        
        SalesOrderLineItem *tempLineItem=  [tempSalesOrder.SalesOrderLineItemsArray objectAtIndex:i];
        if([offerProductCodesArray containsObject:tempLineItem.OrderProduct.Item_Code])
        {
            if(tempLineItem.manualFOCQty>0 || bonusproductsArray.count==0){ /** Asst.bonus not applicable for manual foc given products*/
                tempLineItem.showApliedAssortMentPlanBonusProducts=NO;
                tempLineItem.AppliedAssortMentPlanID=NSNotFound;

            }else{
                tempLineItem.showApliedAssortMentPlanBonusProducts=NO;
                tempLineItem.AppliedAssortMentPlanID=bnsPlan.PlanId;
                
                if([OrderItem.Guid isEqualToString:tempLineItem.Guid])
                    tempLineItem.showApliedAssortMentPlanBonusProducts=YES;
                else if ((OrderItem==nil || OrderItem.manualFOCQty>0) && IsShowApliedAssortMentPlanBonusProductsAssigned==NO){
                    IsShowApliedAssortMentPlanBonusProductsAssigned=YES;
                    tempLineItem.showApliedAssortMentPlanBonusProducts=YES;
                }
            }
            [tempSalesOrder.SalesOrderLineItemsArray replaceObjectAtIndex:i withObject:tempLineItem];
        }
    }
    
    tempSalesOrder.AssortmentBonusProductsArray=tempAssortMentBonusProductsArray;
   // [OrdersTableView reloadData];
    if(OrderItem==nil){/** allocation pop up shown on deleting the line item*/
        AssortmentBonusProductsArray=tempSalesOrder.AssortmentBonusProductsArray;
        orderLineItemsArray=[tempSalesOrder.SalesOrderLineItemsArray mutableCopy];
        [self SortOrderLineItems];
        [self clearButtonTapped:nil];
    }
    else{ /** allocation pop up shown from add or updating order item*/
        [self CheckAndADDOrUpdateProductToOrderLineItems];
    }
}

-(void)PresentAssortmentBonusViewControllerWithPlanId:(NSInteger )PlanId WithBonusQuantity:(double)QtyToBeAssigned AndAssignedBonusItemsArray:(NSMutableArray *)assignedBnsItems AndSelectedOrderLineItem:(SalesOrderLineItem *)OrderLineItem AndTempSalesOrder:(SalesOrder *)tempOrder IsAllocationMandatory:(BOOL )IsMandatory
{
    
    NSLog(@"PresentAssortmentBonusViewControll ---> %ld",(long)PlanId);
    
    NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)PlanId]];

    SalesOrderAssortmentBonusAllocationViewController *baVC=   [[SalesOrderAssortmentBonusAllocationViewController alloc]initWithNibName:@"SalesOrderAssortmentBonusAllocationViewController" bundle:[NSBundle mainBundle]];
    baVC.view.backgroundColor = [UIColor clearColor];
    baVC.modalPresentationStyle = UIModalPresentationCustom;
    baVC.plan=[[tempOrder.AssortmentPlansArray filteredArrayUsingPredicate:planIDPredicate] objectAtIndex:0];
    
    
    NSArray * OrderedOfferProductsArray=[tempOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.OrderProduct.Item_Code IN %@ AND manualFOCQty==0", [baVC.plan.OfferProductsArray valueForKey:@"Item_Code"]]];
    
    baVC.OrderedOfferItemsArray=[[NSMutableArray alloc] initWithArray:OrderedOfferProductsArray];
    
    if([currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.manageOrderStatus isEqualToString:KConfirmOrderStatusString]){
        baVC.isReadOnlyView=YES;
    }
    else{
        baVC.isBonusAllocationMandatory=IsMandatory;
    }

    if(OrderLineItem==nil){
        baVC.deletingOfferProduct=[orderLineItemsArray objectAtIndex:swipedCellIndexpath.section];
    }
    
    baVC.bonusQty=QtyToBeAssigned;
    baVC.assignedBonusItemsArray=assignedBnsItems;
    baVC.AllProductsArray=productsArray;
    baVC.selectedSalesOrderLineItem=OrderLineItem;
    baVC.assortmentBonusAllocationViewControllerDelegate=self;
    UINavigationController * syncNavController=[[UINavigationController alloc]initWithRootViewController:baVC];
    syncNavController.navigationBarHidden=YES;
    syncNavController.view.backgroundColor = KPopUpsBackGroundColor;
    syncNavController.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:syncNavController animated:NO completion:nil];
}
-(void)RemoveAssignedAssortmentBnsPlan:(NSInteger)planID InSalesOrder:(SalesOrder *)tempOrder
{
    for (NSInteger i=0; i<tempOrder.SalesOrderLineItemsArray.count; i++) {
        SalesOrderLineItem *tempLineItem=  [tempOrder.SalesOrderLineItemsArray objectAtIndex:i];
        if(tempLineItem.AppliedAssortMentPlanID==planID)
        {
            tempLineItem.AppliedAssortMentPlanID=NSNotFound;
            tempLineItem.showApliedAssortMentPlanBonusProducts=NO;
            [tempOrder.SalesOrderLineItemsArray replaceObjectAtIndex:i withObject:tempLineItem];
        }
    }
    NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId != %ld",(long)planID]];
    NSMutableArray *tempAssortMentBonusProductsArray=  [[tempOrder.AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
    tempOrder.AssortmentBonusProductsArray=tempAssortMentBonusProductsArray;
}
-(BOOL )AlterBonusPlanIfRequired:(NSInteger )PlanId AndSelectedLineItem:(SalesOrderLineItem *)OrderLineItem AndSalesOrder:(SalesOrder *)tempOrder/** If user deleting line item then OrderLineItem it will be nil*/
{
    SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
    NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)PlanId]];
    NSMutableArray *tempAssortMentBonusProductsArray=  [[tempOrder.AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
    double appliedBonusQty= [[tempAssortMentBonusProductsArray valueForKeyPath:@"@sum.assignedQty"] doubleValue];
    
    double PlanAvailableBonusQty=[bnsManager FetchAvailableBonusQuantityForSalesOrder:tempOrder ForPlanId:PlanId];
    
    if(PlanAvailableBonusQty==0){
        
        if(appliedBonusQty==0){
            [self RemoveAssignedAssortmentBnsPlan:PlanId InSalesOrder:tempOrder];
            if(OrderLineItem==nil){/** if order item deleting*/
                AssortmentBonusProductsArray=tempSalesOrder.AssortmentBonusProductsArray;
                orderLineItemsArray=[tempSalesOrder.SalesOrderLineItemsArray mutableCopy];
                [OrdersTableView reloadData];
            }
            return NO; /** Since deleting the assortment bonus items no need to show bonus allocation pop up*/
            
        }else{
            [self PresentAssortmentBonusViewControllerWithPlanId:PlanId WithBonusQuantity:PlanAvailableBonusQty AndAssignedBonusItemsArray:tempAssortMentBonusProductsArray AndSelectedOrderLineItem:OrderLineItem AndTempSalesOrder:tempOrder IsAllocationMandatory:OrderLineItem==nil?YES:NO];
            return YES;
        }
        
    }
    else if (PlanAvailableBonusQty==appliedBonusQty)
    {
        if(OrderLineItem==nil){/** if order item deleting*/

            NSMutableArray  *tempLineitems=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"AppliedAssortMentPlanID==%d AND showApliedAssortMentPlanBonusProducts== 1",PlanId]] mutableCopy] ;
            /** if any item is available with deleting product bns_plan_id and showApliedAssortMentPlanBonusProducts status is YES*/
            if(tempLineitems.count==0){
                NSMutableArray  *tempLineitems2=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"AppliedAssortMentPlanID==%d",PlanId]] mutableCopy] ;
                /** if any item is available with deleting product bns_plan_id */
                if(tempLineitems2.count>0){
                    for (NSInteger i=0; i<tempSalesOrder.SalesOrderLineItemsArray.count; i++) {
                        
                        
                        SalesOrderLineItem *tempLineItem=  [tempSalesOrder.SalesOrderLineItemsArray objectAtIndex:i];
                        if(tempLineItem.AppliedAssortMentPlanID==PlanId){
                            tempLineItem.showApliedAssortMentPlanBonusProducts=YES;
                            [tempSalesOrder.SalesOrderLineItemsArray replaceObjectAtIndex:i withObject:tempLineItem];
                            break;
                        }
                    }
                }
            }
            AssortmentBonusProductsArray=tempSalesOrder.AssortmentBonusProductsArray;
            orderLineItemsArray=[tempSalesOrder.SalesOrderLineItemsArray mutableCopy];
            [self SortOrderLineItems];
            [self clearButtonTapped:nil];
        }else if(OrderLineItem.AppliedAssortMentPlanID==NSNotFound){
            
            NSMutableArray *tempItemsarray=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid==%@",OrderLineItem.Guid]] mutableCopy];
            
            if(OrderLineItem.manualFOCQty==0){
                OrderLineItem.AppliedAssortMentPlanID=PlanId;
                OrderLineItem.showApliedAssortMentPlanBonusProducts=NO;
            }
            if(tempItemsarray.count>0){
                [tempSalesOrder.SalesOrderLineItemsArray replaceObjectAtIndex:[tempSalesOrder.SalesOrderLineItemsArray indexOfObject:[tempItemsarray objectAtIndex:0]] withObject:OrderLineItem];
            }
        }else if(OrderLineItem.manualFOCQty>0){ /** if user applying foc to the product which is already included in any bonus plan, Then we need to remove paln id and showApliedAssortMentPlanBonusProducts need to asign to other product in the Plan porducts if ant exist*/
            NSLog(@"OrderLineItem.manualFOCQty>0");
            
            OrderLineItem.AppliedAssortMentPlanID=NSNotFound;
            
            if(OrderLineItem.showApliedAssortMentPlanBonusProducts){
                OrderLineItem.showApliedAssortMentPlanBonusProducts=NO;
                NSMutableArray *tempItemsarray=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid==%@",OrderLineItem.Guid]] mutableCopy];

                [tempSalesOrder.SalesOrderLineItemsArray replaceObjectAtIndex:[tempSalesOrder.SalesOrderLineItemsArray indexOfObject:[tempItemsarray objectAtIndex:0]] withObject:OrderLineItem];

                /** Assign showApliedAssortMentPlanBonusProducts assign yes to other same plan product*/
                NSMutableArray  *tempLineitems=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"AppliedAssortMentPlanID==%d AND showApliedAssortMentPlanBonusProducts== 1",PlanId]] mutableCopy] ;
                /** if any item is available with deleting product bns_plan_id and showApliedAssortMentPlanBonusProducts status is YES*/
                if(tempLineitems.count==0){
                    NSMutableArray  *tempLineitems2=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"AppliedAssortMentPlanID==%d",PlanId]] mutableCopy] ;
                    /** if any item is available with deleting product bns_plan_id */
                    if(tempLineitems2.count>0){
                        for (NSInteger i=0; i<tempSalesOrder.SalesOrderLineItemsArray.count; i++) {
                            SalesOrderLineItem *tempLineItem=  [tempSalesOrder.SalesOrderLineItemsArray objectAtIndex:i];
                            if(tempLineItem.AppliedAssortMentPlanID==PlanId){
                                tempLineItem.showApliedAssortMentPlanBonusProducts=YES;
                                [tempSalesOrder.SalesOrderLineItemsArray replaceObjectAtIndex:i withObject:tempLineItem];
                                break;
                            }
                        }
                    }
                }
            }
            

        }
        
    }
    else if (appliedBonusQty!=PlanAvailableBonusQty){
        [self PresentAssortmentBonusViewControllerWithPlanId:PlanId WithBonusQuantity:PlanAvailableBonusQty AndAssignedBonusItemsArray:tempAssortMentBonusProductsArray AndSelectedOrderLineItem:OrderLineItem AndTempSalesOrder:tempOrder IsAllocationMandatory:OrderLineItem==nil?YES:NO];
        return YES;
    }
    return NO;
}
-(BOOL)IsAnyUpdateOrNewAsstBnsPlanAvailableThenShowBonusAllocation:(SalesOrderLineItem *)OrderLineItem
{
    if(tempSalesOrder==nil){
        tempSalesOrder=[self getSalesOrderWithCurrentUnSavedOrderLineItem:OrderLineItem];
    }else{
        NSMutableArray *tempItemsarray=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid==%@",OrderLineItem.Guid]] mutableCopy];
        if(tempItemsarray.count>0){
            SalesOrderLineItem *lineItem=[tempItemsarray objectAtIndex:0];
            OrderLineItem.AppliedAssortMentPlanID=lineItem.AppliedAssortMentPlanID;
            OrderLineItem.showApliedAssortMentPlanBonusProducts=lineItem.showApliedAssortMentPlanBonusProducts;
            
        }
        
        tempSalesOrder.SalesOrderLineItemsArray=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid!=%@",OrderLineItem.Guid]] mutableCopy];
        [tempSalesOrder.SalesOrderLineItemsArray addObject:OrderLineItem];
    }
    
    /** checking availability of assortment bonus*/
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]&&
       ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
        /** Updating order item bonus checking*/
        if (OrderLineItem.AppliedAssortMentPlanID!=NSNotFound)
            return [self AlterBonusPlanIfRequired:OrderLineItem.AppliedAssortMentPlanID AndSelectedLineItem:OrderLineItem AndSalesOrder:tempSalesOrder];
        else{
            /** new order item bonus checking*/
            SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
            if([bnsManager IsAnyAssortmentBonusAvailableForTheProduct:[OrderLineItem.OrderProduct copy] AssortmentPlans:tempSalesOrder.AssortmentPlansArray])
            {
                NSMutableDictionary *planDetailsDic= [bnsManager FetchNewOrUpdatedAssortmentBonusPlanAvailableForSalesOrder:tempSalesOrder ForProduct:OrderLineItem] ;
                if(planDetailsDic!=nil){
                    return [self AlterBonusPlanIfRequired:[(SalesworxAssortmentBonusPlan*)[planDetailsDic valueForKey:@"Plan"]  PlanId] AndSelectedLineItem:OrderLineItem AndSalesOrder:tempSalesOrder];
                }else{
                    /** getting Default bonus*/
                    NSInteger availableBonusPlanId=[[[SalesWorxAssortmentBonusManager alloc]init]FetchAvailableBonusPlanForProduct:[OrderLineItem.OrderProduct copy] AssortmentPlans:tempSalesOrder.AssortmentPlansArray];
                    /** Checking if plan is applied to any other items in sales order*/
                    NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.AppliedAssortMentPlanID == %ld",(long)availableBonusPlanId]];
                    NSMutableArray *tempAssortMentBonusAppliedItemsArray=  [[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
                    /** altering the bonus plan items*/
                    if(tempAssortMentBonusAppliedItemsArray.count>0){
                        return [self AlterBonusPlanIfRequired:availableBonusPlanId AndSelectedLineItem:[tempAssortMentBonusAppliedItemsArray objectAtIndex:0] AndSalesOrder:tempSalesOrder];

                    }
                }
            }
        }
    }
    return NO;
}
-(SalesOrder *)getSalesOrderWithCurrentUnSavedOrderLineItem:(SalesOrderLineItem *)OrderLineItem
{
    /** creating temp salesorer */
        SalesOrder *tempOrderWithUnsavedLineItem=[[SalesOrder alloc]init];
        tempOrderWithUnsavedLineItem.SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
        tempOrderWithUnsavedLineItem.SalesOrderLineItemsArray=[[orderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid!=%@",OrderLineItem.Guid]] mutableCopy];
        [tempOrderWithUnsavedLineItem.SalesOrderLineItemsArray addObject:OrderLineItem];
        tempOrderWithUnsavedLineItem.AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
        tempOrderWithUnsavedLineItem.AssortmentBonusProductsArray=[AssortmentBonusProductsArray mutableCopy];
        tempOrderWithUnsavedLineItem.AssortmentPlansArray=[[NSMutableArray alloc]init];
        tempOrderWithUnsavedLineItem.AssortmentPlansArray=currentVisit.visitOptions.salesOrder.AssortmentPlansArray;
    return tempOrderWithUnsavedLineItem;
}
-(void)AddOrUpdateAsstBonusPlanForOrderLineItems{
    
        SalesOrderLineItem *lineItem=[[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid==%@",selectedOrderLineItem.Guid]] mutableCopy] objectAtIndex:0];
        selectedOrderLineItem.AppliedAssortMentPlanID=lineItem.AppliedAssortMentPlanID;
        selectedOrderLineItem.showApliedAssortMentPlanBonusProducts=lineItem.showApliedAssortMentPlanBonusProducts;
        
    
        NSMutableArray * SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
        if(isUpdatingOrder){
            SalesOrderLineItemsArray=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid!=%@",selectedOrderLineItem.Guid]] mutableCopy];
           // [SalesOrderLineItemsArray addObject:selectedOrderLineItem];
            [SalesOrderLineItemsArray insertObject:selectedOrderLineItem atIndex:0];

        }
        else{
            
            SalesOrderLineItemsArray=[[tempSalesOrder.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid!=%@",selectedOrderLineItem.Guid]] mutableCopy];

            [SalesOrderLineItemsArray insertObject:selectedOrderLineItem atIndex:0];
            
        }

        AssortmentBonusProductsArray=tempSalesOrder.AssortmentBonusProductsArray;
        orderLineItemsArray=SalesOrderLineItemsArray;
        [self SortOrderLineItems];
//    if([selectedOrderLineItem.Guid isEqualToString:]){
//        
//    }
    
}

#pragma mark SalesWorxSalesOrderItemDuplicateOHViewControllerDelegate methods
-(void)didUserConfirmedOHDuplicateItem{
    selectedOrderLineItem.isOHDuplicateItemEntryAlertConfirmedByUser=YES;
    [self CheckAndADDOrUpdateProductToOrderLineItems];
}
-(void)didUserCancelledOHDuplicateItem{
      [self clearAllProductLabelsAndTextFields];
      [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
}

#pragma mark Product Media Button Action
- (IBAction)productMediaButtonTapped:(id)sender {
    SalesWorxProductMediaViewController *productMediaViewController = [[SalesWorxProductMediaViewController alloc]init];
    productMediaViewController.selectedProductID = selectedProduct.ItemID;
    productMediaViewController.currentVisit = self.currentVisit;
    [self.navigationController pushViewController:productMediaViewController animated:YES];
}

@end


