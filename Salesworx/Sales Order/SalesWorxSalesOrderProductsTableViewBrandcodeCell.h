//
//  SalesWorxSalesOrderProductsTableViewBrandcodeCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxSalesOrderProductsTableViewBrandcodeCell : UITableViewCell

{
    
}
@property (strong,nonatomic) IBOutlet UILabel *brandCodeLabel;
@property (strong,nonatomic) IBOutlet UIImageView *expandCollapseArrowImageView;

@end
