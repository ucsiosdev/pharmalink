//
//  SalesWorxAssortmentBonusInfoProductsTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/5/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxAssortmentBonusInfoProductsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *productnameLabel;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *mandatoryLabel;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *ItemCodeLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xMadatoryLabelWidthConstraint;

@end
