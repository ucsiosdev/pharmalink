//
//  SalesOrderSalesHistoryInfoViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderSalesHistoryInfoViewController.h"

@interface SalesOrderSalesHistoryInfoViewController ()

@end

@implementation SalesOrderSalesHistoryInfoViewController
@synthesize OrderLineItem,customerVisit,isFromReturnsView,returnLineItem;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    salesHistoryArray=[[NSMutableArray
                        alloc]init];
    
    appControl=[AppControl retrieveSingleton];
     peroidsArray=[[NSMutableArray alloc]initWithObjects:@"Current Month",@"30 days ago",@"60 days ago",@"90 days ago",@"120 days ago",@"150 days ago",@"Prior",nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    /** if parent view  returns*/
    if(isFromReturnsView)
    {
        productCodeLabel.text=returnLineItem.returnProduct.Item_Code;
        productNameLabel.text=returnLineItem.returnProduct.Description;
        productDetails=returnLineItem.returnProduct;
    }
    else
    {
        productCodeLabel.text=OrderLineItem.OrderProduct.Item_Code;
        productNameLabel.text=OrderLineItem.OrderProduct.Description;
        productDetails=OrderLineItem.OrderProduct;

    }

    [self getServiceDidGetProductSales:[[SWDatabaseManager retrieveManager] dbGetCustomerProductSalesWithCustomerID:customerVisit.visitOptions.customer.Ship_Customer_ID andProductId:productDetails.Inventory_Item_ID]];
}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//NSNumber * avarageSales = [salesHistoryArray valueForKeyPath:@"@sum.BkB_Qty"];
-(void)UpdateAverageSalesLabel
{
    
    if([appControl.SHOW_AVERAGE_SALES_HISTORY isEqualToString:KAppControlsNOCode])
    {
        MonthlySalesAverageContainerViewHeightConstraint.constant=0;
    }
    else
    {
        NSNumber * totalSales = [salesHistoryArray valueForKeyPath:@"@sum.Quantity"];
        double averageSales= [totalSales doubleValue]/12;
        
        AverageSalesLabel.text=[NSString stringWithFormat:@"%0.2f",averageSales];
    }

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self UpdateAverageSalesLabel];
    return salesHistoryArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, salesHistoryTableHeaderView.frame.size.width, salesHistoryTableHeaderView.frame.size.height)];
    salesHistoryTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:salesHistoryTableHeaderView];
    return view;
}   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"SalesHistoryCell";
    SalesOrderSalesHistoryTableViewCell *cell = (SalesOrderSalesHistoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesOrderSalesHistoryTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSString *bonus;
//    if(indexPath.row==[salesHistoryArray count]-1)
//    {
//        double price=0;
//        for(NSInteger i = 0; i < salesHistoryArray.count-1; i++)
//        {
//            NSDictionary *row1 = [salesHistoryArray objectAtIndex:i];
//            price = price + [[row1 stringForKey:@"Bonus"] doubleValue];
//        }
//        double finalValue = [[[salesHistoryArray valueForKey:@"Bonus"] objectAtIndex:indexPath.row] doubleValue] - price;
//        bonus = [NSString stringWithFormat:@"%.0f",finalValue];
//    }
//    else
//    {
//        bonus = [[salesHistoryArray valueForKey:@"Bonus"] objectAtIndex:indexPath.row];
//    }
    bonus = [[salesHistoryArray valueForKey:@"Bonus"] objectAtIndex:indexPath.row];

    NSString *Quantity;

//    if(indexPath.row==[salesHistoryArray count]-1)
//    {
//        double price=0;
//        for(NSInteger i = 0; i < salesHistoryArray.count-1; i++)
//        {
//            NSDictionary *row1 = [salesHistoryArray objectAtIndex:i];
//            price = price + [[row1 stringForKey:@"Quantity"] doubleValue];
//        }
//        double finalValue = [[[salesHistoryArray valueForKey:@"Quantity"] objectAtIndex:indexPath.row] doubleValue] - price;
//        Quantity = [NSString stringWithFormat:@"%.0f",finalValue];
//    }
//    else
//    {
//        Quantity = [[salesHistoryArray valueForKey:@"Quantity"] objectAtIndex:indexPath.row];
//    }
    Quantity = [[salesHistoryArray valueForKey:@"Quantity"] objectAtIndex:indexPath.row];
    NSString *LastSold = [[salesHistoryArray valueForKey:@"LastSold"] objectAtIndex:indexPath.row];
    if(![LastSold isKindOfClass:[NSNull class]] && ![LastSold isEqualToString:@"<null>"])
    {
        LastSold = [[salesHistoryArray valueForKey:@"LastSold"] objectAtIndex:indexPath.row];
    }
    else
    {
        LastSold = @"NA";
   
    }

    cell.peroidLbl.text=[peroidsArray objectAtIndex:indexPath.row];
    cell.BonusLbl.text=bonus;
    cell.QuantityLbl.text=Quantity;
    cell.LastSoldLbl.text = LastSold;
    
    cell.peroidLbl.textColor=MedRepMenuTitleFontColor;
    cell.BonusLbl.textColor=MedRepMenuTitleFontColor;
    cell.QuantityLbl.textColor=MedRepMenuTitleFontColor;
    cell.LastSoldLbl.textColor=MedRepMenuTitleFontColor;

      cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
    return cell;
    
}

- (void)getServiceDidGetProductSales:(NSArray *)list
{

    if(list.count !=0)
    {
        NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary: [list objectAtIndex:0]];
        
        NSMutableDictionary *BK0Dict = [NSMutableDictionary dictionary];
        [BK0Dict setValue:[dic stringForKey:@"Bk0_Bns"] forKey:@"Bonus"];
        [BK0Dict setValue:[dic stringForKey:@"Bk0_Qty"] forKey:@"Quantity"];
        [BK0Dict setValue:[dic stringForKey:@"Bk0_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:[dic stringForKey:@"Last_Sold_At"] forKey:@"LastSold"];
        [BK0Dict setValue:@"0"  forKey:@"Index"];
        
        NSMutableDictionary *BK1Dict = [NSMutableDictionary dictionary];
        [BK1Dict setValue:[dic stringForKey:@"Bk1_Bns"] forKey:@"Bonus"];
        [BK1Dict setValue:[dic stringForKey:@"Bk1_Qty"] forKey:@"Quantity"];
        [BK1Dict setValue:[dic stringForKey:@"Bk1_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:[dic stringForKey:@"Last_Sold_At"] forKey:@"LastSold"];
        [BK1Dict setValue:@"1"  forKey:@"Index"];
        
        NSMutableDictionary *BK2Dict = [NSMutableDictionary dictionary];
        [BK2Dict setValue:[dic stringForKey:@"Bk2_Bns"] forKey:@"Bonus"];
        [BK2Dict setValue:[dic stringForKey:@"Bk2_Qty"] forKey:@"Quantity"];
        [BK2Dict setValue:[dic stringForKey:@"Bk2_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:[dic stringForKey:@"Last_Sold_At"] forKey:@"LastSold"];
        [BK2Dict setValue:@"2"  forKey:@"Index"];
        
        NSMutableDictionary *BK3Dict = [NSMutableDictionary dictionary];
        [BK3Dict setValue:[dic stringForKey:@"Bk3_Bns"] forKey:@"Bonus"];
        [BK3Dict setValue:[dic stringForKey:@"Bk3_Qty"] forKey:@"Quantity"];
        [BK3Dict setValue:[dic stringForKey:@"Bk3_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:[dic stringForKey:@"Last_Sold_At"] forKey:@"LastSold"];
        [BK3Dict setValue:@"3"  forKey:@"Index"];
        
        NSMutableDictionary *BK4Dict = [NSMutableDictionary dictionary];
        [BK4Dict setValue:[dic stringForKey:@"Bk4_Bns"] forKey:@"Bonus"];
        [BK4Dict setValue:[dic stringForKey:@"Bk4_Qty"] forKey:@"Quantity"];
        [BK4Dict setValue:[dic stringForKey:@"Bk4_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:[dic stringForKey:@"Last_Sold_At"] forKey:@"LastSold"];
        [BK4Dict setValue:@"4"  forKey:@"Index"];
        
        NSMutableDictionary *BK5Dict = [NSMutableDictionary dictionary];
        [BK5Dict setValue:[dic stringForKey:@"Bk5_Bns"] forKey:@"Bonus"];
        [BK5Dict setValue:[dic stringForKey:@"Bk5_Qty"] forKey:@"Quantity"];
        [BK5Dict setValue:[dic stringForKey:@"Bk5_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:[dic stringForKey:@"Last_Sold_At"] forKey:@"LastSold"];
        [BK5Dict setValue:@"5"  forKey:@"Index"];
        
        NSMutableDictionary *BKBDict = [NSMutableDictionary dictionary];
        [BKBDict setValue:[dic stringForKey:@"BkB_Bns"] forKey:@"Bonus"];
        [BKBDict setValue:[dic stringForKey:@"BkB_Qty"] forKey:@"Quantity"];
        [BKBDict setValue:[dic stringForKey:@"BkB_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:[dic stringForKey:@"Last_Sold_At"] forKey:@"LastSold"];
        [BKBDict setValue:@"6"  forKey:@"Index"];
        

        
        [salesHistoryArray addObject:BK0Dict];
        [salesHistoryArray addObject:BK1Dict];
        [salesHistoryArray addObject:BK2Dict];
        [salesHistoryArray addObject:BK3Dict];
        [salesHistoryArray addObject:BK4Dict];
        [salesHistoryArray addObject:BK5Dict];
        [salesHistoryArray addObject:BKBDict];
        
        //NSNumber * avarageSales = [salesHistoryArray valueForKeyPath:@"@sum.BkB_Qty"];
    }
    
    [salesHistoryTableView reloadData];
    
}


@end
