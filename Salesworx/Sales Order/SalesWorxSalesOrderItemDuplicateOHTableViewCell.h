//
//  SalesWorxSalesOrderItemDuplicateOHTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxSalesOrderItemDuplicateOHTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *orderNumberLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *orderQuantityLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *orderDateLabel;
@end
