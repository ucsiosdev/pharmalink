//
//  SalesOrderNotesViewConrollerViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderNotesViewConrollerViewController.h"

@interface SalesOrderNotesViewConrollerViewController ()

@end

@implementation SalesOrderNotesViewConrollerViewController
@synthesize notesText,isReadOnly,enableCancelButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    notesTextView.text=notesText;
    notesTextView.delegate=self;
    if(isReadOnly)
    {
        [saveButton setHidden:YES];
        [notesTextView setIsReadOnly:YES];
    }
    if(enableCancelButton){
        cancelButton.hidden = NO;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    notesTextView.text=[notesTextView.text trimString];
}
-(IBAction)saveButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    
    if([[notesTextView.text trimString]isEqualToString:@""])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter notes" withController:nil];
    }
    else
    {
        notesText=notesTextView.text;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [_salesOrderNotesViewConrollerViewControllerDelegate SaveNotes:notesText];
            [self dimissViewControllerWithSlideDownAnimation:YES];
        });
    }

}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [_salesOrderNotesViewConrollerViewControllerDelegate NotesCancelled];
        [self dimissViewControllerWithSlideDownAnimation:YES];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
#pragma mark UITextView methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textView.text.length==0 && [text isEqualToString:@" "])
    {
        return NO;
    }
    if(textView==notesTextView)
    {
        if([text isEqualToString:@"\n"])
        {
            [notesTextView endEditing:YES];
            return NO;
        }
        
        return ( newString.length<=KSalesOrderScreenNotesPopOverTextViewMaximumDigitsLimit);
    }
    
    return YES;
    
}

@end
