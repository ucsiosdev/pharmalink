//
//  SalesWorxAssortmentBonusInfoSlabsTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/5/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxAssortmentBonusInfoSlabsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *fromNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ToNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *bonusLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *typeLbl;
@end
