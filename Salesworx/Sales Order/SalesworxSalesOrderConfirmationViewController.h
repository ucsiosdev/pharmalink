//
//  SalesworxSalesOrderConfirmationViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/21/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJRSignatureView.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "CurrencyViewController.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SalesWorxTableViewHeaderView.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxSalesOrdersTableCellTableViewCell.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "SWDatabaseManager.h"
#import "FMDatabase.h"
#import <FMDBHelper.h>
#import "SalesOrderTemplateNameViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxVisitOptionsViewController.h"
#import "AppControl.h"
#import "SalesWorxTitleLabel2.h"
#import "SalesWorxImageAnnimateButton.h"
#import "SalesWorxSalesOrderLPOImagesCollectionViewCell.h"
#import "LPOImagesGalleryCollectionDetailViewController.h"
#import "SalesWorxImageView.h"
#import "SalesWorxDescriptionLabel1.h"
#import "SalesWorxDescriptionLabel2.h"
#import "SalesWorxSignatureView.h"


@interface SalesworxSalesOrderConfirmationViewController : UIViewController<SalesWorxDatePickerDelegate,SalesWorxPopoverControllerDelegate,SalesOrderTemplateNameViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,LPOImagesGalleryCollectionDetailViewControllerDelegate,SalesWorxSignatureViewDelegate,UIPopoverPresentationControllerDelegate>
{
    IBOutlet UIView *signatureViewContinerView;
    IBOutlet MedRepHeader *customerNameLabel;
    IBOutlet MedRepProductCodeLabel *customerCodeLabel;
    IBOutlet UILabel *customerAvailableBalanceLabel;
    IBOutlet SalesWorxImageView *customerstatusImageView;

    IBOutlet SalesWorxTitleLabel2 *lpoImagesTitleLabel;
    IBOutlet MedRepTextField *DocReferanceTexField;
    IBOutlet MedRepTextField *shipDateTextField;
    IBOutlet MedRepTextField *nameTextField;
    IBOutlet MedRepTextView *commentsTextView;
    IBOutlet MedRepTextField *DeliverToTextField;
    IBOutlet MedRepElementTitleLabel *DeliverToStringTitleLabel;

    IBOutlet UITableView *OrderAmountsTableView;
    NSMutableArray * OrderAmountsTableDataArray;

    IBOutlet UITableView *OrdersTableView;
    IBOutlet SalesWorxTableViewHeaderView *OrdersTableHeaderView;
    NSMutableArray *orderLineItemsArray;
    NSString *popOverTitle;
    IBOutlet UIPopoverController *confirmationPagePopOverController;
    SalesOrderConfirmationDetails *confirmationDetails;
    
    IBOutlet SalesWorxDefaultSegmentedControl *skipConsolidationSegment;
    IBOutlet UISwitch *skipConsolidationSwitch;
    IBOutlet UISwitch *wholeSaleOrderSwitch;
    BOOL isKeyBoardShowing;
    AppControl *appControl;
    
    IBOutlet UIView *skipConslidationView;
    IBOutlet UIView *wholeSaleOrderView;
    IBOutlet NSLayoutConstraint *skipConslidationViewHeightConstraint;
    IBOutlet NSLayoutConstraint *wholeSaleOrderViewHeightConstraint;
    IBOutlet NSLayoutConstraint *OrderAmountDetailsViewTopConstarint;
    
    UIBarButtonItem *confirmBarButtonItem;
    BOOL isWholeSaleOrderViewHidden;
    BOOL isSkipConsolidationViewHidden;
    NSMutableArray *confrimButtonPopOverOptions;
    
    NSString *shipDate;
    IBOutlet UICollectionView *lPOImgesCollectionView;
    NSMutableArray *LPOImgasesArray;
    IBOutlet NSLayoutConstraint *ConfirmationDetailsLeftSideBoxHeightConstraint;
    IBOutlet UIView *LPOImagesContentView;
    
    
    IBOutlet SalesWorxSignatureView *customerSignatureTopContainerView;
    IBOutlet SalesWorxTableViewHeaderView *orderItemsTableViewHeaderView;
    IBOutlet SalesWorxTableViewHeaderView *orderItemsTableViewHeaderwithOutDiscountLabelsView;
    BOOL isLPOPhotoLibraryAccessCheckingDone;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    
    /** Order Table */
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderDiscountLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderDiscountLabelLeadingMarginConstraint;
    IBOutlet MedRepElementTitleLabel * OrderTableHeaderTotalLabel;
    IBOutlet NSLayoutConstraint *xOrderAmountsTableViewHeightConstraint;
    IBOutlet NSLayoutConstraint *xOrderAmountsTableViewBottomMarginConstraint;

    IBOutlet NSLayoutConstraint *xDocRefNoLabelTopMarginToSuperViewConstraint;
    
    NSMutableArray * selectedCustomerShipSitesDetailsArray;
    NSDictionary * selectedDeliverToSiteDetailsDic;

}
@property (strong,nonatomic) SalesWorxVisit *currentVisit;

@end
