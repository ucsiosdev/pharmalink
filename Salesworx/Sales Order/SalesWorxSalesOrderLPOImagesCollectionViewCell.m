//
//  SalesWorxSalesOrderLPOImagesCollectionViewCell.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSalesOrderLPOImagesCollectionViewCell.h"

@implementation SalesWorxSalesOrderLPOImagesCollectionViewCell
@synthesize lpoImageview;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIGraphicsBeginImageContextWithOptions(lpoImageview.bounds.size, NO, 1.0);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:lpoImageview.bounds
                                cornerRadius:lpoImageview.frame.size.height /2] addClip];
    // Draw your image
    [lpoImageview.image drawInRect:lpoImageview.bounds];
    
    // Get the image, here setting the UIImageView image
    lpoImageview.image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxSalesOrderLPOImagesCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
    
}
@end
