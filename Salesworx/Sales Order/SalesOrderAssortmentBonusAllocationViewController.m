//
//  SalesOrderAssortmentBonusAllocationViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/26/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesOrderAssortmentBonusAllocationViewController.h"
#import "SalesOrderAssortmentBonusTableViewCell.h"
#import "SalesWorxAssortmentBonusInfoViewController.h"

#define KDeletingRowBackgroundColor [[UIColor redColor] colorWithAlphaComponent:0.6]
#define KAddButtonTitle @"Add"
#define KUpdateButtonTitle @"Update"
#define KAssortmentBonusProductsPopoverTitle @"Bonus Products"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
@interface SalesOrderAssortmentBonusAllocationViewController ()

@end

@implementation SalesOrderAssortmentBonusAllocationViewController
@synthesize isReadOnlyView,assignedBonusItemsArray,OrderedOfferItemsArray,isBonusAllocationMandatory,deletingOfferProduct;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{

    
    if(self.isMovingToParentViewController){
        if(assignedBonusItemsArray==nil){
            assignedBonusItemsArray=[[NSMutableArray alloc]init];
        }
        
        bonusQuantityLabel.text=[NSString stringWithFormat:@"%0.0f",_bonusQty];
        bonusItemsSelectionTableView.delegate=self;
        bonusItemsSelectionTableView.dataSource=self;
        isUpdating=NO;
        AssignedQtyLbl.text=@"0";
        [self updateAssignedQtyLabel];

        if(assignedBonusItemsArray.count>0){
            [bonusItemsSelectionTableView reloadData];
        }

        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
        if(isReadOnlyView){
            [QuantityTextField setIsReadOnly:YES];
            [productTextField setIsReadOnly:YES];
            [addButton setHidden:YES];
            [saveButton setHidden:YES];
            [CloseButton setHidden:NO];
        }else{
        //if(isBonusAllocationMandatory)
           // [CloseButton setHidden:YES];
        }
    }
    else{
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        [assigningBonusContentView setHidden:NO];
        
    }
    
    if (self.isMovingToParentViewController) {
        [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(ShowBonusQtyAssignmentAlertIfRequired) userInfo:nil repeats:NO];

    }
    if(_selectedSalesOrderLineItem==nil){/**Order item deleting by user*/
        errorMessageLbl.textColor=[UIColor redColor];
        NSDecimalNumber *quantityAlloted = [assignedBonusItemsArray valueForKeyPath:@"@sum.assignedQty"];
        [OrderedOfferItemsArray addObject:deletingOfferProduct];
        if(_bonusQty==0){
            [QuantityTextField setIsReadOnly:YES];
            [productTextField setIsReadOnly:YES];
            [addButton setHidden:YES];
            errorMessageLbl.text=@"The higlighted item and associated bonus are going to deleted.";
            [saveButton setTitle:@"Confirm" forState:UIControlStateNormal];
        }
        else if(_bonusQty!=quantityAlloted.doubleValue)
            errorMessageLbl.text=@"The highlighted item is going to deleted.Bonus quantity is changed, Please reassign bonus product quantities.";
    }else{
        errorMessageLbl.text=@"";
    }

}
-(void)ShowBonusQtyAssignmentAlertIfRequired
{
    if(assignedBonusItemsArray.count>0 ){
         NSDecimalNumber *quantityAlloted = [assignedBonusItemsArray valueForKeyPath:@"@sum.assignedQty"];
        if([[NSString stringWithFormat:@"%f",_bonusQty] integerValue]!=quantityAlloted.integerValue){
        [SWDefaults showAlertAfterHidingKeyBoard:@"Update bonus" andMessage:@"Bonus quantity has been changed. Please update bonus items quantity" withController:self];
        }
    }else if(assignedBonusItemsArray.count==0){
        [SWDefaults showAlertAfterHidingKeyBoard:@"New bonus" andMessage:@"New bonus has been achieved. Please assign bonus items" withController:self];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma UITABLEVIEW Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView==OrderItemsTableView)
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, OrderItemsTableHeaderView.frame.size.width, OrderItemsTableHeaderView.frame.size.height)];
        OrderItemsTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
        [view addSubview:OrderItemsTableHeaderView];
        return view;
    }
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, bonusItemsTableHeaderView.frame.size.width, bonusItemsTableHeaderView.frame.size.height)];
    bonusItemsTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:bonusItemsTableHeaderView];
    return view;
}   // custom view for header. will be adjusted to default or specified header height



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==OrderItemsTableView)
        return OrderedOfferItemsArray.count;
    return assignedBonusItemsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"LotsCell";
    SalesOrderAssortmentBonusTableViewCell *cell = (SalesOrderAssortmentBonusTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesOrderAssortmentBonusTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    if(tableView==OrderItemsTableView)
    {
        SalesOrderLineItem *orderItem=[OrderedOfferItemsArray objectAtIndex:indexPath.row];
        cell.productnameLabel.text=orderItem.OrderProduct.Description;
        cell.assignedbonusQuantityLabel.text=[NSString stringWithFormat:@"%@",orderItem.OrderProductQty];
        cell.UOmLabel.text=orderItem.OrderProduct.selectedUOM;
        
         if(deletingOfferProduct!=nil && deletingOfferProduct.Guid==orderItem.Guid){
            cell.contentView.backgroundColor=KDeletingRowBackgroundColor;
            cell.productnameLabel.textColor=[UIColor whiteColor];
            cell.UOmLabel.textColor=[UIColor whiteColor];
            cell.assignedbonusQuantityLabel.textColor=[UIColor whiteColor];
        }else{
            cell.contentView.backgroundColor=[UIColor whiteColor];
            cell.assignedbonusQuantityLabel.textColor=MedRepMenuTitleFontColor;
            cell.UOmLabel.textColor=MedRepMenuTitleFontColor;
            cell.productnameLabel.textColor=MedRepMenuTitleFontColor;
        }
    }
    else
    {
        SalesworxAssortmentBonusProduct *bnsPro=[assignedBonusItemsArray objectAtIndex:indexPath.row];
        cell.productnameLabel.text=bnsPro.product.Description;
        cell.assignedbonusQuantityLabel.text=[NSString stringWithFormat:@"%@",bnsPro.assignedQty ];
        cell.UOmLabel.text=[NSString stringWithFormat:@"%@",bnsPro.product.primaryUOM];
        
        if([selectedIndexpath isEqual:indexPath])
        {
            /*seclecting the present selected cell*/
            cell.productnameLabel.textColor=[UIColor whiteColor];
            cell.UOmLabel.textColor=[UIColor whiteColor];
            cell.assignedbonusQuantityLabel.textColor=[UIColor whiteColor];
            cell.contentView.backgroundColor=UITableviewSelectedCellBackgroundColor;
            
        }
        else
        {
            cell.assignedbonusQuantityLabel.textColor=MedRepMenuTitleFontColor;
            cell.UOmLabel.textColor=MedRepMenuTitleFontColor;
            cell.productnameLabel.textColor=MedRepMenuTitleFontColor;
            cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
            
        }
        if(_bonusQty==0 && _selectedSalesOrderLineItem==nil){ /** if user deleting the bonus quantity*/
            cell.contentView.backgroundColor=KDeletingRowBackgroundColor;
            cell.userInteractionEnabled=NO;
            cell.productnameLabel.textColor=[UIColor whiteColor];
            cell.UOmLabel.textColor=[UIColor whiteColor];
            cell.assignedbonusQuantityLabel.textColor=[UIColor whiteColor];
        }
        cell.rightButtons = @[[MGSwipeButton buttonWithTitle:NSLocalizedString(@"Delete", nil) backgroundColor:[UIColor redColor]]];
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
        cell.delegate=self;
    }

    
    return cell;
    
}
-(void)updateAddButtonTitle
{
    
    if(isUpdating)
    {
        [addButton setTitle:KUpdateButtonTitle forState:UIControlStateNormal];
    }
    else
    {
        [addButton setTitle:KAddButtonTitle forState:UIControlStateNormal];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==OrderItemsTableView)
    {
        
    }
    else
    {
        isUpdating=YES;
        [self updateAddButtonTitle];
        [productTextField setIsReadOnly:YES];
        selectedAssortmentBonusProduct=[[assignedBonusItemsArray objectAtIndex:indexPath.row] copy];
        QuantityTextField.text=[NSString stringWithFormat:@"%@",selectedAssortmentBonusProduct.assignedQty];
        productTextField.text=selectedAssortmentBonusProduct.product.Description;
        [self updateTheLotsTableViewSelectedCellIndexpath:indexPath];
  
    }
}
-(void)updateTheLotsTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    
    /* deselecting the previous selected cell*/
    if(selectedIndexpath!=nil)
    {
        SalesOrderAssortmentBonusTableViewCell *previousSelectedCell=[bonusItemsSelectionTableView cellForRowAtIndexPath:selectedIndexpath];
        previousSelectedCell.assignedbonusQuantityLabel.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.UOmLabel.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.productnameLabel.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
    }
    
    /*seclecting the present selected cell*/
    SalesOrderAssortmentBonusTableViewCell *currentSelectedcell=[bonusItemsSelectionTableView cellForRowAtIndexPath:indexpath];
    currentSelectedcell.assignedbonusQuantityLabel.textColor=[UIColor whiteColor];
    currentSelectedcell.UOmLabel.textColor=[UIColor whiteColor];
    currentSelectedcell.productnameLabel.textColor=[UIColor whiteColor];
    currentSelectedcell.contentView.backgroundColor=UITableviewSelectedCellBackgroundColor;
    selectedIndexpath=indexpath;
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (NSInteger)index, fromExpansion ? @"YES" : @"NO");
    
    if (direction == MGSwipeDirectionRightToLeft) {
        
        if(index==0)
        {
            //delete button
            NSIndexPath * path = [bonusItemsSelectionTableView indexPathForCell:cell];
            [assignedBonusItemsArray removeObjectAtIndex:path.row];
            [UIView beginAnimations:@"myAnimationId" context:nil];
            
            [UIView setAnimationDuration:0.5]; // Set duration here
            [CATransaction begin];
            [CATransaction setCompletionBlock:^{
                NSLog(@"Complete!");
            }];
            
            [bonusItemsSelectionTableView beginUpdates];
            [bonusItemsSelectionTableView deleteRowsAtIndexPaths:[[NSArray alloc]initWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [bonusItemsSelectionTableView endUpdates];
            
            [CATransaction commit];
            [UIView commitAnimations];
            [self  clearButtonTapped:nil];
            return NO; //Don't autohide to improve delete expansion animation
        }
        
    }
    
    return YES;
}
-(IBAction)clearButtonTapped:(id)sender
{
    [productTextField setIsReadOnly:NO];
    selectedIndexpath=nil;
    productTextField.text=@"";
    QuantityTextField.text=@"";
    isUpdating=NO;
    [self updateAddButtonTitle];
    [bonusItemsSelectionTableView reloadData];
    [self updateAssignedQtyLabel];

}
-(void)updateAssignedQtyLabel
{
    if(assignedBonusItemsArray.count>0){
        NSDecimalNumber *quantityAlloted = [assignedBonusItemsArray valueForKeyPath:@"@sum.assignedQty"];
        AssignedQtyLbl.text=[NSString stringWithFormat:@"%ld",(long)[quantityAlloted integerValue]];
        if([[NSString stringWithFormat:@"%f",_bonusQty] integerValue]!=quantityAlloted.integerValue){
            [AssignedQtyLbl setTextColor:[UIColor redColor]];
        }else{
            [AssignedQtyLbl setTextColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]];
        }
        
    }
    else{
        AssignedQtyLbl.text=@"0";
        [AssignedQtyLbl setTextColor:[UIColor redColor]];
    }
}

#pragma POPOVER DELEGATE METHODS
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath
{
    
}
-(void)didDismissPopOverController
{
    
}
#pragma UITextField Delegate Methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==productTextField) {
        popOverTitle=KAssortmentBonusProductsPopoverTitle;
        [self presentPopoverfor:productTextField withTitle:KAssortmentBonusProductsPopoverTitle withContent:[_plan.bonusProductsArray valueForKey:@"Description"]];
        return NO;
    }
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==QuantityTextField) {
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField==QuantityTextField)
    {
        NSString *expression = KWholeNumberQuantityRegExpression;
        
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 /*&& newString.doubleValue<=_bonusQty*/);
    }
    return YES;
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popOverTitle isEqualToString:KAssortmentBonusProductsPopoverTitle]) {
        Products *selectedProduct=[[_plan.bonusProductsArray objectAtIndex:selectedIndexPath.row] copy];
        selectedAssortmentBonusProduct=[[SalesworxAssortmentBonusProduct alloc]init];
        selectedAssortmentBonusProduct.product=selectedProduct;
        selectedAssortmentBonusProduct.assignedQty=0;
        QuantityTextField.text=@"";
        
        productTextField.text=selectedProduct.Description;
        [LotNumbersPopOverController dismissPopoverAnimated:YES];
        LotNumbersPopOverController=nil;
    }
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    [self.view endEditing:YES];
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    LotNumbersPopOverController=nil;
    LotNumbersPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    LotNumbersPopOverController.delegate=self;
    popOverVC.popOverController=LotNumbersPopOverController;
    popOverVC.titleKey=popOverString;
    [LotNumbersPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [LotNumbersPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


#pragma mark Calculations
-(IBAction)addButtonTapped:(id)sender
{
    [self.view endEditing:YES];

        NSPredicate * ProductItemNoPredicate=[NSPredicate predicateWithFormat:@"SELF.product.Item_Code ==[cd] %@",selectedAssortmentBonusProduct.product.Item_Code];
        
        NSMutableArray* filteredPredicateArray=[[assignedBonusItemsArray filteredArrayUsingPredicate:ProductItemNoPredicate] mutableCopy];
    
    
    
        if(productTextField.text.length==0)
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                                          andMessage:@"Please select bonus item" withController:self];

        }
        else if(QuantityTextField.text.doubleValue==0)
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                                    andMessage:@"Please enter quantity" withController:self];
        }
        
        else if((!isUpdating && filteredPredicateArray.count>0))
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                                    andMessage:@"Selected Product already added" withController:self];
        }
        else if([QuantityTextField.text doubleValue]>_bonusQty)
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                                    andMessage:@"Entered quantity greater than bonus quantity" withController:self];

        }
        else if(![self checkForTotalAssignedQty:[QuantityTextField.text doubleValue]])
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                                          andMessage:@"total quantity greater than bonus quantity" withController:self];
            
        }
        else
        {
            selectedAssortmentBonusProduct.assignedQty=[NSDecimalNumber ValidDecimalNumberWithString:QuantityTextField.text];
            selectedAssortmentBonusProduct.PlanId=_plan.PlanId;
            if(!isUpdating)
                [assignedBonusItemsArray addObject:selectedAssortmentBonusProduct];
            else
                [assignedBonusItemsArray replaceObjectAtIndex:selectedIndexpath.row withObject:selectedAssortmentBonusProduct];
            
            [bonusItemsSelectionTableView reloadData];
            QuantityTextField.text=@"";
            productTextField.text=@"";
            isUpdating=NO;
            [self updateAddButtonTitle];
            [self clearButtonTapped:nil];
            
        }
    
    [self updateAssignedQtyLabel];
    
}
-(BOOL)checkForTotalAssignedQty:(double)Qty
{
    
    NSDecimalNumber *quantityAlloted = [assignedBonusItemsArray valueForKeyPath:@"@sum.assignedQty"];
    
    if(isUpdating)
    {
        
        SalesworxAssortmentBonusProduct *tempLot=[assignedBonusItemsArray objectAtIndex:selectedIndexpath.row];
        NSDecimalNumber * updatingLotAssignedQuantity=  tempLot.assignedQty;
        if(_bonusQty<[quantityAlloted doubleValue]+Qty-[updatingLotAssignedQuantity doubleValue])
        {
            return NO;
        }
    }
    else
    {
        if(_bonusQty<[quantityAlloted doubleValue]+Qty)
        {
            return NO;
        }
    }
    
    return YES;
}
-(IBAction)saveButtonTapped:(id)sender
{
    
    NSDecimalNumber *quantityAlloted = [assignedBonusItemsArray valueForKeyPath:@"@sum.assignedQty"];
    
    if([quantityAlloted doubleValue]==_bonusQty || (_bonusQty==0 && _selectedSalesOrderLineItem==nil)/*if main items order quantity is less than any bonus slab bonus quantity will be zero.*/)
    {
        if(_bonusQty==0 && _selectedSalesOrderLineItem==nil)/*if main items order quantity is less than any bonus slab bonus quantity will be zero. so deleting the allocated bonus items and user deleting the line item.*/
            assignedBonusItemsArray=[[NSMutableArray alloc]init];
            
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseStockAllocationPopOver) userInfo:nil repeats:NO];
    }
    else if([quantityAlloted doubleValue]<_bonusQty)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error"
                                andMessage:@"Allocated Quantity less than bonus Quantity" withController:self];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error"
                                andMessage:@"Allocated Quantity exceeds bonus Quantity" withController:self];
    }
    
    
}
-(IBAction)cancelButtonTapped:(id)sender
{
    if(isReadOnlyView)
    {
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CancelStockAllocation) userInfo:nil repeats:NO];
    }
    else{
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action){
                                        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
                                        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CancelStockAllocation) userInfo:nil repeats:NO];                                }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:KAlertNoButtonTitle],nil];
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KWarningAlertTitleStr andMessage:@"Would you like to close? Unsaved changes will be discarded." andActions:actionsArray withController:self];
    }
   
}
-(void)CancelStockAllocation
{
    [self dismissViewControllerAnimated:NO completion:^{
        
        //[_salesOrderLotAssigningViewControllerDelegate lotsAllocationCancelled];
        
    }];
    
}

-(void)CloseStockAllocationPopOver
{
    [self dismissViewControllerAnimated:NO completion:^{
        if(_bonusQty==0){
            assignedBonusItemsArray=[[NSMutableArray alloc]init];
        }
        
        [assignedBonusItemsArray setValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.0f",_bonusQty]] forKey:@"PlanDefaultBnsQty"];/** assigning plan bonus qty as default qty*/
        [_assortmentBonusAllocationViewControllerDelegate UpdateAssortmentBonusAllcation:assignedBonusItemsArray WithPlan:_plan AndSelectedOrderItem:_selectedSalesOrderLineItem];
    }];
    
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = assigningBonusContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

- (IBAction)AsstBonusInfoButtonTapped:(id)sender {
    SalesWorxAssortmentBonusInfoViewController *assortmentBonusInfoViewController=[[SalesWorxAssortmentBonusInfoViewController alloc]initWithNibName:@"SalesWorxAssortmentBonusInfoViewController" bundle:[NSBundle mainBundle]];
    assortmentBonusInfoViewController.plan=_plan;
    [assigningBonusContentView setHidden:YES];
    [self.navigationController pushViewController:assortmentBonusInfoViewController animated:NO];
}
@end
