//
//  SalesWorxAssortmentBonusInfoViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/5/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SalesWorxTableViewHeaderView.h"
@interface SalesWorxAssortmentBonusInfoViewController : UIViewController
{
    IBOutlet UITableView *offerItemsTableView;
    IBOutlet UITableView *BonusItemsTableView;
    IBOutlet UITableView *PlanSlabsTableView;
    IBOutlet UIView *bonusPopUpView;

    IBOutlet SalesWorxTableViewHeaderView *bonusTableHeaderView;

    IBOutlet NSLayoutConstraint *xContentViewHeightConstraint;
}
@property (strong,nonatomic) SalesworxAssortmentBonusPlan *plan;
@end
