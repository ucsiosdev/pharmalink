//
//  SalesWorxSalesOrderItemDuplicateOHViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableViewHeaderView.h"
#import "SalesWorxTableView.h"
#import "SalesWorxPresentControllerBaseViewController.h"
#import "MedRepElementDescriptionLabel.h"
@protocol SalesWorxSalesOrderItemDuplicateOHViewControllerDelegate
-(void)didUserConfirmedOHDuplicateItem;
-(void)didUserCancelledOHDuplicateItem;
@end

@interface SalesWorxSalesOrderItemDuplicateOHViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet SalesWorxTableViewHeaderView *stockTableHeaderView;
    IBOutlet SalesWorxTableView *productOrderHistoryTableView;
    IBOutlet MedRepElementDescriptionLabel *messageLabel;

}
- (IBAction)CancelButtonTapped:(id)sender;
- (IBAction)ConfirmButtonTapped:(id)sender;
@property (strong,nonatomic) NSMutableArray *productOrderHistoryArray;
@property (strong,nonatomic) id <SalesWorxSalesOrderItemDuplicateOHViewControllerDelegate>oHDelegate;

@end
