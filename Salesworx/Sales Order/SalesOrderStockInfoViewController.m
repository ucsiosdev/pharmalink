//
//  SalesOrderStockInfoViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderStockInfoViewController.h"

@interface SalesOrderStockInfoViewController ()

@end

@implementation SalesOrderStockInfoViewController
@synthesize OrderLineItem,stockLotsArray,returnLineItem,isFromReturnsView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
       stockLotsArray=[[NSMutableArray alloc]init];
        appControl = [AppControl retrieveSingleton];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    /** if parent view  returns*/
    if(isFromReturnsView)
    {
        productCodeLabel.text=returnLineItem.returnProduct.Item_Code;
        productNameLabel.text=returnLineItem.returnProduct.Description;
        productDetails=returnLineItem.returnProduct;
        
    }
    else
    {
        productCodeLabel.text=OrderLineItem.OrderProduct.Item_Code;
        productNameLabel.text=OrderLineItem.OrderProduct.Description;
        productDetails=OrderLineItem.OrderProduct;

    }

    [self fetchStockForProduct:productDetails];
    
    if([appControl.SHOW_ON_ORDER isEqualToString:KAppControlsNOCode])
    {
        [onOrderQuantityView setHidden:YES];
        onOrderQuantityViewHeightConstraint.constant=0;
    }
    else
    {
        
        
        NSString *onOrderQty=[[[SWDatabaseManager alloc]init]dbGetOnOrderQuantityOfProduct:productDetails.ItemID];
        OnOrderQtyLabel.text=onOrderQty==nil?@"0":onOrderQty;
        
    }
}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return stockLotsArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, stockTableHeaderView.frame.size.width, stockTableHeaderView.frame.size.height)];
    stockTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:stockTableHeaderView];
    return view;
}   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"stockLotCell";
    SalesOrderStockInfoTableViewCell *cell = (SalesOrderStockInfoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesOrderStockInfoTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    

    
    Stock *lot=[stockLotsArray objectAtIndex:indexPath.row];
    cell.LotNumberLbl.text=lot.Lot_No;
    cell.QuantityLbl.text=[NSString stringWithFormat:@"%@ %@",lot.Lot_Qty,productDetails.primaryUOM];
    cell.ExpiryDateLbl.text=[lot.Expiry_Date isEqualToString:@""]?KNotApplicable:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:lot.Expiry_Date];
    cell.WareHouseLbl.text=lot.warehouse;
    cell.OnOrderLbl.text=lot.On_Order_Qty;
    
    
    cell.LotNumberLbl.textColor=MedRepMenuTitleFontColor;
    cell.QuantityLbl.textColor=MedRepMenuTitleFontColor;
    cell.ExpiryDateLbl.textColor=MedRepMenuTitleFontColor;
    cell.WareHouseLbl.textColor=MedRepMenuTitleFontColor;
    cell.OnOrderLbl.textColor=MedRepMenuTitleFontColor;
    cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
    return cell;
    
}

-(void)fetchStockForProduct:(Products*)selectedProduct
{
    NSInteger productStockValue=0;
    
    stockLotsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * tempProductStockArray=[[NSMutableArray alloc]init];;
    
    //    NSMutableArray * tempProductStockArray=[[[SWDatabaseManager retrieveManager]dbGetStockInfo:selectedProduct.ItemID] mutableCopy];
   // NSLog(@"stock array in fetchStockDataforItemID %@", tempProductStockArray);
    
    
    if([appControl.SHOW_ORG_STOCK_ONLY isEqualToString:KAppControlsYESCode])
    {
       tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:selectedProduct WareHouseId:_wareHouseId];
    }
    else
    {
       tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:selectedProduct WareHouseId:KNotApplicable];
    }

    
    
    if (tempProductStockArray.count>0) {
        
        for (NSMutableDictionary * currentStockDict in tempProductStockArray) {
            
            Stock * productStock=[[Stock alloc]init];
            //NSLog(@"refined stock dict is %@", currentStockDict);
            productStock.Lot_No=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Lot_No"]];
            productStock.Lot_Qty=[NSString stringWithFormat:@"%0.0f",[[currentStockDict valueForKey:@"Lot_Qty"] doubleValue] *[SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[selectedProduct copy]]];
            productStock.Org_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Org_ID"]];
            productStock.Stock_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Stock_ID"]];
            productStock.Expiry_Date=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Expiry_Date"]];
            productStock.warehouse=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Description"]];
            
            productStockValue= productStockValue+[productStock.Lot_Qty integerValue];
            
            if( [productStock.Lot_Qty doubleValue]>0)
            {
                [stockLotsArray addObject:productStock];
                
            }
        }
    }
    [stockTableView reloadData];
    
}

@end
