//
//  SalesOrderTitleLabel.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderTitleLabel.h"

@implementation SalesOrderTitleLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.font=KOpenSansSemiBold12;
    self.textColor=MedRepElementTitleLabelFontColor;
    
}



@end
