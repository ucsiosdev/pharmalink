//
//  SalesOrderBonusInfoViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderBonusInfoViewController.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"

@interface SalesOrderBonusInfoViewController ()
    
    @end

@implementation SalesOrderBonusInfoViewController
    @synthesize OrderLineItem,bonusArray,productsArray,isFromReturnsView,returnLineItem;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
    {
        /** if parent view  returns*/
        if(isFromReturnsView)
        {
            productCodeLabel.text=returnLineItem.returnProduct.Item_Code;
            productNameLabel.text=returnLineItem.returnProduct.Description;
            
        }
        else
        {
            productCodeLabel.text=OrderLineItem.OrderProduct.Item_Code;
            productNameLabel.text=OrderLineItem.OrderProduct.Description;
        }
        
        if (self.isOrderQuantityRecommended || self.isOrderQuantityRecommendationMandatory) {
            titleLabel.text = @"Order Quantity Recommendation";
            lblEnteredQuantity.hidden = NO;
            lblTitleEnteredQuantity.hidden = NO;
            lblEnteredQuantity.text = [NSString stringWithFormat:@"%@ %@", OrderLineItem.OrderProductQty, OrderLineItem.OrderProduct.primaryUOM];
            
            // find recommended bonus index
            int recommendedBonus = INT32_MAX;
            int x = OrderLineItem.OrderProductQty.intValue;
            
            for (int i=0; i < [bonusArray count]; i++)
            {
                ProductBonusItem *bonus = [bonusArray objectAtIndex:i];
                int num = bonus.Prom_Qty_From.intValue;
                int diff = abs(num - x);
                if (diff < recommendedBonus)
                {
                    recommendedBonus = diff;
                    indexOfRecommendedBonus = i;
                }
            }
        } else {
            titleLabel.text = @"Bonus Details";
            lblEnteredQuantity.hidden = YES;
            lblTitleEnteredQuantity.hidden = YES;
        }
        
        if (self.isOrderQuantityRecommendationMandatory) {
            saveButton.hidden = NO;
            notesView.hidden = NO;
            xNotesViewHeightConstraint.constant = 120.0;
            xNotesViewBottomConstraint.constant = 8.0;
            recommendedLegendView.hidden = NO;
            xRecommendedViewHeightConstraint.constant = 50.0;
            xRecommendedViewBottomConstraint.constant = 8.0;
        } else if (self.isOrderQuantityRecommended) {
            saveButton.hidden = YES;
            notesView.hidden = YES;
            xNotesViewHeightConstraint.constant = 0.0;
            xNotesViewBottomConstraint.constant = 0.0;
            recommendedLegendView.hidden = NO;
            xRecommendedViewHeightConstraint.constant = 50.0;
            xRecommendedViewBottomConstraint.constant = 8.0;
        }
        else {
            saveButton.hidden = YES;
            notesView.hidden = YES;
            xNotesViewHeightConstraint.constant = 0.0;
            xNotesViewBottomConstraint.constant = 0.0;
            recommendedLegendView.hidden = YES;
            xRecommendedViewHeightConstraint.constant = 0.0;
            xRecommendedViewBottomConstraint.constant = 0.0;
        }
    }
-(IBAction)cancelButtonTapped:(id)sender
    {
        [self dimissViewControllerWithSlideDownAnimation:YES];
    }
- (IBAction)saveButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (notesTextView.text.length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:@"Notes is mandatory if no bonus has been applied to the item." withController:self];
    } else {
        OrderLineItem.proximityNotes = notesTextView.text;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self dimissViewControllerWithSlideDownAnimation:YES];
            
            [_bonusDelegate updateOrderLineItem:OrderLineItem atIndex:_selectedOrderLineItemIndexForBonusRecommendation];
        });
    }
}
    
#pragma mark UITextView methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
    {
        NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
        if([text length] == 0)
        {
            if([textView.text length] != 0)
            {
                return YES;
            }
            else {
                return NO;
            }
        }
        if((textView.text.length==0 && [text isEqualToString:@" "]) || (textView.text.length==0 && [text isEqualToString:@"\n"]))
        {
            return NO;
        }
        if(textView==notesTextView)
        {
            return (newString.length<=KSalesOrderScreenCommentsTextViewMaximumDigitsLimit);
        }
        return YES;
    }
    
- (void)textViewDidBeginEditing:(UITextView *)textView {
        [self animateTextView:textView up:YES];
    }
    
- (void)textViewDidEndEditing:(UITextView *)textView {
        [self animateTextView:textView up:NO];
    }
    
- (void) animateTextView:(UITextView*)textField up: (BOOL) up
    {
        NSInteger movementDistance = 190;
        const float movementDuration = 0.3f; // tweak as needed
        
        NSInteger movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
    
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        
        return 1;
    }
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        
        return bonusArray.count;
    }
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
    {
        return 44;
    }
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
    {
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
    }
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, bonusTableHeaderView.frame.size.width, bonusTableHeaderView.frame.size.height)];
        bonusTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
        if (self.isOrderQuantityRecommended || self.isOrderQuantityRecommendationMandatory) {
            xFromLabelLeadingConstraint.constant = 12.0;
        } else {
            xFromLabelLeadingConstraint.constant = 8.0;
        }
        [view addSubview:bonusTableHeaderView];
        return view;
    }   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        static NSString* identifier=@"LotsCell";
        SalesOrderBonusInfoTableViewCell *cell = (SalesOrderBonusInfoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesOrderBonusInfoTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        
        
        ProductBonusItem *bonus=[bonusArray objectAtIndex:indexPath.row];
        cell.fromNumberLbl.text=bonus.Prom_Qty_From;
        cell.ToNumberLbl.text=bonus.Prom_Qty_To;
        cell.typeLbl.text=bonus.Price_Break_Type_Code;
        cell.bonusItemNameLabel.text=bonus.Get_Item;
        cell.bonusLbl.text=bonus.Get_Qty;
        
        
        cell.fromNumberLbl.textColor=MedRepMenuTitleFontColor;
        cell.ToNumberLbl.textColor=MedRepMenuTitleFontColor;
        cell.bonusLbl.textColor=MedRepMenuTitleFontColor;
        cell.typeLbl.textColor=MedRepMenuTitleFontColor;
        cell.bonusItemNameLabel.textColor=MedRepMenuTitleFontColor;
        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
        
        NSPredicate *bonusProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",bonus.Get_Item];
        NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:bonusProductPredicate]];
        
        
        if(tempProductsArray.count>0)
        {
            Products* bonusProduct=[tempProductsArray objectAtIndex:0];
            cell.bonusItemNameLabel.text=bonusProduct.Description;
            cell.bonusLbl.text=[NSString stringWithFormat:@"%@ %@",bonus.Get_Qty,bonusProduct.primaryUOM];
            
        }
        
        if (self.isOrderQuantityRecommended || self.isOrderQuantityRecommendationMandatory) {
            cell.btnSetQty.hidden = NO;
            cell.btnSetQty.tag = indexPath.row;
            [cell.btnSetQty addTarget:self action:@selector(btnSetQtyTapped:) forControlEvents:UIControlEventTouchUpInside];
            cell.xBtnSetQtyWidthConstraint.constant = 100.0;
            cell.xBtnSetQtyTrailingConstraint.constant = 8.0;
            cell.xFromLabelLeadingConstraint.constant = 12.0;
            
            if (indexOfRecommendedBonus == indexPath.row) {
                cell.statusView.hidden = NO;
                cell.contentView.backgroundColor = [UIColor colorWithRed:(251.0/255.0) green:(233.0/255.0) blue:(255.0/255.0) alpha:1];
            } else {
                cell.statusView.hidden = YES;
                cell.contentView.backgroundColor = UIColor.whiteColor;
            }
        } else {
            cell.btnSetQty.hidden = YES;
            cell.xBtnSetQtyWidthConstraint.constant = 0;
            cell.xBtnSetQtyTrailingConstraint.constant = 0;
            cell.xFromLabelLeadingConstraint.constant = 8.0;
            cell.contentView.backgroundColor = UIColor.whiteColor;
            cell.statusView.hidden = YES;
        }
        
        return cell;
    }
    
-(void)btnSetQtyTapped:(id)Sender
{
    UIButton *selectedButton = Sender;
    ProductBonusItem *bonus=[bonusArray objectAtIndex:selectedButton.tag];
    OrderLineItem.OrderProductQty = [NSDecimalNumber ValidDecimalNumberWithString:bonus.Prom_Qty_From];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self dimissViewControllerWithSlideDownAnimation:YES];
        
        [_bonusDelegate updateOrderLineItem:OrderLineItem atIndex:_selectedOrderLineItemIndexForBonusRecommendation];
    });
}
    
    @end
