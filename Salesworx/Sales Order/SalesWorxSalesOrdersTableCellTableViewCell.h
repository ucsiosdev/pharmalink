//
//  SalesWorxSalesOrdersTableCellTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementTitleLabel.h"
#import "MedRepView.h"
#import "MGSwipeTableCell.h"
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"
@interface SalesWorxSalesOrdersTableCellTableViewCell : MGSwipeTableCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ProductNameLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *QunatutyLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *UnitPriceLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ToatlLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *DiscountLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *VATChargesLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *NetAmountLbl;
@property (strong, nonatomic) IBOutlet MedRepView *statusView;
@property (strong, nonatomic) IBOutlet MedRepView *lineItemRowAnimationView;
@property (nonatomic) OrderItemsTableViewCellType CellItemType;
@property (weak, nonatomic) IBOutlet UIButton *btnProximityIndicator;
    
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xVATChargeLabelWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xNetAmountLabelWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xDiscountLabelWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xVATChargeLabelLeadingMarginConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xNetAmountLabelLeadingMarginConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xDiscountLabelLeadingMarginConstraint;
@end
