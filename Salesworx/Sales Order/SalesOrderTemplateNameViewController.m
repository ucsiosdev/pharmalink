//
//  SalesOrderTemplateNameViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/25/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderTemplateNameViewController.h"

@interface SalesOrderTemplateNameViewController ()<UITextFieldDelegate>

@end

@implementation SalesOrderTemplateNameViewController
@synthesize templateName;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    if(templateName!=nil)
        TemplateNameTextField.text=templateName;
    
    [TemplateNameTextField becomeFirstResponder];
}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self.view endEditing:YES];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self dimissViewControllerWithSlideDownAnimation:YES];
    });
}

-(IBAction)saveButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    
    if(TemplateNameTextField.text.length==0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter template name" withController:self];
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self dimissViewControllerWithSlideDownAnimation:YES];
            
            NSString *trimmedString = [TemplateNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [_salesOrderTemplateNameViewControllerDelegate saveTemplateName:trimmedString];
        });
    }
}
-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:alertTitle
                                      message:alertMessage
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:KAlertOkButtonTitle
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField==TemplateNameTextField)
    {
        NSString *expression = @"^[0-9a-z ]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<50);
    }
    
    return YES;
}
@end
