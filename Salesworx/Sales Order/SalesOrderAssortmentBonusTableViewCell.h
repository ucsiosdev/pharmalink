//
//  SalesOrderAssortmentBonusTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/28/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "MGSwipeTableCell.h"
@interface SalesOrderAssortmentBonusTableViewCell : MGSwipeTableCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *productnameLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *assignedbonusQuantityLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *UOmLabel;
@end
