//
//  SalesOrderTemplateNameViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/25/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepTextField.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@protocol SalesOrderTemplateNameViewControllerDelegate

-(void)saveTemplateName:(NSString *)templateName;

@end

@interface SalesOrderTemplateNameViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet MedRepTextField *TemplateNameTextField;
    
}
@property (strong,nonatomic) id <SalesOrderTemplateNameViewControllerDelegate>salesOrderTemplateNameViewControllerDelegate;
@property (strong,nonatomic) NSString *templateName;
@end
