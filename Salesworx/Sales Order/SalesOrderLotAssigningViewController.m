//
//  SalesOrderLotAssigningViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderLotAssigningViewController.h"
#import "SWDatabaseManager.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
#define KLotsPopOverTitle @"Assigning Lots"
#define KSalesOrderLotAddButtonTitle @"Add"
#define KSalesOrderLotUpdateButtonTitle @"Update"
@interface SalesOrderLotAssigningViewController ()
{
    AppControl* appControl ;
}
@end

@implementation SalesOrderLotAssigningViewController
@synthesize OrderLineItem,isReAllocationForUpdatedQuantity,isLotsAssignmentReadOnly;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    AssignedLotsArray=[[NSMutableArray alloc]init];
    appControl = [AppControl retrieveSingleton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated

{
    [self fetchStockDataforItemID:OrderLineItem.OrderProduct];
    isUpdating=NO;
    orderQuantityLabel.text=[NSString stringWithFormat:@"%@ %@",OrderLineItem.OrderProductQty,OrderLineItem.OrderProduct.selectedUOM];
    if(OrderLineItem.AssignedLotsArray.count>0)
        AssignedLotsArray=[OrderLineItem.AssignedLotsArray mutableCopy];
    [lotSelectionTableView reloadData];
    
    
    productNameLabel.text=OrderLineItem.OrderProduct.Description;
    productCodeLabel.text=OrderLineItem.OrderProduct.Item_Code;
    
    
    if(isLotsAssignmentReadOnly)
    {
        [QuantityTextField setIsReadOnly:YES];
        [LotNumberTextField setIsReadOnly:YES];
        [addButton setHidden:YES];
        [saveButton setHidden:YES];
        
    }
    
}

-(void)fetchStockDataforItemID:(Products*)selectedProduct
{
    NSInteger productStockValue=0;
    
    productStockArray=[[NSMutableArray alloc]init];
    NSMutableArray *tempProductStockArray=[[NSMutableArray alloc]init];
    
    
   
//    if([appControl.SHOW_ORG_STOCK_ONLY isEqualToString:KAppControlsYESCode])
//    {
//       tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:selectedProduct WareHouseId:_wareHouseId];
//    }
//    else
//    {
//       tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:selectedProduct WareHouseId:KNotApplicable];
//    }
    /**  stock will be allocated to selected to ware house only*/
    tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:selectedProduct WareHouseId:_wareHouseId];

    NSLog(@"stock array in fetchStockDataforItemID %@", tempProductStockArray);

    if (tempProductStockArray.count>0) {
        
        for (NSMutableDictionary * currentStockDict in tempProductStockArray) {
            
            Stock * productStock=[[Stock alloc]init];
            NSLog(@"refined stock dict is %@", currentStockDict);
            productStock.Lot_No=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Lot_No"]];
            productStock.Lot_Qty=[NSString stringWithFormat:@"%0.0f",[[currentStockDict valueForKey:@"Lot_Qty"] doubleValue] *[SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[selectedProduct copy]]];
            
            productStock.lotQuantityNumber=[SWDefaults getQuantityWithRespectToSelectedUOMCodeForProducrt:[selectedProduct copy] PrimaryUOMQuanity:[NSDecimalNumber ValidDecimalNumberWithString:[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Lot_Qty"]]]];
            
            
            productStock.Org_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Org_ID"]];
            productStock.Stock_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Stock_ID"]];
            productStock.Expiry_Date=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Expiry_Date"]];
            productStock.On_Order_Qty=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"On_Order_Qty"]];
            
            productStock.warehouse=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Description"]];
            productStock.lotNumberWithExpiryDate=[NSString stringWithFormat:@"%@ ## %@ ",productStock.Lot_No,[productStock.Expiry_Date isEqualToString:@""]?KNotApplicable:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:productStock.Expiry_Date]];
            productStockValue= productStockValue+[productStock.Lot_Qty integerValue];
            if( [productStock.lotQuantityNumber doubleValue]>0)
            {
                [productStockArray insertObject:productStock atIndex:0];
                
            }
        }
    }
    [lotSelectionTableView reloadData];
    
    SalesOrderLotAssigningTableViewCell *Cell=[lotSelectionTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    Cell.contentView.alpha = 0.5;
    UIView *border = [UIView new];
    border.backgroundColor = [UIColor colorWithRed:(30.0/255.0) green:(216.0/255.0) blue:(255.0/255.0) alpha:1.0];
    border.alpha=0.3;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(Cell.frame.size.width/2, 0, 10.0, 2.0);
    [Cell addSubview:border];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    border.alpha = 1.0;
    Cell.contentView.alpha = 1.0;
    border.frame = CGRectMake(0, 0, Cell.frame.size.width, 2.0);
    [UIView commitAnimations];


}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)addButtonTapped:(id)sender
{
    
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];

    } completion: ^(BOOL finished) {

        NSPredicate * lotNumberPredicate=[NSPredicate predicateWithFormat:@"SELF.lot.Lot_No ==[cd] %@",selectedAssignedLot.lot.Lot_No];
        
        NSMutableArray* filteredPredicateArray=[[AssignedLotsArray filteredArrayUsingPredicate:lotNumberPredicate] mutableCopy];
        
        if(QuantityTextField.text.doubleValue==0)
        {
            [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                                    andMessage:@"Please enter quantity" withController:nil];
        }

        else if((!isUpdating && filteredPredicateArray.count>0))
        {
            [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                 andMessage:@"Lot already added" withController:nil];
        }
        else if(![self checkForLotQuantity:[NSDecimalNumber ValidDecimalNumberWithString:QuantityTextField.text]])
        {
            [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                 andMessage:@"Entered quantity greater than lot quantity" withController:nil];
        }
        else if(![self checkForOrderQty:[NSDecimalNumber ValidDecimalNumberWithString:QuantityTextField.text]])
        {
            [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                 andMessage:@"Entered quantity greater than ordered quantity" withController:nil];
        }
        else if(![self checkForAssignedQty:[NSDecimalNumber ValidDecimalNumberWithString:QuantityTextField.text]])
        {
            [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                 andMessage:@"Total assigned quantity  greater than ordered quantity" withController:nil];
        }
        else if(![self checkForTotalLotsQty:[NSDecimalNumber ValidDecimalNumberWithString:QuantityTextField.text]])
        {
            [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                 andMessage:@"Total assigned quantity greater than available lots quantity" withController:nil];
        }
        else if(![self isAssignedLotItemWithValidDecimalAllotedQuantity:[NSDecimalNumber ValidDecimalNumberWithString:QuantityTextField.text]])
        {
            [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr
                                    andMessage:@"Please enter valid quantity" withController:nil];
        }
        else
        {
            if(selectedAssignedLot){
                selectedAssignedLot.assignedQuantity=[NSDecimalNumber ValidDecimalNumberWithString:QuantityTextField.text];
                if(!isUpdating)
                {
                    if (selectedAssignedLot != nil ) {
                        [AssignedLotsArray addObject:selectedAssignedLot];
                    }
                }
                else
                {
                    if (selectedAssignedLot != nil ) {
                        [AssignedLotsArray replaceObjectAtIndex:selectedLotIndexpath.row withObject:selectedAssignedLot];
                    }
                }
            }
            [lotSelectionTableView reloadData];
            QuantityTextField.text=@"";
            LotNumberTextField.text=@"";
            isUpdating=NO;
            lotQuantityLabel.text=KNotApplicable;
            [self updateAddButtonTitle];
            [self clearButtonTapped:nil];
            
        }

    }];
}
-(BOOL)checkForLotQuantity:(NSDecimalNumber *)Qty
{
    if([selectedAssignedLot.lot.lotQuantityNumber isLessThanDecimal:Qty])
    {
        return NO;
    }
    return YES;

}
-(BOOL)checkForOrderQty:(NSDecimalNumber *)Qty
{
    if([OrderLineItem.OrderProductQty isLessThanDecimal:Qty])
    {
        return NO;
    }
    return YES;
    
}
-(BOOL)checkForAssignedQty:(NSDecimalNumber *)Qty
{
    
    NSDecimalNumber *quantityAlloted = [AssignedLotsArray valueForKeyPath:@"@sum.assignedQuantity"];
    
    if(isUpdating)
    {
        
        SalesOrderAssignedLot *tempLot=[AssignedLotsArray objectAtIndex:selectedLotIndexpath.row];
       NSDecimalNumber * updatingLotAssignedQuantity=  tempLot.assignedQuantity;
        if([OrderLineItem.OrderProductQty isLessThanDecimal:([[quantityAlloted decimalNumberByAdding:Qty]decimalNumberBySubtracting:updatingLotAssignedQuantity])])
        {
            return NO;
        }
    }
    else
    {
        if([OrderLineItem.OrderProductQty  isLessThanDecimal:[quantityAlloted decimalNumberByAdding:Qty]])
        {
            return NO;
        }
    }
    
    
   
    
    return YES;
}
-(BOOL)checkForTotalLotsQty:(NSDecimalNumber *)Qty
{
    
    NSDecimalNumber *quantityAlloted = [AssignedLotsArray valueForKeyPath:@"@sum.assignedQuantity"];
    NSDecimalNumber *lotsQuantitySum= [productStockArray valueForKeyPath:@"@sum.lotQuantityNumber"];

    
    if(isUpdating)
    {
        
        SalesOrderAssignedLot *tempLot=[AssignedLotsArray objectAtIndex:selectedLotIndexpath.row];
        NSDecimalNumber * updatingLotAssignedQuantity=  tempLot.assignedQuantity;
        if([lotsQuantitySum isLessThanDecimal:[[quantityAlloted decimalNumberByAdding:Qty] decimalNumberBySubtracting:updatingLotAssignedQuantity]])
        {
            return NO;
        }
    }
    else
    {
        if([lotsQuantitySum  isLessThanDecimal:[quantityAlloted decimalNumberByAdding:Qty]])
        {
            return NO;
        }
    }

    
    return YES;
}
-(IBAction)saveButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    NSDecimalNumber *quantityAlloted = [AssignedLotsArray valueForKeyPath:@"@sum.assignedQuantity"];
//    NSDecimalNumber *lotsQuantitySum= [productStockArray valueForKeyPath:@"@sum.lotQuantityNumber"];
    
    if(([quantityAlloted isEqualToDecimal:OrderLineItem.OrderProductQty]) || [quantityAlloted floatValue]==0)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [_salesOrderLotAssigningViewControllerDelegate updateLotsAllcation:AssignedLotsArray isReAllocationForUpdatedQuantity:isReAllocationForUpdatedQuantity];
            [self dimissViewControllerWithSlideDownAnimation:YES];
        });
    }

    else if([quantityAlloted isLessThanDecimal:OrderLineItem.OrderProductQty])
    {
        [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Allocated Quantity less than Order Quantity" withController:nil];
    }
    else
    {
        [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Allocated Quantity exceeds Order Quantity" withController:nil];
    }
}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [_salesOrderLotAssigningViewControllerDelegate lotsAllocationCancelled];
        [self dimissViewControllerWithSlideDownAnimation:YES];
    });
}

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    [self.view endEditing:YES];
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    LotNumbersPopOverController=nil;
    LotNumbersPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    LotNumbersPopOverController.delegate=self;
    popOverVC.popOverController=LotNumbersPopOverController;
    popOverVC.titleKey=popOverString;
    [LotNumbersPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [LotNumbersPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma POPOVER DELEGATE METHODS
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath
{
    
}
-(void)didDismissPopOverController
{
    
}
#pragma UITextField Delegate Methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==LotNumberTextField) {
        popOverTitle=KLotsPopOverTitle;
        [self presentPopoverfor:LotNumberTextField withTitle:KLotsPopOverTitle withContent:[productStockArray valueForKey:@"lotNumberWithExpiryDate"]];
        return NO;
    }
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==QuantityTextField) {

    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField==QuantityTextField)
    {
        NSString *expression = ([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode]&&
                                ![OrderLineItem.OrderProduct.primaryUOM isEqualToString:OrderLineItem.OrderProduct.selectedUOM]&&
                                ([SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[OrderLineItem.OrderProduct copy]]<1))?KDecimalQuantityRegExpression:KWholeNumberQuantityRegExpression;

        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<12);
    }
     return YES;
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popOverTitle isEqualToString:KLotsPopOverTitle]) {
         Stock *stock=[productStockArray objectAtIndex:selectedIndexPath.row];
        
        NSPredicate * lotNumberPredicate=[NSPredicate predicateWithFormat:@"SELF.lot.Lot_No ==[cd] %@",stock.Lot_No];
        NSMutableArray* filteredPredicateArray=[[AssignedLotsArray filteredArrayUsingPredicate:lotNumberPredicate] mutableCopy];

        if(isUpdating && selectedAssignedLot.lot.Lot_No==stock.Lot_No)
        {
            /** user can update*/
            
        }
        else if(isUpdating && filteredPredicateArray.count>0)
        {
            [self showAlertAfterHidingKeyBoard:@"Update error" andMessage:@"Selected lot already added" withController:nil];
            
            return;
        }
        else if(isUpdating && filteredPredicateArray.count==0)
        {
            selectedAssignedLot.lot=stock;
            selectedAssignedLot.assignedQuantity=0;
            QuantityTextField.text=@"";

        }
        else if(!isUpdating)
        {
            lotQuantityLabel.text=[NSString stringWithFormat:@"%0.2f %@",[[[productStockArray objectAtIndex:selectedIndexPath.row]valueForKey:@"lotQuantityNumber"] doubleValue],OrderLineItem.OrderProduct.selectedUOM];
            
            selectedAssignedLot=[[SalesOrderAssignedLot alloc]init];
            selectedAssignedLot.lot=stock;
            selectedAssignedLot.assignedQuantity=0;
            QuantityTextField.text=@"";
        }
        LotNumberTextField.text=selectedAssignedLot.lot.Lot_No;
        lotQuantityLabel.text=[NSString stringWithFormat:@"%0.2f %@",[[[productStockArray objectAtIndex:selectedIndexPath.row]valueForKey:@"lotQuantityNumber"] doubleValue],OrderLineItem.OrderProduct.selectedUOM];
        [LotNumbersPopOverController dismissPopoverAnimated:YES];
        LotNumbersPopOverController=nil;
    }
}

-(IBAction)clearButtonTapped:(id)sender
{
    [LotNumberTextField setIsReadOnly:NO];
    selectedLotIndexpath=nil;
    LotNumberTextField.text=@"";
    lotQuantityLabel.text=KNotApplicable;
    QuantityTextField.text=@"";
    isUpdating=NO;
    [self updateAddButtonTitle];
    [lotSelectionTableView reloadData];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma UITABLEVIEW Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, lotsTableHeaderView.frame.size.width, lotsTableHeaderView.frame.size.height)];
    lotsTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:lotsTableHeaderView];
    return view;
}   // custom view for header. will be adjusted to default or specified header height



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return AssignedLotsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
         static NSString* identifier=@"LotsCell";
        SalesOrderLotAssigningTableViewCell *cell = (SalesOrderLotAssigningTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesOrderLotAssigningTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
            SalesOrderAssignedLot *assignLot=[AssignedLotsArray objectAtIndex:indexPath.row];
            cell.lotNumber.text=assignLot.lot.Lot_No;
            cell.LotQunatityLbl.text=[NSString stringWithFormat:@"%@",assignLot.lot.lotQuantityNumber ];
            cell.OrderQuantityLbl.text=[NSString stringWithFormat:@"%@",assignLot.assignedQuantity];
    
            cell.LotQunatityLbl.text=[NSString stringWithFormat:@"%0.2f",[assignLot.lot.lotQuantityNumber doubleValue]];
    
            cell.UOMLbl.text=OrderLineItem.OrderProduct.selectedUOM;
            cell.expiryDateLbl.text=[assignLot.lot.Expiry_Date isEqualToString:@""]?KNotApplicable:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:assignLot.lot.Expiry_Date];
            if([selectedLotIndexpath isEqual:indexPath])
            {
                /*seclecting the present selected cell*/
                cell.lotNumber.textColor=[UIColor whiteColor];
                cell.LotQunatityLbl.textColor=[UIColor whiteColor];
                cell.OrderQuantityLbl.textColor=[UIColor whiteColor];
                cell.expiryDateLbl.textColor=[UIColor whiteColor];
                cell.UOMLbl.textColor=[UIColor whiteColor];
                cell.contentView.backgroundColor=UITableviewSelectedCellBackgroundColor;
                
            }
            else
            {
                cell.lotNumber.textColor=MedRepMenuTitleFontColor;
                cell.LotQunatityLbl.textColor=MedRepMenuTitleFontColor;
                cell.OrderQuantityLbl.textColor=MedRepMenuTitleFontColor;
                cell.expiryDateLbl.textColor=MedRepMenuTitleFontColor;
                cell.UOMLbl.textColor=MedRepMenuTitleFontColor;
                cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
                
            }
            cell.rightButtons = @[[MGSwipeButton buttonWithTitle:NSLocalizedString(@"Delete", nil) backgroundColor:[UIColor redColor]]];
            cell.rightSwipeSettings.transition = MGSwipeTransition3D;
            cell.delegate=self;
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        isUpdating=YES;
        [self updateAddButtonTitle];
        [LotNumberTextField setIsReadOnly:YES];
        selectedAssignedLot=[[AssignedLotsArray objectAtIndex:indexPath.row] copy];
        lotQuantityLabel.text=[NSString stringWithFormat:@"%0.2f",selectedAssignedLot.lot.lotQuantityNumber.doubleValue];
        LotNumberTextField.text=selectedAssignedLot.lot.Lot_No;
        QuantityTextField.text=[NSString stringWithFormat:@"%0.2f",selectedAssignedLot.assignedQuantity.doubleValue];
        [self updateTheLotsTableViewSelectedCellIndexpath:indexPath];
}


-(void)updateAddButtonTitle
{
    if(isUpdating)
    {
        [addButton setTitle:KSalesOrderLotUpdateButtonTitle forState:UIControlStateNormal];
    }
    else
    {
        [addButton setTitle:KSalesOrderLotAddButtonTitle forState:UIControlStateNormal];
        
    }
}
-(void)updateTheLotsTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    
    /* deselecting the previous selected cell*/
    if(selectedLotIndexpath!=nil)
    {
        SalesOrderLotAssigningTableViewCell *previousSelectedCell=[lotSelectionTableView cellForRowAtIndexPath:selectedLotIndexpath];
        previousSelectedCell.lotNumber.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.LotQunatityLbl.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.OrderQuantityLbl.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.expiryDateLbl.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.UOMLbl.textColor=MedRepMenuTitleFontColor;
        previousSelectedCell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
    }
    
    /*seclecting the present selected cell*/
    SalesOrderLotAssigningTableViewCell *currentSelectedcell=[lotSelectionTableView cellForRowAtIndexPath:indexpath];
    currentSelectedcell.lotNumber.textColor=[UIColor whiteColor];
    currentSelectedcell.LotQunatityLbl.textColor=[UIColor whiteColor];
    currentSelectedcell.OrderQuantityLbl.textColor=[UIColor whiteColor];
    currentSelectedcell.expiryDateLbl.textColor=[UIColor whiteColor];
    currentSelectedcell.UOMLbl.textColor=[UIColor whiteColor];
    currentSelectedcell.contentView.backgroundColor=UITableviewSelectedCellBackgroundColor;
    selectedLotIndexpath=indexpath;
}


-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (NSInteger)index, fromExpansion ? @"YES" : @"NO");
    
    if (direction == MGSwipeDirectionRightToLeft) {
        
        if(index==0)
        {
            //delete button
            NSIndexPath * path = [lotSelectionTableView indexPathForCell:cell];
            [AssignedLotsArray removeObjectAtIndex:path.row];
            [UIView beginAnimations:@"myAnimationId" context:nil];
            
            [UIView setAnimationDuration:0.5]; // Set duration here
            [CATransaction begin];
            [CATransaction setCompletionBlock:^{
                NSLog(@"Complete!");
            }];
            
            [lotSelectionTableView beginUpdates];
            [lotSelectionTableView deleteRowsAtIndexPaths:[[NSArray alloc]initWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [lotSelectionTableView endUpdates];
            
            [CATransaction commit];
            [UIView commitAnimations];
            [self  clearButtonTapped:nil];
            return NO; //Don't autohide to improve delete expansion animation
        }
 
    }
    
    return YES;
}
-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}



-(BOOL)isAssignedLotItemWithValidDecimalAllotedQuantity:(NSDecimalNumber *)qty
{
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",OrderLineItem.OrderProduct.primaryUOM];
    NSPredicate *selecetdUOMPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",OrderLineItem.OrderProduct.selectedUOM];
    
    NSMutableArray *UOMarray=[OrderLineItem.OrderProduct.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    NSMutableArray *selecetdUOMFilterArray=[[UOMarray filteredArrayUsingPredicate:selecetdUOMPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    ProductUOM *selectedUOMObj=[selecetdUOMFilterArray objectAtIndex:0];
    
    NSDecimalNumber *OrderProductQtyInPrimaryUOM= [[qty decimalNumberByMultiplyingBy:[NSDecimalNumber ValidDecimalNumberWithString:selectedUOMObj.Conversion]]decimalNumberByDividingBy:[NSDecimalNumber ValidDecimalNumberWithString:primaryUOMObj.Conversion]];
    
    
    if([[NSString stringWithFormat:@"%0.2f",OrderProductQtyInPrimaryUOM.doubleValue] integerValue]==OrderProductQtyInPrimaryUOM.integerValue)
    {
        return YES;
    }
    else{
        return NO;
    }
    
    
    return YES;
}
@end
