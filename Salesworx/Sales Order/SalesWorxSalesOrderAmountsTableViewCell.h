//
//  SalesWorxSalesOrderAmountsTableViewCell.h
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 12/3/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTitleLabel2.h"
#import "SalesWorxDescriptionLabel2.h"
@interface SalesWorxSalesOrderAmountsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxTitleLabel2 *amountTitleStringLabel;
@property (strong, nonatomic) IBOutlet SalesWorxDescriptionLabel2 *AmountValueLabel;

@end
