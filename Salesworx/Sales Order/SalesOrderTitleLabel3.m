//
//  SalesOrderTitleLabel3.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderTitleLabel3.h"

@implementation SalesOrderTitleLabel3

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    self.font=KOpenSansSemiBold15;
    self.textColor=MedRepElementTitleLabelFontColor;
}

@end
