//
//  SalesOrderLotAssigningTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "SalesWorxSingleLineLabel.h"
@interface SalesOrderLotAssigningTableViewCell : MGSwipeTableCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *lotNumber;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *LotQunatityLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *expiryDateLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *OrderQuantityLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *UOMLbl;

@end
