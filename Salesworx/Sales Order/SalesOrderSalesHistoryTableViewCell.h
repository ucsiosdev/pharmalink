//
//  SalesOrderSalesHistoryTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesOrderSalesHistoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *peroidLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *BonusLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *QuantityLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *LastSoldLbl;
@end
