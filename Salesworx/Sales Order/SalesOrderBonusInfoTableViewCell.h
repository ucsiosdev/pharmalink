//
//  SalesOrderBonusInfoTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesOrderBonusInfoTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *fromNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ToNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *bonusLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *typeLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *bonusItemNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnSetQty;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xBtnSetQtyWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xBtnSetQtyTrailingConstraint;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xFromLabelLeadingConstraint;

@end
