//
//  SalesworxSalesOrderConfirmationViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/21/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//
#define KSaveAsProformaOptionStr @"Save as Proforma"
#define KConfirmOrderOptionStr @"Confirm Order"
#define KSaveAsTemplateOptionStr @"Save as Template"
#define KMaximumAllowedLPOImages 5
#define KLPOImagePlaceholderImageName @"SalesOrder_AddImage_CameraIcon"

#import "SalesworxSalesOrderConfirmationViewController.h"
#import <Photos/Photos.h>
#import "MBProgressHUD.h"
#import "SalesWorxSalesOrderAmountsTableViewCell.h"

@interface SalesworxSalesOrderConfirmationViewController ()

@end

@implementation SalesworxSalesOrderConfirmationViewController
@synthesize currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=UIViewBackGroundColor;
    isKeyBoardShowing=NO;
    appControl=[AppControl retrieveSingleton];
    confrimButtonPopOverOptions=[[NSMutableArray alloc]init ];
    [confrimButtonPopOverOptions addObject:KSaveAsProformaOptionStr];

    if([appControl.ENABLE_TEMPLATES isEqualToString:KAppControlsYESCode])
    {
        [confrimButtonPopOverOptions addObject:KSaveAsTemplateOptionStr];
    }
    [confrimButtonPopOverOptions addObject:KConfirmOrderOptionStr];

    LPOImgasesArray=[[NSMutableArray alloc]init];
    if([appControl.ENABLE_LPO_IMAGES isEqualToString:KAppControlsYESCode])
    {
        lPOImgesCollectionView.delegate=self;
        lPOImgesCollectionView.dataSource=self;
        [lPOImgesCollectionView registerClass:[SalesWorxSalesOrderLPOImagesCollectionViewCell class] forCellWithReuseIdentifier:@"LPOImageCell"];
     
       // xOrderAmountsTableViewBottomMarginConstraint.constant=92.0f;
    }
    else
    {
       // xOrderAmountsTableViewBottomMarginConstraint.constant=8.0f;
        [LPOImagesContentView setHidden:YES];
    }
    
    customerSignatureTopContainerView.delegate=self;
    
    OrderAmountsTableDataArray = [[NSMutableArray alloc] initWithObjects:@"OrderLineItems",@"TotalAmount",@"Discount",@"VATAmount",@"NetAmount",nil];
    
    if(![appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
        [OrderAmountsTableDataArray removeObject:@"VATAmount"];
    }
    
    xOrderAmountsTableViewHeightConstraint.constant = 28 * OrderAmountsTableDataArray.count;

}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    

    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    
    if(self.isMovingToParentViewController)
    {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesSalesOrderConfirmationScreenName];

    [self showCustomerDetails];
    orderLineItemsArray=currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray;
    [OrdersTableView reloadData];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
        
    
    if([appControl.ALLOW_SKIP_CONSOLIDATION isEqualToString:KAppControlsYESCode]&& [appControl.ENABLE_ORDER_CONSOLIDATION isEqualToString:KAppControlsYESCode])
    {
        //[skipConsolidationSwitch setEnabled:YES];
        skipConsolidationSegment.enabled=YES;
        
        if ([appControl.SKIP_CONSOLIDATION_MANDATORY isEqualToString:KAppControlsYESCode]) {
            
        }
        else{
            [skipConsolidationSegment setSelectedSegmentIndex:1];
        }
        
    }
    else
    {
        skipConsolidationSegment.enabled=NO;
        skipConsolidationSegment.tintColor=[UIColor lightGrayColor];
        

//        [skipConsolidationSwitch setEnabled:NO];
//        [skipConsolidationSwitch setTintColor:[UIColor lightGrayColor]];
//        skipConsolidationSwitch.thumbTintColor=[UIColor lightGrayColor];

    }
    if([appControl.ENABLE_WHOLESALE_ORDER isEqualToString:KAppControlsYESCode])
    {
        [wholeSaleOrderSwitch setEnabled:YES];

    }
    else
    {
        [wholeSaleOrderSwitch setEnabled:NO];
        [wholeSaleOrderSwitch setTintColor:[UIColor lightGrayColor]];
        wholeSaleOrderSwitch.thumbTintColor=[UIColor lightGrayColor];
    }

    confirmBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Confirm",nil) style:UIBarButtonItemStylePlain target:self action:@selector(confirmButtonTapped)];
    confirmBarButtonItem.tintColor=[UIColor whiteColor];
    [confirmBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                     forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=confirmBarButtonItem;

    

    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    
    NSMutableAttributedString* salesOrderattributeString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Sales Order", nil),[NSString stringWithFormat:@"[ Type: %@, Category: %@, W/H: %@ ]",[[currentVisit.visitOptions.salesOrder.selectedOrderType stringByReplacingOccurrencesOfString:@"Order" withString:@""] trimString],currentVisit.visitOptions.salesOrder.selectedCategory.Category,currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID]]];
    [salesOrderattributeString addAttribute:NSForegroundColorAttributeName
                                      value:[UIColor whiteColor]
                                      range:NSMakeRange(0, salesOrderattributeString.length)];
    
    [salesOrderattributeString addAttribute:NSFontAttributeName
                                      value:headerTitleFont
                                      range:NSMakeRange(0, salesOrderattributeString.length)];
    NSMutableAttributedString* orderDetailsAttributedString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"[ Type: %@, Category: %@, W/H: %@ ]",[currentVisit.visitOptions.salesOrder.selectedOrderType stringByReplacingOccurrencesOfString:@"Order" withString:@""],currentVisit.visitOptions.salesOrder.selectedCategory.Category,currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID]];
    [orderDetailsAttributedString addAttribute:NSForegroundColorAttributeName
                                         value:[UIColor whiteColor]
                                         range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    [orderDetailsAttributedString addAttribute:NSFontAttributeName
                                         value:kSWX_FONT_REGULAR(16)
                                         range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    
    //[salesOrderattributeString appendAttributedString:orderDetailsAttributedString];
    
    label.attributedText=salesOrderattributeString;
    self.navigationItem.titleView = label;

    
    
    if([appControl.ALLOW_SHIP_DATE_CHANGE isEqualToString:KAppControlsYESCode])
    {
        [shipDateTextField setIsReadOnly:NO];
    }
    else
    {
        [shipDateTextField setIsReadOnly:YES];
    }

        shipDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:[MedRepQueries fetchNextDateFromCurrentDateTimeinDatabaseFormat]];
        NSString * refinedDatetoDisplay=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[MedRepQueries fetchNextDateFromCurrentDateTimeinDatabaseFormat]];
        [shipDateTextField setText:refinedDatetoDisplay];
        [SWDefaults deleteLPOTemporaryImages];
        
        
        if([appControl.HIDE_DISCOUNT_FIELDS_AND_LABELS isEqualToString:KAppControlsYESCode]){
            /** orderTable*/
            xOrderTableHeaderDiscountLabelWidthConstraint.constant=0;
            xOrderTableHeaderDiscountLabelLeadingMarginConstraint.constant=0;
            
            /** Hiding VAT Columns*/
            if(![appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                xOrderTableHeaderNetAmountLabelLeadingMarginConstraint.constant=0;
                xOrderTableHeaderNetAmountLabelWidthConstraint.constant=0;
                xOrderTableHeaderVATChargeLabelLeadingMarginConstraint.constant=0;
                xOrderTableHeaderVATChargeLabelWidthConstraint.constant=0;
                OrderTableHeaderTotalLabel.textAlignment = NSTextAlignmentRight;
            }
        }else{
            /** Hiding VAT Columns*/
            if(![appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                xOrderTableHeaderVATChargeLabelLeadingMarginConstraint.constant=0;
                xOrderTableHeaderVATChargeLabelWidthConstraint.constant=0;
            }
        }
        
        selectedCustomerShipSitesDetailsArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_Name,Site_Use_ID,Customer_ID from TBL_Customer_Ship_Address where customer_id='%ld' AND Site_Use_ID IS NOT NULL AND Customer_Name IS NOT NULL",(long)currentVisit.visitOptions.customer.Ship_Customer_ID.integerValue]];
        
        /**Hiding ship to selection*/
        if(![appControl.ENABLE_SALESORDER_SHIPTO_SELECTION isEqualToString:KAppControlsYESCode]){
            xDocRefNoLabelTopMarginToSuperViewConstraint.constant=8;
            DeliverToTextField.hidden = YES;
            DeliverToStringTitleLabel.hidden = YES;
        }else{
            if(selectedCustomerShipSitesDetailsArray.count>0){
                 if(selectedCustomerShipSitesDetailsArray.count==1){
                     [DeliverToTextField setIsReadOnly:YES];
                 }
                /** Assigning the selected ship as delivery site*/
                selectedDeliverToSiteDetailsDic = [[[selectedCustomerShipSitesDetailsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Site_Use_ID=%ld",currentVisit.visitOptions.customer.Ship_Site_Use_ID.integerValue]] mutableCopy] objectAtIndex:0];
                DeliverToTextField.text = [selectedDeliverToSiteDetailsDic valueForKey:@"Customer_Name"];
            }
        }
        DeliverToStringTitleLabel.text=@"Delivery To";
    }
    
    
    
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
       ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
    NSSortDescriptor *PlandIdSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"AppliedAssortMentPlanID"
                                                 ascending:YES];
     NSSortDescriptor *showApliedAssortMentPlanBonusProductsSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"showApliedAssortMentPlanBonusProducts"
                                                 ascending:YES];
    NSArray *sortDescriptors = @[PlandIdSortDescriptor,showApliedAssortMentPlanBonusProductsSortDescriptor];
    NSArray *sortedArray = [orderLineItemsArray sortedArrayUsingDescriptors:sortDescriptors];
    orderLineItemsArray=[[NSMutableArray alloc]initWithArray:sortedArray];
    currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=orderLineItemsArray;
    }
}

-(void)viewDidAppear:(BOOL)animated
{

    if(self.isMovingToParentViewController)
    {
        if([appControl.ENABLE_LPO_IMAGES isEqualToString:KAppControlsYESCode]){
        if(!isLPOPhotoLibraryAccessCheckingDone){
            [self checkPhotoLibraryAccess];
        }
        }
        if([currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
        {
            [customerSignatureTopContainerView setStatus:KSignatureDisabled];
            [nameTextField setIsReadOnly:YES];
        }
    }
    
    [skipConsolidationSegment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:kSWX_FONT_REGULAR(15), NSFontAttributeName, nil] forState:UIControlStateNormal];


}

-(void)showCustomerDetails
{
    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    customerAvailableBalanceLabel.text=[currentVisit.visitOptions.customer.Avail_Bal currencyString];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerstatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerstatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)SalesWorxSignatureViewClearButtonTapped:(UIView *)signView
{
    [customerSignatureTopContainerView setStatus:KEmptySignature];
}

-(void)SalesWorxSignatureViewSaveButtonTapped:(UIView *)signView
{
    if(customerSignatureTopContainerView.status==KEmptySignature)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please sign to save" withController:self];
    }
    else
    {
        [self.view endEditing:YES];
        [customerSignatureTopContainerView performSignatureSaveAnimation];
    }
}
-(void)confirmButtonTapped
{
    [self.view endEditing:YES];
    /** FOC order for confirmed orders can only conform*/
    if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle] && currentVisit.visitOptions.salesOrder.focParentOrderNumber!=nil && ![[currentVisit.visitOptions.salesOrder.focParentOrderNumber trimString] isEqualToString:@""]){
        [self confirmOrderOptionTapped];
    }
    else{
        [self showConfirmOptions];
    }
}
-(void)showConfirmOptions
{
    NSString * popOverString=@"Confirm Options";
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=confrimButtonPopOverOptions;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    popOverVC.enableArabicTranslation=YES;
    popOverVC.titleKey=popOverString;
    popOverTitle=popOverString;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(225, 180);    
    popover.delegate = self;
    popover.barButtonItem = self.navigationItem.rightBarButtonItem;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{
        
    }];

}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if([popOverTitle isEqualToString:@"Ship-To Sites"]){
        selectedDeliverToSiteDetailsDic = [selectedCustomerShipSitesDetailsArray objectAtIndex:selectedIndexPath.row];
        DeliverToTextField.text=[selectedDeliverToSiteDetailsDic valueForKey:@"Customer_Name"];
    }else{
        NSLog(@"%@",[confrimButtonPopOverOptions objectAtIndex:selectedIndexPath.row]);
        [confirmationPagePopOverController dismissPopoverAnimated:YES];
        confirmationPagePopOverController=nil;
        
        
        if([[confrimButtonPopOverOptions objectAtIndex:selectedIndexPath.row]isEqualToString:KSaveAsProformaOptionStr]) /* save as ProformaOrder*/
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self saveProformaOrder];
        }
        else if ([[confrimButtonPopOverOptions objectAtIndex:selectedIndexPath.row]isEqualToString:KSaveAsTemplateOptionStr])/* save as Template*/
        {
            SalesOrderTemplateNameViewController *salesOrderTemplateNameViewController=[[SalesOrderTemplateNameViewController alloc]initWithNibName:@"SalesOrderTemplateNameViewController" bundle:[NSBundle mainBundle]];
            salesOrderTemplateNameViewController.view.backgroundColor = [UIColor clearColor];
            salesOrderTemplateNameViewController.modalPresentationStyle = UIModalPresentationCustom;
            salesOrderTemplateNameViewController.salesOrderTemplateNameViewControllerDelegate=self;
            
            if(currentVisit.visitOptions.salesOrder.isTemplateOrder){
                salesOrderTemplateNameViewController.templateName=currentVisit.visitOptions.salesOrder.selectedTemplete.Template_Name;
            }
            else{
                salesOrderTemplateNameViewController.templateName=@"";
            }
            
            [self.navigationController presentViewController:salesOrderTemplateNameViewController animated:NO completion:nil];
            
        }
        else if ([[confrimButtonPopOverOptions objectAtIndex:selectedIndexPath.row]isEqualToString:KConfirmOrderOptionStr])/* save as Order*/{
            [self confirmOrderOptionTapped];
        }
    }
}


-(void)confirmOrderOptionTapped
{
    
    
    
    if([appControl.IS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsNOCode] &&  nameTextField.text.length==0 && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please provide 'Signature' with 'Name'." withController:nil];
        
    }
    else if([appControl.IS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsNOCode] &&  customerSignatureTopContainerView.status==KEmptySignature && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please provide signature" withController:nil];
        
    }
    else if([appControl.IS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsNOCode] && customerSignatureTopContainerView.status==KSignatureNotSaved && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please save the signature to continue" withController:nil];
        
    }
    else if(customerSignatureTopContainerView.status==KSignatureSaved && nameTextField.text.length==0 && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please provide the signee name" withController:nil];
        
    }
    else if([appControl.ALLOW_SKIP_CONSOLIDATION isEqualToString:KAppControlsYESCode]&& [appControl.ENABLE_ORDER_CONSOLIDATION isEqualToString:KAppControlsYESCode] && skipConsolidationSegment.selectedSegmentIndex==UISegmentedControlNoSegment )
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:NSLocalizedString(@"Please select skip consolidation", nil) withController:nil];
    }

    else
    {
        confirmationDetails=[[SalesOrderConfirmationDetails alloc]init];
        confirmationDetails.signeeName=nameTextField.text;
        confirmationDetails.DocReferenceNumber=DocReferanceTexField.text;
        confirmationDetails.shipDate=shipDateTextField.text.length>0?shipDate:@"";
        confirmationDetails.comments=commentsTextView.text;

        if(!wholeSaleOrderSwitch.isEnabled)
        {
            confirmationDetails.isWholeSaleOrder=nil;
        }
        else if(wholeSaleOrderSwitch.isOn)
        {
            confirmationDetails.isWholeSaleOrder=KAppControlsYESCode;
        }
        else
        {
            confirmationDetails.isWholeSaleOrder=KAppControlsNOCode;
        }

//        if(!skipConsolidationSwitch.isEnabled)
//        {
//            confirmationDetails.SkipConsolidation=nil;
//        }
        //update segment details here
        
         if (skipConsolidationSegment.selectedSegmentIndex==0)
        {
            confirmationDetails.SkipConsolidation=KAppControlsYESCode;
 
        }
        else{
            confirmationDetails.SkipConsolidation=KAppControlsNOCode;
  
        }
//        else if(skipConsolidationSwitch.isOn)
//        {
//            confirmationDetails.SkipConsolidation=KAppControlsYESCode;
//        }
//        else
//        {
//            confirmationDetails.SkipConsolidation=KAppControlsNOCode;
//        }


        NSLog(@"skip consolidation is %ld", (long)skipConsolidationSegment.selectedSegmentIndex);
        
        
        
        if(customerSignatureTopContainerView.status==KSignatureSaved)
            confirmationDetails.isCustomerSigned=YES;
        else
            confirmationDetails.isCustomerSigned=NO;
        
        
        if(LPOImgasesArray.count>0)
        {
            confirmationDetails.isLPOImagesCaptured=YES;
        }
        
        currentVisit.visitOptions.salesOrder.confrimationDetails=confirmationDetails;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self saveConfirmedOrderDetails:currentVisit.visitOptions.salesOrder andCustomerDetails:currentVisit.visitOptions.customer andVisitId:currentVisit.Visit_ID andSignatureImage:[customerSignatureTopContainerView.signatureArea getSignatureImage]];
    }

}

//- (IBAction)confirmButtonTapped:(id)sender
//{
//    // include form dictionary in infodictionary
//    [self.view endEditing:YES];
//
//
//      //  [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:proformaOrderRef];
//
////
////        NSMutableDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[salesOrderDict objectForKey:@"info"]] ;
////        [infoOrder setValue:orderAdditionalInfoDict forKey:@"extraInfo"];
////        [infoOrder setValue:@"I" forKey:@"order_Status"];
////        
////        [salesOrderDict setValue:infoOrder forKey:@"info"];
////        
//    
//    
//        NSString *orderRef = [[SWDatabaseManager retrieveManager] saveOrderWithCustomerInfo:customerDict andOrderInfo:salesOrderDict andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
//        Singleton *single = [Singleton retrieveSingleton];
//        
//        
//        
//        NSString* attributeVal;
//        
//        
//        if (wholesaleOrder)
//        {
//            if (wholesaleOrder==YES) {
//                attributeVal=@"Y";
//            }
//            else
//            {
//                attributeVal=@"N";
//            }
//            
//            
//            [[SWDatabaseManager retrieveManager] saveDocAdditionalInfoWithDoc:orderRef andDocType:@"I" andAttName:@"WO" andAttValue:attributeVal andCust_Att_5:@""];
//        }
//        if (skipConsolidation)
//        {
//            
//            if (skipConsolidation==YES) {
//                attributeVal=@"Y";
//            }
//            
//            else
//            {
//                attributeVal=@"N";
//            }
//            
//            [[SWDatabaseManager retrieveManager] saveDocAdditionalInfoWithDoc:orderRef andDocType:@"I" andAttName:@"SC" andAttValue:attributeVal andCust_Att_5:@""];
//        }
//        
//        
//        
//        
//        
//        
//        if (image) {
//            [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",orderRef] ];
//            
//        }
//        
//        
//        
//        
//        if([single.visitParentView isEqualToString:@"MO"])
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Order Created Successfully", nil) message:[NSString stringWithFormat:@"%@: \n %@",NSLocalizedString(@"Reference No", nil),orderRef] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//            alert.tag = 30;
//            [alert show];
//        }
//        else
//        {
//            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Order Created Successfully", nil) message:[NSString stringWithFormat:@"%@: \n %@. \n",NSLocalizedString(@"Reference No", nil),orderRef] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            alert.tag = 40;
//            [alert show];
//        }
//        
//        
//    }
//    
//}
//
//- (void)saveDocAdditionalInfoWithDoc:(NSString *)docNumber andDocType:(NSString *)docType andAttName:(NSString *)attName andAttValue:(NSString *)attVale andCust_Att_5:(NSString *)cust_Att_5{
//    NSString *temp = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES ('{0}', '{1}', '{2}', '{3}','{4}','{5}')";
//    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:docNumber];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:docType];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:attName];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:attVale];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:cust_Att_5];
//    
//    
//    [self executeNonQuery:temp];
//}


- (FMDatabase *)getDatabase
{
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    
    //    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}


- (BOOL)saveConfirmedOrderDetails:(SalesOrder *)salesOrder andCustomerDetails:(Customer*)customerDetails andVisitId:(NSString *)Visit_ID andSignatureImage:(UIImage *)image{
    NSString *proformaOrderReference;
    if(salesOrder.isManagedOrder && salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref.length>0)
    {
        BOOL isProformaOrderDeleted=[[SWDatabaseManager retrieveManager]DeleteProformaOrderWithId:salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref];
        if(isProformaOrderDeleted )
        {
            proformaOrderReference=salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref;
        }
        else
        {
            [self showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to delete proforma order" withController:nil];
            return NO;
        }
        
    }
 
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    NSDictionary *userInfo=[SWDefaults userProfile];
    NSString *orderID = [NSString createGuid];
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    if(nUserId.length == 1)
    {nUserId = [NSString stringWithFormat:@"00%@",nUserId];}
    else if(nUserId.length == 2)
    {nUserId = [NSString stringWithFormat:@"0%@",nUserId];}
    
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
    
    if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
    {
        [SWDefaults setLastOrderReference:lastOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
        if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
        {
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
    }
    NSString *dateString =  [MedRepQueries fetchDatabaseDateFormat];
    NSString *dateTimeString =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString *startTime = salesOrder.OrderStartTime;
    //save the order based on app control flag
    NSString *InsertOrderQuery;
    
    
    if ([salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]) {/*previously saving the visit type in the custom attribute _4 . now saving the foc order number for noraml order OR parent order number for foc order - Source:harpal */

        
        InsertOrderQuery= @"INSERT INTO TBL_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number,Shipping_Instructions,Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    }
    else
    {
        InsertOrderQuery = @"INSERT INTO TBL_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number,Shipping_Instructions) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    }
    
    NSNumber* Totalamount = [salesOrder.SalesOrderLineItemsArray valueForKeyPath: @"@sum.OrderItemNetamount"];
    NSString *TotalamountStr = [[[NSString stringWithFormat:@"%@",Totalamount] floatString] AmountStringWithOutComas];

    NSMutableArray *insertOrderParameters=[[NSMutableArray alloc]initWithObjects:
                                    orderID,
                                    lastOrderReference,
                                    dateString,
                                    [userInfo stringForKey:@"SalesRep_ID"],
                                    customerDetails.Ship_Customer_ID ,
                                    customerDetails.Ship_Site_Use_ID ,
                                    customerDetails.Customer_ID ,
                                    customerDetails.Site_Use_ID ,
                                    salesOrder.confrimationDetails.comments,
                                    dateString,
                                    salesOrder.confrimationDetails.shipDate,
                                    @"N",
                                    startTime,
                                    dateTimeString,
                                    salesOrder.confrimationDetails.signeeName,
                                    salesOrder.confrimationDetails.isCustomerSigned==YES?@"[SIGN:Y]":@"[SIGN:N]",
                                    [userInfo stringForKey:@"Emp_Code"],
                                    TotalamountStr,
                                    Visit_ID,
                                    dateString,
                                    @"0",/**Selected Deliver To customer Details*/
                                    selectedDeliverToSiteDetailsDic!=nil?[NSNumber numberWithInteger:[[selectedDeliverToSiteDetailsDic valueForKey:@"Site_Use_ID"] integerValue]]:[NSNull null],
                                    dateString,
                                    [userInfo stringForKey:@"SalesRep_ID"],
                                    salesOrder.selectedCategory.Category,
                                    salesOrder.selectedCategory.Org_ID,
                                    @"0.0",
                                    salesOrder.confrimationDetails.DocReferenceNumber,
                                    [salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]?@"F":@"0",
                                      nil];
    
    if ([salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle] ) /** independet foc orders does not contains parent order numbers*/{
        [insertOrderParameters addObject:salesOrder.focParentOrderNumber==nil?@"":salesOrder.focParentOrderNumber];
    }
    
    BOOL isOrderDetailsSaved= [database executeUpdate:InsertOrderQuery withArgumentsInArray:insertOrderParameters];
    
    if(isOrderDetailsSaved)
    {
        NSLog(@"Saved success");
    }
       NSString *insertLPOQuery= @"INSERT INTO TBL_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number,Shipping_Instructions,Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    NSArray *orderArray=salesOrder.SalesOrderLineItemsArray;
    NSInteger lineNumber=0;
    NSString *orderLineItemId;
    BOOL orderLineItemsSaved=NO;
    for (NSInteger i = 0; i < orderArray.count; i++)
    {
        lineNumber=lineNumber+1;
        NSInteger orderLineNumber=lineNumber;
        SalesOrderLineItem *orderItem=[orderArray objectAtIndex:i];

        if(orderLineItemId==nil || orderLineItemId.length==0){
            orderLineItemId=[NSString createGuid];
        }
        else if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
                ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
            if(orderItem.AppliedAssortMentPlanID==NSNotFound){/*Asst bonus not applied*/
                orderLineItemId=[NSString createGuid];
            }
        }else{
            orderLineItemId=[NSString createGuid];
        }
        
        NSString *OrderLineItemQuery;
        if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                OrderLineItemQuery = @"INSERT INTO TBL_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID, Unit_Selling_Price, Def_Bonus,Calc_Price_Flag, Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID',VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code, Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            } else {
                OrderLineItemQuery = @"INSERT INTO TBL_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID, Unit_Selling_Price, Def_Bonus,Calc_Price_Flag, Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID',VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
        }else{
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                OrderLineItemQuery = @"INSERT INTO TBL_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID, Unit_Selling_Price, Def_Bonus,Calc_Price_Flag, Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID', Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            } else {
                OrderLineItemQuery = @"INSERT INTO TBL_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID, Unit_Selling_Price, Def_Bonus,Calc_Price_Flag, Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
        }
        
        
        
        NSMutableArray *insertOrderLineItemParameters=[[NSMutableArray alloc]initWithObjects:
                                               [NSString createGuid],
                                               lastOrderReference,
                                               [NSString stringWithFormat:@"%ld",(long)orderLineNumber],
                                               orderItem.OrderProduct.selectedUOM,
                                               orderItem.OrderProduct.selectedUOM,
                                               orderItem.OrderProductQty ,
                                               orderItem.OrderProduct.Inventory_Item_ID ,
                                               [[[[NSString stringWithFormat:@"%f",orderItem.productSelleingPrice] AmountStringWithOutComas] floatString] AmountStringWithOutComas] ,
                                               @"0",
                                               [salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]?@"F":@"N",
                                               [NSString stringWithFormat:@"%f",(orderItem.appliedDiscount/100)],
                                                orderLineItemId,
                                                orderItem.NotesStr==nil?@"":orderItem.NotesStr,
                                                [NSNull null],
                                               nil];
        if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.OrderItemVATChargeamount:[NSNull null]];
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCharge:[NSNull null]];
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATLevel:[NSNull null]];
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCode:[NSNull null]];
        }
        
        if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
            [insertOrderLineItemParameters addObject:orderItem.proximityNotes.length == 0 ? [NSNull null] : orderItem.proximityNotes];
        }
        
        BOOL orderLineItemSaved= [database executeUpdate:OrderLineItemQuery withArgumentsInArray:insertOrderLineItemParameters];
        if(!orderLineItemSaved)
        {
            orderLineItemsSaved=NO;
            break;
        }
        else
        {
            orderLineItemsSaved=YES;
        }

        if(orderLineItemSaved)
        {
            if(orderItem.requestedBonusQty>0)
            {
                lineNumber=lineNumber+1;

                NSMutableArray *insertOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                               [NSString createGuid],
                                                               lastOrderReference,
                                                               [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                               orderItem.bonusProduct.selectedUOM,
                                                               orderItem.bonusProduct.selectedUOM,
                                                              [NSString stringWithFormat:@"%0.0f",orderItem.requestedBonusQty] ,
                                                               orderItem.bonusProduct.Inventory_Item_ID ,
                                                               @"0" ,
                                                              [NSString stringWithFormat:@"%0.0f",orderItem.defaultBonusQty] ,
                                                               @"F",
                                                               @"0",
                                                               orderLineItemId,
                                                               @"",
                                                              [NSNull null],
                                                               nil];
                if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                }
                if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                }
                
                BOOL orderLineItemSaved= [database executeUpdate:OrderLineItemQuery withArgumentsInArray:insertOrderLineItemBonusParameters];
                if(!orderLineItemSaved)
                {
                    orderLineItemsSaved=NO;
                    break;
                }
                else
                {
                    orderLineItemsSaved=YES;
                }
                    

            }
            if(orderItem.manualFOCQty>0)
            {
                lineNumber=lineNumber+1;
                
                NSMutableArray *insertOrderLineItemManualFocParameters=[[NSMutableArray alloc]initWithObjects:
                                                                    [NSString createGuid],
                                                                    lastOrderReference,
                                                                    [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                    orderItem.manualFocProduct.selectedUOM,
                                                                    orderItem.manualFocProduct.selectedUOM,
                                                                    [NSString stringWithFormat:@"%0.0f",orderItem.manualFOCQty],
                                                                    orderItem.manualFocProduct.Inventory_Item_ID ,
                                                                    @"0" ,
                                                                    @"0",
                                                                    @"M",
                                                                    @"0",
                                                                    orderLineItemId,
                                                                    @"",
                                                                    [NSNull null],
                                                                    nil];
                if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                    [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                    [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                     [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                     [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                    }
                if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                    [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                }
                
                BOOL orderLineItemSaved= [database executeUpdate:OrderLineItemQuery withArgumentsInArray:insertOrderLineItemManualFocParameters];
                if(!orderLineItemSaved)
                {
                    orderLineItemsSaved=NO;
                    break;
                }
                else
                {
                    orderLineItemsSaved=YES;
                }
            }
            
            if(orderItem.AppliedAssortMentPlanID!=NSNotFound && orderItem.showApliedAssortMentPlanBonusProducts)
            {
                /** saving assortment bonus line Items*/
                
                NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)orderItem.AppliedAssortMentPlanID]];
                NSMutableArray *tempAssortMentBonusProductsArray=  [[currentVisit.visitOptions.salesOrder.AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
                
                for (NSInteger bns=0; bns<tempAssortMentBonusProductsArray.count; bns++) {
                    SalesworxAssortmentBonusProduct *tempAssortMentBonusProduct=[tempAssortMentBonusProductsArray objectAtIndex:bns];

                    lineNumber=lineNumber+1;
                    
                    NSMutableArray *insertOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                                        [NSString createGuid],
                                                                        lastOrderReference,
                                                                        [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                        tempAssortMentBonusProduct.product.primaryUOM,
                                                                        tempAssortMentBonusProduct.product.primaryUOM,
                                                                        [NSString stringWithFormat:@"%@",tempAssortMentBonusProduct.assignedQty] ,
                                                                        tempAssortMentBonusProduct.product.Inventory_Item_ID ,
                                                                        @"0" ,
                                                                        tempAssortMentBonusProduct.PlanDefaultBnsQty ,
                                                                        @"F",
                                                                        @"0",
                                                                        orderLineItemId,
                                                                        @"",
                                                                        [NSNumber numberWithInteger:orderItem.AppliedAssortMentPlanID],

                                                                        nil];
                    if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                        [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                        [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                        [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                        [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    }
                    if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                        [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    }
                    
                    BOOL orderLineItemSaved= [database executeUpdate:OrderLineItemQuery withArgumentsInArray:insertOrderLineItemBonusParameters];
                    if(!orderLineItemSaved)
                    {
                        orderLineItemsSaved=NO;
                        break;
                    }
                    else
                    {
                        orderLineItemsSaved=YES;
                    }
                    

                }
                orderLineItemId=nil;
            }
        }
        BOOL insertLotStatus=YES;
        for (NSInteger j = 0; j < orderItem.AssignedLotsArray.count; j++)
        {
            SalesOrderAssignedLot *assignLot=[orderItem.AssignedLotsArray objectAtIndex:j];
             NSString *InsertLotQuery = /*kSQLSaveOrderLotItem */@"INSERT INTO TBL_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES (?,?,?,?,?,?,?)";
            NSMutableArray *InsertLotItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                                    [NSString createGuid],
                                                                    lastOrderReference,
                                                                    [NSString stringWithFormat:@"%ld",(long)orderLineNumber],
                                                                    assignLot.lot.Lot_No,
                                                                    [NSString stringWithFormat:@"%@",assignLot.assignedQuantity],
                                                                    @"N" ,
                                                                    assignLot.lot.Org_ID,
                                                                    nil];
            insertLotStatus= [database executeUpdate:InsertLotQuery withArgumentsInArray:InsertLotItemParameters];
            if(!insertLotStatus)
            {
                orderLineItemsSaved=NO;
                break;
            }

        }
        if(orderItem.AppliedAssortMentPlanID==NSNotFound){
            orderLineItemId=nil;
        }
    }

    
    BOOL wholeSaleOrderDocInfoInsertStatus=YES;
    if(salesOrder.confrimationDetails.isWholeSaleOrder!=nil)
    {
        
       wholeSaleOrderDocInfoInsertStatus=  [self saveDocAdditionalInfoInDataBase:database WitDocNumber:lastOrderReference andDocType:@"I" andAttributeName:@"WO" andAttributeValue:salesOrder.confrimationDetails.isWholeSaleOrder andCustomer_Attribute_5:@""];
        
    }
    BOOL skipConsolidationSaleOrderDocInfoInsertStatus=YES;

    if(salesOrder.confrimationDetails.SkipConsolidation!=nil)
    {
        skipConsolidationSaleOrderDocInfoInsertStatus=[self saveDocAdditionalInfoInDataBase:database WitDocNumber:lastOrderReference andDocType:@"I" andAttributeName:@"SC" andAttributeValue:salesOrder.confrimationDetails.SkipConsolidation andCustomer_Attribute_5:@""];
        
    }
    
    BOOL LPOImagesSaleOrderDocInfoInsertStatus=YES;

    if(salesOrder.confrimationDetails.isLPOImagesCaptured && LPOImgasesArray.count>0)
    {
        NSString *lpoImagesNames=[[NSString alloc]init];
        for (NSInteger i=0; i<LPOImgasesArray.count; i++) {
            lpoImagesNames=[lpoImagesNames stringByAppendingString:[NSString stringWithFormat:@"%@_%ld",lastOrderReference,i+1]];
            if(i!=LPOImgasesArray.count-1)
               lpoImagesNames=[lpoImagesNames stringByAppendingString:@","];
        }
        
        LPOImagesSaleOrderDocInfoInsertStatus=[self saveDocAdditionalInfoInDataBase:database WitDocNumber:lastOrderReference andDocType:@"I" andAttributeName:@"LPO Images" andAttributeValue:lpoImagesNames andCustomer_Attribute_5:@""];

    }
    
    BOOL isOrderHistorySaved=  [self saveOrderHistory:salesOrder WithCustomerInfo:customerDetails andVisitId:Visit_ID InDatabase:database WithOrderRowID:orderID andOrderRef:lastOrderReference];

    if(!isOrderDetailsSaved ||!orderLineItemsSaved || !wholeSaleOrderDocInfoInsertStatus || !skipConsolidationSaleOrderDocInfoInsertStatus || !isOrderHistorySaved || !LPOImagesSaleOrderDocInfoInsertStatus)
    {
        [database rollback];
        [self showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to save order history" withController:nil];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        return NO;
    }
    else
    {
        
        
        
        if ([salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle] && salesOrder.focParentOrderNumber!=nil) {
            
            NSString *updateFOCOrderNumberForParentOrderQuery =@"UPDATE  TBL_Order SET  Custom_Attribute_4=? Where Orig_Sys_Document_Ref=?";
            
            NSMutableArray *updateFOCOrderNumberForParentOrderQueryParameters=[[NSMutableArray alloc]initWithObjects:
                                                                               lastOrderReference,
                                                    salesOrder.focParentOrderNumber,
                                                     nil];

            
          BOOL  isUpdatedOrderFOCNumber= [database executeUpdate:updateFOCOrderNumberForParentOrderQuery withArgumentsInArray:updateFOCOrderNumberForParentOrderQueryParameters];
            
            if(isUpdatedOrderFOCNumber)
            {
                NSLog(@"isUpdatedOrderFOCNumber -Success");
            }
        }
        if (salesOrder.confrimationDetails.isCustomerSigned) {
            [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",lastOrderReference] ];
        }
        if(LPOImgasesArray.count>0)
        {
            [SWDefaults copyFilesFromLPOImagesTempFolder:[SWDefaults getLPOImagesFolderPath] withPrefixName:[SWDefaults lastOrderReference]];
        }

        [SWDefaults updateGoogleAnalyticsEvent:kSalesOrderTakenEventName];
       // [SWDefaults UpdateGoogleAnalyticsforScreenName:kSalesOrderTakenEventName];
        

        [self popToVisitOptionsScreenAfterShowingAlertWithTitle:NSLocalizedString(@"Order Created Successfully", nil) andMessage:[NSString stringWithFormat:@"%@: \n %@. \n",NSLocalizedString(@"Reference No", nil),lastOrderReference]];
        [database commit];
        [database close];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        
        return YES;


    }
}
-(BOOL)saveDocAdditionalInfoInDataBase:(FMDatabase*)database WitDocNumber:(NSString *)DocNumber andDocType:(NSString *)DocType andAttributeName:(NSString *)AttributeName andAttributeValue:(NSString *)AttributeValue andCustomer_Attribute_5:(NSString *)cust_Att_5
{
    NSString *InsertDocAdditionalInfoQuery = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES (?,?,?,?,?,?)";
    
    
    NSMutableArray *InsertDocAdditionalInfoParameters=[[NSMutableArray alloc]initWithObjects:
                                                       [NSString createGuid],
                                                       DocNumber,
                                                       DocType,
                                                       AttributeName,
                                                       AttributeValue,
                                                       cust_Att_5 ,
                                                       nil];
    BOOL  insertDocInfoStatus= [database executeUpdate:InsertDocAdditionalInfoQuery withArgumentsInArray:InsertDocAdditionalInfoParameters];
    
    return insertDocInfoStatus;
  
}
- (BOOL)saveOrderHistory:(SalesOrder*)salesOrder WithCustomerInfo:(Customer*)customerDetails andVisitId:(NSString *)VisitId InDatabase:(FMDatabase*)database WithOrderRowID:(NSString *)OrderRowID andOrderRef:(NSString *)lastOrderReference{

    NSDictionary *userInfo=[SWDefaults userProfile];
    

    NSString * dummyTimeStr=@" 00:00:00";
    NSString *dateString =  [MedRepQueries fetchDatabaseDateFormat] ;
    dateString=[dateString stringByAppendingString:dummyTimeStr];
    NSString *dateTimeString =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString *startTime = salesOrder.OrderStartTime;
    
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] ;
    NSDateComponents *components=[gregCalendar components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];

    NSDate *lastDateofMonth= [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    NSString *lastDateMonthString =  [formatter stringFromDate:lastDateofMonth];
   NSArray* orderArray=salesOrder.SalesOrderLineItemsArray;
    

    
    NSString *OrderHistoryInsertQuery = @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp,Custom_Attribute_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    if([appControl.ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode])
    {
            /* Reorder requires  orgid and category to fetch products data*/
            OrderHistoryInsertQuery = @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp, Custom_Attribute_5, Custom_Attribute_2,Custom_Attribute_3,Custom_Attribute_6) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    }
    
    NSNumber* Totalamount = [salesOrder.SalesOrderLineItemsArray valueForKeyPath: @"@sum.OrderItemNetamount"];
    
    NSMutableArray *insertOrderParameters=[[NSMutableArray alloc]initWithObjects:
                                           OrderRowID,
                                           lastOrderReference,
                                           @"I",
                                           dateString,
                                           [userInfo stringForKey:@"SalesRep_ID"],
                                           dateString,
                                           dateString ,
                                           salesOrder.confrimationDetails.shipDate,
                                           customerDetails.Ship_Customer_ID ,
                                           customerDetails.Ship_Site_Use_ID,
                                           customerDetails.Customer_ID,
                                           customerDetails.Site_Use_ID ,
                                           @"N",
                                           startTime,
                                           dateTimeString,
                                           salesOrder.confrimationDetails.comments,
                                           [userInfo stringForKey:@"Emp_Code"],
                                           @"N",
                                           Totalamount,
                                           lastDateMonthString,
                                           @"0",/**Selected Deliver To customer Details*/
                                           selectedDeliverToSiteDetailsDic!=nil?[NSNumber numberWithInteger:[[selectedDeliverToSiteDetailsDic valueForKey:@"Site_Use_ID"] integerValue]]:[NSNull null],
                                           VisitId,
                                           dateString,
                                           salesOrder.confrimationDetails.DocReferenceNumber,
                                           nil];

    if([appControl.ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode])
    {
        [insertOrderParameters addObject:currentVisit.visitOptions.salesOrder.selectedCategory.Category];
        [insertOrderParameters addObject:currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID];
        [insertOrderParameters addObject:[salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]?@"F":@"0"];

    }
    
    BOOL isOrderHistoryDetailsSaved= [database executeUpdate:OrderHistoryInsertQuery withArgumentsInArray:insertOrderParameters];

    
    
    
    
    
    orderArray=salesOrder.SalesOrderLineItemsArray;
    NSInteger lineNumber=0;
    BOOL orderHistoryLineItemsSaved=NO;
    NSString *orderHistoryLineItemId;
    for (NSInteger i = 0; i < orderArray.count; i++)
    {
        lineNumber=lineNumber+1;
        NSInteger orderLineNumber=lineNumber;
        SalesOrderLineItem *orderItem=[orderArray objectAtIndex:i];

        if(orderHistoryLineItemId==nil || orderHistoryLineItemId.length==0){
            orderHistoryLineItemId=[NSString createGuid];
        }
        else if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
                ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
            if(orderItem.AppliedAssortMentPlanID==NSNotFound){/*Asst bonus not applied*/
                orderHistoryLineItemId=[NSString createGuid];
            }
        }else{
            orderHistoryLineItemId=[NSString createGuid];
        }
        
        
        NSString *  OrderHistoryLineItemQuery;
        if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){

            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                OrderHistoryLineItemQuery = @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3,Assortment_Plan_ID,VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code, Custom_Attribute_6) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            } else {
                OrderHistoryLineItemQuery = @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3,Assortment_Plan_ID,VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
           
        }else{
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                OrderHistoryLineItemQuery = @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3,Assortment_Plan_ID, Custom_Attribute_6) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            } else {
                OrderHistoryLineItemQuery = @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3,Assortment_Plan_ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
        }

        
        NSMutableArray *insertOrderLineItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                       [NSString createGuid],
                                                       lastOrderReference,
                                                       [NSString stringWithFormat:@"%ld",(long)orderLineNumber],
                                                       [NSString stringWithFormat:@"%@",orderItem.OrderProduct.Inventory_Item_ID] ,
                                                       orderItem.OrderProduct.selectedUOM,
                                                       orderItem.OrderProductQty,
                                                       [salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]?@"F":@"N",
                                                       [[[[NSString stringWithFormat:@"%f",orderItem.productSelleingPrice] AmountStringWithOutComas] floatString] AmountStringWithOutComas] ,
                                                       [[[NSString stringWithFormat:@"%@",orderItem.OrderItemNetamount] floatString]AmountStringWithOutComas] ,
                                                       @"",
                                                       @"",
                                                       orderHistoryLineItemId,
                                                       orderItem.NotesStr==nil?@"":orderItem.NotesStr,
                                                       [NSString stringWithFormat:@"%f",(orderItem.appliedDiscount/100)],
                                                       [NSNull null],
                                                       nil];

        if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.OrderItemVATChargeamount:[NSNull null]];
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCharge:[NSNull null]];
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATLevel:[NSNull null]];
            [insertOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCode:[NSNull null]];
        }
        if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
            [insertOrderLineItemParameters addObject:orderItem.proximityNotes.length == 0 ? [NSNull null] : orderItem.proximityNotes];
        }
        
        NSLog(@"insert order line items %@", insertOrderLineItemParameters);
        
        
        BOOL orderHistoryLineItemSaved= [database executeUpdate:OrderHistoryLineItemQuery withArgumentsInArray:insertOrderLineItemParameters];
        
        if(!orderHistoryLineItemSaved)
        {
            orderHistoryLineItemsSaved=NO;
            break;
        }
        else
        {
            orderHistoryLineItemsSaved=YES;
        }
        
        if(orderItem.requestedBonusQty>0)
        {
            lineNumber=lineNumber+1;
            
            NSMutableArray *insertOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                           [NSString createGuid],
                                                           lastOrderReference,
                                                           [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                           [NSString stringWithFormat:@"%@",orderItem.bonusProduct.Inventory_Item_ID] ,
                                                           orderItem.bonusProduct.selectedUOM,
                                                           [NSString stringWithFormat:@"%0.0f",orderItem.requestedBonusQty],
                                                           @"F",
                                                           @"0" ,
                                                           @"0",
                                                           @"",
                                                           @"",
                                                            orderHistoryLineItemId,
                                                            @"",
                                                            @"0",
                                                            [NSNull null],
                                                           nil];

            if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                [insertOrderLineItemBonusParameters addObject:[NSNull null]];
            }
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                [insertOrderLineItemBonusParameters addObject:[NSNull null]];
            }
            
             orderHistoryLineItemSaved= [database executeUpdate:OrderHistoryLineItemQuery withArgumentsInArray:insertOrderLineItemBonusParameters];
            if(!orderHistoryLineItemSaved)
            {
                orderHistoryLineItemsSaved=NO;
                break;
            }

        }
        if(orderItem.manualFOCQty>0)
        {
            lineNumber=lineNumber+1;
            
            NSMutableArray *insertOrderLineItemManualFocParameters=[[NSMutableArray alloc]initWithObjects:
                                                                [NSString createGuid],
                                                                lastOrderReference,
                                                                [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                [NSString stringWithFormat:@"%@",orderItem.manualFocProduct.Inventory_Item_ID] ,
                                                                orderItem.manualFocProduct.selectedUOM,
                                                                [NSString stringWithFormat:@"%0.0f",orderItem.manualFOCQty],
                                                                @"M",
                                                                @"0" ,
                                                                @"0",
                                                                @"",
                                                                @"",
                                                                orderHistoryLineItemId,
                                                                @"",
                                                                @"0",
                                                                [NSNull null],
                                                                nil];
            if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
            }
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
            }
            
             orderHistoryLineItemSaved= [database executeUpdate:OrderHistoryLineItemQuery withArgumentsInArray:insertOrderLineItemManualFocParameters];
            if(!orderHistoryLineItemSaved)
            {
                orderHistoryLineItemsSaved=NO;
                break;
            }
            else
            {
                orderHistoryLineItemsSaved=YES;
            }
        }
        if(orderItem.AppliedAssortMentPlanID!=NSNotFound && orderItem.showApliedAssortMentPlanBonusProducts)
        {
            /** saving assortment bonus line Items*/
            
            NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)orderItem.AppliedAssortMentPlanID]];
            NSMutableArray *tempAssortMentBonusProductsArray=  [[currentVisit.visitOptions.salesOrder.AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
            
            for (NSInteger bns=0; bns<tempAssortMentBonusProductsArray.count; bns++) {
                SalesworxAssortmentBonusProduct *tempAssortMentBonusProduct=[tempAssortMentBonusProductsArray objectAtIndex:bns];
                lineNumber=lineNumber+1;
                
                NSMutableArray *insertOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                                    [NSString createGuid],
                                                                    lastOrderReference,
                                                                    [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                    [NSString stringWithFormat:@"%@",tempAssortMentBonusProduct.product.Inventory_Item_ID] ,
                                                                    tempAssortMentBonusProduct.product.primaryUOM,
                                                                    tempAssortMentBonusProduct.assignedQty,
                                                                    @"F",
                                                                    @"0" ,
                                                                    @"0",
                                                                    @"",
                                                                    @"",
                                                                    orderHistoryLineItemId,
                                                                    @"",
                                                                    @"0",
                                                                    [NSNumber numberWithInteger:orderItem.AppliedAssortMentPlanID],
                                                                    nil];
                if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                }
                if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                    [insertOrderLineItemBonusParameters addObject:[NSNull null]];
                }
                
                orderHistoryLineItemSaved= [database executeUpdate:OrderHistoryLineItemQuery withArgumentsInArray:insertOrderLineItemBonusParameters];
                if(!orderHistoryLineItemSaved)
                {
                    orderHistoryLineItemsSaved=NO;
                    break;
                }
            }
            orderHistoryLineItemId=nil;
        }
        if(orderItem.AppliedAssortMentPlanID==NSNotFound){
            orderHistoryLineItemId=nil;
        }
    
    }
    
    if(orderHistoryLineItemsSaved && isOrderHistoryDetailsSaved)
        return YES;
    
     return NO;
}

#pragma mark saving signature methods

- (NSString*) dbGetImagesDocumentPath
{
    
    //iOS 8 support
    NSString *path;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}


#pragma  mark SalesOrderTemplateNameViewControllerDelegateMethods
-(void)saveTemplateName:(NSString *)templateName
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(!currentVisit.visitOptions.salesOrder.isTemplateOrder && currentVisit.visitOptions.salesOrder.selectedTemplete.Order_Template_ID!=nil)
    {
        [self saveTemplateWithName:templateName andTemplateId:[NSString createGuid] andOrderInfo:currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray];

    }
    else  if(currentVisit.visitOptions.salesOrder.isTemplateOrder && [currentVisit.visitOptions.salesOrder.selectedTemplete.Template_Name isEqualToString:templateName])
    {
        [[SWDatabaseManager retrieveManager] deleteTemplateOrderWithRef:currentVisit.visitOptions.salesOrder.selectedTemplete.Order_Template_ID];
        [self saveTemplateWithName:templateName andTemplateId:currentVisit.visitOptions.salesOrder.selectedTemplete.Order_Template_ID andOrderInfo:currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray];

    }
    else
    {
        [self saveTemplateWithName:templateName andTemplateId:[NSString createGuid] andOrderInfo:currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray];

    }
}


- (void)saveTemplateWithName:(NSString *)name andTemplateId:(NSString*)templateId andOrderInfo:(NSMutableArray *)orderInfo {
    
    FMDatabase *database = [self getDatabase];
    [database open];
    
    NSString *insertTemplateOrder= [[ NSString alloc]initWithFormat: @"INSERT INTO TBL_Order_Template (Order_Template_ID, Template_Name, SalesRep_ID, Custom_Attribute_1, Custom_Attribute_2)  VALUES (?,?,?,?,?)"];
    [database beginTransaction];
    
        NSArray *TemplteDetailsArray=[[NSArray alloc]initWithObjects:templateId,
                                                        name,
                                                    [[SWDefaults userProfile] stringForKey:@"SalesRep_ID"],                                     currentVisit.visitOptions.salesOrder.selectedCategory.Category,currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID,nil];
   BOOL isTemplteDetailsSaved= [database executeUpdate:insertTemplateOrder withArgumentsInArray:TemplteDetailsArray];
    
    NSLog(@"TemplteDetailsArray %@ ",TemplteDetailsArray);
    BOOL isTemplteOrderDetailsSaved=NO;

    if(isTemplteDetailsSaved)
    {
        NSString *insertTemplateOrderLineItems= [[ NSString alloc]initWithFormat: @"INSERT INTO TBL_Order_Template_Items (Template_Line_ID, Order_Template_ID, Inventory_Item_ID, Organization_ID, Quantity,Custom_Attribute_1) VALUES (?,?,?,?,?,?)"];
        
        for (SalesOrderLineItem *lineItem in orderInfo) {
            NSString *itemID = [NSString createGuid];
            
            NSArray *templateLineItemArray=[[NSArray alloc]initWithObjects:itemID,
                            templateId,
                            lineItem.OrderProduct.Inventory_Item_ID,
                            lineItem.OrderProduct.OrgID,
                            lineItem.OrderProductQty,
                             lineItem.OrderProduct.selectedUOM,nil];
            isTemplteOrderDetailsSaved=[database executeUpdate:insertTemplateOrderLineItems withArgumentsInArray:templateLineItemArray];
            NSLog(@"TemplteDetailsArray %@ ",templateLineItemArray);

            if(!isTemplteOrderDetailsSaved)
                break;
        }
        
        if(!isTemplteOrderDetailsSaved)
        {
            [database rollback];
        }
 
    }
    else
    {
        [database rollback];

    }
    
    
    if(!isTemplteDetailsSaved
       || !isTemplteOrderDetailsSaved)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to save template" withController:nil];
        });
    }
    else
    {
        
        [database commit];
        [database close];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self showAlertAfterHidingKeyBoard:@"Success" andMessage:@"Template saved successfully" withController:nil];
        });
        
       // [self popToVisitOptionsScreenAfterShowingAlertWithTitle:@"Success" andMessage:@"Template Saved"];
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    
}
-(void)popToVisitOptionsScreenAfterShowingAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    NSDictionary *visitOptionDict;
    
    if(currentVisit.visitOptions.salesOrder.isManagedOrder)
    {
        visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kManageOrdersTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
    }
    else
    {
       visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kSalesOrderTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                        object:self
                                                      userInfo:visitOptionDict];

    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(title,nil)
                                      message:NSLocalizedString(message,nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 for (NSInteger i=0; i<self.navigationController.viewControllers.count; i++) {
                                     if( [[self.navigationController.viewControllers objectAtIndex:i] isKindOfClass:[SalesWorxVisitOptionsViewController class]])
                                     {
                                         [self.navigationController popToViewController:(SalesWorxVisitOptionsViewController*)[self.navigationController.viewControllers objectAtIndex:i] animated:YES];
                                         break;
                                     }
                                 }
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:^{
            [self saveLpoPhotosToAlbum];
        }];
    }];
}



-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath
{
    
}
-(void)didDismissPopOverController
{
    
}


#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==OrdersTableView) {
        return 44.0f;
    }else if (tableView==OrderAmountsTableView){
        return 28.0f;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==OrdersTableView) {
        if(section==0){
            return 0;
        }
        else{
            SalesOrderLineItem *orderLineItem=[orderLineItemsArray objectAtIndex:section];
            SalesOrderLineItem *previousOrderLineItem=[orderLineItemsArray objectAtIndex:section-1];
            
            if(orderLineItem.showApliedAssortMentPlanBonusProducts){
                return 0;
            }else if(previousOrderLineItem.AppliedAssortMentPlanID==NSNotFound || orderLineItem.AppliedAssortMentPlanID!=previousOrderLineItem.AppliedAssortMentPlanID){
                return 5.0f;
            }
        }
        return 0;
    }

    
    return 0.01f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==OrdersTableView) {
        
        if(section==0){
            return nil;
        }
        else{
            SalesOrderLineItem *orderLineItem=[orderLineItemsArray objectAtIndex:section];
            SalesOrderLineItem *previousOrderLineItem=[orderLineItemsArray objectAtIndex:section-1];
            
            if(orderLineItem.showApliedAssortMentPlanBonusProducts){
                return nil;
            }else if(previousOrderLineItem.AppliedAssortMentPlanID==NSNotFound || orderLineItem.AppliedAssortMentPlanID!=previousOrderLineItem.AppliedAssortMentPlanID){
                UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 5.0)];
                view.backgroundColor=UIViewBackGroundColor;
                return view;
            }
        }
        return nil;
    }
    else
    {
        return [[UIView alloc]initWithFrame:CGRectZero];
    }
    
}   // custom view for header. will be adjusted to default or specified header height




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==OrdersTableView) {
        return orderLineItemsArray.count;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (tableView==OrdersTableView)
    {
        SalesOrderLineItem *orderLineItem=[orderLineItemsArray objectAtIndex:section];
        NSInteger numberOfrows=0;
        if(orderLineItem.OrderProductQty>0)
            numberOfrows++;
        if(orderLineItem.requestedBonusQty>0)
            numberOfrows++;
        if(orderLineItem.manualFOCQty>0)
            numberOfrows++;
        if(orderLineItem.AppliedAssortMentPlanID!=NSNotFound && orderLineItem.showApliedAssortMentPlanBonusProducts)
        {
            
            NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)orderLineItem.AppliedAssortMentPlanID]];
            NSMutableArray *tempAssortMentBonusProductsArray=  [[currentVisit.visitOptions.salesOrder.AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
            numberOfrows=numberOfrows+tempAssortMentBonusProductsArray.count;
        }
        
        return numberOfrows;
    }else if(tableView == OrderAmountsTableView){
        return OrderAmountsTableDataArray.count;
    }
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==OrdersTableView){
        static NSString* identifier=@"SalesOrderItemCell";
        SalesWorxSalesOrdersTableCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesOrdersTableCellTableViewCell" owner:nil options:nil] firstObject];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        SalesOrderLineItem *currentLineItem=[orderLineItemsArray objectAtIndex:indexPath.section];
        cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
        cell.btnProximityIndicator.hidden = YES;
        
        if(indexPath.row==0)
        {
            cell.UnitPriceLbl.text=[[NSString
                                     stringWithFormat:@"%f",currentLineItem.productSelleingPrice] floatString];
            cell.ProductNameLbl.text=currentLineItem.OrderProduct.Description;
            cell.NetAmountLbl.text=[[NSString
                                     stringWithFormat:@"%@",currentLineItem.OrderItemNetamount] floatString];
            cell.ToatlLbl.text=[[NSString
                                 stringWithFormat:@"%@",currentLineItem.OrderItemTotalamount] floatString];
            cell.DiscountLbl.text=[[NSString
                                    stringWithFormat:@"%@",currentLineItem.OrderItemDiscountamount] floatString];
            NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.OrderProduct.selectedUOM];
            cell.QunatutyLbl.text=[NSString stringWithFormat:@"%@ %@",currentLineItem.OrderProductQty,uomStr];
            
            cell.VATChargesLbl.text=currentLineItem.VATChargeObj!=nil?[[NSString
                                                                        stringWithFormat:@"%.2f",currentLineItem.VATChargeObj.VATCharge.floatValue]stringByAppendingString:@"%"]:@"0.00%" ;
            
            [cell.statusView setHidden:YES];
            cell.ProductNameLbl.textColor=MedRepMenuTitleFontColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleFontColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleFontColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleFontColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleFontColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleFontColor;
            cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
            
        }
        else if(indexPath.row==1 && (currentLineItem.requestedBonusQty>0 || currentLineItem.manualFOCQty>0))
        {
            [cell.statusView setHidden:NO];
            
            if(currentLineItem.manualFOCQty>0)
            {
                NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.manualFocProduct.selectedUOM];
                
                cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.manualFOCQty,uomStr];
                cell.ProductNameLbl.text=currentLineItem.manualFocProduct.Description;
                
            }
            else
            {
                NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.bonusProduct.selectedUOM];
                
                cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.requestedBonusQty,uomStr];
                cell.ProductNameLbl.text=currentLineItem.bonusProduct.Description;
            }
            
            cell.UnitPriceLbl.text=[@"0.0" floatString];
            cell.NetAmountLbl.text=[@"0.0" floatString];
            cell.ToatlLbl.text=[@"0.0" floatString];
            cell.DiscountLbl.text=[@"0.0" floatString];
            cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
            cell.VATChargesLbl.text=@"0.00%" ;
            cell.ProductNameLbl.textColor=MedRepMenuTitleFontColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleFontColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleFontColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleFontColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleFontColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleFontColor;
            cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
            
        }
        else if(indexPath.row==2 && (currentLineItem.requestedBonusQty>0 && currentLineItem.manualFOCQty>0))
        {
            [cell.statusView setHidden:NO];
            
            NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.bonusProduct.selectedUOM];
            cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.requestedBonusQty,uomStr];            cell.UnitPriceLbl.text=[@"0.0" floatString];
            cell.ProductNameLbl.text=currentLineItem.bonusProduct.Description;
            cell.NetAmountLbl.text=[@"0.0" floatString];
            cell.ToatlLbl.text=[@"0.0" floatString];
            cell.DiscountLbl.text=[@"0.0" floatString];
            cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
            cell.VATChargesLbl.text=@"0.00%" ;

            cell.ProductNameLbl.textColor=MedRepMenuTitleFontColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleFontColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleFontColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleFontColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleFontColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleFontColor;
            cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
            
        }
        else
        {
            NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)currentLineItem.AppliedAssortMentPlanID]];
            NSMutableArray *tempAssortMentBonusProductsArray=  [[currentVisit.visitOptions.salesOrder.AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
            
            NSInteger bonusProductIndex=indexPath.row-(currentLineItem.requestedBonusQty>0?1:0)-(currentLineItem.manualFOCQty>0?1:0)-1/*main product*/;
            SalesworxAssortmentBonusProduct *BnsPro= [tempAssortMentBonusProductsArray objectAtIndex:bonusProductIndex];
            
            
            [cell.statusView setHidden:NO];
            
            NSString *uomStr=[NSString stringWithFormat:@"%@",BnsPro.product.primaryUOM];
            cell.QunatutyLbl.text=[NSString stringWithFormat:@"%@ %@",BnsPro.assignedQty,uomStr];
            cell.UnitPriceLbl.text=[@"0.0" floatString];
            cell.ProductNameLbl.text=BnsPro.product.Description;
            cell.NetAmountLbl.text=[@"0.0" floatString];
            cell.ToatlLbl.text=[@"0.0" floatString];
            cell.DiscountLbl.text=[@"0.0" floatString];
            cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
            cell.VATChargesLbl.text=@"0.00%" ;

            cell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
            cell.rightButtons=[[NSArray alloc]init];
            cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
            cell.CellItemType=AssortMentBonusTypeCell;
        }
        return cell;
    }else{
        static NSString* identifier=@"salesOrderAmountCellIdentifier";
        SalesWorxSalesOrderAmountsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesOrderAmountsTableViewCell" owner:nil options:nil] firstObject];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        if([[OrderAmountsTableDataArray objectAtIndex:indexPath.row] isEqualToString:@"NetAmount"]){
            NSNumber* Netamount = [orderLineItemsArray valueForKeyPath: @"@sum.OrderItemNetamount"];
            cell.AmountValueLabel.text=[[NSString stringWithFormat:@"%@",Netamount] floatString];
            cell.amountTitleStringLabel.text=[NSString stringWithFormat:@"%@(%@):",NSLocalizedString(@"Net Amount", nil),[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
        }else if([[OrderAmountsTableDataArray objectAtIndex:indexPath.row] isEqualToString:@"TotalAmount"]){
            NSNumber* Totalamount = [orderLineItemsArray valueForKeyPath: @"@sum.OrderItemTotalamount"];
            cell.AmountValueLabel.text=[[NSString stringWithFormat:@"%@",Totalamount] floatString];
            cell.amountTitleStringLabel.text=[NSString stringWithFormat:@"%@(%@):",NSLocalizedString(@"Total", nil),[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
        }else if([[OrderAmountsTableDataArray objectAtIndex:indexPath.row] isEqualToString:@"Discount"]){
            NSNumber* Dicountamount = [orderLineItemsArray valueForKeyPath: @"@sum.OrderItemDiscountamount"];
            cell.AmountValueLabel.text=[[NSString stringWithFormat:@"%@",Dicountamount] floatString];
            cell.amountTitleStringLabel.text=[NSString stringWithFormat:@"%@(%@):",NSLocalizedString(@"Discount", nil),[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
        }else if([[OrderAmountsTableDataArray objectAtIndex:indexPath.row] isEqualToString:@"OrderLineItems"]){
            cell.AmountValueLabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray.count];
            cell.amountTitleStringLabel.text=@"Order Line Item(s):";
        }else if([[OrderAmountsTableDataArray objectAtIndex:indexPath.row] isEqualToString:@"VATAmount"]){
            NSNumber* VATamount = [orderLineItemsArray valueForKeyPath: @"@sum.OrderItemVATChargeamount"];
            cell.AmountValueLabel.text=[[NSString stringWithFormat:@"%@",VATamount] floatString];
            cell.amountTitleStringLabel.text=[NSString stringWithFormat:@"%@(%@):",NSLocalizedString(@"VAT Amount", nil),[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
        }
        
        return cell;
        
    }

        
    
}
#pragma mark UITextField Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self animateTextField:textField up:YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *trimmedString = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    textField.text=trimmedString;
    [self animateTextField:textField up:NO];
}
- (void) animateTextField:(UITextField*)textField up: (BOOL) up
{
    NSInteger movementDistance = 50;
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==shipDateTextField) {
        [self showDatePicker];
        return NO;
    }else if (textField==DeliverToTextField) {
        popOverTitle=@"Ship-To Sites";
        [self presentPopoverfor:DeliverToTextField withTitle:@"Ship-To Sites" withContent:[selectedCustomerShipSitesDetailsArray valueForKey:@"Customer_Name"]];
        return NO;
    }
    return YES;
}
-(void)showDatePicker
{
    popOverTitle=@"Ship Date";
    SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
    popOverVC.didSelectDateDelegate=self;
    popOverVC.titleString = popOverTitle;
    popOverVC.setMaximumDateCurrentDate=NO;
    popOverVC.salesWorxDatePicker.minimumDate=[NSDate date];
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    confirmationPagePopOverController=nil;
    confirmationPagePopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    confirmationPagePopOverController.delegate=self;
    popOverVC.datePickerPopOverController=confirmationPagePopOverController;
    
    [confirmationPagePopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];

    
    if(isKeyBoardShowing)
    {
        [UIView animateWithDuration:0.2 animations: ^{
            [self.view endEditing:YES];
            
        } completion:^(BOOL finished) {
        }];
 
    }
    [confirmationPagePopOverController presentPopoverFromRect:shipDateTextField.dropdownImageView.frame inView:shipDateTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}
-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(void)didSelectDate:(NSString *)selectedDate
{
    //refine date
    shipDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
    NSString * refinedDatetoDisplay=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:kDateFormatWithoutTime scrString:selectedDate];
    [shipDateTextField setText:refinedDatetoDisplay];
    
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }

    if(textField==DocReferanceTexField)
    {
        NSString *expression = @"^[0-9a-z ]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KSalesOrderScreenDocReferenceTextFieldMaximumDigitsLimit);
    }
    else if(textField==nameTextField)
    {
//        NSString *expression = @"^[0-9a-z ]*$";
//        NSError *error = nil;
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
//        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (newString.length<=KSalesOrderScreenSigneeNameTextFieldMaximumDigitsLimit);
    }
    

    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==DocReferanceTexField)
    {
        [shipDateTextField becomeFirstResponder];
    }
    if(textField==shipDateTextField)
    {
        [nameTextField becomeFirstResponder];
    }
    if(textField==nameTextField)
    {
        [commentsTextView becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return NO;
}

#pragma mark UITextView methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textView.text.length==0 && [text isEqualToString:@" "])
    {
        return NO;
    }
    if(textView==commentsTextView)
    {
        return ( newString.length<=KSalesOrderScreenCommentsTextViewMaximumDigitsLimit);
    }
    return YES;
    
}

- (void)saveProformaOrder{

    
    
    NSDictionary *userInfo=[SWDefaults userProfile];
    NSString *orderID = [NSString createGuid];
    
    NSString *proformaOrderReference;
    if(currentVisit.visitOptions.salesOrder.isManagedOrder && currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref.length>0)
    {
        BOOL isProformaOrderDeleted=[[SWDatabaseManager retrieveManager]DeleteProformaOrderWithId:currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref];
       if(isProformaOrderDeleted )
       {
           proformaOrderReference=currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref;
       }
        else
        {
            [self showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to update proforma order" withController:nil];
            return;
        }
        
    }
    else
    {
        proformaOrderReference = [SWDefaults lastPerformaOrderReference];
        NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
        
        unsigned long long lastRefId = [[proformaOrderReference substringFromIndex: [proformaOrderReference length] - 10] longLongValue];
        lastRefId = lastRefId + 1;
        
        
        if(nUserId.length == 1)
        {
            nUserId = [NSString stringWithFormat:@"00%@",nUserId];
        }
        else if(nUserId.length == 2)
        {
            nUserId = [NSString stringWithFormat:@"0%@",nUserId];
        }
        proformaOrderReference = [NSString stringWithFormat:@"M%@D%010lld", nUserId, lastRefId];
        if (![proformaOrderReference isEqualToString:[SWDefaults lastPerformaOrderReference]])
        {
            [SWDefaults setLastPerformaOrderReference:proformaOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            proformaOrderReference = [NSString stringWithFormat:@"M%@D%010lld", nUserId, lastRefId];
            if (![proformaOrderReference isEqualToString:[SWDefaults lastPerformaOrderReference]])
            {
                [SWDefaults setLastPerformaOrderReference:proformaOrderReference];
            }
            else
            {
                lastRefId = lastRefId + 1;
                proformaOrderReference = [NSString stringWithFormat:@"M%@D%010lld", nUserId, lastRefId];
                [SWDefaults setLastPerformaOrderReference:proformaOrderReference];
            }
        }
        
    }

    
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    
    NSString *dateString =  [MedRepQueries fetchDatabaseDateFormat];
    //NSString *dateTimeString =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString *startTime = currentVisit.visitOptions.salesOrder.OrderStartTime;

    NSString *InsertProformaOrderQuery=@"INSERT INTO TBL_Proforma_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Start_Time, Emp_Code,Order_Amt,Visit_ID , Custom_Attribute_2,Credit_Customer_ID,Credit_Site_ID,Custom_Attribute_3,Last_Update_Date,Last_Updated_By,Custom_Attribute_1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    
    NSNumber* Totalamount = [orderLineItemsArray valueForKeyPath: @"@sum.OrderItemNetamount"];
    NSString *TotalamountStr = [[[NSString stringWithFormat:@"%@",Totalamount] floatString] AmountStringWithOutComas];
    NSMutableArray *insertProformaOrderParameters=[[NSMutableArray alloc]initWithObjects:
                                           orderID,
                                           proformaOrderReference,
                                           dateString,
                                           [userInfo stringForKey:@"SalesRep_ID"],
                                           currentVisit.visitOptions.customer.Ship_Customer_ID ,
                                           currentVisit.visitOptions.customer.Ship_Site_Use_ID ,
                                           startTime,
                                          [userInfo stringForKey:@"Emp_Code"],
                                           TotalamountStr,
                                            currentVisit.Visit_ID,
                                            currentVisit.visitOptions.salesOrder.selectedCategory.Category,
                                            @"0",/**Selected Deliver To customer Details*/
                                            selectedDeliverToSiteDetailsDic!=nil?[NSNumber numberWithInteger:[[selectedDeliverToSiteDetailsDic valueForKey:@"Site_Use_ID"] integerValue]]:[NSNull null],
                                            currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID,
                                            startTime,
                                            [userInfo stringForKey:@"SalesRep_ID"],
                                            [currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]?@"F":@"0",
                                           nil];
    

    
    BOOL isProformaOrderDetailsSaved= [database executeUpdate:InsertProformaOrderQuery withArgumentsInArray:insertProformaOrderParameters];

    
    
    NSArray *orderArray=currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray;
    
    NSInteger lineNumber=0;
    BOOL proformaOrderLineItemsSaved=NO;
    NSString *proformaOrderLineItemId;

    for (NSInteger i = 0; i < orderArray.count; i++)
    {
        SalesOrderLineItem *orderItem=[orderArray objectAtIndex:i];

        lineNumber=lineNumber+1;
        NSInteger proformaOrderLineNumber=lineNumber;
        
        
        if([NSString createGuid]==nil || proformaOrderLineItemId.length==0){
            proformaOrderLineItemId=[NSString createGuid];
        }
        else if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] &&
           ![currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle]){
            if(orderItem.AppliedAssortMentPlanID==NSNotFound){/*Asst bonus not applied*/
                proformaOrderLineItemId=[NSString createGuid];
            }
        }else{
            proformaOrderLineItemId=[NSString createGuid];
        }
        NSString *proformaOrderLineItemQuery;
        if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                proformaOrderLineItemQuery = @"INSERT INTO TBL_Proforma_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID,Unit_Selling_Price,Def_Bonus,Calc_Price_Flag,Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID',VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code, Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            } else {
                proformaOrderLineItemQuery = @"INSERT INTO TBL_Proforma_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID,Unit_Selling_Price,Def_Bonus,Calc_Price_Flag,Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID',VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
        }else{
            if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                proformaOrderLineItemQuery = @"INSERT INTO TBL_Proforma_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID,Unit_Selling_Price,Def_Bonus,Calc_Price_Flag,Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID', Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            } else {
                proformaOrderLineItemQuery = @"INSERT INTO TBL_Proforma_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID,Unit_Selling_Price,Def_Bonus,Calc_Price_Flag,Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
        }
        

        
        /** Custom_Attribute_1 added In kSQLSavePerfomrmaLineItem*/
        
        NSMutableArray *insertProformaOrderLineItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                       [NSString createGuid],
                                                       proformaOrderReference,
                                                       [NSString stringWithFormat:@"%ld",(long)proformaOrderLineNumber],
                                                       orderItem.OrderProduct.selectedUOM,
                                                       orderItem.OrderProduct.primaryUOM,
                                                       orderItem.OrderProductQty ,
                                                       orderItem.OrderProduct.Inventory_Item_ID ,
                                                       [[[[NSString stringWithFormat:@"%f",orderItem.productSelleingPrice] AmountStringWithOutComas] floatString] AmountStringWithOutComas] ,
                                                       @"0",
                                                       @"N",
                                                       [NSString stringWithFormat:@"%f",(orderItem.appliedDiscount/100)],
                                                       proformaOrderLineItemId,
                                                       orderItem.NotesStr==nil?@"":orderItem.NotesStr,
                                                       [NSNull null],
                                                       nil];
        if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            [insertProformaOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.OrderItemVATChargeamount:[NSNull null]];
            [insertProformaOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCharge:[NSNull null]];
            [insertProformaOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATLevel:[NSNull null]];
            [insertProformaOrderLineItemParameters addObject:orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCode:[NSNull null]];
        }
        if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
            [insertProformaOrderLineItemParameters addObject:orderItem.proximityNotes.length == 0 ? [NSNull null] : orderItem.proximityNotes];
        }
        
        BOOL proormaOrderLineItemSaved= [database executeUpdate:proformaOrderLineItemQuery withArgumentsInArray:insertProformaOrderLineItemParameters];
        if(!proormaOrderLineItemSaved)
        {
            proformaOrderLineItemsSaved=NO;
            break;
        }
        else
        {
            proformaOrderLineItemsSaved=YES;
        }
        if(proformaOrderLineItemsSaved)
        {
            if(orderItem.requestedBonusQty>0)
            {
                lineNumber=lineNumber+1;
                
                NSMutableArray *insertProformaOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                                    [NSString createGuid],
                                                                    proformaOrderReference,
                                                                    [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                    orderItem.bonusProduct.selectedUOM,
                                                                    orderItem.bonusProduct.primaryUOM,
                                                                    [NSString stringWithFormat:@"%0.0f",orderItem.requestedBonusQty] ,
                                                                    orderItem.bonusProduct.Inventory_Item_ID ,
                                                                    @"0" ,
                                                                    [NSString stringWithFormat:@"%0.0f",orderItem.defaultBonusQty] ,
                                                                    @"F",
                                                                    @"0",
                                                                    proformaOrderLineItemId,
                                                                    @"",
                                                                    [NSNull null],
                                                                    nil];
                if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                    [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                    [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                     [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                     [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                     }
                
                if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                    [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                }
                
                BOOL proformaOrderLineItemSaved= [database executeUpdate:proformaOrderLineItemQuery withArgumentsInArray:insertProformaOrderLineItemBonusParameters];
                if(!proformaOrderLineItemSaved)
                {
                    proformaOrderLineItemsSaved=NO;
                    break;
                }
                else
                {
                    proformaOrderLineItemsSaved=YES;
                }
                
                
            }
            if(orderItem.manualFOCQty>0)
            {
                lineNumber=lineNumber+1;
                
                NSMutableArray *insertOrderLineItemManualFocParameters=[[NSMutableArray alloc]initWithObjects:
                                                                        [NSString createGuid],
                                                                        proformaOrderReference,
                                                                        [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                        orderItem.manualFocProduct.selectedUOM,
                                                                        orderItem.manualFocProduct.primaryUOM,
                                                                        [NSString stringWithFormat:@"%0.0f",orderItem.manualFOCQty] ,
                                                                        orderItem.manualFocProduct.Inventory_Item_ID ,
                                                                        @"0" ,
                                                                        @"0" ,
                                                                        @"M",
                                                                        @"0",
                                                                        proformaOrderLineItemId,
                                                                        @"",
                                                                        [NSNull null],
                                                                        nil];
                if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                    [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                    [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                     [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                     [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                     }
                if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                    [insertOrderLineItemManualFocParameters addObject:[NSNull null]];
                }
                
                BOOL proformaOrderLineItemSaved= [database executeUpdate:proformaOrderLineItemQuery withArgumentsInArray:insertOrderLineItemManualFocParameters];
                if(!proformaOrderLineItemSaved)
                {
                    proformaOrderLineItemsSaved=NO;
                    break;
                }
                else
                {
                    proformaOrderLineItemsSaved=YES;
                }

            }
            
            if(orderItem.AppliedAssortMentPlanID!=NSNotFound && orderItem.showApliedAssortMentPlanBonusProducts)
            {
                /** saving assortment bonus line Items*/
                
                NSPredicate * planIDPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.PlanId == %ld",(long)orderItem.AppliedAssortMentPlanID]];
                NSMutableArray *tempAssortMentBonusProductsArray=  [[currentVisit.visitOptions.salesOrder.AssortmentBonusProductsArray filteredArrayUsingPredicate:planIDPredicate] mutableCopy];
                
                for (NSInteger bns=0; bns<tempAssortMentBonusProductsArray.count; bns++) {
                    SalesworxAssortmentBonusProduct *tempAssortMentBonusProduct=[tempAssortMentBonusProductsArray objectAtIndex:bns];
                    
                    lineNumber=lineNumber+1;
                    
                    NSMutableArray *insertProformaOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                                                [NSString createGuid],
                                                                                proformaOrderReference,
                                                                                [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                                tempAssortMentBonusProduct.product.primaryUOM,
                                                                                tempAssortMentBonusProduct.product.primaryUOM,
                                                                                [NSString stringWithFormat:@"%@",tempAssortMentBonusProduct.assignedQty] ,
                                                                                tempAssortMentBonusProduct.product.Inventory_Item_ID ,
                                                                                @"0" ,
                                                                                tempAssortMentBonusProduct.PlanDefaultBnsQty ,
                                                                                @"F",
                                                                                @"0",
                                                                                proformaOrderLineItemId,
                                                                                @"",
                                                                                [NSNumber numberWithInteger:orderItem.AppliedAssortMentPlanID],
                                                                                nil];
                    if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
                        [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                        [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                         [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                         [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                        }
                    
                    if ([appControl.ENABLE_BONUS_ALERTS isEqualToString:KAppControlsYESCode]) {
                        [insertProformaOrderLineItemBonusParameters addObject:[NSNull null]];
                    }
                    
                    BOOL proformaOrderLineItemSaved= [database executeUpdate:proformaOrderLineItemQuery withArgumentsInArray:insertProformaOrderLineItemBonusParameters];
                    if(!proformaOrderLineItemSaved)
                    {
                        proformaOrderLineItemsSaved=NO;
                        break;
                    }
                    else
                    {
                        proformaOrderLineItemsSaved=YES;
                    }
                    
                }
                /** On completion of bonus plan bonus items insertion releasing the proformaOrderLineItemId*/
                proformaOrderLineItemId=nil;
            }
            
        }
        BOOL insertLotStatus=YES;
        for (NSInteger j = 0; j < orderItem.AssignedLotsArray.count; j++)
        {
            SalesOrderAssignedLot *assignLot=[orderItem.AssignedLotsArray objectAtIndex:j];
            NSString *InsertLotQuery = /*kSQLSavePerfomrmaLotItem */@"INSERT INTO TBL_Proforma_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES (?,?,?,?,?,?,?)";
            NSMutableArray *InsertLotItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                     [NSString createGuid],
                                                     proformaOrderReference,
                                                     [NSString stringWithFormat:@"%ld",(long)proformaOrderLineNumber],
                                                     assignLot.lot.Lot_No,
                                                     assignLot.assignedQuantity,
                                                     @"N" ,
                                                     assignLot.lot.Org_ID,
                                                     nil];
            insertLotStatus= [database executeUpdate:InsertLotQuery withArgumentsInArray:InsertLotItemParameters];
            if(!insertLotStatus)
            {
                proformaOrderLineItemsSaved=NO;
                break;
            }
            
        }
        if(orderItem.AppliedAssortMentPlanID==NSNotFound){/*Current item Asst bonus not applied*/
            proformaOrderLineItemId=nil;
        }
    }
    
    if(!proformaOrderLineItemsSaved|| !isProformaOrderDetailsSaved)
    {
        [database rollback];
        [self showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to save order details" withController:nil];
    }
    else
    {
        [database commit];
        [database close];

        if(currentVisit.visitOptions.salesOrder.isManagedOrder && currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails.Orig_Sys_Document_Ref.length>0)
        {
            [self popToVisitOptionsScreenAfterShowingAlertWithTitle:@"" andMessage:@"Order saved Successfully"];

        }
        else
        {
            [self popToVisitOptionsScreenAfterShowingAlertWithTitle:NSLocalizedString(@"Order saved Successfully",nil) andMessage:[NSString stringWithFormat:@"%@: \n %@. \n",NSLocalizedString(@"Reference No", nil),proformaOrderReference]];
        }
        
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    isKeyBoardShowing=YES;
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    isKeyBoardShowing=NO;
}


-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}
#pragma mark UICollectionview Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(LPOImgasesArray.count==KMaximumAllowedLPOImages)
        return  LPOImgasesArray.count;
    
    return  LPOImgasesArray.count+1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(50, 50);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"LPOImageCell";
    
    
    
    SalesWorxSalesOrderLPOImagesCollectionViewCell *cell = (SalesWorxSalesOrderLPOImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.lpoImageview.layer.sublayers=nil;
    cell.selectedImgView.hidden=YES;
    if((LPOImgasesArray.count>0 &&  ([collectionView numberOfItemsInSection:indexPath.section]-1)!=indexPath.row)||
       (LPOImgasesArray.count==KMaximumAllowedLPOImages))
    {
        cell.lpoImageview.image=[UIImage imageWithContentsOfFile:[[SWDefaults getLPOImagesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",[LPOImgasesArray objectAtIndex:indexPath.row]]]];
        
        // Initialization code
        UIGraphicsBeginImageContextWithOptions(cell.lpoImageview.bounds.size, NO, 1.0);
        UIBezierPath *mybezierpath = [UIBezierPath bezierPathWithRoundedRect:cell.lpoImageview.bounds
                                                                cornerRadius:cell.lpoImageview.frame.size.height /2];
        // Add a clip before drawing anything, in the shape of an rounded rect
        [mybezierpath addClip];
        // Draw your image
        [cell.lpoImageview.image drawInRect:cell.lpoImageview.bounds];
        
        // Get the image, here setting the UIImageView image
        cell.lpoImageview.image = UIGraphicsGetImageFromCurrentImageContext();
        
        // Lets forget about that we were drawing
        UIGraphicsEndImageContext();
        
        
        //  UIBezierPath *mybezierpath = [UIBezierPath
        //     bezierPathWithOvalInRect:cell.lpoImageview.bounds];
        
        CAShapeLayer *circle = [CAShapeLayer layer];
        circle.path = mybezierpath.CGPath;
        circle.bounds = CGPathGetBoundingBox(circle.path);
        circle.strokeColor = [UIColor colorWithRed:(54.0/255.0) green:(193.0/255.0) blue:(195.0/255.0) alpha:1].CGColor;
        circle.cornerRadius=cell.lpoImageview.frame.size.height /2;
        circle.fillColor = [UIColor clearColor].CGColor; /*if you just want lines*/
        circle.lineWidth = 3;
        circle.position = CGPointMake(cell.lpoImageview.frame.size.width/2.0, cell.lpoImageview.frame.size.height/2.0);
        circle.anchorPoint = CGPointMake(.5, .5);
        circle.masksToBounds = YES;
        
        [cell.lpoImageview.layer addSublayer:circle];

    }
    
    else
    {
        cell.lpoImageview.image=[UIImage imageNamed:KLPOImagePlaceholderImageName];
    }
    
  //  cell.backgroundColor=[UIColor redColor];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if((LPOImgasesArray.count>0 &&  ([collectionView numberOfItemsInSection:indexPath.section]-1)!=indexPath.row) || LPOImgasesArray.count==KMaximumAllowedLPOImages )
    {
        LPOImagesGalleryCollectionDetailViewController *gallertyViewController=[[LPOImagesGalleryCollectionDetailViewController alloc]initWithNibName:@"LPOImagesGalleryCollectionDetailViewController" bundle:[NSBundle mainBundle]];
        gallertyViewController.view.backgroundColor = KPopUpsBackGroundColor;
        gallertyViewController.modalPresentationStyle = UIModalPresentationCustom;
        NSMutableArray *imagesPathsArray=[[NSMutableArray alloc]init];
        for (NSString * imageId in LPOImgasesArray) {
          [imagesPathsArray addObject:[[SWDefaults getLPOImagesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",imageId]]];
        }
        
        gallertyViewController.swipImagesUrls=imagesPathsArray;
        gallertyViewController.selectedImageIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        gallertyViewController.delegate=self;
        [self.navigationController presentViewController:gallertyViewController animated:YES completion:nil];
    }
    else
    {
        //allow to upload images from library.
        SalesWorxSalesOrderLPOImagesCollectionViewCell *cell = (SalesWorxSalesOrderLPOImagesCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];

        UIAlertAction* pictureAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Take a Picture", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [SWDefaults showCameraWithMode:UIImagePickerControllerCameraCaptureModePhoto withDelegate:self];

                                   }];
        UIAlertAction* uploadAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"Upload from Camera Roll", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                          UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                            picker.delegate = self;
                                            picker.allowsEditing = NO;
                                            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                            UIPopoverController *imagePickerPopOver=[[UIPopoverController alloc]initWithContentViewController:picker];
                                            imagePickerPopOver.delegate=self;
                                            [imagePickerPopOver setPopoverContentSize:CGSizeMake(600, 273) animated:YES];
                                            CGRect relativeFrame = [lpoImagesTitleLabel convertRect:lpoImagesTitleLabel.bounds toView:self.view];
                                            [imagePickerPopOver presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                                        }];

        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:pictureAction,uploadAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)] ,nil];
        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Please select an option" andMessage:@"" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
    }
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    if(picker.sourceType==UIImagePickerControllerSourceTypeCamera)
    {
        UIImage  *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
        NSString *imageGUID=[NSString createGuid];
        [SWDefaults saveLPOImage:image withName:imageGUID];
        [LPOImgasesArray addObject:imageGUID];
        [picker dismissViewControllerAnimated:NO completion:^{
            [lPOImgesCollectionView reloadData];
           
        }];//Or call YES if you want the nice dismissal animation
    }
    else if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary){
        UIImage  *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
        NSString *imageGUID=[NSString createGuid];
        [SWDefaults saveLPOImage:image withName:imageGUID];
        [LPOImgasesArray addObject:imageGUID];
        [picker dismissViewControllerAnimated:NO completion:^{
            [lPOImgesCollectionView reloadData];
            
        }];
    }
    else
    {
//        [picker dismissViewControllerAnimated:NO completion:nil];
//        
//        NSString *oldDirectory = [info objectForKey:UIImagePickerControllerMediaURL];
//        // get the image path
//        NSString *documentsDirectory=[NSMutableString stringWithString:[AWRDefaults getdocumentsdirectory]];
//        documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",appDelegate.jobCardDetails.jobGUID]];
//        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
//                                  withIntermediateDirectories:YES
//                                                   attributes:nil
//                                                        error:nil];
//        
//        documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",Selectedmark.markGUID]];
//        
//        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
//                                  withIntermediateDirectories:YES
//                                                   attributes:nil
//                                                        error:nil];
//        documentsDirectory= [documentsDirectory stringByAppendingPathComponent:@"/temp"];
//        
//        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
//                                  withIntermediateDirectories:YES
//                                                   attributes:nil
//                                                        error:nil];
//        
//        
//        NSString *newDirectory = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-video.MOV",Selectedmark.markGUID]];
//        NSFileManager *fm = [NSFileManager defaultManager];
//        NSError *error;
//        
//        if([fm fileExistsAtPath:newDirectory]) {
//            [fm removeItemAtPath:newDirectory error:nil];
//        }
//        
//        [fm moveItemAtPath:oldDirectory
//                    toPath:newDirectory
//                     error:&error];
//        Selectedmark.videoTaken=KYesCode;
//        Selectedmark.videoPath=newDirectory;
//        [videoButton setImage:[self thumbnailImageFromURL:[NSURL fileURLWithPath:Selectedmark.videoPath]] forState:UIControlStateNormal];
    }
    
}
-(void)updateImagesArray:(NSMutableArray*)imagesArray
{
    NSLog(@"imagesArray after Delete%@",imagesArray);
    NSMutableArray *tempLPOImagesArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<imagesArray.count; i++) {
        [tempLPOImagesArray addObject:[[[imagesArray objectAtIndex:i]lastPathComponent]stringByDeletingPathExtension]];
    }
    
    LPOImgasesArray=tempLPOImagesArray;
    [lPOImgesCollectionView reloadData];
}


-(void)saveLpoPhotosToAlbum {
    
    
    for (NSInteger i=0;i<LPOImgasesArray.count;i++)
    {
        NSString *LPOImagesDirectory=[SWDefaults getLPOImagesFolderPath];
        NSString *imagePath=   [NSString stringWithFormat:@"%@/%@", LPOImagesDirectory, [NSString stringWithFormat:@"%@_%ld.jpg",[SWDefaults lastOrderReference],(long)i+1]];
        
    __block PHObjectPlaceholder *placeholder;
    
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest* createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromImageAtFileURL:/*[UIImage imageWithData:[NSData dataWithContentsOfFile:imagePath]]*/[NSURL URLWithString:imagePath]];
        placeholder = [createAssetRequest placeholderForCreatedAsset];
        
        } completionHandler:^(BOOL success, NSError *error) {
            if (success)
            {
                NSLog(@"didFinishRecordingToOutputFileAtURL - success for ios9");
            }
            else
            {
                NSLog(@"%@", error);
            }
        }];
    }
}


-(void)checkPhotoLibraryAccess
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    isLPOPhotoLibraryAccessCheckingDone=YES;
    if (status == PHAuthorizationStatusAuthorized) {
        // Access has been granted.
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        // Access has been denied.
        
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Go To Settings", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       BOOL canOpenSettings =(UIApplicationOpenSettingsURLString != NULL);
                                       if (canOpenSettings)
                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                   }];
        UIAlertAction* CancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           
                                       }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];

        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Photo Library Access Denied" andMessage:@"You previously chose not to use your Photo library with the Salesworx app. \n \n To save the LPO imges in photos album,turn on the \"Photo Library\" in the privacy section." andActions:actionsArray withController:self];
    }
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
            }
            
            else {
                // Access has been denied.
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
       // [SWDefaults showDefualtAlertMessage:@"You've been restricted from using the photo library on this device. Without photo library access this feature won't work." withtitle:@"OK" withViewController:self];
    }
}


-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.titleKey=popOverString;
    if([popOverString isEqualToString:KSalesOrderUOMPopOverTitle])
    {
        popOverVC.preferredContentSize = CGSizeMake(250, 250);
        popOverVC.disableSearch=YES;
        
    }
    else{
        popOverVC.preferredContentSize = CGSizeMake(300, 300);
        popOverVC.disableSearch=NO;
    }
    
    popover.delegate = self;
    popOverVC.titleKey=popOverString;
    popover.sourceView = selectedTextField.rightView;
    popover.sourceRect =selectedTextField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{
        if(isKeyBoardShowing)
        {
            [UIView animateWithDuration:0.2 animations: ^{
                [self.view endEditing:YES];
                
            } completion:^(BOOL finished) {
            }];
            
        }
        
    }];
    
}


@end
