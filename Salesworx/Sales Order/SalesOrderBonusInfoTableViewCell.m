//
//  SalesOrderBonusInfoTableViewCell.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesOrderBonusInfoTableViewCell.h"

@implementation SalesOrderBonusInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.btnSetQty.layer.masksToBounds = YES;
    self.btnSetQty.layer.cornerRadius = self.btnSetQty.frame.size.height/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
