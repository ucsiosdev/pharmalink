//
//  SalesWorxAssortmentBonusInfoViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/5/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxAssortmentBonusInfoViewController.h"
#import "SalesWorxAssortmentBonusInfoProductsTableViewCell.h"
#import "SalesWorxAssortmentBonusInfoSlabsTableViewCell.h"
@interface SalesWorxAssortmentBonusInfoViewController ()

@end

@implementation SalesWorxAssortmentBonusInfoViewController
@synthesize plan;
-(void)viewDidLoad
{
    
}
-(void)viewWillAppear:(BOOL)animated

{
    bonusPopUpView.backgroundColor=UIViewBackGroundColor;
    if(plan.OfferProductsArray.count>3 || plan.bonusProductsArray.count>3)
    {
        //xContentViewHeightConstraint.constant
        CGFloat calHeight =500+((plan.OfferProductsArray.count>plan.bonusProductsArray.count?(plan.OfferProductsArray.count-2):(plan.bonusProductsArray.count-2))*44.0f);
        xContentViewHeightConstraint.constant=calHeight>650?650:calHeight;
    }
    if(self.navigationController!=nil && self.navigationController.viewControllers.count>1){
        [self setHiddenAnimated:NO duration:1.25];
    }else{
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];

    }
}
-(IBAction)cancelButtonTapped:(id)sender
{
    if(self.navigationController!=nil && self.navigationController.viewControllers.count>1){
        [self.navigationController popViewControllerAnimated:NO];
    }
    else{
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseBonusPopOver) userInfo:nil repeats:NO];
    }
}
-(void)CloseBonusPopOver
{
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = bonusPopUpView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==offerItemsTableView)
    {
        return plan.OfferProductsArray.count;
    }
    else if (tableView==BonusItemsTableView)
    {
        return plan.bonusProductsArray.count;
    }
    else if (tableView==PlanSlabsTableView)
    {
        return plan.SlabsArray.count;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView==PlanSlabsTableView)
    {
        return 44.0f;
    }
    return 0;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView==PlanSlabsTableView)
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, bonusTableHeaderView.frame.size.width, bonusTableHeaderView.frame.size.height)];
        bonusTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
        [view addSubview:bonusTableHeaderView];
        return view;
    }
    return [[UIView alloc] init];
}   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
    
    
    if(tableView==offerItemsTableView)
    {
        static NSString* identifier=@"AsstBnsInfoProductsCellIdentifier";
        SalesWorxAssortmentBonusInfoProductsTableViewCell *cell = (SalesWorxAssortmentBonusInfoProductsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil || cell.tag!=100) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxAssortmentBonusInfoProductsTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.tag=100;
        Products *pro=[plan.OfferProductsArray objectAtIndex:indexPath.row];
        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
        cell.ItemCodeLabel.text=pro.Item_Code;
        cell.productnameLabel.text=pro.Description;

        if([plan.MandatoryOfferProductItemCodesArray containsObject:pro.Item_Code]){
            cell.ItemCodeLabel.textColor=[UIColor colorWithRed:(18.0/255.0) green:(186.0/255.0) blue:(156.0/255.0) alpha:1.0];
            cell.productnameLabel.textColor=[UIColor colorWithRed:(18.0/255.0) green:(186.0/255.0) blue:(156.0/255.0) alpha:1.0];
        }else{
            cell.ItemCodeLabel.textColor=MedRepSingleLineLabelSemiBoldFontColor;
            cell.productnameLabel.textColor=MedRepSingleLineLabelSemiBoldFontColor;
        }
        return cell;
    }
    else if (tableView==BonusItemsTableView)
    {
        static NSString* identifier=@"AsstBnsInfoProductsCellIdentifier";
        SalesWorxAssortmentBonusInfoProductsTableViewCell *cell = (SalesWorxAssortmentBonusInfoProductsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil || cell.tag!=101) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxAssortmentBonusInfoProductsTableViewCell" owner:nil options:nil] firstObject];
        }
        cell.tag=101;

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        Products *pro=[plan.bonusProductsArray objectAtIndex:indexPath.row];
        cell.productnameLabel.text=pro.Description;
        cell.ItemCodeLabel.text=pro.Item_Code;
        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;

        return cell;

    }
    else{
        
        
        static NSString* identifier=@"AsstBnsInfoSlabCellIdentifier";
        SalesWorxAssortmentBonusInfoSlabsTableViewCell *cell = (SalesWorxAssortmentBonusInfoSlabsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        cell.tag=102;

        if(cell == nil || cell.tag!=102) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxAssortmentBonusInfoSlabsTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        SalesworxAssortmentBonusSlab *slab=[plan.SlabsArray objectAtIndex:indexPath.row];
        cell.fromNumberLbl.text=slab.Prom_Qty_From;
        cell.ToNumberLbl.text=slab.Prom_Qty_To;
        cell.typeLbl.text=slab.Price_Break_Type_Code;
        cell.bonusLbl.text=slab.Get_Qty;
        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;

        return cell;
    }
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = bonusPopUpView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
