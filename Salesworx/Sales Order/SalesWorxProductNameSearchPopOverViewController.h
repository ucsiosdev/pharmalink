//
//  SalesWorxProductNameSearchPopOverViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/22/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepCustomClass.h"

@protocol SalesWorxProductNameSearchPopOverViewControllerDelegate <NSObject>

-(void)didSelectProductNameSearchPopOverController:(NSIndexPath*)selectedIndexPath;
//-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath;
//-(void)didDismissPopOverController;
@end

@interface SalesWorxProductNameSearchPopOverViewController : UIViewController<UISearchControllerDelegate,UISearchResultsUpdating>

{
    NSMutableArray* filteredPopOverContentArray;
    
    id delegate;
}

@property(nonatomic) id salesWorxPopOverControllerDelegate;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *popoverTableViewTopConstraint;
@property(strong,nonatomic) NSMutableArray* popOverContentArray;
@property(nonatomic) BOOL isComingFromProductsScreen;
@property(strong,nonatomic) NSString* titleKey;

@property(strong,nonatomic) UIPopoverController * popOverController;
@property (strong, nonatomic) IBOutlet UITableView *popOverTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *popOverSearchBar;
@property(nonatomic) BOOL enableSwipeToDelete;
@property(strong,nonatomic) NSString* popoverType;
@property(strong,nonatomic) NSString*headerTitle;
@property (strong, nonatomic) UISearchController *searchController;

@property(nonatomic) BOOL disableSearch;
@property(nonatomic) BOOL enableArabicTranslation;


@property (strong, nonatomic) IBOutlet NSString *filterText;

@end
