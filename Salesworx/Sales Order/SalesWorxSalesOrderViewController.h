//
//  SalesWorxSalesOrderViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/6/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SalesWorxPaymentCollectionInvoicesTableViewCell.h"
#import "SalesWorxSalesOrdersTableCellTableViewCell.h"
#import "SWDefaults.h"
#import "SalesWorxTableView.h"
#import "SalesWorxSingleLineLabel.h"
#import "SalesWorxTableViewHeaderView.h"
#import "MedRepUpdatedDesignCell.h"
#import "SWDatabaseManager.h"
#import "Singleton.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "MedRepButton.h"
#import "SalesWorxSalesOrderProductsTableViewBrandcodeCell.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxProductsFilterViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesOrderNotesViewConrollerViewController.h"
#import "SalesOrderLotAssigningViewController.h"
#import "SalesOrderBonusInfoViewController.h"
#import "SalesOrderStockInfoViewController.h"
#import "SalesOrderSalesHistoryInfoViewController.h"
#import "SalesworxSalesOrderConfirmationViewController.h"
#import "SalesWorxDescriptionLabel1.h"
#import "SalesWorxTitleLabel1.h"
#import "SalesWorxTitleLabel2.h"
#import "SalesWorxDescriptionLabel4.h"
#import "SalesWorxDescriptionLabel5.h"
#import "SalesWorxImageView.h"
#import "SalesWorxTitleLabel4.h"
#import "SalesWorxProductNameSearchPopOverViewController.h"
#import "SalesWorxDefaultButton.h"
#import "ZBarSDK.h"
#import "SalesWorxBarCodeScannerManager.h"
#import "SalesOrderAssortmentBonusAllocationViewController.h"
#import "MedRepPanelTitle.h"
#import "SalesWorxSalesOrderItemDuplicateOHViewController.h"
#import "SalesWorxProductMediaViewController.h"
#import "SalesWorxSalesOrderProductImageDescriptionViewController.h"

@interface SalesWorxSalesOrderViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SalesWorxProductsFilterViewControllerDelegate,SalesWorxPopoverControllerDelegate,MGSwipeTableCellDelegate,SalesOrderNotesViewConrollerViewControllerDelegate,SalesOrderLotAssigningViewControllerDelegate,SalesWorxProductNameSearchPopOverViewControllerDelegate,SalesOrderAssortmentBonusAllocationViewControllerDelegate,SalesWorxSalesOrderItemDuplicateOHViewControllerDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate, SalesOrderBonusInfoViewControllerDelegate>
{
    IBOutlet SalesWorxTableView *OrdersTableView;
    IBOutlet SalesWorxTableViewHeaderView *OrdersTableHeaderView;
    IBOutlet UIView *NoSelectionView;

    IBOutlet UITableView *productsTableView;
    IBOutlet SalesWorxDescriptionLabel5 *NoSelectionMessageLabel;
    IBOutlet UIImageView *NoSelectionImageView;
    BOOL isSearching;
    NSMutableArray* filteredProductsArray;
    NSMutableArray * manualFocProductsArray;
    NSMutableArray*productsArray;
    NSMutableArray* filteredProductsArrayGroupedByBrandCode;
    NSMutableArray* productsArrayGroupedByBrandCode;
    NSMutableArray *searchingProductsArray;
    NSMutableArray * productsArraywithoutRestrictedProducts;
    
    NSMutableArray *selectedProductIndexPathArray;
    NSIndexPath *selectedProductIndexpath;
    
    IBOutlet MedRepHeader *customerNameLabel;
    IBOutlet MedRepProductCodeLabel *customerCodeLabel;
    IBOutlet UILabel *customerAvailableBalanceLabel;
    IBOutlet SalesWorxImageView *customerstatusImageView;
    IBOutlet UILabel *productNameLabel;
    IBOutlet MedRepElementTitleLabel *productCodeLabel;
    IBOutlet UIView *ProductSearchContainerView;
    
    /* product Details View UI Elements*/
    IBOutlet SalesWorxDropShadowView *productDetailsView;
    IBOutlet SalesWorxDescriptionLabel1 *productAvailableStockLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productRetailPriceLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productWholeSalePriceLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productNearestExpiryDateLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productDefaultDiscountLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productDCQtyLabel;
    IBOutlet UILabel *DCQtyLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productSpecialDiscountLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productBonusItemLabel;
    IBOutlet SalesWorxDescriptionLabel1 *productDefaultBonusLabel;
    IBOutlet UIButton *productDescriptionInfoButton;
    IBOutlet SalesWorxDefaultButton *productStockInfoButton;
    IBOutlet SalesWorxDefaultButton *productBonusInfoButton;
    IBOutlet SalesWorxDefaultButton *productSalesHistoryInfoButton;
    
    IBOutlet SalesWorxTitleLabel1 *defaultDiscountStringLabel;
    IBOutlet SalesWorxTitleLabel1 *specialDiscountStringLabel;
    IBOutlet SalesWorxTitleLabel1 *bonusItemStringLabel;
    IBOutlet SalesWorxTitleLabel1 *defaultBonusItemStringLabel;
    NSString *enableProductRank;
    NSString *showProductLongDescription;
    NSString *popOverTitle;
    /* product Order Details View UI Elements*/
    IBOutlet SalesWorxDropShadowView *productOrderDetailsView;
    IBOutlet MedRepTextField *orderQtyTextField;
    IBOutlet MedRepTextField *UOMTextField;
    IBOutlet MedRepTextField *sellingPriceTextField;
    IBOutlet MedRepTextField *discountTextField;
    IBOutlet MedRepTextField *requestedBonusTextField;
    IBOutlet MedRepTextField *manualFOCTextField;
    IBOutlet MedRepTextField *FOCQtyTextField;
    
    IBOutlet SalesWorxTitleLabel2 *manualFocStringLabel;
    IBOutlet SalesWorxTitleLabel2 *FocQtyStringLabel;
    IBOutlet SalesWorxDescriptionLabel4 *OrderItemTotalAmountLabel;
    IBOutlet SalesWorxDescriptionLabel4 *OrderItemDiscountAmountLabel;
    IBOutlet SalesWorxDescriptionLabel4 *OrderItemNetAmountLabel;
    IBOutlet MedRepButton *addToProductButton;

    
    /**Product TableView UI elements*/
    IBOutlet UIButton *productsFilterButton;
    IBOutlet UISearchBar *productsSearchBar;
    
    NSMutableArray *productsBrandCodesArray;
    NSMutableArray *ProductTableViewExpandedSectionsArray;

    Products * selectedProduct;
    SalesOrderLineItem *selectedOrderLineItem;
    
    
    /*Layout Constarints*/
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *NoSelectionTopMarginConstraint;
    IBOutlet NSLayoutConstraint *productDetailsViewHeightConstraiint;
    IBOutlet NSLayoutConstraint *productDetailsViewTopConstraiint;
    IBOutlet NSLayoutConstraint *productOrderDetailsViewHeightConstraiint;
    IBOutlet NSLayoutConstraint *productOrderDetailsViewTopConstraiint;
    
    IBOutlet NSLayoutConstraint *productSalesHistoryInfoButtonHeightContraint;
    IBOutlet NSLayoutConstraint *productSalesHistoryInfoButtonWidthContraint;
    IBOutlet NSLayoutConstraint *productSalesHistoryInfoButtontarilingMarginContraint;

    
    UIBarButtonItem *saveBarButtonItem;
    UIBarButtonItem *closeVisitButton;
    
    NSMutableDictionary* previousFilteredParameters;
    UIImageView *blurredBgImage;
    NSMutableArray* orderLineItemsArray;

    UIPopoverController * FOCPopOverController;
    
    AppControl *appControl;
    NSIndexPath *selectedOrderItemIndexpath;
    BOOL isUpdatingOrder;
    NSMutableArray *selectedLineItemMoreOptionsArray;
    NSIndexPath *swipedCellIndexpath;
    IBOutlet MedRepElementDescriptionLabel*ordersTotalAmountLbl;
    
    
    
    /** this bools for to show alert messages for templates and confirm orders do not use for any other ptrpose*/
    BOOL showOneOrMoreProductPriceNotAvailable;
    BOOL showOneOrMoreProductsNotAvailable;
    BOOL showOneOrMoreManualFocProductsNotAvailable;
    BOOL showOneOrMoreBonusProductsNotAvailable;
    BOOL showOneOrMoreProductsDiscountChanged;
    BOOL showOneOrMoreProductsBonusQtyChanged;
    BOOL showOneOrMoreProductsBonusProductChanged;
    BOOL showOneOrMoreProductsLotsareDiscarded;

   /*end*/
    BOOL showOrderTableViewNewItemAnimationColor;
    BOOL isBonusGroupEnableForSelectedCustomer;
    BOOL allowSearchBarEditing;
    UIActivityIndicatorView *searchBarActivitySpinner;
    BOOL isBrandCodeSectionCellTapped;
    
    
    IBOutlet SalesWorxTitleLabel2 *RequestedBonusStringLabel;
    IBOutlet SalesWorxTitleLabel2 *OrderQuantityStringLabel;

    IBOutlet SalesWorxTitleLabel2 *DiscountStringLabel;
    IBOutlet NSLayoutConstraint *AddToOrderButtonleftMargin;
    IBOutlet NSLayoutConstraint *AddToOrderButtonRightMargin;
    IBOutlet NSLayoutConstraint *manualFocStringLabelHeightConstrain;
    IBOutlet NSLayoutConstraint *ManualFocTextfieldHeightConstrain;
    IBOutlet NSLayoutConstraint *FOCQtyStringLabelHeightConstraint;
    IBOutlet NSLayoutConstraint *FOCQtyTextFieldHeightConstraint;
    IBOutlet NSLayoutConstraint *AddtoOrderButtonBottomMargin;
    
    BOOL isManageSalesOrderUpdated;
    
    NSDate *methodStart;
    NSDate * searchStartTime;
    IBOutlet NSLayoutConstraint *xDefaultBonusValueLabelTrailingMarginConstraint;
    IBOutlet NSLayoutConstraint *xBonusItemStringLabelLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint *xBonusItemStringLabelLeadingMarginToViewConstraint;
    IBOutlet NSLayoutConstraint *xDefaultBonusStringLabelLeadinggMarginConstraint;

    IBOutlet NSLayoutConstraint *xOrderProductTotalAmountLabelTrailingMarginToOrderDetailsViewConstraint;
    IBOutlet NSLayoutConstraint *xOrderProductTotalAmountLabelTrailingMarginToDiscountLabelConstraint;
    IBOutlet NSLayoutConstraint *xRequestedBonusTextfieldLeadingMargintoSellingPriceTextfield;
    IBOutlet SalesWorxTitleLabel4 *OrderProductNetAmountLabel;
    IBOutlet SalesWorxTitleLabel4 *OrderProductDiscountStringLabel;
    
    IBOutlet SalesWorxTableViewHeaderView *orderItemsTableViewHeaderView;

    IBOutlet UIButton *OrderTextfieldsClearButton;
    
    
    BOOL isSearchBarClearButtonTapped; /* do not use BOOL other than for product search popover */
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    
    IBOutlet NSLayoutConstraint *xSearchBarTrailingspaceToFilterButton;
    IBOutlet SalesWorxDefaultButton *barcodeScanButton;
    ZBarReaderViewController *reader;
    ProductSearchType proSearchType;
    
    NSMutableArray *AssortmentBonusProductsArray;
    IBOutlet MedRepPanelTitle *OrderItemsViewHeaderTitleLabel;
    IBOutlet SalesWorxDefaultSegmentedControl *OrderItemsTableSegmentedControl;
    NSIndexPath *selectedAssortmentBonusItemIndexpath;
    
    NSMutableArray *tempSalesOrderAssortmentBonusProductsArray;
    SalesOrder *tempSalesOrder;
    
    /**Order tableView header*/
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderDiscountLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderDiscountLabelLeadingMarginConstraint;
    IBOutlet MedRepElementTitleLabel * OrderTableHeaderTotalLabel;
    

    IBOutlet MedRepButton *productMediaButton;
}
- (void)BarCodeScannerButtonBackPressed:(id)sender;
-(IBAction)productDescriptionInfoButtonTapped:(id)sender;
-(IBAction)productStockInfoButtonTapped:(id)sender;
-(IBAction)productBonusInfoButtonTapped:(id)sender;
-(IBAction)productSalesHistoryInfoButtonTapped:(id)sender;
-(IBAction)addToOrderButtonTapped:(id)sender;
-(IBAction)clearButtonTapped:(id)sender;
- (IBAction)SalesOrderTextFieldEditingChanged:(id)sender;
-(IBAction)barCodeScanButtonTapped:(id)sender;



@property (strong,nonatomic) NSString *parentViewString;
@property (strong,nonatomic) NSString *orderTypeTitle;
@property  BOOL isOrderConfirmed;
@property(strong,nonatomic) SalesWorxVisit * currentVisit;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;


-(id)fetchProductPriceListData:(Products*)Product;



@end
