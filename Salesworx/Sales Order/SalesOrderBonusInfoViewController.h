//
//  SalesOrderBonusInfoViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepTextField.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesOrderLotAssigningTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxTableView.h"
#import "MedRepButton.h"
#import "SalesOrderBonusInfoTableViewCell.h"
#import "MedRepView.h"
#import "SalesWorxPresentControllerBaseViewController.h"
#import "SalesWorxNavigationHeaderLabel.h"
#import "MedRepTextView.h"

@protocol SalesOrderBonusInfoViewControllerDelegate
    @optional
    -(void)updateOrderLineItem:(SalesOrderLineItem *)orderLineItem atIndex:(NSInteger)index;
    
@end

@interface SalesOrderBonusInfoViewController : SalesWorxPresentControllerBaseViewController
{
    int indexOfRecommendedBonus;
    IBOutlet SalesWorxDefaultButton *saveButton;
    IBOutlet  UILabel *productNameLabel;
    IBOutlet  UILabel *productCodeLabel;
    IBOutlet UITableView *bonusTableView;
    IBOutlet SalesWorxTableViewHeaderView *bonusTableHeaderView;
    IBOutlet SalesWorxNavigationHeaderLabel *titleLabel;
    
    IBOutlet UILabel *lblTitleEnteredQuantity;
    IBOutlet UILabel *lblEnteredQuantity;
    
    IBOutlet MedRepView *notesView;
    IBOutlet MedRepTextView *notesTextView;
    IBOutlet NSLayoutConstraint *xNotesViewHeightConstraint;
    IBOutlet NSLayoutConstraint *xNotesViewBottomConstraint;
    
    IBOutlet UIView *recommendedLegendView;
    IBOutlet NSLayoutConstraint *xRecommendedViewHeightConstraint;
    IBOutlet NSLayoutConstraint *xRecommendedViewBottomConstraint;
    IBOutlet NSLayoutConstraint *xFromLabelLeadingConstraint;
}
@property (strong,nonatomic) id <SalesOrderBonusInfoViewControllerDelegate>bonusDelegate;
    
@property (strong,nonatomic)SalesOrderLineItem *OrderLineItem;
@property (strong,nonatomic)ReturnsLineItem *returnLineItem;
@property (strong,nonatomic)NSMutableArray *bonusArray;
@property (strong,nonatomic)NSMutableArray *productsArray;
@property BOOL isFromReturnsView;
@property BOOL isOrderQuantityRecommended;
@property BOOL isOrderQuantityRecommendationMandatory;
@property NSInteger selectedOrderLineItemIndexForBonusRecommendation;
@end
