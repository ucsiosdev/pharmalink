//
//  SalesWorxSalesOrderItemDuplicateOHViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxSalesOrderItemDuplicateOHViewController.h"
#import "SalesWorxSalesOrderItemDuplicateOHTableViewCell.h"
#import "MedRepQueries.h"
@interface SalesWorxSalesOrderItemDuplicateOHViewController ()

@end

@implementation SalesWorxSalesOrderItemDuplicateOHViewController
@synthesize productOrderHistoryArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    messageLabel.textColor=[UIColor redColor];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return productOrderHistoryArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, stockTableHeaderView.frame.size.width, stockTableHeaderView.frame.size.height)];
    stockTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:stockTableHeaderView];
    return view;
}   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"DuplicateOHItemCellIdentifier";
    SalesWorxSalesOrderItemDuplicateOHTableViewCell *cell = (SalesWorxSalesOrderItemDuplicateOHTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesOrderItemDuplicateOHTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    
    
    NSDictionary *previousOrderItem=[productOrderHistoryArray objectAtIndex:indexPath.row];
    cell.orderNumberLabel.text=[previousOrderItem valueForKey:@"OrderNumber"];
    cell.orderDateLabel.text=[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[NSString stringWithFormat:@"%@", [previousOrderItem valueForKey:@"OrderDate"]]];
    cell.orderQuantityLabel.text=[SWDefaults getValidStringValue:[previousOrderItem valueForKey:@"OrderQuantity"]];
    //[MedRepQueries getCurrentDateInDeviceDateWithOutTimeDisplayFormat:[MedRepQueries convertNSStringtoNSDate:[previousOrderItem valueForKey:@"OrderDate"]] ];

    cell.orderDateLabel.textColor=MedRepMenuTitleFontColor;
    cell.orderNumberLabel.textColor=MedRepMenuTitleFontColor;
    cell.orderQuantityLabel.textColor=MedRepMenuTitleFontColor;
    cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
    return cell;
    
}
- (IBAction)CancelButtonTapped:(id)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.oHDelegate didUserCancelledOHDuplicateItem];
    });
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

- (IBAction)ConfirmButtonTapped:(id)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.oHDelegate didUserConfirmedOHDuplicateItem];
    });
    [self dimissViewControllerWithSlideDownAnimation:YES];

}
@end
