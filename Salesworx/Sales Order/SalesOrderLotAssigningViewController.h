//
//  SalesOrderLotAssigningViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepTextField.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesOrderLotAssigningTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxTableView.h"
#import "MedRepButton.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@protocol SalesOrderLotAssigningViewControllerDelegate

-(void)updateLotsAllcation:(NSMutableArray *)assignedLotsArray isReAllocationForUpdatedQuantity:(BOOL)status;
-(void)lotsAllocationCancelled;
@end

@interface SalesOrderLotAssigningViewController : SalesWorxPresentControllerBaseViewController<MGSwipeTableCellDelegate>
{
    NSMutableArray *productStockArray;
    UIPopoverController * LotNumbersPopOverController;
    NSString *popOverTitle;
    
    IBOutlet MedRepTextField *LotNumberTextField;
    IBOutlet MedRepTextField *QuantityTextField;
    IBOutlet UILabel *lotQuantityLabel;
    IBOutlet UILabel *orderQuantityLabel;
    

    IBOutlet SalesWorxTableViewHeaderView *lotsTableHeaderView;
    IBOutlet SalesWorxTableView *lotSelectionTableView;
    NSIndexPath *selectedLotIndexpath;
    NSMutableArray *AssignedLotsArray;
    SalesOrderAssignedLot *selectedAssignedLot;
    BOOL isUpdating;
    IBOutlet MedRepButton *addButton;
    
    
    IBOutlet MedRepElementTitleLabel *productCodeLabel;
    IBOutlet UILabel *productNameLabel;
    
    IBOutlet UIButton *saveButton;

}
@property (strong,nonatomic)SalesOrderLineItem *OrderLineItem;
@property (nonatomic) BOOL isReAllocationForUpdatedQuantity;
@property (nonatomic) BOOL isLotsAssignmentReadOnly;

-(IBAction)saveButtonTapped:(id)sender;
-(IBAction)cancelButtonTapped:(id)sender;
@property (strong,nonatomic)NSString *wareHouseId;
@property (strong,nonatomic) id <SalesOrderLotAssigningViewControllerDelegate>salesOrderLotAssigningViewControllerDelegate;


@end
