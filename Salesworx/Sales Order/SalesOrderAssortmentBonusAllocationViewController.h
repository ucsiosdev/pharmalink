//
//  SalesOrderAssortmentBonusAllocationViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/26/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepButton.h"
#import "MedRepTextField.h"
#import "SalesWorxTableView.h"
#import "SalesWorxTableViewHeaderView.h"
#import "MGSwipeTableCell.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxDescriptionLabel2.h"
@protocol SalesOrderAssortmentBonusAllocationViewControllerDelegate

-(void)UpdateAssortmentBonusAllcation:(NSMutableArray *)bonusproductsArray WithPlan:(SalesworxAssortmentBonusPlan *)bnsPlan AndSelectedOrderItem:(SalesOrderLineItem *)OrderItem;
//-(void)lotsAllocationCancelled;
@end
@interface SalesOrderAssortmentBonusAllocationViewController : UIViewController<MGSwipeTableCellDelegate>

{
    IBOutlet SalesWorxDescriptionLabel2 *bonusQuantityLabel;
    IBOutlet MedRepButton   *addButton;
    IBOutlet UILabel         *productNameLabel;
    IBOutlet UIButton        *saveButton;
    IBOutlet UIView *assigningBonusContentView;

    
    IBOutlet MedRepTextField *productTextField;
    IBOutlet MedRepTextField *QuantityTextField;
    IBOutlet SalesWorxTableViewHeaderView *bonusItemsTableHeaderView;
    IBOutlet SalesWorxTableView *bonusItemsSelectionTableView;
    IBOutlet SalesWorxTableView *OrderItemsTableView;
    IBOutlet MedRepElementTitleLabel *bonusItemsSelectionTableViewBonusQtyLabel;
    IBOutlet SalesWorxTableViewHeaderView *OrderItemsTableHeaderView;

    NSIndexPath *selectedIndexpath;
    BOOL isUpdating;
    
    SalesworxAssortmentBonusProduct *selectedAssortmentBonusProduct;
    UIPopoverController * LotNumbersPopOverController;
    NSString *popOverTitle;
     IBOutlet SalesWorxDefaultButton *CloseButton;
    IBOutlet SalesWorxDescriptionLabel2 *AssignedQtyLbl;
    IBOutlet MedRepElementTitleLabel *errorMessageLbl;

}
- (IBAction)AsstBonusInfoButtonTapped:(id)sender;
@property (strong,nonatomic) SalesworxAssortmentBonusPlan  *plan;
@property (strong,nonatomic) NSMutableArray *AllProductsArray;
@property (nonatomic) double bonusQty;
@property (strong,nonatomic) id <SalesOrderAssortmentBonusAllocationViewControllerDelegate>assortmentBonusAllocationViewControllerDelegate;
@property (nonatomic) BOOL isReadOnlyView;
@property (nonatomic) BOOL isBonusAllocationMandatory;

@property (nonatomic,strong) NSMutableArray *assignedBonusItemsArray;
@property (nonatomic,strong) SalesOrderLineItem *selectedSalesOrderLineItem;
@property (strong,nonatomic) NSMutableArray *OrderedOfferItemsArray;
@property (strong,nonatomic) SalesOrderLineItem *deletingOfferProduct;

@end
