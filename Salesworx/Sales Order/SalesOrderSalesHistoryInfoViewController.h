//
//  SalesOrderSalesHistoryInfoViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepTextField.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesOrderLotAssigningTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxTableView.h"
#import "MedRepButton.h"
#import "SalesOrderSalesHistoryTableViewCell.h"
#import "MedRepView.h"
#import "SWDatabaseManager.h"
#import "AppControl.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@interface SalesOrderSalesHistoryInfoViewController : SalesWorxPresentControllerBaseViewController
{
IBOutlet  UILabel *productNameLabel;
IBOutlet  UILabel *productCodeLabel;
IBOutlet UITableView *salesHistoryTableView;
IBOutlet SalesWorxTableViewHeaderView *salesHistoryTableHeaderView;
    AppControl *appControl;
    NSMutableArray *salesHistoryArray;
    NSMutableArray * peroidsArray;
    Products *productDetails;
    IBOutlet SalesWorxSingleLineLabel *AverageSalesLabel;
    IBOutlet NSLayoutConstraint *MonthlySalesAverageContainerViewHeightConstraint;
}

@property (strong,nonatomic)SalesOrderLineItem *OrderLineItem;
@property (strong,nonatomic)SalesWorxVisit *customerVisit;
@property (strong,nonatomic)ReturnsLineItem *returnLineItem;
@property BOOL isFromReturnsView;
@end

