//
//  SalesOrderStockInfoTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesOrderStockInfoTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *LotNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *QuantityLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ExpiryDateLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *WareHouseLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *OnOrderLbl;
@end
