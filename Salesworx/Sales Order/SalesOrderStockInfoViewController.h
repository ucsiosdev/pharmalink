//
//  SalesOrderStockInfoViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepTextField.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesOrderLotAssigningTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxTableView.h"
#import "MedRepButton.h"
#import "SalesOrderStockInfoTableViewCell.h"
#import "MedRepView.h"
#import "SWDatabaseManager.h"
#import "AppControl.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@interface SalesOrderStockInfoViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet  UILabel *productNameLabel;
    IBOutlet  UILabel *productCodeLabel;
    IBOutlet UITableView *stockTableView;
    IBOutlet SalesWorxTableViewHeaderView *stockTableHeaderView;
    
    IBOutlet UIView *onOrderQuantityView;
    IBOutlet NSLayoutConstraint *onOrderQuantityViewHeightConstraint;
    IBOutlet SalesWorxSingleLineLabel*OnOrderQtyLabel;
    
    Products *productDetails;
    AppControl *appControl;
}

@property (strong,nonatomic)SalesOrderLineItem *OrderLineItem;
@property (strong,nonatomic)NSMutableArray *stockLotsArray;
@property (strong,nonatomic)ReturnsLineItem *returnLineItem;
@property BOOL isFromReturnsView;

@property (strong,nonatomic)NSString *wareHouseId;

@end
