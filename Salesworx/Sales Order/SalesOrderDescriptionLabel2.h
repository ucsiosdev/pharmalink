//
//  SalesOrderDescriptionLabel2.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"

@interface SalesOrderDescriptionLabel2 : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
-(NSString*)text;
-(void)setText:(NSString*)newText;
@end
