//
//  SaleWorxSalesOrderVATChargesCalculationsManager.h
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 11/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppControl.h"
#import "SalesWorxCustomClass.h"
@interface SaleWorxVATChargesCalculationsManager : NSObject
{
    NSMutableArray * VATChargesArray;
    AppControl * appControl;
}
-(SalesOrderLineItemVATCharge *)getVATChargeForProduct:(Products *)product AndCustomer:(Customer *)customer;
@end
