//
//  SalesWorxProductNameSearchPopOverViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/22/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxProductNameSearchPopOverViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
@interface SalesWorxProductNameSearchPopOverViewController ()

@end

@implementation SalesWorxProductNameSearchPopOverViewController
@synthesize enableArabicTranslation;
@synthesize popOverContentArray,titleKey,popOverSearchBar,popOverTableView,popOverController,disableSearch,popoverTableViewTopConstraint,popoverType,enableSwipeToDelete,headerTitle,filterText;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (enableSwipeToDelete==YES) {
        popOverTableView.allowsMultipleSelectionDuringEditing=NO;
    }
    
    
    // Do any additional setup after loading the view from its nib.
    filteredPopOverContentArray=[[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:titleKey];
    
    // self.navigationController.navigationBar.topItem.title = @"";
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }
    
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=closeButton;
    
    
    if (disableSearch==YES) {
        
        
        popOverSearchBar.hidden=YES;
        // popoverTableViewTopConstraint.constant=-44;
        
        [self.view layoutIfNeeded];
    }
    
    else
    {
        popOverSearchBar.hidden=NO;
        self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        self.searchController.searchResultsUpdater = self;
        self.searchController.delegate=self;
        self.searchController.dimsBackgroundDuringPresentation = NO;
        self.popOverTableView.tableHeaderView = self.searchController.searchBar;
        
        [popOverTableView reloadData];
        self.definesPresentationContext = NO;
        self.searchController.hidesNavigationBarDuringPresentation = NO;
        
        
        
    }
    filteredPopOverContentArray=[popOverContentArray mutableCopy];
    
    popOverTableView.estimatedRowHeight = 44.0;
    popOverTableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.searchController setActive:YES];
    
}
- (void)didPresentSearchController:(UISearchController *)searchController
{
    [self.searchController.searchBar becomeFirstResponder];

}

-(void)closeTapped
{
    if(popOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [popOverController dismissPopoverAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UITableView DataSource Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filteredPopOverContentArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.font=MedRepSingleLineLabelFont;
    
    cell.textLabel.textColor=MedRepDescriptionLabelFontColor;
    
    cell.textLabel.numberOfLines=2;
    
    if (self.isComingFromProductsScreen) {
        NSMutableDictionary *productDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[productDict valueForKey:@"Description"]];
    } else {
        Products *product=[filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",product.Description];
    }
    
    return cell;
    
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//        
//        if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didDeleteItematIndex:)] && enableSwipeToDelete==YES) {
//            
//            NSIndexPath *selectedIndexPath;
//            selectedIndexPath = [NSIndexPath indexPathForRow:[popOverContentArray indexOfObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]] inSection:0];
//            [filteredPopOverContentArray removeObjectAtIndex:indexPath.row];
//            [popOverContentArray removeObjectAtIndex:selectedIndexPath.row];
//            [self.salesWorxPopOverControllerDelegate didDeleteItematIndex:selectedIndexPath];
//            [popOverTableView reloadData];
//        }
//    }
//}
//
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (enableSwipeToDelete==YES) {
//        
//        return UITableViewCellEditingStyleDelete;
//    }
//    else
//    {
//        return UITableViewCellEditingStyleNone;
//        
//    }
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSIndexPath *matchedindexPath = [NSIndexPath indexPathForRow:[popOverContentArray indexOfObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]] inSection:0];
    if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectProductNameSearchPopOverController:)]) {
        
        [self.salesWorxPopOverControllerDelegate didSelectProductNameSearchPopOverController:matchedindexPath];
    }
    if(popOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [popOverController dismissPopoverAnimated:YES];
}

-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
    // [textField release] // uncomment if not using ARC
}


#pragma mark Search DisplayController methods


//- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
//
//
//
//
//    tableView.frame = CGRectMake(0, -20, self.popOverTableView.frame.size.width, self.popOverTableView.frame.size.height);
//
//}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    if(searchText.length==0)
    {
        filteredPopOverContentArray=[popOverContentArray mutableCopy];
    }
    else{
       // NSPredicate *resultPredicate = [NSPredicate
         //                               predicateWithFormat:@"SELF.Description contains[cd] %@",
           //                             searchText];
        filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:[SWDefaults fetchMultipartSearchPredicate:searchText withKey:@"Description"]] mutableCopy];
    }
     [popOverTableView reloadData];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentForSearchText:searchController.searchBar.text
                               scope:@""];
    
}
#pragma mark - UISearchDisplayController delegate methods
- (void)didDismissSearchController:(UISearchController *)searchController
{
    self.navigationController.navigationBar.translucent = NO;
    
    filteredPopOverContentArray=[popOverContentArray mutableCopy];
    [popOverTableView reloadData];
}
- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
    self.navigationController.navigationBar.translucent = YES;
}



@end
