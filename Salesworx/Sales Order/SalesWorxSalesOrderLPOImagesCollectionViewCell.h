//
//  SalesWorxSalesOrderLPOImagesCollectionViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxSalesOrderLPOImagesCollectionViewCell : UICollectionViewCell
@property (weak,nonatomic) IBOutlet UIImageView *lpoImageview;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImgView;
@end
