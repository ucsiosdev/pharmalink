//
//  FCWGalleryCollectionDetailViewController.m
//  FalconCityofWonders
//
//  Created by Unique Computer Systems on 9/24/14.
//  Copyright (c) 2014 UCS. All rights reserved.
//

#import "LPOImagesGalleryCollectionDetailViewController.h"
#import "SwipeView.h"
#import "UIImageView+AFNetworking.h"
#import "MedRepDefaults.h"
@interface LPOImagesGalleryCollectionDetailViewController ()

@end

@implementation LPOImagesGalleryCollectionDetailViewController
@synthesize imageView,image,imageURL,swipeView,imageURLLabel,selectedImageIndex,isImageFromWebservices,isPaymentImage,paymentImageObjectsArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
 
}
- (void)viewWillAppear:(BOOL)animated
{
    self.imageView.image=image;
    // NSArray *imageurlSubs=[imageURL componentsSeparatedByString:@"%2"];
    //self.imageURL=[imageurlSubs objectAtIndex:[imageurlSubs count]-1];
    
    
    imageURLLabel.lineBreakMode = NSLineBreakByCharWrapping;
    imageURLLabel.text=imageURL;
    // Do any additional setup after loading the view from its nib.
    swipeView.alignment = SwipeViewAlignmentCenter;
    swipeView.pagingEnabled = YES;
    swipeView.itemsPerPage = 1;
    swipeView.truncateFinalPage = YES;

    [swipeView scrollToItemAtIndex:[selectedImageIndex integerValue] duration:0];
    if([isImageFromWebservices isEqualToString:@"YES"])
    {
        [deleteButton setHidden:YES];
    }
    
    imagePageControl.numberOfPages=self.swipImagesUrls.count;

    if (_strTitle != nil) {
        titlelabel.text = _strTitle;
    }
    
    
    if(self.isMovingToParentViewController)
    {
        contentView.backgroundColor=UIViewControllerBackGroundColor;
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        [contentView setHidden:NO];
    }
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //generate 100 item views
    //normally we'd use a backing array
    //as shown in the basic iOS example
    //but for this example we haven't bothered
    return [self.swipImagesUrls count];
}
- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
   // NSLog(@"afdsd %@",swipeView.currentItemIndex);
   //  NSArray *imageurlSubs=[[self.swipImagesUrls objectAtIndex:self.swipeView.currentItemIndex] componentsSeparatedByString:@"%2"];
    imagePageControl.currentPage=self.swipeView.currentItemIndex;


   // imageURLLabel.text=[imageurlSubs objectAtIndex:[imageurlSubs count]-1];
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (!view)
    {
    	//load new item view instance from nib
        //control events are bound to view controller in nib file
        //note that it is only safe to use the reusingView if we return the same nib for each
        //item view, if different items have different contents, ignore the reusingView value
    	
     }
    view = [[NSBundle mainBundle] loadNibNamed:@"GalleryItemView" owner:self options:nil][0];
    NSString *imgUrl=[self.swipImagesUrls objectAtIndex:index];
    NSLog(@"index %d",index);
    
    if([isImageFromWebservices isEqualToString:@"YES"])
    {
        [_swipeItemImage setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"default_loading.png"]];
    }
    else
    {
        UIImage* galleryImg = [UIImage imageWithContentsOfFile:imgUrl];
        _swipeItemImage.image=galleryImg;
    }
    _swipeItemImage.contentMode=UIViewContentModeScaleAspectFit;
    return view;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return swipeView;
}
- (IBAction)shareImage:(id)sender {
  //  UIImage *imagetoShare=imageView.image;
    
    NSArray* dataToShare = [NSArray arrayWithObjects:[self.swipImagesUrls objectAtIndex:self.swipeView.currentItemIndex], nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    [self presentViewController:activityViewController animated:YES completion:^{}];
}
-(IBAction)DeleteButtonTapped:(id)sender
{
//    [self dismissViewControllerAnimated:YES completion:nil];
//    
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//        
//        
//    }];
    
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];

    
    [self deleteImageInContectUsImagesDir: [_swipImagesUrls objectAtIndex:self.swipeView.currentItemIndex ]];
    [_swipImagesUrls removeObjectAtIndex:self.swipeView.currentItemIndex ];

    if (_delegate != nil) {
        if (isPaymentImage) {
            
            PaymentImage *imgObj = [paymentImageObjectsArray objectAtIndex:[selectedImageIndex integerValue]];
            imgObj.image = [UIImage imageNamed:@"SalesOrder_AddImage_CameraIcon"];
            imgObj.imageID = nil;
            [paymentImageObjectsArray replaceObjectAtIndex:[selectedImageIndex integerValue] withObject:imgObj];
            [_delegate updateImagesArray:paymentImageObjectsArray];
        }
        else
        {
            [_delegate updateImagesArray:_swipImagesUrls];
        }
    }
}

-(IBAction)backButtonTapped:(id)sender

{
    
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];

}


- (void)CloseSyncPopOver
{
    //do what you need to do when animation ends...
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
    
}

-(void)deleteImageInContectUsImagesDir:(NSString *)filepath
{
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filepath error:&error];
    if (!success || error) {
        // it failed.
    }
}

@end
