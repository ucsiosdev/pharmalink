//
//  SalesWorxSalesOrderProductImageDescriptionViewController.m
//  MedRep
//
//  Created by UshyakuMB2 on 25/10/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSalesOrderProductImageDescriptionViewController.h"
#import "SWDatabaseManager.h"
#define VIEW_FOR_ZOOM_TAG (1)

@interface SalesWorxSalesOrderProductImageDescriptionViewController ()<UIScrollViewDelegate>

@end

@implementation SalesWorxSalesOrderProductImageDescriptionViewController
@synthesize selectedProduct;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    ImagesParentView.layer.borderWidth = 1;
    ImagesParentView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    NSString *description = [[SWDatabaseManager retrieveManager] getLongDescriptionForInventoryItemId:selectedProduct.Inventory_Item_ID];
    descTextView.text = description.length == 0 ? @"N/A" : description;
    
    NSMutableArray *selectedImagesArray = [[SWDatabaseManager retrieveManager]fetchProductImagesforSelectedProduct:selectedProduct];
    
    
    if (![pageControl respondsToSelector:@selector(semanticContentAttribute)] && [UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
        //pageControl.transform = CGAffineTransformMakeScale(-1, 1);
    }
    
    imageScrollView.pagingEnabled = YES;
    imageScrollView.delegate=self;
    imageScrollView.backgroundColor=[UIColor clearColor];
    imageScrollView.showsHorizontalScrollIndicator = NO;
    imageScrollView.showsVerticalScrollIndicator = NO;
    
    CGRect innerScrollFrame = imageScrollView.bounds;
    
    if (selectedImagesArray.count>0) {
        selectedProduct.productImagesArray = [[NSMutableArray alloc]initWithArray:selectedImagesArray];
        NSPredicate *defaultImagePredicate = [NSPredicate predicateWithFormat:@"SELF.Custom_Attribute_2 == 'Y'"];
        NSArray *filteredArray = [selectedProduct.productImagesArray filteredArrayUsingPredicate:defaultImagePredicate];
        
        if (filteredArray.count>0) {
            NSInteger numberOfPages = filteredArray.count;
            pageControl.numberOfPages = filteredArray.count;
            imageScrollView.contentSize = CGSizeMake(numberOfPages * imageScrollView.frame.size.width, imageScrollView.frame.size.height);
            
            for (NSInteger i = 0; i <filteredArray.count; i++) {
                ProductMediaFile *mediaFile = [filteredArray objectAtIndex:i];
                UIImage* currentImage=[UIImage imageWithContentsOfFile:mediaFile.File_Path];
                
                imageForZooming = [[UIImageView alloc] initWithImage:currentImage];
                [imageForZooming setFrame:CGRectMake(0, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
                imageForZooming.tag = VIEW_FOR_ZOOM_TAG;
                imageForZooming.contentMode=UIViewContentModeScaleAspectFit;
                imageForZooming.backgroundColor=[UIColor clearColor];
                
                pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
                pageScrollView.minimumZoomScale = 1.0f;
                pageScrollView.backgroundColor=[UIColor clearColor];
                pageScrollView.maximumZoomScale = 6.0f;
                pageScrollView.zoomScale = 1.0f;
                pageScrollView.tag=i;
                pageScrollView.contentSize = imageForZooming.bounds.size;
                pageScrollView.delegate = self;
                pageScrollView.showsHorizontalScrollIndicator = NO;
                pageScrollView.showsVerticalScrollIndicator = NO;
                [pageScrollView addSubview:imageForZooming];
                [imageScrollView addSubview:pageScrollView];
                
                if (i < numberOfPages-1) {
                    innerScrollFrame.origin.x += innerScrollFrame.size.width;
                }
            }
        }
        else {
            pageControl.numberOfPages = 1;
            imageScrollView.contentSize = CGSizeMake(imageScrollView.frame.size.width, imageScrollView.frame.size.height);
            
            ProductMediaFile *mediaFile = [selectedProduct.productImagesArray objectAtIndex:0];
            UIImage *currentImage=[UIImage imageWithContentsOfFile:mediaFile.File_Path];
            
            imageForZooming = [[UIImageView alloc] initWithImage:currentImage];
            [imageForZooming setFrame:CGRectMake(0, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
            imageForZooming.tag = VIEW_FOR_ZOOM_TAG;
            imageForZooming.contentMode=UIViewContentModeScaleAspectFit;
            imageForZooming.backgroundColor=[UIColor clearColor];
            
            pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
            pageScrollView.minimumZoomScale = 1.0f;
            pageScrollView.backgroundColor=[UIColor clearColor];
            pageScrollView.maximumZoomScale = 6.0f;
            pageScrollView.zoomScale = 1.0f;
            pageScrollView.tag=0;
            pageScrollView.contentSize = imageForZooming.bounds.size;
            pageScrollView.delegate = self;
            pageScrollView.showsHorizontalScrollIndicator = NO;
            pageScrollView.showsVerticalScrollIndicator = NO;
            [pageScrollView addSubview:imageForZooming];
            [imageScrollView addSubview:pageScrollView];
        }
    } else {
        pageControl.numberOfPages = 1;
        imageScrollView.contentSize = CGSizeMake(imageScrollView.frame.size.width, imageScrollView.frame.size.height);
        
        imageForZooming = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ProductsDefaultImage"]];
        [imageForZooming setFrame:CGRectMake(0, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        imageForZooming.tag = VIEW_FOR_ZOOM_TAG;
        imageForZooming.contentMode=UIViewContentModeScaleAspectFit;
        imageForZooming.backgroundColor=[UIColor clearColor];
        
        pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
        pageScrollView.minimumZoomScale = 1.0f;
        pageScrollView.backgroundColor=[UIColor clearColor];
        pageScrollView.maximumZoomScale = 6.0f;
        pageScrollView.zoomScale = 1.0f;
        pageScrollView.tag=0;
        pageScrollView.contentSize = imageForZooming.bounds.size;
        pageScrollView.delegate = self;
        pageScrollView.showsHorizontalScrollIndicator = NO;
        pageScrollView.showsVerticalScrollIndicator = NO;
        [pageScrollView addSubview:imageForZooming];
        [imageScrollView addSubview:pageScrollView];
    }
}

#pragma mark UIScrollView methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = imageScrollView.frame.size.width;
    float fractionalPage = imageScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    pageControl.currentPage = page;
    [imageScrollView setContentOffset: CGPointMake(imageScrollView.contentOffset.x,0)];
}

- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
