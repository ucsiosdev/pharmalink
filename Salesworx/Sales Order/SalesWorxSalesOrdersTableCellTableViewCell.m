//
//  SalesWorxSalesOrdersTableCellTableViewCell.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSalesOrdersTableCellTableViewCell.h"
#import "AppControl.h"
@implementation SalesWorxSalesOrdersTableCellTableViewCell
@synthesize xDiscountLabelWidthConstraint,xNetAmountLabelWidthConstraint,xVATChargeLabelWidthConstraint,xDiscountLabelLeadingMarginConstraint,xNetAmountLabelLeadingMarginConstraint,xVATChargeLabelLeadingMarginConstraint;
- (void)awakeFromNib {
    [super awakeFromNib];
    
    AppControl * appcontrol=[AppControl retrieveSingleton];
    // Initialization code
    if([appcontrol.HIDE_DISCOUNT_FIELDS_AND_LABELS isEqualToString:KAppControlsYESCode]){
        
        xDiscountLabelWidthConstraint.constant=0;
        xDiscountLabelLeadingMarginConstraint.constant=0;
        
        if(![appcontrol.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            xNetAmountLabelWidthConstraint.constant=0;
            xNetAmountLabelLeadingMarginConstraint.constant=0;
            self.ToatlLbl.textAlignment = NSTextAlignmentRight;
        }
    }
    
    if(![appcontrol.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
        xVATChargeLabelWidthConstraint.constant = 0;
        xVATChargeLabelLeadingMarginConstraint.constant=0;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
