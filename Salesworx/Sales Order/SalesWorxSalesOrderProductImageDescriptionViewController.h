//
//  SalesWorxSalesOrderProductImageDescriptionViewController.h
//  MedRep
//
//  Created by UshyakuMB2 on 25/10/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@interface SalesWorxSalesOrderProductImageDescriptionViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet UITextView *descTextView;
    IBOutlet UIScrollView *imageScrollView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIView *ImagesParentView;
    
    UIScrollView *pageScrollView;    
    UIImageView *imageForZooming;
}
@property (strong,nonatomic) Products *selectedProduct;

@end
