//
//  LastVisitDetailsViewController.m
//  MedRep
//
//  Created by Unique Computer Systems on 1/22/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "LastVisitDetailsViewController.h"

@interface LastVisitDetailsViewController ()

@end

@implementation LastVisitDetailsViewController
@synthesize contactNameLabel,contactNumberLabel,visitNotesTextView,selectedCustomer,XcontactDetailsViewHeightConstraint,XcontactDetailsBottomConstraint;

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Previous Visit Details"];
    
    UIBarButtonItem * closebutton = [[UIBarButtonItem alloc ]initWithTitle:NSLocalizedString(kCloseTitle,nil) style:UIBarButtonItemStylePlain target:self action:@selector(CloseButtonTapped)];
    [closebutton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:kSWX_FONT_SEMI_BOLD(14),NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = closebutton;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    AppControl *appCtrl = [AppControl retrieveSingleton];
    if ([appCtrl.SHOW_FS_VISIT_CUSTOMER_CONTACT isEqualToString:KAppControlsYESCode]) {
        XcontactDetailsViewHeightConstraint.constant=162.0f;
        XcontactDetailsBottomConstraint.constant=8.0f;
    }
    else{

        XcontactDetailsViewHeightConstraint.constant=0.0f;
        XcontactDetailsBottomConstraint.constant=0.0f;
    }
    NSString* visitNotesstring=[[SWDatabaseManager retrieveManager]fetchFieldSalesVisitNotesForCustomer:selectedCustomer];
    visitNotesTextView.text=[NSString isEmpty:[NSString getValidStringValue:visitNotesstring]]?@"N/A":[NSString getValidStringValue:visitNotesstring];

    NSMutableDictionary * contactDetailsDict=[[SWDatabaseManager retrieveManager]fetchFieldSalesVisitContactDetailsForCustomer:selectedCustomer];
    NSString* contactName=[NSString getValidStringValue:[contactDetailsDict valueForKey:@"Contact_Name"]];

    NSString* contactNumber=[NSString getValidStringValue:[contactDetailsDict valueForKey:@"Contact_Number"]];

    contactNameLabel.text=[NSString isEmpty:contactName]?@"N/A":contactName;
    
    contactNumberLabel.text=[NSString isEmpty:contactNumber]?@"N/A":contactNumber;

}
-(void)CloseButtonTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
