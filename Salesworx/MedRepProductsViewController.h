//
//  MedRepProductsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "StringPopOverViewController_ReturnRow.h"
#import <MediaPlayer/MPMoviePlayerViewController.h>
#import "ProductFilterViewController.h"
#import "MedRepProductFilterViewController.h"
#import "MedRepProductMediaDisplayViewController.h"
#import "MedRepProductImagesViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepProductVideosViewController.h"
#import "ZUUIRevealController.h"
#import "MedRepTextView.h"
#import "SalesWorxImageView.h"
@interface MedRepProductsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIPopoverControllerDelegate,StringPopOverViewController_ReturnRow,SelectedDemoPlanDelegate,FilteredProductDelegate,PDFViewDismissDelegate,ViewedImagesDelegate,ViewedPDFDelegate,ProductVideoDelegate,ZUUIRevealControllerDelegate,ViewedImagesDelegate,ProductVideoDelegate,UIDocumentInteractionControllerDelegate>

{
    NSMutableArray* filteredProducts;
    NSMutableArray* demoPlanArray;
    ProductFilterViewController* locationPopOver;
    
    NSString* popUpString;
    NSIndexPath * selectedProductIndexPath;
    IBOutlet SalesWorxImageView *productStatusIamgeView;

    
    NSMutableArray* productMediaFilesArray;
    NSMutableArray* productVideoFilesArray;
    NSMutableArray* productPdfFilesArray;
    NSMutableArray* productImagesArray;
    NSMutableArray* productHTMLFilesArray;

    NSMutableArray* productDetailsArray;

    BOOL isSearching;
    
    MPMoviePlayerViewController *moviePlayer;

    NSMutableArray * imagesPathArray;
    
    
    NSIndexPath *selectedIndexPath;
    
    NSMutableArray* collectionViewDataArray;
    UISearchBar *customSearchBar;

    IBOutlet UIView *productsHeaderView;
    
    UIImageView* blurredBgImage;
    
    NSString * selectedButton;
    
    NSInteger selectedCellIndex;
    
    NSMutableArray* selectedIndexPathArray;
    NSMutableDictionary* selectedProductDetails;
    
    NSMutableDictionary * previousFilteredParameters;

    NSString*documentsDirectory;

    
    
    IBOutlet UIImageView *productDefaultImageView;
    
    
    BOOL isFiltered;
    
    NSMutableArray* unFilteredProducts;
    
    NSCache * productImagesCache;
    NSString * thumbnailFolderPath;
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *customerViewTopConstraint;
}
@property(strong,nonatomic)    UIPopoverController *productsPopoverController;


@property(strong,nonatomic)NSMutableArray* productsArray;
@property (strong, nonatomic) IBOutlet UITableView *productsTblView;
@property (strong, nonatomic) IBOutlet UISearchBar *productSearchBar;
@property (strong, nonatomic) IBOutlet UICollectionView *productMediaCollectionView;

@property (strong, nonatomic) IBOutlet UITableView *productImagesTblView;

@property (strong, nonatomic) IBOutlet UILabel *productNameLbl;

@property (strong, nonatomic) IBOutlet UILabel *skuLbl;

@property (strong, nonatomic) IBOutlet UILabel *brandLbl;

@property (strong, nonatomic) IBOutlet UILabel *categoryLbl;

@property (strong, nonatomic) IBOutlet UILabel *typeLbl;

@property (strong, nonatomic) IBOutlet UILabel *otcRxLbl;

@property (strong, nonatomic) IBOutlet UILabel *stockLbl;

@property (strong, nonatomic) IBOutlet UITextView *productDescTxtView;
@property (strong, nonatomic) IBOutlet UIView *productImageBGView;

@property (strong, nonatomic) IBOutlet UIImageView *productImageView;

@property (strong, nonatomic) IBOutlet UIButton *imagesButton;

@property (strong, nonatomic) IBOutlet UIButton *videoButton;

@property (strong, nonatomic) IBOutlet UIButton *pptButton;

@property (weak, nonatomic) IBOutlet UIButton *btnHTML;

- (IBAction)filterButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *medRepFilterButton;


- (IBAction)filterButtonTapped:(id)sender;
- (IBAction)searchButtonTapped:(id)sender;

- (IBAction)refreshButtonTapped:(id)sender;









































@end
