//
//  GroupSectionFooterView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "GroupSectionFooterView.h"
#import "SWFoundation.h"
@implementation GroupSectionFooterView

@synthesize titleLabel;

- (id)initWithWidth:(CGFloat)width text:(NSString *)text {
    self = [self initWithFrame:CGRectMake(0, 0, width, 58)];
    
    if (self) {
        [self.titleLabel setText:text];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
        
        self.titleLabel=nil;
        [self setTitleLabel:[[UILabel alloc] initWithFrame:frame]];
        [self.titleLabel setText:@""];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:UIColorFromRGB(0x4E566C)];
        [self.titleLabel setShadowOffset:CGSizeMake(0, 1)];
        [self.titleLabel setShadowColor:[UIColor whiteColor]];
        [self.titleLabel setFont:BoldSemiFontOfSize(16.0f)];
        [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.titleLabel setNumberOfLines:0];
        [self.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        
        [self addSubview:self.titleLabel];
    }
    
    return self;
}




- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.titleLabel setFrame:CGRectMake(55, 2, self.bounds.size.width - 110, self.bounds.size.height +4)];
}
@end
