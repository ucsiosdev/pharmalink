//
//  MedRepProductVideosViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/17/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
#import "SWDatabaseManager.h"
#import <AVKit/AVKit.h>

@protocol ProductVideoDelegate <NSObject>

-(void)videosViewedinFullScreenDemo:(NSMutableArray*)imagesArray;

-(void)fullScreenVideosDidCancel;

@optional
-(void)setBorderOnLastViewedImage:(NSInteger)index;
@end

@interface MedRepProductVideosViewController : UIViewController<MFMailComposeViewControllerDelegate>


{
    AVPlayerViewController * moviePlayer;
    
    NSTimer* demoTimer;
    NSInteger demoTimeInterval;

    NSDate* playBackTime;
    
    
    IBOutlet UIImageView *moviePlayerThumbnailImageView;
    IBOutlet UIButton *emailButton;
    
    NSDate * timerDate;
    
    NSString *buttonTitle;
    NSString*documentsDirectory;

    
    IBOutlet UILabel *productTitle;
    BOOL isEmailTapped;
    
    UIImageView *fullScreenImageView;
    id videosViewedDelegate;
    
    NSIndexPath * selectedCellIndexPath;
    
    NSMutableArray* viewedVideos;
    
    NSIndexPath * selectedProductIndexPath;
    
    NSInteger selectedCellIndex;
    
    NSMutableArray* selectedIndexPathArray;
    NSString *firstFileName;
    BOOL sendEmail;

    IBOutlet UIButton *cancelButton;
    NSString *emailIDForAutoInsert;
}
@property(strong,nonatomic) SalesWorxVisit* currentVisit;
@property (strong, nonatomic) IBOutlet UIView *moviePlayerView;

@property(strong,nonatomic)NSMutableArray* productDataArray;
@property(nonatomic)    NSInteger currentImageIndex;
@property(nonatomic) BOOL isViewing;
@property(nonatomic) BOOL isFromProductsScreen;

@property(strong,nonatomic) id videosViewedDelegate;
@property (strong, nonatomic) IBOutlet UICollectionView *productVideosCollectionView;


@property(strong,nonatomic) NSMutableArray* viewedMoviesArray;


@property (strong, nonatomic) IBOutlet UIPageControl *customPageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *imagesScrollView;
@property(strong,nonatomic)NSMutableArray* productVideosArray;
@property(strong,nonatomic)NSIndexPath* selectedIndexPath;
- (IBAction)closeButtontapped:(id)sender;
@property(nonatomic) BOOL isCurrentUserDoctor;
@property(nonatomic) BOOL isCurrentUserPharmacy;
@property(strong,nonatomic) NSString *doctor_ID ;
@property(strong,nonatomic) NSString *contact_ID ;

@end
