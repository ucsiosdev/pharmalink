//
//  SWCustomerOrderHistoryDescriptionTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWCustomerOrderHistoryDescriptionTableViewCell.h"
#import "SWDefaults.h"

@implementation SWCustomerOrderHistoryDescriptionTableViewCell
@synthesize dividerImageView;

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setHideDividerImage:(BOOL)hideDividerImage
{
    if (hideDividerImage==YES) {
        dividerImageView.hidden=YES;
        self.frame=CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,kUITableViewCellHeight );
        NSLog(@"frame of cell is %@", NSStringFromCGRect(self.frame));
    }
}

-(void)setIsHeader:(BOOL)isHeader
{
    if (isHeader==YES) {
        
        CGRect sepFrame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1);
        UIView*  seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = kUITableViewSaperatorColor;
        [self addSubview:seperatorView];
    }
}
@end
