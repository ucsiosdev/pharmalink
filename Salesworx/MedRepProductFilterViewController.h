//
//  MedRepProductFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/10/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepTextField.h"
@protocol FilteredProductDelegate <NSObject>


-(void)filteredProduct:(NSMutableArray*)filteredContent;

-(void)filteredProductParameters:(NSMutableDictionary*)filterParameters;


-(void)productFilterDidCancel;


@end

@interface MedRepProductFilterViewController : UIViewController<SelectedFilterDelegate,UITextFieldDelegate>



{
    IBOutlet UIButton *typeButton;
    NSMutableDictionary * filterDict;
    
    NSMutableArray* filteredArray;
    IBOutlet UIButton *demoPlanButton;
    
    IBOutlet UIButton *categoryButton;
    id filteredProductDelegate;
    
    IBOutlet UIButton *brandButton;
    IBOutlet UIButton *uomButton;
    NSString* selectedDataString;
    
    NSString* deomPlanID;
    NSMutableArray*  demoPlanArray,*demoPlanDetailsArray;
    
    
    IBOutlet MedRepTextField *productTypeTextfield;
    IBOutlet MedRepTextField *demoPlanTextfield;
    IBOutlet MedRepTextField *UOMTextfield;
    IBOutlet MedRepTextField *productBrandTextfield;
    IBOutlet MedRepTextField *productCategoryTextfield;

    IBOutlet UISwitch *productStockSwitch;
}
@property (strong, nonatomic) IBOutlet UIView *uomView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uomViewBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uomViewHeightConstraint;
@property(strong,nonatomic)    IBOutlet NSLayoutConstraint *uomButtonHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uomLblHeightConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uomTextFieldHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uomBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uomTextFieldBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uomButtonTopConstraint;

@property(strong,nonatomic) NSMutableDictionary* previouslyFilteredContent;

@property(strong,nonatomic) id filteredProductDelegate;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property(strong,nonatomic) NSMutableArray* contentArray;
@property(strong,nonatomic) UINavigationController * navController;
@property(nonatomic) BOOL isEdetailingProduct;
- (IBAction)typeButtonTapped:(id)sender;
@end
