//
//  SalesWorxPaymentSummaryViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxPaymentSummaryViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxPaymentSummaryTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxDateHelper.h"
#import "SalesWorxPaymentSummaryPDFTemplateView.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"


@interface SalesWorxPaymentSummaryViewController ()
{
    SalesWorxDateHelper * dateHelper;
}
@end

@implementation SalesWorxPaymentSummaryViewController
@synthesize arrPaymentSummary,currentVisit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    viewFooter.backgroundColor = MedRepMenuTitleUnSelectedCellTextColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kPaymentSummaryTitle localizedCapitalizedString], nil)];
    
    dateHelper=[[SalesWorxDateHelper alloc] init];
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    
    [DateRangeTextField SetCustomDateRangeText];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
    
    lblTotalAmount.font=kSWX_FONT_SEMI_BOLD(20);
    
    
    pieChartPlaceholderImage.hidden = NO;
    parentViewOfPieChart.hidden = YES;
    
    arrCustomerName = [[NSMutableArray alloc]init];
    arrCustomersDetails = [[NSMutableArray alloc]init];
    
    NSMutableArray *customerListArray = [[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
    for (NSMutableDictionary *currentDict in customerListArray)
    {
        NSMutableDictionary *currentCustDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
        Customer * currentCustomer = [[Customer alloc] init];
        currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
        currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
        currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
        currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
        currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
        currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
        currentCustomer.City=[currentCustDict valueForKey:@"City"];
        currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
        currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
        currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
        currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
        currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
        currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
        currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
        currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
        currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
        currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
        currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
        currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
        currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
        currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
        currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
        currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
        currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
        
        [arrCustomersDetails addObject:currentCustomer];
        if ([currentCustomer.Customer_Name containsString:currentCustomer.Customer_No]) {
            [arrCustomerName addObject:[currentCustDict valueForKey:@"Customer_Name"]];
        } else {
            [arrCustomerName addObject:[[currentCustDict valueForKey:@"Customer_Name"] stringByAppendingString:[NSString stringWithFormat:@" [%@]",[currentCustDict valueForKey:@"Customer_No"]]]];
        }
    }
    
    arrPaymentType = [NSMutableArray arrayWithObjects:@"All", @"Cash",@"Current Cheque",@"PDC", nil];
    
    if (self.isMovingToParentViewController)
    {
        if(currentVisit){
            txtCustomer.text = [SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Customer_Name];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController)
    {
        customerPieChart.backgroundColor = [UIColor clearColor];
        customerPieChart.labelFont = [UIFont boldSystemFontOfSize:14];
        self.slicesForCustomerPotential = [[NSMutableArray alloc]init];
        
        customerPieChart.labelRadius = 25;
        [customerPieChart setDelegate:self];
        [customerPieChart setDataSource:self];
        [customerPieChart setPieCenter:CGPointMake(70, 65)];
        [customerPieChart setShowPercentage:YES];
        [customerPieChart setLabelColor:[UIColor whiteColor]];
        [customerPieChart setUserInteractionEnabled:NO];
        
        self.sliceColorsForCustomerPotential = [NSArray arrayWithObjects:
                                                [UIColor colorWithRed:74.0/255.0 green:145.0/255.0 blue:207.0/255.0 alpha:1],
                                                [UIColor colorWithRed:62.0/255.0 green:193.0/255.0 blue:194.0/255.0 alpha:1],
                                                nil];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            NSMutableArray *reviewDocumentsArray = [[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select A.*,IFNULL(A.Amount,'0') AS Collected_Amount,C.Customer_Name,B.Customer_No,(select sum(Pending_Amount)  from TBL_Open_Invoices  Where Customer_ID=A.Customer_ID group by Customer_ID) AS Pending_Amount from TBL_Collection AS A LEFT JOIN TBL_Customer  AS  B ON A.Customer_ID=B.Customer_ID LEFT JOIN TBL_Customer_Ship_Address as C ON B.Customer_ID = C.Customer_ID Where A.Amount is not null order by C.Customer_Name"] mutableCopy];
            arrPaymentSummary = [[NSMutableArray alloc]init];
            if (reviewDocumentsArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary * currentDict in reviewDocumentsArray) {
                    
                    NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Payment_Summary *currentCustomer = [[Payment_Summary alloc] init];
                    currentCustomer.Collected_Amount = [currentCustDict valueForKey:@"Amount"];
                    currentCustomer.Collected_On = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"yyyy-MM-dd" scrString:[currentCustDict valueForKey:@"Collected_On"]];
                    currentCustomer.Collected_OnWithTime = [currentCustDict valueForKey:@"Collected_On"];
                    currentCustomer.Collection_Type = [currentCustDict valueForKey:@"Collection_Type"];
                    currentCustomer.Collection_ID = [currentCustDict valueForKey:@"Collection_ID"];
                    currentCustomer.Cheque_No = [currentCustDict valueForKey:@"Cheque_No"];
                    currentCustomer.Cheque_Date = [currentCustDict valueForKey:@"Cheque_Date"];
                    currentCustomer.Bank_Name = [currentCustDict valueForKey:@"Bank_Name"];
                    currentCustomer.Bank_Branch = [currentCustDict valueForKey:@"Bank_Branch"];

                    currentCustomer.Collection_Ref_No = [currentCustDict valueForKey:@"Collection_Ref_No"];
                    currentCustomer.Collection_Type = [currentCustDict valueForKey:@"Collection_Type"];
                    currentCustomer.Customer_ID = [currentCustDict valueForKey:@"Customer_ID"];
                    currentCustomer.DocDate = [currentCustDict valueForKey:@"DocDate"];
                    currentCustomer.Doctype = [currentCustDict valueForKey:@"Doctype"];
                    currentCustomer.Start_Time = [currentCustDict valueForKey:@"Start_Time"];
                    currentCustomer.customerTotalPendingAmount=[currentCustDict valueForKey:@"Pending_Amount"];
                    currentCustomer.Customer_No=[currentCustDict valueForKey:@"Customer_No"];
                    currentCustomer.Customer_Name= [currentCustDict valueForKey:@"Customer_Name"];
                    [arrPaymentSummary addObject:currentCustomer];
                }
            }
            unfilteredPaymentSummaryArray = arrPaymentSummary;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(Collected_On >= %@) AND (Collected_On <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]];
                NSMutableArray* filteredArray = [[unfilteredPaymentSummaryArray filteredArrayUsingPredicate:predicate] mutableCopy];
                
                arrPaymentSummary = filteredArray;
                [self loadPieChart:arrPaymentSummary];
            });
        });
    }
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrPaymentSummary.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"reviewDocuments";
    SalesWorxPaymentSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    
    cell.lblDocumentNo.isHeader=YES;
    cell.lblDate.isHeader=YES;
    cell.lblName.isHeader=YES;
    cell.lblType.isHeader=YES;
    cell.lblAmount.isHeader=YES;
    
    cell.lblDocumentNo.text = NSLocalizedString(@"Document No", nil) ;
    cell.lblDate.text = NSLocalizedString(@"Date", nil);
    cell.lblName.text = NSLocalizedString(@"Name", nil);
    cell.lblType.text = NSLocalizedString(@"Type", nil);
    cell.lblAmount.text = NSLocalizedString(@"Amount", nil);
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"reviewDocuments";
    SalesWorxPaymentSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Payment_Summary *data = [arrPaymentSummary objectAtIndex:indexPath.row];
    cell.lblDocumentNo.text = data.Collection_Ref_No;
    cell.lblDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:data.Collected_OnWithTime];
    cell.lblName.text = data.Customer_Name;
    cell.lblType.text = data.Collection_Type;
    if ([data.Collection_Type isEqualToString:@"POST-CHQ"]) {
        cell.lblType.text = @"PDC";
    }
    cell.lblAmount.text = [[NSString stringWithFormat:@"%@",data.Collected_Amount] currencyString];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   Payment_Summary *data = [arrPaymentSummary objectAtIndex:indexPath.row];
                                  // [self GeneratePDFDocument:data];
                                   [self generateReceiptPDFDocument:data];
                               }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Payment summary report" andMessage:@"Would you like to send a receipt via email?" andActions:actionsArray withController:self];
}

#pragma  mark textfield methods
-(BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{
    if(textField == txtCustomer)
    {
        [self textFieldTapped:textField :kCustomerNamePopOverTitle :arrCustomerName :400];
        return NO;
    }
    else if(textField == DateRangeTextField)
    {
        SalesWorxReportsDateSelectorViewController * popOverVC=[[SalesWorxReportsDateSelectorViewController alloc]init];
        popOverVC.delegate=self;
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(450, 570);
        popover.delegate = self;
        popover.sourceView = DateRangeTextField.rightView;
        popover.sourceRect = DateRangeTextField.dropdownImageView.frame;
        popover.permittedArrowDirections = UIPopoverArrowDirectionLeft;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
        return NO;
    }
    else if(textField == txtPaymentType)
    {
        [self textFieldTapped:textField :kPaymentTypePopOverTitle :arrPaymentType :266];
        return NO;
    }
    return YES;
}

#pragma  mark textfield tapped method

-(void)textFieldTapped:(MedRepTextField *)txtField :(NSString *)strPop :(NSMutableArray *)arr :(CGFloat )width
{
    txtCustomer.enabled = YES;
    popString = strPop;
    
    SalesWorxPopOverViewController *popOverVC = [[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = arr;
    popOverVC.salesWorxPopOverControllerDelegate = self;
    popOverVC.disableSearch = YES;
    popOverVC.enableArabicTranslation = YES;
    
    if([popString isEqualToString:kCustomerNamePopOverTitle])
    {
        popOverVC.disableSearch = NO;
    }
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor = [UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(width, 273);
    popover.delegate = self;
    popOverVC.titleKey = popString;
    popover.sourceView = txtField.rightView;
    popover.sourceRect = txtField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    
    [self presentViewController:nav animated:YES completion:^{
    }];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popString isEqualToString:kCustomerNamePopOverTitle])
    {
        txtCustomer.text = [NSString stringWithFormat:@"%@",[arrCustomerName objectAtIndex:selectedIndexPath.row]];
    }
    else if ([popString isEqualToString:kPaymentTypePopOverTitle])
    {
        txtPaymentType.text = [NSString stringWithFormat:@"%@",[arrPaymentType objectAtIndex:selectedIndexPath.row]];
    }
}

#pragma mark Pie chart delegate methods

-(void)loadPieChart:(NSArray *)arr
{
    if ([arr count] == 0) {
        pieChartPlaceholderImage.hidden = NO;
        parentViewOfPieChart.hidden = YES;
    } else {
        pieChartPlaceholderImage.hidden = YES;
        parentViewOfPieChart.hidden = NO;
    }
    
    [_slicesForCustomerPotential removeAllObjects];
    [customerPieChart reloadData];
    [tblPaymentSummary reloadData];
    
    
    
    
    NSArray *uniqueCustomerIDs=[arrPaymentSummary valueForKeyPath:@"@distinctUnionOfObjects.Customer_ID"];
    ;
    NSNumber* invoiceAmountNumber=0;
    NSNumber* colelctedamount=0
    ;
    if(uniqueCustomerIDs.count>0){
        invoiceAmountNumber = [[[unfilteredPaymentSummaryArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Customer_ID IN %@",uniqueCustomerIDs]] valueForKey:@"customerTotalPendingAmount"] valueForKeyPath: @"@sum.self"];
        
        
        colelctedamount=[[arrPaymentSummary valueForKey:@"Collected_Amount"]  valueForKeyPath: @"@sum.self"];
        
        [_slicesForCustomerPotential removeAllObjects];
        [_slicesForCustomerPotential addObject:colelctedamount];
        [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:(invoiceAmountNumber.doubleValue-colelctedamount.doubleValue)]];
        [customerPieChart reloadData];
        lblTotalAmount.text=[colelctedamount.stringValue currencyString];
        
    }else{
        lblTotalAmount.text=[@"0" currencyString];
    }
    
    
    if ([uniqueCustomerIDs count] == 0) {
        pieChartPlaceholderImage.hidden = NO;
        parentViewOfPieChart.hidden = YES;
    } else {
        pieChartPlaceholderImage.hidden = YES;
        parentViewOfPieChart.hidden = NO;
    }
    
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    if (pieChart == customerPieChart && self.slicesForCustomerPotential.count>0) {
        count = self.slicesForCustomerPotential.count;
    }
    return count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    if (pieChart == customerPieChart) {
        count = [[NSString stringWithFormat:@"%@",[self.slicesForCustomerPotential objectAtIndex:index]]floatValue];
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == customerPieChart) {
        colour = [self.sliceColorsForCustomerPotential objectAtIndex:(index % self.sliceColorsForCustomerPotential.count)];
    }
    return colour;
}

#pragma  mark button tapped method

- (IBAction)btnSearch:(id)sender {
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    if (![txtCustomer.text isEqualToString:@"N/A"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF.Customer_Name",txtCustomer.text];
        NSMutableArray *filteredArray = [[arrCustomersDetails filteredArrayUsingPredicate:predicate] mutableCopy];
        
        if ([filteredArray count] > 0) {
            Customer *selectdCustomer = [filteredArray objectAtIndex:0];
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"Customer_ID == %d",[selectdCustomer.Customer_ID intValue]]];
        }
    }
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"(Collected_On >= %@) AND (Collected_On <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]]];
    if (![txtPaymentType.text isEqualToString:@"N/A"])
    {
        if ([txtPaymentType.text isEqualToString:@"All"]){
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Collection_Type ==[cd] 'CASH' or SELF.Collection_Type ==[cd] 'CURR-CHQ' or SELF.Collection_Type ==[cd] 'POST-CHQ' or SELF.Collection_Type ==[cd] 'PDC'"]];
        }
        else if ([txtPaymentType.text isEqualToString:@"Cash"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Collection_Type ==[cd] 'CASH'"]];
        }
        else if ([txtPaymentType.text isEqualToString:@"Current Cheque"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Collection_Type ==[cd] 'CURR-CHQ'"]];
        }
        else if ([txtPaymentType.text isEqualToString:@"PDC"]){
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Collection_Type ==[cd] 'PDC' OR SELF.Collection_Type ==[cd] 'POST-CHQ'"]];
        }
    }
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [predicate description]);
    
    NSMutableArray* filteredArray = [[unfilteredPaymentSummaryArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    if (filteredArray.count == 0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
    }
    arrPaymentSummary = filteredArray;
    [self loadPieChart:arrPaymentSummary];
}
- (IBAction)btnReset:(id)sender {
    txtCustomer.text = @"N/A";
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kDateFormatWithoutTime];
    [df setLocale:KUSLOCALE];
    
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    DateRangeTextField.SelecetdCustomDateRangeText = nil;
    [DateRangeTextField SetCustomDateRangeText];
    txtPaymentType.text = @"N/A";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(Collected_On >= %@) AND (Collected_On <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]];
    NSMutableArray* filteredArray = [[unfilteredPaymentSummaryArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    arrPaymentSummary = filteredArray;
    [self loadPieChart:arrPaymentSummary];
}
#pragma mark date picker method

-(void)DidUserSelectDateRange:(NSDate *)StartDate EndDate:(NSDate *)EndDate AndCustomRangePeriodText:(NSString *)customPeriodText{
    
    DateRangeTextField.StartDate = StartDate;
    DateRangeTextField.EndDate = EndDate;
    DateRangeTextField.SelecetdCustomDateRangeText = customPeriodText;
    
    [DateRangeTextField SetCustomDateRangeText];
    
    NSLog(@"StartDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate]);
    NSLog(@"EndDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]);
}

#pragma mark email pdf

-(void)GeneratePDFDocument:(Payment_Summary *)selectedReport{
    
    if ([MFMailComposeViewController canSendMail])
    {
        self.navigationController.navigationBar.userInteractionEnabled = NO;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *documentsDirectory=[SWDefaults applicationDocumentsDirectory];
            [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:nil];
            
            // get the image path
            NSString *documentPathComponent=[NSString stringWithFormat:@"PaymentSummary-%@.pdf",selectedReport.Customer_No];
            NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
            
            // make sure the file is removed if it exists
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if([fileManager fileExistsAtPath:filename]) {
            }
            
            NSMutableData *pdfData = [NSMutableData data];
            UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
            

            
            SalesWorxPaymentSummaryPDFTemplateView * PageTemplate =[[SalesWorxPaymentSummaryPDFTemplateView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
            PageTemplate.selectedPaymentSummary = selectedReport;
            [PageTemplate UpdatePdfPgae];
            UIGraphicsBeginPDFPage();
            CGContextRef pdfContext = UIGraphicsGetCurrentContext();
            [PageTemplate.layer renderInContext:pdfContext];
            
            
            UIGraphicsEndPDFContext();
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                self.navigationController.navigationBar.userInteractionEnabled = YES;
                
                MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
                mailController.mailComposeDelegate = self;
                [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
                [mailController setSubject:[NSString stringWithFormat:@"Payment Summary - %@",selectedReport.Customer_Name]];
                
                
                if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode] ){
                    
                    NSString *appUserEmailAddress = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:selectedReport.Customer_ID];
                    [mailController setToRecipients:@[appUserEmailAddress]];
                }
                
                
                [mailController setMessageBody:@"Hi,\n Please find the attached payment summary." isHTML:NO];
                [mailController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"PaymentSummary-%@.pdf",selectedReport.Customer_No]];
                [self presentViewController:mailController animated:YES completion:nil];
            });
        });
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
    }
}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Payment receipt methods
-(int)getUserProfileNumberOfDecimalsStr{
    return [[[SWDefaults userProfile]valueForKey:@"Decimal_Digits"] intValue];
}
-(void)generateReceiptPDFDocument:(Payment_Summary *)currentPaymentSummary{
    
    
    NSMutableDictionary * currentCustomerDictionary = [[NSMutableDictionary alloc]init];
    [currentCustomerDictionary setValue:currentPaymentSummary.Customer_Name forKey:@"Customer_Name"];
    [currentCustomerDictionary setValue:currentPaymentSummary.Customer_No forKey:@"Customer_No"];
    
    NSMutableDictionary * bankDetailsDictionary = [[NSMutableDictionary alloc]init];
    [bankDetailsDictionary setValue:currentPaymentSummary.Bank_Name forKey:@"Bank_Name"];
    [bankDetailsDictionary setValue:currentPaymentSummary.Bank_Branch forKey:@"Bank_Branch"];
    [bankDetailsDictionary setValue:currentPaymentSummary.Cheque_No forKey:@"Cheque_No"];
    [bankDetailsDictionary setValue:currentPaymentSummary.Cheque_Date forKey:@"Cheque_Date"];
    
    
    CustomerPaymentClass * currentPaymentDetails = [[CustomerPaymentClass alloc]init];
    currentPaymentDetails.totalAmount=[NSDecimalNumber decimalNumberWithString:currentPaymentSummary.Amount];
    currentPaymentDetails.customerPaidAmount=[NSDecimalNumber decimalNumberWithString:[SWDefaults getValidStringValue:currentPaymentSummary.Collected_Amount]];
    currentPaymentDetails.paymentMethod = currentPaymentSummary.Collection_Type;
    

    NSMutableArray* invoicesArray = [[SWDatabaseManager retrieveManager]fetchInvoicesforReportsPaymentReceipt:currentPaymentSummary.Collection_ID];
    
    NSString *documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    // get the image path
    NSString *documentPathComponent=[NSString stringWithFormat:@"Payment Receipt-%@.pdf",currentPaymentSummary.Customer_No];
    NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
    }
    
    NSInteger numberOfPages = (invoicesArray.count%15) == 0 ? (invoicesArray.count/15) : (invoicesArray.count/15)+1;
    if (numberOfPages == 0) {
        //in case if the invoice settlement is disabled pdf is not getting generated because number of pages is linked to settles invoices count, so if settled invoices count is 0 setting it to 1
        numberOfPages = 1;
    }
    NSMutableData *pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
    for (NSInteger i=0; i< numberOfPages ; i ++) {
        SalesWorxPaymentCollectionReceiptPDFTemplateView * PageTemplate =[[SalesWorxPaymentCollectionReceiptPDFTemplateView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
        PageTemplate.numberOfPages=numberOfPages;
        PageTemplate.currentPaymentSummary=invoicesArray[i];
        PageTemplate.selectedCustomer=currentCustomerDictionary;
        PageTemplate.invoicesArrayInReports=invoicesArray;
        PageTemplate.isFromReports = YES;
        PageTemplate.pageNumber =i+1;
        [PageTemplate UpdatePdfPgae];
        
        UIGraphicsBeginPDFPage();
        CGContextRef pdfContext = UIGraphicsGetCurrentContext();
        [PageTemplate.layer renderInContext:pdfContext];
    }
    
    UIGraphicsEndPDFContext();
    /** Saving pdf in documents Directory*/
    //    if(pdfData !=nil && NO == [pdfData writeToFile:filename atomically:YES]) {
    //        [NSException raise:@"PDF Save Failed" format:@"Unable to store image %@", [NSString stringWithFormat:@"CustomerSatement-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]]];
    //    }else{
    //
    //    }
    //emailButton.title = NSLocalizedString(@"Email", nil);
    if ([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        [mailController setSubject:[NSString stringWithFormat:@"Payment Receipt - %@",currentPaymentSummary.Customer_Name]];
        
        if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
            
            NSString *appUserEmailAddress = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:currentPaymentSummary.Customer_ID];
            [mailController setToRecipients:@[appUserEmailAddress]];
        }
        
        
        [mailController setMessageBody:@"Hi,\n Please find the attached statement." isHTML:NO];
        [mailController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"Payment Receipt-%@.pdf",currentPaymentSummary.Customer_No]];
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        //[self EnableNavigationBarActions];
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
