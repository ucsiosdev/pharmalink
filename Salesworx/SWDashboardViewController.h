//
//  SWDashboardViewController.h
//  SWDashboard
//
//  Created by Irfan Bashir on 5/6/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "BEMSimpleLineGraphView.h"
#import "SWAppDelegate.h"
#import <GAITracker.h>
#import <GAI.h>
#import <Google/Analytics.h>

#import "SalesWorxFieldSalesDashboardViewController.h"

@protocol DashboardViewDeleget <NSObject>
@optional
- (void)DashboardViewDelegateGetData;
@end

@interface SWDashboardViewController : SWViewController <UIPopoverControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate>
{
    id < DashboardViewDeleget > delegate;

    NSMutableArray * salesTrendArray;
    
    UIButton * customerDetailsButton;
    UIImageView *mainImage;
    UILabel *plannedLabel;
    UILabel *completedLabel;
    UILabel *outOfRouteLabel;
    UILabel *customerStatusLabel;
    UILabel *customerOutstandingLabel;
    UILabel *customerSalesLabel;
    UILabel *customerNameLabel;
    UILabel *orderLabel;
    UILabel *returnLabel;
    UILabel *collectionLabel;
    UILabel *syncDateLabel;
    UILabel *syncStatusLabel;
    UILabel *monthLabel;    
    NSMutableDictionary *customerDict;
    NSMutableArray *customerArray;
    UIButton *selectCustomerButton;
    UIButton *detailCustomerButton;
    UIPopoverController *datePickerPopOver;
   // PCLineChartView *lineChartView;
   // PCLineChartViewComponent *component;
    SWAppDelegate *appDel;
    UIPickerView *sortPickerView;
    
    NSString *dateString;
    BOOL isFirst;
    BOOL isVisitAvailble;
    
    NSInteger selectedCustomerIndex;
    
    BOOL previousSelectionAvailable;
    
    NSMutableDictionary* selectedCustomerDict;
    UIBarButtonItem *toDoListButton;
}

@property (unsafe_unretained) id <DashboardViewDeleget> delegate;

@property (strong, nonatomic) IBOutlet BEMSimpleLineGraphView *myGraph;
@property (strong, nonatomic) NSMutableArray *ypoints;
@property (strong, nonatomic) NSMutableArray *xPoints;


@end
