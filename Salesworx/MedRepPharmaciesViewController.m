//
//  MedRepPharmaciesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepPharmaciesViewController.h"
#import "ZUUIRevealController.h"
#import "MedRepTableViewCell.h"
#import "SWAppDelegate.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "MedRepUpdatedDesignCell.h"
#import "MedRepDoctorLocationsUpdatedTableViewCell.h"
#import "MedRepPharmaciesFilterViewController.h"
#import "MedRepPopOverBackgroundView.h"
#import "MedrepPharmacyLocationsTableViewCell.h"
#import "MedRepEdetailTaskViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepDoctorsLastVisitDetailsPopOverViewController.h"
#import "MedRep-Swift.h"

@interface MedRepPharmaciesViewController ()

@end

@implementation MedRepPharmaciesViewController


@synthesize pharmaciesTablevIew,pharmaciesSearchBar,pharmacyNameLbl,pharmacyIDLbl,pharmacyStatusLbl,emirateLbl,contactNumberLbl,emailAddressLbl,classLbl,otcRXLbl,tradeChannelLbl,locationsTblView,pharmaciesMapView,visitsRequiredLbl,visitsCompletedLbl,pharmaciesArray,pharmacyLocationsArray,locationAddressTxtView,blurredBgImage,blurMask, lastVisitedOnLbl;


-(void)viewDidAppear:(BOOL)animated
{
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    self.lastVisitedOnLbl.textColor=LastVisitedOnTextColor;
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(66.0/255.0) green:(126.0/255.0) blue:(196.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    indexPathArray=[[NSMutableArray alloc]init];
    
    unFilteredPharmaciesArray=[[NSMutableArray alloc]init];
    
    pharmacyContactsArray=[[NSMutableArray alloc]init];
    
    customSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0, 298, 44)];
    
    customSearchBar.hidden=YES;
    customSearchBar.delegate=self;
    
    pharmaciesArray=[[NSMutableArray alloc]init];
    pharmacyLocationsArray=[[NSMutableArray alloc]init];
    filteredPharmacies=[[NSMutableArray alloc]init];

    appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSLog(@"current location from app delegate is %0.2f", appDel.currentLocation.coordinate.latitude);
    

    
    // Create Annotation point
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = appDel.currentLocation.coordinate;

    // add annotation to mapview
    [pharmaciesMapView addAnnotation:Pin];
    

    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (appDel.currentLocation.coordinate, 800, 800);
    [pharmaciesMapView setRegion:regionCustom animated:YES];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)closeSplitView

{
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
}


-(void)onKeyboardHide:(NSNotification *)notification
{
  
//    [pharmaciesSearchBar setShowsCancelButton:NO animated:YES];
//    
//    if (isSearching==YES) {
//        
//    }
//    
//    else
//    {
//        [self searchBarCancelButtonClicked:pharmaciesSearchBar];
//        [pharmaciesTablevIew reloadData];
//        [self deselectPreviousCellandSelectFirstCell];
//        
//    }
//    
//    if ([filterPopOverController isPopoverVisible]==YES) {
//
//    
//    }

}

-(void)willRevealCalled
{
    
    NSLog(@"will reveal notification method called");
    
    [pharmaciesSearchBar setShowsCancelButton:NO animated:YES];
    
    if (isSearching==YES) {
        
    }
    
    else
    {
        [self searchBarCancelButtonClicked:pharmaciesSearchBar];
    }
}


-(void)onKeyboardShow:(NSNotification *)notification
{
    
//    [pharmaciesSearchBar becomeFirstResponder];
//    
//    [pharmaciesSearchBar setShowsCancelButton:YES animated:YES];
//    
//    if ([filterPopOverController isPopoverVisible]==YES) {
//       
//       
//    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"WillRevealNotification" object:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    visitsRequiredLbl.font=kSWX_FONT_SEMI_BOLD(44);
    visitsCompletedLbl.font=kSWX_FONT_SEMI_BOLD(44);
    
    
    locationIndexArray =[[NSMutableArray alloc]init];
    
    pharmaciesTablevIew.delegate=self;
    pharmaciesTablevIew.rowHeight = UITableViewAutomaticDimension;
    pharmaciesTablevIew.estimatedRowHeight = 70.0;
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingPharmacies];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRevealCalled) name:@"WillRevealNotification" object:nil];
    
    
    //test predicate
    //
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    
    
    [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
            
            taskPopOverButton= [[SalesWorxPopOverNavigationBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"TaskIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnTask)];

            notePopOverButton=[[SalesWorxPopOverNavigationBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NoteIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnNote)];
            
            self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:notePopOverButton,taskPopOverButton, nil];
        }
    }
    
    NSMutableArray* unsortedArray=[MedRepQueries fetchPharmaciesData];

    if (unsortedArray.count>0) {
  
        unFilteredPharmaciesArray=unsortedArray;
        
        pharmacyContactsArray=[MedRepQueries fetchPharmacieswithContacts];

        pharmaciesArray=unsortedArray;
    }
    

    
    if (pharmaciesArray.count>0) {

        [locationsTblView reloadData];
        
        [self deselectPreviousCellandSelectFirstCell];
    }
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Pharmacies"];
    
    
    
    pharmaciesMapView.layer.borderColor = UITextFieldDarkBorderColor.CGColor;
    pharmaciesMapView.layer.borderWidth = 1.0;
    pharmaciesMapView.layer.cornerRadius = 3.0;
    
    
    contactsTblView.layer.borderColor = UITextFieldDarkBorderColor.CGColor;
    contactsTblView.layer.borderWidth = 1.0;
    contactsTblView.layer.cornerRadius = 3.0;

    
    locationAddressTxtView.layer.borderColor = UITextFieldDarkBorderColor.CGColor;
    locationAddressTxtView.layer.borderWidth = 1.0;
    locationAddressTxtView.layer.cornerRadius = 2.0;
    
    for (UIView *borderView in self.view.subviews) {
        
        if ([borderView isKindOfClass:[UIView class]]) {

            if (borderView.tag==1000 || borderView.tag==1001||borderView.tag==1003||borderView.tag==1004||borderView.tag==1005||borderView.tag==1010) {
                //
                borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                borderView.layer.shadowOffset = CGSizeMake(2, 2);
                borderView.layer.shadowOpacity = 0.1;
                borderView.layer.shadowRadius = 1.0;
            }
        }
    }
    
    [self hideNoSelectionView];
}
    
-(void)selectFirstCellinContactsTableView
{
    if ([contactsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedrepPharmacyLocationsTableViewCell *cell = (MedrepPharmacyLocationsTableViewCell *)[pharmaciesTablevIew cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        if (cell) {
            
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [contactsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];

        [contactsTblView.delegate tableView:contactsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

-(void)deselectPreviousCellandSelectFirstCell
{
    if ([pharmaciesTablevIew.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[pharmaciesTablevIew cellForRowAtIndexPath:selectedPharmacyIndexPath];
        
        if (cell) {
            
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [pharmaciesTablevIew selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        [pharmaciesTablevIew.delegate tableView:pharmaciesTablevIew didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}


-(void)populatePharmacyrDescriptionData:(NSInteger)index
{
    visitsCompletedLbl.textColor=[UIColor redColor];
    
    NSMutableArray* pharmacyDescDetails;
    
    
    if (isSearching==YES  && filteredPharmacies.count>0) {
        pharmacyDescDetails=[filteredPharmacies objectAtIndex:index];
    }
    else if (isSearching==NO  && pharmaciesArray.count>0) {
        
        pharmacyDescDetails=[pharmaciesArray objectAtIndex:index];
    }
    
    else if (pharmaciesArray.count>0 && isFiltered==YES)
    {
        pharmacyDescDetails=[pharmaciesArray objectAtIndex:index];
    }
    
    pharmacyNameLbl.text=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Location_Name"]];
    
    if([NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Location_Code"]].length>0)
        pharmacyIDLbl.text=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Location_Code"]];
    else
    {
        pharmacyIDLbl.text=@"N/A";
    }
    
    
    NSString* pharmacyStatusStr=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Is_Active"]];
    
    if (pharmacyStatusStr.length>0 && [pharmacyStatusStr isEqualToString:@"Active"]) {
        [pharmacyStatusImageView setBackgroundColor:[UIColor greenColor]];
        
    }
    else
    {
        [pharmacyStatusImageView setBackgroundColor:[UIColor redColor]];
    }
    
    
    //fetch data from PHX_TBL_Contacts
    
    NSMutableArray* contactDetailsArray=[MedRepQueries fetchContactDetaislwithLocationID:[pharmacyDescDetails valueForKey:@"Location_ID"]];
    
    NSLog(@"pharmacy contact detaisl array is %@", [contactDetailsArray description]);
    
    if (contactDetailsArray.count>0)
    {
        
        contactName = [contactDetailsArray valueForKey:@"Contact_Name"];
        emailAddressLbl.text=[NSString stringWithFormat:@"%@", [contactDetailsArray valueForKey:@"Email"]];
        
        emirateLbl.text=[NSString stringWithFormat:@"%@", [contactDetailsArray valueForKey:@"City"]];
        
        contactNumberLbl.text=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Phone"]];
        
        
    }
    
    pharmacyStatusLbl.text=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Is_Active"]];
    classLbl.text=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Class"]];
    otcRXLbl.text=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"OTCRX"]];
    tradeChannelLbl.text=[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"TradeChannel"]];
    visitsCompletedLbl.text=@"0";
    visitsRequiredLbl.text=@"0";
    
    NSString* pharmacyAddress=[[NSString stringWithFormat:@"%@\n", [pharmacyDescDetails valueForKey:@"Address_1"]]stringByAppendingString:[NSString stringWithFormat:@"%@", [pharmacyDescDetails valueForKey:@"Address_2"]]];
    if(pharmacyAddress.length>0)
    {
        locationAddressTxtView.text=pharmacyAddress;
    }
    else
    {
        locationAddressTxtView.text=@"N/A";
        
    }
    
    
    /** last visited on*/
    NSMutableArray* visitsCompletedArray=[MedRepQueries fetchCompltedVisitsforCurrentMonthforPharmacy:[pharmacyDescDetails valueForKey:@"Location_ID"]];
    
    NSString* visitEndDateStr=[NSString stringWithFormat:@"%@", [[visitsCompletedArray objectAtIndex:0] valueForKey:@"Visit_End_Date"]];
    
    NSString* refinedLastVisitedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:visitEndDateStr];
  
    if (refinedLastVisitedDate.length>0) {
        self.lastVisitedOnLbl.text=refinedLastVisitedDate;
    }

    /** completed visits*/
    NSNumber * visitsCompleted=[MedRepQueries fetchNumberOFVisitsCompletedForaPharmcyInCurrentMonth:[pharmacyDescDetails valueForKey:@"Location_ID"]];

    visitsCompletedLbl.text=[NSString stringWithFormat:@"%@",visitsCompleted];
    
    
    /* required visits*/
    NSNumber * visitsReq=[NSNumber numberWithInt:0];
    visitsRequiredLbl.text=@"0";


    if (visitsCompleted.integerValue<visitsReq.integerValue) {
        
        visitsCompletedLbl.textColor=[UIColor redColor];
    }
    else if (visitsCompleted.integerValue>=visitsReq.integerValue) {
        visitsCompletedLbl.textColor=[UIColor greenColor];
    }
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView DataSource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES && tableView==pharmaciesTablevIew ) {
        
        if (filteredPharmacies.count>0) {
            return filteredPharmacies.count;
        } else {
            return 0;
        }
    }
    else {
        if (tableView==pharmaciesTablevIew) {
            
            if (pharmaciesArray.count>0) {
                return pharmaciesArray.count;
            } else {
                return 0;
            }
        }
        else {
            if (pharmacyLocationsArray.count>0) {
                return pharmacyLocationsArray.count;
            } else {
                return 0;
            }
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==pharmaciesTablevIew) {
        
        return UITableViewAutomaticDimension;
    }
    else {
        return 70.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==pharmaciesTablevIew) {
        
        static NSString* identifier=@"updatedDesignCell";
        
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        
        if (isSearching && !NoSelectionView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([indexPathArray containsObject:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
        
        if (isSearching==YES && filteredPharmacies.count>0) {
            
            cell.titleLbl.text=[[NSString stringWithFormat:@"%@", [[filteredPharmacies valueForKey:@"Location_Name"] objectAtIndex:indexPath.row]] capitalizedString];
            cell.descLbl.text=[[NSString stringWithFormat:@"%@", [[filteredPharmacies valueForKey:@"Address_1"] objectAtIndex:indexPath.row]] capitalizedString];
        }
        
        else {
            cell.titleLbl.text=[[NSString stringWithFormat:@"%@", [[pharmaciesArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row]] capitalizedString];
            
            cell.descLbl.text=[[NSString stringWithFormat:@"%@", [[pharmaciesArray valueForKey:@"Address_1"] objectAtIndex:indexPath.row]]capitalizedString];
        }
        return cell;
    }
    
    else
    {
        static NSString* identifier=@"contactsCell";
        
        MedrepPharmacyLocationsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedrepPharmacyLocationsTableViewCell" owner:nil options:nil] firstObject];
            
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.separatorInset = UIEdgeInsetsZero;
        
        if ([locationIndexArray containsObject:indexPath]) {
            
            [cell setSelectedCellStatus];
        }
        else{
            
            [cell setDeSelectedCellStatus];
        }
        
        if (pharmacyLocationsArray.count>0) {
            cell.titleLbl.text=[NSString stringWithFormat:@"%@", [[pharmacyLocationsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]];
            
            
            NSString* contactPhoneNumber=[NSString stringWithFormat:@"%@", [[pharmacyLocationsArray objectAtIndex:indexPath.row] valueForKey:@"Phone"]];
            
            if ([contactPhoneNumber isEqualToString:@""]) {
                
                cell.descLbl.text=@"N/A";
            }
            
            else
            {
                cell.descLbl.text=[NSString stringWithFormat:@"%@", [[pharmacyLocationsArray objectAtIndex:indexPath.row] valueForKey:@"Phone"]];
            }
        }

        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedPharmacyIndexPath=indexPath;

    if (tableView==pharmaciesTablevIew) {
       
        [self hideNoSelectionView];
        
        [self showDefaultValuesForPharmacyLables];

        [self populatePharmacyrDescriptionData:indexPath.row];
        
        if (indexPathArray.count==0) {
            
            [indexPathArray addObject:indexPath];
        }
        
        else {
            
            indexPathArray=[[NSMutableArray alloc]init];
            [indexPathArray addObject:indexPath];
        }

        NSString* pharmacyLocationID;
        NSString* pharmacyLatStr;
        NSString* pharmacyLongStr;
        
        if (isSearching==YES && filteredPharmacies.count>0)
        {
            selectedVisitDetailsDict = [filteredPharmacies objectAtIndex:indexPath.row];
            pharmacyLocationID=[NSString stringWithFormat:@"%@", [[filteredPharmacies objectAtIndex:indexPath.row] valueForKey:@"Location_ID"]];
            pharmacyLatStr=[NSString stringWithFormat:@"%@",[[filteredPharmacies objectAtIndex:indexPath.row] valueForKey:@"Latitude"]];
            
            pharmacyLongStr=[NSString stringWithFormat:@"%@",[[filteredPharmacies objectAtIndex:indexPath.row] valueForKey:@"Longitude"]];
            
            NSLog(@"coordinates for selected Pharmacy %@ %@", pharmacyLatStr,pharmacyLongStr);
        }
        else if(isSearching==NO && pharmaciesArray.count>0)
        {
            selectedVisitDetailsDict = [pharmaciesArray objectAtIndex:indexPath.row];
            pharmacyLocationID=[NSString stringWithFormat:@"%@", [[pharmaciesArray objectAtIndex:indexPath.row] valueForKey:@"Location_ID"]];
            pharmacyLatStr=[NSString stringWithFormat:@"%@",[[pharmaciesArray objectAtIndex:indexPath.row] valueForKey:@"Latitude"]];
            pharmacyLongStr=[NSString stringWithFormat:@"%@",[[pharmaciesArray objectAtIndex:indexPath.row] valueForKey:@"Longitude"]];
            
            NSLog(@"coordinates for selected Pharmacy %@ %@", pharmacyLatStr,pharmacyLongStr);
            
        }
        
        [self showLastVisitDetails:[selectedVisitDetailsDict valueForKey:@"Location_ID"]];
        
        //use predicate

        NSPredicate * contactsPredicate=[NSPredicate predicateWithFormat:@"SELF.Location_ID == [cd] %@",pharmacyLocationID];
        
        pharmacyLocationsArray=[[pharmacyContactsArray filteredArrayUsingPredicate:contactsPredicate] mutableCopy];
        
        if (pharmacyLocationsArray.count>0) {
            
            //set map coords
            
            pharmacyCoords.latitude=[pharmacyLatStr doubleValue];
            pharmacyCoords.longitude=[pharmacyLongStr doubleValue];
            
            [self addAnnotationforLocation:pharmacyCoords];

            [contactsTblView reloadData];
        }
        else
        {
            [contactsTblView reloadData];
        }
        
        [pharmaciesTablevIew reloadData];
    }
    
    if (tableView==contactsTblView) {

        if (locationIndexArray.count==0) {
            [locationIndexArray addObject:indexPath];
        }
        else
        {
            [locationIndexArray replaceObjectAtIndex:0 withObject:indexPath];
        }
        
        [contactsTblView reloadData];
    }
}



#pragma MapView Delegate
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    NSLog(@"annotation selected");
    
       BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    NSString* googleMapsUrlString;
    
    
    if (canHandle) {
        // Google maps installed
        
        if (CLLocationCoordinate2DIsValid(pharmacyCoords)) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,pharmacyCoords.latitude,pharmacyCoords.longitude];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            
            [[UIApplication sharedApplication] openURL:googleMapsUrl];
        }
    }
    
    else
    {
        if (CLLocationCoordinate2DIsValid(pharmacyCoords)) {
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,pharmacyCoords.latitude,pharmacyCoords.longitude];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        
        else {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
    }
}

#pragma mark Search Bar Methods

-(void)removeKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (IBAction)filterButtonTapped:(id)sender
{
    [self removeKeyBoardObserver];

       
    /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
    
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];

    [self.view addSubview:blurredBgImage];
    
    UIButton* searchBtn=(id)sender;

    if (indexPathArray.count>0) {
        [indexPathArray replaceObjectAtIndex:0 withObject:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
   
    pharmaciesSearchBar.text=@"";

    [self.view endEditing:YES];
    [pharmaciesSearchBar setShowsCancelButton:NO animated:YES];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    [pharmaciesTablevIew reloadData];
    
    [self deselectPreviousCellandSelectFirstCell];

    MedRepPharmaciesFilterViewController * popOverVC=[[MedRepPharmaciesFilterViewController alloc]init];
    
    popOverVC.contentArray=unFilteredPharmaciesArray;
    popOverVC.pharmacyFilterDelegate=self;
    if (previousFilteredParameters.count>0) {
        
        popOverVC.previousFilteredParameters=previousFilteredParameters;
    }
    popOverVC.filterNavController=self.navigationController;
    popOverVC.filterTitle=@"Filter";
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    
    
    
    filterPopOverController=nil;
    filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    filterPopOverController.delegate=self;
    popOverVC.filterPopOverController=filterPopOverController;
    
    
    [filterPopOverController setPopoverContentSize:CGSizeMake(366, 430) animated:YES];
    
    
    CGRect btnFrame=searchBtn.frame;
    
    btnFrame.origin.x=btnFrame.origin.x+8;

    CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];

    [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


- (IBAction)searchButtonTapped:(id)sender {
    
    customSearchBar.hidden=NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [UIView animateWithDuration:0.25 animations:^{
        customSearchBar.frame = CGRectMake(8, 48, 300, 44);
        [self.view addSubview:customSearchBar];
    }];
}


#pragma mark Search delegate methods

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isSearching=NO;
    [self hideNoSelectionView];
    
    [pharmaciesSearchBar setText:@""];
    
    self.lastVisitedOnLbl.text=@"N/A";
    [self.view endEditing:YES];
    
    if ([searchBar isFirstResponder]) {
        
        [searchBar resignFirstResponder];
    }
    
    [searchBar setShowsCancelButton:NO animated:YES];
    

    if (isFiltered==YES) {
        
    } else {
        [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    }
    
    if (pharmaciesArray.count>0) {
        [pharmaciesTablevIew reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {

    [pharmaciesSearchBar setShowsCancelButton:YES animated:YES];
    
    if ([searchBar isFirstResponder]) {
        
    } else {
        [searchBar becomeFirstResponder];
    }

    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {

    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        
        isSearching = YES;
        [self searchContent];
    }
    else {
        [self hideNoSelectionView];
        
        isSearching = NO;
        [pharmaciesTablevIew reloadData];
        
        if (searchText.length==0) {
            if ([pharmaciesTablevIew.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
                
                [pharmaciesTablevIew selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            [pharmaciesTablevIew.delegate tableView:pharmaciesTablevIew didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {

    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    
    isSearching=YES;
    [searchBar setShowsCancelButton:NO animated:YES];
}

-(void)searchContent
{
    NSString *searchString = pharmaciesSearchBar.text;
    NSLog(@"searching with text in medrep pharmacies %@",searchString);

    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate* predicateTest = [NSPredicate predicateWithFormat:filter, @"Location_Name", [NSString stringWithFormat:@"%@", searchString]];
    
    filteredPharmacies=[[pharmaciesArray filteredArrayUsingPredicate:predicateTest] mutableCopy];

    if (filteredPharmacies.count>0) {
        [pharmaciesTablevIew reloadData];
        
//        [self selectFirstCell];
    }
    
    else if(filteredPharmacies.count==0)
    {
        isSearching=YES;
        [pharmaciesTablevIew reloadData];
//        [pharmacyLocationsArray removeAllObjects];
//        [contactsTblView reloadData];
//        [self resetPharmaciesData];
    }
}

-(void)resetPharmaciesData
{
    pharmacyNameLbl.text=@"";
    emirateLbl.text=@"";
    pharmacyIDLbl.text=@"";
    emailAddressLbl.text=@"";
    contactNumberLbl.text=@"";
    tradeChannelLbl.text=@"";
    otcRXLbl.text=@"";
    self.lastVisitedOnLbl.text=@"";
    locationAddressTxtView.text=@"";
    
}

#pragma mark Add Annotation Method

-(void)addAnnotationforLocation:(CLLocationCoordinate2D)coord
{
    // Define pin location
    
    
    
    // Create Annotation point
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    
    if (CLLocationCoordinate2DIsValid(coord)) {
        Pin.coordinate=coord;
    }
    else
    {
        
        Pin.coordinate = appDel.currentLocation.coordinate;
        //Pin.title = @"Annotation Title";
        //Pin.subtitle = @"Annotation Subtitle";
    }
    
    
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (Pin.coordinate, 800, 800);
    [pharmaciesMapView setRegion:regionCustom animated:NO];
    
    [pharmaciesMapView removeAnnotations:[pharmaciesMapView annotations]];
    
    
    [pharmaciesMapView addAnnotation:Pin];
    
    // add annotation to mapview
}

#pragma mark Blur Effect methods

- (UIImage *)takeSnapshotOfView:(UIView *)view
{
    CGFloat reductionFactor = 1;
    UIGraphicsBeginImageContext(CGSizeMake(view.frame.size.width/reductionFactor, view.frame.size.height/reductionFactor));
    [view drawViewHierarchyInRect:CGRectMake(0, 0, view.frame.size.width/reductionFactor, view.frame.size.height/reductionFactor) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)blurWithImageEffects:(UIImage *)image
{
    return [image applyBlurWithRadius:2 tintColor:[UIColor colorWithWhite:0 alpha:0] saturationDeltaFactor:1 maskImage:nil];
}

- (UIImage *)blurWithCoreImage:(UIImage *)sourceImage
{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@30 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.view.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, self.view.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
    CGContextFillRect(outputContext, self.view.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}

-(void)selectFirstCell
{
    if (pharmaciesArray.count>0 && isSearching==NO) {
        
        if ([pharmaciesTablevIew.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
            
            [pharmaciesTablevIew selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                             animated:YES
                                       scrollPosition:UITableViewScrollPositionNone];
        
        [pharmaciesTablevIew.delegate tableView:pharmaciesTablevIew didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [locationsTblView reloadData];
    }
    
    else if (filteredPharmacies.count>0 && isSearching==YES)
    {
        if ([pharmaciesTablevIew.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
            
            [pharmaciesTablevIew selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                             animated:YES
                                       scrollPosition:UITableViewScrollPositionNone];
        
        [pharmaciesTablevIew.delegate tableView:pharmaciesTablevIew didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [locationsTblView reloadData];

    }
    else if (pharmaciesArray.count>0 && isFiltered==YES)
        
    {
        if ([pharmaciesTablevIew.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
            
            [pharmaciesTablevIew selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                             animated:YES
                                       scrollPosition:UITableViewScrollPositionNone];
        
        [pharmaciesTablevIew.delegate tableView:pharmaciesTablevIew didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [locationsTblView reloadData];
    }
}

#pragma mark Pharmacy Filter Delegate Methods


-(void)filteredPharmacyParameters:(NSMutableDictionary*)parameters
{
    [self addKeyBoardObserver];
    
    self.view.alpha=1.0;
    
    if ([self.view.subviews containsObject:blurredBgImage]) {
        [blurredBgImage removeFromSuperview];
    }
    
    previousFilteredParameters=parameters;
}

-(void)filteredPharmacyDetails:(NSMutableArray*)filteredPharmacyArray
{
    [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];

    isFiltered=YES;

    pharmaciesArray=filteredPharmacyArray;
    
    [pharmaciesTablevIew reloadData];
    
    [blurredBgImage removeFromSuperview];
    
    [filterPopOverController dismissPopoverAnimated:YES];
    
    [self selectFirstCell];
}


-(void)pharmacyFilterDidReset
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [self addKeyBoardObserver];
    
    self.view.alpha=1.0;
    
    if ([self.view.subviews containsObject:blurredBgImage]) {
        [blurredBgImage removeFromSuperview];
        
    }
    
    [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    
    [filterPopOverController dismissPopoverAnimated:YES];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    pharmaciesArray=unFilteredPharmaciesArray;
    
    [pharmaciesTablevIew reloadData];
    
    [self selectFirstCell];
}

-(void)pharmacyFilterDidCancel
{
    [self addKeyBoardObserver];
    
    [blurredBgImage removeFromSuperview];
    [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];

    
    [filterPopOverController dismissPopoverAnimated:YES];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    pharmaciesArray=unFilteredPharmaciesArray;

    
    [pharmaciesTablevIew reloadData];
    
    [self selectFirstCell];
}

#pragma mark Button Action
-(void)btnTask
{
    [self.view endEditing:YES];

//    NSString* locationType = [selectedVisitDetailsDict valueForKey:@"Location_Type"];
//    NSMutableArray *arr = [self fetchContactIDWithLocationID];
    
    [selectedVisitDetailsDict setValue:@"" forKey:@"Contact_ID"];
    currentVisitTasksArray=[self fetchTaskObjectswithLocationID:[selectedVisitDetailsDict valueForKey:@"Location_ID"]];

    currentVisitTasksArray=[MedRepQueries fetchTaskObjectswithLocationID:[selectedVisitDetailsDict valueForKey:@"Location_ID"] andContactID:@""];

    if (pharmaciesArray.count>0)
    {
        popOverController=nil;
        
        MedRepEdetailTaskViewController * edetailTaskVC = [[MedRepEdetailTaskViewController alloc]init];
        edetailTaskVC.visitDetailsDict = selectedVisitDetailsDict;
        edetailTaskVC.delegate=self;
        edetailTaskVC.dashBoardViewing = YES;
        edetailTaskVC.tasksArray = currentVisitTasksArray;
        
        UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
        
        popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
        [popOverController setDelegate:self];
        [popOverController setPopoverContentSize:CGSizeMake(376, 600) animated:YES];
        edetailTaskVC.tasksPopOverController=popOverController;
       
        [popOverController presentPopoverFromRect:[taskPopOverButton.sourceButton bounds] inView:taskPopOverButton.sourceButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:kDataUnavailable andMessage:@"Task data unavailable" withController:self];
    }
}
-(void)btnNote
{
    [self.view endEditing:YES];
    if (pharmaciesArray.count>0)
    {
        popOverController=nil;
        
        MedRepEDetailNotesViewController * edetailTaskVC=[[MedRepEDetailNotesViewController alloc]init];
        [selectedVisitDetailsDict setValue:@"" forKey:@"Contact_ID"];
        edetailTaskVC.visitDetailsDict=selectedVisitDetailsDict;
        edetailTaskVC.dashBoardViewing=YES;
        
        UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
        popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
        [popOverController setDelegate:self];
        [popOverController setPopoverContentSize:CGSizeMake(376, 550) animated:YES];
        
        edetailTaskVC.notesPopOverController=popOverController;
        [popOverController presentPopoverFromRect:[notePopOverButton.sourceButton bounds] inView:notePopOverButton.sourceButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:kDataUnavailable andMessage:@"Notes data unavailable" withController:self];
    }
}
-(NSMutableArray*)fetchContactIDWithLocationID
{
    NSString* locationsQuery=[NSString stringWithFormat:@"select Contact_ID from PHX_TBL_Contacts where Contact_Name = '%@'",contactName];
    
    NSMutableArray* locationsData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationsQuery];
    if (locationsData.count>0)
    {
        return locationsData;
    }
    else
    {
        return nil;
    }
}

-(NSMutableArray*)fetchTaskObjectswithLocationID:(NSString*)locationID
{
    NSString* taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where  Location_ID= '%@' and  Is_Active='Y' and Is_Deleted='N' ",locationID];
    
    NSMutableArray* taskDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* taskObjectsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    if (taskObjectsArray.count>0) {
        
        VisitTask * task;
        
        for (NSInteger i=0; i<taskObjectsArray.count; i++) {
            
            task=[[VisitTask alloc]init];
            NSMutableDictionary * currentDict=[taskObjectsArray objectAtIndex:i];
            
            task.Title=[currentDict valueForKey:@"Title"];
            task.Task_ID=[currentDict valueForKey:@"Task_ID"];
            task.Description=[currentDict valueForKey:@"Description"];
            task.Category=[currentDict valueForKey:@"Category"];
            task.Priority=[currentDict valueForKey:@"Priority"];
            task.Start_Time=[currentDict valueForKey:@"Start_Time"];
            task.Status=[currentDict valueForKey:@"Status"];
            task.Assigned_To=[currentDict valueForKey:@"Assigned_To"];
            task.Created_At=[currentDict valueForKey:@"Created_At"];
            task.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            task.Show_Reminder=[currentDict valueForKey:@"Show_Reminder"];
            
            
            [taskDataArray addObject:task];
        }
        if (taskDataArray.count>0) {
            return taskDataArray;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}
#pragma Mark Task popover controller delegate

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray
{
    currentVisitTasksArray = tasksArray;

    if (tasksArray.count>0)
    {
        NSPredicate * updatedTasksPredicate = [NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
        NSMutableArray *updatedTasksArray = [[tasksArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
        NSLog(@"updated tasks are %@", updatedTasksArray);
        
        if (updatedTasksArray.count>0)
        {
            NSString *documentDir;
            if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                documentDir = [paths objectAtIndex:0];
            }
            else
            {
                NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
                documentDir = [filePath path] ;
            }
            
            FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
            [database open];
            BOOL updatedTasksSuccessfully;
            
            NSString* assignedTo=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
            NSString* userID=[[SWDefaults userProfile]valueForKey:@"User_ID"];
            NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
            
            
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++)
            {
                VisitTask * currentTask=[updatedTasksArray objectAtIndex:i];
                
                NSLog(@"current task details in pharmacies %@",currentTask.Contact_ID);
                

                @try {
                    
                    updatedTasksSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Tasks (Task_ID,Title, Description,Category,Priority,Start_Time, Status,Assigned_To,Emp_ID, Location_ID,Show_Reminder,IS_Active,Is_deleted,Created_At,Created_By,Doctor_ID,Contact_ID,End_Time,Last_Updated_At,Last_Updated_By,Custom_Attribute_1)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", currentTask.Task_ID, currentTask.Title==nil?@"":currentTask.Title, currentTask.Description==nil?@"":currentTask.Description, currentTask.Category==nil?@"":currentTask.Category, currentTask.Priority==nil?@"":currentTask.Priority, currentTask.Start_Time, currentTask.Status==nil?@"":currentTask.Status,assignedTo, assignedTo,  [NSString isEmpty:currentTask.Location_ID]?[NSNull null]:currentTask.Location_ID, @"N",@"Y",@"N", currentTask.Start_Time, createdBy, [NSNull null],[NSString isEmpty:currentTask.Contact_ID]?[NSNull null]: currentTask.Contact_ID, currentTask.End_Time, currentTask.Start_Time, userID, @"IPAD",nil];
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
            }
            
            //after updating in visit addl info update the status in task table as well.
            
            BOOL updatetaskStatus=NO;
            
            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [formatterTime setLocale:usLocaleq];
            NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++)
            {
                VisitTask * task=[updatedTasksArray objectAtIndex:i];
                @try
                {
                    updatetaskStatus =  [database executeUpdate:@"Update  PHX_TBL_Tasks set Status = ?, Last_Updated_At=?, Last_Updated_By=?  where Task_ID='%@'",task.Status,createdAt,createdBy,task.Task_ID, nil];
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    [database close];
                }
            }
        }
    }
    [self updateStatusforTasksandNotes];
}

#pragma mark Task and Notes Status Update

-(void)updateStatusforTasksandNotes
{
    NSString* locationType=[selectedVisitDetailsDict valueForKey:@"Location_Type"];
    NSString* locationID=[selectedVisitDetailsDict valueForKey:@"Location_ID"];
    
    NSInteger  taskCount=0;
    NSInteger notesCount=0;
    
    if ([locationType isEqualToString:@"P"])
    {
        NSString* contactID=[selectedVisitDetailsDict valueForKey:@"Contact_ID"];
        NSMutableArray* tasksArray=[MedRepQueries fetchTaskObjectswithLocationID:locationID andContactID:@""];
        if (tasksArray.count>0)
        {
            for (NSInteger i=0; i<tasksArray.count; i++)
            {
                NSMutableDictionary * currentDict=[tasksArray objectAtIndex:i];
                NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:taskDueDate];
                
                
                BOOL testDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                
                if (testDate==YES)
                {
                    NSLog(@"RED RED TASKS OVER DUE");
                    [selectedVisitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                }
                else
                {
                    NSLog(@"YELLOW");
                    [selectedVisitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                }
            }
        }

        notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andContactID:contactID];
        NSLog(@"task count : %d notes count : %d",taskCount,notesCount);
        
        if (notesCount>0)
        {
            [selectedVisitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
        }
        else
        {
            [selectedVisitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
        }
    }
}

-(void)showDefaultValuesForPharmacyLables
{
    pharmacyNameLbl.text=KNotApplicable;
    emirateLbl.text=KNotApplicable;
    pharmacyIDLbl.text=KNotApplicable;
    emailAddressLbl.text=KNotApplicable;
    contactNumberLbl.text=KNotApplicable;
    tradeChannelLbl.text=KNotApplicable;
    otcRXLbl.text=KNotApplicable;
    self.lastVisitedOnLbl.text=KNotApplicable;
    locationAddressTxtView.text=@"";
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    customerViewTopConstraint.constant = 8;
}
-(void)showNoSelectionView
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    customerViewTopConstraint.constant = 1000;
}

-(void)showLastVisitDetails:(NSString*)selectedLocationID
{
    lastVisitDetailsDict = [MedRepQueries fetchLastVisitDetailsforLocation:selectedLocationID];
    NSLog(@"last visit details data is %@",lastVisitDetailsDict);

    NSString* lastVisitOnString=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Last_Visited_On"]];
    if (![NSString isEmpty:lastVisitOnString]) {
        NSString* refinedLastVisitedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:lastVisitOnString];
        lastVisitedOnLbl.text=[NSString getValidStringValue:refinedLastVisitedDate];
    }
}

- (IBAction)lastVisitDetailsButton:(id)sender {
    
    
    NSLog(@"dict is %@",selectedVisitDetailsDict);
    NSString* locationID = [NSString getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Location_ID"]];
    
    if([[AppControl retrieveSingleton].FM_ENABLE_VISIT_HISTORY_REPORT isEqualToString:KAppControlsYESCode]){
        
        FMVisitHistoryViewController * visitHistoryVC =  [[FMVisitHistoryViewController alloc]init];
        CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
        NSError *error;
        NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
        NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
        
        NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
        NSURL *url = [NSURL URLWithString:strurl];
        NSString *strParams =[[NSString alloc] init];
        NSString *strName=@"sync_MC_ExecGetVisitHistory";
        NSString* username = [SWDefaults username];
        NSString* password = [SWDefaults password];
        NSString* userID = [[SWDefaults userProfile]valueForKey:@"SalesRep_ID"];
        NSMutableDictionary* paramatersDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:userID,@"EmpID",@"P",@"Type",locationID,@"DoctorContactID", nil];
        NSArray* valuesArray = paramatersDict.allValues;
        NSArray* keysArray = paramatersDict.allKeys;
        
        
        for (NSInteger i=0; i<paramatersDict.count; i++) {
            strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[keysArray objectAtIndex:i]];
            strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[valuesArray objectAtIndex:i]];
            
        }
        NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
        NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"ORDER_HISTORY_PARAMETERS_DICT"];

        NSString *requestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_Pharmacy_VisitHistory",[testServerDict stringForKey:@"name"],strName,strParams];
        
        NSDictionary* orderHistoryDict = [[NSDictionary alloc]initWithObjectsAndKeys:url,@"SalesWorx_Order_History_URL",requestString,@"SalesWorx_Order_History_Paramater_String", nil];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:orderHistoryDict] forKey:@"ORDER_HISTORY_PARAMETERS_DICT"];

        [self.navigationController pushViewController:visitHistoryVC animated:YES];
    }
    else{
        MedRepDoctorsLastVisitDetailsPopOverViewController * popOverVC=[[MedRepDoctorsLastVisitDetailsPopOverViewController alloc]init];
        popOverVC.lastVisitDetailsDict=lastVisitDetailsDict;
        popOverVC.isVisitScreen = false;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(410, 380);
        popover.delegate = self;
        popover.sourceView = lastVisitDetailsButton.superview;
        popover.sourceRect = lastVisitDetailsButton.frame;
        popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
        [self presentViewController:nav animated:YES completion:^{
        
    }];
    }
}

@end
