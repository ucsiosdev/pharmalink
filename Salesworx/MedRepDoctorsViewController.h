//
//  MedRepDoctorsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "SWAppDelegate.h"
#import "MedRepQueries.h"
#import <CoreImage/CoreImage.h>
#import "FXBlurView.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepDoctorFilterViewController.h"
#import <MapKit/MapKit.h>
#import "MedRepCreateDoctorViewController.h"
#import "MedRepSearchPopOverViewController.h"
#import <MessageUI/MessageUI.h>
#import "JCRBlurView.h"
#import "ZUUIRevealController.h"
#import "SalesWorxImageView.h"
#import "SalesWorxImageBarButtonItem.h"
#import "SalesWorxPopOverNavigationBarButtonItem.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepDoctorsLastVisitDetailsPopOverViewController.h"


@interface MedRepDoctorsViewController : SWViewController<MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,createDoctorDelegate,UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate,DismissPopoverDelegate,StringPopOverViewController_ReturnRow,FilteredDoctorDelegate,MFMailComposeViewControllerDelegate,ZUUIRevealControllerDelegate>


{
    SWAppDelegate* appDel;
    UIBlurEffect *blurEffect;
    UIVisualEffectView *blurEffectView;
    NSMutableArray* selectedDoctorData;

    BOOL isSearching;
    
    NSMutableArray* filteredDoctors;
    
    Doctors * doctor;
    NSString* lastVisitedAtStr;
    NSString* lastVisitAtLocationID;
    NSString* lastVisitConclusionNotes;
    UIButton * taskButton;
    
    UIPopoverController * popOverController,*filterPopOverController;
    NSMutableDictionary * lastVisitDetailsDict;
    
    UIPopoverController * searchBarPopOverController;
    
    NSString* popUpString;
    
    NSMutableArray* filterByArray;
    
    StringPopOverViewController_ReturnRow * stringPopOverCustomController;
    
    UIView* baseView;
    
    UIBarButtonItem* addDoctorButton;
    
    IBOutlet UIView *tblHeaderView;
    UISearchBar *customSearchBar;
    
    CLLocationCoordinate2D  doctorLocationCoord;
    
    NSMutableDictionary* selectedDoctorDict;
    
    IBOutlet SalesWorxImageView *doctorStatusImageView;
    
    NSIndexPath *selectedDoctorIndexPath;
    
    
    
    NSMutableArray* indexPathArray;

    NSMutableArray* indexPathLocationsArray;
    
    NSMutableDictionary* previousFilteredParameters;

    
    BOOL isFiltered;
    
    BOOL comingFromWillAppear;
    
    
    NSMutableArray* filteredContentArray;
    
    NSMutableArray* unfilteredDoctorObjects;
    

    BOOL noSerchResults;
    
    NSMutableArray * currentVisitTasksArray;
    NSMutableDictionary* selectedVisitDetailsDict;
        
    UIButton * tempButton;
    
    SalesWorxPopOverNavigationBarButtonItem * taskPopOverButton;
    SalesWorxPopOverNavigationBarButtonItem * notePopOverButton;

    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *customerViewTopConstraint;
    
    IBOutlet MedRepElementDescriptionLabel * doctorPersonalitystyleLabel;
    IBOutlet MedRepElementDescriptionLabel * doctorAdoptionstyleLabel;

    IBOutlet NSLayoutConstraint *xDoctorTypeStringLabelLeadingMarginToSuperViewContsraint;
    IBOutlet NSLayoutConstraint *xDoctorClassLabelLeadingMarginToSuperViewConstraint;
    IBOutlet MedRepElementTitleLabel *ClassificationStringLabel;
    IBOutlet UIButton *ClassificationDetailsButton;
    IBOutlet MedRepElementDescriptionLabel *DoctorClassificationLabel;
    IBOutlet UIButton *lastVisitDetailsButton;
}

- (IBAction)lastVisitDetailsButtonTapped:(id)sender;


@property UIView *blurMask;
@property UIImageView *blurredBgImage;

- (IBAction)DoctorClassificationButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *statusImageView;
- (IBAction)filterButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *medRepFilterButton;

@property (strong, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) IBOutlet UITableView *doctorsTblView;
@property (strong, nonatomic) IBOutlet UISearchBar *doctorsSearchBar;
@property (strong, nonatomic) IBOutlet UILabel *doctorNameLbl;

@property (strong, nonatomic) IBOutlet UILabel *doctorSpecializationLbl;
@property (strong, nonatomic) IBOutlet UILabel *contactNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *emailAddressLbl;
@property (strong, nonatomic) IBOutlet UILabel *tradeChannelLbl;

@property (strong, nonatomic) IBOutlet UILabel *classLbl;
@property (strong, nonatomic) IBOutlet UILabel *otcRxLbl;

@property (strong, nonatomic) IBOutlet UILabel *idLbl;

@property (strong, nonatomic) IBOutlet UILabel *statusLbl;
@property (strong, nonatomic) IBOutlet UITableView *doctorLocationTblView;

@property (strong, nonatomic) IBOutlet MKMapView *locationMapView;

@property (strong, nonatomic) IBOutlet UILabel *requiredVisitsLbl;
@property (strong, nonatomic) IBOutlet UILabel *visitsCompletedLbl;

@property (strong, nonatomic) IBOutlet UILabel *lastVisitedOnLbl;

@property (strong, nonatomic) IBOutlet UILabel *lastVisitedAtLbl;

@property (nonatomic,strong) JCRBlurView *blurView;

@property(strong,nonatomic)NSMutableArray* doctorsArray,*doctorLocationsArray;




































@end
