//
//  SWDuesView.m
//  SWCustomer
//
//  Created by Irfan on 12/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDuesView.h"
#import "ProductSaleCategoryViewController.h"



@implementation SWDuesView




- (id)initWithCustomer:(NSDictionary *)customerDict;

{
    self = [super init];
    if (self) {
 
        customer = [NSMutableDictionary dictionaryWithDictionary:customerDict] ;
        
        
       
        
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    
    //customerSer.delegate = self;
    

    //routeSer.delegate=self;
    
    
    NSLog(@"dues view frame is %@", NSStringFromCGRect(self.view.frame));
    
    [self getVisitServiceDidGetDues:[[SWDatabaseManager retrieveManager] dbGetDuesStaticWithCustomerID:[customer stringForKey:@"Customer_ID"]]];
        self.view.backgroundColor = [UIColor whiteColor];
    self.title=@"Customer Dues";
    customerHeaderView =[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:customer] ;
    [customerHeaderView setShouldHaveMargins:YES];
    [self.view addSubview:customerHeaderView];

    
    duesTableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 60, 1000, 600) style:UITableViewStyleGrouped] ;
    duesTableView.delegate = self;
    duesTableView.dataSource = self;
    duesTableView.backgroundView = nil;
    duesTableView.backgroundColor = [UIColor whiteColor];

    
    NSLog(@"dues table frame is %@", NSStringFromCGRect(duesTableView.frame));
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        duesTableView.cellLayoutMarginsFollowReadableWidth = NO;
        
        
    }
    
    
    [self.view addSubview:duesTableView];
    
    noteLable = [[UILabel alloc]initWithFrame:CGRectMake(10, 620, self.view.frame.size.height-20, 50)] ;
    noteLable.backgroundColor = [UIColor clearColor];
    noteLable.textColor = [UIColor redColor];
    noteLable.numberOfLines = 2;
    noteLable.text=@"Note: All new orders for the customer would be held for approval on the back office";
    [noteLable setTextAlignment:NSTextAlignmentCenter];
    noteLable.font = RegularFontOfSize(18.0);
    [self.view addSubview:noteLable];
    
    if([[SWDefaults availableBalance] intValue] < 0)
    {
        noteLable.hidden = NO;
    }
    else {
        noteLable.hidden= NO;
    }   
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Return to Visit Options" style:UIBarButtonItemStyleDone target:self action:@selector(stopVisit:)] ];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Continue Sales Order" style:UIBarButtonItemStyleDone target:self action:@selector(continueVisit:)] ];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [Flurry logEvent:@"Dues  View"];
    //[Crittercism leaveBreadcrumb:@"<Dues View>"];


}

#pragma mark UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    return 11;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }
    

    
        if(indexPath.row==0)
        {
            [cell.textLabel setText:@"30 Days"];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"M0_Due"]];
        }
        else  if(indexPath.row==1)
        {
            [cell.textLabel setText:@"60 Days"];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"M1_Due"]];
        }  
        else  if(indexPath.row==2)
        {
            [cell.textLabel setText:@"90 Days"];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"M2_Due"]];
        }  
        else  if(indexPath.row==3)
        {
            [cell.textLabel setText:@"120 Days"];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"M3_Due"]];
        }  
        else  if(indexPath.row==4)
        {
            [cell.textLabel setText:@"150 Days"];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"M4_Due"]];
        }  
        else  if(indexPath.row==5)
        {
            [cell.textLabel setText:NSLocalizedString(@"PDC Due", nil)];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"PDC_Due"]];
        }  
        else  if(indexPath.row==6)
        {
            [cell.textLabel setText:@"Prior Due"];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"Prior_Due"]];
        }  
        else  if(indexPath.row==7)
        {
            [cell.textLabel setText:@"Total Due"];
            [cell.detailTextLabel setText:[duesDict currencyStringForKey:@"Total_Due"]];
        } 
        else  if(indexPath.row==8)
        {
            [cell.textLabel setText:@"Credit Period"];
            //[cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ Days",[SWDefaults Credit_Period] ]];
            [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ Days",[customer stringForKey:@"Bill_Credit_Period"]]];
        }  
        else  if(indexPath.row==9)
        {
            [cell.textLabel setText:NSLocalizedString(@"Credit Limit", nil)];
            [cell.detailTextLabel setText:[[customer stringForKey:@"Credit_Limit"]currencyString ]];
        }  
        else  if(indexPath.row==10)
        {
            [cell.textLabel setText:NSLocalizedString(@"Available Balance", nil)];
            [cell.detailTextLabel setText:[[SWDefaults availableBalance] currencyString]];
        } 
    
    
    cell.textLabel.font=LightFontOfSize(14.0f);
    cell.detailTextLabel.font=LightFontOfSize(14.0f);
    return cell;
}
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
  
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:@"Customer Dues"] ;
        return sectionHeader;        
    
    
}

- (CGFloat)tableView:(UITableView *)tv heightForHeaderInSection:(NSInteger)section {
    return 48.0f;
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma Dues View Actions

- (void)continueVisit:(id)sender
{
    ProductSaleCategoryViewController *productSaleCategoryViewController = [[ProductSaleCategoryViewController alloc] initWithCustomer:customer] ;
    [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
    
}

- (void)stopVisit:(id)sender
{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
    
}
- (void)getVisitServiceDidGetDues:(NSArray *)routes
{
    if([routes count]>0)
    {
        duesItems = [NSArray arrayWithArray:routes  ] ;
        duesDict = [NSMutableDictionary dictionaryWithDictionary:[duesItems objectAtIndex:0]] ;
        //customerSer.delegate=self;

       // [SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[self.customer stringForKey:@"Customer_ID"]];
        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customer stringForKey:@"Customer_ID"]]];

        
    }
    else
    {
        
    }
    routes=nil;
}

//- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{


    [SWDefaults setCustomer:customer];
    customer = [SWDefaults validateAvailableBalance:orderAmmount];

    [duesTableView reloadData];
    orderAmmount=nil;
}


@end
