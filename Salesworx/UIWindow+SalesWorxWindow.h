//
//  UIWindow+SalesWorxWindow.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/22/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (SalesWorxWindow)
- (UIViewController *)visibleViewController;
@end
