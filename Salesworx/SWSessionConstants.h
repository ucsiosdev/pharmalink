//
//  SWSessionConstants.h
//  SWSession
//
//  Created by Irfan Bashir on 5/6/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#ifndef SWSession_SWSessionConstants_h
#define SWSession_SWSessionConstants_h

#define kSessionLoginDoneNotification   @"SessionLoginDone"
#define kSessionLogoutNotification      @"SessionLogout"
#define kSessionActivationErrorNotification      @"ActivationError"

#endif
