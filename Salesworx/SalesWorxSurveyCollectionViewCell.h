//
//  SalesWorxSurveyCollectionViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 5/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxSurveyCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UILabel *captionLbl;
@property (strong, nonatomic) IBOutlet UIImageView *captionBackGroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *surveyNumber;
@property (strong, nonatomic) IBOutlet UILabel *statusLbl;
@property (strong, nonatomic) IBOutlet UIImageView *surveyImage;


@end
