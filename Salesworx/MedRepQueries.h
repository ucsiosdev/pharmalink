//
//  MedRepQueries.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWDatabaseManager.h"
#import "FMDBHelper.h"
#import "SWDefaults.h"
#import "MedRepCustomClass.h"

#import "SWAppDelegate.h"
#import "MedRepCustomClass.h"

@interface MedRepQueries : NSObject


+(NSMutableArray*)fetchDoctorsData;
+(NSMutableArray*)fetchDoctorsDataWhenPendingVisitsEnabled;

+(NSMutableArray*)fetchCitiesData;
+(NSString*)fetchDoctorDetails:(NSString*)doctorID;
+(NSMutableArray*)fetchLocationDetails:(NSString*)locationID loactionType:(NSString*)locationType;

+(NSString*)fetchAppCodeValueforProductType:(NSString*)productType;

+(NSString*)fetchContactwithContactID:(NSString*)contactID;


+(NSMutableArray*)fetchDoctorLocations:(NSString*)doctorID;
+(NSMutableArray*)fetchPharmaciesData;
+(NSMutableArray*)fetchLocationsforPharmacy:(NSString*)pharmacyLocationID;
+(NSMutableArray*)fetchProductsData;
+(BOOL)insertDoctors:(NSMutableDictionary*)doctorsData;
+(BOOL)isProductActive:(NSString*)productID;
+(NSMutableArray*)fetchMediaFilesforProduct:(NSString*)productID;
+(NSMutableArray*)fetchProductDetails:(NSString*)productID;
+(NSString*)fetchDocumentsDirectory;
+(NSMutableArray*)fetchTaskAppCodes:(NSString*)code;
+(BOOL)insertTask:(NSMutableDictionary*)taskData;
+(NSMutableArray*)fetchTaskswithLocationID:(NSString*)locationID andcontactID:(NSString*)contactID;
+(NSString*)fetchModifiedDate:(NSInteger)selectedValue;



+(NSMutableArray*)fetchLocations;
+(NSMutableArray*)fetchDemoPlansforSelectedDate:(NSDate*)selectedDate;

+(NSMutableArray*)fetchDoctorswithLocationID:(NSString*)locationID;
+(NSMutableArray*)fetchDoctorswithLocationIDWhenPendingVisitsEnabled:(NSString*)locationID;

+(BOOL)createVisit:(NSMutableDictionary*)visitDetailsDict;
+(NSMutableArray*)fetchPlannedVisits;
+(NSMutableArray*)fetchPlannedVisitswithDate:(NSString*)visitDate withSelection:(NSInteger)selectedValue;
+(NSMutableArray*)fetchContactsforPharmacywithLocationID:(NSString*)locationID;
+(NSMutableArray*)fetchContactsforPharmacywithLocationIdWhenPendingVisitsEnabled:(NSString*)locationID;

+(NSMutableArray*)fetchPlannedVisitsforNextDay:(NSString*)visitDate;

+(NSMutableArray*)fetchPlannedVisitswithDate:(NSString*)date;
+(NSString*)fetchDoctorwithID:(NSString*)doctorID;

+(NSMutableArray*)fetchVisitDetailsForDoctorwithDoctorID:(NSString*)doctorID visitID:(NSString*)visitID;


+(BOOL)rescheduleVisitwithVisitDetails:(NSMutableDictionary*)visitDetailsDict;
+(NSString*)fetchLocationNamewithID:(NSString*)locationID locationType:(NSString*)locationType;



+(NSMutableArray*)fetchDemoPlanforProducts:(NSString*)date;
+(NSMutableArray*)fetchProductIDforSelectedDemoPlan:(NSString*)demoPlanID;
+(NSMutableArray*)fetchProductDetaislwithMediaFileID:(NSString*)mediaFileID;
+(NSString*)fetchAppCOdeforProductType:(NSString*)productType;


+(NSString*)fetchDemoPlanwithID:(NSString*)demoPlanID;

+(NSString*)fetchLocationTypewithLocationID:(NSString*)locationID;

+(NSMutableArray*)fetchVisitDetailsfForPharmacywithLocationID:(NSString*)locationID visitID:(NSString*)visitID;

+(BOOL)createLocationWithDetails:(NSMutableDictionary*)locationDetails;

+(BOOL)insertDoctorLocationMapwithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID;




+(NSMutableArray*)fetchLocationData;
+(NSString*)fetchDateStringofDemoPlan:(NSString*)fieldName;
+(NSMutableArray*)fetchMediaFilesforDemoPlanID:(NSString*)demoPlanID;
+(NSMutableArray*)fetchMediaFileDetailswithMediaFileID:(NSString*)mediaFileID;

+(NSString*)fetchDatabaseDateFormat;


+(NSMutableArray*)fetchSamplesforEdetailing:(NSString *)productType;
+(NSMutableArray *)fetchEmpStockwithEmpID:(NSString*)empID ProductID:(NSString*)productID;


+(NSMutableArray*)fetchCoordinatesforLocationID:(NSString*)locationID;


+(BOOL)saveVisitDetails:(NSMutableDictionary*)visitDetails withProductsDemonstrated:(NSMutableArray*)productsDemonstrated andSamplesGiven:(NSMutableArray*)samplesGiven withAdditionalInfo:(NSMutableDictionary*)additionalInfo andVisitDetails:(VisitDetails*)currentVisitDetails;

+(NSMutableArray *)fetchProductswithStock;

+(NSMutableArray*)fetchContactDetaislwithLocationID:(NSString*)locationID;



+(NSMutableArray*)fetchAccompaniedByData;

+(BOOL)InsertNotes:(NSMutableDictionary*)notesDetails andVisitDetails:(NSMutableDictionary*)visitDetailsDict;


+(NSMutableArray*)fetchNoteswithVisitID:(NSString*)visitID;

+(NSMutableArray*)fetchNoteswithLocationID:(NSString*)locationID andContactID:(NSString*)contactID;
+(NSString*)fetchCurrentDateTimeinDatabaseFormat;
+(NSString*)fetchNextDateFromCurrentDateTimeinDatabaseFormat;
+(NSDate*)convertNSStringtoNSDate:(NSString*)dateString;

+(NSMutableArray*)fetchCompltedVisitsforCurrentMonthforPharmacy:(NSString*)locationID;
+(NSMutableArray*)fetchNoteswithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID;

+(NSMutableArray*)fetchPlannedVisitsforSelectedDate:(NSString*)selectedDate;

+(NSMutableArray*)fetchVisitEndDateFromActualVisits:(NSString*)plannedVisitID;
+(NSMutableArray*)fetchVisitsRequiredwithClass:(NSString*)classStr;
+(NSMutableArray*)fetchCompltedVisitsforCurrentMonth:(NSString*)locationID;
+(NSMutableArray*)fetchVisitCompletionData:(NSString*)doctorID;

+(NSString*)fetchVisitCompletionStatuswithVisitID:(NSString*)plannedVisitID;

+(BOOL)updateStockQuantity:(NSMutableArray*)productsArray;

+(NSString*)createVisitandReturnPlannedVisitID:(NSMutableDictionary*)visitDetailsDict;

+(NSMutableArray*)fetchPharmacieswithContacts;

+(NSMutableArray*)fetchDoctorsforCreateLocation;


+(NSMutableArray*)fetchFilteredContentforPharmacy:(NSString*)columnName;
+(NSMutableArray*)fetchTasksCountforLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID;
+(NSMutableArray*)fetchTaskCountforLocationID:(NSString*)locationID andContactID:(NSString*)contactID;
+(NSString*)fetchDetailedInfoforProduct:(NSString*)productID;

+(NSInteger)fetchNotesCountforLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID;
+(NSInteger)fetchNotesCountforLocationID:(NSString*)locationID andContactID:(NSString*)contactID;



+(NSMutableArray*)fetchFilteredContent:(NSString*)columnName;
+(NSMutableArray*)fetchFilteredProductsContent:(NSString*)columnName;
+(NSMutableArray*)fetchLocationDetailswithLocationID:(NSString*)columnName locationID:(NSString*)locID;
+(NSMutableArray*)fetchLocationIDwithLocationName:(NSString*)locationName;
+(NSMutableArray*)fetchTaskswithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID;



+(BOOL)saveVisitDetailsWithOutCall:(NSMutableDictionary*)visitDetails AdditionalInfo:(NSMutableDictionary*)visitAdditionalInfo andVisitDetails:(VisitDetails*)currentVisitDetails;
//+(BOOL)saveVisitDetailsWithOutCall:(NSMutableDictionary*)visitDetails AdditionalInfo:(NSMutableDictionary*)visitAdditionalInfo;

+(NSMutableArray*)fetchAllPlannedVisits;


+(NSMutableArray*)fetchTradeChannelforCustomerNumber:(NSString*)customerNumber;

+(NSMutableArray*)fetchDoctorObjects;
+(NSString*)fetchDemoPlanNamewithID:(NSString*)demoPlanID;
+(NSMutableArray*)fetchAllPlannedVisitsforFilter;
+(NSMutableArray*)fetchCallsInformationFroSelectedVisit:(NSString*)PlannedVisitId;
+(NSMutableArray*)fetchAllPharmacyDetails;
+(NSMutableArray*)fetchAllPharmacyDetailsWhenPendingVisitsEnabled;

+(BOOL)createContactforPharmacy:(PharmacyContact*)contact;


+(NSMutableArray*)fetchTaskObjectsforLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID;
+(NSMutableArray*)fetchTaskObjectswithLocationID:(NSString*)locationID andContactID:(NSString*)contactID;

+(NSMutableArray*)fetchAllTaskObjects;

+(NSMutableArray*)fetchMediaFileCountforDemoPlanID:(NSString*)demoPlanID;
+(NSMutableArray*)fetchMediaFileswithDemoPlanID:(NSString*)demoPlanID;
+(NSMutableArray*)fetchCodeDescriptionfromAppCodes:(NSString*)codeType;


+(BOOL)doctorMatcheswithLocation:(NSString*)locationID andDoctorID:(NSString*)doctorID;
+(BOOL)contactMatcheswithLocation:(NSString*)locationID andDoctorID:(NSString*)contactID;



+(NSMutableDictionary*)fetchVisitDataforSelectedDoctor:(NSString*)doctorID andDemoPlanID:(NSString*)demoPlanID;

+(NSMutableDictionary*)fetchVisitDataforSelectedPharmacy:(NSString*)contactID andDemoPlanID:(NSString*)demoPlanID;
+(NSString*)fetchAppCodeforLocationtype:(NSString*)codeValue;

+(NSMutableArray*)fetchLocationDetailsforDoctorID:(NSString*)doctorID;
+(NSInteger)fetchPreviouslyusedQtyforProduct:(NSString*)productID lotNumber:(NSString*)lotNumber;


//notes data object methods


+(NSMutableArray*)fetchNoteObjectwithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID;

+(NSMutableArray*)fetchNotesObjectswithLocationID:(NSString*)locationID andContactID:(NSString*)contactID;


+(BOOL)InsertNoteObject:(VisitNotes*)notes andVisitDetails:(VisitDetails*)visitDetails;


//get data for Dashboard
+(NSMutableArray*)fetchCompltedVisitsOfDoctorAndPharmaforCurrentMonth:(NSString*)str;
+(NSMutableArray*)fetchTotalTasksForDoctorAndLocation;


// delete future Visit
+(BOOL)deleteFutureVisit:(NSMutableDictionary *)visitDetailsDict;

+(NSMutableArray*)getTBLAppCodesDataForCodeType:(NSString *)codeType;
+(BOOL)updateVisitDetailsToOldvisit:(NSString*)oldPlannedVisitId;
+(BOOL)isSurveyCompletedForLocation:(NSString*)locationID andSurveyID:(NSString*)selectedSurveyID;


//multi calls methods
+(NSMutableArray*)fetchReasonsforNoCallBrandAmbassadorVisit;


+(NSMutableArray*)fetchContactsObjectsforPharmacyWithLocationID:(NSString*)locationID;
+ (FMDatabase *)getDatabase;
+(BOOL)updateContactDetails:(VisitDetails*)currentVisit;
+(NSMutableArray*)fetchDemoPlanObjects;
+(NSMutableArray*)fetchDoctorObjectswithLocationID:(NSString*)locationID;

+(NSMutableArray*)fetchCompletedCallsData:(VisitDetails*)currentVisit;
+(BOOL)updateVisitStatus:(VisitDetails*)currentVisit;
+(NSMutableArray*)fetchReasonsforNoCall;
+(NSMutableArray*)fetchTaskObjectsforHospital:(NSString*)locationID;
+(NSMutableArray*)fetchTaskObjectsforPharmacy:(NSString*)locationID;
+(NSMutableArray*)fetchNoteObjectsforHospital:(NSString*)locationID;

+(NSMutableArray*)fetchNotesObjectforPharmacy:(NSString*)locationID;
+(NSMutableDictionary*)fetchStatusCodeForTaskandNotes:(VisitDetails*)visitDetails;
+(NSMutableArray*)fetchMediaFilesforAllProducts;
+(NSMutableArray*)fetchAllproductswithMediaFiles;

+(BOOL)insertPharmacyLocationMap:(NSString*)location_ID withContactID:(NSString*)Contact_ID;

+(NSString *)getPDARIGHTSForSalesRep;
+(NSMutableDictionary *)fetchStatusCodesForBrandAmbassadorTasksandNotes:(SalesWorxBrandAmbassadorVisit*)visitDetails;

+(NSMutableArray*)fetchSampelsForBrandAmbassadorVisit;
+(NSString*)convertNSDatetoDataBaseDateTimeFormat:(NSDate *)inputDate;
+(NSDate *)getDateFromString:(NSString *)dateStr Format:(NSString *)dateFormat;
+(NSString *)getCurrentDateInDeviceDateWithOutTimeDisplayFormat;
+(NSString *)getCurrentDateInDeviceDateWithTimeDisplayFormat;

+(BOOL)saveVisitDetailsOnCloseAfterStartCall:(NSMutableDictionary*)visitDetails andVisitDetails:(VisitDetails*)currentVisitDetails;
+(NSString*)fetchCurrentDateInDatabaseFormat;
+(NSMutableArray *)getInCompleteVisits;
+(NSNumber*)fetchNumberOfVisitsRequiredForADoctor:(Doctors*)doctor;
+(NSNumber *)fetchNumberOFVisitsCompletedForaDoctorInCurrentMonth:(Doctors *)doctor;
+(NSNumber *)fetchCompletedPharmacyVisitsInCurrentMonth;
+(NSNumber *)fetchCompletedDoctorsCallsInCurrentMonth;
+(NSNumber *)fetchNumberOfVisitsRequiredForDoctorsInaMonth;
+(NSNumber *)fetchNumberOfVisitsRequiredForPharmaciesInaMonth;
+(NSNumber *)fetchCompletedPharmaciesCallsInCurrentMonth;
+(NSNumber *)fetchNumberOFVisitsCompletedForaPharmcyInCurrentMonth:(NSString *)locationId;
+(NSString*)convertNSDatetoDataBaseDateFormat:(NSDate *)inputDate;

+(NSMutableArray*)fetchHolidays;
+(BOOL)UpdateTask:(VisitTask*)currentTask;
+(NSMutableDictionary*)fetchLastVisitDetails:(NSString*)doctorID;
+(NSString*)fetchPreviousVisitObjective:(NSMutableDictionary*)visitDetailsDict;
+(NSMutableDictionary*)fetchLastVisitDetailsforLocation:(NSString*)locationID
;
+(NSMutableDictionary*)fetchLastVisitDetailsforContactID:(NSString*)contactID;
+(NSString *)ConvertDateIntoDeviceDateWithTimeDisplayFormat:(NSDate *)date;
+(NSString *)ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:(NSDate *)date;
+(NSDictionary*)fetchTasksDueForTodayForNotifications;
+(NSString*)fetchDescriptionForAppCode:(NSString*)appCode;

+(NSMutableArray*)fetchReasonsforRescheduleVisit;
+(NSMutableDictionary*)fetchLastVisitNote:(NSString*)visitID;

@end
