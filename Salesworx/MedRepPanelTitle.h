//
//  MedRepPanelTitle.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepPanelTitle : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
@property (nonatomic) IBInspectable BOOL isTextEndingWithExtraSpecialCharacter;
-(NSString*)text;
-(void)setText:(NSString*)newText;
@end
