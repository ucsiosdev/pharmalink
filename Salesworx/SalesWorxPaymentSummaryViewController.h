//
//  SalesWorxPaymentSummaryViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "XYPieChart/XYPieChart.h"
#import "SalesWorxReportsDateSelectorViewController.h"
#import "MedRepDateRangeTextfield.h"
#import <MessageUI/MessageUI.h>
#import "SalesWorxPaymentCollectionReceiptPDFTemplateView.h"

@interface SalesWorxPaymentSummaryViewController : UIViewController<StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,XYPieChartDataSource,XYPieChartDelegate, UIPopoverPresentationControllerDelegate,SalesWorxReportsDateSelectorViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    NSString * popString, *datePopOverTitle;
    NSMutableArray *unfilteredPaymentSummaryArray, *arrCustomerName, *arrCustomersDetails, *arrPaymentType;
 
    IBOutlet UITableView *tblPaymentSummary;
    IBOutlet MedRepTextField *txtCustomer;
    IBOutlet MedRepTextField *txtPaymentType;
    IBOutlet MedRepDateRangeTextfield *DateRangeTextField;
    IBOutlet XYPieChart *customerPieChart;
    IBOutlet UIView *parentViewOfPieChart;
    IBOutlet UIImageView *pieChartPlaceholderImage;
    IBOutlet UIView *viewFooter;
    IBOutlet UILabel *lblTotalAmount;

}
@property(strong,nonatomic) SalesWorxVisit* currentVisit;

@property (strong,nonatomic) NSMutableArray *arrPaymentSummary;
@property (nonatomic, strong) NSMutableArray *slicesForCustomerPotential;
@property (nonatomic, strong) NSArray        *sliceColorsForCustomerPotential;
@end
