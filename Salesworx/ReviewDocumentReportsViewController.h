//
//  ReviewDocumentReportsViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 1/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ReviewDocumentReportsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
   // NSString* itemCode,*itemDesc,*quantity,*unitPrice,*totalPrice;
    NSMutableArray* dataArray;
    NSString *invIDforQuery;
    NSMutableArray* productDataArray;
    NSString* documentNumber;
    
    NSString* orderType;
    
    double totalReturns;
    NSMutableArray* returnLineItems;
    
    float finalTot,orderedTotal,invoicedTotal;
    
    NSMutableArray* invoicedDataArray,*invoicedProductDataArray,*inVoicesInventoryArray,*orderedItemsValueArray,*invoicedItemsValueArray;
    
   
}

@property (strong, nonatomic) IBOutlet UIButton *prevOrderButton;
@property (strong, nonatomic) IBOutlet UIButton *nextOrderButton;

@property(strong,nonatomic)NSString* reportType;

@property(nonatomic)int selectedIndex;
@property(strong,nonatomic)NSMutableArray* totalContentDataArray;
@property (strong, nonatomic) IBOutlet UILabel *orderNumberLbl;
@property (strong, nonatomic) IBOutlet UITableView *reportsTableView;

@property (strong, nonatomic) IBOutlet UILabel *orderDateLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderValueLbl;
@property (strong, nonatomic) IBOutlet UILabel *invoiceDateLbl;
@property (strong, nonatomic) IBOutlet UILabel *invoiceValueLbl;
@property (strong, nonatomic) IBOutlet UILabel *fsrLbl;
@property(strong,nonatomic) NSMutableDictionary* dataDictionary;
@property(nonatomic) float grandTotal;
@property(strong,nonatomic)    NSString* itemCode,*itemDesc,*quantity,*unitPrice,*totalPrice,*inventoryID,* appendedStr,*appendedUnitPrice;

- (IBAction)segmentSwitch:(id)sender;
//@property(nonatomic,strong)NSMutableArray *dataArray;

@property (strong, nonatomic) IBOutlet UILabel *orderStatusLbl;

@property (strong, nonatomic) IBOutlet UILabel *CustomerNameLbl;

@property(strong,nonatomic)NSMutableArray *ordersArray;
@property (strong, nonatomic) IBOutlet UISegmentedControl *customSegment;

- (IBAction)prevOrderButton:(id)sender;

- (IBAction)nextOrderButton:(id)sender;



//static status labels

@property (strong, nonatomic) IBOutlet UILabel *orderStatusStaticLbl;
@property (strong, nonatomic) IBOutlet UILabel *invDateStaticLbl;
@property (strong, nonatomic) IBOutlet UILabel *invvalueStaticLbl;

@property (strong, nonatomic) IBOutlet UILabel *fsrStaticLbl;

@property (strong, nonatomic) IBOutlet UILabel *orderDateStaticLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderValueStatic;






@end
