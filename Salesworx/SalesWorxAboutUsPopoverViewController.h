//
//  SalesWorxAboutUsPopoverViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 12/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "SalesWorxPresentControllerBaseViewController.h"

@interface SalesWorxAboutUsPopoverViewController : SalesWorxPresentControllerBaseViewController<MFMailComposeViewControllerDelegate>
{    
    IBOutlet UILabel *lblCustomerID;
    IBOutlet UILabel *lblUser;
    IBOutlet UILabel *lblDeviceID;
    IBOutlet UILabel *lblVersionNo;
    
    IBOutlet UITableView *tblAboutUs;
    NSMutableArray *arrAboutOptions;
}
@property(nonatomic) id SalesWorxAboutUsPopoverViewControllerDelegate;

@end
