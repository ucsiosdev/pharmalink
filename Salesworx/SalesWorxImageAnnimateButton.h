//
//  SalesWorxImageAnnimateButton.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDefaultButton.h"
@interface SalesWorxImageAnnimateButton : SalesWorxDefaultButton
-(void)UpdateImage:(UIImage *)newImage WithAnimation:(BOOL)Animated;
@end
