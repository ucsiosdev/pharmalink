//    //
////  NewActivationViewController.m
////  SWSession
////
////  Created by Irfan Bashir on 5/3/12.
////  Copyright (c) 2012 UCS Solutions. All rights reserved.
////
//
//#import "NewActivationViewController.h"
//#import "SWFoundation.h"
//#import "ServerSelectionViewController.h"
//#import "SWPlatform.h"
//#import "SWSession.h"
//#import "SWDatabaseManager.h"
//#import <sqlite3.h>
//#import "SSKeychain.h"
////#import "SWDashboard/SWDashboard.h"
//#import "Reachability.h"
//#import <SystemConfiguration/SystemConfiguration.h>
//#import "LoginViewController.h"
//#import <SalesWorxPopOverViewController.h>
//#import "MedRepDefaults.h"
//
//
//
//@interface NewActivationViewController ()
//@end
//
//@implementation NewActivationViewController
//
//@synthesize alertDisplayed,invalidCredentials,logTextView;
//
//#define kLoginField 1
//#define kPasswordField 2
//#define kSNameField 3
//#define kSLinkField 4
//
//
//
//@synthesize target,action,isDemo;
//- (id)initWithNoDB {
//    self = [super init];
//    
//    if (self) {
//        [self setTitle:@"New Activation"];
//        isRootView=YES;
//        isActivation = YES;
//        ////NSLog(@"From Appdelegate");
//    }
//    
//    return self;
//}
//
//- (id)init {
//    self = [super init];
//    
//    if (self) {
//        isActivation = NO;
//        [self setTitle:@"New Activation"];
//        isRootView=NO;
//        ////NSLog(@"From Lofig");
//    }
//    
//    return self;
//}
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
//        return YES;
//    
//    return NO;}
////- (void)dealloc
////{
////    NSLog(@"Dealloc %@",self.title);
////    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////}
//
//
//-(void)viewWillAppear:(BOOL)animated
//{
//    self.navigationItem.hidesBackButton=NO;
//    
//    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
//    
//    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
//  NSString*  ClientVersion =avid;
//
//    
//    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
//    
//    
//    
//    lblVersion.text= [NSString stringWithFormat:@"Version  %@ (%@)",ClientVersion,build];
//
//    
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postSycStatus:) name:@"SyncStatusNotification"  object:nil];
//    
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
//
//    
//    
//    tymProgressBarView.barBorderWidth=2.0;
//    tymProgressBarView.barBorderColor=UIViewControllerBackGroundColor;
//    tymProgressBarView.barBackgroundColor=UIViewControllerBackGroundColor;
//    tymProgressBarView.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
//    tymProgressBarView.barInnerBorderColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
//    
//    
//    tymProgressBarView.usesRoundedCorners=YES;
//
//    
//    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
//    
//    NSError *error;
//    NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
//    
//    NSMutableArray *testServerArray = [NSMutableArray arrayWithArray:[djsonSerializer deserializeAsArray:data error:&error]  ];
//    
//    
//    NSLog(@"server details array is %@",testServerArray );
//    
//    
//    
//    serverDetailsArray=[[NSMutableArray alloc]init];
//    
//    serverDetailsArray=testServerArray;
//
////    medRepLeftView.layer.borderWidth=5.0;
//////    medRepLeftView.layer.borderColor=[[UIColor redColor] CGColor];
////    medRepLeftView.layer.shadowColor = [[UIColor blueColor]CGColor];
////    medRepLeftView.layer.shadowOffset = CGSizeMake(5, 5);
////    medRepLeftView.layer.shadowOpacity =0.1;
////    medRepLeftView.layer.shadowRadius = 1.0;
//    
//}
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//
//    
//    
//    
//    
//    
//
//    [Flurry logEvent:@"Activation View"];
//    //[Crittercism leaveBreadcrumb:@"<Activation View>"];
//
//    //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//
//    appData=[DataSyncManager sharedManager];
//
//    
//    //to be done
//    
////    progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
////    progressHUD.delegate = self;
////    loadingView=[[SWLoadingView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
//    
//    serverDict = [NSMutableDictionary dictionary];
//    editServerDict = [NSMutableDictionary dictionary ];
//
//    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
//    NSError *error;
//    
//    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
//
//    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]];
//
//    
//    
//    //to be done
//    
//    
//    serverDict =[NSMutableDictionary dictionaryWithDictionary:[testServerDict mutableCopy]];
//    
//    
//    
//    
////    NSMutableDictionary * localDict=[[NSMutableDictionary alloc]init];
////    
////    [localDict setValue:@"Local Server" forKey:@"serverName"];
////    
////    [localDict setValue:@"192.168.100.14:3512" forKey:@"serverLink"];
////    
////    serverDict=localDict;
////    
////    NSLog(@"server dict is %@", serverDict);
//
//    
//    login = [SWDefaults usernameForActivate];
//   // [SWDefaults setUsername:login];
//    
//    password = [SWDefaults passwordForActivate];
//    //[SWDefaults setPassword:password];
//    
//    editServerName1 = [serverDict stringForKey:@"serverName"];
//    editServerLink = [serverDict stringForKey:@"serverLink"];
//    
//    
//    //to be done
//    
//    editServerName1 =@"Demo Server";
//    
//    editServerLink=@"192.168.100.14:3512";
//    
//    [SWDefaults setUsernameForActivate:@"Rashid"];
//    [SWDefaults setPasswordForActivate:@"password"];
//    
//    
//    
//
//    
//
//    _footerView = [[UIView alloc] initWithFrame:CGRectMake(100, 0, 400, 400)];
//    
//    _footerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    
//    UIView *secFooterView = [[UIView alloc] initWithFrame:CGRectMake(45, 10, 934, 344)];
//    secFooterView.backgroundColor = [UIColor lightTextColor];
//    secFooterView.layer.shadowOffset = CGSizeMake(4, 4);
//    //secFooterView.layer.shadowRadius = 3;
//    // secFooterView.layer.shadowOpacity = 0.2;
//    secFooterView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    secFooterView.layer.borderWidth = 1.0;
//
//    
//    
//    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(10,10, 910, 44.0f)];
//    l.font = RegularFontOfSize(20);
//    l.textColor = [UIColor darkGrayColor];
//    l.text = @"  Sync Status";
//    l.backgroundColor = [UIColor lightTextColor];
//    
//    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
//    if([language isEqualToString:@"ar"])
//    {
//        bacgroundBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_ActProgress_AR" cache:NO]];
//    }
//    else
//    {
//        bacgroundBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_ActProgress" cache:NO]];
//    }
//    bacgroundBar.frame = CGRectMake(100,10, 60.0f, 44.0f);
//    bacgroundBar.contentMode = UIViewContentModeCenter;
//
//    [_footerView addSubview:secFooterView];
//    [secFooterView addSubview:l];
//    
//    logTextView = [[UITextView alloc] initWithFrame:CGRectMake(10,54 , 910, 280)];
//    logTextView.scrollEnabled=YES;
//    logTextView.editable=NO;
//    logTextView.textColor=[UIColor darkGrayColor];
//    logTextView.backgroundColor = [UIColor whiteColor];
//    [secFooterView addSubview:logTextView];
//    
//     _footerView.backgroundColor = [UIColor clearColor];
//    
//    
//    
//    tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped ];
//    
//    [tableView setDataSource:self];
//    [tableView setDelegate:self];
//    tableView.backgroundView = nil;
//    tableView.backgroundColor = [UIColor whiteColor];
//    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
//    
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
//        
//        //in ios 9 tableview cell frame getting changed this will fix it
//        tableView.cellLayoutMarginsFollowReadableWidth = NO;
//        
//    }
//    
//    //creating new UI with Xib
//    
//   // [self.view addSubview:tableView];
//    
//    if (isRootView)
//    {
//       // [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Activate" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
//    }
//    else
//    {
//        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Upload Database" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
//    }
//    
//    
//    
//
//}
//- (void)showActivate{
//    
//    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    if (internetStatus == NotReachable) {
//    
//        NSLog(@"internet unavailable");
//        
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Internet unavailable", nil) message:@"please connect to internet and try again" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//
//        
//    }
//    else {
//        //there-is-no-connection warning
//        
//    
//        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
//
//    
//    alertDisplayed=NO;
//    
//    //self.navigationItem.rightBarButtonItem.enabled=NO;
//    [self.view endEditing:YES];
//    logTextView.text = @"";
//    progressHUD.labelText = @"";
//    progressHUD.detailsLabelText = @"";
//    progressHUD.progress = 0;
//    progressHUD.mode = MBProgressHUDModeIndeterminate;
//  //  progressHUD.backgroundColor =[UIColor colorWithWhite:0 alpha:0.6];
//    
//   // [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.6]];
//
//    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
//    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
//
//    NSError *error;
//    NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
//    
//    NSMutableArray *testServerArray = [NSMutableArray arrayWithArray:[djsonSerializer deserializeAsArray:data error:&error]  ];
//        
//        
//        NSLog(@"server details array is %@",testServerArray );
//
//        
//        
//        NSMutableDictionary * localDict=[[NSMutableDictionary alloc]init];
//        
//        [localDict setValue:@"Local Server" forKey:@"serverName"];
//        
//        [localDict setValue:@"192.168.100.14:3512" forKey:@"serverLink"];
//        
//        if ([testServerArray containsObject: localDict]) {
//            
//            
//        }
//        
//        else
//        {
//        
//        [testServerArray insertObject:localDict atIndex:1];
//        
//        }
//        
//        
//        serverDetailsArray=[[NSMutableArray alloc]init];
//        
//        serverDetailsArray=testServerArray;
//        
//    if([self validateInput])
//    {
//        for(int i=0 ; i<[testServerArray count] ; i++ )
//        {
//            NSDictionary *row = [testServerArray objectAtIndex:i];
//        
//            if([[row stringForKey:@"serverName"] isEqualToString:[serverDict stringForKey:@"serverName"]])
//            {
//            
//                if(editServerName1.length > 0)
//                {
//                    [serverDict setValue:editServerName1 forKey:@"serverName"];
//                }
//            
//                if(editServerLink.length > 0)
//                {
//                    [serverDict setValue:editServerLink forKey:@"serverLink"];
//                }
//            
//                [testServerArray removeObjectAtIndex:i];
//                [testServerArray addObject:serverDict];
//                NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
//                NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
//                [SWDefaults setSelectedServerDictionary:stringDataDict];
//                break;
//            }
//        }
//        if([serverDict count]==0)
//        {
//            serverDict = editServerDict;
//            [testServerArray addObject:serverDict];
//            NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
//            NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
//            [SWDefaults setSelectedServerDictionary:stringDataDict];
//        }
//        NSData *datas = [jsonSerializer serializeObject:testServerArray error:&error];
//        NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
//        [SWDefaults setServerArray:stringData];
//    
//        NSLog(@"server array is %@", stringData);
//        
//        
//        if (isRootView)
//        {
//            if ([SWDefaults usernameForActivate].length > 0 && [SWDefaults passwordForActivate].length > 0)
//            {
//               // [self.view addSubview:self.loadingView];
//                
//                
//                
//                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                
//                
//                [self.view addSubview:progressHUD];
//                
//                viewSync=nil;
//                viewSync.delegate=nil;
//                viewSync = [[SynViewController alloc] init];
//                viewSync.delegate=self;
//                [viewSync setTarget:self];
//                [viewSync setAction:@selector(activationDone:)];
//                //[self.view addSubview: [DataSyncManager sharedManager].progressHUD];
//                
//
//                //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                
//                
//                [viewSync setRequestForSyncActivateWithUserName:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate]];
//            }
//            else
//            {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"PLease fill all fields", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                [alert show];
//
//            }
//        }
//        else
//        {
//           // [self.view addSubview:self.loadingView];
//            [self.view addSubview:progressHUD];
//            Singleton *single = [Singleton retrieveSingleton];
//            single.isActivated = NO;
//            viewSync=nil;
//            viewSync.delegate=nil;
//            viewSync = [[SynViewController alloc] init];
//            viewSync.delegate=self;
//            [viewSync setTarget:self];
//            [viewSync setAction:@selector(uploadDone:)];
//            [viewSync setRequestForSyncUpload];
//        }
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Please fill all fields", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//
//    }
//}
//}
//- (void)activationDone:(NSString *)msg{
//    
//    
//    NSLog(@"new activation called");
//    
//    
//    NSArray* arr=[[NSArray alloc]init];
//    
//    arr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat: @"select Username from TBL_User"]];
//    
//    if ([arr count]>0) {
//        NSString * userNameStr = [[arr objectAtIndex:0]valueForKey:@"Username"];
//        
//        if ([userNameStr isEqualToString:@"demo"]) {
//            NSLog(@"it is a demo");
//        }
//        else
//        {
//            NSLog(@"its not a demo");
//            
//            isDemo=NO;
//            
//            [SSKeychain setPassword:@"registereduser" forService:@"registereduser" account:@"user"];
//            
//            
//            //        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isDemo"];
//            //        [[NSUserDefaults standardUserDefaults] synchronize];
//        }
//        
//        
//        
//        [SWDefaults setLastSyncStatus:@"N/A"];
//        if([msg isEqualToString:@"error"])
//        {
//            [SWDefaults setIsActivationDone:@"NO"];
//            [progressHUD removeFromSuperview];
//            //[self.loadingView removeFromSuperview];
//        }
//        else
//        {
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            [SWDefaults setIsActivationDone:@"YES"];
//            //NSLog(@"user : %@ Pass :%@",[SWDefaults usernameForActivate],[SWDefaults passwordForActivate]);
//            NSString *temp =[[SWDatabaseManager retrieveManager] verifyLogin:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate]];
//            if ([temp isEqualToString:@"Done"])
//            {
//                viewSync.delegate=nil;
//                viewSync=nil;
//                viewSync = [[SynViewController alloc] init];
//                [viewSync viewDidLoad];
//                viewSync.delegate=self;
//                [viewSync setTarget:self];
//                [viewSync setAction:@selector(activationCompleted)];
//                [viewSync sendRequestForExecWithUserName:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate] andProcName:@"sync_MC_GetLastDocumentReferenceAll" andProcParam:@"SalesRepID" andProcValue:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
//                
//            }
//            else
//            {
//                
//                [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
//
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:temp delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                [alert show];
//                
//                
//            }
//            [SWDefaults setActivationStatus:@"YES"];
//        }
//
//    }
//    else
//    {
//        
//        if (alertDisplayed==NO) {
//            
//        
//        invalidAlert=[[UIAlertView alloc]initWithTitle:@"Invalid Credentials" message:@"Login/Server  credentials are invalid" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            invalidAlert.tag=2000;
//            invalidCredentials=YES;
//        [invalidAlert show];
//        
//        }
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//    }
//    
//   }
//
//-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    
//    
////    if (alertView.tag==2000) {
////        
////        alertDisplayed=YES;
////        
////    }
//    
//    if (alertView.tag==1001) {
//
//    LoginViewController *loginVC = [[LoginViewController alloc] init];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
//    [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
//    
//    }
//    
//
//}
//
//- (void)uploadDone:(NSString *)msg {
//
//    //[loadingView removeFromSuperview];
//    [progressHUD removeFromSuperview];
//   
//    if([msg isEqualToString:@"error"])
//    {
//
//    }
//    else
//    {
//        isRootView=YES;
//        [SWDefaults setIsActivationDone:@"NO"];
//        [tableView reloadData];
//        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Activate" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
//        self.navigationItem.hidesBackButton = YES;
//        [SWDefaults setUsernameForActivate:@""];
//        [SWDefaults setPasswordForActivate:@""];
//        [SWDefaults setPassword:@""];
//        [SWDefaults setUsername:@""];
//    }
//}
//-(void)viewDidDisappear:(BOOL)animated
//{
//    //[[NSNotificationCenter defaultCenter] removeObserver:self];
//    [tableView setDelegate:nil];
//    [tableView setDataSource:nil];
//    tableView=nil;
//    popoverController=nil;
//    window=nil;
//    applicationViewController=nil;
//    login=nil;
//    password=nil;
//    serverDict=nil;
//    
//    editServerLink=nil;
//    editServerName1=nil;
//    newServerDict=nil;
//    loadingView=nil;
//    appData=nil;
//    logTextView=nil;
//    _footerView=nil;
//    bacgroundBar=nil;
//    progressHUD=nil;
//    progressHUD.delegate=nil;
//    [[HttpClient sharedManager].operationQueue cancelAllOperations];
//    [viewSync cancelHTTPRequest];
//    viewSync.delegate = nil;
//    viewSync=nil;
//    
//}
//
//- (void)serverChanged:(NSMutableDictionary *)d {
//    serverDict=nil;
//    serverDict=d ;
//    
//    
//    editServerLink = [d stringForKey:@"serverLink"];
//    editServerName1 = [d stringForKey:@"serverName"];
//    [tableView reloadData];
//    [popoverController dismissPopoverAnimated:YES];
//    
//}
//
//#pragma mark UIView delegate
//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    [[HttpClient sharedManager].operationQueue cancelAllOperations];
//    [viewSync cancelHTTPRequest];
//    viewSync.delegate = nil;
//    viewSync=nil;
//    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
//}
//
//#pragma mark UITableViewDataSource
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if(isRootView)
//    {
//        return 3;
//    }
//    else
//    {
//        return 1;
//    }
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    
//    if(isRootView)
//    {
//        if (section==2)
//        {
//            return 1;
//        }
//        else
//        {
//            return 2;
//        }
//    }
//    else
//    {
//        return 2;
//    }
//    
//}
//- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    {
//    NSString *IDENT = @"SessionCellIdent";
//    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:IDENT];
//    if(cell)
//    {
//        cell=nil;
//    }
//        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
//    if(isRootView)
//    {
//        if (indexPath.section == 0) {
//            if (indexPath.row == 0) {
//                ((SWTextFieldCell *)cell).textField.placeholder = @"username";
//                ((SWTextFieldCell *)cell).textField.delegate=self;
//                ((SWTextFieldCell *)cell).textField.tag=kLoginField;
//                ((SWTextFieldCell *)cell).textField.text = [SWDefaults usernameForActivate];
//
//
//            } else {
//                ((SWTextFieldCell *)cell).textField.placeholder = @"password";
//                ((SWTextFieldCell *)cell).textField.delegate=self;
//                ((SWTextFieldCell *)cell).textField.secureTextEntry = YES;
//                ((SWTextFieldCell *)cell).textField.tag=kPasswordField;
//                ((SWTextFieldCell *)cell).textField.text = [SWDefaults passwordForActivate];
//
//
//            }
//            ((SWTextFieldCell *)cell).textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        }
//        else if (indexPath.section == 1) {
//
//            if (indexPath.row == 0) {
//                ((SWTextFieldCell *)cell).textField.placeholder = @"server name";
//                ((SWTextFieldCell *)cell).textField.delegate=self;
//                ((SWTextFieldCell *)cell).textField.tag=kSNameField;
//                ((SWTextFieldCell *)cell).textField.text=[serverDict stringForKey:@"serverName"];
//                
//            } else {
//                ((SWTextFieldCell *)cell).textField.placeholder = @"server link";
//                ((SWTextFieldCell *)cell).textField.delegate=self;
//                ((SWTextFieldCell *)cell).textField.tag=kSLinkField;
//                ((SWTextFieldCell *)cell).textField.text=[serverDict stringForKey:@"serverLink"];
//            }
//        }
//        
//        else
//        {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENT] ;
//            [cell.textLabel setText:NSLocalizedString(@"Server locations", nil)];
//            cell.textLabel.font = LightFontOfSize(14);
//            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//        }
//    
//    }
//    else
//    {
//        if (indexPath.row == 1)
//        {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENT] ;
//            //[cell.textLabel setText:NSLocalizedString(@"Server locations", nil)];
//            [cell.textLabel setText:[serverDict stringForKey:@"serverName"]];
//            cell.textLabel.font = LightFontOfSize(14);
//            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//        }
//        else
//        {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENT] ;
//            cell.textLabel.font = LightFontOfSize(14);
//            [cell.textLabel setText:@"Please synchronize existing data prior to activation."];
//        }
//    }
//    return cell;
//}
//}
//- (void)reloadRowAtIndex:(int)rowIndex andSection:(int)section{
//    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
//}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    if(isRootView)
//    {
//    if (section == 0) {
//        return NSLocalizedString(@"Login", nil);
//    }
//    else if(section == 1)
//    {
//        return NSLocalizedString(@"Server", nil);
//    }
//    else
//    {
//        return nil;
//    }
//    }
//    else{
//        return nil;
//    }
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if((isRootView && section==2) || !isRootView)
//    {
//        return _footerView.frame.size.height;
//    }
//    else
//    {
//        return  false;
//    }
//}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    if((isRootView && section==2) || !isRootView)
//    {
//        return _footerView;
//    }
//    else 
//    {
//        return  nil;
//    }
//}
//#pragma mark UITableViewDataSource
//- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    [self.view endEditing:YES];
//    if(isRootView)
//    {
//        if (indexPath.section == 0 || indexPath.section == 1 )
//        {
//            return;
//        }
//    
//        ServerSelectionViewController *serverSelectionViewController = [[ServerSelectionViewController alloc] init] ;
//        [serverSelectionViewController setTarget:self];
//        [serverSelectionViewController setSelectionChanged :@selector(serverChanged:)];
//        [serverSelectionViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
//
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:serverSelectionViewController] ;
//        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
//        popoverController.delegate=self;
//        UITableViewCell *cell = [tv cellForRowAtIndexPath:indexPath];
//        [popoverController setDelegate:self];
//        [popoverController presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//    }
//    else
//    {
//        if (indexPath.row == 1)
//        {
//            ServerSelectionViewController *serverSelectionViewController = [[ServerSelectionViewController alloc] init] ;
//            [serverSelectionViewController setTarget:self];
//            [serverSelectionViewController setSelectionChanged :@selector(serverChanged:)];
//            [serverSelectionViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
//            
//            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:serverSelectionViewController] ;
//            popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
//            popoverController.delegate=self;
//            UITableViewCell *cell = [tv cellForRowAtIndexPath:indexPath];
//            [popoverController setDelegate:self];
//            [popoverController presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//
//        }
//    }
//}
//#pragma mark UIPopOver Delegates
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
//    return YES;
//}
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController1{
//    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
//    popoverController1=nil;
//}
//#pragma mark UITextFieldDelegate
//- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//    
//    
//    if ([usernameTextField isFirstResponder]) {
//        
//        [usernameTextField resignFirstResponder];
//        [passwordTextField becomeFirstResponder];
//    }
//
//    
//    [textField resignFirstResponder];
//    
//    return YES;
//}
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//    [self.navigationItem setRightBarButtonItem:nil];
//    return YES;
//}
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
//
//    if (textField.tag == kLoginField) {
//        login=textField.text;
//        [SWDefaults setUsername:textField.text];
//        [SWDefaults setUsernameForActivate:textField.text];
//    }
//    else if (textField.tag == kPasswordField) {
//        password=textField.text;
//        [SWDefaults setPassword:textField.text];
//        [SWDefaults setPasswordForActivate:textField.text];
//        
//    }
//    else if (textField.tag == kSNameField) {
//        editServerName1=textField.text;
//        [editServerDict setValue:textField.text forKey:@"serverName"];
//
//    }
//    else if (textField.tag == kSLinkField) {
//        editServerLink=textField.text;
//        [editServerDict setValue:textField.text forKey:@"serverLink"];
//
//    }
//    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Activate" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
//
//    return YES;
//}
//
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
//    
//    
//    if (textField.text.length >= 30 && range.length == 0)
//
//        
//    {
//        
//        return NO;
//    }
//    
//    
//  else if (textField==usernameTextField) {
//       
//    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
//    
//    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//    
//    return [string isEqualToString:filtered];
//        
//    }
//    
//   
//    
//    else if (textField==servernameTextField)
//        
//    {
//        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_URL] invertedSet];
//        
//        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//        
//        return [string isEqualToString:filtered];
//    }
//    
//    else if (textField==serverLocationTextField)
//    {
//        
//        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_URL] invertedSet];
//        
//        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//        
//        return [string isEqualToString:filtered];
//
//    }
//    
//    else
//    {
//        return YES;
//        
//    }
//}
//
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    
//    
//    if (alertView.tag==2000) {
//        alertDisplayed=YES;
//        
//        
//        }
//    
//    if (alertView.tag==1001) {
//        if (isActivation)
//        {
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
//            //[self.target performSelector:self.action withObject:nil];
//            
//            //activation compted here push to login
//            
////            LoginViewController *loginVC = [[LoginViewController alloc] init];
////            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
////            [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
////            
////
////            
//#pragma clang diagnostic pop
//        }
//        else
//        {
//            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//            [self.navigationController  popViewControllerAnimated:YES];
//        }
//
//    }
//}
//- (void)activationCompleted{
//    
//    
//    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"Activation completed successfully" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//    alert.tag=1001;
//    [alert show];
//   
//    
//    [viewSync cancelHTTPRequest];
//    viewSync.delegate = nil;
//    viewSync=nil;
//    [progressHUD removeFromSuperview];
//
//    }
//- (BOOL)validateInput{
//
//    if (login.length > 0 && password.length > 0 && editServerName1.length > 0 && editServerLink.length > 0 )
//    {
//        return YES;
//    }
//    else
//    {
//        return NO;
//    }
//}
//- (void)updateActivationTextDelegate:(NSString *)synchDate{
//
//    logTextView.font = LightFontOfSize(14.0f);
//    logTextView.text = [logTextView.text stringByAppendingString:synchDate];
//    CGPoint p = [logTextView contentOffset];
//    [logTextView setContentOffset:p animated:YES];
//    [logTextView scrollRangeToVisible:NSMakeRange([logTextView.text length], 0)];
//}
//- (void)updateProgressDelegateWithType:(MBProgressHUDMode)type andTitle:(NSString *)title andDetail:(NSString *)detail andProgress:(float)progress{
//    progressHUD.mode=type;
//    progressHUD.labelText=title;
//    progressHUD.detailsLabelText=detail;
//    progressHUD.progress=progress;
//    
//    //update task and status here
//    
//    NSLog(@"TASK AND PROGRESS IS %@  %@ % 0.2f",progressHUD.labelText, progressHUD.detailsLabelText, progressHUD.progress);
//    
//    
//    
//    tymProgressBarView.progress=progress;
//    
//    if (progress==100.00) {
//        
//        activationProgressView.progress=progressHUD.progress;
//        activationProgressView.progress=0;
//
//    }
//    activationProgressView.progress=progressHUD.progress;
//    
//    
//    
//}
//
//- (void)postSycStatus:(NSNotification *)notification
//{
//    
//    NSLog(@"*******SYC STATUS NOTIFICATION NEW ACTIVATION ********** %@", [notification object]);
//    
//    if ([[notification object]isEqualToString:@"Order reference has been retrieved"]) {
//        
//        downloadStatusLbl.text=@"Activation Completed Successfully";
//
//    }
//    else
//    {
//    
//    downloadStatusLbl.text=[notification object];
//    }
//    
//}
//
//- (void)didReceiveMemoryWarning{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        
//        self.view = nil;
//    
//    // Dispose of any resources that can be recreated.
//}
//
//
//#pragma mark Server Location button Methods
//
//- (IBAction)syncLocationButtonTapped:(id)sender {
//    
//    
//    
//    
//    NSLog(@"server details array is %@",serverDetailsArray );
//    
//    
//    
//    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
//    popOverVC.popOverContentArray=[serverDetailsArray valueForKey:@"serverName"];
//    popOverVC.salesWorxPopOverControllerDelegate=self;
//    popOverVC.disableSearch=YES;
//    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
//    popoverController=nil;
//    popoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
//    popoverController.delegate=self;
//    popOverVC.popOverController=popoverController;
//    
//    [popoverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
//    
//    
//    
//    
//    CGFloat X = synLocationDropDownImage.frame.origin.x + [synLocationDropDownImage superview].frame.origin.x;
//    CGFloat Y = synLocationDropDownImage.frame.origin.y + [synLocationDropDownImage superview].frame.origin.y;
//    
//    CGRect btnFrame = CGRectMake(X, Y, 18, 9);
//    
//    
//    NSLog(@"frame is %@", NSStringFromCGRect(btnFrame));
//    
//    [popoverController presentPopoverFromRect:btnFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//
//
//}
//
//
//
//-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
//{
//    
//    if (selectedIndexPath.row+1==serverDetailsArray.count) {
//        
//        NSLog(@"last index");
//        servernameTextField.text=@"";
//        
//        servernameTextField.userInteractionEnabled = YES;
//
//        
//        serverLocationTextField.userInteractionEnabled = YES;
//
//        [servernameTextField becomeFirstResponder];
//        
//        
//    }
//    
//    
//    else
//    {
//    
//    NSLog(@"server details in delegate %d",serverDetailsArray.count);
//
//    NSLog(@"index is  %d",selectedIndexPath.row);
//
//    
//    servernameTextField.text=[[serverDetailsArray objectAtIndex:selectedIndexPath.row] valueForKey:@"serverName"];
//    serverLocationTextField.text=[[serverDetailsArray objectAtIndex:selectedIndexPath.row] valueForKey:@"serverLink"];
//    
//    }
//    
//
//}
//
//#pragma mark UITextField Delegate
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//
//{
//    
//   }
//
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//
//{
//    
//    
//    
//    if (textField==usernameTextField) {
//        
//        [SWDefaults setUsernameForActivate:textField.text];
//        
//           }
//    
//    else if (textField==passwordTextField)
//        
//    {
//        [SWDefaults setPasswordForActivate:textField.text];
//
//    }
//    
//    else if (textField==servernameTextField)
//        
//    {
//        selectedServerName=textField.text;
//        
//    }
//    
//    else if (textField==serverLocationTextField)
//        
//    {
//        selectedServerLink=textField.text;
//
//        
//    }
//
//}
//- (IBAction)serverNameButtonTapped:(id)sender {
//    
//    //NSLog(@"server detaisl array is %@", serverDetailsArray );
//    
//    
//    if ([servernameTextField isFirstResponder]) {
//        
//        [servernameTextField resignFirstResponder];
//    }
//    
//    NSMutableDictionary * localDict=[[NSMutableDictionary alloc]init];
//    
//    [localDict setValue:@"Local Server" forKey:@"serverName"];
//
//    [localDict setValue:@"192.168.100.14:3512" forKey:@"serverLink"];
//
//
//        if ([serverDetailsArray containsObject: localDict]) {
//        
//        
//    }
//    
//    else
//    {
//        
//        [serverDetailsArray insertObject:localDict atIndex:1];
//        
//    }
//
//    
//    
//    NSMutableDictionary * othersDict=[[NSMutableDictionary alloc]init];
//    
//    [othersDict setValue:@"Other" forKey:@"serverName"];
//    
//    [othersDict setValue:@"" forKey:@"serverLink"];
//
//    if ([serverDetailsArray containsObject: othersDict]) {
//
//    }
//    
//    else
//    {
//        [serverDetailsArray addObject:othersDict ];
// 
//    }
//    
//    
//    
//    NSLog(@"server detaisl array is %@", serverDetailsArray );
//
//    
//    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
//    popOverVC.popOverContentArray=[serverDetailsArray valueForKey:@"serverName"];
//    popOverVC.salesWorxPopOverControllerDelegate=self;
//    popOverVC.disableSearch=YES;
//    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
//    popoverController=nil;
//    popoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
//    popoverController.delegate=self;
//    popOverVC.popOverController=popoverController;
//    
//    [popoverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
//    
//  
//    //        [popoverController presentPopoverFromRect:btnFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//    
//    [popoverController presentPopoverFromRect:servernameTextField.dropdownImageView.frame inView:servernameTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//    
//    
//    
//}
//
//- (void)keyboardWillShow:(NSNotification *)notification
//
//{
//    if ([serverLocationTextField isFirstResponder]) {
//        
//        loginDetailsViewTopConstraint.constant=-40;
//        
//        [self.view layoutIfNeeded];
//    }
//}
//
//
//- (void)keyboardWillHide:(NSNotification *)notification
//{
// 
//    loginDetailsViewTopConstraint.constant=40;
//    [self.view layoutIfNeeded];
//
//}
//
//
//- (IBAction)activateButtonTapped:(id)sender {
//    
//    NSMutableDictionary * selectedServerDict=[[NSMutableDictionary alloc]init];
//    
//    [selectedServerDict setValue :selectedServerName forKey : @"serverName"];
//
//    [selectedServerDict setValue :selectedServerLink forKey : @"serverLink"];
//    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
//    NSError *error;
//
//    
//    NSData *dataDict = [jsonSerializer serializeObject:selectedServerDict error:&error];
//    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
//    [SWDefaults setSelectedServerDictionary:stringDataDict];
//
//    
//    
//    [self showActivate];
//    
//
//    
//}
//@end
//
//
///*
// //#pragma mark UIAlertView Delegate
// //- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
// //
// //
// //    if (alertView.tag==1001)
// //    {
// //        if (isActivation)
// //        {
// //#pragma clang diagnostic push
// //#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
// //            [self.target performSelector:self.action withObject:nil];
// //#pragma clang diagnostic pop
// //        }
// //        else
// //        {
// //            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//                   [self.navigationController  popViewControllerAnimated:YES];
// //        }
// //
// //    }
// //}*/





//
//  NewActivationViewController.m
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "NewActivationViewController.h"
#import "SWFoundation.h"
#import "ServerSelectionViewController.h"
#import "SWPlatform.h"
#import "SWSession.h"
#import "SWDatabaseManager.h"
#import <sqlite3.h>
#import "SSKeychain.h"
//#import "SWDashboard/SWDashboard.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "LoginViewController.h"
#import <SalesWorxPopOverViewController.h>
#import "MedRepDefaults.h"
#import <UIAlertView+MedRepAlertView.h>
#import "UIDeviceHardware.h"
#import "SWAppDelegate.h"

@interface NewActivationViewController ()
{
    SWAppDelegate * appdel;

}
@end

@implementation NewActivationViewController

@synthesize alertDisplayed,invalidCredentials,logTextView;

#define kLoginField 1
#define kPasswordField 2
#define kSNameField 3
#define kSLinkField 4



@synthesize target,action,isDemo;
- (id)initWithNoDB {
    self = [super init];
    
    if (self)
    {
        customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
        if ([customerID isEqualToString:@"C16K82563"])
        {
            [self setTitle:@"Preparing Data"];
        }
        else{
            [self setTitle:@"New Activation"];
        }
        
        isRootView=YES;
        isActivation = YES;
        ////NSLog(@"From Appdelegate");
    }
    
    return self;
}

- (id)init {
    self = [super init];
    
    if (self) {
        customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
        isActivation = NO;
        if ([customerID isEqualToString:@"C16K82563"])
        {
            [self setTitle:@"Preparing Data"];
        }
        else{
            [self setTitle:NSLocalizedString(@"New Activation", nil)];
        }
        isRootView=NO;
        ////NSLog(@"From Lofig");
    }
    
    return self;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton=NO;
    syncLocations=[[SyncLoctions alloc]init];
    syncLocationsWebserviceArray=[[NSMutableArray alloc]init];
    
    NSLog(@"is actication %hhd and isrootview %hhd", isActivation,isRootView);
    if ([SharedUtils isConnectedToInternet]==YES) {
        [self fetchSyncLocations];
    }
    
    usernameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    servernameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    serverLocationTextField.autocorrectionType = UITextAutocorrectionTypeNo;

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postSycStatus:) name:@"SyncStatusNotification"  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];

    NSLog(@"version in new activation %@", avid);
    NSString*  ClientVersion =avid;
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    lblVersion.text= [NSString stringWithFormat:@"Version  %@ (%@)",ClientVersion,build];
    
    tymProgressBarView.barBorderWidth=2.0;
    tymProgressBarView.barBorderColor=UIViewControllerBackGroundColor;
    tymProgressBarView.barBackgroundColor=UIViewControllerBackGroundColor;
    tymProgressBarView.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    tymProgressBarView.barInnerBorderColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    tymProgressBarView.usesRoundedCorners=YES;
    
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *testServerArray = [NSMutableArray arrayWithArray:[djsonSerializer deserializeAsArray:data error:&error]  ];
    
   // NSLog(@"server details array is %@",testServerArray );
    NSLog(@"server array is %@", [SWDefaults serverArray]);
    
    serverDetailsArray=[[NSMutableArray alloc]init];
    serverDetailsArray=testServerArray;
    
    UIDeviceHardware *hardware=[[UIDeviceHardware alloc] init];
    NSLog(@"Device is %@",[hardware platformString]);
    
    if ([[hardware platformString] isEqualToString:@"Simulator"]) {
        
              // [self fetchSyncLocations];
        
    }
    
    
    if ([customerID isEqualToString:@"C16K82563"] &&![[hardware platformString] isEqualToString:@"Simulator"])
    {
        _allFieldsView.hidden = YES;
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(450, 40, 533, 475)];
        view.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblMsg = [[UILabel alloc]initWithFrame:CGRectMake(70, 210, 400, 40)];
        lblMsg.text = @"Preparing application for first time use";
        lblMsg.textAlignment = NSTextAlignmentCenter;
        lblMsg.font = MedRepElementDescriptionLabelFont;
        lblMsg.textColor = MedRepDescriptionLabelFontColor;
        [view addSubview:lblMsg];
        [self.view addSubview:view];
        
        login = @"demo";
        password = @"demo123";
        
        [SWDefaults setUsername:login];
        
        NSLog(@"setting user name for demo C15K82563");
        
        
        [SWDefaults setUsernameForActivate:login];
        
        [SWDefaults setPassword:password];
        [SWDefaults setPasswordForActivate:password];
        
        editServerName1 = [serverDict stringForKey:@"name"];
        if ([editServerName1 isEqualToString:@""]) {
            editServerName1 = [serverDict stringForKey:@"serverName"];
            [editServerDict setValue:editServerName1 forKey:@"name"];
        } else {
            [editServerDict setValue:editServerName1 forKey:@"name"];
        }
        selectedServerName = editServerName1;
        
        editServerLink = [serverDict stringForKey:@"url"];
        if ([editServerLink isEqualToString:@""]) {
            editServerLink = [serverDict stringForKey:@"serverLink"];
            [editServerDict setValue:editServerLink forKey:@"url"];
        } else {
            [editServerDict setValue:editServerLink forKey:@"url"];
        }
        selectedServerLink = editServerLink;
        [self performSelector:@selector(showActivate) withObject:nil afterDelay:2.0f];
        
    } else {
        _allFieldsView.hidden = NO;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    appdel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];

    
    [Flurry logEvent:@"Activation View"];
    appData=[DataSyncManager sharedManager];

    serverDict = [NSMutableDictionary dictionary];
    editServerDict = [NSMutableDictionary dictionary ];
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]];
    serverDict =[NSMutableDictionary dictionaryWithDictionary:[testServerDict mutableCopy]];
    
    login = [SWDefaults usernameForActivate];
    password = [SWDefaults passwordForActivate];
    
    editServerName1 = [serverDict stringForKey:@"serverName"];
    editServerLink = [serverDict stringForKey:@"serverLink"];

    editServerName1=servernameTextField.text;
    editServerLink=serverLocationTextField.text;
    
    _footerView = [[UIView alloc] initWithFrame:CGRectMake(100, 0, 400, 400)];
    _footerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIView *secFooterView = [[UIView alloc] initWithFrame:CGRectMake(45, 10, 934, 344)];
    secFooterView.backgroundColor = [UIColor lightTextColor];
    secFooterView.layer.shadowOffset = CGSizeMake(4, 4);
    //secFooterView.layer.shadowRadius = 3;
    // secFooterView.layer.shadowOpacity = 0.2;
    secFooterView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    secFooterView.layer.borderWidth = 1.0;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(10,10, 910, 44.0f)];
    l.font = RegularFontOfSize(20);
    l.textColor = [UIColor darkGrayColor];
    l.text = @"  Sync Status";
    l.backgroundColor = [UIColor lightTextColor];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {
        bacgroundBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_ActProgress_AR"]];
    }
    else
    {
        bacgroundBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_ActProgress"]];
    }
    bacgroundBar.frame = CGRectMake(100,10, 60.0f, 44.0f);
    bacgroundBar.contentMode = UIViewContentModeCenter;
    
    [_footerView addSubview:secFooterView];
    [secFooterView addSubview:l];
    
    logTextView = [[UITextView alloc] initWithFrame:CGRectMake(10,54 , 910, 280)];
    logTextView.scrollEnabled=YES;
    logTextView.editable=NO;
    logTextView.textColor=[UIColor darkGrayColor];
    logTextView.backgroundColor = [UIColor whiteColor];
    [secFooterView addSubview:logTextView];
    
    _footerView.backgroundColor = [UIColor clearColor];
    
    
    
    tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped ];
    
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor whiteColor];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
        
    }
    
    //creating new UI with Xib
    if (isRootView)
    {
        //        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Activate" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
    }
    else
    {
        
//        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Upload Database" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
        
        NSLog(@"server details are %@", [SWDefaults serverArray]);
        
        usernameTextField.enabled=NO;
        passwordTextField.enabled=NO;
        servernameTextField.enabled=NO;
        serverLocationTextField.enabled=NO;
        
        serverLocationTextField.text=[serverDict valueForKey:@"url"];
        servernameTextField.text=[serverDict valueForKey:@"name"];
        
        servernameTextField.userInteractionEnabled=NO;
        serverLocationTextField.userInteractionEnabled=NO;
        usernameTextField.userInteractionEnabled=NO;
        passwordTextField.userInteractionEnabled=NO;
        
        serverNameButton.enabled=NO;
        [activateButton setTitle:@"Upload Database" forState:UIControlStateNormal];
        
        usernameTextField.isReadOnly=YES;
        passwordTextField.isReadOnly=YES;
        
        usernameTextField.text=[SWDefaults usernameForActivate];
        passwordTextField.text=[SWDefaults passwordForActivate];
        comingfromNewActivation=YES;
    }
}

- (void)showActivate
{
    NSLog(@"show activate called");
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (comingfromNewActivation==YES)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    if (internetStatus == NotReachable)
    {
        NSLog(@"internet unavailable");
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Internet unavailable", nil) message:@"please connect to internet and try again" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
        
        alertDisplayed=NO;
        [self.view endEditing:YES];
        
        logTextView.text = @"";
        progressHUD.labelText = @"";
        progressHUD.detailsLabelText = @"";
        progressHUD.progress = 0;
        progressHUD.mode = MBProgressHUDModeIndeterminate;
        
        CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
        CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
        NSError *error;
        NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableArray *testServerArray = [NSMutableArray arrayWithArray:[djsonSerializer deserializeAsArray:data error:&error]  ];
        
       // NSLog(@"server details array is %@",testServerArray );
        NSLog(@"username  for activate %@", [SWDefaults usernameForActivate]);


        if (_allFieldsView.hidden != YES)
        {
            [SWDefaults setUsernameForActivate:usernameTextField.text];
            [SWDefaults setPasswordForActivate:passwordTextField.text];
        }
        else
        {
            [self didSelectPopOverController:nil];
        }
        NSLog(@"selected server name is %@", [SWDefaults selectedServerDictionary]);
        
        serverDetailsArray=[[NSMutableArray alloc]init];
        serverDetailsArray=testServerArray;
        NSLog(@"isrootView %hhd", isRootView);
        
        if (isRootView==YES)
        {
            if([self validateInput])
            {
                for(int i=0 ; i<[testServerArray count] ; i++ )
                {
                    NSDictionary *row = [testServerArray objectAtIndex:i];
                    
                    if([[row stringForKey:@"name"] isEqualToString:[serverDict stringForKey:@"name"]])
                    {
                        
                        if(editServerName1.length > 0)
                        {
                            [serverDict setValue:editServerName1 forKey:@"name"];
                        }
                        
                        if(editServerLink.length > 0)
                        {
                            [serverDict setValue:editServerLink forKey:@"url"];
                        }
                        
                        [testServerArray removeObjectAtIndex:i];
                        //[testServerArray addObject:serverDict];
                        NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
                        NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
                        NSLog(@"setting selected server dict %@", stringDataDict);
                        
                        //[SWDefaults setSelectedServerDictionary:stringDataDict];
                        break;
                    }
                }
                if([serverDict count]==0)
                {
                    serverDict = editServerDict;
                    [testServerArray addObject:serverDict];
                    NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
                    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
                    NSLog(@"setting selected server dict %@", stringDataDict);
                    
                    //[SWDefaults setSelectedServerDictionary:stringDataDict];
                }
                NSData *datas = [jsonSerializer serializeObject:testServerArray error:&error];
                NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
                // [SWDefaults setServerArray:stringData];
                
                NSLog(@"server array is %@", stringData);
                
                if (isRootView)
                {
                    NSLog(@"%@",[SWDefaults usernameForActivate]);
                    if ([SWDefaults usernameForActivate].length > 0 && [SWDefaults passwordForActivate].length > 0)
                    {
                        // [self.view addSubview:self.loadingView];
                        
                        
                        
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        
                        
                        [self.view addSubview:progressHUD];
                        
                        viewSync=nil;
                        viewSync.delegate=nil;
                        viewSync = [[SynViewController alloc] init];
                        viewSync.delegate=self;
                        [viewSync setTarget:self];
                        [viewSync setAction:@selector(activationDone:)];
                        //[self.view addSubview: [DataSyncManager sharedManager].progressHUD];
                        
                        
                        //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        
                        
                        [viewSync setRequestForSyncActivateWithUserName:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate]];
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Please fill all fields", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        [alert show];
                        
                    }
                }
            }
        
        }
        else
        {
            //upload database
            
            [self.view addSubview:progressHUD];
            Singleton *single = [Singleton retrieveSingleton];
            single.isActivated = NO;
            viewSync=nil;
            viewSync.delegate=nil;
            viewSync = [[SynViewController alloc] init];
            viewSync.delegate=self;
            [viewSync setTarget:self];
            [viewSync setAction:@selector(uploadDone:)];
            
            NSLog(@"send request for upload called from new activation");
            
            [viewSync setRequestForSyncUpload];
        }
    }
}
- (void)activationDone:(NSString *)msg
{
    NSLog(@"new activation called");
    
    NSArray* arr=[[NSArray alloc]init];
    arr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat: @"select Username from TBL_User"]];
    
    if ([arr count]>0)
    {
        NSString * userNameStr = [[arr objectAtIndex:0]valueForKey:@"Username"];
        
        if ([userNameStr isEqualToString:@"demo"]) {
            NSLog(@"it is a demo");
        }
        else
        {
            NSLog(@"its not a demo");
            NSLog(@"message %@", msg);
            
            isDemo=NO;
            
            [SSKeychain setPassword:@"registereduser" forService:@"registereduser" account:@"user"];
            
            
            //        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isDemo"];
            //        [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        [SWDefaults setLastSyncStatus:@"N/A"];
        if([msg isEqualToString:@"error"])
        {
            [SWDefaults setIsActivationDone:@"NO"];
            [progressHUD removeFromSuperview];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        else
        {
            [SWDefaults setIsActivationDone:@"YES"];
            NSString *temp =[[SWDatabaseManager retrieveManager] verifyLogin:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate]];
            if ([temp isEqualToString:@"Done"])
            {
                viewSync.delegate=nil;
                viewSync=nil;
                viewSync = [[SynViewController alloc] init];
                [viewSync viewDidLoad];
                viewSync.delegate=self;
                [viewSync setTarget:self];
                [viewSync setAction:@selector(activationCompleted)];
                [viewSync sendRequestForExecWithUserName:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate] andProcName:@"sync_MC_GetLastDocumentReferenceAll" andProcParam:@"SalesRepID" andProcValue:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
            }
            else
            {
                [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:temp delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
            }
            [SWDefaults setActivationStatus:@"YES"];
        }
    }
    else
    {
        if (alertDisplayed==NO)
        {
            invalidAlert=[[UIAlertView alloc]initWithTitle:@"Invalid Credentials" message:@"Login/Server credentials are invalid" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            invalidAlert.tag=2000;
            invalidCredentials=YES;
            alertDisplayed=YES;
            [invalidAlert show];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"ALERT EXISTS IN DISMISS %hhd", [self checkAlertExists]);
    
    [self.view endEditing:YES];
    if ([self checkAlertExists])
    {

    }
    else
    {
        
    }

}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1001)
    {
        if (isActivation) {
            LoginViewController *loginVC = [[LoginViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
            [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(BOOL)checkAlertExists
{
    if ([[UIApplication sharedApplication].keyWindow isMemberOfClass:[UIWindow class]])
    {
        ////There is no alertview present
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)uploadDone:(NSString *)msg
{
    NSLog(@"upload done message is %@", msg);
    [progressHUD removeFromSuperview];
    
    if([msg isEqualToString:@"error"])
    {
        
    }
    else
    {
        isRootView=YES;
        [SWDefaults setIsActivationDone:@"NO"];
        [tableView reloadData];
        
        usernameTextField.isReadOnly=NO;
        passwordTextField.isReadOnly=NO;
        
        [activateButton setTitle:@"Activate" forState:UIControlStateNormal];
        [activateButton addTarget:self action:@selector(showActivate) forControlEvents:UIControlEventTouchUpInside];
        
        
        self.navigationItem.hidesBackButton = YES;
        [SWDefaults setUsernameForActivate:@""];
        [SWDefaults setPasswordForActivate:@""];
        [SWDefaults setPassword:@""];
        [SWDefaults setUsername:@""];
        
        //clear fields
        
        
        serverLocationTextField.text=@"";
        servernameTextField.text=@"";
        
        usernameTextField.text=@"";
        passwordTextField.text=@"";
        
        //reset the status bar
        
        tymProgressBarView.progress=0.0;
        downloadStatusLbl.text =@"Sync Status :";
        
        usernameTextField.enabled=YES;
        passwordTextField.enabled=YES;
        serverNameButton.enabled=YES;
        
        usernameTextField.userInteractionEnabled=YES;
        passwordTextField.userInteractionEnabled=YES;
        servernameTextField.userInteractionEnabled=YES;
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView * uploadSuccessfull=[[UIAlertView alloc]initWithTitle:@"Database Uploaded Successfully" message:@"please enter user credentials to activate new user" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [uploadSuccessfull show];
        
    }
}


-(void)viewDidDisappear:(BOOL)animated
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    [tableView setDelegate:nil];
    [tableView setDataSource:nil];
    tableView=nil;
    popoverController=nil;
    window=nil;
    applicationViewController=nil;
    login=nil;
    password=nil;
    serverDict=nil;
    
    editServerLink=nil;
    editServerName1=nil;
    newServerDict=nil;
    loadingView=nil;
    appData=nil;
    logTextView=nil;
    _footerView=nil;
    bacgroundBar=nil;
    progressHUD=nil;
    progressHUD.delegate=nil;
    [[HttpClient sharedManager].operationQueue cancelAllOperations];
    [viewSync cancelHTTPRequest];
    viewSync.delegate = nil;
    viewSync=nil;

}
- (void)serverChanged:(NSMutableDictionary *)d
{
    serverDict=nil;
    serverDict=d ;
    
    
    editServerLink = [d stringForKey:@"serverLink"];
    editServerName1 = [d stringForKey:@"serverName"];
    [tableView reloadData];
    [popoverController dismissPopoverAnimated:YES];
}


-(void)showUploadAlert
{
    if (isRootView) {
        
    }
    
    else
    {
        UIAlertView * uploadDatabaseAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please tap on upload database to upload database of existing user before activating a new user." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        uploadDatabaseAlert.tag=100;
        [uploadDatabaseAlert show];
    }
}

#pragma mark UIView delegate
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self showUploadAlert];
    
    
    [[HttpClient sharedManager].operationQueue cancelAllOperations];
    [viewSync cancelHTTPRequest];
    viewSync.delegate = nil;
    viewSync=nil;
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(isRootView)
    {
        return 3;
    }
    else
    {
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(isRootView)
    {
        if (section==2)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
    else
    {
        return 2;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    {
        NSString *IDENT = @"SessionCellIdent";
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:IDENT];
        if(cell)
        {
            cell=nil;
        }
        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        if(isRootView)
        {
            if (indexPath.section == 0) {
                if (indexPath.row == 0) {
                    ((SWTextFieldCell *)cell).textField.placeholder = @"username";
                    ((SWTextFieldCell *)cell).textField.delegate=self;
                    ((SWTextFieldCell *)cell).textField.tag=kLoginField;
                    ((SWTextFieldCell *)cell).textField.text = [SWDefaults usernameForActivate];
                    
                    
                } else {
                    ((SWTextFieldCell *)cell).textField.placeholder = @"password";
                    ((SWTextFieldCell *)cell).textField.delegate=self;
                    ((SWTextFieldCell *)cell).textField.secureTextEntry = YES;
                    ((SWTextFieldCell *)cell).textField.tag=kPasswordField;
                    ((SWTextFieldCell *)cell).textField.text = [SWDefaults passwordForActivate];
                    
                    
                }
                ((SWTextFieldCell *)cell).textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            }
            else if (indexPath.section == 1) {
                
                if (indexPath.row == 0) {
                    ((SWTextFieldCell *)cell).textField.placeholder = @"server name";
                    ((SWTextFieldCell *)cell).textField.delegate=self;
                    ((SWTextFieldCell *)cell).textField.tag=kSNameField;
                    ((SWTextFieldCell *)cell).textField.text=[serverDict stringForKey:@"serverName"];
                    
                } else {
                    ((SWTextFieldCell *)cell).textField.placeholder = @"server link";
                    ((SWTextFieldCell *)cell).textField.delegate=self;
                    ((SWTextFieldCell *)cell).textField.tag=kSLinkField;
                    ((SWTextFieldCell *)cell).textField.text=[serverDict stringForKey:@"serverLink"];
                }
            }
            
            else
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENT] ;
                [cell.textLabel setText:NSLocalizedString(@"Server locations", nil)];
                cell.textLabel.font = LightFontOfSize(14);
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            
        }
        else
        {
            if (indexPath.row == 1)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENT] ;
                //[cell.textLabel setText:NSLocalizedString(@"Server locations", nil)];
                [cell.textLabel setText:[serverDict stringForKey:@"serverName"]];
                cell.textLabel.font = LightFontOfSize(14);
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            else
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENT] ;
                cell.textLabel.font = LightFontOfSize(14);
                [cell.textLabel setText:@"Please synchronize existing data prior to activation."];
            }
        }
        return cell;
    }
}
- (void)reloadRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(isRootView)
    {
        if (section == 0) {
            return NSLocalizedString(@"Login", nil);
        }
        else if(section == 1)
        {
            return NSLocalizedString(@"Server", nil);
        }
        else
        {
            return nil;
        }
    }
    else{
        return nil;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if((isRootView && section==2) || !isRootView)
    {
        return _footerView.frame.size.height;
    }
    else
    {
        return  false;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if((isRootView && section==2) || !isRootView)
    {
        return _footerView;
    }
    else
    {
        return  nil;
    }
}
#pragma mark UITableViewDataSource
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view endEditing:YES];
    if(isRootView)
    {
        if (indexPath.section == 0 || indexPath.section == 1 )
        {
            return;
        }
        
        ServerSelectionViewController *serverSelectionViewController = [[ServerSelectionViewController alloc] init] ;
        [serverSelectionViewController setTarget:self];
        [serverSelectionViewController setSelectionChanged :@selector(serverChanged:)];
        [serverSelectionViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:serverSelectionViewController] ;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
        popoverController.delegate=self;
        UITableViewCell *cell = [tv cellForRowAtIndexPath:indexPath];
        [popoverController setDelegate:self];
        [popoverController presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        if (indexPath.row == 1)
        {
            ServerSelectionViewController *serverSelectionViewController = [[ServerSelectionViewController alloc] init] ;
            [serverSelectionViewController setTarget:self];
            [serverSelectionViewController setSelectionChanged :@selector(serverChanged:)];
            [serverSelectionViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:serverSelectionViewController] ;
            popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
            popoverController.delegate=self;
            UITableViewCell *cell = [tv cellForRowAtIndexPath:indexPath];
            [popoverController setDelegate:self];
            [popoverController presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
    }
}
#pragma mark UIPopOver Delegates
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController1{
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    popoverController1=nil;
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([usernameTextField isFirstResponder])
    {
        [usernameTextField resignFirstResponder];
        [passwordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.navigationItem setRightBarButtonItem:nil];
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.tag == kLoginField)
    {
        NSCharacterSet *charc=[NSCharacterSet characterSetWithCharactersInString:@" "];
        login= [login stringByTrimmingCharactersInSet:charc];

        login=textField.text;
        [SWDefaults setUsername:login];
        [SWDefaults setUsernameForActivate:login];
    }
    else if (textField.tag == kPasswordField) {
        password=textField.text;
        [SWDefaults setPassword:textField.text];
        [SWDefaults setPasswordForActivate:textField.text];
        
    }
    else if (textField.tag == kSNameField) {
        editServerName1=textField.text;
        [editServerDict setValue:textField.text forKey:@"serverName"];
        
    }
    else if (textField.tag == kSLinkField) {
        editServerLink=textField.text;
        [editServerDict setValue:textField.text forKey:@"serverLink"];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= UsernameTextFieldLimit && range.length == 0)
    {
        return NO;
    }
    else if (textField==usernameTextField)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else if (textField==servernameTextField)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_ServerName] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else if (textField==serverLocationTextField)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_URL] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else
    {
        return YES;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"alert clicked method called with tag %d", alertView.tag);
    
    if (alertView.tag==2000)
    {
        alertDisplayed=YES;
    }
    if (alertView.tag==1001)
    {
        if (isActivation)
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            //[self.target performSelector:self.action withObject:nil];
            
            //activation compted here push to login
            
            //            LoginViewController *loginVC = [[LoginViewController alloc] init];
            //            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
            //            [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
            //
            //
            //
#pragma clang diagnostic pop
        }
        else
        {
            
            NSLog(@"check flags %hhd %hhd", isActivation, isRootView);
            
            NSLog(@"removing superviews in alert method");
            
            //[self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController  popViewControllerAnimated:YES];
        }
    }
}
- (void)activationCompleted
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
    
    if ([customerID isEqualToString:@"C16K82563"])
    {
        if (isActivation)
        {
            LoginViewController *loginVC = [[LoginViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
            [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"Activation completed successfully" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        alert.tag=1001;
        [alert show];
    }
    
    [viewSync cancelHTTPRequest];
    viewSync.delegate = nil;
    viewSync=nil;
    [progressHUD removeFromSuperview];
    
}
- (BOOL)validateInput{
    
    if (login.length > 0 && password.length > 0 && editServerName1.length > 0 && editServerLink.length > 0 )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)updateActivationTextDelegate:(NSString *)synchDate
{
    logTextView.font = LightFontOfSize(14.0f);
    logTextView.text = [logTextView.text stringByAppendingString:synchDate];
    CGPoint p = [logTextView contentOffset];
    [logTextView setContentOffset:p animated:YES];
    [logTextView scrollRangeToVisible:NSMakeRange([logTextView.text length], 0)];
}
- (void)updateProgressDelegateWithType:(MBProgressHUDMode)type andTitle:(NSString *)title andDetail:(NSString *)detail andProgress:(float)progress{
    progressHUD.mode=type;
    progressHUD.labelText=title;
    progressHUD.detailsLabelText=detail;
    progressHUD.progress=progress;
    
    //update task and status here
    
   // NSLog(@"TASK AND PROGRESS IS %@  %@ % 0.2f",progressHUD.labelText, progressHUD.detailsLabelText, progressHUD.progress);
    tymProgressBarView.progress=progress;
    if (progress==100.00)
    {
        activationProgressView.progress=progressHUD.progress;
        activationProgressView.progress=0;
    }
    activationProgressView.progress=progressHUD.progress;
}

- (void)postSycStatus:(NSNotification *)notification
{
    NSLog(@"*******SYC STATUS NOTIFICATION NEW ACTIVATION ********** %@", [notification object]);
    
    if ([[notification object]isEqualToString:@"Order reference has been retrieved"])
    {
        downloadStatusLbl.text=@"Activation Completed Successfully";
    }
    else
    {
        downloadStatusLbl.text=[notification object];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}


#pragma mark Server Location button Methods

- (IBAction)syncLocationButtonTapped:(id)sender
{
   // NSLog(@"server details array is %@",serverDetailsArray );
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=[serverDetailsArray valueForKey:@"serverName"];
    
    
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    popoverController=nil;
    popoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    popoverController.delegate=self;
    popOverVC.popOverController=popoverController;
    
    [popoverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    
    CGFloat X = synLocationDropDownImage.frame.origin.x + [synLocationDropDownImage superview].frame.origin.x;
    CGFloat Y = synLocationDropDownImage.frame.origin.y + [synLocationDropDownImage superview].frame.origin.y;
    CGRect btnFrame = CGRectMake(X, Y, 18, 9);
    
    NSLog(@"frame is %@", NSStringFromCGRect(btnFrame));
    [popoverController presentPopoverFromRect:btnFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark select popover controller method

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    @try {
        syncLocations=[syncLocationsWebserviceArray objectAtIndex:selectedIndexPath.row];
    }
    @catch (NSException *exception)
    {
        @try {
            syncLocations=[syncLocationsWebserviceArray objectAtIndex:0];
        }
        @catch (NSException *exception) {
            
        }
    }
    NSLog(@"selected popover is %@",syncLocations.name );
    
    NSMutableDictionary* selectedServerDictionary=[[NSMutableDictionary alloc]init];
    [selectedServerDictionary setValue:syncLocations.name forKey:@"name"];
    [selectedServerDictionary setValue:syncLocations.url forKey:@"url"];
    [selectedServerDictionary setValue:syncLocations.seq forKey:@"seq"];
    
    servernameTextField.text=syncLocations.name;
    serverLocationTextField.text=syncLocations.url;
    selectedServerName=servernameTextField.text;
    
    editServerName1=servernameTextField.text;
    editServerLink=syncLocations.url;
    
    NSError * error;
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    
    NSData *dataDict = [jsonSerializer serializeObject:selectedServerDictionary error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
    
    NSLog(@"setting selected server dict in popover selection %@", stringDataDict);
    
    [SWDefaults setSelectedServerDictionary:stringDataDict];
    
    [SWDefaults setActivatedServerDetailsDictionary:stringDataDict];

    NSData *datas = [jsonSerializer serializeObject:copiedArray error:&error];
    NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
    [SWDefaults setServerArray:stringData];
}

#pragma mark UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==usernameTextField)
    {
        [SWDefaults setUsernameForActivate:textField.text];
    }
    else if (textField==passwordTextField)
    {
        [SWDefaults setPasswordForActivate:textField.text];
    }
    
    else if (textField==servernameTextField)
    {
        selectedServerName=textField.text;
        editServerName1=textField.text;
        
        [servernameTextField resignFirstResponder];
        [serverLocationTextField becomeFirstResponder];
    }
    else if (textField==serverLocationTextField)
    {
        selectedServerLink=textField.text;
        editServerLink=textField.text;
    }
}
- (IBAction)serverNameButtonTapped:(id)sender
{
    
    //NSLog(@"server detaisl array is %@", serverDetailsArray );
    
    
//    
//    NSDictionary *dictRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SalesWorxServerList" ofType:@"plist"]];
//    
//    
//    NSArray *arrayList = [NSArray arrayWithArray:[dictRoot objectForKey:@"Server List"]];
//    
//    NSLog(@"server List is %@", [arrayList description]);
//    
//    
//    [arrayList enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
//        
//        
//        NSLog(@"Server name : %@",[obj valueForKey:@"Server_Name"]);
//        NSLog(@"Server url   : %@",[obj valueForKey:@"Server_Url"]);
//    }];
//
//    
//    
//    serverDetailsArray=[[NSMutableArray alloc]init];
//    
//    serverDetailsArray=[arrayList mutableCopy];
//    
    
    if ([servernameTextField isFirstResponder])
    {
        [servernameTextField resignFirstResponder];
    }
  
    
    /*
    NSMutableDictionary * localDict=[[NSMutableDictionary alloc]init];
    
    [localDict setValue:@"Local Server" forKey:@"serverName"];
    
    [localDict setValue:@"192.168.100.14:3512" forKey:@"serverLink"];
    
    
    if ([serverDetailsArray containsObject: localDict]) {
        
        
    }
    
    else
    {
        
        [serverDetailsArray insertObject:localDict atIndex:1];
        
    }
    */
    
    
    
//    NSMutableDictionary * othersDict=[[NSMutableDictionary alloc]init];
//    
//    [othersDict setValue:@"Other" forKey:@"serverName"];
//    
//    [othersDict setValue:@"" forKey:@"serverLink"];
//    
//    if ([serverDetailsArray containsObject: othersDict]) {
//        
//    }
//    
//    else
//    {
//        [serverDetailsArray addObject:othersDict ];
//        
//    }
//    
//    
    
   // NSLog(@"server details array is %@", serverDetailsArray );
    
    
    
//    
//    if ([SharedUtils isConnectedToInternet]==YES) {
//        [self fetchSyncLocations];
//        
//    }
//    
    
    if (syncLocationsWebserviceArray.count>0)
    {
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray=syncLocationsWebserviceArray;
        
        popOverVC.popoverType=@"Activation";
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        popoverController=nil;
        popoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        popoverController.delegate=self;
        popOverVC.popOverController=popoverController;
        
        [popoverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
        
        //        [popoverController presentPopoverFromRect:btnFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        [popoverController presentPopoverFromRect:servernameTextField.dropdownImageView.frame inView:servernameTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    else
    {
        UIAlertView * noLocationsAlert=[[UIAlertView alloc]initWithTitle:@"No Locations Available" message:@"Please connect to internet and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noLocationsAlert show];
    }
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if ([serverLocationTextField isFirstResponder])
    {
        loginDetailsViewTopConstraint.constant=-40;
        [self.view layoutIfNeeded];
    }
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    loginDetailsViewTopConstraint.constant=40;
    [self.view layoutIfNeeded];
}


- (IBAction)activateButtonTapped:(id)sender
{
//    NSMutableDictionary * selectedServerDict=[[NSMutableDictionary alloc]init];
//    
//    [selectedServerDict setValue :selectedServerName forKey : @"serverName"];
//    
//    [selectedServerDict setValue :selectedServerLink forKey : @"serverLink"];
//    
//    editServerName1=selectedServerName;
//    editServerLink=selectedServerLink;
//
//    
//    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
//    NSError *error;
//    

    
//    NSData *dataDict = [jsonSerializer serializeObject:selectedServerDict error:&error];
//    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
//    
    
    [self showActivate];
}

#pragma mark Sync Location web service


-(void)fetchSyncLocations
{
    //do stuff
    customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    NSString* syncUrlString=[NSString stringWithFormat:@"http://www.ucssolutions.com/licman/get-sync-locations.php?cid=%@&avid=%@",customerID,@"19"];
    
    NSMutableURLRequest * syncUrlRequest=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:syncUrlString]];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:syncUrlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    
    {
        // do something with the data
        
        NSError* dataerror;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&dataerror];
        
        copiedArray = json.copy;
        NSLog(@"copied array is %@", copiedArray);
        
        if (!dataerror)
        {
            for (NSInteger i=0; i<copiedArray.count; i++) {
                
                NSMutableDictionary * currentDict=[copiedArray objectAtIndex:i];
                
                syncLocations=[SyncLoctions new];
                
                syncLocations.name=[currentDict valueForKey:@"name"];
                syncLocations.seq=[currentDict valueForKey:@"seq"];
                syncLocations.url=[currentDict valueForKey:@"url"];
                
                [syncLocationsWebserviceArray addObject:syncLocations];
                
               // NSLog(@"webservices array is %@", currentDict);
                
                
                if (_allFieldsView.hidden != YES) {
                    [SWDefaults setUsernameForActivate:usernameTextField.text];
                    [SWDefaults setPasswordForActivate:passwordTextField.text];
                }
                
                //                NSError * error;
                //
                CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
                //
                //                NSData *dataDict = [jsonSerializer serializeObject:currentDict error:&error];
                //                NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
                //
                //                NSLog(@"setting selected server dict in fetchSyncLocations %@", stringDataDict);
                //
                //                [SWDefaults setSelectedServerDictionary:stringDataDict];
                //
                
                
              //  NSLog(@"setting server array %@", copiedArray);
                
                NSData *datas = [jsonSerializer serializeObject:copiedArray error:&error];
                NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
                [SWDefaults setServerArray:stringData];
            }
            
        }
    }];
    [dataTask resume];
}

@end
