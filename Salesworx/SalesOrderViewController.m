
////
////  SalesOrderViewController.m
////  SWCustomer
////
////  Created by Irfan Bashir on 7/11/12.
////  Copyright (c) 2012 UCS Solutions. All rights reserved.
////
//
//#import "SalesOrderViewController.h"
//#import "ProductOrderViewController.h"
//#import "Singleton.h"
//#import "ProductSaleCategoryViewController.h"
//#import "TamplateNameViewController.h"
//#include <CommonCrypto/CommonHMAC.h>
//
//#define NUMERIC  @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890 "
//
//@interface SalesOrderViewController ()
//
//@end
//
//@implementation SalesOrderViewController
//@synthesize delegate;
//
////-(void)dealloc
////{
////    NSLog(@"SalesOrderViewController");
////}
////@synthesize manageOrder,category;
//
//- (id) initWithCustomer:(NSDictionary *)c andCategory:(NSDictionary *)categorya{
//    self = [super init];
//    if (self)
//    {
//        customer = [NSMutableDictionary dictionaryWithDictionary:c];
//        category = [NSDictionary dictionaryWithDictionary:categorya];
//        
//        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
//        [self setItems:[NSMutableArray array]];
//        Singleton *single = [Singleton retrieveSingleton];
//        single.isManageOrder=NO;
//        single.manageOrderRefNumber = @"SO";
//        isManageOrder = NO;
//        
//
//        mslOrderItem = [[UIButton alloc] init] ;
//        [mslOrderItem setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//        [mslOrderItem setFrame:CGRectMake(self.view.frame.size.width-400,self.gridView.bounds.size.height + 50, 140, 42)];
//        [mslOrderItem addTarget:self action:@selector(mslItems:) forControlEvents:UIControlEventTouchUpInside];
//        [mslOrderItem bringSubviewToFront:self.view];
//        mslOrderItem.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//        [mslOrderItem setTitle:NSLocalizedString(@"Load from MSL", nil) forState:UIControlStateNormal];
//        mslOrderItem.titleLabel.font = BoldSemiFontOfSize(15);
//        [self.view addSubview:mslOrderItem];
//        
//        if (!single.isDistributionChecked)
//        {
//        }
//        else
//        {
//            [mslOrderItem setEnabled:NO];
//        }
//        
//        
//
//    }
//    return self;
//}


//- (id) initWithCustomer:(NSDictionary *)c andOrders:(NSDictionary *)order andManage:(NSString *)manage{
//    self = [super init];
//    if (self)
//    {
//        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
//        
//        Singleton *single = [Singleton retrieveSingleton];
//        customer = [NSMutableDictionary dictionaryWithDictionary:c];
//        category = [NSDictionary dictionary];
//        selectedOrder=[NSMutableDictionary dictionaryWithDictionary:order] ;
//        savedCategoryID = [selectedOrder stringForKey:@"Custom_Attribute_2"];
//        manageOrder = manage;
//
//        if (![customer objectForKey:@"Ship_Customer_ID"])
//        {
//            NSString *shipCustomer = [customer stringForKey:@"Customer_ID"];
//            NSString *shipSite = [customer stringForKey:@"Site_Use_ID"];
//            [customer setValue:shipCustomer forKey:@"Ship_Customer_ID"];
//            [customer setValue:shipSite forKey:@"Ship_Site_Use_ID"];
//        }
//        
//        [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
//        
//        if([selectedOrder objectForKey:@"Schedule_Ship_Date"])
//        {
//            [self dbGetSalesOrderServiceDiddbGetConfirmOrderItems:[[SWDatabaseManager retrieveManager] dbGetSalesConfirmOrderItems:[selectedOrder objectForKey:@"Orig_Sys_Document_Ref"]]];
//        }
//        else
//        {
//            //Saad Line
//            [self dbGetSalesOrderServiceDiddbGetPerformaOrderItems:[[SWDatabaseManager retrieveManager] dbGetSalesPerformaOrderItems:[selectedOrder objectForKey:@"Orig_Sys_Document_Ref"]]];
//        }
//        
//        single.isManageOrder=YES;
//        isManageOrder = YES;
//        single.manageOrderRefNumber = [selectedOrder objectForKey:@"Orig_Sys_Document_Ref"];
//    }
//    return self;
//}
//
//- (id) initWithCustomer:(NSDictionary *)c andTemplate:(NSDictionary *)order andTemplateItem:(NSArray *)orderItems{
//    self = [super init];
//    if (self)
//    {
//        
//        
//        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
//        customer = [NSMutableDictionary dictionaryWithDictionary:c];
//        category = [NSDictionary dictionary];
//        selectedOrder=[NSMutableDictionary dictionaryWithDictionary:order] ;
//        savedCategoryID = [order stringForKey:@"Custom_Attribute_1"];
//
//
//        name_TEMPLATES = [order stringForKey:@"Template_Name"];
//        
//        //serProduct.delegate=self;
//        
//        [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
//        [self setItems:orderItems];
//        [self.gridView reloadData];
//        [self updateTotal];
//        
//    }
//    return self;
//}
//- (void) viewWillAppear:(BOOL)animated{
//    
//    [Flurry logEvent:@"Sales Order  View"];
//    //[Crittercism leaveBreadcrumb:@"<Sales Order View>"];
//    self.navigationController.toolbarHidden=YES;
//}
//- (void) viewDidLoad
//{
//    [super viewDidLoad];
//  
//    gridView.frame=CGRectMake(0, 40, self.view.bounds.size.width, self.view.bounds.size.height-100);
//    //gridView.frame=CGRectMake(0, 60, self.view.bounds.size.width, 100);
//    self.view.backgroundColor = UIColorFromRGB(0xE0E5EC);
//    //    self.view.backgroundColor = [UIColor redColor ];
//    
//    
//    productDict = [NSMutableDictionary dictionary];
//    arrayTemp = [NSMutableArray array];
//    tempSalesArray = [NSMutableArray array];
//    p = [NSMutableDictionary dictionary];
//    b = [NSMutableDictionary dictionary ];
//    f = [NSMutableDictionary dictionary ];
//    deleteRowDict = [NSMutableDictionary dictionary ];
//    
//   Singleton *single = [Singleton retrieveSingleton];
//    single.isSaveOrder = NO;
//    
//    
//   // CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60) andCustomer:customer] ;
////    [headerView setShouldHaveMargins:YES];
//    //[self.view addSubview:headerView];
//    
//    saveButton = [[UIButton alloc] init] ;
//    [saveButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//    [saveButton setFrame:CGRectMake(self.view.frame.size.width-120,self.gridView.bounds.size.height + 50, 120, 42)];
//    [saveButton addTarget:self action:@selector(saveOrder:) forControlEvents:UIControlEventTouchUpInside];
//    [saveButton bringSubviewToFront:self.view];
//    saveButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [saveButton setTitle:NSLocalizedString(@"Save Order", nil) forState:UIControlStateNormal];
//    saveButton.titleLabel.font = BoldSemiFontOfSize(15);
//    [self.view addSubview:saveButton];
//    
//    saveAsTemplateButton = [[UIButton alloc] init] ;
//    [saveAsTemplateButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//    [saveAsTemplateButton setFrame:CGRectMake(self.view.frame.size.width-250,self.gridView.bounds.size.height + 50, 120, 42)];
//    [saveAsTemplateButton addTarget:self action:@selector(saveTemplate:) forControlEvents:UIControlEventTouchUpInside];
//    [saveAsTemplateButton bringSubviewToFront:self.view];
//    saveAsTemplateButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [saveAsTemplateButton setTitle:NSLocalizedString(@"Save Template", nil) forState:UIControlStateNormal];
//    saveAsTemplateButton.titleLabel.font =BoldSemiFontOfSize(15);
//    [self.view addSubview:saveAsTemplateButton];
//    
//    //gridView.userInteractionEnabled=NO;
//    //[self.gridView.tableView setTableHeaderView:headerView];
//    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeOrder)] ];
//    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
//    {
//    }
//    else
//    {
//        // [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentProductList:)] ];
//    }
//    
//    [self.gridView setShouldAllowDeleting:YES];
//    
//    UILabel *orderItemLbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)] ;
//    [orderItemLbl setText:@"Order Items"];
//    [orderItemLbl setTextAlignment:NSTextAlignmentCenter];
//    [orderItemLbl setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
//    [orderItemLbl setTextColor:[UIColor darkGrayColor]];
//    [orderItemLbl setFont:BoldSemiFontOfSize(20.0)];
//    [self.view addSubview:orderItemLbl];
//    
//    totalProduct=[[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-200, 0, 180, 40)] ;
//    [totalProduct setText:@"0 item(s)"];
//    [totalProduct setTextAlignment:NSTextAlignmentCenter];
//    [totalProduct setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
//    [totalProduct setTextColor:[UIColor darkGrayColor]];
//    [totalProduct setFont:RegularFontOfSize(20.0)];
//    [self.view addSubview:totalProduct];
//    
//    totalPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,self.gridView.bounds.size.height + 65, 300, 30)] ;
//    [totalPriceLabel setText:@""];
//    [totalPriceLabel setTextAlignment:NSTextAlignmentLeft];
//    [totalPriceLabel setBackgroundColor:[UIColor clearColor]];
//    [totalPriceLabel setTextColor:[UIColor darkGrayColor]];
//    [totalPriceLabel setFont:BoldSemiFontOfSize(20.0)];
//    totalPriceLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [self.view addSubview:totalPriceLabel];
//    
//    AppControl *appControl = [AppControl retrieveSingleton];
//    ENABLE_TEMPLATES = appControl.ENABLE_TEMPLATES;
//    [self updateTotal];
//    
//}
//- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if (buttonIndex==0)
//    {
//        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//                   [self.navigationController  popViewControllerAnimated:YES];
//    }
//}
//- (void) closeOrder
//{
//    // exit(1);
//    
//    if([self.items count] > 0 )
//    {
//       Singleton *single = [Singleton retrieveSingleton];
//        if(single.isSaveOrder)
//        {
//            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//                   [self.navigationController  popViewControllerAnimated:YES];
//        }
//        else {
//            if([manageOrder isEqualToString:@"confirmed"])
//            {
//                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//                   [self.navigationController  popViewControllerAnimated:YES];
//            }
//            else
//            {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:@"Would you like to cancel the order without saving?" delegate:self cancelButtonTitle:NSLocalizedString(@"Yes", nil) otherButtonTitles:NSLocalizedString(@"No", nil) ,nil];
//                [alert show];
//                
////                [ILAlertView showWithTitle:NSLocalizedString(@"Warning", nil)
////                                   message:@"Would you like to cancel the order without saving?"
////                          closeButtonTitle:NSLocalizedString(@"No", nil)
////                         secondButtonTitle:NSLocalizedString(@"Yes", nil)
////                       tappedButtonAtIndex:^(NSInteger buttonIndex) {
////                           if (buttonIndex==1)
////                           {
////                               [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////                   [self.navigationController  popViewControllerAnimated:YES];
////                           }
////                       }
////                 ];
//                
//            }
//            
//        }
//    }
//    else
//    {
//        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//                   [self.navigationController  popViewControllerAnimated:YES];
//    }
//}
//- (void) presentProductList:(id)sender{
//   Singleton *single = [Singleton retrieveSingleton];
//    
//    if ([single.visitParentView isEqualToString:@"SO"])
//    {
//        [SWDefaults setProductCategory:category];
//        [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
//    }
//    else
//    {
//        [self categorySelected:category];
//    }
//    NSLog(@"setProductCategory 2");
//
//}
//#pragma mark Product Service Delegate
//- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories {
//    //categoryArray=categories;
//    
//    categoryArray = [NSMutableArray arrayWithArray:categories];
//    
//	for(int idx = 0; idx < categories.count; idx++)
//    {
//        NSDictionary *row = [categoryArray objectAtIndex:idx];
//        
//        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID]) {
//            category=[NSDictionary dictionaryWithDictionary:row];
//            [SWDefaults setProductCategory:category];
//            break;
//        }
//    }
//    
//    //NSLog(@"setProductCategory 3 %@",[SWDefaults productCategory]);
//
//    
//    //    productListViewController = [[ProductListViewController alloc] initWithCategory:category] ;
//    //    [productListViewController setTarget:self];
//    //    [productListViewController setAction:@selector(productSelected:)];
//    //    navigationControllerS = [[UINavigationController alloc] initWithRootViewController:productListViewController] ;
//    //    productListViewController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
//    categories=nil;
//}
//- (void) categorySelected:(NSDictionary *)product{
//    
//    [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
//}
//- (void) presentProductOrder:(NSDictionary *)product {
//    
//    if (![product objectForKey:@"Guid"])
//    {
//        [product setValue:[NSString createGuid] forKey:@"Guid"];
//    }
//    if (isManageOrder)
//    {
//        if(![product objectForKey:@"ItemID"])
//        {
//            [product setValue:[product stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
//        }
//    }
//    [self.delegate selectedProduct:[product mutableCopy]];
//    //    orderViewController = [[ProductOrderViewController alloc] initWithProduct:product] ;
//    //    [orderViewController setTarget:self];
//    //    [orderViewController setAction:@selector(productAdded:)];
//    //    [self.navigationController pushViewController:orderViewController animated:YES];
//    //    orderViewController = nil ;
//    product=nil;
//}
//#pragma mark ProductListView Controller action
//- (void) productSelected:(NSDictionary *)product{
//    productDict = [NSMutableDictionary dictionaryWithDictionary:product];
//    //serProduct.delegate=self;
//    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[product stringForKey:@"ItemID"]]];
//    productDict=nil;
//}
//- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
//    if([priceDetail count]!=0)
//    {
//        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customer stringForKey:@"Price_List_ID"]])
//        {
//            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
//            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
//            //[self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.50f];
//        }
//        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
//        {
//            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
//            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
//            //[self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.50f];
//        }
//        else
//        {
//            // no price . stop here
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//            [alert show];
////            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                               message:@"No prices availble for selected product."
////                      closeButtonTitle:NSLocalizedString(@"OK", nil)
////                     secondButtonTitle:nil
////                   tappedButtonAtIndex:nil];
//            
//            
//        }
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//        
////        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                           message:@"No prices availble for selected product."
////                  closeButtonTitle:NSLocalizedString(@"OK", nil)
////                 secondButtonTitle:nil
////               tappedButtonAtIndex:nil];
//        
//    }
//    priceDetail=nil;
//}
//- (void)mslItems:(id)sender
//{
//    Singleton *single = [Singleton retrieveSingleton];
//    
//    NSMutableArray *distItem = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Inventory_Item_ID , B.Category,B.Organization_ID  FROM TBL_Distribution_Check_Items AS A , TBL_Product AS B WHERE A.Inventory_Item_ID = B.Inventory_Item_ID AND A.DistributionCheck_ID = '%@' AND A.Is_Available='N' ",single.distributionRef]];
//    NSMutableArray *tempArray = [NSMutableArray new];
//    BOOL isCategory=NO;
//    for (int i=0 ; i<[distItem count]; i++)
//    {
//        if ([[[distItem objectAtIndex:i] stringForKey:@"Category"] isEqualToString:[[SWDefaults productCategory] stringForKey:@"Category"]])
//        {
//            [tempArray addObject:[distItem objectAtIndex:i]];
//            isCategory=YES;
//        }
//    }
//    
//    NSMutableArray *tempArray1 = [NSMutableArray new];
//    
//    for (int i = 0 ; i<tempArray.count; i++)
//    {
//        NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[[tempArray objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] organizationId:[[tempArray objectAtIndex:i] stringForKey:@"Organization_ID"]];
//        NSMutableDictionary *tempDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
//        NSString *itemID = [tempDict stringForKey:@"Inventory_Item_ID"];
//        [tempDict setValue:itemID forKey:@"ItemID"];
//        [tempDict setValue:@"1" forKey:@"Qty"];
//        [tempDict setValue:[NSString createGuid] forKey:@"Guid"];
//        [tempDict setValue:@"N" forKey:@"priceFlag"];
//        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
//        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
//        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Discounted_Price"];
//        
//        [tempArray1 addObject:tempDict];
//    }
//    
//    if (isCategory) {
//        //single.isDistributionChecked = NO;
//        single.isDistributionItemGet = YES;
//        [mslOrderItem setEnabled:NO];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:@"No MSL items are available in this product category." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//    }
//    
//    NSMutableArray *templateItemsArray = [NSMutableArray array];
//    BOOL isBonusDone;
//    for(int i = 0; i < tempArray1.count; i++)
//    {
//        NSMutableDictionary *iTemDict= [tempArray1 objectAtIndex:i];
//        NSMutableArray *bonusArray = [NSMutableArray arrayWithArray:[[[SWDatabaseManager retrieveManager] dbGetBonusInfoOfProduct:[iTemDict stringForKey:@"Item_Code"]] mutableCopy]];
//        int quantity = 0;
//        quantity = [[iTemDict objectForKey:@"Qty"] intValue];
//        int bonus = 0;
//        isBonusDone=NO;
//      //  NSLog(@"NAme %@",[iTemDict stringForKey:@"Description"]);
//        if([bonusArray count]!=0)
//        {
//            for(int j=0 ; j<[bonusArray count] ; j++ )
//            {
//                NSDictionary *row = [bonusArray objectAtIndex:j];
//                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
//                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
//                if (quantity >= rangeStart && quantity <=rangeEnd)
//                {
//                    //NSMutableDictionary *bonusProduct=[NSMutableDictionary new];
//                    if(j == [bonusArray count]-1)
//                    {
//                        if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
//                        {
//                            int dividedValue = quantity / rangeStart ;
//                            bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
//                            
//                            NSString *childGUID = [NSString createGuid];
//                            
//                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
//                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
//                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
//                            [bonusProduct setValue:childGUID forKey:@"Guid"];
//                            //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
//                            [bonusProduct setValue:@"0" forKey:@"Price"];
//                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
//                            
//                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
//                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
//                            // [iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
//                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
//                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
//                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
//                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
//                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
//                            [templateItemsArray addObject:iTemDict];
//                            [templateItemsArray addObject:bonusProduct];
//                            isBonusDone=YES;
//                        }
//                        
//                        else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
//                        {
//                            int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
//                            bonus = floor(dividedValue) ;
//                            
//                            NSString *childGUID = [NSString createGuid];
//                            
//                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
//                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
//                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
//                            [bonusProduct setValue:childGUID forKey:@"Guid"];
//                           // [bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
//                            [bonusProduct setValue:@"0" forKey:@"Price"];
//                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
//                            
//                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
//                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
//                            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
//                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
//                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
//                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
//                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
//                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
//                            [templateItemsArray addObject:iTemDict];
//                            [templateItemsArray addObject:bonusProduct];
//                            isBonusDone=YES;
//                        }
//                        
//                    }
//                    else
//                    {
//                        bonus = [[row objectForKey:@"Get_Qty"] intValue];
//                        NSString *childGUID = [NSString createGuid];
//                        
//                        NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
//                        [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
//                        [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
//                        [bonusProduct setValue:childGUID forKey:@"Guid"];
//                        //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
//                        [bonusProduct setValue:@"0" forKey:@"Price"];
//                        [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
//                        
//                        [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
//                        [iTemDict setValue:@"N"   forKey:@"priceFlag"];
//                        //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
//                        int qty = [[iTemDict stringForKey:@"Qty"] intValue];
//                        double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
//                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
//                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
//                        [iTemDict setValue:childGUID forKey:@"ChildGuid"];
//                        [templateItemsArray addObject:iTemDict];
//                        [templateItemsArray addObject:bonusProduct];
//                        isBonusDone=YES;
//                    }
//                }
//                
//            }
//            if (!isBonusDone)
//            {
//                [iTemDict setValue:@"N"   forKey:@"priceFlag"];
//                //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
//                int qty = [[iTemDict stringForKey:@"Qty"] intValue];
//                double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
//                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
//                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
//                [templateItemsArray addObject:iTemDict];
//            }
//        }
//        else
//        {
//            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
//            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
//            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
//            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
//            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
//            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
//            [templateItemsArray addObject:iTemDict];
//        }
//    }
//    
//    
//    
//    self.items = templateItemsArray;
//    
//    [self.gridView reloadData];
//    [self updateTotal];
//    
//    
//    
//}
//- (void) productAdded:(NSMutableDictionary *)q{
//    
//    arrayTemp=nil;
//    p=nil;
//    
//    
//    arrayTemp= [NSMutableArray arrayWithArray:self.items];
//    p=q;
//    if([[q stringForKey:@"Qty"] isEqualToString:@"0"] || [[q stringForKey:@"Qty"] isEqualToString:@""])
//    {
//        for (int i = arrayTemp.count - 1; i >= 0; i--)
//        {
//            dataItemArray=nil;
//            dataItemArray = [self.items objectAtIndex:i];
//            if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
//            {
//                [arrayTemp removeObjectAtIndex:i];
//            }
//            else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
//            {
//                [arrayTemp removeObjectAtIndex:i];
//            }
//            else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid_1"]])
//            {
//                [arrayTemp removeObjectAtIndex:i];
//            }
//        }
//    }
//    else
//    {
//        @autoreleasepool
//        {
//            for (int i = arrayTemp.count - 1; i >= 0; i--)
//            {
//                dataItemArray=nil;
//                dataItemArray = [self.items objectAtIndex:i];
//                if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
//                {
//                    [arrayTemp removeObjectAtIndex:i];
//                }
//                else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
//                {
//                    [arrayTemp removeObjectAtIndex:i];
//                }
//                else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid_1"]])
//                {
//                    [arrayTemp removeObjectAtIndex:i];
//                }
//            }
//        }
//        
//        if ([p objectForKey:@"Bonus_Product"])
//        {
//            b=nil;
//            b = [p objectForKey:@"Bonus_Product"];
//            [b setValue:[NSString createGuid] forKey:@"Guid"];
//            [b setValue:@"0" forKey:@"Price"];
//            [b setValue:@"0" forKey:@"Bonus"];
//            [b setValue:@"0" forKey:@"Discounted_Price"];
//            [b setValue:@"0" forKey:@"DiscountPercent"];
//            [b setValue:@"F" forKey:@"priceFlag"];
//            
//            [p setValue:[b stringForKey:@"Guid"] forKey:@"ChildGuid"];
//            [b setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
//            [p setValue:@"N" forKey:@"priceFlag"];
//            
//            if([p objectForKey:@"FOC_Product"])
//            {
//                f=nil;
//                f = [p objectForKey:@"FOC_Product"];
//                [f setValue:[NSString createGuid] forKey:@"Guid"];
//                [f setValue:@"0" forKey:@"Price"];
//                [f setValue:@"0" forKey:@"Bonus"];
//                [f setValue:@"0" forKey:@"Discounted_Price"];
//                [f setValue:@"0" forKey:@"DiscountPercent"];
//                [f setValue:@"M" forKey:@"priceFlag"];
//                [p setValue:[f stringForKey:@"Guid"] forKey:@"ChildGuid_1"];
//                [f setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
//                [arrayTemp addObject:p];
//                [arrayTemp addObject:b];
//                [arrayTemp addObject:f];
//            }
//            else
//            {
//                [arrayTemp addObject:p];
//                [arrayTemp addObject:b];
//            }
//        }
//        else if([p objectForKey:@"FOC_Product"])
//        {
//            f=nil;
//            f = [p objectForKey:@"FOC_Product"];
//            [f setValue:[NSString createGuid] forKey:@"Guid"];
//            [f setValue:@"0" forKey:@"Price"];
//            [f setValue:@"0" forKey:@"Bonus"];
//            [f setValue:@"0" forKey:@"Discounted_Price"];
//            [f setValue:@"0" forKey:@"DiscountPercent"];
//            [f setValue:@"M" forKey:@"priceFlag"];
//            [p setValue:[f stringForKey:@"Guid"] forKey:@"ChildGuid"];
//            [f setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
//            [p setValue:@"N" forKey:@"priceFlag"];
//            
//            [arrayTemp addObject:p];
//            [arrayTemp addObject:f];
//        }
//        else
//        {
//            [p setValue:@"N" forKey:@"priceFlag"];
//            [arrayTemp addObject:p];
//        }
//    }
//    
//    [self setItems:arrayTemp];
//    [self.gridView reloadData];
//    [self updateTotal];
//    arrayTemp=nil;
//    p=nil;
//    b=nil;
//    f=nil;
//    q=nil;
//    
//    [totalProduct setText:[NSString stringWithFormat:@"%d item(s)",self.items.count]];
//
//}
//- (void) setupToolbar {
//    
//    //    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
//    //    UIBarButtonItem *totalLabelButton = [[UIBarButtonItem alloc] init];
//    //    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] init];
//    //    UIBarButtonItem *saveTemplateButton = [[UIBarButtonItem alloc] init];
//    
//    amountString =[NSString stringWithFormat:@"%@ : ",NSLocalizedString(@"Total Price", nil)];
//    amountString = [amountString stringByAppendingString:[[NSString stringWithFormat:@"%.2f",totalOrderAmount] currencyString]];
//    
//    totalPriceLabel.text=amountString;
//    //    totalLabelButton.title = amountString;
//    
//    //    saveButton.title = NSLocalizedString(@"Save Order", nil);
//    //    saveButton.style = UIBarButtonItemStyleBordered;
//    //    saveButton.target=self;
//    //    saveButton.action=@selector(saveOrder:);
//    //
//    //    saveTemplateButton.title =NSLocalizedString(@"Save as Template", nil);
//    //    saveTemplateButton.style = UIBarButtonItemStyleBordered;
//    //    saveTemplateButton.target=self;
//    //    saveTemplateButton.action=@selector(saveTemplate:);
//    
//    Singleton *single = [Singleton retrieveSingleton];
//    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
//    {
//        
//        saveButton.hidden=YES;
//        
//        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
//        {
//            //[self setupToolbar:[NSArray arrayWithObjects:saveTemplateButton,flexibleSpace, flexibleSpace, totalLabelButton, nil]];
//            saveAsTemplateButton.hidden=NO;
//        }
//        else
//        {
//            // [self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, flexibleSpace, totalLabelButton, nil]];
//            saveAsTemplateButton.hidden=YES;
//        }
//        
//        saveAsTemplateButton.hidden = YES;//OLA!!
//    }
//    else
//    {
//        saveButton.hidden=NO;
//        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
//        {
//            //[self setupToolbar:[NSArray arrayWithObjects:saveTemplateButton,flexibleSpace,flexibleSpace,totalLabelButton, saveButton, nil]];
//            saveAsTemplateButton.hidden=NO;
//            
//        }
//        else
//        {
//            //[self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, flexibleSpace, totalLabelButton, saveButton, nil]];
//            saveAsTemplateButton.hidden=YES;
//            
//        }
//    }
//    
//    //    flexibleSpace = nil;
//    //    totalLabelButton = nil;
//    //    saveButton = nil;
//    //    saveTemplateButton = nil;
//}
//- (void) updateTotal {
//    price = 0.0f;
//    discountAmt = 0.0f;
//    for (int i = 0;i < self.items.count; i++)
//    {
//        dataItemArray=nil;
//        dataItemArray = [self.items objectAtIndex:i];
//        if([dataItemArray objectForKey:@"Discounted_Price"])
//        {
//            price = price + [[dataItemArray objectForKey:@"Discounted_Price"] doubleValue];
//            discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
//        }
//        else
//        {
//            price = price + [[dataItemArray objectForKey:@"Price"] doubleValue];
//            discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
//        }
//    }
//    totalDiscountAmount = discountAmt;
//    totalOrderAmount = price;
//    [totalPriceLabel setText:[[NSString stringWithFormat:@"%@ %.2f",NSLocalizedString(@"Total Price", nil), price] currencyString]];
//    [totalProduct setText:[NSString stringWithFormat:@"%d item(s)",self.items.count]];
//
//    [self setupToolbar];
//}
//- (void) saveTemplate:(id)sender{
//    if(items.count!=0)
//    {
//        TamplateNameViewController *viewController = [[TamplateNameViewController alloc] initWithTemplateName] ;
//        [viewController setContentSizeForViewInPopover:CGSizeMake(600, self.view.bounds.size.height - 100)];
//        [viewController setTarget:self];
//        [viewController setAction:@selector(itemUpdated:)];
//        viewController.templateName = name_TEMPLATES;
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController] ;
//        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
//        [popoverController setDelegate:self];
//        [popoverController presentPopoverFromRect:saveAsTemplateButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
////        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                           message:@"Please add items first"
////                  closeButtonTitle:NSLocalizedString(@"OK", nil)
////                 secondButtonTitle:nil
////               tappedButtonAtIndex:nil];
//    }
//}
//- (void) itemUpdated:(NSString *)item {
//    NSCharacterSet *unacceptedInput = nil;
//    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
//    if ([[item componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1)
//    {
//       Singleton *single = [Singleton retrieveSingleton];
//        if (item)
//        {
//            [popoverController dismissPopoverAnimated:YES];
//            NSMutableArray *tempTemplate = [NSMutableArray array];
//            NSMutableDictionary *dataDict;
//            for (int i = 0;i < self.items.count; i++)
//            {
//                
//                if([[[self.items objectAtIndex:i] stringForKey:@"priceFlag"] isEqualToString:@"N"] || [[[self.items objectAtIndex:i] stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
//                {
//                    dataDict =[NSMutableDictionary dictionary] ;
//                    if(![[self.items objectAtIndex:i] objectForKey:@"ItemID"])
//                    {
//                        [dataDict setValue:[[self.items objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
//                    }
//                    else
//                    {
//                        [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"ItemID"] forKey:@"ItemID"];
//                    }
//                    [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"Qty"] forKey:@"Qty"];
//                    [tempTemplate addObject:dataDict];
//                }
//            }
//            
//            //salesSer.delegate=self;
//            if([item isEqualToString:name_TEMPLATES])
//            {
//                [[SWDatabaseManager retrieveManager] deleteTemplateOrderWithRef:[selectedOrder stringForKey:@"Order_Template_ID"]];
//                [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
//                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Template saved" message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                [alert show];
//                
////                [ILAlertView showWithTitle:@"Template saved"
////                                   message:nil
////                          closeButtonTitle:NSLocalizedString(@"OK", nil)
////                         secondButtonTitle:nil
////                       tappedButtonAtIndex:nil];
//            }
//            else
//            {
//                BOOL isTheObjectThere = [single.savedOrderTemplateArray containsObject:item];
//                if(isTheObjectThere)
//                {
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Template name already exist." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                    [alert show];
////                    [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                                       message:@"Template name already exist."
////                              closeButtonTitle:NSLocalizedString(@"OK", nil)
////                             secondButtonTitle:nil
////                           tappedButtonAtIndex:nil];
//                }
//                else
//                {
//                    [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
//                    
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Template saved", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                    [alert show];
////                    [ILAlertView showWithTitle:@"Template saved"
////                                       message:nil
////                              closeButtonTitle:NSLocalizedString(@"OK", nil)
////                             secondButtonTitle:nil
////                           tappedButtonAtIndex:nil];
//                }
//            }
//        }
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Template name cannot have special characters.Please correct name and try to save again" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//
////        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                           message:@"Template name cannot have special characters.Please correct name and try to save again"
////                  closeButtonTitle:NSLocalizedString(@"OK", nil)
////                 secondButtonTitle:nil
////               tappedButtonAtIndex:nil];
//    }
//}
//- (void) saveOrder:(id)sender{
//   Singleton *single = [Singleton retrieveSingleton];
//    NSMutableDictionary *info = [[NSMutableDictionary alloc] init] ;
//    NSMutableDictionary *salesOrder = [[NSMutableDictionary alloc] init] ;
//    NSString *orderRef;
//    
//    [tempSalesArray setArray:nil];
//    
//    
//    for (int i = 0 ; i < [self.items count]; i++){
//        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
//        //dataDict = ;
//        if(![dataDict objectForKey:@"ItemID"])
//        {
//            [dataDict setValue:[dataDict stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
//        }
//        
//    }
//    for (int i = 0 ; i < [self.items count]; i++){
//        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
//        /// dataDict = [self.items objectAtIndex:i];
//        if([dataDict objectForKey:@"Discounted_Price"])
//        {
//            [dataDict setValue:[dataDict objectForKey:@"Discounted_Price"] forKey:@"Price"];
//        }
//        [tempSalesArray addObject:dataDict];
//    }
//    
//    self.items=[tempSalesArray copy];
//    
//    
//    [salesOrder setValue:self.items forKey:@"OrderItems"];
//    [info setValue:[NSString stringWithFormat:@"%.2f",totalOrderAmount] forKey:@"orderAmt"];
//    [info setValue:[NSString stringWithFormat:@"%.2f",totalDiscountAmount] forKey:@"discountAmt"];
//    [info setValue:[category stringForKey:@"Category"] forKey:@"productCategory"];
//    [salesOrder setValue:info forKey:@"info"];
//    category =[SWDefaults productCategory];
//    
//    if([single.visitParentView isEqualToString:@"MO"] || single.isSaveOrder){
//        if(![self.items count]==0)
//        {
//            
//            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
//            
//            //salesSer.delegate=self;
//            [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:single.savedPerformaCurrentOrder];
//            orderRef = [[SWDatabaseManager retrieveManager] updatePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID] andOrderRef:single.savedPerformaCurrentOrder];
//            
//            
//            //NSLog(@"orderRef %@",orderRef);
//            orderAdditionalInfoViewController = nil;
//
//            orderAdditionalInfoViewController = [[OrderAdditionalInfoViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
//            orderAdditionalInfoViewController.performaOrderRef = orderRef;
//            [self.navigationController pushViewController:orderAdditionalInfoViewController animated:YES];
//            orderAdditionalInfoViewController = nil;
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//             [alert show];
//        }
//    }
//    else{
//        if(![self.items count]==0)
//        {
//            single.isSaveOrder = YES;
//            
//            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
//            
//            
//            //NSLog(@"OLA!!***** salesOrder %@",salesOrder);
//            
//            //salesSer.delegate=self;
//            orderRef =  [[SWDatabaseManager retrieveManager] savePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
//            single.savedPerformaCurrentOrder = orderRef;
//            
//            orderAdditionalInfoViewController = nil;
//            orderAdditionalInfoViewController = [[OrderAdditionalInfoViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
//            orderAdditionalInfoViewController.performaOrderRef = orderRef;
//            [self.navigationController pushViewController:orderAdditionalInfoViewController animated:YES];
//            orderAdditionalInfoViewController = nil;
//            
//        }
//        else
//        {
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//              [alert show];
//        }
//        
//    }
//}
//
//- (int)numberOfRowsInGrid:(GridView *)gridView
//{
//    return self.items.count;
//}
//
////CRASH FIXED
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//}
//
//#pragma mark GridView DataSource
//- (int) numberOfColumnsInGrid:(GridView *)gridView {
//    return 6;
//}
//- (float) gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
//    switch (columnIndex) {
//        case 0:
//            return 40.0f;
//        case 1:
//            return 10.0f;
//        case 2:
//            return 12.0f;
//        case 3:
//            return 12.0f;
//        case 4:
//            return 14.0f;
//        case 5:
//            return 14.0f;
//        default:
//            break;
//    }
//    return 10.0f;
//}
//- (NSString *) gridView:(GridView *)gridView titleForColumn:(int)column {
//    title=@"";
//    switch (column) {
//        case 0:
//            title = NSLocalizedString(@"Name", nil);
//            break;
//        case 1:
//            title = NSLocalizedString(@"Qty", nil);
//            break;
//        case 2:
//            title = NSLocalizedString(@"Unit Price", nil);
//            break;
//        case 3:
//            title = NSLocalizedString(@"Total", nil);
//            break;
//        case 4:
//            title = NSLocalizedString(@"Discount", nil);
//            break;
//        case 5:
//            title = NSLocalizedString(@"Net Amount", nil);
//            break;
//        default:
//            break;
//    }
//    
//    return title;
//}
//- (NSString *) gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
//    title = @"";
//    dataItemArray=nil;
//    dataItemArray = [self.items objectAtIndex:row];
//    switch (column) {
//        case 0:
//            title = [dataItemArray stringForKey:@"Description"];
//            NSLog(@"Title is %@",title);
//            break;
//        case 1:
//            title = [dataItemArray stringForKey:@"Qty"];
//            break;
//        case 2:
//            title = [dataItemArray currencyStringForKey:@"Net_Price"];
//            break;
//        case 3:
//            title = [dataItemArray currencyStringForKey:@"Price"];
//            break;
//        case 4:
//            title = [dataItemArray currencyStringForKey:@"Discounted_Amount"];
//            break;
//        case 5:
//            title = [dataItemArray currencyStringForKey:@"Discounted_Price"];
//            break;
//        default:
//            break;
//    }
//    return title;
//}
//#pragma mark GridView Delegate
//- (void) gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex{
//   
//    
//   //Singleton *single = [Singleton retrieveSingleton];
//  /*  if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Confirmed orders cannot be changed" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//        
////        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                           message:@"Confirmed orders cannot be changed"
////                  closeButtonTitle:NSLocalizedString(@"OK", nil)
////                 secondButtonTitle:nil
////               tappedButtonAtIndex:nil];
//    }
//    else*/
//    //{
//        if(![[[self.items objectAtIndex:rowIndex] valueForKey:@"Price"] isEqualToString:@"0"])
//        {
//            deleteRowDict=nil;
//            deleteRowDict = [self.items objectAtIndex:rowIndex];
//            NSString *guid = [deleteRowDict stringForKey:@"Guid"];
//            if ([deleteRowDict objectForKey:@"ParentGuid"])
//            {
//                guid = [deleteRowDict objectForKey:@"ParentGuid"];
//            }
//            
//            for (int i = 0;i < self.items.count; i++)
//            {
//                dataItemArray=nil;
//                dataItemArray = [self.items objectAtIndex:i];
//                if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:guid])
//                {
//                    deleteRowDict=nil;
//                    deleteRowDict = dataItemArray ;
//                    break;
//                }
//            }
//            if (deleteRowDict)
//            {
//                [deleteRowDict setValue:[deleteRowDict stringForKey:@"Bonus"] forKey:@"GivenBonus"];
//                [self presentProductOrder:deleteRowDict];
//            }
//        }
//        else
//        {
//            // No joy...
//            // aww..why?
//            deleteRowDict=nil;
//            deleteRowDict = [self.items objectAtIndex:rowIndex-1];
//            NSString *guid = [deleteRowDict stringForKey:@"Guid"];
//            if ([deleteRowDict objectForKey:@"ParentGuid"])
//            {
//                guid = [deleteRowDict objectForKey:@"ParentGuid"];
//            }
//            
//            for (int i = 0;i < self.items.count; i++)
//            {
//                dataItemArray=nil;
//                dataItemArray = [self.items objectAtIndex:i];
//                if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:guid])
//                {
//                    deleteRowDict = nil;
//                    deleteRowDict = dataItemArray ;
//                    break;
//                }
//            }
//            if (deleteRowDict)
//            {
//                [deleteRowDict setValue:[deleteRowDict stringForKey:@"Bonus"] forKey:@"GivenBonus"];
//                [self presentProductOrder:deleteRowDict];
//            }
//
//        }
//    //}
//}
//- (void) gridView:(GridView *)gv commitDeleteRowAtIndex:(int)rowIndex {
//    
//    NSMutableArray *array = [NSMutableArray arrayWithArray:self.items];
//    deleteRowDict=nil;
//    deleteRowDict = [array objectAtIndex:rowIndex];
//   Singleton *single = [Singleton retrieveSingleton];
//    if([single.visitParentView isEqualToString:@"MO"])
//    {
//        single.savedPerformaCurrentOrder =  [[self.items objectAtIndex:0] stringForKey:@"Orig_Sys_Document_Ref"];
//    }
//    
//    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Confirmed orders cannot be changed" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//          [alert show];
//        
////        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                           message:@"Confirmed orders cannot be changed"
////                  closeButtonTitle:NSLocalizedString(@"OK", nil)
////                 secondButtonTitle:nil
////               tappedButtonAtIndex:nil];
//        
//        
//    }
//    else
//    {
//        if(![[deleteRowDict valueForKey:@"Price"] isEqualToString:@"0"])
//        {
//            if(rowIndex+1 < [array count] && [[[array objectAtIndex:rowIndex+1]stringForKey:@"priceFlag"] isEqualToString:@"M"])
//            {
//                if(rowIndex+2 < [array count] &&[[[array objectAtIndex:rowIndex+2]stringForKey:@"priceFlag"] isEqualToString:@"F"])
//                {
//                    //For F
//                    [array removeObjectAtIndex:rowIndex+2];
//                    [self setItems:array];
//                    [self.gridView deleteRowAtIndex:rowIndex+2];
//                    //For M
//                    [array removeObjectAtIndex:rowIndex+1];
//                    [self setItems:array];
//                    [self.gridView deleteRowAtIndex:rowIndex+1];
//                    
//                }
//                else
//                {
//                    //For M
//                    [array removeObjectAtIndex:rowIndex+1];
//                    [self setItems:array];
//                    [self.gridView deleteRowAtIndex:rowIndex+1];
//                }
//            }
//            else if(rowIndex+1 < [array count] && [[[array objectAtIndex:rowIndex+1]stringForKey:@"priceFlag"] isEqualToString:@"F"])
//            {
//                
//                if(rowIndex+2 < [array count] &&[[[array objectAtIndex:rowIndex+2]stringForKey:@"priceFlag"] isEqualToString:@"M"])
//                {
//                    // M
//                    [array removeObjectAtIndex:rowIndex+2];
//                    [self setItems:array];
//                    [self.gridView deleteRowAtIndex:rowIndex+2];
//                    //for F
//                    [array removeObjectAtIndex:rowIndex+1];
//                    [self setItems:array];
//                    [self.gridView deleteRowAtIndex:rowIndex+1];
//                }
//                else
//                {
//                    //For F
//                    [array removeObjectAtIndex:rowIndex+1];
//                    [self setItems:array];
//                    [self.gridView deleteRowAtIndex:rowIndex+1];
//                }
//            }
//            
//            
//            [array removeObjectAtIndex:rowIndex];
//            [self setItems:array];
//            [gv deleteRowAtIndex:rowIndex];
//            [self updateTotal];
//        }
//        else
//        {
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Bonus product can not be deleted" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                  [alert show];
//            
////            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                               message:@"Bonus product can not be deleted"
////                      closeButtonTitle:NSLocalizedString(@"OK", nil)
////                     secondButtonTitle:nil
////                   tappedButtonAtIndex:nil];
//            
//        }
//        
//    }
//    [totalProduct setText:[NSString stringWithFormat:@"%d item(s)",self.items.count]];
//
//}
//#pragma saleorder delegate
//- (void) dbGetSalesOrderServiceDiddbGetConfirmOrderItems:(NSArray *)performaOrder{
//    for(int i = 0; i < performaOrder.count; i++)
//    {
//        NSDictionary *row = [performaOrder objectAtIndex:i];
//        if([[row stringForKey:@"Unit_Selling_Price"] isEqualToString:@"0"])
//        {
//            [row setValue:[[performaOrder objectAtIndex:i-1] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
//            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
//            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
//            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
//            [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
//        }
//        else
//        {
//            if (![[row stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
//            {
//                if(![[row stringForKey:@"Def_Bonus"] isEqualToString:@""])
//                {
//                    [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
//                    [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"GivenBonus"];
//                    [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Bonus"];
//                    [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"FOC_Bonus"];
//                }
//            }
//            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
//            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
//            
//            int qty = [[row stringForKey:@"Ordered_Quantity"] intValue];
//            double totalPrice = (double)qty * ([[row objectForKey:@"Unit_Selling_Price"] doubleValue]);
//            
//            [row setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
//            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
//            double dicounttedPrice = totalPrice * ([[row objectForKey:@"Custom_Attribute_3"] doubleValue])/(double)100;
//            
//            double dicountedAmt = totalPrice - dicounttedPrice;
//            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
//            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
//            [row setValue:[NSString stringWithFormat:@"%@",[row objectForKey:@"Custom_Attribute_3"]] forKey:@"DiscountPercent"];
//            
//            double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
//            [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
//        }
//        
//    }
//    [self setItems:performaOrder];
//    [self.gridView reloadData];
//    [self updateTotal];
//    
//}
//- (void) dbGetSalesOrderServiceDiddbGetPerformaOrderItems:(NSArray *)performaOrder{
//   // NSLog(@"Order %@",performaOrder);
//    
//    for(int i = 0; i < performaOrder.count; i++)
//    {
//        
//        NSDictionary *row = [performaOrder objectAtIndex:i];
//        
//        if([[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
//        {
//            if(i+1 < [performaOrder count] && [[[performaOrder objectAtIndex:i+1]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
//            {
//                [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
//                [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"FOC_Product"];
//                
//                if(i+2 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+2]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
//                {
//                    [row setValue:[[performaOrder objectAtIndex:i+2] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
//                    [row setValue:[performaOrder objectAtIndex:i+2] forKey:@"Bonus_Product"];
//                }
//            }
//            else if(i+1 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+1]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
//            {
//                [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
//                [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"Bonus_Product"];
//                
//                if(i+2 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+2]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
//                {
//                    [row setValue:[[performaOrder objectAtIndex:i+2] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
//                    [row setValue:[performaOrder objectAtIndex:i+2] forKey:@"FOC_Product"];
//                    //[row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
//                }
//            }
//            Singleton *single = [Singleton retrieveSingleton];
//            single.savedPerformaCurrentOrder = [row stringForKey:@"Orig_Sys_Document_Ref"];
//            [row setValue:[row stringForKey:@"Calc_Price_Flag"] forKey:@"priceFlag"];
//            
//            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
//            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
//            
//            int qty = [[row stringForKey:@"Ordered_Quantity"] intValue];
//            double totalPrice = (double)qty * ([[row objectForKey:@"Unit_Selling_Price"] doubleValue]);
//            
//            [row setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
//            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
//            [row setValue:[row stringForKey:@"Custom_Attribute_2"] forKey:@"lineItemNotes"];
//            
//            double dicounttedPrice = totalPrice * ([[row objectForKey:@"Custom_Attribute_3"] doubleValue])/(double)100;
//            
//            double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
//            [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
//            
//            double dicountedAmt = totalPrice - dicounttedPrice;
//            
//            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
//            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
//            [row setValue:[NSString stringWithFormat:@"%@",[row objectForKey:@"Custom_Attribute_3"]] forKey:@"DiscountPercent"];
//        }
//        
//        else if([[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"] || [[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
//        {
//            if([[[performaOrder objectAtIndex:i-1] stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
//            {
//                [row setValue:[[performaOrder objectAtIndex:i-1] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
//            }
//            else
//            {
//                [row setValue:[[performaOrder objectAtIndex:i-2] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
//            }
//            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
//            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
//            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
//            [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
//            [row setValue:[row stringForKey:@"Calc_Price_Flag"] forKey:@"priceFlag"];
//        }
//
//        [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
//    }
//    [self setItems:performaOrder];
//    [self.gridView reloadData];
//    [self updateTotal];
//}
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC
//{
//    popoverController=nil;
//}
//
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    
//    return YES;
//}
//- (void) viewDidUnload{
//    // //NSLog(@" Sales Order NLOAD");
//    
//}
//- (void) viewDidDisappear:(BOOL)animated{
//    
//    //[[NSNotificationCenter defaultCenter] removeObserver:self];
//}
//- (void) didReceiveMemoryWarning{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        self.view = nil;
//    
//    // Dispose of any resources that can be recreated.
//}
//@end









//
//  SalesOrderViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/11/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SalesOrderViewController.h"
#import "ProductOrderViewController.h"
#import "Singleton.h"
#import "ProductSaleCategoryViewController.h"
#import "TamplateNameViewController.h"
#include <CommonCrypto/CommonHMAC.h>

#import "SWOrderConfirmationViewController.h"


#define NUMERIC  @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890 "

@interface SalesOrderViewController ()

@end

@implementation SalesOrderViewController
@synthesize delegate,wareHouseDataSalesOrder,wareHouseDataOldSalesArray;

//-(void)dealloc
//{
//    NSLog(@"SalesOrderViewController");
//}
//@synthesize manageOrder,category;

- (id) initWithCustomer:(NSDictionary *)c andCategory:(NSDictionary *)categorya{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        category = [NSDictionary dictionaryWithDictionary:categorya];
        
        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
        [self setItems:[NSMutableArray array]];
        Singleton *single = [Singleton retrieveSingleton];
        single.isManageOrder=NO;
        single.manageOrderRefNumber = @"SO";
        isManageOrder = NO;
        
        
        
        
        
        
//        mslOrderItem1 = [[UIButton alloc] init] ;
//        [mslOrderItem1 setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//        [mslOrderItem1 setFrame:CGRectMake(320,self.gridView.bounds.size.height + 50, 140, 42)];
//        [mslOrderItem1 addTarget:self action:@selector(mslItems:) forControlEvents:UIControlEventTouchUpInside];
//        [mslOrderItem1 bringSubviewToFront:self.view];
//        mslOrderItem1.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//        [mslOrderItem1 setTitle:NSLocalizedString(@"Load from MSL1", nil) forState:UIControlStateNormal];
//        mslOrderItem1.titleLabel.font = BoldSemiFontOfSize(15);
//        [self.view addSubview:mslOrderItem1];
        
        
        
        
        
        
        
        
//        saveButton = [[UIButton alloc] init] ;
//        [saveButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//        [saveButton setFrame:CGRectMake(360,self.gridView.bounds.size.height + 50, 120, 42)];
//        [saveButton addTarget:self action:@selector(saveOrder:) forControlEvents:UIControlEventTouchUpInside];
//        [saveButton bringSubviewToFront:self.view];
//        saveButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//        [saveButton setTitle:NSLocalizedString(@"Save Order", nil) forState:UIControlStateNormal];
//        saveButton.titleLabel.font = BoldSemiFontOfSize(15);
//        [self.view addSubview:saveButton];
//        
//        saveAsTemplateButton = [[UIButton alloc] init] ;
//        [saveAsTemplateButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//        [saveAsTemplateButton setFrame:CGRectMake(490,self.gridView.bounds.size.height + 50, 120, 42)];
//        [saveAsTemplateButton addTarget:self action:@selector(saveTemplate:) forControlEvents:UIControlEventTouchUpInside];
//        [saveAsTemplateButton bringSubviewToFront:self.view];
//        saveAsTemplateButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//        [saveAsTemplateButton setTitle:NSLocalizedString(@"Save Template", nil) forState:UIControlStateNormal];
//        saveAsTemplateButton.titleLabel.font =BoldSemiFontOfSize(15);
//        [self.view addSubview:saveAsTemplateButton];
        
        
        
        
        
        
        
        if (!single.isDistributionChecked)
        {
        }
        else
        {
            [mslOrderItem setEnabled:NO];
        }
        
        
        
    }
    return self;
}


- (id) initWithCustomer:(NSDictionary *)c andOrders:(NSDictionary *)order andManage:(NSString *)manage{
    self = [super init];
    if (self)
    {
        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
        
        Singleton *single = [Singleton retrieveSingleton];
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        category = [NSDictionary dictionary];
        selectedOrder=[NSMutableDictionary dictionaryWithDictionary:order] ;
        savedCategoryID = [selectedOrder stringForKey:@"Custom_Attribute_2"];
        manageOrder = manage;
        
        if (![customer objectForKey:@"Ship_Customer_ID"])
        {
            NSString *shipCustomer = [customer stringForKey:@"Customer_ID"];
            NSString *shipSite = [customer stringForKey:@"Site_Use_ID"];
            [customer setValue:shipCustomer forKey:@"Ship_Customer_ID"];
            [customer setValue:shipSite forKey:@"Ship_Site_Use_ID"];
        }
        
        [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
        
        if([selectedOrder objectForKey:@"Schedule_Ship_Date"])
        {
            [self dbGetSalesOrderServiceDiddbGetConfirmOrderItems:[[SWDatabaseManager retrieveManager] dbGetSalesConfirmOrderItems:[selectedOrder objectForKey:@"Orig_Sys_Document_Ref"]]];
        }
        else
        {
            //Saad Line
            [self dbGetSalesOrderServiceDiddbGetPerformaOrderItems:[[SWDatabaseManager retrieveManager] dbGetSalesPerformaOrderItems:[selectedOrder objectForKey:@"Orig_Sys_Document_Ref"]]];
        }
        
        single.isManageOrder=YES;
        isManageOrder = YES;
        single.manageOrderRefNumber = [selectedOrder objectForKey:@"Orig_Sys_Document_Ref"];
    }
    return self;
}

- (id) initWithCustomer:(NSDictionary *)c andTemplate:(NSDictionary *)order andTemplateItem:(NSArray *)orderItems{
    
    NSLog(@"template items in init with customer %@", orderItems);
    
    self = [super init];
    if (self)
    {
        
        
        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        category = [NSDictionary dictionary];
        selectedOrder=[NSMutableDictionary dictionaryWithDictionary:order] ;
        savedCategoryID = [order stringForKey:@"Custom_Attribute_1"];
        
        
        name_TEMPLATES = [order stringForKey:@"Template_Name"];
        
        
        [[NSUserDefaults standardUserDefaults]setValue:name_TEMPLATES forKey:@"Template_Name"];
        
        //serProduct.delegate=self;
        
        
        //to be done
        [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
        
        
       
        [self setItems:orderItems];
        [self.gridView reloadData];
        [self updateTotal];
        
    }
    return self;
}
- (void) viewWillAppear:(BOOL)animated{
    
    [Flurry logEvent:@"Sales Order  View"];
    //[Crittercism leaveBreadcrumb:@"<Sales Order View>"];
    self.navigationController.toolbarHidden=YES;
}
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    gridView.frame=CGRectMake(0, 40, self.view.bounds.size.width+5, self.view.bounds.size.height-100);
    
    NSLog(@"grid view frame is : %@ view frame is : %@ tableview frame is %@", NSStringFromCGRect(gridView.frame),NSStringFromCGRect(self.view.frame),NSStringFromCGRect(gridView.tableView.frame));
    
    
    //gridView.frame=CGRectMake(0, 60, self.view.bounds.size.width, 100);
    self.view.backgroundColor = UIColorFromRGB(0xE0E5EC);
   //     self.view.backgroundColor = [UIColor redColor ];
    
    
    productDict = [NSMutableDictionary dictionary];
    arrayTemp = [NSMutableArray array];
    tempSalesArray = [NSMutableArray array];
    p = [NSMutableDictionary dictionary];
    b = [NSMutableDictionary dictionary ];
    f = [NSMutableDictionary dictionary ];
    deleteRowDict = [NSMutableDictionary dictionary ];
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isSaveOrder = NO;
    
    
    // CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60) andCustomer:customer] ;
    //    [headerView setShouldHaveMargins:YES];
    //[self.view addSubview:headerView];
    
    
    
    
    if (saveButton==nil) {
        saveButton = [[UIButton alloc] init] ;
        [saveButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
        [saveButton setFrame:CGRectMake(610,self.gridView.bounds.size.height + 50, 120, 42)];
        [saveButton addTarget:self action:@selector(saveOrder:) forControlEvents:UIControlEventTouchUpInside];
        [saveButton bringSubviewToFront:self.view];
        saveButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [saveButton setTitle:NSLocalizedString(@"Save Order", nil) forState:UIControlStateNormal];
        saveButton.titleLabel.font = BoldSemiFontOfSize(15);
       // [self.view addSubview:saveButton];
        
      
    }
    
    if (saveAsTemplateButton==nil) {
        saveAsTemplateButton = [[UIButton alloc] init] ;
        [saveAsTemplateButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
        [saveAsTemplateButton setFrame:CGRectMake(480,self.gridView.bounds.size.height + 50, 120, 42)];
        [saveAsTemplateButton addTarget:self action:@selector(saveTemplate:) forControlEvents:UIControlEventTouchUpInside];
        [saveAsTemplateButton bringSubviewToFront:self.view];
        saveAsTemplateButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [saveAsTemplateButton setTitle:NSLocalizedString(@"Save Template", nil) forState:UIControlStateNormal];
        saveAsTemplateButton.titleLabel.font =BoldSemiFontOfSize(15);
       // [self.view addSubview:saveAsTemplateButton];

    }
    
  if (mslOrderItem==nil) {
    mslOrderItem = [[UIButton alloc] init] ;
    [mslOrderItem setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
    [mslOrderItem setFrame:CGRectMake(320,self.gridView.bounds.size.height + 50, 140, 42)];
    [mslOrderItem addTarget:self action:@selector(mslItems:) forControlEvents:UIControlEventTouchUpInside];
    [mslOrderItem bringSubviewToFront:self.view];
    mslOrderItem.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [mslOrderItem setTitle:NSLocalizedString(@"Load from MSL", nil) forState:UIControlStateNormal];
    mslOrderItem.titleLabel.font = BoldSemiFontOfSize(15);
      
      
     // NSLog(@"app control flags are %@",[[SWDefaults appControl] objectAtIndex:0]);
      
      
   // [self.view addSubview:mslOrderItem];

    
  }
    
   
    
                if (!single.isDistributionChecked)
                {
                    
                }
                else
                {
                    [mslOrderItem setEnabled:NO];
                }

    
//    saveButton = [[UIButton alloc] init] ;
//    [saveButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//    [saveButton setFrame:CGRectMake(self.view.frame.size.width-120,self.gridView.bounds.size.height + 50, 120, 42)];
//    [saveButton addTarget:self action:@selector(saveOrder:) forControlEvents:UIControlEventTouchUpInside];
//    [saveButton bringSubviewToFront:self.view];
//    saveButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [saveButton setTitle:NSLocalizedString(@"Save Order", nil) forState:UIControlStateNormal];
//    saveButton.titleLabel.font = BoldSemiFontOfSize(15);
//    [self.view addSubview:saveButton];
//    
//    saveAsTemplateButton = [[UIButton alloc] init] ;
//    [saveAsTemplateButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//    [saveAsTemplateButton setFrame:CGRectMake(self.view.frame.size.width-250,self.gridView.bounds.size.height + 50, 120, 42)];
//    [saveAsTemplateButton addTarget:self action:@selector(saveTemplate:) forControlEvents:UIControlEventTouchUpInside];
//    [saveAsTemplateButton bringSubviewToFront:self.view];
//    saveAsTemplateButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [saveAsTemplateButton setTitle:NSLocalizedString(@"Save Template", nil) forState:UIControlStateNormal];
//    saveAsTemplateButton.titleLabel.font =BoldSemiFontOfSize(15);
//    [self.view addSubview:saveAsTemplateButton];
    
    //gridView.userInteractionEnabled=NO;
    //[self.gridView.tableView setTableHeaderView:headerView];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeOrder)] ];
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    {
    }
    else
    {
        // [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentProductList:)] ];
    }
    
    [self.gridView setShouldAllowDeleting:YES];
    
    orderItemLbl=[[UILabel alloc] initWithFrame:CGRectMake(500, 0, 270, 40)] ;
    [orderItemLbl setText:@"Order Items: 0 item(s)"];
    [orderItemLbl setTextAlignment:NSTextAlignmentCenter];
    [orderItemLbl setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
    //[orderItemLbl setBackgroundColor:[UIColor redColor]];
    [orderItemLbl setTextColor:[UIColor darkGrayColor]];
    [orderItemLbl setFont:BoldSemiFontOfSize(20.0)];
    [self.view addSubview:orderItemLbl];
    
    NSLog(@"order item lbl frame is %@", NSStringFromCGRect(orderItemLbl.frame));
    
    
    totalProduct=[[UILabel alloc] initWithFrame:CGRectMake(560, 0, 180, 40)] ;
    [totalProduct setText:@"0 item(s)"];
    [totalProduct setTextAlignment:NSTextAlignmentCenter];
//    [totalProduct setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
    [totalProduct setBackgroundColor:[UIColor blackColor]];
  [totalProduct setTextColor:[UIColor darkGrayColor]];
    [totalProduct setFont:RegularFontOfSize(20.0)];
    //[self.view addSubview:totalProduct];
    
    
    NSLog(@"total product lbl frame is %@", NSStringFromCGRect(totalProduct.frame));

    
    totalPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(8,self.gridView.bounds.size.height + 65, 300, 30)] ;
    
    NSLog(@"total price lable frame is %@", NSStringFromCGRect(totalPriceLabel.frame));
    [totalPriceLabel setText:@""];
    [totalPriceLabel setTextAlignment:NSTextAlignmentLeft];
    [totalPriceLabel setBackgroundColor:[UIColor clearColor]];
    [totalPriceLabel setTextColor:[UIColor darkGrayColor]];
    [totalPriceLabel setFont:BoldSemiFontOfSize(20.0)];
    totalPriceLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:totalPriceLabel];
    
    AppControl *appControl = [AppControl retrieveSingleton];
    ENABLE_TEMPLATES = appControl.ENABLE_TEMPLATES;
    [self updateTotal];
    
}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0)
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
}
- (void) closeOrder
{
    // exit(1);
    
    if([self.items count] > 0 )
    {
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isSaveOrder)
        {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController  popViewControllerAnimated:YES];
        }
        else {
            if([manageOrder isEqualToString:@"confirmed"])
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController  popViewControllerAnimated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:@"Would you like to cancel the order without saving?" delegate:self cancelButtonTitle:NSLocalizedString(@"Yes", nil) otherButtonTitles:NSLocalizedString(@"No", nil) ,nil];
                [alert show];
                
                //                [ILAlertView showWithTitle:NSLocalizedString(@"Warning", nil)
                //                                   message:@"Would you like to cancel the order without saving?"
                //                          closeButtonTitle:NSLocalizedString(@"No", nil)
                //                         secondButtonTitle:NSLocalizedString(@"Yes", nil)
                //                       tappedButtonAtIndex:^(NSInteger buttonIndex) {
                //                           if (buttonIndex==1)
                //                           {
                //                               [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                //                   [self.navigationController  popViewControllerAnimated:YES];
                //                           }
                //                       }
                //                 ];
                
            }
            
        }
    }
    else
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
}
- (void) presentProductList:(id)sender{
    Singleton *single = [Singleton retrieveSingleton];
    
    if ([single.visitParentView isEqualToString:@"SO"])
    {
        [SWDefaults setProductCategory:category];
        [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
    }
    else
    {
        [self categorySelected:category];
    }
    NSLog(@"setProductCategory 2");
    
}
#pragma mark Product Service Delegate
- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories {
    //categoryArray=categories;
    
    categoryArray = [NSMutableArray arrayWithArray:categories];
    
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID]) {
            category=[NSDictionary dictionaryWithDictionary:row];
            
            //this is causing problems in case of template
            Singleton * single=[Singleton retrieveSingleton];
            
            [SWDefaults setProductCategory:category];

            
            if (single.isTemplateOrder==YES) {
                
            }
            else{

            }
            break;
        }
    }
    
    //NSLog(@"setProductCategory 3 %@",[SWDefaults productCategory]);
    
    
    //    productListViewController = [[ProductListViewController alloc] initWithCategory:category] ;
    //    [productListViewController setTarget:self];
    //    [productListViewController setAction:@selector(productSelected:)];
    //    navigationControllerS = [[UINavigationController alloc] initWithRootViewController:productListViewController] ;
    //    productListViewController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    categories=nil;
}
- (void) categorySelected:(NSDictionary *)product{
    
    [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
}
- (void) presentProductOrder:(NSDictionary *)product {
    
    if (![product objectForKey:@"Guid"])
    {
        [product setValue:[NSString createGuid] forKey:@"Guid"];
    }
    if (isManageOrder)
    {
        if(![product objectForKey:@"ItemID"])
        {
            [product setValue:[product stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
        }
    }
    [self.delegate selectedProduct:[product mutableCopy]];
    //    orderViewController = [[ProductOrderViewController alloc] initWithProduct:product] ;
    //    [orderViewController setTarget:self];
    //    [orderViewController setAction:@selector(productAdded:)];
    //    [self.navigationController pushViewController:orderViewController animated:YES];
    //    orderViewController = nil ;
    product=nil;
}
#pragma mark ProductListView Controller action
- (void) productSelected:(NSDictionary *)product{
    productDict = [NSMutableDictionary dictionaryWithDictionary:product];
    //serProduct.delegate=self;
//    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[product stringForKey:@"ItemID"]]];
    
    
    
    
    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[product stringForKey:@"ItemID"] :[customer stringForKey:@"Price_List_ID"]]];
    
    productDict=nil;
}
- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customer stringForKey:@"Price_List_ID"]])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            //[self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.50f];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            //[self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.50f];
        }
        else
        {
            // no price . stop here
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            //            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
            //                               message:@"No prices availble for selected product."
            //                      closeButtonTitle:NSLocalizedString(@"OK", nil)
            //                     secondButtonTitle:nil
            //                   tappedButtonAtIndex:nil];
            
            
        }
    }
    else
    {
        NSLog(@"no prices alert called");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No prices available for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
        //                           message:@"No prices availble for selected product."
        //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:nil];
        
    }
    priceDetail=nil;
}
- (void)mslItems:(id)sender
{
    Singleton *single = [Singleton retrieveSingleton];
    
    NSMutableArray *distItem = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Inventory_Item_ID , B.Category,B.Organization_ID  FROM TBL_Distribution_Check_Items AS A , TBL_Product AS B WHERE A.Inventory_Item_ID = B.Inventory_Item_ID AND A.DistributionCheck_ID = '%@' AND A.Is_Available='N' ",single.distributionRef]];
    NSMutableArray *tempArray = [NSMutableArray new];
    BOOL isCategory=NO;
    for (int i=0 ; i<[distItem count]; i++)
    {
        if ([[[distItem objectAtIndex:i] stringForKey:@"Category"] isEqualToString:[[SWDefaults productCategory] stringForKey:@"Category"]])
        {
            [tempArray addObject:[distItem objectAtIndex:i]];
            isCategory=YES;
        }
    }
    
    NSMutableArray *tempArray1 = [NSMutableArray new];
    
    for (int i = 0 ; i<tempArray.count; i++)
    {
        NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[[tempArray objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] organizationId:[[tempArray objectAtIndex:i] stringForKey:@"Organization_ID"]];
        NSMutableDictionary *tempDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
        NSString *itemID = [tempDict stringForKey:@"Inventory_Item_ID"];
        [tempDict setValue:itemID forKey:@"ItemID"];
        [tempDict setValue:@"1" forKey:@"Qty"];
        [tempDict setValue:[NSString createGuid] forKey:@"Guid"];
        [tempDict setValue:@"N" forKey:@"priceFlag"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Discounted_Price"];
        
        [tempArray1 addObject:tempDict];
    }
    
    if (isCategory) {
        //single.isDistributionChecked = NO;
        single.isDistributionItemGet = YES;
        [mslOrderItem setEnabled:NO];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:@"No MSL items are available in this product category." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
    
    NSMutableArray *templateItemsArray = [NSMutableArray array];
    BOOL isBonusDone;
    for(int i = 0; i < tempArray1.count; i++)
    {
        NSMutableDictionary *iTemDict= [tempArray1 objectAtIndex:i];
        NSMutableArray *bonusArray = [NSMutableArray arrayWithArray:[[[SWDatabaseManager retrieveManager] dbGetBonusInfoOfProduct:[iTemDict stringForKey:@"Item_Code"]] mutableCopy]];
        int quantity = 0;
        quantity = [[iTemDict objectForKey:@"Qty"] intValue];
        int bonus = 0;
        isBonusDone=NO;
        //  NSLog(@"NAme %@",[iTemDict stringForKey:@"Description"]);
        if([bonusArray count]!=0)
        {
            for(int j=0 ; j<[bonusArray count] ; j++ )
            {
                NSDictionary *row = [bonusArray objectAtIndex:j];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    //NSMutableDictionary *bonusProduct=[NSMutableDictionary new];
                    if(j == [bonusArray count]-1)
                    {
                        if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                        {
                            int dividedValue = quantity / rangeStart ;
                            bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                            
                            NSString *childGUID = [NSString createGuid];
                            
                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                            // [iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:iTemDict];
                            [templateItemsArray addObject:bonusProduct];
                            isBonusDone=YES;
                        }
                        
                        else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                        {
                            int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                            bonus = floor(dividedValue) ;
                            
                            NSString *childGUID = [NSString createGuid];
                            
                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            // [bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:iTemDict];
                            [templateItemsArray addObject:bonusProduct];
                            isBonusDone=YES;
                        }
                        
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue];
                        NSString *childGUID = [NSString createGuid];
                        
                        NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                        [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                        [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                        [bonusProduct setValue:childGUID forKey:@"Guid"];
                        //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                        [bonusProduct setValue:@"0" forKey:@"Price"];
                        [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                        
                        [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                        [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                        //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                        int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                        double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                        [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                        [templateItemsArray addObject:iTemDict];
                        [templateItemsArray addObject:bonusProduct];
                        isBonusDone=YES;
                    }
                }
                
            }
            if (!isBonusDone)
            {
                [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                [templateItemsArray addObject:iTemDict];
            }
        }
        else
        {
            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
            [templateItemsArray addObject:iTemDict];
        }
    }
    
    
    
    self.items = templateItemsArray;
    
    [self.gridView reloadData];
    [self updateTotal];
    
    
    
}

-(void)loadFromMSLTapped
{
    Singleton *single = [Singleton retrieveSingleton];
    
    NSMutableArray *distItem = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Inventory_Item_ID , B.Category,B.Organization_ID  FROM TBL_Distribution_Check_Items AS A , TBL_Product AS B WHERE A.Inventory_Item_ID = B.Inventory_Item_ID AND A.DistributionCheck_ID = '%@' AND A.Is_Available='N' ",single.distributionRef]];
    NSMutableArray *tempArray = [NSMutableArray new];
    BOOL isCategory=NO;
    for (int i=0 ; i<[distItem count]; i++)
    {
        if ([[[distItem objectAtIndex:i] stringForKey:@"Category"] isEqualToString:[[SWDefaults productCategory] stringForKey:@"Category"]])
        {
            [tempArray addObject:[distItem objectAtIndex:i]];
            isCategory=YES;
        }
    }
    
    NSMutableArray *tempArray1 = [NSMutableArray new];
    
    for (int i = 0 ; i<tempArray.count; i++)
    {
        NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[[tempArray objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] organizationId:[[tempArray objectAtIndex:i] stringForKey:@"Organization_ID"]];
        NSMutableDictionary *tempDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
        NSString *itemID = [tempDict stringForKey:@"Inventory_Item_ID"];
        [tempDict setValue:itemID forKey:@"ItemID"];
        [tempDict setValue:@"1" forKey:@"Qty"];
        [tempDict setValue:[NSString createGuid] forKey:@"Guid"];
        [tempDict setValue:@"N" forKey:@"priceFlag"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Discounted_Price"];
        
        [tempArray1 addObject:tempDict];
    }
    
    if (isCategory) {
        //single.isDistributionChecked = NO;
        single.isDistributionItemGet = YES;
        [mslOrderItem setEnabled:NO];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:@"No MSL items are available in this product category." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
    
    NSLog(@"temo array one for load from msl is %@", tempArray1);
    
    NSMutableArray *templateItemsArray = [NSMutableArray array];
    BOOL isBonusDone;
    for(int i = 0; i < tempArray1.count; i++)
    {
        NSMutableDictionary *iTemDict= [tempArray1 objectAtIndex:i];
        NSMutableArray *bonusArray = [NSMutableArray arrayWithArray:[[[SWDatabaseManager retrieveManager] dbGetBonusInfoOfProduct:[iTemDict stringForKey:@"Item_Code"]] mutableCopy]];
        int quantity = 0;
        quantity = [[iTemDict objectForKey:@"Qty"] intValue];
        int bonus = 0;
        isBonusDone=NO;
        //  NSLog(@"NAme %@",[iTemDict stringForKey:@"Description"]);
        if([bonusArray count]!=0)
        {
            for(int j=0 ; j<[bonusArray count] ; j++ )
            {
                NSDictionary *row = [bonusArray objectAtIndex:j];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    //NSMutableDictionary *bonusProduct=[NSMutableDictionary new];
                    if(j == [bonusArray count]-1)
                    {
                        if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                        {
                            int dividedValue = quantity / rangeStart ;
                            bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                            
                            NSString *childGUID = [NSString createGuid];
                            
                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                            // [iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:iTemDict];
                            [templateItemsArray addObject:bonusProduct];
                            isBonusDone=YES;
                        }
                        
                        else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                        {
                            int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                            bonus = floor(dividedValue) ;
                            
                            NSString *childGUID = [NSString createGuid];
                            
                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            // [bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:iTemDict];
                            [templateItemsArray addObject:bonusProduct];
                            isBonusDone=YES;
                        }
                        
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue];
                        NSString *childGUID = [NSString createGuid];
                        
                        NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                        [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                        [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                        [bonusProduct setValue:childGUID forKey:@"Guid"];
                        //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                        [bonusProduct setValue:@"0" forKey:@"Price"];
                        [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                        
                        [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                        [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                        //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                        int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                        double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                        [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                        [templateItemsArray addObject:iTemDict];
                        [templateItemsArray addObject:bonusProduct];
                        isBonusDone=YES;
                    }
                }
                
            }
            if (!isBonusDone)
            {
                [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                [templateItemsArray addObject:iTemDict];
            }
        }
        else
        {
            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
            [templateItemsArray addObject:iTemDict];
        }
    }
    
    
    
    self.items = templateItemsArray;
    
    [self.gridView reloadData];
    [self updateTotal];
    
    
    
}
- (void) productAdded:(NSMutableDictionary *)q{
    
    arrayTemp=nil;
    p=nil;
    arrayTemp= [NSMutableArray arrayWithArray:self.items];
    p=q;
    if([[q stringForKey:@"Qty"] isEqualToString:@"0"] || [[q stringForKey:@"Qty"] isEqualToString:@""])
    {
        for (int i = arrayTemp.count - 1; i >= 0; i--)
        {
            dataItemArray=nil;
            dataItemArray = [self.items objectAtIndex:i];
            if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
            {
                [arrayTemp removeObjectAtIndex:i];
            }
            else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
            {
                [arrayTemp removeObjectAtIndex:i];
            }
            else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid_1"]])
            {
                [arrayTemp removeObjectAtIndex:i];
            }
        }
    }
    else
    {
        @autoreleasepool
        {
            for (int i = arrayTemp.count - 1; i >= 0; i--)
            {
                dataItemArray=nil;
                dataItemArray = [self.items objectAtIndex:i];
                if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
                {
                    [arrayTemp removeObjectAtIndex:i];
                }
                else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
                {
                    [arrayTemp removeObjectAtIndex:i];
                }
                else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid_1"]])
                {
                    [arrayTemp removeObjectAtIndex:i];
                }
            }
        }
        
        if ([p objectForKey:@"Bonus_Product"])
        {
            b=nil;
            b = [p objectForKey:@"Bonus_Product"];
            [b setValue:[NSString createGuid] forKey:@"Guid"];
            [b setValue:@"0" forKey:@"Price"];
            [b setValue:@"0" forKey:@"Bonus"];
            [b setValue:@"0" forKey:@"Discounted_Price"];
            [b setValue:@"0" forKey:@"DiscountPercent"];
            [b setValue:@"F" forKey:@"priceFlag"];
            
            [p setValue:[b stringForKey:@"Guid"] forKey:@"ChildGuid"];
            [b setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
            [p setValue:@"N" forKey:@"priceFlag"];
            
            if([p objectForKey:@"FOC_Product"])
            {
                f=nil;
                f = [p objectForKey:@"FOC_Product"];
                [f setValue:[NSString createGuid] forKey:@"Guid"];
                [f setValue:@"0" forKey:@"Price"];
                [f setValue:@"0" forKey:@"Bonus"];
                [f setValue:@"0" forKey:@"Discounted_Price"];
                [f setValue:@"0" forKey:@"DiscountPercent"];
                [f setValue:@"M" forKey:@"priceFlag"];
                [p setValue:[f stringForKey:@"Guid"] forKey:@"ChildGuid_1"];
                [f setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
                [arrayTemp addObject:p];
                [arrayTemp addObject:b];
                [arrayTemp addObject:f];
            }
            else
            {
                [arrayTemp addObject:p];
                [arrayTemp addObject:b];
            }
        }
        else if([p objectForKey:@"FOC_Product"])
        {
            f=nil;
            f = [p objectForKey:@"FOC_Product"];
            [f setValue:[NSString createGuid] forKey:@"Guid"];
            [f setValue:@"0" forKey:@"Price"];
            [f setValue:@"0" forKey:@"Bonus"];
            [f setValue:@"0" forKey:@"Discounted_Price"];
            [f setValue:@"0" forKey:@"DiscountPercent"];
            [f setValue:@"M" forKey:@"priceFlag"];
            [p setValue:[f stringForKey:@"Guid"] forKey:@"ChildGuid"];
            [f setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
            [p setValue:@"N" forKey:@"priceFlag"];
            
            [arrayTemp addObject:p];
            [arrayTemp addObject:f];
        }
        else
        {
            [p setValue:@"N" forKey:@"priceFlag"];
            [arrayTemp addObject:p];
        }
    }
    
    [self setItems:arrayTemp];
    [self.gridView reloadData];
    [self updateTotal];
    arrayTemp=nil;
    p=nil;
    b=nil;
    f=nil;
    q=nil;
    
    [orderItemLbl setText:[NSString stringWithFormat:@"Order Items: %d item(s)",self.items.count]];
    
    
    
    //[[NSUserDefaults standardUserDefaults]setObject:self.items forKey:@"Items"];
    
    
}
- (void) setupToolbar {
    
    //    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    //    UIBarButtonItem *totalLabelButton = [[UIBarButtonItem alloc] init];
    //    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] init];
    //    UIBarButtonItem *saveTemplateButton = [[UIBarButtonItem alloc] init];
    
    amountString =[NSString stringWithFormat:@"%@: ",NSLocalizedString(@"Total Price", nil)];
    amountString = [amountString stringByAppendingString:[[NSString stringWithFormat:@"%.2f",totalOrderAmount] currencyString]];
    
   // totalPriceLabel.text=amountString;
    
    
    
    //    totalLabelButton.title = amountString;
    
    //    saveButton.title = NSLocalizedString(@"Save Order", nil);
    //    saveButton.style = UIBarButtonItemStyleBordered;
    //    saveButton.target=self;
    //    saveButton.action=@selector(saveOrder:);
    //
    //    saveTemplateButton.title =NSLocalizedString(@"Save as Template", nil);
    //    saveTemplateButton.style = UIBarButtonItemStyleBordered;
    //    saveTemplateButton.target=self;
    //    saveTemplateButton.action=@selector(saveTemplate:);
    
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    {
        
        saveButton.hidden=YES;
        
        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
        {
            //[self setupToolbar:[NSArray arrayWithObjects:saveTemplateButton,flexibleSpace, flexibleSpace, totalLabelButton, nil]];
            saveAsTemplateButton.hidden=NO;
        }
        else
        {
            // [self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, flexibleSpace, totalLabelButton, nil]];
            saveAsTemplateButton.hidden=YES;
           
        }
         mslOrderItem.hidden=YES;
        saveAsTemplateButton.hidden = YES;//OLA!!
    }
    else
    {
        saveButton.hidden=NO;
        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
        {
            //[self setupToolbar:[NSArray arrayWithObjects:saveTemplateButton,flexibleSpace,flexibleSpace,totalLabelButton, saveButton, nil]];
            saveAsTemplateButton.hidden=NO;
            
        }
        else
        {
            //[self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, flexibleSpace, totalLabelButton, saveButton, nil]];
            saveAsTemplateButton.hidden=YES;
            
        }
    }
    
    //    flexibleSpace = nil;
    //    totalLabelButton = nil;
    //    saveButton = nil;
    //    saveTemplateButton = nil;
}
//- (void) updateTotal {
//    price = 0.0f;
//    discountAmt = 0.0f;
//    for (int i = 0;i < self.items.count; i++)
//    {
//        dataItemArray=nil;
//        dataItemArray = [self.items objectAtIndex:i];
//        if([dataItemArray objectForKey:@"Discounted_Price"])
//        {
//            price = price + [[dataItemArray objectForKey:@"Discounted_Price"] doubleValue];
//            discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
//        }
//        else
//        {
//            price = price + [[dataItemArray objectForKey:@"Price"] doubleValue];
//            discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
//        }
//        
//        
//        
//        //check for net price here
//        
//        
//    
//        testPriceforLbl=[[dataItemArray valueForKey:@"Net_Price"] doubleValue]+testPriceforLbl;
//        
//        
//        
//        NSLog(@"net price for label %@", [dataItemArray valueForKey:@"Net_Price"]);
//        
//    }
//    NSLog(@"net price for label out of loop %f", testPriceforLbl);
//
//    totalDiscountAmount = discountAmt;
//    totalOrderAmount = price;
//    
//    
//    
//    
//    double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
//    price=price*qty;
//    
//    
//    //taking discount into consideration
//    
//    
//    double disc=[[dataItemArray valueForKey:@"DiscountPercent"] doubleValue];
//    
//    double discountedPrice=[[dataItemArray valueForKey:@"Discounted_Amount"] doubleValue];
//    
//    
//    disc=disc/100;
//    
//    price=price*(1-disc);
//    
//    
//    
//    //calculate total from array
//    
//    
//    
//    
//    
//    
//    totalPriceLabel.text=[NSString stringWithFormat:@"%0.2f", price];
//    
//    
//   // [totalPriceLabel setText:[[NSString stringWithFormat:@"%@ %.2f",NSLocalizedString(@"Total Price", nil), price] currencyString]];
//    [totalProduct setText:[NSString stringWithFormat:@"%d item(s)",self.items.count]];
//    
//    [self setupToolbar];
//}


- (void) updateTotal {
    price = 0.0f;
    discountAmt = 0.0f;
    for (int i = 0;i < self.items.count; i++)
    {
        dataItemArray=nil;
        dataItemArray = [self.items objectAtIndex:i];
        if([dataItemArray objectForKey:@"Discounted_Price"])
        {
//            price = price + [[dataItemArray objectForKey:@"Discounted_Price"] doubleValue];
//            discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
//            
            
            // sample disc0unt amount
            
            Singleton *single = [Singleton retrieveSingleton];
            
            
            if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"notconfirmed"] )
                
            {
                
                NSLog(@"this is manage order scrrrn total %@", [dataItemArray description]);
                int qty = [[dataItemArray valueForKey:@"Qty"] intValue];
                
                
                
                //manage order with foc
                double totalPrice;
                
                if ([[dataItemArray valueForKey:@"Price"]isEqualToString:@"0"]) {
                    
                    //this is foc item
                    
                  totalPrice   = (double)qty * ([[dataItemArray valueForKey:@"Price"] doubleValue]);

                    
                    
                    
                }
                
                else
                {
                
                
                totalPrice = (double)qty * ([[dataItemArray valueForKey:@"Net_Price"] doubleValue]);
                }
                
                double dicounttedPrice = totalPrice * ([[dataItemArray valueForKey:@"DiscountPercent"] doubleValue])/(double)100;
                
                double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
                // [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
                
                double dicountedAmt = totalPrice - dicounttedPrice;
                
                //            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
                //            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
                
                price = price + [[NSString stringWithFormat:@"%f",dicountedAmt] doubleValue];
                discountAmt = discountAmt + [[NSString stringWithFormat:@"%f",dicounttedPrice] doubleValue];
                
                

                
                
            }
            
            else
            {
                            price = price + [[dataItemArray objectForKey:@"Discounted_Price"] doubleValue];
                            discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
            }
            
            
            
        }
        else
        {
            
            
            //case new item added in manage order / unconfirmed
            
            Singleton *single = [Singleton retrieveSingleton];
            
            
            if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"notconfirmed"] )
                
            {
                NSLog(@"this is manage order scrrrn total %@", [dataItemArray description]);
                int qty = [[dataItemArray valueForKey:@"Qty"] intValue];
                double totalPrice = (double)qty * ([[dataItemArray valueForKey:@"Net_Price"] doubleValue]);
                
                double dicounttedPrice = totalPrice * ([[dataItemArray valueForKey:@"DiscountPercent"] doubleValue])/(double)100;
                
                double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
                // [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
                
                double dicountedAmt = totalPrice - dicounttedPrice;
                
                //            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
                //            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
                
                price = price + [[NSString stringWithFormat:@"%f",dicountedAmt] doubleValue];
                discountAmt = discountAmt + [[NSString stringWithFormat:@"%f",dicounttedPrice] doubleValue];
                

            }
            
            
            else
            {
                            price = price + [[dataItemArray objectForKey:@"Price"] doubleValue];
                            discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];

            }
            
            
        }
        
        
        
        totalDiscountAmount = discountAmt;
        totalOrderAmount = price;
        
        NSLog(@"total order amount is %f", totalOrderAmount);
        
        //[totalPriceLabel setText:[[NSString stringWithFormat:@"%@ %.2f",NSLocalizedString(@"Total Price", nil), price] currencyString]];
        

    }
    
    NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
    
    
    //totalPriceLabel.text=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode, totalOrderAmount];

    
    
    
    totalPriceLabel.text=[[NSString stringWithFormat:@"%0.2f", totalOrderAmount] currencyString];
    
   // totalPriceLabel.text=[NSString stringWithFormat:@"AED%0.2f", totalOrderAmount];
    
    
    
//    totalDiscountAmount = discountAmt;
//    totalOrderAmount = price;
//    [totalPriceLabel setText:[[NSString stringWithFormat:@"%@ %.2f",NSLocalizedString(@"Total Price", nil), price] currencyString]];
//    
    
    if (self.items.count==0) {
        
        totalPriceLabel.text=@"";
    }
    
    [orderItemLbl setText:[NSString stringWithFormat:@"Order Items: %d item(s)",self.items.count]];
    
    [self setupToolbar];
}

- (void) saveTemplate:(id)sender{
    if(items.count!=0)
    {
        TamplateNameViewController *viewController = [[TamplateNameViewController alloc] initWithTemplateName] ;
        [viewController setPreferredContentSize:CGSizeMake(600, self.view.bounds.size.height - 100)];
        [viewController setTarget:self];
        [viewController setAction:@selector(itemUpdated:)];
        viewController.templateName = [[NSUserDefaults standardUserDefaults]valueForKey:@"Template_Name"];
        NSLog(@"templatename is %@",viewController.templateName);
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController] ;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        [popoverController setDelegate:self];
        [popoverController presentPopoverFromRect:saveAsTemplateButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
        //                           message:@"Please add items first"
        //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:nil];
    }
}


- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)saveTemplateTapped:(id)sender
{
    
    NSLog(@"save template called with items count %@", items);
    
    UIButton * saveTemplateBtn=sender;
    
    popoverController=nil;
    
    NSLog(@"save button frame is %@", NSStringFromCGRect(saveTemplateBtn.frame));
    
    CGRect buttonFrame=saveTemplateBtn.frame;
    
    buttonFrame.origin.y=saveTemplateBtn.frame.origin.y+60;
    
    
    if(items.count!=0)
    {
        TamplateNameViewController *viewController = [[TamplateNameViewController alloc] initWithTemplateName] ;
        [viewController setPreferredContentSize:CGSizeMake(600, self.view.bounds.size.height - 100)];
        [viewController setTarget:self];
        [viewController setAction:@selector(itemUpdated:)];
        viewController.templateName = [[NSUserDefaults standardUserDefaults] valueForKey:@"Template_Name"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController] ;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        [popoverController setDelegate:self];

       // [self.navigationController pushViewController:viewController animated:YES];
       [popoverController presentPopoverFromRect:buttonFrame inView:[self topMostController].view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
        //                           message:@"Please add items first"
        //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:nil];
    }
}
- (void) itemUpdated:(NSString *)item {
    NSCharacterSet *unacceptedInput = nil;
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    if ([[item componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1)
    {
        Singleton *single = [Singleton retrieveSingleton];
        if (item)
        {
            [popoverController dismissPopoverAnimated:YES];
            NSMutableArray *tempTemplate = [NSMutableArray array];
            NSMutableDictionary *dataDict;
            for (int i = 0;i < self.items.count; i++)
            {
                
                if([[[self.items objectAtIndex:i] stringForKey:@"priceFlag"] isEqualToString:@"N"] || [[[self.items objectAtIndex:i] stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
                {
                    dataDict =[NSMutableDictionary dictionary] ;
                    if(![[self.items objectAtIndex:i] objectForKey:@"ItemID"])
                    {
                        [dataDict setValue:[[self.items objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
                    }
                    else
                    {
                        [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"ItemID"] forKey:@"ItemID"];
                    }
                    [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"Qty"] forKey:@"Qty"];
                    [tempTemplate addObject:dataDict];
                }
            }
            
            //salesSer.delegate=self;
            if([item isEqualToString:name_TEMPLATES])
            {
                [[SWDatabaseManager retrieveManager] deleteTemplateOrderWithRef:[selectedOrder stringForKey:@"Order_Template_ID"]];
                [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Template saved" message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
                
                //                [ILAlertView showWithTitle:@"Template saved"
                //                                   message:nil
                //                          closeButtonTitle:NSLocalizedString(@"OK", nil)
                //                         secondButtonTitle:nil
                //                       tappedButtonAtIndex:nil];
            }
            else
            {
                BOOL isTheObjectThere = [single.savedOrderTemplateArray containsObject:item];
                if(isTheObjectThere)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Template name already exist." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    [alert show];
                    //                    [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
                    //                                       message:@"Template name already exist."
                    //                              closeButtonTitle:NSLocalizedString(@"OK", nil)
                    //                             secondButtonTitle:nil
                    //                           tappedButtonAtIndex:nil];
                }
                else
                {
                    [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Template saved", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    [alert show];
                    //                    [ILAlertView showWithTitle:@"Template saved"
                    //                                       message:nil
                    //                              closeButtonTitle:NSLocalizedString(@"OK", nil)
                    //                             secondButtonTitle:nil
                    //                           tappedButtonAtIndex:nil];
                }
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Template name cannot have special characters.Please correct name and try to save again" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
        //                           message:@"Template name cannot have special characters.Please correct name and try to save again"
        //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:nil];
    }
}
- (void) saveOrder:(id)sender{
    Singleton *single = [Singleton retrieveSingleton];
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init] ;
    NSMutableDictionary *salesOrder = [[NSMutableDictionary alloc] init] ;
    NSString *orderRef;
    
    [tempSalesArray setArray:nil];
    
    
    for (int i = 0 ; i < [self.items count]; i++){
        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
        //dataDict = ;
        if(![dataDict objectForKey:@"ItemID"])
        {
            [dataDict setValue:[dataDict stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
        }
        
    }
    for (int i = 0 ; i < [self.items count]; i++){
        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
        /// dataDict = [self.items objectAtIndex:i];
        if([dataDict objectForKey:@"Discounted_Price"])
        {
            [dataDict setValue:[dataDict objectForKey:@"Discounted_Price"] forKey:@"Price"];
        }
        [tempSalesArray addObject:dataDict];
    }
    
    self.items=[tempSalesArray copy];
    
    NSLog(@"ITEMS SET AT 2957");
    
   // NSLog(@"items in sales order %@", [self.items description]);
    
    [salesOrder setValue:self.items forKey:@"OrderItems"];
    [info setValue:[NSString stringWithFormat:@"%.2f",totalOrderAmount] forKey:@"orderAmt"];
    [info setValue:[NSString stringWithFormat:@"%.2f",totalDiscountAmount] forKey:@"discountAmt"];
    [info setValue:[category stringForKey:@"Category"] forKey:@"productCategory"];
    [salesOrder setValue:info forKey:@"info"];
    category =[SWDefaults productCategory];
    
    if([single.visitParentView isEqualToString:@"MO"] || single.isSaveOrder){
        if(![self.items count]==0)
        {
            
            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
            
            //salesSer.delegate=self;
            [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:single.savedPerformaCurrentOrder];
            orderRef = [[SWDatabaseManager retrieveManager] updatePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID] andOrderRef:single.savedPerformaCurrentOrder];
            
            
            //NSLog(@"orderRef %@",orderRef);
            orderAdditionalInfoViewController = nil;
            
            
            
            SWOrderConfirmationViewController* orderConfirmationVC=[[SWOrderConfirmationViewController alloc]init];
            orderConfirmationVC.proformaOrderRef=orderRef;
            orderConfirmationVC.salesOrderDict=salesOrder;
            orderConfirmationVC.customerDict=customer;
            orderConfirmationVC.warehouseDataArray=wareHouseDataOldSalesArray;
            orderConfirmationVC.itemsSalesOrder=[self.items mutableCopy];
            
            [self.navigationController pushViewController:orderConfirmationVC animated:YES];
            /*
            
            orderAdditionalInfoViewController = [[OrderAdditionalInfoViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
            orderAdditionalInfoViewController.performaOrderRef = orderRef;
            
            
            orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
            
            orderAdditionalInfoViewController.itemsSalesOrder=[self.items mutableCopy];
            
            
            
            NSLog(@"data being pushed to order addl final %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
            
                       // orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
            //NSLog(@"ware house data being pushed to order addl %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
            
            [self.navigationController pushViewController:orderAdditionalInfoViewController animated:YES];
             */
            
            orderAdditionalInfoViewController = nil;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
        }
    }
    else{
        if(![self.items count]==0)
        {
            single.isSaveOrder = YES;
            
            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
            
            
            //NSLog(@"OLA!!***** salesOrder %@",salesOrder);
            
            //salesSer.delegate=self;
            
            
            NSLog(@"performa order while saving is %@", salesOrder);
            
            orderRef =  [[SWDatabaseManager retrieveManager] savePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
            single.savedPerformaCurrentOrder = orderRef;
            
            orderAdditionalInfoViewController = nil;
            orderAdditionalInfoViewController = [[OrderAdditionalInfoViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
            orderAdditionalInfoViewController.performaOrderRef = orderRef;
            
            orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
             orderAdditionalInfoViewController.itemsSalesOrder=[self.items mutableCopy];
            
            //NSLog(@"data being pushed to order addl final %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
            
            //test new vc
            
            
            SWOrderConfirmationViewController* swConfirmationVC=[[SWOrderConfirmationViewController alloc]init];
            
            
            //[self.navigationController pushViewController:swConfirmationVC animated:YES];
            orderAdditionalInfoViewController = nil;
            
            
            
            
            
            
            
            swConfirmationVC.salesOrderDict=salesOrder;
            swConfirmationVC.customerDict=customer;
            swConfirmationVC.proformaOrderRef = orderRef;
            
            swConfirmationVC.warehouseDataArray=wareHouseDataSalesOrder;
            swConfirmationVC.itemsSalesOrder=[self.items mutableCopy];
            
            //NSLog(@"data being pushed to order addl final %@", [swConfirmationVC.warehouseDataArray description]);
            
            NSLog(@"customer dict before pushing %@", [swConfirmationVC.customerDict description]);
            
            
            [self.navigationController pushViewController:swConfirmationVC animated:YES];

            
            orderAdditionalInfoViewController = nil;

            
            
            
            
            
            
            
            
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
        }
        
    }
}


-(void)saveOrderTapped
{
    
    
    if (self.items.count>0) {
   
    Singleton *single = [Singleton retrieveSingleton];
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init] ;
    NSMutableDictionary *salesOrder = [[NSMutableDictionary alloc] init] ;
    NSString *orderRef;
    
    [tempSalesArray setArray:nil];
    
    
    for (int i = 0 ; i < [self.items count]; i++){
        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
        //dataDict = ;
        if(![dataDict objectForKey:@"ItemID"])
        {
            [dataDict setValue:[dataDict stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
        }
        
    }
    for (int i = 0 ; i < [self.items count]; i++){
        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
        /// dataDict = [self.items objectAtIndex:i];
        if([dataDict objectForKey:@"Discounted_Price"])
        {
            [dataDict setValue:[dataDict objectForKey:@"Discounted_Price"] forKey:@"Price"];
        }
        [tempSalesArray addObject:dataDict];
    }
    
    self.items=[tempSalesArray copy];
        
        NSLog(@"ITEMS SET AT 3314");

    
    NSLog(@"items in sales order %@", [self.items description]);
    
    

    
    for (NSInteger i=0; i<self.items.count; i++) {
        
//        NSString* appliedDiscount=[[self.items objectAtIndex:i] valueForKey:@"Applied_Discount"];
//        
//        NSMutableDictionary * currentDict=[self.items objectAtIndex:i];
//
//        [currentDict setValue:appliedDiscount forKey:[[self.items objectAtIndex:i] valueForKey:@"Discount"]];
//        
        
    }
    
    NSLog(@"check discount in items %@", [[self.items objectAtIndex:0] valueForKey:@"Applied_Discount"]);
    
    NSLog(@"check normal disc in items %@", [[self.items objectAtIndex:0] valueForKey:@"Discount"]);
    
    
    [salesOrder setValue:self.items forKey:@"OrderItems"];
    [info setValue:[NSString stringWithFormat:@"%.2f",totalOrderAmount] forKey:@"orderAmt"];
    [info setValue:[NSString stringWithFormat:@"%.2f",totalDiscountAmount] forKey:@"discountAmt"];
    [info setValue:[category stringForKey:@"Category"] forKey:@"productCategory"];
    [salesOrder setValue:info forKey:@"info"];
    category =[SWDefaults productCategory];
    
    
    
   // NSLog(@"sales order being saved into performa order %@", salesOrder);
    
    if([single.visitParentView isEqualToString:@"MO"] || single.isSaveOrder){
        if(![self.items count]==0)
        {
            
            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
            
            //salesSer.delegate=self;
            [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:single.savedPerformaCurrentOrder];
           
            orderRef = [[SWDatabaseManager retrieveManager] updatePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID] andOrderRef:single.savedPerformaCurrentOrder];
            
            
            //NSLog(@"orderRef %@",orderRef);
            orderAdditionalInfoViewController = nil;
            
            
            
            SWOrderConfirmationViewController* orderConfirmationVC=[[SWOrderConfirmationViewController alloc]init];
            orderConfirmationVC.proformaOrderRef=orderRef;
            orderConfirmationVC.salesOrderDict=salesOrder;
            orderConfirmationVC.customerDict=customer;
            orderConfirmationVC.warehouseDataArray=wareHouseDataOldSalesArray;
            orderConfirmationVC.itemsSalesOrder=[self.items mutableCopy];
            
            [self.navigationController pushViewController:orderConfirmationVC animated:YES];
            /*
             
             orderAdditionalInfoViewController = [[OrderAdditionalInfoViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
             orderAdditionalInfoViewController.performaOrderRef = orderRef;
             
             
             orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
             
             orderAdditionalInfoViewController.itemsSalesOrder=[self.items mutableCopy];
             
             
             
             NSLog(@"data being pushed to order addl final %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
             
             // orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
             //NSLog(@"ware house data being pushed to order addl %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
             
             [self.navigationController pushViewController:orderAdditionalInfoViewController animated:YES];
             */
            
            orderAdditionalInfoViewController = nil;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
        }
    }
    else{
        if(![self.items count]==0)
        {
            single.isSaveOrder = YES;
            
            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
            
            
            //NSLog(@"OLA!!***** salesOrder %@",salesOrder);
            
            //salesSer.delegate=self;
            orderRef =  [[SWDatabaseManager retrieveManager] savePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
            single.savedPerformaCurrentOrder = orderRef;
            
            orderAdditionalInfoViewController = nil;
            orderAdditionalInfoViewController = [[OrderAdditionalInfoViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
            orderAdditionalInfoViewController.performaOrderRef = orderRef;
            
            orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
            orderAdditionalInfoViewController.itemsSalesOrder=[self.items mutableCopy];
            
           // NSLog(@"data being pushed to order addl final %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
            
            //test new vc
            
            
            SWOrderConfirmationViewController* swConfirmationVC=[[SWOrderConfirmationViewController alloc]init];
            
            
            //[self.navigationController pushViewController:swConfirmationVC animated:YES];
            orderAdditionalInfoViewController = nil;
            
            
            
            
            
            
            
            swConfirmationVC.salesOrderDict=salesOrder;
            swConfirmationVC.customerDict=customer;
            swConfirmationVC.proformaOrderRef = orderRef;
            
            swConfirmationVC.warehouseDataArray=wareHouseDataSalesOrder;
            swConfirmationVC.itemsSalesOrder=[self.items mutableCopy];
            
           // NSLog(@"data being pushed to order addl final %@", [swConfirmationVC.warehouseDataArray description]);
            
            NSLog(@"customer dict before pushing %@", [swConfirmationVC.customerDict description]);
            
            
            [self.navigationController pushViewController:swConfirmationVC animated:YES];
            
            
            orderAdditionalInfoViewController = nil;
            
            
            
            
            
            
            
            
            
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add items first" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
        }
        
    }
        
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Items", nil) message:@"Please add items first and then save the order" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }

}

- (int)numberOfRowsInGrid:(GridView *)gridView
{
    return self.items.count;
}

//CRASH FIXED
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark GridView DataSource
- (int) numberOfColumnsInGrid:(GridView *)gridView {
    return 6;
}
- (float) gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return 39.0f;
        case 1:
            return 5.0f;
        case 2:
            return 14.0f;
        case 3:
            return 14.0f;
        case 4:
            return 14.0f;
        case 5:
            return 14.0f;
        default:
            break;
    }
    return 10.0f;
}
- (NSString *) gridView:(GridView *)gridView titleForColumn:(int)column {
    title=@"";
    switch (column) {
        case 0:
            title = NSLocalizedString(@"Name", nil);
            break;
        case 1:
            title = NSLocalizedString(@"Qty", nil);
            break;
        case 2:
            title = NSLocalizedString(@"Unit Price", nil);
            break;
        case 3:
            title = NSLocalizedString(@"Total", nil);
            break;
        case 4:
            title = NSLocalizedString(@"Discount", nil);
            break;
        case 5:
            title = NSLocalizedString(@"Net Amount", nil);
            break;
        default:
            break;
    }
    
    return title;
}
- (NSString *) gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    
    
    
    title = @"";
    dataItemArray=nil;
    dataItemArray = [self.items objectAtIndex:row];
    
    
    
    NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
    NSString* currecyCheckStr=[NSString stringWithFormat:@"%@0.00",userCurrencyCode];

    
//    NSString* titleStr=[dataItemArray valueForKey:@"Discounted_Amount"] ;
//
//    float titleVal=[titleStr floatValue]*100;
    NSString* unitPriceMO;
    NSString* checkNetPrice=[dataItemArray currencyStringForKey:@"Net_Price"];

    
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    {
        unitPriceMO=[dataItemArray stringForKey:@"Unit_Selling_Price"];
        
    }

    

    
    switch (column) {
        case 0:
            title = [dataItemArray stringForKey:@"Description"];
            NSLog(@"Title is %@",title);
            break;
        case 1:
            title = [dataItemArray stringForKey:@"Qty"];
            break;
        case 2:
            
            if (unitPriceMO!=nil) {
                
                
                NSLog(@"check net price here %@", [dataItemArray currencyStringForKey:@"Net_Price"]);
              
                //check for foc item
                if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {

               // if ([checkNetPrice isEqualToString:currecyCheckStr]) {
                    
                    title =checkNetPrice;
                }
                else
                {
                    
                    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
                        
                    {
                        
                        title=[NSString stringWithFormat:@"%@%@",userCurrencyCode,unitPriceMO];
                        
                    }
                   
                    else
                    {
                    
                    
                    title=[dataItemArray currencyStringForKey:@"Net_Price"];
                    }
                }

                
            }
            else
            {
                
                //if its a FOC it is displaying correctly in SO because of currency string for key method which lacks in manage orders
                
                
                NSLog(@"check net price here %@", [dataItemArray currencyStringForKey:@"Net_Price"]);

                
                
//                if ([manageOrder isEqualToString:@"notconfirmed"]) {
//                    
//                    //unconfirmed manage order
//                    
//                    //check for FOC now
//                    
//                    
//                    NSLog(@" check manual FOC item price %@", [dataItemArray valueForKey:@"Price"]);
//                    
//                    title=[dataItemArray currencyStringForKey:@"Price"];
//                    
//                    
//                    
//                    
//                }
                
                
                
                
                if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {
                   
                   
                   
                   //check if this is sales order FOC item
                   
                   if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {
                       //its not FOC item
                       
                       title=[dataItemArray currencyStringForKey:@"Price"];
                       
                       
                       
                       
                   }
                   
                   else
                   {
                   
                   
                    title =checkNetPrice;
                       
                   }
                }
                else
                {
                
                
                    title = [dataItemArray currencyStringForKey:@"Net_Price"];
                }
            }
            
            
            
            break;
        case 3:
            if([single.visitParentView isEqualToString:@"MO"])
               {
            
//                   double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
//                   
//                   double usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];
//
//                   double discountPrice=[[dataItemArray valueForKey:@"Discounted_Amount"] doubleValue];
//                   
//                   
//                   
//                   double tempTotal= qty*usp;
//                   
//                   double totalMO=tempTotal-discountPrice;
                   
                   double usp=0.0;
                   
                   
                   if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
                       
                   {
                       //foc
                       if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {

                       }
                       else
                       {
                       }
                       
                       usp=[[dataItemArray valueForKey:@"Unit_Selling_Price"] doubleValue];


                   }
                   
                   else
                   {
                       usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];

                   }
                   
                   double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
                   
                   NSLog(@"usp in managed order %0.2f", usp);
                   
                   
                   double discountPrice=[[dataItemArray valueForKey:@"DiscountPercent"] doubleValue];
                   NSLog(@"check discount percent %f", discountPrice);
                   
                   
                   
                   double tempTotal= qty*usp;
                   
                   discountPrice=discountPrice/100;
                   
                   double totalMO=tempTotal*discountPrice;
                   

                   
                   if ([manageOrder isEqualToString:@"notconfirmed"]) {
                       
                       //unconfirmed manage order
                       
                       //check for FOC now
                       
                       
                       
                       
                       if ([[dataItemArray valueForKey:@"Price"]isEqualToString:@"0"]) {
                           
                           //this is foc
                           title=[dataItemArray currencyStringForKey:@"Price"];

                       }
                       
                       else
                       {
                           
                           
                           
                       NSLog(@" check manual FOC item price %@", [dataItemArray valueForKey:@"Price"]);
                           title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,tempTotal];

                       
                       }
                       
                       
                   }

                 else  if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {
                   
                 //else  if ([checkNetPrice isEqualToString:currecyCheckStr]) {
                       
                       title =checkNetPrice;
                   }
                   else
                   {
                       
                       
                       title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,tempTotal];
                   }

                   
                   
               }
            
            else
            {
            title = [dataItemArray currencyStringForKey:@"Price"];
            }
            break;
        case 4:
            
            
            //calculate discount
            
            
            
              if ([single.visitParentView isEqualToString:@"MO"]&& ![manageOrder isEqualToString:@"confirmed"]) {
                  
                  NSLog(@"data item array in manager order discount %@", dataItemArray);
                  
                  double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
                  
                  double usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];
                  
                  double discountPrice=[[dataItemArray valueForKey:@"Discount"] doubleValue];
                  NSLog(@"check discount percent %f", discountPrice);
                  
                  
                  
                  double tempTotal= qty*usp;
                  
                  discountPrice=discountPrice/100;
                  
                  double totalMO=tempTotal*discountPrice;
                  
                  
                  NSLog(@"check total MO for discount %f", totalMO);
                  
                  NSLog(@"check discounted amount in doct %@", [dataItemArray valueForKey:@"Discounted_Price"]);
                  
                  NSLog(@"item price in manage order %@", [dataItemArray valueForKey:@"Price"]);
                  
                  //check for FOC ITEM
                  
                  if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {

                      title=[[NSString stringWithFormat:@"0"] currencyString];

                  }
                  
                else if ([checkNetPrice isEqualToString:currecyCheckStr]) {
                      
                      title =checkNetPrice;
                  }
                  else
                  {
                      title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,totalMO];
                  }

                  
              }
            else
            {
                
                
                
                //this is for foc
                NSLog(@"check for foc item in discount %@",[dataItemArray valueForKey:@"Price"] );
                
                
                if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {
                    
                    title=[[NSString stringWithFormat:@"0"] currencyString];
                }
                
                else
                {
            
            title = [dataItemArray currencyStringForKey:@"Discounted_Amount"];//[NSString stringWithFormat:@"AED%0.2f",titleVal];
                }
            }
            break;
        case 5:
            
            if ([single.visitParentView isEqualToString:@"MO"]&& ![manageOrder isEqualToString:@"confirmed"]) {
                
                
                double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
                
                double usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];
                
                double discountPrice=[[dataItemArray valueForKey:@"DiscountPercent"] doubleValue];
                
                double tempTotal= qty*usp;
                
                discountPrice=discountPrice/100;
                
                double totalMO=tempTotal*(1-discountPrice);
                
                
                if ([manageOrder isEqualToString:@"notconfirmed"]) {
                    
                    //unconfirmed manage order
                    
                    //check for FOC now
                    
                    
                    NSLog(@" check manual FOC item price %@", [dataItemArray valueForKey:@"Price"]);
                    
                    
                    
                    
                    if ([[dataItemArray valueForKey:@"Price"]isEqualToString:@"0"]) {
                        
                        title=[dataItemArray currencyStringForKey:@"Price"];

                        
                        
                    }
                    
                    else
                    {
                    
                        title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,totalMO];
                    
                    }
                    
                    
                    
                }

              else  if ([checkNetPrice isEqualToString:currecyCheckStr]) {
                    
                    title =checkNetPrice;
                }
                else
                {
                    
                    
                    title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,totalMO];
                }

                
                //title=[NSString stringWithFormat:@"AED%0.2f",totalMO];

                
                totalPriceforLbl=totalPriceforLbl+totalMO;
                
                
                
                //totalPriceLabel.text=[NSString stringWithFormat:@"AED%0.2f", totalMO];
                
            }
            
            else
            {
            title = [dataItemArray currencyStringForKey:@"Discounted_Price"];
                NSLog(@"total price is %0.2f", totalPriceforLbl);

                
            }
            break;
        default:
            break;
    }
    return title;
}
#pragma mark GridView Delegate
- (void) gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex{
    
    
    //Singleton *single = [Singleton retrieveSingleton];
    /*  if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
     {
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Confirmed orders cannot be changed" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
     [alert show];
     
     //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
     //                           message:@"Confirmed orders cannot be changed"
     //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
     //                 secondButtonTitle:nil
     //               tappedButtonAtIndex:nil];
     }
     else*/
    //{
    if(![[[self.items objectAtIndex:rowIndex] valueForKey:@"Price"] isEqualToString:@"0"])
    {
        deleteRowDict=nil;
        deleteRowDict = [self.items objectAtIndex:rowIndex];
        NSString *guid = [deleteRowDict stringForKey:@"Guid"];
        if ([deleteRowDict objectForKey:@"ParentGuid"])
        {
            guid = [deleteRowDict objectForKey:@"ParentGuid"];
        }
        
        for (int i = 0;i < self.items.count; i++)
        {
            dataItemArray=nil;
            dataItemArray = [self.items objectAtIndex:i];
            if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:guid])
            {
                deleteRowDict=nil;
                deleteRowDict = dataItemArray ;
                break;
            }
        }
        if (deleteRowDict)
        {
            [deleteRowDict setValue:[deleteRowDict stringForKey:@"Bonus"] forKey:@"GivenBonus"];
            [self presentProductOrder:deleteRowDict];
        }
    }
    else
    {
        // No joy...
        // aww..why?
        deleteRowDict=nil;
        deleteRowDict = [self.items objectAtIndex:rowIndex-1];
        NSString *guid = [deleteRowDict stringForKey:@"Guid"];
        if ([deleteRowDict objectForKey:@"ParentGuid"])
        {
            guid = [deleteRowDict objectForKey:@"ParentGuid"];
        }
        
        for (int i = 0;i < self.items.count; i++)
        {
            dataItemArray=nil;
            dataItemArray = [self.items objectAtIndex:i];
            if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:guid])
            {
                deleteRowDict = nil;
                deleteRowDict = dataItemArray ;
                break;
            }
        }
        if (deleteRowDict)
        {
            [deleteRowDict setValue:[deleteRowDict stringForKey:@"Bonus"] forKey:@"GivenBonus"];
            [self presentProductOrder:deleteRowDict];
        }
        
    }
    //}
}
- (void) gridView:(GridView *)gv commitDeleteRowAtIndex:(int)rowIndex {
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.items];
    deleteRowDict=nil;
    deleteRowDict = [array objectAtIndex:rowIndex];
    
    NSLog(@"deleting row %@",deleteRowDict);
    
    
    if ([self.delegate respondsToSelector:@selector(productDeleted:)]) {
        
        
        [self.delegate productDeleted:deleteRowDict];
    }
    
    
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"])
    {
        single.savedPerformaCurrentOrder =  [[self.items objectAtIndex:0] stringForKey:@"Orig_Sys_Document_Ref"];
    }
    
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Confirmed orders cannot be changed" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
        //                           message:@"Confirmed orders cannot be changed"
        //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:nil];
        
        
    }
    else
    {
        if(![[deleteRowDict valueForKey:@"Price"] isEqualToString:@"0"])
        {
            if(rowIndex+1 < [array count] && [[[array objectAtIndex:rowIndex+1]stringForKey:@"priceFlag"] isEqualToString:@"M"])
            {
                if(rowIndex+2 < [array count] &&[[[array objectAtIndex:rowIndex+2]stringForKey:@"priceFlag"] isEqualToString:@"F"])
                {
                    //For F
                    [array removeObjectAtIndex:rowIndex+2];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+2];
                    //For M
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                    
                }
                else
                {
                    //For M
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                }
            }
            else if(rowIndex+1 < [array count] && [[[array objectAtIndex:rowIndex+1]stringForKey:@"priceFlag"] isEqualToString:@"F"])
            {
                
                if(rowIndex+2 < [array count] &&[[[array objectAtIndex:rowIndex+2]stringForKey:@"priceFlag"] isEqualToString:@"M"])
                {
                    // M
                    [array removeObjectAtIndex:rowIndex+2];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+2];
                    //for F
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                }
                else
                {
                    //For F
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                }
            }
            
            
            [array removeObjectAtIndex:rowIndex];
            [self setItems:array];
            [gv deleteRowAtIndex:rowIndex];
            [self updateTotal];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Bonus product can not be deleted" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            
            //            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
            //                               message:@"Bonus product can not be deleted"
            //                      closeButtonTitle:NSLocalizedString(@"OK", nil)
            //                     secondButtonTitle:nil
            //                   tappedButtonAtIndex:nil];
            
        }
        
    }
    [totalProduct setText:[NSString stringWithFormat:@"Order Items: %d item(s)",self.items.count]];
    
}
#pragma saleorder delegate

//solved the discount issue here

- (void) dbGetSalesOrderServiceDiddbGetConfirmOrderItems:(NSArray *)performaOrder{
    for(int i = 0; i < performaOrder.count; i++)
    {
        NSDictionary *row = [performaOrder objectAtIndex:i];
        if([[row stringForKey:@"Unit_Selling_Price"] isEqualToString:@"0"])
        {
            [row setValue:[[performaOrder objectAtIndex:i-1] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
        }
        else
        {
            if (![[row stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
            {
                if(![[row stringForKey:@"Def_Bonus"] isEqualToString:@""])
                {
                    [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
                    [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"GivenBonus"];
                    [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Bonus"];
                    [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"FOC_Bonus"];
                }
            }
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            
            int qty = [[row stringForKey:@"Ordered_Quantity"] intValue];
            double totalPrice = (double)qty * ([[row objectForKey:@"Unit_Selling_Price"] doubleValue]);
            
            [row setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            double dicounttedPrice = totalPrice * ([[row objectForKey:@"Custom_Attribute_3"] doubleValue]);
            
            double dicountedAmt = totalPrice - dicounttedPrice;
            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
            [row setValue:[NSString stringWithFormat:@"%@",[row objectForKey:@"Custom_Attribute_3"]] forKey:@"DiscountPercent"];
            
            double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
            [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
        }
        
    }
    [self setItems:performaOrder];
    [self.gridView reloadData];
    [self updateTotal];
    
}
- (void) dbGetSalesOrderServiceDiddbGetPerformaOrderItems:(NSArray *)performaOrder{
    // NSLog(@"Order %@",performaOrder);
    
    for(int i = 0; i < performaOrder.count; i++)
    {
        
        NSDictionary *row = [performaOrder objectAtIndex:i];
        
        if([[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
        {
            if(i+1 < [performaOrder count] && [[[performaOrder objectAtIndex:i+1]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
            {
                [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
                [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"FOC_Product"];
                
                if(i+2 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+2]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
                {
                    [row setValue:[[performaOrder objectAtIndex:i+2] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
                    [row setValue:[performaOrder objectAtIndex:i+2] forKey:@"Bonus_Product"];
                }
            }
            else if(i+1 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+1]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
            {
                [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
                [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"Bonus_Product"];
                
                if(i+2 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+2]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
                {
                    [row setValue:[[performaOrder objectAtIndex:i+2] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
                    [row setValue:[performaOrder objectAtIndex:i+2] forKey:@"FOC_Product"];
                    //[row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
                }
            }
            Singleton *single = [Singleton retrieveSingleton];
            single.savedPerformaCurrentOrder = [row stringForKey:@"Orig_Sys_Document_Ref"];
            [row setValue:[row stringForKey:@"Calc_Price_Flag"] forKey:@"priceFlag"];
            
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            
            int qty = [[row stringForKey:@"Ordered_Quantity"] intValue];
            double totalPrice = (double)qty * ([[row objectForKey:@"Unit_Selling_Price"] doubleValue]);
            
            [row setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            [row setValue:[row stringForKey:@"Custom_Attribute_2"] forKey:@"lineItemNotes"];
            
            double dicounttedPrice = totalPrice * ([[row objectForKey:@"Custom_Attribute_3"] doubleValue])/(double)100;
            
            double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
            [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
            
            double dicountedAmt = totalPrice - dicounttedPrice;
            
            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
            [row setValue:[NSString stringWithFormat:@"%@",[row objectForKey:@"Custom_Attribute_3"]] forKey:@"DiscountPercent"];
        }
        
        else if([[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"] || [[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
        {
            if([[[performaOrder objectAtIndex:i-1] stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
            {
                [row setValue:[[performaOrder objectAtIndex:i-1] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
            }
            else
            {
                [row setValue:[[performaOrder objectAtIndex:i-2] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
            }
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
            [row setValue:[row stringForKey:@"Calc_Price_Flag"] forKey:@"priceFlag"];
        }
        
        [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
    }
    [self setItems:performaOrder];
    [self.gridView reloadData];
    [self updateTotal];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC
{
    popoverController=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void) viewDidUnload{
    // //NSLog(@" Sales Order NLOAD");
    
}
- (void) viewDidDisappear:(BOOL)animated{
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void) didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
