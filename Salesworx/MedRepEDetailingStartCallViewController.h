//
//  MedRepEDetailingStartCallViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/5/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepEdetailTaskViewController.h"
#import <MediaPlayer/MPMoviePlayerViewController.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepProductImagesViewController.h"
#import "SamplesTableViewCell.h"
#import "MedRepEDetailingQuantityPickerViewController.h"
#import "MedRepButton.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepTextField.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepEdetailTaskViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepCustomClass.h"
#import "SWAppDelegate.h"
#import "MedRepEDetailingProductsViewController.h"
#import "SWDefaults.h"
#import "MedRepVisitsEndCallViewController.h"
#import "MedRepVisitsStartMultipleCallsPresentationCollectionViewCell.h"
#import "UCSPresentationViewerViewController.h"
#import "SalesWorxPresentationHomeViewController.h"
#import "SalesWorxCoachReportPharmacyDetailsViewController.h"
#import "SalesWorxCoachReportViewController.h"

#define kFieldMarketingEdetailinigProductTitle @"Products"

#define kFieldMarketingEdetailingLotNumberTitle @"Lot Number"

#define kFieldMarketingEdetailinigItemTypeTitle @"Product Type"

@interface MedRepEDetailingStartCallViewController : SWViewController<UIPopoverControllerDelegate,StringPopOverViewController_ReturnRow,UITextFieldDelegate,ViewedImagesDelegate,QuantityPickerDelegate,ViewedPDFDelegate,ProductVideoDelegate,TaskPopOverDelegate,EDetailingNotesDelegate, VisitCompletedWithOutCallsDelegate,UCSPresentationDelegate>

{
    IBOutlet SalesWorxImageView *taskStatusImageView;
    IBOutlet MedRepElementTitleLabel *doctorPharmacyHeaderlbl;
    NSMutableArray* filteredProducts;
    SWAppDelegate *appDelegate;
    NSMutableArray* productMediaFilesArray;
    NSMutableArray* productVideoFilesArray;
    NSMutableArray* productPdfFilesArray;
    NSMutableArray* productImagesArray;
    NSMutableArray * presentationsArray;
    ProductMediaFile* SelectedProductMediaFile;
    IBOutlet SalesWorxImageView *notesStatusImageView;
    NSMutableArray* distinctMediaArray;
    IBOutlet MedRepTextField *productMediaTextField;
    NSIndexPath* selectedCustomIndexPath;
    
    NSMutableArray* productDetailsArray;
    
    NSMutableArray* sampleProductsArray;
    
    NSMutableArray* qtyArray;
    
    BOOL isSearching;
    
    MPMoviePlayerViewController *moviePlayer;
    NSMutableArray* unfilteredMediaArray;
    
    NSMutableArray * imagesPathArray;
    
    
    NSIndexPath *selectedIndexPath;
    
    NSIndexPath *selectedLotNumberIndex;
    
    NSMutableArray* collectionViewDataArray;
    
    NSDate * callDate;
    
    NSTimer* faceTimeTimer;
    
    IBOutlet UIImageView *expiryImgView;
    IBOutlet UIImageView *avblQtyImgView;
    NSMutableArray* demoPlanMediaFiles;
    
    UIPopoverController* popOverController;
    
    NSMutableArray* selectedSamplesArray;
    
    StringPopOverViewController_ReturnRow* locationPopOver;
    
    NSMutableDictionary* selectedSamplesDictionary;
    
    NSMutableArray* samplesDataArray;
    
    NSInteger selectedIndexValue;
    
    IBOutlet UIButton *taskStatusButton;
    
    IBOutlet UIButton *notesStatusButton;
    UIPopoverController * filterPopOverController;
    NSString * selectedProductID;
    NSString *selectedStockrowId;
    
    NSMutableArray* selectedSampleProductsArray;
    
    NSMutableArray* empStockArray;

    
    NSMutableArray * demoedMediaFiles;
    
    
    BOOL clearSelectedCells;
    
    NSString* popString;
    
    NSMutableArray* tempSelectedProducts;
    
    NSNumber* deletedIndex;
    
    BOOL presentationSelected;

    
    IBOutlet UIView *samplesBgView;
    
    
    IBOutlet UIButton *productButton;
    
    IBOutlet UIButton *lotNumberButton;
    
    
    IBOutlet UIButton *qtyButton;
    
    IBOutlet UIButton *tasksButton;
    
    IBOutlet UIButton *notesButton;
    
    
    IBOutlet MedRepTextField *productTextField;

    IBOutlet MedRepTextField *lotNumberTextField;
    IBOutlet MedRepTextField *QtyTextField;
    IBOutlet MedRepTextField *itemTypeTextField;
    
    NSString* selectedLotNumberforProductID;
    
    NSString* productExpiryDate;

    NSMutableArray *VisitSamplesProductsArray;
    NSMutableArray *VisitGivenSampleProductsArray;
    NSMutableArray *ProductLotsArray;
    NSMutableArray *ProductNamesArray;
    NSMutableArray *ProductIdsArray;
    NSMutableArray *ItemTypeArray;
    NSMutableArray *ItemCodeValueArray;
    
    IBOutlet UIButton *CancelButton;
    BOOL isTableViewSampleProductSelected;
    SampleProductClass *tableSelectedProductDetails;

    SampleProductLotClass *SelectedSampleProductLot;
    NSMutableArray* surveysArray;
    
    
    NSInteger  selectedCellIndex;
    
    UIPopoverController * multiCallsPopOverController;
    
    IBOutlet NSLayoutConstraint * xPPTButtonTrailingMArginConstraintToParentView;
    
    IBOutlet UIView *itemTypeView;
    IBOutlet NSLayoutConstraint *xItemViewWidthConstraint;
    IBOutlet NSLayoutConstraint *xItemViewTrailingConstraint;
}
-(IBAction)cancelButtonTapped:(id)sender;
- (IBAction)qtyButtonTapped:(id)sender;
-(NSString*)stringByTrimmingLeadingWhitespace:(NSString*)string;
@property (strong, nonatomic) IBOutlet UICollectionView *demoPlanProductsCollectionView;
@property (strong, nonatomic) IBOutlet UILabel *faceTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel *avblQtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *doctorLbl;
@property (strong, nonatomic) IBOutlet UILabel *samplesLbl;

@property (strong, nonatomic) IBOutlet UILabel *locationLbl;

@property (strong, nonatomic) IBOutlet UIButton *imagesButton;
@property (strong, nonatomic) IBOutlet UIButton *addProductsBtn;

@property (strong, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *btnProduct;
@property (weak, nonatomic) IBOutlet UIButton *btnPresentation;

@property (strong, nonatomic) IBOutlet UIButton *pptButton;

- (IBAction)productDropDownTapped:(id)sender;

@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;

@property (strong, nonatomic) IBOutlet UILabel *expiryDateLbl;

@property (strong, nonatomic) IBOutlet UITextField *qtyTxtFld;
- (IBAction)addProductButtontapped:(id)sender;
- (IBAction)lotNumberDropDownTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *samplesTblView;
@property (strong, nonatomic) IBOutlet UIView *tblBorderView;

@property(strong,nonatomic) VisitDetails * visitDetails;

- (IBAction)productsButtonTapped:(id)sender;

- (IBAction)btnPresentationTapped:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *borderView;
@property (strong, nonatomic) IBOutlet MedRepButton *addButton;

@property(nonatomic) BOOL isFromDashboard;
@property (strong, nonatomic) IBOutlet MedRepButton *customPresentationsButton;
- (IBAction)customPresentationsButtontapped:(id)sender;

@end
