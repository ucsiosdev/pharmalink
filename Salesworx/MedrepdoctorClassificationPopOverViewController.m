//
//  MedrepdoctorClassificationPopOverViewController.m
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 12/5/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "MedrepdoctorClassificationPopOverViewController.h"
//classificationpopUptableVeiwCellIdentifier
#import "MedrepdoctorClassificationPopOverTableViewCell.h"
#import "SWDefaults.h"
@interface MedrepdoctorClassificationPopOverViewController ()

@end

@implementation MedrepdoctorClassificationPopOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    UIBarButtonItem * closebutton = [[UIBarButtonItem alloc ]initWithTitle:NSLocalizedString(kCloseTitle,nil) style:UIBarButtonItemStylePlain target:self action:@selector(CloseButtonTapped)];
    [closebutton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:kSWX_FONT_SEMI_BOLD(14),NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = closebutton;
}
-(void)CloseButtonTapped{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MedrepdoctorClassificationPopOverTableViewCell* cell = (MedrepdoctorClassificationPopOverTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"classificationpopUptableVeiwCellIdentifier"];
    
    if (cell==nil) {
        NSArray* nibArray=[[NSBundle mainBundle]loadNibNamed:@"MedrepdoctorClassificationPopOverTableViewCell" owner:self options:nil];
        cell=[nibArray objectAtIndex:0];
        
    }
    
    if(indexPath.row==0){
        cell.IconImageView.image=[UIImage imageNamed:@"PatientIcon"];
        cell.TitleLbl.text=@"Patient Volume";
        cell.valueLbl.text=@"1000";

    }else if(indexPath.row==1){
        cell.IconImageView.image=[UIImage imageNamed:@"brandAffinityIcon"];
        cell.TitleLbl.text=@"Brand Affinity";
        cell.valueLbl.text=@"600";
        
    }else if(indexPath.row==2){
        cell.IconImageView.image=[UIImage imageNamed:@"SpecializationIcon"];
        cell.TitleLbl.text=@"Specialization Volume";
        cell.valueLbl.text=@"400";
        
    }else if(indexPath.row==3){
        cell.IconImageView.image=[UIImage imageNamed:@"ProductAffinityIcon"];
        cell.TitleLbl.text=@"Product Affinity";
        cell.valueLbl.text=@"600";
        
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
