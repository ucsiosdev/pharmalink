//
//  StockInfoTableViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/1/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesOrderNewViewController.h"
#import "StockInfoTableViewCell.h"
#import <StockInfoTableFooterCell.h>

@interface StockInfoTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSString * productCodeString;
    NSString * numberStr;
    NSNumber  *strtoNumber;
    StockInfoTableViewCell *cell;
    StockInfoTableFooterCell * footerView;
    NSMutableArray *savedTextFieldArray;
    NSMutableArray * avblStockArray;
    BOOL saved;
    NSString *totalString;
    BOOL allocatedQuantityLabel;
    UIAlertView *misMatchAlert;
    BOOL bachButtonPressed;
    BOOL saveButtonPressed;
    int total;
    int productQty;
    int allocatedTotal;
    BOOL QuantityExceeds;
    
    
}
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
- (NSMutableDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array;
@property (strong, nonatomic) IBOutlet UITableView *stockTblView;
@property (strong, nonatomic) IBOutlet UIButton *save;

//@property (strong, nonatomic) IBOutlet UITableView *tableView;
//@property(strong,nonatomic)UITableView * stockInfoTableView;
@property (strong, nonatomic) IBOutlet UILabel *productTextHeader;
- (IBAction)Save:(id)sender;
- (IBAction)Back:(id)sender;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


@property(strong,nonatomic)IBOutlet UIButton* saveButton;
@property(strong,nonatomic)NSMutableDictionary* TBL_Product_Dictionary;

@property(strong,nonatomic) NSString * productCodeText;

@property(strong,nonatomic)SalesOrderNewViewController * salesOrderNewVC;

@property(strong,nonatomic) NSString * productCodeString;

@property(strong,nonatomic)NSString * productName;
@property (strong, nonatomic) IBOutlet UILabel *lblProductCode;
@property(strong,nonatomic)NSArray * stockArray;

@property(strong,nonatomic)NSString * productQtyTxtFieldText;
@property(strong,nonatomic)NSMutableArray * savedStockInfoArray;
@property(strong,nonatomic)ProductStockViewController * pVC;


@end
