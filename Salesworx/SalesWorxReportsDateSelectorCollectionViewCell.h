//
//  SalesWorxReportsDateSelectorCollectionViewCell.h
//  CustomReportDateSelector
//
//  Created by Pavan Kumar Singamsetti on 11/2/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxReportsDateSelectorCollectionViewCell : UICollectionViewCell
@property (strong,nonatomic) IBOutlet UILabel * TitleLabel;
@end
