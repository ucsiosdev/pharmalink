//
//  SalesWorxCustomerListTableViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/29/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxCustomerListTableViewController : UITableViewController

@property(strong,nonatomic) NSMutableArray * customerListContentArray;
@property(strong,nonatomic) NSString* titleKey;

@property(strong,nonatomic) NSString* descKey;

@end
