//
//  UIDeviceHardware.h
//  Salesworx
//
//  Created by msaad on 18/11/13.
//  Copyright (c) 2013 msaad. All rights reserved.

#import <Foundation/Foundation.h>

@interface UIDeviceHardware : NSObject 

- (NSString *) platform;
- (NSString *) platformString;

@end