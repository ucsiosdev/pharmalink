//
//  TemplateViewController.h
//  Salesworx
//
//  Created by msaad on 5/26/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SWPlatform.h"
#import "SalesOrderViewController.h"
@interface TemplateViewController : SWViewController <UIAlertViewDelegate   , GridViewDataSource , GridViewDelegate>
{
    NSMutableDictionary *customer;
    //
    GridView *gridView;
    NSMutableArray *templateOrderArray;
    NSMutableArray *totalOrderArray;
    SalesOrderViewController  *salesOrderViewController;
    
}
- (id)initWithCustomer:(NSDictionary *)customer;
@end