//
//  SalesWorxDashboardViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/25/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "SimpleBarChart.h"
#import "BEMSimpleLineGraphView.h"
#import "PNChartDelegate.h"
#import "PNCircleChart.h"
#import "PNChart.h"
#import "SWAppDelegate.h"
#import "GKLineGraph.h"
#import "SalesWorxImageBarButtonItem.h"
#import "TaskPopoverViewController.h"
#import "AppControl.h"

@interface SalesWorxDashboardViewController : SWViewController<SimpleBarChartDataSource, SimpleBarChartDelegate,BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate,PNChartDelegate, GKLineGraphDataSource, TaskPopoverDelegate>
{
    AppControl *appControl;
    
    PNCircleChart *circleChartForDoctors;
    PNCircleChart *circleChartForPhamacies;
    PNCircleChart *circleChartForTasks;
    PNCircleChart *circleChartForProductiveVisits;
    
    NSArray *_values;
    SimpleBarChart *_chart;
    
    NSArray *_barColors;
    NSInteger _currentBarColor;
    NSMutableArray *arrproductName;
    
    int previousStepperValue;
    int totalNumber;
    NSString *monthString;
    SWAppDelegate *appDel;

    BOOL isEmptyGraph;
    IBOutlet UILabel *monthLabel;
    
    NSMutableArray *arrVisitFrequency;
    IBOutlet UIImageView *viewDemonstratedGraph;
    SalesWorxImageBarButtonItem *btnTask;
    
    NSArray *sortedPlannedVisitArray;
    IBOutlet SalesWorxDefaultButton *btnVisit;
}
@property (weak, nonatomic) IBOutlet UIView *viewUpcomingAppointment;

@property (strong, nonatomic) IBOutlet GKLineGraph *gkLineGraph;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *labels;

@property (nonatomic) PNLineChart * lineChart;

@property (strong, nonatomic) IBOutlet BEMSimpleLineGraphView *myGraph;
@property (strong, nonatomic) NSMutableArray *ypoints;
@property (strong, nonatomic) NSMutableArray *xPoints;


@property (weak, nonatomic) IBOutlet UILabel *lblDoctorName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblTelephoneNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblATime;


@property (weak, nonatomic) IBOutlet UIView *viewFaceTimeVsWaitTime;


@property (weak, nonatomic) IBOutlet UIView *viewDoctorsVisit;
@property (weak, nonatomic) IBOutlet UILabel *lblDoctorsVisitsRequired;
@property (weak, nonatomic) IBOutlet UILabel *lblDoctorsVisitsCompleted;


@property (weak, nonatomic) IBOutlet UIView *viewPharmaciesVisit;
@property (weak, nonatomic) IBOutlet UILabel *lblPharmaciesVisitsRequired;
@property (weak, nonatomic) IBOutlet UILabel *lblPharmaciesVisitsCompleted;


@property (weak, nonatomic) IBOutlet UIView *viewProductiveVisits;
@property (weak, nonatomic) IBOutlet UILabel *lblProductsDemonstrated;
@property (weak, nonatomic) IBOutlet UILabel *lblSamplesGiven;


@property (weak, nonatomic) IBOutlet UIView *viewTasks;
@property (weak, nonatomic) IBOutlet UILabel *lblTasksOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblTasksCompleted;


@property (weak, nonatomic) IBOutlet UIView *viewVisitfrquency;
@property (weak, nonatomic) IBOutlet UIView *viewDemonstratedProducts;

@property(nonatomic) NSMutableArray *arrDoctorsVisitsRequired;
@property(nonatomic) NSMutableArray *arrDoctorsVisitsCompleted;

@property(nonatomic) NSMutableArray *arrPharmaciesVisitsRequired;
@property(nonatomic) NSMutableArray *arrPharmaciesVisitsCompleted;

@property(nonatomic) NSMutableArray *arrTasksOpen;
@property(nonatomic) NSMutableArray *arrTasksCompleted;



@end
