//
//  MedRepCreateVisitDatePickerViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/18/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepCreateVisitDatePickerViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@interface MedRepCreateVisitDatePickerViewController ()

@end

@implementation MedRepCreateVisitDatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchDown];
    button.tintColor=[UIColor blackColor];
    button.titleLabel.font=MedRepTitleFont;
    
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    button.frame = CGRectMake(10.0, 15.0, 86, 30.0);
    [self.view addSubview:button];
    
    UIButton *buttonDone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonDone addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchDown];
    buttonDone.tintColor=[UIColor blackColor];
    buttonDone.titleLabel.font=MedRepTitleFont;
    [buttonDone setTitle:@"Done" forState:UIControlStateNormal];
    buttonDone.frame = CGRectMake(230.0, 15.0, 86, 30.0);
    [self.view addSubview:buttonDone];
    
    datepicker=[[UIDatePicker alloc]init];//Date picker
    datepicker.frame=CGRectMake(0,40,320, 216);
    
    datepicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    [datepicker setMinimumDate:[NSDate date]];
    [datepicker setMinuteInterval:1];
    [datepicker setTag:10];
    //[datepicker addTarget:self action:@selector(result) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datepicker];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
