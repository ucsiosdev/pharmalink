//
//  MedRepEDetailingQuantityPickerViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/24/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuantityPickerDelegate <NSObject>

-(void)selectedQuantity:(NSIndexPath*)indexPath;


@end

@interface MedRepEDetailingQuantityPickerViewController : UIViewController
{
    id quantityPickerDelegate;

}

@property(strong,nonatomic) id quantityPickerDelegate;
@property(strong,nonatomic) UIPopoverController* qtyPopoverController;

@property(strong,nonatomic) NSMutableArray* qtyArray;

@end
