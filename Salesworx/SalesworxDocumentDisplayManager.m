//
//  SalesworxDocumentDisplayManager.m
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 11/15/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesworxDocumentDisplayManager.h"

@implementation SalesworxDocumentDisplayManager
- (void)ShowUserActivityShareViewInView:(UIView *)SourceView SourceRect:(CGRect)SourceFrame AndFilePath:(NSString *)filePath{
    NSURL *imageURL=[NSURL fileURLWithPath:filePath];
    docIntController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
    docIntController.delegate = self;
    [docIntController presentOptionsMenuFromRect:SourceFrame inView:SourceView animated:YES];
}
// Preview presented/dismissed on document.  Use to set up any HI underneath.
- (void)documentInteractionControllerWillBeginPreview:(UIDocumentInteractionController *)controller{
    NSLog(@"documentInteractionControllerWillBeginPreview");
}
- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller{
    NSLog(@"documentInteractionControllerDidEndPreview");
}
@end
