//
//  TextTypeQuestionView.m
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "TextTypeQuestionView.h"

@implementation TextTypeQuestionView
@synthesize tableViewController;
#define kAnswerField 1
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setTableViewController:[[UITableView alloc] initWithFrame:self.frame style:UITableViewStyleGrouped]];
        [self.tableViewController setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.tableViewController setDelegate:self];
        [self.tableViewController setDataSource:self];
        self.tableViewController.backgroundView = nil;
        self.tableViewController.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.tableViewController];
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    return 2;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 1;
    }
    else
    {
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
    }
    

    
    if (indexPath.section==0 && indexPath.row == 0)
    {
        ((SWTextFieldCell *)cell).label.text = @"Question :";
        ((SWTextFieldCell *)cell).textField.text = @"What is your name?";
        ((SWTextFieldCell *)cell).textField.userInteractionEnabled = NO ;
    }
    else if (indexPath.section==1 &&indexPath.row == 0)
    {
        ((SWTextFieldCell *)cell).textField.tag = kAnswerField;
        ((SWTextFieldCell *)cell).label.text = @"Answer :";
        ((SWTextFieldCell *)cell).textField.placeholder = @"Type your answer";
        ((SWTextFieldCell *)cell).textField.text =answerString ;
    }
 
    return cell;
}
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0)
    {
        return @"Question";
    }
    else
    {
        return @"Answer";
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    
    return 50.0f;
    
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
    if (s ==0)
    {
        
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Question"];
        return sectionHeader;
    }
    else {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Answer"];
        return sectionHeader;    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tv deselectRowAtIndexPath:indexPath animated:YES];
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField.tag == kAnswerField) {
        answerString = textField.text;
    } 
    
   
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (answerString.length > 0 )
    {
//        [self.dataDict removeAllObjects];
//        [self.dataDict setValue:self.name forKey:@"cash_Name"];
//        [self.dataDict setValue:self.phone forKey:@"cash_Phone"];
    }
    
    // if the e-mail address has a value but the password doesn't, select the password
    if ([answerString length] > 0) {
        [[(SWTextFieldCell *)[self.tableViewController cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] textField] becomeFirstResponder];
    }
    [self.tableViewController reloadData];
    return YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
 */


@end
