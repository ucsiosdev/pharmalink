//
//  SalesWorxPopOverNavigationBarButtonItem.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPopOverNavigationBarButtonItem.h"

@implementation SalesWorxPopOverNavigationBarButtonItem

@synthesize sourceButton;

- (instancetype)initWithImage:(nullable UIImage *)buttonImage style:(UIBarButtonItemStyle)style target:(nullable id)target action:(nullable SEL)action
{
    sourceButton =[[UIButton alloc]initWithFrame:CGRectMake(0, 0, buttonImage.size.width+10, buttonImage.size.height)];
    sourceButton.imageView.contentMode=UIViewContentModeScaleAspectFit;
    sourceButton.tintColor=[UIColor whiteColor];
    [sourceButton setImage:[[buttonImage imageFlippedForRightToLeftLayoutDirection] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [sourceButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    self.tintColor=[UIColor whiteColor];
    self=[super initWithCustomView:sourceButton];
    
    return self;
}

@end
