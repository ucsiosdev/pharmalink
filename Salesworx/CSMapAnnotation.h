//
//  CSMapAnnotation.h
//  mapLines
//
//  Created by Craig on 5/15/09.
//  Copyright 2009 Craig Spitzkoff. All rights reserved.
// -fno-objc-arc

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Place.h"
// types of annotations for which we will provide annotation views. 
@interface CSMapAnnotation : NSObject <MKAnnotation>
{
	CLLocationCoordinate2D _coordinate;
	NSString*              _title;
    NSString*              _subtitle;
	NSString*              _userData;
	NSURL*                 _url;
	Place* place;
}
@property (nonatomic, strong) NSString* userData;
@property (nonatomic, strong) NSURL* url;
@property (nonatomic, strong) Place* place;

-(id) initWithPlace: (Place*) p;

@end
