//
//  ProductListViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "ProductListViewController.h"
#import "SWSection.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "ProductBrandFilterViewController.h"
#import "SWCustomerListCell.h"
@interface ProductListViewController ()
- (void)setupToolbar;
- (void)executeSearch:(NSString *)term;
@end

@implementation ProductListViewController
@synthesize target;
@synthesize action,products,searchData,category;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setTitle:NSLocalizedString(@"Products", nil)];

        searchData=[NSMutableArray array];
    }
    
    return self;
}

- (id)initWithCategory:(NSDictionary *)c {
    self = [self init];
    
    if (self) {
        [self setTitle:@"Select Product"];

        category=[NSDictionary dictionaryWithDictionary:c] ;
    }
    
    return self;
}




//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}
- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    sectionSearch = [[NSMutableDictionary alloc] init];
    
    
    selectedData=[[NSMutableArray alloc]init];
    
    [Flurry logEvent:@"Product List View"];
    //[Crittercism leaveBreadcrumb:@"<Product List View>"];
    
    //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
    
    // loading view
    [SWDefaults setFilterForProductList:nil];
    
    loadingView=[[SWLoadingView alloc] initWithFrame:self.view.bounds];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    isBarCode=NO;
    
    // table view
    mainTableView=[[UITableView alloc] initWithFrame:CGRectMake(0,44.0, self.view.bounds.size.width, self.view.bounds.size.height-44) style:UITableViewStylePlain] ;
    [mainTableView setDataSource:self];
    [mainTableView setDelegate:self];
    [mainTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    // search display controller
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44.0f)] ;
    searchBar.delegate = self;
    
    searchController=[[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];

    
    [searchController setDelegate:self];
    [searchController setSearchResultsDataSource:self];
    [searchController setSearchResultsDelegate:self];
    [searchController.searchResultsTableView setDelegate:self];
    
    
    
    [self.view addSubview:searchBar];
    [self.view addSubview:mainTableView];
    [self.view addSubview:loadingView];
    
    // Count label
    countLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)];
    [countLabel setText:@""];
    [countLabel setTextAlignment:NSTextAlignmentCenter];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor:[UIColor whiteColor]];
    [countLabel setFont:BoldSemiFontOfSize(14.0f)];
    
    
    // popovers
    ProductBrandFilterViewController *filterViewController = [[ProductBrandFilterViewController alloc] init] ;
    [filterViewController setTarget:self];
    [filterViewController setAction:@selector(filterChanged)];
    [filterViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:filterViewController] ;
    
    filterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
    [filterPopOver setDelegate:self];
    [mainTableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];

    totalLabelButton = [[UIBarButtonItem alloc] init];
    totalLabelButton = [UIBarButtonItem labelButtonWithLabel:countLabel];
    
    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    if (!category)
    {
        displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_filterImage"] style:UIBarButtonItemStylePlain target:self action:@selector(showFilters:)] ;
        displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_barcode"] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
        self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    }
    if (self.target && [self.target respondsToSelector:self.action])
    {
       Singleton *single = [Singleton retrieveSingleton];
        if ([single.productBarPopOver isEqualToString:@"PopOver"])
        {
            single.productBarPopOver = @"NoPopOver";
        }
        else
        {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)] ];
        }
        [self setTitle:@"Select Product"];
    }
    [self performSelector:@selector(navigatetodeals) withObject:nil afterDelay:0.0];
}


-(void)navigatetodeals
{
    
    //serProduct.delegate=self;
    if (category) {
        //[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:category];
        [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:category]];

    } else {
        
        [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductCollection]];
    }
    [self setupToolbar];

}
- (void)setupToolbar
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0)
    {
        

    }
    [countLabel setText:[NSString stringWithFormat:@"Total Products: %d", productArray.count]];
    totalLabelButton.title = [NSString stringWithFormat:@"Total Products: %d", productArray.count];
    [self setToolbarItems:nil];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton, flexibleSpace, nil]];
    [self.navigationController setToolbarHidden:NO animated:YES];

    if (!category)
    {
        self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    }
}

- (void)displayMap:(id)sender {
   
    Singleton *single = [Singleton retrieveSingleton];
    [filterPopOver dismissPopoverAnimated:YES];

    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    //reader.showsZBarControls = Y;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)] ;
    customOverlay.backgroundColor = [UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;

    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];

    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    reader.wantsFullScreenLayout = NO;
    reader.readerView.zoom = 1.0;
    reader.showsZBarControls=NO;
    [self presentViewController: reader animated: YES completion:nil];
   // [reader release];
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil];

}
- (void) imagePickerController: (UIImagePickerController*) readers
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];

    for(symbol in results){
        single.valueBarCode=symbol.data;
       // NSString *upcString = symbol.data;
        
        ProductBrandFilterViewController *filterViewController = [[ProductBrandFilterViewController alloc] init] ;
        [filterViewController setTarget:self];
        [filterViewController setAction:@selector(filterChangedBarCode)];
        [filterViewController selectionDone:self];
        [reader dismissViewControllerAnimated: YES completion:nil];
        
        single.isBarCode = NO;
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
//    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//    [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
   // [self searchBarCancelButtonClicked:searchBar];
   // [mainTableView setContentOffset:CGPointZero animated:YES];
    
    //iOS 9 UITableView cell frame getting reset issue
    
    NSLog(@"PRODUCT LIST CALLED");
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        mainTableView.cellLayoutMarginsFollowReadableWidth = NO;
        searchController.searchResultsTableView.cellLayoutMarginsFollowReadableWidth=NO;
        
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];


}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}


//
//- (void)executeSearch:(NSString *)term
//{
//    [searchData removeAllObjects];
//    NSDictionary *element=[NSDictionary dictionary];
//    for(element in productArray)
//    {
//        NSString *customerName = [element objectForKey:@"Description"];
//        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
//        if (r.length > 0)
//        {
//            [searchData addObject:element];
//        }
//	}
//    
//    
//    BOOL found;
//    
//    // Loop through the books and create our keys
//    for (NSDictionary *book in searchData)
//    {
//        NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
//        
//        found = NO;
//        
//        for (NSString *str in [sectionSearch allKeys])
//        {
//            if ([str isEqualToString:c])
//            {
//                found = YES;
//            }
//        }
//        
//        if (!found)
//        {
//            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
//        }
//    }
//    
//    // Loop again and sort the books into their respective keys
//    for (NSDictionary *book in searchData)
//    {
//        [[sectionSearch objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
//    }
//    
//    // Sort each section array
//    for (NSString *key in [sectionSearch allKeys])
//    {
//        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
//    }
//    
//    
//    //[mainTableView reloadData];
//    
//
//    [countLabel setText:[NSString stringWithFormat:@"Products found: %d", searchData.count]];
//}


- (void)executeSearch:(NSString *)term
{
    
    
    
    
    
    Singleton *single = [Singleton retrieveSingleton];
    single.customerIndexPath = 0;
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
    
    
    
    [searchData removeAllObjects];
    NSDictionary *element=[NSDictionary dictionary];
    for(element in productArray)
    {
        NSString *customerName = [element objectForKey:@"Description"];
        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        if (r.length > 0)
        {
            [searchData addObject:element];
        }
    }
    
    
    BOOL found;
    
    // Loop through the books and create our keys
    for (NSDictionary *book in searchData)
    {
        NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [sectionSearch allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the books into their respective keys
    for (NSDictionary *book in searchData)
    {
        [[sectionSearch objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
    }
    
    // Sort each section array
    for (NSString *key in [sectionSearch allKeys])
    {
        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
    }
    
    
    [mainTableView reloadData];
    
//    
//    NSLog(@"section search in execute search %@", [sectionSearch allValues]);
//    
//    
//    NSLog(@"section search all keys %@", [sectionSearch allKeys]);
    
    
    [countLabel setText:[NSString stringWithFormat:@"Products found: %d", searchData.count]];
}


- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showFilters:(id)sender {    
    [filterPopOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)filterChangedBarCode
{    
    [loadingView setHidden:NO];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    //serProduct.delegate=self;
    [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductCollection]];

}

- (void)filterChanged {
    [filterPopOver dismissPopoverAnimated:YES];
    
    [loadingView setHidden:NO];
//    [self.navigationController setToolbarHidden:YES animated:YES];
//    [self.navigationItem setRightBarButtonItem:nil animated:YES];
    //serProduct.delegate=self;

    [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductCollection]];
}
- (void)clearFilter {
    [SWDefaults setFilterForProductList:nil];
    [SWDefaults setProductFilterProductID:nil];
    [SWDefaults setProductFilterName:nil];
    [SWDefaults setProductFilterBrand:nil];
     #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;
    
}
#pragma mark Product List service delegate
- (void)getProductServiceDidGetList:(NSArray *)p
{
    [loadingView setHidden:YES];
    productArray =[NSMutableArray arrayWithArray:p];
   // NSLog(@"product array is %@", productArray);
    
    filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray ) {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Brand_Code"]];
        if ( theMutableArray == nil ) {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Brand_Code"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    _collapsedSections = [NSMutableSet new];
    
    bSearchIsOn = NO;
    totalRecords=products.count;
    [mainTableView reloadData];
}
-(void)sectionButtonTouchUpInside:(UIButton*)sender {
    [mainTableView beginUpdates];
    int section = sender.tag;
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (shouldCollapse) {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        //sender.backgroundColor = [UIColor lightGrayColor];
        
        int numOfRows = [mainTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [mainTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    else {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        
        int numOfRows = [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [mainTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
    }
    
    [mainTableView endUpdates];
    //[_tableView reloadData];
}
#pragma mark search display controller methods
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    
    [self executeSearch:searchString];
    
    return YES;
}





#pragma mark - UISearchBar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBara textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        [searchBara performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0];
        bSearchIsOn = NO;
        [mainTableView reloadData];
        [mainTableView setScrollsToTop:YES];
    }
    else
    {
        
        bSearchIsOn=YES;
        
        
        [self executeSearch:searchText];
        
        
        //[self searchBarSearchButtonClicked:searchBar];
        
        
        
    }

}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar
{
    bSearchIsOn = YES;
    [searchBar resignFirstResponder];
    int len = [ [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length];
    if (len > 0)
    {
        NSString *searchText = searchBar.text;
        
        if ([searchText length] > 0)
        {
            [filteredCandyArray removeAllObjects];
            NSDictionary *element=[NSDictionary dictionary];
            for(element in productArray)
            {
                NSString *customerName = [element objectForKey:@"Description"];
                NSRange r = [customerName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (r.length > 0)
                {
                    [filteredCandyArray addObject:element];
                }
            }
            NSMutableDictionary *sectionSearchq = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in filteredCandyArray)
            {
                NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionSearchq allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionSearchq setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in filteredCandyArray)
            {
                [[sectionSearchq objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each section array
            for (NSString *key in [sectionSearchq allKeys])
            {
                [[sectionSearchq objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
            }
            
            [mainTableView reloadData];
        }
        
    }
    else
    {
        [ searchBar resignFirstResponder ];
    }
}



- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"cancel button clicked");
    
    // NSLog(@"search data is %@, %@", [searchData description], [finalProductArray description]);
    
    bSearchIsOn=NO;
    
    [mainTableView reloadData];
    
}


#pragma mark - UITableView

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

        if (bSearchIsOn|| tableView==self.searchDisplayController.searchResultsTableView)
        {
            return 1;
            
        }
        else
        {
            return [finalProductArray count];
        }

}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (bSearchIsOn||tableView==self.searchDisplayController.searchResultsTableView) {
        return [searchData count];
        
    } else {
        return [_collapsedSections containsObject:@(section)] ? 0 : [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count];
        
    }
    
    
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{

        if (bSearchIsOn) {
            return 0;
        }
        else
        {
            return 40.0;
        }
    
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{

        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            
            headerView.backgroundColor=[UIColor redColor];
            
            UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
            result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
            [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            result.backgroundColor=UIColorFromRGB(0xE0E5EC);
            [result setTitle:[[row objectAtIndex:0] stringForKey:@"Brand_Code"] forState:UIControlStateNormal];
            result.tag = s;
            [result.layer setBorderColor:[UIColor whiteColor].CGColor];
            [result.layer setBorderWidth:1.0f];
            result.titleLabel.font=BoldSemiFontOfSize(14);
            [result setTitleColor:UIColorFromRGB(0x4A5866) forState:UIControlStateNormal];
            
            [headerView addSubview:result];
            return headerView;
        }
   }

- (UITableViewCell *)tableView:(UITableView *)tableViewq cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    tableViewq.backgroundColor=[UIColor whiteColor];
    
    
    UITableViewCell *cell = [tableViewq dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.textLabel.font=UITableViewCellFont;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        //adding bottom border custom separator
        
        UIImageView * saperatorImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 54, 1024, 6)];
        saperatorImageView.image=[UIImage imageNamed:@"DividerImage"];
        [cell.contentView addSubview:saperatorImageView];
        
        
        //adding  custom accessoryIndicator
        
        UIImageView * accessoryImageView=[[UIImageView alloc]initWithFrame:CGRectMake(1007, 21, 9, 18)];
        accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        [cell.contentView addSubview:accessoryImageView];

        
    }
    
    


    
    cell.textLabel.font=UITableViewCellFont;
    
    tableViewq.separatorStyle = UITableViewCellSeparatorStyleNone;

    
    if (bSearchIsOn || tableViewq==self.searchDisplayController.searchResultsTableView)
        {
//            NSDictionary * row = [filteredCandyArray objectAtIndex:indexPath.row];
            //cell.textLabel.text = [searchData valueForKey:@"Description"];
            
//            [cell setText:[[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
            
          //  NSLog(@"search data is %@", [searchData valueForKey:@"Description"]);
            
            
            if (searchData.count==0) {
                

            }
            
            else
            {
                cell.textLabel.text=[[searchData valueForKey:@"Description"] objectAtIndex:indexPath.row];

            }
            
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
        }
    
        return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.numberOfLines=2;
    //cell.textLabel.font=RegularFontOfSize(5);
//    cell.backgroundColor=UIColorFromRGB(0xF6F7FB);
//    cell.textLabel.textColor=UIColorFromRGB(0x687281);
//    cell.textLabel.textColor=[UIColor darkTextColor ];
    //cell.textLabel.font  =  LightFontOfSize(14) ;
}
#pragma mark UITableView Delegate

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
//    UITableViewCell* cell=(UITableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
//
//    cell.textLabel.textColor = [UIColor blackColor];
//    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
//    
//    
//    UIImageView* rightArrow=(UIImageView*)[cell.contentView.subviews lastObject];
//    rightArrow.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    
    
    
    

}


- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [mainTableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *row ;
    if (bSearchIsOn)
    {
        row = [filteredCandyArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        row= [objectsForCountry objectAtIndex:indexPath.row];
    }
    //NSLog(@"Product Detail %@",row);
    
    NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
    NSDictionary * product =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
    NSString *itemID = [product stringForKey:@"Inventory_Item_ID"];
    [product setValue:itemID forKey:@"ItemID"];
    
    
    if (self.target)
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:product];
#pragma clang diagnostic pop
        
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
