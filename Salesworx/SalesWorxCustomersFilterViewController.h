//
//  SalesWorxCustomersFilterViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepButton.h"

@protocol SalesWorxCustomersFilterViewControllerDelegate <NSObject>
-(void)didFinishFilter:(NSMutableArray*)filteredArray;

-(void)didCancelFilter:(BOOL)selection;
@end

@interface SalesWorxCustomersFilterViewController : UIViewController<UIPopoverPresentationControllerDelegate>

{


}

@property(nonatomic) id <SalesWorxCustomersFilterViewControllerDelegate> paymentFilterDelegate;
@property (strong, nonatomic) IBOutlet MedRepButton *customerNumberButton;
- (IBAction)customerNameButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepButton *searchButton;
- (IBAction)searchButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepButton *tradeChannelButton;
- (IBAction)tradeChannelButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *cashCustomerSwitch;
- (IBAction)cashCustomerSwitch:(UISegmentedControl *)sender ;
- (IBAction)cashCustomerSwitchTapped:(id)sender;
@property(strong,nonatomic) NSMutableArray* customerListArray;
@end

