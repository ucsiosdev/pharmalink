//
//  SalesWorxToDoDetailPopUpViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 10/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepButton.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "MedRepElementTitleLabel.h"

@interface SalesWorxToDoDetailPopUpViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate,UITextViewDelegate,UIPopoverPresentationControllerDelegate,UITextFieldDelegate>
{
    IBOutlet UIView *TopContentView;
    IBOutlet MedRepTextField *titleTextfield;
    IBOutlet MedRepTextView *DescriptionTextView;
    IBOutlet MedRepTextField *dateTextField;
    IBOutlet MedRepTextField *statusTextField;
    IBOutlet MedRepButton *cancelButton;
    IBOutlet MedRepButton *saveButton;
    UIPopoverController * datePickerPopOverController,*statusPopOverController;
    NSMutableArray *toDoStatusArray;
    
    IBOutlet MedRepElementTitleLabel *titleLbl;
    IBOutlet MedRepElementTitleLabel *messagelbl;
    IBOutlet MedRepElementTitleLabel *dateLbl;
    IBOutlet MedRepElementTitleLabel *statusLbl;

}
- (IBAction)saveButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;
@property (strong,nonatomic) ToDo *currentToDoItem;


@end
