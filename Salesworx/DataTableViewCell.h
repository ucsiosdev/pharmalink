//
//  DataTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/11/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *agencyLbl;
@property (strong, nonatomic) IBOutlet UILabel *targetLbl;
@property (strong, nonatomic) IBOutlet UILabel *MTDsalesLbl;
@property (strong, nonatomic) IBOutlet UILabel *achPercentLbl;
@property (strong, nonatomic) IBOutlet UILabel *btgLbl;
@property (strong, nonatomic) IBOutlet UILabel *dailySalesLbl;

@end
