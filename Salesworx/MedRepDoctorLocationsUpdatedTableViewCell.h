//
//  MedRepDoctorLocationsUpdatedTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/9/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepDoctorLocationsUpdatedTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UILabel *accountNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *locationLbl;
@property (strong, nonatomic) IBOutlet UILabel *accountNameTitleLbl;

@property (strong, nonatomic) IBOutlet UIImageView *saperatorImageView;

-(void)setSelectedCellStatus;
-(void)setDeSelectedCellStatus;


@end
