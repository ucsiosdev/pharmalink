//
//  SWCustomerOrderHistoryDescriptionViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/31/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWCustomerOrderHistoryDescriptionViewController.h"
#import "SalesWorxPopOverViewController.h"
#define KAllOption @"ALL"

@interface SWCustomerOrderHistoryDescriptionViewController ()
{
    NSArray *erpRefNumbersArray;
}
@end

@implementation SWCustomerOrderHistoryDescriptionViewController
@synthesize selectedCustomer,orderLineItems,invoiceLineItems,orderNumber,status,erpOrderNumber;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.view.backgroundColor=KPopUpsBackGroundColor;

    
    //titleLabel.text=@"Customer Order History Information";
    [closeButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    [descriptionSegment setTitle:NSLocalizedString(@"Ordered Items", nil) forSegmentAtIndex:0];
    [descriptionSegment setTitle:NSLocalizedString(@"Invoiced Items", nil) forSegmentAtIndex:1];
    //orderNumberTitleLbl.text=@"Order No";
    //statusTitleLbl.text=@"Status:";
    headerView.backgroundColor=UINavigationBarBackgroundColor;
    orderNumberDescriptionLbl.text=orderNumber;
    //statusTitleLbl.text=NSLocalizedString(@"Status", nil);
    
    if ([status isEqualToString:@"N/A"]) {
        statusDescriptionLbl.text=kNewStatus;
    }
    else
    {
        statusDescriptionLbl.text=status;
    }
    
    erpOrderNumberDescriptionLbl.text=[MedRepDefaults getDefaultStringForEmptyString:erpOrderNumber];
    invoiceSelectionTextfield.hidden=YES;
    erpRefNumbersArray = [invoiceLineItems valueForKeyPath:@"@distinctUnionOfObjects.ERP_Ref_No"];
    filteredInvoiceLineItems=[invoiceLineItems mutableCopy];
    
    [self ResetInvoiceTextfield];
    
    
    

}

-(void)ResetInvoiceTextfield
{
    invoiceSelectionTextfield.text=KNotApplicable;
    
    if(erpRefNumbersArray.count==0)
    {
        [invoiceSelectionTextfield setIsReadOnly:YES];
    }
    else if(erpRefNumbersArray.count==1)
    {
        invoiceSelectionTextfield.text=[erpRefNumbersArray objectAtIndex:0];
        [invoiceSelectionTextfield setIsReadOnly:YES];
    }
    else
    {
        invoiceSelectionTextfield.text=KAllOption;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)descriptionSegmentTapped:(UISegmentedControl *)sender {
    
    UISegmentedControl *lineItemsSegmentController=(id)sender;

    
    if (lineItemsSegmentController.selectedSegmentIndex==1) {
        
        NSLog(@"invoiced items tapped ");
        erpReferenceNumerTextLabel.text=@"ERP Invoice No.:";
        erpOrderNumberDescriptionLbl.hidden=YES;
        invoiceSelectionTextfield.hidden=NO;
        
        selectedCustomer.customerOrderHistoryLineItemsArray=invoiceLineItems;
        [orderDescTblView reloadData];
        
        [self ResetInvoiceTextfield];
    }
    
    else if (lineItemsSegmentController.selectedSegmentIndex==0)
        
    {
        erpReferenceNumerTextLabel.text=@"ERP Order No.:";

        erpOrderNumberDescriptionLbl.hidden=NO;
        invoiceSelectionTextfield.hidden=YES;

        selectedCustomer.customerOrderHistoryLineItemsArray=orderLineItems;
        [orderDescTblView reloadData];
    }
    
}

- (IBAction)closeButtonTapped:(id)sender {
    
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

#pragma mark UITableView Methods

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"orderHistoryDescription";
    SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.dividerImageView.hidden=YES;
    cell.isHeader=YES;
    
    cell.itemCodeLbl.isHeader=YES;
    cell.descriptionLbl.isHeader=YES;
    cell.itemCodeLbl.isHeader=YES;
    cell.qtyLbl.isHeader=YES;
    cell.totalLbl.isHeader=YES;
    cell.unitPriceLbl.isHeader=YES;
    
    
    
    cell.itemCodeLbl.text=@"Item Code";
    cell.descriptionLbl.text=@"Description";
    cell.qtyLbl.text=@"Qty";
    cell.unitPriceLbl.text=@"Unit Price";
    cell.totalLbl.text=@"Total";
    
    return cell.contentView;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectedCustomer.customerOrderHistoryLineItemsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"orderHistoryDescription";
    SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.dividerImageView.hidden=YES;
   
    CustomerOrderHistoryLineItems * currentCustomerHistoryLineItems=[selectedCustomer.customerOrderHistoryLineItemsArray objectAtIndex:indexPath.row];
    
    if ([NSString isEmpty:[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Item_Code]]==YES) {
        
        cell.itemCodeLbl.text=KNotApplicable;
    }
    else
    {
        cell.itemCodeLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Item_Code];
        
    }
    
    if ([NSString isEmpty:[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Description]]==YES) {
        cell.descriptionLbl.text=kProductInfoUnavailable;
    }
    
    else
    {
        
        cell.descriptionLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Description];
    }
    
    NSString *uomStr=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Order_Quantity_UOM];
    
    cell.qtyLbl.text=[NSString stringWithFormat:@"%@ %@",currentCustomerHistoryLineItems.Ordered_Quantity,uomStr];

    
    //cell.qtyLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Ordered_Quantity];
    cell.unitPriceLbl.text=[[SWDefaults getValidStringValue:currentCustomerHistoryLineItems.Unit_Selling_Price]  currencyString];
    cell.totalLbl.text=[[SWDefaults getValidStringValue:currentCustomerHistoryLineItems.Item_Value ]  currencyString];
    
    return cell;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==invoiceSelectionTextfield) {
        
        
        popOverTitle=kOrderHistoryScreenERPInvoiceNumberPopOverTitle;
        NSMutableArray *refsArray=[[NSMutableArray alloc]initWithArray:erpRefNumbersArray];
        [refsArray insertObject:KAllOption atIndex:0];
        [self presentPopoverfor:invoiceSelectionTextfield withTitle:kOrderHistoryScreenERPInvoiceNumberPopOverTitle withContent:refsArray];
        return NO;
    }
    return YES;
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
    
    
    if(selectedIndexPath.row==0)
    {
        invoiceSelectionTextfield.text=KAllOption;

        selectedCustomer.customerOrderHistoryLineItemsArray=invoiceLineItems;
        [orderDescTblView reloadData];
    }
    else
    {
        invoiceSelectionTextfield.text=[erpRefNumbersArray objectAtIndex:selectedIndexPath.row-1];

        NSPredicate *erpRefPredicate=[NSPredicate predicateWithFormat:@"SELF.ERP_Ref_No ==[cd] %@",[erpRefNumbersArray objectAtIndex:selectedIndexPath.row-1]];
        NSMutableArray* temppErpRefArray=[[NSMutableArray alloc]initWithArray:[invoiceLineItems filteredArrayUsingPredicate:erpRefPredicate]];
        
        
        
        
        filteredInvoiceLineItems=temppErpRefArray;
        selectedCustomer.customerOrderHistoryLineItemsArray=filteredInvoiceLineItems;
        [orderDescTblView reloadData];
    }

}
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath{
    
}
-(void)didDismissPopOverController
{
    
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(266, 273);
    popover.delegate = self;
    popOverVC.titleKey=popOverString;
    popover.sourceView = selectedTextField.rightView;
    popover.sourceRect =selectedTextField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{
        
    }];
    
}
@end
