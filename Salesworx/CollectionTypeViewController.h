//
//  CollectionTypeViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@interface CollectionTypeViewController : SWTableViewController {
    NSArray *types;
//       id target;
    SEL action;
}

@property (nonatomic, strong) NSArray *types;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@end