//
//  SalesWorxReportsViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReportsViewController.h"
#import "SalesWorxSalesSummaryViewController.h"
#import "SalesWorxBlockedCustomersViewController.h"
#import "SalesWorxCustomerStatementViewController.h"
#import "PaymentReportViewController.h"
#import "ReviewDocumentReportsViewController.h"
#import "SalesWorxVisitOptionsCollectionViewCell.h"
#import "ReviewDocumentViewController.h"
#import "SWDefaults.h"
#import "ReportsCollectionViewCell.h"
#import "DailyVisitSummaryViewController.h"
#import "SalesWorxReviewDocumentsViewController.h"
#import "SalesWorxPaymentSummaryViewController.h"
#import "SalesWorxProductsDemonstratedViewController.h"
#import "SalesWorxTargetVSAchievementViewController.h"


static NSString *cellIdentifier = @"ReportsCollectionViewCell";

@interface SalesWorxReportsViewController ()

@end

@implementation SalesWorxReportsViewController
@synthesize currentVisit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Reports"];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    
    [_reportsCollectionView registerClass:[ReportsCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier];
    
    reportsArray=[[NSMutableArray alloc]init];
    
//    NSMutableDictionary *salesPerAgencyDict=[[NSMutableDictionary alloc]init];
//    [salesPerAgencyDict setValue:kSalesReportTitle forKey:@"Title"];
//    [salesPerAgencyDict setValue:kSalesReportDescription forKey:@"Description"];
//    [salesPerAgencyDict setValue:[UIImage imageNamed:@"SalesPerAgencyReports.png"] forKey:@"ThumbNailImage"];
//    [reportsArray addObject:salesPerAgencyDict];
//    
//    
//    NSMutableDictionary *stockReportDict=[[NSMutableDictionary alloc]init];
//    [stockReportDict setValue:kStockReportTitle forKey:@"Title"];
//    [stockReportDict setValue:kStockReportDescription forKey:@"Description"];
//    [stockReportDict setValue:[UIImage imageNamed:@"StockReportReports.png"] forKey:@"ThumbNailImage"];
//    [reportsArray addObject:stockReportDict];
    
    AppControl *appControl = [AppControl retrieveSingleton];
    
    if([appControl.ENABLE_PDARIGHTS_REPORTS isEqualToString:KAppControlsYESCode])
    {
        if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
        {
            NSString *PDARIGHTS=[[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]] trimString];
            NSArray *arrPDARights = [PDARIGHTS componentsSeparatedByString:@","];
            
            
            if([arrPDARights containsObject:KReportsSalesSummaryPDA_RightsCode]) {
                NSMutableDictionary * salesSummaryDict=[[NSMutableDictionary alloc]init];
                [salesSummaryDict setValue:kSalesSummaryTitle forKey:@"Title"];
                [salesSummaryDict setValue:kSalesSummaryDescription forKey:@"Description"];
                [salesSummaryDict setValue:[UIImage imageNamed:@"Reports_SalesSummary"] forKey:@"ThumbNailImage"];
                [reportsArray addObject:salesSummaryDict];
            }
            if([arrPDARights containsObject:KReportsReviewDocumentsPDA_RightsCode]) {
                NSMutableDictionary * reviewDocsDict=[[NSMutableDictionary alloc]init];
                [reviewDocsDict setValue:kReviewDocumentsTitle forKey:@"Title"];
                [reviewDocsDict setValue:kReviewDocumentsDescription forKey:@"Description"];
                [reviewDocsDict setValue:[UIImage imageNamed:@"Reports_ReviewDocuments"] forKey:@"ThumbNailImage"];
                [reportsArray addObject:reviewDocsDict];
            }
            if([arrPDARights containsObject:KReportsBlockedCustomerPDA_RightsCode]) {
                NSMutableDictionary * blockedCustomersDict=[[NSMutableDictionary alloc]init];
                [blockedCustomersDict setValue:kBlockedCustomersTitle forKey:@"Title"];
                [blockedCustomersDict setValue:kBlockedCustomersDescription forKey:@"Description"];
                [blockedCustomersDict setValue:[UIImage imageNamed:@"Reports_BlockedCustomers"] forKey:@"ThumbNailImage"];
                [reportsArray addObject:blockedCustomersDict];
            }
            if([arrPDARights containsObject:KReportsCustomerStatementPDA_RightsCode]) {
                NSMutableDictionary * customerStatementDict=[[NSMutableDictionary alloc]init];
                [customerStatementDict setValue:kCustomerStatementTitle forKey:@"Title"];
                [customerStatementDict setValue:kCustomerStatementDescription forKey:@"Description"];
                [customerStatementDict setValue:[UIImage imageNamed:@"Reports_CustomerStatement"] forKey:@"ThumbNailImage"];
                [reportsArray addObject:customerStatementDict];
            }
            if([arrPDARights containsObject:KReportsPaymentSummaryPDA_RightsCode]) {
                NSMutableDictionary * paymentSummaryDict=[[NSMutableDictionary alloc]init];
                [paymentSummaryDict setValue:kPaymentSummaryTitle forKey:@"Title"];
                [paymentSummaryDict setValue:kPaymentSummaryDescription forKey:@"Description"];
                [paymentSummaryDict setValue:[UIImage imageNamed:@"Reports_PaymentSummary"] forKey:@"ThumbNailImage"];
                [reportsArray addObject:paymentSummaryDict];
            }
            if([arrPDARights containsObject:KReportsDailyVisitSummaryPDA_RightsCode]) {
                NSMutableDictionary * dailyVisitSummaryDict=[[NSMutableDictionary alloc]init];
                [dailyVisitSummaryDict setValue:kDailyVisitSummaryTitle forKey:@"Title"];
                [dailyVisitSummaryDict setValue:kDailyVisitSummaryDescription forKey:@"Description"];
                [dailyVisitSummaryDict setValue:[UIImage imageNamed:@"Reports_DailyVisitSummary"] forKey:@"ThumbNailImage"];
                [reportsArray addObject:dailyVisitSummaryDict];
            }
            if([arrPDARights containsObject:KReportsEdetailingReportPDA_RightsCode]) {
                // eDetailing report should be added when Marketing module is enabled.
                if([SWDefaults isMedRep])
                {
                    NSMutableDictionary *eDetailingReportDict = [[NSMutableDictionary alloc]init];
                    [eDetailingReportDict setValue:kEDetailingReportTitle forKey:@"Title"];
                    [eDetailingReportDict setValue:kEDetailingReportDescription forKey:@"Description"];
                    [eDetailingReportDict setValue:[UIImage imageNamed:@"Reports_DailyVisitSummary"] forKey:@"ThumbNailImage"];
                    [reportsArray addObject:eDetailingReportDict];
                }
            }
            if([arrPDARights containsObject:KReportsTargetVsAchivementPDA_RightsCode]){
            NSMutableDictionary * targetVSAchievementDict=[[NSMutableDictionary alloc]init];
            [targetVSAchievementDict setValue:kTargetVSAchievmentTitle forKey:@"Title"];
            [targetVSAchievementDict setValue:kTargetVSAhievementDescription forKey:@"Description"];
            [targetVSAchievementDict setValue:[UIImage imageNamed:@"target-vs-achievement"] forKey:@"ThumbNailImage"];
            [reportsArray addObject:targetVSAchievementDict];
            }
        }
    } else {
        NSMutableDictionary * salesSummaryDict=[[NSMutableDictionary alloc]init];
        [salesSummaryDict setValue:kSalesSummaryTitle forKey:@"Title"];
        [salesSummaryDict setValue:kSalesSummaryDescription forKey:@"Description"];
        [salesSummaryDict setValue:[UIImage imageNamed:@"Reports_SalesSummary"] forKey:@"ThumbNailImage"];
        [reportsArray addObject:salesSummaryDict];
        
        
        NSMutableDictionary * reviewDocsDict=[[NSMutableDictionary alloc]init];
        [reviewDocsDict setValue:kReviewDocumentsTitle forKey:@"Title"];
        [reviewDocsDict setValue:kReviewDocumentsDescription forKey:@"Description"];
        [reviewDocsDict setValue:[UIImage imageNamed:@"Reports_ReviewDocuments"] forKey:@"ThumbNailImage"];
        [reportsArray addObject:reviewDocsDict];
        
        
        NSMutableDictionary * blockedCustomersDict=[[NSMutableDictionary alloc]init];
        [blockedCustomersDict setValue:kBlockedCustomersTitle forKey:@"Title"];
        [blockedCustomersDict setValue:kBlockedCustomersDescription forKey:@"Description"];
        [blockedCustomersDict setValue:[UIImage imageNamed:@"Reports_BlockedCustomers"] forKey:@"ThumbNailImage"];
        [reportsArray addObject:blockedCustomersDict];
        
        
        NSMutableDictionary * customerStatementDict=[[NSMutableDictionary alloc]init];
        [customerStatementDict setValue:kCustomerStatementTitle forKey:@"Title"];
        [customerStatementDict setValue:kCustomerStatementDescription forKey:@"Description"];
        [customerStatementDict setValue:[UIImage imageNamed:@"Reports_CustomerStatement"] forKey:@"ThumbNailImage"];
        [reportsArray addObject:customerStatementDict];
        
        
        NSMutableDictionary * paymentSummaryDict=[[NSMutableDictionary alloc]init];
        [paymentSummaryDict setValue:kPaymentSummaryTitle forKey:@"Title"];
        [paymentSummaryDict setValue:kPaymentSummaryDescription forKey:@"Description"];
        [paymentSummaryDict setValue:[UIImage imageNamed:@"Reports_PaymentSummary"] forKey:@"ThumbNailImage"];
        [reportsArray addObject:paymentSummaryDict];
        
        
        NSMutableDictionary * dailyVisitSummaryDict=[[NSMutableDictionary alloc]init];
        [dailyVisitSummaryDict setValue:kDailyVisitSummaryTitle forKey:@"Title"];
        [dailyVisitSummaryDict setValue:kDailyVisitSummaryDescription forKey:@"Description"];
        [dailyVisitSummaryDict setValue:[UIImage imageNamed:@"Reports_DailyVisitSummary"] forKey:@"ThumbNailImage"];
        [reportsArray addObject:dailyVisitSummaryDict];

        // eDetailing report should be added when Marketing module is enabled.
        if([SWDefaults isMedRep])
        {
            NSMutableDictionary *eDetailingReportDict = [[NSMutableDictionary alloc]init];
            [eDetailingReportDict setValue:kEDetailingReportTitle forKey:@"Title"];
            [eDetailingReportDict setValue:kEDetailingReportDescription forKey:@"Description"];
            [eDetailingReportDict setValue:[UIImage imageNamed:@"Reports_DailyVisitSummary"] forKey:@"ThumbNailImage"];
            [reportsArray addObject:eDetailingReportDict];
        }
    }
    
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
    {
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self)
        {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
        }
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesReportsScreenName];
        if(currentVisit){
            NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kReportsTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                                object:self
                                                              userInfo:visitOptionDict];

        }
    }

}
#pragma mark UICollectionView Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  reportsArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(300, 170);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReportsCollectionViewCell *cell = (ReportsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.titleLbl.text=NSLocalizedString([[[reportsArray objectAtIndex:indexPath.row] valueForKey:@"Title"] uppercaseString], nil) ;
    cell.descLbl.text=NSLocalizedString([[reportsArray objectAtIndex:indexPath.row] valueForKey:@"Description"],nil) ;
    cell.optionImageView.image=[[reportsArray objectAtIndex:indexPath.row] valueForKey:@"ThumbNailImage"];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        NSString* cellTitle=[[reportsArray objectAtIndex:indexPath.row] valueForKey:@"Title"];
        
        if ([cellTitle isEqualToString:kSalesSummaryTitle])
        {
            SalesWorxSalesSummaryViewController *salesSummaryVC=[[SalesWorxSalesSummaryViewController alloc]init];
            if(currentVisit){
                salesSummaryVC.currentVisit = currentVisit;
            }
            [self.navigationController pushViewController:salesSummaryVC animated:YES];
        }
        else if ([cellTitle isEqualToString:kReviewDocumentsTitle]) {
            SalesWorxReviewDocumentsViewController * reviewDocumentVC=[[SalesWorxReviewDocumentsViewController alloc]init];
            
            [self.navigationController pushViewController:reviewDocumentVC animated:YES];
        }
        else if ([cellTitle isEqualToString:kBlockedCustomersTitle])
        {
            SalesWorxBlockedCustomersViewController *blockedCustomerVC=[[SalesWorxBlockedCustomersViewController alloc]init];
            [self.navigationController pushViewController:blockedCustomerVC animated:YES];
        }
        else if ([cellTitle isEqualToString:kCustomerStatementTitle])
        {
            SalesWorxCustomerStatementViewController *statementVC=[[SalesWorxCustomerStatementViewController alloc]init];
            if(currentVisit){
                statementVC.selectedCustomer = currentVisit.visitOptions.customer;
            }
            [self.navigationController pushViewController:statementVC animated:YES];
        }
        else if ([cellTitle isEqualToString:kPaymentSummaryTitle])
        {
            SalesWorxPaymentSummaryViewController *paymentVC=[[SalesWorxPaymentSummaryViewController alloc]init];
            if(currentVisit){
                paymentVC.currentVisit = currentVisit;
            }
            [self.navigationController pushViewController:paymentVC animated:YES];
        }
        else if ([cellTitle isEqualToString:kDailyVisitSummaryTitle])
        {
            DailyVisitSummaryViewController *dailyVisitSummaryVC=[[DailyVisitSummaryViewController alloc]init];
            [self.navigationController pushViewController:dailyVisitSummaryVC animated:YES];
        }
        else if ([cellTitle isEqualToString:kEDetailingReportTitle])
        {
            SalesWorxProductsDemonstratedViewController *productsDemonstratedVC = [[SalesWorxProductsDemonstratedViewController alloc]init];
            [self.navigationController pushViewController:productsDemonstratedVC animated:YES];
        }
        else if ([cellTitle isEqualToString:kTargetVSAchievmentTitle])
        {
            SalesWorxTargetVSAchievementViewController
            *productsDemonstratedVC = [[SalesWorxTargetVSAchievementViewController alloc]init];
            [self.navigationController pushViewController:productsDemonstratedVC animated:YES];
        }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
}


@end
