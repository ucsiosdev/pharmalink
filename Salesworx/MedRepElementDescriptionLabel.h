//
//  MedRepElementDescriptionLabel.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepElementDescriptionLabel : UILabel
{
    UITapGestureRecognizer *readMoreGesture;
    NSString *originalText;
    CALayer *readMoreLayerLabel;
    UILabel *readMoreLabel;

}
@property (nonatomic) IBInspectable BOOL RTLSupport;
@property (nonatomic) IBInspectable BOOL EnableReadMore;

-(NSString*)text;
-(void)setText:(NSString*)newText;

@end
