//
//  SalesWorxRouteRescheduleViewController.m
//  MedRep
//
//  Created by Neha Gupta on 11/04/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxRouteRescheduleViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"

@interface SalesWorxRouteRescheduleViewController ()

@end

@implementation SalesWorxRouteRescheduleViewController
@synthesize selectedCustomer, dismissDelegate, changeType;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    reasonsArray = [[MedRepQueries fetchReasonsforRescheduleVisit] mutableCopy];
    otherReasonView.hidden = true;
    dateViewTopConstraint.constant = 8;
    
    if ([changeType isEqualToString:@"R"]) {
        self.preferredContentSize = CGSizeMake(350, 400);
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Reschedule Route", nil)];
    } else {
        self.preferredContentSize = CGSizeMake(300, 300);
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Delete Route", nil)];
        textFieldDate.hidden = true;
        lblDate.hidden = true;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDataTapped)];
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    saveBtn.tintColor=[UIColor whiteColor];
    
    
    
    UIBarButtonItem * closeBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    [closeBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                            forState:UIControlStateNormal];
    closeBtn.tintColor=[UIColor whiteColor];
    
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.rightBarButtonItem=saveBtn;
    self.navigationItem.leftBarButtonItem=closeBtn;
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    } 
}

#pragma mark UITextFieldMethods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    if (textField == textFieldReason) {
        SalesWorxPopOverViewController *reasonVC = [[SalesWorxPopOverViewController alloc]init];
        reasonVC.popOverContentArray=reasonsArray;
        reasonVC.salesWorxPopOverControllerDelegate=self;
        reasonVC.disableSearch=YES;
        reasonVC.titleKey=@"Reason";
        [self.navigationController pushViewController:reasonVC animated:NO];
    }
    else if (textField == textFieldDate) {
        SalesWorxDatePickerPopOverViewController *popOverVC = [[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = @"Reschedule Date";
        [self.navigationController pushViewController:popOverVC animated:NO];
    }
    return NO;
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath {

    NSString *selectedReason = [NSString getValidStringValue:[reasonsArray objectAtIndex:selectedIndexPath.row]];
    textFieldReason.text = selectedReason;
    
    if ([selectedReason isEqualToString:@"Others"]) {
        textViewOthers.text = @"";
        otherReasonView.hidden = NO;
        dateViewTopConstraint.constant = 176;
    } else {
        otherReasonView.hidden = true;
        [textViewOthers resignFirstResponder];
        dateViewTopConstraint.constant = 8;
    }
}

-(void)didSelectDate:(NSString*)selectedDate {
    if ([NSString isEmpty:selectedDate]==NO) {
        textFieldDate.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:[SWDefaults getValidStringValue:selectedDate]]];
    }
}

-(void)closeButtonTapped
{
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)saveDataTapped
{
    [self.view endEditing:YES];
    if([textFieldReason.text isEqualToString:@""] || textFieldReason.text.length == 0)
    {
        if ([changeType isEqualToString:@"R"]) {
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter reason for rescheduling" withController:self];
        } else {
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter reason for delete" withController:self];
        }
    }
    else if ([textFieldReason.text isEqualToString:@"Others"] && textViewOthers.text.length == 0) {
        if ([changeType isEqualToString:@"R"]) {
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please specify other reason for rescheduling" withController:self];
        } else {
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please specify other reason for delete" withController:self];
        }
    }
    else if([changeType isEqualToString:@"R"] && ([textFieldDate.text isEqualToString:@""] || textFieldDate.text.length == 0))
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter date for rescheduling" withController:self];
    }
    else
    {
        NSDictionary *FSR = [SWDefaults userProfile];
        NSString *currentTime = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        NSString *newVisitDateString = [MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:kDatabseDefaultDateFormatWithoutTime scrString:textFieldDate.text];
        
        BOOL isNewDateHasRouteDayOff = false;
        NSDate *newVisitDate = [MedRepQueries getDateFromString:newVisitDateString Format:kDatabseDefaultDateFormatWithoutTime];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:newVisitDate];
        NSInteger currentWeekday = [components weekday];
        
        NSMutableArray *routePlanDayOffArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select IFNULL(Control_Value,'N/A') AS Code_Value from TBL_App_Control where Control_Key = 'ROUTEPLAN_DAY_OFF'"];
        if (routePlanDayOffArray.count>0) {
            NSString *routePlanDayOffName = [[routePlanDayOffArray valueForKey:@"Code_Value"]objectAtIndex:0];
            
            NSArray *routePlanDayOffNameArray = [routePlanDayOffName componentsSeparatedByString:@","];
            for (int i = 0; i< [routePlanDayOffNameArray count]; i++) {
                
                NSMutableArray *routePlanDayOffArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_1,'N/A') AS Day_OFF from TBL_App_Codes where Code_Type = 'ROUTEPLAN_DAY_OFF' AND Code_Value = '%@'",routePlanDayOffNameArray[i]]];
                if (routePlanDayOffArray.count>0) {
                    NSString *dayOFF = [[routePlanDayOffArray valueForKey:@"Day_OFF"]objectAtIndex:0];
                    if ([dayOFF integerValue] == currentWeekday) {
                        isNewDateHasRouteDayOff = true;
                        break;
                    }
                }
            }
        }
        
        if (isNewDateHasRouteDayOff) {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Please select a valid working day" withController:self];
        } else {
            RouteRescheduleDetails *routeDetails = [[RouteRescheduleDetails alloc]init];
            routeDetails.Visit_Change_ID = [NSString createGuid];
            routeDetails.FSR_Plan_Detail_ID = selectedCustomer.FSR_Plan_Detail_ID;
            routeDetails.Visit_Date = selectedCustomer.Visit_Date;
            routeDetails.Customer_ID = selectedCustomer.Customer_ID;
            routeDetails.Site_Use_ID = selectedCustomer.Ship_Site_Use_ID;
            routeDetails.SalesRep_ID = [FSR stringForKey:@"SalesRep_ID"];
            routeDetails.Proc_Status = @"N";
            routeDetails.Approval_Status = @"N";
            
            if ([textFieldReason.text isEqualToString:@"Others"]) {
                routeDetails.Reason = textFieldReason.text;
                routeDetails.Comments = textViewOthers.text;
            } else {
                routeDetails.Reason = textFieldReason.text;
            }
            
            routeDetails.Last_Updated_At = currentTime;
            routeDetails.Last_Updated_By = [FSR stringForKey:@"User_ID"];
            
            routeDetails.Requested_At = currentTime;
            routeDetails.Requested_By = [FSR stringForKey:@"User_ID"];
            
            
            if ([changeType isEqualToString:@"R"]) {
                routeDetails.Change_Type = @"R";
                routeDetails.New_Visit_Date = newVisitDateString;
            } else {
                routeDetails.Change_Type = @"D";
            }
            
            BOOL status = [[SWDatabaseManager retrieveManager] saveRouteRescheduleDetails:routeDetails];
            if (status == YES)
            {
                [self dismissViewControllerAnimated:YES completion:^{
                    if ([self.dismissDelegate respondsToSelector:@selector(updateRouteRescheduleData:)])
                    {
                        [self.dismissDelegate updateRouteRescheduleData:changeType];
                    }
                }];
            }
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
