//
//  SalesWorxTableView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxTableView.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@implementation SalesWorxTableView
@synthesize hideSeparator;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderColor = kUITableViewBorderColor.CGColor;
    self.layer.borderWidth = 1.0;
    self.layer.cornerRadius = 1.0;
    self.layer.masksToBounds=YES;
    
    if (hideSeparator) {
        self.separatorStyle=UITableViewCellSeparatorStyleNone;
    }
    else{
        self.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    }
    
    self.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    self.separatorColor=kUITableViewSaperatorColor;
    self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);

    
}

- (void)reloadDataWithCompletion:(void (^)(void))completionBlock
{
    self.reloadDataCompletionBlock = completionBlock;
    [super reloadData];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.reloadDataCompletionBlock) {
        self.reloadDataCompletionBlock();
        self.reloadDataCompletionBlock = nil;
    }
}


@end
