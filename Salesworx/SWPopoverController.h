//
//  SWPopoverController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 1/24/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "FTPBaseViewController.h"

@interface SWPopoverController : FTPBaseViewController


@property(strong,nonatomic) NSString* headerTitle;
@property (strong, nonatomic) IBOutlet UITextView *productDescriptionTextView;

@property(strong,nonatomic) NSString* longDescription;


@end
