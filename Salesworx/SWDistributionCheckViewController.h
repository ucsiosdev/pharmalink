//
//  SWDistributionCheckViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/31/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "MedRepTextField.h"
#import "PNCircleChart.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxDefaultSegmentedControl.h"
#import "MedRepButton.h"
@interface SWDistributionCheckViewController : UIViewController < UIPopoverControllerDelegate , UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    AppControl* appControl;
    
    NSDictionary *customer;
    CustomerHeaderView *customerHeaderView;
    UIPopoverController *popoverController;
    int selectedRowIndex;
    BOOL shouldAllowSave;
    BOOL allUpdated;
    BOOL anyUpdated;
    
    NSString* previousSelectedDate;
    
    //PCPieChart *pieChartNew;
    
    NSMutableDictionary *distributionItem;
    NSMutableDictionary *tempDistributionItem;
    
    NSString* popOverTitle;
    UIPopoverController * paymentPopOverController;
    NSMutableDictionary *itemDataWithLocation;
    
    NSArray *items;
    UITapGestureRecognizer *tap;
    BOOL flagRetake;    
    NSMutableArray* indexPathArray;
    NSString *locationKey;

    NSIndexPath *selectedIndex;
    PNCircleChart *pieChartNew;
    
    NSMutableArray *stockArray;
    BOOL isAddButtonPressed;
    NSIndexPath *editIndexPath;
    IBOutlet MedRepButton *btnAdd;
    IBOutlet UIView *checkCompletionPieChartContainerView;
}
@property (weak, nonatomic) IBOutlet UITableView *itemTableView;
@property (strong, nonatomic) IBOutlet SalesWorxTableViewHeaderView *headerView;

@property (weak, nonatomic) IBOutlet UIView *gridMainView;
@property (weak, nonatomic) IBOutlet UIView *pieChartMainView;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIView *detailMainView;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;

@property (weak, nonatomic) IBOutlet UIButton *btnDropDown;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtDropdown;

@property (weak, nonatomic) IBOutlet UILabel *lblLastVisitStock;
@property (weak, nonatomic) IBOutlet UILabel *lblLastVisitOn;

@property (weak, nonatomic) IBOutlet SalesWorxDefaultSegmentedControl *tabAvailable;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtQuantity;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtLotNo;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtExpiryDate;
@property (weak, nonatomic) IBOutlet UIButton *btnExpiryDate;

@property(nonatomic,retain) NSMutableArray *arrayOfLocation;

- (id)initWithCustomer:(NSDictionary *)customer;


@property (weak, nonatomic) IBOutlet UIView *ImageMainView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblImage;
@property (weak, nonatomic) IBOutlet UIImageView *dividerLineForImageView;



@end
