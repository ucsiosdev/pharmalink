//
//  ProductFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"

@protocol SelectedDemoPlanDelegate <NSObject>

-(void)selectedDemoPlanDetails:(NSInteger)selectedIndex;

@end

@interface ProductFilterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    id productDelegate;
}

@property(strong,nonatomic) id productDelegate;



@property (strong, nonatomic) IBOutlet UITableView *demoPlanTbl;

@property(strong,nonatomic) NSMutableArray* productsArray;

@property(strong,nonatomic) UIPopoverController *productsPopoverController;
- (IBAction)cancelTapped:(id)sender;

@end
