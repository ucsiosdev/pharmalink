//
//  ReturnProductCategoryViewController.h
//  Salesworx
//
//  Created by msaad on 5/27/13.
//  Copyright (c) 2013 msaad. All rights reserved.


#import "SWPlatform.h"
#import "SalesOrderViewController.h"

@interface ReturnProductCategoryViewController : SWTableViewController  {
    //       id target;
    SEL action;
    
    NSMutableDictionary *customer;
    NSArray *items;
    BOOL isPopover;
    //
    SalesOrderViewController *salesOrderViewController;
    
    UIView *myBackgroundView;
}



@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


- (id)initWithCustomer:(NSDictionary *)customer;

@end