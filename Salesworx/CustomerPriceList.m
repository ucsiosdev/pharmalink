//
//  CustomerPriceList.m
//  SWCustomer
//
//  Created by Irfan on 10/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.

#import "CustomerPriceList.h"
#import "SWSection.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "ProductBrandFilterViewController.h"
#import "SWFoundation.h"




@implementation CustomerPriceList

//@synthesize tableViewP;
//@synthesize customerP;
//@synthesize price;
//@synthesize popOverController;
//@synthesize customerHeaderView;
//@synthesize refLabel,amountLabel,dateLabel,statusLabel,priceLabel;
//@synthesize gridView,filterPopOver,filterButton;


- (id)initWithCustomer:(NSDictionary *)row {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        [SWDefaults setProductFilterProductID:nil];
        
        customerP = [NSDictionary dictionaryWithDictionary:row];
        //[self setCustomerP:row];
        [SWDefaults setFilterForProductList:nil];
        self.backgroundColor = [UIColor whiteColor];

        gridView=nil;
        gridView=[[GridView alloc] initWithFrame:CGRectMake(0, 60, self.bounds.size.width, self.bounds.size.height)] ;
        [gridView setDataSource:self];
        [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self addSubview:gridView];
        
        customerHeaderView=nil;
        
        
        //apply flag for customer dashboard
        
//        customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(-50, 0, 430, 60) andCustomer:customerP];
        
        customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:customerP];
        
        [customerHeaderView setShouldHaveMargins:YES];
        
        filterButton=nil;
        filterButton = [[UIButton alloc] init] ;
        //filterButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
//        if([language isEqualToString:@"ar"])
//        {
//            [filterButton setImage:[UIImage imageNamed:@"searchMSG-AR.png" cache:NO] forState:UIControlStateNormal];
//        }
//        else
//        {
//            [filterButton setImage:[UIImage imageNamed:@"searchMSG.png" cache:NO] forState:UIControlStateNormal];
//        }
        
        [filterButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
        [filterButton setTitle:NSLocalizedString(@"Search", nil) forState:UIControlStateNormal];
        filterButton.titleLabel.font = BoldSemiFontOfSize(14);
        
        //apply flag for customer dashboard
  //[filterButton setFrame:CGRectMake(370,10, 100, 32)];
        
        
        
        [filterButton setFrame:CGRectMake(900,12, 100, 32)];
        [filterButton addTarget:self action:@selector(showFilters:) forControlEvents:UIControlEventTouchUpInside];
        [filterButton bringSubviewToFront:self];
        [self addSubview:filterButton];
        
       // [self.gridView.tableView setTableHeaderView:self.customerHeaderView];
        [self addSubview:customerHeaderView];
        
        ProductBrandFilterViewController *filterViewController = [[ProductBrandFilterViewController alloc] init] ;
        [filterViewController setTarget:self];
        [filterViewController setAction:@selector(filterChanged)];
        [filterViewController setPreferredContentSize:CGSizeMake(300, 270)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:filterViewController] ;
        
        filterPopOver=nil;
        filterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
        [filterPopOver setDelegate:self];
        [self getServiceDiddbGetPriceList:[[SWDatabaseManager retrieveManager] dbGetPriceList:[customerP objectForKey:@"Customer_No"]]];

       // [self loadprice];
    }
    return self;
}


-(void)willMoveToSuperview:(UIView *)newSuperview
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
       gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
        
    }
}


- (void)filterChanged
{
    [filterPopOver dismissPopoverAnimated:YES];
    //customerSer.delegate = self;

    
    [self getServiceDiddbGetPriceList:[[SWDatabaseManager retrieveManager] dbGetPriceList:[customerP objectForKey:@"Customer_No"]]];

}

- (void)showFilters:(id)sender {    
    
    [filterPopOver presentPopoverFromRect:CGRectMake(0, 0, 10 ,32) inView:filterButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
   // filterPopOver=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)layoutSubviews {
    [super layoutSubviews];
}


- (void)loadprice {
    
    
    //customerSer.delegate = self;
    

}

#pragma UITableView Datasource

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tv deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return price.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 5;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column
{
    NSString *title = @"";
    
    if (column == 0) {
        title = NSLocalizedString(@"Item Code", nil);
    } else if (column == 1) {
        title = NSLocalizedString(@"Description", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Selling Price", nil);
    } else if(column == 3) {
        title = NSLocalizedString(@"List Price", nil);
    } else if(column == 4){
        title = NSLocalizedString(@"UOM", nil);
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column
{
    NSString *text = @"";
    NSDictionary *data = [price objectAtIndex:row];
    if (column == 0) {
        text = [NSString stringWithFormat:@"     %@", [data objectForKey:@"Item_Code"]];
    } else if (column == 1) {
        text = [NSString stringWithFormat:@"%@", [data objectForKey:@"Description"]];
    } else if (column == 2) {
        text = [NSString stringWithFormat:@"%@", [data currencyStringForKey:@"Unit_Selling_Price"]];
    } else if (column == 3) {
        text = [NSString stringWithFormat:@"%@", [data currencyStringForKey:@"Unit_List_Price"]];
    }else if (column == 4) {
        text = [NSString stringWithFormat:@"%@", [data objectForKey:@"Item_UOM"]];
    } 
    return text;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 20;
    } else if (columnIndex == 1) {
        return 40;
    } else if (columnIndex == 2) {
        return 15;
    }
    else if (columnIndex == 3) {
        return 15;
    }
    else if (columnIndex == 4) {
        return 15;
    }
    else
        return 0;
}

#pragma mark SWCustomerService Delegate
//- (void)customerServiceDiddbGetPriceList:(NSArray *)priceList {
- (void)getServiceDiddbGetPriceList:(NSArray *)priceList {
    
    price=[NSArray arrayWithArray:priceList];
    
    if((!price || !price.count))
    {
        [self getServiceDiddbGetPriceListGeneric:[[SWDatabaseManager retrieveManager] dbGetPriceListGeneric:[self updateDictionaryToCustomer:customerP]]];
    }
    else
    {
        [gridView reloadData];

    }
    priceList=nil;
}
//- (void)customerServiceDiddbGetPriceListGeneric:(NSArray *)priceList
- (void)getServiceDiddbGetPriceListGeneric:(NSArray *)priceList
{
   price=[NSArray arrayWithArray:priceList];
    [gridView reloadData];
    priceList=nil;
}

-(Customer *)updateDictionaryToCustomer:(NSDictionary *)currentCustDict
{
    Customer * currentCustomer=[[Customer alloc] init];
    currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
    currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
    currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
    currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
    currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
    currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
    currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
    currentCustomer.City=[currentCustDict valueForKey:@"City"];
    currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
    currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
    currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
    currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
    currentCustomer.Cust_Lat=[currentCustDict valueForKey:@"Cust_Lat"];
    currentCustomer.Cust_Long=[currentCustDict valueForKey:@"Cust_Long"];
    currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
    currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
    currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
    currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
    currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
    currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
    currentCustomer.Customer_Segment_ID=[currentCustDict valueForKey:@"Customer_Segment_ID"];
    currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
    currentCustomer.Dept=[currentCustDict valueForKey:@"Dept"];
    currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
    currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
    currentCustomer.Postal_Code=[currentCustDict valueForKey:@"Postal_Code"];
    currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
    currentCustomer.SalesRep_ID=[currentCustDict valueForKey:@"SalesRep_ID"];
    currentCustomer.Sales_District_ID=[currentCustDict valueForKey:@"Sales_District_ID"];
    currentCustomer.Ship_Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]];
    currentCustomer.Ship_Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]];
    currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
    currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
    currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:@"CUST_LOC_RNG"];

    return currentCustomer;
}



@end
