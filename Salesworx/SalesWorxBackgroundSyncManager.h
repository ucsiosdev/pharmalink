//
//  SalesWorxBackgroundSyncManager.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesWorxBackgroundSyncManager : NSObject
{
    NSString *lastSynctimeStr;
    NSString *currentSyncStarttimeStr;
}
-(void)postVisitDataToserver;
-(void)postRouteDataToserver:(NSMutableArray *)routesarray;
-(void)postRouteCancelDataToserver:(NSMutableArray *)routesarray;
-(void)syncVisitDataToserver;
-(void)executeVisitChangeLogDataToserver;

@end
