//
//  SynchroniseViewController.h
//  SWPlatform
//
//  Created by msaad on 1/23/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestResponseManager.h"
#import "AppConstantsList.h"
#import "DataSyncManager.h"
#import "Status.h"
#import "SWPlatform.h"
#import "JSON.h"
#import "RSA.h"
#import "AES.h"


@interface SynchroniseViewController : NSObject<RequestManagerDelegate>
{
    DataSyncManager *appData;
    Status *status_;
    NSString *avid,*cid,*sid,*lt,*ll;
    RSA *rsaObj;
    AES *aesObj;
}
- (NSDate *)getServerDate;
- (BOOL)startLicenceingWithCustomerIF:(NSString *)custID andLicenceType:(NSString *)licenceT andLicenceLimit:(NSString *)LicenceL;
- (BOOL)licenceValidation:(NSString *)lstring;
- (BOOL)licenseVerification;



@end
