//
//  SalesWorxRouteRescheduleViewController.h
//  MedRep
//
//  Created by Neha Gupta on 11/04/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "MedRepElementTitleLabel.h"

@protocol routeRescheduleDelegate <NSObject>
-(void)updateRouteRescheduleData:(NSString *)changeType;
@end

@interface SalesWorxRouteRescheduleViewController : UIViewController<SalesWorxPopoverControllerDelegate, UIPopoverPresentationControllerDelegate, SalesWorxDatePickerDelegate>
{
    NSMutableArray *reasonsArray;
    id dismissDelegate;
    
    NSString *lastSynctimeStr;
    NSString *currentSyncStarttimeStr;
    
    IBOutlet MedRepElementTitleLabel *lblDate;
    
    IBOutlet MedRepTextField *textFieldReason;
    IBOutlet MedRepTextField *textFieldDate;
    IBOutlet MedRepTextView *textViewOthers;
    IBOutlet UIView *otherReasonView;
    
    IBOutlet NSLayoutConstraint *dateViewTopConstraint;
}
@property(nonatomic) id dismissDelegate;
@property(nonatomic) Customer *selectedCustomer;
@property(nonatomic) NSString *changeType;

@end
