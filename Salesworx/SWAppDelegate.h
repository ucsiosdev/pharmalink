//
//  SWAppDelegate.h
//  Salesworx
//
//  Created by msaad on 5/9/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//
//http://install.diawi.com/n81FoA

#import <UIKit/UIKit.h>
#import "SWSession.h"
#import "SalesworxLicenseActivationViewController.h"
#import "GAIFields.h"
//#import "Crittercism.h"
#import <Firebase.h>
#import "SalesWorxProductDataSyncManager.h"
#import <KontaktSDK/KontaktSDK.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
@import UserNotifications;

@import GoogleMaps;

@interface SWAppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,KTKBeaconManagerDelegate,CBCentralManagerDelegate,UNUserNotificationCenterDelegate>
{
    BOOL databaseIsOpen;
    UIAlertView *deviceIDAlert;
    NSTimer *V_TRX_BS_Timer;
    NSTimer *COMM_BS_Timer;
    NSTimer * prodStkTimer;
    BOOL pushToCustomers;

}
@property KTKBeaconManager *beaconManager;
@property (nonatomic) BOOL startBeaconVisit;
@property (strong, nonatomic) VisitDetails *CurrentRunningMedRepVisitDetails;
@property (strong, nonatomic) SalesWorxVisit *CurrentRunningSalesWorxVisit;

@property (strong, nonatomic) UIWindow *window;



@property (nonatomic, strong) SWSessionManager *sessionManager;
@property (nonatomic, strong) LicensingViewController *licenseVC;
//@property (nonatomic, strong) SalesworxLicenseActivationViewController *licenseVC;

@property (nonatomic, strong) NewActivationViewController *activationManager;
@property (strong, nonatomic) UINavigationController *mainNavigationController;
@property (strong, nonatomic) UINavigationController *menuNavigationController;
@property(strong,nonatomic)NSMutableString * itemCodeString;

@property(strong,nonatomic)CLLocationManager *locationManager;
@property(strong,nonatomic) NSMutableDictionary * taskDictionary;
@property(strong,nonatomic) NSMutableDictionary * notesDictionary;

@property(nonatomic) BOOL isFullSyncMandatory;

@property(nonatomic) BOOL isFullSync;

@property(nonatomic) BOOL isNewSync;

@property(nonatomic) BOOL isBackgroundProcessRunning;


@property (nonatomic, copy) void(^backgroundTransferCompletionHandler)();

@property (strong, nonatomic) NSMutableArray *currentDownloadingDocumentsArray;

-(NSString *)getUTCFormateDate:(NSDate *)localDate;

@property(strong,nonatomic) NSURLSession * currentMediaDownloadingSession;
-(void)startMediaDownloadProcess;
-(void)registerforPushNotificationwithDeviceToken:(NSString*)deviceToken;


@property(nonatomic)CLLocation * currentLocation;
@property (strong, nonatomic) NSString *currentLocationCapturedTime;
@property (strong, nonatomic) NSIndexPath *selectedMenuIndexpath;
@property (nonatomic,strong) NSString * currentExceutingDataUploadProcess;
@property (nonatomic,strong) NSString * currentExceutingDataDownloadProcess;

@property(strong,nonatomic) SalesWorxBeacon * currentBeacon;
//708DF295-24DC-4A42-8DB7-09EC3EBE7688

- (void)reminderToDo:(NSString *)customerID;
@end


//http://stackoverflow.com/questions/19360012/app-works-fine-in-ios6-slow-and-jerky-in-ios7
// Product Toket iPad4 : 3ad63f01849e0250ea710a17e471597fa8467efbf82aff26f4f3b345e20405b6
