//
//  AppControl.h
//  SWPlatform
//
//  Created by msaad on 5/5/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppControl : NSObject
{
    NSMutableDictionary *appControlDict;

    
}
@property (nonatomic, strong)NSString *ENABLE_ORDER_HISTORY;
@property (nonatomic, strong)NSString *ENABLE_ITEM_DISCOUNT;
@property(strong,nonatomic) NSString*      NEW_DOCTOR_CLASS;
@property(strong,nonatomic)     NSString * SHOW_ON_ORDER_QTY;
@property(strong,nonatomic) NSString * ENABLE_CUST_LEVEL_TRX_RESTRICTION;
@property (nonatomic, strong)NSString *ENABLE_REORDER_OPTION;
@property (nonatomic, strong)NSString *ENABLE_TEMPLATES;
@property (nonatomic, strong)NSString *ENABLE_COLLECTION;
@property (nonatomic, strong)NSString *ENABLE_DISTRIB_CHECK;
@property (nonatomic, strong)NSString *ALLOW_LOT_SELECTION;
@property (nonatomic, strong)NSString *ENABLE_WHOLESALE_ORDER;
@property (nonatomic, strong)NSString *ALLOW_SKIP_CONSOLIDATION;
@property (nonatomic, strong)NSString *ALLOW_SHIP_DATE_CHANGE;
@property (nonatomic, strong)NSString *ALLOW_BONUS_CHANGE;
@property (nonatomic, strong)NSString *ENABLE_MANUAL_FOC;
@property (nonatomic, strong)NSString *ALLOW_MANUAL_FOC_ITEM_CHANGE;
@property (nonatomic, strong)NSString *ALLOW_DISCOUNT;
@property (nonatomic, strong)NSString *ALLOW_DUAL_FOC;
@property (nonatomic, strong)NSString *IS_SIGNATURE_OPTIONAL;
@property (nonatomic, strong)NSString *ENABLE_LINE_ITEM_NOTES;
@property (nonatomic, strong)NSString *SHOW_ON_ORDER;
@property (nonatomic, strong)NSString *SHOW_PRODUCT_TARGET;
@property (nonatomic, strong)NSString *ROUTE_SYNC_DAYS;
@property (nonatomic, strong)NSString *ENABLE_ORDER_CONSOLIDATION;
@property (nonatomic, strong)NSString *SD_BUCKET_SIZE;
@property (nonatomic, strong)NSString *SD_DURATION_MON;
@property (nonatomic, strong)NSString *SHOW_SALES_HISTORY;
@property (nonatomic, strong)NSString *ODOMETER_READING;
@property (nonatomic, strong)NSString *IS_START_DAY;
@property (nonatomic, strong)NSString *ALLOW_MULTI_CURRENCY;
@property (nonatomic, strong)NSString *IS_LOT_OPTIONAL;
@property (nonatomic, strong)NSString *ENABLE_BONUS_GROUPS;
@property (nonatomic, strong)NSString *BONUS_MODE;
@property (nonatomic, strong)NSString *ENABLE_PRODUCT_IMAGE;
@property (nonatomic, strong)NSString *IS_SURVEY_OPTIONAL;
@property (nonatomic, strong)NSString *CUST_DTL_SCREEN;
@property (nonatomic, strong)NSString *DASHBOARD_TYPE;
@property(strong,nonatomic) NSString * ENABLE_VISIT_TYPE;
@property(strong,nonatomic)NSString * ALLOW_SPECIAL_DISCOUNT;
@property(strong,nonatomic) NSString* ENABLE_MULTI_CAT_TRX;
@property(strong,nonatomic) NSString* ENABLE_FOC_ORDER;
@property(strong,nonatomic) NSString* FOC_ORDER_MODE;
@property(strong,nonatomic) NSString* DUPLICATE_ITEM_ENTRY_CHECK;
@property(strong,nonatomic) NSString*  OVER_STOCK;
@property(strong,nonatomic) NSString*  OVER_LIMIT;
@property(strong,nonatomic)NSString * ENABLE_DIST_CHECK_MULTI_LOCATION;
@property(strong,nonatomic)NSString * SHOW_ORG_STOCK_ONLY;
@property(strong,nonatomic)NSString * ENABLE_PROD_RANK;
@property(strong,nonatomic)NSString *SHOW_UNCONFIRMED_ORDER_SCREEN_ONSYNC;
@property(strong,nonatomic)NSString *ALLOW_RESTRICTED_FOC;
@property(strong,nonatomic)NSString *FILTER_FOC_LIST;
@property(strong,nonatomic)NSString *SHOW_ALERT_FOR_TRD_LIC_EXP;
@property(strong,nonatomic)NSString *ENABLE_ALERT_NO_BONUS;


@property(strong,nonatomic)NSString * SHOW_SURVEY_ALERT;
@property(strong,nonatomic)NSString *  ENABLE_PDA_RIGHTS;
@property(strong,nonatomic)NSString *  ENABLE_COLLECTIONS;
@property(strong,nonatomic)NSString *  DISABLE_COLL_INV_SETTLEMENT;
@property(strong,nonatomic)NSString* ENABLE_RECOMMENDED_ORDER;
@property(strong,nonatomic)NSString* SHOW_AVERAGE_SALES_HISTORY;
@property(strong,nonatomic)NSString* ENABLE_DIST_CHECK_MULTI_LOTS ;
@property(strong,nonatomic)NSString* ENABLE_LPO_IMAGES ;
@property(strong,nonatomic)NSString* ENABLE_DAILY_FULL_SYNC ;
@property(strong,nonatomic)NSString* ENABLE_BACKGROUND_SYNC ;
@property(strong,nonatomic)NSString* USER_ENABLED_BACKGROUND_SYNC ;
@property(strong,nonatomic)NSString * ENABLE_TODO;
@property(strong,nonatomic) NSString* ENABLE_PROMO_ITEM_VALIDITY;
@property(strong,nonatomic) NSString* DISTRIBUTIONCHECK_MANDATORY;
@property(strong,nonatomic) NSString* SURVEY_MANDATORY;
@property(strong,nonatomic) NSString* V_TRX_SYNC_INTERVAL;
@property(strong,nonatomic) NSString* COMM_SYNC_INTERVAL;
@property(strong,nonatomic) NSString* VALIDATE_CUST_LOC;
@property(strong,nonatomic) NSString* FS_STOCK_SYNC_TIMER;
@property(strong,nonatomic) NSString* ENABLE_FS_STOCK_SYNC;
@property(strong,nonatomic) NSString* SKIP_CONSOLIDATION_MANDATORY;
@property(strong,nonatomic) NSString* ENABLE_FS_ACCOMPANIED_BY;
@property(strong,nonatomic) NSString *COLL_INV_SETTLEMENT_MANDATORY;
@property(strong,nonatomic) NSString *UNALLOC_COLL_MODE;
@property(strong,nonatomic) NSString* MANDATORY_QTY_IN_DC;
@property(strong,nonatomic) NSString* MANDATORY_EXP_DT_IN_DC;
@property(strong,nonatomic) NSString* DEFAULT_AVAILABILITY_IN_DC;



@property(strong,nonatomic) NSString* ENABLE_FS_VISIT_TIMER;
@property(strong,nonatomic) NSString* FS_VISIT_TIMER_LIMIT;
@property(strong,nonatomic) NSString* ENABLE_VISIT_LOC_UPDATE_ON_CLOSE;
@property(strong,nonatomic) NSString* CLOSE_VISIT_LOC_UPDATE_LIMIT;
@property(strong,nonatomic) NSString* ENABLE_COLLECTION_IMAGE_CAPTURE;
@property(strong,nonatomic) NSString* ENABLE_TGT_ACVMT_DASHBOARD;
@property(strong,nonatomic) NSString* SHOW_COLLECTION_BANK_DROPDOWN;
@property(strong,nonatomic) NSString* MSG_MODULE_VERSION;
@property(strong,nonatomic) NSString* MESSAGE_DEFAULT_EXPIRY_DAYS;
@property(strong,nonatomic) NSString* RO_DESCRIPTION;
@property(strong,nonatomic) NSString* IS_COLLECTIONS_BRANCH_NAME_OPTIONAL;
@property(strong,nonatomic) NSString* SHOW_PRODUCT_NAME_SEARCH_POPOVER;

@property(strong,nonatomic) NSString * AGEING_REP_DISP_FORMAT;
@property(strong,nonatomic) NSString * ENABLE_MULTI_UOM;
@property(strong,nonatomic) NSString*   DISABLE_FOC_FOR_RESTRICTED;
@property(strong,nonatomic) NSString* IS_COLLECTIONS_SIGNATURE_OPTIONAL;
//Visit time validation flags
@property(strong,nonatomic) NSString* VALIDATE_DEVICE_TIME;
@property(strong,nonatomic) NSString* DEVICE_TIME_VALIDATION_RANGE;


@property(strong,nonatomic) NSString* IS_FM_OBJECTIVE_MANDATORY;
@property(strong,nonatomic) NSString* IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY;
@property(strong,nonatomic) NSString* DISABLE_FM_CREATE_NEW_DOCTOR;

@property(strong,nonatomic) NSString *FM_SHOW_WOCOACH;
@property(strong,nonatomic) NSString *DISABLE_FM_CREATE_NEW_LOCATION;

@property(strong,nonatomic) NSString *ENABLE_FSR_VISIT_NOTES;
@property(strong,nonatomic) NSString *FSR_TARGET_TYPE;
@property(strong,nonatomic) NSString *FS_IS_RETURN_DATE_MANDATORY;
@property(strong,nonatomic) NSString *ENABLE_PRODUCT_BARCODE_SCAN;
@property (strong,nonatomic) NSString *ENABLE_ASSORTMENT_BONUS;
@property (strong,nonatomic) NSString * USE_GEO_BRANCH_LOC;
@property (strong,nonatomic) NSString *ENABLE_MEDREP_NO_VISIT_OPTION;
@property (strong,nonatomic) NSString * MEDREP_NO_VISIT_LOCATION;
@property (strong,nonatomic) NSString * ENABLE_MEDREP_INCOMPLETE_VISIT_COMMENTS;
@property (strong,nonatomic) NSString *OH_DUPLICATE_ITEM_CHECK_PERIOD;
@property (strong,nonatomic) NSString *START_VISIT_ON_ACTIVITY;
@property (strong,nonatomic) NSString *FSR_TARGET_PRIMARY;
@property (strong,nonatomic) NSString *FSR_VALUE_TYPE;
@property (strong,nonatomic) NSString *FS_ENABLE_RETURN_RESTRICTION;

// flag to disable sales order Order entry for blocked customers
@property (strong,nonatomic) NSString *DISABLE_BLOCKED_CUSTOMER_SALES_ORDER_ENTRY;
// flag to disable holiday dates for medrep visits
@property (strong,nonatomic) NSString *FM_ENABLE_HOLIDAY;
// flag to disable  medrep visits Deletion
@property (strong,nonatomic) NSString *DISABLE_FM_VISIT_DELETE;

//iBeacon
@property(strong,nonatomic) NSString*  VALIDATE_CUST_IBEACON;

//collection target
@property(strong,nonatomic) NSString *SHOW_COLLECTION_TARGET;

@property(strong,nonatomic)NSString* FS_SHOW_DC_FILTER;
@property(strong,nonatomic)NSString* FS_SHOW_DC_AVBL_SEG;
@property(strong,nonatomic)NSString* FS_SHOW_DC_RO;
@property(strong,nonatomic)NSString* FS_SHOW_DC_MIN_STOCK;
@property(strong,nonatomic) NSString* ALLOW_EXCESS_CASH_COLLECTION;
@property(strong,nonatomic) NSString* ENABLE_VAT;
@property(strong,nonatomic) NSString* VAT_RULE_LEVEL;
@property(strong,nonatomic) NSString* VAT_LEVEL;
@property(strong,nonatomic) NSString* MERCH_MSL_CLASSIFICATION;
@property(strong,nonatomic) NSString* ENABLE_MERCHANDISING;
@property(strong,nonatomic) NSString* SHOW_TWO_LEVEL_DOCTOR_CLASSIFICATION;
@property(strong,nonatomic) NSString* ENABLE_SALESORDER_SHIPTO_SELECTION;
@property(strong,nonatomic) NSString* ENABLE_CUSTOMER_STATEMENT_PDF_EMAIL;
@property(strong,nonatomic) NSString* SHOW_FS_VISIT_CUSTOMER_CONTACT;
@property(strong,nonatomic) NSString* ENABLE_COLLECTION_RECEIPT_PDF_EMAIL;



//field marketing
@property(strong,nonatomic) NSString* FM_VISIT_CONCLUSION_WORD_LIMIT;
@property(strong,nonatomic) NSString* FM_NEXT_VISIT_OBJECTIVE_WORD_LIMIT;
@property(strong,nonatomic) NSString* FM_ENABLE_WORD_LIMIT;
@property(strong,nonatomic) NSString* FM_SHOW_NEXT_VISIT_OBJECTIVE;
@property(strong,nonatomic) NSString* FM_ENABLE_PRESENTATION_MODULE;
@property(strong,nonatomic) NSString* IS_FM_VISIT_RATING_MANDATORY;
@property(strong,nonatomic) NSString *ENABLE_ITEM_TYPE;


//StockCheck
@property(strong,nonatomic) NSString* FM_ENABLE_DISTRIBUTION_CHECK;
@property(strong,nonatomic) NSString* FM_ENABLE_STOCK_CHECK_MULTI_LOTS;
@property(strong,nonatomic) NSString* FM_SHOW_STOCK_CHECK_FILTER;
@property(strong,nonatomic) NSString* DEFAULT_GEO_LOCATION_RANGE;

//INMA
@property(strong,nonatomic) NSString* FM_DISPLAY_CALLS_VISIT_FREQUENCY;
@property(strong,nonatomic) NSString* FM_EDETAILING_IMAGE_DEFAULT_DEMO_TIME;
@property(strong,nonatomic) NSString* FM_EDETAILING_VIDEO_DEFAULT_DEMO_TIME;
@property(strong,nonatomic) NSString* FM_EDETAILING_PDF_DEFAULT_DEMO_TIME;
@property(strong,nonatomic) NSString* FM_EDETAILING_PPT_DEFAULT_DEMO_TIME;
@property(strong,nonatomic) NSString* FM_ENABLE_DEMO_PLAN_SPECIALIZATION;
@property(strong,nonatomic) NSString* FM_ENABLE_DR_APPROVAL;
@property(strong,nonatomic) NSString* FM_ENABLE_LOCATION_APPROVAL;
@property(strong,nonatomic) NSString* FM_SURVEY_VERSION;
@property(strong,nonatomic) NSString* FM_ENABLE_VISIT_HISTORY_REPORT;
@property(strong,nonatomic) NSString* FM_ENABLE_GIFT_ITEM_TYPE;
@property(strong,nonatomic) NSString* FM_DISABLE_CREATE_DR_TRADE_CHANNEL_VALIDATION;
@property(strong,nonatomic) NSString* MENU_SHOW_BO_URL;
@property(strong,nonatomic) NSString* ENABLE_COLLECTION_REMARKS;
@property(strong,nonatomic) NSString* FM_DEFAULT_VISIT_RATING;
@property(strong,nonatomic) NSString* SET_DEFAULT_EMAIL_FOR_LOG;
@property(strong,nonatomic) NSString* DEFAULT_EMAILID_FOR_LOG;
@property(strong,nonatomic) NSString* ENABLE_PDARIGHTS_REPORTS;
@property(strong,nonatomic) NSString* ENABLE_VISIT_REQUIRED_PER_MONTH_FIELD;
@property(strong,nonatomic) NSString* INCLUDE_UNAPPR_DR_LOC;
@property(strong,nonatomic) NSString* FM_VISIT_WITH_PLANNED_DOCTOR_ONLY;
@property(strong,nonatomic) NSString *ALLOW_FM_VISIT_WITHOUT_CONT_DR;
@property(strong,nonatomic) NSString *DISABLE_FM_CREATE_NEW_CONTACT;
@property(strong,nonatomic) NSString *FS_COLLECTION_REMARKS_MANDATORY;


@property(strong,nonatomic) NSString* FS_ENABLE_ROUTE_RESCHEDULING;
@property(strong,nonatomic) NSString* FS_ENABLE_PRODUCT_MEDIA;
@property(strong,nonatomic) NSString* ENABLE_INVOICE_ATTACHMENT_IN_RETURNS;
@property(strong,nonatomic) NSString* DISABLE_EMAIL_PRODUCT_MEDIA;


@property(strong,nonatomic) NSString* POPULATE_CUSTOMER_EMAIL;

@property(strong,nonatomic) NSString* FM_ENABLE_MULTISEL_ACCOMP_BY;
@property(strong,nonatomic) NSString* ENABLE_LOT_MANDATORY_RETURN;
@property(strong,nonatomic) NSString *NEARBY_CUSTOMERS_DISTANCE_THRESHOLD;
@property(strong,nonatomic) NSString *DISABLE_PAST_DATE_FOR_CHECK;
@property(strong,nonatomic) NSString *CURR_CHQ_MIN_DAYS;
@property(strong,nonatomic) NSString *FS_PRODUCT_GROUP_BY_BRAND;
@property(strong,nonatomic) NSString *ENABLE_VISIT_NOTE_ON_END_VISIT;
@property(strong,nonatomic) NSString *USE_EMPNAME_IN_USERLIST;

    
@property(strong,nonatomic) NSString *ENABLE_BONUS_ALERTS;
@property(strong,nonatomic) NSString *BONUS_ALERT_THRESHOLD_TYPE;
@property(strong,nonatomic) NSString *BONUS_ALERT_THRESHOLD;
@property(strong,nonatomic) NSString *MANDATORY_CITY_DOCTOR_CREATION;
    
- (void)initializeAppControl;
+ (AppControl*) retrieveSingleton;
+ (void) destroyMySingleton;


@property (strong,nonatomic) NSString *VALIDATE_MEDREP_CUST_LOC;

#pragma mark iPad_Side_flags
@property(strong,nonatomic) NSString*  HIDE_DISCOUNT_FIELDS_AND_LABELS;
@property(strong,nonatomic) NSString*  VISIT_CLOSURE_INTERVAL;




@end
