//
//  SalesWorxDistributionStockViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"
#import "SalesWorxDistributionStockTableViewCell.h"
#import "SWDefaults.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@protocol dismissDelegate <NSObject>

-(void)updateStockValue:(NSMutableArray *)updatedStockArray;

@end

@interface SalesWorxDistributionStockViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet UILabel *productNameLabel;
    IBOutlet UILabel *productCodeLabel;
    IBOutlet UITableView *stockTableView;
    
    IBOutlet UIView *onOrderQuantityView;
    IBOutlet SalesWorxSingleLineLabel*OnOrderQtyLabel;
    
    AppControl *appControl;
    IBOutlet SalesWorxTableViewHeaderView *stockTableHeaderViewWithDistribution;
}
@property(nonatomic) id Delegate;
@property (strong,nonatomic)DistriButionCheckItem *selectedDistributionCheckItem;
@property(nonatomic) BOOL isStockCheck;

@end
