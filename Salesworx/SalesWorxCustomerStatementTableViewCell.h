//
//  SalesWorxCustomerStatementTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxCustomerStatementTableViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocumentNo;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblInvoiceDate;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblNetAmount;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPaidAmount;

@property(nonatomic) BOOL isHeader;

@end
