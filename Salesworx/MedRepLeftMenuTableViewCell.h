//
//  MedRepLeftMenuTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/5/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepLeftMenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (strong, nonatomic) IBOutlet UIImageView *medrepSelectedLeftMenuImage;
@property (strong, nonatomic) IBOutlet UIImageView *menuArrowImageView;

@end
