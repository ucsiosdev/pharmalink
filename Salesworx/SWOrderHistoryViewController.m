//
//  SWOrderHistoryViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWOrderHistoryViewController.h"

@interface SWOrderHistoryViewController ()

@end

@implementation SWOrderHistoryViewController



- (id)initWithCustomer:(NSDictionary *)c {
    self = [super init];
    
    if (self) {
        customer=c;
        [self setTitle:NSLocalizedString(@"Order History", nil)];
    }
    
    return self;
}



//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    orderHistoryView=[[CustomerOrderHistoryView alloc] initWithCustomer:customer];
    
    [orderHistoryView setFrame:self.view.bounds];
    [orderHistoryView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [self.view addSubview:orderHistoryView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Order History View"];
    //[Crittercism leaveBreadcrumb:@"<Order History View>"];

    NSLog(@"order history called");

    [orderHistoryView loadOrders];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end