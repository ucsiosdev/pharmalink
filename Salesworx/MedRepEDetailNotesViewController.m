//
//  MedRepEDetailNotesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepEDetailNotesViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"
#import "MedRepEdetailNotesDetailViewController.h"
@interface MedRepEDetailNotesViewController ()

@end

@implementation MedRepEDetailNotesViewController

@synthesize notesArray,notesTblView,visitDetailsDict,dashBoardViewing;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    notes=[[VisitNotes alloc]init];
    
    NSString* visitLocationType;    
    Doctors *doctor;
    
    @try {
        if([NSString isEmpty:[visitDetailsDict valueForKey:@"Location_Type"]]==NO)
        {
            visitLocationType = [visitDetailsDict valueForKey:@"Location_Type"];
        }
        else
        {
            visitLocationType=_visitDetails.Location_Type;
        }
    }
    @catch (NSException *exception)
    {
        doctor = (Doctors *)visitDetailsDict;
        if([NSString isEmpty:doctor.locations.Location_Type] == NO)
        {
            visitLocationType = doctor.locations.Location_Type;
        }
    }
    
    if (notesArray.count>0)
    {
        
    }
    else  if ([visitLocationType isEqualToString:@"P"])
    {
        //get note objects
        if(_visitDetails.selectedContact.Contact_ID == nil)
        {
            notesArray=[MedRepQueries fetchNotesObjectswithLocationID:[visitDetailsDict valueForKey:@"Location_ID"] andContactID:[visitDetailsDict valueForKey:@"Contact_ID"]];
        }
        else {
            notesArray=[MedRepQueries fetchNotesObjectswithLocationID:_visitDetails.Location_ID andContactID:_visitDetails.selectedContact.Contact_ID];
        }
    }
    
    else if([visitLocationType isEqualToString:@"D"])
    {
        if(_visitDetails.selectedDoctor.Doctor_ID == nil)
        {
            @try {
                notesArray=[MedRepQueries fetchNoteObjectwithLocationID:[visitDetailsDict valueForKey:@"Location_ID"] andDoctorID:[visitDetailsDict valueForKey:@"Doctor_ID"]];
            }
            @catch (NSException *exception) {
                notesArray=[MedRepQueries fetchNoteObjectwithLocationID:doctor.locations.Location_ID andDoctorID:doctor.Doctor_ID];
            }
        }
        else  {
            notesArray=[MedRepQueries fetchNoteObjectwithLocationID:_visitDetails.Location_ID andDoctorID:_visitDetails.selectedDoctor.Doctor_ID];
        }
    }
    
    NSLog(@"notes array is %@", notesArray);
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingNotes];
    }

    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Notes"];
    
    NSLog(@"doctor locations in mult calls %@", self.visitDetails.doctorsforLocationArray);

    
    
    NSLog(@"visit details in notes are %@", [visitDetailsDict description]);
    
    
    if (self.isVisitCompleted==YES|| self.isInMultiCalls==YES) {
        
        
    }
    else if (dashBoardViewing==YES)
    {
        
    }
    
    
    
    else
    {
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Add", nil) style:UIBarButtonItemStylePlain target:self action:@selector(addTapped)];
        [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem .rightBarButtonItem=saveBtn;
    }
    
    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem .leftBarButtonItem=cancelBtn;


    
    //if in case of doctor used doctor id as contact id
    
    
//    NSLog(@"contact id is %@", contactID);
//    
//    if ([contactID isEqual:[NSNull null]]) {
//        
//        contactID=[visitDetailsDict valueForKey:@"Doctor_ID"];
//
//    }
//    
//  else  if ([contactID isEqualToString:@"0"]) {
//        
//        contactID=[visitDetailsDict valueForKey:@"Doctor_ID"];
//    }
    
    

    NSLog(@"will appear called");
    
   
    
    
    //notesArray=[MedRepQueries fetchNoteswithLocationID:[visitDetailsDict valueForKey:@"Location_ID"]];
    
//    notesArray=[MedRepQueries fetchNoteswithVisitID:[visitDetailsDict valueForKey:@"Location_ID"]];
    if (notesArray.count>0) {
        [notesTblView reloadData];
    }
    
}


-(void)addTapped
{
    MedRepEdetailNotesDetailViewController* detailVC=[[MedRepEdetailNotesDetailViewController alloc]init];
    detailVC.visitDetailsDict=self.visitDetailsDict;
    detailVC.isAddingNotes=YES;
   // detailVC.notesArray=notesArray;
    detailVC.visitDetails=self.visitDetails;
    detailVC.notesArray=self.visitDetails.notesArray;
    detailVC.delegate=self;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self
    || (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController)
    || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}
-(void)cancelTapped
{
   
    
    
    NSLog(@"notes array in cancel tapped %@", notesArray);
    
    
    if (self.visitDetails.notesArray!=nil) {
        self.visitDetails.notesArray=notesArray;
  
    }
    
    else
    {
        self.visitDetails.notesArray=[[NSMutableArray alloc]init];
        
        [self.visitDetails.notesArray addObjectsFromArray:notesArray];
        
    }
    

    NSLog(@"notes array count in cancel tapped %d", self.visitDetails.notesArray.count);
    
    
    if ([self.delegate respondsToSelector:@selector(notesDidClose:)]) {
        
        [self.delegate notesDidClose:self.visitDetails.notesArray];
    }
    
    if ([self isModal]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.notesPopOverController dismissPopoverAnimated:YES];
        
    }
}

#pragma mark UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (notesArray.count>0) {
        
        return notesArray.count;
        
        
    }
    
    
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isInMultiCalls==NO) {
        
        return 44.0f;
    }
    else
    {
    return 75.0f;
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.font=MedRepElementDescriptionLabelFont;
    cell.textLabel.textColor=MedRepMenuTitleFontColor;
    
    cell.detailTextLabel.font=MedRepElementDescriptionLabelFont;
    cell.detailTextLabel.textColor=MedRepElementTitleLabelFontColor;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    notes=[notesArray objectAtIndex:indexPath.row];
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.textLabel.font=MedRepElementDescriptionLabelFont;
    cell.textLabel.textColor=MedRepMenuTitleFontColor;
    
    cell.detailTextLabel.font=MedRepElementDescriptionLabelFont;
    cell.detailTextLabel.textColor=MedRepElementTitleLabelFontColor;
    
    
    if ([SWDefaults isRTL]) {
        cell.textLabel.textAlignment=NSTextAlignmentRight;
        cell.detailTextLabel.textAlignment=NSTextAlignmentRight;
    }
    else{
        cell.textLabel.textAlignment=NSTextAlignmentLeft;
        cell.detailTextLabel.textAlignment=NSTextAlignmentRight;


    }
    cell.textLabel.text=[NSString stringWithFormat:@"%@", notes.Title];
    
    if (self.isInMultiCalls==YES) {
        cell.detailTextLabel.hidden=NO;

    if ([self.visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        cell.detailTextLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchDoctorwithID:notes.Doctor_ID]];
 
    }
    else if ([self.visitDetails.Location_Type isEqualToString:kPharmacyLocation])
    {
        cell.detailTextLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchContactwithContactID:notes.Contact_ID]];

    }
    }
    
    else
    {
        cell.detailTextLabel.hidden=YES;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MedRepEdetailNotesDetailViewController* detailVC=[[MedRepEdetailNotesDetailViewController alloc]init];
    detailVC.isVisitCompleted=self.isVisitCompleted;
    detailVC.notesArray=notesArray;
    detailVC.delegate=self;
    detailVC.isInMultiCalls=self.isInMultiCalls;
    detailVC.selectedNoteIndex=indexPath;
    detailVC.visitDetails=self.visitDetails;
    detailVC.visitDetailsDict=self.visitDetailsDict;
    detailVC.isUpdatingNotes=YES;
    detailVC.dashBoardViewing=self.dashBoardViewing;
    
    [self.navigationController pushViewController:detailVC animated:YES];

    
}


#pragma mark Note Delegate

-(void)savedNoteDetails:(NSMutableArray*)updatedNotesArray
{
    
    notesArray=updatedNotesArray;
    
    
    
    self.visitDetails.notesArray=notesArray;
    
    
    if (notesArray>0) {
        
        [notesTblView reloadData];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark UIPopOver Controller delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}



@end
