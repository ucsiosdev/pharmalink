//
//  SalesWorxNearByCustomersViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "NSString+Additions.h"
#import <MapKit/MapKit.h>
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SWAppDelegate.h"
#import "SalesWorxNearByCustomersCollectionViewCell.h"
@interface SalesWorxNearByCustomersViewController : UIViewController
{
    Customer* selectedCustomer;
    IBOutlet UICollectionView *nearByCustomersCollectionView;
    NSMutableArray* nearByCustomersArray;
    
    IBOutlet SalesWorxDropShadowView *nearByParentView;
    
    
    NSMutableArray *customersDataDicsArray;
    NSDate *selectedDate;
    BOOL isSelectedCustomerRealLocAvaialble;
    
}

-(void)updateNearByCustomersData:(Customer*)selectedCustomer;
-(void)updateDate:(NSDate*)newDate;
@property (strong,nonatomic) IBOutlet MKMapView *nearByCustomersMapView;

@end

