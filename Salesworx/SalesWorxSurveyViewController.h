//
//  SalesWorxSurveyViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 5/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "SWLoadingView.h"
#import "SWPlatform.h"
#import "ZBarSDK.h"
#import "Singleton.h"
#import "DataSyncManager.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxImageView.h"
#import "SalesWorxDescriptionLabel1.h"

@class MedRepElementTitleLabel;

@interface SalesWorxSurveyViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    DataSyncManager *appDelegate;
    BOOL isSearching;
    
    NSArray *customerList;
    NSArray *searchArray;
    NSArray *surveyArray;
    
    IBOutlet SalesWorxDropShadowView *customersView;
    
    IBOutlet MedRepElementTitleLabel *surveyLocationTitleLbl;
    NSIndexPath *selectedIndexPath;

    
    IBOutlet NSLayoutConstraint *customersViewLeadingConstraint;
    NSIndexPath * selectedSurveyIndex;
    IBOutlet NSLayoutConstraint *customersViewWidthConstraint;
    
    SurveyDetails* previousSelectedSurvey;
    
    NSMutableArray* indexPathArray;
    
    IBOutlet UITableView *tblView;    
    
    UISearchDisplayController *searchController;
    NSMutableArray *searchData;
    
    UIPopoverController *locationFilterPopOver;
    SEL action;
    
    UILabel *infoLabel;
    ZBarReaderViewController *reader;
    

//    UIBarButtonItem *displayActionBarButton;
//    UIBarButtonItem *displayMapBarButton;
    
    UIView *myBackgroundView;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *customerViewTopConstraint;
}
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@property (weak, nonatomic) IBOutlet UISearchBar *surveySearchBar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet SalesWorxImageView *customerStatusImageView;

@property (weak, nonatomic) IBOutlet UICollectionView *surveyCollectionView;
@property(strong,nonatomic) NSString* surveyParentLocation;
@property(strong,nonatomic) NSMutableDictionary* customer;

@property(nonatomic) BOOL isFromVisit;

@property(nonatomic) BOOL isBrandAmbassadorVisit;

@property(strong,nonatomic) SalesWorxVisit * currentVisit;

@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * brandAmbassadorVisit;



@end



