//
//  SalesWorxFieldSalesDailyVisitSummaryViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableViewHeaderView.h"

@interface SalesWorxFieldSalesDailyVisitSummaryViewController : UIViewController
{
    IBOutlet UITableView *tblDailyVisitSummary;
    IBOutlet SalesWorxTableViewHeaderView *dailyVisitSummaryTableHeaderView;
    
    NSMutableArray *arrDailyVisit;
}

@end
