//
//  SalesWorxReviewDocumentsTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxReviewDocumentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocNo;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocType;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocDate;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblName;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblAmount;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblStatus;

@property(nonatomic) BOOL isHeader;


@end
