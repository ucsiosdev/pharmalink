//
//  SalesWorxSurveyViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 5/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSurveyViewController.h"
#import "SWFoundation.h"
#import "SWSection.h"
#import "LocationFilterViewController.h"
#import "SWCustomerListCell.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "SWDefaults.h"
#import "MedRepUpdatedDesignCell.h"
#import "MedRepDefaults.h"
#import "SalesWorxSurveyCollectionViewCell.h"
#import "SalesWorxFeedbackViewController.h"
#import "OrderInitializationCollectionViewCell.h"
#import "SalesWorxCustomClass.h"

#define kNavigationFilterLocationButton 1
#define kNavigationFilterCategoryButton 2


@interface SalesWorxSurveyViewController ()
- (void)locationFilter:(id)sender;
- (void)locationFilterSelected;
- (void)executeSearch:(NSString *)term;

@end

@implementation SalesWorxSurveyViewController

@synthesize target;
@synthesize action, customer,customerStatusImageView,isBrandAmbassadorVisit,brandAmbassadorVisit;

- (id)init {
    self = [super init];
    
    if (self)
    {
//        [self setTitle:NSLocalizedString(@"Survey", nil) ];
        searchData=[NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSLog(@"**SURVEY CALLED**");
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Survey"];
    
    if (isBrandAmbassadorVisit) {
        surveyLocationTitleLbl.text=@"Location";
        _lblName.text=brandAmbassadorVisit.selectedLocation.Location_Name;
    }
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(self.currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if (self.currentVisit.visitOptions.customer.Visit_Type.length == 0) {
        lblVisitType.hidden = YES;
    }
    
    
    
    // Do any additional setup after loading the view from its nib.
  
    /*
    self.view.backgroundColor=UIViewBackGroundColor;
    
    _surveyCollectionView.backgroundColor = [UIColor clearColor];

    
    indexPathArray=[[NSMutableArray alloc]init];
    
    appDelegate=[DataSyncManager sharedManager];
    appDelegate.alertMessageShown=0;
    appDelegate.currentPage=0;
    appDelegate.MaxPages=0;
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
    {
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
        } else {
        }
    }
    
    [self.surveyCollectionView registerClass:[SalesWorxSurveyCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    
    customerList = [[SWDatabaseManager retrieveManager] dbGetCollection];

    if (self.isFromVisit==YES) {
        
        
        NSPredicate * currentVisitCustomerPredicate=[NSPredicate predicateWithFormat:@"Customer_ID =='%@'", self.currentVisit.visitOptions.customer.Customer_ID];
        
        customerList=[customerList filteredArrayUsingPredicate:currentVisitCustomerPredicate];
        
    }
    
    NSLog(@"customer list is %@", customerList);
    
    
    
    if ([_surveyParentLocation isEqualToString:@"Visit"])
    {
        if ([customerList count] > 0)
        {
            searchArray = customerList;
            NSInteger index = 0;
            
            for (NSDictionary* dict in customerList) {
                
                if ([[dict objectForKey:@"Customer_Name"] isEqualToString:[customer stringForKey:@"Customer_Name"]])
                {
                    index = [customerList indexOfObject:dict];
                    break;
                }
            }
            [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            [tblView.delegate tableView:tblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            [tblView setContentOffset:CGPointMake(0.0, tblView.contentSize.height - tblView.bounds.size.height) animated:YES];
            [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }
    else
    {
        if ([customerList count] > 0)
        {
            searchArray = customerList;
            [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            [tblView.delegate tableView:tblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
    
    
    
//    displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_filterImage" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(locationFilter:)] ;
//    displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_barcode" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
     
     */
    
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    
    [super viewWillAppear:YES];
    
    tblView.delegate=self;
    tblView.rowHeight = UITableViewAutomaticDimension;
    tblView.estimatedRowHeight = 70.0;

    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesSurveyScreenName];
    }
    
    
    if (self.isMovingToParentViewController) {
        // Do any additional setup after loading the view from its nib.
        
        self.view.backgroundColor=UIViewBackGroundColor;
        
        _surveyCollectionView.backgroundColor = [UIColor clearColor];
        
        
        indexPathArray=[[NSMutableArray alloc]init];
        
        appDelegate=[DataSyncManager sharedManager];
        appDelegate.alertMessageShown=0;
        appDelegate.currentPage=0;
        appDelegate.MaxPages=0;
        
        if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
        {
            if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
                UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
                
                [self.navigationItem setLeftBarButtonItem:leftBtn];
                
            } else {
                
                [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
            }
        }
        
        [self.surveyCollectionView registerClass:[SalesWorxSurveyCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        
        customerList = [[SWDatabaseManager retrieveManager] dbGetCollection];
        
        if (self.isFromVisit==YES) {
            
            NSMutableArray * tempCustomerArray=[[NSMutableArray alloc]init];
            
            NSPredicate * currentVisitCustomerPredicate=[NSPredicate predicateWithFormat:@"Customer_ID==%d AND Ship_Site_Use_ID==%d", [self.currentVisit.visitOptions.customer.Customer_ID integerValue],[self.currentVisit.visitOptions.customer.Ship_Site_Use_ID integerValue]];
            
            tempCustomerArray=[[customerList filteredArrayUsingPredicate:currentVisitCustomerPredicate] mutableCopy];
            customerList=tempCustomerArray;
            
        }
         if (self.isBrandAmbassadorVisit)
        {
                surveyArray =[[SWDatabaseManager retrieveManager] fetchSurveyforBrandAmbassador];
        }
        
        NSLog(@"customer list is %@", customerList);
        
        
        
        if ([[SWDefaults getValidStringValue:_surveyParentLocation] isEqualToString:@"Visit"])
        {
            if ([customerList count] > 0)
            {
                searchArray = customerList;
                NSInteger index = 0;
                
                for (NSDictionary* dict in customerList) {
                    
                    if ([[dict objectForKey:@"Customer_Name"] isEqualToString:[customer stringForKey:@"Customer_Name"]])
                    {
                        index = [customerList indexOfObject:dict];
                        break;
                    }
                }
                [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                [tblView.delegate tableView:tblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                [tblView setContentOffset:CGPointMake(0.0, tblView.contentSize.height - tblView.bounds.size.height) animated:YES];
                [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            }
        }
        else
        {
            if ([customerList count] > 0)
            {
                searchArray = customerList;
                [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                [tblView.delegate tableView:tblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            }
        }
        
        
        
        //    displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_filterImage" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(locationFilter:)] ;
        //    displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_barcode" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
    }
    
    
    [SWDefaults clearCustomer];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0"))
    {
        //in ios 9 tableview cell frame getting changed this will fix it
        tblView.cellLayoutMarginsFollowReadableWidth = NO;
        searchController.searchResultsTableView.cellLayoutMarginsFollowReadableWidth=NO;
    }
    
    self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
    self.navigationController.toolbar.translucent = NO;
    
    if (!self.isMovingToParentViewController) {
    if (selectedIndexPath) {
        
        
        NSPredicate * isSelectedPredicate=[NSPredicate predicateWithFormat:@"SELF.isSelected=='Y'"];
        
         previousSelectedSurvey=[[surveyArray filteredArrayUsingPredicate:isSelectedPredicate] objectAtIndex:0];
        
        
        
       

        [self selectedPreviousSelectedCell];

    }
    }
    
    
    if (self.isFromVisit==YES) {
        
        customersViewWidthConstraint.constant=0.0;
        customersViewLeadingConstraint.constant=0.0;
        customersView.hidden=YES;
    }
    
    if (isBrandAmbassadorVisit==YES && surveyArray.count>0) {
        
        [_surveyCollectionView reloadData];
    }
    
    [self hideNoSelectionView];
}
#pragma mark UITableView Data source Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES && searchArray.count>0)
    {
        return searchArray.count;
    }
    else if (isSearching==NO && customerList.count>0)
    {
        return customerList.count;
    }
    else
    {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * row;
    
    if (isSearching==YES) {
        row = [searchArray objectAtIndex:indexPath.row];
    }
    else{
        row = [customerList objectAtIndex:indexPath.row];
    }
    
    static NSString* identifier=@"updatedDesignCell";
    MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (isSearching && !NoSelectionView.hidden) {
        [cell setDeSelectedCellStatus];
    } else {
        if ([indexPathArray containsObject:indexPath]) {
            [cell setSelectedCellStatus];
        }
        else
        {
            [cell setDeSelectedCellStatus];
        }
    }
    
    cell.titleLbl.text=[[NSString stringWithFormat:@"%@", [row valueForKey:@"Customer_Name"]] capitalizedString];
    cell.descLbl.text=[NSString stringWithFormat:@"%@", [row valueForKey:@"Customer_No"]];
    
    return cell;
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideNoSelectionView];
    
    selectedIndexPath=indexPath;
    if (isSearching==YES)
    {
        customer = [searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        customer = [customerList objectAtIndex:indexPath.row];
    }
    
    if (indexPathArray.count==0)
    {
        [indexPathArray addObject:indexPath];
    }
    else
    {
        indexPathArray=[[NSMutableArray alloc]init];
        [indexPathArray addObject:indexPath];
    }
    
    MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.titleLbl.textColor=[UIColor whiteColor];
    [cell.descLbl setTextColor:[UIColor whiteColor]];
    cell.cellDividerImg.hidden=YES;
    cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
    [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    
    _lblName.text = [[NSString stringWithFormat:@"%@", [customer valueForKey:@"Customer_Name"]] capitalizedString];
    _lblNumber.text = [NSString stringWithFormat:@"%@", [customer valueForKey:@"Customer_No"]];
    
    BOOL customerStatus = [[SWDefaults getValidStringValue:[customer objectForKey:@"Cust_Status"]] isEqualToString:@"Y"];
    BOOL cashCustomer = [[SWDefaults getValidStringValue:[customer objectForKey:@"Cash_Cust"]] isEqualToString:@"Y"];
    BOOL creditHold = [[SWDefaults getValidStringValue:[customer objectForKey:@"Credit_Hold"]] isEqualToString:@"Y"];
    
    [customerStatusImageView setBackgroundColor:[UIColor whiteColor]];
    
    if (cashCustomer)
    {
        [customerStatusImageView setBackgroundColor:[UIColor yellowColor]];
    }
    
    if (!customerStatus || creditHold)
    {
        [customerStatusImageView setBackgroundColor:[UIColor redColor]];
        [customerStatusImageView setBackgroundColor:[UIColor redColor]];
    }
    
    if (isBrandAmbassadorVisit) {
        surveyArray =[[SWDatabaseManager retrieveManager] fetchSurveyforBrandAmbassador];

    }
    else
    {

    surveyArray =[[SWDatabaseManager retrieveManager] selectSurveyWithCustomerID:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Ship_Site_Use_ID"]];
    }
    
    if (previousSelectedSurvey) {
      NSPredicate * isSelectedPredicate=[NSPredicate predicateWithFormat:@"SELF.Survey_ID== %d",previousSelectedSurvey.Survey_ID];
    
   SurveyDetails * currentSurvey=[[surveyArray filteredArrayUsingPredicate:isSelectedPredicate] objectAtIndex:0];
    
    currentSurvey.isSelected=@"Y";
        previousSelectedSurvey=nil;
    }
    
   
    [_surveyCollectionView reloadData];
    [tblView reloadData];
}

#pragma mark SearchBar Delegate Methods
-(void)closeSplitView
{
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self closeSplitView];
    [_surveySearchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
    }
    else
    {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        
        isSearching = YES;
        [self searchContent];
    }
    else {
        
        [self hideNoSelectionView];
        isSearching = NO;
        if (customerList.count>0) {
            [tblView reloadData];
            [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            [tblView.delegate tableView:tblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [_surveySearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    [self hideNoSelectionView];
    
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (customerList.count>0) {
        [tblView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(void)searchContent
{
    NSString *searchString = _surveySearchBar.text;
    NSLog(@"searching with text in survey %@",searchString);

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@", @"Customer_Name", searchString];
    searchArray = [[customerList filteredArrayUsingPredicate:predicate] mutableCopy];
    
    NSLog(@"filtered Products count %d", searchArray.count);
    if (searchArray.count>0)
    {
        [tblView reloadData];
//        [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
//        [tblView.delegate tableView:tblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    else if (searchArray.count==0)
    {
        isSearching=YES;
        [tblView reloadData];
//        [self resetSurveyDetails];
    }
}

-(void)selectedPreviousSelectedCell
{
    if ([tblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tblView cellForRowAtIndexPath:selectedIndexPath];
        if (cell)
        {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [tblView selectRowAtIndexPath:selectedIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [tblView.delegate tableView:tblView didSelectRowAtIndexPath:selectedIndexPath];
        
    }
}

-(void)deselectPreviousCellandSelectFirstCell
{
    if ([tblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tblView cellForRowAtIndexPath:selectedIndexPath];
        if (cell)
        {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]animated:YES scrollPosition:UITableViewScrollPositionNone];
        [tblView.delegate tableView:tblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}
-(void)resetSurveyDetails
{
    _lblName.text=@"N/A";
    _lblNumber.text=@"N/A";
    [customerStatusImageView setBackgroundColor:[UIColor whiteColor]];
    
    surveyArray = nil;
    [_surveyCollectionView reloadData];
}

#pragma mark Collection Datasource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [surveyArray count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(232, 150);
}
// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *cellIdentifier = @"Cell";
//    
//
//    SalesWorxSurveyCollectionViewCell *cell = (SalesWorxSurveyCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
//    
//    cell.captionLbl.text = [[surveyArray objectAtIndex:indexPath.row] valueForKey:@"Survey_Title"];
//    cell.surveyNumber.text = [NSString stringWithFormat:@"Survey %d",(indexPath.row + 1)];
//    
//    return cell;
    
    
        static NSString *cellIdentifier = @"Cell";
        
       // ProductCategories * selectedCategories=[tempCategoriesArray objectAtIndex:indexPath.row];
    
    SurveyDetails * currentSurvey=[surveyArray objectAtIndex:indexPath.row];
    
    
        NSLog(@"check image status %@", currentSurvey.isSelected);
        
        SalesWorxSurveyCollectionViewCell *cell = (SalesWorxSurveyCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.surveyImage.image=[UIImage imageNamed:@""];
        
        if ([currentSurvey.isSelected isEqualToString:@"Y"]) {
            
            //cell.selectedImageView.image=[UIImage imageNamed:@"Tick_CollectionView"];
            
            cell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;//
//            cell.captionLbl.textColor=[UIColor whiteColor];
        }
        else{
            //cell.selectedImageView.image=[UIImage imageNamed:@""];
            cell.contentView.backgroundColor=[UIColor whiteColor];
            
            cell.captionLbl.textColor=MedRepProductCodeLabelFontColor;
            
        }
    cell.captionLbl.textAlignment=NSTextAlignmentCenter;
    
    cell.captionLbl.text=currentSurvey.Survey_Title;
    
    
    //check id survey is taken for brand ambassador visit
    
    
    BOOL surveyStatus;
    
    if (isBrandAmbassadorVisit && brandAmbassadorVisit.walkinSurveyResponsesArray) {

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Survey_ID == %@", [NSString stringWithFormat:@"%d",currentSurvey.Survey_ID]];
        NSMutableArray * surveyStatusPredicateArray = [[brandAmbassadorVisit.walkinSurveyResponsesArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (surveyStatusPredicateArray.count>0) {
            NSLog(@"brand ambassador survey matched");
            surveyStatus=YES;
        }
        else{
            surveyStatus=NO;
        }
    }
    else{
     surveyStatus=[self checkSurveyStatusforCustomer:customer withSurveyID:currentSurvey.Survey_ID];
   
    
    NSLog(@"survey status is %hhd", surveyStatus);
    }
    NSString * statusString=[[NSString alloc]init];
    
    if (surveyStatus == YES) {
        statusString=@"Completed";
        cell.statusLbl.textColor = [UIColor colorWithRed:(72.0/255.0) green:(213.0/255.0) blue:(145.0/255.0) alpha:1.0];
        cell.statusLbl.text=statusString;
    }
    else
    {
        statusString=@"Pending";
        cell.statusLbl.textColor = [UIColor colorWithRed:(225.0/255.0) green:(115.0/255.0) blue:(110.0/255.0) alpha:1.0];
        cell.statusLbl.text=statusString;
    }
    
    
    
    NSMutableAttributedString* productNameAttrStr =
    [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Status:", nil)];
    [productNameAttrStr addAttribute:NSFontAttributeName
                               value:MedRepElementTitleLabelFont
                               range:NSMakeRange(0, productNameAttrStr.length)];
    
    [productNameAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepElementTitleLabelFontColor range:NSMakeRange(0, productNameAttrStr.length)];
    
    
    NSMutableAttributedString *atrStr = [[NSMutableAttributedString alloc]initWithString:statusString ];
    
    
    [atrStr addAttribute:NSFontAttributeName
                               value:SurveyStatusFont
                               range:NSMakeRange(0, atrStr.length)];
    
    [atrStr addAttribute:NSForegroundColorAttributeName value:MedRepDescriptionLabelFontColor range:NSMakeRange(0, atrStr.length)];
    
    
    
    [productNameAttrStr appendAttributedString:atrStr];
    
    //cell.statusLbl.textAlignment=NSTextAlignmentCenter;
    
    //cell.statusLbl.attributedText=productNameAttrStr;
    
    
    return cell;
        
    
    
}


-(BOOL)checkSurveyStatusforCustomer:(NSMutableDictionary*)customerDict withSurveyID:(NSInteger)surveyID
{
    BOOL completed =NO;
     NSArray *tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Ship_Site_Use_ID"] ansSurveyID:[NSString stringWithFormat:@"%d",surveyID]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
    
    if (tempArray.count>0) {
        
   
    NSLog(@"survey Status response is %@", tempArray);
    
    NSMutableDictionary *currentSurveyDict =[tempArray objectAtIndex:0];
    
  
    
    NSDateFormatter* df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [df setLocale:usLocale];
    NSDate *selectedDate = [df dateFromString:[currentSurveyDict stringForKey:@"Start_Time"]];
    int noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
    df=nil;
    usLocale=nil;
    if(noOfDays <= 0)
    {
        completed=YES;
    }
    else
    {
        completed=NO;
    }
    
    }
   
    return completed;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedSurveyIndex=indexPath;
    
    // Navigation logic may go here. Create and push another view controller.
    if([surveyArray count]!=0)
    {
        NSLog(@"survey array in first %@", [surveyArray description]);
        
        SurveyDetails *info =[surveyArray objectAtIndex:indexPath.row];
        
        [surveyArray setValue:@"N" forKey:@"isSelected"];

        
        if([info.isSelected isEqualToString: @"Y"])
        {
            info.isSelected=@"N";
        }
        else{
            info.isSelected=@"Y";
            
        }
        
      
        
        
        [[surveyArray mutableCopy] replaceObjectAtIndex:indexPath.row withObject:info];

        NSLog(@"check title %@",[[surveyArray objectAtIndex:indexPath.row] valueForKey:@"Survey_Title"]);
        
        NSLog(@"selection after replacing %@", [[surveyArray objectAtIndex:indexPath.row] valueForKey:@"isSelected"]);
        
        NSLog(@"sales rep id is %d", appDelegate.surveyDetails.SalesRep_ID);

        
        
        
        NSLog(@"info %d, %@, %@", info.Survey_ID,info.Survey_Title,info.Survey_Type_Code);
        
        //to be cheked for brand ambassadors
        NSArray * tempArray;
        if (isBrandAmbassadorVisit) {
           tempArray=[[SWDatabaseManager retrieveManager]checkBrandAmbassadorSurveyTaken:[NSString stringWithFormat:@"%d",info.Survey_ID]];
        }
        else
        {
            tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Ship_Site_Use_ID"] ansSurveyID:[NSString stringWithFormat:@"%d",info.Survey_ID]];

        }
        
        
        
        NSLog(@"temp array in did select %@", [tempArray description]);
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        
        if([tempArray count]==0)
        {
            SurveyViewController *objSurveyVC = [[SurveyViewController alloc] initWithNibName:@"SurveyViewController" bundle:nil];
            appDelegate.surveyDetails = [surveyArray objectAtIndex:indexPath.row];
            objSurveyVC.isBrandAmbassadorVisit=isBrandAmbassadorVisit;
            
            NSLog(@"survey old  id %d", appDelegate.surveyDetails.Survey_ID);
            
            NSLog(@"sales rep ID %d", appDelegate.surveyDetails.SalesRep_ID);
            
            NSLog(@"Emp code %@", [[SWDefaults userProfile]stringForKey:@"Emp_Code"]);
            
            
            NSDate *date =[NSDate date];
            NSDateFormatter *formater =[NSDateFormatter new];
            [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formater setLocale:usLocale];
            NSString *dateString = [formater stringFromDate:date];
            
            NSLog(@"Survey Time Stamp is %@", dateString);
            
            
            
            if([info.Survey_Type_Code isEqualToString:@"N"]) {
                NSLog(@"app delegate key %d", appDelegate.key);
            }
            
            SalesWorxFeedbackViewController *newSurveyVC = [[SalesWorxFeedbackViewController alloc]init];
            newSurveyVC.isBrandAmbassadorSurvey=isBrandAmbassadorVisit;
            newSurveyVC.customerID = [customer stringForKey:@"Customer_ID"];
            if (self.surveyParentLocation)
            {
                newSurveyVC.surveyParentLocation=self.surveyParentLocation;
            }
            [self.navigationController pushViewController:newSurveyVC animated:YES];
        }
        else
        {
            NSMutableDictionary *tempDictionary=[tempArray objectAtIndex:0];
            NSDateFormatter* df = [NSDateFormatter new];
            [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [df setLocale:usLocale];
            NSDate *selectedDate = [df dateFromString:[tempDictionary stringForKey:@"Start_Time"]];
            int noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
            df=nil;
            usLocale=nil;
            if(noOfDays <= 0)
            {
                
                info.isCompleted=@"Y";
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Survey has been already submitted for this customer", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
            }
            else
            {
                SurveyViewController *objSurveyVC = [[SurveyViewController alloc] initWithNibName:@"SurveyViewController" bundle:nil];
                appDelegate.surveyDetails = [surveyArray objectAtIndex:indexPath.row];
                NSLog(@"survey old %@", [surveyArray objectAtIndex:indexPath.row]);
                
                
                SalesWorxFeedbackViewController *newSurveyVC = [[SalesWorxFeedbackViewController alloc]init];
                newSurveyVC.isBrandAmbassadorSurvey=isBrandAmbassadorVisit;

                newSurveyVC.customerID = [customer stringForKey:@"Customer_ID"];
                [self.navigationController pushViewController:newSurveyVC animated:YES];
            }
        }
        
        
        [_surveyCollectionView reloadData];
    }
}

- (int)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2
{
    NSUInteger unitFlags = NSDayCalendarUnit;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return [components day];
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    customerViewTopConstraint.constant = 8;
}
-(void)showNoSelectionView
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    customerViewTopConstraint.constant = 1000;
}

//- (void)displayMap:(id)sender
//{
//    Singleton *single = [Singleton retrieveSingleton];
//    [locationFilterPopOver dismissPopoverAnimated:YES];
//    
//    single.isBarCode = YES;
//    reader = [ZBarReaderViewController new];
//    reader.readerDelegate = self;
//    //reader.showsZBarControls = Y;
//    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
//    
//    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)] ;
//    customOverlay.backgroundColor = [UIColor clearColor];
//    customOverlay.opaque = NO;
//    
//    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
//    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
//    toolbar.barStyle = UIBarStyleBlackTranslucent;
//    
//    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
//    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
//    
//    [toolbar setItems:arr animated:YES];
//    [customOverlay addSubview:toolbar];
//    
//    reader.cameraOverlayView =customOverlay;
//    reader.readerView.zoom = 1.0;
//    reader.showsZBarControls=NO;
//    [self presentViewController: reader animated: YES completion:nil ];
//}
//- (void)barButtonBackPressed:(id)sender
//{
//    [reader dismissViewControllerAnimated: YES completion:nil];
//}
//- (void)imagePickerController: (UIImagePickerController*) readers didFinishPickingMediaWithInfo: (NSDictionary*) info
//{
//    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
//    
//    ZBarSymbol *symbol = nil;
//    Singleton *single = [Singleton retrieveSingleton];
//    for(symbol in results)
//    {
//        single.valueBarCode=symbol.data;
//        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
//        [locationFilterController setTarget:self];
//        [locationFilterController setAction:@selector(filterChangedBarCode)];
//        [locationFilterController selectionDone:self];
//        [reader dismissViewControllerAnimated: YES completion:nil];
//        single.isBarCode = NO;
//    }
//}
//- (void)locationFilter:(id)sender
//{
//    [locationFilterPopOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//}
//- (void)filterChangedBarCode
//{
//    [loadingView setHidden:NO];
//    [self.navigationController setToolbarHidden:YES animated:YES];
//    [self.navigationItem setRightBarButtonItem:nil animated:YES];
//    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
//}
//- (void)locationFilterSelected
//{
//    [locationFilterPopOver dismissPopoverAnimated:YES];
//    [[SWDatabaseManager retrieveManager] cancel];
//    
//    [loadingView setHidden:NO];
//    [self.navigationController setToolbarHidden:YES animated:YES];
//    [self.navigationItem setRightBarButtonItem:nil animated:YES];
//    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
//}
//- (void)cancel:(id)sender
//{
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//}
//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
}

@end
