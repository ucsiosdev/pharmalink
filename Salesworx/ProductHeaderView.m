//
//  ProductHeaderView.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductHeaderView.h"
#import "SWFoundation.h"

@implementation ProductHeaderView

@synthesize headingLabel;
@synthesize detailLabel;



- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setHeadingLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self.headingLabel setText:@""];
        [self.headingLabel setBackgroundColor:[UIColor clearColor]];
        [self.headingLabel setTextColor:UIColorFromRGB(GroupHeaderTextColor)];
        [self.headingLabel setShadowColor:[UIColor whiteColor]];
        [self.headingLabel setShadowOffset:CGSizeMake(0, 1)];
        [self.headingLabel setFont:RegularFontOfSize(26.0f)];
        
        [self setDetailLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self.detailLabel setText:@""];
        [self.detailLabel setBackgroundColor:[UIColor clearColor]];
        [self.detailLabel setTextColor:UIColorFromRGB(GroupHeaderTextColor)];
        [self.detailLabel setShadowColor:[UIColor whiteColor]];
        [self.detailLabel setShadowOffset:CGSizeMake(0, 1)];
        [self.detailLabel setFont:LightFontOfSize(16.0f)];
        
        
        
        [self addSubview:self.detailLabel];
        [self addSubview:self.headingLabel];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect b = self.bounds;
    int margin = 50;
    
    [self.headingLabel setFrame:CGRectMake(margin, 10, b.size.width - (margin * 2), 30)];
    [self.detailLabel setFrame:CGRectMake(margin, 40, b.size.width - (margin * 2), 16)];
}


@end
