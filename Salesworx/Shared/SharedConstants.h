//
//  Constants.h
//  EServices
//
//  Created by Charith Nidarsha on 5/29/12.
//  Copyright (c) 2013 Emaar Technologies LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - common constants

#define NEW_LINE                        @"\n"
#define SINGLE_COLAN                    @":"
#define VALUE_SEPERATOR                 @","
#define URL_ENCODE_SEPERATOR            @"|"

#pragma mark - dateformat constants

#define DT_FORMAT_dd_MMM_yyyy                   @"dd-MMM-yyyy"
#define DT_FORMAT_dd_MM_yyyy                    @"dd-MM-yyyy"
#define DT_FORMAT_yyyy_MM_dd_HH_mm_ss           @"yyyy-MM-dd HH:mm:ss"
#define DT_FORMAT_yyyy_MMM_dd_HH_mm_ss          @"yyyy-MMM-dd HH:mm:ss"
#define DT_FORMAT_yyyy_MMM_dd                   @"yyyy MMM dd"
#define DT_FORMAT_yyyy_MM_dd                    @"yyyy-MM-dd"
#define DT_FORMAT_dd_MMM_YYYY_HH_mm             @"dd-MMM-yyyy hh:mm a"



#define QTY_FORMATTER                           @"###,###"

typedef NS_ENUM(NSInteger, iOSDeviceFamily)
{
    iPhone4Retina = 1,
    iPhoneStandardOld,
    iPhone5,
    iPadStandard,
    iPadRetina,
    iPadMini
};

typedef NS_ENUM(NSInteger, MajoriOSVersion)
{
    MajoriOSVersion7 = 1,
    MajoriOSVersion6,
    MajoriOSVersionLessThan6
};


