//
//  CompetitorImageDisplayViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 1/30/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompetitorImageDisplayViewController : UIViewController
- (IBAction)closeButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *competitorImageView;
@property (strong,nonatomic) UIImage * competitorImage;
@end
