//
//  SalesWorxMerchandisingPOSViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+Additions.h"
#import "SalesWorxCustomClass.h"
#import "MedRepTextView.h"
#import "PosPromotionCollectionViewCell.h"
#import "PosCollectionViewCell.h"

@interface SalesWorxMerchandisingPOSViewController : UIViewController<UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    IBOutlet UICollectionView *promotionCollectionView;
    
    IBOutlet UICollectionView *posCollectionView;
    
    
    
    
    NSMutableArray *promotionMaterialArray;
    NSMutableArray *posMaterialArray;
    NSMutableArray *positionDetailsArray;
    
    IBOutlet UITableView *tblPOSMaterial;
    IBOutlet UITableView *tblPromotionMaterial;
    IBOutlet UITableView *tblProductPosition;
    
    NSMutableArray* selectedPOSMaterialArray;
    NSMutableArray* selectedPromotionMaterialArray;
    NSMutableArray* selectedPositionDetailsArray;
    
    IBOutlet UISwitch *posImplementedSwitch;
    
    IBOutlet UISwitch *promotionSwitch;
    
    IBOutlet UISwitch *productPositionSwitch;
    
    MerchandisingPOS * currentPOS;
    
    NSMutableArray* tempPosMaterialArray;
    NSMutableArray* tempPromotionMaterialArray;
    NSMutableArray*tempPositionDetailsArray;

    
    
    IBOutlet MedRepTextView *commentsTextView;
}

- (IBAction)posImplementedSwitchStatusChanged:(id)sender;
- (IBAction)promotionSwitchStatusChanged:(id)sender;
- (IBAction)productPositionStatusChanged:(id)sender;

-(void)updateTextViewData;



-(void)showPosDataForBrand:(SalesWorxMerchandisingBrand*)currentMerchandisingBrand;



@end


