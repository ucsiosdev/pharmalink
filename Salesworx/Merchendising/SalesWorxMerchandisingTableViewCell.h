//
//  SalesWorxMerchandisingTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxImageView.h"
#import "SalesWorxItemDividerLabel.h"

@interface SalesWorxMerchandisingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *titleLbl;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *accessoryImageView;
-(void)setSelectedCellStatus;
-(void)setDeSelectedCellStatus;
@property (strong, nonatomic) IBOutlet SalesWorxItemDividerLabel *dividerLabel;


@end
