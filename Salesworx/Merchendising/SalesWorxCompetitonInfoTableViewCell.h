//
//  SalesWorxCompetitonInfoTableViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 1/29/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SalesWorxSingleLineLabel.h"


@interface SalesWorxCompetitonInfoTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *brandLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *productNameLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *priceLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *promotionLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *placementLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *stockLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *threatLevelLbl;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImageView;




@end
