//
//  SalesWorxMerchandisingBrandShareViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"
#import "SalesWorxCustomClass.h"
#import "MedRepTextField.h"
#import "SalesWorxMerchandisingRadioButtonTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
#import "TGPCamelLabels7.h"
#import "TGPDiscreteSlider7.h"

/*
 @import TGPControls;
#import "TGPCamelLabels.h"
#import "TGPDiscreteSlider.h"
*/


@interface SalesWorxMerchandisingBrandShareViewController : UIViewController<XYPieChartDelegate, XYPieChartDataSource,UITableViewDelegate,UITableViewDataSource,UIPopoverControllerDelegate,UITextFieldDelegate>
{
    MerchandisingBrandShare *selectedBrandShare;
    IBOutlet XYPieChart *SOS_PieChart;
    NSMutableArray *slicesForCustomerPotential;
    IBOutlet UITableView *brandShareTableView;
    NSArray *sliceColorsForCustomerPotential;
    
    NSMutableArray *arrayPromotions;
    IBOutlet UIButton *btnPromotionImplemented;
    IBOutlet UIButton *btnPromotionNotImplemented;
    IBOutlet UICollectionView *promotionCollectionView;
    
    IBOutlet MedRepTextField *txtBrandOfShare;
    IBOutlet MedRepTextField *txtCategory;
    IBOutlet MedRepTextField *txtSOSPercentage;
    IBOutlet MedRepTextField *txtMarketPercentage;
    IBOutlet MedRepTextField *categoryTextField;
    
    NSMutableArray* promotionArray;
    NSMutableArray* selectedIndexPathArray;
    IBOutlet MedRepTextField *calculationTextField;
    
    IBOutlet MedRepTextField *brandShareTextField;
    IBOutlet MedRepTextField *sosPercentageTextField;
    IBOutlet MedRepTextField *marketSharePercentageTextField;
    
    
    NSMutableArray * calculationInArray;
    UIPopoverController *calculationInPopOverController;
    
    IBOutlet TGPCamelLabels7 *camelLabelsView;
    IBOutlet TGPDiscreteSlider7 *descreteSliderView;
    
}

-(void)showBrandShareData:(MerchandisingBrandShare *)selectedBrand;

@end
