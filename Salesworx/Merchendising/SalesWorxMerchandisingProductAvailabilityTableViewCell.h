//
//  SalesWorxMerchandisingProductAvailabilityTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/22/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MerchandisingTextField.h"
#import "MedRepTextField.h"


@interface SalesWorxMerchandisingProductAvailabilityTableViewCell : UITableViewCell<UITextFieldDelegate>

{
}

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *productNameLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *stockLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *previousStockLabel;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *stockVariancePercentageLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *priceLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *previousPriceLabel;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *priceVariencePercentageLabel;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *positionLabel;

@property(nonatomic) BOOL isHeader;

@property(nonatomic) BOOL allowResignFirstResponder;


@property (strong, nonatomic) IBOutlet MedRepTextField *stockTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *priceTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *positiontextField;










@end
