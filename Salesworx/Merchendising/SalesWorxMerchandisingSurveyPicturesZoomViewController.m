//
//  SalesWorxMerchandisingSurveyPicturesZoomViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 2/14/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingSurveyPicturesZoomViewController.h"

#define KMaximumAllowedSurveyPictures 5
#define KMaximumAllowedMarkOnSurveyPictures 5

@interface SalesWorxMerchandisingSurveyPicturesZoomViewController ()

@end

@implementation SalesWorxMerchandisingSurveyPicturesZoomViewController
@synthesize zoomVCDelegate, selectedSurveyPicture, markCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    singleTapOnImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapOnImage:)];
    singleTapOnImageView.numberOfTapsRequired = 1;
    [surveyPictureImageView addGestureRecognizer:singleTapOnImageView];
    
    [surveyPictureImageView setImage:[UIImage imageWithContentsOfFile:[[SWDefaults getMerchandisingSurveyMarkingPicturesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",selectedSurveyPicture.imageName]]]];
    
    [self showMarkFrameOfSelectedImage];
}

#pragma mark Image marking
- (void)handleSingleTapOnImage:(UITapGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:sender.view];
    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    if ([viewTouched isKindOfClass:[UIImageView class]]) {
        // respond to touch action
        
        if (selectedSurveyPicture.imageMarkArray.count == KMaximumAllowedMarkOnSurveyPictures) {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Mark limit exceed" andMessage:@"You can add 5 mark only" withController:self];
        } else {
            
            UIColor *ContrastColor = [SWDefaults getContrastColorForImage:surveyPictureImageView.image];
            
            CGRect imageFrame = CGRectMake(point.x, point.y, 200, 200);
            SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:imageFrame color:ContrastColor];
            imageResizableView.backgroundColor = [UIColor clearColor];
            [imageResizableView showEditingHandles];
            imageResizableView.tag = markCount;
            imageResizableView.delegate = self;
            [surveyPictureImageView addSubview:imageResizableView];
            [imageResizableView resizeUsingTouchLocation:point];
            
            UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapOnImage:)];
            doubleTap.numberOfTapsRequired = 2;
            [imageResizableView addGestureRecognizer:doubleTap];
            [singleTapOnImageView requireGestureRecognizerToFail:doubleTap];
            
            MerchandisingSurveyPicturesMark *objMark = [[MerchandisingSurveyPicturesMark alloc]init];
            objMark.markId = [NSString stringWithFormat:@"%d",markCount];
            [selectedSurveyPicture.imageMarkArray addObject:objMark];
            
            markCount++;
            [self printMarkFrames];
        }
    }
}

- (void)handleDoubleTapOnImage:(UITapGestureRecognizer *)sender
{
    SPUserResizableView *markView = (SPUserResizableView *)[surveyPictureImageView viewWithTag:sender.view.tag];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"markId = %@",[NSString stringWithFormat:@"%ld",(long)markView.tag]];
    NSMutableArray *tempMarksArray = [selectedSurveyPicture.imageMarkArray mutableCopy];
    [tempMarksArray filterUsingPredicate:predicate];
    
    if (tempMarksArray.count > 0) {
        MerchandisingSurveyPicturesMark *selectedSurveyPicturesMark = [tempMarksArray objectAtIndex:0];
        
        SalesWorxMerchandisingSurveyPictureMarkDetailsViewController *markVC = [[SalesWorxMerchandisingSurveyPictureMarkDetailsViewController alloc]init];
        markVC.markVCDelegate=self;
        markVC.markID = selectedSurveyPicturesMark.markId;
        markVC.markDescription = selectedSurveyPicturesMark.markDescription;
        
        UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:markVC];
        mapPopUpViewController.navigationBarHidden = YES;
        mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
        markVC.view.backgroundColor = [UIColor clearColor];
        markVC.modalPresentationStyle = UIModalPresentationCustom;
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:mapPopUpViewController animated:NO completion:nil];
    }
}

- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView {
    currentlyEditingView = userResizableView;
}

- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView {
    lastEditedView = userResizableView;
    [self printMarkFrames];
}

-(void)printMarkFrames{
    for (id view in surveyPictureImageView.subviews){
        if([view isKindOfClass:[SPUserResizableView class]]){
            
            SPUserResizableView *markView = view;
            
            NSPredicate *predicate=[NSPredicate predicateWithFormat:@"markId = %@",[NSString stringWithFormat:@"%ld",(long)markView.tag]];
            NSMutableArray *tempMarksArray = [selectedSurveyPicture.imageMarkArray mutableCopy];
            [tempMarksArray filterUsingPredicate:predicate];
            
            if (tempMarksArray.count > 0) {
                MerchandisingSurveyPicturesMark *selectedSurveyPicturesMark = [tempMarksArray objectAtIndex:0];
                
                selectedSurveyPicturesMark.markViewframe = markView.frame;
                selectedSurveyPicturesMark.markViewframeWithRespectToImageView = [self convertFrameFromUIimageViewFramePixelToUIimageViewImagePixel:markView.frame];
            }
        }
    }
}

-(CGRect )convertFrameFromUIimageViewFramePixelToUIimageViewImagePixel:(CGRect)markRect
{
    float widthRatio= surveyPictureImageView.frame.size.width /surveyPictureImageView.image.size.width;
    float heightRatio=  surveyPictureImageView.frame.size.height /surveyPictureImageView.image.size.height;
    
    CGPoint rectPointInImagePixels = CGPointMake(markRect.origin.x/widthRatio,markRect.origin.y/heightRatio);
    CGSize rectSizeInImagePixels = CGSizeMake(markRect.size.width/widthRatio,markRect.size.height/heightRatio);
    
    return CGRectMake(rectPointInImagePixels.x, rectPointInImagePixels.y, rectSizeInImagePixels.width, rectSizeInImagePixels.height);
}

-(CGRect )convertFrameFromUIimageViewImagePixelToUIimageViewFramePixel:(CGRect)markRect
{
    float widthRatio= surveyPictureImageView.image.size.width /surveyPictureImageView.frame.size.width;
    float heightRatio=  surveyPictureImageView.image.size.height /surveyPictureImageView.frame.size.height;
    
    CGPoint rectPointInImagePixels = CGPointMake(markRect.origin.x/widthRatio,markRect.origin.y/heightRatio);
    CGSize rectSizeInImagePixels = CGSizeMake(markRect.size.width/widthRatio,markRect.size.height/heightRatio);
    
    return CGRectMake(rectPointInImagePixels.x, rectPointInImagePixels.y, rectSizeInImagePixels.width, rectSizeInImagePixels.height);
}

#pragma mark Mark detail delegate
-(void)saveMarkDescription:(NSString *)markID WithDesc:(NSString *)markDescription {
    
    for (MerchandisingSurveyPicturesMark *markObj in selectedSurveyPicture.imageMarkArray) {
        if([markObj.markId isEqualToString:markID])
        {
            markObj.markDescription = markDescription;
        }
    }
}

-(void)deleteMark:(NSString *)markID {
    
    for (MerchandisingSurveyPicturesMark *markObj in selectedSurveyPicture.imageMarkArray) {
        if([markObj.markId isEqualToString:markID])
        {
            [selectedSurveyPicture.imageMarkArray removeObject:markObj];
            break;
        }
    }
    SPUserResizableView *markObj = (SPUserResizableView *)[surveyPictureImageView viewWithTag:[markID intValue]];
    [markObj removeFromSuperview];
}

-(void)showMarkFrameOfSelectedImage {
    
    NSLog(@"%lu", (unsigned long)[surveyPictureImageView subviews].count);
    [[surveyPictureImageView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSLog(@"%lu", (unsigned long)[surveyPictureImageView subviews].count);
    
    UIColor *ContrastColor = [SWDefaults getContrastColorForImage:surveyPictureImageView.image];
    
    for (MerchandisingSurveyPicturesMark *currentMarkObj in selectedSurveyPicture.imageMarkArray) {

        CGRect markFrameWithRespectToImageView = [self convertFrameFromUIimageViewImagePixelToUIimageViewFramePixel:currentMarkObj.markViewframeWithRespectToImageView];
        
        SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:markFrameWithRespectToImageView color:ContrastColor];
        imageResizableView.backgroundColor = [UIColor clearColor];
        [imageResizableView showEditingHandles];
        imageResizableView.tag = [currentMarkObj.markId intValue];
        imageResizableView.delegate = self;
        [surveyPictureImageView addSubview:imageResizableView];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapOnImage:)];
        doubleTap.numberOfTapsRequired = 2;
        [imageResizableView addGestureRecognizer:doubleTap];
        [singleTapOnImageView requireGestureRecognizerToFail:doubleTap];
    }
}

- (IBAction)closeButtonTapped:(id)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if ([self.zoomVCDelegate respondsToSelector:@selector(refreshSurveyPicture:withMarkCount:)]) {
            [self.zoomVCDelegate refreshSurveyPicture:selectedSurveyPicture withMarkCount:markCount];
        }
    });
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
