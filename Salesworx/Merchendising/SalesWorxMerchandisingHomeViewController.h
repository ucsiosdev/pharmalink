//
//  SalesWorxMerchendisingHomeViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/19/17.
//  Copyright © 2017 msaad. All rights reserved.
//
#define kSegmentWidth 750


#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SWFoundation.h"
#import "ZUUIRevealController.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxDescriptionLabel1.h"
#import "SalesWorxCustomerAvailableBalanceLabel.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxImageView.h"
#import "HMSegmentedControl.h"
#import "AppControl.h"
#import "SWDatabaseManager.h"
#import "MBProgressHUD.h"
#import "MedRepUpdatedDesignCell.h"
#import "SalesWorxDefaultTableViewCell.h"
#import "SalesWorxMerchandisingProductDetailsViewController.h"
#import "SalesWorxMerchandisingBrandShareViewController.h"
#import "SalesWorxMerchandisingPlanogramViewController.h"
#import "SalesWorxMerchandisingPOSViewController.h"
#import "SalesWorxMerchandisingCompetitionInfoViewController.h"
#import "SalesWorxMerchandisingInStoreActivityViewController.h"
#import "SalesWorxMerchandisingSurveyPicturesViewController.h"
#import "SalesWorxMerchandisingCommentsViewController.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxMerchandisingTableViewCell.h"
#import "SalesWorxSearchBar.h"
#import "FMDatabase.h"
#import "MedRepQueries.h"
#import "SalesWorxiBeaconManager.h"


@interface SalesWorxMerchandisingHomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray* brandsArray;
    NSMutableArray* currentVisitMerchandisingArray;
    AppControl * appControl;
    NSMutableArray* productsArray;
    IBOutlet UITableView *brandsTableView;
    IBOutlet UIButton *filterButton;
    IBOutlet SalesWorxSearchBar *brandsSearchBar;
    NSArray *filteredBrandsArray;
    
    /**Child View controllers*/
    SalesWorxMerchandisingProductDetailsViewController * priceDetailsChildViewController;
    SalesWorxMerchandisingBrandShareViewController* brandShareChildViewController;
    SalesWorxMerchandisingPlanogramViewController* planogramChildViewController;
    SalesWorxMerchandisingPOSViewController* posChildViewController;
    SalesWorxMerchandisingCompetitionInfoViewController * competitionChildViewController;
    SalesWorxMerchandisingInStoreActivityViewController *instoreChildViewController;
    SalesWorxMerchandisingSurveyPicturesViewController* surveyPicturesChildViewController;
    SalesWorxMerchandisingCommentsViewController * commentsChildViewController;
    
    NSMutableArray* selectedIndexPathsArray;
    
    IBOutlet SalesWorxDropShadowView *noBrandSelectedPlaceHolderView;
    
    BOOL isSearching;
    BOOL errorAlertDisplayed;
    NSString* comments;
    BOOL commentsInserted;
    
    
    SalesWorxMerchandisingBrand *currentMerchandising;
    BOOL insertFSRVisitData;
    SWAppDelegate* appDel;
    
    
}
@property(strong,nonatomic) SalesWorxVisit * currentVisit;


//customer details

@property (strong, nonatomic) IBOutlet SalesWorxDescriptionLabel1 *visitTypeLabel;
@property (strong, nonatomic) IBOutlet SalesWorxCustomerAvailableBalanceLabel *availableBalanceLabel;
@property (strong, nonatomic) IBOutlet MedRepHeader *customerNameLabel;
@property (strong, nonatomic) IBOutlet MedRepProductCodeLabel *customerCodeLabel;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *customerStatusImageView;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *merchandisingTitlesSegment;
@property (strong, nonatomic) IBOutlet UIScrollView *merchandisingScrollView;

@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *lblLastVisitedOn;


@end
