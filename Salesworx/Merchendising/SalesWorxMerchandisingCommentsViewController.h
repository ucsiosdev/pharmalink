//
//  SalesWorxMerchandisingCommentsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SalesWorxCustomClass.h"
#import "NSString+Additions.h"

@interface SalesWorxMerchandisingCommentsViewController : UIViewController<UIPopoverControllerDelegate>
{
    SalesWorxMerchandisingBrand *selectedMerchandisingBrand;
    MerchandisingComments *selectedBrandComment;
    
    IBOutlet MedRepTextField *productTextField;
    IBOutlet MedRepTextView *commentsTextView;
    
    NSMutableArray *productArray;
}
-(void)showCommentsforBrand:(SalesWorxMerchandisingBrand*)merchandisingObject;

@end
