//
//  PosPromotionCollectionViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 3/20/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface PosPromotionCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIImageView *tickImageView;

@end
