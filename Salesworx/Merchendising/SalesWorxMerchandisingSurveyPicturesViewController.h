//
//  SalesWorxMerchandisingSurveyPicturesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SPUserResizableView.h"
#import "SalesWorxMerchandisingSurveyPictureMarkDetailsViewController.h"
#import "MedRepTextView.h"
#import "MedRepTextField.h"
#import <AVFoundation/AVFoundation.h>

@interface SalesWorxMerchandisingSurveyPicturesViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, SPUserResizableViewDelegate, UIPopoverPresentationControllerDelegate, MarkDetailsViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>
{
    SalesWorxMerchandisingBrand *selectedMerchandisingBrand;
    MerchandisingSurveyPictures *selectedsurveyPicture;
    
    NSMutableArray *productArray;
    IBOutlet MedRepTextField *productTextField;
    IBOutlet MedRepTextView *descTextView;
    UITapGestureRecognizer *singleTapOnImageView;
    NSIndexPath *selectedIndexPath;
    
    SPUserResizableView *currentlyEditingView;
    SPUserResizableView *lastEditedView;
    
    IBOutlet UIImageView *surveyPictureImageView;
    IBOutlet UICollectionView *markCollectionView;
    IBOutlet UIView *placeholderView;
    IBOutlet UIButton *cameraButton;
    
    int markCount;
}
-(void)showSurveyPicturesforBrand:(SalesWorxMerchandisingBrand*)merchandisingObject;

@end
