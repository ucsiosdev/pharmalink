//
//  SalesWorxMerchandisingRadioButtonTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/3/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SalesWorxSingleLineLabel.h"


@interface SalesWorxMerchandisingRadioButtonTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;
@end
