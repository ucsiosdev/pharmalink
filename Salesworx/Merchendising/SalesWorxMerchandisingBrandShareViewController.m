//
//  SalesWorxMerchandisingBrandShareViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingBrandShareViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxMerchandisingBrandSharePromotionCollectionViewCell.h"

@interface SalesWorxMerchandisingBrandShareViewController ()

@end

@implementation SalesWorxMerchandisingBrandShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    camelLabelsView.tickCount=10;
    camelLabelsView.ticksDistance=75;
    camelLabelsView.value=0;
    camelLabelsView.upFontName=@"WeblySleekUISemibold";
    camelLabelsView.upFontSize=20;
    camelLabelsView.upFontColor=kNavigationBarBackgroundColor;
    camelLabelsView.downFontName=@"WeblySleekUISemibold";
    camelLabelsView.downFontSize=20;
    camelLabelsView.downFontColor=[UIColor lightGrayColor];
    camelLabelsView.backgroundColor=[UIColor clearColor];
    
    
    descreteSliderView.tickStyle=1;
    descreteSliderView.tickSize = CGSizeMake(1, 8);
    descreteSliderView.tickCount=10;
    descreteSliderView.trackThickness=2;
    descreteSliderView.incrementValue=0;
    descreteSliderView.minimumValue=0;
    descreteSliderView.value=0;
    [descreteSliderView addTarget:self action:@selector(customSliderValueChanged) forControlEvents:UIControlEventValueChanged];
    
    descreteSliderView.tintColor=kNavigationBarBackgroundColor;

    
    
    descreteSliderView.ticksListener=camelLabelsView;
    
    [promotionCollectionView registerClass:[SalesWorxMerchandisingBrandSharePromotionCollectionViewCell class] forCellWithReuseIdentifier:@"promotionIdentifierCell"];
    
    calculationInArray=[[NSMutableArray alloc]initWithObjects:@"Meter",@"Facing", nil];
    
}

-(void)customSliderValueChanged
{
    selectedBrandShare.quality_Rating = [NSString stringWithFormat:@"%f",descreteSliderView.value+1];
    selectedBrandShare.isUpdated = KAppControlsYESCode;
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [self loadSOSPieChart];
    }
    
    arrayPromotions = [[NSMutableArray alloc]init];
    NSMutableArray *promotionArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"SELECT Code_Description FROM TBL_App_Codes where Code_Type = 'MERCH_PROMO_INFO'"];
    
    for (NSDictionary *dicPromotion in promotionArray) {
        [arrayPromotions addObject:[dicPromotion valueForKey:@"Code_Description"]];
    }
}

-(void)showBrandShareData:(MerchandisingBrandShare *)selectedBrand
{
    [self.view endEditing:YES];
    [slicesForCustomerPotential removeAllObjects];
    [SOS_PieChart reloadData];

    selectedBrandShare = selectedBrand;
    
    if (!selectedBrandShare.selectedSOS) {
        selectedBrandShare.selectedSOS=[[MerchandisingBrandShareOfSelf alloc]init];
        calculationTextField.text=@"";
        brandShareTextField.text=@"";
        categoryTextField.text=@"";
        sosPercentageTextField.text=@"";
        marketSharePercentageTextField.text=@"";
    }
    else{
        calculationTextField.text=selectedBrandShare.selectedSOS.calculationIn;
        brandShareTextField.text=[NSString getValidStringValue:selectedBrandShare.selectedSOS.brandShare.stringValue];
        categoryTextField.text=[NSString getValidStringValue:selectedBrandShare.selectedSOS.category.stringValue];
        sosPercentageTextField.text=[NSString getValidStringValue:selectedBrandShare.selectedSOS.shareOfShelf.stringValue];
        marketSharePercentageTextField.text=[NSString getValidStringValue:selectedBrandShare.selectedSOS.marketShare.stringValue];
        
            [slicesForCustomerPotential removeAllObjects];
            [slicesForCustomerPotential addObject:[NSNumber numberWithFloat:[selectedBrandShare.selectedSOS.brandShare floatValue]]];
            [slicesForCustomerPotential addObject:[NSNumber numberWithFloat:[selectedBrandShare.selectedSOS.category floatValue]]];
        
        NSLog(@"pie chart data is %@",slicesForCustomerPotential);
        
            [SOS_PieChart reloadData];
        
    }
    if ([selectedBrandShare.quality_Rating doubleValue] > 0) {
        double currentRating = [selectedBrandShare.quality_Rating doubleValue]-1;
        descreteSliderView.value= currentRating;
        camelLabelsView.value=currentRating;
    }
    else{
        descreteSliderView.value=0;
        camelLabelsView.value=0;
    }
    
    
    promotionArray=[[NSMutableArray alloc]init];
    selectedIndexPathArray=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *implDict=[[NSMutableDictionary alloc]init];
    [implDict setObject:@"Implemented" forKey:@"Title"];
    [promotionArray addObject:implDict];
    
    NSMutableDictionary *notImplDict=[[NSMutableDictionary alloc]init];
    [notImplDict setObject:@"Not Implemented" forKey:@"Title"];
    [promotionArray addObject:notImplDict];


    if ([selectedBrandShare.promotion isEqualToString:@"Implemented"]) {
        [selectedIndexPathArray addObject:[NSIndexPath indexPathForRow:0 inSection:0]];
    } else if ([selectedBrandShare.promotion isEqualToString:@"Not Implemented"]){
        [selectedIndexPathArray addObject:[NSIndexPath indexPathForRow:1 inSection:0]];
    }
    [brandShareTableView reloadData];
}

#pragma mark Load PieChart
-(void)loadSOSPieChart
{
    slicesForCustomerPotential = [[NSMutableArray alloc]init];
    sliceColorsForCustomerPotential = [[NSMutableArray alloc]init];
    sliceColorsForCustomerPotential = [NSArray arrayWithObjects:
                                            [UIColor colorWithRed:53.0/255.0 green:194.0/255.0 blue:195.0/255.0 alpha:1],
                                            [UIColor colorWithRed:16.0/255.0 green:49.0/255.0 blue:57.0/255.0 alpha:1],
                                            nil];
    
    SOS_PieChart.backgroundColor = [UIColor clearColor];
    SOS_PieChart.labelFont = [UIFont boldSystemFontOfSize:14];
    
    SOS_PieChart.labelRadius = 30;
    [SOS_PieChart setDelegate:self];
    [SOS_PieChart setDataSource:self];
    [SOS_PieChart setPieCenter:CGPointMake(SOS_PieChart.frame.size.width/2, SOS_PieChart.frame.size.height/2)];
    [SOS_PieChart setShowPercentage:YES];
    [SOS_PieChart setLabelColor:[UIColor whiteColor]];
    [SOS_PieChart setUserInteractionEnabled:NO];
    
    //[slicesForCustomerPotential removeAllObjects];
   // [slicesForCustomerPotential addObject:[NSNumber numberWithFloat:2]];
    //[slicesForCustomerPotential addObject:[NSNumber numberWithFloat:5]];
    [SOS_PieChart reloadData];
}

#pragma mark Pie chart delegate methods

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    if (pieChart == SOS_PieChart && slicesForCustomerPotential.count>0) {
        count = slicesForCustomerPotential.count;
    }
    return count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    if (pieChart == SOS_PieChart) {
        count = [[NSString stringWithFormat:@"%@",[slicesForCustomerPotential objectAtIndex:index]]floatValue];
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == SOS_PieChart) {
        colour = [sliceColorsForCustomerPotential objectAtIndex:(index % sliceColorsForCustomerPotential.count)];
    }
    return colour;
}

- (IBAction)btnPromotionImplemented:(id)sender {
    
    selectedBrandShare.promotion = @"Implemented";
    selectedBrandShare.isUpdated = KAppControlsYESCode;
    [btnPromotionImplemented setImage:[UIImage imageNamed:@"Survey_RadioActive"] forState:UIControlStateNormal];
    [btnPromotionNotImplemented setImage:[UIImage imageNamed:@"Survey_RadioInActive"] forState:UIControlStateNormal];
}

- (IBAction)btnPromotionNotImplemented:(id)sender {
    selectedBrandShare.promotion = @"Not Implemented";
    selectedBrandShare.isUpdated = KAppControlsYESCode;
    [btnPromotionImplemented setImage:[UIImage imageNamed:@"Survey_RadioInActive"] forState:UIControlStateNormal];
    [btnPromotionNotImplemented setImage:[UIImage imageNamed:@"Survey_RadioActive"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return promotionArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier= @"radioCell";
    
    NSMutableDictionary * currentDict=[promotionArray objectAtIndex:indexPath.row];
    SalesWorxMerchandisingRadioButtonTableViewCell* cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxMerchandisingRadioButtonTableViewCell" owner:self options:nil] firstObject];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([selectedIndexPathArray containsObject:indexPath]) {
        cell.selectionImageView.image=[UIImage imageNamed:kSelectedRadioButtonImage];
    }
    else
    {
        cell.selectionImageView.image=[UIImage imageNamed:kUnSelectRadioButtonImage];
    }
    cell.lblTitle.text=[NSString getValidStringValue:[currentDict valueForKey:@"Title"]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0) {
        selectedBrandShare.promotion = @"Implemented";
    }
    else{
        selectedBrandShare.promotion = @"Not Implemented";
    }
    
    selectedIndexPathArray=[[NSMutableArray alloc]init];
    [selectedIndexPathArray addObject:indexPath];
    selectedBrandShare.isUpdated = KAppControlsYESCode;

    [brandShareTableView reloadData];

}

#pragma mark Share of Shelf Calculation methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==calculationTextField) {
        [self showCalculationInPopOver];
        return NO;
    }
    if (textField==sosPercentageTextField) {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    selectedBrandShare.isUpdated = KAppControlsYESCode;

    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSString* regexString=[[NSString alloc]init];
    
    if (textField==brandShareTextField || textField==categoryTextField) {
        regexString=KDecimalQuantityRegExpression;
    }
    else if (textField==marketSharePercentageTextField)
    {
        regexString=KWholeNumberQuantityRegExpression;
    }
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:updatedString options:0 range:NSMakeRange(0, [updatedString length])];
    
    if (updatedString.length>9) {
        return NO;
    }
    else if (numberOfMatches>0)
    {
        if (textField==brandShareTextField)
        {
            if ([NSString isEmpty:updatedString]) {
               sosPercentageTextField.text=@"";
                selectedBrandShare.selectedSOS.shareOfShelf=nil;
                
                [slicesForCustomerPotential removeAllObjects];
                [SOS_PieChart reloadData];
            }
            else{
            selectedBrandShare.selectedSOS.brandShare=[NSDecimalNumber decimalNumberWithString:[NSString getValidStringValue:updatedString]];
            [self calculateSOSPercentage];
            }

        }
        else if (textField==categoryTextField)
        {
            if ([NSString isEmpty:updatedString]) {
                sosPercentageTextField.text=@"";
                selectedBrandShare.selectedSOS.category=nil;
                [slicesForCustomerPotential removeAllObjects];
                [SOS_PieChart reloadData];

            }
            else{
            selectedBrandShare.selectedSOS.category=[NSDecimalNumber decimalNumberWithString:[NSString getValidStringValue:updatedString]];
            [self calculateSOSPercentage];
            }

        }
        else if (textField==marketSharePercentageTextField)
        {
            selectedBrandShare.selectedSOS.marketShare=[NSString isEmpty:updatedString]?nil: [NSDecimalNumber decimalNumberWithString:[NSString getValidStringValue:updatedString]];
            
        }
        
        return (numberOfMatches!=0);
  
    }
    else{
        return (numberOfMatches!=0);
    }
    
}
-(void)showCalculationInPopOver
{
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=calculationInArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    calculationInPopOverController=nil;
    calculationInPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    calculationInPopOverController.delegate=self;
    popOverVC.popOverController=calculationInPopOverController;
    popOverVC.titleKey=@"Calculation In";
    
    [calculationInPopOverController setPopoverContentSize:CGSizeMake(230, 200) animated:YES];
    [calculationInPopOverController presentPopoverFromRect:calculationTextField.dropdownImageView.frame inView:calculationTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSString* calculationInString=[NSString getValidStringValue:[calculationInArray objectAtIndex:selectedIndexPath.row]];
    NSLog(@"selected calculation in is %@", calculationInString);
    
    calculationTextField.text=calculationInString;
    
    selectedBrandShare.selectedSOS.calculationIn=calculationInString;
    
}

-(void)calculateSOSPercentage
{
    if (selectedBrandShare.selectedSOS.category && selectedBrandShare.selectedSOS.brandShare) {
    
    NSDecimalNumber * differencePercentage= [[selectedBrandShare.selectedSOS.brandShare  decimalNumberByDividingBy:selectedBrandShare.selectedSOS.category] decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithDouble:100]];
       // NSLog(@"difference percentage is %@", differencePercentage);
        sosPercentageTextField.text = [NSString stringWithFormat:@"%.2f",[differencePercentage floatValue]];
        selectedBrandShare.selectedSOS.shareOfShelf = [NSDecimalNumber decimalNumberWithString:sosPercentageTextField.text];
        
        
        [slicesForCustomerPotential removeAllObjects];
        [slicesForCustomerPotential addObject:[NSNumber numberWithFloat:[selectedBrandShare.selectedSOS.brandShare floatValue]]];
        [slicesForCustomerPotential addObject:[NSNumber numberWithFloat:[selectedBrandShare.selectedSOS.category floatValue]]];
        
        NSLog(@"pie chart data is %@",slicesForCustomerPotential);

        [SOS_PieChart reloadData];

        
    }
    else
    {
        sosPercentageTextField.text=@"";
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
