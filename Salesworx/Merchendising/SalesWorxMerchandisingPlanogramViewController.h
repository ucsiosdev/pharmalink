//
//  SalesWorxMerchandisingPlanogramViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWAppDelegate.h"
#import "SalesWorxCustomClass.h"
#import "MMCameraPickerController.h"
#import "RippleAnimation.h"
#import "CameraViewController.h"
#import "PlanogramImagesCollectionViewCell.h"
#import "UIImage+SalesWorxImage.h"
#import "PlanogramDetailsViewController.h"
#import "PresentationDestinationCollectionViewCell.h"

@interface SalesWorxMerchandisingPlanogramViewController : UIViewController<MMCameraDelegate,PlanogramDelegate,UICollectionViewDelegate,UICollectionViewDataSource,PlanogramDetailsDelegate>
{
    NSMutableArray* coordsArray;
    IBOutlet UIImageView *bgImageView;
    IBOutlet UIImageView *imgView;
    RippleAnimation *ripple;
    IBOutlet UIButton *cameraButton;
    CameraViewController * cameraVC;
    IBOutlet UICollectionView *planogramImagesCollectionView;
    
    NSMutableArray* planogromImagesArray;
    UIImage * selectedtemplateImage;
    NSIndexPath* selectedIndexPath;
    
    IBOutlet UIButton *deleteButton;
    NSMutableArray* planogramDataArray;
    
    MerchandisingPlanogram * selectedPlanogram;
}
@property (strong, nonatomic) IBOutlet UIView *placeholderView;

-(void)showPlanogramImagesforBrand:(NSMutableArray*)detailsArray;
-(UIImage*) rotate:(UIImage*)src andOrientation:(UIImageOrientation)orientation;

- (IBAction)cameraButtonTapped:(id)sender;
- (IBAction)deleteImageButtonTapped:(id)sender;

@end
