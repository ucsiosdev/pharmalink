//
//  SalesWorxMerchandisingSurveyPictureMarkDetailsViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 1/31/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxPresentControllerBaseViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@protocol MarkDetailsViewControllerDelegate

-(void)saveMarkDescription:(NSString *)markID WithDesc:(NSString *)markDescription;
-(void)deleteMark:(NSString *)markID;

@end

@interface SalesWorxMerchandisingSurveyPictureMarkDetailsViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet  UITextView *markDescTextView;
    
    id markVCDelegate;
}
@property (strong,nonatomic) NSString *markID;
@property (strong,nonatomic) NSString *markDescription;

@property(nonatomic) id markVCDelegate;

@end
