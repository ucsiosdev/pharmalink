//
//  SalesWorxMerchandisingCompetitionInfoViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCompetitonInfoTableViewCell.h"
#import "SalesWorxTableView.h"
#import "MedRepTextField.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepTextView.h"
#import "NSString+Additions.h"
#import "CompetitorImageDisplayViewController.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"


#define kStockLevelTextField @"Stock Level"
#define kThreatLevelTextField @"Threat Level"
#define kShelfPlacementTextField @"Shelf Placement"

#define competitorInfoCharacterLimit 40
#define productNameCharacterLimit 1000
#define priceCharacterLimit 18


@interface SalesWorxMerchandisingCompetitionInfoViewController : UIViewController<UIPopoverControllerDelegate,UITextViewDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate>
{
    IBOutlet MedRepTextField *brandTextField;
    IBOutlet MedRepTextField *productNameTextField;
    IBOutlet UISwitch *activePromotionSwitch;
    IBOutlet MedRepTextField *priceTextField;
    IBOutlet MedRepTextField *threatLevelTextField;
    IBOutlet MedRepTextView *notesTextView;
    IBOutlet MedRepTextField *stockLevelTextField;
    IBOutlet MedRepTextField *shelfPlacementTextField;
    UIPopoverController * competitorInfoPopOverController;
    
    NSString* selectedTextField;
    NSMutableArray* stockLevelArray;
    NSMutableArray* threatLevelArray;
    NSMutableArray* shelfPlacementArray;
    
    MerchandisingCompetitionInfo * currentCompetitorInfo;
    SalesWorxMerchandisingBrand * selectedMerchandisingBrand;
    
    NSString* tempStockLevelString;
    NSString* tempShelfPlacementString;
    NSString* tempThreatlevelString;

    IBOutlet UIImageView *competitorImageView;
    IBOutlet MedRepButton *addButton;
}
@property (strong, nonatomic) IBOutlet UIView *headerView;
- (IBAction)activePromotionSwitchTapped:(id)sender;
@property (strong, nonatomic) IBOutlet SalesWorxTableView *competitorTableView;
- (IBAction)addButtonTapped:(id)sender;
- (IBAction)clearButtonTapped:(id)sender;
- (IBAction)cameraButtonTapped:(id)sender;


@property(strong,nonatomic) NSMutableArray* competitorInfoArray;
-(void)showCompetitorInfoData:(SalesWorxMerchandisingBrand*)selectedBrand;
@end
