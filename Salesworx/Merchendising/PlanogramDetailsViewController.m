//
//  PlanogramDetailsViewController.m
//  MedRep
//
//  Created by Unique Computer Systems on 2/5/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "PlanogramDetailsViewController.h"

@interface PlanogramDetailsViewController ()

@end

@implementation PlanogramDetailsViewController
@synthesize matchedSegment,selectedPlanogram,sourceImageView,destinationImageView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [matchedSegment setTitle:@"YES" forSegmentAtIndex:0];
    [matchedSegment setTitle:@"NO" forSegmentAtIndex:1];
    
    self.view.backgroundColor= UIViewControllerBackGroundColor;
    
    matchedSegment.selectedSegmentIndex = UISegmentedControlNoSegment;
    


}

-(void)viewWillAppear:(BOOL)animated
{
    MediaFile* currentFile=selectedPlanogram.sourceImage;
    sourceImageView.image=[UIImage imageWithContentsOfFile:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:currentFile.Filename]];
    destinationImageView.image=[UIImage imageWithContentsOfFile:[[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kMerchandisingPlanogramImages]stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedPlanogram.capturedImageName]]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveButtontapped:(id)sender {
    
    if (matchedSegment.selectedSegmentIndex==0 || matchedSegment.selectedSegmentIndex==1) {
    selectedPlanogram.imageMatched = matchedSegment.selectedSegmentIndex==0?KAppControlsYESCode:KAppControlsNOCode;
    
    if ([self.planogramDetailsDelegate respondsToSelector:@selector(didSavePlanogramDetails:)]) {
        
        [self.planogramDetailsDelegate didSavePlanogramDetails:selectedPlanogram];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select planogram matched value" withController:self];
    }
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    
    UIAlertAction* yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    if ([self.planogramDetailsDelegate respondsToSelector:@selector(didCancelPlanogramMatching)]) {
                                        
                                        [self.planogramDetailsDelegate didCancelPlanogramMatching];
                                    }
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    
    UIAlertAction * noAction=[UIAlertAction actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction ,nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Cancel" andMessage:@"Would you like to cancel? captured image will be discarded." andActions:actionsArray withController:self];

   
}


@end
