//
//  NSDecimalNumber+SalesWorxDecimalNumber.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 1/30/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDecimalNumber (SalesWorxDecimalNumber)
+ (NSDecimalNumber *_Nullable)ValidDecimalNumberWithString:(nullable NSString *)numberValue;

- (BOOL) isEqualToDecimal:(nullable NSDecimalNumber *)i;
- (BOOL) isGraterThanDecimal:(nullable NSDecimalNumber *)i;
- (BOOL) isGreaterThanOrEqualToDecimal:(nullable NSDecimalNumber *)i;
- (BOOL) isLessThanDecimal:(nullable NSDecimalNumber *)i;
- (BOOL) isLessThanOrEqualToDecimal:(nullable NSDecimalNumber *)i;
-(BOOL)isWholeNumber;
- (BOOL) isBetweenOREqualToFromDecimal:(nullable NSDecimalNumber *)i ToDescimal:(nullable NSDecimalNumber *)j;
- (BOOL) isBetweenFromDecimal:(nullable NSDecimalNumber *)i ToDescimal:(nullable NSDecimalNumber *)j;
@end
