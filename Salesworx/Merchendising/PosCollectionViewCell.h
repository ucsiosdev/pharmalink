//
//  PosCollectionViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 3/20/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PosCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *captionLbl;
@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIImageView *captionBgView;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageView;

@end
