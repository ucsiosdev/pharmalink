//
//  PosPromotionCollectionViewCell.m
//  MedRep
//
//  Created by Unique Computer Systems on 3/20/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "PosPromotionCollectionViewCell.h"

@implementation PosPromotionCollectionViewCell
@synthesize containerView;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    containerView.layer.borderWidth=1.0f;
    containerView.layer.masksToBounds=YES;
    containerView.layer.borderColor=[UIColor colorWithRed:(227.0/255.0) green:(231.0/255.0) blue:(234.0/255.0) alpha:1].CGColor;
    containerView.layer.cornerRadius = 10.0f;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"PosPromotionCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
}
@end
