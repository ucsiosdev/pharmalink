//
//  SalesWorxMerchandisingPlanogramViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingPlanogramViewController.h"
#import <rembrandt/rembrandt-Swift.h>

@interface SalesWorxMerchandisingPlanogramViewController ()

@end

@implementation SalesWorxMerchandisingPlanogramViewController
@synthesize placeholderView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    coordsArray=[[NSMutableArray alloc]init];
//    [self isTheImage:[UIImage imageNamed:@"IncompletePlanoGram"] apparentlyEqualToImage:[UIImage imageNamed:@"CompletePlaneGram"]
//        accordingToRandomPixelsPer1:0.001];
    
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didMatchTemplate:) name:@"Planogram_Matched" object:nil];
    
    [planogramImagesCollectionView registerClass:[PlanogramImagesCollectionViewCell class] forCellWithReuseIdentifier:@"planogramImageCell"];

   // [planogramImagesCollectionView registerClass:[PresentationDestinationCollectionViewCell class] forCellWithReuseIdentifier:@"destinationCell"];

    
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    self.view.layer.shadowOffset = CGSizeMake(3, 3);
    self.view.layer.shadowOpacity = 0.1;
    self.view.layer.shadowRadius = 1.0;

    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
   
}

-(void)didMatchTemplate:(NSNotification*)planogramNotification
{
    NSDictionary* userInfoDict= planogramNotification.userInfo;
    
    
    [cameraVC dismissViewControllerAnimated:YES completion:^{
        UIImage * capturedImage = [userInfoDict valueForKey:@"CapturedImage"];
        imgView.image=capturedImage;
        NSLog(@"did match template called with user info dict %@",capturedImage);
        
//        NSData *pngData = UIImagePNGRepresentation(capturedImage);
//        NSString *filePath = [[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"PlanogramMatchedImage.png"]; //Add the file name
//        [pngData writeToFile:filePath atomically:YES]; //Write the file


    }];
}
-(void)showPlanogramImagesforBrand:(NSMutableArray*)detailsArray
{
    
    //fetch planogram template images
    selectedtemplateImage=nil;
    planogramDataArray=[[NSMutableArray alloc]init];
    planogramDataArray=detailsArray;
    
    //Setting empty image when brands are changed
    imgView.image=[UIImage imageNamed:@""];
    cameraButton.hidden=NO;
    deleteButton.hidden=YES;
    
    [planogramImagesCollectionView reloadData];

    
    if (planogramDataArray.count>0) {
        placeholderView.hidden=YES;
        
        
/*
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSInteger i=0;i<planogramDataArray.count;i++) {
            MerchandisingPlanogram* planoGram = [planogramDataArray objectAtIndex:i];
            UIImage* fetchedImage=[[UIImage alloc]init];
            if (![NSString isEmpty:planoGram.capturedImageName]) {
                 fetchedImage =[UIImage imageWithContentsOfFile:[[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kMerchandisingPlanogramImages]stringByAppendingPathComponent:@"temp"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",planoGram.capturedImageName]]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                imgView.image= fetchedImage;
               // [planogramImagesCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
            });
        }
        
    });
        */
    
    }
    else
    {
        placeholderView.hidden=NO;
    }

   /* dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        IMGLYRembrandt* remb= [[IMGLYRembrandt alloc]init];
        RembrandtResult *rest=[remb compareWithImageA:[UIImage imageNamed:@"TestA"] imageB:[UIImage imageNamed:@"TestB"] ];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           // imgView.image=rest.composition;
            //imgView.backgroundColor=[UIColor clearColor];
            
            
            
        });
    });*/
}

- (IBAction)cameraButtonTapped:(id)sender {
    
    NSLog(@"selected template image is %@", selectedtemplateImage);
    
    //selectedtemplateImage =[UIImage imageNamed:@"Template.jpg"];
    
    if (selectedtemplateImage) {
        
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if(authStatus == AVAuthorizationStatusAuthorized)
            {
                [self openCamera];
            }
            else if(authStatus == AVAuthorizationStatusNotDetermined)
            {
                NSLog(@"%@", @"Camera access not determined. Ask for permission.");
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
                 {
                     if(granted)
                     {
                         NSLog(@"Granted access to %@", AVMediaTypeVideo);
                         [self openCamera];
                     }
                     else
                     {
                         NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                         [self cameraDenied];
                     }
                 }];
            }
            else if (authStatus == AVAuthorizationStatusRestricted)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"You've been restricted from using the camera on this device." withController:self];
            }
            else if(authStatus == AVAuthorizationStatusDenied) {
                [self cameraDenied];
            }
            else
            {
                NSLog(@"%@", @"Camera access unknown error.");
            }
        } else {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Device has no camera" withController:self];
        }
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select template image" withController:self];
        
    }
}


-(void)openCamera {
    
    cameraVC=[[CameraViewController alloc]init];
    cameraVC.templateImage=selectedtemplateImage;
    cameraVC.planogramPatternDelegate=self;
    [self presentViewController:cameraVC animated:YES completion:nil];
}
-(void)cameraDenied
{
    // denied
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Select Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
        
        alertButton = @"Go";
    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:alertButton
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   if (canOpenSettings)
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Error" andMessage:alertText andActions:actionsArray withController:[SWDefaults currentTopViewController]];
}


- (IBAction)deleteImageButtonTapped:(id)sender {
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(kYesTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        selectedPlanogram.isUpdated=KAppControlsNOCode;
        selectedPlanogram.capturedImageName = nil;
        imgView.image=[[UIImage alloc]init];
        cameraButton.hidden=NO;
        deleteButton.hidden=YES;

    }];
    
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:NSLocalizedString(kNoTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    NSMutableArray* actionArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction, nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Delete Image" andMessage:@"Would you like to delete the Image?" andActions:actionArray withController:self];
    
    
}

-(UIImage*) rotate:(UIImage*)src andOrientation:(UIImageOrientation)orientation
{
    UIGraphicsBeginImageContext(src.size);
    
    CGContextRef context=(UIGraphicsGetCurrentContext());
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, 90/180*M_PI) ;
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, -90/180*M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 90/180*M_PI);
    }
    
    [src drawAtPoint:CGPointMake(0, 0)];
    UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
}

-(void)didCaptureCameraImage:(UIImage*)capturedImage
{
//    UIImage *imageToDisplay =
//    [UIImage imageWithCGImage:[capturedImage CGImage]
//                        scale:[capturedImage scale]
//                  orientation: UIImageOrientationRight];
    
    NSString* imageName=[NSString createGuid];

    
    selectedPlanogram.isUpdated=KAppControlsYESCode;
    selectedPlanogram.capturedImageName = imageName;
    
    [SWDefaults savePlanogramImages:capturedImage withImageName:imageName];

    imgView.image=[UIImage imageWithContentsOfFile:[[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kMerchandisingPlanogramImages]stringByAppendingPathComponent:@"temp"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedPlanogram.capturedImageName]]];

    cameraButton.hidden=YES;
    deleteButton.hidden=NO;
    [self dismissViewControllerAnimated:YES completion:^{
        PlanogramDetailsViewController * detailsVC = [[PlanogramDetailsViewController alloc]init];
        detailsVC.selectedPlanogram=selectedPlanogram;
        detailsVC.planogramDetailsDelegate=self;
        [self presentViewController:detailsVC animated:YES completion:nil]
        ;

    }];

}

-(void)didSavePlanogramDetails:(MerchandisingPlanogram*)savedPlanogram
{
    selectedPlanogram=savedPlanogram;
    
    
}
-(void)didCancelPlanogramMatching
{
    selectedPlanogram.isUpdated=KAppControlsNOCode;
    selectedPlanogram.capturedImageName = nil;

    imgView.image=[[UIImage alloc]init];
    cameraButton.hidden=NO;
    deleteButton.hidden=YES;

}



-(void)didDetectPlanogramPattern:(NSMutableDictionary*)parmetersDict
{
    NSLog(@"success delegate called");
    
    NSDictionary* userInfoDict= parmetersDict;
    [cameraVC dismissViewControllerAnimated:YES completion:^{
        UIImage * capturedImage = [userInfoDict valueForKey:@"CapturedImage"];
        imgView.image=capturedImage;
        NSLog(@"did match template called with user info dict %@",capturedImage);
    }];
}
-(void)didFailToDetectPlanogramPattern
{
    NSLog(@"failure delegate called");
    [cameraVC dismissViewControllerAnimated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)isTheImage:(UIImage *)image1 apparentlyEqualToImage:(UIImage *)image2 accordingToRandomPixelsPer1:(float)pixelsPer1
{
//    if (!CGSizeEqualToSize(image1.size, image2.size))
//    {
//        return false;
//    }
    
    int pixelsWidth = CGImageGetWidth(image1.CGImage);
    int pixelsHeight = CGImageGetHeight(image1.CGImage);
    
    int pixelsToCompare = pixelsWidth * pixelsHeight * pixelsPer1;
    
    uint32_t pixel1;
    CGContextRef context1 = CGBitmapContextCreate(&pixel1, 1, 1, 8, 4, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaNoneSkipFirst);
    uint32_t pixel2;
    CGContextRef context2 = CGBitmapContextCreate(&pixel2, 1, 1, 8, 4, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaNoneSkipFirst);
    
    bool isEqual = true;
    
    
    for (int x=0;x<pixelsWidth-1;x++) {
        
        
        for (int y=0;x<pixelsHeight-1;y++) {

            CGContextDrawImage(context1, CGRectMake(-x, -y, pixelsWidth, pixelsHeight), image1.CGImage);
            CGContextDrawImage(context2, CGRectMake(-x, -y, pixelsWidth, pixelsHeight), image2.CGImage);
            
            if (pixel1 != pixel2)
            {
                [coordsArray addObject:[NSValue valueWithCGRect:CGRectMake(x, y, 1, 1)]];
                
                //isEqual = false;
                //break;
            }

            
        }
        
    }
    
    for (int i = 0; i < pixelsToCompare; i++)
    {
        int pixelX = arc4random() % pixelsWidth;
        int pixelY = arc4random() % pixelsHeight;
        
        CGContextDrawImage(context1, CGRectMake(-pixelX, -pixelY, pixelsWidth, pixelsHeight), image1.CGImage);
        CGContextDrawImage(context2, CGRectMake(-pixelX, -pixelY, pixelsWidth, pixelsHeight), image2.CGImage);
        
        if (pixel1 != pixel2)
        {
           // [coordsArray addObject:[NSValue valueWithCGRect:CGRectMake(pixelX, pixelY, 1, 1)]];

            //isEqual = false;
            //break;
        }
    }
    CGContextRelease(context1);
    CGContextRelease(context2);
    
   // return isEqual;
}

#pragma mark UICollection View Cell Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return planogramDataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    static NSString *cellIdentifier = @"destinationCell";
    PresentationDestinationCollectionViewCell *cell = (PresentationDestinationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.deleteButton.hidden=YES;
    cell.countLbl.hidden=YES;
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    MerchandisingPlanogram * currentPlanogram = [planogramDataArray objectAtIndex:indexPath.row];
    
    MediaFile* currentFile=currentPlanogram.sourceImage;
    
    cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:currentFile.Filename]];
    
    cell.captionLbl.text=[NSString getValidStringValue:currentFile.Caption];
    
        cell.placeHolderImageView.hidden=NO;
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    
    return cell;
    */
    
    PlanogramImagesCollectionViewCell * cell =(PlanogramImagesCollectionViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"planogramImageCell" forIndexPath:indexPath];
    
    MerchandisingPlanogram * currentPlanogram = [planogramDataArray objectAtIndex:indexPath.row];
    
    MediaFile* currentFile=currentPlanogram.sourceImage;
    cell.captionLbl.text=[NSString getValidStringValue:currentFile.Caption];
    if ([currentPlanogram.isSelected isEqualToString:KAppControlsYESCode]) {
        cell.selectedImgView.hidden=NO;
    }
    else
    {
        cell.selectedImgView.hidden=YES;

    }
//    if (selectedIndexPath==indexPath) {
//        cell.selectedImgView.hidden=NO;
//    }
//    else
//    {
//        cell.selectedImgView.hidden=YES;
//    }
    cell.productImgViewTopConstraint.constant=2.0;
    cell.productimageViewLeadingConstraint.constant=2.0;
    cell.productImageViewTrailingConstraint.constant=2.0;
    
   __block UIImage *localImage = [[UIImage alloc]init];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        localImage =[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:currentFile.Filename]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.productImageView.image = localImage;
        });
    });
    

    

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(160, 162);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedPlanogram = [planogramDataArray objectAtIndex:indexPath.row];
    
    [planogramDataArray setValue:KAppControlsNOCode forKey:@"isSelected"];
    
    
    selectedPlanogram.isSelected=KAppControlsYESCode;

//    if([selectedPlanogram.isSelected isEqualToString:KAppControlsYESCode])
//    {
//        selectedPlanogram.isSelected=KAppControlsNOCode;
//    }
//    else
//    {
//        selectedPlanogram.isSelected=KAppControlsNOCode;
//    }
    
    
    MediaFile* currentFile=selectedPlanogram.sourceImage;
    selectedtemplateImage=nil;
    selectedtemplateImage=[UIImage imageWithContentsOfFile:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:currentFile.Filename]];
    selectedIndexPath=indexPath;
    [collectionView reloadData];
    
    if (![NSString isEmpty:selectedPlanogram.capturedImageName]) {
        imgView.image=[UIImage imageWithContentsOfFile:[[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kMerchandisingPlanogramImages]stringByAppendingPathComponent:@"temp"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedPlanogram.capturedImageName]]];
        cameraButton.hidden=YES;
        deleteButton.hidden=NO;

    }
    else{
        cameraButton.hidden=NO;
        deleteButton.hidden=YES;
        imgView.image=[UIImage imageNamed:@""];
    }
    

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {

    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
