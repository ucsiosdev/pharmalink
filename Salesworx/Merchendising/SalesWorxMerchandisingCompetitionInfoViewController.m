//
//  SalesWorxMerchandisingCompetitionInfoViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingCompetitionInfoViewController.h"

@interface SalesWorxMerchandisingCompetitionInfoViewController ()

@end

@implementation SalesWorxMerchandisingCompetitionInfoViewController
@synthesize headerView,competitorInfoArray,competitorTableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    stockLevelArray = [MedRepQueries getTBLAppCodesDataForCodeType:@"MERCH_STOCK_LEVEL"];

    NSMutableArray* tempShelfDataArray = [MedRepQueries getTBLAppCodesDataForCodeType:@"MERCH_SHELF_PLCMT"];

    if (tempShelfDataArray.count>0) {
        
        shelfPlacementArray=[[NSMutableArray alloc]init];
        
        for (TBLAppCode *appCodeObj in tempShelfDataArray) {
            if ([[appCodeObj.Code_Description uppercaseString] isEqualToString:@"EYE LEVEL"]) {
                [shelfPlacementArray addObject:appCodeObj];
            }
            else if ([[appCodeObj.Code_Description uppercaseString] isEqualToString:@"WAIST LEVEL"])
            {
                [shelfPlacementArray addObject:appCodeObj];
            }
            else if ([[appCodeObj.Code_Description uppercaseString] isEqualToString:@"ANKLE LEVEL"])
            {
                [shelfPlacementArray addObject:appCodeObj];
            }
        }
    }
    
    //stockLevelArray=[[NSMutableArray alloc]initWithObjects:@"High",@"Even",@"Low", nil];
    threatLevelArray=[[NSMutableArray alloc]initWithObjects:@"High",@"Medium",@"Low", nil];
   // shelfPlacementArray=[[NSMutableArray alloc]initWithObjects:@"Eye Level",@"Waist Level",@"Ankle Level", nil];
    [activePromotionSwitch setOn:NO];
    
    UITapGestureRecognizer * imageGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewTapped:)];
    competitorImageView.userInteractionEnabled=YES;
    [competitorImageView addGestureRecognizer:imageGesture];

    // Do any additional setup after loading the view from its nib.
}

-(void)showImage
{
    CompetitorImageDisplayViewController *imageDisplayVC = [[CompetitorImageDisplayViewController alloc]init];
    imageDisplayVC.competitorImage=competitorImageView.image;
    [self presentViewController:imageDisplayVC animated:YES completion:nil];
    
}
-(void)imageViewTapped:(UITapGestureRecognizer *)gesture
{
    NSLog(@"Image Gesture tapped");
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"Select Action", nil)
                                  message:NSLocalizedString(@"", nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* viewAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"View", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self showImage];
                                 }];
    
    UIAlertAction* editAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Retake", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self captureImage];
                                 }];
    
    UIAlertAction* deleteAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Delete", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     if (currentCompetitorInfo.imagesArray.count>0) {
                                         NSString* imageName = [currentCompetitorInfo.imagesArray objectAtIndex:0];
                                         NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"Merchandising Competitor Pictures"] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageName]];
                                         
                                         if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
                                             NSError * deleteError;
                                             BOOL delete =  [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&deleteError];
                                             if (delete) {
                                                 NSLog(@"file deleted successfully %@",imageName);
                                             }
                                         }
                                     }
                                     currentCompetitorInfo.imagesArray =[[NSMutableArray alloc]init];
                                     competitorImageView.image=nil;
                                     [competitorTableView reloadData];
                                 }];
    
    UIAlertAction* cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:viewAction,editAction,deleteAction,cancelAction ,nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Select Action" andMessage:@"" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Methods

-(void)showCompetitorInfoData:(SalesWorxMerchandisingBrand*)selectedBrand{

    selectedMerchandisingBrand=selectedBrand;
    if (selectedMerchandisingBrand.competitionInfoArray.count>0) {
        
    }
    else{
        selectedMerchandisingBrand.competitionInfoArray=[[NSMutableArray alloc]init];
        currentCompetitorInfo=[[MerchandisingCompetitionInfo alloc]init];
    }

    
    competitorTableView.rowHeight = UITableViewAutomaticDimension;
    competitorTableView.estimatedRowHeight = 44.0;
    [competitorTableView reloadData];
    [self clearAllFields];
   
}
-(void)updateCurrentData:(MerchandisingCompetitionInfo*)currentCompetitorInfo
{
    NSString* currentBrandNameString=[NSString getValidStringValue:currentCompetitorInfo.brandName];
    brandTextField.text = [NSString isEmpty:currentBrandNameString]?@"":currentBrandNameString;

    NSString* currentProductNameString=[NSString getValidStringValue:currentCompetitorInfo.productName];
    productNameTextField.text = [NSString isEmpty:currentProductNameString]?@"":currentProductNameString;

    NSString* currentPriceString=[NSString getValidStringValue:currentCompetitorInfo.price];
    priceTextField.text = [NSString isEmpty:currentPriceString]?@"":currentPriceString;

    NSString* activePromotionString=[NSString getValidStringValue:currentCompetitorInfo.activePromotion];
    if (![NSString isEmpty:currentPriceString]) {
        if ([activePromotionString isEqualToString:KAppControlsYESCode]) {
            [activePromotionSwitch setOn:YES];
        }
        else if ([activePromotionString isEqualToString:KAppControlsNOCode])
        {
            [activePromotionSwitch setOn:NO];
        }
    }
    else{
        [activePromotionSwitch setOn:NO];
    }
    
    NSString* stockLevelString=[NSString getValidStringValue:currentCompetitorInfo.stockLevel];
    stockLevelTextField.text = [NSString isEmpty:stockLevelString]?@"":stockLevelString;


    NSString* shelfPlacementString=[NSString getValidStringValue:currentCompetitorInfo.shelfPlacement];
    shelfPlacementTextField.text = [NSString isEmpty:shelfPlacementString]?@"":shelfPlacementString;
    
    
    NSString* threatLevelString=[NSString getValidStringValue:currentCompetitorInfo.threatLevel];
    threatLevelTextField.text = [NSString isEmpty:threatLevelString]?@"":threatLevelString;
    
    NSString* notesString=[NSString getValidStringValue:currentCompetitorInfo.notes];
    notesTextView.text = [NSString isEmpty:notesString]?@"":notesString;
    
    if (currentCompetitorInfo.imagesArray.count>0) {
        NSString* imageFileName = [currentCompetitorInfo.imagesArray objectAtIndex:0];
        NSString* imagePath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingCompetitorInfoImages] stringByAppendingPathComponent:@"temp"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageFileName]];
        competitorImageView.image= [UIImage imageWithContentsOfFile:imagePath];
    } else {
        competitorImageView.image= nil;
    }
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectedMerchandisingBrand.competitionInfoArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier= @"competitorCell";
    SalesWorxCompetitonInfoTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCompetitonInfoTableViewCell" owner:nil options:nil] firstObject];
    }
   // [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    tableView.allowsMultipleSelectionDuringEditing = NO;

    MerchandisingCompetitionInfo* competitionInfo=[[MerchandisingCompetitionInfo alloc]init];
    competitionInfo=[selectedMerchandisingBrand.competitionInfoArray objectAtIndex:indexPath.row];
    cell.brandLbl.text=[NSString getValidStringValue:competitionInfo.brandName];
    cell.productNameLbl.text=[NSString getValidStringValue:competitionInfo.productName];
    cell.priceLbl.text=[NSString getValidStringValue:competitionInfo.price];
    cell.promotionLbl.text=[NSString getValidStringValue:competitionInfo.activePromotion];
    cell.placementLbl.text=[NSString getValidStringValue:competitionInfo.shelfPlacement];
    cell.stockLbl.text=[NSString getValidStringValue:competitionInfo.stockLevel];
    cell.threatLevelLbl.text=[NSString getValidStringValue:competitionInfo.threatLevel];
    
    if (competitionInfo.imagesArray.count>0) {
        
        NSString* imageFileName = [competitionInfo.imagesArray objectAtIndex:0];
        NSString* imagePath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingCompetitorInfoImages] stringByAppendingPathComponent:@"temp"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageFileName]];

        cell.thumbnailImageView.image= [UIImage imageWithContentsOfFile:imagePath];
    } else {
        cell.thumbnailImageView.image= nil;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {


    currentCompetitorInfo=[selectedMerchandisingBrand.competitionInfoArray objectAtIndex:indexPath.row];
    if (currentCompetitorInfo.imagesArray.count>0) {
        NSString* imageName = [currentCompetitorInfo.imagesArray objectAtIndex:0];
        NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"Merchandising Competitor Pictures"] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageName]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
            NSError * deleteError;
         BOOL delete =  [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&deleteError];
            if (delete) {
                NSLog(@"file deleted successfully %@",imageName);
            }
        }
    }
    
    [selectedMerchandisingBrand.competitionInfoArray removeObjectAtIndex:indexPath.row];
    [competitorTableView reloadData];
    [self clearAllFields];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentCompetitorInfo=[selectedMerchandisingBrand.competitionInfoArray objectAtIndex:indexPath.row];
    currentCompetitorInfo.isUpdating=KAppControlsYESCode;
    [addButton setTitle:@"UPDATE" forState:UIControlStateNormal];
    [self updateCurrentData:currentCompetitorInfo];
}


#pragma mark TextView Delegate methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if([newString isEqualToString:@" "])
    {
        return NO;
    }
    NSString *expression =@"^[a-zA-Z0-9 ]+$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
    return (numberOfMatches != 0 && newString.length<=productNameCharacterLimit);
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
}

#pragma mark UITextField Delegate Methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([newString length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if([newString isEqualToString:@" "])
    {
        return NO;
    }
    if(textField==brandTextField)
    {
        NSString *expression =@"^[a-zA-Z0-9 ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=competitorInfoCharacterLimit);
    }
    else if(textField==productNameTextField)
    {
        NSString *expression =@"^[a-zA-Z0-9 ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=productNameCharacterLimit);
    }
    else if(textField==priceTextField)
    {
        NSString *expression =[NSString stringWithFormat:@"^[0-9]*((\\.|,)[0-9]{0,%0.0f})?$",4.0];
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=priceCharacterLimit);
    }
    else{
        return YES;
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==priceTextField) {
    }
   else if (textField==brandTextField) {
    }
    else if (textField==productNameTextField) {
    }
    [textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==brandTextField||textField==productNameTextField||textField==priceTextField) {
        return YES;
    }
    else if (textField==stockLevelTextField)
    {
        [self.view endEditing:YES];
        selectedTextField=kStockLevelTextField;
        [self presentPopoverfor:stockLevelTextField withTitle:kStockLevelTextField withContent:[stockLevelArray valueForKey:@"Code_Value"] ];
        return NO;
    }
    else if (textField==shelfPlacementTextField)
    {
        [self.view endEditing:YES];
        selectedTextField=kShelfPlacementTextField;
        [self presentPopoverfor:shelfPlacementTextField withTitle:selectedTextField withContent:[shelfPlacementArray valueForKey:@"Code_Value"]];
        return NO;
    }
    else if (textField==threatLevelTextField)
    {
        [self.view endEditing:YES];
        selectedTextField=kThreatLevelTextField;
        [self presentPopoverfor:threatLevelTextField withTitle:selectedTextField withContent:threatLevelArray];
        return NO;
    }
    else{
        return YES;
    }
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    competitorInfoPopOverController=nil;
    competitorInfoPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    competitorInfoPopOverController.delegate=self;
    popOverVC.popOverController=competitorInfoPopOverController;
    popOverVC.titleKey=popOverString;
    [competitorInfoPopOverController setPopoverContentSize:CGSizeMake(230, 200) animated:YES];
    [competitorInfoPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([selectedTextField isEqualToString:kStockLevelTextField]) {
        
        TBLAppCode * selectedCode = [stockLevelArray objectAtIndex:selectedIndexPath.row];
        
        //NSString* stockLevel=[NSString getValidStringValue:[stockLevelArray objectAtIndex:selectedIndexPath.row]];
        stockLevelTextField.text = [NSString getValidStringValue:selectedCode.Code_Value];
    }
    else if ([selectedTextField isEqualToString:kShelfPlacementTextField])
    {
        TBLAppCode * selectedCode = [shelfPlacementArray objectAtIndex:selectedIndexPath.row];

       // NSString* shelfPlacement =[NSString getValidStringValue:[shelfPlacementArray objectAtIndex:selectedIndexPath.row]];
        shelfPlacementTextField.text = [NSString getValidStringValue:selectedCode.Code_Value];
    }
    else if ([selectedTextField isEqualToString:kThreatLevelTextField])
    {
        NSString* threatlevel =[NSString getValidStringValue:[threatLevelArray objectAtIndex:selectedIndexPath.row]];
        threatLevelTextField.text = threatlevel;
    }
}


- (IBAction)activePromotionSwitchTapped:(id)sender {
    [self.view endEditing:YES];
    
}
- (IBAction)addButtonTapped:(id)sender {
    
    if (selectedMerchandisingBrand.competitionInfoArray.count>=5) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Maximum Limit Reached" andMessage:@"Only 5 competitors allowed per brand,if you wish to add more please delete from the existing data" withController:self];
        //[self clearAllFields];

    }
    else{
    [self.view endEditing:YES];
    
    if (![NSString isEmpty:notesTextView.text] || ![NSString isEmpty:priceTextField.text]||![NSString isEmpty:brandTextField.text]||![NSString isEmpty:productNameTextField.text]||![NSString isEmpty:stockLevelTextField.text] ||![NSString isEmpty:shelfPlacementTextField.text]||![NSString isEmpty:threatLevelTextField.text]) {
        
    currentCompetitorInfo.notes=[NSString getValidStringValue:notesTextView.text];
    currentCompetitorInfo.price=[NSString getValidStringValue:priceTextField.text];
    currentCompetitorInfo.brandName=[NSString getValidStringValue:brandTextField.text];
    currentCompetitorInfo.productName=[NSString getValidStringValue:productNameTextField.text];

    currentCompetitorInfo.stockLevel=[NSString getValidStringValue: stockLevelTextField.text];
    currentCompetitorInfo.shelfPlacement=[NSString getValidStringValue: shelfPlacementTextField.text];
    currentCompetitorInfo.threatLevel=[NSString getValidStringValue: threatLevelTextField.text];


    if ([activePromotionSwitch isOn]) {
        NSLog(@"Promotion Switch is On");
        currentCompetitorInfo.activePromotion=KAppControlsYESCode;
    }
    else{
        NSLog(@"Promotion Switch is Off");
        currentCompetitorInfo.activePromotion=KAppControlsNOCode;
    }
    if ([selectedMerchandisingBrand.competitionInfoArray containsObject:currentCompetitorInfo]) {
        
        NSInteger idx = [selectedMerchandisingBrand.competitionInfoArray indexOfObject:currentCompetitorInfo];
        
        [selectedMerchandisingBrand.competitionInfoArray replaceObjectAtIndex:idx withObject:currentCompetitorInfo];
    }
    else
    {
        [selectedMerchandisingBrand.competitionInfoArray addObject:currentCompetitorInfo];

    }
        
    [competitorTableView reloadData];
    [self clearAllFields];
    [addButton setTitle:@"ADD" forState:UIControlStateNormal];

    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter data" withController:self];
    }
    }
        
}

- (IBAction)clearButtonTapped:(id)sender {

    UIAlertAction* yesAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   if (currentCompetitorInfo.imagesArray.count>0 && [addButton.titleLabel.text isEqualToString:@"ADD"]) {
                                       NSString* imageName = [currentCompetitorInfo.imagesArray objectAtIndex:0];
                                       NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"Merchandising Competitor Pictures"] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageName]];
                                       
                                       if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
                                           NSError * deleteError;
                                           BOOL delete =  [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&deleteError];
                                           if (delete) {
                                               NSLog(@"file deleted successfully %@",imageName);
                                           }
                                       }
                                   }
                
                                   [self clearAllFields];
                               }];
    UIAlertAction* noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    NSMutableArray* actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction, nil];
    
    [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Clear Data?" andMessage:@"Would you like to clear all fields? unsaved data will be lost" andActions:actionsArray withController:self];
    
}

-(void)captureImage
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    picker.showsCameraControls = YES;
    picker.allowsEditing=NO;
    [self presentViewController:picker animated:YES
                     completion:nil];
}
- (IBAction)cameraButtonTapped:(id)sender {
    [self captureImage];
}

#pragma mark image delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    
    
    competitorImageView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    NSString* imageName=[NSString createGuid];
    
    [SWDefaults saveMerchandisingCompetitorImages:competitorImageView.image withImageName:imageName];
    
//    MerchandisingCompetitorPictures * currentPicture = [[MerchandisingCompetitorPictures alloc]init];
//    currentPicture.capturedImage=competitorImageView.image;
//    currentPicture.Image_File_Name=[NSString createGuid];
    
    
    if (currentCompetitorInfo.imagesArray.count>0) {
        NSString* imageName = [currentCompetitorInfo.imagesArray objectAtIndex:0];
        NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"Merchandising Competitor Pictures"] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageName]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
            NSError * deleteError;
            BOOL delete =  [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&deleteError];
            if (delete) {
                NSLog(@"file deleted successfully %@",imageName);
            }
        }
    }
    
    NSMutableArray* imagesArray=[[NSMutableArray alloc]initWithObjects:imageName, nil];
    currentCompetitorInfo.imagesArray=imagesArray;
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)clearAllFields
{
    brandTextField.text = @"";
    productNameTextField.text = @"";
    priceTextField.text = @"";
    threatLevelTextField.text = @"";
    stockLevelTextField.text = @"";
    shelfPlacementTextField.text = @"";
    notesTextView.text = @"";
    competitorImageView.image=nil;
    [activePromotionSwitch setOn:NO];
    
    currentCompetitorInfo = [[MerchandisingCompetitionInfo alloc]init];
    [addButton setTitle:@"ADD" forState:UIControlStateNormal];

}
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
////    if ([otherGestureRecognizer.view isKindOfClass:[UITableView class]]) {
////        return YES;
////    }
//    return YES;
//}

// Allows inner UITableView swipe-to-delete gesture
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(nonnull UIGestureRecognizer *)otherGestureRecognizer
//{
//    return [otherGestureRecognizer.view.superview isKindOfClass:[UITableView class]];
//}

@end
