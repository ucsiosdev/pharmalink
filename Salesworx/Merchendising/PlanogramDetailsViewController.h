//
//  PlanogramDetailsViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/5/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDefaultSegmentedControl.h"
#import "SWDefaults.h"


@protocol PlanogramDetailsDelegate
-(void)didSavePlanogramDetails:(MerchandisingPlanogram*)savedPlanogram;
-(void)didCancelPlanogramMatching;

@end

@interface PlanogramDetailsViewController : UIViewController
{
    id planogramDetailsDelegate;
}
@property(nonatomic) id planogramDetailsDelegate;
@property (strong, nonatomic) IBOutlet UIImageView *sourceImageView;
@property (strong, nonatomic) IBOutlet UIImageView *destinationImageView;
@property (strong, nonatomic) IBOutlet SalesWorxDefaultSegmentedControl *matchedSegment;
- (IBAction)saveButtontapped:(id)sender;
@property(nonatomic) MerchandisingPlanogram * selectedPlanogram;
- (IBAction)cancelButtonTapped:(id)sender;

@end
