//
//  SalesWorxMerchandisingSurveyPicturesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingSurveyPicturesViewController.h"
#import "SalesWorxSalesOrderLPOImagesCollectionViewCell.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxMerchandisingSurveyPicturesZoomViewController.h"

#define KMaximumAllowedSurveyPictures 5
#define KMaximumAllowedMarkOnSurveyPictures 5

@interface SalesWorxMerchandisingSurveyPicturesViewController ()

@end

@implementation SalesWorxMerchandisingSurveyPicturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    markCount = 1;
    [markCollectionView registerClass:[SalesWorxSalesOrderLPOImagesCollectionViewCell class] forCellWithReuseIdentifier:@"LPOImageCell"];
    
    singleTapOnImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapOnImage:)];
    singleTapOnImageView.numberOfTapsRequired = 1;
    [surveyPictureImageView addGestureRecognizer:singleTapOnImageView];
}

-(void)showSurveyPicturesforBrand:(SalesWorxMerchandisingBrand*)merchandisingObject
{
    selectedMerchandisingBrand = merchandisingObject;
    if (selectedMerchandisingBrand.surveyPicturesArray.count == 0) {
        selectedMerchandisingBrand.surveyPicturesArray = [[NSMutableArray alloc]init];
    }
    [self reloadSurveyPicturesCollectionView];
    
    productArray = merchandisingObject.productArray;
}

#pragma mark Image marking
- (void)handleSingleTapOnImage:(UITapGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:sender.view];
    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    if ([viewTouched isKindOfClass:[UIImageView class]]) {
        // respond to touch action
        
        if (selectedsurveyPicture.imageMarkArray.count == KMaximumAllowedMarkOnSurveyPictures) {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Mark limit exceed" andMessage:@"You can add 5 mark only" withController:self];
        } else {
            UIColor * ContrastColor = [SWDefaults getContrastColorForImage:surveyPictureImageView.image];

            CGRect imageFrame = CGRectMake(point.x, point.y, 200, 200);
            SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:imageFrame color:ContrastColor];
            imageResizableView.backgroundColor = [UIColor clearColor];
            [imageResizableView showEditingHandles];
            imageResizableView.tag = markCount;
            imageResizableView.delegate = self;
            [surveyPictureImageView addSubview:imageResizableView];
            [imageResizableView resizeUsingTouchLocation:point];
            
            UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapOnImage:)];
            doubleTap.numberOfTapsRequired = 2;
            [imageResizableView addGestureRecognizer:doubleTap];
            [singleTapOnImageView requireGestureRecognizerToFail:doubleTap];
            
            MerchandisingSurveyPicturesMark *objMark = [[MerchandisingSurveyPicturesMark alloc]init];
            objMark.markId = [NSString stringWithFormat:@"%d",markCount];
            [selectedsurveyPicture.imageMarkArray addObject:objMark];
            
            markCount++;
            [self printMarkFrames];
        }
    }
}

- (void)handleDoubleTapOnImage:(UITapGestureRecognizer *)sender
{
    SPUserResizableView *markView = (SPUserResizableView *)[surveyPictureImageView viewWithTag:sender.view.tag];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"markId = %@",[NSString stringWithFormat:@"%ld",(long)markView.tag]];
    NSMutableArray *tempMarksArray = [selectedsurveyPicture.imageMarkArray mutableCopy];
    [tempMarksArray filterUsingPredicate:predicate];
    
    if (tempMarksArray.count > 0) {
        MerchandisingSurveyPicturesMark *selectedSurveyPicturesMark = [tempMarksArray objectAtIndex:0];
        
        SalesWorxMerchandisingSurveyPictureMarkDetailsViewController *markVC = [[SalesWorxMerchandisingSurveyPictureMarkDetailsViewController alloc]init];
        markVC.markVCDelegate=self;
        markVC.markID = selectedSurveyPicturesMark.markId;
        markVC.markDescription = selectedSurveyPicturesMark.markDescription;
        
        UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:markVC];
        mapPopUpViewController.navigationBarHidden = YES;
        mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
        markVC.view.backgroundColor = [UIColor clearColor];
        markVC.modalPresentationStyle = UIModalPresentationCustom;
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:mapPopUpViewController animated:NO completion:nil];
    }
}

- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView {
    currentlyEditingView = userResizableView;
}

- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView {
    lastEditedView = userResizableView;
    [self printMarkFrames];
}

-(void)printMarkFrames{
    for (id view in surveyPictureImageView.subviews){
        if([view isKindOfClass:[SPUserResizableView class]]){

            SPUserResizableView *markView = view;
            
            NSPredicate *predicate=[NSPredicate predicateWithFormat:@"markId = %@",[NSString stringWithFormat:@"%ld",(long)markView.tag]];
            NSMutableArray *tempMarksArray = [selectedsurveyPicture.imageMarkArray mutableCopy];
            [tempMarksArray filterUsingPredicate:predicate];

            if (tempMarksArray.count > 0) {
                MerchandisingSurveyPicturesMark *selectedSurveyPicturesMark = [tempMarksArray objectAtIndex:0];
                
                selectedSurveyPicturesMark.markViewframe = markView.frame;
                selectedSurveyPicturesMark.markViewframeWithRespectToImageView = [self convertFrameFromUIimageViewFramePixelToUIimageViewImagePixel:markView.frame];
            }
            NSLog(@"markView frame %@",NSStringFromCGRect(markView.frame));
            NSLog(@"markView frame related to image pixels %@",NSStringFromCGRect([self convertFrameFromUIimageViewFramePixelToUIimageViewImagePixel:markView.frame]));
        }
    }
}

-(CGRect )convertFrameFromUIimageViewFramePixelToUIimageViewImagePixel:(CGRect)markRect
{
    float widthRatio= surveyPictureImageView.frame.size.width /surveyPictureImageView.image.size.width;
    float heightRatio=  surveyPictureImageView.frame.size.height /surveyPictureImageView.image.size.height;

    CGPoint rectPointInImagePixels = CGPointMake(markRect.origin.x/widthRatio,markRect.origin.y/heightRatio);
    CGSize rectSizeInImagePixels = CGSizeMake(markRect.size.width/widthRatio,markRect.size.height/heightRatio);
    
    return CGRectMake(rectPointInImagePixels.x, rectPointInImagePixels.y, rectSizeInImagePixels.width, rectSizeInImagePixels.height);
}

#pragma mark Mark detail delegate
-(void)saveMarkDescription:(NSString *)markID WithDesc:(NSString *)markDescription {
    
    for (MerchandisingSurveyPicturesMark *markObj in selectedsurveyPicture.imageMarkArray) {
        if([markObj.markId isEqualToString:markID])
        {
            markObj.markDescription = markDescription;
        }
    }
}

-(void)deleteMark:(NSString *)markID {
    
    for (MerchandisingSurveyPicturesMark *markObj in selectedsurveyPicture.imageMarkArray) {
        if([markObj.markId isEqualToString:markID])
        {
            [selectedsurveyPicture.imageMarkArray removeObject:markObj];
            break;
        }
    }
    SPUserResizableView *markObj = (SPUserResizableView *)[surveyPictureImageView viewWithTag:[markID intValue]];
    [markObj removeFromSuperview];
}

#pragma mark button action
- (IBAction)cameraButtonTapped:(id)sender {
    
    if (selectedMerchandisingBrand.surveyPicturesArray.count == KMaximumAllowedSurveyPictures) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Survey picture limit exceed" andMessage:@"You can take 5 pictures only" withController:self];
    } else {
        
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if(authStatus == AVAuthorizationStatusAuthorized)
            {
                [self openCamera];
            }
            else if(authStatus == AVAuthorizationStatusNotDetermined)
            {
                NSLog(@"%@", @"Camera access not determined. Ask for permission.");
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
                 {
                     if(granted)
                     {
                         NSLog(@"Granted access to %@", AVMediaTypeVideo);
                         [self openCamera];
                     }
                     else
                     {
                         NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                         [self cameraDenied];
                     }
                 }];
            }
            else if (authStatus == AVAuthorizationStatusRestricted)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"You've been restricted from using the camera on this device." withController:self];
            }
            else if(authStatus == AVAuthorizationStatusDenied) {
                [self cameraDenied];
            }
            else
            {
                NSLog(@"%@", @"Camera access unknown error.");
            }
        } else {
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Device has no camera" withController:self];
        }
    }
}

-(void)openCamera {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    picker.showsCameraControls = YES;
    picker.allowsEditing=NO;
    [self presentViewController:picker animated:YES completion:nil];
}
-(void)cameraDenied
{
    // denied
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Select Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
        
        alertButton = @"Go";
    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:alertButton
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   if (canOpenSettings)
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Error" andMessage:alertText andActions:actionsArray withController:[SWDefaults currentTopViewController]];
}

- (IBAction)deleteImageButtonTapped:(id)sender {
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(kYesTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        [selectedMerchandisingBrand.surveyPicturesArray removeObject:selectedsurveyPicture];
        [self reloadSurveyPicturesCollectionView];
    }];
    
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:NSLocalizedString(kNoTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    NSMutableArray* actionArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction, nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Delete Image" andMessage:@"Would you like to delete the Image?" andActions:actionArray withController:self];
}

- (IBAction)zoomButtonTapped:(id)sender {
    
    SalesWorxMerchandisingSurveyPicturesZoomViewController *zoomVC = [[SalesWorxMerchandisingSurveyPicturesZoomViewController alloc]init];
    zoomVC.zoomVCDelegate = self;
    zoomVC.selectedSurveyPicture = selectedsurveyPicture;
    zoomVC.markCount = markCount;
    zoomVC.smallImageViewFrame = surveyPictureImageView.frame;
    [self presentViewController:zoomVC animated:YES completion:nil];
}

#pragma mark zoom delegate

-(void)refreshSurveyPicture:(MerchandisingSurveyPictures *)surveyPicture withMarkCount:(int)Count {
    
    markCount = Count;
    selectedsurveyPicture = surveyPicture;
    for (MerchandisingSurveyPicturesMark *currentMarkObj in selectedsurveyPicture.imageMarkArray) {
        
        CGRect markFrame = [self convertFrameFromUIimageViewImagePixelToUIimageViewFramePixel:currentMarkObj.markViewframeWithRespectToImageView];
        currentMarkObj.markViewframe = markFrame;
    }
    
    [self showMarkFrameOfSelectedImage];
}

-(CGRect )convertFrameFromUIimageViewImagePixelToUIimageViewFramePixel:(CGRect)markRect
{
    float widthRatio= surveyPictureImageView.image.size.width /surveyPictureImageView.frame.size.width;
    float heightRatio=  surveyPictureImageView.image.size.height /surveyPictureImageView.frame.size.height;
    
    CGPoint rectPointInImagePixels = CGPointMake(markRect.origin.x/widthRatio,markRect.origin.y/heightRatio);
    CGSize rectSizeInImagePixels = CGSizeMake(markRect.size.width/widthRatio,markRect.size.height/heightRatio);
    
    return CGRectMake(rectPointInImagePixels.x, rectPointInImagePixels.y, rectSizeInImagePixels.width, rectSizeInImagePixels.height);
}

#pragma mark UICollectionview Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return selectedMerchandisingBrand.surveyPicturesArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(150, 150);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LPOImageCell";
    
    SalesWorxSalesOrderLPOImagesCollectionViewCell *cell = (SalesWorxSalesOrderLPOImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.lpoImageview.layer.sublayers=nil;
    
    MerchandisingSurveyPictures *currentsurveyPicture = [selectedMerchandisingBrand.surveyPicturesArray objectAtIndex:indexPath.row];
    cell.lpoImageview.image = [UIImage imageWithContentsOfFile:[[SWDefaults getMerchandisingSurveyMarkingPicturesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",currentsurveyPicture.imageName]]];
    
    // Initialization code
    UIGraphicsBeginImageContextWithOptions(cell.lpoImageview.bounds.size, NO, 1.0);
    UIBezierPath *mybezierpath = [UIBezierPath bezierPathWithRoundedRect:cell.lpoImageview.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(10.0, 10.0)];
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [mybezierpath addClip];
    // Draw your image
    [cell.lpoImageview.image drawInRect:cell.lpoImageview.bounds];
    
    // Get the image, here setting the UIImageView image
    cell.lpoImageview.image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    CAShapeLayer *circle = [CAShapeLayer layer];
    circle.path = mybezierpath.CGPath;
    circle.bounds = CGPathGetBoundingBox(circle.path);
    circle.strokeColor = [UIColor colorWithRed:(54.0/255.0) green:(193.0/255.0) blue:(195.0/255.0) alpha:1].CGColor;
    circle.cornerRadius = 10;
    circle.fillColor = [UIColor clearColor].CGColor; /*if you just want lines*/
    circle.lineWidth = 3;
    circle.position = CGPointMake(cell.lpoImageview.frame.size.width/2.0, cell.lpoImageview.frame.size.height/2.0);
    circle.anchorPoint = CGPointMake(.5, .5);
    circle.masksToBounds = YES;
    
    [cell.lpoImageview.layer addSublayer:circle];
    
    if (selectedIndexPath==indexPath) {
        cell.selectedImgView.hidden=NO;
    }
    else
    {
        cell.selectedImgView.hidden=YES;
    }

    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];

    selectedIndexPath = indexPath;
    [markCollectionView reloadData];
    
    selectedsurveyPicture = [selectedMerchandisingBrand.surveyPicturesArray objectAtIndex:indexPath.row];
    descTextView.text = [NSString getValidStringValue:selectedsurveyPicture.imageDesc];
    productTextField.text = [NSString getValidStringValue:selectedsurveyPicture.productName];
    [surveyPictureImageView setImage:[UIImage imageWithContentsOfFile:[[SWDefaults getMerchandisingSurveyMarkingPicturesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",selectedsurveyPicture.imageName]]]];
    
    [self showMarkFrameOfSelectedImage];
}

-(void)showMarkFrameOfSelectedImage {
    
    NSLog(@"%lu", (unsigned long)[surveyPictureImageView subviews].count);
    [[surveyPictureImageView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSLog(@"%lu", (unsigned long)[surveyPictureImageView subviews].count);
    
    UIColor * ContrastColor = [SWDefaults getContrastColorForImage:surveyPictureImageView.image];
    
    for (MerchandisingSurveyPicturesMark *currentMarkObj in selectedsurveyPicture.imageMarkArray) {
        SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:currentMarkObj.markViewframe color:ContrastColor];
        imageResizableView.backgroundColor = [UIColor clearColor];
        [imageResizableView showEditingHandles];
        imageResizableView.tag = [currentMarkObj.markId intValue];
        imageResizableView.delegate = self;
        [surveyPictureImageView addSubview:imageResizableView];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapOnImage:)];
        doubleTap.numberOfTapsRequired = 2;
        [imageResizableView addGestureRecognizer:doubleTap];
        [singleTapOnImageView requireGestureRecognizerToFail:doubleTap];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    if(picker.cameraCaptureMode==UIImagePickerControllerCameraCaptureModePhoto)
    {
        UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
        NSString *imageGUID = [NSString createGuid];
        [SWDefaults saveMerchandisingSurveyMarkingPictures:image withName:imageGUID];
        
        MerchandisingSurveyPictures *objSurveyPictures = [[MerchandisingSurveyPictures alloc]init];
        objSurveyPictures.imageName = imageGUID;
        objSurveyPictures.imageMarkArray = [[NSMutableArray alloc]init];
        [selectedMerchandisingBrand.surveyPicturesArray addObject:objSurveyPictures];
        
        [picker dismissViewControllerAnimated:NO completion:^{
            [self reloadSurveyPicturesCollectionView];
        }];//Or call YES if you want the nice dismissal animation
    }
}

-(void)updateImagesArray:(NSMutableArray*)imagesArray
{
    NSLog(@"imagesArray after Delete%@",imagesArray);
    NSMutableArray *tempLPOImagesArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<imagesArray.count; i++) {
        [tempLPOImagesArray addObject:[[[imagesArray objectAtIndex:i]lastPathComponent]stringByDeletingPathExtension]];
    }
    
    selectedMerchandisingBrand.surveyPicturesArray = tempLPOImagesArray;
    [self reloadSurveyPicturesCollectionView];
}

-(void)reloadSurveyPicturesCollectionView {
    [markCollectionView reloadData];
    if (selectedMerchandisingBrand.surveyPicturesArray.count>0) {
        placeholderView.hidden=YES;
        descTextView.text = [NSString getValidStringValue:selectedsurveyPicture.imageDesc];
        productTextField.text = [NSString getValidStringValue:selectedsurveyPicture.productName];
        
        [markCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        [markCollectionView.delegate collectionView:markCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    else{
        placeholderView.hidden=NO;
    }
}

#pragma mark TextView Delegate methods

- (void)textViewDidEndEditing:(UITextView *)textView
{
    selectedsurveyPicture.imageDesc = textView.text;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if([newString isEqualToString:@" "] || [newString isEqualToString:@"\n"])
    {
        return NO;
    }
    
    
    NSString *expression = @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.,\n";
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:expression] invertedSet];
    
    NSString *filtered = [[newString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return ([newString isEqualToString:filtered] && newString.length<=SurveyPictureCommentTextViewLimit);
}

#pragma mark UITextField Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    if (productArray.count == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"No Product found associated with brand" withController:self];
    } else {
        [self presentPopoverfor:productTextField withTitle:@"Product" withContent:[productArray valueForKey:@"Product_Name"]];
    }
    return NO;
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    UIPopoverController *competitorInfoPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    competitorInfoPopOverController.delegate=self;
    popOverVC.popOverController=competitorInfoPopOverController;
    popOverVC.titleKey=popOverString;
    [competitorInfoPopOverController setPopoverContentSize:CGSizeMake(300, 350) animated:YES];
    [competitorInfoPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSString *productName = [NSString getValidStringValue:[[productArray objectAtIndex:selectedIndexPath.row]valueForKey:@"Product_Name"]];
    productTextField.text = productName;
    selectedsurveyPicture.productName = productName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
