//
//  SalesWorxMerchandisingPOSViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingPOSViewController.h"
#import "SalesWorxMerchandisingPOSTableViewCell.h"

@interface SalesWorxMerchandisingPOSViewController ()

@end

@implementation SalesWorxMerchandisingPOSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [posImplementedSwitch setOn:NO];
    [promotionSwitch setOn:NO];
    [productPositionSwitch setOn:NO];

    [promotionCollectionView registerClass:[PosPromotionCollectionViewCell class] forCellWithReuseIdentifier:@"promotionCell"];

    [posCollectionView registerClass:[PosCollectionViewCell class] forCellWithReuseIdentifier:@"posCell"];

    
    
//    selectedPOSMaterialArray=[[NSMutableArray alloc]init];
//    selectedPromotionMaterialArray=[[NSMutableArray alloc]init];
//    selectedPositionDetailsArray=[[NSMutableArray alloc]init];
    
}

-(void)viewWillAppear:(BOOL)animated{

}

- (IBAction)posImplementedSwitchStatusChanged:(id)sender {
    
    if ([posImplementedSwitch isOn]) {
        currentPOS.posMaterialSelected=KAppControlsYESCode;
    }
    else{
        currentPOS.posMaterialSelected=KAppControlsNOCode;
        [currentPOS.posMaterialArray setValue:KAppControlsNOCode forKey:@"isSelected"];
        [posCollectionView reloadData];
    }
}

- (IBAction)promotionSwitchStatusChanged:(id)sender {
    
    if ([promotionSwitch isOn]) {
        currentPOS.promotionMaterialSelected=KAppControlsYESCode;
    }
    else{
        currentPOS.promotionMaterialSelected=KAppControlsNOCode;
        [currentPOS.promotionMaterialArray setValue:KAppControlsNOCode forKey:@"isSelected"];
        [promotionCollectionView reloadData];

    }
}

- (IBAction)productPositionStatusChanged:(id)sender {
    
    if ([productPositionSwitch isOn]) {
        currentPOS.positionDetailsSelected=KAppControlsYESCode;
    }
    else{
        currentPOS.positionDetailsSelected=KAppControlsNOCode;
        [currentPOS.positionDetailsArray setValue:KAppControlsNOCode forKey:@"isSelected"];
        [tblProductPosition reloadData];

    }
}

-(void)showPosDataForBrand:(SalesWorxMerchandisingBrand*)currentMerchandisingBrand
{
    
    currentPOS=currentMerchandisingBrand.pos;
  
    if ([currentPOS.posMaterialSelected isEqualToString:KAppControlsYESCode]) {
        [posImplementedSwitch setOn:YES];
    }
    else{
        [posImplementedSwitch setOn:NO];
    }
    
    if ([currentPOS.promotionMaterialSelected isEqualToString:KAppControlsYESCode]) {
        [promotionSwitch setOn:YES];
    }
    else{
        [promotionSwitch setOn:NO];
    }
    
    if ([currentPOS.positionDetailsSelected isEqualToString:KAppControlsYESCode]) {
        [productPositionSwitch setOn:YES];
    }
    else{
        [productPositionSwitch setOn:NO];
    }
    
    [tblPOSMaterial reloadData];
    [tblPOSMaterial scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    [tblPromotionMaterial reloadData];
    [tblPromotionMaterial scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];

    [tblProductPosition reloadData];
    [tblProductPosition scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];

    [promotionCollectionView reloadData];
    [posCollectionView reloadData];
    
    commentsTextView.text = [NSString getValidStringValue:currentPOS.comments];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self updateTextViewData];
}

-(void)updateTextViewData
{
    if (![NSString isEmpty:commentsTextView.text]) {
        currentPOS.comments=[NSString getValidStringValue:commentsTextView.text];
    }
}

#pragma mark UITableview data source methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tblPOSMaterial) {
        return currentPOS.posMaterialArray.count;
    } else if (tableView == tblPromotionMaterial) {
        return currentPOS.promotionMaterialArray.count;
    } else {
        return currentPOS.positionDetailsArray.count;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *identifier = @"POSIdentifier";
    SalesWorxMerchandisingPOSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxMerchandisingPOSTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    cell.isHeader=YES;
    cell.lblProductName.isHeader=YES;
    cell.selectionImageView.hidden = YES;
    
    if (tableView == tblPOSMaterial) {
        cell.lblProductName.text = NSLocalizedString(@"POS Material", nil);
    } else if (tableView == tblPromotionMaterial) {
        cell.lblProductName.text = NSLocalizedString(@"Promotion Material", nil);
    } else {
        cell.lblProductName.text = NSLocalizedString(@"Position Details", nil);
    }

    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"POSIdentifier";
    SalesWorxMerchandisingPOSTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxMerchandisingPOSTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    if (tableView == tblPOSMaterial) {
        MerchandisingPOSMaterial *currentMaterial = [currentPOS.posMaterialArray objectAtIndex:indexPath.row];
        cell.lblProductName.text = currentMaterial.title;
        cell.selectionImageView.image =[UIImage imageNamed:[currentMaterial.isSelected isEqualToString:KAppControlsYESCode]?kSelectedRadioButtonImage:kUnSelectRadioButtonImage];
    } else if (tableView == tblPromotionMaterial) {
        MerchandisingPromotionMaterial *currentMaterial = [currentPOS.promotionMaterialArray objectAtIndex:indexPath.row];
        cell.lblProductName.text = currentMaterial.title;
        cell.selectionImageView.image =[UIImage imageNamed:[currentMaterial.isSelected isEqualToString:KAppControlsYESCode]?kSelectedRadioButtonImage:kUnSelectRadioButtonImage];
    } else {
        MerchandisingPositionDetails *currentMaterial = [currentPOS.positionDetailsArray objectAtIndex:indexPath.row];
        cell.lblProductName.text = currentMaterial.title;
        cell.selectionImageView.image =[UIImage imageNamed:[currentMaterial.isSelected isEqualToString:KAppControlsYESCode]?kSelectedRadioButtonImage:kUnSelectRadioButtonImage];

    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==tblPOSMaterial) {
        
        if ([posImplementedSwitch isOn]) {
            currentPOS.isUpdated=KAppControlsYESCode;
            currentPOS.posMaterialSelected=KAppControlsYESCode;
            MerchandisingPOSMaterial *currentMaterial = [currentPOS.posMaterialArray objectAtIndex:indexPath.row];
            

            if ([currentMaterial.isSelected isEqualToString:KAppControlsNOCode]) {
             currentMaterial.isSelected=KAppControlsYESCode;
            }
            else if ([currentMaterial.isSelected isEqualToString:KAppControlsYESCode])
            {
                currentMaterial.isSelected=KAppControlsNOCode;
            }
            [tblPOSMaterial reloadData];
        }
//        if (![selectedPOSMaterialArray containsObject:indexPath]) {
//            [selectedPOSMaterialArray addObject:indexPath];
//        }
//        [tblPOSMaterial reloadData];
//        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Please tap on POS Implemented, before making a selection" withController:self];
        }
    }
   else if (tableView==tblPromotionMaterial) {
       if ([promotionSwitch isOn]) {
           MerchandisingPromotionMaterial *currentMaterial = [currentPOS.promotionMaterialArray objectAtIndex:indexPath.row];
           currentPOS.promotionMaterialSelected=KAppControlsYESCode;

           currentPOS.isUpdated=KAppControlsYESCode;
           if ([currentMaterial.isSelected isEqualToString:KAppControlsNOCode]) {
               currentMaterial.isSelected=KAppControlsYESCode;
           }
           else if ([currentMaterial.isSelected isEqualToString:KAppControlsYESCode])
           {
               currentMaterial.isSelected=KAppControlsNOCode;
           }
//        if (![selectedPromotionMaterialArray containsObject:indexPath]) {
//            [selectedPromotionMaterialArray addObject:indexPath];
//        }
       [tblPromotionMaterial reloadData];
       }
       else{
           [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Please tap on Promotion, before making a selection" withController:self];
       }
    }
    else if (tableView==tblProductPosition) {
        if ([productPositionSwitch isOn]) {
            currentPOS.isUpdated=KAppControlsYESCode;
            currentPOS.positionDetailsSelected=KAppControlsYESCode;

        //single selection
            MerchandisingPositionDetails *currentMaterial = [currentPOS.positionDetailsArray objectAtIndex:indexPath.row];
            
            [currentPOS.positionDetailsArray setValue:KAppControlsNOCode forKey:@"isSelected"];
            
            currentPOS.isUpdated=KAppControlsYESCode;
            if ([currentMaterial.isSelected isEqualToString:KAppControlsNOCode]) {
                currentMaterial.isSelected=KAppControlsYESCode;
            }
            
//            selectedPositionDetailsArray=[[NSMutableArray alloc]init];
//            [selectedPositionDetailsArray addObject:indexPath];
        [tblProductPosition reloadData];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Please tap on Product Position, before making a selection" withController:self];
 
        }

    }
    
    /*
    SalesWorxMerchandisingPOSTableViewCell *cell = (SalesWorxMerchandisingPOSTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.selectionImageView.image isEqual:[UIImage imageNamed:@"Sync_ GreenTickMark"]]) {
        cell.selectionImageView.image = [UIImage imageNamed:@"Sync_GreenUnTickMark"];
    } else {
        cell.selectionImageView.image = [UIImage imageNamed:@"Sync_ GreenTickMark"];
    }*/
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionView Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==promotionCollectionView) {
        return currentPOS.promotionMaterialArray.count;
    }
    else if (collectionView==posCollectionView)
    {
        return currentPOS.posMaterialArray.count;
    }
    else{
        return 0;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
     if (collectionView==posCollectionView)
     {
         return CGSizeMake(160, 162);
     }
    else
    {
        CGSize cellSize = CGSizeMake(collectionView.frame.size.width/4, 50);
    return cellSize;
    }
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==promotionCollectionView) {
        PosPromotionCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"promotionCell" forIndexPath:indexPath];
        MerchandisingPromotionMaterial *currentMaterial = [currentPOS.promotionMaterialArray objectAtIndex:indexPath.row];
        cell.titleLbl.text = currentMaterial.title;
        cell.tickImageView.image =[UIImage imageNamed:[currentMaterial.isSelected isEqualToString:KAppControlsYESCode]?kSelectedRadioButtonImage:kUnSelectRadioButtonImage];
        return cell;
    }
    else if (collectionView==posCollectionView)
    {
        PosCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"posCell" forIndexPath:indexPath];
        MerchandisingPOSMaterial *currentMaterial = [currentPOS.posMaterialArray objectAtIndex:indexPath.row];
        cell.captionLbl.text = currentMaterial.title;
        cell.selectedImageView.image =[UIImage imageNamed:[currentMaterial.isSelected isEqualToString:KAppControlsYESCode]?kSelectedRadioButtonImage:@""];
        cell.productImageView.image= [UIImage imageWithContentsOfFile:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:currentMaterial.imageName]];
        return cell;
    }
    else{
        return nil;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==promotionCollectionView) {
        if ([promotionSwitch isOn]) {
            MerchandisingPromotionMaterial *currentMaterial = [currentPOS.promotionMaterialArray objectAtIndex:indexPath.row];
            currentPOS.promotionMaterialSelected=KAppControlsYESCode;
            
            currentPOS.isUpdated=KAppControlsYESCode;
            if ([currentMaterial.isSelected isEqualToString:KAppControlsNOCode]) {
                currentMaterial.isSelected=KAppControlsYESCode;
            }
            else if ([currentMaterial.isSelected isEqualToString:KAppControlsYESCode])
            {
                currentMaterial.isSelected=KAppControlsNOCode;
            }
            [promotionCollectionView reloadData];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Please tap on Promotion, before making a selection" withController:self];
        }
    }
    else if (collectionView==posCollectionView)
    {
        if ([posImplementedSwitch isOn]) {
            currentPOS.isUpdated=KAppControlsYESCode;
            currentPOS.posMaterialSelected=KAppControlsYESCode;
            MerchandisingPOSMaterial *currentMaterial = [currentPOS.posMaterialArray objectAtIndex:indexPath.row];
            if ([currentMaterial.isSelected isEqualToString:KAppControlsNOCode]) {
                currentMaterial.isSelected=KAppControlsYESCode;
            }
            else if ([currentMaterial.isSelected isEqualToString:KAppControlsYESCode])
            {
                currentMaterial.isSelected=KAppControlsNOCode;
            }
            [posCollectionView reloadData];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Please tap on POS Implemented, before making a selection" withController:self];
        }
    }
}


@end
