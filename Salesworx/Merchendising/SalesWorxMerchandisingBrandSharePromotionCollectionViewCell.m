//
//  SalesWorxMerchandisingBrandSharePromotionCollectionViewCell.m
//  MedRep
//
//  Created by USHYAKU-IOS on 11/30/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingBrandSharePromotionCollectionViewCell.h"

@implementation SalesWorxMerchandisingBrandSharePromotionCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxMerchandisingBrandSharePromotionCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
}

@end
