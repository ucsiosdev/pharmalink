//
//  SalesWorxMerchendisingHomeViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/19/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingHomeViewController.h"

@interface SalesWorxMerchandisingHomeViewController ()

@end

@implementation SalesWorxMerchandisingHomeViewController

@synthesize currentVisit,customerNameLabel,customerCodeLabel,availableBalanceLabel,customerStatusImageView,visitTypeLabel,merchandisingTitlesSegment,merchandisingScrollView,lblLastVisitedOn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
     appDel = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];

    self.view.backgroundColor=UIViewControllerBackGroundColor;
    selectedIndexPathsArray=[[NSMutableArray alloc]init];
    errorAlertDisplayed=NO;

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Merchandising"];
    
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=closeButton;
    
    //[[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    
    UIBarButtonItem* saveButton=[[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    [saveButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=saveButton;
    

    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];

    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
    {
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self)
        {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
        }
    }
    
    [self fetchBrandsData];
    
    [self showCustomerDetails];
    
        merchandisingTitlesSegment.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
        merchandisingTitlesSegment.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
        merchandisingTitlesSegment.sectionTitles = @[NSLocalizedString(@"Availability", nil),NSLocalizedString(@"Brand Share", nil), NSLocalizedString(@"Planogram", nil),NSLocalizedString(@"POSM", nil),NSLocalizedString(@"Competition Info", nil),NSLocalizedString(@"In-Store Activity", nil),NSLocalizedString(@"Survey Pictures", nil),NSLocalizedString(@"Comments", nil)];
        
        merchandisingTitlesSegment.selectionIndicatorBoxOpacity=0.0f;
        merchandisingTitlesSegment.verticalDividerEnabled=YES;
        merchandisingTitlesSegment.verticalDividerWidth=1.0f;
        merchandisingTitlesSegment.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
        
        merchandisingTitlesSegment.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
        merchandisingTitlesSegment.selectionStyle = HMSegmentedControlSelectionStyleBox;
        merchandisingTitlesSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        merchandisingTitlesSegment.tag = 3;
        merchandisingTitlesSegment.selectedSegmentIndex = 0;
        
        merchandisingTitlesSegment.layer.shadowColor = [UIColor blackColor].CGColor;
        merchandisingTitlesSegment.layer.shadowOffset = CGSizeMake(2, 2);
        merchandisingTitlesSegment.layer.shadowOpacity = 0.1;
        merchandisingTitlesSegment.layer.shadowRadius = 1.0;
        
        [merchandisingTitlesSegment addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
        
        __weak typeof(self) weakSelf = self;
        [merchandisingTitlesSegment addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
        
        [merchandisingTitlesSegment setIndexChangeBlock:^(NSInteger index) {
            [weakSelf.merchandisingScrollView setContentOffset:CGPointMake(merchandisingScrollView.frame.size.width * index, 0)];
            // [weakSelf UpdateCustomerDetailsWithAccount:customerDetailsChildViewController.SelectedAccount AndBillToSite:customerDetailsChildViewController.SelectedBillToSiteDetails InSection:index];
        }];
    
    
    [self addVisitDetailsToTheDatabaseIfNotAdded];
   
}
-(void)addVisitDetailsToTheDatabaseIfNotAdded{
    
    if (!currentVisit.Visit_ID) {
        currentVisit.Visit_ID=[NSString createGuid];
        [SWDefaults setCurrentVisitID:currentVisit.Visit_ID];
        NSLog(@"current visit id in sales order is %@", [SWDefaults currentVisitID]);
    }
    if (insertFSRVisitData==NO) { /** inserting start viist if its  not already added to database*/
        insertFSRVisitData=  [[SWDatabaseManager retrieveManager]saveVisitDetails:currentVisit];
    }
}
-(void)validateLocationCoOrdinateforOnSiteVisit
{
    NSString *customerLatitude=[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Cust_Lat];
    NSString *customerLongitude=[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Cust_Long];
    
    //check if customer location is available
    
    NSString *fsrLatitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude];
    NSString *fsrLongitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
    
    NSLog(@"CLOSE_VISIT_LOC_UPDATE_LIMIT is %@",appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT);
    
    //fetch range from tbl_addl_info
    double distanceLimit = [appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT doubleValue];
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[customerLatitude doubleValue] longitude:[customerLongitude doubleValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[fsrLatitude doubleValue] longitude:[fsrLongitude doubleValue]];
    CLLocationDistance distance = [location1 distanceFromLocation:location2];
    
    NSLog(@"Distance between Locations in meters: %f", distance);
    
    if (distance > distanceLimit)
    {
        //just update the current coordinates dont chnage customer coordinates, doing so, if we again start the onsite visit for same customer current coordinates and fsr coordinates so validatation fails
        
        //        salesWorxVisit.visitOptions.customer.Cust_Lat = fsrLatitude;
        //        salesWorxVisit.visitOptions.customer.Cust_Long = fsrLongitude;
        
        [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@',Custom_Attribute_6='Y' Where Actual_Visit_ID='%@'",fsrLatitude,fsrLongitude,[SWDefaults currentVisitID]]];
    }
}

- (void)closeVisitWithEndDate
{
    
    
    [SalesWorxiBeaconManager destroyBeaconSingleton];
    
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
    
    [SWDefaults invalidateCurrentFieldSalesVisitTimer];
    
    if ([SWDefaults currentVisitID]) {
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:[NSDate date]]];
        
        if ([appControl.ENABLE_VISIT_LOC_UPDATE_ON_CLOSE isEqualToString:@"Y"] && [currentVisit.visitOptions.customer.Visit_Type isEqualToString:kMerchandisingVisitType]) {
            [self validateLocationCoOrdinateforOnSiteVisit];
        }
        [SWDefaults clearCurrentVisitID];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
    
    
}
- (void)getRouteServiceDidGetRoute:(NSArray *)r{
    
    
    NSLog(@"planned visit for customer from route %@",currentVisit.visitOptions.customer.Planned_visit_ID);
    
    
 
    
    if (currentVisit.visitOptions.customer.Planned_visit_ID)
    {
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID ) {
            [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:currentVisit.visitOptions.customer.Planned_visit_ID];
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    
    
    
    
    
    NSString* tempVisitID=[SWDefaults currentVisitID];
    
    if (tempVisitID) {
        
        
        
        
        NSMutableArray *locationDict = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select Latitude , Longitude from TBL_FSR_Actual_Visits where Actual_Visit_ID='%@'",[SWDefaults currentVisitID]]];
        
        
        NSLog(@"check the location dict %@", [locationDict description]);
        
        
        if (locationDict.count>0) {
            
            
            if ([[[locationDict objectAtIndex:0]stringForKey:@"Latitude"]isEqualToString:@"0"] || [[[locationDict objectAtIndex:0]stringForKey:@"Longitude"]isEqualToString:@"0"])
            {
                // #define kSQLUpdateEndDate @"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='{0}' WHERE Actual_Visit_ID='{1}'; "
                
                NSString *latitudePoint,*longtitudePoint;
                latitudePoint=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude] ;
                longtitudePoint = [NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
                
                
                
                [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@' Where Actual_Visit_ID='%@'",latitudePoint,longtitudePoint,[SWDefaults currentVisitID]]];
            }
            
        }
        
        
        Singleton * single = [Singleton retrieveSingleton];
        SWVisitManager *visitManager = [[SWVisitManager alloc] init];
        [visitManager closeVisit];
        
        if ([single.isCashCustomer isEqualToString:@"Y"]) {
            single.popToCash = @"Y";
        }
        else
        {
            single.popToCash = @"N";
            
        }
        r=nil;
        [self.navigationController  popViewControllerAnimated:YES];
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
    
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"went to visit options no activity done");
    }
    
    
}
-(void)closeButtonTapped
{
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self closeVisitWithEndDate];
                               }];
    UIAlertAction* CancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
    [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Close?", nil) andMessage:NSLocalizedString(@"Would you like to close Merchandising?", nil) andActions:actionsArray withController:self];
}

-(NSMutableArray*)fetchBrandCodeFromPOSData:(NSMutableArray*)sourceArray
{
    NSMutableArray * filteredArray=[[NSMutableArray alloc]init];
    
    
    return filteredArray;
}

-(void)saveButtonTapped
{
    
    [posChildViewController updateTextViewData];
    
    /*Availability*/
    NSMutableArray* refinedAvailabilityArray=[[NSMutableArray alloc]init];
    NSMutableArray *refinedBrandShareArray = [[NSMutableArray alloc]init];
    
    NSMutableArray* posImplementedArray=[[NSMutableArray alloc]init];
    NSMutableArray* posPromotionArray=[[NSMutableArray alloc]init];
    NSMutableArray* posPositionDetailsArray=[[NSMutableArray alloc]init];
    
    
    
    NSMutableArray * competitionInfoArray=[[NSMutableArray alloc]init];
    
    
    
    
    for (SalesWorxMerchandisingBrand * currentMerchandising in currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray) {
        
        NSPredicate * filterPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated == %@",KAppControlsYESCode];
        NSArray *  currentAvailabilityArray= [[currentMerchandising.availabilityArray filteredArrayUsingPredicate:filterPredicate] mutableCopy];
        if (currentAvailabilityArray.count>0) {
            [refinedAvailabilityArray addObjectsFromArray:currentAvailabilityArray];
        }
        
        /*Brand Share*/
        if ([currentMerchandising.brandShare.isUpdated isEqualToString:KAppControlsYESCode]) {
            [refinedBrandShareArray addObject:currentMerchandising];
        }
        
    }
    
    NSLog(@"refined availability array is %@", refinedAvailabilityArray);
    
    
    NSLog(@"refined brand share array is %@", refinedBrandShareArray);
    
    //insert merchandising data
    
    BOOL insertAvailabilitySuccessful=NO;
    BOOL insertBrandShareSuccessful=NO;
    BOOL insertPlanogramSuccessful=NO;
    BOOL insertPOSSuccessful=NO;
    BOOL insertCompetitionInfoSuccessful=NO;
    BOOL insertInStoreActivitySuccessful=NO;
    BOOL insertSurveyPicturesSuccessful=NO;
    BOOL insertCommentsSuccessful=NO;
    BOOL createSession=NO;
    
    
    FMDatabase *fmdatabase = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [fmdatabase open];
    [fmdatabase beginTransaction];
    
    //createSession=[[SWDatabaseManager retrieveManager]insertMerchandisingSessionData:currentVisit];
    
    createSession =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Session (Session_ID, Actual_Visit_ID, Start_Date, End_Date,SalesRep_ID,Customer_ID ,Site_Use_ID ) VALUES (?,?,?,?,?,?,?)",currentVisit.visitOptions.salesWorxMerchandising.Session_ID,currentVisit.Visit_ID,currentVisit.visitOptions.salesWorxMerchandising.Start_Time,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"],currentVisit.visitOptions.customer.Customer_ID,currentVisit.visitOptions.customer.Site_Use_ID, nil];
    
    
    
    //insert availability data
    if (refinedAvailabilityArray.count>0) {
        
        for (NSInteger i=0;i<refinedAvailabilityArray.count;i++) {
            
            MerchandisingAvailability * currentAvailaiblity=[refinedAvailabilityArray objectAtIndex:i];
            
            NSString* rowID=[NSString createGuid];
            NSString* sessionID=currentVisit.visitOptions.salesWorxMerchandising.Session_ID;
            NSString* brandCode=currentAvailaiblity.Brand_Code;
            NSString* inventoryItemID=currentAvailaiblity.Inventory_Item_ID;
            NSString* stock=[NSString getValidStringValue:currentAvailaiblity.Stock];
            NSString* previousStock=[NSString getValidStringValue:currentAvailaiblity.Previous_Stock];
            NSString* stockVarience=[NSString getValidStringValue:[NSString stringWithFormat:@"%0.2f",currentAvailaiblity.Stock_Varience.doubleValue]];
            
            NSString* price=[NSString getValidStringValue:currentAvailaiblity.Price];
            NSString* previousPrice=[NSString getValidStringValue:currentAvailaiblity.Previous_Price];
            NSString* priceVarience=[NSString getValidStringValue:[NSString stringWithFormat:@"%0.2f",currentAvailaiblity.Price_Varience.doubleValue]];
            NSString* position=[NSString getValidStringValue:currentAvailaiblity.Position];
            
            
            insertAvailabilitySuccessful =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Product_Availability (Row_ID,Session_ID, Brand_Code, Inventory_Item_ID, Stock,Previous_Stock ,Stock_Variance,Price,Previous_Price,Price_Variance,Position ) VALUES (?,?,?,?,?,?,?,?,?,?,?)",rowID,sessionID,brandCode,inventoryItemID,stock,[NSString isEmpty:previousStock]?[NSNull null]:previousStock,stockVarience,price,[NSString isEmpty:previousPrice]?[NSNull null]:previousPrice,priceVarience,position, nil];
            
            NSLog(@"insert availability ? %hhd", insertAvailabilitySuccessful);
            
        }
    }
    else
    {
        insertAvailabilitySuccessful=YES;
    }
    
    
    //insert brand share data
    if (refinedBrandShareArray.count>0) {
        for (SalesWorxMerchandisingBrand *currentBrand in refinedBrandShareArray) {
            MerchandisingBrandShare *selectedBrandShare = currentBrand.brandShare;
            insertBrandShareSuccessful =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Brand_Share (Row_ID,Session_ID, Brand_Code, Share_Of_Shelf, Quality_Of_Presence, Promotion_Info,Calculation_In,Brand_Share,Category,Market_Share) VALUES (?,?,?,?,?,?,?,?,?,?)",[NSString createGuid], currentVisit.visitOptions.salesWorxMerchandising.Session_ID, currentBrand.Brand_Code, selectedBrandShare.selectedSOS.shareOfShelf.stringValue, selectedBrandShare.quality_Rating, selectedBrandShare.promotion,selectedBrandShare.selectedSOS.calculationIn,selectedBrandShare.selectedSOS.brandShare.stringValue,selectedBrandShare.selectedSOS.category.stringValue,selectedBrandShare.selectedSOS.marketShare.stringValue, nil];
        }
    }
    else{
        insertBrandShareSuccessful=YES;
    }
    
    
    //insert POS DATA
    NSArray* filteredPOSMaterialArray=[[NSArray alloc]init];
    NSArray* filteredPromotionMaterialArray=[[NSArray alloc]init];
    NSArray* filteredPositionDetailsArray = [[NSArray alloc]init];
    for (SalesWorxMerchandisingBrand * currentMerchandising in currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray) {
        
        
        //insert comments if any
        
        comments=[[NSString alloc]init];
        comments=[NSString getValidStringValue:currentMerchandising.pos.comments];
        
        
        /*POS*/
        if ([currentMerchandising.pos.posMaterialSelected isEqualToString:KAppControlsYESCode]) {
            NSPredicate * filterPredicate = [NSPredicate predicateWithFormat:@"SELF.isSelected == %@",KAppControlsYESCode];
             filteredPOSMaterialArray=[currentMerchandising.pos.posMaterialArray filteredArrayUsingPredicate:filterPredicate];
            NSLog(@"filtered pos is %@", filteredPOSMaterialArray);
            [posImplementedArray addObjectsFromArray:filteredPOSMaterialArray];
            
            //insert parent line data
            if (filteredPOSMaterialArray.count>0) {
                BOOL insertIntoPOSImplementedParentSuccessfull= NO;
                BOOL insertIntoPOSImplementedChildSuccessfull= NO;
                NSString* parentLineID =[NSString createGuid];
                NSString* brandCodeforParent=[NSString getValidStringValue:[[filteredPOSMaterialArray valueForKey:@"brandCode"]objectAtIndex:0]];
                
                
                commentsInserted=NO;
                if (![NSString isEmpty:comments]) {
                    
                    commentsInserted=[fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value) VALUES (?,?,?,?,?)",[NSString createGuid], currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"POS Comments", comments, nil];
                    
                    if (commentsInserted) {
                        insertPOSSuccessful=YES;
                    }
                    else{
                        insertPOSSuccessful=NO;
                        return;
                    }
                    
                }
                else{
                    
                }
                
                insertIntoPOSImplementedParentSuccessfull =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value) VALUES (?,?,?,?,?)",parentLineID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"POS Material", KAppControlsYESCode, nil];
                
                if (insertIntoPOSImplementedParentSuccessfull) {
                    //insert child
                    for (MerchandisingPOSMaterial *currentMaterial in filteredPOSMaterialArray) {
                        insertIntoPOSImplementedChildSuccessfull =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value,Custom_Attribute_1) VALUES (?,?,?,?,?,?)",[NSString createGuid], currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"POS Material", currentMaterial.title,parentLineID, nil];
                        if (insertIntoPOSImplementedChildSuccessfull) {
                            insertPOSSuccessful=YES;
                        }
                        else
                        {
                            insertPOSSuccessful=NO;
                            return;
                            
                        }
                    }
                }
                else
                {
                    insertPOSSuccessful=NO;
                    return;
                    
                }
            }
        }
        
        
        if ([currentMerchandising.pos.promotionMaterialSelected isEqualToString:KAppControlsYESCode]) {
            NSPredicate * filterPredicate = [NSPredicate predicateWithFormat:@"SELF.isSelected == %@",KAppControlsYESCode];
             filteredPromotionMaterialArray=[currentMerchandising.pos.promotionMaterialArray filteredArrayUsingPredicate:filterPredicate];
            NSLog(@"filtered pos is %@", filteredPromotionMaterialArray);
            
            //insert parent line data
            if (filteredPromotionMaterialArray.count>0) {
                BOOL insertIntoPOSPromotionParentSuccessfull= NO;
                BOOL insertIntoPOSPromotionChildSuccessfull= NO;
                NSString* parentLineID =[NSString createGuid];
                NSString* brandCodeforParent=[NSString getValidStringValue:[[filteredPromotionMaterialArray valueForKey:@"brandCode"]objectAtIndex:0]];
                
                
                if (![NSString isEmpty:comments] && commentsInserted==NO) {
                    
                    commentsInserted=[fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value) VALUES (?,?,?,?,?)",[NSString createGuid], currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"Comments", comments, nil];
                    
                    if (commentsInserted) {
                        insertPOSSuccessful=YES;
                    }
                    else{
                        insertPOSSuccessful=NO;
                        return;
                    }
                    
                }
                
                insertIntoPOSPromotionParentSuccessfull =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value) VALUES (?,?,?,?,?)",parentLineID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"Promotion Material", KAppControlsYESCode, nil];
                
                if (insertIntoPOSPromotionParentSuccessfull) {
                    //insert child
                    for (MerchandisingPromotionMaterial *currentMaterial in filteredPromotionMaterialArray) {
                        insertIntoPOSPromotionChildSuccessfull =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value,Custom_Attribute_1) VALUES (?,?,?,?,?,?)",[NSString createGuid], currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"POS Material", currentMaterial.title,parentLineID, nil];
                        if (insertIntoPOSPromotionChildSuccessfull) {
                            insertPOSSuccessful=YES;
                        }
                        else
                        {
                            insertPOSSuccessful=NO;
                            return;
                            
                        }
                    }
                }
                else
                {
                    insertPOSSuccessful=NO;
                    return;
                    
                }
            }
        }
        
        if ([currentMerchandising.pos.positionDetailsSelected isEqualToString:KAppControlsYESCode]) {
            NSPredicate * filterPredicate = [NSPredicate predicateWithFormat:@"SELF.isSelected == %@",KAppControlsYESCode];
             filteredPositionDetailsArray=[currentMerchandising.pos.positionDetailsArray filteredArrayUsingPredicate:filterPredicate];
            NSLog(@"filtered pos is %@", filteredPositionDetailsArray);
            
            //insert parent line data
            if (filteredPositionDetailsArray.count>0) {
                BOOL insertIntoPOSPositionDetailsParentSuccessfull= NO;
                BOOL insertIntoPOSPositionDetailsSuccessfull= NO;
                NSString* parentLineID =[NSString createGuid];
                NSString* brandCodeforParent=[NSString getValidStringValue:[[filteredPositionDetailsArray valueForKey:@"brandCode"]objectAtIndex:0]];
                
                if (![NSString isEmpty:comments] && commentsInserted==NO) {
                    
                    commentsInserted=[fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value) VALUES (?,?,?,?,?)",[NSString createGuid], currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"Comments", comments, nil];
                    
                    if (commentsInserted) {
                        insertPOSSuccessful=YES;
                    }
                    else{
                        insertPOSSuccessful=NO;
                        return;
                    }
                    
                }
                
                insertIntoPOSPositionDetailsParentSuccessfull =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value) VALUES (?,?,?,?,?)",parentLineID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"Position Details", KAppControlsYESCode, nil];
                
                if (insertIntoPOSPositionDetailsParentSuccessfull) {
                    //insert child
                    for (MerchandisingPositionDetails *currentMaterial in filteredPositionDetailsArray) {
                        insertIntoPOSPositionDetailsSuccessfull =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Responses (Row_ID,Session_ID, Brand_Code, Response_Type, Response_Value,Custom_Attribute_1) VALUES (?,?,?,?,?,?)",[NSString createGuid], currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"POS Material", currentMaterial.title,parentLineID, nil];
                        if (insertIntoPOSPositionDetailsSuccessfull) {
                            insertPOSSuccessful=YES;
                        }
                        else
                        {
                            insertPOSSuccessful=NO;
                            return;
                            
                        }
                    }
                }
                else
                {
                    insertPOSSuccessful=NO;
                    return;
                    
                }
            }
        }
        
        
        if (filteredPOSMaterialArray.count==0 && filteredPositionDetailsArray.count==0 && filteredPromotionMaterialArray.count==0) {
            insertPOSSuccessful=YES;
        }
        
        
        //insert Competitor info
        
        [competitionInfoArray addObjectsFromArray:currentMerchandising.competitionInfoArray];
        if(currentMerchandising.competitionInfoArray.count>0)
        {
            for (MerchandisingCompetitionInfo *currentCompetitorInfo in currentMerchandising.competitionInfoArray) {
                
                NSString* parentLineID=[NSString createGuid];
                NSString* brandCodeforParent = currentMerchandising.Brand_Code;
//                insertCompetitionInfoSuccessful =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Competitor_Info (Competitor_Info_ID,Session_ID, Brand, Item, Price,Has_Active_Promotion,Stock_Level,Shelf_Placement,Notes,Custom_Attribute_1) VALUES (?,?,?,?,?,?,?,?,?,?)",parentLineID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, currentCompetitorInfo.productName,currentCompetitorInfo.price, KAppControlsYESCode,currentCompetitorInfo.activePromotion,currentCompetitorInfo.stockLevel,currentCompetitorInfo.shelfPlacement,currentCompetitorInfo.notes,currentCompetitorInfo.threatLevel, nil];
                insertCompetitionInfoSuccessful =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Competitor_Info (Competitor_Info_ID,Session_ID, Brand, Item, Price,Has_Active_Promotion,Stock_Level,Shelf_Placement,Notes,Custom_Attribute_1) VALUES (?,?,?,?,?,?,?,?,?,?)",parentLineID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, currentCompetitorInfo.productName,currentCompetitorInfo.price, KAppControlsYESCode,currentCompetitorInfo.stockLevel,currentCompetitorInfo.shelfPlacement,currentCompetitorInfo.notes,currentCompetitorInfo.threatLevel, nil];
                
                if (insertCompetitionInfoSuccessful) {
                    
                //save images to documents folder
                BOOL competitionInfoImageInserted = NO;
                for (NSString* imageFileName in currentCompetitorInfo.imagesArray) {
                    NSString* imageID = [NSString createGuid];
                    competitionInfoImageInserted =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Competitor_Pictures (Competitor_Info_ID,Row_ID, Image_File_Name) VALUES (?,?,?)",parentLineID, imageID,imageFileName, nil];
                    
                    if (competitionInfoImageInserted) {
                        //save image to documents directory
                        NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingCompetitorInfoImages] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageFileName]];

                        if ([[NSFileManager defaultManager]fileExistsAtPath:tempPath]) {
                            NSError * error;
                            NSString* actualPath = [[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kMerchandisingCompetitorInfoImages]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageFileName]];
                            
                            BOOL moveSuccessfull =[[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:actualPath error:&error];
                            
                            if (moveSuccessfull) {
                            //now delete from temp
                            NSError * deleteError;
                            BOOL deleteStatus = NO;
                            deleteStatus=[[NSFileManager defaultManager]removeItemAtPath:tempPath error:&deleteError];
                            }
                            
                        }
                    }
                }
            }
            }
        }
        else
        {
            insertCompetitionInfoSuccessful=YES;
        }
        
        //insert planogram
        NSPredicate *planogramPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated == %@",KAppControlsYESCode];
        NSArray* filteredArray=[currentMerchandising.planogramDetailsArray filteredArrayUsingPredicate:planogramPredicate];

        if (filteredArray.count>0) {
            for (MerchandisingPlanogram * currentPlanogram in filteredArray) {
                NSString* rowID = [NSString createGuid];
                insertPlanogramSuccessful =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Images (Row_ID, Session_ID,Brand_Code,Image_Type,Image_File,Custom_Attribute_1) VALUES (?,?,?,?,?,?)",rowID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID,currentMerchandising.Brand_Code,@"Planogram",currentPlanogram.capturedImageName,currentPlanogram.imageMatched, nil];
                
                if (insertPlanogramSuccessful) {
                    NSLog(@"planogram data inserted successfully");
                    
                    NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingPlanogramImages] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentPlanogram.capturedImageName]];
                    
                    if ([[NSFileManager defaultManager]fileExistsAtPath:tempPath]) {
                        NSError * error;
                        NSString* actualPath = [[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kMerchandisingPlanogramImages]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentPlanogram.capturedImageName]];
                        BOOL moveSuccessfull =[[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:actualPath error:&error];
                        
                        if (moveSuccessfull) {
                            //now delete from temp
                            NSError * deleteError;
                            BOOL deleteStatus = NO;
                            deleteStatus=[[NSFileManager defaultManager]removeItemAtPath:tempPath error:&deleteError];
                        }

                    }
                    

                    
                }
            }
        }
        else{
            insertPlanogramSuccessful=YES;
        }
        
        //insert InstoreActivity
        
        NSString* updatedFlag = [NSString getValidStringValue:currentMerchandising.inStoreActivity.isUpdated];
        
        if(currentMerchandising.inStoreActivity != nil && [updatedFlag isEqualToString:KAppControlsYESCode]){
            NSString *rowID = [NSString createGuid];
            NSString *brandCodeforParent = currentMerchandising.Brand_Code;
            MerchandisingInStoreActivity * tempInstoreActivity = currentMerchandising.inStoreActivity;
            insertInStoreActivitySuccessful = [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Instore_Activity (Row_ID,Session_ID,Store_Condition,Condition_Comments,Manager_Interaction,Interaction_Comments,New_SKU_Listing,New_SKU_Comments,New_Promo_Activation,Promo_Start_Date,Promo_End_Date,Promo_Notes,Custom_Attribute_1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",rowID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID,tempInstoreActivity.StoreCondition,tempInstoreActivity.StoreConditionNotes,tempInstoreActivity.ManagerInteractionStatus == YES?@"Y":@"N",tempInstoreActivity.managerInteractionNotes,tempInstoreActivity.NewSKUListingstatus==YES?@"Y":@"N",tempInstoreActivity.NewSKUListingNotes,tempInstoreActivity.NewPromoActivationStatus == YES?@"Y":@"N",tempInstoreActivity.NewPromoActivationstartDate,tempInstoreActivity.NewPromoActivationEndDate,tempInstoreActivity.NewPromoActivationNotes,brandCodeforParent,nil];
        }
        else
        {
            insertInStoreActivitySuccessful=YES;
        }
        
        
        //insert Survey Picture
        if(currentMerchandising.surveyPicturesArray.count > 0)
        {
            for (MerchandisingSurveyPictures *currentSurveyPicture in currentMerchandising.surveyPicturesArray) {
                
                NSString *rowID = [NSString createGuid];
                NSString *brandCodeforParent = currentMerchandising.Brand_Code;
                insertSurveyPicturesSuccessful = [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Images (Row_ID, Session_ID, Brand_Code, Image_Type, Comments, Image_File, Custom_Attribute_1) VALUES (?,?,?,?,?,?,?)",rowID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, @"Merch_Survey_Pictures", currentSurveyPicture.imageDesc, currentSurveyPicture.imageName, currentSurveyPicture.productName, nil];
                
                if (insertSurveyPicturesSuccessful) {
                    
                    BOOL surveyPictureMarkInserted = NO;
                    
                    if (currentSurveyPicture.imageMarkArray.count>0) {
                    for (MerchandisingSurveyPicturesMark *currentPictureMark in currentSurveyPicture.imageMarkArray) {
                        
                        CGRect markViewframe = currentPictureMark.markViewframeWithRespectToImageView;
                        
                        NSString * originX=[NSString stringWithFormat:@"%f",markViewframe.origin.x];
                        NSString * originY=[NSString stringWithFormat:@"%f",markViewframe.origin.y];
                        NSString * markWidth=[NSString stringWithFormat:@"%f",markViewframe.size.width];
                        NSString * markHeight=[NSString stringWithFormat:@"%f",markViewframe.size.height];
                        NSString * markDesc = [NSString getValidStringValue:currentPictureMark.markDescription];
                        
                        surveyPictureMarkInserted = [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Picture_Annotations (Row_ID, Picture_ID, Top_X, Top_Y, Length, Height, Annotation) VALUES (?,?,?,?,?,?,?)",[NSString createGuid], rowID, originX, originY, markWidth, markHeight, markDesc, nil];
                        
                        if (surveyPictureMarkInserted) {
                            //save image to documents directory
                            NSString *tempPath = [[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:KMerchandisingSurveyPicturesFolderName] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentSurveyPicture.imageName]];
                            
                            if ([[NSFileManager defaultManager]fileExistsAtPath:tempPath]) {
                                NSError * error;
                                NSString *actualPath = [[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:KMerchandisingSurveyPicturesFolderName]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentSurveyPicture.imageName]];
                                
                                BOOL moveSuccessfull = [[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:actualPath error:&error];
                                
                                if (moveSuccessfull) {
                                    //now delete from temp
                                    NSError * deleteError;
                                    BOOL deleteStatus = NO;
                                    deleteStatus=[[NSFileManager defaultManager]removeItemAtPath:tempPath error:&deleteError];
                                }
                            }
                        }
                    }
                }
                else
                {
                    //save image to documents directory
                    NSString *tempPath = [[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:KMerchandisingSurveyPicturesFolderName] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentSurveyPicture.imageName]];
                    
                    if ([[NSFileManager defaultManager]fileExistsAtPath:tempPath]) {
                        NSError * error;
                        NSString *actualPath = [[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:KMerchandisingSurveyPicturesFolderName]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentSurveyPicture.imageName]];
                        
                        BOOL moveSuccessfull = [[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:actualPath error:&error];
                        
                        if (moveSuccessfull) {
                            //now delete from temp
                            NSError * deleteError;
                            BOOL deleteStatus = NO;
                            deleteStatus=[[NSFileManager defaultManager]removeItemAtPath:tempPath error:&deleteError];
                        }
                    }
                }
            }
        }
        
        }
        else
        {
            insertSurveyPicturesSuccessful=YES;
        }
        
        //insert Comments
        if(currentMerchandising.commentsArray.count>0)
        {
            for (MerchandisingComments *currentComment in currentMerchandising.commentsArray) {
                
                if (currentComment.comment.length > 0) {
                    NSString *rowID = [NSString createGuid];
                    NSString *brandCodeforParent = currentMerchandising.Brand_Code;
                    insertCommentsSuccessful = [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Brand_Comments (Row_ID,Session_ID, Brand_Code, Comments, Custom_Attribute_1) VALUES (?,?,?,?,?)",rowID, currentVisit.visitOptions.salesWorxMerchandising.Session_ID, brandCodeforParent, currentComment.comment, currentComment.productName, nil];
                }
            }
        }
        else{
            insertCommentsSuccessful=YES;
        }
    }
    
    

    
    if (insertAvailabilitySuccessful&&insertBrandShareSuccessful && insertPlanogramSuccessful&& insertPOSSuccessful&&insertCompetitionInfoSuccessful&&insertInStoreActivitySuccessful&&insertSurveyPicturesSuccessful&&insertCommentsSuccessful&&createSession) {
        
        [fmdatabase commit];
        
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Success", nil) andMessage:NSLocalizedString(@"Merchandising Completed", nil) andActions:actionsArray withController:self];
        
        
    }
    else{
        NSLog(@"error while inserting db in merchandising %@", fmdatabase.lastError);
        [fmdatabase rollback];
        [fmdatabase close];
        
        if (!errorAlertDisplayed) {
            
            
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(kYesTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           errorAlertDisplayed=YES;
                                           [self saveButtonTapped];
                                       }];
            
            UIAlertAction* noAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(kNoTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [self.navigationController popViewControllerAnimated:YES];
                                       }];
            
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,noAction,nil];
            [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(KErrorAlertTitlestr, nil) andMessage:NSLocalizedString(@"Unable to insert data to database, would you like to try again?", nil) andActions:actionsArray withController:self];
        }
        else
        {
            
            
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(kNoTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [self.navigationController popViewControllerAnimated:YES];
                                       }];
            
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
            [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(KErrorAlertTitlestr, nil) andMessage:NSLocalizedString(@"Unable to insert data to database, please try again later", nil) andActions:actionsArray withController:self];
        }
        
        
    }
    
    
}

-(void)prepareMerchandisingContentArray
{

}

//-(void)backButtonTapped:(id)sender
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    if (self.isMovingToParentViewController) {
        
    }
    
    
   
    
    
    if (self.isMovingToParentViewController) {
       
    }
    else
    {
        NSLog(@"did not hit");
    }
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"scroll view frame in did appear is %@", NSStringFromCGRect(merchandisingScrollView.frame));
    
    if (priceDetailsChildViewController==nil) {
        [self prepareChildViewControllers];
        
        [self displaySegmentedData];
    }
//    if (self.isMovingToParentViewController) {
//        if (priceDetailsChildViewController==nil) {
//            [self prepareChildViewControllers];
//        }
//    }
}

-(void)prepareChildViewControllers
{
    [merchandisingScrollView setPagingEnabled:YES];
    [merchandisingScrollView setContentSize:CGSizeMake(merchandisingScrollView.frame.size.width*8, merchandisingScrollView.frame.size.height)];
    
   // NSLog(@"merchandising scrollview content size is %@", NSStringFromCGSize(merchandisingScrollView.contentSize));

    
    
    priceDetailsChildViewController=[[SalesWorxMerchandisingProductDetailsViewController alloc] initWithNibName:@"SalesWorxMerchandisingProductDetailsViewController" bundle:[NSBundle mainBundle]];
    brandShareChildViewController=[[SalesWorxMerchandisingBrandShareViewController alloc] initWithNibName:@"SalesWorxMerchandisingBrandShareViewController" bundle:[NSBundle mainBundle]];
    planogramChildViewController=[[SalesWorxMerchandisingPlanogramViewController alloc] initWithNibName:@"SalesWorxMerchandisingPlanogramViewController" bundle:[NSBundle mainBundle]];
    posChildViewController=[[SalesWorxMerchandisingPOSViewController alloc] initWithNibName:@"SalesWorxMerchandisingPOSViewController" bundle:[NSBundle mainBundle]];
    competitionChildViewController=[[SalesWorxMerchandisingCompetitionInfoViewController alloc] initWithNibName:@"SalesWorxMerchandisingCompetitionInfoViewController" bundle:[NSBundle mainBundle]];
    instoreChildViewController=[[SalesWorxMerchandisingInStoreActivityViewController alloc] initWithNibName:@"SalesWorxMerchandisingInStoreActivityViewController" bundle:[NSBundle mainBundle]];
    surveyPicturesChildViewController=[[SalesWorxMerchandisingSurveyPicturesViewController alloc] initWithNibName:@"SalesWorxMerchandisingSurveyPicturesViewController" bundle:[NSBundle mainBundle]];
    commentsChildViewController=[[SalesWorxMerchandisingCommentsViewController alloc] initWithNibName:@"SalesWorxMerchandisingCommentsViewController" bundle:[NSBundle mainBundle]];

    
    priceDetailsChildViewController.view.frame=CGRectMake(0, 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);
    brandShareChildViewController.view.frame=CGRectMake(merchandisingScrollView.frame.size.width, 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);
    planogramChildViewController.view.frame=CGRectMake((merchandisingScrollView.frame.size.width *2), 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);
    posChildViewController.view.frame=CGRectMake((merchandisingScrollView.frame.size.width *3), 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);
    competitionChildViewController.view.frame=CGRectMake((merchandisingScrollView.frame.size.width *4), 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);
    instoreChildViewController.view.frame=CGRectMake((merchandisingScrollView.frame.size.width *5), 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);
    surveyPicturesChildViewController.view.frame=CGRectMake((merchandisingScrollView.frame.size.width *6), 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);
    commentsChildViewController.view.frame=CGRectMake((merchandisingScrollView.frame.size.width *7), 0, merchandisingScrollView.frame.size.width-2, merchandisingScrollView.frame.size.height-2);

    
    [merchandisingScrollView addSubview:priceDetailsChildViewController.view];
    [merchandisingScrollView addSubview:brandShareChildViewController.view];
    [merchandisingScrollView addSubview:planogramChildViewController.view];
    [merchandisingScrollView addSubview:posChildViewController.view];
    [merchandisingScrollView addSubview:competitionChildViewController.view];
    [merchandisingScrollView addSubview:instoreChildViewController.view];
    [merchandisingScrollView addSubview:surveyPicturesChildViewController.view];
    [merchandisingScrollView addSubview:commentsChildViewController.view];
    
    
    [self addChildViewController:priceDetailsChildViewController];
    [self addChildViewController:brandShareChildViewController];
    [self addChildViewController:planogramChildViewController];
    [self addChildViewController:posChildViewController];
    [self addChildViewController:competitionChildViewController];
    [self addChildViewController:instoreChildViewController];
    [self addChildViewController:surveyPicturesChildViewController];
    [self addChildViewController:commentsChildViewController];
}

#pragma mark Segment Control Action

-(void)displaySegmentedData
{
    if (merchandisingTitlesSegment.selectedSegmentIndex==0) {
        //availability
        [priceDetailsChildViewController showAvailabilityData:currentMerchandising];
    }
    else if (merchandisingTitlesSegment.selectedSegmentIndex==1)
    {
        //Brand Share
        [brandShareChildViewController showBrandShareData:currentMerchandising.brandShare];
    }
    else if (merchandisingTitlesSegment.selectedSegmentIndex==2)
    {
        //Planogram
        [planogramChildViewController showPlanogramImagesforBrand:currentMerchandising.planogramDetailsArray];
    }
    else if (merchandisingTitlesSegment.selectedSegmentIndex==3)
    {
        //Pos
        [posChildViewController showPosDataForBrand:currentMerchandising];
    }
    else if (merchandisingTitlesSegment.selectedSegmentIndex==4)
    {
        //competiton Info
        [competitionChildViewController showCompetitorInfoData:currentMerchandising];
    }
    else if (merchandisingTitlesSegment.selectedSegmentIndex==5)
    {
        //In store activity
        [instoreChildViewController showInStoreActivity:currentMerchandising];
    }
    else if (merchandisingTitlesSegment.selectedSegmentIndex==6)
    {
        //Survey Pictures
        [surveyPicturesChildViewController showSurveyPicturesforBrand:currentMerchandising];
    }
    else if (merchandisingTitlesSegment.selectedSegmentIndex==7)
    {
        //Comments
        [commentsChildViewController showCommentsforBrand:currentMerchandising];
    }
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    
   // NSLog(@"selected segment index is %ld", (long)segmentedControl.selectedSegmentIndex);
    [self.view endEditing:YES];
    [self displaySegmentedData];
}
#pragma mark Customer Details Action

-(void)showCustomerDetails
{
    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    availableBalanceLabel.text=[currentVisit.visitOptions.customer.Avail_Bal currencyString];
    
    visitTypeLabel.layer.cornerRadius = 8.0f;
    [visitTypeLabel.layer setMasksToBounds:YES];
    visitTypeLabel.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerStatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerStatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    
    __block NSString *lastVisitedDateStr=[[NSString alloc] init];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:currentVisit.visitOptions.customer];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            lblLastVisitedOn.text=lastVisitedDateStr.length==0?KNotApplicable:lastVisitedDateStr;
        });
    });
}

#pragma mark Database Methods
-(void)fetchBrandsData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    brandsArray = [[SWDatabaseManager retrieveManager]fetchBrandCodes];

    if (brandsArray.count>0) {
        
        NSMutableArray* contentArray=[[NSMutableArray alloc]init];
        
        for (NSString* brandCode in brandsArray) {
            SalesWorxMerchandisingBrand *currentMerchandising = [[SalesWorxMerchandisingBrand alloc]init];
            currentMerchandising.Brand_Code = brandCode;
            currentMerchandising.availabilityArray = [[NSMutableArray alloc]init];

            NSMutableArray * currentAvailaibilityArray=[[SWDatabaseManager retrieveManager]fetchAvailabilityDatawithBrandCode:brandCode andVisit:currentVisit];
            
            for (NSInteger i=0; i<currentAvailaibilityArray.count; i++) {
                
                NSMutableDictionary* currentAvblDict=[currentAvailaibilityArray objectAtIndex:i];
                MerchandisingAvailability * currentAvailability= [[MerchandisingAvailability alloc]init];
                currentAvailability.Description=[NSString getValidStringValue:[currentAvblDict valueForKey:@"Description"]];
                currentAvailability.Inventory_Item_ID=[NSString getValidStringValue:[currentAvblDict valueForKey:@"Inventory_Item_ID"]];
                currentAvailability.Brand_Code=brandCode;
                
                NSString* prevStockStr=[NSString getValidStringValue:[currentAvblDict valueForKey:@"Previous_Stock"]];
                
                if (![NSString isEmpty:prevStockStr]) {
                    currentAvailability.Previous_Stock=[NSDecimalNumber decimalNumberWithString:prevStockStr];
                }
                else{

                }
                
                
                NSString* prevPriceStr=[NSString getValidStringValue:[currentAvblDict valueForKey:@"Previous_Price"]];
                if (![NSString isEmpty:prevPriceStr]) {
                    currentAvailability.Previous_Price=[NSDecimalNumber decimalNumberWithString:prevPriceStr];
                }
                else{

                }
                
                currentAvailability.isUpdated=KAppControlsNOCode;
                
                [currentMerchandising.availabilityArray addObject:currentAvailability];
            }
            
            
            // Brand Share
            currentMerchandising.brandShare = [[MerchandisingBrandShare alloc]init];
            currentMerchandising.brandShare.brand_Share = @"";
            currentMerchandising.brandShare.category_Share = @"";
            currentMerchandising.brandShare.SOS_Percentage = @"";
            currentMerchandising.brandShare.marketShare_Percentage = @"";
            currentMerchandising.brandShare.quality_Rating = @"0";
            currentMerchandising.brandShare.promotion = @"";
            currentMerchandising.brandShare.isUpdated = KAppControlsNOCode;
            
            
            currentMerchandising.planogram = [[MerchandisingPlanogram alloc]init];
            MerchandisingPOS *pos = [[MerchandisingPOS alloc]init];
            pos.selectedBrandCode = brandCode;
            pos.isUpdated = KAppControlsNOCode;
            pos.posMaterialArray=[[NSMutableArray alloc]init];
            pos.promotionMaterialArray=[[NSMutableArray alloc]init];
            pos.positionDetailsArray=[[NSMutableArray alloc]init];

            
//           NSMutableArray* tempPosMaterialArray = [NSMutableArray arrayWithObjects:@"Retail Topper", @"BookCase Topper", @"Shelf Topper", @"Roll Banners", @"Holiday POS", @"Door Hangers",nil];
//         NSMutableArray*   tempPromotionMaterialArray = [NSMutableArray arrayWithObjects:@"Booth", @"Samples", @"Gifts",  nil];
//
//          NSMutableArray*  tempPositionDetailsArray = [NSMutableArray arrayWithObjects:@"Good", @"Needs Improvement", @"Poor", @"Not Applicable",  nil];
            
            /*MERCH_POSM: POS Materials
             MERCH_POSITION_DET: Position Details
             MERCH_PROMO_MAT: Promotion Materials
*/
            
            NSMutableArray* tempPosMaterialArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select Code_Value, Custom_Attribute_1 from tbl_app_codes where code_type = 'POSM_TITLE'"];
            
            NSMutableArray*   tempPromotionMaterialArray = [[SWDatabaseManager retrieveManager]fetchAppCodeValueforPOS:@"MERCH_PROMO_MAT"];
            
            NSMutableArray*  tempPositionDetailsArray=[[SWDatabaseManager retrieveManager]fetchAppCodeValueforPOS:@"MERCH_POSITION_DET"];
            
            for (NSInteger i=0; i<tempPosMaterialArray.count; i++) {
                MerchandisingPOSMaterial *currentMaterial = [[MerchandisingPOSMaterial alloc]init];
                currentMaterial.title=[NSString getValidStringValue:[[tempPosMaterialArray objectAtIndex:i] valueForKey:@"Code_Value"]];
                currentMaterial.isSelected=KAppControlsNOCode;
                currentMaterial.brandCode=brandCode;
                currentMaterial.imageName=[NSString getValidStringValue:[[tempPosMaterialArray objectAtIndex:i] valueForKey:@"Custom_Attribute_1"]];
                [pos.posMaterialArray addObject:currentMaterial];
            }
            
            for (NSInteger i=0; i<tempPromotionMaterialArray.count; i++) {
                MerchandisingPromotionMaterial *currentMaterial = [[MerchandisingPromotionMaterial alloc]init];
                currentMaterial.title=[NSString getValidStringValue:[tempPromotionMaterialArray objectAtIndex:i]];
                currentMaterial.isSelected=KAppControlsNOCode;
                currentMaterial.brandCode=brandCode;
                [pos.promotionMaterialArray addObject:currentMaterial];
            }
            
            for (NSInteger i=0; i<tempPositionDetailsArray.count; i++) {
                MerchandisingPositionDetails *currentMaterial = [[MerchandisingPositionDetails alloc]init];
                currentMaterial.title=[NSString getValidStringValue:[tempPositionDetailsArray objectAtIndex:i]];
                currentMaterial.isSelected=KAppControlsNOCode;
                currentMaterial.brandCode=brandCode;
                [pos.positionDetailsArray addObject:currentMaterial];
            }
            
            
            currentMerchandising.pos=pos;
            
            currentMerchandising.competitionInfoArray = [[NSMutableArray alloc]init];

            currentMerchandising.inStoreActivity = [[MerchandisingInStoreActivity alloc]init];

            
            
            //planogramArray
            NSMutableArray * planogramDataArray=[[NSMutableArray alloc]init];
            planogramDataArray=[[SWDatabaseManager retrieveManager]fetchPlanogramData:currentMerchandising.Brand_Code];
            currentMerchandising.planogramDetailsArray=planogramDataArray;
            
            
            [contentArray addObject:currentMerchandising];
            
            
            //product for survey and comments
            NSMutableArray* productsDatarray=[[SWDatabaseManager retrieveManager]fetchMerchandisingProductsForSelectedBrand:currentMerchandising.Brand_Code withClassification:currentVisit];
            
            currentMerchandising.productArray=[[NSMutableArray alloc]init];
            currentMerchandising.productArray=productsDatarray;
            
            
        }
        
        
        
        
        currentVisitMerchandisingArray=[[NSMutableArray alloc]init];
        currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray=[[NSMutableArray alloc]init];

        currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray=contentArray;

        NSLog(@"merchandising array is %@",currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray);

        [brandsTableView reloadData];
        
        if (currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray.count>0) {
            [brandsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
            [brandsTableView.delegate tableView:brandsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

        }
        
        
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark UITableView Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching) {
        return filteredBrandsArray.count;
    } else {
        return currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"merchandisingCell";
    SalesWorxMerchandisingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxMerchandisingTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    SalesWorxMerchandisingBrand *currentMerchandising;
    if (isSearching) {
        currentMerchandising = [filteredBrandsArray objectAtIndex:indexPath.row];
    } else {
        currentMerchandising = [currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray objectAtIndex:indexPath.row];
    }

    cell.titleLbl.text = [NSString getValidStringValue:currentMerchandising.Brand_Code];
    if ([selectedIndexPathsArray containsObject:indexPath]) {
        [cell setSelectedCellStatus];
    }
    else
    {
        [cell setDeSelectedCellStatus];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}

#pragma mark UITableView Delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if (isSearching) {
        currentMerchandising = [filteredBrandsArray objectAtIndex:indexPath.row];
    } else {
        currentMerchandising = [currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray objectAtIndex:indexPath.row];
    }
    
    noBrandSelectedPlaceHolderView.hidden=YES;
    selectedIndexPathsArray =[[NSMutableArray alloc]init];
    [selectedIndexPathsArray addObject:indexPath];
    [brandsTableView reloadData];
    
    NSLog(@"selected segment in did select %ld",merchandisingTitlesSegment.selectedSegmentIndex);
    
    
    [self displaySegmentedData];

    
    /*
    [priceDetailsChildViewController showAvailabilityData:currentMerchandising];
    [brandShareChildViewController showBrandShareData:currentMerchandising.brandShare];
    [planogramChildViewController showPlanogramImagesforBrand:currentMerchandising.planogramDetailsArray];
    
    [competitionChildViewController showCompetitorInfoData:currentMerchandising];
    
    [posChildViewController showPosDataForBrand:currentMerchandising];
    [commentsChildViewController showCommentsforBrand:currentMerchandising];
    [surveyPicturesChildViewController showSurveyPicturesforBrand:currentMerchandising];
    [instoreChildViewController showInStoreActivity:currentMerchandising];*/
}


#pragma mark UIScroll View Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView==merchandisingScrollView){
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        [merchandisingTitlesSegment setSelectedSegmentIndex:page animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Search Bar Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
    }
    else
    {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    selectedIndexPathsArray =[[NSMutableArray alloc]init];
    if([searchText length] != 0) {
        
        noBrandSelectedPlaceHolderView.hidden = NO;
        isSearching = YES;
        [self searchContent];
    }
    else {
        isSearching = NO;
        if (currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray.count > 0) {
            NSLog(@"reloading in search did change");
             
            [brandsTableView reloadData];
            [brandsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                            animated:YES
                                      scrollPosition:UITableViewScrollPositionNone];
        }
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [brandsSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    selectedIndexPathsArray =[[NSMutableArray alloc]init];
    noBrandSelectedPlaceHolderView.hidden = NO;

    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray.count > 0) {
        NSLog(@"reloading in search cancel");
        
        [brandsTableView reloadData];
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(void)searchContent
{
    NSString *searchString = brandsSearchBar.text;
    NSLog(@"searching with text in merchandising %@",searchString);

    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, @"Brand_Code", searchString];
    filteredBrandsArray = [[currentVisit.visitOptions.salesWorxMerchandising.merchandisingArray filteredArrayUsingPredicate:predicate] mutableCopy];
    NSLog(@"filtered customers count %lu", (unsigned long)filteredBrandsArray.count);
    if (filteredBrandsArray.count>0) {
        NSLog(@"reloading in search content");
        
        [brandsTableView reloadData];
    }
    else if (filteredBrandsArray.count==0)
    {
        isSearching=YES;
        NSLog(@"reloading in search content2");
        
        [brandsTableView reloadData];
    }
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(nonnull UIGestureRecognizer *)otherGestureRecognizer
//{
//    return [otherGestureRecognizer.view.superview isKindOfClass:[UITableView class]];
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
