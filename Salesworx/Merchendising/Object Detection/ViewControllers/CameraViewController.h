////////
// This sample is published as part of the blog article at www.toptal.com/blog 
// Visit www.toptal.com/blog and subscribe to our newsletter to read great posts
////////

//
//  CameraViewController.h
//  LogoDetector
//
//  Created by altaibayar tseveenbayar on 13/05/15.
//  Copyright (c) 2015 altaibayar tseveenbayar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/highgui/cap_ios.h>
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"
#import "UIImage+SalesWorxImage.h"
#import "MBProgressHUD.h"

typedef void (^TakeAPhotoCompletionBlock)(UIImage *image);

@protocol PlanogramDelegate

-(void)didCaptureCameraImage:(UIImage*)capturedImage;
-(void)didDetectPlanogramPattern:(NSMutableDictionary*)parmetersDict;
-(void)didFailToDetectPlanogramPattern;

@end


@interface CameraViewController : UIViewController < CvPhotoCameraDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    CGRect imageRect;
    NSTimer* failureTimer;
    id planogramPatternDelegate;
    BOOL patternMatched;
}

@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (strong,nonatomic) TakeAPhotoCompletionBlock photoHandler;
@property (strong, nonatomic) UIImage *templateImage;

@property (nonatomic) id planogramPatternDelegate;



@end
