//
//  SalesWorxMerchandisingInStoreActivityViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingInStoreActivityViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"
#import "SalesWorxDateHelper.h"
@interface SalesWorxMerchandisingInStoreActivityViewController (){
    NSMutableArray * StoreConditionTypesArray;
    SalesWorxDateHelper * dateHelper;
}
@end

@implementation SalesWorxMerchandisingInStoreActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    StoreConditionTypesArray = [NSMutableArray arrayWithObjects:@"Good", @"Needs Improvement", @"Poor",  nil];
    dateHelper = [[SalesWorxDateHelper alloc] init];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    


}
-(void)showInStoreActivity:(SalesWorxMerchandisingBrand*)SelectedMerchandisingBrnadObject{
    inStoreActivityObj = SelectedMerchandisingBrnadObject.inStoreActivity;
    if(SelectedMerchandisingBrnadObject.inStoreActivity == nil){
        inStoreActivityObj = [[MerchandisingInStoreActivity alloc] init];
    }
    StoreconditionTextfield.text = inStoreActivityObj.StoreCondition;
    StoreConditionNotesTextView.text = inStoreActivityObj.StoreConditionNotes;
    
    ManagerInteractionstatusSwitch.on = inStoreActivityObj.ManagerInteractionStatus;
    ManagerInteractionNotesTextview.text = inStoreActivityObj.managerInteractionNotes;
    
    NewSKUListingStatusSwitch.on = inStoreActivityObj.NewSKUListingstatus;
    NewSKUListingNotesTextview.text = inStoreActivityObj.NewSKUListingNotes;
    
    NewPromoActivationStatusSwitch.on = inStoreActivityObj.NewPromoActivationStatus;
    NewPromoActivationNotesTextView.text = inStoreActivityObj.NewPromoActivationNotes;
    
    SelectedMerchandisingBrnadObject.inStoreActivity = inStoreActivityObj;

    
    [self UpdatePromoDateFields];
}
-(void)UpdatePromoDateFields{
    if(!inStoreActivityObj.NewPromoActivationStatus){
        
        inStoreActivityObj.NewPromoActivationstartDate = @"";
        inStoreActivityObj.NewPromoActivationEndDate = @"";
        NewPromoStartDateTextField.text = @"";
        NewPromoEndDateTextField.text = @"";
        [NewPromoEndDateTextField setIsReadOnly:YES];
        [NewPromoStartDateTextField setIsReadOnly:YES];
        
    }else{
        
        NewPromoStartDateTextField.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:inStoreActivityObj.NewPromoActivationstartDate];
        NewPromoEndDateTextField.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:inStoreActivityObj.NewPromoActivationEndDate];
        [NewPromoEndDateTextField setIsReadOnly:NO];
        [NewPromoStartDateTextField setIsReadOnly:NO];

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UITextFieldDelegateMethods


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    inStoreActivityObj.isUpdated=KAppControlsYESCode;
    if(textField    == StoreconditionTextfield){
        currentPopOverViewString = @"Store Condition";
        [self presentPopoverfor:StoreconditionTextfield withTitle:currentPopOverViewString withContent:StoreConditionTypesArray];
        return NO;
    }
    else if(textField == NewPromoStartDateTextField){
        currentPopOverViewString=@"Start Date";
        [self ShowDatePickerForTextField:NewPromoStartDateTextField withTitle:currentPopOverViewString];
        return NO;
    }
    else if(textField == NewPromoEndDateTextField){
        currentPopOverViewString=@"End Date";
        if(inStoreActivityObj.NewPromoActivationstartDate.length == 0){
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select start date first" withController:self];
        }else{
            [self ShowDatePickerForTextField:NewPromoEndDateTextField withTitle:currentPopOverViewString];
        }
        return NO;
    }
    return YES;
}
#pragma mark expirydateMethods
-(void)ShowDatePickerForTextField:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString{
  
    SalesWorxDatePickerPopOverPresentationViewController * popOverVC=[[SalesWorxDatePickerPopOverPresentationViewController alloc]init];
    popOverVC.datePickerPopOverPresentationViewControllerDelegate = self;
    popOverVC.titleString = popOverString;
    
    
    if([currentPopOverViewString isEqualToString:@"Start Date"]){
        popOverVC.datePickerMinimumDate = [dateHelper addToDate:[NSDate date] months:-6];
        popOverVC.previousSelectedDate = [MedRepQueries getDateFromString:inStoreActivityObj.NewPromoActivationstartDate Format:kDatabseDefaultDateFormatWithoutTime];
    }else if([currentPopOverViewString isEqualToString:@"End Date"]){
        popOverVC.datePickerMinimumDate = [MedRepQueries getDateFromString:inStoreActivityObj.NewPromoActivationstartDate Format:kDatabseDefaultDateFormatWithoutTime];
        popOverVC.previousSelectedDate = [MedRepQueries getDateFromString:inStoreActivityObj.NewPromoActivationEndDate Format:kDatabseDefaultDateFormatWithoutTime];

    }
    
    popOverVC.preferredContentSize = CGSizeMake(366, 273);
    [self presentViewController:[SWDefaults CreatePoPoverPresentationNavigationControllerWithViewController:popOverVC AndSourceView:selectedTextField.rightView AndSourceRect:selectedTextField.dropdownImageView.frame AndPerimittedArrowDirections:UIPopoverArrowDirectionAny AndDelegate:self] animated:YES completion:^{
        
    }];
}
-(void)datePicket:(UIDatePicker *)datePicker didSelectDate:(NSDate *)selectedDate{
    /** Displaying selected dates in satrt date and end dates and storing in instoreactivityobjecy*/
    NSString * selectedDateInDatabaseDateWithOuttimeFormat = [MedRepQueries convertNSDatetoDataBaseDateFormat:selectedDate];
    selectedDate = [MedRepQueries getDateFromString:selectedDateInDatabaseDateWithOuttimeFormat Format:kDatabseDefaultDateFormatWithoutTime];
    NSString * selectedDateInDisplayDateWithOuttimeFormat =[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:selectedDate];

    if([currentPopOverViewString isEqualToString:@"Start Date"]){
        
        inStoreActivityObj.NewPromoActivationstartDate = selectedDateInDatabaseDateWithOuttimeFormat;
        NewPromoStartDateTextField.text = selectedDateInDisplayDateWithOuttimeFormat;
        
        /** startdate is greater than enddate , clearing the end date*/
        if(inStoreActivityObj.NewPromoActivationEndDate.length !=0 &&
           [dateHelper date:[MedRepQueries getDateFromString:inStoreActivityObj.NewPromoActivationEndDate Format:kDatabseDefaultDateFormatWithoutTime] isBefore:selectedDate]){
            inStoreActivityObj.NewPromoActivationEndDate = @"";
            NewPromoEndDateTextField.text = @"";
        }
        
    }else if([currentPopOverViewString isEqualToString:@"End Date"]){
        inStoreActivityObj.NewPromoActivationEndDate = selectedDateInDatabaseDateWithOuttimeFormat;
        NewPromoEndDateTextField.text = selectedDateInDisplayDateWithOuttimeFormat;
    }
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray{
    
    SalesWorxPopOverViewController * popOverVC = [[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = contentArray;
    popOverVC.salesWorxPopOverControllerDelegate = self;
    popOverVC.preferredContentSize = CGSizeMake(300, 300);
    popOverVC.disableSearch = NO;
    popOverVC.titleKey = popOverString;
    popOverVC.salesWorxPopOverControllerDelegate = self;
    [self presentViewController:[SWDefaults CreatePoPoverPresentationNavigationControllerWithViewController:popOverVC AndSourceView:selectedTextField.rightView AndSourceRect:selectedTextField.dropdownImageView.frame AndPerimittedArrowDirections:UIPopoverArrowDirectionAny AndDelegate:self] animated:YES completion:^{
        
    }];
    
}

#pragma mark SalesWorxPopoverControllerDelegate
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath{
    if([currentPopOverViewString isEqualToString:@"Store Condition"]){
        inStoreActivityObj.StoreCondition = [StoreConditionTypesArray objectAtIndex:selectedIndexPath.row];
        StoreconditionTextfield.text = inStoreActivityObj.StoreCondition;
    }
}
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath{
    
}
-(void)didDismissPopOverController{
    
}

#pragma mark UItextViewMethods
- (void)textViewDidChange:(UITextView *)textView{
    inStoreActivityObj.isUpdated=KAppControlsYESCode;

    if(textView == StoreConditionNotesTextView){
        inStoreActivityObj.StoreConditionNotes = textView.text;
    }else if(textView == ManagerInteractionNotesTextview){
        inStoreActivityObj.managerInteractionNotes = textView.text;
    }else if(textView == NewSKUListingNotesTextview){
        inStoreActivityObj.NewSKUListingNotes = textView.text;
    }else if(textView == NewPromoActivationNotesTextView){
        inStoreActivityObj.NewPromoActivationNotes = textView.text;
    }
}
#pragma Mark UISwitchMethods
- (IBAction)InStoreActivityStatusSwitchValueChanged:(UISwitch *)sender {
    inStoreActivityObj.isUpdated=KAppControlsYESCode;

    if(sender == ManagerInteractionstatusSwitch){
        inStoreActivityObj.ManagerInteractionStatus = [sender isOn]?YES:NO;
    }else if(sender == NewSKUListingStatusSwitch){
        inStoreActivityObj.NewSKUListingstatus = [sender isOn]?YES:NO;
    }else if(sender == NewPromoActivationStatusSwitch){
        inStoreActivityObj.NewPromoActivationStatus = [sender isOn]?YES:NO;
        [self UpdatePromoDateFields];
    }
}
@end
