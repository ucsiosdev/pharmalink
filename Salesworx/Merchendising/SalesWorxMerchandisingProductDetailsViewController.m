//
//  SalesWorxMerchandisingProductDetailsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/20/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingProductDetailsViewController.h"

@interface SalesWorxMerchandisingProductDetailsViewController ()

@end

@implementation SalesWorxMerchandisingProductDetailsViewController
@synthesize availabilityArray,productAvailabilityTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    availabilityArray=[[NSMutableArray alloc]init];
    productAvailabilityTableView.estimatedRowHeight=64.0f;
    productAvailabilityTableView.rowHeight=UITableViewAutomaticDimension;
    //positionArray=[[NSMutableArray alloc]initWithObjects:@"Good",@"Bad", nil];
    positionArray=[[SWDatabaseManager retrieveManager]fetchAppCodeValueforPOS:@"MERCH_POSITION_DET"];

    
    productAvailabilityTableView.layer.borderColor = kUITableViewBorderColor.CGColor;
    productAvailabilityTableView.layer.borderWidth = 1.0;
    productAvailabilityTableView.layer.cornerRadius = 1.0;
    productAvailabilityTableView.layer.masksToBounds=YES;
    productAvailabilityTableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    productAvailabilityTableView.separatorColor=kUITableViewSaperatorColor;
    productAvailabilityTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
}

-(void)fetchProductsData
{
    
    availabilityArray=[[NSMutableArray alloc]init];
    NSMutableArray* tempArray=[[NSMutableArray alloc]initWithObjects:@"DETTOL SKINCARE HW 200ML PUMP (2+1FREE)",
    @"##AIRWICK ARSL SNDLWD/ROSE 4X(2+1)X300ML [RA10202]",
    @"###DETTOL SOAP SKINCARE 120G WHOLESALE [RC46123]",
    @"DETTOL SKINCARE HW 200ML PUMP (2+1FREE) [RC38202]",
    @"###DETTOL DAILY CARE HANDWASH 6X500ML [RC695]",
    @"AWICK FMATIC RFL MUMS BAKING 6X250ML [RA116]",
    @"AWCK FM RFL TRQUOIS OAS 250ML 2PK 25%OFF [RA11801]", nil];
    
    for (NSInteger i=0; i<tempArray.count; i++) {
        MerchandisingAvailability * currentAvailability= [[MerchandisingAvailability alloc]init];
        currentAvailability.Description=[tempArray objectAtIndex:i];
        currentAvailability.Previous_Stock=[NSDecimalNumber decimalNumberWithString:@"100"];
        currentAvailability.isUpdated=KAppControlsNOCode;
        currentAvailability.Previous_Price=[NSDecimalNumber decimalNumberWithString:@"200"];
        [availabilityArray addObject:currentAvailability];
    }
    resignFirstResponderTableViewTextFields=YES;
    [productAvailabilityTableView reloadData];
   
}

-(void)viewDidAppear:(BOOL)animated
{

}
-(void)showAvailabilityData:(SalesWorxMerchandisingBrand*)merchandisingObject
{
    availabilityArray=[[NSMutableArray alloc]init];
    availabilityArray=merchandisingObject.availabilityArray;
    
    if (availabilityArray.count>0) {
        placeholderView.hidden=YES;
        [productAvailabilityTableView reloadData];
    }
    else{
        placeholderView.hidden=NO;
    }
    

    //[self fetchProductsData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableview data source methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;

}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return self.availabilityTableHeaderView;


}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return availabilityArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier= @"avblCell";
    SalesWorxMerchandisingProductAvailabilityTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxMerchandisingProductAvailabilityTableViewCell" owner:nil options:nil] firstObject];
    }
    if(indexPath.row % 2 == 0) {
        cell.backgroundColor=[UIColor colorWithRed:(245.0/255.0) green:(245.0/255.0) blue:(245.0/255.0) alpha:1];
    }
    else{
        cell.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

    
    
    MerchandisingAvailability * currentAvailability= [availabilityArray objectAtIndex:indexPath.row];
    cell.productNameLabel.text=[NSString getValidStringValue:currentAvailability.Description];
    cell.previousStockLabel.text=[NSString getValidStringValue:[currentAvailability.Previous_Stock stringValue]];
    cell.previousPriceLabel.text=[NSString getValidStringValue:[currentAvailability.Previous_Price stringValue]];
    cell.stockTextField.text=[NSString getValidStringValue:[currentAvailability.Stock stringValue]];
    cell.priceTextField.text=[NSString getValidStringValue:[currentAvailability.Price stringValue]];
    cell.positiontextField.text=[NSString getValidStringValue:currentAvailability.Position];
    
    
    NSString* stockVarienceString=[NSString getValidStringValue:[currentAvailability.Stock_Varience stringValue]];
    
    if (![stockVarienceString hasPrefix:@"-"] && ![NSString isEmpty:stockVarienceString]) {
        stockVarienceString=[NSString stringWithFormat:@"+%@",stockVarienceString];
    }
    
    cell.stockVariancePercentageLabel.text=[NSString isEmpty:stockVarienceString]?@"":[[NSString stringWithFormat:@"%.2f",stockVarienceString.floatValue] stringByAppendingString:@"%"];
    
    
    NSString* pricevarienceString=[NSString getValidStringValue:[currentAvailability.Price_Varience stringValue]];
    
    if (![pricevarienceString hasPrefix:@"-"] && ![NSString isEmpty:pricevarienceString]) {
        pricevarienceString=[NSString stringWithFormat:@"+%@",pricevarienceString];
    }
    
    cell.priceVariencePercentageLabel.text=[NSString isEmpty:pricevarienceString]?@"":[[NSString stringWithFormat:@"%.2f",pricevarienceString.floatValue] stringByAppendingString:@"%"];
    
    
    cell.stockTextField.delegate=self;
    cell.stockTextField.identifier=kStockTextField;
    cell.stockTextField.textAlignment=NSTextAlignmentRight;
    cell.stockTextField.tag=indexPath.row;
    
    cell.priceTextField.delegate=self;
    cell.priceTextField.identifier=kPriceTextField;
    cell.priceTextField.textAlignment=NSTextAlignmentRight;
    cell.priceTextField.tag=indexPath.row;

    cell.positiontextField.delegate=self;
    cell.positiontextField.identifier=kPositionTextField;
    cell.positiontextField.textAlignment=NSTextAlignmentRight;
    cell.positiontextField.tag=indexPath.row;

    return cell;
}

#pragma mark UITextFeild Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    SalesWorxMerchandisingProductAvailabilityTableViewCell * cell = [productAvailabilityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0]];
    
    if ([cell.stockTextField isFirstResponder]) {
        [cell.stockTextField resignFirstResponder];
      //  cell.stockTextField.textAlignment=NSTextAlignmentRight;
        [cell.priceTextField becomeFirstResponder];
    }
    else if ([cell.priceTextField isFirstResponder])
    {
        [cell.priceTextField resignFirstResponder];
        //cell.priceTextField.textAlignment=NSTextAlignmentRight;

        [cell.positiontextField becomeFirstResponder];
    }
    
   
    
    
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    MedRepTextField* currentTxFld=(MedRepTextField*)textField;
    
    if ([currentTxFld.identifier isEqualToString:kPositionTextField]) {
        
        positionTextField=currentTxFld;

        [self.view endEditing:YES];

        [self presentPopoverfor:currentTxFld withTitle:@"Position" withContent:positionArray];

        return NO;
    }
    else{
        
        selectedIndex=textField.tag;
        return YES;
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   
    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    MedRepTextField* currentTxFld=(MedRepTextField*)textField;
    NSString* regexString=[[NSString alloc]init];
    
    regexString= [currentTxFld.identifier isEqualToString:kStockTextField]?KWholeNumberQuantityRegExpression:KDecimalQuantityRegExpression;
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:updatedString options:0 range:NSMakeRange(0, [updatedString length])];

    //NSLog(@"number of matches %ld", numberOfMatches);
    
    
    if (updatedString.length>9) {
        return NO;
    }
    else if (numberOfMatches>0)
    {//if (![NSString isEmpty:updatedString]) {
        

    MerchandisingAvailability * currentAvailability= [availabilityArray objectAtIndex:currentTxFld.tag];
    
    currentAvailability.isUpdated=KAppControlsYESCode;
    
    if ([currentTxFld.identifier isEqualToString:kStockTextField]) {
        currentAvailability.Stock=[NSDecimalNumber ValidDecimalNumberWithString:[NSString getValidStringValue:updatedString]];
        
        //stock varience
        
        
        NSDecimalNumber * previousStockDecimal=currentAvailability.Previous_Stock;
       
        if (!previousStockDecimal) {
            //in case if there is no previous stock
            previousStockDecimal=[NSDecimalNumber decimalNumberWithString:@"0"];
            
        NSDecimalNumber * differenceVal =[[NSDecimalNumber ValidDecimalNumberWithString:[currentAvailability.Stock stringValue]] decimalNumberBySubtracting:previousStockDecimal];
        
        NSDecimalNumber * differencePercentage= [[differenceVal  decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"1"]] decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithDouble:100]];
        
       // NSLog(@"difference percentage is %@", differencePercentage);
//        currentAvailability.Stock_Varience=differencePercentage;
            currentAvailability.Stock_Varience=[[NSDecimalNumber alloc]initWithDouble:100];

        }
        
        else{
            
            NSDecimalNumber * differenceVal =[[NSDecimalNumber ValidDecimalNumberWithString:[currentAvailability.Stock stringValue]] decimalNumberBySubtracting:previousStockDecimal];
            
            NSDecimalNumber * differencePercentage= [[differenceVal  decimalNumberByDividingBy:previousStockDecimal] decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithDouble:100]];
            
            //NSLog(@"difference percentage is %@", differencePercentage);
            currentAvailability.Stock_Varience=differencePercentage;
        }
    }
    else if ([currentTxFld.identifier isEqualToString:kPriceTextField])
    {
        
        NSDecimalNumber * previousPriceDecimal=currentAvailability.Previous_Price;
        
        if (!previousPriceDecimal) {
            //in case if there is no previous stock
            previousPriceDecimal=[NSDecimalNumber decimalNumberWithString:@"0"];
            currentAvailability.Price=[NSDecimalNumber ValidDecimalNumberWithString:[NSString getValidStringValue:updatedString]];
            
            //price varience
            NSDecimalNumber * priceDifferenceVal =[[NSDecimalNumber ValidDecimalNumberWithString:[currentAvailability.Price stringValue]] decimalNumberBySubtracting:previousPriceDecimal];
            
            NSDecimalNumber * priceDifferencePercentage= [[priceDifferenceVal  decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"1"]] decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithDouble:100]];
//            currentAvailability.Price_Varience=priceDifferencePercentage;
            currentAvailability.Price_Varience=[[NSDecimalNumber alloc]initWithDouble:100];

            

        }
        else{
            currentAvailability.Price=[NSDecimalNumber ValidDecimalNumberWithString:[NSString getValidStringValue:updatedString]];
            
            //price varience
            NSDecimalNumber * priceDifferenceVal =[[NSDecimalNumber ValidDecimalNumberWithString:[currentAvailability.Price stringValue]] decimalNumberBySubtracting:previousPriceDecimal];
            
            NSDecimalNumber * priceDifferencePercentage= [[priceDifferenceVal  decimalNumberByDividingBy:previousPriceDecimal] decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithDouble:100]];
            currentAvailability.Price_Varience=priceDifferencePercentage;

        }
        
        
        
    }
    
    [availabilityArray replaceObjectAtIndex:currentTxFld.tag withObject:currentAvailability];
        
    SalesWorxMerchandisingProductAvailabilityTableViewCell * cell = [productAvailabilityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0]];
        
        NSString* stockString=[NSString getValidStringValue:[currentAvailability.Stock stringValue]];
        NSString* priceString=[NSString getValidStringValue:[currentAvailability.Price stringValue]];

        
        cell.productNameLabel.text=[NSString getValidStringValue:currentAvailability.Description];
        
        cell.previousStockLabel.text=[NSString getValidStringValue:[currentAvailability.Previous_Stock stringValue]];
        
        
        cell.previousPriceLabel.text=[NSString getValidStringValue:[currentAvailability.Previous_Price stringValue]];
        
        
        if ([stockString isEqualToString:@"0"]) {
            cell.stockVariancePercentageLabel.text=@"";
        }
        else{
            NSString* stockVarienceString=[NSString getValidStringValue:[currentAvailability.Stock_Varience stringValue]];
            
            if (![stockVarienceString hasPrefix:@"-"] && ![NSString isEmpty:stockVarienceString]) {
                stockVarienceString=[NSString stringWithFormat:@"+%@",stockVarienceString];
            }
            
            cell.stockVariancePercentageLabel.text=[NSString isEmpty:stockVarienceString]?@"":[[NSString stringWithFormat:@"%.2f",stockVarienceString.floatValue] stringByAppendingString:@"%"];
        }
        
        if ([priceString isEqualToString:@"0"]) {
          cell.priceVariencePercentageLabel.text=@"";
        }
        else
        {
            NSString* pricevarienceString=[NSString getValidStringValue:[currentAvailability.Price_Varience stringValue]];
            
            if (![pricevarienceString hasPrefix:@"-"] && ![NSString isEmpty:pricevarienceString]) {
                pricevarienceString=[NSString stringWithFormat:@"+%@",pricevarienceString];
            }
            
            cell.priceVariencePercentageLabel.text=[NSString isEmpty:pricevarienceString]?@"":[[NSString stringWithFormat:@"%.2f",pricevarienceString.floatValue] stringByAppendingString:@"%"];
  
        }
        
        return (numberOfMatches!=0);
     
    }
}


#pragma mark popover methods

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    NSLog(@"content array being passed %@",contentArray);
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    availabilityPopOverController=nil;
    availabilityPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    availabilityPopOverController.delegate=self;
    popOverVC.popOverController=availabilityPopOverController;
    popOverVC.titleKey=popOverString;
    
    [availabilityPopOverController setPopoverContentSize:CGSizeMake(200, 220) animated:YES];
    [availabilityPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
    if (positionTextField!=nil)
    {
    NSString* selectedPosition=[NSString getValidStringValue:[positionArray objectAtIndex:selectedIndexPath.row]];
    NSLog(@"selected position is %@", selectedPosition);
    
    MerchandisingAvailability * currentAvailability= [availabilityArray objectAtIndex:positionTextField.tag];
    currentAvailability.isUpdated=KAppControlsYESCode;
    currentAvailability.Position=selectedPosition;
    [availabilityArray replaceObjectAtIndex:positionTextField.tag withObject:currentAvailability];

    SalesWorxMerchandisingProductAvailabilityTableViewCell * cell = [productAvailabilityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:positionTextField.tag inSection:0]];
    cell.positiontextField.text=selectedPosition;
    
    positionTextField=nil;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did select called");
}

-(void)updateAvailabilityDataforIndex:(NSInteger)idx
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
