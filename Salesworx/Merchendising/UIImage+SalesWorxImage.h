//
//  UIImage+SalesWorxImage.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/5/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SalesWorxImage)
- (UIImage *)normalizedImage;

- (UIImage *)updateOrientation;
-(UIImage*) rotate:(UIImage*) src andOrientation:(UIImageOrientation)orientation;

@end
