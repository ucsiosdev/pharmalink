//
//  SalesWorxMerchandisingInStoreActivityViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxDatePickerPopOverPresentationViewController.h"
@interface SalesWorxMerchandisingInStoreActivityViewController : UIViewController<UIPopoverPresentationControllerDelegate,SalesWorxPopoverControllerDelegate,SalesWorxDatePickerPopOverPresentationViewControllerDelegate>{
    
    IBOutlet MedRepTextField *StoreconditionTextfield;
    IBOutlet MedRepTextView *StoreConditionNotesTextView;
    IBOutlet UISwitch *ManagerInteractionstatusSwitch;
    IBOutlet MedRepTextView *ManagerInteractionNotesTextview;
    IBOutlet UISwitch *NewSKUListingStatusSwitch;
    IBOutlet MedRepTextView *NewSKUListingNotesTextview;
    IBOutlet UISwitch *NewPromoActivationStatusSwitch;
    IBOutlet MedRepTextView *NewPromoActivationNotesTextView;
    IBOutlet MedRepTextField *NewPromoEndDateTextField;
    IBOutlet MedRepTextField *NewPromoStartDateTextField;
    NSString * currentPopOverViewString;
    
    MerchandisingInStoreActivity * inStoreActivityObj;
}
- (IBAction)InStoreActivityStatusSwitchValueChanged:(UISwitch *)sender;
-(void)showInStoreActivity:(SalesWorxMerchandisingBrand*)SelectedMerchandisingBrnadObject;
@end
