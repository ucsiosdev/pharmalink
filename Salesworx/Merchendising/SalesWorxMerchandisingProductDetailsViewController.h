//
//  SalesWorxMerchandisingProductDetailsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/20/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#define kStockTextField @"Stock_TextField"
#define kPriceTextField @"Price_TextField"
#define kPositionTextField @"Position_TextField"


#import <UIKit/UIKit.h>
#import "SalesWorxMerchandisingProductAvailabilityTableViewCell.h"
#import "SalesWorxTableView.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SWDatabaseManager.h"
#import "MerchandisingTextField.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
#import "SalesWorxPopOverViewController.h"


@interface SalesWorxMerchandisingProductDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPopoverControllerDelegate>
{
    BOOL resignFirstResponderTableViewTextFields;
    UIPopoverController* availabilityPopOverController;
    NSMutableArray* positionArray;
    MedRepTextField * positionTextField;
    IBOutlet UIView *placeholderView;
    
    NSInteger  selectedIndex;
}
@property(strong,nonatomic)NSMutableArray* availabilityArray;
@property (strong, nonatomic) IBOutlet UIView *availabilityTableHeaderView;
@property (strong, nonatomic) IBOutlet SalesWorxTableView *productAvailabilityTableView;
-(void)showAvailabilityData:(SalesWorxMerchandisingBrand*)dataArray;

@end
