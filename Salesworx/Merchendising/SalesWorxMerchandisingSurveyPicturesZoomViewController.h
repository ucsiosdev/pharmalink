//
//  SalesWorxMerchandisingSurveyPicturesZoomViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/14/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"
#import "SPUserResizableView.h"
#import "SalesWorxMerchandisingSurveyPictureMarkDetailsViewController.h"

@protocol zoomViewControllerDelegate

-(void)refreshSurveyPicture:(MerchandisingSurveyPictures *)surveyPicture withMarkCount:(int)Count;

@end

@interface SalesWorxMerchandisingSurveyPicturesZoomViewController : UIViewController<SPUserResizableViewDelegate>
{
    id zoomVCDelegate;
    IBOutlet UIImageView *surveyPictureImageView;
    UITapGestureRecognizer *singleTapOnImageView;
    
    SPUserResizableView *currentlyEditingView;
    SPUserResizableView *lastEditedView;
}

@property (strong,nonatomic) MerchandisingSurveyPictures *selectedSurveyPicture;
@property(nonatomic) int markCount;
@property(nonatomic) CGRect smallImageViewFrame;

@property(nonatomic) id zoomVCDelegate;


@end
