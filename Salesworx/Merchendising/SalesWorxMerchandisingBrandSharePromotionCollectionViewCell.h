//
//  SalesWorxMerchandisingBrandSharePromotionCollectionViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/30/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxMerchandisingBrandSharePromotionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPromotion;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;

@end
