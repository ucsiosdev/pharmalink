//
//  SalesWorxMerchandisingPOSTableViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/29/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxMerchandisingPOSTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblProductName;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;


@property(nonatomic) BOOL isHeader;

@end
