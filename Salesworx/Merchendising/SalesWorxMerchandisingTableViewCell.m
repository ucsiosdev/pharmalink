//
//  SalesWorxMerchandisingTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingTableViewCell.h"

@implementation SalesWorxMerchandisingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSelectedCellStatus
{
    self.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    self.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    [self.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    self.dividerLabel.backgroundColor=[UIColor clearColor];

}
-(void)setDeSelectedCellStatus
{
    
    self.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    self.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    [self.contentView setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
    self.dividerLabel.backgroundColor=kUIElementDividerBackgroundColor;


}

@end
