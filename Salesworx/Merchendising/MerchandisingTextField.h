//
//  MerchandisingTextField.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchandisingTextField : UITextField
@property  NSString *identifier;
@property (nonatomic,assign) BOOL canResign;

@end
