//
//  SalesWorxMerchandisingCommentsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingCommentsViewController.h"
#import "SalesWorxPopOverViewController.h"
@interface SalesWorxMerchandisingCommentsViewController ()

@end

@implementation SalesWorxMerchandisingCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)showCommentsforBrand:(SalesWorxMerchandisingBrand*)merchandisingObject
{
    selectedMerchandisingBrand = merchandisingObject;
    if (selectedMerchandisingBrand.commentsArray.count == 0) {
        selectedMerchandisingBrand.commentsArray = [[NSMutableArray alloc]init];
        selectedBrandComment = [[MerchandisingComments alloc]init];
    } else {
        selectedBrandComment = [selectedMerchandisingBrand.commentsArray objectAtIndex:0];
    }
    productTextField.text = [NSString getValidStringValue:selectedBrandComment.productName];
    commentsTextView.text = [NSString getValidStringValue:selectedBrandComment.comment];
    
    productArray=merchandisingObject.productArray;
    
}

#pragma mark TextView Delegate methods

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    [self.view endEditing:YES];
    if (selectedBrandComment.productName.length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"Please select product first" withController:self];
        return NO;
    }
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    selectedBrandComment.comment = textView.text;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if([newString isEqualToString:@" "] || [newString isEqualToString:@"\n"])
    {
        return NO;
    }
    

    NSString *expression = @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.\n";
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:expression] invertedSet];

    NSString *filtered = [[newString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
    return ([newString isEqualToString:filtered] && newString.length<=MerchandisingCommentTextViewLimit);
}

#pragma mark UITextField Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    if (productArray.count == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"No Product found associated with brand" withController:self];
    } else {
        [self presentPopoverfor:productTextField withTitle:@"Product" withContent:[productArray valueForKey:@"Product_Name"]];
    }
    return NO;
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    UIPopoverController *competitorInfoPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    competitorInfoPopOverController.delegate=self;
    popOverVC.popOverController=competitorInfoPopOverController;
    popOverVC.titleKey=popOverString;
    [competitorInfoPopOverController setPopoverContentSize:CGSizeMake(300, 350) animated:YES];
    [competitorInfoPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSString *productName = [NSString getValidStringValue:[[productArray objectAtIndex:selectedIndexPath.row]valueForKey:@"Product_Name"]];
    productTextField.text = productName;
    
    BOOL isSelectedProductCommentAdded = NO;
    if (selectedMerchandisingBrand.commentsArray.count > 0) {
        for (MerchandisingComments *currentComment in selectedMerchandisingBrand.commentsArray) {
            if ([currentComment.productName isEqualToString:productName]) {
                selectedBrandComment = currentComment;
                isSelectedProductCommentAdded = YES;
                break;
            } else {
                isSelectedProductCommentAdded = NO;
            }
        }
    }
    
    if (!isSelectedProductCommentAdded) {
        selectedBrandComment = [[MerchandisingComments alloc]init];
        [selectedMerchandisingBrand.commentsArray addObject:selectedBrandComment];
    }
    
    selectedBrandComment.productName = productName;
    productTextField.text = [NSString getValidStringValue:selectedBrandComment.productName];
    commentsTextView.text = [NSString getValidStringValue:selectedBrandComment.comment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
