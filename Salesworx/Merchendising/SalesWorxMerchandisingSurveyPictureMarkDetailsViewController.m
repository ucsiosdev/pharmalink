//
//  SalesWorxMerchandisingSurveyPictureMarkDetailsViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 1/31/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxMerchandisingSurveyPictureMarkDetailsViewController.h"

@interface SalesWorxMerchandisingSurveyPictureMarkDetailsViewController ()

@end

@implementation SalesWorxMerchandisingSurveyPictureMarkDetailsViewController
@synthesize markID, markDescription, markVCDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    topContentView.backgroundColor = UIViewControllerBackGroundColor;

    markDescTextView.text = [SWDefaults getValidStringValue:markDescription];
}
- (IBAction)closeButtonTapped:(id)sender {
    
    BOOL iskeyboardShowing = false;
    if([markDescTextView isFirstResponder]){
        iskeyboardShowing = YES;
    }
    [self.view endEditing:YES];
    topContentView.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, iskeyboardShowing?0.3 * NSEC_PER_SEC:0), dispatch_get_main_queue(), ^{
        [self dimissViewControllerWithSlideDownAnimation:YES];
    });
}

-(IBAction)btnDeleteMark:(id)sender
{
    BOOL iskeyboardShowing = false;
    if([markDescTextView isFirstResponder]){
        iskeyboardShowing = YES;
    }
    [self.view endEditing:YES];
    topContentView.userInteractionEnabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, iskeyboardShowing?0.3 * NSEC_PER_SEC:0), dispatch_get_main_queue(), ^{
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if ([self.markVCDelegate respondsToSelector:@selector(deleteMark:)]) {
                [self.markVCDelegate deleteMark:markID];
            }
        });
        [self dimissViewControllerWithSlideDownAnimation:YES];
    });
}

- (IBAction)btnSaveMark:(id)sender
{
    BOOL iskeyboardShowing = false;
    if([markDescTextView isFirstResponder]){
        iskeyboardShowing = YES;
    }
    [self.view endEditing:YES];
    
    
    if (markDescTextView.text.length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter mark description" withController:self];
    } else {
        topContentView.userInteractionEnabled = NO;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, iskeyboardShowing?0.3 * NSEC_PER_SEC:0), dispatch_get_main_queue(), ^{
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                if ([self.markVCDelegate respondsToSelector:@selector(saveMarkDescription:WithDesc:)]) {
                    [self.markVCDelegate saveMarkDescription:markID WithDesc:[SWDefaults getValidStringValue:markDescTextView.text]];
                }
            });
            [self dimissViewControllerWithSlideDownAnimation:YES];
        });
    }
}

#pragma mark TextView Delegate methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextField:textView up:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextField:textView up:NO];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    if([newString isEqualToString:@" "] || [newString isEqualToString:@"\n"])
    {
        return NO;
    }
    
    if(textView == markDescTextView)
    {
        return (newString.length <= SurveyPictureMarkCommentTextViewLimit);
    }
    return YES;
}
- (void) animateTextField: (UITextView*) textView up: (BOOL) up
{
    NSInteger movementDistance=50;
    if(textView == markDescTextView)
    {
        movementDistance=130;
    }
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
