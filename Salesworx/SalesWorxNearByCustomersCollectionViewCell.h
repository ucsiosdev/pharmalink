//
//  SalesWorxNearByCustomersCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/19/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"



@interface SalesWorxNearByCustomersCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *distanceLbl;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *customerNumberLbl;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *outStandingLbl;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *lastVisitedOnLbl;
@property (weak, nonatomic) IBOutlet MedRepElementDescriptionLabel *lblInRoute;

@end
