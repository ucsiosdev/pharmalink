//
//  DataTableViewCell.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/11/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "DataTableViewCell.h"

@implementation DataTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
