//
//  DataType.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/26/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataType : NSObject




@end


@interface SurveyQuestion : NSObject
@property (nonatomic,strong) NSString *Question_ID;
@property (nonatomic,strong) NSString *Question_Text;
@property (nonatomic,strong) NSString *Survey_ID;
@property (nonatomic,strong) NSString *Default_Response_ID;
@property (nonatomic,strong) NSString *Response_Type_ID;
@end

@interface SurveyResponse : NSObject
@property (nonatomic,strong) NSString *Response_ID;
@property (nonatomic,strong) NSString *Response_Text;
@property (nonatomic,strong) NSString *Question_ID;
@property (nonatomic,strong) NSString *Response_Type_ID;
@end

@interface TextQuestionAnswer : NSObject
@property (nonatomic,strong) NSString *Question_ID;
@property (nonatomic,strong) NSString *Text;
@end

@interface CheckBoxQuestionAnswer : NSObject
@property (nonatomic,strong) NSString *Question_ID;
@property (nonatomic,strong) NSMutableArray *ResponseIdArray;
@end

@interface radioQuestionAnswer : NSObject
@property (nonatomic,strong) NSString *Question_ID;
@property (nonatomic,strong) NSString *Response_ID;
@end

@interface sliderQuestionAnswer : NSObject
@property (nonatomic,strong) NSString *Question_ID;
@property (nonatomic,strong) NSString *Response_ID;
@end

@interface MediaFile : NSObject
@property (nonatomic,strong) NSString *Media_File_ID;
@property (nonatomic,strong) NSString *Entity_ID_1;
@property (nonatomic,strong) NSString *Entity_ID_2;
@property (nonatomic,strong) NSString *Entity_Type;
@property (nonatomic,strong) NSString *Media_Type;
@property (nonatomic,strong) NSString *Filename;
@property (nonatomic,strong) NSString *Caption;
@property (nonatomic,strong) NSString *Thumbnail;
@property (nonatomic,strong) NSString *Is_Deleted;
@property (nonatomic,strong) NSString *Download_Flag;

//this is only for download media only
@property (nonatomic,strong) NSString *Download_Media_Type;

@end
