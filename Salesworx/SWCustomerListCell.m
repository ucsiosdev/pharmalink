//
//  SWCustomerListCell.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWCustomerListCell.h"
#import "SWFoundation.h"
#import "Singleton.h"
#import "SWDefaults.h"

@implementation SWCustomerListCell

@synthesize statusImageView;
@synthesize codeLabel;


- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        
        [self setStatusImageView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_red"cache:NO]]];
        [self.statusImageView setBackgroundColor:[UIColor clearColor]];
        //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

        
       // [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

        //[self.textLabel setFont:RegularFontOfSize(26.0f)];
      //  self.textLabel.font=UITableViewCellFont;

        [self.textLabel setBackgroundColor:[UIColor whiteColor]];
        [self.contentView addSubview:self.statusImageView];
        
        [self setCodeLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self.codeLabel setBackgroundColor:[UIColor clearColor]];
        //[self.codeLabel setFont:LightFontOfSize(13.0)];
        //self.codeLabel.font=UITableViewCellDescFont;
        
        
        self.textLabel.textColor=UITableViewTitleLabelFontColor;
        self.codeLabel.textColor=UITableViewTitleDescriptionLabelFontColor;
        self.textLabel.font=UITableViewCellFont;
        self.codeLabel.font=UITableViewCellDescFont;
        
        
        [self.contentView addSubview:self.codeLabel];

        
        //adding bottom border custom separator
        
        UIImageView * saperatorImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 54, 1024, 6)];
        saperatorImageView.image=[UIImage imageNamed:@"DividerImage"];
        [self.contentView addSubview:saperatorImageView];
        

        
        
        //adding  custom accessoryIndicator
        
        UIImageView * accessoryImageView=[[UIImageView alloc]initWithFrame:CGRectMake(964, 21, 9, 18)];
        accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        [self.contentView addSubview:accessoryImageView];
        
       // NSLog(@"app control flags are %@",[[SWDefaults appControl] objectAtIndex:0]);

        //

        
        self.contentView.backgroundColor=[UIColor clearColor];
        
       

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.statusImageView setFrame:CGRectMake(self.contentView.bounds.size.width - 16,10, 15,40)];
    CGRect labelFrame = self.textLabel.frame;
    labelFrame.size.width = labelFrame.size.width - 17;
    labelFrame.origin.y = labelFrame.origin.y - 7;
    [self.textLabel setFrame:labelFrame];
    [self bringSubviewToFront:self.statusImageView];
    
    self.separatorInset=UIEdgeInsetsMake(0, 0, 0, 0);
    [self.codeLabel setFrame:CGRectMake(16, 30, 200, 25)];

}



- (NSMutableDictionary *)recursive:(NSMutableDictionary *)dictionary {
    for (NSString *key in [dictionary allKeys]) {
        id nullString = [dictionary objectForKey:key];
        if ([nullString isKindOfClass:[NSDictionary class]]) {
            [self recursive:(NSMutableDictionary*)nullString];
        } else {
            if ((NSString*)nullString == (id)[NSNull null])
                [dictionary setValue:@"" forKey:key];
        }
    }
    return dictionary;
}
- (void)applyAttributes:(NSDictionary *)row
{
    
  NSMutableDictionary*  nullFree=[self recursive:[row mutableCopy]];
    
    row=nullFree;
    
   
    self.textLabel.textColor=UITableViewTitleLabelFontColor;
    self.codeLabel.textColor=UITableViewTitleDescriptionLabelFontColor;
    self.textLabel.font=UITableViewCellFont;
    self.codeLabel.font=UITableViewCellDescFont;
    
    self.backgroundColor=[UIColor clearColor];
    
    
    [self.textLabel setText:[row objectForKey:@"Customer_Name"]];
    [self.codeLabel setText:[row objectForKey:@"Customer_No"]];
    BOOL customerStatus = [[row objectForKey:@"Cust_Status"] isEqualToString:@"Y"];
    BOOL cashCustomer = [[row objectForKey:@"Cash_Cust"] isEqualToString:@"Y"];
    BOOL creditHold = [[row objectForKey:@"Credit_Hold"] isEqualToString:@"Y"];
   
    NSString *imageName = @"Old_transparent";
    
    if (cashCustomer) {
        imageName = @"Old_yellow";
    }
    
    if (!customerStatus || creditHold) {
        imageName = @"Old_red";
        self.textLabel.textColor=[UIColor redColor];
        self.codeLabel.textColor=[UIColor redColor];
    }
    
    
    [self.statusImageView setImage:[UIImage imageNamed:imageName cache:NO]];
}



- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    
    
   // NSLog(@"content subviews are %@", [self.contentView.subviews description]);
    
    
    
    if (highlighted) {
        [self.contentView setBackgroundColor:UITableviewSelectedCellBackgroundColor];
        
        self.codeLabel.textColor=[UIColor whiteColor];
        self.textLabel.textColor=[UIColor whiteColor];
        UIImageView* rightArrow=(UIImageView*)[self.contentView.subviews lastObject];
        rightArrow.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
                
       
    } else {
        self.textLabel.textColor=UITableViewTitleLabelFontColor;
        self.codeLabel.textColor=UITableViewTitleDescriptionLabelFontColor;
        [self.contentView setBackgroundColor:[UIColor clearColor]];

        
        UIImageView* rightArrow=(UIImageView*)[self.contentView.subviews lastObject];
        rightArrow.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        

        

    }
    
}


@end
