//
//  SalesWorxSalesInformationLabel.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxSalesInformationLabel : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
@property (nonatomic) IBInspectable BOOL isTextEndingWithExtraSpecialCharacter;
-(NSString*)text;
-(void)setText:(NSString*)newText;

@end
