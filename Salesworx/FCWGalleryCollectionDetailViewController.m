//
//  FCWGalleryCollectionDetailViewController.m
//  FalconCityofWonders
//
//  Created by Unique Computer Systems on 9/24/14.
//  Copyright (c) 2014 UCS. All rights reserved.
//

#import "FCWGalleryCollectionDetailViewController.h"
#import "SwipeView.h"
@interface FCWGalleryCollectionDetailViewController ()

@end

@implementation FCWGalleryCollectionDetailViewController
@synthesize imageView,image,imageURL,swipeView,imageURLLabel,selectedImageIndex,isImageFromMainBundle;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageView.image=image;
   // NSArray *imageurlSubs=[imageURL componentsSeparatedByString:@"%2"];
    //self.imageURL=[imageurlSubs objectAtIndex:[imageurlSubs count]-1];
    
    
    imageURLLabel.lineBreakMode = NSLineBreakByCharWrapping;
    imageURLLabel.text=imageURL;
    // Do any additional setup after loading the view from its nib.
    swipeView.alignment = SwipeViewAlignmentCenter;
    swipeView.pagingEnabled = YES;
    swipeView.itemsPerPage = 1;
    swipeView.truncateFinalPage = YES;
    [swipeView scrollToItemAtIndex:[selectedImageIndex integerValue] duration:0.1];

}
- (void)viewWillAppear:(BOOL)animated
{
    
    imagePageControl.numberOfPages=self.swipImagesUrls.count;
}
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //generate 100 item views
    //normally we'd use a backing array
    //as shown in the basic iOS example
    //but for this example we haven't bothered
    return [self.swipImagesUrls count];
}
- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
   // NSLog(@"afdsd %@",swipeView.currentItemIndex);
   //  NSArray *imageurlSubs=[[self.swipImagesUrls objectAtIndex:self.swipeView.currentItemIndex] componentsSeparatedByString:@"%2"];
    
    imagePageControl.currentPage=self.swipeView.currentItemIndex;
   // imageURLLabel.text=[imageurlSubs objectAtIndex:[imageurlSubs count]-1];
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (!view)
    {
    	//load new item view instance from nib
        //control events are bound to view controller in nib file
        //note that it is only safe to use the reusingView if we return the same nib for each
        //item view, if different items have different contents, ignore the reusingView value
    	
     }
    view = [[NSBundle mainBundle] loadNibNamed:@"GalleryItemView" owner:self options:nil][0];
    NSString *imgUrl=[self.swipImagesUrls objectAtIndex:index];
    NSLog(@"index %d",index);
    UIImage* galleryImg;
    if([isImageFromMainBundle isEqualToString:@"YES"])
    {
        galleryImg = [UIImage imageNamed:imgUrl];

    }
    else
    {
        galleryImg = [UIImage imageWithContentsOfFile:imgUrl];

    }
    
    //UIImage* galleryImg = [UIImage imageWithContentsOfFile:imgUrl];
    _swipeItemImage.image=galleryImg;
    _swipeItemImage.contentMode=UIViewContentModeScaleAspectFit;
    return view;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return swipeView;
}
- (IBAction)shareImage:(id)sender {
  //  UIImage *imagetoShare=imageView.image;
    
    NSArray* dataToShare = [NSArray arrayWithObjects:[self.swipImagesUrls objectAtIndex:self.swipeView.currentItemIndex], nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    [self presentViewController:activityViewController animated:YES completion:^{}];
}
-(IBAction)DeleteButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        
        
    }];
    [self deleteImageInContectUsImagesDir: [_swipImagesUrls objectAtIndex:self.swipeView.currentItemIndex ]];
    [_swipImagesUrls removeObjectAtIndex:self.swipeView.currentItemIndex ];

    if (_delegate != nil) {
        [_delegate updateImagesArray:_swipImagesUrls];
    }
}

-(IBAction)CloseButtonTapped:(id)sender

{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)deleteImageInContectUsImagesDir:(NSString *)filepath
{
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filepath error:&error];
    if (!success || error) {
        // it failed.
    }
}
- (IBAction)shareButtonTapped:(id)sender {

    
    UIImage* galleryImg;
    NSString *imgUrl=[self.swipImagesUrls objectAtIndex:swipeView.currentItemIndex];

    if([isImageFromMainBundle isEqualToString:@"YES"])
    {
        galleryImg = [UIImage imageNamed:imgUrl];
        
    }
    else
    {
        
        galleryImg = [UIImage imageWithContentsOfFile:imgUrl];
        
    }

    NSArray* dataToShare = [NSArray arrayWithObjects:galleryImg, nil];
    
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:activityViewController animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        self.poc = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [ self.poc  presentPopoverFromRect:shareButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
    //  [self presentViewController:activityViewController animated:YES completion:^{}];
    
}

@end
