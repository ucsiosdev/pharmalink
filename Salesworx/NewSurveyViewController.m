//
//  NewSurveyViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/22/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import "NewSurveyViewController.h"
#import "SWDatabaseManager.h"
#import "SurveyQuestionViewController.h"
#import "SurveyQuestionDetails.h"
#import "SurveyResponseDetails.h"
#import "DataType.h"
#import "FMDB/FMDBHelper.h"


#define saveSurveyResponse @"insert into TBL_Survey_Cust_Responses(Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('%d','%d','%@','%d','%d','%d','%@','%@')"


@interface NewSurveyViewController ()

@end

@implementation NewSurveyViewController
@synthesize surveyQuestionArray=_surveyQuestionArray,survetIDNew,surveyResponseArray=_surveyResponseArray,qID,containerView,scrollView,modifiedSurveyQuestionsArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     modifiedSurveyQuestionsArray=[[NSMutableArray alloc]init];
    
    viewX = 20;
    viewY =30;
    viewWidth = 955;
    
    textAreaQuestionAnswerArray = [[NSMutableArray alloc]init];
    checkBoxQuestionAnswerArray = [[NSMutableArray alloc]init];
    radioQuestionAnswerArray = [[NSMutableArray alloc]init];
    
    
    
    
    textAreaQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    checkBoxQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    radioQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    textAreaAnswerDescriptionArray=[[NSMutableArray alloc]init];


    
    appDelegate.SurveyType = appDelegate.surveyDetails.Survey_Type_Code;
    
    NSLog(@"survey type new is %@", appDelegate.SurveyType);
    
   
    
    
    UIBarButtonItem *Save = [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(Save)];
    
   self.navigationItem.rightBarButtonItem = Save;
    
    self.navigationItem.title=@"";
    
        scrollView.scrollEnabled=YES;
    
       
    scrollView.delegate=self;
    [scrollView setScrollEnabled:YES];
    
    [scrollView setContentSize: CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    

    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    
    
    
    
    
    appDelegate=[DataSyncManager sharedManager];
    self.title =appDelegate.surveyDetails.Survey_Title;
    
    
    if(appDelegate.customerResponseArray)
        [appDelegate.customerResponseArray removeAllObjects];
    
    
    appDelegate.SurveyType = appDelegate.surveyDetails.Survey_Type_Code;
    
    NSLog(@"Survey type is %@",appDelegate.SurveyType);
    
    NSLog(@"survey type code is %@", appDelegate.surveyDetails.Survey_Type_Code);
    
    
    // Get survey question
    NSLog(@"survey id new %d", survetIDNew);
    
    
    NSLog(@"survey id in new %d", appDelegate.surveyDetails.Survey_ID);
    
    saveSurveyID=appDelegate.surveyDetails.Survey_ID;
    
    NSLog(@"saved survey id is %d", saveSurveyID);
    
    
    
    
    //new logs
    
    
    
    
    self.surveyQuestionArray = [[SWDatabaseManager retrieveManager] selectQuestionBySurveyId:appDelegate.surveyDetails.Survey_ID];//survetIDNew];
    
    
    
    
    [self ModifyQuestionResponse];
    
    NSLog(@"new survey questions array count is %d", [modifiedSurveyQuestionsArray count]);
    
    if ([modifiedSurveyQuestionsArray count]>0) {
        [self CreateQuestionsView];
    }
    
    
    
    //got survey questions here
    for (int i=0; i<[self.surveyQuestionArray count]; i++) {
        surveyQuestionDetails = [_surveyQuestionArray objectAtIndex:i];
        NSLog(@"Survey questions new %@", surveyQuestionDetails.Question_Text);
        NSLog(@"Question id is %d", surveyQuestionDetails.Question_ID);
        
        
        //answer here
        self.surveyResponseArray=[[SWDatabaseManager retrieveManager] selectResponseType:surveyQuestionDetails.Question_ID];
        
        for (int j=0; j<[self.surveyResponseArray count]; j++) {
            info=[_surveyResponseArray objectAtIndex:j];
            
            NSLog(@"info tect is %@", info.Response_Text);
            
            NSLog(@"response type is %d", info.Response_Type_ID);
            
            
            

        }
        
        
        
        
        //try switch
        
        
       
       
        
        NSLog(@"response type id before view gets created is %@", [_surveyResponseArray valueForKey:@"Response_Type_ID"]);
        
//        if (info.Response_Type_ID==1) {
//            NSLog(@"radio button");
//            
//            [self createViewForTextAreaQuestion:info withQuestionNumber:i];
//            
//        }
//        
//      if (info.Response_Type_ID==2)
//        {
//            [self createViewForRadioQuestion:info withQuestionNumber:i];
//        }
//        
//        if (info.Response_Type_ID==3)
//        {
//            [self createViewForCheckBoxQuestion:info withQuestionNumber:i];
//        }
//        
    }
    
    NSLog(@"info count is %d", [self.surveyResponseArray count]);

  //  [self BuildScrollPage];
    
    //answers
   
    
//    NSLog(@"question is old %d", surveyQuestionDetails.Question_ID);
//    
//    self.surveyResponseArray  = [[SWDatabaseManager retrieveManager] selectResponseType:surveyQuestionDetails.Question_ID];
//    
//    for (int j=0; j<[self.surveyResponseArray count]; j++) {
//        info =[self.surveyResponseArray objectAtIndex:j];

//    SurveyQuestionDetails* qdetails=[[SurveyQuestionDetails alloc]init];
//    
   
//    
//    
   
//    
//    
    //NSLog(@"new response %@", [[info valueForKey:@"Response_Text"]objectAtIndex:0]);
//    
//    NSLog(@"surder q array new %@", [self.surveyQuestionArray description]);
    
    
    
    
//    SurveyPageControlViewControl* sample=[[SurveyPageControlViewControl alloc]initWithSurveyQuestion:surveyQuestionDetails];
//    
//    NSLog(@"q id fetched %@", [sample.surveyResponseArray description]);
//    
//    
//    SurveyResponseDetails * resp=[sample.surveyResponseArray objectAtIndex:0];
//
//    
//   
//    NSLog(@"info here is %@", resp.Response_Text);
    
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}


-(void)CreateQuestionsView
{
    
    for (int i=0;i<modifiedSurveyQuestionsArray.count;i++) {
       //SurveyQuestionDetails *question = [modifiedSurveyQuestionsArray objectAtIndex:i];
    
        SurveyQuestion* question=[modifiedSurveyQuestionsArray objectAtIndex:i];
        
      if ([question.Response_Type_ID isEqualToString:@"1"]) {
          [self createViewForTextAreaQuestion:question withQuestionNumber:i];

        }
        else if ([question.Response_Type_ID isEqualToString:@"2"])
        {
            [self createViewForRadioQuestion:question withQuestionNumber:i];
        }
        else if ([question.Response_Type_ID isEqualToString:@"3"])
        {
             [self createViewForCheckBoxQuestion:question withQuestionNumber:i];
        }
        
}
}


-(void)ModifyQuestionResponse
{
    
    [modifiedSurveyQuestionsArray removeAllObjects];
   
    
    
  NSLog(@"int is %d", saveSurveyID);
    
    NSString *query = [NSString stringWithFormat:@"SELECT DISTINCT A.*, B.Response_Type_ID FROM TBL_Survey_Questions As A INNER JOIN TBL_Survey_Responses As B ON A.Question_ID=B.Question_ID WHERE A.Survey_ID= %d", saveSurveyID];
    
    NSLog(@"query to fetch questions is %@", query);
    
    
    
    
    NSArray *temp =[FMDBHelper executeQuery:query];
    

    
    NSLog(@"temp is %@", [temp description]);
    
    for (NSMutableDictionary *customerDic in temp) {
        SurveyQuestion *customer = [SurveyQuestion new];
        
        if ([[customerDic valueForKey:@"Question_ID"] isEqual: [NSNull null]]) {
            customer.Question_ID = @"";
        }else{
            customer.Question_ID = [customerDic valueForKey:@"Question_ID"];
        }
        
        if ([[customerDic valueForKey:@"Question_Text"] isEqual: [NSNull null]]) {
            customer.Question_Text = @"";
        }else{
            customer.Question_Text = [customerDic valueForKey:@"Question_Text"];
        }
        
        if ([[customerDic valueForKey:@"Survey_ID"] isEqual: [NSNull null]]) {
            customer.Survey_ID = @"";
        }else{
            customer.Survey_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Survey_ID"]];
        }
        
        if ([[customerDic valueForKey:@"Default_Response_ID"] isEqual: [NSNull null]]) {
            customer.Default_Response_ID= @"";
        }else{
            customer.Default_Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Default_Response_ID"]];
        }
        
        if ([[customerDic valueForKey:@"Response_Type_ID"] isEqual: [NSNull null]]) {
            customer.Response_Type_ID = @"";
        }else{
            customer.Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        }
        
        [modifiedSurveyQuestionsArray addObject:customer];
    
    }
    
}










-(void)createViewForRadioQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber{
    int viewHeightCalculator = 0;
    
    UIView *textQuestionView = [[UIView alloc] initWithFrame: CGRectMake ( viewX, viewY, viewWidth, 100)];
   // UIColor * color =[UIColor colorWithRed:233/255.0 green:232/255.0 blue:235/255.0 alpha:1];
    textQuestionView.backgroundColor = [UIColor clearColor];;
    textQuestionView.tag = questionNumber;
    
    viewHeightCalculator = viewHeightCalculator+10;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (10, viewHeightCalculator, viewWidth-15, 20)];
    NSString * QuestionText = [NSString stringWithFormat:@"%d%@ %@",questionNumber+1,@")",question.Question_Text];
    //UIColor * questionColor =[UIColor colorWithRed:237/255.0 green:148/255.0 blue:0/255.0 alpha:1];
    //questionLabel.textColor = questionColor;
    questionLabel.font = LightFontOfSize(18.0);
    
    //QuestionText = question.Question_Text;
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    
    

//    CGRect textRect = [QuestionText
//                       boundingRectWithSize:maximumLabelSize
//                       options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
//                       attributes:@{NSFontAttributeName: questionLabel.font}
//                       context:nil];
    
//    CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
    
    
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
    
    
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    viewHeightCalculator = viewHeightCalculator+20;
    
    NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
    NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
    NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
    for (NSMutableDictionary *customerDic in array) {
        SurveyResponse *customer = [SurveyResponse new];
        customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
        customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
        customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
        customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        [SurveyResponseArray addObject: customer];
    }
    //button 1
    for (int i= 0; i<SurveyResponseArray.count; i++) {
        SurveyResponse * response = [SurveyResponseArray objectAtIndex:i];
        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.frame = CGRectMake(40, viewHeightCalculator, 32, 32);
        UIImage *btnImage = [UIImage imageNamed:@"Survey_RadioInActive"];
        UIImage *btnImage1 = [UIImage imageNamed:@"Survey_RadioActive"];
        [imageButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [imageButton setBackgroundImage:btnImage1 forState:UIControlStateSelected];
        [imageButton addTarget:self action:@selector(buttonClickedForRadio:) forControlEvents:UIControlEventTouchDown];
        
        imageButton.tag =questionNumber  * 100 + [response.Response_ID intValue];
        NSLog(@" image button tag is %d",imageButton.tag);
        NSLog(@"response id is %@", response.Response_ID);
        
        if ([question.Default_Response_ID isEqualToString:response.Response_ID]) {
            //imageButton.selected =YES;
        }
        
        [textQuestionView addSubview:imageButton];
        
        UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (80, viewHeightCalculator+5, viewWidth-15, 20)];
        questionLabel.textColor = [UIColor darkGrayColor];
        questionLabel.font = LightFontOfSize(18.0);
        NSString * QuestionText = response.Response_Text;
        //QuestionText = @"HelohiiiiiiiiiiiHelohiiiiiiiiiiiHelohi";
        questionLabel.text = QuestionText;
        questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        questionLabel.numberOfLines = 0;
        CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
        
        
        CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
        
        
//        CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
        CGRect questionLabelOldFrame = questionLabel.frame;
        questionLabelOldFrame.size.height = expectedLabelSize.height;
        questionLabelOldFrame.size.width = expectedLabelSize.width;
        questionLabel.frame = questionLabelOldFrame;
        [textQuestionView addSubview:questionLabel];
        
        viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height+25;
    }
    
    viewHeightCalculator = viewHeightCalculator+20;
    
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 20;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    //if (viewY > containerViewOldFrame.size.height) {
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
    //}
    
    UIImageView * imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Divider_horizontal"]];
    imageView.frame =CGRectMake ( viewX, viewY, viewWidth, 2);
    [self.containerView addSubview:imageView];

}



-(void)createViewForTextAreaQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber{
    
    int viewHeightCalculator = 0;
    
    UIView *textQuestionView = [[UIView alloc] initWithFrame: CGRectMake ( viewX, viewY, viewWidth, 100)];
    //UIColor * color =[UIColor colorWithRed:233/255.0 green:232/255.0 blue:235/255.0 alpha:1];
    textQuestionView.backgroundColor = [UIColor clearColor];
    textQuestionView.tag = questionNumber;
    
    viewHeightCalculator = viewHeightCalculator+10;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (10, viewHeightCalculator, viewWidth-15, 20)];
    //UIColor * questionColor =[UIColor colorWithRed:237/255.0 green:148/255.0 blue:0/255.0 alpha:1];
    //questionLabel.textColor = questionColor;
    questionLabel.font = LightFontOfSize(18.0);
    NSString * QuestionText = [NSString stringWithFormat:@"%d%@ %@",questionNumber+1,@")",question.Question_Text];
    //QuestionText = @"HelohiiiiiiiiiiiHelohiiiiiiiiiiiHelohi";
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    
    
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
    
    
    
//    CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    viewHeightCalculator = viewHeightCalculator+20;
    
     txtview =[[UITextView alloc]initWithFrame:CGRectMake(40,viewHeightCalculator,900,100)];
    [txtview setReturnKeyType:UIReturnKeyDone];
    [textQuestionView addSubview:txtview];
    txtview.text = @"";
    [txtview setFont:[UIFont fontWithName:@"Helvetica Neue" size:17.0f]];
    txtview.textColor = [UIColor darkGrayColor];
    txtview.tag = questionNumber * 100+0;
    txtview.delegate = self;
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame: CGRectMake(0,0,900,100)];
    imgView.image = [UIImage imageNamed: @"survey_comments.png"];
    [txtview addSubview: imgView];
    [txtview sendSubviewToBack: imgView];
    
    viewHeightCalculator = viewHeightCalculator+txtview.frame.size.height;
    
    viewHeightCalculator = viewHeightCalculator+20;
    
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 20;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY+100))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    //if (viewY > containerViewOldFrame.size.height) {
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
    // }
    
    UIImageView * imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Divider_horizontal"]];
    imageView.frame =CGRectMake ( viewX, viewY, viewWidth, 2);
    [self.containerView addSubview:imageView];}

-(void)createViewForCheckBoxQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber{
    
    int viewHeightCalculator = 0;
    
    UIView *textQuestionView = [[UIView alloc] initWithFrame: CGRectMake ( viewX, viewY, viewWidth, 150)];
    UIColor * color =[UIColor colorWithRed:233/255.0 green:232/255.0 blue:235/255.0 alpha:1];
    textQuestionView.backgroundColor = [UIColor clearColor];;
    textQuestionView.tag = questionNumber;
    
    viewHeightCalculator = viewHeightCalculator+10;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (10, viewHeightCalculator, viewWidth-15, 23)];
    UIColor * questionColor =[UIColor colorWithRed:237/255.0 green:148/255.0 blue:0/255.0 alpha:1];
    //questionLabel.textColor = questionColor;
    questionLabel.font = LightFontOfSize(18.0);
    
    NSString * QuestionText = [NSString stringWithFormat:@"%d%@ %@",questionNumber +1,@")",question.Question_Text];
    //QuestionText = question.Question_Text;
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    
    
    
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
    

    
    
//    CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
   
    
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    viewHeightCalculator = viewHeightCalculator+110;
    
    NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
    NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
    NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
    for (NSMutableDictionary *customerDic in array) {
        SurveyResponse *customer = [SurveyResponse new];
        customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
        customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
        customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
        customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        [SurveyResponseArray addObject: customer];
    }
    //button 1
    for (int i= 0; i<SurveyResponseArray.count; i++) {
        SurveyResponse * response = [SurveyResponseArray objectAtIndex:i];
        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.frame = CGRectMake(40, viewHeightCalculator-80, 32, 34);
        UIImage *btnImage = [UIImage imageNamed:@"Survey_CheckboxInActive"];
        UIImage *btnImage1 = [UIImage imageNamed:@"Survey_CheckboxActive"];
        [imageButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [imageButton setBackgroundImage:btnImage1 forState:UIControlStateSelected];
        [imageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
        imageButton.tag =questionNumber  * 100 + [response.Response_ID intValue];
        NSLog(@"%d",imageButton.tag);
        if ([question.Default_Response_ID isEqualToString:response.Response_ID]) {
            imageButton.selected =YES;
        }
        
        [textQuestionView addSubview:imageButton];
        
        UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (80, viewHeightCalculator-80, viewWidth-15, 20)];
        questionLabel.textColor = [UIColor darkGrayColor];
        questionLabel.font = LightFontOfSize(18.0);
        NSString * QuestionText = response.Response_Text;
        //QuestionText = @"HelohiiiiiiiiiiiHelohiiiiiiiiiiiHelohi";
        questionLabel.text = QuestionText;
        questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        questionLabel.numberOfLines = 0;
        CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
        
        
        CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
        
        
//        CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
        CGRect questionLabelOldFrame = questionLabel.frame;
        questionLabelOldFrame.size.height = expectedLabelSize.height;
        questionLabelOldFrame.size.width = expectedLabelSize.width;
        questionLabel.frame = questionLabelOldFrame;
        
        [textQuestionView addSubview:questionLabel];
        
        viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height+25;
    }
    
    viewHeightCalculator = viewHeightCalculator+20;
    
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 20;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    //if (viewY > containerViewOldFrame.size.height) {
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
    //}
    
    UIImageView * imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Divider_horizontal"]];
    imageView.frame =CGRectMake ( viewX, viewY, viewWidth, 2);
    [self.containerView addSubview:imageView];
}
- (void)buttonClicked:(UIButton*)button
{
    
    
    NSLog(@"button tag new  %d",button.tag);
    
    
    if (button.selected) {
        button.selected = NO;
    }else{
        button.selected = YES;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    
}
- (void)buttonClickedForRadio:(UIButton*)button
{
    UIView *textQuestionView = button.superview;
    NSLog(@"Hi %d",textQuestionView.tag);
    SurveyQuestion * question = [modifiedSurveyQuestionsArray objectAtIndex:textQuestionView.tag];
    if ([question.Response_Type_ID isEqualToString:@"2"]){
        
        NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
        NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
        NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
        for (NSMutableDictionary *customerDic in array) {
            SurveyResponse *customer = [SurveyResponse new];
            customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
            customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
            customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
            customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
            [SurveyResponseArray addObject: customer];
            
            
            UIButton * allButton  = (UIButton *)[textQuestionView viewWithTag:textQuestionView.tag * 100 + [customer.Response_ID intValue]];
            
            if (allButton.tag == button.tag) {
                allButton.selected = YES;
            }else{
                allButton.selected = NO;
            }
        }
        
    }

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (sender.contentOffset.x != 0) {
        CGPoint offset = sender.contentOffset;
        offset.x = 0;
        sender.contentOffset = offset;
    }
}
-(void)Save
{
    NSLog(@"%d",self.containerView.subviews.count);
    [textAreaQuestionAnswerArray removeAllObjects];
    [checkBoxQuestionAnswerArray removeAllObjects];
    [radioQuestionAnswerArray removeAllObjects];
    
    
    
    [textAreaQuestionAnswerValidationArray removeAllObjects];
    [checkBoxQuestionAnswerValidationArray removeAllObjects];
    [radioQuestionAnswerValidationArray removeAllObjects];

    
    
    for (UIView * subVw in self.containerView.subviews) {
        if (![subVw isKindOfClass:[UILabel class]] && ![subVw isKindOfClass:[UIImageView class]]) {
            NSLog(@"Hi %d",subVw.tag);
            SurveyQuestion * question = [modifiedSurveyQuestionsArray objectAtIndex:subVw.tag];
            if ([question.Response_Type_ID isEqualToString:@"1"]) {
                
                UITextView * text  = (UITextView *)[subVw viewWithTag:subVw.tag * 100 + 0];
                NSLog(@"text %@",text.text);
                
                TextQuestionAnswer * answer = [TextQuestionAnswer new];
                answer.Question_ID = question.Question_ID;
                answer.Text = text.text;
                [textAreaQuestionAnswerArray addObject:answer];
                
            }else if ([question.Response_Type_ID isEqualToString:@"3"]){
                
                NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
                NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
                NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
                
                NSMutableArray * idArray = [[NSMutableArray alloc]init];
                for (NSMutableDictionary *customerDic in array) {
                    SurveyResponse *customer = [SurveyResponse new];
                    customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
                    customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
                    customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
                    customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
                    [SurveyResponseArray addObject: customer];
                    
                    UIButton * button  = (UIButton *)[subVw viewWithTag:subVw.tag * 100 + [customer.Response_ID intValue]];
                    if (button.selected) {
                        NSLog(@"Seletced");
                        [idArray addObject:customer.Response_ID];
                    }else{
                        NSLog(@"NotSeletced");
                    }
                    
                }
                
                CheckBoxQuestionAnswer * checkBoxAns = [CheckBoxQuestionAnswer new];
                checkBoxAns.Question_ID = question.Question_ID;
                checkBoxAns.ResponseIdArray = idArray;
                [checkBoxQuestionAnswerArray addObject:checkBoxAns];
                
            }else if ([question.Response_Type_ID isEqualToString:@"2"]){
                
                NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
                NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
                NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
                
                NSString * responseID;
                for (NSMutableDictionary *customerDic in array) {
                    SurveyResponse *customer = [SurveyResponse new];
                    customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
                    customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
                    customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
                    customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
                    [SurveyResponseArray addObject: customer];
                    
                    UIButton * button  = (UIButton *)[subVw viewWithTag:subVw.tag * 100 + [customer.Response_ID intValue]];
                    
                    if (button.selected) {
                        NSLog(@"Seletced");
                        responseID = customer.Response_ID;
                    }else{
                        NSLog(@"NotSeletced");
                    }
                }
                
                radioQuestionAnswer * radioAnswer = [radioQuestionAnswer new];
                radioAnswer.Question_ID = question.Question_ID;
                radioAnswer.Response_ID =responseID;
                [radioQuestionAnswerArray addObject:radioAnswer];
                
            }
            
        }
    }
    
    if (textAreaQuestionAnswerArray.count > 0) {
        
        for (TextQuestionAnswer * answer in textAreaQuestionAnswerArray) {
            
            for (NSInteger i=0; i<textAreaQuestionAnswerArray.count; i++) {
            
            
            if ([answer.Text isEqualToString:@""]|| answer.Text==nil ||[answer.Text isEqual:[NSNull null]]) {
                
            }
            else
            {
            [textAreaQuestionAnswerValidationArray addObject:answer.Text];
                
                NSLog(@"tect area response data %@", [textAreaQuestionAnswerValidationArray description]);
                
                
                NSLog(@"adding text area response %@", answer.Text);
                
                [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Text];
            }
            
        }
        
    }
    }
    if (checkBoxQuestionAnswerArray.count > 0) {
        
        for (CheckBoxQuestionAnswer *answer in checkBoxQuestionAnswerArray) {
            
            for (NSInteger i = 0; i< answer.ResponseIdArray.count; i++) {
                
                
                if ([answer.ResponseIdArray objectAtIndex:i]==nil || [[answer.ResponseIdArray objectAtIndex:i] isEqual:[NSNull null]] ||[[answer.ResponseIdArray objectAtIndex:i] isEqualToString:@""]) {
                    
                }
                else
                {
                    [checkBoxQuestionAnswerValidationArray addObject:[answer.ResponseIdArray objectAtIndex:i]];
                    
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:[answer.ResponseIdArray objectAtIndex:i]];                }

               
            }
        }
    }
    
    if (radioQuestionAnswerArray.count > 0) {
        
        for (radioQuestionAnswer *answer in radioQuestionAnswerArray) {
            
            
            if (answer.Response_ID==nil|| [answer.Response_ID isEqualToString:@""]) {
              
            }
            
            else
            {
                NSLog(@"radio answer before inserting %@", [radioQuestionAnswerValidationArray description]);
                [radioQuestionAnswerValidationArray addObject:answer.Response_ID];
                [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Response_ID];
 
            }
            
            
            
        }
    }
    
    NSLog(@"arrays to be saved are %@,%@,%@ ",[textAreaQuestionAnswerArray description],[checkBoxQuestionAnswerArray description],[radioQuestionAnswerArray description]);
    
    
    
    finalArray=[NSMutableArray arrayWithObjects:textAreaQuestionAnswerArray,checkBoxQuestionAnswerArray,radioQuestionAnswerArray, nil];
    
    
    NSInteger numberofQuestions=textAreaQuestionAnswerArray.count+checkBoxQuestionAnswerArray.count+radioQuestionAnswerArray.count;
    
    
    
    NSInteger numberofAnswers=radioQuestionAnswerValidationArray.count+checkBoxQuestionAnswerValidationArray.count+textAreaQuestionAnswerValidationArray.count;
    
    
    if (numberofAnswers==0 || numberofAnswers<numberofQuestions) {
        
        [self saveError];
    }
    
    
    NSLog(@"final array description %@", [finalArray description]);
    
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Survey Completed Successfully!" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//    alert.tag=1001;
//    [alert show];

    
}
-(void) saveSurveyCustResponse :(NSString *)questionID AndResponse :(NSString *)response {
    
    
    //surveyIncomplete=NO;
    
    NSDate * startDate = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *startDateString =[formatter stringFromDate:startDate];
//
//   NSString * Customer_Survey_ID =[NSString createGuid];
//   NSString * Survey_ID = [NSString stringWithFormat:@"%d",appDelegate.surveyDetails.Customer_ID];
//    NSString * Question_ID = [NSString stringWithFormat:@"%@",questionID];
////    NSString * Response = response;
//    NSString * Cust_ID = [NSString stringWithFormat:@"%d",  appDelegate.surveyDetails.Customer_ID];
////    NSString * Personnel_Code = [[SWDefaults userProfile] valueForKey:@"Personnel_Code"];
//    NSString * Survey_Timestamp = startDateString;
    
    
    
//    CustomerResponseDetails *responseInfo =[[CustomerResponseDetails alloc] initWithCustomer_Survey_ID:strCustomer_survey_ID Survey_ID:appDelegate.surveyDetails.Survey_ID Question_ID:_surveyQuestion.Question_ID Response:strResponse Customer_ID:appDelegate.surveyDetails.Customer_ID Site_Use_ID:appDelegate.surveyDetails.Site_Use_ID SalesRep_ID:appDelegate.surveyDetails.SalesRep_ID Emp_Code:[[SWDefaults userProfile]stringForKey:@"Emp_Code"] Survey_Timestamp:dateString];
//    
//    NSMutableDictionary *objectDict = [NSMutableDictionary dictionary];
//    [objectDict setValue:[NSString stringWithFormat:@"%d",info.Survey_ID] forKey:@"Survey_ID"];
//    [objectDict setValue:[NSString stringWithFormat:@"%d",info.Customer_ID] forKey:@"Customer_ID"];
//    [objectDict setValue:[NSString stringWithFormat:@"%d",info.Site_Use_ID] forKey:@"Site_Use_ID"];
//    [objectDict setValue:[NSString stringWithFormat:@"%d",info.SalesRep_ID] forKey:@"SalesRep_ID"];
//    [objectDict setValue:[NSString stringWithFormat:@"%d",info.Question_ID] forKey:@"Question_ID"];
    
   
    
    NSString * Customer_Survey_ID =[NSString createGuid];
    
   NSString* survey_ID= [NSString stringWithFormat:@"%d" ,appDelegate.surveyDetails.Survey_ID];
    
    NSString* salesRep_ID= [NSString stringWithFormat:@"%d",appDelegate.surveyDetails.SalesRep_ID];
    
    NSString* empCode=[[SWDefaults userProfile]stringForKey:@"Emp_Code"];
    
    NSString* customer_ID=[[SWDefaults customer] stringForKey:@"Customer_ID"];
    
    NSString* site_Use_ID=[NSString stringWithFormat:@"%d",appDelegate.surveyDetails.Site_Use_ID ];
    
    NSString* Question_ID=[NSString stringWithFormat:@"%@", questionID];
    
    AuditResponseDetail* auditResponse=[[AuditResponseDetail alloc]init];
    
    
    
    NSString* survey_By=[NSString stringWithFormat:@"%d", SurveyedBy];
    
    NSLog(@"survey by %@", survey_By);
    
    
    NSLog(@"new logs");
    NSLog(@"survey old  id %d", appDelegate.surveyDetails.Survey_ID);
   
    
    NSLog(@"question id is %@", questionID);
    NSLog(@"sales rep ID %d", appDelegate.surveyDetails.SalesRep_ID);
    
    NSLog(@"Emp code %@", [[SWDefaults userProfile]stringForKey:@"Emp_Code"]);
    
    NSLog(@"Customer id is %@", [[SWDefaults customer] stringForKey:@"Customer_ID"]);
    
    //NSLog(@"customer survey ID %@", [NSString createGuid]);
    
    
    NSLog(@"new survey type is %@", appDelegate.SurveyType);
    
    if ([appDelegate.SurveyType isEqualToString:@"N"]) {
     
    
    NSString *temp = @"insert into TBL_Survey_Cust_Responses(Customer_Survey_ID,Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')";
    
   
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:Customer_Survey_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString: survey_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:Question_ID];
       
        
       
        if (!response || response==nil ||[response isEqual:[NSNull null]] || [response isEqualToString:@""]) {
            response=@" ";
            surveyIncomplete=YES;
            
            
            
            if (!saveError || ![[NSUserDefaults standardUserDefaults] boolForKey:@"alertShown"]) {
               
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"alertShown"];
                [self saveError];
                
            }
            
        }
        else{
           
            
//            if (textAreaQuestionAnswerValidationArray.count>0 && checkBoxQuestionAnswerValidationArray.count>0 && radioQuestionAnswerValidationArray.count>0 )
//            
//            {
            
           
           
//            for (NSInteger i=0; i<textAreaQuestionAnswerValidationArray.count; i++) {
//                
//                [textAreaQuestionAnswerValidationArray removeObject:@""];
//            }
            
            NSLog(@"radio questions %d", radioQuestionAnswerArray.count );
            NSLog(@"check box questions %d", checkBoxQuestionAnswerArray.count );
            NSLog(@"text area questions %d", textAreaQuestionAnswerArray.count );
            
            NSLog(@"radio answers %d", radioQuestionAnswerValidationArray.count );
            NSLog(@"check box answers %d", checkBoxQuestionAnswerValidationArray.count );
            NSLog(@"text area answers %d", textAreaQuestionAnswerValidationArray.count );
            
            
             questionsCount=radioQuestionAnswerArray.count+checkBoxQuestionAnswerArray.count+textAreaQuestionAnswerArray.count;
            answersCount=radioQuestionAnswerValidationArray.count+checkBoxQuestionAnswerValidationArray.count+textAreaQuestionAnswerValidationArray.count;

            
            
            NSLog(@"questions count is %lu", (unsigned long)questionsCount);
            NSLog(@"answers count is %lu", (unsigned long)answersCount);

            
           // }
            
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:response];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:customer_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString: site_Use_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:salesRep_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString: empCode];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:startDateString];
   
    NSLog(@"temp query is %@", temp);
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InsertSurveyNotification" object:nil];
        
            if (!saveSuccess ) {
                
                
//                if (!response || response==nil ||[response isEqual:[NSNull null]] || [response isEqualToString:@""]) {
//                    surveyIncomplete=YES;
//                }
//                
//                else
//                {
//                    surveyIncomplete=NO;
//                }
                
                if (answersCount>questionsCount || answersCount==questionsCount ) {
            
            
            
            
            
                    [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:temp];

            
            [self saveAlert];
            
                }
                
                else
                {
                    
                    
                    
                    
                }

            }
    
         
    }
    }
    else{
        
        NSString * temp=@"insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By) Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')";
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:Customer_Survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString: survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:Question_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:response];
        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:salesRep_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString: empCode];
        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:startDateString];
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:survey_By];
    
        [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:temp];
 
        if ([response isEqual:@""]) {
            NSLog(@"captured");
        }
    }
    
    
}

    //NSLog(@"app delegate alert message %d", appDelegate.alertMessageShown);
    //appDelegate.key++;
    
    
    


#pragma mark Alert View Delegate Method




-(void)saveError
{
    saveError=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Survey InComplete", nil) message:NSLocalizedString(@"Please fill all details", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    saveError.tag=87952;
    
    
    [saveError show];
}

-(void)saveAlert
{
    saveSuccess=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Survey Completed", nil) message:NSLocalizedString(@"Survey saved successfully", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil)  otherButtonTitles:nil,nil];
    saveSuccess.tag=87760;
    
    
    
    [saveSuccess show] ;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==87760 ) {
       
        if (buttonIndex==1) {
            
            
            if ([self.surveyParentLocation isEqualToString:@"Visit"]) {
[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
                
            }
            
            else
            {
                [self.navigationController popToRootViewControllerAnimated:YES];

            
            }
            
//            [self.navigationController popToRootViewControllerAnimated:YES];
           // [self.navigationController popViewControllerAnimated:YES];

        }
        else{
             [self.navigationController popViewControllerAnimated:YES];
            
        }
        
            
        }
        else if (alertView.tag==87952)
        {
           // [self.navigationController popToRootViewControllerAnimated:YES];
            //yes button action
           
           // saveError=nil;
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"alertShown"];
        }
    
}


#pragma mark textView delegate methods



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
