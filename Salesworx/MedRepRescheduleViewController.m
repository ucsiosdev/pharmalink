

//
//  MedRepRescheduleViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 5/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepRescheduleViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepQueries.h"
#import "MedRepCalanderwithTimeViewController.h"


@interface MedRepRescheduleViewController ()

@end

@implementation MedRepRescheduleViewController

@synthesize isBrandAmbassadorVisit,brandAmbassadorVisit;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Reschedule Visit", nil)];
    
    rescheduledLocation=brandAmbassadorVisit.selectedLocation;
    rescheduledDemoPlan=brandAmbassadorVisit.selectedDemoPlan;
    
    NSString *visitDate = [_visitDetailsDict valueForKey:@"Visit_Date"];
    [_visitDetailsDict setValue:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd/MM/yyyy hh:mm a" scrString:visitDate] forKey:@"Visit_Date"];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (isBrandAmbassadorVisit==YES) {
        
        locationsArray=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchLocationsForBrandAmbassadorVisit];
        demoPlanArray=[MedRepQueries fetchDemoPlanObjects];
        
        self.doctorContactLabel.text=NSLocalizedString(@"Location", nil);
        
        if (brandAmbassadorVisit) {
            
            dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:brandAmbassadorVisit.Date];
        }
    }
    
    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnClose)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=cancelBtn;
    
    UIBarButtonItem *btnSave=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnSave)];
    [btnSave setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=btnSave;
}
-(void)btnClose
{
    if (isBrandAmbassadorVisit) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [self.popOverController dismissPopoverAnimated:YES];
    }
    if ([self.dismissDelegate respondsToSelector:@selector(closeReschedulePopover)])
    {
        [self.dismissDelegate closeReschedulePopover];
    }

    
}

-(void)btnSave
{
    if (doctorTextField.text.length == 0 && dateTextField.text.length == 0 && demoPlanTextField.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Kindly make changes for reschedule visit" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else
    {
        NSMutableDictionary *tempVisitDetails=[_visitDetailsDict mutableCopy];

        BOOL status = NO;

        if (isBrandAmbassadorVisit==YES) {
            status = [MedRepQueries updateVisitDetailsToOldvisit:brandAmbassadorVisit.Planned_Visit_ID];
            [tempVisitDetails setValue:brandAmbassadorVisit.Planned_Visit_ID forKey:@"Old_Visit_ID"];

            
 
        }
        
        else{
        status = [MedRepQueries updateVisitDetailsToOldvisit:[_visitDetailsDict valueForKey:@"Planned_Visit_ID"]];
            [tempVisitDetails setValue:[_visitDetailsDict valueForKey:@"Planned_Visit_ID"] forKey:@"Old_Visit_ID"];

        }
        
        NSString *plannedVisitIDfromCreateVisit;
        if (isBrandAmbassadorVisit==YES) {
            
            if (rescheduledLocation) {
                brandAmbassadorVisit.selectedLocation=rescheduledLocation;
                brandAmbassadorVisit.selectedDemoPlan=rescheduledDemoPlan;
            }
            plannedVisitIDfromCreateVisit=[[SalesWorxBrandAmbassadorQueries retrieveManager] rescheduleBrandAmbassadorVisit:brandAmbassadorVisit];
            
        }
        
        else
        {
            if(status)
            {
                plannedVisitIDfromCreateVisit = [MedRepQueries createVisitandReturnPlannedVisitID:tempVisitDetails];
            }
            
            if (plannedVisitIDfromCreateVisit.length>0)
            {
                [_visitDetailsDict setObject:plannedVisitIDfromCreateVisit forKey:@"Planned_Visit_ID"];
            }
            
        }
        
       // BOOL status=[MedRepQueries rescheduleVisitwithVisitDetails:_visitDetailsDict];
        if (status == YES)
        {
            [self btnClose];
        }
    }
}

#pragma mark UITextField Delegate


-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withContentArray:(NSMutableArray*)contentArray
{
    
    if (tappedTextField == dateTextField) {
        MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        dateFilterVC.datePickerMode=@"dateandTime";
        dateFilterVC.isBrandAmbassadorVisit=YES;
        dateFilterVC.visitDateDelegate=self;
        [self.navigationController pushViewController:dateFilterVC animated:YES];
    }
    else
    {
        SalesWorxBrandAmbassadorContentFilterViewController * contentVC=[[SalesWorxBrandAmbassadorContentFilterViewController alloc]init];
        contentVC.isCreateVisit=YES;
        contentVC.predicateType=kPredicateTypeContains;
        contentVC.selectedObjectDelegate=self;
        if (tappedTextField==doctorTextField) {
            contentVC.keyString=@"Location_Name";
            contentVC.contentArray=locationsArray;
            
        }
        else if (tappedTextField==demoPlanTextField)
        {
            contentVC.contentArray=demoPlanArray;
            contentVC.keyString=@"Description";
        }
        [self.navigationController pushViewController:contentVC animated:YES];
        
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    if (isBrandAmbassadorVisit) {
   if (textField==doctorTextField) {
        
        selectedTextField=kLocationTextField;
        [self textfieldDidTap:doctorTextField withTitle:kLocationTextField withContentArray:locationsArray];
        
    }
    else if (textField==demoPlanTextField)
    {
        selectedTextField=kDemoPlanTextField;
        [self textfieldDidTap:demoPlanTextField withTitle:kDemoPlanTextField withContentArray:demoPlanArray];
        
    }
        else if (textField==dateTextField)
        {
            [self dateandTimeTextFieldTapped];

        }
    }
    else{

    if(textField==doctorTextField)
    {
        [self doctorTextFieldTapped];
    }
    else if(textField==dateTextField)
    {
        [self dateandTimeTextFieldTapped];
    }
    else if(textField==demoPlanTextField)
    {
        [self demonstrationPlantextFieldTapped];
    }
    }
    return NO;
}


//-(void)selectedObject:(id)selectedItem
//{
//    if ([selectedTextField isEqualToString:kLocationTextField]) {
//        
//        SalesWorxBrandAmbassadorLocation * selectedLocation=selectedItem;
//        NSLog(@"selected location is %@", selectedLocation.Location_Name);
//        locationTextField.text=[SWDefaults getValidStringValue:selectedLocation.Location_Name];
//        baCreateVisit.selectedLocation=selectedLocation;
//        
//    }
//    else if ([selectedTextField isEqualToString:kDemoPlanTextField])
//    {
//        DemoPlan * selectedDemoPlan=selectedItem;
//        demoPlanTextField.text=[SWDefaults getValidStringValue:selectedDemoPlan.Description];
//        baCreateVisit.selectedDemoPlan=selectedDemoPlan;
//        
//    }
//}

-(void)selectedObject:(id)selectedItem
{
    if (isBrandAmbassadorVisit) {
        
        if ([selectedTextField isEqualToString:kDemoPlanTextField]) {
            rescheduledDemoPlan=selectedItem;
            demoPlanTextField.text=[SWDefaults getValidStringValue:rescheduledDemoPlan.Description];
            
            [_visitDetailsDict setValue:rescheduledDemoPlan.Description forKey:@"Demonstration_Plan"];
            [_visitDetailsDict setValue:rescheduledDemoPlan.Demo_Plan_ID forKey:@"Demo_Plan_ID"];
            
        }else if ([selectedTextField isEqualToString:kLocationTextField])
        {
            rescheduledLocation=selectedItem;
            [_visitDetailsDict setValue:rescheduledLocation.Location_ID forKey:@"Location_ID"];
            [_visitDetailsDict setValue:rescheduledLocation.Location_Name forKey:@"Location_Name"];
            doctorTextField.text=rescheduledLocation.Location_Name;
            
        }
    }
    
}

- (void)doctorTextFieldTapped
{
    NSMutableArray* contentArray=[[NSMutableArray alloc]init];
    
    if (isBrandAmbassadorVisit==YES) {
        
        SalesWorxBrandAmbassadorContentFilterViewController * contentVC=[[SalesWorxBrandAmbassadorContentFilterViewController alloc]init];
        contentVC.predicateType=kPredicateTypeContains;
        contentVC.selectedObjectDelegate=self;
        contentVC.contentArray= self.contentArray;
            contentVC.keyString=@"Location_Name";
        [self.navigationController pushViewController:contentVC animated:YES];
    }
    
    
   else
   {
       
       
       if ([_locationType isEqualToString:@"P"])
    {
        pharmacyContactDetailsArray = [MedRepQueries fetchContactsforPharmacywithLocationID:_locationID];
        
        popUpString=@"Pharmacy";
        
        doctorpopOverController=nil;
        locationPopOver = nil;
        locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
        locationPopOver.colorNames = pharmacyContactDetailsArray;
        contentArray = [pharmacyContactDetailsArray valueForKey:@"Contact_Name"];
        locationPopOver.delegate = self;
    }
    else
    {
        doctorsArray = [MedRepQueries fetchDoctorswithLocationID:_locationID];
        popUpString=@"Doctors";
        
        doctorpopOverController=nil;
        locationPopOver = nil;
        locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
        
        NSMutableArray* doctorNamesArray=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<doctorsArray.count; i++)
        {
            NSMutableArray *arrDoctorWithSpecilization  = [[NSMutableArray alloc]init];
            
//            NSString *str = [[doctorsArray valueForKey:@"Doctor_Name"] objectAtIndex:i];
//            str = [str stringByAppendingString:@"-"];
//            [doctorNamesArray addObject:[str stringByAppendingString:[[doctorsArray valueForKey:@"Specialization"] objectAtIndex:i]]];
            [arrDoctorWithSpecilization addObject:[[doctorsArray valueForKey:@"Doctor_Name"] objectAtIndex:i]];
            [arrDoctorWithSpecilization addObject:[[doctorsArray valueForKey:@"Specialization"] objectAtIndex:i]];
            
            [doctorNamesArray addObject:arrDoctorWithSpecilization];
            
        }
        
        contentArray = doctorNamesArray;
        locationPopOver.colorNames = doctorNamesArray;
        locationPopOver.delegate = self;
    }
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.customRect=self.view.frame;
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=contentArray;
    filterDescVC.descTitle=popUpString;
    [self.navigationController pushViewController:filterDescVC animated:YES];
   }
}
-(void)demonstrationPlantextFieldTapped
{
    
    
    if (dateTextField.text.length==0)
    {
        UIAlertView * dateAlert=[[UIAlertView alloc]initWithTitle:@"Missing Date" message:NSLocalizedString(@"Please select date before selecting demo plan", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [dateAlert show];
    }
    else
    {
        popUpString=@"Demo Plan";
        demonstrationPlanpopOverController=nil;
        locationPopOver = nil;
        locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
        
        NSMutableArray* demoPlanNamesArray=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<_arrDemoPlan.count; i++)
        {
            [demoPlanNamesArray addObject:[[_arrDemoPlan valueForKey:@"Description"] objectAtIndex:i]];
        }
        
        locationPopOver.colorNames = demoPlanNamesArray;
        locationPopOver.delegate = self;
        
        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
        filterDescVC.filterType=@"Visit";
        filterDescVC.selectedFilterDelegate=self;
        filterDescVC.descTitle=popUpString;
        filterDescVC.customRect=self.view.frame;
        filterDescVC.isCreateVisit=YES;
        filterDescVC.filterDescArray=demoPlanNamesArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
}

-(IBAction)dateandTimeBtnTapped:(id)sender
{
    [self dateandTimeTextFieldTapped];
}

- (void)dateandTimeTextFieldTapped
{
    AppControl *appCntrl = [AppControl retrieveSingleton];
    if ([appCntrl.FM_ENABLE_HOLIDAY isEqualToString:KAppControlsYESCode]) {
        MedRepCalanderwithTimeViewController* calenderVC=[[MedRepCalanderwithTimeViewController alloc]init];
        calenderVC.dateSelected = [MedRepDefaults convertToDateFrom:[MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDatabseDefaultDateFormat scrString:[_visitDetailsDict valueForKey:@"Visit_Date"]]];
        calenderVC.calenderDelegate=self;
        [self.navigationController pushViewController:calenderVC animated:YES];
    } else {
        MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        dateFilterVC.datePickerMode=@"dateandTime";
        dateFilterVC.visitDateDelegate=self;
        [self.navigationController pushViewController:dateFilterVC animated:YES];
    }
}

-(void)selectedDateDelegate:(NSString*)selectedDate
{
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:kDatabseDefaultDateFormat];
    [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithName:@"GMT"];
    [df1 setTimeZone:gmt];
    
    NSDate *dtPostDate = [df1 dateFromString:selectedDate];
    _arrDemoPlan =[[NSMutableArray alloc]init];
    
    BOOL isDemoAvbl=[MedRepDefaults isDemoPlanAvailableforDate:dtPostDate];
    
    if (isDemoAvbl==YES)
    {
        _arrDemoPlan=[MedRepQueries fetchDemoPlansforSelectedDate:dtPostDate];
    }
    dateTextField.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:[NSString stringWithFormat:@"%@",selectedDate]];
    [_visitDetailsDict setValue:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"dd/MM/yyyy hh:mm a" scrString:selectedDate] forKey:@"Visit_Date"];
    brandAmbassadorVisit.Date = selectedDate;
}

-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDatabseDefaultDateFormat scrString:selectedDate];
    
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:kDatabseDefaultDateFormat];
    [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithName:@"GMT"];
    [df1 setTimeZone:gmt];
    
    NSDate *dtPostDate = [df1 dateFromString:refinedDate];
    _arrDemoPlan =[[NSMutableArray alloc]init];
    
    BOOL isDemoAvbl=[MedRepDefaults isDemoPlanAvailableforDate:dtPostDate];

    if (isDemoAvbl==YES)
    {
        _arrDemoPlan=[MedRepQueries fetchDemoPlansforSelectedDate:dtPostDate];
    }
    dateTextField.text = [MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDateFormatWithTime scrString:[NSString stringWithFormat:@"%@",selectedDate]];
    [_visitDetailsDict setValue:selectedDate forKey:@"Visit_Date"];
    brandAmbassadorVisit.Date=refinedDate;
}


#pragma mark selected value delegate

-(void)selectedValueforCreateVisit:(NSIndexPath *)indexPath
{
    if ([popUpString isEqualToString:@"Demo Plan"])
    {
        demoPlanTextField.text=[[NSString stringWithFormat:@" %@", [[_arrDemoPlan valueForKey:@"Description"] objectAtIndex:indexPath.row]] capitalizedString];
        [_visitDetailsDict setValue:[[_arrDemoPlan valueForKey:@"Description"] objectAtIndex:indexPath.row] forKey:@"Demonstration_Plan"];
        [_visitDetailsDict setValue:[[_arrDemoPlan valueForKey:@"Demo_Plan_ID"] objectAtIndex:indexPath.row] forKey:@"Demo_Plan_ID"];
        
        if (isBrandAmbassadorVisit) {
        brandAmbassadorVisit.selectedDemoPlan.Demo_Plan_ID=[SWDefaults getValidStringValue:[[_arrDemoPlan valueForKey:@"Demo_Plan_ID"] objectAtIndex:indexPath.row]];
        brandAmbassadorVisit.selectedDemoPlan.Description=[SWDefaults getValidStringValue:[[_arrDemoPlan valueForKey:@"Description"] objectAtIndex:indexPath.row]];
        }
        
        if (demonstrationPlanpopOverController)
        {
            [demonstrationPlanpopOverController dismissPopoverAnimated:YES];
            demonstrationPlanpopOverController = nil;
            demonstrationPlanpopOverController.delegate=nil;
        }
    }
    else if ([popUpString isEqualToString:@"Doctors"])
    {
        doctorTextField.text=[[NSString stringWithFormat:@" %@", [[doctorsArray valueForKey:@"Doctor_Name"] objectAtIndex:indexPath.row]]capitalizedString];
        [_visitDetailsDict setValue:[[doctorsArray valueForKey:@"Doctor_Name"] objectAtIndex:indexPath.row] forKey:@"Doctor_Name"];
        [_visitDetailsDict setValue:[[doctorsArray valueForKey:@"Doctor_ID"] objectAtIndex:indexPath.row] forKey:@"Doctor_ID"];
        
        if (doctorpopOverController)
        {
            [doctorpopOverController dismissPopoverAnimated:YES];
            doctorpopOverController = nil;
            doctorpopOverController.delegate=nil;
        }
    }
    
    else if ([popUpString isEqualToString:@"Pharmacy"])
    {
        if ([_locationType isEqualToString:@"P"])
        {
            doctorTextField.text=[[NSString stringWithFormat:@" %@", [[pharmacyContactDetailsArray valueForKey:@"Contact_Name"] objectAtIndex:indexPath.row]]capitalizedString];
            [_visitDetailsDict setValue:[[pharmacyContactDetailsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"] forKey:@"Contact_Name"];
            [_visitDetailsDict setValue:[[pharmacyContactDetailsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_ID"] forKey:@"Contact_ID"];
            
            if (doctorpopOverController)
            {
                [doctorpopOverController dismissPopoverAnimated:YES];
                doctorpopOverController = nil;
                doctorpopOverController.delegate=nil;
            }
        }
    }

    NSLog(@"selected index %d", indexPath.row);
    locationPopOver.delegate=nil;
    locationPopOver=nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
