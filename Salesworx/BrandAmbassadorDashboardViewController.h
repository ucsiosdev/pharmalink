//
//  BrandAmbassadorDashboardViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 1/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "SimpleBarChart.h"
#import "BEMSimpleLineGraphView.h"
#import "PNChartDelegate.h"
#import "PNCircleChart.h"
#import "PNChart.h"
#import "SWAppDelegate.h"
#import "GKLineGraph.h"
#import "SalesWorxImageBarButtonItem.h"

@interface BrandAmbassadorDashboardViewController : SWViewController<SimpleBarChartDataSource, SimpleBarChartDelegate,BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate,PNChartDelegate, GKLineGraphDataSource>
{
    PNCircleChart *circleChartForTasks;
    PNCircleChart *circleChartForProductiveVisits;
    
    NSArray *_values;
    SimpleBarChart *_chart;
    
    NSArray *_barColors;
    NSInteger _currentBarColor;
    NSMutableArray *arrproductName;
    
    NSString *monthString;
    BOOL isEmptyGraph;
    IBOutlet UILabel *monthLabel;
    
    NSArray *sortedPlannedVisitArray;
    NSMutableArray *arrVisitFrequency;
    IBOutlet UIImageView *viewDemonstratedGraph;
    SalesWorxImageBarButtonItem *btnTask;
    IBOutlet SalesWorxDefaultButton *btnVisit;
}
@property (weak, nonatomic) IBOutlet UIView *viewUpcomingAppointment;
@property (weak, nonatomic) IBOutlet UILabel *lblDoctorName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblTelephoneNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblATime;


@property (strong, nonatomic) IBOutlet GKLineGraph *gkLineGraph;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *labels;

@property (nonatomic) PNLineChart * lineChart;

@property (strong, nonatomic) IBOutlet BEMSimpleLineGraphView *myGraph;
@property (strong, nonatomic) NSMutableArray *ypoints;
@property (strong, nonatomic) NSMutableArray *xPoints;


@property (weak, nonatomic) IBOutlet UIView *viewFaceTimeVsWaitTime;
@property (weak, nonatomic) IBOutlet UIView *viewVisitfrquency;
@property (weak, nonatomic) IBOutlet UIView *viewDemonstratedProducts;


@property (weak, nonatomic) IBOutlet UIView *viewProductiveVisits;
@property (weak, nonatomic) IBOutlet UILabel *lblProductsDemonstrated;
@property (weak, nonatomic) IBOutlet UILabel *lblSamplesGiven;


@property (weak, nonatomic) IBOutlet UIView *viewTasks;
@property (weak, nonatomic) IBOutlet UILabel *lblTasksOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblTasksCompleted;


@property(nonatomic) NSMutableArray *arrTasksOpen;
@property(nonatomic) NSMutableArray *arrTasksCompleted;

@end
