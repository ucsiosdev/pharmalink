//
//  MedRepMeetingAttendeesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepMeetingAttendeesViewController.h"

@interface MedRepMeetingAttendeesViewController ()

@end

@implementation MedRepMeetingAttendeesViewController
@synthesize meetingAttendeesArray,meetingAttendeesPopOverController,meetingAttendeesTblView,selectedMeetingAttendeesArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cellSelected = [NSMutableArray array];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Meeting Attendees"];
    
//    meetingAttendeesTblView.allowsMultipleSelectionDuringEditing = NO;
//    meetingAttendeesTblView.editing = YES;
//
    
    selectedAttendeesArray=[selectedMeetingAttendeesArray mutableCopy];
    
    UIBarButtonItem * saveButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    
    [saveButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem.rightBarButtonItem=saveButton;
    
    
    UIBarButtonItem* cancelButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    
    [cancelButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=cancelButton;
    
    
    NSLog(@"meeting attendees being passed %@",meetingAttendeesArray);
    
  

    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate=self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    meetingAttendeesTblView.tableHeaderView = self.searchController.searchBar;
    
    [meetingAttendeesTblView reloadData];
    self.definesPresentationContext = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    
    meetingAttendeesTblView.estimatedRowHeight = 44.0;
    meetingAttendeesTblView.rowHeight = UITableViewAutomaticDimension;

}
-(void)cancelTapped
{
    [meetingAttendeesPopOverController dismissPopoverAnimated:YES];
}


-(void)saveTapped
{
    NSLog(@"selected attendees %@", selectedAttendeesArray);
    if ([self.delegate respondsToSelector:@selector(selectedMeetingAttendees:)]) {
        
        [self.delegate selectedMeetingAttendees:selectedAttendeesArray];
        
    }
    [meetingAttendeesPopOverController dismissPopoverAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES) {
        return  filteredPopOverContentArray.count;
    }
    else
    {
    return  meetingAttendeesArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    BOOL isTheObjectThere=NO;
    
    if (isSearching==YES) {
        isTheObjectThere = [selectedAttendeesArray containsObject: [filteredPopOverContentArray objectAtIndex:indexPath.row]];
 
    }
    else{
       isTheObjectThere  = [selectedAttendeesArray containsObject: [meetingAttendeesArray objectAtIndex:indexPath.row]];
    }
    

    
    if ( isTheObjectThere==YES)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    cell.textLabel.font=MedRepSingleLineLabelFont;
    
    cell.textLabel.textColor=MedRepDescriptionLabelFontColor;
    
    if (isSearching==YES) {
        cell.textLabel.text=[SWDefaults getValidStringValue:[filteredPopOverContentArray objectAtIndex:indexPath.row]];

    }
    else
    {
    cell.textLabel.text=[SWDefaults getValidStringValue:[meetingAttendeesArray objectAtIndex:indexPath.row]];
    }
    
    return cell;
    
    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //the below code will allow multiple selection
    [meetingAttendeesTblView deselectRowAtIndexPath:indexPath animated:YES];

    
    
//    NSString* selectedAttendee=[meetingAttendeesArray objectAtIndex:indexPath.row];
//    
//    if ([selectedMeetingAttendeesArray containsObject:selectedAttendee]) {
//     [selectedMeetingAttendeesArray removeObject:selectedAttendee];
//    
//    }
    
    
    if (isSearching==YES) {
        
        if ([selectedAttendeesArray containsObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]]) {
            [selectedAttendeesArray removeObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]];
        }
        else
        {
            [selectedAttendeesArray addObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]];
        }

    }
    else{
        if ([selectedAttendeesArray containsObject:[meetingAttendeesArray objectAtIndex:indexPath.row]]) {
            [selectedAttendeesArray removeObject:[meetingAttendeesArray objectAtIndex:indexPath.row]];
        }
        else
        {
            [selectedAttendeesArray addObject:[meetingAttendeesArray objectAtIndex:indexPath.row]];
        }
    }
    
    [meetingAttendeesTblView reloadData];

}

//-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return YES if you want the specified item to be editable.
//    return YES;
//}

// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//        
//        [selectedAttendeesArray removeObjectAtIndex:indexPath.row];
//        [meetingAttendeesTblView reloadData];
//
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Search DisplayController methods


//- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
//
//
//
//
//    tableView.frame = CGRectMake(0, -20, self.popOverTableView.frame.size.width, self.popOverTableView.frame.size.height);
//
//}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    isSearching=NO;

    if(searchText.length==0)
    {
        filteredPopOverContentArray = [meetingAttendeesArray mutableCopy];
    }
    else  {
        isSearching=YES;

        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF contains[cd] %@",
                                        searchText];
        filteredPopOverContentArray = [[meetingAttendeesArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
        [meetingAttendeesTblView reloadData];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentForSearchText:searchController.searchBar.text
                               scope:@""];
    
}
- (void)didDismissSearchController:(UISearchController *)searchController
{
    self.navigationController.navigationBar.translucent = NO;
    
    filteredPopOverContentArray=[meetingAttendeesArray mutableCopy];
    [meetingAttendeesTblView reloadData];
}
- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
    self.navigationController.navigationBar.translucent = YES;
}

@end
