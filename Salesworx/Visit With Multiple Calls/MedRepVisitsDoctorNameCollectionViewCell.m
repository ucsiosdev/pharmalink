//
//  MedRepVisitsDoctorNameCollectionViewCell.m
//  MedRep
//
//  Created by Prasann on 6/8/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "MedRepVisitsDoctorNameCollectionViewCell.h"

@implementation MedRepVisitsDoctorNameCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MedRepVisitsDoctorNameCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
    
}
@end
