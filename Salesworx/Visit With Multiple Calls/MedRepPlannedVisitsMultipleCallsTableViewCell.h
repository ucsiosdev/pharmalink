//
//  MedRepPlannedVisitsMultipleCallsTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/22/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface MedRepPlannedVisitsMultipleCallsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *visitCallContactNameLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *visitCallContactClassLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *specializationLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *visitCallDemoPlanLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *visitCallStartTimeLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *visitCallFaceTimeLbl;

@end
