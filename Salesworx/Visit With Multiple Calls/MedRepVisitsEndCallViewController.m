//
//  MedRepVisitsEndCallViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepVisitsEndCallViewController.h"

@interface MedRepVisitsEndCallViewController ()

@end

@implementation MedRepVisitsEndCallViewController
@synthesize contentView,notesTxtView,reasonTextField,visitDetailsDict,currentVisit,isBrandAmbassadorVisit,currentBrandAmbassadorVisit, isCloseFromEdetailingStartCall;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    
    UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close Visit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeVisitTapped)];
    
    [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=closeVisitButton;
    
    
    
    UIBarButtonItem *cancelVisitButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    
    
    [cancelVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem=cancelVisitButton;
    
    if (isCloseFromEdetailingStartCall) {
        lblReasonTitle.text = @"Please select a reason for closing this call without demonstrating any product.";
        lblNavigationTitle.text = @"Close Call";
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{

    if(self.isMovingToParentViewController)
    {
        contentView.backgroundColor=UIViewControllerBackGroundColor;
        notesTxtView.backgroundColor=[UIColor whiteColor];
        visitAdditionalInfoDict=[[NSMutableDictionary alloc]init];
        
        [saveButton.titleLabel setFont:NavigationBarButtonItemFont];
        [cancelButton.titleLabel setFont:NavigationBarButtonItemFont];
        if (isBrandAmbassadorVisit) {
            currentBrandAmbassadorVisit.Visit_Conclusion_Notes=[[NSString alloc]init];

        }
        else
        {
        [visitDetailsDict removeObjectForKey:@"Visit_Conclusion_Notes"];
        }
        for (UIView *borderView in contentView.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                if (borderView.tag==101) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                    
                }
            }
            
        }
        
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
       
        if (isBrandAmbassadorVisit==YES) {
            reasonsArray=[[MedRepQueries fetchReasonsforNoCallBrandAmbassadorVisit] mutableCopy];
  
        }
        else{
            reasonsArray=[[MedRepQueries fetchReasonsforNoCall] mutableCopy];

        }
        [reasonsArray addObject:kOtherReasonType];
        
        [self updatedConstraintswithReasonCode:NO];
    }
    
    else
        {
            
            NSString* reasonText=reasonTextField.text;
            [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionReveal];
            [contentView setHidden:NO];

            if ([NSString isEmpty:reasonText]==NO && [reasonText isEqualToString:kOtherReasonType]) {
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(focusTheNotesTextViewAfterAnimation) userInfo:nil repeats:NO];

            }
    }

    
}
-(void)focusTheNotesTextViewAfterAnimation
{
    if(![notesTxtView isHidden])
    [notesTxtView becomeFirstResponder];
}

-(void)updatedConstraintswithReasonCode:(BOOL)reasonFlag
{
    if (reasonFlag==NO) {
        notesViewHeightConstraint.constant=0.0f;
        notesView.hidden=YES;
        contentViewHeightConstraint.constant=152.0f;
    }
    else
    {
        notesView.hidden=NO;
        notesViewHeightConstraint.constant=148.0f;
        contentViewHeightConstraint.constant=300.0f;
    }
 
}

-(void)viewDidAppear:(BOOL)animated
{
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
   
}


-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}




#pragma mark AnimationMethods
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
#pragma mark buttopnActions
- (IBAction)doneButtontapped:(id)sender {
    
    NSLog(@"visit details in save visit %@",visitDetailsDict);
    
    
    
    if ([notesTxtView isFirstResponder]) {
        
        [notesTxtView resignFirstResponder];
    }
    
    NSString* conclusionNotes=[[NSString alloc]init];
    if (isBrandAmbassadorVisit) {
        conclusionNotes=currentBrandAmbassadorVisit.Visit_Conclusion_Notes;

    }
    else
    {
     conclusionNotes=[visitDetailsDict valueForKey:@"Visit_Conclusion_Notes"];
    }
    if ([NSString isEmpty:conclusionNotes]==YES   ) {
        if([NSString isEmpty:reasonTextField.text])
            [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select reason" withController:nil];
        else
            [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter notes" withController:nil];
        
        if ([selectedReason isEqualToString:kOtherReasonType]) {
            [notesTxtView becomeFirstResponder];
        }
        else{
            [notesTxtView resignFirstResponder];
        }
        
  
    }
    
    else
    {
    
        [[NSUserDefaults standardUserDefaults]setObject:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"Call_End_Date"];
        
        BOOL status=NO;
        
        if (isBrandAmbassadorVisit) {
            status=[[SalesWorxBrandAmbassadorQueries retrieveManager] saveBrandAmbassadorVisitWithOutCalls:currentBrandAmbassadorVisit];
        }
        else
        {
            if (isCloseFromEdetailingStartCall) {
                status=[MedRepQueries saveVisitDetailsOnCloseAfterStartCall:visitDetailsDict andVisitDetails:currentVisit];
            } else {
                status=[MedRepQueries saveVisitDetailsWithOutCall:visitDetailsDict AdditionalInfo:visitAdditionalInfoDict andVisitDetails:currentVisit];
            }
        }
    
    if (status==YES) {
        
        popToRoot=YES;
        
        if (isBrandAmbassadorVisit) {

            [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:currentBrandAmbassadorVisit];

        }
        else
        {
            if (!isCloseFromEdetailingStartCall) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:currentVisit];
            }
        }
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];

    }
        else
        {
            
        }
    
    }
   
}

- (IBAction)closeButtonTapped:(id)sender {
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];
}


- (void)CloseSyncPopOver
{
    //do what you need to do when animation ends...
    
    
  
    
   
    
    if (popToRoot==YES) {

        if ([self.visitCompletedDelegate respondsToSelector:@selector(visitCompletedWithOutCalls:)]) {
            
            [self.visitCompletedDelegate visitCompletedWithOutCalls:YES];
        }
        
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldMethods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (textField==reasonTextField) {
        
        [contentView setHidden:YES];

        MedRepMultiCallsCloseVisitReasonCodeViewController *reasonVC= [[MedRepMultiCallsCloseVisitReasonCodeViewController alloc]init];
        
        reasonVC.reasonCodeDelegate=self;
       // reasonVC.contentViewHeight=contentViewHeightConstraint.constant+200;

        reasonVC.contentArray=reasonsArray;
        [notesTxtView resignFirstResponder];
        
        [self.navigationController pushViewController:reasonVC animated:NO];
    }
    
    return NO;
}

-(void)selectedReasonCode:(NSString*)selectedReasonCode
{
    selectedReason=[MedRepDefaults getDefaultStringForEmptyString:selectedReasonCode];
    reasonTextField.text=[MedRepDefaults getDefaultStringForEmptyString:selectedReasonCode];

    if ([selectedReasonCode isEqualToString:kOtherReasonType]) {
        
        notesTxtView.text=@"";
        
        [self updatedConstraintswithReasonCode:YES];
        notesTxtView.userInteractionEnabled=YES;
        //[notesTxtView becomeFirstResponder];

    }
    else
    {
    
        //notesTxtView.text=selectedReason;
        [notesTxtView resignFirstResponder];
        notesTxtView.userInteractionEnabled=NO;
        [self updatedConstraintswithReasonCode:NO];
        

        if (isBrandAmbassadorVisit)
        {
            currentBrandAmbassadorVisit.Visit_Conclusion_Notes=selectedReason;
        }
        else
        {
        [visitDetailsDict setValue:selectedReason forKey:@"Visit_Conclusion_Notes"];
        }
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITextViewMethods

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (isBrandAmbassadorVisit) {
        currentBrandAmbassadorVisit.Visit_Conclusion_Notes=textView.text;
    }
    else
    {
    [visitDetailsDict setValue:[SWDefaults getValidStringValue:textView.text] forKey:@"Visit_Conclusion_Notes"];
    }
    [self animateTextField:textView up:NO];

}



- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextField:textView up:YES];

}


-(void)closeVisitTapped
{
    
}


- (void) animateTextField: (UITextView*) textView up: (BOOL) up
{
    NSInteger movementDistance=50;
    if(textView==notesTxtView)
    {
        movementDistance=130;
    }
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
@end
