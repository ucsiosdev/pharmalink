


//
//  MedRepVisitsStartMultipleCallsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepVisitsStartMultipleCallsViewController.h"
#import "MedRepQueries.h"
#import "SWDefaults.h"
#import "MedRepVisitsMultiCallsTableViewCell.h"
#import "SalesWorxImageBarButtonItem.h"
#import "SalesworxDocumentDisplayManager.h"
#import "MedRepVisitsDoctorNameCollectionViewCell.h"


@interface MedRepVisitsStartMultipleCallsViewController ()
{
    SalesworxDocumentDisplayManager * docDispalayManager;
    SalesWorxImageBarButtonItem* startVisitBarButtonItem;

}
@end

@implementation MedRepVisitsStartMultipleCallsViewController
@synthesize visitDetailsDict,segmentControl,mediaScrollView,visitDetails,demoPlanTextField,callsTableView, isFromDashboard,demoPlanProductsCollectionView,filterButton;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    appControl= [AppControl retrieveSingleton];
    docDispalayManager=[[SalesworxDocumentDisplayManager alloc] init];
    isDemoPlanSelected = false;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Edetailing Start Call", nil)];
    selectedMediaContentArray=[[NSMutableArray alloc]init];
    NSString* nextVisitObjectiveTitle = [MedRepQueries fetchDescriptionForAppCode:kNextVisitObjectiveTitleAppCode];
    if ([NSString isEmpty:[SWDefaults getValidStringValue:nextVisitObjectiveTitle]])
    {
        nextVisitObjectiveTitle = kNextVisitObjective;
    }
    nextVisitObjectiveTitleLabel.text =nextVisitObjectiveTitle;
    
    timeOfArrivalLbl.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[SWDefaults getValidStringValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callDidFinishNotification:)
                                                 name:@"CallDidFinishNotification"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveVisitDidFailNotification:)
                                                 name:@"VisitDidFailNotification"
                                               object:nil];
    // Do any additional setup after loading the view from its nib.
    
    if ([visitDetails.Location_Type isEqualToString:kDoctorLocation] && [appControl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode]) {
        addDoctorButton.enabled = NO;
    }
    
    for (int i = 0; i<[visitDetails.doctorsforLocationArray count]; i++) {
        Doctors *currentDoc = [visitDetails.doctorsforLocationArray objectAtIndex:i];
        if ([[currentDoc.Doctor_ID capitalizedString]  isEqualToString:[visitDetails.selectedDoctor.Doctor_ID capitalizedString]] && [[currentDoc.Doctor_Name capitalizedString] isEqualToString:[visitDetails.selectedDoctor.Doctor_Name capitalizedString]]) {
            selectedIndexPathOfDoctorCollectionView = [NSIndexPath indexPathForItem:i inSection:0];
            
            [self moveObjectFromIndex:selectedIndexPathOfDoctorCollectionView.row toIndex:0 OfArray:visitDetails.doctorsforLocationArray];
            break;
        }
    }
    
    for (int i = 0; i<[visitDetails.locationContactsArray count]; i++) {
        Contact *currentContact = [visitDetails.locationContactsArray objectAtIndex:i];
        if ([[currentContact.Contact_ID capitalizedString] isEqualToString:[visitDetails.selectedContact.Contact_ID capitalizedString]] && [[currentContact.Contact_Name capitalizedString] isEqualToString:[visitDetails.selectedContact.Contact_Name capitalizedString]]) {
            selectedIndexPathOfDoctorCollectionView = [NSIndexPath indexPathForItem:i inSection:0];
            
            [self moveObjectFromIndex:selectedIndexPathOfDoctorCollectionView.row toIndex:0 OfArray:visitDetails.locationContactsArray];
            break;
        }
    }
    
    if ([appControl.FM_VISIT_WITH_PLANNED_DOCTOR_ONLY isEqualToString:KAppControlsYESCode]) {
        addDoctorButton.hidden = YES;
        for (int i = 0; i<[visitDetails.doctorsforLocationArray count]; i++) {
            Doctors *currentDoc = [visitDetails.doctorsforLocationArray objectAtIndex:i];
            if ([[currentDoc.Doctor_ID capitalizedString]  isEqualToString:[visitDetails.selectedDoctor.Doctor_ID capitalizedString]] && [[currentDoc.Doctor_Name capitalizedString] isEqualToString:[visitDetails.selectedDoctor.Doctor_Name capitalizedString]]) {
                
                visitDetails.doctorsforLocationArray = [[NSMutableArray alloc]init];
                [visitDetails.doctorsforLocationArray addObject:currentDoc];
                break;
            }
        }
        
        for (int i = 0; i<[visitDetails.locationContactsArray count]; i++) {
            Contact *currentContact = [visitDetails.locationContactsArray objectAtIndex:i];
            if ([[currentContact.Contact_ID capitalizedString] isEqualToString:[visitDetails.selectedContact.Contact_ID capitalizedString]] && [[currentContact.Contact_Name capitalizedString] isEqualToString:[visitDetails.selectedContact.Contact_Name capitalizedString]]) {

                visitDetails.locationContactsArray = [[NSMutableArray alloc]init];
                [visitDetails.locationContactsArray addObject:currentContact];
                break;
            }
        }
    }
}
- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to OfArray:(NSMutableArray *)array
{
    if (to != from) {
        id obj = [array objectAtIndex:from];

        [array removeObjectAtIndex:from];
        if (to >= [array count]) {
            [array addObject:obj];
        } else {
            [array insertObject:obj atIndex:to];
        }
    }
    selectedIndexPathOfDoctorCollectionView = [NSIndexPath indexPathForItem:0 inSection:0];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //stop wait timer and update value
    
    if (goToNextScreen)
    {
//        [[NSUserDefaults standardUserDefaults]setObject:waitTimeLbl.text forKey:@"waitTimer"];
//        
//        NSLog(@"WAIT TIME WHILE DISAPPEARING %@", [[NSUserDefaults standardUserDefaults]objectForKey:@"waitTimer"]);
//        
//        
//        [waitTimer invalidate];
//        waitTimer=nil;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)setCallStartDate
{
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
    return callStartDate;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if(startVisitBarButtonItem!=nil){
        startVisitBarButtonItem.enabled = NO;
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(enableStartCallButton) userInfo:nil repeats:NO];
    }
    

    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingMultipleVisits];
        
        if ([appControl.FM_SHOW_WOCOACH isEqualToString:KAppControlsYESCode] && [appControl.FM_ENABLE_MULTISEL_ACCOMP_BY isEqualToString:KAppControlsNOCode]) {
            accompaniedByArray = [[NSMutableArray alloc]init];
            
            for (int i =0; i<2; i++) {
                NSMutableDictionary *dicAccompaniedBy = [[NSMutableDictionary alloc]init];
                
                if (i==0) {
                    [dicAccompaniedBy setObject:@"With Coach" forKey:@"Username"];
                    [dicAccompaniedBy setObject:[NSString stringWithFormat:@"%d",-1] forKey:@"User_ID"];
                } else {
                    [dicAccompaniedBy setObject:@"Without Coach" forKey:@"Username"];
                    [dicAccompaniedBy setObject:[NSString stringWithFormat:@"%d",0] forKey:@"User_ID"];
                }
                accompaniedByArray = (NSMutableArray *)[accompaniedByArray arrayByAddingObject:dicAccompaniedBy];
            }
        }
        else{
            accompaniedByArray = [MedRepQueries fetchAccompaniedByData];
        }
        
        NSLog(@"accompanied by array is %@",accompaniedByArray);
        
        
        //prepopulating if array count is 1
        
        if (accompaniedByArray.count==1) {
            accompaniedByTextField.isReadOnly=YES;
            accompaniedByTextField.text=[SWDefaults getValidStringValue:[[accompaniedByArray objectAtIndex:0] valueForKey:@"Username"]];
            accompaniedBy=[SWDefaults getValidStringValue:[[accompaniedByArray objectAtIndex:0] valueForKey:@"User_ID"]];
            visitDetails.Accompanied_By=accompaniedBy;
            [visitDetailsDict setObject:accompaniedBy forKey:@"Accompanied_By"];
        }
        
        
        
        if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            selectedDoctor = visitDetails.selectedDoctor;
            doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[SWDefaults getValidStringValue:selectedDoctor.Doctor_Name]];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedDoctor.Doctor_ID] forKey:@"Doctor_ID"];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedDoctor.Doctor_Name] forKey:@"Doctor_Name"];
        }
        else
        {
            selectedContact = visitDetails.selectedContact;
            doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[SWDefaults getValidStringValue:selectedContact.Contact_Name]];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedContact.Contact_ID] forKey:@"Contact_ID"];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedContact.Contact_Name] forKey:@"Pharmacy_Contact_Name"];
        }
        
        NSLog(@"visit details dict is %@", visitDetailsDict);
        

        
        
    }
    
        
        
    [waitTimer invalidate];
    waitTimer = nil;
    
    if (!isDemoPlanSelected) {
        testDate=[NSDate date];
    }
    
    waitTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
    
    timeOfArrivalLbl.font=kSWX_FONT_SEMI_BOLD(18);
    waitTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    
    waitTimeLbl.textColor = KWaitTimeColor;

    [self refreshTaskandNotesStatus:self.visitDetails];
    
    [[NSUserDefaults standardUserDefaults]setObject:[self setCallStartDate] forKey:@"Call_Start_Date"];
    
    NSLog(@"setting call start date %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"Call_Start_Date"]);
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_End_Date"];
    
    
    if (self.isMovingToParentViewController) {
        
        demoPlanObjectsArray=[MedRepQueries fetchDemoPlanObjects];
        
       // NSLog(@"demo plan objects are %@", demoPlanObjectsArray);
        
        
        NSPredicate * selectedDemoPlanPredicate=[NSPredicate predicateWithFormat:@"SELF.Demo_Plan_ID.integerValue=%d",visitDetails.Demo_Plan_ID.integerValue];
        
        NSArray *filteredArray = [demoPlanObjectsArray filteredArrayUsingPredicate:selectedDemoPlanPredicate];
        if (filteredArray.count>0) {
            selectedDemoPlan=[filteredArray objectAtIndex:0];
            demoPlanTextField.text=selectedDemoPlan.Description;
            visitDetails.selectedDemoPlan=selectedDemoPlan;
            
        }
        
        
        VisitTask * task=[[VisitTask alloc]init];
        
        
        for (NSInteger i=0; i<self.visitDetails.taskArray.count; i++) {
            
            task= [self.visitDetails.taskArray objectAtIndex:i];
            
            NSLog(@"status in will appear %@", task.Status);
            
        }
        
        
        
        //objectiveTxtView.contentInset = UIEdgeInsetsMake(0, 6, 0, 0);
        
        
        
        
        //adding bar button items
        
        
        UIBarButtonItem* taskBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"medRepTasks.png"] style:UIBarButtonItemStylePlain target:self action:@selector(taskBarButtonTapped:)];
        
        
        //    UIBarButtonItem* taskBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"T" style:UIBarButtonItemStylePlain target:self action:@selector(taskBarButtonTapped:)];
        
        
        
        UIBarButtonItem* notesBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"medRepNotes.png"] style:UIBarButtonItemStylePlain target:self action:@selector(notesBarButtonTapped:)];
        
        
        //    UIBarButtonItem* notesBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"N" style:UIBarButtonItemStylePlain target:self action:@selector(notesBarButtonTapped:)];
        
        
        
        startVisitBarButtonItem=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartCall"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitBarButtonTapped:)];
        
        SalesWorxImageBarButtonItem* createPresentationButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"CreatePresentation"] style:UIBarButtonItemStylePlain target:self action:@selector(createPresentationTapped)];

         NSArray*barButtonArray=[[NSArray alloc]initWithObjects:startVisitBarButtonItem, nil];
        self.navigationItem.rightBarButtonItems=barButtonArray;

        // self.navigationItem.rightBarButtonItem=startVisitBarButtonItem;

        
        // NSArray*barButtonArray=[[NSArray alloc]initWithObjects:taskBarButtonItem,notesBarButtonItem,startVisitBarButtonItem, nil];

        //    UIBarButtonItem* startVisitBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"S" style:UIBarButtonItemStylePlain target:self action:@selector(startVisitBarButtonTapped:)];
        
        // NSArray*barButtonArray=[[NSArray alloc]initWithObjects:taskBarButtonItem,notesBarButtonItem,startVisitBarButtonItem, nil];
        
        //    self.navigationItem.rightBarButtonItems=barButtonArray;
        
        

        
        //    objectiveTxtView.layer.borderColor = UITextViewBorderColor.CGColor;
        //    objectiveTxtView.layer.borderWidth = 1.0;
        //    objectiveTxtView.layer.cornerRadius = 3;
        
        
        //   accompaniedByLbl .layer.borderColor = UIMapViewViewBorderColor.CGColor;
        //    accompaniedByLbl.layer.borderWidth = 1.0;
        //    accompaniedByLbl.layer.cornerRadius = 3.0;
        
        
        
        if (visitDetailsDict.count>0) {
            
            NSLog(@"visit details in start edetailing %@", [visitDetailsDict description]);
            locationNameLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Location_Name"]];
            
            // locationNameTextField.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Location_Name"]];
            
            
            
            
            if ([[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
                
                // doctorLbl.text=@"N/A";
                
                //this might be a pharmacy
                
                
                doctorContactHeaderLbl.text=@"Contact Name";
                
                doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Pharmacy_Contact_Name"]]];
                
            }
            else
            {
                
                doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Doctor_Name"]]];
                
            }
            objectiveTxtView.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Objective"]];
            
        }
        
        
        NSString *leftBarButtonTitle;
        if ([appControl.FM_ENABLE_DISTRIBUTION_CHECK isEqualToString:KAppControlsYESCode] && [[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
            
            leftBarButtonTitle = NSLocalizedString(@"Close", nil);
        } else {
            leftBarButtonTitle = NSLocalizedString(@"Close Visit", nil);
        }

        
        UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:leftBarButtonTitle style:UIBarButtonItemStylePlain target:self action:@selector(closeVisitTapped)];
        
        
        [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                        forState:UIControlStateNormal];
        
        
        self.navigationItem.leftBarButtonItem=closeVisitButton;
        
        
        
        tasksButton.layer.borderWidth=1.0f;
        notesButton.layer.borderWidth=1.0f;
        tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
        notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
        
        
        
        notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
        notesButton.layer.shadowOffset = CGSizeMake(2, 2);
        notesButton.layer.shadowOpacity = 0.1;
        notesButton.layer.shadowRadius = 1.0;
        
        
        
        tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
        tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
        tasksButton.layer.shadowOpacity = 0.1;
        tasksButton.layer.shadowRadius = 1.0;
        
        
        
        
        for (UIView *borderView in self.view.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                
                
                
                if (borderView.tag==1 || borderView.tag==2|| borderView.tag==3 ) {
                    
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                    
                }
            }
        }
        
        //[demoPlanProductsCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
        
        //[presentationCollectionView registerClass:[MedRepVisitsStartMultipleCallsPresentationCollectionViewCell class] forCellWithReuseIdentifier:@"presentationCell"];
        
       // [doctorDetailCollectionView registerClass:[MedRepVisitsDoctorNameCollectionViewCell class] forCellWithReuseIdentifier:@"doctorDetail"];
        
        [doctorDetailCollectionView registerNib:[UINib nibWithNibName:@"MedRepVisitsDoctorNameCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"doctorDetail"];
        
        [presentationCollectionView registerNib:[UINib nibWithNibName:@"MedRepVisitsStartMultipleCallsPresentationCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"presentationCell"];
        
        [demoPlanProductsCollectionView registerNib:[UINib nibWithNibName:@"MedRepProductsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"productCell"];


        //seggregating images,pdf and video
        
        segmentMenuArray=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"IMAGE", nil),NSLocalizedString(@"VIDEO", nil),NSLocalizedString(@"PDF", nil),NSLocalizedString(@"PRESENTATION", nil), nil];
        
        
        segmentControl.sectionTitles = segmentMenuArray;
        //    segmentControl.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
        segmentControl.backgroundColor = [UIColor clearColor];
        
        segmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
        segmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
        
        segmentControl.selectionIndicatorBoxOpacity=0.0f;
        segmentControl.verticalDividerEnabled=YES;
        segmentControl.verticalDividerWidth=1.0f;
        segmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
        
        segmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
        segmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
        segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        segmentControl.tag = 3;
        segmentControl.selectedSegmentIndex = 0;
        
        
        
        
        segmentContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
        segmentContainerView.layer.shadowOffset = CGSizeMake(0, -2);
        segmentContainerView.layer.shadowOpacity = 0.1;
        segmentContainerView.layer.shadowRadius = 2.0;
        
        
        
        mediaScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 301, kVisitsSegmentControlWidth, 350)];
        mediaScrollView.backgroundColor = [UIColor clearColor];
        mediaScrollView.pagingEnabled = YES;
        mediaScrollView.showsHorizontalScrollIndicator = NO;
        mediaScrollView.contentSize = CGSizeMake(kVisitsSegmentControlWidth * segmentMenuArray.count, 407);
        mediaScrollView.delegate = self;
        
        
        [mediaScrollView setShowsHorizontalScrollIndicator:NO];
        [mediaScrollView setShowsVerticalScrollIndicator:NO];
        
        
        //[self.view addSubview:mediaScrollView];
        
        
        
        if (segmentControl.selectedSegmentIndex==0) {
            
            [mediaScrollView scrollRectToVisible:CGRectMake(0, 0, kVisitsSegmentControlWidth, 407) animated:NO];
            
        }
        else if (segmentControl.selectedSegmentIndex==1)
        {
            //[customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth, 0, SegmentControlWidth, 200) animated:NO];
            [mediaScrollView setContentOffset:CGPointMake(kVisitsSegmentControlWidth, 0)];
            
            
        }
        else
        {
            // [customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth *2, 0, SegmentControlWidth, 200) animated:NO];
            [mediaScrollView setContentOffset:CGPointMake(kVisitsSegmentControlWidth*2, 0)];
            
        }
        
        
        
        [segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
        
        
        
        
        NSLog(@"collection view data array count is %d", collectionViewDataArray.count);
        __weak typeof(self) weakSelf = self;
        
        [segmentControl setIndexChangeBlock:^(NSInteger index) {
            // [weakSelf.customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth * index, 0, SegmentControlWidth, 200) animated:YES];
            [weakSelf.mediaScrollView setContentOffset:CGPointMake(kVisitsSegmentControlWidth * index, 0)];
            
            
        }];
        NSLog(@"content offset for scroll view in visits %@", NSStringFromCGPoint(mediaScrollView.contentOffset));
        
        contactsArray=[MedRepQueries fetchContactDetaislwithLocationID:[visitDetailsDict valueForKey:@"Location_ID"]];
        
        
        //[mediaScrollView setContentSize:demoPlanProductsCollectionView.contentSize];
        
        // [self updateStatusforTasksandNotes];
        
        [self refreshTaskandNotesStatus:self.visitDetails];
        
        
    
    
    
    
    currentDemoPlanData=[[NSMutableArray alloc]init];
    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [self fetchMediaFilesData];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (productMediaFilesArray.count>0) {
                
                customPresentationsArray=[[NSMutableArray alloc]init];
                
                productImagesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
                
                productVideoFilesArray= [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
                
                productPdfFilesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
                
                presentationsArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
                
                currentDemoPlanData=[[NSMutableArray alloc]init];
                [currentDemoPlanData addObject:productImagesArray];
                [currentDemoPlanData addObject:productVideoFilesArray];
                [currentDemoPlanData addObject:productPdfFilesArray];
                [currentDemoPlanData addObject:presentationsArray];
                
                
                if (visitDetails.fieldMarketingPresentationsArray.count>0) {
                    
                }
                else{
                    visitDetails.fieldMarketingPresentationsArray=[[NSMutableArray alloc]init];
                }
                
                FieldMarketingPresentation * currentPresentation=[[FieldMarketingPresentation alloc]init];
                currentPresentation.isDefaultPresentation=YES;
                currentPresentation.presentationName=@"Default";
                currentPresentation.presentationID=[NSString createGuid];
                currentPresentation.presentationContentArray=productMediaFilesArray;
                currentPresentation.demoPlanID=visitDetails.Demo_Plan_ID;
                demoPlanIDforCreatePresentation=currentPresentation.demoPlanID;
                
                [visitDetails.fieldMarketingPresentationsArray addObject:currentPresentation];
                
                
                unfilteredFieldMarketingPresentationsArray=[[NSMutableArray alloc]init];
                
                unfilteredFieldMarketingPresentationsArray=visitDetails.fieldMarketingPresentationsArray;
                
                NSLog(@"unfiltered field marketing presentation isSelected in did appear %@", [unfilteredFieldMarketingPresentationsArray valueForKey:@"isSelected"]);
                
                
                [presentationCollectionView reloadData];
                
                
               
                
                
                selectedMediaContentArray=productImagesArray;
                [demoPlanProductsCollectionView reloadData];
                [self UpdateDemoPlanMediaIcons];

                [self imageButtonTapped:nil];

                [mediaSwipeView reloadData];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
    }
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    //[demoPlanProductsCollectionView reloadData];

    UITapGestureRecognizer * longPressGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cellTapped:)];
    longPressGesture.numberOfTouchesRequired=1;
    longPressGesture.numberOfTapsRequired=2;
    [presentationCollectionView addGestureRecognizer:longPressGesture];
    
    
    // appControl.FM_SHOW_NEXT_VISIT_OBJECTIVE=KAppControlsNOCode;
   
    

    NSMutableDictionary* lastVisitDetailsDict=[[NSMutableDictionary alloc]init];
    if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        lastVisitDetailsDict= [MedRepQueries fetchLastVisitDetails:[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Doctor_ID"]]];
    }
    else
    {
        lastVisitDetailsDict= [MedRepQueries fetchLastVisitDetailsforContactID:[visitDetailsDict valueForKey:@"Contact_ID"]];
    }
    NSString* nextCallObjective = [NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Next_Visit_Objective"]];
    if ([appControl.FM_SHOW_NEXT_VISIT_OBJECTIVE isEqualToString:KAppControlsYESCode] ) {
        objectiveViewWidthConstraint.constant = 235.0;
        xObjectiveTrailingConstraint.constant=8.0;
        nextVisitObjectiveTrailingConstraint.constant=7.0;
        nextVisitObjectiveLeadingConstraint.constant=8.0;
    }
    else{
        nextCallObjectiveView.hidden=YES;
        objectiveViewWidthConstraint.constant = 480.0;
        xObjectiveTrailingConstraint.constant=0.0;
        nextVisitObjectiveTrailingConstraint.constant=0.0;
        nextVisitObjectiveLeadingConstraint.constant=0.0;
       // nextCallObjectiveView.hidden=NO;
        //xObjectiveTrailingConstraint.constant=376.0;
    }
    nextCallObjectiveTextView.text =[NSString isEmpty:nextCallObjective]?KNotApplicable:nextCallObjective;
    
    if ([appControl.FM_ENABLE_PRESENTATION_MODULE isEqualToString: KAppControlsYESCode]) {
        //xMediaCollectionViewBottomConstraint.constant=273.0;
        xPresentationViewHeightConstraint.constant=193.0;
        xMediaViewHeightConstraint.constant=269.0f;
        xPresentationViewTopConstraint.constant=8.0f;
        xPresentationViewBottonConstraint.constant=8.0f;


    }
    else
    {
        xPresentationViewTopConstraint.constant=0.0f;
        xPresentationViewBottonConstraint.constant=8.0f;
        xMediaViewHeightConstraint.constant=470.0f;
        //xMediaCollectionViewBottomConstraint.constant=8.0;
        xPresentationViewHeightConstraint.constant=0.0;

    }

    [demoPlanProductsCollectionView reloadData];

    NSLog(@"Presentations container view frame is %@\n media view frame is %@", NSStringFromCGRect(presentationsContainerView.bounds),NSStringFromCGRect(_mediaView.bounds));
    
}

-(void)enableStartCallButton{
    startVisitBarButtonItem.enabled = YES;
}

-(void)cellTapped:(UITapGestureRecognizer *)tapGesture
{
    
    NSIndexPath *selectedIndexPath = [presentationCollectionView indexPathForItemAtPoint:[tapGesture locationInView:presentationCollectionView]];

    FieldMarketingPresentation * selectedPresentation=[visitDetails.fieldMarketingPresentationsArray objectAtIndex:selectedIndexPath.row];
    
    NSLog(@"long press activated");
    SalesWorxPresentationViewController * presentationVC=[[SalesWorxPresentationViewController alloc]init];
    presentationVC.visitDetails=visitDetails;
    presentationVC.selectedPresentationID=selectedPresentation.presentationID;
    presentationVC.presentationDelegate=self;
    presentationVC.presentationNavController=self.navigationController;
    [self.navigationController pushViewController:presentationVC animated:YES];
}

-(void)createPresentationTapped
{
    
     //this is just to test presentation
    SalesWorxPresentationViewController * presentationVC=[[SalesWorxPresentationViewController alloc]init];
    presentationVC.visitDetails=visitDetails;
    presentationVC.presentationDelegate=self;
    presentationVC.isCreatePresentation=YES;
    presentationVC.presentationNavController=self.navigationController;
    [self.navigationController pushViewController:presentationVC animated:YES];
     //[self.navigationController presentViewController:presentationVC animated:YES completion:nil];
     
}
-(void)createdPresentations:(NSMutableArray*)presentationsArray
{
    //visitDetails.presentationArray=presentationsArray;
}


-(void)fetchMediaFilesData
{
    NSString* demoPlanID=[visitDetailsDict valueForKey:@"Demo_Plan_ID"];
    if (demoPlanID!=nil)
    {
        productMediaFilesArray=[MedRepQueries fetchMediaFileswithDemoPlanID:demoPlanID];
        
    }
}

-(void)refreshContentAfterCall:(VisitDetails*)currentCall
{
    
    NSString*locationType =currentCall.Location_Type;
    
    
    NSMutableArray * currentVisitTasksArray=[[NSMutableArray alloc]init];
    //as suggested by manish, all tasks should be based on doctor id for hospital and contact id for pharmacy
    //29/02/2016
    
    if ([locationType isEqualToString:@"D"])
    {
        //        currentVisitTasksArray=[MedRepQueries fetchTaskObjectsforLocationID:currentCall.Location_ID andDoctorID:currentCall.selectedDoctor.Doctor_ID];
        
        currentVisitTasksArray=[MedRepQueries fetchTaskObjectsforLocationID:currentCall.Location_ID andDoctorID:@""];
    }
    
    else if ([locationType isEqualToString:@"P"])
    {
        //        currentVisitTasksArray=[MedRepQueries fetchTaskObjectswithLocationID:currentCall.Location_ID andContactID:currentCall.selectedContact.Contact_ID];
        
        currentVisitTasksArray=[MedRepQueries fetchTaskObjectswithLocationID:currentCall.Location_ID andContactID:@""];
        
        
    }
    
    else
    {
        
    }
    
    visitDetails.taskArray=currentVisitTasksArray;
    
    NSLog(@"task array after call is %@",visitDetails.taskArray);
    
    NSMutableArray * tempNotesArray=[[NSMutableArray alloc]init];
    
    
    if ([locationType isEqualToString:kPharmacyLocation]) {
        
        //get note objects
        
        tempNotesArray=[MedRepQueries fetchNotesObjectswithLocationID:currentCall.Location_ID andContactID:@""];
        
    }
    
    else if([locationType isEqualToString:kDoctorLocation])
    {
        
        
        tempNotesArray=[MedRepQueries fetchNoteObjectwithLocationID:currentCall.Location_ID andDoctorID:@""];
        
    }
    
    visitDetails.notesArray=tempNotesArray;
    NSLog(@"notes array in start visit %@", visitDetails.notesArray);

    if ([appControl.FM_VISIT_WITH_PLANNED_DOCTOR_ONLY isEqualToString:KAppControlsNOCode]) {
        if ([currentCall.Location_Type isEqualToString:kDoctorLocation]) {
            
            //fetch objects
            NSMutableArray * doctorObjectsArray=[MedRepQueries fetchDoctorObjectswithLocationID:currentCall.Location_ID];
            if (doctorObjectsArray.count>0) {
                visitDetails.doctorsforLocationArray=doctorObjectsArray;
            }
            
        }
        else if ([currentCall.Location_Type isEqualToString:kPharmacyLocation])
        {
            NSMutableArray *locationContactsArray = [MedRepQueries fetchContactsObjectsforPharmacyWithLocationID:currentCall.Location_ID];
            if (locationContactsArray.count>0) {
                visitDetails.locationContactsArray = locationContactsArray;
            }
        }
    }
}

-(void)closeVisitTapped
{
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
        
//    UIAlertView * endVisitAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Close Visit", nil) message:NSLocalizedString(@"Would you like to close this visit", nil) delegate:self cancelButtonTitle:NSLocalizedString(KAlertOkButtonTitle, nil) otherButtonTitles:NSLocalizedString(KAlertCancelButtonTitle, nil), nil];
//    endVisitAlert.tag=100000;
//    [endVisitAlert show];
        
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       NSLog(@"current visit calles array is %@",currentVisitCallsArray);
                                       
                                       if (currentVisitCallsArray.count>0) {
                                           
                                           BOOL status=[MedRepQueries updateVisitStatus:visitDetails];
                                           NSLog(@"visit status updated successfully? %hhd",status);
                                           if (status==YES) {
                                               
                                               if ([appControl.FM_ENABLE_DISTRIBUTION_CHECK isEqualToString:KAppControlsYESCode] && [[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
                                                   
                                                   [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:visitDetails];
                                                   [self.navigationController popViewControllerAnimated:YES];
                                               } else {
                                                   [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:visitDetails];
                                                   [self.navigationController popToRootViewControllerAnimated:YES];
                                               }
                                           }
                                           
                                           
                                       }
                                       else
                                       {
                                           [self displayContactPopUpView];
                                       }
                                   }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)] ,nil];

        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Close Visit" andMessage:@"Would you like to close this visit?" andActions:actionsArray withController:self];
    }
    
}

-(void)displayContactPopUpView
{
    
    
    MedRepVisitsEndCallViewController *presentingView=   [[MedRepVisitsEndCallViewController alloc]init];
    presentingView.visitDetailsDict=visitDetailsDict;
    presentingView.currentVisit=visitDetails;
    presentingView.visitCompletedDelegate=self;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    mapPopUpViewController.navigationBarHidden=YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}

-(void)visitCompletedWithOutCalls:(BOOL)status
{
    if (status==YES) {
        
        if ([appControl.FM_ENABLE_DISTRIBUTION_CHECK isEqualToString:KAppControlsYESCode] && [[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag==100000) {
        
        if (buttonIndex==1) {
            
        }
        else
        {
            
            if (currentVisitCallsArray.count>0) {
                
                BOOL status=[MedRepQueries updateVisitStatus:visitDetails];
                NSLog(@"visit status updated successfully? %hhd",status);
                if (status==YES) {
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:visitDetails];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    
                }
                
                
            }
            else
            {
                //show popover
                
                [self displayContactPopUpView];
                
                
                
                
                
                
                //                MedRepEDetailingEndVisitViewController* endCallVC=[[MedRepEDetailingEndVisitViewController alloc]init];
                //                endCallVC.visitDetailsDict=visitDetailsDict;
                //                endCallVC.endVisitWithoutCall=YES;
                //                endCallVC.visitDetails=self.visitDetails;
                //
                //                [self.navigationController pushViewController:endCallVC animated:YES];
            }
            
            
            
        }
        
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    
    
    
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    
    //    if (segmentedControl.selectedSegmentIndex==0) {
    //
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=productImagesArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //    }
    //    else if (segmentedControl.selectedSegmentIndex==1) {
    //
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=productVideoFilesArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //    }
    //    else if (segmentedControl.selectedSegmentIndex==2)
    //    {
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=productPdfFilesArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //    }
    //    else if (segmentedControl.selectedSegmentIndex==3)
    //    {
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=presentationsArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //    }
    
    [mediaSwipeView scrollToItemAtIndex:segmentedControl.selectedSegmentIndex duration:0.0];
    
    
}
- (IBAction)taskButtonTapped:(id)sender {
    
    popOverController=nil;
    MedRepEdetailTaskViewController * edetailTaskVC=[[MedRepEdetailTaskViewController alloc]init];
    NSMutableDictionary *TempVisitDetailsDict=[[NSMutableDictionary alloc]initWithDictionary:visitDetailsDict copyItems:YES];
    [TempVisitDetailsDict setValue:@"" forKey:@"Contact_ID"];
    [TempVisitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
    edetailTaskVC.visitDetails=[MedRepDefaults fetchVisitDetailsObject:visitDetailsDict];
    NSString* locationType=[visitDetailsDict valueForKey:@"Location_Type"];
    if ([locationType isEqualToString:@"D"])
    {
        edetailTaskVC.visitDetails.taskArray=[MedRepQueries fetchTaskObjectsforLocationID:[visitDetailsDict valueForKey:@"Location_ID"] andDoctorID:@""];
    }
    else
    {
        edetailTaskVC.visitDetails.taskArray=[MedRepQueries fetchTaskObjectswithLocationID:[visitDetailsDict valueForKey:@"Location_ID"] andContactID:@""];
        
    }
    edetailTaskVC.isInMultiCalls=YES;
    edetailTaskVC.visitDetailsDict=TempVisitDetailsDict;
    edetailTaskVC.tasksArray=edetailTaskVC.visitDetails.taskArray;
    edetailTaskVC.delegate=self;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    
    
    
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(376, 600) animated:YES];
    edetailTaskVC.tasksPopOverController=popOverController;
    
    
    CGRect relativeFrame = [tasksButton convertRect:tasksButton.bounds toView:self.view];
    
    if ([SWDefaults isRTL]) {
        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else
    {
        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    }
    
    
}


- (IBAction)notesButtonTapped:(id)sender {
    
    popOverController=nil;
    MedRepEDetailNotesViewController * edetailTaskVC=[[MedRepEDetailNotesViewController alloc]init];
    NSMutableDictionary *TempVisitDetailsDict=[[NSMutableDictionary alloc]initWithDictionary:visitDetailsDict copyItems:YES];
    [TempVisitDetailsDict setValue:@"" forKey:@"Contact_ID"];
    [TempVisitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
    edetailTaskVC.isInMultiCalls=YES;
    edetailTaskVC.visitDetails=[MedRepDefaults fetchVisitDetailsObject:visitDetailsDict];
    edetailTaskVC.visitDetailsDict=TempVisitDetailsDict;
    edetailTaskVC.delegate=self;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(376, 350) animated:YES];
    edetailTaskVC.notesPopOverController=popOverController;
    CGRect relativeFrame = [notesButton convertRect:notesButton.bounds toView:self.view];
    [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

-(void)startVisitBarButtonTapped:(UIBarButtonItem*)barBtn
{
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
    
    
    NSLog(@"shall i start a visit?");
        
        BOOL isDoctorEmpty= YES;
        
        if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {

            if ([NSString isEmpty:selectedDoctor.Doctor_Name] && [NSString isEmpty:visitDetails.selectedDoctor.Doctor_Name]) {
                
                isDoctorEmpty =YES;
            }
            else{
                isDoctorEmpty = NO;
            }
            
        }
        else{
            
            if ([NSString isEmpty:selectedContact.Contact_Name] &&  [NSString isEmpty:visitDetails.selectedContact.Contact_Name]) {
                
                isDoctorEmpty =YES;
            }
            else{
                isDoctorEmpty = NO;
            }
        }
        
        
        if (isDoctorEmpty==YES) {
            
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select Doctor/Contact name" withController:self];
        }
        
   else if ([NSString isEmpty:accompaniedBy]==NO )
   {
       bool isSelectedUserCompleteCallAlready = false;
       
       if (currentVisitCallsArray.count > 0) {
           if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]) {
               
               for (Calls *call in currentVisitCallsArray) {
                   NSString *name = call.Contact_Name;
                   
                   if ([name isEqualToString:visitDetails.selectedContact.Contact_Name]) {
                       isSelectedUserCompleteCallAlready = true;
                   }
               }
           }
           else if ([visitDetails.Location_Type isEqualToString:kDoctorLocation])
           {
               for (Calls *call in currentVisitCallsArray) {
                   NSString *name = call.Contact_Name;
                   
                   if ([name isEqualToString:selectedDoctor.Doctor_Name]) {
                       isSelectedUserCompleteCallAlready = true;
                   }
               }
           }
       }
       
       
       if (isSelectedUserCompleteCallAlready == true) {
           if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]) {
               [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"You have already completed a call with selected contact. Please choose another one to continue." withController:self];
           }else{
               [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"You have already completed a call with selected doctor. Please choose another one to continue." withController:self];
           }
       }else{
           goToNextScreen = true;
           
           NSDateFormatter *formatterTime = [NSDateFormatter new] ;
           NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
           [formatterTime setDateFormat:@"hh:mm a"];
           [formatterTime setLocale:usLocaleq];
           NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
           NSLog(@"call start time is %@", callStartDate);
           visitDetails.currentCallStartTime=callStartDate;
           
           
           [visitDetailsDict setObject:accompaniedBy forKey:@"Accompanied_By"];
           [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Call_End_Date"];
           [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Visit_Conclusion_Notes"];
           [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Next_Visit_Objective"];
           
           
           [visitDetailsDict setValue:nil forKey:@"Visit_Rating"];
           [visitDetailsDict setValue:nil forKey:@"Visit_Conclusion_Notes"];
           [visitDetailsDict setValue:nil forKey:@"Next_Visit_Objective"];
           [visitDetailsDict setValue:nil forKey:@"Meeting_Attendees"];
           
           
           
           
           
           
           BOOL validated=NO;
           NSString* missingString=[[NSString alloc]init];
           
           if ([visitDetails.Location_Type isEqualToString:kDoctorLocation] && visitDetails.selectedDoctor!=nil) {
               
               validated=YES;
           }
           else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation] && visitDetails.selectedContact!=nil)
           {
               validated=YES;
               
           }
           //FMCG contact validation bypassed.
           validated=YES;
           
           if (validated==YES) {
               
               [self refreshContentAfterCall:self.visitDetails];
               [self refreshTaskandNotesStatus:self.visitDetails];
               
               //[self updateStatusforTasksandNotes];
               
               //stop wait timer and update value
               
               [[NSUserDefaults standardUserDefaults]setObject:waitTimeLbl.text forKey:@"waitTimer"];
               
               NSLog(@"WAIT TIME WHILE MOVING TO NEXT SCREEN %@", [[NSUserDefaults standardUserDefaults]objectForKey:@"waitTimer"]);
               
               
               [waitTimer invalidate];
               waitTimer=nil;
               
               isDemoPlanSelected = false;
               
               MedRepEDetailingStartCallViewController* startCallVC=[[MedRepEDetailingStartCallViewController alloc]init];
               startCallVC.visitDetailsDict=visitDetailsDict;
               
               [unfilteredFieldMarketingPresentationsArray setValue:@"N" forKey:@"isSelected"];
               
               self.visitDetails.fieldMarketingPresentationsArray=unfilteredFieldMarketingPresentationsArray;
               
               
               NSLog(@"unfiltered field marketing presentation isSelected in push %@", [unfilteredFieldMarketingPresentationsArray valueForKey:@"isSelected"]);
               self.visitDetails.coachSurveyConfirmationArray=[[NSMutableArray alloc]init];
               self.visitDetails.coachSurveyArray=[[NSMutableArray alloc]init];
               
               startCallVC.visitDetails=self.visitDetails;
               
               if (isFromDashboard) {
                   startCallVC.isFromDashboard = YES;
               }
               
               if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
                   
                   [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
               
               
               [self.navigationController pushViewController:startCallVC animated:YES];
               
           }
           else
           {
               if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
                   
                   missingString=kMissingDoctorTitle;
               }
               else if([visitDetails.Location_Type isEqualToString:kPharmacyLocation])
               {
                   missingString=kMissingContactTitle;
               }
               
               UIAlertView* missingDataAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(KMissingData, nil) message:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Please select/create", nil),NSLocalizedString(missingString, nil)] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
               [missingDataAlert show];
           }
       }
   }
    else
    {
        UIAlertView* missingDataAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please select Accompanied By", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [missingDataAlert show];
        
    }
    
    }
    
    
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

-(void)startTimer{
    NSDate * currentTimer=testDate;
    NSDate *now = [NSDate date];
    NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
    
    // NSLog(@"wait time counter ticking %@",[self stringFromTimeInterval:elapsedTimeDisplayingPoints]);
    
    waitTimeLbl.text=[self stringFromTimeInterval:elapsedTimeDisplayingPoints];
    
    
    // NSLog(@"WAIT TIME START TIMER %@", waitTimeLbl.text);
    
}

-(void)selectedStringValue:(NSIndexPath *)indexPath

{
    //    self.accompaniedByLbl.text=[NSString stringWithFormat:@" %@",[[accompaniedByArray valueForKey:@"Username"] objectAtIndex:indexPath.row] ];
    
    // [accompaniedButton setTitle:[NSString stringWithFormat:@" %@",[[accompaniedByArray valueForKey:@"Username"] objectAtIndex:indexPath.row] ] forState:UIControlStateNormal];
    
    accompaniedByTextField.text=[NSString stringWithFormat:@" %@",[[accompaniedByArray valueForKey:@"Username"] objectAtIndex:indexPath.row] ];
    accompaniedBy=[NSString stringWithFormat:@"%@",[[accompaniedByArray valueForKey:@"User_ID"] objectAtIndex:indexPath.row] ];
    
    
    
    if (accompaniedByPopOver)
    {
        [accompaniedByPopOver dismissPopoverAnimated:YES];
        accompaniedByPopOver = nil;
        accompaniedByPopOver.delegate=nil;
    }
    
    
    
    if (popOverController)
    {
        [popOverController dismissPopoverAnimated:YES];
        popOverController = nil;
        popOverController.delegate=nil;
    }
    locationPopOver.delegate=nil;
    locationPopOver=nil;
    
}



#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==presentationCollectionView) {
        return visitDetails.fieldMarketingPresentationsArray.count;
    }else if (collectionView == doctorDetailCollectionView){
        
        if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]) {
           return visitDetails.locationContactsArray.count;
        }
        else
        {
            return visitDetails.doctorsforLocationArray.count;
        }
    }
    else{
        return selectedMediaContentArray.count;
    }
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    

    if (collectionView==demoPlanProductsCollectionView && [appControl.FM_ENABLE_PRESENTATION_MODULE isEqualToString:KAppControlsNOCode]) {
        return CGSizeMake(180, 172);
    } else if (collectionView == doctorDetailCollectionView){
        return CGSizeMake(230, 50);
    }
    else{
        return CGSizeMake(150, 150);
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==presentationCollectionView) {
        static NSString *cellIdentifier = @"presentationCell";
        MedRepVisitsStartMultipleCallsPresentationCollectionViewCell *cell = (MedRepVisitsStartMultipleCallsPresentationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.selectedImage.hidden=YES;
        FieldMarketingPresentation * currentPresentation=[visitDetails.fieldMarketingPresentationsArray objectAtIndex:indexPath.row];
        cell.titleLbl.font = kSWX_FONT_SEMI_BOLD(12);
        cell.titleLbl.text= [SWDefaults getValidStringValue:currentPresentation.presentationName];
        return cell;
        
    }else if (collectionView == doctorDetailCollectionView){
        static NSString *cellIdentifier = @"doctorDetail";
        MedRepVisitsDoctorNameCollectionViewCell *cell = (MedRepVisitsDoctorNameCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.layer.borderWidth=1.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(189.0/255.0) green:(207.0/255.0) blue:(211.0/255.0) alpha:1]CGColor];
        
        if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]) {
            Contact *currentContact = [visitDetails.locationContactsArray objectAtIndex:indexPath.row];
            cell.doctorNameLabel.text =[SWDefaults getValidStringValue:currentContact.Contact_Name];
            
            if (selectedIndexPathOfDoctorCollectionView == indexPath) {
                cell.tickImageView.image = [UIImage imageNamed:@"Tick_CollectionView"];
            } else {
                cell.tickImageView.image = nil;
            }
        }
        else
        {
            Doctors *currentDoctor = [visitDetails.doctorsforLocationArray objectAtIndex:indexPath.row];
            cell.doctorNameLabel.text = [SWDefaults getValidStringValue:currentDoctor.Doctor_Name];
            cell.doctorDepartmentLabel.text = [SWDefaults getValidStringValue:currentDoctor.Specialization];
            
            if (selectedIndexPathOfDoctorCollectionView == indexPath) {
                cell.tickImageView.image = [UIImage imageNamed:@"Tick_CollectionView"];
            } else {
                cell.tickImageView.image = nil;
            }
        }
        return cell;
    }
    else{
    
    static NSString *cellIdentifier = @"productCell";
    NSMutableArray *currentMediaArray= selectedMediaContentArray;
    NSString* mediaType = [[currentMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.selectedImgView.hidden=YES;
    
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
    
    if ([mediaType isEqualToString:@"Image"])
    {
        //        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
        
        NSString* thumbnailImageName = [[currentMediaArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
        
        cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
        cell.captionLbl.text=[NSString getValidStringValue:[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"]];
        cell.placeHolderImageView.hidden=NO;
        
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        cell.captionLbl.text=[NSString getValidStringValue:[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"]];
        
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        cell.captionLbl.text=[NSString getValidStringValue:[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"]];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
        cell.captionLbl.text=[NSString getValidStringValue:[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"]];
    }
    
    
    //adding check mark to selected cell
    if (demoedMediaFiles.count>0)
    {
        for (NSInteger i=0; i<demoedMediaFiles.count; i++)
        {
            NSString* selectedFileName=[demoedMediaFiles objectAtIndex:i];
            NSString * currentFileName=[[currentMediaArray valueForKey:@"Media_File_ID"] objectAtIndex:indexPath.row] ;
            
            NSLog(@"selected file name : %@  current file name : %@", selectedFileName,currentFileName);
            
            if ([selectedFileName isEqualToString:currentFileName])
            {
                cell.selectedImgView.hidden=NO;
            }
            else
            {
            }
        }
    }
    
    cell.productImageView.backgroundColor=[UIColor clearColor];
    if (indexPath.row== selectedCellIndex)
    {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else
    {
        cell.layer.borderWidth=0;
    }
    return cell;
    }
}


/*
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return selectedMediaContentArray.count;
 
    if (currentDemoPlanData.count>0) {
        NSMutableArray *tempMediaarray= [currentDemoPlanData objectAtIndex:collectionView.tag];
        return  tempMediaarray.count;
    }
    return 0;
 
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(150, 150);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productCell";
    
    NSMutableArray *currentMediaArray= [currentDemoPlanData objectAtIndex:collectionView.tag];
    
    NSString* mediaType = [[currentMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.selectedImgView.hidden=YES;
    
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[currentMediaArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row] ] ];
    
    if ([mediaType isEqualToString:@"Image"])
    {
//        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
        
        NSString* thumbnailImageName = [[currentMediaArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
        
        cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];

        
        cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
        
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        cell.captionLbl.text=[[productVideoFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        cell.captionLbl.text=[[productPdfFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
        cell.captionLbl.text=[[presentationsArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    
    
    //adding check mark to selected cell
    
    MedRepCollectionViewCheckMarkViewController* checkMarkVC=[[MedRepCollectionViewCheckMarkViewController alloc]init];
    if (demoedMediaFiles.count>0)
    {
        for (NSInteger i=0; i<demoedMediaFiles.count; i++)
        {
            NSString* selectedFileName=[demoedMediaFiles objectAtIndex:i];
            NSString * currentFileName=[[currentMediaArray valueForKey:@"Media_File_ID"] objectAtIndex:indexPath.row] ;
            
            NSLog(@"selected file name : %@  current file name : %@", selectedFileName,currentFileName);
            
            if ([selectedFileName isEqualToString:currentFileName])
            {
                cell.selectedImgView.hidden=NO;
            }
            else
            {
            }
        }
    }
    
    cell.productImageView.backgroundColor=[UIColor clearColor];
    if (indexPath.row== selectedCellIndex)
    {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else
    {
        cell.layer.borderWidth=0;
    }
    return cell;
}
  */

-(void)showPresentationActionSheet:(NSIndexPath*)selectedIndex
{
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"Select Action", nil)
                                  message:NSLocalizedString(@"", nil)
                                  preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction* viewAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"View", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   if (visitDetails.fieldMarketingPresentationsArray.count>0) {
                                      /*
                                       UCSPresentationViewerViewController * viewerVC=[[UCSPresentationViewerViewController alloc]init];
                                       viewerVC.MediaFiles=refinedPresentationMedia;
                                       [self.navigationController pushViewController:viewerVC animated:YES];*/
                                       
                                       NSMutableArray* currentPresentationMedia=[[visitDetails.fieldMarketingPresentationsArray valueForKey:@"presentationContentArray"] objectAtIndex:selectedIndex.row];
                                       NSMutableArray* refinedPresentationMedia=[[NSMutableArray alloc]init];
                                       for (ProductMediaFile * currentFile in currentPresentationMedia) {
                                           [refinedPresentationMedia addObject:currentFile];
                                       }
                                       
                                       SalesWorxPresentationHomeViewController * tempVC=[[SalesWorxPresentationHomeViewController alloc]init];
                                       tempVC.mediaArray=refinedPresentationMedia;
                                       [self.navigationController pushViewController:tempVC animated:YES];
                                       
                                   }
                               }];
    
    UIAlertAction* editAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Edit", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     FieldMarketingPresentation * selectedPresentation=[visitDetails.fieldMarketingPresentationsArray objectAtIndex:selectedIndex.row];
                                     
                                     NSLog(@"long press activated");
                                     SalesWorxPresentationViewController * presentationVC=[[SalesWorxPresentationViewController alloc]init];
                                     presentationVC.visitDetails=visitDetails;
                                     presentationVC.selectedDemoPlanID=selectedPresentation.demoPlanID;

                                     presentationVC.isEditPresentation=YES;
                                     presentationVC.selectedPresentationID=selectedPresentation.presentationID;
                                     presentationVC.presentationDelegate=self;
                                     presentationVC.presentationNavController=self.navigationController;
                                     [self.navigationController pushViewController:presentationVC animated:YES];
                                     
                                     
                                 }];
    
    UIAlertAction* cancelAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];

                                 }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:viewAction,editAction,cancelAction ,nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Select Action" andMessage:@"" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
}
-(void)updateDemoplanwithSpecialization
{
    NSString* enableDemoPlanSpecialization = [NSString getValidStringValue:appControl.FM_ENABLE_DEMO_PLAN_SPECIALIZATION];
    NSString* selectedDoctorSpecialization = [NSString getValidStringValue:selectedDoctor.Specialization];
    
    NSLog(@"dictionary specialization %@ doctor specialization %@",selectedDoctorSpecialization,selectedDoctor.Specialization);
    
    if ([enableDemoPlanSpecialization isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:selectedDoctorSpecialization])
    {
        //NSLog(@"demoplan Array is %@ and selectedDoctor is %@", demoPlanArray,visitDetailsDict);
        NSPredicate* selectedDemoPlanPredicate = [NSPredicate predicateWithFormat:@"SELF.Specialization ==[cd] %@",selectedDoctorSpecialization];
        NSMutableArray* selectedDemoPlanArray = [[demoPlanObjectsArray filteredArrayUsingPredicate:selectedDemoPlanPredicate] mutableCopy];
        NSLog(@"demo plan based on specialization in start call %@",selectedDemoPlanArray);
        
        if (selectedDemoPlanArray.count>0)
        {
            selectedDemoPlan = selectedDemoPlanArray[0];
            [self updateDemoPlanDetails];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==presentationCollectionView) {
        isDemoPlanSelected = true;
        [self showPresentationActionSheet:indexPath];
        
    }
    else if (collectionView == doctorDetailCollectionView){
        selectedIndexPathOfDoctorCollectionView = indexPath;
        [doctorDetailCollectionView reloadData];
        
        
       

        if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            selectedDoctor=[visitDetails.doctorsforLocationArray objectAtIndex:indexPath.row];
            doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[SWDefaults getValidStringValue:selectedDoctor.Doctor_Name]];
            visitDetails.selectedDoctor=selectedDoctor;
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedDoctor.Doctor_ID] forKey:@"Doctor_ID"];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedDoctor.Doctor_Name] forKey:@"Doctor_Name"];
            [self updateDemoplanwithSpecialization];
        }
        else
        {
            selectedContact=[visitDetails.locationContactsArray objectAtIndex:indexPath.row];
            doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[SWDefaults getValidStringValue:selectedContact.Contact_Name]];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedContact.Contact_ID] forKey:@"Contact_ID"];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedContact.Contact_Name] forKey:@"Pharmacy_Contact_Name"];
            visitDetails.selectedContact=selectedContact;
        }
        NSMutableDictionary* lastVisitDetailsDict=[[NSMutableDictionary alloc]init];
        if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            lastVisitDetailsDict= [MedRepQueries fetchLastVisitDetails:[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Doctor_ID"]]];
        }
        else
        {
            lastVisitDetailsDict= [MedRepQueries fetchLastVisitDetailsforContactID:[visitDetailsDict valueForKey:@"Contact_ID"]];
        }
        NSString* nextCallObjective = [NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Next_Visit_Objective"]];
        nextCallObjectiveTextView.text =[NSString isEmpty:nextCallObjective]?KNotApplicable:nextCallObjective;
    }
    else{
        
        MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        
        selectedCellIndex=indexPath.row;
        //    NSMutableDictionary * tempDict=[collectionViewDataArray objectAtIndex:indexPath.row];
        //
        //    [tempDict setObject:@"Yes" forKey:@"Demoed"];
        //    [collectionViewDataArray replaceObjectAtIndex:indexPath.row withObject:tempDict];
        //
        
        //    NSMutableArray *currentMediaArray= [currentDemoPlanData objectAtIndex:collectionView.tag];
        
        NSMutableArray *currentMediaArray= selectedMediaContentArray;
        
        SelectedProductMediaFile=[currentMediaArray objectAtIndex:indexPath.row];
        
        
        
        SelectedProductMediaFile.isDemoed=YES;
        
        NSLog(@"data after replacing %@", [currentMediaArray objectAtIndex:indexPath.row]);
        
        NSString* selectedFileID=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID"];
        if ([demoedMediaFiles containsObject:selectedFileID])
        {
            
        }
        else
        {
            [demoedMediaFiles addObject:selectedFileID];
        }
        
        NSLog(@"demoed file id %@", selectedFileID);
        
        NSString* mediaType = [[currentMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row];
        NSString*documentsDirectory;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        
        //make a file name to write the data to using the documents directory:
        if ([mediaType isEqualToString:@"Image"])
        {
            isDemoPlanSelected = true;
            
            [imagesPathArray removeAllObjects];
            for (NSInteger i=0; i<productImagesArray.count; i++)
            {
                [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i] ]];
            }
            
            NSLog(@"product images array before selecting %@", [productImagesArray description]);
            
            MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
            imagesVC.productImagesArray=imagesPathArray;
            imagesVC.imagesViewedDelegate=self;
            
            imagesVC.productDataArray=productImagesArray;
            imagesVC.currentImageIndex=indexPath.row;
            imagesVC.selectedIndexPath=indexPath;
            
            //handle auto insert email in TO: in MFMailComposeViewController pop up
            if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]){
                imagesVC.isCurrentUserDoctor = YES;
                imagesVC.doctor_ID = visitDetails.Doctor_ID;
            }
            
            else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]){
                imagesVC.isCurrentUserPharmacy = YES;
                imagesVC.contact_ID = visitDetails.Contact_ID;
            }
            
            [self presentViewController:imagesVC animated:YES completion:nil];
        }
        else if ([mediaType isEqualToString:@"Video"])
        {
            isDemoPlanSelected = true;
            
            MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
            videosVC.videosViewedDelegate=self;
            videosVC.currentImageIndex=indexPath.row;
            videosVC.selectedIndexPath=indexPath;
            videosVC.productDataArray=productVideoFilesArray;
            
            //handle auto insert email in TO: in MFMailComposeViewController pop up
            if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]){
                videosVC.isCurrentUserDoctor = YES;
                videosVC.doctor_ID = visitDetails.Doctor_ID;
            }
            
            else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]){
                videosVC.isCurrentUserPharmacy = YES;
                videosVC.contact_ID = visitDetails.Contact_ID;
            }
            
            [self.navigationController presentViewController:videosVC animated:YES completion:nil];
        }
        else if ([mediaType isEqualToString:@"Brochure"])
        {
            if (productPdfFilesArray.count>0)
            {
                isDemoPlanSelected = true;
                
                MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
                pdfVC.pdfViewedDelegate=self;
                pdfVC.productDataArray=productPdfFilesArray;
                pdfVC.selectedIndexPath=indexPath;
                
                //handle auto insert email in TO: in MFMailComposeViewController pop up
                if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]){
                    pdfVC.isCurrentUserDoctor = YES;
                    pdfVC.doctor_ID = visitDetails.Doctor_ID;
                }
                
                else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]){
                    pdfVC.isCurrentUserPharmacy = YES;
                    pdfVC.contact_ID = visitDetails.Contact_ID;
                }
                
                [self presentViewController:pdfVC animated:YES completion:nil];
            }
            else
            {
                UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No PDF files are available for the selected demo plan" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [noDataAvblAlert show];
            }
        }
        else if ([mediaType isEqualToString:@"Powerpoint"])
        {
            if (presentationsArray.count>0) {
                isDemoPlanSelected = true;
                
                NSString * selectedFilePath=[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:[[presentationsArray objectAtIndex:indexPath.row]valueForKey:@"File_Name"]];
                
                /** Power point slide show files will display in external apps*/
                if([[selectedFilePath lowercaseString] hasSuffix:KppsxFileExtentionStr]){
                    [docDispalayManager ShowUserActivityShareViewInView:collectionView SourceRect:cell.frame AndFilePath:selectedFilePath];
                }else{
                    MedRepProductHTMLViewController *obj = [[MedRepProductHTMLViewController alloc]init];
                    obj.HTMLViewedDelegate = self;
                    obj.productDataArray = presentationsArray;
                    obj.showAllProducts=YES;
                    obj.isObjectData=YES;
                    obj.selectedIndexPath = indexPath;
                    obj.currentImageIndex=indexPath.row;
                    
                    //handle auto insert email in TO: in MFMailComposeViewController pop up
                    if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]){
                        obj.isCurrentUserDoctor = YES;
                        obj.doctor_ID = visitDetails.Doctor_ID;
                    }
                    
                    else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation]){
                        obj.isCurrentUserPharmacy = YES;
                        obj.contact_ID = visitDetails.Contact_ID;
                    }
                    
                    
                    [self presentViewController:obj animated:YES completion:nil];
                }
                
                
                
                
            }
            else
            {
                UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No presentation files are available " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [noDataAvblAlert show];
            }
        }
    }
}

-(void)setBorderOnLastViewedImage:(NSInteger)index {
    
    [demoPlanProductsCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [demoPlanProductsCollectionView.delegate collectionView:demoPlanProductsCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark Task and Notes Status Update

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}


-(void)updateStatusforTasksandNotes

{
    
    NSString* locationType=[visitDetailsDict valueForKey:@"Location_Type"];
    
    NSString* locationID=[visitDetailsDict valueForKey:@"Location_ID"];
    
    NSInteger  taskCount=0;
    
    NSInteger notesCount=0;
    
    if ([locationType isEqualToString:@"D"]) {
        
        NSString* doctorID=[visitDetailsDict valueForKey:@"Doctor_ID"];
        
        
        NSMutableArray* taskCount=[MedRepQueries fetchTasksCountforLocationID:locationID andDoctorID:doctorID];
        
        
        NSLog(@"task array is %@", taskCount);
        
        
        if (taskCount.count>0) {
            
            for (NSInteger i=0; i<taskCount.count; i++) {
                
                NSMutableDictionary * currentDict=[taskCount objectAtIndex:i];
                
                NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:taskDueDate];
                
                NSLog(@"date from string is %@", dateFromString);
                
                BOOL testDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                
                BOOL colorSet=NO;
                
                NSLog(@"check test date %hhd", testDate);
                
                if (testDate==YES) {
                    
                    NSLog(@"RED RED TASKS OVER DUE");
                    [visitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                    
                    // [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
                    
                    colorSet=YES;
                }
                
                else
                {
                    NSLog(@"YELLOW");
                    
                    if (colorSet==YES) {
                        
                    }
                    else
                    {
                        // [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
                    }
                    
                    [visitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                    
                    
                }
                
            }
            
        }
        else
        {
            [visitDetailsDict setValue:nil forKey:@"TASK_STATUS"];
        }
        
        
        notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andDoctorID:doctorID];
        
        NSLog(@"task count : %@ notes count : %d",taskCount,notesCount);
        
        
        if (taskCount>0) {
            [visitDetailsDict setValue:@"YES" forKey:@"Tasks_Available"];
        }
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Tasks_Available"];
            
        }
        if (notesCount>0)
            
        {
            [visitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
            
        }
        
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
            
        }
        
        
    }
    
    else if ([locationType isEqualToString:@"P"])
        
    {
        NSString* contactID=[visitDetailsDict valueForKey:@"Contact_ID"];
        
        NSMutableArray* tasksArray=[MedRepQueries fetchTaskCountforLocationID:locationID andContactID:contactID];
        
        
        
        if (tasksArray.count>0) {
            
            for (NSInteger i=0; i<tasksArray.count; i++) {
                
                NSMutableDictionary * currentDict=[tasksArray objectAtIndex:i];
                
                NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:taskDueDate];
                
                
                BOOL testTaskDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                
                NSLog(@"task test date %hhd", testTaskDate);
                
                BOOL colorUpdated=NO;
                
                if (testTaskDate==YES) {
                    
                    NSLog(@"RED RED TASKS OVER DUE");
                    [visitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                    
                    //[taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
                    
                }
                
                
                
                else
                {
                    NSLog(@"YELLOW");
                    
                    //[taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
                    
                    [visitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                    
                    
                }
                
            }
            
        }
        
        
        else if (tasksArray.count==0)
            
        {
            [taskStatusButton setBackgroundColor:[UIColor whiteColor]];
            [visitDetailsDict setValue:nil forKey:@"TASK_STATUS"];
            
            
        }
        
        notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andContactID:contactID];
        
        
        
        
        
        NSLog(@"task count : %d notes count : %d",taskCount,notesCount);
        
        
        
        
        if (notesCount>0)
            
        {
            [visitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
            
        }
        
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
            
        }
        
        
        
    }
    
    
    if (notesCount>0) {
        
        notesStatusButton.backgroundColor=KNOTESAVAILABLECOLOR;
    }
    else
    {
        notesStatusButton.backgroundColor=KNOTESUNAVAILABLECOLOR;
        
    }
    
    
    
    //update task objects
    
    
    
    
    
    
    
    
    
    
    NSLog(@"task array while updating color  %@", self.visitDetails.taskArray);
    
    BOOL isColorUpdated=NO;
    
    
    for (VisitTask * task in self.visitDetails.taskArray) {
        
        
        NSLog(@"task status is %@", task.Status);
        
        NSString* taskDueDate=task.Start_Time;
        
        
        NSLog(@"task due date is %@", task.Start_Time);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        
        NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setTimeZone:timeZone];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *dateFromString = [[NSDate alloc] init];
        // voila!
        dateFromString = [dateFormatter dateFromString:taskDueDate];
        
        NSLog(@"date being passed %@", dateFromString);
        
        BOOL testTaskDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
        
        NSLog(@"test task date %hhd", testTaskDate);
        
        NSLog(@"task status is %@", task.Status);
        
        if ([task.Status isEqualToString:@"Closed"] && isColorUpdated==NO) {
            
            [taskStatusButton setBackgroundColor:[UIColor whiteColor]];
            
        }
        
        
        else if (testTaskDate==YES && isColorUpdated==NO) {
            
            NSLog(@"task status color updated to red");
            
            [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
            isColorUpdated=YES;
            
            
        }
        
        else if (isColorUpdated==YES)
            
        {
            
        }
        
        else
        {
            [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
            NSLog(@"task status color updated to green");
            
        }
        
        
    }
    
    
    
    //update notes based on custom objects
    
    NSLog(@"notes array in edet %@", self.visitDetails.notesArray);
    notesCount=self.visitDetails.notesArray.count;
    
    if (notesCount>0) {
        
        notesStatusButton.backgroundColor=KNOTESAVAILABLECOLOR;
    }
    else
    {
        notesStatusButton.backgroundColor=KNOTESUNAVAILABLECOLOR;
        
    }
    
    
    
}


#pragma mark popover methods

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    NSLog(@"content array being passed %@",contentArray);
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    if ([popOverString isEqualToString:kDoctorTitle]) {
        popOverVC.isDoctorFilter=YES;
    }
    if ([popOverString isEqualToString:kAccompaniedByTitle] && [appControl.FM_ENABLE_MULTISEL_ACCOMP_BY isEqualToString:KAppControlsYESCode]) {
        popOverVC.isEdetailingAccompaniedBy = YES;
        popOverVC.popOverContentArray=accompaniedByArray;
    }
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    multiCallsPopOverController=nil;
    multiCallsPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    multiCallsPopOverController.delegate=self;
    popOverVC.popOverController=multiCallsPopOverController;
    popOverVC.titleKey=popOverString;
    if ([popOverString isEqualToString:kDoctorTitle]) {
        [multiCallsPopOverController setPopoverContentSize:CGSizeMake(400, 273) animated:YES];
        
    }
    else{
        [multiCallsPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
    }
    [multiCallsPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}



#pragma mark UITextfield methods


-(void)updateNewDoctorContactDetails
{
    //this is for the scenario where new location is created and visit has been started and in multi calls screen new doctor is being added.here update the planned visits table with newly created doctor and contact id.
    //check if planned visits doctor id is nil
    
    NSString * doctorIDQry = [NSString stringWithFormat:@"Select IFNULL(Doctor_ID,'N/A') AS Doctor_ID, IFNULL(Contact_ID,'N/A') AS Contact_ID from PHX_TBL_Planned_Visits where Planned_Visit_ID = '%@'", visitDetails.Planned_Visit_ID];
    NSMutableArray *doctorIDArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorIDQry];
    
    
    
    if (doctorIDArray.count>0) {
        NSMutableDictionary * responseDict=[doctorIDArray objectAtIndex:0];
        NSString * doctorIDfromDB = [SWDefaults getValidStringValue:[responseDict valueForKey:@"Doctor_ID"]];
        NSString * contactIDfromDB = [SWDefaults getValidStringValue:[responseDict valueForKey:@"Contact_ID"]];
        
        FMDatabase * database=[MedRepQueries getDatabase];
        [database open];
        BOOL insertIntoPlannedVisits=NO;
        BOOL insertIntoActualVisits=NO;

        NSString *fieldValue=[[NSString alloc]init];
        
        if ([doctorIDfromDB isEqualToString:@"N/A"] && [visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            fieldValue=visitDetails.selectedDoctor.Doctor_ID;
            
            @try {
                insertIntoPlannedVisits =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Doctor_ID = ?  where Planned_Visit_ID=?",fieldValue,visitDetails.Planned_Visit_ID, nil];
                
//                if (insertIntoPlannedVisits==YES) {
//                    insertIntoActualVisits =  [database executeUpdate:@"Update  PHX_TBL_Actual_Visits set Doctor_ID = ?  where Planned_Visit_ID=?",fieldValue,visitDetails.Planned_Visit_ID, nil];
//                }
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception updating database: %@", exception.reason);
            }
            @finally
            {
                [database close];
            }
            
           
            
        }
        else if ([contactIDfromDB isEqualToString:@"N/A"] && [visitDetails.Location_Type isEqualToString:kPharmacyLocation])
        {
            fieldValue=visitDetails.selectedContact.Contact_ID;
            @try {
                insertIntoPlannedVisits =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Contact_ID = ?  where Planned_Visit_ID=?",fieldValue,visitDetails.Planned_Visit_ID, nil];
                
//                if (insertIntoPlannedVisits==YES) {
//                    insertIntoActualVisits =  [database executeUpdate:@"Update  PHX_TBL_Actual_Visits set Contact_ID = ?  where Planned_Visit_ID=?",fieldValue,visitDetails.Planned_Visit_ID, nil];
//                }

                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception updating database: %@", exception.reason);
            }
            @finally
            {
                [database close];
            }
            
            
        }
        
        NSLog(@"newly created doctor/contact tbl insertion status %hhd", insertIntoPlannedVisits);
    }
}

-(void)refreshDoctorContactDetails
{
    if ([appControl.FM_VISIT_WITH_PLANNED_DOCTOR_ONLY isEqualToString:KAppControlsNOCode]) {
        
        [self updateNewDoctorContactDetails];
        if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            
            NSMutableArray *doctorObjectsArray=[MedRepQueries fetchDoctorObjectswithLocationID:visitDetails.Location_ID];
            if (doctorObjectsArray.count>0) {
                visitDetails.doctorsforLocationArray=doctorObjectsArray;
            }
            
        }
        else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation])
        {
            NSMutableArray *locationContactsArray=[MedRepQueries fetchContactsObjectsforPharmacyWithLocationID:visitDetails.Location_ID];
            
            if (locationContactsArray.count>0) {
                visitDetails.locationContactsArray=locationContactsArray;
            }
        }
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==accompaniedByTextField)
    {
        accompaniedByTextField.isReadOnly=NO;
        popOverTitle=kAccompaniedByTitle;
        [self presentPopoverfor:accompaniedByTextField withTitle:kAccompaniedByTitle withContent:[accompaniedByArray valueForKey:@"Username"]];
        return NO;
    }
    else if (textField == doctorContactTextField)
    {
        
        if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            [self presentPopoverfor:doctorContactTextField withTitle:kDoctorTitle withContent:[visitDetails.doctorsforLocationArray valueForKey:@"Doctor_Name"]];
        }
        else
        {
            [self presentPopoverfor:doctorContactTextField withTitle:kContactTitle withContent:[visitDetails.locationContactsArray valueForKey:@"Contact_Name"]];
        }
        
        popOverTitle=kContactTitle;
        return NO;
        
    }
    else if (textField==demoPlanTextField)
    {
        popOverTitle=kDemoPlanTitle;
        
        if (demoPlanObjectsArray.count>0) {
            [self presentPopoverfor:demoPlanTextField withTitle:kDemoPlanTitle withContent:[demoPlanObjectsArray valueForKey:@"Description"]];
            return NO;
        }
        
    }
    return YES;
}





#pragma Mark Task popover controller delegate

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray;
{
    
    self.visitDetails.taskArray=tasksArray;
    
    NSLog(@"visit details task array is %@", self.visitDetails.taskArray);
    
    // [self updateStatusforTasksandNotes];
    [self refreshTaskandNotesStatus:self.visitDetails];
    
    
}

-(void)notesDidClose:(NSMutableArray*)updatedNotes;

{
    self.visitDetails.notesArray=updatedNotes;
    NSLog(@"notes array in did close %@", self.visitDetails.notesArray);
    
    
    if (self.visitDetails.notesArray.count>0) {
        //[self updateStatusforTasksandNotes];
        [self refreshTaskandNotesStatus:self.visitDetails];
        
        
    }
    
    
}

-(void)didSelectPopOverControllerForEdetailingAccompaniedByMultiSelection:(NSMutableArray *)selectedIndexPathArray
{
    accompaniedByArray = selectedIndexPathArray;
    NSMutableArray *accompaniedByNameArray = [[NSMutableArray alloc]init];
    NSMutableArray *accompaniedByIDArray = [[NSMutableArray alloc]init];
    
    for (NSMutableDictionary *dictObj in accompaniedByArray) {
        
        if ([[dictObj valueForKey:@"isSelected"] isEqualToString:KAppControlsYESCode]) {
            [accompaniedByNameArray addObject:[SWDefaults getValidStringValue:[dictObj valueForKey:@"Username"]]];
            [accompaniedByIDArray addObject:[SWDefaults getValidStringValue:[dictObj valueForKey:@"User_ID"]]];
        }
    }
    
    accompaniedByTextField.text=[accompaniedByNameArray componentsJoinedByString:@","];
    accompaniedBy=[accompaniedByIDArray componentsJoinedByString:@","];
    
    visitDetails.Accompanied_By = accompaniedBy;
    [visitDetailsDict setObject:accompaniedBy forKey:@"Accompanied_By"];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popOverTitle isEqualToString:kAccompaniedByTitle]) {
        accompaniedByTextField.text=[SWDefaults getValidStringValue:[[accompaniedByArray objectAtIndex:selectedIndexPath.row] valueForKey:@"Username"]];
        
        accompaniedBy=[SWDefaults getValidStringValue:[[accompaniedByArray objectAtIndex:selectedIndexPath.row] valueForKey:@"User_ID"]];
        
        visitDetails.Accompanied_By = accompaniedBy;
        
        [visitDetailsDict setObject:accompaniedBy forKey:@"Accompanied_By"];
        
        
    }
    else if ([popOverTitle isEqualToString:kContactTitle])
    {
        
        if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            selectedDoctor=[visitDetails.doctorsforLocationArray objectAtIndex:selectedIndexPath.row];
            doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[SWDefaults getValidStringValue:selectedDoctor.Doctor_Name]];
            visitDetails.selectedDoctor=selectedDoctor;
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedDoctor.Doctor_ID] forKey:@"Doctor_ID"];
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedDoctor.Doctor_Name] forKey:@"Doctor_Name"];
            
            NSMutableDictionary* lastVisitDetailsDict=[[NSMutableDictionary alloc]init];
            if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
                lastVisitDetailsDict= [MedRepQueries fetchLastVisitDetails:[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Doctor_ID"]]];
            }
            else
            {
                lastVisitDetailsDict= [MedRepQueries fetchLastVisitDetailsforContactID:[visitDetailsDict valueForKey:@"Contact_ID"]];
            }
            NSString* nextCallObjective = [NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Next_Visit_Objective"]];
            if (![appControl.FM_SHOW_NEXT_VISIT_OBJECTIVE isEqualToString:KAppControlsYESCode] ) {
                nextCallObjectiveView.hidden=YES;
                xObjectiveTrailingConstraint.constant=8.0;
                
            }
            else{
                // nextCallObjectiveView.hidden=NO;
                //xObjectiveTrailingConstraint.constant=376.0;
            }
            nextCallObjectiveTextView.text =[NSString isEmpty:nextCallObjective]?KNotApplicable:nextCallObjective;
           
            
        }
        else
        {
             selectedContact=[visitDetails.locationContactsArray objectAtIndex:selectedIndexPath.row];
            
            doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[SWDefaults getValidStringValue:selectedContact.Contact_Name]];
            
            NSLog(@"visit details dict is %@", [visitDetailsDict description]);
            
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedContact.Contact_ID] forKey:@"Contact_ID"];
            
            
            [visitDetailsDict setValue:[SWDefaults getValidStringValue:selectedContact.Contact_Name] forKey:@"Pharmacy_Contact_Name"];
            visitDetails.selectedContact=selectedContact;
            
            //visitDetails.Planned_Visit_ID=[visitDetailsDict valueForKey:@"Planned_Visit_ID"];
            //update contact id in db
            
            //BOOL status=[MedRepQueries updateContactDetails:visitDetails];
            
        }
        
        
        
        
        
    }
    else if ([popOverTitle isEqualToString:kDemoPlanTitle])
    {
        
       
        selectedDemoPlan=[demoPlanObjectsArray objectAtIndex:selectedIndexPath.row];
        [self updateDemoPlanDetails];
        NSLog(@"selected demo plan id %@",selectedDemoPlan.Demo_Plan_ID);
        
       
        //[self reloadCollectionViewWithIndex:mediaSwipeView.currentPage];
    }
    
    
}
-(void)updateDemoPlanDetails
{
    demoPlanIDforCreatePresentation=selectedDemoPlan.Demo_Plan_ID;
    
    demoPlanTextField.text=selectedDemoPlan.Description;
    
    visitDetails.selectedDemoPlan=selectedDemoPlan;
    
    [visitDetailsDict setValue:selectedDemoPlan.Demo_Plan_ID forKey:@"Demo_Plan_ID"];
    
    NSMutableArray* tempArray=[MedRepQueries fetchMediaFileswithDemoPlanID:selectedDemoPlan.Demo_Plan_ID];
    
    productImagesArray=[[[NSMutableArray alloc] initWithArray:[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] ] mutableCopy];
    productVideoFilesArray= [[[NSMutableArray alloc] initWithArray:[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] ] mutableCopy];
    productPdfFilesArray=[[[NSMutableArray alloc] initWithArray:[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] ] mutableCopy];
    presentationsArray=[[[NSMutableArray alloc] initWithArray:[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] ] mutableCopy];
    
    
    
    //        productImagesArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
    //        productVideoFilesArray= [[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
    //        productPdfFilesArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
    //        presentationsArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
    //
    
    NSLog(@"There are %lu images %lu videos %lu pdf and %lu presentations in demoplan with id %@", (unsigned long)productImagesArray.count,(unsigned long)productVideoFilesArray.count,(unsigned long)productPdfFilesArray.count,(unsigned long)presentationsArray.count,selectedDemoPlan.Demo_Plan_ID);
    
    
    selectedMediaContentArray=[[NSMutableArray alloc]init];
    selectedMediaContentArray=productImagesArray;
    [demoPlanProductsCollectionView reloadData];
    
    [self imageButtonTapped:nil];
    
    NSLog(@"selected segment index in demo plan popover title is %ld",(long)mediaSwipeView.currentPage);
    
    currentDemoPlanData=[[NSMutableArray alloc]init];
    [currentDemoPlanData addObject:productImagesArray];
    [currentDemoPlanData addObject:productVideoFilesArray];
    [currentDemoPlanData addObject:productPdfFilesArray];
    [currentDemoPlanData addObject:presentationsArray];
    
    [mediaSwipeView reloadData];
}
-(void)UpdateDemoPlanMediaIcons{
    /** 0 images*/
    /** 1 videos*/
    /** 2 ppts*/
    /** 3 ppts*/
    
    if(currentDemoPlanData.count==4){
        
        [imageButton setImage:[UIImage imageNamed:[(NSMutableArray *)[currentDemoPlanData objectAtIndex:0] count]==0?@"MedRep_ImageBtnInActive":@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
        
        [videoButton setImage:[UIImage imageNamed:[(NSMutableArray *)[currentDemoPlanData objectAtIndex:1] count]==0?@"MedRep_VideoBtnInActive":@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];

        [pdfButton setImage:[UIImage imageNamed:[(NSMutableArray *)[currentDemoPlanData objectAtIndex:2] count]==0?@"MedRep_PDFBtnInActive":@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
        
        [pptButton setImage:[UIImage imageNamed:[(NSMutableArray *)[currentDemoPlanData objectAtIndex:3] count]==0?@"MedRep_PresentationBtnInActive":@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
    }else{
        [imageButton setImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
        [videoButton setImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
        [pdfButton setImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
        [pptButton setImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
    }

}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if([scrollView isEqual:demoPlanProductsCollectionView]) {
//        CGPoint offset = mediaScrollView.contentOffset;
//        offset.y = demoPlanProductsCollectionView.contentOffset.y;
//        [mediaScrollView setContentOffset:offset];
//    } else {
//        CGPoint offset = demoPlanProductsCollectionView.contentOffset;
//        offset.y = mediaScrollView.contentOffset.y;
//        [demoPlanProductsCollectionView setContentOffset:offset];
//    }
//}

-(void)reloadCollectionViewWithIndex:(NSInteger)index
{
    
    
    //    if (index==0) {
    //
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=productImagesArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //
    //    }
    //    else if (index==1) {
    //
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=productVideoFilesArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //
    //    }
    //    else if (index==2)
    //    {
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=productPdfFilesArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //
    //    }
    //    else if (index==3)
    //    {
    //        collectionViewDataArray=[[NSMutableArray alloc]init];
    //        collectionViewDataArray=presentationsArray;
    //        [demoPlanProductsCollectionView reloadData];
    //
    //
    //    }
    
    
    
    
    
    
    
    [segmentControl setSelectedSegmentIndex:index];
    
    if (collectionViewDataArray.count>0) {
        //        [demoPlanProductsCollectionView performBatchUpdates:^{}
        //                                                 completion:^(BOOL finished) {
        //                                                     /// collection-view finished reload
        //                                                     [demoPlanProductsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
        //                                                 }];
        
        demoPlanProductsCollectionView.contentOffset=CGPointZero;
    }
    
    
}



#pragma mark swipe view methods


- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return 4;
}


- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    NSLog(@"swipe view index did changed called current indec is %ld", (long)swipeView.currentPage);
    
    
    
}

-(void)updateSegmentIndex
{
    NSLog(@"performing selector after delay in mult visits");
    [segmentControl setSelectedSegmentIndex:selectedSegmentIndex animated:YES];
    
}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView
{
    NSLog(@"swipe view did end decelerating called in visit multiple options");
    selectedSegmentIndex=swipeView.currentPage;
    
    [self performSelector:@selector(updateSegmentIndex) withObject:self afterDelay:1.0];
    
    
    
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    demoPlanProductsCollectionView = nil;
    view = [[UIView alloc] initWithFrame:mediaSwipeView.bounds];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(150, 150);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *demoPlanCollectionView = [[UICollectionView alloc] initWithFrame:view.bounds collectionViewLayout:flowLayout];
    demoPlanCollectionView.delegate=self;
    demoPlanCollectionView.dataSource=self;
    demoPlanCollectionView.tag = index;
    
    demoPlanCollectionView.backgroundColor=[UIColor whiteColor];
    [demoPlanCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    
    
    [view addSubview:demoPlanCollectionView];
    
    
    [self reloadCollectionViewWithIndex:index];
    
    return view;
}

#pragma mark UITableViewMethods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (currentVisitCallsArray.count>0) {
        return  currentVisitCallsArray.count;
        
    }
    else
    {
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 96;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"multiCallsCell";
    
    MedRepVisitsMultiCallsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepVisitsMultiCallsTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    Calls * currentCall=[currentVisitCallsArray objectAtIndex:indexPath.row];
    cell.ContactNameLbl.text=currentCall.Contact_Name;
    cell.callStartTimelbl.text=currentCall.Call_Started_At;
    cell.demoPlanLbl.text=currentCall.Description;
    
    
    return cell;
    
}






- (IBAction)createContactTapped:(id)sender {
    
    UIButton *createContactButton=sender;
    
    
    if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        
        MedRepCreateDoctorViewController *popOverVC=[[MedRepCreateDoctorViewController alloc]init];
        popOverVC.locationName=visitDetails.Location_Name;
        popOverVC.isFromMultiCalls=YES;
        popOverVC.dismissDelegate=self;
        popOverVC.selectedLocationID=visitDetails.Location_ID;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        multiCallsPopOverController=nil;
        multiCallsPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        multiCallsPopOverController.delegate=self;
        popOverVC.createDoctorPopover=multiCallsPopOverController;
        popOverVC.titleKey=kCreateDoctor;
        CGRect relativeFrame = [createContactButton convertRect:createContactButton.bounds toView:self.view];
        [multiCallsPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
        if ([SWDefaults isRTL]) {
            [multiCallsPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        }
        else
        {
            [multiCallsPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
        }
        
    }
    else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation])
    {
        MedRepCreatePharmacyContactViewController * popOverVC=[[MedRepCreatePharmacyContactViewController alloc]init];
        popOverVC.isFromMultiCalls=YES;
        popOverVC.locationDetailsDictionary=visitDetailsDict;
        popOverVC.delegate=self;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        multiCallsPopOverController=nil;
        multiCallsPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        multiCallsPopOverController.delegate=self;
        popOverVC.createContactPopOverController=multiCallsPopOverController;
        CGRect relativeFrame = [createContactButton convertRect:createContactButton.bounds toView:self.view];
        [multiCallsPopOverController setPopoverContentSize:CGSizeMake(400, 400) animated:YES];
        if ([SWDefaults isRTL]) {
            [multiCallsPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        }
        else
        {
            [multiCallsPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
        }
    }
}
#pragma mark Create Doctor Delegate

-(void)addDoctorData:(NSMutableDictionary*)locationDetails
{
    NSLog(@"created doctor details are %@", locationDetails);
    
    createdDoctor=[[Doctors alloc]init];
    createdDoctor.Doctor_ID=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Doctor_ID"]];
    createdDoctor.Classification_2=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Class"]];
    createdDoctor.Doctor_Name=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"DoctorName"]];
    createdDoctor.Email=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Email"]];
    createdDoctor.locations=[[Locations alloc]init];
    createdDoctor.locations.Location_ID=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Location"]];
    createdDoctor.Custom_Attribute_2=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"MOHID"]];
    createdDoctor.Classification_3=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"OTCRX"]];
    createdDoctor.Classification_1=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Specialization"]];
    createdDoctor.Phone=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Telephone"]];
    createdDoctor.Custom_Attribute_1=[SWDefaults getValidStringValue:[locationDetails valueForKey:@"TradeChannel"]];
    
    
    NSLog(@"location id is %@", createdDoctor.locations.Location_ID);
    visitDetails.selectedDoctor=createdDoctor;
    visitDetails.Contact_ID=createdDoctor.Doctor_ID;
    [visitDetailsDict setValue:[SWDefaults getValidStringValue:createdDoctor.Doctor_ID] forKey:@"Contact_ID"];
    [visitDetailsDict setValue:[SWDefaults getValidStringValue:createdDoctor.Doctor_Name] forKey:@"Doctor_Name"];
    
    doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:visitDetails.selectedDoctor.Doctor_Name];
    
    [self refreshDoctorContactDetails];
    [doctorDetailCollectionView reloadData];
}

#pragma mark Create Contact Delegate

-(void)createdContactforPharmacy:(NSMutableDictionary*)pharmacyContactDetails
{
    NSLog(@"created pharmacy contact is %@", pharmacyContactDetails);
    
    Contact * createdContact=[[Contact alloc]init];
    
    createdContact.Contact_ID=[SWDefaults getValidStringValue:[pharmacyContactDetails valueForKey:@"Contact_ID"]];
    createdContact.Email=[SWDefaults getValidStringValue:[pharmacyContactDetails valueForKey:@"Contact_Email"]];
    createdContact.Contact_Name=[SWDefaults getValidStringValue:[pharmacyContactDetails valueForKey:@"Contact_Name"]];
    createdContact.Phone=[SWDefaults getValidStringValue:[pharmacyContactDetails valueForKey:@"Contact_Telephone"]];
    
    [visitDetailsDict setValue:[SWDefaults getValidStringValue:createdContact.Contact_ID] forKey:@"Contact_ID"];
    
    
    [visitDetailsDict setValue:[SWDefaults getValidStringValue:createdContact.Contact_Name] forKey:@"Pharmacy_Contact_Name"];
    visitDetails.selectedContact=createdContact;
    
    doctorContactTextField.text=[MedRepDefaults getDefaultStringForEmptyString:createdContact.Contact_Name];
    
    [self refreshDoctorContactDetails];
    [doctorDetailCollectionView reloadData];
    
}


#pragma Call Did Finish Notification

-(void)callDidFinishNotification:(NSNotification*)visitFinishNotification
{
    
    //fetchfrom db
    
    [self refreshDoctorContactDetails];
    
    VisitDetails * finishedVisit=visitFinishNotification.object;
    
    [self refreshContentAfterCall:finishedVisit];
    
    [self refreshTaskandNotesStatus:self.visitDetails];
    
    //[self updateStatusforTasksandNotes];
    
    
    currentVisitCallsArray=[[NSMutableArray alloc]init];
    
    
    currentVisitCallsArray=[MedRepQueries fetchCompletedCallsData:finishedVisit];
    
    if (currentVisitCallsArray.count>0) {
        
        
        
        [callsTableView reloadData];
    }
    
    testDate=[NSDate date];
    
    waitTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
    NSLog(@"curent visit calls array is %@", currentVisitCallsArray);
    
}

- (void)countUp
{
    seconds += 1;
    
    if (seconds == 60)
    {
        seconds = 0;
        minutes++;
        
        if (minutes == 60)
        {
            minutes = 0;
            hours++;
        }
    }
    
    waitTimeLbl.text=[NSString stringWithFormat:@"%ld:%ld:%ld",(long)hours,(long)minutes,(long)seconds ];
    
    
}

- (IBAction)waitTimeTapped:(id)sender {
    
    
    if ([waitTimeBtn.titleLabel.text isEqualToString:@"Start"])
    {
        [waitTimeBtn setTitle:@"Pause" forState:UIControlStateNormal];
        [waitTimeBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countUp) userInfo:nil repeats:YES];
        timerDown = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        
    } else if ([waitTimeBtn.titleLabel.text isEqualToString:@"Pause"])
    {
        [waitTimeBtn setTitle:@"Resume" forState:UIControlStateNormal];
        [waitTimeBtn setTitleColor:[UIColor colorWithRed:0/255 green:0/255 blue:255/255 alpha:1.0] forState:UIControlStateNormal];
        
        pauseStart = [NSDate dateWithTimeIntervalSinceNow:0];
        previousFireDate = [timer fireDate];
        [timer setFireDate:[NSDate distantFuture]];
        
    } else if ([waitTimeBtn.titleLabel.text isEqualToString:@"Resume"])
    {
        [waitTimeBtn setTitle:@"Pause" forState:UIControlStateNormal];
        [waitTimeBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        float pauseTime = -1*[pauseStart timeIntervalSinceNow];
        [timer setFireDate:[NSDate dateWithTimeInterval:pauseTime sinceDate:previousFireDate]];
    }
}


#pragma Mark Task popover controller delegate

-(void)refreshTaskandNotesStatus:(VisitDetails*)currentVisitDetails
{
    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodeForTaskandNotes:currentVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }
    
}

- (IBAction)demoPlanMediaButtonTapped:(id)sender {
    
    
}


- (IBAction)imageButtonTapped:(id)sender {
    
    [imageButton setSelected:YES];
    [videoButton setSelected:NO];
    [pdfButton setSelected:NO];
    [pptButton setSelected:NO];

    if ([imageButton isSelected])
    {
        imageButton .layer.borderWidth=2.0;
        imageButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    videoButton.layer.borderWidth=0.0;
    pdfButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;

    selectedMediaContentArray=productImagesArray;
    [demoPlanProductsCollectionView reloadData];
}

- (IBAction)videoButtonTapped:(id)sender {
    
    [videoButton setSelected:YES];
    [imageButton setSelected:NO];
    [pdfButton setSelected:NO];
    [pptButton setSelected:NO];
    
    if ([videoButton isSelected])
    {
        videoButton .layer.borderWidth=2.0;
        videoButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imageButton.layer.borderWidth=0.0;
    pdfButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    
    
    selectedMediaContentArray=productVideoFilesArray;
    [demoPlanProductsCollectionView reloadData];

}

- (IBAction)pdfButtonTapped:(id)sender {
   
    
    [pdfButton setSelected:YES];
    [videoButton setSelected:NO];
    [imageButton setSelected:NO];
    [pptButton setSelected:NO];
    
    if ([pdfButton isSelected])
    {
        pdfButton .layer.borderWidth=2.0;
        pdfButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    videoButton.layer.borderWidth=0.0;
    imageButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    
    NSLog(@"PDF files are %@", [productPdfFilesArray valueForKey:@"Caption"]);
    
    selectedMediaContentArray=productPdfFilesArray;
    [demoPlanProductsCollectionView reloadData];

}

- (IBAction)pptButtonTapped:(id)sender {
    [pptButton setSelected:YES];
    [videoButton setSelected:NO];
    [pdfButton setSelected:NO];
    [imageButton setSelected:NO];
    
    if ([pptButton isSelected])
    {
        pptButton .layer.borderWidth=2.0;
        pptButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    videoButton.layer.borderWidth=0.0;
    pdfButton.layer.borderWidth=0.0;
    imageButton.layer.borderWidth=0.0;

    selectedMediaContentArray=presentationsArray;
    [demoPlanProductsCollectionView reloadData];

}

- (IBAction)filterButtonTapped:(id)sender {
    
    popOverTitle=kDemoPlanTitle;
    if (demoPlanObjectsArray.count>0) {
        [self presentPopoverfor:demoPlanTextField withTitle:kDemoPlanTitle withContent:[demoPlanObjectsArray valueForKey:@"Description"]];
    }
    
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=[demoPlanObjectsArray valueForKey:@"Description"];
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    multiCallsPopOverController=nil;
    multiCallsPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    multiCallsPopOverController.delegate=self;
    popOverVC.popOverController=multiCallsPopOverController;
    popOverVC.titleKey=kDemoPlanTitle;
    
    [multiCallsPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [multiCallsPopOverController presentPopoverFromRect:filterButton.frame inView:filterButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
- (IBAction)createPresentationTapped:(id)sender {
    isDemoPlanSelected = true;

   // [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"HELP_VIEWED"];

    NSString* isInitialPresentation = [NSString getValidStringValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"HELP_VIEWED"]];
    
    if (![NSString isEmpty:isInitialPresentation] && [isInitialPresentation isEqualToString:KAppControlsYESCode]) {
    /*
    NSMutableArray* currentPresentationMedia=[[visitDetails.fieldMarketingPresentationsArray valueForKey:@"presentationContentArray"] objectAtIndex:0];
    NSMutableArray* refinedPresentationMedia=[[NSMutableArray alloc]init];
    for (ProductMediaFile * currentFile in currentPresentationMedia) {
        [refinedPresentationMedia addObject:currentFile];
    }
    
    SalesWorxPresentationHomeViewController * tempVC=[[SalesWorxPresentationHomeViewController alloc]init];
    tempVC.mediaArray=refinedPresentationMedia;
    [self.navigationController pushViewController:tempVC animated:YES];*/
    
    
    SalesWorxPresentationViewController * presentationVC=[[SalesWorxPresentationViewController alloc]init];
    presentationVC.selectedDemoPlanID=demoPlanIDforCreatePresentation;
    presentationVC.presentationDelegate=self;
    presentationVC.isCreatePresentation=YES;
    presentationVC.visitDetails=visitDetails;
    presentationVC.presentationNavController=self.navigationController;
    [self.navigationController pushViewController:presentationVC animated:YES];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setValue:KAppControlsYESCode forKey:@"HELP_VIEWED"];
        [self showPresentationHelpViewController];
    }
        
}
-(void)updatedVisitWithPresentations:(VisitDetails*)updatedDetails
{
    visitDetails=updatedDetails;
    
    [presentationCollectionView reloadData];
}

-(void)showPresentationHelpViewController
{
    SalesWorxPresentationHelpViewController * helpVC=[[SalesWorxPresentationHelpViewController alloc]init];
    [self presentViewController:helpVC animated:YES completion:^{
        SalesWorxPresentationViewController * presentationVC=[[SalesWorxPresentationViewController alloc]init];
        presentationVC.selectedDemoPlanID=demoPlanIDforCreatePresentation;
        presentationVC.presentationDelegate=self;
        presentationVC.isCreatePresentation=YES;
        presentationVC.visitDetails=visitDetails;
        presentationVC.presentationNavController=self.navigationController;
        [self.navigationController pushViewController:presentationVC animated:YES];
    }];
}


@end

