//
//  MedRepMeetingAttendeesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MedRepDefaults.h"
#import "SWDefaults.h"

@protocol MeetingAttendeesDelegate <NSObject>

-(void)selectedMeetingAttendees:(NSMutableArray*)selectedAttendees;

@end

@interface MedRepMeetingAttendeesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIPopoverControllerDelegate,UISearchControllerDelegate,UISearchResultsUpdating>

{
    NSMutableArray * selectedAttendeesArray;
    
    NSIndexPath * lastIndexPath;
    
    NSMutableArray * filteredPopOverContentArray;
    BOOL isSearching;
    
    id delegate;
}
@property(strong,nonatomic) NSMutableArray * meetingAttendeesArray;
@property(strong,nonatomic) NSMutableArray* selectedMeetingAttendeesArray;
@property(strong,nonatomic)UIPopoverController * meetingAttendeesPopOverController;
@property (strong, nonatomic) IBOutlet UITableView *meetingAttendeesTblView;
@property(nonatomic) id delegate;
@property(strong,nonatomic) NSMutableArray* cellSelected;
@property (strong, nonatomic) UISearchController *searchController;

@end
