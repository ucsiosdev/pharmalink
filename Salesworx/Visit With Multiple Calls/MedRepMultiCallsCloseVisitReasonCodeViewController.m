//
//  MedRepMultiCallsCloseVisitReasonCodeViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepMultiCallsCloseVisitReasonCodeViewController.h"

@interface MedRepMultiCallsCloseVisitReasonCodeViewController ()

@end

@implementation MedRepMultiCallsCloseVisitReasonCodeViewController
@synthesize headerView,contentView,contentTableView,headerTitleLbl,contentArray,contentViewHeightConstraint,contentViewHeight;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
    
    //contentViewHeightConstraint.constant=contentViewHeight;
    
    headerTitleLbl.textColor=[UIColor whiteColor];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setHiddenAnimated:NO duration:1.25];
    
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return contentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.textColor=MedRepTableViewCellTitleColor;
    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(16);

    cell.textLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[contentArray objectAtIndex:indexPath.row]];
    
    return cell;

}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.reasonCodeDelegate respondsToSelector:@selector(selectedReasonCode:)]) {
        [self.navigationController popViewControllerAnimated:NO];
        
        [self.reasonCodeDelegate selectedReasonCode:[MedRepDefaults getValidStringValue:[contentArray objectAtIndex:indexPath.row]]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];

}
@end
