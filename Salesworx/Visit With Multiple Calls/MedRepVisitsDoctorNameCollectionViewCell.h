//
//  MedRepVisitsDoctorNameCollectionViewCell.h
//  MedRep
//
//  Created by Prasann on 6/8/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepVisitsDoctorNameCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *doctorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *doctorDepartmentLabel;
@property (strong, nonatomic) IBOutlet UIImageView *tickImageView;

@end
