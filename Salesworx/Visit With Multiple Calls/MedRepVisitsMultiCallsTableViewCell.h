//
//  MedRepVisitsMultiCallsTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/23/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MedRepElementTitleLabel.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxSingleLineDarkColorLabel.h"


@interface MedRepVisitsMultiCallsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *ContactNameLbl;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *callStartTimelbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineDarkColorLabel *demoPlanLbl;

@end
