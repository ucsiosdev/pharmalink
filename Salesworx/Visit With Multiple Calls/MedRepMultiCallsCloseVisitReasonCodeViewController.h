//
//  MedRepMultiCallsCloseVisitReasonCodeViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"

@protocol ReasonCodeDelegate <NSObject>

-(void)selectedReasonCode:(NSString*)selectedReasonCode;


@end


@interface MedRepMultiCallsCloseVisitReasonCodeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UITableView *contentTableView;
@property (strong, nonatomic) IBOutlet UILabel *headerTitleLbl;
@property(strong,nonatomic) NSMutableArray * contentArray;
@property(nonatomic) CGFloat contentViewHeight;

@property(nonatomic) id reasonCodeDelegate;

- (IBAction)cancelButtonTapped:(id)sender;
@end
