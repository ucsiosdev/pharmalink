//
//  MedRepVisitsEndCallViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextView.h"
#import "SalesWorxDropShadowView.h"
#import "MedRepCustomClass.h"

#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "MedRepTextField.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepMultiCallsCloseVisitReasonCodeViewController.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxBrandAmbassadorQueries.h"
#import "SalesWorxSingleLineDarkColorLabel.h"
#import "SalesWorxNavigationHeaderLabel.h"

#define kOtherReasonType @"Other"

@protocol VisitCompletedWithOutCallsDelegate <NSObject>

-(void)visitCompletedWithOutCalls:(BOOL)status;

@end


@interface MedRepVisitsEndCallViewController : UIViewController<UITextViewDelegate,UITextFieldDelegate,SelectedFilterDelegate,ReasonCodeDelegate,UITextViewDelegate>

{
    IBOutlet UIView *headerView;
    
    IBOutlet NSLayoutConstraint *contentViewHeightConstraint;
    IBOutlet MedRepElementTitleLabel *notesTitleLbl;
    IBOutlet NSLayoutConstraint *notesTitleLblHeightConstraint;
    IBOutlet UIButton *cancelButton;
    IBOutlet NSLayoutConstraint *notesTxtViewHeightConstraint;
    IBOutlet UIButton *saveButton;
    NSMutableArray * reasonsArray;
    
    NSString* selectedReason;
    
    IBOutlet UIView *notesView;
    BOOL popToRoot;
    IBOutlet NSLayoutConstraint *notesViewHeightConstraint;
    
    NSMutableDictionary * visitAdditionalInfoDict;
    IBOutlet SalesWorxNavigationHeaderLabel *lblNavigationTitle;
    IBOutlet SalesWorxSingleLineDarkColorLabel *lblReasonTitle;
    
    
}
@property (strong, nonatomic) IBOutlet MedRepTextView *notesTxtView;
@property (strong, nonatomic) IBOutlet MedRepTextField *reasonTextField;
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *contentView;
- (IBAction)doneButtontapped:(id)sender;
@property(nonatomic) id visitCompletedDelegate;
@property(strong,nonatomic) VisitDetails* currentVisit;
@property(strong,nonatomic) NSMutableDictionary * visitDetailsDict;
@property(strong,nonatomic) UINavigationController * edetailingNavController;
@property(nonatomic) BOOL isBrandAmbassadorVisit;
@property(nonatomic) BOOL isCloseFromEdetailingStartCall;
@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * currentBrandAmbassadorVisit;

@end
