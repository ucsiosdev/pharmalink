//
//  MedRepVisitsStartMultipleCallsPresentationCollectionViewCell.m
//  MedRep
//
//  Created by Unique Computer Systems on 10/12/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "MedRepVisitsStartMultipleCallsPresentationCollectionViewCell.h"

@implementation MedRepVisitsStartMultipleCallsPresentationCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MedRepVisitsStartMultipleCallsPresentationCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
    }
    
    return self;
    
}


@end
