//
//  MedRepVisitsStartMultipleCallsPresentationCollectionViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 10/12/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"

@interface MedRepVisitsStartMultipleCallsPresentationCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImage;

@end
