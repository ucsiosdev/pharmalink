//
//  MedRepEDetailingProductsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "SwipeView.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "SWDefaults.h"
#import "MedRepProductsCollectionViewCell.h"
#import "MedRepCollectionViewCheckMarkViewController.h"
#import "MedRepUpdatedDesignCell.h"
#import "MedRepProductTableViewCell.h"
#import "MedRepCustomClass.h"
#import "MedRepHeader.h"
#import "MedRepProductImagesViewController.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepProductFilterViewController.h"
#import "ProductFilterViewController.h"
#import "MedRepProductHTMLViewController.h"

@protocol DemoedProductsOutofDemoPlanDelegate <NSObject>

-(void)demoedProductsOutOfDemoPlan:(NSMutableArray*)productsArray;

@end

@interface MedRepEDetailingProductsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,ViewedImagesDelegate,ProductVideoDelegate,ViewedPDFDelegate,UIPopoverControllerDelegate,SwipeViewDelegate,SwipeViewDataSource,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * segmentMenuArray;
    
    NSMutableArray * collectionViewDataArray;
    NSMutableArray* productImagesArray;
    NSMutableArray * productVideoFilesArray;
    NSMutableArray * productPdfFilesArray;
    NSMutableArray* presentationsArray;
    NSMutableArray* demoedMediaFiles;
    NSMutableArray*selectedIndexPathArray;
    NSMutableArray * filteredProducts;
    BOOL isSearching;
    NSString*documentsDirectory;
    NSInteger selectedSegmentIndex;
    NSInteger selectedCellIndex;
    UICollectionView * demoPlanProductsCollectionView;
    NSIndexPath * selectedIndexPath;
    NSMutableArray* indexPathArray;
    Products * selectedProduct;
    IBOutlet MedRepHeader *productNameDescriptionLbl;
    NSMutableArray * filteredProductsArray;
    NSMutableArray* productsArray;
    NSMutableArray*imagesPathArray;
    ProductMediaFile* SelectedProductMediaFile;
    
    NSMutableArray* demoPlanArray;
    UIImageView * blurredBgImage;
    NSMutableArray* unFilteredProducts;
    UIPopoverController * productsPopoverController;
    NSMutableDictionary * previousFilteredParameters;
    
    BOOL isFiltered;
    
    id delegate;
}
@property (strong, nonatomic) IBOutlet HMSegmentedControl *productsSegmentControl;
@property (strong, nonatomic) IBOutlet SwipeView *productsSwipeView;
@property (strong, nonatomic) IBOutlet UITableView *productsTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *productsSearchBar;
@property (strong, nonatomic) IBOutlet UIButton *productsFilterButton;
@property(strong,nonatomic) NSMutableArray * productsContentArray;
@property(strong,nonatomic)NSMutableArray* productMediaFilesArray;
- (IBAction)productsFIlterButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *segmentContainerView;
@property(nonatomic) id delegate;

@end
