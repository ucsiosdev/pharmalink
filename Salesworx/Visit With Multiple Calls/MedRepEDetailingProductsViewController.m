//
//  MedRepEDetailingProductsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepEDetailingProductsViewController.h"
#import "SalesworxDocumentDisplayManager.h"
@interface MedRepEDetailingProductsViewController ()
{
    SalesworxDocumentDisplayManager * docDispalayManager;
}
@end

@implementation MedRepEDetailingProductsViewController
@synthesize productsSearchBar,productsSwipeView,productsTableView,productsFilterButton,productsSegmentControl,segmentContainerView,productMediaFilesArray,productsContentArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    docDispalayManager=[[SalesworxDocumentDisplayManager alloc] init];
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Products"];
    
    imagesPathArray=[[NSMutableArray alloc]init];
    demoedMediaFiles=[[NSMutableArray alloc]init];
    
    segmentMenuArray=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"IMAGE", nil),NSLocalizedString(@"VIDEO", nil),NSLocalizedString(@"PDF", nil),NSLocalizedString(@"PRESENTATION", nil), nil];
    indexPathArray=[[NSMutableArray alloc]init];
    UIBarButtonItem * closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    
    [closeButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=closeButton;
    
    
    productsSegmentControl.sectionTitles = segmentMenuArray;
    //    productsSegmentControl.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    productsSegmentControl.backgroundColor = [UIColor clearColor];
    
    productsSegmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    productsSegmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    
    productsSegmentControl.selectionIndicatorBoxOpacity=0.0f;
    productsSegmentControl.verticalDividerEnabled=YES;
    productsSegmentControl.verticalDividerWidth=1.0f;
    productsSegmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    productsSegmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    productsSegmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    productsSegmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    productsSegmentControl.tag = 3;
    productsSegmentControl.selectedSegmentIndex = 0;
    
    
    
    
    segmentContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    segmentContainerView.layer.shadowOffset = CGSizeMake(0, -2);
    segmentContainerView.layer.shadowOpacity = 0.1;
    segmentContainerView.layer.shadowRadius = 2.0;
    
    
      [productsSegmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    
    //fetch products with media files
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [self fetchProductsData];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (productMediaFilesArray.count>0) {
                
                productImagesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
                
                
                productVideoFilesArray= [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
                
                productPdfFilesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
                
                presentationsArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
                
                collectionViewDataArray=[[NSMutableArray alloc]initWithObjects:productImagesArray,productVideoFilesArray,productPdfFilesArray,presentationsArray, nil];
                

                [productsTableView reloadData];
                [self deselectPreviousCellandSelectFirstCell];
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
    
    // Do any additional setup after loading the view from its nib.
}
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl{
    [productsSwipeView scrollToItemAtIndex:segmentedControl.selectedSegmentIndex duration:0.0];
}

-(void)fetchProductsData
{
    productMediaFilesArray=[MedRepQueries fetchMediaFilesforAllProducts];
    productImagesArray=[[NSMutableArray alloc]init];
    productVideoFilesArray=[[NSMutableArray alloc]init];
    productPdfFilesArray=[[NSMutableArray alloc]init];
    presentationsArray=[[NSMutableArray alloc]init];
    productsArray=[MedRepQueries fetchAllproductswithMediaFiles];
    unFilteredProducts=productsArray;
    
   

}

-(void)viewWillAppear:(BOOL)animated
{
    
    productsTableView.delegate=self;
    productsTableView.rowHeight = UITableViewAutomaticDimension;
    productsTableView.estimatedRowHeight = 70.0;

    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
}

-(void)closeTapped
{
    if ([self.delegate respondsToSelector:@selector(demoedProductsOutOfDemoPlan:)]) {
        
        [self.delegate demoedProductsOutOfDemoPlan:demoedMediaFiles];
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"swipe view did select called");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SwipeView Methods

#pragma mark swipe view methods


- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return segmentMenuArray.count;
}


- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    NSLog(@"swipe view index did changed called current indec is %ld", (long)swipeView.currentPage);
    
    
    
}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView
{
    NSLog(@"swipe view did end decelerating called");
    selectedSegmentIndex=swipeView.currentPage;
    
    [productsSegmentControl setSelectedSegmentIndex:swipeView.currentPage animated:YES];
    
}


- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return productsSwipeView.bounds.size;
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    demoPlanProductsCollectionView = nil;
    view = [[UIView alloc] initWithFrame:productsSwipeView.bounds];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(150, 150);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *demoPlanCollectionView = [[UICollectionView alloc] initWithFrame:view.bounds collectionViewLayout:flowLayout];
    demoPlanCollectionView.delegate=self;
    demoPlanCollectionView.dataSource=self;
    demoPlanCollectionView.tag = index;
    
    demoPlanCollectionView.backgroundColor=[UIColor whiteColor];
    [demoPlanCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    
    
    [view addSubview:demoPlanCollectionView];
    
    
    //[self reloadCollectionViewWithIndex:index];
    
    return view;
}

//- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
//{
//    demoPlanProductsCollectionView = nil;
//    
//    
//    NSLog(@"swipe view view for item at index called with view %@",view);
//    //create new view if no view is available for recycling
//    if (view == nil)
//    {
//        //don't do anything specific to the index within
//        //this `if (view == nil) {...}` statement because the view will be
//        //recycled and used with other index values later
//        view = [[UIView alloc] initWithFrame:productsSwipeView.bounds];
//        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        
//        UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
//        flowLayout.itemSize = CGSizeMake(150, 150);
//        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
//        
//        demoPlanProductsCollectionView = [[UICollectionView alloc] initWithFrame:view.bounds collectionViewLayout:flowLayout];
//        demoPlanProductsCollectionView.delegate=self;
//        demoPlanProductsCollectionView.dataSource=self;
//        demoPlanProductsCollectionView.tag = 1;
//        demoPlanProductsCollectionView.backgroundColor=[UIColor whiteColor];
//        [demoPlanProductsCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
//        demoPlanProductsCollectionView.contentSize=CGSizeMake(732*3, 0);
//        
//        [view addSubview:demoPlanProductsCollectionView];
//        
//        
//    }
//    else
//    {
//        //get a reference to the label in the recycled view
//        demoPlanProductsCollectionView = (UICollectionView *)[view viewWithTag:1];
//    }
//    
//    
//    
//    [self reloadCollectionViewWithIndex:index];
//    
//    return view;
//}




#pragma mark UICollectionView Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if(collectionView.tag==0)

    {
        return productImagesArray.count;
    }
    else if(collectionView.tag==1)
    {
        return productVideoFilesArray.count;
 
    }
    else if(collectionView.tag==2)

    {
        return productPdfFilesArray.count;

    }
    else if(collectionView.tag==3)
    {
        return presentationsArray.count;

    }
    return  0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(150, 150);
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productCell";
    NSMutableArray *currentSelectedMediaTypeDataarray=[collectionViewDataArray objectAtIndex:collectionView.tag];
    
    
     SelectedProductMediaFile=[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row];
    NSString *fileName = SelectedProductMediaFile.File_Path;
    NSString* mediaType = SelectedProductMediaFile.Media_Type;
    
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.selectedImgView.hidden=YES;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
   
    NSLog(@"file name is %@ File_Name is %@", SelectedProductMediaFile.Filename,SelectedProductMediaFile.File_Name);
   
    
    if ([mediaType isEqualToString:@"Image"])
    {
        
        
        NSString * thumbnailImagePath=[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:[kThumbnailsFolderName stringByAppendingPathComponent:[SWDefaults getValidStringValue:SelectedProductMediaFile.File_Name]]];
        cell.productImageView.image=[UIImage imageWithContentsOfFile:thumbnailImagePath];
        
        
//        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
        cell.captionLbl.text=[[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
        
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        cell.captionLbl.text=[[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        cell.captionLbl.text=[[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
        cell.captionLbl.text=[[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    
    //adding check mark to selected cell
    
    MedRepCollectionViewCheckMarkViewController* checkMarkVC=[[MedRepCollectionViewCheckMarkViewController alloc]init];
    if (demoedMediaFiles.count>0)
    {
        for (NSInteger i=0; i<demoedMediaFiles.count; i++)
        {
            ProductMediaFile* selectedFileName=[demoedMediaFiles objectAtIndex:i];
            NSString * currentFileName=[[currentSelectedMediaTypeDataarray valueForKey:@"Media_File_ID"] objectAtIndex:indexPath.row] ;
            
            //NSLog(@"selected file name : %@  current file name : %@", selectedFileName,currentFileName);
            
            if ([selectedFileName.Media_File_ID isEqualToString:currentFileName])
            {
                cell.selectedImgView.hidden=NO;
            }
            else
            {
            }
        }
    }
    
    cell.productImageView.backgroundColor=[UIColor clearColor];
    if (indexPath.row== selectedCellIndex)
    {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else
    {
        cell.layer.borderWidth=0;
    }
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSMutableArray *currentSelectedMediaTypeDataarray=[collectionViewDataArray objectAtIndex:collectionView.tag];

    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];

    selectedCellIndex=indexPath.row;
    
    SelectedProductMediaFile.isDemoed=YES;
    
    

    NSString* selectedFileID=[[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID" ] ;
    if ([demoedMediaFiles containsObject:selectedFileID]) {
        
    }
    else
    {
        //[demoedMediaFiles addObject:selectedFileID];
        
        if (![demoedMediaFiles containsObject:[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row]]) {
            [demoedMediaFiles addObject:[currentSelectedMediaTypeDataarray objectAtIndex:indexPath.row]];
        }
        
        
        
    }
    NSString* mediaType = [[currentSelectedMediaTypeDataarray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row] ;
    if ([mediaType isEqualToString:@"Image"]) {
        [imagesPathArray removeAllObjects];
        for (NSInteger i=0; i<currentSelectedMediaTypeDataarray.count; i++) {
            
            // [imagesPathArray addObject:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i]];
            
            [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:[[currentSelectedMediaTypeDataarray valueForKey:@"File_Name"] objectAtIndex:i] ]];
            
            
        }
        MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
        imagesVC.productImagesArray=imagesPathArray;
        imagesVC.imagesViewedDelegate=self;
        
        imagesVC.productDataArray=productImagesArray;
        imagesVC.currentImageIndex=indexPath.row;
        imagesVC.selectedIndexPath=indexPath;
        [self presentViewController:imagesVC animated:YES completion:nil];
    }
    
    else if ([mediaType isEqualToString:@"Video"])
    {
        
        MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
        videosVC.videosViewedDelegate=self;
        videosVC.currentImageIndex=indexPath.row;
        videosVC.selectedIndexPath=indexPath;
        videosVC.productDataArray=currentSelectedMediaTypeDataarray;
        [self.navigationController presentViewController:videosVC animated:YES completion:nil];
    }
    
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        if (currentSelectedMediaTypeDataarray.count>0) {
            MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
            pdfVC.pdfViewedDelegate=self;
            pdfVC.productDataArray=currentSelectedMediaTypeDataarray;
            pdfVC.selectedIndexPath=indexPath;
            [self presentViewController:pdfVC animated:YES completion:nil];
            
        }
        else
        {
//            UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No PDF files are available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [noDataAvblAlert show];
//            
            [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"No PDF files are available" withController:self];
            
            
        }
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        if (currentSelectedMediaTypeDataarray.count>0) {

            NSString * selectedFilePath=[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:[[presentationsArray objectAtIndex:indexPath.row]valueForKey:@"File_Name"]];
            
            /** Power point slide show files will display in external apps*/
            if([[selectedFilePath lowercaseString] hasSuffix:KppsxFileExtentionStr]){
                [docDispalayManager ShowUserActivityShareViewInView:collectionView SourceRect:cell.frame AndFilePath:selectedFilePath];
            }else{
                MedRepProductHTMLViewController *obj = [[MedRepProductHTMLViewController alloc]init];
                obj.HTMLViewedDelegate = self;
                obj.productDataArray = currentSelectedMediaTypeDataarray;
                obj.showAllProducts=YES;
                obj.isObjectData=YES;
                obj.selectedIndexPath = indexPath;
                obj.currentImageIndex=indexPath.row;
                [self presentViewController:obj animated:YES completion:nil];
            }
            
        }
        else
        {
            UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No presentation files are available " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [noDataAvblAlert show];
        }
    }
    [productsSwipeView reloadData];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma media files delegates

-(void)imagesViewedinFullScreenDemo:(NSMutableArray*)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        
        ProductMediaFile* currentMediaFile=[imagesArray objectAtIndex:i];
        
        //NSString* selectedFileID=[[imagesArray objectAtIndex:i]valueForKey:@"Media_File_ID"] ;
        if ([demoedMediaFiles containsObject:currentMediaFile]) {
            
        }
        else
        {
            [demoedMediaFiles addObject:currentMediaFile];
            
        }
        
        
    }
    
    NSLog(@"demoed files are %@", [demoedMediaFiles description]);
    
    NSLog(@"images viewed in delegate are %@", [imagesArray description]);
    
    [demoPlanProductsCollectionView reloadData];
    
    
}

#pragma mark Viewed Videos delegate

-(void)videosViewedinFullScreenDemo:(NSMutableArray *)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        
       // NSString* selectedFileID=[[imagesArray objectAtIndex:i]valueForKey:@"Media_File_ID"] ;
        
        ProductMediaFile* currentMediaFile=[imagesArray objectAtIndex:i];

        if ([demoedMediaFiles containsObject:currentMediaFile.Media_File_ID]) {
            
        }
        else
        {
            [demoedMediaFiles addObject:currentMediaFile];
            
        }
        
        
    }
    
    NSLog(@"demoed files are %@", [demoedMediaFiles description]);
    
    NSLog(@"images viewed in delegate are %@", [imagesArray description]);
    
    [demoPlanProductsCollectionView reloadData];

}


#pragma mark PDF's Viewed delegate

-(void)PDFViewedinFullScreenDemo:(NSMutableArray*)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        
       // NSString* selectedFileID=[[imagesArray objectAtIndex:i]valueForKey:@"Media_File_ID"] ;
        ProductMediaFile* currentMediaFile=[imagesArray objectAtIndex:i];

        if ([demoedMediaFiles containsObject:currentMediaFile.Media_File_ID]) {
            
        }
        else
        {
            [demoedMediaFiles addObject:currentMediaFile];
            
        }
        
        
    }
    
    NSLog(@"demoed files are %@", [demoedMediaFiles description]);
    
    NSLog(@"pdf viewed in delegate are %@", [imagesArray description]);
    
    [demoPlanProductsCollectionView reloadData];

}


#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (isSearching==YES ) {
        
        if (filteredProducts.count>0) {
            return  filteredProducts.count;
            
        }
        
        else
        {
            return 0;
        }
        
    }
    
   
    else
    {
        
        if (productsArray.count>0) {
            return productsArray.count;
            
        }
        else
        {
            return 0;
            
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString* identifier=@"updatedDesignCell";
        
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        
    if ([indexPathArray containsObject:indexPath]) {
        [cell setSelectedCellStatus];
    }
    else
    {
        [cell setDeSelectedCellStatus];
    }
    
        if (isSearching==YES && filteredProducts.count>0) {
            
            Products * currentProduct=[filteredProducts objectAtIndex:indexPath.row];
            
            cell.titleLbl.text=[[MedRepDefaults getDefaultStringForEmptyString:currentProduct.Product_Name] capitalizedString];
            cell.descLbl.text=[[MedRepDefaults getDefaultStringForEmptyString:currentProduct.Brand_Code]capitalizedString];
            
        }
    
        else
        {
            if (productMediaFilesArray.count>0) {
                Products * currentProduct=[productsArray objectAtIndex:indexPath.row];
                
                cell.titleLbl.text=[[MedRepDefaults getDefaultStringForEmptyString:currentProduct.Product_Name] capitalizedString];
                cell.descLbl.text=[[MedRepDefaults getDefaultStringForEmptyString:currentProduct.Brand_Code]capitalizedString];
                
                
            }
        }
        
        
        
    
        return cell;
        
    }



- (nullable NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        
        
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        cell.titleLbl.textColor=MedRepMenuTitleFontColor;
        cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        
        cell.cellDividerImg.hidden=NO;
        
        return indexPath;
    
    
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==productsTableView) {
       
        selectedProduct=[[Products alloc]init];
        if (isSearching==YES) {
            selectedProduct=[filteredProducts objectAtIndex:indexPath.row];
            
        }
        else
        {
            selectedProduct=[productsArray objectAtIndex:indexPath.row];
        }
        if (indexPathArray.count==0) {
            [indexPathArray addObject:indexPath];
        }
        else
        {
            indexPathArray=[[NSMutableArray alloc]init];
            [indexPathArray addObject:indexPath];
            //[indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
        }
        selectedIndexPath=indexPath;
                //display rounded value to avoid confusion
  
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        cell.titleLbl.textColor=[UIColor whiteColor];
        [cell.descLbl setTextColor:[UIColor whiteColor]];
        cell.cellDividerImg.hidden=YES;
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        [self updateProductDetails:selectedProduct];
        [productsSegmentControl setSelectedSegmentIndex:0];
        [productsTableView reloadData];
    }
    else{
        
    }
}

-(void)updateProductDetails:(Products*)selectedProductObj
{
   // NSLog(@"selected product %@ \n %@ \n %@", selectedProductFile.Entity_ID_1,selectedProductFile.Product_Name,selectedProductFile.Caption);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    productNameDescriptionLbl.text=[MedRepDefaults getDefaultStringForEmptyString:selectedProductObj.Product_Name];
    productImagesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image' AND SELF.Entity_ID_1 == %@",selectedProductObj.Product_ID]] mutableCopy];
    
    
    productVideoFilesArray= [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video' AND SELF.Entity_ID_1 == %@",selectedProductObj.Product_ID]] mutableCopy];
    
    productPdfFilesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure' AND SELF.Entity_ID_1 == %@",selectedProductObj.Product_ID]] mutableCopy];
    
    presentationsArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint' AND SELF.Entity_ID_1 == %@",selectedProductObj.Product_ID]] mutableCopy];
    
    NSLog(@"all media for this product %@", [productMediaFilesArray valueForKey:@"Entity_ID_1"]);
    
    
    collectionViewDataArray=[[NSMutableArray alloc]initWithObjects:productImagesArray,productVideoFilesArray,productPdfFilesArray,presentationsArray, nil];
    [productsSwipeView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [productsSegmentControl setSelectedSegmentIndex:0 animated:NO];
    [productsSwipeView scrollToPage:0 duration:0];

}

#pragma mark Search Methods

#pragma mark Search Bar Methods





- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [productsSearchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
    }
    else
    {
        [searchBar becomeFirstResponder];
    }
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [filteredProducts removeAllObjects];
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchContent];
    }
    else {
        isSearching = NO;
        
        if (productsArray.count>0) {
            
            
            
            [productsTableView reloadData];
            
            [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                         animated:YES
                                   scrollPosition:UITableViewScrollPositionNone];
            
            NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            
            
            
            [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:firstIndexPath];
        }
    }
    // [self.tblContentList reloadData];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [productsSearchBar setText:@""];
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
    isSearching=NO;
    if ([searchBar isFirstResponder]) {
        
        [searchBar resignFirstResponder];
    }
    
    
    [self.view endEditing:YES];
    
    
    if (productsArray.count>0) {
        [productsTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
        
        
    }
   
}



-(void)deselectPreviousCellandSelectFirstCell
{
    if ([productsTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[productsTableView cellForRowAtIndexPath:selectedIndexPath];
        if (cell)
        {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]animated:YES scrollPosition:UITableViewScrollPositionNone];
        [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    
    isSearching=YES;
    
    
    //[productSearchBar resignFirstResponder];
    
    
    // [self searchContent];
}

-(void)searchContent

{
    
    NSString *searchString = self.productsSearchBar.text;
    NSLog(@"searching with text in medrepedetailingproductsviewcontroller %@",searchString);
    
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate* predicateTest = [NSPredicate predicateWithFormat:filter, @"Doctor_Name", searchString];
    
    
    
    NSPredicate *predicate;// = [NSPredicate predicateWithFormat:@"SELF.Product_Name contains[cd] %@", [NSString stringWithFormat:@"%@",searchString]];
    
    predicate=[SWDefaults fetchMultipartSearchPredicate:searchString withKey:@"Product_Name"];
    
    
    filteredProducts=[[productsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    
    //  NSLog(@"filtered doctors array is %@", [filteredProducts description]);
    
    
    if (filteredProducts.count>0) {
        [productsTableView reloadData];
        
        
        
        [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                     animated:YES
                               scrollPosition:UITableViewScrollPositionNone];
        
        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        
        
        
        [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:firstIndexPath];
        
        
    }
    else if (filteredProducts.count==0)
    {
        
        
        isSearching=YES;
        [productsTableView reloadData];
 
   
    }
    

    
}


#pragma mark filter methods


-(void)filteredProduct:(NSMutableArray*)filteredContent
{
    
   // [self addKeyBoardObserver];
    
    
    if (filteredContent.count>0) {
        
        
        isFiltered=YES;
        
        [productsPopoverController dismissPopoverAnimated:YES];
        
        [blurredBgImage removeFromSuperview];
        
        
        productsArray=[[NSMutableArray alloc]init];
        
        productsArray=filteredContent;
        
        
        //filteredProducts=filteredContent;
        
        
        // NSLog(@"filtered products in delegate %@",[productsArray description]);
        
        //isSearching=YES;
        [productsTableView reloadData];
        
        if ([productsTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
            
            [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                         animated:YES
                                   scrollPosition:UITableViewScrollPositionNone];
        
        [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    }
}

-(void)filteredProductParameters:(NSMutableDictionary*)filterParameters
{
    self.view.alpha=1.0;
    
    if ([self.view.subviews containsObject:blurredBgImage]) {
        [blurredBgImage removeFromSuperview];
        
    }
    
    
    previousFilteredParameters=filterParameters;
    
}


-(void)productFilterDidCancel
{
    
    
    
    
    [blurredBgImage removeFromSuperview];
    
    
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    if (isFiltered==YES) {
        
    }
    
    else
    {
        
        
    }
    
    [productsPopoverController dismissPopoverAnimated:YES];
    
    isSearching=NO;
    
    productsArray=unFilteredProducts;
    
    [productsTableView reloadData];
    
    if ([productsTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        
        [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                     animated:YES
                               scrollPosition:UITableViewScrollPositionNone];
    
    [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    
    
    
    
    
    
    
}

- (IBAction)productsFIlterButtonTapped:(id)sender {
    
   
    
    
    NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    
    
    if (selectedIndexPathArray.count>0) {
        [selectedIndexPathArray replaceObjectAtIndex:0 withObject:firstIndexPath];
        
        
    }
    
    
    
    
    UIButton* filterBtn=(UIButton*)sender;
    
    NSMutableArray* demoPlanNamesArray=[[NSMutableArray alloc]init];
    
    ProductFilterViewController *viewController = [[ProductFilterViewController alloc] initWithNibName:@"ProductFilterViewController" bundle:nil];
    
    
    demoPlanArray=[[NSMutableArray alloc]init];
    
    NSString* currentDate=[MedRepQueries fetchDatabaseDateFormat];
    
    
    demoPlanArray=[MedRepQueries fetchDemoPlanforProducts:currentDate];
    for (NSInteger i=0; i<demoPlanArray.count; i++) {
        
        [demoPlanNamesArray addObject:[[demoPlanArray  objectAtIndex:i]valueForKey:@"Description"]];
    }
    
    UIView * tempView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 768)];
    
    
    
    /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
    
    // Blurred with UIImage+ImageEffects
    
    
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    
    
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    
    //    blurredBgImage.image = [MedRepDefaults blurWithImageEffects:[MedRepDefaults takeSnapshotOfView:[self view]]];
    
    
    
    [self.view addSubview:blurredBgImage];
    
    
    
    [productsSearchBar setShowsCancelButton:NO animated:YES];
    productsSearchBar.text=@"";
    
    [self.view endEditing:YES];
    isSearching=NO;
    
    [productsTableView reloadData];
    [self deselectPreviousCellandSelectFirstCell];
    
    
    MedRepProductFilterViewController * popOverVC=[[MedRepProductFilterViewController alloc]init];
    
    //fetch products with stock
    
    popOverVC.isEdetailingProduct=YES;
    popOverVC.contentArray=unFilteredProducts;
    // NSLog(@"products array before filter %@", [popOverVC.contentArray description]);
    
    
    popOverVC.filteredProductDelegate=self;
    
    popOverVC.navController=self.navigationController;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    
    
    
    productsPopoverController=nil;
    productsPopoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    productsPopoverController.delegate=self;
    popOverVC.filterPopOverController=productsPopoverController;
    popOverVC.filteredProductDelegate=self;
    popOverVC.previouslyFilteredContent=previousFilteredParameters;
    [productsPopoverController setPopoverContentSize:CGSizeMake(366, 600) animated:YES];
    
    
    CGRect btnFrame=filterBtn.frame;
    
    btnFrame.origin.x=btnFrame.origin.x+3;
    
    // self.view.alpha=0.70;
    
    
    demoPlanArray=[[NSMutableArray alloc]init];
    
    NSString* currentDt=[MedRepQueries fetchDatabaseDateFormat];
    
    
    demoPlanArray=[MedRepQueries fetchDemoPlanforProducts:currentDt];
    
    NSMutableArray* tempArray=[[NSMutableArray alloc]init];
    
    
    
    for (NSInteger i=0; i<tempArray.count; i++) {
        
        [demoPlanNamesArray addObject:[[demoPlanArray  objectAtIndex:i]valueForKey:@"Description"]];
    }
    
    // NSLog(@"demo  names are %@",[demoPlanNamesArray description]);
    
    
    CGRect currentFrame=CGRectMake(222, 18, 30, 20);
    
    
    CGRect relativeFrame = [filterBtn convertRect:filterBtn.bounds toView:self.view];

    [productsPopoverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    

    
    
    
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return NO;
}




@end
