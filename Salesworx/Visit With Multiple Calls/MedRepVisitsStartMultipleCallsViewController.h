//
//  MedRepVisitsStartMultipleCallsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "MedRepTextField.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepProductsCollectionViewCell.h"
#import "MedRepCollectionViewCheckMarkViewController.h"
#import "MedRepProductImagesViewController.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepCustomClass.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepEdetailTaskViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepEDetailingStartCallViewController.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepEDetailingEndVisitViewController.h"
#import "SwipeView.h"
#import "MedRepCreateDoctorViewController.h"
#import "MedRepCreatePharmacyContactViewController.h"
#import "MedRepVisitsEndCallViewController.h"
#import "CSPausibleTimer.h"
#import "SalesWorxImageView.h"
#import "SalesWorxDefaultTextView.h"
#import "SalesWorxPresentationViewController.h"
#import "MedRepVisitsStartMultipleCallsPresentationCollectionViewCell.h"
#import "SalesWorxPresentationHomeViewController.h"
#import "SalesWorxPresentationHelpViewController.h"
#import "MedRepElementTitleLabel.h"

#define kVisitsSegmentControlWidth 720
#define kContactNamePopOver @"ContactNamePopOver"
#define kAccompaniedByPopOver @"AccompaniedByPopOver"

#define kAccompaniedByTitle @"Accompanied By"
#define kDemoPlanTitle @"Demo Plan"
#define kImageMediaType @"Image"
#define kVideoMediaType @"Video"



@interface MedRepVisitsStartMultipleCallsViewController : UIViewController<UIScrollViewDelegate,UIPopoverControllerDelegate,StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate,TaskPopOverDelegate,EDetailingNotesDelegate,SalesWorxPopoverControllerDelegate,SwipeViewDataSource, SwipeViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate,createDoctorDelegate,CreateContactDelegate,VisitCompletedWithOutCallsDelegate,SalesWorxPresentationDelegate>

{
    IBOutlet UIButton *waitTimeBtn;
    IBOutlet SalesWorxImageView *taskStatusImageView;
    IBOutlet MedRepElementTitleLabel *nextVisitObjectiveTitleLabel;
    
    IBOutlet SalesWorxImageView *notesStatusImageView;
    IBOutlet SwipeView *mediaSwipeView;
    IBOutlet UITextView *locationNameTextField;
    IBOutlet UIView *segmentContainerView;
    IBOutlet MedRepElementTitleLabel *doctorContactHeaderLbl;
    IBOutlet MedRepTextField *doctorContactTextField;
    IBOutlet MedRepElementDescriptionLabel *locationNameLbl;
    NSString* accompaniedBy;
    NSMutableArray * demoPlanObjectsArray;
    DemoPlan * selectedDemoPlan;
    
    Doctors * selectedDoctor;
    
    NSDate *pauseStart, *previousFireDate;

    NSTimer * timer,*timerDown;
    
    NSInteger seconds,minutes,hours;
    
    NSTimer * waitTimer;
    
    NSDate * testDate;
    AppControl* appControl;
    
    Contact * selectedContact;
    
    NSMutableArray* accompaniedByArray;
    StringPopOverViewController_ReturnRow* locationPopOver;

    
    IBOutlet SalesWorxDefaultTextView *objectiveTxtView;
    IBOutlet UIButton *tasksButton;
    IBOutlet UIButton *notesButton;
    
    IBOutlet UIScrollView *demoPlanMediaScrollView;
    
    IBOutlet UIButton *taskStatusButton;
    IBOutlet UIButton *notesStatusButton;

    UIPopoverController * popOverController,*accompaniedByPopOver;
    
    
    IBOutlet MedRepTextField *accompaniedByTextField;
    IBOutlet UILabel *timeOfArrivalLbl;
    
    IBOutlet UILabel *waitTimeLbl;
    
    bool goToNextScreen;

    ProductMediaFile* SelectedProductMediaFile;

    
    
    
    NSMutableArray * collectionViewDataArray;
    NSMutableArray * productImagesArray;
    NSMutableArray * productVideoFilesArray;
    NSMutableArray * productPdfFilesArray;
    NSMutableArray*  presentationsArray;
    NSMutableArray * demoedMediaFiles;
    NSMutableArray * imagesPathArray;
    NSMutableArray * productMediaFilesArray;
    
    NSInteger selectedCellIndex;
    NSIndexPath *selectedIndexPathOfDoctorCollectionView;
    
    NSMutableArray * segmentMenuArray;
    
    NSMutableArray * contactsArray;
    
    NSString* popOverTitle;
    
    UIPopoverController * multiCallsPopOverController;
   // UICollectionView *demoPlanProductsCollectionView;
    
    NSInteger selectedSegmentIndex;
    
    NSMutableArray * currentVisitCallsArray;
    
    Doctors * createdDoctor;
    
    NSMutableArray * currentDemoPlanData;
    
    
    IBOutlet UIButton *addDoctorButton;
    
    BOOL isDemoPlanSelected;

    NSMutableArray* selectedMediaContentArray;
    
    
    IBOutlet MedRepButton *imageButton;
    
    IBOutlet MedRepButton *videoButton;
    
    IBOutlet MedRepButton *pdfButton;
    
    
    IBOutlet MedRepButton *pptButton;
    
    
    IBOutlet UICollectionView *presentationCollectionView;
    NSMutableArray* customPresentationsArray;
    
    NSMutableArray* unfilteredFieldMarketingPresentationsArray
    ;
    
    
    NSString* demoPlanIDforCreatePresentation;
    
    IBOutlet SalesWorxDropShadowView *presentationsContainerView;
    
    IBOutlet NSLayoutConstraint *objectiveViewWidthConstraint;
    
    IBOutlet NSLayoutConstraint *nextVisitObjectiveTrailingConstraint;
    IBOutlet NSLayoutConstraint *nextVisitObjectiveLeadingConstraint;
    
    
    
    IBOutlet NSLayoutConstraint *xObjectiveTrailingConstraint;
    IBOutlet SalesWorxDropShadowView *nextCallObjectiveView;
    IBOutlet NSLayoutConstraint *xPresentationViewHeightConstraint;
    
    IBOutlet MedRepTextView *nextCallObjectiveTextView;
    IBOutlet NSLayoutConstraint *xMediaCollectionViewBottomConstraint;
    
    IBOutlet NSLayoutConstraint *xPresentationViewTopConstraint;
    IBOutlet NSLayoutConstraint *xMediaViewHeightConstraint;
    IBOutlet NSLayoutConstraint *xPresentationViewBottonConstraint;
    IBOutlet UICollectionView *doctorDetailCollectionView;
}
- (IBAction)waitTimeTapped:(id)sender;
- (IBAction)createContactTapped:(id)sender;
@property(strong,nonatomic) VisitDetails * visitDetails;
@property(strong,nonatomic)     UIScrollView * mediaScrollView;
@property(strong,nonatomic)     NSMutableArray * items;

@property (strong, nonatomic) IBOutlet UITableView *callsTableView;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *segmentControl;
@property (strong, nonatomic) IBOutlet UICollectionView *demoPlanProductsCollectionView;
@property (strong, nonatomic) IBOutlet MedRepTextField *demoPlanTextField;
@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;
- (IBAction)taskButtonTapped:(id)sender;
- (IBAction)notesButtonTapped:(id)sender;

@property(nonatomic) BOOL isFromDashboard;
- (IBAction)imageButtonTapped:(id)sender;
- (IBAction)videoButtonTapped:(id)sender;
- (IBAction)pdfButtonTapped:(id)sender;
- (IBAction)pptButtonTapped:(id)sender;
- (IBAction)filterButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *filterButton;

@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *mediaView;

- (IBAction)createPresentationTapped:(id)sender;


@end

