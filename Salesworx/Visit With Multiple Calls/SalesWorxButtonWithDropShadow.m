//
//  SalesWorxButtonWithDropShadow.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/22/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxButtonWithDropShadow.h"

@implementation SalesWorxButtonWithDropShadow

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.*/


 - (void)drawRect:(CGRect)rect {
 UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
 self.layer.masksToBounds = NO;
 self.layer.shadowColor = [UIColor whiteColor].CGColor;
 self.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
 self.layer.shadowOpacity = 0.5f;
 self.layer.shadowPath = shadowPath.CGPath;
 }


@end
