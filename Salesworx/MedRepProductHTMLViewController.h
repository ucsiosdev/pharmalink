//
//  MedRepProductHTMLViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 6/29/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "NSString+Additions.h"
#import "SWDatabaseManager.h"

@protocol HTMLViewedDelegate <NSObject>

-(void)fullScreenHTMLDidCancel;
-(void)htmlViewedinDemo:(NSMutableArray*)htmlArray;

@optional
-(void)setBorderOnLastViewedImage:(NSInteger)index;
@end

@interface MedRepProductHTMLViewController : UIViewController<MFMailComposeViewControllerDelegate,UIWebViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *viewedFiles;
    NSMutableArray* selectedIndexPathArray;
    
    NSInteger selectedCellIndex;
    NSDate* viewedTime;
    NSInteger demoTimeInterval;

    BOOL isEmailTapped;
    BOOL sendEmail;
    
    NSTimer* demoTimer;
    NSURLRequest *req;
    
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *emailButton;
    NSString *emailIDForAutoInsert;
}
@property(strong,nonatomic) SalesWorxVisit* currentVisit;
@property(nonatomic)BOOL showAllProducts;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UICollectionView *productHTMLCollectionView;

@property(strong,nonatomic) id HTMLViewedDelegate;
@property(strong,nonatomic)NSMutableArray* productDataArray;
@property(strong,nonatomic)NSIndexPath* selectedIndexPath;
@property(nonatomic)    NSInteger currentImageIndex;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property(nonatomic) BOOL staticData;
@property(nonatomic) BOOL isObjectData;
@property(nonatomic) BOOL isFromProductsScreen;



- (IBAction)emailButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;
@property(nonatomic) BOOL isCurrentUserDoctor;
@property(nonatomic) BOOL isCurrentUserPharmacy;
@property(strong,nonatomic) NSString *doctor_ID ;
@property(strong,nonatomic) NSString *contact_ID ;

@end
