//
//  MedrepPharmacyLocationsTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedrepPharmacyLocationsTableViewCell.h"

@implementation MedrepPharmacyLocationsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSelectedCellStatus
{
    
    self.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    [self.descLbl setTextColor:MedRepMenuTitleSelectedCellTextColor];
    [self.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
}
-(void)setDeSelectedCellStatus
{
    self.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    self.descLbl.textColor=MedRepDescriptionLabelUnselectedColor;
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.dividerImgView.hidden=NO;
}

@end
