//
//  AES.h
//  HelloPhone
//
//  Created by Harpal Singh on 4/18/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>


@interface AES : NSObject {
    
}

- (NSString *) encrypt:(NSString *) inputData withKey:(NSString *) key;
- (NSString *) decrypt:(NSString *) inputData withKey:(NSString *) key;
- (NSData *) transform:(CCOperation) encryptOrDecrypt data:(NSData *) inputData withKey:(NSString *) key;

@end
