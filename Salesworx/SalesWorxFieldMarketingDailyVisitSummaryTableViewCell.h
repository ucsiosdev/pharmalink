//
//  SalesWorxFieldMarketingDailyVisitSummaryTableViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxFieldMarketingDailyVisitSummaryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblLocationName;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblProductDemonstrated;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblSamplesGiven;


@end
