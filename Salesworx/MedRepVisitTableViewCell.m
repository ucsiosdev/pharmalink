//
//  MedRepVisitTableViewCell.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/28/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "MedRepVisitTableViewCell.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@implementation MedRepVisitTableViewCell
@synthesize isCellSelected,cellStatusViewType;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLbl.font= MedRepHeaderTitleFont;
    self.nameLbl.font= MedRepHeaderTitleFont;
    self.descLbl.font = kSWX_FONT_SEMI_BOLD(14);
    cellStatusViewType=KSelectionColorStatusView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSelectedCellStatus
{
    
    self.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    self.nameLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    [self.descLbl setTextColor:MedRepMenuTitleSelectedCellTextColor];
    self.cellDividerImg.hidden=YES;
    self.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    [self.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    
    if(cellStatusViewType==KSelectionColorStatusView)
    {
        self.statusView.backgroundColor =MedRepUITableviewSelectedCellStatusViewBackgroundColor;
        
    }
    else if(cellStatusViewType==KCustomColorStatusView)
    {
    }
    else if(cellStatusViewType==KNoColorStatusView)
    {
        [self.statusView setHidden:YES];
    }
}
-(void)setDeSelectedCellStatus
{
    
    self.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    self.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    self.nameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    self.descLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    [self.contentView setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
    self.cellDividerImg.hidden=NO;
    
    if(cellStatusViewType==KSelectionColorStatusView)
    {
        self.statusView.backgroundColor =[UIColor clearColor];
    }
    else if(cellStatusViewType==KCustomColorStatusView)
    {
    }
    else if(cellStatusViewType==KNoColorStatusView)
    {
        [self.statusView setHidden:YES];
    }
}


@end
