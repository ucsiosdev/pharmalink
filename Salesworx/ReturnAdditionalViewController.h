//
//  ReturnAdditionalViewController.h
//  SWCustomer
//
//  Created by msaad on 1/2/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//


#import "SWPlatform.h"
#import "JBSignatureController.h"


@interface ReturnAdditionalViewController : SWViewController <SWSignatureViewDelegate,JBSignatureControllerDelegate,EditableCellDelegate,UITextFieldDelegate,UITextViewDelegate, UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
{
    NSMutableDictionary *salesOrder;
    NSMutableDictionary *info;
    JBSignatureController *signatureController;
    UIView *signatureBaseView;
    NSMutableDictionary *form;
    NSDictionary *customerDict;
    UITableView *tableView;
    CustomerHeaderView *customerHeaderView;
    NSString *performaOrderRef;
    UIPopoverController *collectionTypePopOver;
    NSString *returnTypeString;
    NSString *isGoodCollectedString;
    UIImage *signatureSaveImage;
    NSString *isSignatureOptional;
    
    BOOL scrollViewDelegateFreezed;
    BOOL isSignature ;
    //

    UIView *myBackgroundView;
}

- (id)initWithOrder:(NSMutableDictionary *)so andCustomer:(NSDictionary *)customer;
@end