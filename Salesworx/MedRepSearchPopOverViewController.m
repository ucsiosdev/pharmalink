//
//  MedRepSearchPopOverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/3/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepSearchPopOverViewController.h"

@interface MedRepSearchPopOverViewController ()

@end

@implementation MedRepSearchPopOverViewController
@synthesize popoverControllerSearchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    [popoverControllerSearchBar becomeFirstResponder];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [popoverControllerSearchBar setValue:@"Reset" forKey:@"_cancelButtonText"];

}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [popoverControllerSearchBar setShowsCancelButton:YES animated:NO];
    
    
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if ([self.searchPopoverDelegate respondsToSelector:@selector(medRepPopOverControllerSearchString:)]) {
        
        [self.searchPopoverDelegate medRepPopOverControllerSearchString:searchBar.text];
        
        
    }

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    
    
    
    if ([self.searchPopoverDelegate respondsToSelector:@selector(medRepPopOverControllerSearchString:)]) {
        
        [self.searchPopoverDelegate medRepPopOverControllerSearchString:searchBar.text];
        
        
    }
    
    [self.view endEditing:YES];

    [self.popOverController dismissPopoverAnimated:YES];
    

}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Cancel clicked");
    [self.view endEditing:YES];
    
    [popoverControllerSearchBar setShowsCancelButton:NO animated:YES];
   
    
    [self.popOverController dismissPopoverAnimated:YES];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
