////
////  PaymentReportViewController.h
////  Salesworx
////
////  Created by msaad on 6/13/13.
////  Copyright (c) 2013 msaad. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "SWPlatform.h"
//#import "CustomersListViewController.h"
//#import "CustomerPopOverViewController.h"
//
//@interface PaymentReportViewController : SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
//{
//    GridView *gridView;
//    IBOutlet UITableView *filterTableView;
//    
//    UIPopoverController *currencyTypePopOver;
//    UIPopoverController *datePickerPopOver;
//    UIPopoverController *customPopOver;
//    
//    CustomersListViewController *currencyTypeViewController;
//    CustomerPopOverViewController *customVC;
//
//    NSMutableDictionary *customerDict;
//    
//    NSDate *selectedDate;
//
//    
//    NSString *fromDate;
//    NSString *toDate;
//    BOOL isFromDate;
//    
//    NSString *custType;
//    NSString *DocType;
//    BOOL isCustType;
//    NSMutableArray *dataArray;
//
//}
//@end




//
//  PaymentReportViewController.h
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "CustomersListViewController.h"
#import "CustomerPopOverViewController.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
@interface PaymentReportViewController : SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
{
    GridView *gridView;
    IBOutlet UITableView *filterTableView;
    
    UIPopoverController *currencyTypePopOver;
    UIPopoverController *datePickerPopOver;
    UIPopoverController *customPopOver;
    
    CustomersListViewController *currencyTypeViewController;
    CustomerPopOverViewController *customVC;
    
    NSMutableDictionary *customerDict;
    
    NSDate *selectedDate;
    
    
    NSString *fromDate;
    NSString *toDate;
    BOOL isFromDate;
    
    NSDate * minDate;
    
    
    NSString *custType;
    NSString *DocType;
    BOOL isCustType;
    NSMutableArray *dataArray;
    
}

@property(strong,nonatomic)SWDatePickerViewController *datePickerViewControllerDate;
@end

