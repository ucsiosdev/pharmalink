//
//  SWCustomer.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWCustomer_1.h"

@implementation SWCustomer_1

@synthesize customerNumber;
@synthesize siteUseId;
@synthesize customerId;
@synthesize location;
@synthesize customerName;
@synthesize address;
@synthesize city;
@synthesize postalCode;
@synthesize customerStatus;
@synthesize customerBarCode;
@synthesize department;
@synthesize customerSegmentId;
@synthesize salesDistrictId;
@synthesize salesRepId;



@end