////
////  SalesSummaryViewController.m
////  Salesworx
////
////  Created by msaad on 6/13/13.
////  Copyright (c) 2013 msaad. All rights reserved.
////
//
//#import "SalesSummaryViewController.h"
//@interface SalesSummaryViewController ()
//
//@end
//
//@implementation SalesSummaryViewController
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
////- (void)dealloc
////{
////    NSLog(@"Dealloc %@",self.title);
////    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//        self.view.backgroundColor = [UIColor whiteColor];
//    gridView = [[GridView alloc] initWithFrame:CGRectMake(302, 27, 695, 600)];
//    [gridView setDataSource:self];
//    [gridView setDelegate:self];
//    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [gridView.layer setBorderWidth:1.0f];
//    [self.view addSubview:gridView];
//    
//    filterTableView.backgroundView=nil;[filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [filterTableView.layer setBorderWidth:1.0f];
//    
//    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
//    datePickerViewController.isRoute=NO;
//    datePickerViewController.forReports=YES;
//    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
//    navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    navigationController.navigationBar.translucent = NO;
//    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
//    datePickerPopOver.delegate=self;
//    // Do any additional setup after loading the view.
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    toDate=[formatter stringFromDate:[NSDate date]];
//    
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//    [offsetComponents setMonth:-1]; // note that I'm setting it to -1
//    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
//    DocType = @"All";
//    custType = @"All";
//    formatter=nil;
//    usLocale=nil;
//    [self viewReportAction];
//}
//-(void)viewWillAppear:(BOOL)animated
//{
//    //NSLog(@"Sales Documents");
//    [self viewReportAction];
//}
//- (void)typeChanged:(NSString *)sender
//{
//    [customPopOver dismissPopoverAnimated:YES];
//    if(isCustType)
//    {
//        custType=sender;
//    }
//    else
//    {
//        DocType = sender;
//    }
//    [filterTableView reloadData];
//    [self viewReportAction];
//
//}
//- (void)dateChanged:(SWDatePickerViewController *)sender
//{
//    [datePickerPopOver dismissPopoverAnimated:YES];
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    if(isFromDate)
//    {
//        fromDate=[formatter stringFromDate:sender.selectedDate];
//    }
//    else
//    {
//        toDate = [formatter stringFromDate:sender.selectedDate];
//    }
//    [filterTableView reloadData];
//    formatter=nil;
//    usLocale=nil;
//    [self viewReportAction];
//
//}
//- (void)currencyTypeChanged:(NSDictionary *)customer {
//    [currencyTypePopOver dismissPopoverAnimated:YES];
//    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//    [filterTableView reloadData];
//    [self viewReportAction];
//
//}
//
//#pragma TableView Delegate
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 44;
//}
//- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
//        return 5;
//    
//}
//- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//        return 44;
//}
//- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
//    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
//    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
//    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
//    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
//    //    [companyNameLabel setShadowColor:[UIColor whiteColor]];
//    //    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
//    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
//    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
//    
//    [headerView addSubview:companyNameLabel];
//    return headerView;
//
//}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//	return NSLocalizedString(@"Filter", nil);
//}
//- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    @autoreleasepool
//    {
//        NSString *CellIdentifier = @"CustomerDetailViewCell";
//        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (!cell)
//        {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
//        }
//
//        if(indexPath.row==0)
//        {
//            if(customerDict.count!=0)
//            {
//                cell.textLabel.text = [customerDict stringForKey:@"Customer_Name"];
//            }
//            else
//            {
//                cell.textLabel.text = NSLocalizedString(@"Select Customer", nil);
//            }
//        }
//        else if(indexPath.row==1)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"From Date", nil)];
//            [cell.detailTextLabel setText:fromDate];
//        }
//        else if(indexPath.row==2)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"To Date", nil)];
//            [cell.detailTextLabel setText:toDate];
//        }
//        else if(indexPath.row==3)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"Customer Type", nil)];
//            [cell.detailTextLabel setText:custType];
//        }
//        else if(indexPath.row==4)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
//            [cell.detailTextLabel setText:DocType];
//        }
//        else
//        {
//            [cell.textLabel setText:@"View Report"];
//
//        }
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        cell.textLabel.font=LightFontOfSize(14.0f);
//        cell.detailTextLabel.font=LightFontOfSize(14.0);
//
//        return cell;
//    }
//}
//#pragma mark UITableView Delegate
//- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row==0)
//    {
//        currencyTypeViewController=nil;
//        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
//        [currencyTypeViewController setTarget:self];
//        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
//        [currencyTypeViewController setContentSizeForViewInPopover:CGSizeMake(400,600)];
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
//        navigationController.navigationBar.barStyle = UIBarStyleBlack;
//        navigationController.navigationBar.translucent = NO;
//        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
//        currencyTypePopOver.delegate=self;
//        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
//    }
//    else if(indexPath.row==1)
//    {
//        isFromDate=YES;
//        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else if(indexPath.row==2)
//    {
//        isFromDate=NO;
//        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else if(indexPath.row==3)
//    {
//        customVC=nil;
//        customVC = [[CustomerPopOverViewController alloc] init] ;
//        [customVC setTarget:self];
//        [customVC setAction:@selector(typeChanged:)];
//        [customVC setContentSizeForViewInPopover:CGSizeMake(200,250)];
//        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
//        customPopOver.delegate=self;
//        isCustType=YES;
//        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Cash",@"Credit" , nil]];
//        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else if(indexPath.row==4)
//    {
//        customVC=nil;
//        customVC = [[CustomerPopOverViewController alloc] init] ;
//        [customVC setTarget:self];
//        [customVC setAction:@selector(typeChanged:)];
//        [customVC setContentSizeForViewInPopover:CGSizeMake(200,250)];
//        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
//        customPopOver.delegate=self;
//        isCustType=NO;
//        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil]];
//        [customPopOver presentPopoverFromRect:CGRectMake(280,235, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else
//    {
//                
//    }
//    [tv deselectRowAtIndexPath:indexPath animated:YES];
//
//}
//
//#pragma GridView Delegate
//- (int)numberOfRowsInGrid:(GridView *)gridView {
//    return dataArray.count;
//}
//
//- (int)numberOfColumnsInGrid:(GridView *)gridView {
//    return 7;
//}
//
//- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
//    NSString *title = @"";
//    
//    if (column == 0) {
//        title = NSLocalizedString(@"Document No", nil);//changed Invoice No to Document No. OLA!!
//    } else if (column == 1) {
//        title = NSLocalizedString(@"Doc Type", nil);//changed Category to Doc Type. OLA!!
//    } else if (column == 2) {
//        title = NSLocalizedString(@"Date", nil);//changed Code to Date. Corresponding change in displayed values. OLA!!
//    } else if(column == 3) {
//        title = NSLocalizedString(@"Name", nil);
//    } else if(column == 4){
//        title = NSLocalizedString(@"Amount", nil);
//    }else if(column == 5) {
//        title = NSLocalizedString(@"Date", nil);
//    } else if(column == 6){
//        title = NSLocalizedString(@"Timestamp", nil);
//    }
//    return title;
//}
//
//- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
//    NSDictionary *data = [dataArray objectAtIndex:row];
//    
//    NSString *value = @"";
//    if (column == 0) {
//        value = [NSString stringWithFormat:@"     %@", [data stringForKey:@"ERP_Ref_No"]];
//    } else if (column == 1) {
//        if([[data stringForKey:@"Doc_Type"] isEqualToString:@"I"])
//        {
//            value = @"Invoice";
//        }
//        else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"R"])
//        {
//            value = @"Credit Note";
//        }
//    } else if (column == 2) {
//       value = [data dateStringForKey:@"Creation_Date" withDateFormat:NSDateFormatterMediumStyle];//[data stringForKey:@"Customer_No"];
//    } else if (column == 3) {
//        value = [data stringForKey:@"Customer_Name"];
//    } else if (column == 4) {
//        value = [data currencyStringForKey:@"Transaction_Amt"];
//    }else if (column == 5) {
//        value = [data dateStringForKey:@"Creation_Date" withDateFormat:NSDateFormatterMediumStyle];
//    }
//    else if (column == 6) {
//        value = [data currencyStringForKey:@"End_Time"];
//    }
//    return value;
//    
//}
//- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
//    
//    //modified widths. OLA!
//    
//    if (columnIndex == 0) {
//        return 17;
//    }
//    else if (columnIndex == 1) {
//        return 15;
//    }
//    else if (columnIndex == 3) {
//        return 33;
//    }
//
//        return 20;
//}
//-(IBAction)viewReportAction
//{
//    
//    //OLA!! Added ERP_Ref_No to list
//    NSString *sQry = @" SELECT A.Orig_Sys_Document_Ref, A.Creation_Date,A.Doc_Type,A.Transaction_Amt, B.Customer_No, B.Customer_Name,C.Cash_Cust AS Status,C.Phone, A.Visit_ID, A.End_Time, A.ERP_Ref_No FROM TBL_Sales_History AS A INNER JOIN  TBL_Customer_Ship_Address AS B ON A.Ship_To_Customer_ID = B.Customer_ID AND A.Ship_To_Site_ID = B.Site_Use_ID INNER JOIN  TBL_Customer AS C ON C.Customer_ID = A.Inv_To_Customer_ID AND C.Site_Use_ID = A.Inv_To_Site_ID WHERE A.Creation_Date >='{0}'  AND A.Creation_Date<='{1}'";
//    if(customerDict.count!=0)
//    {
//        sQry=[sQry stringByAppendingString:@" AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}'"];
//    }
//    if(![DocType isEqualToString:@"All"])
//    {
//        sQry=[sQry stringByAppendingString:@" AND A.Doc_Type='{5}'"];
//    }
//    if(![custType isEqualToString:@"All"])
//    {
//        sQry=[sQry stringByAppendingString:@" AND C.Cash_Cust='{6}'"];
//    }
//    sQry=[sQry stringByAppendingString:@" ORDER BY C.Cash_Cust Desc , A.End_Time ASC,A.ERP_Ref_No ASC"];
//    
//    sQry = [sQry stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
//    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
//    if(customerDict.count!=0)
//    {
//        sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
//        sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
//    }
//    if(![DocType isEqualToString:@"All"])
//    {
//        if([DocType isEqualToString:@"Invoice"])
//        {
//            sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:@"I"];
//        }
//        else
//        {
//            sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:@"R"];
//        }
//    }
//    else
//    {
//        sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:DocType];
//    }
//    
//    if(![custType isEqualToString:@"All"])
//    {
//        if([custType isEqualToString:@"Cash"])
//        {
//            sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:@"Y"];
//        }
//        else
//        {
//            sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:@"N"];
//        }
//    }
//    else
//    {
//        sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:custType];
//    }
//    dataArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry];
//    [gridView reloadData];
//
//    
//}
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
//{
//    currencyTypePopOver=nil;
//    // datePickerPopOver=nil;
//    customPopOver=nil;
//
//    currencyTypeViewController=nil;
//    customVC=nil;
//}
//
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    
//    return YES;
//}
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        self.view = nil;
//    // Dispose of any resources that can be recreated.
//}
//
//@end




//
//  SalesSummaryViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SalesSummaryViewController.h"
#import "MedRepDefaults.h"
@interface SalesSummaryViewController ()

@end

@implementation SalesSummaryViewController
@synthesize datePickerViewControllerDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:[kSalesSummaryTitle localizedCapitalizedString]];

    
    gridView = [[GridView alloc] initWithFrame:CGRectMake(283, 8, 733, 689)];
    [gridView setDataSource:self];
    [gridView setDelegate:self];
    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [gridView.layer setBorderWidth:1.0f];
    [self.view addSubview:gridView];
    
    NSLog(@"sales summary called");
    
    filterTableView.backgroundView=nil;[filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [filterTableView.layer setBorderWidth:1.0f];
    
    filterTableView.backgroundColor=[UIColor whiteColor];

    
    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=NO;
    
     self.datePickerViewControllerDate=datePickerViewController;
    
    
    
    
    //test
    
    
    
    
    datePickerViewController.forReports=YES;
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    navigationController.navigationBar.translucent = NO;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    // Do any additional setup after loading the view.
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    toDate=[formatter stringFromDate:[NSDate date]];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-1]; // note that I'm setting it to -1
    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
    DocType = @"All";
    custType = @"All";
    formatter=nil;
    usLocale=nil;
    [self viewReportAction];
}
-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"Sales Documents");
    [self viewReportAction];
}
- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
    
}
- (void)dateChanged:(SWDatePickerViewController *)sender
{
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        NSString * sampleDate=fromDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            fromDate=sampleDate;
        }
        else
        {
            fromDate=[formatter stringFromDate:sender.selectedDate];
            
        }
        
        
    }
    else
    {
        NSString* sampleToDate=toDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            toDate=sampleToDate;
        }
        else
        {
            toDate=[formatter stringFromDate:sender.selectedDate];
            
            
        }
        
        
    }
    [filterTableView reloadData];
    
    if ([formatter stringFromDate:sender.selectedDate]==nil) {
        
    }
    else
    {
        [self viewReportAction];
        
    }
    formatter=nil;
    usLocale=nil;
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterTableView reloadData];
    [self viewReportAction];
    
}

#pragma TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
    return 5;
    
}
- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
    //    [companyNameLabel setShadowColor:[UIColor whiteColor]];
    //    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:companyNameLabel];
    return headerView;
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return NSLocalizedString(@"Filter", nil);
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool
    {
        NSString *CellIdentifier = @"CustomerDetailViewCell";
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
        }
        
        if(indexPath.row==0)
        {
            if(customerDict.count!=0)
            {
                cell.textLabel.text = [customerDict stringForKey:@"Customer_Name"];
            }
            else
            {
                cell.textLabel.text = NSLocalizedString(@"Select Customer", nil);
            }
        }
        else if(indexPath.row==1)
        {
            [cell.textLabel setText:NSLocalizedString(@"From Date", nil)];
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:fromDate]];
        }
        else if(indexPath.row==2)
        {
            [cell.textLabel setText:NSLocalizedString(@"To Date", nil)];
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:toDate]];
        }
        else if(indexPath.row==3)
        {
            [cell.textLabel setText:NSLocalizedString(@"Customer Type", nil)];
            [cell.detailTextLabel setText:custType];
        }
        else if(indexPath.row==4)
        {
            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
            [cell.detailTextLabel setText:DocType];
        }
        else
        {
            [cell.textLabel setText:@"View Report"];
            
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font=UITableViewCellFont;
        cell.detailTextLabel.font=UITableViewCellDescFont;
        
        return cell;
    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0)
    {
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else if(indexPath.row==1)
    {
        isFromDate=YES;
        
        NSLog(@"From Date is %@", fromDate);
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:fromDate];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;
        
        
        
        
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==2)
    {
        
        //this is to date minimum date should be today
        
        
        
        NSLog(@"From Date is %@", fromDate);
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:toDate];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;
        
        datePickerViewControllerDate.isToDate=YES;
        
        isFromDate=NO;
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==3)
    {
        customVC=nil;
        customVC = [[CustomerPopOverViewController alloc] init] ;
        [customVC setTarget:self];
        [customVC setAction:@selector(typeChanged:)];
        [customVC setPreferredContentSize:CGSizeMake(200,250)];
        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
        customPopOver.delegate=self;
        isCustType=YES;
        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Cash",@"Credit" , nil]];
        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==4)
    {
        customVC=nil;
        customVC = [[CustomerPopOverViewController alloc] init] ;
        [customVC setTarget:self];
        [customVC setAction:@selector(typeChanged:)];
        [customVC setPreferredContentSize:CGSizeMake(200,250)];
        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
        customPopOver.delegate=self;
        isCustType=NO;
        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil]];
        [customPopOver presentPopoverFromRect:CGRectMake(280,235, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return dataArray.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 7;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0) {
        title = NSLocalizedString(@"Document No", nil);//changed Invoice No to Document No. OLA!!
    } else if (column == 1) {
        title = NSLocalizedString(@"Doc Type", nil);//changed Category to Doc Type. OLA!!
    } else if (column == 2) {
        title = NSLocalizedString(@"Date", nil);//changed Code to Date. Corresponding change in displayed values. OLA!!
    } else if(column == 3) {
        title = NSLocalizedString(@"Name", nil);
    } else if(column == 4){
        title = NSLocalizedString(@"Amount", nil);
    }else if(column == 5) {
        title = NSLocalizedString(@"Date", nil);
    } else if(column == 6){
        title = NSLocalizedString(@"Timestamp", nil);
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSDictionary *data = [dataArray objectAtIndex:row];
    
    NSLog(@"creation date is %@",[NSString stringWithFormat:@"%@", [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd/MM/yyyy" scrString:[data stringForKey:@"Creation_Date"]]]);
    
    NSString * creationDate=[NSString stringWithFormat:@"%@", [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd/MM/yyyy" scrString:[data stringForKey:@"Creation_Date"]]];
    
    
    
    NSString *value = @"";
    if (column == 0) {
        value = [NSString stringWithFormat:@"     %@", [data stringForKey:@"ERP_Ref_No"]];
    } else if (column == 1) {
        if([[data stringForKey:@"Doc_Type"] isEqualToString:@"I"])
        {
            value = @"Invoice";
        }
        else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"R"])
        {
            value = @"Credit Note";
        }
    } else if (column == 2) {
        value = creationDate;//[data stringForKey:@"Customer_No"];
    } else if (column == 3) {
        value = [data stringForKey:@"Customer_Name"];
    } else if (column == 4) {
        value = [data currencyStringForKey:@"Transaction_Amt"];
    }else if (column == 5) {
        value = creationDate ;
        // [data dateStringForKey:@"Creation_Date" withDateFormat:NSDateFormatterMediumStyle];
    }
    else if (column == 6) {
        value = [data currencyStringForKey:@"End_Time"];
    }
    return value;
    
}
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    
    //modified widths. OLA!
    
    if (columnIndex == 0) {
        return 17;
    }
    else if (columnIndex == 1) {
        return 15;
    }
    else if (columnIndex == 3) {
        return 33;
    }
    
    return 20;
}
-(IBAction)viewReportAction
{
    //try distinct
    
    //OLA!! Added ERP_Ref_No to list
    NSString *sQry = @" SELECT  A.Orig_Sys_Document_Ref, A.Creation_Date,A.Doc_Type,A.Transaction_Amt, B.Customer_No, B.Customer_Name,C.Cash_Cust AS Status,C.Phone, A.Visit_ID, A.End_Time, A.ERP_Ref_No FROM TBL_Sales_History AS A INNER JOIN  TBL_Customer_Ship_Address AS B ON A.Ship_To_Customer_ID = B.Customer_ID AND A.Ship_To_Site_ID = B.Site_Use_ID INNER JOIN  TBL_Customer AS C ON C.Customer_ID = A.Inv_To_Customer_ID AND C.Site_Use_ID = A.Inv_To_Site_ID WHERE A.Creation_Date >='{0}'  AND A.Creation_Date<='{1}'";
    if(customerDict.count!=0)
    {
        sQry=[sQry stringByAppendingString:@" AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}'"];
    }
    if(![DocType isEqualToString:@"All"])
    {
        sQry=[sQry stringByAppendingString:@" AND A.Doc_Type='{5}'"];
    }
    if(![custType isEqualToString:@"All"])
    {
        sQry=[sQry stringByAppendingString:@" AND C.Cash_Cust='{6}'"];
    }
    sQry=[sQry stringByAppendingString:@" ORDER BY C.Cash_Cust Desc , A.End_Time ASC,A.ERP_Ref_No ASC"];
    
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    if(customerDict.count!=0)
    {
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
    }
    if(![DocType isEqualToString:@"All"])
    {
        if([DocType isEqualToString:@"Invoice"])
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:@"I"];
        }
        else
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:@"R"];
        }
    }
    else
    {
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:DocType];
    }
    
    if(![custType isEqualToString:@"All"])
    {
        if([custType isEqualToString:@"Cash"])
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:@"Y"];
        }
        else
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:@"N"];
        }
    }
    else
    {
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:custType];
    }
    
    NSLog(@"sales summary query is %@", sQry);
    
    
    dataArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry];
    [gridView reloadData];
    
    
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    // datePickerPopOver=nil;
    customPopOver=nil;
    
    currencyTypeViewController=nil;
    customVC=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end

