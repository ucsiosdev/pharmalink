//
//  MedRepDoctorFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/3/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepDoctorFilterViewController.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
@interface MedRepDoctorFilterViewController ()

@end

@implementation MedRepDoctorFilterViewController
@synthesize filterArray,classButton,tradeChannelButton,specilizationButton,previouslyFilteredContent;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:self.filterTitle];
    
    specificationSegment.selectedSegmentIndex=UISegmentedControlNoSegment;

    
    
    filterDict=[[NSMutableDictionary alloc]init];
    
    // Do any additional setup after loading the view from its nib.
    [filterDict setValue:@"Active" forKey:@"Is_Active"];
   // NSLog(@"previously filtered content is %@",[previouslyFilteredContent description] );
    
    
    if ([previouslyFilteredContent valueForKey:@"Class"]!=nil) {
        classTextfield.text= [previouslyFilteredContent valueForKey:@"Class"];
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Class"] forKey:@"Class"];
        
        
    }
    
    if ([previouslyFilteredContent valueForKey:@"TradeChannel"]!=nil) {
        tradeChannelTextfield.text= [previouslyFilteredContent valueForKey:@"TradeChannel"];
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"TradeChannel"] forKey:@"TradeChannel"];
        
        
    }
    
    if ([previouslyFilteredContent valueForKey:@"Specialization"]!=nil) {
        specializationTextfield.text= [previouslyFilteredContent valueForKey:@"Specialization"];
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Specialization"] forKey:@"Specialization"];
        
        
    }
    
    if ([previouslyFilteredContent valueForKey:@"Specification"]!=nil) {
        
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Specification"] forKey:@"Specification"];
        
        if ([[previouslyFilteredContent valueForKey:@"Specification"] isEqualToString:@"Rx"]) {
            
            
            
            specificationSegment.selectedSegmentIndex=1;
        }
        
        else if ([[previouslyFilteredContent valueForKey:@"Specification"] isEqualToString:@"OTC"]) {
            
            specificationSegment.selectedSegmentIndex=0;
        }
        
        else
        {
            specificationSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
            
        }
    }
    
    
    
    
    if ([previouslyFilteredContent valueForKey:@"Is_Active"]!=nil) {
        
        if ([[previouslyFilteredContent valueForKey:@"Is_Active"]isEqualToString:@"Active"]) {
            [self.statusSwitch setOn:YES animated:NO];
            
        }
        
        else
        {
            [self.statusSwitch setOn:NO animated:NO];
        }
        
    }

    
    if ([previouslyFilteredContent valueForKey:@"Account_Name"]!=nil) {
        
        accountNameTextField.text= [previouslyFilteredContent valueForKey:@"Account_Name"];
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Account_Name"] forKey:@"Account_Name"];
        

    }
    
    
    if ([previouslyFilteredContent valueForKey:@"Location"]!=nil) {
        
        locationTextField.text= [previouslyFilteredContent valueForKey:@"Location"];
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Location"] forKey:@"Location"];
        
    }
   
    
    
    
    if ([previouslyFilteredContent valueForKey:@"Territory"]!=nil) {
        
        territoryTextField.text= [previouslyFilteredContent valueForKey:@"Territory"];
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Territory"] forKey:@"Territory"];
        
    }
   
    
    if ([previouslyFilteredContent valueForKey:@"Clinic"]!=nil) {
        
        clinicTextField.text= [previouslyFilteredContent valueForKey:@"Clinic"];
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Clinic"] forKey:@"Clinic"];
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)saveFilter
{

   // NSLog(@"saved filters are %@", [filterDict description]);
    
    //start filter
    
   // NSLog(@"content array is %@", [self.contentArray description]);
    
    
    
    NSMutableArray* filteredDoctorsArray=[[NSMutableArray alloc]init];
    

    
    NSString* filter = @"%K == [c] %@";
   
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    
    
    
    if([[filterDict valueForKey:@"Class"] length]>0) {
        

        
//          NSPredicate*  searchPredicate = [NSPredicate predicateWithFormat:@"SELF.Class contains[cd] %@",[filterDict valueForKey:@"Class"]];
//        
//        [NSPredicate predicateWithFormat:<#(nonnull NSString *), ...#>]
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Classification_2 ==[cd] %@",[filterDict valueForKey:@"Class"]]];
    }
   
    
    if([[filterDict valueForKey:@"Specialization"] length]>0) {
        
        
       
        
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Classification_1 ==[cd] %@",[filterDict valueForKey:@"Specialization"]]];
    }
    if([[filterDict valueForKey:@"TradeChannel"] length]>0) {

        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Custom_Attribute_1 ==[cd] %@",[filterDict valueForKey:@"TradeChannel"]]];
        
        
        
    }
    
    if([[filterDict valueForKey:@"Specification"] length]>0) {
        
        
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Classification_3 ==[cd] %@",[filterDict valueForKey:@"Specification"]]];
        
        
       
    }
    if([[filterDict valueForKey:@"Is_Active"] length]>0) {
        
//        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Is_Active", [NSString stringWithFormat:@"%@",[filterDict valueForKey:@"Is_Active"]]]];
        
          [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.isActive ==[cd] %@",[filterDict valueForKey:@"Is_Active"]]];
    }
    
    
    if ([[filterDict valueForKey:@"Account_Name"]length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.locations.Custom_Attribute_2 ==[cd] %@",[filterDict valueForKey:@"Account_Name"]]];
        
    }
   
    
    if ([[filterDict valueForKey:@"Territory"]length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.locations.City ==[cd] %@",[filterDict valueForKey:@"Territory"]]];
        
    }
   
    
    if ([[filterDict valueForKey:@"Location"]length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.locations.Address_2 ==[cd] %@",[filterDict valueForKey:@"Location"]]];
        
    }
  
    
    if ([[filterDict valueForKey:@"Clinic"]length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.locations.Location_Name ==[cd] %@",[filterDict valueForKey:@"Clinic"]]];
        
    }
    
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];

    
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredDoctorsArray=[[self.contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];

    //NSLog(@"filtered doctors in filterVC %@", [filteredDoctorsArray description]);
    
    
    
    if (predicateArray.count==0) {
        
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"Missing Filter Criteria" message:@"Please select filter criteria and try again" delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
//        [noFilterAlert show];
        
                
        
        
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:@"Please change your filter criteria and try again" withController:self];
    }
    
  else  if (filteredDoctorsArray.count>0) {
        
        
        if ([self.filteredDoctorDelegate respondsToSelector:@selector(filteredDoctorDelegate:)]) {
            
            [self.filteredDoctorDelegate filteredDoctorDelegate:filteredDoctorsArray];
            [self.filteredDoctorDelegate filteredParameters:filterDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];

            
        }
        
    }
    
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
//        [noFilterAlert show];
        
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
        
    }
    
//    
//    NSString* filterTest = @"%K == [c] %@";
//    NSPredicate* predicateTest = [NSPredicate predicateWithFormat:filterTest, @"Is_Active", [NSString stringWithFormat:@"%@",[filterDict valueForKey:@"Is_Active"]]];
//    
//    
//    NSArray * testArray=[[NSMutableArray alloc]init];
//    
//    testArray=[self.contentArray filteredArrayUsingPredicate:predicateTest];
//    
//    
//    NSLog(@"test predicate is %@",[testArray description]);
//    
    
    
    
}


-(void)clearFilter

{
    
    /*
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        UIAlertController * closeAlert=[UIAlertController alertControllerWithTitle:@"Exit?" message:@"Would you like to close the filter" preferredStyle:UIAlertControllerStyleAlert];
        
        [closeAlert addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            if ([self.filteredDoctorDelegate respondsToSelector:@selector(CustomPopOverCancelDelegate)]) {
                
                
                [self.filteredDoctorDelegate CustomPopOverCancelDelegate];
                
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }]];
       
        
        [closeAlert addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }]];

        
        [self presentViewController:closeAlert animated:YES completion:nil];
        
    }
    
    
    else
    {
        UIAlertView * closeAlertView=[[UIAlertView alloc]initWithTitle:@"Exit?" message:@"Would you like to close the filter" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
        closeAlertView.tag=100;
        closeAlertView.delegate=self;
        [closeAlertView show];
        
    }
    
    */
    
    specificationSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    specilizationLbl.text=@"";
    classLbl.text=@"";
    tradeChannelLbl.text=@"";
    locationTextField.text=@"";
    clinicTextField.text=@"";
    territoryTextField.text=@"";
    accountNameTextField.text=@"";

    
    classTextfield.text=@"";
    tradeChannelTextfield.text=@"";
    specializationTextfield.text=@"";
    [self.statusSwitch setOn:YES animated:YES];
    
    filterDict=[[NSMutableDictionary alloc]init];
    [filterDict setValue:@"Active" forKey:@"Is_Active"];

    
    /*
    if ([self.filteredDoctorDelegate respondsToSelector:@selector(CustomPopOverCancelDelegate)]) {
        
        
        [self.filteredDoctorDelegate CustomPopOverCancelDelegate];
        
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];
*/
    
}

-(void)closeFilter
{
    
    
    
  if ([self.filteredDoctorDelegate respondsToSelector:@selector(CustomPopOverCancelDelegate)]) {
      
      
      //[self.filteredDoctorDelegate CustomPopOverCancelDelegate];
      [self.filteredDoctorDelegate filteredParameters:previouslyFilteredContent];

  }
    [self.filterPopOverController dismissPopoverAnimated:YES];

}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingDoctorFilter];
    }
    
    doctorFilterScrollView.contentSize=CGSizeMake(366, 780);
  
    
//NSLog(@"content in filter %@", [self.contentArray description]);
   
    Doctors* tempDoc=[self.contentArray objectAtIndex:0];
    
    
        
    
//    NSMutableDictionary * previousFilteredDict=[previouslyFilteredContent objectAtIndex:0];
//    
//    classTextfield.text= [previousFilteredDict valueForKey:@"Class"];
//    tradeChannelTextfield.text= [previousFilteredDict valueForKey:@"Trade_Channel"];
//    specializationTextfield.text= [previousFilteredDict valueForKey:@"Specialization"];
    
    
    
    
    UIFont *font = MedRepSegmentControlFont;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    
    
    [specificationSegment setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
    
    
    [statusSegment setTitleTextAttributes:attributes
                                        forState:UIControlStateNormal];

    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear",nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    
    
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    
    
   UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    
   
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];

    
    
    //filtering out contents
    
    
//    for (UIView* currentLbl in self.view.subviews) {
//        
//        if ([currentLbl isKindOfClass:[UILabel class]]) {
//            
//            if (currentLbl.tag==1001||currentLbl.tag==1002||currentLbl.tag==1003) {
//                CALayer *bottomBorder = [CALayer layer];
//                bottomBorder.frame = CGRectMake(0.0f, currentLbl.frame.size.height - 1, currentLbl.frame.size.width, 1.0f);
//                bottomBorder.backgroundColor = [UIColor colorWithRed:(139.0/255.0) green:(145.0/255.0) blue:(160.0/0/255.0) alpha:1].CGColor;
//                
//                [currentLbl.layer addSublayer:bottomBorder];
//            }
//            
//            
//        }
//        
//    }
    
   

    /*
    specificationSegment.layer.borderWidth=1.0f;
    specificationSegment.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    specificationSegment.layer.masksToBounds = NO;
    specificationSegment.layer.shadowColor = [UIColor blackColor].CGColor;
    specificationSegment.layer.shadowOpacity = 0.1f;
    specificationSegment.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    specificationSegment.layer.shadowRadius = 1.0f;
    specificationSegment.layer.shouldRasterize = YES;*/
    



    [self.filterPopOverController setPopoverContentSize:CGSizeMake(366, 685) animated:YES];
    statusSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    

    Doctors * doctor=[self.contentArray objectAtIndex:0];
    
    
  //  NSLog(@"is active %@", doctor.isActive);
}


-(void)viewDidAppear:(BOOL)animated{
    if ([SWDefaults isRTL]) {
        
    }
    else{
        
        classtitleLbl.textAlignment=NSTextAlignmentLeft;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView DataSource Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (filterArray.count>0) {
        
        return filterArray.count;
    }
    
    else
    {
        return 0;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.font=MedRepFont;
    
    
    cell.textLabel.text=[NSString stringWithFormat:@"%@",[filterArray objectAtIndex:indexPath.row]];
    return cell;
    
}


#pragma mark UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    
    
  //  NSLog(@"selected filter %@", [filterArray objectAtIndex:indexPath.row]);
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    
    if ([[filterArray objectAtIndex:indexPath.row] isEqualToString:@"Class"]) {
        filterDescArray=[[MedRepQueries fetchFilteredContent:@"Classification_2"] valueForKey:@"Classification_2"];
    }
    else if ([[filterArray objectAtIndex:indexPath.row] isEqualToString:@"Trade Channel"])
    {
        filterDescArray=[MedRepQueries fetchFilteredContent:@"Custom_Attribute_1"];
    }
    else if ([[filterArray objectAtIndex:indexPath.row] isEqualToString:@"Specialization"])
    {
        filterDescArray=[MedRepQueries fetchFilteredContent:@"Classification_2"];
    }
    
   // NSLog(@"filter desc array is %@", [filterDescArray description]);
    filterDescVC.filterDescArray=filterDescArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
}



- (IBAction)classButtonTapped:(id)sender {
    
    selectedDataString=@"Class";
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    filterDescVC.filterNavController=self.filterNavController;

    filterDescVC.filterPopOverController=self.filterPopOverController;
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredContent:@"Classification_2"] valueForKey:@"Classification_2"];
    
    
  //  NSLog(@"filter desc array is %@", [filterDescArray description]);
    
    NSMutableArray* refinedClassArray=[[NSMutableArray alloc]init];
    if (filterDescArray.count>0) {
        
    for (NSInteger i=0; i<filterDescArray.count; i++) {
        
        NSString* currentString=[NSString stringWithFormat:@"%@", [filterDescArray objectAtIndex:i]];
        
        NSString* refinedStr=[self getValidStringValue:currentString];
        
        if (refinedStr.length>0) {
            [refinedClassArray addObject:refinedStr];

        }
        
        
    }
    
    [refinedClassArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    
        
    
        filterDescVC.filterDescArray=refinedClassArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];

    }
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
//        [unavailableAlert show];
        
        
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
        
    }

   
}


-(NSString *)getValidStringValue:(id)inputstr
{
    if ([inputstr isEqual:(id)[NSNull null]] || inputstr==nil)
    {
        return [[NSString alloc]init];
    }
    if([inputstr isKindOfClass:[NSString class]] && [inputstr isEqualToString:@"<null>"])
    {
        return [[NSString alloc]init];
    }
    
    return [NSString stringWithFormat:@"%@",inputstr];
}
- (IBAction)tradeChannelButtonTapped:(id)sender {
    
    selectedDataString=@"TradeChannel";
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Doctor_Trade_Channel";
    
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;

    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredContent:@"Custom_Attribute_1"] valueForKey:@"Custom_Attribute_1"];
    
    
    NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
    
    if (filterDescArray.count>0) {
        
        for (NSInteger i=0; i<filterDescArray.count; i++) {
            NSString* currentValue=[NSString stringWithFormat:@"%@",[filterDescArray objectAtIndex:i]];
            NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
            
            if (refinedValue.length>0) {
                [refinedArray addObject:refinedValue];
                
            }

            
        }
        
        [refinedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

        
        filterDescVC.filterDescArray=refinedArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:NSLocalizedString(@"Please try later", nil) withController:self];

    }

    
}

- (IBAction)specilizationButtonTapped:(id)sender {
    
    selectedDataString=@"Specialization";
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.descTitle=selectedDataString;
    filterDescVC.selectedFilterDelegate=self;
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredContent:@"Classification_1"] valueForKey:@"Classification_1"];
    NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
    
    
    if (filterDescArray.count>0) {
        
        for (NSInteger i=0; i<filterDescArray.count; i++) {
            NSString* currentValue=[filterDescArray objectAtIndex:i];
            
            
            NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
            
            if (refinedValue.length>0) {
                [refinedArray addObject:refinedValue];
                
            }

        }
        
        [refinedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

        filterDescVC.filterDescArray=refinedArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:NSLocalizedString(@"Please try later", nil) withController:self];

    }


}

-(void)selectedFilterValue:(NSString*)selectedString
{

 //   NSLog(@"select value is %@", selectedString);
    
    if ([selectedDataString isEqualToString:@"Class"]) {
        
       // classLbl.text=[NSString stringWithFormat:@"%@",selectedString];

        
        //[classButton setTitle:nil forState:UIControlStateNormal];

       // [classButton setTitle:selectedString forState:UIControlStateNormal];
        classTextfield.text=selectedString;
//        NSAttributedString* AttributedTitle=[[NSAttributedString alloc]initWithString:@"HI"];
//        
//        [classButton setAttributedTitle:selectedString forState:UIControlStateNormal];
//        
        
        [filterDict setValue:selectedString forKey:selectedDataString];
        
        
    }
    else if ([selectedDataString isEqualToString:@"TradeChannel"]) {
        
        //tradeChannelLbl.text=[NSString stringWithFormat:@"%@",selectedString];
        
        //[tradeChannelButton setTitle:[NSString stringWithFormat:@"%@",selectedString] forState:UIControlStateNormal];
        tradeChannelTextfield.text=selectedString;

        
        [filterDict setValue:selectedString forKey:selectedDataString];

    }
    
    else if ([selectedDataString isEqualToString:@"Specialization"]) {
        
       // specilizationLbl.text=[NSS tring stringWithFormat:@"%@",selectedString];
        
     //   [specilizationButton setTitle:[NSString stringWithFormat:@"%@",selectedString] forState:UIControlStateNormal];
        specializationTextfield.text=selectedString;

        
        [filterDict setValue:selectedString forKey:selectedDataString];

        
        
    }
    
    else if ([selectedDataString isEqualToString:@"Location"])
        
    {
        locationTextField.text=selectedString;
        
        [filterDict setValue:selectedString forKey:selectedDataString];
    }
    
    else if ([selectedDataString isEqualToString:@"Account_Name"])
        
    {
        accountNameTextField.text=selectedString;
        
        [filterDict setValue:selectedString forKey:@"Account_Name"];
        
    }
    
    else if ([selectedDataString isEqualToString:@"Clinic"])
        
    {
        clinicTextField.text=selectedString;
        
        [filterDict setValue:selectedString forKey:@"Clinic"];
        
    }
    
    else if ([selectedDataString isEqualToString:@"Territory"])
        
    {
        territoryTextField.text=selectedString;
        
        [filterDict setValue:selectedString forKey:@"Territory"];
    }
}

- (IBAction)segmentSwitch:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        NSLog(@"This OTC");
        
        [filterDict setValue:@"OTC" forKey:@"Specification"];
        
    }
    else{
        [filterDict setValue:@"Rx" forKey:@"Specification"];
        
        
        NSLog(@"This is Rx");
    }
    
    
}

- (IBAction)statusSegmentSwitch:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        NSLog(@"This Active");
        
        [filterDict setValue:@"Active" forKey:@"Is_Active"];
        
    }
    else{
        [filterDict setValue:@"InActive" forKey:@"Is_Active"];
        
        
        NSLog(@"This is InActive");
    }
    
    
}


#pragma mark PopOver Controller delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController == self.filterPopOverController) {
        
        return NO;
    }
    else
    {
        return YES;
    }
}

- (IBAction)searchButtonTapped:(id)sender {
    
    [self saveFilter];
}

#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
  //  NSLog(@"button index is %d", buttonIndex);
    
    
    if (alertView.tag==100) {
        
        if (buttonIndex==1) {
            
            if ([self.filteredDoctorDelegate respondsToSelector:@selector(CustomPopOverCancelDelegate)]) {
                
                
                [self.filteredDoctorDelegate CustomPopOverCancelDelegate];
                
            }
            [self dismissViewControllerAnimated:YES completion:nil];

        }
        
    }
}

- (IBAction)statusSwitchTapped:(id)sender {
    
    BOOL state = [sender isOn];
    
  //  NSLog(@"Switch is %hhd", state);
    
    if (state == YES) {
        
       // NSLog(@"This Active");
        
        [filterDict setValue:@"Active" forKey:@"Is_Active"];
        
    }
    else{
        [filterDict setValue:@"InActive" forKey:@"Is_Active"];
        
        
       // NSLog(@"This is InActive");
    }

    

}

- (IBAction)resetButtonTapped:(id)sender {
    
    
     if ([self.filteredDoctorDelegate respondsToSelector:@selector(CustomPopOverCancelDelegate)]) {
     
     
     [self.filteredDoctorDelegate CustomPopOverCancelDelegate];
         
         [self.filteredDoctorDelegate filteredParameters:nil];
     
     }
     [self.filterPopOverController dismissPopoverAnimated:YES];
    
}



- (IBAction)accountNameButtonTapped:(id)sender {
    
    
    selectedDataString=@"Account_Name";
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.descTitle=selectedDataString;
    filterDescVC.selectedFilterDelegate=self;
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];

    NSMutableArray* tempArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"Custom_Attribute_2"];
    
    
//    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(id str, NSDictionary *unused) {
//        return ![str isEqual:[NSNull null]];
//    }];
//    NSArray *filtered = [tempArray filteredArrayUsingPredicate:pred];

    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@)", @"N/A"];
    ;
    NSArray* refinedArray=[tempArray filteredArrayUsingPredicate:predicate];
    
    NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
    
    
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    
    [[arrayWithoutDuplicates mutableCopy] sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    if (arrayWithoutDuplicates.count>0) {
        
        selectedDataString=@"Account_Name";
        
        
        [self textFieldTapped:accountNameTextField withTitle:@"Account Name" withContentArray:[arrayWithoutDuplicates mutableCopy]];
        
        
//        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
//        filterDescVC.selectedFilterDelegate=self;
//        filterDescVC.descTitle=@"Account Name";
//        filterDescVC.filterNavController=self.filterNavController;
//        
//        filterDescVC.filterPopOverController=self.filterPopOverController;
//        
//        filterDescVC.filterDescArray=[arrayWithoutDuplicates mutableCopy];
//        
//        NSLog(@"content array is %@", [arrayWithoutDuplicates description]);
//
//        
//        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    
    else
    {
        
    }

    
    
    
    
}

- (IBAction)locationButtonTapped:(id)sender {
    
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.descTitle=selectedDataString;
    filterDescVC.selectedFilterDelegate=self;
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* tempArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"Address_2"];
    
    

    
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(id str, NSDictionary *unused) {
        return ![str isEqual:[NSNull null]]
        ;
    }];
    NSArray *filtered = [tempArray filteredArrayUsingPredicate:pred];
    
    
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@) AND SELF !=''", @"N/A"];
    
    
    
    NSArray* refinedArray=[filtered filteredArrayUsingPredicate:predicate];
   
    
    NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
    
    
    
    

   // NSLog(@"refined array with  duplicates count is %d", [refinedArray count]);

    
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
   // NSLog(@"refined array with out duplicates for locations is %@", [refinedArray description]);
    

    NSMutableArray* sortedArray=    [arrayWithoutDuplicates mutableCopy];

    [sortedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    
    if (arrayWithoutDuplicates.count>0) {
        
        selectedDataString=@"Location";
        
        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
        filterDescVC.selectedFilterDelegate=self;
        filterDescVC.descTitle=selectedDataString;
        filterDescVC.filterNavController=self.filterNavController;
        
        filterDescVC.filterPopOverController=self.filterPopOverController;

        filterDescVC.filterDescArray=sortedArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    
    else
    {
        
    }
}

- (IBAction)clinicButtonTapped:(id)sender {
    
    
    selectedDataString=@"Clinic";
    
      NSMutableArray* clinicsArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"Location_Name"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@)", @"N/A"];
    ;
    
    
    NSArray* refinedArray=[clinicsArray filteredArrayUsingPredicate:predicate];
    
    
    NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];

    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    
    
    
    
    
    

    
    
    
  //  NSLog(@"refined array with out duplicates count is %d", [arrayWithoutDuplicates count]);
    

    
    NSMutableArray* sortedArray=    [arrayWithoutDuplicates mutableCopy];
    [sortedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    

    
    if (arrayWithoutDuplicates.count>0) {
        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
        filterDescVC.selectedFilterDelegate=self;
        filterDescVC.descTitle=selectedDataString;
        filterDescVC.filterNavController=self.filterNavController;
        
        filterDescVC.filterPopOverController=self.filterPopOverController;
        
        filterDescVC.filterDescArray=sortedArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];

    }
    
    else
    {
        
    }
    


}

- (IBAction)territoryButtonTapped:(id)sender {
    
    selectedDataString=@"Territory";
    
    NSMutableArray* clinicsArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"City"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@)", @"N/A"];
    
    
    
    NSArray* refinedArray=[clinicsArray filteredArrayUsingPredicate:predicate];
    
    NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
  //  NSLog(@"refined array with out duplicates count is %@", [arrayWithoutDuplicates description]);
    
    
    NSMutableArray* sortedArray=    [arrayWithoutDuplicates mutableCopy];
    [sortedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    
    if (arrayWithoutDuplicates.count>0) {
        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
        filterDescVC.selectedFilterDelegate=self;
        filterDescVC.descTitle=selectedDataString;
        filterDescVC.filterNavController=self.filterNavController;
        
        filterDescVC.filterPopOverController=self.filterPopOverController;
        
        filterDescVC.filterDescArray=sortedArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    else
    {
        
    }

}

#pragma mark TextField Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (textField==accountNameTextField)     {
        selectedDataString=@"Account_Name";
        NSMutableArray* tempArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"Custom_Attribute_2"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@)", @"N/A"];
        ;
        NSArray* refinedArray=[tempArray filteredArrayUsingPredicate:predicate];
        NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
        NSMutableArray *arrayWithoutDuplicates = [[orderedSet array]mutableCopy];
        [arrayWithoutDuplicates sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        if (arrayWithoutDuplicates.count>0) {
            selectedDataString=@"Account_Name";
            [self textFieldTapped:accountNameTextField withTitle:@"Account Name" withContentArray:[arrayWithoutDuplicates mutableCopy]];
        }
        else
        {
            
        }
    }
    else if (textField==locationTextField)
    {
    NSMutableArray* tempArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"Address_2"];
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(id str, NSDictionary *unused) {
            return ![str isEqual:[NSNull null]]
            ;
        }];
        NSArray *filtered = [tempArray filteredArrayUsingPredicate:pred];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@) AND SELF !=''", @"N/A"];
        NSArray* refinedArray=[filtered filteredArrayUsingPredicate:predicate];
        NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        
        NSMutableArray* sortedArray=    [arrayWithoutDuplicates mutableCopy];
        
        [sortedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        if (arrayWithoutDuplicates.count>0) {
            selectedDataString=@"Location";
            [self textFieldTapped:locationTextField withTitle:selectedDataString withContentArray:sortedArray];
        }
        else
        {
            
        }
    }
    else if (textField==clinicTextField)
    {
        selectedDataString=@"Clinic";
        NSMutableArray* clinicsArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"Location_Name"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@)", @"N/A"];
        ;
        NSArray* refinedArray=[clinicsArray filteredArrayUsingPredicate:predicate];
        NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        
        NSMutableArray* sortedArray=    [arrayWithoutDuplicates mutableCopy];
        [sortedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        if (arrayWithoutDuplicates.count>0) {
            [self textFieldTapped:clinicTextField withTitle:selectedDataString withContentArray:sortedArray];
        }
        else
        {
            
        }
    }
    else if (textField==classTextfield)
    {
        
        selectedDataString=@"Class";
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredContent:@"Classification_2"] valueForKey:@"Classification_2"];
        NSMutableArray* refinedClassArray=[[NSMutableArray alloc]init];
        if (filterDescArray.count>0) {
            for (NSInteger i=0; i<filterDescArray.count; i++) {
                
                NSString* currentString=[NSString stringWithFormat:@"%@", [filterDescArray objectAtIndex:i]];
                
                NSString* refinedStr=[self getValidStringValue:currentString];
                
                if (refinedStr.length>0) {
                    [refinedClassArray addObject:refinedStr];
                    
                }
            }
            [refinedClassArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            
            [self textFieldTapped:clinicTextField withTitle:selectedDataString withContentArray:refinedClassArray];
            
        }
        else
        {
            [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
        }
        
        
    }
    else if (textField==tradeChannelTextfield)
    {
        
        selectedDataString=@"TradeChannel";
        
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredContent:@"Custom_Attribute_1"] valueForKey:@"Custom_Attribute_1"];
        
        
        NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
        
        if (filterDescArray.count>0) {
            
            for (NSInteger i=0; i<filterDescArray.count; i++) {
                NSString* currentValue=[NSString stringWithFormat:@"%@",[filterDescArray objectAtIndex:i]];
                NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
                
                if (refinedValue.length>0) {
                    [refinedArray addObject:refinedValue];
                    
                }
            }
            
            [refinedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            
            [self textFieldTapped:tradeChannelTextfield withTitle:selectedDataString withContentArray:refinedArray];
        }
        
        
        else
        {
            
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:NSLocalizedString(@"Please try later", nil) withController:self];
            
        }
        
        
    }
    
    else if (textField==specializationTextfield)
    {
        
        selectedDataString=@"Specialization";
        
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredContent:@"Classification_1"] valueForKey:@"Classification_1"];
        NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
        
        
        if (filterDescArray.count>0) {
            
            for (NSInteger i=0; i<filterDescArray.count; i++) {
                NSString* currentValue=[filterDescArray objectAtIndex:i];
                
                
                NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
                
                if (refinedValue.length>0) {
                    [refinedArray addObject:refinedValue];
                    
                }
                
            }
            [refinedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            [self textFieldTapped:specializationTextfield withTitle:selectedDataString withContentArray:refinedArray];
        }
        
        
        else
        {
            
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:NSLocalizedString(@"Please try later", nil) withController:self];
            
        }
        
        
    }
    else if (textField==territoryTextField)
    {
        
        selectedDataString=@"Territory";
        
        NSMutableArray* clinicsArray=[[self.contentArray valueForKey:@"locations"]valueForKey:@"City"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF CONTAINS %@)", @"N/A"];
        
        
        
        NSArray* refinedArray=[clinicsArray filteredArrayUsingPredicate:predicate];
        
        NSArray *capArray = [refinedArray valueForKeyPath:@"capitalizedString"];
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:capArray];
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        
        //  NSLog(@"refined array with out duplicates count is %@", [arrayWithoutDuplicates description]);
        
        
        NSMutableArray* sortedArray=    [arrayWithoutDuplicates mutableCopy];
        [sortedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        
        if (arrayWithoutDuplicates.count>0) {
            
            [self textFieldTapped:specializationTextfield withTitle:selectedDataString withContentArray:sortedArray];

            
        }
        
        else
        {
            
        }
        
    }
    
    
    
    
    return NO;
}

-(void)textFieldTapped:(MedRepTextField*)selectedTextField withTitle:(NSString*)textFieldTitle withContentArray:(NSMutableArray*)textFieldContentArray
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=textFieldTitle;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterDescArray=textFieldContentArray;
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
}



@end
