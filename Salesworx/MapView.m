//
//  MapViewController.m
//  Miller
//
//  Created by kadir pekel on 2/7/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MapView.h"
#import "Singleton.h"
@interface MapView()

//-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded;
//-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) from to: (CLLocationCoordinate2D) to;
-(void) centerMap;

@end

@implementation MapView

@synthesize lineColor;
@synthesize mapView;


- (id) init
{
	self = [super init];
	if (self != nil) {
        Singleton *single = [Singleton retrieveSingleton];
        
        if (mapView == nil)
        {
            self.mapView = [[MKMapView alloc] init];
        }
		self.mapView.frame = CGRectMake(0, 0, single.frameWidth, single.frameHeight) ;
        
		[self.mapView setMapType:MKMapTypeStandard];
		[self.mapView setZoomEnabled:YES];
		[self.mapView setScrollEnabled:YES];
		[self.mapView setShowsUserLocation:YES];
		[self.mapView setUserInteractionEnabled:YES];
        
       // Singleton *single = [Singleton retrieveSingleton];
  
        span.latitudeDelta=0.2;
        span.longitudeDelta=0.2;
        region.span=span;

        [self.mapView setRegion:region animated:YES];
        [self.mapView regionThatFits:region];
        
        
		[self.mapView setDelegate:self];
		[self.view addSubview:self.mapView];
	
  
        
	}
	return self;
}

- (void) showCurrentLocationAddress:(Place*)currentPlace{
	//Hide the keypad
	
	span.latitudeDelta   = 0.02;
	span.longitudeDelta  = 0.02;
	
	CLLocationCoordinate2D location = {currentPlace.latitude,currentPlace.longitude};
	region.span=span;
    
	region.center=location;
	
	//CSMapAnnotation *annot=[[CSMapAnnotation alloc] initWithPlace:currentPlace] ;
	//[self.mapView addAnnotation:annot];
    if ( (region.center.latitude >= -90) && (region.center.latitude <= 90) && (region.center.longitude >= -180) && (region.center.longitude <= 180))
    {
        [mapView setRegion:[mapView regionThatFits:region]];
    }

	[self centerMap];
}

-(void) centerMap {

	CLLocationDegrees maxLat = -90;
	CLLocationDegrees maxLon = -180;
	CLLocationDegrees minLat = 90;
	CLLocationDegrees minLon = 180;
	for(int idx = 0; idx < routes.count; idx++)
	{
		CLLocation* currentLocation = [routes objectAtIndex:idx] ;
		if(currentLocation.coordinate.latitude > maxLat)
			maxLat = currentLocation.coordinate.latitude;
		if(currentLocation.coordinate.latitude < minLat)
			minLat = currentLocation.coordinate.latitude;
		if(currentLocation.coordinate.longitude > maxLon)
			maxLon = currentLocation.coordinate.longitude;
		if(currentLocation.coordinate.longitude < minLon)
			minLon = currentLocation.coordinate.longitude;
	}
	region.center.latitude     = (maxLat + minLat) / 2;
	region.center.longitude    = (maxLon + minLon) / 2;
	region.span.latitudeDelta  = maxLat - minLat;
	region.span.longitudeDelta = maxLon - minLon;
    

}


#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{

}

#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation {
    if(annotation == map.userLocation) {
           return nil;
         }
    
     
     static NSString *defaultID=@"MYLoction";
     
     MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultID] ;
    [pinView setSelected:YES animated:YES];
    [pinView setEnabled:YES];
     
     if (!pinView) {
           
           pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultID];
           
           
           pinView.canShowCallout=YES;
            pinView.animatesDrop=YES;
           
    
         }
     return pinView;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self RemoveIT];
}

-(void)RemoveIT
{
    
    switch (self.mapView.mapType) {
        case MKMapTypeHybrid:
        {
            self.mapView.mapType = MKMapTypeStandard;
        }
            
            break;
        case MKMapTypeStandard:
        {
            self.mapView.mapType = MKMapTypeHybrid;
        }
            
            break;
        default:
            break;
    }
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = NO;
    [self.mapView removeAnnotations:self.mapView.annotations];
    self.mapView.delegate=nil;
    self.mapView = nil;
    [self.mapView removeFromSuperview];

}






@end
