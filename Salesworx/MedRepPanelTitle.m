//
//  MedRepPanelTitle.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepPanelTitle.h"
#import "MedRepDefaults.h"

@implementation MedRepPanelTitle

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    self.font=MedRepPanelTitleLabelFont;
    self.textColor=MedRepPanelTitleLabelFontColor;
    //    self.textColor=[UIColor yellowColor];
    
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && super.text.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[super.text characterAtIndex:super.text.length-1]];
            NSString *nString=[super.text stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(super.text, nil);
            
        }
    }
    
    
}

-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && newText.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[newText characterAtIndex:newText.length-1]];
            NSString *nString=[newText stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(newText, nil);
            
        }
    }
    else
        super.text = newText;
    
    
}

@end
