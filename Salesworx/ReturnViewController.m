//
//  ReturnViewController.m
//  SWCustomer
//
//  Created by msaad on 1/2/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.

#import "ReturnViewController.h"
#import "SWReturnConfirmationViewController.h"
@interface ReturnViewController ()

@end

@implementation ReturnViewController
@synthesize delegate;

- (id)initWithCustomer:(NSDictionary *)customerDict andCategory:(NSDictionary *)categoryDict;
{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionaryWithDictionary:customerDict];
        category = [NSMutableDictionary dictionaryWithDictionary:categoryDict];
        [self setTitle:NSLocalizedString(@"Create Returns", nil)];
       Singleton *single = [Singleton retrieveSingleton];
        single.isSaveOrder = NO;
        productDict = [NSMutableDictionary dictionary ];
        
        
        productListViewController = [[ProductListViewController alloc] initWithCategory:category] ;
        [productListViewController setTarget:self];
        [productListViewController setAction:@selector(productSelected:)];
        navigationControllerS = [[UINavigationController alloc] initWithRootViewController:productListViewController] ;
        
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentProductList:)] ];
        
        CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60) andCustomer:customer] ;
        [headerView setShouldHaveMargins:YES];
        //[self.gridView.tableView setTableHeaderView:headerView];
        
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeOrder)] ];
        [self.gridView setShouldAllowDeleting:YES];
        
        

        

    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    gridView.frame=CGRectMake(0,40, self.view.bounds.size.width, self.view.bounds.size.height-140);

    
    UILabel *orderItemLbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)] ;
    [orderItemLbl setText:@"Return Items"];
    [orderItemLbl setTextAlignment:NSTextAlignmentCenter];
    [orderItemLbl setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
    [orderItemLbl setTextColor:[UIColor darkGrayColor]];
    [orderItemLbl setFont:BoldSemiFontOfSize(20.0)];
    [self.view addSubview:orderItemLbl];
    
    totalProduct=[[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-200, 0, 180, 40)] ;
    [totalProduct setText:@"0 item(s)"];
    [totalProduct setTextAlignment:NSTextAlignmentCenter];
    [totalProduct setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
    [totalProduct setTextColor:[UIColor darkGrayColor]];
    [totalProduct setFont:RegularFontOfSize(20.0)];
    [self.view addSubview:totalProduct];
    
    saveButton = [[UIButton alloc] init] ;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
    [saveButton setFrame:CGRectMake(620,425, 120, 42)];
    [saveButton addTarget:self action:@selector(saveOrder:) forControlEvents:UIControlEventTouchUpInside];
    [saveButton bringSubviewToFront:self.view];
   // saveButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [saveButton setTitle:NSLocalizedString(@"Save Returns", nil) forState:UIControlStateNormal];
    saveButton.titleLabel.font = BoldSemiFontOfSize(15);
    [self.view addSubview:saveButton];
    
    totalPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,self.gridView.bounds.size.height + 80, 300, 30)] ;
    [totalPriceLabel setText:@""];
    [totalPriceLabel setTextAlignment:NSTextAlignmentLeft];
    [totalPriceLabel setBackgroundColor:[UIColor clearColor]];
    [totalPriceLabel setTextColor:[UIColor darkGrayColor]];
    [totalPriceLabel setFont:BoldSemiFontOfSize(20.0)];
    totalPriceLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:totalPriceLabel];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [Flurry logEvent:@"Return Order View"];
    //[Crittercism leaveBreadcrumb:@"<Return Order View>"];

    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:YES];
    
    //
    //[self setupToolbar];
}

- (void)presentProductList:(id)sender
{
    [SWDefaults setProductCategory:category];
    productListViewController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
}

#pragma mark ProductListView Controller action
- (void)productSelected:(NSDictionary *)product {
    productDict = [NSMutableDictionary dictionaryWithDictionary:product];
    
    //serProduct.delegate=self;
    
//    [self getProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[product stringForKey:@"ItemID"]]];
    
    NSLog(@"check product in return delegate %@", product);
    
      [self getProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[product stringForKey:@"ItemID"] :[customer stringForKey:@"Price_List_ID"]]];
}

- (void)presentProductOrder:(NSDictionary *)product {
    
    if (![product objectForKey:@"Guid"])
    {
        [product setValue:[NSString createGuid] forKey:@"Guid"];
    }
    
    
    orderViewController = [[ProductReturnViewController alloc] initWithProduct:product] ;
    [orderViewController setTarget:self];
    [orderViewController setAction:@selector(productAdded:)];
    orderViewController.hidesBottomBarWhenPushed = YES;

    [self.navigationController pushViewController:orderViewController animated:YES];
    orderViewController = nil;

}


- (void)getProductServiceDidGetCheckedPrice:(NSArray *)priceDetail
{
   
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customer stringForKey:@"Price_List_ID"]])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            [self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.0f];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            [self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.0f];
        }
        else
        {
            // no price . stop here
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        [alert show];
//            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                               message:@"No prices availble for selected product."
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:nil];
            
            
        }
    }
    else
    {
        // no price . stop here
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"No prices availble for selected product."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        
    }
priceDetail=nil;
}
- (void)productAdded:(NSMutableDictionary *)q{

    NSMutableArray *array = [NSMutableArray arrayWithArray:self.items];
    NSMutableDictionary *p = [NSMutableDictionary dictionaryWithDictionary:q  ];

    for (int i = array.count - 1; i >= 0; i--)
    {
        @autoreleasepool{
        NSDictionary *data = [array objectAtIndex:i];
        if ([[data objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
        {
            [array removeObjectAtIndex:i];
        }
        else if ([[data objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
        {
            [array removeObjectAtIndex:i];
        }
        }
    }
    [array addObject:p];
    if ([p stringForKey:@"Bonus"].length > 0)
    {
        if ([[p objectForKey:@"Bonus"] intValue] > 0)
        {
            NSMutableDictionary *b = [NSMutableDictionary dictionaryWithDictionary:p];
            [b setValue:[NSString createGuid] forKey:@"Guid"];
            [b setValue:@"0" forKey:@"Price"];
            [b setValue:[p stringForKey:@"Bonus"] forKey:@"Qty"];
            [b setValue:@"0" forKey:@"Bonus"];
            [b removeObjectForKey:@"StockAllocation"];
            [p setValue:[b stringForKey:@"Guid"] forKey:@"ChildGuid"];
            [b setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
            [array removeLastObject];
            [array addObject:p];
            [array addObject:b];
        }
    }
    [self setItems:array];
    [self.gridView reloadData];
    [self updateTotal];
    [totalProduct setText:[NSString stringWithFormat:@"%d item(s)",self.items.count]];

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0)
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}
- (void)closeOrder {
    if([self.items count] > 0 )
    {
       Singleton *single = [Singleton retrieveSingleton];
        if(single.isSaveOrder)
        {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
            [self.navigationController setToolbarHidden:YES animated:YES];

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:@"Would you like to cancel the sales return without saving?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
            [alert show];
//            [ILAlertView showWithTitle:NSLocalizedString(@"Warning", nil)
//                               message:@"Would you like to cancel the order without saving?"
//                      closeButtonTitle:NSLocalizedString(@"No", nil)
//                     secondButtonTitle:NSLocalizedString(@"Yes", nil)
//                   tappedButtonAtIndex:^(NSInteger buttonIndex) {if (buttonIndex==1)
//                   {
//                       [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//                   [self.navigationController  popViewControllerAnimated:YES];
//                   }}];
        }
            
    }
    else
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
        [self.navigationController setToolbarHidden:YES animated:YES];

    }

}
- (void)setupToolbar {
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }
    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    NSString *amountString =[NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Credit Note Value", nil)];
    totalLabelButton1 = [UIBarButtonItem labelButtonWithTitle:amountString];
    
    totalLabelButton = [UIBarButtonItem labelButtonWithTitle:[[NSString stringWithFormat:@"%.2f",totalOrderAmount] currencyString]];
  //  saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(saveOrder:)] ;
    //[self setupToolbar:[NSArray arrayWithObjects:flexibleSpace,totalLabelButton1,totalLabelButton, saveButton, nil]];
}

- (void)updateTotal {
    double price = 0.0f;
    for (int i = 0;i < self.items.count; i++)
    {
        @autoreleasepool{
        NSDictionary *data = [self.items objectAtIndex:i];
       
            //return price fixed
            
            
            //remove bonus items price
            if ([[data valueForKey:@"Price"] isEqualToString:@"0"]) {
                
                
                //bonus item
            }
            
            else
            {
            
            
           double currentPrice = [[data objectForKey:@"Net_Price"] doubleValue];
            currentPrice=currentPrice*[[data objectForKey:@"Qty"] doubleValue];
            price=price+currentPrice;
//
            }
            
            
//            NSDictionary *data = [self.items objectAtIndex:i];
//            float netPrice =[[data stringForKey:@"Net_Price"] floatValue];
//            float qty=[[data stringForKey:@"Qty"] floatValue];
//            price=netPrice*qty;
            
        }
    }
    totalOrderAmount = price;
    //[totalPriceLabel setText:[[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Total Price : ", nil), [[NSString stringWithFormat:@"%.2f",totalOrderAmount] currencyString]]]];
    [totalPriceLabel setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Total Price: ", nil),[[NSString stringWithFormat:@"%.2f",totalOrderAmount] currencyString]]];
  //  [self setupToolbar];
}


-(void)saveOrder:(id)sender
{
    //Singleton *single = [Singleton retrieveSingleton];
    
    NSLog(@"check items in save return %@", self.items);
    
    if (self.items.count>0) {
        NSMutableDictionary *salesOrder = [NSMutableDictionary dictionary];
        [salesOrder setValue:self.items forKey:@"OrderItems"];
        
        NSMutableDictionary *info = [NSMutableDictionary dictionary];
        [info setValue:[NSString stringWithFormat:@"%.2f",totalOrderAmount] forKey:@"orderAmt"];
        [info setValue:[category stringForKey:@"Category"] forKey:@"productCategory"];
        [salesOrder setValue:info forKey:@"info"];
        
        SWReturnConfirmationViewController* returnConfrimation=[[SWReturnConfirmationViewController alloc]init];
        returnConfrimation.salesOrderDict=salesOrder;
        returnConfrimation.customerDict=customer;
        [self.navigationController pushViewController:returnConfrimation animated:YES];
        
        /*
         orderAdditionalInfoViewController = [[ReturnAdditionalViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
         [self.navigationController pushViewController:orderAdditionalInfoViewController animated:YES];
         */
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add item." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
}
//- (void)saveOrder:(id)sender {
//   //Singleton *single = [Singleton retrieveSingleton];
//    if (self.items!=0) {
//        NSMutableDictionary *salesOrder = [NSMutableDictionary dictionary];
//        [salesOrder setValue:self.items forKey:@"OrderItems"];
//        
//        NSMutableDictionary *info = [NSMutableDictionary dictionary];
//        [info setValue:[NSString stringWithFormat:@"%.2f",totalOrderAmount] forKey:@"orderAmt"];
//        [info setValue:[category stringForKey:@"Category"] forKey:@"productCategory"];
//        [salesOrder setValue:info forKey:@"info"];
//        
//        orderAdditionalInfoViewController = [[ReturnAdditionalViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
//        [self.navigationController pushViewController:orderAdditionalInfoViewController animated:YES];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please add item." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//    }
//}
#pragma mark GridView DataSource
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return self.items.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return 40.0f;
            break;
        case 1:
            return 20.0f;
            break;
        case 2:
            return 20.0f;
            break;
        case 3:
            return 20.0f;
            break;
        default:
            break;
    }
    return 20.0f;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    switch (column) {
        case 0:
            title = NSLocalizedString(@"Name", nil);
            break;
        case 1:
            title = NSLocalizedString(@"Quantity", nil);
            break;
        case 2:
            title = NSLocalizedString(@"Wholesale Price", nil);
            break;
        case 3:
            title = NSLocalizedString(@"Total Price", nil);
            break;
        default:
            break;
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [self.items objectAtIndex:row];
    
    
    
    
    
    float netPrice =[[data stringForKey:@"Net_Price"] floatValue];
    float qty=[[data stringForKey:@"Qty"] floatValue];
    float totalReturnPrice=netPrice*qty;
    

    switch (column) {
        case 0:
            text = [data stringForKey:@"Description"];
            break;
        case 1:
            text = [data stringForKey:@"Qty"];
            break;
        case 2:
            
            if ([[data valueForKey:@"Price"] isEqualToString:@"0"]) {
                
                
                //bonus item so price is 0
                
                
                NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

                
                text=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,[[data valueForKey:@"Price"] floatValue]];
                
            }
            else
            {
                text = [data currencyStringForKey:@"Net_Price"];

                
            }
            break;
        case 3:
           // text = [data currencyStringForKey:@"Price"];
            if ([[data valueForKey:@"Price"] isEqualToString:@"0"]) {
                
                
                //bonus item so price is 0
                
                NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

                
                text=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,[[data valueForKey:@"Price"] floatValue]];
                
            }
            else
            {
            
                
                NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

            text=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode, totalReturnPrice];
            }
            
            break;
        default:
            break;
    }
    
    return text;
}

#pragma mark GridView Delegate
- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex
{
    if([[[self.items objectAtIndex:rowIndex] valueForKey:@"Price"] isEqualToString:@"0"])
    {
        if (![[self.items objectAtIndex:rowIndex-1] objectForKey:@"Guid"])
        {
            [[self.items objectAtIndex:rowIndex-1] setValue:[NSString createGuid] forKey:@"Guid"];
        }
        [self.delegate selectedProduct:[self.items objectAtIndex:rowIndex-1]];
    }
    else
    {
        if (![[self.items objectAtIndex:rowIndex] objectForKey:@"Guid"])
        {
            [[self.items objectAtIndex:rowIndex] setValue:[NSString createGuid] forKey:@"Guid"];
        }
        [self.delegate selectedProduct:[self.items objectAtIndex:rowIndex]];
    }
    
//    orderViewController = [[ProductReturnViewController alloc] initWithProduct:[self.items objectAtIndex:rowIndex]] ;
//    [orderViewController setTarget:self];
//    [orderViewController setAction:@selector(productAdded:)];
//    [self.navigationController pushViewController:orderViewController animated:YES];
//    orderViewController = nil;
}

- (void)gridView:(GridView *)gv commitDeleteRowAtIndex:(int)rowIndex {
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.items];
    
    NSDictionary *product = [array objectAtIndex:rowIndex];
   //Singleton *single = [Singleton retrieveSingleton];
    if(![[product valueForKey:@"Price"] isEqualToString:@"0"])
    {
            if ([product objectForKey:@"ParentGuid"])
            {
                for (int i = 0;i < array.count; i++)
                {
                    @autoreleasepool{
                    NSDictionary *data = [array objectAtIndex:i];
                    if ([[data objectForKey:@"Guid"] isEqualToString:[product objectForKey:@"ParentGuid"]])
                    {
                        [data setValue:@"0" forKey:@"Bonus"];
                        [array replaceObjectAtIndex:i withObject:data];
                        [self.gridView reloadRowAtIndex:i];
                        break;
                    }
                    }
                }
            }
            else if ([product objectForKey:@"ChildGuid"])
            {
                for (int i = 0;i < array.count; i++)
                {
                    @autoreleasepool{
                    NSDictionary *data = [array objectAtIndex:i];
                    if ([[data objectForKey:@"Guid"] isEqualToString:[product objectForKey:@"ChildGuid"]])
                    {
                        [array removeObjectAtIndex:i];
                        [self setItems:array];
                        [self.gridView deleteRowAtIndex:i];
                        break;
                    }
                    }
                }
            }
            [self.delegate selectedProduct:nil];
            [array removeObjectAtIndex:rowIndex];
            [self setItems:array];
            [gv deleteRowAtIndex:rowIndex];
            [self updateTotal];
        }
    else
    {
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Bonus product can not be deleted" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Bonus product can not be deleted"
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        }
    [totalProduct setText:[NSString stringWithFormat:@"%d item(s)",self.items.count]];

}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}


@end
