//
//  CustomPopOverViewController.h
//  MathMonsters
//
//  Created by Transferred on 1/12/13.
//  Copyright (c) 2013 Designated Nerd Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "filterPopupPushVC.h"
#import "MedRepDefaults.h"


@protocol StringPopOverViewController_ReturnRow <NSObject>
@optional
-(void)selectedStringValue:(NSIndexPath *)indexPath;
-(void)selectedIndex:(NSInteger )parentIndex andChildIndex :(NSInteger)childIndex;
@end

@interface StringPopOverViewController_ReturnRow : UITableViewController <UISearchBarDelegate,UISearchDisplayDelegate,filterPopupPushVCDelegate,UITextFieldDelegate>{
    
    UISearchBar *searchBar;
    UISearchDisplayController *searchDisplayController;
    BOOL searching;
    BOOL forFilter;
    
    NSInteger selectedParentIndex;
}
-(id)initwithStyle:(UITableViewStyle)style andContentSize:(CGSize)popoverContentSize;

-(id)initWithStyle:(UITableViewStyle)style withWidth:(int)withWidth;
-(id)initWithStyle:(UITableViewStyle)style withWidthForFilter:(int)withWidth;
@property (nonatomic, strong) UIPopoverController *selectedPopover;
@property (nonatomic, strong) NSMutableArray *childArray;


@property (nonatomic, strong) NSMutableArray *colorNames;
@property (nonatomic, strong) NSMutableArray *colorNamesFiltered;
@property(nonatomic) BOOL isProductsUOM;
@property (nonatomic, weak) id<StringPopOverViewController_ReturnRow> delegate;
@end
