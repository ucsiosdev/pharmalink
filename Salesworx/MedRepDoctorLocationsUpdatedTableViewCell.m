//
//  MedRepDoctorLocationsUpdatedTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/9/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepDoctorLocationsUpdatedTableViewCell.h"
#import "MedRepDefaults.h"

@implementation MedRepDoctorLocationsUpdatedTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.titleLbl.font= kSWX_FONT_SEMI_BOLD(16);
    self.descLbl.font = kSWX_FONT_SEMI_BOLD(14);
    self.accountNameTitleLbl.font = kSWX_FONT_SEMI_BOLD(14);
    self.accountNameLbl.font = kSWX_FONT_SEMI_BOLD(14);

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSelectedCellStatus
{
    
    self.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    self.accountNameLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    self.accountNameTitleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    [self.descLbl setTextColor:MedRepMenuTitleSelectedCellTextColor];
    
    [self.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
}
-(void)setDeSelectedCellStatus
{
    self.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    self.descLbl.textColor=MedRepDescriptionLabelUnselectedColor;
    self.accountNameLbl.textColor=MedRepDescriptionLabelUnselectedColor;
    self.accountNameTitleLbl.textColor=MedRepDescriptionLabelUnselectedColor;
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.saperatorImageView.hidden=NO;
}
@end
