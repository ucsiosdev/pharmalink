//
//  SalesWorxPaymentCollectionViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxPaymentCollectionViewController.h"
#import "MedRepUpdatedDesignCell.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxPopOverViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxPaymentCollectionInvoicesTableViewCell.h"
#import "SalesWorxPaymentCollectionInvoicesHeaderTableViewCell.h"
#import "MedRepCreateVisitDatePickerViewController.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxStatementViewController.h"
#import "SalesWorxSalesOrderLPOImagesCollectionViewCell.h"
#import "LPOImagesGalleryCollectionDetailViewController.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
#import "SalesWorxCustomerStatementPDFTemplateView.h"
#define KMaximumAllowedCollectionImages 1
#define KCollectionMaximumAmount 10000000000
@interface SalesWorxPaymentCollectionViewController ()

@end

@implementation SalesWorxPaymentCollectionViewController

@synthesize customerListArray,customerListTableView,customerSectionsListArray,customerSearchBar,selectedCustomerIndexPath,customerNameLbl,customerNumberLbl,paymentMethodButton,paymentButtonDropDownImageView,currencyButton,invoicesTableView,signatureBgView,customerDetailsHeaderView,chequeDateButton,chequeDateDropDownImageView,customersSearchButton,invoicesView,currencyTextField,chequeDateTextField,paymentTypeSegmentControl,imageViewCustomerStatus;


- (id)init {
    self = [super init];
    if (self)
    {
        shouldPresentCustomerList = YES;
    }
    return self;
}

- (id)initWithCustomer:(NSDictionary *)c {
    self = [self init];
    if (self) {
        customer = c;
        shouldPresentCustomerList = NO;
    }
    return self;
}

-(void)popBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(popBack) userInfo:nil repeats:NO];
    
    
    self.view.backgroundColor=UIViewBackGroundColor;
    
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    
    keyboardIsShowing=NO;
    isStatementPopUpIsOpen = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Payment Collection", nil)];
    
    indexPathArray=[[NSMutableArray alloc]init];
    collectionInfoDictionary=[[NSMutableDictionary alloc]init];
    selectedCustomerIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    invoicesArray=[[NSMutableArray alloc]init];
    
    settledAmount=0;
    unsettledAmount=[NSDecimalNumber zero];
    collectedAmount=0;
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {            
            
            leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            [leftBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        }
        else
        {
            leftBtn = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnBack:)];
            [leftBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
            self.navigationItem.leftBarButtonItem = leftBtn;
        }
    }
    
    statementButton = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Collection_StatementIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(showStatement)];
    [statementButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    saveButton = [[SalesWorxImageBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Collection_SaveIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnSave)];
    [saveButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    
    
    cameraBarButton=[[SalesWorxImageBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"CameraIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(showCameraPopOver)];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:saveButton,statementButton, cameraBarButton,nil];
    
    
    appCtrl = [AppControl retrieveSingleton];
    bankNames=[[NSMutableArray alloc]init];
}

-(void)viewWillAppear:(BOOL)animated
{
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    
    customerListTableView.delegate=self;
    customerListTableView.rowHeight = UITableViewAutomaticDimension;
    customerListTableView.estimatedRowHeight = 70.0;
    
    
    lblVisitType.text = NSLocalizedString(self.currentVisit.visitOptions.customer.Visit_Type, nil);
    if (self.currentVisit.visitOptions.customer.Visit_Type.length == 0) {
        lblVisitType.hidden = YES;
    }
    
    signatureBGViewFrame=signatureBgView.frame;
    invoicesViewFrame=invoicesView.frame;
    previousFrame=self.view.frame;
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:@"Collections"];
    }
    
    if (shouldPresentCustomerList)
    {
        //        _xConstraintOfCustomerView.constant = 260.0f;
        //        _xConstraintOfCustomerDetailView.constant = 260.0f;
        //        _xConstraintOfInvoiceView.constant = 260.0f;
        //        _xConstraintOfSignatureView.constant = 260.0f;
        
        
        xSettleMentViewTrailingConstraint.constant=0;
        xSettleMentViewWidthConstraint.constant=0;
        
        
        NSMutableArray *unSortedArray = [[SWDatabaseManager retrieveManager]fetchCustomerswithOpenInvoices];
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Customer_Name" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        customerListArray = (NSMutableArray *)[unSortedArray sortedArrayUsingDescriptors:sortDescriptors];
        if (@available(iOS 13, *)) {
            customerSearchBar.searchTextField.background = [[UIImage alloc] init];
            customerSearchBar.searchTextField.backgroundColor = MedRepSearchBarBackgroundColor;
        }else{
            customerSearchBar.backgroundImage = [[UIImage alloc] init];
            customerSearchBar.backgroundColor = MedRepSearchBarBackgroundColor;
        }
        
        
        [_settlementView setHidden:YES];
        //        pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(8,8,667,129)];
        //        pjrSignView.delegate=self;
        //        [pjrSignView.layer setBorderWidth: 1.0];
        //        [pjrSignView.layer setCornerRadius:5.0f];
        //        [pjrSignView.layer setMasksToBounds:YES];
        //        [pjrSignView.layer setBorderColor:[[UIColor colorWithRed:(234.0/255.0) green:(237.0/255.0) blue:(242.0/255.0) alpha:1] CGColor]];
        //        [signatureBgView addSubview:pjrSignView];
        
    }
    else
    {
        customerListArray  = [[NSMutableArray alloc]init];
        [customerListArray addObject:customer];
        
        parentViewOfTable.hidden = YES;
        titleInvoiceAMount.hidden = YES;
        titleUnsettled.hidden = YES;
        balanceLbl.hidden = YES;
        unsettledLbl.hidden = YES;
        xCustomerTableViewContainerWidthConstraint.constant=0;
        CustomerTableViewTrailingConstraint.constant=0;
        //        [_settlementView setFrame:CGRectMake(764, invoicesView.frame.origin.y, _settlementView.frame.size.width, invoicesView.frame.size.height)];
        //        [self.view addSubview:_settlementView];
        [_settlementView setHidden:NO];
        
        
    }
    
    if([appCtrl.ENABLE_COLLECTION_IMAGE_CAPTURE isEqualToString:KAppControlsYESCode])
    {
        collectionImgesCollectionView.delegate=self;
        collectionImgesCollectionView.dataSource=self;
        [collectionImgesCollectionView registerClass:[SalesWorxSalesOrderLPOImagesCollectionViewCell class] forCellWithReuseIdentifier:@"LPOImageCell"];
    }
    else
    {
        [collectionImgesCollectionView setHidden:YES];
    }
    
    if([appCtrl.ENABLE_COLLECTION_REMARKS isEqualToString:KAppControlsNOCode])
    {
        remarksTextField.hidden = YES;
        remarksLabel.hidden = YES;
        xRemarksLabelTrailingConstraint.constant = 0;
        xRemarksLabelWidthConstraint.constant = 0;
        xRemarksLabelLeadingConstraint.constant = 0;
    }
    
    [self hideNoSelectionView];
}
-(void)viewDidAppear:(BOOL)animated
{
    [pjrSignContainerView setNeedsLayout];
    [pjrSignContainerView layoutIfNeeded];
    
    if(self.isMovingToParentViewController)
    {
        if (customerListArray.count>0)
        {
            NSLog(@"check temp collection %@", [invoicesArray description]);
            
            unfilteredProductsArray = customerListArray;
            // Do any additional setup after loading the view from its nib.
            NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:MedRepSegmentControlFont, NSFontAttributeName, nil];
            [paymentTypeSegmentControl setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
            paymentMethodsArray=[[NSMutableArray alloc]initWithObjects:@"Current Cheque",@"Cash",@"PDC", nil];

            [customerListTableView reloadData];
            
            //fetch incoices data
            
            if ([customerListTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])[customerListTableView.delegate tableView:customerListTableView didSelectRowAtIndexPath:selectedCustomerIndexPath];
            
            [customerListTableView scrollToRowAtIndexPath:selectedCustomerIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            [customerListTableView scrollToRowAtIndexPath:selectedCustomerIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
            [customerListTableView.delegate tableView:customerListTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        
        [_paymentTextField setText:@"Current Cheque"];
        if ([appCtrl.ENABLE_COLLECTION_IMAGE_CAPTURE isEqualToString:@"Y"]) {
            collectionImgesCollectionView.hidden = NO;
        }
        [self loadOnChequeSelection];
        if(self.isMovingToParentViewController)
        {
            [self UpdateSignatureViewsForStatus:KEmptyString];
        }
        
        //     pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(8,8,920,129)];
        pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(0, 0, pjrSignContainerView.frame.size.width, pjrSignContainerView.frame.size.height)];;
        pjrSignView.delegate=self;
        [pjrSignView.layer setBorderWidth: 1.0];
        [pjrSignView.layer setCornerRadius:5.0f];
        [pjrSignView.layer setMasksToBounds:YES];
        [pjrSignView.layer setBorderColor:[[UIColor colorWithRed:(234.0/255.0) green:(237.0/255.0) blue:(242.0/255.0) alpha:1] CGColor]];
        [pjrSignContainerView addSubview:pjrSignView];
        
    }
    if([appCtrl.SHOW_COLLECTION_BANK_DROPDOWN isEqualToString:KAppControlsYESCode])
    {
        [bankTextField setDropDownImage:@"Arrow_DropDown"];
        bankNames=[[SWDatabaseManager retrieveManager]fetchListValueOfBankNames];
        bankTextField.text = @"N/A";
    }
}
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    keyboardIsShowing = YES;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    keyboardIsShowing = NO;
    if(frameChanged){
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectOffset(self.view.frame, 0, 220);
        } completion:^(BOOL finished) {
            
        }];
    }
    NSLog(@"FRAME AFTER CHANGE IN DID HIDE %@", NSStringFromCGRect(self.view.frame));
    frameChanged=NO;
}

#pragma mark Back methods
- (void) btnBack:(id)sender
{
    if ([_amountTxtFld.text floatValue] >0)
    {
        [self showPaymentCollectionCancellationConfirmationalert];
        
    }
    else {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
}

-(NSMutableArray*)fetchPaymentImageArray
{
    NSMutableArray* imageArray=[[NSMutableArray alloc]init];
    
    if ([appCtrl.ENABLE_COLLECTION_IMAGE_CAPTURE isEqualToString:KAppControlsYESCode]) {
        PaymentImage* chequeImage=[[PaymentImage alloc]init];
        chequeImage.title=@"Cheque";
        chequeImage.imageType=kChequeImageType;
        chequeImage.image=[UIImage imageNamed:@"SalesOrder_AddImage_CameraIcon"];
        [imageArray addObject:chequeImage];
    }
    
    PaymentImage* receiptImage=[[PaymentImage alloc]init];
    receiptImage.title=@"Receipt";
    receiptImage.imageType=kReceiptImageType;
    receiptImage.image=[UIImage imageNamed:@"SalesOrder_AddImage_CameraIcon"];
    [imageArray addObject:receiptImage];
    
    return imageArray;
}
-(void)capturedImages:(NSMutableArray*)imagesArray
{
    collectionImagesFromDelegate=[[NSMutableArray alloc]init];
    collectionImagesFromDelegate=imagesArray;
}

-(void)showCameraPopOver
{
    SalesWorxPaymentCollectionImagePopoverViewController *viewController = [[SalesWorxPaymentCollectionImagePopoverViewController alloc]init];
    viewController.delegate=self;
    if (collectionImagesFromDelegate.count>0) {
        viewController.contentArray=collectionImagesFromDelegate;
    }
    else{
        viewController.contentArray=[self fetchPaymentImageArray];
    }
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:viewController];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    viewController.preferredContentSize = CGSizeMake(200, 300);
    popover.delegate = self;
    popover.barButtonItem = cameraBarButton;
    popover.sourceView = self.view;
    viewController.popOverController = popover;
    popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    [self presentViewController:nav animated:YES completion:^{
        if(keyboardIsShowing)
        {
            [UIView animateWithDuration:0.2 animations: ^{
                [self.view endEditing:YES];
                
            } completion:^(BOOL finished) {
            }];
        }
    }];
}
-(void)showStatement
{
    [self.view endEditing:YES];
    
    NSMutableArray* statementArray=[[NSMutableArray alloc]init];
    if (customerListArray.count>0)
    {
        isStatementPopUpIsOpen = YES;
        NSMutableDictionary * customerDict = selectedCustomer;
        statementArray=[[[SWDatabaseManager retrieveManager] dbGetInvoicesOfCustomer:[customerDict stringForKey:@"Customer_ID"] withSiteId:[customerDict stringForKey:@"Site_Use_ID"]andShipCustID:[customerDict stringForKey:@"Ship_Customer_ID"] withShipSiteId:[customerDict stringForKey:@"Ship_Site_Use_ID"] andIsStatement:YES] mutableCopy];
        
        
        
        SalesWorxStatementViewController *viewController = [[SalesWorxStatementViewController alloc]initWithCustomer:customerDict];
        
        Customer * selectedCustomerObject=[SWDefaults fetchCurrentCustObject:customerDict];
        viewController.selectedCustomer=selectedCustomerObject;
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:viewController];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        viewController.preferredContentSize = CGSizeMake(686, 700);
        popover.delegate = self;
        popover.barButtonItem = statementButton;
        popover.sourceView = self.view;
        viewController.popOverController = popover;
        popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
        
        [self presentViewController:nav animated:YES completion:^{
        }];
        leftBtn.enabled = NO;
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Statements Available" andMessage:@"Please try later" withController:self];

    }
}

//-(NSData*)makePDFfromView:(UIView*)view
//{
//
//    UIGraphicsBeginPDFPage();
//    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
//    [view.layer renderInContext:pdfContext];
//
//    UIGraphicsBeginPDFPage();
//    pdfContext = UIGraphicsGetCurrentContext();
//    [view.layer renderInContext:pdfContext];
//
//    UIGraphicsBeginPDFPage();
//    pdfContext = UIGraphicsGetCurrentContext();
//    [view.layer renderInContext:pdfContext];
//
//    UIGraphicsEndPDFContext();
//
//    return pdfData;
//}
-(void)loadSelectedCustomerDetails:(NSIndexPath*)selectedIndexPath
{
    customerPaymentDetails=[[CustomerPaymentClass alloc]init];
    
    customerPaymentDetails.customerName=[NSString stringWithFormat:@"%@", [selectedCustomer valueForKey:@"Customer_Name"]];
    
    customerPaymentDetails.customerId=[NSString stringWithFormat:@"%@", [selectedCustomer valueForKey:@"Customer_No"]];
    
    customerNameLbl.text=customerPaymentDetails.customerName;
    
    customerPaymentDetails.paymentMethod=@"current cheque";
    
    customerNumberLbl.text=customerPaymentDetails.customerId;
    
    customerPaymentDetails.settledAmount=[NSDecimalNumber zero];
}

#pragma mark UITableView Data source Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:customerListTableView]) {
        return UITableViewAutomaticDimension;
    } else {
        return 49.0f;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%d",tableView == customerListTableView);
    NSLog(@"%lu",(unsigned long)customerListArray.count);
    if (tableView==invoicesTableView )
    {
        return customerPaymentDetails.invoicesArray.count;
    }
    else if (tableView==customerListTableView)
    {
        [saveButton setEnabled:YES];
        [signatureClearButton setEnabled:YES];
        [signatureSaveButton setEnabled:YES];
        [signatureBgView setHidden:NO];
        [statementButton setEnabled:YES];
        
        _paymentButton.userInteractionEnabled = YES;
        _paymentTextField.userInteractionEnabled = YES;
        currencyTextField.userInteractionEnabled = YES;
        currencyButton.userInteractionEnabled = YES;
        _amountTxtFld.userInteractionEnabled = YES;
        chequeNumberTxtFld.userInteractionEnabled = YES;
        remarksTextField.userInteractionEnabled = YES;
        chequeDateTextField.userInteractionEnabled = YES;
        chequeDateButton.userInteractionEnabled = YES;
        bankTextField.userInteractionEnabled = YES;
        branchTxtFld.userInteractionEnabled = YES;
        
        if (isSearching==YES) {
            return tempCustomerListArray.count;
        }
        else if (isSearching==NO) {
            return customerListArray.count;
        }
        else {
            [self clearTheViewsOnNoSelection];
            return 0;
        }
    }
    else {
        [self clearTheViewsOnNoSelection];
        return 0;
    }
}
-(void)clearTheViewsOnNoSelection
{
    selectedCustomerIndexPath=nil;
    customerPaymentDetails=[[CustomerPaymentClass alloc]init];
    [_paymentTextField setText:@"Current Cheque"];
    if ([appCtrl.ENABLE_COLLECTION_IMAGE_CAPTURE isEqualToString:@"Y"]) {
        collectionImgesCollectionView.hidden = NO;
    }
    [self.amountTxtFld setText:@""];
    [bankTextField setText:@""];
    if([appCtrl.SHOW_COLLECTION_BANK_DROPDOWN isEqualToString:KAppControlsYESCode])
    {
        bankTextField.text = @"N/A";
    }
    
    [chequeNumberTxtFld setText:@""];
    [remarksTextField setText:@""];
    [branchTxtFld setText:@""];
    unsettledLbl.text=@"N/A";
    balanceLbl.text=@"N/A";
    
    newBalanceLbl.text=@"N/A";
    newUnsettledLbl.text=@"N/A";
    
    customerNameLbl.text=KNotApplicable;
    customerNumberLbl.text=KNotApplicable;
    [imageViewCustomerStatus setBackgroundColor:[UIColor whiteColor]];
    
    [invoicesTableView reloadData];
    [saveButton setEnabled:NO];
    [signatureBgView setHidden:YES];
    [statementButton setEnabled:NO];
    
    _paymentButton.userInteractionEnabled = NO;
    _paymentTextField.userInteractionEnabled = NO;
    currencyTextField.userInteractionEnabled = NO;
    currencyButton.userInteractionEnabled = NO;
    _amountTxtFld.userInteractionEnabled = NO;
    chequeNumberTxtFld.userInteractionEnabled = NO;
    remarksTextField.userInteractionEnabled = NO;
    chequeDateTextField.userInteractionEnabled = NO;
    chequeDateButton.userInteractionEnabled = NO;
    bankTextField.userInteractionEnabled = NO;
    branchTxtFld.userInteractionEnabled = NO;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark UIAlertview Delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==906)
    {
        [self refreshTheCustomerCollectionDetailsView];
    }
    else if (alertView.tag == 1000)
    {
        if (buttonIndex == 0)
        {
            @try
            {
                keyboardIsShowing = YES;
                selectedCustomerIndexPath = tempCustomerIndexPath;
                [self.amountTxtFld setText:@""];
                [self tableView:customerListTableView didSelectRowAtIndexPath:selectedCustomerIndexPath];
            }
            @catch (NSException *exception) {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController  popViewControllerAnimated:YES];
            }
        }
    }
}
-(void)showPaymentCollectionCancellationConfirmationalert
{
    [UIView setAnimationsEnabled:YES];
    UIAlertAction* yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    if(shouldPresentCustomerList){
                                        selectedCustomerIndexPath = tempCustomerIndexPath;
                                        [self.amountTxtFld setText:@""];
                                        [self tableView:customerListTableView didSelectRowAtIndexPath:selectedCustomerIndexPath];
                                        [self updateAmountLabels];
                                    }
                                    else
                                    {
                                        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                                        [self.navigationController  popViewControllerAnimated:YES];
                                    }
                                }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects: yesAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:KAlertNoButtonTitle],nil];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(KWarningAlertTitleStr, nil) message:NSLocalizedString(@"Do you want to cancel the collection", nil) preferredStyle:UIAlertControllerStyleAlert];
    for (NSInteger i=0; i<actionsArray.count; i++) {
        [alert addAction:[actionsArray objectAtIndex:i]];
    }
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark Currency conversion methods

-(void)convertCurrency:(NSString*)collectedAmountString collectedCurrency:(NSString*)collectedCurrencyString
{
    NSString* currencyRate;
    float currencyDigits;
    
    /** Get selected Currency Rate*/
    for (NSInteger i=0; i<currencyDetailsArray.count; i++){
        NSMutableDictionary* currenctDict=[currencyDetailsArray objectAtIndex:i];
        if ([[currenctDict valueForKey:@"CCode"]isEqualToString:collectedCurrencyString]){
            currencyRate=[currenctDict valueForKey:@"CRate"];
            currencyDigits=[[currenctDict valueForKey:@"CDigits"] floatValue];
        }
    }
    
    NSLog(@"currency details %@ %@", collectedAmountString,currencyRate);
    
    /** Get Amount selected Currency Rate*/
    NSDecimalNumber * tempCurrencyRate=[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.4f",[currencyRate doubleValue]]];
    customerPaymentDetails.customerPaidAmountAfterConversion=[customerPaymentDetails.customerPaidAmount decimalNumberByMultiplyingBy:tempCurrencyRate];
    [collectionInfoDictionary setValue:[NSString stringWithFormat:@"%0.2f",collectedAmount] forKey:@"convertedAmount"];
}
-(void)updateAmountLabels{
    unsettledLbl.text=[unsettledAmount.stringValue currencyString];
    newUnsettledLbl.text=[unsettledAmount.stringValue currencyString];
    newBalanceLbl.text=[customerPaymentDetails.totalAmount.stringValue currencyString];
   // balanceLbl.text=[customerPaymentDetails.customerDueAmount.stringValue currencyString];
    balanceLbl.text=[customerPaymentDetails.totalAmount.stringValue currencyString];

   // TotalDueAmountLabel.text=[customerPaymentDetails.customerDueAmount.stringValue currencyString];
}
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [UIView setAnimationsEnabled:YES];
    UITextField *txfSearchField = [[UITextField alloc]init];
    //check if searchbar field is first responder
    if (@available(iOS 13, *)) {
         txfSearchField = customerSearchBar.searchTextField;
        
    }else{
        txfSearchField = [customerSearchBar valueForKey:@"_searchField"];
    }

    
    if ([txfSearchField isFirstResponder] && indexPath.row==0) {
        
    }
    else
    {
        [self.view endEditing:YES];
    }
    
    if (tableView==customerListTableView)
    {
        
        collectionImagesFromDelegate=[[NSMutableArray alloc]init];
        [self hideNoSelectionView];
        
        if ([_amountTxtFld.text floatValue] >0)
        {
            [self showPaymentCollectionCancellationConfirmationalert];
            tempCustomerIndexPath = indexPath;
            return;
        } else{
            
            if (isSearching==YES) {
                selectedCustomer=[tempCustomerListArray objectAtIndex:indexPath.row];
            }else{
                selectedCustomer=[customerListArray objectAtIndex:indexPath.row];
            }
            
            if ([[SWDefaults getValidStringValue:[selectedCustomer valueForKey:@"Cust_Status"]] isEqualToString:@"Y"]) {
                [imageViewCustomerStatus setBackgroundColor:CustomerActiveColor];
            }else {
                [imageViewCustomerStatus setBackgroundColor:[UIColor redColor]];
            }
            
            signatureTouchesBegan = NO;
            indexPathArray=[[NSMutableArray alloc]initWithObjects:indexPath, nil];

            [self refreshTheCustomerCollectionDetailsView];
            
            
            MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
            cell.titleLbl.textColor=[UIColor whiteColor];
            [cell.descLbl setTextColor:[UIColor whiteColor]];
            cell.cellDividerImg.hidden=YES;
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
            [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        }
    }
    if (tableView==invoicesTableView)
    {
        if (signatureTouchesBegan == YES)
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionAfterSignatureSavedAlertTitleStr andMessage:KPaymentCollectionAfterSignatureSavedAlertViewMessageStr withController:self];

        } else {
            
            #if DEBUG
           // appCtrl.DISABLE_COLL_INV_SETTLEMENT = KAppControlsYESCode;
            #endif
            
            
            if(self.amountTxtFld.text.length>0 && [appCtrl.DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsNOCode])
            {
                SalesWorxPaymentCollectionInvoicesTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                selectedInvoiceIndexPath=indexPath;
                
                CustomerPaymentInvoiceClass *invoice=[customerPaymentDetails.invoicesArray objectAtIndex:indexPath.row];
                if(invoice.settledAmountForInvoice==nil ||
                   invoice.settledAmountForInvoice.doubleValue==0){
                    /** Settle invoice amount*/
                    if([unsettledAmount isGreaterThanOrEqualToDecimal:invoice.DueAmount]){
                        invoice.settledAmountForInvoice=invoice.DueAmount;
                    }else{
                        invoice.settledAmountForInvoice=unsettledAmount;
                    }
                    unsettledAmount=[unsettledAmount decimalNumberBySubtracting:invoice.settledAmountForInvoice];
                }else{
                    
                }
                cell.settledAmountTxtFld.text=[[NSString stringWithFormat:@"%@",invoice.settledAmountForInvoice] currencyString];
                [self updateAmountLabels];
                
                [customerPaymentDetails.invoicesArray replaceObjectAtIndex:indexPath.row withObject:invoice];
                
                if ([cell canBecomeFirstResponder]) {
                    [cell.settledAmountTxtFld setEnabled:YES];
                    [cell becomeFirstResponder];
                }
                return;
            }else{
                
            }
        }
    }
    else
    {
        if ([pjrSignView.previousSignature superview] || [pjrSignView.previousSignature superview]){
            [pjrSignView.lblSignature removeFromSuperview];
            
            [MedRepDefaults clearDoctorSignature];
            [pjrSignView.previousSignature removeFromSuperview];
        }
        
        [pjrSignView clearSignature];
        
        selectedCustomerIndexPath=indexPath;
        [customerListTableView reloadData];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==invoicesTableView) {
        
        return 44;
    }
    else{
        return 0;
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==invoicesTableView) {
        
        static NSString* identifier=@"invoicesHeaderCell";
        
        SalesWorxPaymentCollectionInvoicesHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentCollectionInvoicesHeaderTableViewCell" owner:nil options:nil] firstObject];
            
        }
        [cell.lblDocumentNo setIsHeader:YES];
        [cell.lblInvoiceDate setIsHeader:YES];
        [cell.lblBalanceAmt setIsHeader:YES];
        [cell.lblPaidAmt setIsHeader:YES];
        [cell.lblSettledAmt setIsHeader:YES];
        
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        [cell.contentView.layer setBorderWidth: 1.0];
        [cell.contentView.layer setMasksToBounds:YES];
        [cell.contentView.layer setBorderColor:kUITableViewSaperatorColor.CGColor];
        return cell;
    }
    else
    {
        return nil;
    }
    
}   // custom view for header. will be adjusted to default or specified header height


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==customerListTableView)
    {
        Customer * currentProduct;
        
        if (isSearching==YES) {
            currentProduct=[tempCustomerListArray objectAtIndex:indexPath.row];
        }
        else{
            currentProduct=[customerListArray objectAtIndex:indexPath.row];
            
        }
        
        static NSString* identifier=@"updatedDesignCell";
        
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
        }
        
        if (isSearching && !NoSelectionView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([indexPathArray containsObject:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
        
        cell.titleLbl.text=[[NSString stringWithFormat:@"%@", [currentProduct valueForKey:@"Customer_Name"]] capitalizedString];
        cell.descLbl.text=[NSString stringWithFormat:@"%@", [currentProduct valueForKey:@"Customer_No"]];
        
        return cell;
    }
    else
    {
        static NSString* identifier=@"invoicesCell";
        
        SalesWorxPaymentCollectionInvoicesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentCollectionInvoicesTableViewCell" owner:nil options:nil] firstObject];
            cell.settledAmountTxtFld.delegate=self;
            cell.settledAmountTxtFld.enabled=NO;
        }
        
        
        CustomerPaymentInvoiceClass *currentInvoice=[customerPaymentDetails.invoicesArray objectAtIndex:indexPath.row];
        cell.documentNumberLabel.text=currentInvoice.invoiceNumber;
        
        cell.invoiceDateLabel.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[NSString stringWithFormat:@"%@",currentInvoice.InvoiceDate]];
        
        
        cell.payedAmountLabel.text=[[[currentInvoice.settledAmountForInvoice decimalNumberByAdding:currentInvoice.previousSettledAmount]stringValue] currencyString];
        
        
        cell.balanceAmountLabel.text=[currentInvoice.dueNetAmount.stringValue currencyString];
        
        if([unsettledLbl.text isEqualToString:@"N/A"])
        {
            cell.settledAmountTxtFld.text=@"";
        }
        else
        {
            cell.settledAmountTxtFld.text=[[NSString stringWithFormat:@"%@",currentInvoice.settledAmountForInvoice] currencyString];
        }
        
        cell.settledAmountTxtFld.tag = indexPath.row;
        
        //        if ([appCtrl.DISABLE_COLL_INV_SETTLEMENT isEqualToString:@"Y"])
        //        {
        //            [cell.settledAmountTxtFld setHidden:YES];
        //        }
        
        return cell;
        
    }
}

- (IBAction)customersSearchTapped:(id)sender
{
    [self closeSplitView];
    isSearching=NO;
    [self hideNoSelectionView];
    
    if (customerListArray.count>0) {
        [customerSearchBar setShowsCancelButton:NO animated:YES];
        customerSearchBar.text=@"";
        _amountTxtFld.text = @"";
        [customerSearchBar resignFirstResponder];
        [self.view endEditing:YES];
        [customerListTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
        
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        // Blurred with UIImage+ImageEffects
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        [self.view addSubview:blurredBgImage];
        
        UIButton* searchBtn=(id)sender;
        
        SalesWorxPaymentCollectionFilterViewController * popOverVC=[[SalesWorxPaymentCollectionFilterViewController alloc]init];
        popOverVC.paymentFilterDelegate=self;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict = previousFilteredParameters;
            
        }
        popOverVC.customerListArray = unfilteredProductsArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 370) animated:YES];
        CGRect btnFrame=searchBtn.frame;
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        btnFrame.origin.x=btnFrame.origin.x+3;
        
        CGRect relativeFrame = [customersSearchButton convertRect:customersSearchButton.bounds toView:self.view];
        
        
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
        
        //        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:nil cancelButtonTitle:KAlertOkButtonTitle otherButtonTitles: nil];
        //        [noFilterAlert show];
    }
}
-(void)closeSplitView
{
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
}
-(void)deselectPreviousCellandSelectFirstCell
{
    if ([customerListTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[customerListTableView cellForRowAtIndexPath:selectedCustomerIndexPath];
        
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [customerListTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [customerListTableView.delegate tableView:customerListTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
}
-(void)filteredProducts:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [customersSearchButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    customerListArray = filteredArray;
    [customerListTableView reloadData];
    NSLog(@"customers array count in filter %lu",(unsigned long)customerListArray.count);
    [self deselectPreviousCellandSelectFirstCell];
}
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    previousFilteredParameters=parametersDict;
}
-(void)productsFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [customersSearchButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    customerListArray = unfilteredProductsArray;
    [customerListTableView reloadData];
    [self deselectPreviousCellandSelectFirstCell];
    
}
-(void)productsFilterDidClose
{
    [blurredBgImage removeFromSuperview];
}

#pragma mark UIPopOver delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    if (popoverController == filterPopOverController || isStatementPopUpIsOpen) {
        return NO;
    }
    else
    {
        leftBtn.enabled = YES;
        return YES;
    }
}
-(BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    return NO;
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    [customerSearchBar setShowsCancelButton:NO animated:YES];
}

-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (IBAction)paymentMethodSegmentedControllerValueChanged:(UISegmentedControl *)sender
{
    if (signatureTouchesBegan == YES)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionAfterSignatureSavedAlertTitleStr andMessage:KPaymentCollectionAfterSignatureSavedAlertViewMessageStr withController:self];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Changes are not allowed after the signature is saved" delegate:nil cancelButtonTitle:KAlertOkButtonTitle otherButtonTitles:nil];
        //        [alert show];
    }
    else
    {
        NSLog(@"%ld",(long)sender.tag);
        
        NSString* paymentMethod=[NSString stringWithFormat:@"%@",[paymentMethodsArray objectAtIndex:[sender selectedSegmentIndex]]];
        [collectionInfoDictionary setValue:paymentMethod forKey:@"collectionType"];
        customerPaymentDetails.paymentMethod=paymentMethod;
        
        if([customerPaymentDetails.paymentMethod isEqualToString:@"Cash"])
        {
            bankTextField.text=@"";
            chequeNumberTxtFld.text=@"";
            branchTxtFld.text=@"";
            [chequeDateTextField setText:@""];
            
            bankTextField.enabled = NO;
            bankTextField.alpha = 0.5;
            
            chequeNumberTxtFld.enabled = NO;
            chequeNumberTxtFld.alpha = 0.5;
            
            branchTxtFld.enabled = NO;
            branchTxtFld.alpha = 0.5;
            
            chequeDateButton.enabled = NO;
            chequeDateButton.alpha = 0.5;
            
            chequeDateTextField.enabled = NO;
            chequeDateTextField.alpha = 0.5;
            
            customerPaymentDetails.bankBranchName=@"";
            customerPaymentDetails.bankName=@"";
            customerPaymentDetails.chequeDate=@"";
            customerPaymentDetails.chequeNumber=@"";
            
        }
        else if([customerPaymentDetails.paymentMethod.lowercaseString isEqualToString:@"current cheque"])
        {
            [self enableTextFieldsAndButtons];
            
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [myDateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
            NSString* todayDateString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:[NSDate date]]];
            [self didSelectDate:todayDateString];
            
            //setiing current date for current cheque
            
            NSString* refinedDate=[MedRepQueries fetchDatabaseDateFormat];
            
            
            NSString * refinedDatetoDisplay=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"MMM dd, yyyy" scrString:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
            
            
            [chequeDateTextField setText:refinedDatetoDisplay];
            customerPaymentDetails.chequeDate=refinedDate;
            [collectionInfoDictionary setValue:refinedDate forKey:@"Cheque_Date"];
        }
        else if([customerPaymentDetails.paymentMethod.lowercaseString isEqualToString:@"pdc"])
        {
            [self enableTextFieldsAndButtons];
            chequeDateTextField.text=@"";
            customerPaymentDetails.chequeDate=@"";
        }
    }
}
-(void)enableTextFieldsAndButtons
{
    bankTextField.enabled = YES;
    bankTextField.alpha = 1;
    
    chequeNumberTxtFld.enabled = YES;
    chequeNumberTxtFld.alpha = 1;
    
    branchTxtFld.enabled = YES;
    branchTxtFld.alpha = 1;
    
    chequeDateButton.enabled = YES;
    chequeDateButton.alpha = 1;
    
    chequeDateTextField.enabled = YES;
    chequeDateTextField.alpha = 1;
}

#pragma mark PopOver Delegate

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popOverTitle isEqualToString:kPaymentCollectionScreenPaymentMethodPopOverTitle])
    {
        
        NSString* paymentMethod=[NSString stringWithFormat:@"%@",[paymentMethodsArray objectAtIndex:selectedIndexPath.row]];
        
        if(![paymentMethod.lowercaseString isEqualToString:self.paymentTextField.text.lowercaseString]){
            [_paymentTextField setText:paymentMethod];
            [collectionInfoDictionary setValue:paymentMethod forKey:@"collectionType"];
            customerPaymentDetails.paymentMethod=paymentMethod;
            
            collectionImgesCollectionView.hidden = YES;
            if([customerPaymentDetails.paymentMethod isEqualToString:@"Cash"])
            {
                bankTextField.text=@"";
                chequeNumberTxtFld.text=@"";
                branchTxtFld.text=@"";
                [chequeDateTextField setText:@""];
                
                bankTextField.enabled = NO;
                bankTextField.alpha = 0.5;
                
                chequeNumberTxtFld.enabled = NO;
                chequeNumberTxtFld.alpha = 0.5;
                
                branchTxtFld.enabled = NO;
                branchTxtFld.alpha = 0.5;
                
                chequeDateButton.enabled = NO;
                chequeDateButton.alpha = 0.5;
                
                chequeDateTextField.enabled = NO;
                chequeDateTextField.alpha = 0.5;
                
                customerPaymentDetails.bankBranchName=@"";
                customerPaymentDetails.bankName=@"";
                customerPaymentDetails.chequeDate=@"";
                customerPaymentDetails.chequeNumber=@"";
                
                if([appCtrl.ALLOW_EXCESS_CASH_COLLECTION isEqualToString:KAppControlsNOCode] &&
                   self.amountTxtFld.text.length!=0 &&
                   ![[NSDecimalNumber decimalNumberWithString:[self.amountTxtFld.text AmountStringWithOutComas]] isLessThanOrEqualToDecimal:customerPaymentDetails.customerDueAmount]){
                    
                    /** Reset the amount values*/
                    self.amountTxtFld.text=@"";
                    NSString* collectedAmountString=[self.amountTxtFld.text AmountStringWithOutComas];
                    [collectionInfoDictionary setValue:collectedAmountString forKey:@"Amount"];
                    customerPaymentDetails.customerPaidAmount=[NSDecimalNumber zero];
                    customerPaymentDetails.customerPaidAmountAfterConversion=[NSDecimalNumber zero];
                    unsettledAmount=[NSDecimalNumber zero];
                    [customerPaymentDetails.invoicesArray setValue:[NSDecimalNumber zero] forKey:@"settledAmountForInvoice"];
                    [self updateAmountLabels];
                    [invoicesTableView reloadData];
                }
                
            }
            else if([customerPaymentDetails.paymentMethod.lowercaseString isEqualToString:@"current cheque"])
            {
                [self enableTextFieldsAndButtons];
                [self loadOnChequeSelection];
                if ([appCtrl.ENABLE_COLLECTION_IMAGE_CAPTURE isEqualToString:@"Y"]) {
                    collectionImgesCollectionView.hidden = NO;
                }
            }
            else if([customerPaymentDetails.paymentMethod.lowercaseString isEqualToString:@"pdc"])
            {
                [self enableTextFieldsAndButtons];
                chequeDateTextField.text=@"";
                customerPaymentDetails.chequeDate=@"";
                if ([appCtrl.ENABLE_COLLECTION_IMAGE_CAPTURE isEqualToString:@"Y"]) {
                    collectionImgesCollectionView.hidden = NO;
                }
            }
        }

    }
    else if ([popOverTitle isEqualToString:kPaymentCollectionScreenBanksPopOverTitle])
    {
        bankTextField.text=[bankNames objectAtIndex:selectedIndexPath.row];
        [collectionInfoDictionary setValue:bankTextField.text forKey:@"Bank_Name"];

    }
    else if ([popOverTitle isEqualToString:kPaymentCollectionScreenCurrencyPopOverTitle])
    {
        /** Get selectedCurrency Details*/
        NSString* selectedCurrency=[NSString stringWithFormat:@"%@",[[currencyDetailsArray objectAtIndex:selectedIndexPath.row] valueForKey:@"CCode"]];
        
        if(![selectedCurrency isEqualToString:currencyTextField.text]){
            
            /*** Get Currency Details*/
            NSString* currencyRate=[NSString stringWithFormat:@"%@",[[currencyDetailsArray objectAtIndex:selectedIndexPath.row] valueForKey:@"CRate"]];
            [currencyTextField setText:[NSString stringWithFormat:@"%@",selectedCurrency]];
            customerPaymentDetails.currencyType=selectedCurrency;
            customerPaymentDetails.currencyRate=currencyRate;
            
            /** Reset the amount values*/
            self.amountTxtFld.text=@"";
            NSString* collectedAmountString=[self.amountTxtFld.text AmountStringWithOutComas];
            [collectionInfoDictionary setValue:collectedAmountString forKey:@"Amount"];
            [collectionInfoDictionary setValue:selectedCurrency forKey:@"selectedCurrency"];
            [collectionInfoDictionary setValue:currencyRate forKey:@"currencyRate"];
            
            customerPaymentDetails.customerPaidAmount=[NSDecimalNumber zero];
            customerPaymentDetails.customerPaidAmountAfterConversion=[NSDecimalNumber zero];
            unsettledAmount=[NSDecimalNumber zero];
            [customerPaymentDetails.invoicesArray setValue:[NSDecimalNumber zero] forKey:@"settledAmountForInvoice"];

    
            
            if ([appCtrl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"]) {
                //check currency conversion
                NSLog(@"currency string is %@",[[NSString stringWithFormat:@"%@",[currencyArray objectAtIndex:selectedIndexPath.row]] currencyString]);
            }
            [self updateAmountLabels];
            [invoicesTableView reloadData];
        }


    }
}
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath{
    
}
-(void)didDismissPopOverController{
    
}
#pragma mark  IBOutlet Actions



- (IBAction)currencyButtonTapped:(id)sender
{
    if (signatureTouchesBegan == YES)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionAfterSignatureSavedAlertTitleStr andMessage:KPaymentCollectionAfterSignatureSavedAlertViewMessageStr withController:self];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Changes are not allowed after the signature is saved" delegate:nil cancelButtonTitle:KAlertOkButtonTitle otherButtonTitles:nil];
        //        [alert show];
    }
    else
    {
        [self.view endEditing:YES];
        
        currencyDetailsArray=[[SWDatabaseManager retrieveManager] dbGetMultiCurrency];
        
        popOverTitle = kPaymentCollectionScreenCurrencyPopOverTitle;
        [self presentPopoverfor:currencyTextField withTitle:popOverTitle withContent:[currencyDetailsArray valueForKey:@"CCode"]];
    }
}

- (IBAction)paymentButton:(id)sender
{
    if (signatureTouchesBegan == YES)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionAfterSignatureSavedAlertTitleStr andMessage:KPaymentCollectionAfterSignatureSavedAlertViewMessageStr withController:self];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Changes are not allowed after the signature is saved" delegate:nil cancelButtonTitle:KAlertOkButtonTitle otherButtonTitles:nil];
        //        [alert show];
    }
    else
    {
        [self.view endEditing:YES];
        paymentMethodsArray=[[NSMutableArray alloc]initWithObjects:@"Current Cheque",@"Cash",@"PDC", nil];
        
        popOverTitle = kPaymentCollectionScreenPaymentMethodPopOverTitle;
        [self presentPopoverfor:_paymentTextField withTitle:popOverTitle withContent:paymentMethodsArray];
    }
}

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    if([popOverString isEqualToString:kPaymentCollectionScreenBanksPopOverTitle])
    {
        popOverVC.disableSearch=NO;
        
    }
    if([popOverString isEqualToString:kPaymentCollectionScreenPaymentMethodPopOverTitle])
    {
        popOverVC.enableArabicTranslation=YES;
    }
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(266, 273);
    popover.delegate = self;
    popOverVC.titleKey=popOverString;
    popover.sourceView = selectedTextField.rightView;
    popover.sourceRect =selectedTextField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    
    [self presentViewController:nav animated:YES completion:^{
        if(keyboardIsShowing)
        {
            [UIView animateWithDuration:0.2 animations: ^{
                [self.view endEditing:YES];
                
            } completion:^(BOOL finished) {
            }];
        }
    }];
}

#pragma mark Invoice Methods

-(void)fetchInvoicesforCustomer:(NSMutableDictionary*)customerDict
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [leftBtn setEnabled:NO];
    customerListTableView.userInteractionEnabled=NO;
    invoicesTableView.userInteractionEnabled=NO;

        invoicesArray=[[NSMutableArray alloc]init];
        __block NSMutableArray *invoicesObjectsArray=[[NSMutableArray alloc]init];
        customerPaymentDetails.invoicesArray=[[NSMutableArray alloc]init];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
       {
           
           invoicesArray=[[[SWDatabaseManager retrieveManager] dbGetInvoicesOfCustomer:[customerDict stringForKey:@"Customer_ID"] withSiteId:[customerDict stringForKey:@"Site_Use_ID"]andShipCustID:[customerDict stringForKey:@"Customer_ID"] withShipSiteId:[customerDict stringForKey:@"Site_Use_ID"] andIsStatement:YES] mutableCopy];
           
           NSDecimalNumber *TempInvoiceAmount=[NSDecimalNumber zero];
           
           if (![invoicesArray isKindOfClass:[NSString class]]) {
               

           
           for (NSInteger i=0; i<invoicesArray.count; i++)
           {
               CustomerPaymentInvoiceClass *Invoice=[[CustomerPaymentInvoiceClass alloc]init];
               
               NSDictionary *tempInvoiceDic = [invoicesArray objectAtIndex:i];
               
               Invoice.InvoiceDate=[NSString stringWithFormat:@"%@",[tempInvoiceDic valueForKey:@"InvDate"]];
               Invoice.dueNetAmount=[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],[[tempInvoiceDic valueForKey:@"NetAmount"] doubleValue]]];
               Invoice.dueDate=[NSString stringWithFormat:@"%@",[tempInvoiceDic valueForKey:@"DueDate"]];
               Invoice.invoiceNumber=[NSString stringWithFormat:@"%@",[tempInvoiceDic valueForKey:@"Invoice_Ref_No"]];
               Invoice.previousSettledAmount=[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],[[tempInvoiceDic valueForKey:@"PaidAmt"] doubleValue]]];
               Invoice.DueAmount=[Invoice.dueNetAmount decimalNumberBySubtracting:Invoice.previousSettledAmount];
               Invoice.settledAmountForInvoice=[NSDecimalNumber zero];
              TempInvoiceAmount=[TempInvoiceAmount decimalNumberByAdding:Invoice.dueNetAmount];
               
               if([Invoice.DueAmount isGraterThanDecimal:[NSDecimalNumber zero]]){
                   [invoicesObjectsArray addObject:Invoice];
               }
           }
           }
           customerPaymentDetails.totalAmount=TempInvoiceAmount;
           customerPaymentDetails.invoicesArray=invoicesObjectsArray;

           NSString* customerPaidAmountQry = [NSString stringWithFormat:@"select IFNULL(sum(Amount),'0') AS PaidAmount  from TBL_Collection where (Status!='C' AND Status!='R') AND Customer_ID='%@' AND Site_Use_ID='%@'",[customerDict stringForKey:@"Customer_ID"],[customerDict stringForKey:@"Site_Use_ID"]];
           NSLog(@"paod amount qry is %@",customerPaidAmountQry);
           NSMutableArray* customerPaidAmountArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:customerPaidAmountQry];
           if (customerPaidAmountArray.count>0) {
               NSString * customerPaidAmountStr=[SWDefaults getValidStringValue:[[customerPaidAmountArray objectAtIndex:0] valueForKey:@"PaidAmount"]];
               NSDecimalNumber * customerPaidAmount=[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],customerPaidAmountStr.doubleValue]];
               customerPaymentDetails.customerDueAmount = [customerPaymentDetails.totalAmount decimalNumberBySubtracting:customerPaidAmount];
           }
           else
           {
               customerPaymentDetails.customerDueAmount = [NSDecimalNumber decimalNumberWithString:@"0"];
           }
           

           

           dispatch_async(dispatch_get_main_queue(), ^(void){
               /** disabling the uifields*/
               [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
               
               self.view.userInteractionEnabled = YES;
               [leftBtn setEnabled:YES];
               customerListTableView.userInteractionEnabled=YES;
               invoicesTableView.userInteractionEnabled=YES;
               if([appCtrl.DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsYESCode]){
                   newUnSettledAmtLabel.text=@"Due Amount";
                   titleUnsettled.text=@"Due Amount:";
               }
               [self updateAmountLabels];
               if (invoicesArray.count>0) {
                   [invoicesTableView reloadData];

               }
           });
       });
    
}

#pragma mark Signature Methods


- (IBAction)clearSignatureButtonTapped:(id)sender
{
    signatureTouchesBegan = NO;
    if ([pjrSignView.previousSignature superview]){
        [pjrSignView.lblSignature removeFromSuperview];
        
        [MedRepDefaults clearDoctorSignature];
        [pjrSignView.previousSignature removeFromSuperview];
    }
    [pjrSignView clearSignature];
    [self UpdateSignatureViewsForStatus:KEmptyString];
}

- (IBAction)saveSignatureButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *message;
    NSString *title;
    
    double totalSettledAmount=0.0;
    for (NSInteger i=0; i<customerPaymentDetails.invoicesArray.count; i++)
    {
        CustomerPaymentInvoiceClass *invoice = [customerPaymentDetails.invoicesArray objectAtIndex:i];
        
        NSLog(@"%f",[invoice.settledAmountForInvoice doubleValue]);
        totalSettledAmount=totalSettledAmount+[invoice.settledAmountForInvoice doubleValue];
    }
    
    
    NSString *bankName;
    NSString *bankBranch;
    NSString *chequeDate;
    NSString *chequeNumber;
    if(![customerPaymentDetails.paymentMethod isEqualToString:@"Cash"])
    {
        bankName=bankTextField.text;
        bankBranch=branchTxtFld.text;
        chequeDate=[collectionInfoDictionary stringForKey:@"Cheque_Date"];
        chequeNumber=chequeNumberTxtFld.text;
    }
    else
    {
        bankName=@"";
        bankBranch=@"";
        chequeDate=@"";
        chequeNumber=@"";
    }
    
    customerPaymentDetails.bankBranchName=bankBranch;
    customerPaymentDetails.bankName=bankName;
    customerPaymentDetails.chequeDate=chequeDate;
    customerPaymentDetails.chequeNumber=chequeNumber;
    customerPaymentDetails.remarks=remarksTextField.text;
    
    title=KPaymentCollectionMissingDataAlertViewTitleStr;
    
    BOOL isCashPayment=[customerPaymentDetails.paymentMethod isEqualToString:@"Cash"];
    if([customerPaymentDetails.currencyType isEqualToString:@""] || customerPaymentDetails.currencyType.length==0 )
    {
        message=@"Please select currency";
    }
    else  if([customerPaymentDetails.customerPaidAmountAfterConversion.stringValue isEqualToString:@""] || self.amountTxtFld.text.length==0)
    {
        message=@"Please enter amount";
    }
    else if([appCtrl.ENABLE_COLLECTION_REMARKS isEqualToString:KAppControlsYESCode] && ([customerPaymentDetails.remarks isEqualToString:@""] || remarksTextField.text.length==0) && [appCtrl.FS_COLLECTION_REMARKS_MANDATORY isEqualToString:KAppControlsYESCode])
    {
        message=@"Please enter remarks";
    }
    else  if(!isCashPayment && ([customerPaymentDetails.chequeNumber isEqualToString:@""] || chequeNumberTxtFld.text.length==0))
    {
        message=@"Please enter cheque number";
    }
    else if(!isCashPayment && ([customerPaymentDetails.chequeDate isEqualToString:@""] || self.chequeDateTextField.text.length==0))
    {
        message=@"Please enter cheque date";
    }
    else if(!isCashPayment && ([customerPaymentDetails.bankName isEqualToString:@""] || bankTextField.text.length==0))
    {
        message=@"Please enter bank name";
    }
    else if(!isCashPayment && ([customerPaymentDetails.bankBranchName isEqualToString:@""] || branchTxtFld.text.length==0) && [appCtrl.IS_COLLECTIONS_BRANCH_NAME_OPTIONAL isEqualToString:KAppControlsNOCode])
    {
        message=@"Please enter bank branch name";
    }
    else if (![self isUerSettletheAmountAgainstInvoices])
    {
        NSLog(@"%f",[customerPaymentDetails.customerPaidAmountAfterConversion doubleValue]);
        message=settlementMessage;
        title=KPaymentCollectionErrorAlertTitlestr;
    }
    else
    {
        BOOL touchesBegan=[pjrSignView touchesBegan];
        if (touchesBegan==YES)
        {
            signatureTouchesBegan = YES;
            message=/*@"Signature saved successfully"*/@"";
            title=/*@"Success"*/@"";
            [self UpdateSignatureViewsForStatus:KSavedString];
            
        }
        else
        {
            message=@"Please provide signature";
        }
    }
    if(message.length>0)
    {
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
            //Step 1: Create a UIAlertController
            UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(title,nil)
                                                                                       message:NSLocalizedString(message, nil)
                                                                                preferredStyle:UIAlertControllerStyleAlert                   ];
            //Step 2: Create a UIAlertAction that can be added to the alert
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     //Do some thing here, eg dismiss the alertwindow
                                     [myAlertController dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            //Step 3: Add the UIAlertAction ok that we just created to our AlertController
            [myAlertController addAction: ok];
            
            [[[[UIApplication sharedApplication]keyWindow]rootViewController]presentViewController:myAlertController animated:YES completion:^{
                
            }];
            
        }
        
        else
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
            [SWDefaults showAlertAfterHidingKeyBoard:title andMessage:message withController:self];
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:KAlertOkButtonTitle otherButtonTitles:nil];
            //            [alert show];
#pragma clang diagnostic pop
        }
    }
}

- (IBAction)chequeDateButtonTapped:(id)sender
{
    if (signatureTouchesBegan == YES)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionAfterSignatureSavedAlertTitleStr andMessage:KPaymentCollectionAfterSignatureSavedAlertViewMessageStr withController:self];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Changes are not allowed after the signature is saved" delegate:nil cancelButtonTitle:KAlertOkButtonTitle otherButtonTitles:nil];
        //        [alert show];
    }
    else
    {
        [self.view endEditing:YES];
        if([customerPaymentDetails.paymentMethod.lowercaseString isEqualToString:@"current cheque"])
        {
            popOverTitle = kPaymentCollectionScreenChequeDatePopOverTitle;
            
            SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
            popOverVC.didSelectDateDelegate=self;
            popOverVC.titleString = @"Cheque";
            if ([appCtrl.DISABLE_PAST_DATE_FOR_CHECK isEqualToString:@"Y"]) {
                popOverVC.setMinimumDateCurrentDate = YES;
            }else{
                popOverVC.dateRange = appCtrl.CURR_CHQ_MIN_DAYS;
                
            }
            
            popOverVC.previousSelectedDate = customerPaymentDetails.chequeDate;
            
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
            nav.modalPresentationStyle = UIModalPresentationPopover;
            UIPopoverPresentationController *popover = nav.popoverPresentationController;
            popover.backgroundColor=[UIColor whiteColor];
            popOverVC.preferredContentSize = CGSizeMake(266, 220);
            popover.delegate = self;
            popover.sourceView = chequeDateTextField.rightView;
            popover.sourceRect =chequeDateTextField.dropdownImageView.frame;
            popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
            [self presentViewController:nav animated:YES completion:^{
                if(keyboardIsShowing)
                {
                    [UIView animateWithDuration:0.2 animations: ^{
                        [self.view endEditing:YES];
                        
                    } completion:^(BOOL finished) {
                    }];
                }
            }];
        }
        else
        {
            popOverTitle = kPaymentCollectionScreenChequeDatePopOverTitle;
            
            SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
            popOverVC.didSelectDateDelegate=self;
            popOverVC.titleString = @"PDC";
            
            if (chequeDateTextField.text == nil || [chequeDateTextField.text isEqualToString:@""])
            {
                popOverVC.setMinimumDateCurrentDate=YES;
            } else {
                popOverVC.setMinimumDateCurrentDate=NO;
                popOverVC.previousSelectedDate = customerPaymentDetails.chequeDate;
            }
            
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
            nav.modalPresentationStyle = UIModalPresentationPopover;
            UIPopoverPresentationController *popover = nav.popoverPresentationController;
            popover.backgroundColor=[UIColor whiteColor];
            popOverVC.preferredContentSize = CGSizeMake(266, 220);
            popover.delegate = self;
            popover.sourceView = chequeDateTextField.rightView;
            popover.sourceRect =chequeDateTextField.dropdownImageView.frame;
            popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
            [self presentViewController:nav animated:YES completion:^{
                if(keyboardIsShowing)
                {
                    [UIView animateWithDuration:0.2 animations: ^{
                        [self.view endEditing:YES];
                        
                    } completion:^(BOOL finished) {
                    }];
                }
            }];
        }
    }
}
-(IBAction)BankDropDownTapped:(id)sender
{
    if (signatureTouchesBegan == YES)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionAfterSignatureSavedAlertTitleStr andMessage:KPaymentCollectionAfterSignatureSavedAlertViewMessageStr withController:self];
    }
    else
    {
        popOverTitle = kPaymentCollectionScreenBanksPopOverTitle;
        [self presentPopoverfor:bankTextField withTitle:popOverTitle withContent:bankNames];
        
    }
}
-(void)didSelectDate:(NSString *)selectedDate
{
    //refine date
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
    [chequeDateTextField setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:kDateFormatWithoutTime scrString:selectedDate]];
    
    customerPaymentDetails.chequeDate=refinedDate;
    [collectionInfoDictionary setValue:refinedDate forKey:@"Cheque_Date"];
}

#pragma mark Search Delegate Methods

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"CROSS BUTTON TAPPED");
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Cancel clicked");
    [self.view endEditing:YES];
    isSearching=NO;
    [self hideNoSelectionView];
    
    customerSearchBar.text=@"";
    _amountTxtFld.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
    
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    
    if (customerListArray.count>0) {
        [customerListTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    _amountTxtFld.text = @"";
    [self closeSplitView];
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    _amountTxtFld.text = @"";
    NSLog(@"check search string length %lu", (unsigned long)searchText.length);
    
    if([searchText length] != 0)
    {
        [self showNoSelectionViewWithMessage];
        
        isSearching = YES;
        [self searchContent];
    }
    else {
        
        [self hideNoSelectionView];
        isSearching = NO;
        if (customerListArray.count>0)
        {
            [customerListTableView reloadData];
            [customerListTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            [customerListTableView.delegate tableView:customerListTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    _amountTxtFld.text = @"";
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(void)searchContent
{
    _amountTxtFld.text = @"";
    NSString *searchString = customerSearchBar.text;
    NSLog(@"searching with text in payment collection %@",searchString);

    tempCustomerListArray = [[customerListArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Customer_Name contains[c] %@",searchString]]copy];
    
    NSLog(@"filtered Products count %lu", (unsigned long)tempCustomerListArray.count);
    if (tempCustomerListArray.count>0)
    {
        [customerListTableView reloadData];
        //        [customerListTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        //        [customerListTableView.delegate tableView:customerListTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
    }
    else
    {
        isSearching=YES;
        [customerListTableView reloadData];
    }
}

#pragma mark Database insert method

-(void)insertDatatoDatabase
{
    //validate data
    
    BOOL status=[[SWDatabaseManager retrieveManager]saveCollection:customerPaymentDetails withCustomerInfo:selectedCustomer :[pjrSignView getSignatureImage]:collectionImagesFromDelegate];
    
    if (status==YES) {
        NSLog(@"COLLECTION INSERT SUCCESSFULL");
        
        for (NSInteger i=0; i<invoicesArray.count; i++) {
            
            NSIndexPath * currentIndexPath=[NSIndexPath indexPathForRow:i inSection:0];
            
            
            SalesWorxPaymentCollectionInvoicesTableViewCell *cell = [invoicesTableView cellForRowAtIndexPath:currentIndexPath];
            
            cell.settledAmountTxtFld.text=@"";
        }
        if(!shouldPresentCustomerList)
        {
            NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kCollectionTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                                object:self
                                                              userInfo:visitOptionDict];
            
        }
        if(collectionImagesFromDelegate.count>0)
        {
            [SWDefaults copyFilesFromCollectionImagesTempFolder:[SWDefaults getCollectionImagesFolderPath] withPrefixName:[SWDefaults lastCollectionReference]];
            
            [SWDefaults copyFilesFromCollectionReceiptImagesTempFolder:[SWDefaults getCollectionReceiptImagesFolderPath] withPrefixName:[SWDefaults lastCollectionReference]];
            
        }
        
        
        [SWDefaults updateGoogleAnalyticsEvent:kPaymentCapturedEventName];
        
        UIAlertAction * showReceipt = [UIAlertAction actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            if ([MFMailComposeViewController canSendMail]) {
                
               // [self fetchInvoicesforCustomer:selectedCustomer];

                [self generateReceiptPDFDocument];
                
            }
            else
            {
            [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
            }
            [self refreshTheCustomerCollectionDetailsView];

        }];
        UIAlertAction *noReceipt = [UIAlertAction actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self refreshTheCustomerCollectionDetailsView];

        }];
        
        NSMutableArray* actionsArray=[[NSMutableArray alloc]initWithObjects:showReceipt,noReceipt, nil];
        if([appCtrl.ENABLE_COLLECTION_RECEIPT_PDF_EMAIL isEqualToString:KAppControlsYESCode]){
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Data saved successfully" andMessage:@"Would you like to send a receipt via email?" andActions:actionsArray withController:self];
        }else{
            [SWDefaults showAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Data saved successfully" withController:self];
            [self refreshTheCustomerCollectionDetailsView];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Data insertion failed" withController:self];
        NSLog(@"COLLECTION INSERT FAILED");
    }
}
-(void)loadOnChequeSelection
{
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
    [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [myDateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    NSString* todayDateString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:[NSDate date]]];
    [self didSelectDate:todayDateString];
    
    //setiing current date for current cheque
    
    NSString* refinedDate=[MedRepQueries fetchDatabaseDateFormat];
    
    [chequeDateTextField setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]];
    
    customerPaymentDetails.chequeDate=refinedDate;
    [collectionInfoDictionary setValue:refinedDate forKey:@"Cheque_Date"];
}
-(void)refreshTheCustomerCollectionDetailsView
{
    unsettledAmount=[NSDecimalNumber zero];
    [SWDefaults deleteCollectionTemporaryImages];
    [SWDefaults deleteCollectionReceiptTemporaryImages];
    
    collectionImgasesArray = [[NSMutableArray alloc]init];
    collectionImagesFromDelegate = [[NSMutableArray alloc]init];
    
    customerPaymentDetails=[[CustomerPaymentClass alloc]init];
    [invoicesTableView reloadData];
    [_paymentTextField setText:@"Current Cheque"];
    if ([appCtrl.ENABLE_COLLECTION_IMAGE_CAPTURE isEqualToString:@"Y"]) {
        collectionImgesCollectionView.hidden = NO;
    }
    [self.amountTxtFld setText:@""];
    [bankTextField setText:@""];
    if([appCtrl.SHOW_COLLECTION_BANK_DROPDOWN isEqualToString:KAppControlsYESCode])
    {
        bankTextField.text = @"N/A";
    }
    [chequeNumberTxtFld setText:@""];
    [remarksTextField setText:@""];
    [branchTxtFld setText:@""];
    unsettledLbl.text=@"N/A";
    newUnsettledLbl.text = unsettledLbl.text;
    
    [self enableTextFieldsAndButtons];
    
    [self loadOnChequeSelection];
    [self loadSelectedCustomerDetails:selectedCustomerIndexPath];
    
    [self refreshCustomerSignatureView];
    
    NSString *defaultCurrency = [[SWDefaults userProfile]valueForKey:@"Currency_Code"];
    if ([appCtrl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
    {
        [self refreshTheCustomerInvoicesView];
        
        [currencyTextField setText:@""];
        currencyButton.userInteractionEnabled = YES;
        currencyTextField.dropdownImageView.image = [UIImage imageNamed:@"Arrow_DropDown"];
        currencyTextField.userInteractionEnabled = YES;
        
        [currencyTextField setText:defaultCurrency];
        currencyDetailsArray=[[SWDatabaseManager retrieveManager] dbGetMultiCurrency];
        NSString *currencyRate;
        for (int i = 0; i<[currencyDetailsArray count]; i++)
        {
            if ([[[currencyDetailsArray objectAtIndex:i]valueForKey:@"CCode"] isEqualToString:defaultCurrency]) {
                currencyRate = [[currencyDetailsArray objectAtIndex:i]valueForKey:@"CRate"];
                break;
            }
        }
        
        customerPaymentDetails.currencyType=defaultCurrency;
        customerPaymentDetails.currencyRate=currencyRate;
        
    }
    else
    {
        [currencyTextField setText:defaultCurrency];
        currencyButton.userInteractionEnabled = NO;
        [currencyTextField setIsReadOnly:YES];
        currencyTextField.dropdownImageView.image = nil;
        
        currencyDetailsArray=[[SWDatabaseManager retrieveManager] dbGetMultiCurrency];
        NSString *currencyRate;
        for (int i = 0; i<[currencyDetailsArray count]; i++)
        {
            if ([[[currencyDetailsArray objectAtIndex:i]valueForKey:@"CCode"] isEqualToString:defaultCurrency]) {
                currencyRate = [[currencyDetailsArray objectAtIndex:i]valueForKey:@"CRate"];
                break;
            }
        }
        
        customerPaymentDetails.currencyType=defaultCurrency;
        customerPaymentDetails.currencyRate=currencyRate;
        
        NSString* collectedAmountString=[self.amountTxtFld.text AmountStringWithOutComas];
        customerPaymentDetails.customerPaidAmount=[NSDecimalNumber ValidDecimalNumberWithString:collectedAmountString];
        
        [collectionInfoDictionary setValue:collectedAmountString forKey:@"Amount"];
        [self convertCurrency:collectedAmountString collectedCurrency:currencyTextField.text];
        
        
        [self fetchInvoicesforCustomer:selectedCustomer];
        
        [collectionInfoDictionary setValue:defaultCurrency forKey:@"selectedCurrency"];
        [collectionInfoDictionary setValue:currencyRate forKey:@"currencyRate"];
    }
    [self updateAmountLabels];
    [collectionImgesCollectionView reloadData];
}

-(void)refreshTheCustomerInvoicesView
{
    balanceLbl.text=@"";
    newBalanceLbl.text=@"";
    [self fetchInvoicesforCustomer:selectedCustomer];
}
-(void)refreshCustomerSignatureView
{
    if ([pjrSignView.previousSignature superview] || [pjrSignView.previousSignature superview]){
        [pjrSignView.lblSignature removeFromSuperview];
        
        [MedRepDefaults clearDoctorSignature];
        [pjrSignView.previousSignature removeFromSuperview];
        
    }
    
    [pjrSignView clearSignature];
    [self UpdateSignatureViewsForStatus:KEmptyString];
}

-(void)hideClearButton
{
    [signatureClearButton setHidden:YES];
}
-(void)showClearButton
{
    [signatureClearButton setHidden:NO];
}
-(void)EnableSaveButton
{
    [signatureSaveButton setUserInteractionEnabled:YES];
}
-(void)disableSaveButton
{
    [signatureSaveButton setUserInteractionEnabled:NO];
}
-(void)saveButtonWithCompleteColor
{
    //[signatureSaveButton setBackgroundColor:[UIColor greenColor]];
    [signatureSaveButton UpdateImage:[UIImage imageNamed:@"Signature_Saved"] WithAnimation:YES];
}
-(void)saveButtonWithUnCompleteColor
{
    //  [signatureSaveButton setBackgroundColor:[UIColor redColor]];
    [signatureSaveButton UpdateImage:[UIImage imageNamed:@"Signature_UnSaved"] WithAnimation:YES];
    
}
-(void)EnableSignatureView
{
    [pjrSignView setUserInteractionEnabled:YES];
    //SignatureSaved
}
-(void)DisableSignatureView
{
    [pjrSignView setUserInteractionEnabled:NO];
}


-(void)UpdateSignatureViewsForStatus:(NSString *)status
{
    if([status isEqualToString:KEmptyString])
    {
        [self hideClearButton];
        [self disableSaveButton];
        [self saveButtonWithUnCompleteColor];
        [self EnableSignatureView];
    }
    else if([status isEqualToString:KUnSavedString])
    {
        [self EnableSaveButton];
        [self saveButtonWithUnCompleteColor];
        [self showClearButton];
    }
    else if([status isEqualToString:KSavedString])
    {
        [self disableSaveButton];
        [self saveButtonWithCompleteColor];
        [self hideClearButton];
        [self DisableSignatureView];
    }
}
-(void)PJRSignatureViewTouchesBegans
{
    [self UpdateSignatureViewsForStatus:KUnSavedString];
}

#pragma mark UICollectionview Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionImgasesArray.count == KMaximumAllowedCollectionImages)
        return  collectionImgasesArray.count;
    
    return  collectionImgasesArray.count+1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(40, 40);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LPOImageCell";
    
    SalesWorxSalesOrderLPOImagesCollectionViewCell *cell = (SalesWorxSalesOrderLPOImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.lpoImageview.layer.sublayers=nil;
    cell.selectedImgView.hidden=YES;
    if((collectionImgasesArray.count>0 &&  ([collectionView numberOfItemsInSection:indexPath.section]-1)!=indexPath.row)||
       (collectionImgasesArray.count == KMaximumAllowedCollectionImages))
    {
        cell.lpoImageview.image=[UIImage imageWithContentsOfFile:[[SWDefaults getCollectionImagesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",[collectionImgasesArray objectAtIndex:indexPath.row]]]];
        
        // Initialization code
        UIGraphicsBeginImageContextWithOptions(cell.lpoImageview.bounds.size, NO, 1.0);
        UIBezierPath *mybezierpath = [UIBezierPath bezierPathWithRoundedRect:cell.lpoImageview.bounds
                                                                cornerRadius:cell.lpoImageview.frame.size.height /2];
        // Add a clip before drawing anything, in the shape of an rounded rect
        [mybezierpath addClip];
        // Draw your image
        [cell.lpoImageview.image drawInRect:cell.lpoImageview.bounds];
        
        // Get the image, here setting the UIImageView image
        cell.lpoImageview.image = UIGraphicsGetImageFromCurrentImageContext();
        
        // Lets forget about that we were drawing
        UIGraphicsEndImageContext();
        
        CAShapeLayer *circle = [CAShapeLayer layer];
        circle.path = mybezierpath.CGPath;
        circle.bounds = CGPathGetBoundingBox(circle.path);
        circle.strokeColor = [UIColor colorWithRed:(54.0/255.0) green:(193.0/255.0) blue:(195.0/255.0) alpha:1].CGColor;
        circle.cornerRadius=cell.lpoImageview.frame.size.height /2;
        circle.fillColor = [UIColor clearColor].CGColor; /*if you just want lines*/
        circle.lineWidth = 3;
        circle.position = CGPointMake(cell.lpoImageview.frame.size.width/2.0, cell.lpoImageview.frame.size.height/2.0);
        circle.anchorPoint = CGPointMake(.5, .5);
        circle.masksToBounds = YES;
        
        [cell.lpoImageview.layer addSublayer:circle];
        
    }
    
    else
    {
        cell.lpoImageview.image=[UIImage imageNamed:@"SalesOrder_AddImage_CameraIcon"];
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if((collectionImgasesArray.count>0 &&  ([collectionView numberOfItemsInSection:indexPath.section]-1)!=indexPath.row) || collectionImgasesArray.count==KMaximumAllowedCollectionImages )
    {
        LPOImagesGalleryCollectionDetailViewController *gallertyViewController=[[LPOImagesGalleryCollectionDetailViewController alloc]initWithNibName:@"LPOImagesGalleryCollectionDetailViewController" bundle:[NSBundle mainBundle]];
        gallertyViewController.view.backgroundColor = KPopUpsBackGroundColor;
        gallertyViewController.modalPresentationStyle = UIModalPresentationCustom;
        NSMutableArray *imagesPathsArray=[[NSMutableArray alloc]init];
        for (NSString * imageId in collectionImgasesArray) {
            [imagesPathsArray addObject:[[SWDefaults getCollectionImagesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",imageId]]];
        }
        
        gallertyViewController.swipImagesUrls=imagesPathsArray;
        gallertyViewController.strTitle = @"Collection Images";
        gallertyViewController.selectedImageIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        gallertyViewController.delegate=self;
        [self.navigationController presentViewController:gallertyViewController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showCameraWithMode:UIImagePickerControllerCameraCaptureModePhoto withDelegate:self];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    if(picker.cameraCaptureMode==UIImagePickerControllerCameraCaptureModePhoto)
    {
        UIImage  *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
        NSString *imageGUID=[NSString createGuid];
        [SWDefaults saveCollectionImage:image withName:imageGUID];
        [collectionImgasesArray addObject:imageGUID];
        [picker dismissViewControllerAnimated:NO completion:^{
            [collectionImgesCollectionView reloadData];
        }];//Or call YES if you want the nice dismissal animation
    }
}

-(void)updateImagesArray:(NSMutableArray*)imagesArray
{
    NSLog(@"imagesArray after Delete%@",imagesArray);
    NSMutableArray *tempLPOImagesArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<imagesArray.count; i++) {
        [tempLPOImagesArray addObject:[[[imagesArray objectAtIndex:i]lastPathComponent]stringByDeletingPathExtension]];
    }
    
    collectionImgasesArray = tempLPOImagesArray;
    [collectionImgesCollectionView reloadData];
}
#pragma mark UITextFieldDelegateMethods
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (signatureTouchesBegan == NO)
    {
        if(textField==self.amountTxtFld){
            if(customerPaymentDetails.customerPaidAmount!=nil && self.amountTxtFld.text.length!=0){
                self.amountTxtFld.text=[self ConvertNSdecimalNumberToFloatNumberString:customerPaymentDetails.customerPaidAmount];
            }
        }else if (textField == remarksTextField){
            [collectionInfoDictionary setValue:textField.text forKey:@"Remarks"];
        }else if (textField==bankTextField){
            [collectionInfoDictionary setValue:textField.text forKey:@"Bank_Name"];
        }else if (textField==branchTxtFld){
            [collectionInfoDictionary setValue:textField.text forKey:@"Bank_Branch"];
        }else if (textField==chequeNumberTxtFld){
            [collectionInfoDictionary setValue:textField.text forKey:@"Cheque_No"];
        }else if (textField == chequeDateTextField){
            
        }else {
            if(![textField isKindOfClass:[MedRepTextField class]]){
                if(textField.text.length!=0 && customerPaymentDetails.invoicesArray.count>0){
                    
                    NSDecimalNumber *settledAmount=[[customerPaymentDetails.invoicesArray objectAtIndex:textField.tag] valueForKey:@"settledAmountForInvoice"];
                    if(settledAmount==nil){
                        
                    }else{
                        textField.text=[settledAmount.stringValue currencyString];
                    }
                }
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    
    if (textField == _amountTxtFld)
    {
        [textField resignFirstResponder];
        if([appCtrl.ENABLE_COLLECTION_REMARKS isEqualToString:KAppControlsYESCode])
        {
            [remarksTextField becomeFirstResponder];
        } else {
            [chequeNumberTxtFld becomeFirstResponder];
        }
    }
    else if (textField == remarksTextField)
    {
        [textField resignFirstResponder];
        [chequeNumberTxtFld becomeFirstResponder];
    }
    else if (textField == chequeNumberTxtFld)
    {
        [textField resignFirstResponder];
        [bankTextField becomeFirstResponder];
    }
    else if (textField == bankTextField)
    {
        [textField resignFirstResponder];
        [branchTxtFld becomeFirstResponder];
    }
    else if (textField == branchTxtFld)
    {
        [textField resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if( [string length] == 0 &&
       ( textField!=self.amountTxtFld &&
        [textField isKindOfClass:[MedRepTextField class]]))
    {
        if([textField.text length] != 0){
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if(textField==self.amountTxtFld){
        if(newString.length==0){
            newString=@"0";
        }
        BOOL isValidAmount=YES;
        NSString *expression =[NSString stringWithFormat:@"^[0-9]*((\\.|,)[0-9]{0,%0.0f})?$",[[[SWDefaults userProfile]valueForKey:@"Decimal_Digits"] doubleValue]];
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        
        if(numberOfMatches != 0 && newString.doubleValue<KCollectionMaximumAmount &&
           [customerPaymentDetails.paymentMethod isEqualToString:@"Cash"] &&
           [appCtrl.ALLOW_EXCESS_CASH_COLLECTION isEqualToString:KAppControlsNOCode] &&
           ![[NSDecimalNumber decimalNumberWithString:[newString AmountStringWithOutComas]] isLessThanOrEqualToDecimal:customerPaymentDetails.customerDueAmount]){
            isValidAmount=NO;
            [SWDefaults ShowToastMessage:[NSString stringWithFormat:@"Collection cash amount can not be greater than outstanding amount (%@)",[customerPaymentDetails.customerDueAmount.stringValue currencyString]]];
        }else if(numberOfMatches != 0 && newString.doubleValue<KCollectionMaximumAmount){
            NSDecimalNumber * enteredAmount=[NSDecimalNumber decimalNumberWithString:[newString AmountStringWithOutComas]];
            NSDecimalNumber * currencyRate=[NSDecimalNumber decimalNumberWithString:customerPaymentDetails.currencyRate];
            NSDecimalNumber * AmountInBaseCurrency=[enteredAmount decimalNumberByMultiplyingBy:currencyRate];
            
            customerPaymentDetails.customerPaidAmount=enteredAmount;
            customerPaymentDetails.customerPaidAmountAfterConversion=[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.*f",[self getUserProfileNumberOfDecimalsStr],AmountInBaseCurrency.stringValue.doubleValue]];
            unsettledAmount=customerPaymentDetails.customerPaidAmountAfterConversion;
            [customerPaymentDetails.invoicesArray setValue:[NSDecimalNumber zero] forKey:@"settledAmountForInvoice"];
            [invoicesTableView reloadData];
            isValidAmount=YES;
        }else{
            isValidAmount=NO;
        }
        
        
        if(isValidAmount){
           NSString* collectedAmountString=[self.amountTxtFld.text AmountStringWithOutComas];
            [collectionInfoDictionary setValue:collectedAmountString forKey:@"Amount"];
        }
        
        [self updateAmountLabels];
        return (numberOfMatches != 0 && newString.doubleValue<KCollectionMaximumAmount && isValidAmount);
    }
    else if(textField==remarksTextField)
    {
        return newString.length<500;
    }
    else if(textField==chequeNumberTxtFld)
    {
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<21);
    }
    else if(textField==bankTextField)
    {
        NSString *expression = @"^[a-zA-Z ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<50);
        
    }
    else if(textField==branchTxtFld)
    {
        NSString *expression = @"^[a-zA-Z0-9 ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<50);
    }else{
        NSInteger textfeildTag=textField.tag;
        CustomerPaymentInvoiceClass *invoice=[customerPaymentDetails.invoicesArray objectAtIndex:textfeildTag];
        
        
        
        BOOL isValidAmount=YES;
        NSString *expression =[NSString stringWithFormat:@"^[0-9]*((\\.|,)[0-9]{0,%0.0f})?$",[[[SWDefaults userProfile]valueForKey:@"Decimal_Digits"] doubleValue]];
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        
        if(numberOfMatches == 0){
            return NO;
        }
        
        if(numberOfMatches != 0 && newString.length<12){
            if(newString.length==0){
                newString=@"0";
            }
            unsettledAmount=[unsettledAmount decimalNumberByAdding:invoice.settledAmountForInvoice];
            if([[NSDecimalNumber decimalNumberWithString:[newString AmountStringWithOutComas]] isLessThanOrEqualToDecimal:unsettledAmount] &&
               [[NSDecimalNumber decimalNumberWithString:[newString AmountStringWithOutComas]] isLessThanOrEqualToDecimal:invoice.DueAmount]){
                invoice.settledAmountForInvoice=[NSDecimalNumber decimalNumberWithString:[newString AmountStringWithOutComas]];
                isValidAmount=YES;
            }else{
                isValidAmount=NO;
                [SWDefaults ShowToastMessage:@"Amount can not be greater than invoice pending amount"];
            }
            unsettledAmount=[unsettledAmount decimalNumberBySubtracting:invoice.settledAmountForInvoice];
        }
        [self updateAmountLabels];
        return (numberOfMatches != 0 && newString.length<12 && isValidAmount);
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    if(textField== _paymentTextField)
    {
        [self paymentButton:textField];
        return NO;
    }
    else if(textField==currencyTextField)
    {
        [self currencyButtonTapped:textField];
        
        return NO;
    }
    else if(textField==chequeDateTextField)
    {
        [self chequeDateButtonTapped:textField];
        
        return NO;
    }
    else if(textField==bankTextField && [appCtrl.SHOW_COLLECTION_BANK_DROPDOWN isEqualToString:KAppControlsYESCode])
    {
        [self BankDropDownTapped:textField];
        
        return NO;
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (signatureTouchesBegan == YES){
        [textField resignFirstResponder];
        [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionAfterSignatureSavedAlertTitleStr andMessage:KPaymentCollectionAfterSignatureSavedAlertViewMessageStr withController:self];
    }
    else
    {
        if (textField==_amountTxtFld) {
            
            if(customerPaymentDetails.customerPaidAmount!=nil && self.amountTxtFld.text.length!=0){
                self.amountTxtFld.text=[self ConvertNSdecimalNumberToEditiableNumberString:customerPaymentDetails.customerPaidAmount];
            }
            
        }else{
            if(![textField isKindOfClass:[MedRepTextField class]]){
                
                CustomerPaymentInvoiceClass *invoice = [customerPaymentDetails.invoicesArray objectAtIndex:textField.tag];
                if(invoice.settledAmountForInvoice==nil ||
                   invoice.settledAmountForInvoice.doubleValue==0){
                    /** Settle invoice amount*/
                    if([unsettledAmount isGreaterThanOrEqualToDecimal:invoice.DueAmount]){
                        invoice.settledAmountForInvoice=invoice.DueAmount;
                    }else{
                        invoice.settledAmountForInvoice=unsettledAmount;
                    }
                    unsettledAmount=[unsettledAmount decimalNumberBySubtracting:invoice.settledAmountForInvoice];
                }else{
                    
                }
                textField.text=[self ConvertNSdecimalNumberToEditiableNumberString:invoice.settledAmountForInvoice];
                [self updateAmountLabels];

                if (frameChanged==NO) {
                    
                    //if field is tapped directoy select cell
                    if (self.amountTxtFld.text.length==0) {
                        [self.view endEditing:YES];
                        [SWDefaults showAlertAfterHidingKeyBoard:KPaymentCollectionMissingDataAlertViewTitleStr andMessage:@"Please enter Amount and try again" withController:self];

                    }else{
                        
                        [UIView animateWithDuration:0.2 animations:^{
                            self.view.frame = CGRectOffset(self.view.frame, 0, -220);
                        } completion:^(BOOL finished) {
                            
                        }];
                        frameChanged=YES;
                    }
                }

            }
        }
    }
}
#pragma mark SaveAction
-(void)btnSave
{
    [self.view endEditing:YES];
    

    
    
    
    if ([paymentPopOverController isPopoverVisible] && !isStatementPopUpIsOpen) {
        [paymentPopOverController dismissPopoverAnimated:YES];
        leftBtn.enabled = YES;
    }
    else
    {
        NSString *message;
        NSString *title;
        NSUInteger tag;
        
        double totalSettledAmount=0.0;
        for (NSInteger i=0; i<customerPaymentDetails.invoicesArray.count; i++)
        {
            CustomerPaymentInvoiceClass *invoice = [customerPaymentDetails.invoicesArray objectAtIndex:i];
            
            NSLog(@"%f",[invoice.settledAmountForInvoice doubleValue]);
            totalSettledAmount=totalSettledAmount+[invoice.settledAmountForInvoice doubleValue];
        }
        
        
        NSString *bankName;
        NSString *bankBranch;
        NSString *chequeDate;
        NSString *chequeNumber;
        if(![customerPaymentDetails.paymentMethod isEqualToString:@"Cash"])
        {
            bankName=bankTextField.text;
            bankBranch=branchTxtFld.text;
            chequeDate=[collectionInfoDictionary stringForKey:@"Cheque_Date"];
            chequeNumber=chequeNumberTxtFld.text;
        }
        else
        {
            bankName=@"";
            bankBranch=@"";
            chequeDate=@"";
            chequeNumber=@"";
        }
        
        customerPaymentDetails.bankBranchName=bankBranch;
        customerPaymentDetails.bankName=bankName;
        customerPaymentDetails.chequeDate=chequeDate;
        customerPaymentDetails.chequeNumber=chequeNumber;
        customerPaymentDetails.remarks=remarksTextField.text;
        
        title=KPaymentCollectionMissingDataAlertViewTitleStr;
        
        


        
        BOOL isCashPayment=[customerPaymentDetails.paymentMethod isEqualToString:@"Cash"];
        if([customerPaymentDetails.currencyType isEqualToString:@""] || customerPaymentDetails.currencyType.length==0 )
        {
            message=@"Please select currency";
        }
        else  if(customerPaymentDetails.customerPaidAmountAfterConversion.doubleValue==0 || self.amountTxtFld.text.length==0 || [self.amountTxtFld.text doubleValue] == 0)
        {
            message=@"Please enter amount";
        }
        else if([appCtrl.ENABLE_COLLECTION_REMARKS isEqualToString:KAppControlsYESCode] && ([customerPaymentDetails.remarks isEqualToString:@""] || remarksTextField.text.length==0) && [appCtrl.FS_COLLECTION_REMARKS_MANDATORY isEqualToString:KAppControlsYESCode])
        {
            message=@"Please enter remarks";
        }
        else  if(!isCashPayment && ([customerPaymentDetails.chequeNumber isEqualToString:@""] || chequeNumberTxtFld.text.length==0))
        {
            message=@"Please enter cheque number";
        }
        else if(!isCashPayment && ([customerPaymentDetails.chequeDate isEqualToString:@""] || self.chequeDateTextField.text.length==0))
        {
            message=@"Please enter cheque date";
        }
        else if(!isCashPayment && ([customerPaymentDetails.bankName isEqualToString:@""] || bankTextField.text.length==0))
        {
            message=@"Please enter bank name";
        }
        else if(!isCashPayment && ([customerPaymentDetails.bankBranchName isEqualToString:@""] || branchTxtFld.text.length==0) && [appCtrl.IS_COLLECTIONS_BRANCH_NAME_OPTIONAL isEqualToString:KAppControlsNOCode])
        {
            message=@"Please enter bank branch name";
        }
        else if (![self isUerSettletheAmountAgainstInvoices])
        {
            NSLog(@"%f",[customerPaymentDetails.customerPaidAmountAfterConversion doubleValue]);
            message= settlementMessage;
            title=KPaymentCollectionErrorAlertTitlestr;
        }
        
        else
        {
            BOOL touchesBegan=[pjrSignView touchesBegan];
            if (touchesBegan==YES || [appCtrl.IS_COLLECTIONS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsYESCode])
            {
                if (signatureTouchesBegan == YES || [appCtrl.IS_COLLECTIONS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsYESCode])
                {
//                    message=@"Data saved successfully";
//                    title=@"Success";
//                    tag=906;
                    
                    signatureTouchesBegan = NO;
                    [self insertDatatoDatabase];
                } else {
                    message=@"Please save the signature";
                    title=@"Alert";
                }
            }
            else
            {
                message=@"Please provide signature";
            }
        }
        
        if(message.length > 0)
        {
            
           
            
            
            
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
                //Step 1: Create a UIAlertController
                UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(title, nil)
                                                                                           message:NSLocalizedString(message, nil)
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                //Step 2: Create a UIAlertAction that can be added to the alert
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Do some thing here, eg dismiss the alertwindow
                                         [myAlertController dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                //Step 3: Add the UIAlertAction ok that we just created to our AlertController
                [myAlertController addAction: ok];
                
                [[[[UIApplication sharedApplication]keyWindow]rootViewController]presentViewController:myAlertController animated:YES completion:^{
                    
                }];
                
            }
            
            else
            {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title, nil) message:NSLocalizedString(message, nil) delegate:self cancelButtonTitle:NSLocalizedString(KAlertOkButtonTitle, nil) otherButtonTitles:nil];
                alert.tag=tag;
                [alert show];
#pragma clang diagnostic pop
            }
        }
    }
}
-(BOOL)isUerSettletheAmountAgainstInvoices
{
    settlementMessage = @"Please settle the unsettled amount";
    if ([appCtrl.UNALLOC_COLL_MODE isEqualToString:@"R"]) {
        if([appCtrl.DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsYESCode]){
            return YES;
        }else{
            NSDecimalNumber  * invoicesSettledAmount =[customerPaymentDetails.customerPaidAmountAfterConversion decimalNumberBySubtracting:unsettledAmount];
            NSDecimalNumber * invoicesDueAmount=[NSDecimalNumber decimalNumberWithDecimal:[[customerPaymentDetails.invoicesArray valueForKeyPath: @"@sum.DueAmount"] decimalValue]];
            
            if([unsettledAmount isEqualToDecimal:[NSDecimalNumber zero]]){
                return YES;
            }else if([customerPaymentDetails.customerPaidAmountAfterConversion isGraterThanDecimal:customerPaymentDetails.customerDueAmount] && [invoicesSettledAmount isEqualToDecimal:invoicesDueAmount]){
                return YES;
            }
        }
    }
    else {
        if([appCtrl.DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsYESCode]){
            return YES;
        }else{
            if ([appCtrl.COLL_INV_SETTLEMENT_MANDATORY isEqualToString:KAppControlsYESCode]) {
                NSDecimalNumber  * invoicesSettledAmount =[customerPaymentDetails.customerPaidAmountAfterConversion decimalNumberBySubtracting:unsettledAmount];
                if([invoicesSettledAmount isGraterThanDecimal:[NSDecimalNumber zero]]){
                    return YES;
                }
                settlementMessage = @"Please settle amount for any one of invoice";
            }else{
                return YES;
            }
        }
    }
    return NO;
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    customerViewHeightConstraint.constant = 100.0f;
    customerDetailHeightConstraint.constant = 146.0f;
    signatureViewHeightConstraint.constant = 144.0f;
    
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    customerViewTopConstraint.constant = 8;
}
-(void)showNoSelectionViewWithMessage
{
    customerViewHeightConstraint.constant = KZero;
    customerDetailHeightConstraint.constant = KZero;
    signatureViewHeightConstraint.constant = KZero;
    
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    customerViewTopConstraint.constant = 1000;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NumberMethods
-(NSString *)ConvertNSdecimalNumberToEditiableNumberString:(NSDecimalNumber *)decimalNumberObj{
    
    NSString * tempDecimalStr = [NSString stringWithFormat:@"%@",decimalNumberObj];
    NSLog(@"tempDecimalStr %@",tempDecimalStr);
    
    tempDecimalStr = [tempDecimalStr floatString];
    NSLog(@"tempDecimalStr1 %@",tempDecimalStr);
    
    tempDecimalStr=[tempDecimalStr AmountStringWithOutComas];
    
    NSLog(@"tempDecimalStr.integerValue %ld -- tempDecimalStr.floatValue %f",(long)tempDecimalStr.integerValue,tempDecimalStr.floatValue);

    if(tempDecimalStr.integerValue==tempDecimalStr.floatValue){
        return [NSString stringWithFormat:@"%ld",tempDecimalStr.integerValue];
    }
    
    return tempDecimalStr;
}
-(NSString *)ConvertNSdecimalNumberToCurrencyNumberString:(NSDecimalNumber *)decimalNumberObj{
    
    NSString * tempCurrencyStr = [NSString stringWithFormat:@"%@",decimalNumberObj];
    NSLog(@"tempCurrencyStr %@",tempCurrencyStr);
    
    tempCurrencyStr = [tempCurrencyStr currencyString];
    NSLog(@"tempCurrencyStr %@",tempCurrencyStr);
    
    return tempCurrencyStr;
}
-(NSString *)ConvertNSdecimalNumberToFloatNumberString:(NSDecimalNumber *)decimnumalNumberObj{
    
    NSString * tempFloatStr = [NSString stringWithFormat:@"%@",decimnumalNumberObj];
    NSLog(@"tempFloatStr %@",tempFloatStr);
    
    tempFloatStr=[tempFloatStr AmountStringWithOutComas];

    tempFloatStr = [tempFloatStr floatString];
    NSLog(@"tempFloatStr %@",tempFloatStr);
    
    return tempFloatStr;
}
-(int)getUserProfileNumberOfDecimalsStr{
    return [[[SWDefaults userProfile]valueForKey:@"Decimal_Digits"] intValue];
}

#pragma mark Payment receipt methods
-(void)generateReceiptPDFDocument{
    
    NSMutableArray* settledInvoicesforReceiptArray=[[NSMutableArray alloc]init];
    
    settledInvoicesforReceiptArray = [[customerPaymentDetails.invoicesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.settledAmountForInvoice.integerValue > 0"]]mutableCopy];
    
    NSString *documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    // get the image path
    NSString *documentPathComponent=[NSString stringWithFormat:@"Payment Receipt-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]];
    NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
    }
    
    NSInteger numberOfPages = (settledInvoicesforReceiptArray.count%15) == 0 ? (settledInvoicesforReceiptArray.count/15) : (settledInvoicesforReceiptArray.count/15)+1;
    if (numberOfPages == 0) {
        //in case if the invoice settlement is disabled pdf is not getting generated because number of pages is linked to settles invoices count, so if settled invoices count is 0 setting it to 1
        numberOfPages = 1;
    }
    NSMutableData *pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
    for (NSInteger i=0; i< numberOfPages ; i ++) {
        SalesWorxPaymentCollectionReceiptPDFTemplateView * PageTemplate =[[SalesWorxPaymentCollectionReceiptPDFTemplateView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
        PageTemplate.selectedCustomer = selectedCustomer;
        PageTemplate.unfilteredArray=customerPaymentDetails.invoicesArray;
        PageTemplate.collectionInfoDictionary=collectionInfoDictionary;
        PageTemplate.arrCustomersStatement = settledInvoicesforReceiptArray;
        PageTemplate.pageNumber =i+1;
        PageTemplate.customerPaymentDetails=customerPaymentDetails;
        PageTemplate.paymentMethod=customerPaymentDetails.paymentMethod;
        [PageTemplate UpdatePdfPgae];
        
        UIGraphicsBeginPDFPage();
        CGContextRef pdfContext = UIGraphicsGetCurrentContext();
        [PageTemplate.layer renderInContext:pdfContext];
    }
    
    UIGraphicsEndPDFContext();
    /** Saving pdf in documents Directory*/
    //    if(pdfData !=nil && NO == [pdfData writeToFile:filename atomically:YES]) {
    //        [NSException raise:@"PDF Save Failed" format:@"Unable to store image %@", [NSString stringWithFormat:@"CustomerSatement-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]]];
    //    }else{
    //
    //    }
    //emailButton.title = NSLocalizedString(@"Email", nil);
    if ([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        [mailController setSubject:[NSString stringWithFormat:@"Payment Receipt - %@",[selectedCustomer valueForKey:@"Customer_Name"]]];
        [mailController setMessageBody:@"Hi,\n Please find the attached statement." isHTML:NO];
        [mailController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"Payment Receipt-%@.pdf",[selectedCustomer valueForKey:@"Customer_No"]]];
        
        
        /******** Ravinder code, 12 June, 2019, for fix bug: http://ucssolutions.no-ip.biz:3222/ucsbugtracker/edit_bug.aspx?id=9163    *******/
     
      if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
        if ([selectedCustomer valueForKey:@"Customer_ID"] != nil) {
            NSString *appUserEmailAddress = [[SWDatabaseManager retrieveManager] fetchEmailAddressViaCustomerID:[selectedCustomer valueForKey:@"Customer_ID"]];
            [mailController setToRecipients:@[appUserEmailAddress]];
        }
     }
       
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        //[self EnableNavigationBarActions];
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
   
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
