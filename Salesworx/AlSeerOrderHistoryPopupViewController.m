////
////  AlSeerOrderHistoryPopupViewController.m
////  SalesWorx
////
////  Created by Unique Computer Systems on 6/24/15.
////  Copyright (c) 2015 msaad. All rights reserved.
////
//
//#import "AlSeerOrderHistoryPopupViewController.h"
//#import <QuartzCore/QuartzCore.h>
//#import "SWDatabaseManager.h"
//#import "OrderHistoryPopUplTableViewCell.h"
//
//
//@interface AlSeerOrderHistoryPopupViewController ()
//
//@end
//
//@implementation AlSeerOrderHistoryPopupViewController
//
//
//@synthesize salesOrderHistoryTblView,deliveriesLbl,invoicesLbl,tripsLbl,salesworxOrderNumberLbl,purchaseOrderNumberLbl,purchaseOrderDateLbl,salesOrderNumberLbl,purchaseOrderView,orderDateView,orderNumberView,previousOrderBtn,nextOrderBtn,orderHistoryData,salesOrderNumber,noDataAvailableLbl,salesOrderArray,selectedIndex,noDataAvailableImage,footerLabelsView;
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view from its nib.
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//-(void)viewWillAppear:(BOOL)animated
//{
//    
////    noDataAvailableImage=[[UIImageView alloc]initWithFrame:CGRectMake(252, 56, 633, 536)];
////    
////    noDataAvailableImage.image=[UIImage imageNamed:@"noData.png"];
//
//    
//    NSLog(@"sales order number is %@", salesOrderNumber);
//    
//    
//    salesworxOrderNumberLbl.text=salesOrderNumber;
//    
//
//    
//    purchaseOrderView.layer.cornerRadius = 3.5;
//    purchaseOrderView.layer.masksToBounds = YES;
//    purchaseOrderView.layer.borderWidth = 1;
//    purchaseOrderView.layer.borderColor = [[UIColor colorWithRed:(180.0/255.0) green:(216.0/255.0) blue:(240.0/255.0) alpha:1] CGColor];
//    
//    
//    
//    
//    orderNumberView.layer.cornerRadius = 3.5;
//    orderNumberView.layer.masksToBounds = YES;
//    orderNumberView.layer.borderWidth = 1;
//    orderNumberView.layer.borderColor = [[UIColor colorWithRed:(180.0/255.0) green:(216.0/255.0) blue:(240.0/255.0) alpha:1] CGColor];
//    
//    
//    
//    orderDateView.layer.cornerRadius = 3.5;
//    orderDateView.layer.masksToBounds = YES;
//    orderDateView.layer.borderWidth = 1;
//    orderDateView.layer.borderColor = [[UIColor colorWithRed:(180.0/255.0) green:(216.0/255.0) blue:(240.0/255.0) alpha:1] CGColor];
//    
//    
////    salesOrderHistoryTblView.layer.cornerRadius = 3.5;
////    salesOrderHistoryTblView.layer.masksToBounds = YES;
////    salesOrderHistoryTblView.layer.borderWidth = 1;
////    salesOrderHistoryTblView.layer.borderColor = [[UIColor colorWithRed:(238.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:1] CGColor];
//    
//    [self fetchDataforOrder:[[salesOrderArray valueForKey:@"Orig_Sys_Document_Ref"]objectAtIndex:selectedIndex]];
//     
//    
//   
//    
//    
//    
//}
//
//
//-(void)fetchDataforOrder:(NSString*)orderNumber
//
//{
// 
//    
//    orderHistoryData=[SWDatabaseManager fetchOrderHistoryPopupData:orderNumber];
//    
//    if (orderHistoryData.count>0) {
//        
//        [self populateFooterLabels];
//        
//        
//        
//        if (istableViewHidden==NO) {
//            
//            
//            noDataAvailableImage.hidden=YES;
//            
//            footerLabelsView.hidden=NO;
//            
//            
//            
//            
//            salesOrderHistoryTblView.hidden=NO;
//            [salesOrderHistoryTblView reloadData];
//
//        }
//        
//        NSLog(@"order history data in popup is %@", [orderHistoryData description]);
//
//    }
//    
//    else
//    {
//        
//       // noDataAvailableImage.hidden=NO;
//        
//        footerLabelsView.hidden=YES;
//        
//        
//        salesOrderHistoryTblView.hidden=YES;
//        
//        [self populateFooterLabels];
//        
//        
////        if (soAvailablewithNoData==YES) {
////            
////            noDataAvailableImage.image=[UIImage imageNamed:@"orderinProgress.png"];
////        }
////        
////        else
////        {
////            noDataAvailableImage.image=[UIImage imageNamed:@"noData.png"];
////            
////        }
//        
//        deliveriesLbl.text=@"";
//        invoicesLbl.text=@"";
//        tripsLbl.text=@"";
//        
//        purchaseOrderNumberLbl.text=@"";
//        purchaseOrderDateLbl.text=@"";
//        salesOrderNumberLbl.text=@"";
//        
//        //salesOrderNumberLbl.text=@"";
//        
//        
//    }
//    
//
//}
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//
//-(void)populateFooterLabels
//{
//    
//    NSInteger deliveries=0;
//
//    NSInteger Invoices=0;
//    
//    NSInteger Trips=0;
//
//    
//    if (orderHistoryData.count>0) {
//        
//        
//        for (NSInteger i=0; i<orderHistoryData.count; i++) {
//            
//            
//             NSString* deliveryNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Delivery_Number"] objectAtIndex:i]];
//            
//             NSString* invoiceNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Invoice_Number"] objectAtIndex:i]];
//            
//             NSString* tripNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Trip_Number"] objectAtIndex:i]];
//            
//            
//            NSString* soNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"SO_Number"] objectAtIndex:i]];
//
//            
//            if (deliveryNumber.length>1) {
//                
//                deliveries=deliveries+1;
//            }
//            
//            if (invoiceNumber.length>1) {
//                Invoices=Invoices+1;
//                
//            }
//            
//            if (tripNumber.length>1) {
//                
//                Trips=Trips+1;
//            }
//            
//            
//            
//            
//            if (soNumber.length>1 && deliveryNumber.length==1 && invoiceNumber.length==1 && tripNumber.length==1)
//                
//            {
//                
//                salesOrderHistoryTblView.hidden=YES;
//                
//               
//                istableViewHidden=YES;
//                
//                
//                footerLabelsView.hidden=YES;
//                
//                
//                noDataAvailableImage.hidden=NO;
//                
//                
//                
//                noDataAvailableImage.image=[UIImage imageNamed:@"orderinProgress.png"];
//            }
//            
//            
//        else if (soNumber.length==1 && deliveryNumber.length==1 && invoiceNumber.length==1 && tripNumber.length==1)
//            {
//                salesOrderHistoryTblView.hidden=YES;
//                noDataAvailableImage.hidden=NO;
//
//                istableViewHidden=YES;
//
//                footerLabelsView.hidden=YES;
//
//
//                noDataAvailableImage.image=[UIImage imageNamed:@"noData.png"];
//
//            }
//            
//            else
//            {
//                noDataAvailableImage.image=[UIImage imageNamed:@""];
//                istableViewHidden=NO;
//            }
//            
//            
//            
//        }
//        
//        
//        NSLog(@"Deliveries : %d Invoices : %d Transir : %d", deliveries,Invoices,Trips);
//        
//        
//        deliveriesLbl.text=[NSString stringWithFormat:@"%d", deliveries];
//        
//        invoicesLbl.text=[NSString stringWithFormat:@"%d", Invoices];
//
//        tripsLbl.text=[NSString stringWithFormat:@"%d", Trips];
//        
//        salesworxOrderNumberLbl.text=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"SWX_Doc_Number"] objectAtIndex:0]];
//        
//        
//        purchaseOrderNumberLbl.text=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"PO_Number"] objectAtIndex:0]];
//        
//         salesOrderNumberLbl.text=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"SO_Number"] objectAtIndex:0]];
//        
//        
//        NSString* dateString=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"SO_Date"] objectAtIndex:0]];
//        
//        
//        
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//        NSDate *eventDate = [formatter dateFromString:dateString]; // current Tile
//        
//        
//        NSDateFormatter * requiredDateformatter=[[NSDateFormatter alloc]init];
//        
//        [requiredDateformatter setDateFormat:@"dd MMM yyyy"];
//        
//        NSString* refinedDate=[requiredDateformatter stringFromDate:eventDate];
//        
//        
//        NSLog(@"refined date is %@",refinedDate);
//        
//        
//        
//        
//        purchaseOrderDateLbl.text=refinedDate;
//        
//        
//        
//        
//        
//        //check if table is to be hidden, only so number is there without anydats or no data at all
//        
//        
//        
//       
//      
//        
//
//        
//        
//        
//    }
//    
//    
//    else
//    {
//        //no data in array
//         noDataAvailableImage.hidden=NO;
//        noDataAvailableImage.image=[UIImage imageNamed:@"noData.png"];
//
//        
//    }
//}
//
//#pragma mark UITableviewMethod
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    if (orderHistoryData.count>0 && istableViewHidden==NO) {
//        
//        return orderHistoryData.count;
//    }
//    else
//    {
//        
//        
//        return 0;
//    }
//}
//
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//
//{
//    return 60;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 60;
//}
//
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//    
//    if (istableViewHidden==YES) {
//        
//        return nil;
//        
//        
//    }
//    else
//    {
//    
//    static NSString* identifier=@"orderHistoryPopUpCell";
//    
//    OrderHistoryPopUplTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:identifier];
//    
//    if (cell==nil) {
//        
//        cell=[[[NSBundle mainBundle]loadNibNamed:@"OrderHistoryPopUplTableViewCell" owner:self options:nil]firstObject];
//        
//    }
//    cell.deliveryNumberLbl.text=@"Delivery No";
//    
//    cell.tripIdNumberLbl.text=@"Trip ID";
//    
//    cell.statusLbl.textColor=[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(106.0/255.0) alpha:1];
//    
//    
//    cell.statusLbl.text=@"Status";
//    
//    cell.statusBgView.backgroundColor=[UIColor clearColor];
//    
//    
//    cell.invoiceNumberLbl.text= @"Invoice No";
//    
//    
//    
//    
//    
//    return cell;
//    }
//    
//    
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    OrderHistoryPopUplTableViewCell *cell = (OrderHistoryPopUplTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
////
////    
////    [cell.statusLbl setHighlightedTextColor: [UIColor redColor]];
//    
//}
//
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//   
//    
//    salesOrderHistoryTblView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
//    
//    
//    static NSString* identifier=@"orderHistoryPopUpCell";
//    
//    OrderHistoryPopUplTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:identifier];
//    
//    if (cell==nil) {
//        
//        cell=[[[NSBundle mainBundle]loadNibNamed:@"OrderHistoryPopUplTableViewCell" owner:self options:nil]firstObject];
//        
//    }
//    
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//
//    NSString* invoiceNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Invoice_Number"] objectAtIndex:indexPath.row]];
//    
//    NSString* tripNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Trip_Number"] objectAtIndex:indexPath.row]];
//    
//    NSString* deliveryNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Delivery_Number"] objectAtIndex:indexPath.row]];
//    
//    
//    NSString* soNumber=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"SO_Number"] objectAtIndex:indexPath.row]];
//    
//  
//    if (soNumber.length>1 && deliveryNumber.length>1 && invoiceNumber.length>1 && tripNumber.length>1) {
//        
//        cell.statusBgView.backgroundColor=[UIColor colorWithRed:(51.0/255.0) green:(123.0/255.0) blue:(184.0/255.0) alpha:1];
//        
//        cell.statusLbl.text=@"Delivery in Transit";
//
//    }
//    
//    
//    else if (soNumber.length>1 && deliveryNumber.length>1 && invoiceNumber.length==1 && tripNumber.length==1)
//    {
//        
//        
//        cell.statusBgView.backgroundColor=[UIColor colorWithRed:(134.0/255.0) green:(195.0/255.0) blue:(236.0/255.0) alpha:1];
//        
//        cell.statusLbl.text=@"Stock Allocation in Process";
//    }
//    
//    
//    else if (soNumber.length>1 && deliveryNumber.length>1 && invoiceNumber.length>1 && tripNumber.length==1)
//        
//    {
//        
//        cell.statusBgView.backgroundColor=[UIColor colorWithRed:(91.0/255.0) green:(158.0/255.0) blue:(186.0/255.0) alpha:1];
//        
//        cell.statusLbl.text=@"Picking Confirmation";
//    }
//    
//    
//   
//    
//    else
//    {
//        cell.hidden=YES;
//        
//        NSLog(@"cell is hidden values are SO Number: %@  delivery number :%@  invoice number:%@  trip number: %@", soNumber,deliveryNumber,invoiceNumber,tripNumber);
//        
//    }
//
//       
//    
//    
//    if (deliveryNumber.length==1) {
//        
//        cell.deliveryNumberLbl.text=@"";
//        
//        
//    }
//    
//    else
//    {
//        cell.deliveryNumberLbl.text=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Delivery_Number"] objectAtIndex:indexPath.row]];
//        
//    }
//    
//     if (invoiceNumber.length==1)
//    {
//        cell.invoiceNumberLbl.text=@"";
//        
//    }
//    
//    else
//    {
//        cell.invoiceNumberLbl.text=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Invoice_Number"] objectAtIndex:indexPath.row]];
//    }
//    
//    if (tripNumber.length==1) {
//        
//        cell.tripIdNumberLbl.text=@"";
//    }
//    
//    
//    else
//    {
//       cell.tripIdNumberLbl.text=[NSString stringWithFormat:@"%@", [[orderHistoryData valueForKey:@"Trip_Number"] objectAtIndex:indexPath.row]];
//    }
//    
//    
//    
//    
//    
//    
//    
//    //cell.statusLbl.text=
//    
//
//    
//    
//    
//    
//    
//    
//    return cell;
//    
//}
//
//
//-(void)displayStatusImage
//
//{
//    
//    salesOrderHistoryTblView.hidden=YES;
//    
//    footerLabelsView.hidden=YES;
//    
//    
//   
//}
//
//- (IBAction)closeButtonTapped:(id)sender {
//    
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//
//
//
//
//- (IBAction)previousOrderBtnTapped:(id)sender {
//    
//    if (selectedIndex<=0) {
//        
//        
//        nextOrderBtn.enabled=YES;
//
//        previousOrderBtn.enabled=NO;
//        
//    }
//    else
//    {
//
//        if (nextOrderBtn.enabled==NO) {
//            
//            nextOrderBtn.enabled=YES;
//        }
//        
//        
//        selectedIndex=selectedIndex-1;
//        
//        NSLog(@"current decemented index %d", selectedIndex);
//
//        
//    
//        
//        salesworxOrderNumberLbl.text=[[salesOrderArray valueForKey:@"Orig_Sys_Document_Ref"]objectAtIndex:selectedIndex];
//        
//        
//        
//        [self fetchDataforOrder:[[salesOrderArray valueForKey:@"Orig_Sys_Document_Ref"]objectAtIndex:selectedIndex]];
//        
//    }
//
//}
//- (IBAction)nextOrderBtntapped:(id)sender {
//    
//
//    
//    if (selectedIndex>=salesOrderArray.count-1) {
//        
//        nextOrderBtn.enabled=NO;
//        previousOrderBtn.enabled=YES;
//        
//    }
//    else
//    {
//        NSLog(@"sales order array count %d", salesOrderArray.count);
//        
//        
//        if (previousOrderBtn.enabled==NO) {
//            
//            previousOrderBtn.enabled=YES;
//        }
//        
//        NSLog(@"current  index %d", selectedIndex);
//        selectedIndex=selectedIndex+1;
//
//        
//        NSLog(@"current incemented index %d", selectedIndex);
//
//        salesworxOrderNumberLbl.text=[[salesOrderArray valueForKey:@"Orig_Sys_Document_Ref"]objectAtIndex:selectedIndex];
//
//    
//    [self fetchDataforOrder:[[salesOrderArray valueForKey:@"Orig_Sys_Document_Ref"]objectAtIndex:selectedIndex]];
//    }
//
//}
//@end
