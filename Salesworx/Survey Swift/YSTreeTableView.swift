//
//  YSTreeTableView.swift
//  YSTreeTableView
//
//  Created by yaoshuai on 2017/1/10.
//  Copyright © 2017年 ys. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

private let YSTreeTableViewNodeCellID:String = "nodeCell"
private let YSTreeTableViewContentCellID:String = "contentCell"
private let checkBoxCellIdentifier : String = "checkBoxCell"
private let radioButtonCellIdentifier : String = "radioButtonCell"
private let textViewCellIdentifier : String = "textViewCell"
private let signatureCellIdentifier : String = "signatureCell"
private let segmentCellIdentifier : String = "segmentCell"
private let dropDownCellIdentifier : String = "dropDownCell"




private let YSTreeTableViewNodeCellHeight:CGFloat = 50
private let YSTreeTableViewContentCellHeight:CGFloat = 80
var inputActive: UITextView!

protocol YSTreeTableViewDelegate:NSObjectProtocol {
    
    func treeCellClick(node:YSTreeTableViewNode,indexPath:IndexPath)
}

class YSTreeTableView: UITableView,SwiftSignatureDelegate,UITextViewDelegate,UITextFieldDelegate {
    
    var treeDelegate:YSTreeTableViewDelegate?
    var actualVisitID: String = ""
    var activeField: UITextField?

    /// 外部传入的数据源(根节点集合)
    var rootNodes:[YSTreeTableViewNode] = [YSTreeTableViewNode](){
        didSet{
            getExpandNodeArray()
            reloadData()
        }
    }
    

    
    /// 内部使用的数据源(由nodeArray变形而来)
    fileprivate var tempNodeArray:[YSTreeTableViewNode] = [YSTreeTableViewNode]()
    
    /// 点击“展开”，添加的子节点的“索引数组”
    fileprivate var insertIndexPaths:[IndexPath] = [IndexPath]()
    private var insertRow = 0
    
    /// 点击“收缩”，删除的子节点的“索引数组”
    fileprivate var deleteIndexPaths:[IndexPath] = [IndexPath]()
    
    override init(frame:CGRect, style: UITableViewStyle){
        super.init(frame: frame, style: style)
        setupTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTableView()
    }
    
    private func setupTableView(){
        dataSource = self
        delegate = self
        register(UINib(nibName: "SurveySwiftNodeTableViewCell", bundle: nil), forCellReuseIdentifier: YSTreeTableViewNodeCellID)
        register(UINib(nibName: "SurveySwiftContentTableViewCell", bundle: nil), forCellReuseIdentifier: YSTreeTableViewContentCellID)
        register(UINib(nibName: "SurveySwiftCheckBoxTableViewCell", bundle: nil), forCellReuseIdentifier: checkBoxCellIdentifier)
        register(UINib(nibName: "SwiftSurveyRadioButtonTableViewCell", bundle: nil), forCellReuseIdentifier: radioButtonCellIdentifier)
        register(UINib(nibName: "SurveySwiftConfirmationTextViewCell", bundle: nil), forCellReuseIdentifier: textViewCellIdentifier)
        register(UINib(nibName: "SurveySwiftConfirmationSignatureTableViewCell", bundle: nil), forCellReuseIdentifier: signatureCellIdentifier)
        register(UINib(nibName: "SurveySwiftSegmentTableViewCell", bundle: nil), forCellReuseIdentifier: segmentCellIdentifier)
        register(UINib(nibName: "SurveySwiftDropDownTableViewCell", bundle: nil), forCellReuseIdentifier: dropDownCellIdentifier)

        
        estimatedRowHeight = 76
        rowHeight = UITableViewAutomaticDimension
        layer.cornerRadius = 1.0
        layer.borderWidth = 1.0;
        layer.borderColor = UIColor(red: (182.0/255.0), green: (200.0/255.0), blue: (209.0/255.0), alpha: 1).cgColor
        layer.masksToBounds = true
       // KeyboardAvoiding.avoidingView = self
        
//       NotificationCenter.default.addObserver(self, selector: #selector(actionKeyboardDidShow(with:)), name: .UIKeyboardDidShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(actionKeyboardWillHide(with:)), name: .UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(enableScrollingNotification(notification:)), name: NSNotification.Name("DisableScrolling"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enableScrollingNotification(notification:)), name: NSNotification.Name("EnableScrolling"), object: nil)


        

    }
    
    @objc func enableScrollingNotification(notification: NSNotification) {
        self.isScrollEnabled=true
    }
    @objc func disableScrollingNotification(notification: NSNotification) {
        self.isScrollEnabled=false

    }
    func textViewDidEndEditing(_ textView: UITextView) {
    
        let currentNode = tempNodeArray[textView.tag]
        currentNode.remarksText = textView.text
        
/*
            if currentNode.selectedResponseTypeIDs.count>0{
                currentNode.selectedResponseTypeIDs.append(textView.text)
            }else{
                currentNode.selectedResponseTypeIDs = [String]()
                currentNode.selectedResponseTypeIDs.append(textView.text)
            }
            print("selected option in text view is \(currentNode.selectedResponseTypeIDs)")*/
    }
    
    //MARK:- UITextField Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
   

  

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Keyboard Notifications
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }

    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 1000
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let currentNode = tempNodeArray[textField.tag]
        currentNode.remarksText = textField.text ?? ""
        print("current node remakrs in textfield is  \(currentNode.remarksText)")
    }
    
   

    
//     func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
//        return true
//    }
//   
    
   
    /*
    @objc private func actionKeyboardDidShow(with notification: Notification) {
        guard let userInfo = notification.userInfo as? [String: AnyObject],
            let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            else { return }
        
        var currentContentInset = contentInset
        currentContentInset.bottom += keyboardFrame.height
        
        contentInset = currentContentInset
       scrollIndicatorInsets = currentContentInset
        
    }
    
    @objc private func actionKeyboardWillHide(with notification: Notification) {
        guard let userInfo = notification.userInfo as? [String: AnyObject],
            let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            else { return }
        
        var currentContentInset = contentInset
        currentContentInset.bottom -= keyboardFrame.height
        
        contentInset = currentContentInset
        scrollIndicatorInsets = currentContentInset
    }*/
    
    /// 添加本节点及展开的子节点(包括展开的孙子节点)至数组中
    ///
    /// - Parameters:
    ///   - node: 本节点
    private func addExpandNodeToArray(node:YSTreeTableViewNode) -> Void{
        tempNodeArray.append(node)
        
        if node.isExpand{ // 当前节点展开，添加其子节点
            for childNode in node.subNodes{
                addExpandNodeToArray(node: childNode)
            }
        }
    }
    
    /// 获取所有展开的节点数组
    private func getExpandNodeArray() -> (){
        for rootNode in rootNodes{
            if rootNode.parentNode == nil{ // 再次判断是否是根节点
                addExpandNodeToArray(node: rootNode)
            }
        }
    }
    
    /// 是否是叶子节点
    ///
    /// - Parameter node: 节点
    /// - Returns: true-是；false-否
    fileprivate func isLeafNode(node:YSTreeTableViewNode) -> Bool{
        return node.subNodes.count == 0
    }
    
    
    /// 点击“展开”，添加子节点(如果子节点也处于展开状态，添加孙子节点)
    ///
    /// - Parameter node: 节点
    fileprivate func insertChildNode(node:YSTreeTableViewNode){
        node.isExpand = true
        if node.subNodes.count == 0{
            return
        }
        
        insertRow = tempNodeArray.index(of: node)! + 1
        
        for childNode in node.subNodes{
            let childRow = insertRow
            let childIndexPath = IndexPath(row: childRow, section: 0)
            insertIndexPaths.append(childIndexPath)
            
            tempNodeArray.insert(childNode, at: childRow)
            
            insertRow += 1
            if childNode.isExpand{
                insertChildNode(node: childNode)
            }
        }
    }
    
    /// 点击“收缩”，删除所有处于展开状态的子节点的索引
    ///
    /// - Parameter node: 节点
    fileprivate func getDeleteIndexPaths(node:YSTreeTableViewNode){
        if node.isExpand{ // 再次确认节点处于展开状态
            
            for childNode in node.subNodes{
                let childRow = tempNodeArray.index(of: childNode)!
                let childIndexPath = IndexPath(row: childRow, section: 0)
                deleteIndexPaths.append(childIndexPath)
                
                if childNode.isExpand{
                    getDeleteIndexPaths(node: childNode)
                }
            }
        }
    }
    
    /// 点击“收缩”，删除所有处于展开状态的子节点
    ///
    /// - Parameter node: 节点
    fileprivate func deleteChildNode(node:YSTreeTableViewNode){
        getDeleteIndexPaths(node: node)
        
        node.isExpand = false
        
        for _ in deleteIndexPaths{
            tempNodeArray.remove(at: deleteIndexPaths.first!.row)
        }
    }
}

extension YSTreeTableView:UITableViewDataSource,UITableViewDelegate{
    // MARK: - cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempNodeArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let node = tempNodeArray[indexPath.row]
        node.actualVisitID = actualVisitID
        if isLeafNode(node: node){ // 叶子节点，内容cell

            /*1    Textbox
             2    Radio Button
             3    Checkbox
             4    Slider
             5    Signature
             6    Segment
             7    Dropdown*/
            if node.responseDisplayType == "1"{
                let cell:SurveySwiftConfirmationTextViewCell = tableView.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath) as! SurveySwiftConfirmationTextViewCell
                cell.selectionStyle = .none
                cell.txtView.tag = indexPath.row
                cell.node = node
                cell.txtView.delegate = self
                //cell.titleLbl.text = node.nodeName
                return cell
            }else if node.responseDisplayType == "2"{
                let cell:SwiftSurveyRadioButtonTableViewCell = tableView.dequeueReusableCell(withIdentifier: radioButtonCellIdentifier, for: indexPath) as! SwiftSurveyRadioButtonTableViewCell
                cell.selectionStyle = .none
                cell.node = node
                cell.remarksTextField.tag = indexPath.row
                cell.remarksTextField.delegate = self
                
                return cell
            }else if node.responseDisplayType == "3"{
                let cell:SurveySwiftCheckBoxTableViewCell = tableView.dequeueReusableCell(withIdentifier: checkBoxCellIdentifier, for: indexPath) as! SurveySwiftCheckBoxTableViewCell
                cell.selectionStyle = .none

                cell.node = node
                cell.remarksTextField.tag = indexPath.row
                cell.remarksTextField.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if node.responseDisplayType == "4" {
                let cell:SurveySwiftContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: YSTreeTableViewContentCellID, for: indexPath) as! SurveySwiftContentTableViewCell
                cell.selectionStyle = .none
                cell.node = node
                cell.remarksTextField.tag = indexPath.row
                cell.remarksTextField.delegate = self
               // KeyboardAvoiding.avoidingView = cell
                return cell
            }else if node.responseDisplayType == "5" {
                let cell:SurveySwiftConfirmationSignatureTableViewCell = tableView.dequeueReusableCell(withIdentifier: signatureCellIdentifier, for: indexPath) as! SurveySwiftConfirmationSignatureTableViewCell
                cell.node = node
                cell.selectionStyle = .none

                //                cell.titleLbl.text = node.nodeName
                //                cell.signatureView.tag = indexPath.row
                // cell.signatureView.swiftSignatureDelegate = self
                return cell

            }
            else if node.responseDisplayType == "6" {
                let cell:SurveySwiftSegmentTableViewCell = tableView.dequeueReusableCell(withIdentifier: segmentCellIdentifier, for: indexPath) as! SurveySwiftSegmentTableViewCell
                cell.node = node
                cell.selectionStyle = .none

                cell.remarksTextField.tag = indexPath.row
                cell.remarksTextField.delegate = self
                return cell
            }else if node.responseDisplayType == "7" {
                //this is dropdown type question
                let cell:SurveySwiftDropDownTableViewCell = tableView.dequeueReusableCell(withIdentifier: dropDownCellIdentifier, for: indexPath) as! SurveySwiftDropDownTableViewCell
                cell.dropDownTextField.text = ""
                cell.node = node
                cell.selectionStyle = .none
                cell.remarksTextField.tag = indexPath.row
                cell.remarksTextField.delegate = self
                return cell
            }else{
                let cell = UITableViewCell(style: .default, reuseIdentifier: "defaultCell")
                cell.indentationLevel = 3
                cell.indentationWidth = 16
                cell.textLabel?.text = "Response type not set for this question,please contact your system admin"
                return cell
            }
            
           
        } else{ // 节点cell
            //this is question cell
            let cell:SurveySwiftNodeTableViewCell = tableView.dequeueReusableCell(withIdentifier: YSTreeTableViewNodeCellID, for: indexPath) as! SurveySwiftNodeTableViewCell
            cell.node = node
            return cell

        }
    }
    //MARK: - swipe to delete function
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let node = tempNodeArray[indexPath.row]
        if isLeafNode(node: node){ 
        return true
        }else{
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let swipedNode = tempNodeArray[indexPath.row]
            if isLeafNode(node: swipedNode){
                var currentTitle = swipedNode.isNotApplicable
                if currentTitle == "N"{
                    currentTitle = "Y"
                }else{
                    currentTitle = "N"
                }
                swipedNode.isNotApplicable = currentTitle
                
                swipedNode.selectedResponseTypeIDs = [String]()
                if currentTitle == "Y"{
                    swipedNode.selectedResponseTypeIDs = ["0"]
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? { //return the text you want to add here
        let swipedNode = tempNodeArray[indexPath.row]
        if isLeafNode(node: swipedNode){
            if swipedNode.isNotApplicable == "Y"{
                return "Applicable"
            }else{
                return "Not Applicable"
            }
        }else{
            return ""
        }
        }
    
    // MARK: - 高度
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let node = tempNodeArray[indexPath.row]
        
        if isLeafNode(node: node){ // 叶子节点，内容cell
            return UITableViewAutomaticDimension
        } else{ // 节点cell
            return UITableViewAutomaticDimension
        }
    }
    
    
    // MARK: - 点击
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let node = tempNodeArray[indexPath.row]
        treeDelegate?.treeCellClick(node: node, indexPath: indexPath)
        
        if isLeafNode(node: node){ // 叶子节点，内容cell
            return
        } else{ // 节点cell
            if node.isExpand{ // 当前节点展开，要进行收缩操作
                deleteIndexPaths = [IndexPath]()
                deleteChildNode(node: node)
                deleteRows(at: deleteIndexPaths, with: .top)
            }
            else{ // 当前节点收缩，要进行展开操作
                insertIndexPaths = [IndexPath]()
                insertChildNode(node: node)
                insertRows(at: insertIndexPaths, with: .top)
            }
            let cell:SurveySwiftNodeTableViewCell = tableView.cellForRow(at: indexPath) as! SurveySwiftNodeTableViewCell
            cell.arrowImageView.transform = cell.arrowImageView.transform.rotated(by: .pi/1)

        }
        
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        print("touches should cancel \(view)")
        if view.isKind(of: YPDrawSignatureView.self)
        {
            return false
        }
        return super.touchesShouldCancel(in: view)
        
    }
    
    //MARK :- Signature view delegate
    func enableTableViewScrolling(flag:Bool)
    {
        print("enable tableview scrolling called % hhd",flag)
    }
    func didSelectSaveButton(tappedView: SalesWorxSwiftSignatureView) {
        
        let currentNode = tempNodeArray[tappedView.tag]
        if let signatureImage = tappedView.signatureView.getSignature(){
            
            let uuid = NSUUID().uuidString.lowercased()
            // get the documents directory url
            var documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            documentsDirectory = documentsDirectory.appendingPathComponent("Coach Signatures").appendingPathComponent("temp")
         
            
            if FileManager.default.fileExists(atPath: documentsDirectory.relativePath, isDirectory: nil) == false{
                do {
                    try FileManager.default.createDirectory(atPath: documentsDirectory.relativePath, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print("error creating signature directory \(error)")
                }
            }
            // choose a name for your image
            let fileName = "\(uuid).jpg"
            print("file name is \(uuid)")
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            if let data = UIImageJPEGRepresentation(signatureImage, 1.0),
                !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    // writes the image data to disk
                    try data.write(to: fileURL)
                    print("survey signature file saved")
                    currentNode.signatureImageFileName = fileName
                    print("survey signature file saved with file name in table VC\(currentNode.signatureImageFileName)")

                    
                } catch {
                    print("error saving file:", error)
                }
            }
        }
    }
    
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
//                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer)
//        -> Bool {
//
//            print("current recognizer is \(gestureRecognizer) \n other recognizer is \(otherGestureRecognizer)")
//
//            return true
//    }
    
}
