//
//  PopOverListViewController.swift
//  MedRep
//
//  Created by Unique Computer Systems on 8/2/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications

protocol SalesWorxPopOverDelegate : class
{
    func didSelectPopOver(selectedContent:AnyObject)
    func didCancelPopOver()
    func didSelectStringinPopOver(selectedString:String)
    func didSelectTableViewIndexPath(selectedIndex: NSInteger)
}

class PopOverListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var contentTableView: UITableView!
    @IBOutlet var searchBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var listSearchBar: UISearchBar!
    @IBOutlet var todoSearchContainerView: UIView!
    @IBOutlet var todoSearchContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var dateContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var headerView: UIView!
    @IBOutlet var dateFilterContainerView: UIView!
    enum PopOverType {
        case listPopOver,toDoPopOver,defaultPush
    }
    var popOverType : PopOverType = PopOverType.listPopOver
    
   
    var hideSaveButton : Bool = false
    var showDateFilterView : Bool = false

    var tapGesture : UITapGestureRecognizer? = nil
    var hideSearchBar : Bool?
    
    var stringsArray :[String] = []
    var unfilteredStringsArray :[String] = []

    var isStringArray = false
    var isCommingFromRoute = false
    var showDefaultObjectCell = false
    var isVisitInformationPopPover = false
    var isFieldMarketingRoute = false

    weak var delegate : SalesWorxPopOverDelegate?
    var selectedCellObject : AnyObject?
    var selectedDate : Date = Date.init()
    var titleText : String = ""
    var isFMCreateVisit = false
    var isFMCreateLocation = false
    var isDoctorsList = false
    var isFMCreateDoctor = false
    var isSearching = false
    
    
    @IBOutlet var titleLbl: UILabel!
    

    
    override func viewDidLoad() {
        
        
        let closeBarButton = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
        self.navigationItem.rightBarButtonItem = closeBarButton
        setNavigationBarTitleText(text: "Options")

        unfilteredStringsArray = stringsArray
    }
    
    func setNavigationBarTitleText(text: String){
        //        let titleLabel = UILabel(frame: CGRect(0, 0, self.view.frame.size.width - 120, 44))
        let titleLabel:UILabel = UILabel(frame: CGRect.zero)
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont(name: "WeblySleekUISemibold", size: 18)
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = UIColor.white
        titleLabel.text = text.uppercased()
        self.navigationItem.titleView = titleLabel
    }
    
   
    @objc func saveButtonTapped(_ sender:UIBarButtonItem!)
    {
        delegate?.didSelectPopOver(selectedContent: selectedCellObject!)
        self.dismiss(animated: true, completion: nil);
        
    }
    @objc func cancelButtonTapped(_ sender:UIBarButtonItem!)
    {
        self.dismiss(animated: true, completion: nil);
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - TableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringsArray.count

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
                return UITableViewCell(style: .default, reuseIdentifier: "cell")
            }
            return cell
        }()
        cell.textLabel?.font = SalesWorxFont.KSWXRegularFontWithSize(size: 15)
        cell.textLabel?.text = stringsArray[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var matchedString = ""
        listSearchBar.resignFirstResponder()
        if isStringArray {
            matchedString = stringsArray[indexPath.row]
            if let matchedOffset = unfilteredStringsArray.index(where: {$0 == matchedString}) {
                matchedString = unfilteredStringsArray[matchedOffset]
            } else {

            }
            self.delegate?.didSelectStringinPopOver(selectedString: matchedString)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    // MARK: - UISearchBar
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        isSearching = true
        listSearchBar.setShowsCancelButton(true, animated: true)
        self.searchContentwithText(searchString: searchText)
    }
    
    func searchContentwithText(searchString:String)
    {
        
        if searchString == "" {
            stringsArray = unfilteredStringsArray
            contentTableView.reloadData()
        }
        else
        {
            let filterContent = unfilteredStringsArray.filter({$0.lowercased().contains("\(searchString.lowercased())")})
            stringsArray = filterContent
            contentTableView.reloadData()
        }
    }
    
    func reloadTablewithContent(filteredArray:[String]){
        stringsArray = filteredArray
        contentTableView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        stringsArray = unfilteredStringsArray
        listSearchBar.resignFirstResponder()
        listSearchBar.setShowsCancelButton(false, animated: true)
        listSearchBar.text = ""
        contentTableView.reloadData()
        isSearching = false
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
