//
//  SwiftDefaults.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/22/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import Foundation
import UIKit

let questionCellIndentationWidth : CGFloat = 20
let surveyTitleFont = UIFont(name: "WeblySleekUISemibold", size: 16)
let surveyTextColor = UIColor(red: (45.0/255.0), green: (57.0/255.0), blue: (76.0/255.0), alpha: 1)

class CheckBox: UIButton {
    // Images
    let checkedImage = UIImage(named: "SurveyCheckboxSelected")! as UIImage
    let uncheckedImage = UIImage(named: "SurveyCheckboxUnSelected")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControlState.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControlState.normal)
            }
        }
    }
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
   @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}

class RadioButton: UIButton {
    var alternateButton:Array<RadioButton>?
    let checkedImage = UIImage(named: "SurveyRadioButtonSelected")! as UIImage
    let uncheckedImage = UIImage(named: "SurveyRadioButtonUnSelected")! as UIImage
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 2.0
        self.layer.masksToBounds = true
        //self.layer.borderColor = UIColor.blue.cgColor
    }
    
    func unselectAlternateButtons(){
        if alternateButton != nil {
            self.isSelected = true
            
            for aButton:RadioButton in alternateButton! {
                aButton.isSelected = false
            }
        }else{
            toggleButton()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }
    
    func toggleButton(){
        self.isSelected = !isSelected
    }
    override var isSelected: Bool {
        didSet {
            if isSelected {
                setImage(checkedImage, for: .normal)
            } else {
                setImage(uncheckedImage, for: .normal)
            }
        }
    }
}

extension String{
    
    func createGuid()-> String{
        let uuid = NSUUID().uuidString.lowercased()
        return uuid
    }
}

extension UIViewController {
    //MARK: Keyboard user interactions
    func handleContainerViewFrameOnKeyboardShowHide(originalContainerFrame: CGRect) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: OperationQueue.main) {(notification) in
            var info = (notification as NSNotification).userInfo!
            let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            var tableViewFrame = originalContainerFrame
            tableViewFrame.size.height = originalContainerFrame.size.height - keyboardFrame.size.height
            self.view.frame = tableViewFrame
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: OperationQueue.main) { (notification) in
            self.view.frame = originalContainerFrame
        }
    }
}
