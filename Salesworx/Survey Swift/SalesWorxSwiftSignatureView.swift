
//
//  SalesWorxSwiftSignatureView.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/22/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

protocol SwiftSignatureDelegate {
    
    func didSelectSaveButton(tappedView:SalesWorxSwiftSignatureView)
    func enableTableViewScrolling(flag:Bool)
}

class SalesWorxSwiftSignatureView: UIView,YPSignatureDelegate {

    func didStart(_ view : YPDrawSignatureView) {
        NotificationCenter.default.post(name: Notification.Name("DisableScrolling"), object: nil, userInfo: nil)
    }
    func didFinish(_ view : YPDrawSignatureView){
        NotificationCenter.default.post(name: Notification.Name("EnableScrolling"), object: nil, userInfo: nil)
    }

   

    var swiftSignatureDelegate : SwiftSignatureDelegate?
    
    @IBOutlet var saveSignatureButton: UIButton!
    @IBOutlet var clearSignatureButton: UIButton!
    @IBOutlet var signatureView: YPDrawSignatureView!
    
    @IBAction func saveSignatureTapped(_ sender: Any) {
        signatureView.isUserInteractionEnabled=false
        clearSignatureButton.isHidden = true
        saveSignatureButton.setImage(UIImage(named: "Signature_Saved"), for: .normal)
        swiftSignatureDelegate?.didSelectSaveButton(tappedView: self)
    }
    
    
    
  
    
    @IBAction func clearSignatureTapped(_ sender: Any) {
        signatureView.clear()
//        clearSignatureButton.isHidden = true
//        saveSignatureButton.isHidden = true
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        clearSignatureButton.isHidden = false
        saveSignatureButton.isHidden = false
        self.layer.cornerRadius = 5.0
        let currentColor = UIColor(red: (181.0/255.0), green: (202.0/255.0), blue: (202.0/255.0), alpha: 1)
        self.layer.borderWidth=1.0
        self.layer.borderColor = currentColor.cgColor
        signatureView.delegate = self
        

    }
//    func didStart(_ view : YPDrawSignatureView) {
//        clearSignatureButton.isHidden = false
//        saveSignatureButton.isHidden = false
//    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // load view frame XIB
        commonSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // load view frame XIB
        commonSetup()
        
    }
    
    // MARK: - setup view
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        
        //  An exception will be thrown if the xib file with this class name not found,
        let view = bundle.loadNibNamed("SalesWorxSwiftSignatureView", owner: self, options: nil)?.first as? UIView
        return view
    }
    func commonSetup() {
        let nibView: UIView? = loadViewFromNib()
        nibView?.frame = bounds
        // the autoresizingMask will be converted to constraints, the frame will match the parent view frame
        nibView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Adding nibView on the top of our view
        if let aView = nibView {
            addSubview(aView)
        }
    }


}
