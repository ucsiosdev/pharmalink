//
//  SwiftConstants.swift
//  MedRep
//
//  Created by Unique Computer Systems on 1/9/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

import Foundation

struct SalesWorxColors
{
    let KLPOImageRoundMaskColor = UIColor(red: 54.0/255.0, green: 193.0/255.0, blue: 195.0/255.0, alpha: 1.0)
    let kNavigationBarBackgroundColor = UIColor(red: 71.0/255.0, green: 144.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    let kNavigationBarTitleFontColor = UIColor.white
    let kDarkTitleColor = UIColor(red: 45.0/255.0, green: 57.0/255.0, blue: 76.0/255.0, alpha: 1.0)
    let kVisitOptionsDisbaleBorderColor = UIColor(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0)
    let kVisitOptionsBorderColor = UIColor(red: 64.0/255.0, green: 129.0/255.0, blue: 189.0/255.0, alpha: 1.0)
    let kUITableViewHeaderBackgroundColor = UIColor(red: 248.0/255.0, green: 250.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    let kUITableViewSaperatorColor = UIColor(red: 196.0/255.0, green: 204.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    let kUITableViewBorderColor = UIColor(red: 182.0/255.0, green: 200.0/255.0, blue: 209.0/255.0, alpha: 1.0)
    let kUIElementDividerBackgroundColor = UIColor(red: 231.0/255.0, green: 235.0/255.0, blue: 237.0/255.0, alpha: 1.0)
    let UITableviewSelectedCellBackgroundColor = UIColor(red: 81.0/255.0, green: 102.0/255.0, blue: 136.0/255.0, alpha: 1.0)
    let UITableViewTitleDescriptionLabelFontColor = UIColor(red: 89.0/255.0, green: 114.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    let UITableViewTitleLabelFontColor = UIColor(red: 45.0/255.0, green: 57.0/255.0, blue: 76.0/255.0, alpha: 1.0)
    let fontColor = UIColor(red: 91.0/255.0, green: 91.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    let SalesWorxTableViewCellTitleColor = UIColor(red: 45.0/255.0, green: 57.0/255.0, blue: 76.0/255.0, alpha: 1.0)
    let SalesWorxReadOnlyViewColor = UIColor(red: 248.0/255.0, green: 249.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    let SalesWorxReadOnlyTextFieldColor = UIColor(red: 248.0/255.0, green: 249.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    let SalesWorxReadOnlyTextFieldTextColor = UIColor(red: 105.0/255.0, green: 114.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    let SalesWorxReadOnlyTextViewColor = UIColor(red: 248.0/255.0, green: 249.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    let SalesWorxReadOnlyTextViewTextColor = UIColor(red: 105.0/255.0, green: 114.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    let KSalesWorxTitleLabelColor = UIColor(red: 89.0/255.0, green: 114.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    let KSalesWorxFaceTimeColor = UIColor(red: 49.0/255.0, green: 156.0/255.0, blue: 212.0/255.0, alpha: 1.0)
    let KSalesWorxWaitTimeColor = UIColor(red: 252.0/255.0, green: 166.0/255.0, blue: 88.0/255.0, alpha: 1.0)
    let CreditLimitLableColor = UIColor(red: 19.0/255.0, green: 196.0/255.0, blue: 165.0/255.0, alpha: 1.0)
    let KDistributionCheckAvailabilityColor = UIColor(red: 17.0/255.0, green: 176.0/255.0, blue: 101.0/255.0, alpha: 1.0)
    let KDistributionCheckUnAvailabilityColor = UIColor.red
    let KTableViewSectionCollapsedColor = UIColor(red: 5.0/255.0, green: 114.0/255.0, blue: 98.0/255.0, alpha: 1.0)
    let KTableViewSectionExpandColor = UIColor(red: 54.0/255.0, green: 193.0/255.0, blue: 195.0/255.0, alpha: 1.0)
    let KCoachHeaderColor = UIColor(red: 252.0/255.0, green: 251.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let KCoachContentColor = UIColor(red: 245.0/255.0, green: 242.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let KSalesOrderProductTableViewBrandCodeCellCollapsedColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    let KSalesOrderProductTableViewBrandCodeCellExpandColor = UIColor(red: 182.0/255.0, green: 200.0/255.0, blue: 209.0/255.0, alpha: 1.0)
    let KSalesOrderProductTableViewBrandCodeCellCollapsedTextColor = UIColor(red: 89.0/255.0, green: 114.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    let KSalesOrderProductTableViewBrandCodeCellExpandTextColor = UIColor.white
    let surveyCellExpandColor = UIColor(red: 202.0/255.0, green: 209.0/255.0, blue: 221.0/255.0, alpha: 1.0)
    let surveyCellCollapseColor = UIColor(red: 247.0/255.0, green: 248.0/255.0, blue: 249.0/255.0, alpha: 1.0)
    let surveyCellSecondLevelColor = UIColor(red: 231.0/255.0, green: 235.0/255.0, blue: 237.0/255.0, alpha: 1.0)
    let KNewRowAnimationColor = UIColor(red: 30.0/255.0, green: 216.0/255.0, blue: 235.0/255.0, alpha: 1.0)
    let KSegmentedControltintColor = UIColor(red: 81.0/255.0, green: 102.0/255.0, blue: 136.0/255.0, alpha: 1.0)
    let kNewToDoColor = UIColor(red: 75.0/255.0, green: 145.0/255.0, blue: 207.0/255.0, alpha: 1.0)
    
    let kCancelledToDoColor = UIColor(red: 44.0/255.0, green: 57.0/255.0, blue: 75.0/255.0, alpha: 1.0)
    let kClosedToDoColor = UIColor(red: 252.0/255.0, green: 78.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    let kDefferedToDoColor = UIColor(red: 253.0/255.0, green: 159.0/255.0, blue: 76.0/255.0, alpha: 1.0)
    let kCompletedColor = UIColor(red: 39.0/255.0, green: 195.0/255.0, blue: 165.0/255.0, alpha: 1.0)
    let kBlockedCustomerBackgroundColor = UIColor.red
    let kActiveCustomerBackgroundColor = UIColor.white
    let kSalesWorxPanelHeaderDividerColor = UIColor(red: 184.0/255.0, green: 193.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    let KSalesOrderBonusCellBackgroundColor = UIColor.purple
    
    //new design colors
    let UIViewBackGroundColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0)
    let TitleLabelColor = UIColor(red: 106.0/255.0, green: 111.0/255.0, blue: 123.0/255.0, alpha: 1.0)
    let popoverViewGradientColor = UIColor(red: 44.0/255.0, green: 57.0/255.0, blue: 74.0/255.0, alpha: 1.0)
    let DescriptionLabelColor = UIColor(red: 44.0/255.0, green: 57.0/255.0, blue: 74.0/255.0, alpha: 1.0)
    let PanelTitleLabelColor = UIColor(red: 14.0/255.0, green: 28.0/255.0, blue: 45.0/255.0, alpha: 1.0)
    let HeaderViewBorderColor = UIColor(red: 218.0/255.0, green: 219.0/255.0, blue: 222.0/255.0, alpha: 1.0)
    let HeaderViewBackgroundColor = UIColor(red: 233.0/255.0, green: 251.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    
    let UITextFieldDarkBorderColor = UIColor(red: 181.0/255.0, green: 202.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    
    let UIButtonBackgroundColor = UIColor(red: 0.0/255.0, green: 204.0/255.0, blue: 129.0/255.0, alpha: 1.0)
    
    let todoCompletedStatusColor = UIColor(red: 71.0/255.0, green: 182.0/255.0, blue: 180.0/255.0, alpha: 1.0)
    let todoCancelledStatusColor = UIColor(red: 238.0/255.0, green: 107.0/255.0, blue: 103.0/255.0, alpha: 1.0)
    let todoDeferredStatusColor = UIColor(red: 255.0/255.0, green: 167.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    let todoClosedStatusColor = UIColor(red: 43.0/255.0, green: 56.0/255.0, blue: 73.0/255.0, alpha: 1.0)
    let messageBadgeIconBackgroundColor = UIColor(red: 239/255.0, green: 112/255.0, blue: 109/255.0, alpha: 1.0)
    
    
    let kSalesLineColor = UIColor(red: 245.0/255.0, green: 165.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    let kReturnsLineColor = UIColor(red: 239/255.0, green: 112/255.0, blue: 109/255.0, alpha: 1.0)
    let kCollectionLineColor = UIColor(red: 71/255.0, green: 182/255.0, blue: 129/255.0, alpha: 1.0)
    
    let kDoctorsLineColor = UIColor(red: 25/255.0, green: 196/255.0, blue: 165/255.0, alpha: 1.0)
    let kPharmacyLineColor = UIColor(red: 175/255.0, green: 101/255.0, blue: 215/255.0, alpha: 1.0)
    
    let kLightBorderColor = UIColor(red: 233/255.0, green: 233/255.0, blue: 235/255.0, alpha: 1.0)
    let kAcheivedStatusColor = UIColor(red: 0/255.0, green: 204/255.0, blue: 180/255.0, alpha: 1.0)
    let kLineChartAxisColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)
    
    let kPieChartBalanceToGoColor = UIColor(red: 225/255.0, green: 232/255.0, blue: 237/255.0, alpha: 1.0)
    let kSelectedCellColor = UIColor(red: 233/255.0, green: 242/255.0, blue: 250/255.0, alpha: 1.0)
    let kInternalDividerLabelColor = UIColor(red: 233/255.0, green: 233/255.0, blue: 235/255.0, alpha: 1.0)
    
    // 0,204,180
    
}
struct SalesWorxFont {
    static func KSWXRegularFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "WeblySleekUISemilight", size: size)!
    }
    
    static func KSWXSemiBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "WeblySleekUISemibold", size: size)!
    }
    
    let HeaderFont = KSWXRegularFontWithSize(size: 16)
    let bodyFont = KSWXRegularFontWithSize(size: 12)
    let bodySemiBold = KSWXSemiBoldFontWithSize(size: 14)
    let NavigationBarButtonItemFont = KSWXSemiBoldFontWithSize(size: 14)
    let salesworxTextFont = KSWXRegularFontWithSize(size: 14)
    let UITableViewCellFont = KSWXRegularFontWithSize(size: 19)
    let UITableViewCellDescFont = KSWXSemiBoldFontWithSize(size: 16)
    let SalesWorxTitleFont = KSWXRegularFontWithSize(size: 19)
    let headerTitleFont = KSWXSemiBoldFontWithSize(size: 19)
    let KOpenSans12 = KSWXRegularFontWithSize(size: 12)
    let KOpenSans13 = KSWXRegularFontWithSize(size: 13)
    let KOpenSans14 = KSWXRegularFontWithSize(size: 14)
    let KOpenSans15 = KSWXRegularFontWithSize(size: 15)
    let KOpenSans16 = KSWXRegularFontWithSize(size: 16)
    let KOpenSansSemiBold12 = KSWXSemiBoldFontWithSize(size: 12)
    let KOpenSansSemiBold13 = KSWXSemiBoldFontWithSize(size: 13)
    let KOpenSansSemiBold14 = KSWXSemiBoldFontWithSize(size: 14)
    let KOpenSansSemiBold15 = KSWXSemiBoldFontWithSize(size: 15)
    let KOpenSansSemiBold16 = KSWXSemiBoldFontWithSize(size: 16)
    let KOpenSansSemiBold30 = KSWXSemiBoldFontWithSize(size: 30)
    
}
