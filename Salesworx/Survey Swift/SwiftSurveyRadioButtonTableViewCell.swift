//
//  SwiftSurveyRadioButtonTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/22/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class SwiftSurveyRadioButtonTableViewCell: UITableViewCell {

    @IBOutlet var nodeNameLabel: UILabel!
    @IBOutlet var remarksTextField: UITextField!
    @IBOutlet var radioButtonLabels: [UILabel]!
    @IBOutlet var radioButtons: [RadioButton]!
    @IBOutlet var remarksTextFieldWidthConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!

    @IBOutlet var remarksTextFieldTrailingConstraint: NSLayoutConstraint!
    var node:YSTreeTableViewNode?{
        didSet{
            if let showRemarksTextField = node?.remarksRequired{
                print("remark required? \(showRemarksTextField)")
                if showRemarksTextField == "Y"{
                    remarksTextField.isHidden = false
                    remarksTextFieldWidthConstraint.constant = 250
                    remarksTextFieldTrailingConstraint.constant = 8.0
                    
                }else{
                    remarksTextField.isHidden = true
                    remarksTextFieldTrailingConstraint.constant = 0.0
                    remarksTextFieldWidthConstraint.constant = 0.0
                }
            }
            indentationLevel = node?.depth ?? 8 // 缩进层次
            indentationWidth = questionCellIndentationWidth // 每次缩进宽度
            contentViewLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
            nodeNameLabel?.font = surveyTitleFont

            if let questionText = node?.nodeName{
                if questionText.contains("\\\\n"){
                    let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
                    if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
                        var firstLine = ""
                        var secondLine = ""
                        if lines.count > 0 {
                            firstLine = lines[0]
                            for i in 1..<lines.count {
                                secondLine = secondLine + (lines[i])
                            }
                        }
                        let range1: NSRange = (currentString! as NSString).range(of: firstLine)
                        let range2: NSRange = (currentString! as NSString).range(of: secondLine)
                        let attributedText = NSMutableAttributedString(string: currentString!)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: surveyTextColor], range: range2)
                        nodeNameLabel?.attributedText = attributedText
                    }
                }else{
                    nodeNameLabel?.textColor = surveyTextColor
                    nodeNameLabel?.text = node?.nodeName
                }
            }
        

            if let responseTypeArray = node?.responseTypesContentArray{
                radioButtonLabels = radioButtonLabels.sorted(by: {$0.tag < $1.tag})
                radioButtons = radioButtons.sorted(by: {$0.tag < $1.tag})
                
                if (responseTypeArray.count<=radioButtonLabels.count){
                for i in 0..<responseTypeArray.count{
                    let responseType = responseTypeArray[i]
                    let currentLabel = radioButtonLabels[i]
                    let currentButton = radioButtons[i]
                    if responseType.isUpdated == "Y" {
                        currentButton.setImage(UIImage(named: "SurveyRadioButtonSelected"), for: .normal)
                    }else{
                        currentButton.setImage(UIImage(named: "SurveyRadioButtonUnSelected"), for: .normal)
                    }
                    currentButton.isHidden = false
                    currentLabel.isHidden = false
                    currentLabel.font = surveyTitleFont
                    currentLabel.textColor = surveyTextColor
                    currentLabel.text = responseType.responseText
                }
                }
            }
            
            
            
            /*for(UILabel* label in self.view.subview) {
             
             switch(label.tag) {
             case:0
             label.text = @"A";
             } break;
             }*/
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let alternateButtonsForRadioButtonOne = radioButtons.filter{$0.tag != 0}
        let alternateButtonsForRadioButtonTwo = radioButtons.filter{$0.tag != 1}
        let alternateButtonsForRadioButtonThree = radioButtons.filter{$0.tag != 2}
        let alternateButtonsForRadioButtonFour = radioButtons.filter{$0.tag != 3}

     // KeyboardAvoiding.avoidingView = self
        
            for radioButton in radioButtons{
                
             
            radioButton.layer.borderColor = UIColor.clear.cgColor
            if radioButton.tag == 0 {
            radioButton.alternateButton = alternateButtonsForRadioButtonOne
            }else if radioButton.tag == 1{
            radioButton.alternateButton = alternateButtonsForRadioButtonTwo
            }else if radioButton.tag == 2{
            radioButton.alternateButton = alternateButtonsForRadioButtonThree
            }
            else if radioButton.tag == 3{
            radioButton.alternateButton = alternateButtonsForRadioButtonFour
            }
            }
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func radioButtonTapped(_ sender: UIButton) {
        
        print("selected radio button is \(sender.tag)")
        
        if let currentNode = node{
            let selectedResponseType = currentNode.responseTypesContentArray[sender.tag]
            let tempResponseTypeArray = currentNode.responseTypesContentArray.filter{$0.isUpdated == "Y"}
            if tempResponseTypeArray.count > 0 {
                
                for objResponseType in tempResponseTypeArray {
                    objResponseType.isUpdated = "N"
                    let indexOfResponseType = currentNode.selectedResponseTypeIDs.index{$0 == objResponseType.responseID}
                    
                    if currentNode.selectedResponseTypeIDs.count > 0 {
                        currentNode.selectedResponseTypeIDs.remove(at: indexOfResponseType!)
                    }
                }
            }
            selectedResponseType.isUpdated = "Y"
            if currentNode.selectedResponseTypeIDs.count>0{
              currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
            }else{
                currentNode.selectedResponseTypeIDs = [String]()
                currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
            }
            print("selected option is \(selectedResponseType.responseText)")
        }
        
        
    }
    
    //MARK:- UITextField Delegate
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        node?.remarksText = textField.text
//
//    }
    
}
