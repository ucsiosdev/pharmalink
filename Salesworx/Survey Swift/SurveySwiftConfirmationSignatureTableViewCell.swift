//
//  SurveySwiftConfirmationSignatureTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/21/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

class SurveySwiftConfirmationSignatureTableViewCell: UITableViewCell,SwiftSignatureDelegate {
    func enableTableViewScrolling(flag: Bool) {
        print("enable tableview scrolling called in cell % hhd",flag)

    }
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var signatureView: SalesWorxSwiftSignatureView!
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        signatureView.swiftSignatureDelegate = self
        // Initialization code
    }

    var node:YSTreeTableViewNode?{
        didSet{
            titleLbl?.font = surveyTitleFont
            if let questionText = node?.nodeName{
                if questionText.contains("\\\\n"){
                    let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
                    if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
                        var firstLine = ""
                        var secondLine = ""
                        if lines.count > 0 {
                            firstLine = lines[0]
                            for i in 1..<lines.count {
                                secondLine = secondLine + (lines[i])
                            }
                        }
                        let range1: NSRange = (currentString! as NSString).range(of: firstLine)
                        let range2: NSRange = (currentString! as NSString).range(of: secondLine)
                        let attributedText = NSMutableAttributedString(string: currentString!)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: surveyTextColor], range: range2)
                        titleLbl.attributedText = attributedText
                    }
                }else{
                    titleLbl?.textColor = surveyTextColor
                    titleLbl?.text = node?.nodeName
                }
            }

            indentationLevel = node?.depth ?? 0 // 缩进层次
            indentationWidth = questionCellIndentationWidth // 每次缩进宽度
            contentViewLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
    }
    }
    func didSelectSaveButton(tappedView:SalesWorxSwiftSignatureView){
        
        if let currentNode = node{
        if let signatureImage = tappedView.signatureView.getSignature(){
            let uuid = NSUUID().uuidString.lowercased()
            // get the documents directory url
            var documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            documentsDirectory = documentsDirectory.appendingPathComponent("Coach Signatures").appendingPathComponent("temp")
            
            
            if FileManager.default.fileExists(atPath: documentsDirectory.relativePath, isDirectory: nil) == false{
                do {
                    try FileManager.default.createDirectory(atPath: documentsDirectory.relativePath, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print("error creating signature directory \(error)")
                }
            }
            // choose a name for your image
            let fileName = "\(uuid).jpg"
            print("file name is \(uuid)")
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            if let data = UIImageJPEGRepresentation(signatureImage, 1.0),
                !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    // writes the image data to disk
                    try data.write(to: fileURL)
                    print("survey signature file saved")
                    currentNode.signatureImageFileName = fileName
                    node?.signatureImageFileName = fileName
                    node?.selectedResponseTypeIDs = [fileName]
                    print("survey signature file saved with file name in cell\(currentNode.signatureImageFileName)")
                    
                } catch {
                    print("error saving file:", error)
                }
            }
        }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
