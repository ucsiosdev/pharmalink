//
//  SurveySwiftCheckBoxTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/22/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

class SurveySwiftCheckBoxTableViewCell: UITableViewCell {

    @IBOutlet var nodeNameLabel: UILabel!
    @IBOutlet var remarksTextField: UITextField!
    @IBOutlet var checkboxLabels: [UILabel]!
    @IBOutlet var checkBoxButtons: [CheckBox]!
    @IBOutlet var remarksTextFieldTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var remarksTextFieldWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!
    
    
    var node:YSTreeTableViewNode?{
        didSet{
            if let showRemarksTextField = node?.remarksRequired{
                print("remark required for checkbox cell? \(showRemarksTextField)")
                if showRemarksTextField == "Y"{
                    remarksTextField.isHidden = false
                    remarksTextFieldWidthConstraint.constant = 250.0
                    remarksTextFieldTrailingConstraint.constant = 8.0
                }else{
                    remarksTextField.isHidden = true
                    remarksTextFieldTrailingConstraint.constant = 0.0
                    remarksTextFieldWidthConstraint.constant = 0.0
                }
            }
            indentationLevel = node?.depth ?? 8 // 缩进层次
            indentationWidth = questionCellIndentationWidth // 每次缩进宽度
            contentViewLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
            nodeNameLabel?.font = surveyTitleFont
            if let questionText = node?.nodeName{
                if questionText.contains("\\\\n"){
                    let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
                    if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
                        var firstLine = ""
                        var secondLine = ""
                        if lines.count > 0 {
                            firstLine = lines[0]
                            for i in 1..<lines.count {
                                secondLine = secondLine + (lines[i])
                            }
                        }
                        let range1: NSRange = (currentString! as NSString).range(of: firstLine)
                        let range2: NSRange = (currentString! as NSString).range(of: secondLine)
                        let attributedText = NSMutableAttributedString(string: currentString!)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: surveyTextColor], range: range2)
                        nodeNameLabel?.attributedText = attributedText
                    }
                }else{
                    nodeNameLabel?.textColor = surveyTextColor
                    nodeNameLabel?.text = node?.nodeName
                }
            }

            
            if let responseTypeArray = node?.responseTypesContentArray{
                checkboxLabels = checkboxLabels.sorted(by: {$0.tag < $1.tag})
                checkBoxButtons = checkBoxButtons.sorted(by: {$0.tag < $1.tag})

                if (responseTypeArray.count<=checkboxLabels.count){
                    for i in 0..<responseTypeArray.count{
                        let responseType = responseTypeArray[i]
                        let currentLabel = checkboxLabels[i]
                        let currentButton = checkBoxButtons[i]
                        currentButton.isHidden = false
                        currentButton.titleLabel?.font = surveyTitleFont
                        currentLabel.isHidden = false
                        currentLabel.textColor = surveyTextColor
                        currentLabel.font = surveyTitleFont
                        currentLabel.text = responseType.responseText
                        if responseType.isUpdated == "Y" {
                            currentButton.setImage(UIImage(named: "SurveyCheckboxSelected"), for: .normal)
                        }else{
                            currentButton.setImage(UIImage(named: "SurveyCheckboxUnSelected"), for: .normal)
                        }
                    }
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func checkBoxButtonTapped(_ sender: UIButton) {
        
        print("selected checkbox button is \(sender.tag)")
        
        if let currentNode = node{
            let selectedResponseType = currentNode.responseTypesContentArray[sender.tag]
            
            if selectedResponseType.isUpdated == "N" {
                selectedResponseType.isUpdated = "Y"
                if currentNode.selectedResponseTypeIDs.count>0{
                    currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
                }else{
                    currentNode.selectedResponseTypeIDs = [String]()
                    currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
                }
            } else {
                selectedResponseType.isUpdated = "N"
                currentNode.selectedResponseTypeIDs = currentNode.selectedResponseTypeIDs.filter{$0 != selectedResponseType.responseID}
            }
            
            print("selected option is \(selectedResponseType.responseText)")
        }
        
        
    }
    
}
