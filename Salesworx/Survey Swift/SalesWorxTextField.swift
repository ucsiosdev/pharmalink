//
//  SalesWorxTextField.swift
//  MedRep
//
//  Created by Unique Computer Systems on 8/15/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

@IBDesignable class SalesWorxTextField: UITextField {



    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth=1.0
        let borderColor : UIColor = SalesWorxColors().HeaderViewBorderColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        self.autocorrectionType = .no;
        self.font = SalesWorxFont.KSWXSemiBoldFontWithSize(size: 15)
        self.textColor=SalesWorxColors().DescriptionLabelColor

        let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 16))
        let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: 16, height: 16))
        dropDownImageView.image = UIImage(named: "Arrow_DropDown")
        dropDownView.addSubview(dropDownImageView)
        dropDownView.isUserInteractionEnabled = false
        self.rightView = dropDownView
        self.rightViewMode = .always
        
        
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        leftPaddingView.isUserInteractionEnabled = false
        self.leftView = leftPaddingView
        self.leftViewMode = .always
                
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 1.0
        
        
    }
    func setIsReadOnly(status:Bool)  {
        if status {
            self.isEnabled=false
            self.textColor=SalesWorxColors().DescriptionLabelColor;
            self.backgroundColor=SalesWorxColors().SalesWorxReadOnlyTextFieldColor;
        }
        else
        {
            self.isEnabled = true
            self.textColor=SalesWorxColors().DescriptionLabelColor;
            self.backgroundColor = UIColor.white

            let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 16))
            let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: 16, height: 16))
            dropDownImageView.image = UIImage(named: "Arrow_DropDown")
            dropDownView.addSubview(dropDownImageView)
            dropDownView.isUserInteractionEnabled = false
            self.rightView = dropDownView
            self.rightViewMode = .always
        }
        self.rightView=UIView(frame: CGRect.zero)
    }
    
   
    func disableEditing(status:Bool)  {
        
        if status {
            self.isUserInteractionEnabled = false
            self.textColor=SalesWorxColors().SalesWorxReadOnlyTextViewColor
            self.backgroundColor=SalesWorxColors().SalesWorxReadOnlyTextViewColor
            self.rightView=UIView(frame: CGRect.zero)
        }
        else
        {
            self.textColor=SalesWorxColors().DescriptionLabelColor
            self.backgroundColor=UIColor.white
            self.isUserInteractionEnabled = true
        }
        
    }
    
}

class SalesWorxEditbleTextField: UITextField {
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth=1.0
        let borderColor : UIColor = SalesWorxColors().HeaderViewBorderColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        self.autocorrectionType = .no;
        self.font = SalesWorxFont.KSWXSemiBoldFontWithSize(size: 15)
        self.textColor=SalesWorxColors().DescriptionLabelColor
        
       
        
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        leftPaddingView.isUserInteractionEnabled = false
        self.leftView = leftPaddingView
        self.leftViewMode = .always
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 1.0
        
        
    }
    func setIsReadOnly(status:Bool)  {
        if status {
            self.isEnabled=false
            self.textColor=SalesWorxColors().DescriptionLabelColor;
            self.backgroundColor=SalesWorxColors().SalesWorxReadOnlyTextFieldColor;
        }
        else
        {
            self.isEnabled = true
            self.textColor=SalesWorxColors().DescriptionLabelColor;
            self.backgroundColor = UIColor.white
            
        }
        self.rightView=UIView(frame: CGRect.zero)
    }
    
    
    func disableEditing(status:Bool)  {
        
        if status {
            self.isUserInteractionEnabled = false
            self.textColor=SalesWorxColors().SalesWorxReadOnlyTextViewColor
            self.backgroundColor=SalesWorxColors().SalesWorxReadOnlyTextViewColor
            self.rightView=UIView(frame: CGRect.zero)
        }
        else
        {
            self.textColor=SalesWorxColors().DescriptionLabelColor
            self.backgroundColor=UIColor.white
            self.isUserInteractionEnabled = true
        }
        
    }
    
}
class SalesWorxReadOnlyTextField: UITextField {
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth=1.0
        let borderColor : UIColor = SalesWorxColors().HeaderViewBorderColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        self.autocorrectionType = .no;
        self.font = SalesWorxFont.KSWXSemiBoldFontWithSize(size: 15)
        self.textColor=SalesWorxColors().DescriptionLabelColor
        
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        leftPaddingView.isUserInteractionEnabled = false
        self.leftView = leftPaddingView
        self.leftViewMode = .always
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 1.0
        
        self.isUserInteractionEnabled = false
        self.backgroundColor=SalesWorxColors().SalesWorxReadOnlyTextViewColor
        self.rightView=UIView(frame: CGRect.zero)
    }
    
    
}
