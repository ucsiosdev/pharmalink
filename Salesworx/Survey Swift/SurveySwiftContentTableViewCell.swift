//
//  SurveySwiftContentTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/18/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit
import TGPControls


class SurveySwiftContentTableViewCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var titleLabelLeftConstraint: NSLayoutConstraint!

    @IBOutlet var camelLabel: TGPCamelLabels!
    
    @IBOutlet var tgpSlider: TGPDiscreteSlider!
    
    @IBOutlet var segmentView: UIView!
    
    @IBOutlet var containerView: UIView!
    
    @IBOutlet var surveySegmentControl: UISegmentedControl!
    @IBOutlet var remakrsTextFieldWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var remarksTextField: UITextField!
    @IBOutlet var remarksTextFieldTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!

    var node:YSTreeTableViewNode?{
        didSet{
            indentationLevel = node?.depth ?? 0 // 缩进层次
            indentationWidth = questionCellIndentationWidth // 每次缩进宽度
            contentViewLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
            
            selectionStyle = .none
//            let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
//            if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
//                var firstLine = ""
//                var secondLine = ""
//                if lines.count > 0 {
//                    firstLine = lines[0]
//                    for i in 1..<lines.count {
//                        secondLine = secondLine + (lines[i])
//                    }
//                }
//                let range1: NSRange = (currentString! as NSString).range(of: firstLine)
//                let range2: NSRange = (currentString! as NSString).range(of: secondLine)
//                let attributedText = NSMutableAttributedString(string: currentString!)
//                attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
//                attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor.black], range: range2)
//                titleLbl?.font = surveyTitleFont
//                titleLbl?.attributedText = attributedText
            titleLbl?.font = surveyTitleFont
            if let questionText = node?.nodeName{
                if questionText.contains("\\\\n"){
                    let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
                    if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
                        var firstLine = ""
                        var secondLine = ""
                        if lines.count > 0 {
                            firstLine = lines[0]
                            for i in 1..<lines.count {
                                secondLine = secondLine + (lines[i])
                            }
                        }
                        let range1: NSRange = (currentString! as NSString).range(of: firstLine)
                        let range2: NSRange = (currentString! as NSString).range(of: secondLine)
                        let attributedText = NSMutableAttributedString(string: currentString!)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: surveyTextColor], range: range2)
                        titleLbl?.attributedText = attributedText
                    }
                }else{
                    titleLbl?.textColor = surveyTextColor
                    titleLbl?.text = node?.nodeName
                }
            }
        
                if let responseTypeArray = node?.responseTypesContentArray{
                //camelLabel.tickCount = responseTypeArray.count - 1
                    var namesArray = [String]()
                    for currentResponseType in responseTypeArray{
                        namesArray.append(currentResponseType.responseText)
                    }
                    

                    if(namesArray.count>0){
                        camelLabel.names = namesArray
                        camelLabel.tickCount = namesArray.count-1
                        tgpSlider.tickCount = namesArray.count
                    }else{
                      camelLabel.names = ["1","2", "3"]
                        camelLabel.tickCount = 2
                        tgpSlider.tickCount = 3
                    }
                }
            
                        if let responseTypeArray = node?.responseTypesContentArray{
                            if responseTypeArray.count>0{
                                let updatedArray = responseTypeArray.filter({$0.isUpdated == "Y"})
                                if updatedArray.count > 0  {
                                    
                                    let selectedResponseType = updatedArray[0]
                                    if let formattedNumber = NumberFormatter().number(from: selectedResponseType.responseText) {
                                        let updatedValue = CGFloat(truncating: formattedNumber)
                                        tgpSlider.value = updatedValue
                                        camelLabel.value = UInt(updatedValue)
                                    }
                                   
                                    
                                 
                                }else{
                                    camelLabel.value = 0
                                    tgpSlider.value = 0
                                }
                        }
                    }
        
            tgpSlider.ticksListener = camelLabel

                if let showRemarksTextField = node?.remarksRequired{
                    print("remark required? \(showRemarksTextField)")
                    if showRemarksTextField == "Y"{
                        remarksTextField.isHidden = false
                        remakrsTextFieldWidthConstraint.constant = 250
                        remarksTextFieldTrailingConstraint.constant = 8.0

                    }else{
                        remarksTextField.isHidden = true
                        remarksTextFieldTrailingConstraint.constant = 0.0
                        remakrsTextFieldWidthConstraint.constant = 0.0
                    }
                }

            
                tgpSlider.addTarget(self,
                                         action: #selector(valueChanged(_:event:)),
                                         for: .valueChanged)

//                surveySegmentControl.addTarget(self,
//                                    action: #selector(didSelectSegement(_:event:)),
//                                    for: .valueChanged)
                remarksTextField.delegate=self
                layoutIfNeeded()
                layoutSubviews()
            
        }
    }
    

    @objc func valueChanged(_ sender: TGPDiscreteSlider, event:UIEvent) {
        var filteredResponse = "0"

        let currentSlider = sender
        if let currentNode = node{
            let selectedResponse = "\(Int(sender.value))"
            
            let tempResponseTypeArray = currentNode.responseTypesContentArray.filter{$0.isUpdated == "Y"}
            if tempResponseTypeArray.count > 0 {
                
                for objResponseType in tempResponseTypeArray {
                    objResponseType.isUpdated = "N"
                    let indexOfResponseType = currentNode.selectedResponseTypeIDs.index{$0 == objResponseType.responseID}
                    
                    if currentNode.selectedResponseTypeIDs.count > 0 {
                        currentNode.selectedResponseTypeIDs.remove(at: indexOfResponseType!)
                    }
                }
            }
            
            if currentNode.responseTypesContentArray.count > 0 {
                let selectedResponseTypeArray = currentNode.responseTypesContentArray.filter{$0.responseText == selectedResponse}
                if selectedResponseTypeArray.count > 0 {
                    let selectedResponseType = selectedResponseTypeArray[0]
                    selectedResponseType.isUpdated = "Y"
                }
            }
            
            if currentNode.selectedResponseTypeIDs.count > 0 {
                let selectedValue = Int(currentSlider.value)
                let selectedIndexArray = currentNode.responseTypesContentArray.filter({$0.responseText == "\(selectedValue)"})
                if selectedIndexArray.count > 0 {
                    let currentResponse : SurveyResponseType = selectedIndexArray[0]
                    filteredResponse = currentResponse.responseID
                }
                currentNode.selectedResponseTypeIDs.append(filteredResponse)
            } else {
                currentNode.selectedResponseTypeIDs = [String]()
                currentNode.selectedResponseTypeIDs.append(filteredResponse)
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        node?.remarksText = textField.text ?? ""
    }
    
    func setUpSliderControl()  {
        
        /*
        camelLabelsView.tickCount=5;
        camelLabelsView.ticksDistance=75;
        camelLabelsView.value=0;
        camelLabelsView.upFontName=@"WeblySleekUISemibold";
        camelLabelsView.upFontSize=20;
        camelLabelsView.upFontColor=kNavigationBarBackgroundColor;
        camelLabelsView.downFontName=@"WeblySleekUISemibold";
        camelLabelsView.downFontSize=20;
        camelLabelsView.downFontColor=[UIColor lightGrayColor];
        camelLabelsView.backgroundColor=[UIColor clearColor];
        
        
        sliderView.tickStyle=1;
        sliderView.tickSize = CGSizeMake(1, 8);
        sliderView.tickCount=5;
        sliderView.trackThickness=2;
        sliderView.incrementValue=1;
        sliderView.minimumValue=0;
        sliderView.value=0;
        sliderView.thumbTintColor=kNavigationBarBackgroundColor;
        //[descreteSliderView addTarget:self action:@selector(customSliderValueChanged) forControlEvents:UIControlEventValueChanged];
        
        sliderView.tintColor=kNavigationBarBackgroundColor;
        
        
        
        sliderView.ticksListener=camelLabelsView;*/
        /**/
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- Segment Control Methods
    @IBAction func didTapSegment(_ sender: UISegmentedControl) {
        
        if let currentNode = node{
            let selectedResponseType = currentNode.responseTypesContentArray[sender.tag]
            if currentNode.selectedResponseTypeIDs.count>0{
                currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseText)
            }else{
                currentNode.selectedResponseTypeIDs = [String]()
                currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseText)
            }
            print("selected option in segment type question is \(selectedResponseType.responseText)")
        }
        
    }
    
}
