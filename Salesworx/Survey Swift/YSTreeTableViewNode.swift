//
//  YSTreeTableViewNode.swift
//  YSTreeTableView
//
//  Created by yaoshuai on 2017/1/10.
//  Copyright © 2017年 ys. All rights reserved.
//

import UIKit

/// 节点类型
class YSTreeTableViewNode: NSObject,NSCoding {
    
  
    
    /// 父节点 - nil表示根节点
    var parentNode:YSTreeTableViewNode?{
        didSet{
            if let parentN = parentNode, !parentN.subNodes.contains(self) {
                parentN.subNodes.append(self)
            }
        }
    }
    
    /// 子节点集合
    var subNodes:[YSTreeTableViewNode] = [YSTreeTableViewNode](){
        didSet{
            for childNode in subNodes{
                childNode.parentNode = self
            }
        }
    }
    
    /// 本节点ID
    var nodeID:UInt = 0
    
    /// 本节点名称
    var nodeName:String = ""
    
    /// 左侧图标
    var leftImageName:String = ""
    
    /// 右侧图标
    var rightImageName:String = ""
    
    /// 本节点是否处于展开状态
    var isExpand:Bool = false
    
    /// 深度：根节点为0
    var depth:Int{
        if let parentN = parentNode{
            return parentN.depth + 1
        }
        return 1
    }
    
    var surveyTypeCode : String = ""
    var responseDisplayType : String = ""
    var sliderValue : Double = 0.0
    var remarksText : String = ""
    var remarksRequired : String = ""
    var signatureImageFileName : String = ""
    //this is to hold text of checkboxes and radio button labels
    var responseTypesContentArray : [SurveyResponseType] = [SurveyResponseType]()
    var selectedResponseTypeIDs :[String] = [String]()
    var surveyID : String = ""
    var questionID : String = ""
    var actualVisitID : String = ""
    var isNotApplicable : String = "N"
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.nodeName, forKey: "nodeName")
        aCoder.encode(self.isExpand, forKey: "isExpand")
        aCoder.encode(self.surveyTypeCode, forKey: "surveyTypeCode")
        aCoder.encode(self.responseDisplayType, forKey: "responseDisplayType")
        aCoder.encode(self.responseTypesContentArray, forKey: "responseTypesContentArray")
        aCoder.encode(self.selectedResponseTypeIDs, forKey: "selectedResponseTypeIDs")
        aCoder.encode(self.surveyID, forKey: "surveyID")
        aCoder.encode(self.questionID, forKey: "questionID")
        aCoder.encode(self.subNodes, forKey: "subNodes")
        aCoder.encode(self.actualVisitID, forKey: "actualVisitID")
        aCoder.encode(self.signatureImageFileName, forKey: "signatureImageFileName")
        aCoder.encode(self.isNotApplicable, forKey: "isNotApplicable")
        aCoder.encode(self.remarksText, forKey: "remarksText")

        
        

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.nodeName = aDecoder.decodeObject(forKey: "nodeName") as? String ?? ""
        self.isExpand = aDecoder.decodeObject(forKey: "isExpand") as? Bool ?? false
        self.surveyTypeCode = aDecoder.decodeObject(forKey: "surveyTypeCode") as? String ?? ""
        self.responseDisplayType = aDecoder.decodeObject(forKey: "responseDisplayType") as? String ?? ""
        self.responseTypesContentArray = aDecoder.decodeObject(forKey: "responseTypesContentArray") as? [SurveyResponseType] ?? [SurveyResponseType]()
        self.selectedResponseTypeIDs = aDecoder.decodeObject(forKey: "selectedResponseTypeIDs") as? [String] ?? [String]()
        self.surveyID = aDecoder.decodeObject(forKey: "surveyID") as? String ?? ""
        self.questionID = aDecoder.decodeObject(forKey: "questionID") as? String ?? ""
        self.subNodes = aDecoder.decodeObject(forKey: "subNodes") as? [YSTreeTableViewNode] ?? [YSTreeTableViewNode]()
        self.actualVisitID = aDecoder.decodeObject(forKey: "actualVisitID") as? String ?? ""
        self.signatureImageFileName = aDecoder.decodeObject(forKey: "signatureImageFileName") as? String ?? ""
        self.isNotApplicable = aDecoder.decodeObject(forKey: "isNotApplicable") as? String ?? ""
        self.remarksText = aDecoder.decodeObject(forKey: "remarksText") as? String ?? ""

        
        
        
        
    }
    
    
    /// 私有化构造函数
    private override init(){
        super.init()
    }
    
    /// 便利构造函数
    ///
    /// - Parameters:
    ///   - parentNodeID: 父节点ID(>=-1，-1表示根节点)
    ///   - nodeID: 本节点ID(>=0)
    ///   - nodeName: 本节点名称(!="")
    ///   - leftImageName: 左侧图标
    ///   - rightImageName: 右侧图标
    ///   - isExpand: 本节点是否处于展开状态
    convenience init?(nodeID:UInt,nodeName:String,leftImageName:String,rightImageName:String,isExpand:Bool,surveyTypeCode:String,responseDisplayType:String){
        
        self.init()
        
        if nodeName == ""{
            return nil
        }
        else{
            self.nodeID = nodeID
            self.nodeName = nodeName
            self.leftImageName = leftImageName
            self.rightImageName = rightImageName
            self.isExpand = isExpand
            self.surveyTypeCode=surveyTypeCode
            self.responseDisplayType = responseDisplayType
        }
    }
    
    /// 添加子节点
    ///
    /// - Parameter childNode: 子节点
    func addChildNode(childNode:YSTreeTableViewNode){
        childNode.parentNode = self
    }
}
