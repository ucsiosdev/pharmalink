//
//  SurveySwiftConfirmationViewController.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/21/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

class SurveySwiftConfirmationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SwiftSignatureDelegate {
    func enableTableViewScrolling(flag: Bool) {
        
    }
    
   
   
    
   
    
    @IBOutlet var confirmationTableView: UITableView!
    
    var confirmationDetailsArray = [YSTreeTableViewNode]()
    var nodesArray = [YSTreeTableViewNode]()

    @objc func saveButtonTapped(){
        let currentSurveyID = UserDefaults.standard.value(forKey: "Selected_Survey_ID") as? String ?? ""
        let selectedSurvey = Survey(currentSurveyID: currentSurveyID, currentSurveyTitle: "", currentCustomerID: "", currentSiteUseID: "", currentSalesRepID: "", currentSurveyTypeCode: "", currentStartTime: "", currentEndTime: "")
        //let surveyStructureArray = SurveyQueries.shared.fetchSurveyStructureWithNodes(selectedSurvey: selectedSurvey,withConfirmationQuestions: "Y")
        
        nodesArray.append(contentsOf: confirmationDetailsArray)
       // nodesArray = surveyStructureArray.0

        print("nodes array from previous screen \(nodesArray)")
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: nodesArray)
        UserDefaults.standard.set(encodedData, forKey: "Survey_Nodes")
        
        if let data = UserDefaults.standard.data(forKey: "Survey_Nodes"),
            let myPeopleList = NSKeyedUnarchiver.unarchiveObject(with: data) as? [YSTreeTableViewNode] {
            print("nodes array count after saving \(myPeopleList.count)")
            
        } else {
            print("There is an issue")
        }
        
        if let isFromVisit = UserDefaults.standard.value(forKey: "is_From_Visit") as? String{
            if (isFromVisit == "Y"){
                print("nodes array count before saving \(nodesArray.count)")
               
                let successAlert = UIAlertController(title: "Success", message: "Survey Details Saved Successfully", preferredStyle: UIAlertControllerStyle.alert)
                successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                    //self.navigationController?.popToRootViewController(animated: true)
                    if let viewControllersArray = self.navigationController?.viewControllers{
                        if viewControllersArray.count>2{
                            self.navigationController?.popToViewController(viewControllersArray[3], animated: true)
                        }
                        else{
                            self.navigationController?.popToRootViewController(animated: true)
                        }

                    }
                }))
                self.present(successAlert, animated: true, completion: nil)
            }else{
                insertSurveyToDatabase()
            }
            
        }else{
            insertSurveyToDatabase()
        }
        
        
        
        
        
        //        let surveyInsertionResponse = SurveyQueries.shared.insertSurvey()
        //        print("Survey inserted successfully ? \(surveyInsertionResponse) ")
        
        
        
    }

    
    @objc func insertSurveyToDatabase(){
        //  visitDetailsDictionary = (UserDefaults.standard.object(forKey: "VISIT_DETAILS_SWIFT") as? NSDictionary)!
        
        if let visitDetailsDictFromDefaults = UserDefaults.standard.object(forKey: "VISIT_DETAILS_SWIFT") as? NSDictionary{
            print("Visit details dictionary in swift is \(visitDetailsDictFromDefaults)")
            let surveyInsertionResponse = SurveyQueries.shared.insertSurvey(currentVisitDetailsDict:visitDetailsDictFromDefaults)
            print("Survey inserted successfully ? \(surveyInsertionResponse) ")
            if let isFromVisit = UserDefaults.standard.value(forKey: "is_From_Visit") as? String{
                if (isFromVisit == "Y"){
                    UserDefaults.standard.set(surveyInsertionResponse, forKey: "SURVEY_INSERT_STATUS")
                }else
                {
                    if surveyInsertionResponse{
                        let successAlert = UIAlertController(title: "Success", message: "Survey Details Saved Successfully", preferredStyle: UIAlertControllerStyle.alert)
                        successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(successAlert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        confirmationTableView.register(UINib(nibName: "SurveySwiftConfirmationTextViewCell", bundle: nil), forCellReuseIdentifier: "textViewCell")
        confirmationTableView.register(UINib(nibName: "SurveySwiftConfirmationSignatureTableViewCell", bundle: nil), forCellReuseIdentifier: "signatureCell")

        let saveBarButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem = saveBarButton
        
        print("confrimation details are \(confirmationDetailsArray)")
        confirmationTableView.reloadData()
        // Do any additional setup after loading the view.
        setNavigationBarTitleText(text: "Confirmation")
    }
    
    func setNavigationBarTitleText(text: String){
        //        let titleLabel = UILabel(frame: CGRect(0, 0, self.view.frame.size.width - 120, 44))
        let titleLabel:UILabel = UILabel(frame: CGRect.zero)
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont(name: "WeblySleekUISemibold", size: 18)
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = UIColor.white
        titleLabel.text = text.uppercased()
        self.navigationItem.titleView = titleLabel
    }
    

    //MARK:- UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200;
    }
     public func numberOfSections(in tableView: UITableView) -> Int {
        return confirmationDetailsArray.count
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentNode = confirmationDetailsArray[indexPath.section]
        if currentNode.responseDisplayType == "1"{
            let cell:SurveySwiftConfirmationTextViewCell = tableView.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath) as! SurveySwiftConfirmationTextViewCell
            cell.titleLbl.text = currentNode.nodeName
            return cell
        }else{
            let cell:SurveySwiftConfirmationSignatureTableViewCell = tableView.dequeueReusableCell(withIdentifier: "signatureCell", for: indexPath) as! SurveySwiftConfirmationSignatureTableViewCell
            cell.titleLbl.text = currentNode.nodeName
            cell.signatureView.tag = indexPath.section
            cell.signatureView.swiftSignatureDelegate = self
//            cell.signatureView.saveSignatureButton.tag = indexPath.section
//            cell.signatureView.saveSignatureButton.addTarget(self, action: #selector(saveSignatureTapped(_:)), for: .touchUpInside)
//
//            cell.signatureView.clearSignatureButton.tag = indexPath.section
//            cell.signatureView.clearSignatureButton.addTarget(self, action: #selector(clearButtonTapped(_:)), for: .touchUpInside)
            
            return cell
        }
       
     
    }
    
    func didSelectSaveButton(tappedView: SalesWorxSwiftSignatureView) {

        let currentNode = confirmationDetailsArray[tappedView.tag]
        if let signatureImage = tappedView.signatureView.getSignature(){
            
            let uuid = NSUUID().uuidString.lowercased()
            // get the documents directory url
            var documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            documentsDirectory = documentsDirectory.appendingPathComponent("Signature")
            // choose a name for your image
            let fileName = "\(uuid).jpg"
            print("file name is \(uuid)")
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            if let data = UIImageJPEGRepresentation(signatureImage, 1.0),
                !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    // writes the image data to disk
                    try data.write(to: fileURL)
                    currentNode.signatureImageFileName = fileName
                    currentNode.selectedResponseTypeIDs = [fileName]
                    print("survey signature file saved with file name in confirmation VC\(currentNode.signatureImageFileName)")

                } catch {
                    print("error saving file:", error)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
