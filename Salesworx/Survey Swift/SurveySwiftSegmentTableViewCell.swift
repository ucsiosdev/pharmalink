//
//  SurveySwiftSegmentTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/24/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class SurveySwiftSegmentTableViewCell: UITableViewCell {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var surveySegmentControl: UISegmentedControl!
    @IBOutlet var remarksTextField: UITextField!
    @IBOutlet var remarksTextFieldWidthConstraint: NSLayoutConstraint!
    @IBOutlet var remarksTextFieldTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabelLeftConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!

    
    var node:YSTreeTableViewNode?{
        didSet{
            indentationLevel = node?.depth ?? 0 // 缩进层次
            indentationWidth = questionCellIndentationWidth // 每次缩进宽度
            contentViewLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
            selectionStyle = .none
            titleLbl?.font = surveyTitleFont
            if let questionText = node?.nodeName{
                if questionText.contains("\\\\n"){
                    let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
                    if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
                        var firstLine = ""
                        var secondLine = ""
                        if lines.count > 0 {
                            firstLine = lines[0]
                            for i in 1..<lines.count {
                                secondLine = secondLine + (lines[i])
                            }
                        }
                        let range1: NSRange = (currentString! as NSString).range(of: firstLine)
                        let range2: NSRange = (currentString! as NSString).range(of: secondLine)
                        let attributedText = NSMutableAttributedString(string: currentString!)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: surveyTextColor], range: range2)
                        titleLbl.attributedText = attributedText
                    }
                }else{
                    titleLbl?.textColor = surveyTextColor
                    titleLbl?.text = node?.nodeName
                }
            }

            if let responseTypeArray = node?.responseTypesContentArray{
                    if responseTypeArray.count>1{
                        for i in 0..<responseTypeArray.count{
                            let responseType = responseTypeArray[i]
                            surveySegmentControl.setTitle(responseType.responseText, forSegmentAt: i)
                            let font = surveyTitleFont
                            surveySegmentControl.setTitleTextAttributes([NSAttributedStringKey.font: font!,NSAttributedStringKey.foregroundColor:surveyTextColor],
                                                                    for: .normal)
                            
                            if responseType.isUpdated == "Y" {
                                surveySegmentControl.selectedSegmentIndex = i
                            }else{
                                //currentButton.setImage(UIImage(named: "SurveyCheckboxUnSelected"), for: .normal)
                            }
                        }
                    }
                }
            if let showRemarksTextField = node?.remarksRequired{
                print("remark required? \(showRemarksTextField)")
                if showRemarksTextField == "Y"{
                    remarksTextField.isHidden = false
                    remarksTextFieldWidthConstraint.constant = 250
                    remarksTextFieldTrailingConstraint.constant = 8.0
                    
                }else{
                    remarksTextField.isHidden = true
                    remarksTextFieldTrailingConstraint.constant = 0.0
                    remarksTextFieldWidthConstraint.constant = 0.0
                }
            }
                //remarksTextField.delegate=self
                layoutIfNeeded()
                layoutSubviews()
            
            self.surveySegmentControl.addTarget(self, action: #selector(segmentControlTapped(_:)), for: .valueChanged)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // KeyboardAvoiding.avoidingView = self

    }

    @IBAction func segmentControlTapped(_ sender: UISegmentedControl) {
        
        if let currentNode = node{
            let selectedResponseType = currentNode.responseTypesContentArray[sender.selectedSegmentIndex]
            
            let tempResponseTypeArray = currentNode.responseTypesContentArray.filter{$0.isUpdated == "Y"}
            if tempResponseTypeArray.count > 0 {
                
                for objResponseType in tempResponseTypeArray {
                    objResponseType.isUpdated = "N"
                    let indexOfResponseType = currentNode.selectedResponseTypeIDs.index{$0 == objResponseType.responseID}
                    
                    if currentNode.selectedResponseTypeIDs.count > 0 {
                        currentNode.selectedResponseTypeIDs.remove(at: indexOfResponseType!)
                    }
                }
            }
            
            selectedResponseType.isUpdated = "Y"
            if currentNode.selectedResponseTypeIDs.count > 0 {
                currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
            }else{
                currentNode.selectedResponseTypeIDs = [String]()
                currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
            }
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
}
