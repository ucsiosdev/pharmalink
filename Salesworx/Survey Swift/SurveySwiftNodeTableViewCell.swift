//
//  SurveySwiftNodeTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/18/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

class SurveySwiftNodeTableViewCell: UITableViewCell {
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var arrowImageView: UIImageView!
    
    @IBOutlet var arrowImageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabelLeftConstraint: NSLayoutConstraint!
    var node:YSTreeTableViewNode?{
        didSet{
            indentationLevel = node?.depth ?? 8 // 缩进层次
            indentationWidth = 16 // 每次缩进宽度
            arrowImageLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
            selectionStyle = .none
            titleLbl?.font = surveyTitleFont
            titleLbl?.textColor = surveyTextColor
            titleLbl?.text = node?.nodeName
            print("is node expanded? \(String(describing: node?.isExpand))")
            
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
