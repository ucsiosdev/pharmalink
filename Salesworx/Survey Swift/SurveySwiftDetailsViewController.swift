//
//  SurveySwiftDetailsViewController.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/15/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class SurveySwiftDetailsViewController: UIViewController {
    @IBOutlet var surveyTableView: YSTreeTableView!
    var surveyQuestionGroups : [SurveyQuestionGroup] = [SurveyQuestionGroup]()
    var nodesArray = [YSTreeTableViewNode]()
    var confirmationQuestions = [YSTreeTableViewNode]()
    var menuItemsArray = [[String:Any]]()
    var selectArray = [Int]()
    var actualVisitID : String = ""
    var selectedSurveyID : NSString = ""
    var keyIndent = "level"
    var keyChildren = "Objects"
    var visitDetailsDictionary = NSDictionary()
    
    func setNavigationBarTitleText(text: String){
//        let titleLabel = UILabel(frame: CGRect(0, 0, self.view.frame.size.width - 120, 44))
        let titleLabel:UILabel = UILabel(frame: CGRect.zero)
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont(name: "WeblySleekUISemibold", size: 18)
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = UIColor.white
        titleLabel.text = text.uppercased()
        self.navigationItem.titleView = titleLabel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //KeyboardAvoiding.avoidingView = self.view
//        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "VISIT_DETAILS_SWIFT"), object:nil, queue:nil, using:visitDetailsNotification)

        // Do any additional setup after loading the view.
        
        let currentSurveyID = UserDefaults.standard.value(forKey: "Selected_Survey_ID") as? String ?? ""
        let selectedSurvey = Survey(currentSurveyID: currentSurveyID, currentSurveyTitle: "", currentCustomerID: "", currentSiteUseID: "", currentSalesRepID: "", currentSurveyTypeCode: "", currentStartTime: "", currentEndTime: "")
        let surveyStructureArray = SurveyQueries.shared.fetchSurveyStructureWithNodes(selectedSurvey: selectedSurvey)
        
        nodesArray = surveyStructureArray.0
        confirmationQuestions = surveyStructureArray.1
       
        let saveBarButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem = saveBarButton

        /*
        if(confirmationQuestions.count>0){
            let nextBarButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextButtonTapped))
            self.navigationItem.rightBarButtonItem = nextBarButton
        }else{
            let saveBarButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonTapped))
            self.navigationItem.rightBarButtonItem = saveBarButton
        }*/
        
        print("nodes array count in will appear \(nodesArray)")
        if nodesArray.count>0{
            surveyTableView.rootNodes = nodesArray
        }
        surveyTableView.actualVisitID = actualVisitID
        surveyTableView.treeDelegate = self
    }
//    func visitDetailsNotification(notification:Notification) -> Void {
//        if let extractInfo = notification.userInfo {
//            print("visit details data in table \(extractInfo)");
//            let visitIDFromNotification = extractInfo["Actual_Visit_ID"]
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let selectedSurveyTitle = UserDefaults.standard.value(forKey: "Selected_Survey_Title"){
            setNavigationBarTitleText(text:selectedSurveyTitle as! String)
        }
    }
    
    @objc func nextButtonTapped(){
        
        let updatedArray = nodesArray.flatMap{$0.subNodes}.flatMap{$0.subNodes}.filter{$0.remarksText.count>0}
        
        print("updated array \(updatedArray)")
        
        //to get the selected response type from a question
       // let selectedResponseType = nodesArray.flatMap{$0.subNodes}.flatMap{$0.subNodes}.flatMap{$0.responseTypesContentArray}.filter{$0.remarksText.count>0}
        
        let confirmationVC = SurveySwiftConfirmationViewController()
        confirmationVC.confirmationDetailsArray=confirmationQuestions
        confirmationVC.nodesArray = nodesArray
        self.navigationController?.pushViewController(confirmationVC, animated: true)
        
        
    }
    
    @objc func saveButtonTapped(){
       
        print("nodes array count before saving \(nodesArray.count)")
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: nodesArray)
        UserDefaults.standard.set(encodedData, forKey: "Survey_Nodes")
        
        if let data = UserDefaults.standard.data(forKey: "Survey_Nodes"),
            let myPeopleList = NSKeyedUnarchiver.unarchiveObject(with: data) as? [YSTreeTableViewNode] {
            print("nodes array count after saving \(myPeopleList.count)")
            
        } else {
            print("There is an issue")
        }
        
        if let isFromVisit = UserDefaults.standard.value(forKey: "is_From_Visit") as? String{
            if (isFromVisit == "Y"){
                let successAlert = UIAlertController(title: "Success", message: "Survey Details Saved Successfully", preferredStyle: UIAlertControllerStyle.alert)
                successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(successAlert, animated: true, completion: nil)
            }else{
                insertSurveyToDatabase()
            }

        }else{
            insertSurveyToDatabase()
        }
        
        
       
        
        
//        let surveyInsertionResponse = SurveyQueries.shared.insertSurvey()
//        print("Survey inserted successfully ? \(surveyInsertionResponse) ")
        

        
    }
    
  @objc func insertSurveyToDatabase(){
  //  visitDetailsDictionary = (UserDefaults.standard.object(forKey: "VISIT_DETAILS_SWIFT") as? NSDictionary)!

    if let visitDetailsDictFromDefaults = UserDefaults.standard.object(forKey: "VISIT_DETAILS_SWIFT") as? NSDictionary{
        print("Visit details dictionary in swift is \(visitDetailsDictFromDefaults)")
        let surveyInsertionResponse = SurveyQueries.shared.insertSurvey(currentVisitDetailsDict:visitDetailsDictFromDefaults)
         print("Survey inserted successfully ? \(surveyInsertionResponse) ")
        if let isFromVisit = UserDefaults.standard.value(forKey: "is_From_Visit") as? String{
            if (isFromVisit == "Y"){
                UserDefaults.standard.set(surveyInsertionResponse, forKey: "SURVEY_INSERT_STATUS")
            }else
            {
                if surveyInsertionResponse{
                    let successAlert = UIAlertController(title: "Success", message: "Survey Details Saved Successfully", preferredStyle: UIAlertControllerStyle.alert)
                    successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(successAlert, animated: true, completion: nil)
                }
            }
        }else{
            if surveyInsertionResponse{
                let successAlert = UIAlertController(title: "Success", message: "Survey Details Saved Successfully", preferredStyle: UIAlertControllerStyle.alert)
                successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(successAlert, animated: true, completion: nil)
            }
        }
    }
    }
   

    override func viewWillAppear(_ animated: Bool) {
        
       
    
    }
}
extension SurveySwiftDetailsViewController:YSTreeTableViewDelegate{
    func treeCellClick(node: YSTreeTableViewNode, indexPath: IndexPath) {
        print("indexPath is \(indexPath)")
        print("currentNodeModel is \(node)")
        print("currentNodeModelName is \(node.nodeName)")
        if node.isExpand{ // 当前为展开状态，要进行收缩操作
            print("node  \(node.nodeName) is expanded")
        } else{
            print("node is not expanded")
        }
    }
}


