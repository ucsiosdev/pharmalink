//
//  SurveySwiftViewController.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/15/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit




class SurveySwiftViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let parentVC:ZUUIRevealController = self.parent?.parent as! ZUUIRevealController
        let menuButton    = UIBarButtonItem( image: UIImage(named: "Menu_NavIcon")?.withRenderingMode(.alwaysOriginal) , style: .plain ,target: self.navigationController?.parent, action: #selector(parentVC.revealToggle(_:)))
        self.navigationItem.leftBarButtonItem = menuButton

        // Do any additional setup after loading the view.
    }

    @IBAction func buttonTapped(_ sender: Any) {
        
        let detailsVC : SurveySwiftDetailsViewController =  SurveySwiftDetailsViewController()
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
