//
//  SurveyTextField.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/27/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

class SurveyTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(red: (181.0/255.0), green: (202.0/255.0), blue: (210.0/255.0), alpha: 1).cgColor
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        
        
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(self.copy(_:)) || action == #selector(self.paste(_:)) || action == #selector(self.select(_:)) || action == #selector(self.selectAll(_:)) || action == #selector(self.cut(_:)) {
            return true
        }
        return false
    }

}
