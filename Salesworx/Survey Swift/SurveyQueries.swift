//
//  SurveyQueries.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/15/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import Foundation
import GRDB

class Survey {
    var surveyID : String
    var surveyTitle : String
    var customerID : String
    var siteUseID : String
    var salesRepID : String
    var surveyTypeCode : String
    var startTime : String
    var endTime : String
    init(currentSurveyID:String,currentSurveyTitle:String,currentCustomerID:String,currentSiteUseID:String,currentSalesRepID:String,currentSurveyTypeCode:String,currentStartTime:String,currentEndTime:String){
        self.surveyID=currentSurveyID
        self.surveyTitle=currentSurveyTitle
        self.customerID=currentCustomerID
        self.siteUseID=currentSiteUseID
        self.salesRepID=currentSalesRepID
        self.surveyTypeCode=currentSurveyTypeCode
        self.startTime=currentStartTime
        self.endTime=currentEndTime
        }
}


class SurveyQuestion{

    var surveyID : String
    var questionText : String
    var responseDisplayType : String
    var level : String
    var isQuestion : String
    var sliderValue : String
    var surveyTypeCode : String
    var questionID : String

    init(currentSurveyID:String,currentQuestionText:String,currentResponseDisplayType:String,currentLevel:String,isQuestion:String,currentSliderValue:String,currentSurveyTypeCode:String,currentQuestionID:String){
        self.surveyID = currentSurveyID
        self.questionText = currentQuestionText
        self.responseDisplayType = currentResponseDisplayType
        self.level = currentLevel
        self.isQuestion = isQuestion
        self.sliderValue = currentSliderValue
        self.surveyTypeCode = currentSurveyTypeCode
        self.questionID = currentQuestionID
    }
    
}
class SurveyQuestionGroup {
    var surveyID : String
    var questionDescription : String
    var displaySequence : String
    var surveyQuestionsArray = [SurveyQuestion]()
    var subSectionsArray:[SurveyQuestionGroup]?
    var sectionExapanded:Bool = false
    init(currentSurveyID:String,currentQuestionDescription:String,currentDisplaySequence:String){
        self.surveyID = currentSurveyID
        self.questionDescription = currentQuestionDescription
        self.displaySequence = currentDisplaySequence
    }
    
}

class SurveyResponseType:NSObject,NSCoding{
   
    func encode(with aCoder: NSCoder) {
         aCoder.encode(self.responseID, forKey: "responseID")
        aCoder.encode(self.responseText, forKey: "responseText")
        aCoder.encode(self.questionID, forKey: "questionID")
        aCoder.encode(self.responseTypeID, forKey: "responseTypeID")

    }
    
    required init?(coder aDecoder: NSCoder) {
        self.responseID = aDecoder.decodeObject(forKey: "responseID") as? String ?? ""
        self.responseText = aDecoder.decodeObject(forKey: "responseText") as? String ?? ""
        self.questionID = aDecoder.decodeObject(forKey: "questionID") as? String ?? ""
        self.responseTypeID = aDecoder.decodeObject(forKey: "responseTypeID") as? String ?? ""

    }
    
    var responseID:String
    var responseText:String
    var questionID:String
    var responseTypeID:String
    var isUpdated : String = "N"
    init(currentResponseID:String,currentResponseText:String,currentQuestionID:String,currentResponseTypeID:String) {
        self.responseID = currentResponseID
        self.responseText = currentResponseText
        self.questionID = currentQuestionID
        self.responseTypeID = currentResponseTypeID
    }

}

@objc class SwiftQueries : NSObject {
    class func retrieveQueries() -> SwiftQueries{
        return SwiftQueries()
    }
    func testFunction(){
        print("swift test function called")
    }
}

class SurveyQueries{
    static let shared = SurveyQueries()
    private init(){}
    
    func fetchDatabasePathFromDocumentsDirectory() -> String {
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return  documentsDirectoryURL[0].appendingPathComponent("swx.sqlite").absoluteString
    }
    

    func fetchDataForQuery(queryString:String) -> [Row]
    {
        var resultArray : [Row] = []
        do {
            let dbQueue = try DatabaseQueue(path: fetchDatabasePathFromDocumentsDirectory())
            try dbQueue.inDatabase { db in
                resultArray = try Row.fetchAll(db, queryString)
            }
        } catch {
            print(error.localizedDescription)
        }
        return resultArray
    }
    
    
    
    func fetchSurveyStructureWithNodes(selectedSurvey:Survey) -> ([YSTreeTableViewNode],[YSTreeTableViewNode]) {
        var responseArray = [YSTreeTableViewNode]()
        var emptyString = ""
        var idx = 0
        let parentsQry = "select * from TBL_Survey_Question_Groups where survey_id = '\(selectedSurvey.surveyID)' and parent_group_id = '0' order by Display_Sequence"
        let parentsArray = fetchDataForQuery(queryString: parentsQry)
        if parentsArray.count>0 {
            for i in 0..<parentsArray.count{
                idx+=idx
                let currentParentDictionary = parentsArray[i]
                var nodeName = fetchValidStringValue(sourceString: currentParentDictionary["Description"]?.databaseValue.description ?? "" )
                
                // nodeName=nodeName.replacingOccurrences(of: "\"", with: "")
                let currentSurveyID = selectedSurvey.surveyID
                let currentParentNode = YSTreeTableViewNode(nodeID: UInt(idx), nodeName: nodeName, leftImageName: "", rightImageName: "", isExpand: false, surveyTypeCode: selectedSurvey.surveyTypeCode, responseDisplayType: "")!
                currentParentNode.surveyID = currentSurveyID
                //now add child nodes
                let parentGroupID = currentParentDictionary["Question_Group_ID"]?.databaseValue.description ?? ""
                let childNodesQry = "select * from TBL_Survey_Question_Groups where parent_group_id = '\(parentGroupID)' order by Display_Sequence"
                let sectionsArray = fetchDataForQuery(queryString: childNodesQry)
                if sectionsArray.count>0{
                    for j in 0..<sectionsArray.count{
                        let currentParentDictionary = sectionsArray[j]
                        let questionGroupID = currentParentDictionary["Question_Group_ID"]?.databaseValue.description ?? ""
                        var nodeName = fetchValidStringValue(sourceString: currentParentDictionary["Description"]?.databaseValue.description ?? "")
                        //                    if nodeName.contains("\""){
                        //                        nodeName=nodeName.replacingOccurrences(of: "\"", with: "")
                        //                    }
                        let currentChildNode = YSTreeTableViewNode(nodeID: UInt(idx), nodeName: nodeName, leftImageName: "", rightImageName: "", isExpand: false,surveyTypeCode: selectedSurvey.surveyTypeCode, responseDisplayType: "")!
                        currentChildNode.surveyID = currentSurveyID
                        
                        //questions are the sub nodes
                        let questionsQry = "select * from TBL_Survey_Questions where Question_Group_ID = '\(questionGroupID)' order by Display_Sequence"
                        let questionsArray = fetchDataForQuery(queryString: questionsQry)
                        var subNodesArray = [YSTreeTableViewNode]()
                        if questionsArray.count>0{
                            for k in 0..<questionsArray.count{
                                let currentParentDictionary = questionsArray[k]
                                var nodeName = fetchValidStringValue(sourceString: currentParentDictionary["Question_Text"]?.databaseValue.description ?? "")
                                //                            if nodeName.contains("\""){
                                //                                nodeName=nodeName.replacingOccurrences(of: "\"", with: "")
                                //                            }
                                let surveyID = currentParentDictionary["Survey_ID"]?.databaseValue.description ?? ""
                                let defaultResponseID = currentParentDictionary["Default_Response_ID"]?.databaseValue.description ?? ""
                                
                                var responseDisplayType = currentParentDictionary["Response_Display_Type"]?.databaseValue.description ?? "".validString()
                                var questionID = currentParentDictionary["Question_ID"]?.databaseValue.description ?? ""
                                questionID=questionID.replacingOccurrences(of: "\"", with: "")
                                //print("Node name is \(currentParentDictionary)")
                                responseDisplayType=responseDisplayType.replacingOccurrences(of: "\"", with: "")
                                nodeName=nodeName.replacingOccurrences(of: "\"", with: "")
                                var remarksRequired : String = currentParentDictionary["Remark_Required"]?.databaseValue.description ?? ""
                                remarksRequired = remarksRequired.replacingOccurrences(of: "\"", with: "")
                                let currentSubNode = YSTreeTableViewNode(nodeID: UInt(idx), nodeName: nodeName, leftImageName: "", rightImageName: "", isExpand: false,surveyTypeCode: selectedSurvey.surveyTypeCode, responseDisplayType: responseDisplayType)!
                                currentSubNode.surveyID = surveyID
                                currentSubNode.questionID = questionID
                                currentSubNode.selectedResponseTypeIDs = [String]()
                                currentSubNode.selectedResponseTypeIDs.append(emptyString)
                                if(currentSubNode.responseDisplayType == "2" || currentSubNode.responseDisplayType == "3" || currentSubNode.responseDisplayType == "4" || currentSubNode.responseDisplayType == "6" || currentSubNode.responseDisplayType == "7"){
                                    currentSubNode.responseTypesContentArray = [SurveyResponseType]()
                                    //this is a radio button type question or checkbox type question, fetch the options for lables to be displayed in radio button
                                    /*<SA:2018-11-19>Unfortunately backend team did not add the response types for response types(segment and slider) so i have to hard code data to display, instead of response id i will have to insert response text, if tomorrow some client asks for a slider values to be 10,20,30 instead of 0,1,2 i will have to generate build again publish, if it had been inserted to db from backend it would have been much easier, i have been insisting the backend team to make this dynamic but no luck :(*/
                                    let responseTypeQuery = "select * from TBL_Survey_Responses where Response_Type_ID = '\(currentSubNode.responseDisplayType)' and Question_ID = '\(questionID)' ORDER BY Response_ID ASC"
                                    let responseContentArray = self.fetchDataForQuery(queryString: responseTypeQuery)
                                    if responseContentArray.count>0{
                                        for l in 0..<responseContentArray.count{
                                            let currentResponseDict = responseContentArray[l]
                                            let responseID = currentResponseDict["Response_ID"]?.databaseValue.description ?? "".validString()
                                            var responseText = fetchValidStringValue(sourceString: currentResponseDict["Response_Text"]?.databaseValue.description ?? "")
                                            //responseText=responseText.replacingOccurrences(of: "\"", with: "")
                                            let questionID = currentResponseDict["Question_ID"]?.databaseValue.description ?? "".validString()
                                            let responseTypeID = currentResponseDict["Response_Type_ID"]?.databaseValue.description ?? "".validString()
                                            let currentResponseType = SurveyResponseType(currentResponseID: responseID, currentResponseText: responseText, currentQuestionID: questionID, currentResponseTypeID: responseTypeID)
                                            
                                            if defaultResponseID.count > 0 && defaultResponseID == responseID
                                            {
                                                currentResponseType.isUpdated = "Y"
                                                currentSubNode.selectedResponseTypeIDs.append(defaultResponseID)
                                            }
                                            currentSubNode.responseTypesContentArray.append(currentResponseType)
                                        }
                                    }
                                }
                                currentSubNode.remarksRequired = remarksRequired
                                subNodesArray.append(currentSubNode)
                            }
                        }
                        currentChildNode.subNodes=subNodesArray
                        currentChildNode.parentNode = currentParentNode
                    }
                }
                else
                {
                    //questions are the sub nodes
                    let questionsQry = "select * from TBL_Survey_Questions where Question_Group_ID = '\(parentGroupID)' order by Display_Sequence"
                    let questionsArray = fetchDataForQuery(queryString: questionsQry)
                    var subNodesArray = [YSTreeTableViewNode]()
                    if questionsArray.count>0{
                        for k in 0..<questionsArray.count{
                            let currentParentDictionary = questionsArray[k]
                            var nodeName = fetchValidStringValue(sourceString: currentParentDictionary["Question_Text"]?.databaseValue.description ?? "")
                            //                            if nodeName.contains("\""){
                            //                                nodeName=nodeName.replacingOccurrences(of: "\"", with: "")
                            //                            }
                            var responseDisplayType = currentParentDictionary["Response_Display_Type"]?.databaseValue.description ?? ""
                            var remarksRequired : String = currentParentDictionary["Remark_Required"]?.databaseValue.description ?? ""
                            let surveyID = currentParentDictionary["Survey_ID"]?.databaseValue.description ?? ""
                            let defaultResponseID = currentParentDictionary["Default_Response_ID"]?.databaseValue.description ?? ""
                            
                            // currentParentDictionary["Remark_Required"]?.databaseValue.description ?? ""
                            remarksRequired = remarksRequired.replacingOccurrences(of: "\"", with: "")
                            responseDisplayType=responseDisplayType.replacingOccurrences(of: "\"", with: "")
                            var questionID = currentParentDictionary["Question_ID"]?.databaseValue.description ?? ""
                            questionID=questionID.replacingOccurrences(of: "\"", with: "")
                            
                            let currentSubNode = YSTreeTableViewNode(nodeID: UInt(idx), nodeName: nodeName, leftImageName: "", rightImageName: "", isExpand: false, surveyTypeCode: selectedSurvey.surveyTypeCode, responseDisplayType: responseDisplayType)!
                            currentSubNode.surveyID = surveyID
                            currentSubNode.questionID = questionID
                            currentSubNode.selectedResponseTypeIDs = [String]()
                            currentSubNode.selectedResponseTypeIDs.append(emptyString)
                            if(currentSubNode.responseDisplayType == "2" || currentSubNode.responseDisplayType == "3" || currentSubNode.responseDisplayType == "4" || currentSubNode.responseDisplayType == "6" || currentSubNode.responseDisplayType == "7"){
                                currentSubNode.responseTypesContentArray = [SurveyResponseType]()
                                //this is a radio button type question or checkbox type question, fetch the options for lables to be displayed in radio button
                                let responseTypeQuery = "select * from TBL_Survey_Responses where Response_Type_ID = '\(currentSubNode.responseDisplayType)' and Question_ID = '\(questionID)' ORDER BY Response_ID ASC"
                                let responseContentArray = self.fetchDataForQuery(queryString: responseTypeQuery)
                                if responseContentArray.count>0{
                                    for l in 0..<responseContentArray.count{
                                        let currentResponseDict = responseContentArray[l]
                                        let responseID = currentResponseDict["Response_ID"]?.databaseValue.description ?? "".validString()
                                        var responseText = currentResponseDict["Response_Text"]?.databaseValue.description ?? "".validString()
                                        responseText=responseText.replacingOccurrences(of: "\"", with: "")
                                        
                                        let questionID = currentResponseDict["Question_ID"]?.databaseValue.description ?? "".validString()
                                        let responseTypeID = currentResponseDict["Response_Type_ID"]?.databaseValue.description ?? "".validString()
                                        let currentResponseType = SurveyResponseType(currentResponseID: responseID, currentResponseText: responseText, currentQuestionID: questionID, currentResponseTypeID: responseTypeID)
                                        
                                        if defaultResponseID.count > 0 && defaultResponseID == responseID
                                        {
                                            currentResponseType.isUpdated = "Y"
                                            currentSubNode.selectedResponseTypeIDs.append(defaultResponseID)
                                        }
                                        currentSubNode.responseTypesContentArray.append(currentResponseType)
                                    }
                                }
                                
                            }
                            currentSubNode.remarksRequired = remarksRequired
                            subNodesArray.append(currentSubNode)
                        }
                    }
                    currentParentNode.subNodes = subNodesArray
                }
                responseArray.append(currentParentNode)
                
            }
        }else{
            
            //there can be questions without any groups, and question_group_id for those questions is 0
            //questions are the sub nodes
            let questionsQry = "select * from TBL_Survey_Questions where Question_Group_ID = '0' and Survey_ID = '\(selectedSurvey.surveyID)' order by Display_Sequence"
            let questionsArray = fetchDataForQuery(queryString: questionsQry)
            var subNodesArray = [YSTreeTableViewNode]()
            if questionsArray.count>0{
                for k in 0..<questionsArray.count{
                    let currentParentDictionary = questionsArray[k]
                    var nodeName = fetchValidStringValue(sourceString: currentParentDictionary["Question_Text"]?.databaseValue.description ?? "")
                    //                        if nodeName.contains("\""){
                    //                            nodeName=nodeName.replacingOccurrences(of: "\"", with: "")
                    //                        }
                    var responseDisplayType = currentParentDictionary["Response_Display_Type"]?.databaseValue.description ?? ""
                    var remarksRequired : String = currentParentDictionary["Remark_Required"]?.databaseValue.description ?? ""
                    // currentParentDictionary["Remark_Required"]?.databaseValue.description ?? ""
                    remarksRequired = remarksRequired.replacingOccurrences(of: "\"", with: "")
                    responseDisplayType=responseDisplayType.replacingOccurrences(of: "\"", with: "")
                    var questionID = currentParentDictionary["Question_ID"]?.databaseValue.description ?? ""
                    let surveyID = currentParentDictionary["Survey_ID"]?.databaseValue.description ?? ""
                    let defaultResponseID = currentParentDictionary["Default_Response_ID"]?.databaseValue.description ?? ""
                    
                    questionID=questionID.replacingOccurrences(of: "\"", with: "")
                    
                    let currentSubNode = YSTreeTableViewNode(nodeID: UInt(idx), nodeName: nodeName, leftImageName: "", rightImageName: "", isExpand: false, surveyTypeCode: selectedSurvey.surveyTypeCode, responseDisplayType: responseDisplayType)!
                    currentSubNode.surveyID = surveyID
                    currentSubNode.questionID = questionID
                    currentSubNode.selectedResponseTypeIDs = [String]()
                    currentSubNode.selectedResponseTypeIDs.append(emptyString)
                    if(currentSubNode.responseDisplayType == "2" || currentSubNode.responseDisplayType == "3" || currentSubNode.responseDisplayType == "4" || currentSubNode.responseDisplayType == "6" || currentSubNode.responseDisplayType == "7"){
                        currentSubNode.responseTypesContentArray = [SurveyResponseType]()
                        //this is a radio button type question or checkbox type question, fetch the options for lables to be displayed in radio button
                        let responseTypeQuery = "select * from TBL_Survey_Responses where Response_Type_ID = '\(currentSubNode.responseDisplayType)' and Question_ID = '\(questionID)' ORDER BY Response_ID ASC"
                        let responseContentArray = self.fetchDataForQuery(queryString: responseTypeQuery)
                        if responseContentArray.count>0{
                            for l in 0..<responseContentArray.count{
                                let currentResponseDict = responseContentArray[l]
                                let responseID = currentResponseDict["Response_ID"]?.databaseValue.description ?? "".validString()
                                var responseText = currentResponseDict["Response_Text"]?.databaseValue.description ?? "".validString()
                                responseText=responseText.replacingOccurrences(of: "\"", with: "")
                                
                                let questionID = currentResponseDict["Question_ID"]?.databaseValue.description ?? "".validString()
                                let responseTypeID = currentResponseDict["Response_Type_ID"]?.databaseValue.description ?? "".validString()
                                let currentResponseType = SurveyResponseType(currentResponseID: responseID, currentResponseText: responseText, currentQuestionID: questionID, currentResponseTypeID: responseTypeID)
                                
                                if defaultResponseID.count > 0 && defaultResponseID == responseID
                                {
                                    currentResponseType.isUpdated = "Y"
                                    currentSubNode.selectedResponseTypeIDs.append(defaultResponseID)
                                }
                                currentSubNode.responseTypesContentArray.append(currentResponseType)
                            }
                        }
                    }
                    currentSubNode.remarksRequired = remarksRequired
                    subNodesArray.append(currentSubNode)
                }
            }
            responseArray = subNodesArray
        }
        
        
        
        //append confirmation questions if available
        var confirmationQuestions = [YSTreeTableViewNode]()
        let confirmationQry = "select * from TBL_Survey_Questions where Is_Confirmation_Question = 'Y'order by Display_Sequence asc"
        let confirmationQuestionsContentArray = fetchDataForQuery(queryString: confirmationQry)
        if confirmationQuestionsContentArray.count>0{
            for i in 0..<confirmationQuestionsContentArray.count{
                let currentParentDictionary = confirmationQuestionsContentArray[i]
                var nodeName = fetchValidStringValue(sourceString: currentParentDictionary["Question_Text"]?.databaseValue.description ?? "")
                //                if nodeName.contains("\""){
                //                    nodeName=nodeName.replacingOccurrences(of: "\"", with: "")
                //                }
                var responseDisplayType = currentParentDictionary["Response_Display_Type"]?.databaseValue.description ?? ""
                responseDisplayType=responseDisplayType.replacingOccurrences(of: "\"", with: "")
                let questionID = currentParentDictionary["Question_ID"]?.databaseValue.description ?? "".validString()
                let surveyID = currentParentDictionary["Survey_ID"]?.databaseValue.description ?? ""
                
                let currentSubNode = YSTreeTableViewNode(nodeID: UInt(idx), nodeName: nodeName, leftImageName: "", rightImageName: "", isExpand: false, surveyTypeCode: selectedSurvey.surveyTypeCode, responseDisplayType: responseDisplayType)!
                currentSubNode.questionID = questionID
                currentSubNode.surveyID = surveyID
                currentSubNode.selectedResponseTypeIDs = [String]()
                currentSubNode.selectedResponseTypeIDs.append(emptyString)
                
                confirmationQuestions.append(currentSubNode)
            }
        }
        return (responseArray,confirmationQuestions)
    }
    
    func fetchSurveyStructure(selectedSurvey:Survey)->Dictionary<String, Any>{
        var structureArray = [[String:Any]]()
        var structureDict = [String:Any]()
        //Those with parent group id 0 are treated as parents
        let parentsQry = "select * from TBL_Survey_Question_Groups where survey_id = '\(selectedSurvey.surveyID)' and parent_group_id = '0' order by Display_Sequence"
        let parentsArray = fetchDataForQuery(queryString: parentsQry)
        if parentsArray.count>0 {
            for i in 0..<parentsArray.count{
                let currentParentDictionary = parentsArray[i]
                var currentDictionary = [String:Any]()
                let level = 0
                //fetching sections
                let parentGroupID = currentParentDictionary["Question_Group_ID"]?.databaseValue.description ?? ""
                let sectionsQry = "select * from TBL_Survey_Question_Groups where parent_group_id = '\(parentGroupID)' order by Display_Sequence"
                let sectionsArray = fetchDataForQuery(queryString: sectionsQry)
                if sectionsArray.count>0{
                    currentDictionary["name"] = currentParentDictionary["Description"]
                    currentDictionary["level"] = level
                    var objectsArray = [[String:Any]]()
                    for j in 0..<sectionsArray.count{
                        let sectionlevel = 1
                        let dataDict = sectionsArray[j]
                        let subSectionName = dataDict["Description"]?.databaseValue.description ?? ""
                        let questionGroupID = dataDict["Question_Group_ID"]?.databaseValue.description ?? ""
                        var currentSubSectionDictionary = [String:Any]()
                        currentSubSectionDictionary["name"] = subSectionName
                        currentSubSectionDictionary["level"] = sectionlevel
                        var subsectionObjectsArray = [[String:Any]]()
                        //fetch questions
                        let questionsQry = "select * from TBL_Survey_Questions where Question_Group_ID = '\(questionGroupID)' order by Display_Sequence"
                        let questionsArray = fetchDataForQuery(queryString: questionsQry)
                        if questionsArray.count>0{
                            let questionLevel = 1
                            for k in 0..<questionsArray.count{
                                var currentQuestionDictionary = [String:Any]()
                                let currentQuestionDict = questionsArray[k]
                                let questionText = currentQuestionDict["Question_Text"]?.databaseValue.description ?? ""
                                let responseDisplayType = currentQuestionDict["Response_Display_Type"]?.databaseValue.description ?? ""
                                let questionID = currentQuestionDict["Question_ID"]?.databaseValue.description ?? ""
                                currentQuestionDictionary["name"] = questionText
                                currentQuestionDictionary["Response_Display_Type"] = responseDisplayType
                                currentQuestionDictionary["level"] = questionLevel
                                currentQuestionDictionary["sliderValue"] = "0"
                                currentQuestionDictionary["isQuestion"] = "Y"
                                currentQuestionDictionary["Survey_Type_Code"] = selectedSurvey.surveyTypeCode
                                currentQuestionDictionary["Question_ID"] = questionID
                                currentQuestionDictionary["Survey_ID"] = selectedSurvey.surveyID
                                subsectionObjectsArray.append(currentQuestionDictionary)
                            }
                            currentSubSectionDictionary["Objects"] = subsectionObjectsArray
                        }
                        objectsArray.append(currentSubSectionDictionary)
                    }
                    currentDictionary["Objects"] = objectsArray
                }else{
                    //there are no sub sections for this parent group
                    currentDictionary["name"] = currentParentDictionary["Description"]
                    currentDictionary["level"] = level
                    var objectsArray = [[String:Any]]()
                    //fetch questions
                    let questionsQry = "select * from TBL_Survey_Questions where Question_Group_ID = '\(parentGroupID)' order by Display_Sequence"
                    let questionsArray = fetchDataForQuery(queryString: questionsQry)
                    if questionsArray.count>0{
                        let questionLevel = 1
                        for k in 0..<questionsArray.count{
                            var currentQuestionDictionary = [String:Any]()
                            let currentQuestionDict = questionsArray[k]
                            let questionText = currentQuestionDict["Question_Text"]?.databaseValue.description ?? ""
                            let responseDisplayType = currentQuestionDict["Response_Display_Type"]?.databaseValue.description ?? ""
                            let questionID = currentQuestionDict["Question_ID"]?.databaseValue.description ?? ""
                            currentQuestionDictionary["name"] = questionText
                            currentQuestionDictionary["Response_Display_Type"] = responseDisplayType
                            currentQuestionDictionary["level"] = questionLevel
                            currentQuestionDictionary["sliderValue"] = "0"
                            currentQuestionDictionary["isQuestion"] = "Y"
                            currentQuestionDictionary["Survey_Type_Code"] = selectedSurvey.surveyTypeCode
                            currentQuestionDictionary["Question_ID"] = questionID
                            currentQuestionDictionary["Survey_ID"] = selectedSurvey.surveyID
                            objectsArray.append(currentQuestionDictionary)
                        }
                    }
                    currentDictionary["Objects"] = objectsArray
            }
                structureArray.append(currentDictionary)
        }
        }
        structureDict["Objects"] = structureArray
        return structureDict
    }
                        
    func insertSurvey(currentVisitDetailsDict:NSDictionary)->Bool{
        
        //    NSDictionary * visitDetailsDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:actualVisitID,@"Actual_Visit_ID",salesRepIDSwift,@"SalesRep_ID",empCodeSwift,@"Emp_Code",createdBySwift,@"Created_By",surveyTimeStampSwift,@"Survey_Time_Stamp", nil];

        let actualVisitID = currentVisitDetailsDict["Actual_Visit_ID"] as? String
        let salesRepID = currentVisitDetailsDict["SalesRep_ID"] as? String
        let empCode = currentVisitDetailsDict["Emp_Code"] as? String
        let createdBy = currentVisitDetailsDict["Created_By"] as? String
        let isFromVisit = currentVisitDetailsDict["is_From_Visit"] as? String
        
        
        //retrieve survey nodes from defaults
        var surveyInsertionStatus = false
        if let data = UserDefaults.standard.data(forKey: "Survey_Nodes"),
        let  surveyArray = NSKeyedUnarchiver.unarchiveObject(with: data) as? [YSTreeTableViewNode] {
            
            var refinedNodesArray = [YSTreeTableViewNode]()
            
            refinedNodesArray.append(contentsOf: (surveyArray.flatMap{$0.subNodes}.flatMap{$0.subNodes}))
            refinedNodesArray.append(contentsOf: (surveyArray.flatMap{$0.subNodes}))

            print("refined array count is \(refinedNodesArray.count)")
            /*
            for currentNode in surveyArray{
                if currentNode.subNodes.count>0{
                    for currentSubNode in currentNode.subNodes{
                        refinedNodesArray.append(currentSubNode)
                    }
                }else{
                    refinedNodesArray.append(currentNode)
                }
            }
            */
            //var refinedNodesArray = surveyArray.flatMap{$0.subNodes}.flatMap{$0.subNodes}
          //  var refinedNodesArray = surveyArray.flatMap{$0.subNodes}

            if refinedNodesArray.count == 0 {
                refinedNodesArray = surveyArray
            }
            do{
                let dbQueue = try DatabaseQueue(path: fetchDatabasePathFromDocumentsDirectory())
                try dbQueue.inTransaction{db in
                for currentNode in refinedNodesArray{
                    if currentNode.selectedResponseTypeIDs.count>1{
                        //if there are any responses remove the default empty repsone
                        currentNode.selectedResponseTypeIDs = currentNode.selectedResponseTypeIDs.filter({$0 != ""})
                    }
                    let distinctResponseTypeIDs = Array(Set(currentNode.selectedResponseTypeIDs))
                    if distinctResponseTypeIDs.count>0{
                        print("remarks text during insertion is \(currentNode.remarksText)")
                        for i in 0..<distinctResponseTypeIDs.count
                        {
                            let currentResponse = distinctResponseTypeIDs[i]
                            let guid = NSUUID().uuidString.lowercased()
                            try db.execute("Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By,Custom_Attribute_1,Custom_Attribute_2) Values(?,?,?,?,?,?,?,?,?,?)", arguments: [guid,currentNode.surveyID,currentNode.questionID,currentResponse,salesRepID,empCode,fetchTodaysDateWithTimeinDatabaseDateFormat(),createdBy,currentNode.remarksText,actualVisitID])
                            
                            //if the response type is signature, move the signature file from temp to main folder
                            if currentNode.responseDisplayType == "5"{
                                print("signature file name in survey queries \(currentNode.signatureImageFileName)")
                                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                               let tempSignaturesDirectory = documentsDirectory.appendingPathComponent("Coach Signatures").appendingPathComponent("temp").appendingPathComponent(currentNode.signatureImageFileName)
                                if FileManager.default.fileExists(atPath: tempSignaturesDirectory.relativePath, isDirectory: nil) == true{
                                    let actualPath = documentsDirectory.appendingPathComponent("Coach Signatures").appendingPathComponent(currentNode.signatureImageFileName)
                                    do{
                                        try FileManager.default.moveItem(atPath: tempSignaturesDirectory.relativePath, toPath: actualPath.relativePath)
                                        try FileManager.default.removeItem(atPath: tempSignaturesDirectory.relativePath)
                                    }catch let error as NSError{
                                        print("error moving signature from temp to actual folder \(error)")
                                    }
                                }
                            }
                        }
                    }else{
                    }
                }
                 //insert visit additional info array
                    if let isVisit = isFromVisit{
                        if isVisit == "Y"{
                            for currentNode in surveyArray{
                                let rowID = NSUUID().uuidString.lowercased()
                                let attribName = "SURVEY_ID"
                                let attribValue = currentNode.surveyID
                                let createdAt = fetchTodaysDateWithTimeinDatabaseDateFormat()
                                try db.execute("INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)", arguments: [rowID,actualVisitID,attribName,attribValue,createdAt,createdBy])
                            }
                            
                            
                            /* NSString* rowID=[NSString createGuid];
                             NSString* attributeName=@"SURVEY_ID";
                             
                             NSMutableDictionary* currentDictionary=[currentVisitDetails.coachSurveyArray objectAtIndex:i];
                             NSString* surveyID=[NSString getValidStringValue:[currentDictionary valueForKey:@"Survey_ID"]];
                             NSString * attributeValue=surveyID;
                             
                             NSDateFormatter *formatterTime = [NSDateFormatter new] ;
                             NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                             [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                             [formatterTime setLocale:usLocaleq];
                             NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
                             NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
                             
                             insertIntoSurveyVisitAdditionalInfo =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,actualVisitID,attributeName,attributeValue,createdAt,createdBy, nil];*/
                        }
                    }
            
        
            surveyInsertionStatus = true
            return .commit
            }
        }
        catch {
            print("exception while inserting survey data to db \(error.localizedDescription)")
            surveyInsertionStatus = false
        }
            
            
            
        } else {
            print("unable to retrieve survey data from defaults")
            surveyInsertionStatus = false
        }
        
        
        return surveyInsertionStatus
    }

    
    func fetchTodaysDateWithTimeinDatabaseDateFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let currentDate = Date.init()
        let formattedDate = dateFormatter.string(from: currentDate)
        return formattedDate
    }
                    
    
    /*
    func fetchSurveyStructure(selectedSurveyID:String)->[SurveyQuestionGroup]{
        var questionGroupsArray = [SurveyQuestionGroup]()
       //Those with parent group id 0 are treated as parents
        let parentsQry = "select * from TBL_Survey_Question_Groups where survey_id = '\(selectedSurveyID)' and parent_group_id = '0' order by Display_Sequence"
        let parentsResponseArray = fetchDataForQuery(queryString: parentsQry)
        if parentsResponseArray.count>0{
            for i in 0..<parentsResponseArray.count{
                
                let currentParentsDictionary = parentsResponseArray[i]
                //fetch sections
                let parentGroupID = currentParentsDictionary["Question_Group_ID"]?.databaseValue.description ?? ""
                let parentGroupName = currentParentsDictionary["Description"]?.databaseValue.description ?? ""
                let surveyID = currentParentsDictionary["survey_id"]?.databaseValue.description ?? ""
                let currentQuestionGroup = SurveyQuestionGroup(currentSurveyID: surveyID, currentQuestionDescription: parentGroupName, currentDisplaySequence: "0")

                currentQuestionGroup.subSectionsArray = [SurveyQuestionGroup]()
                
                let sectionsQry = "select * from TBL_Survey_Question_Groups where parent_group_id = '\(parentGroupID)' order by Display_Sequence"
                let sectionsArray = fetchDataForQuery(queryString: sectionsQry)
                if sectionsArray.count>0{

                    for j in 0..<sectionsArray.count{
                        let currentSectionsDict = sectionsArray[j]
                        //fetch sections
                        let parentGroupName = currentSectionsDict["Description"]?.databaseValue.description ?? ""
                        let questionGroupID = currentSectionsDict["Question_Group_ID"]?.databaseValue.description ?? ""
                        let currentSubSectionQuestionGroup = SurveyQuestionGroup(currentSurveyID: surveyID, currentQuestionDescription: parentGroupName, currentDisplaySequence: "1")
                        
                        var questionsArray = [SurveyQuestion]()
                        //fetch questions
                        let questionsQry = "select * from TBL_Survey_Questions where Question_Group_ID = '\(questionGroupID)' order by Display_Sequence"
                        let questionsContentArray = fetchDataForQuery(queryString: questionsQry)
                        if questionsArray.count>0{
                            for k in 0..<questionsContentArray.count{

                                let questionsDict = questionsContentArray[k]
                                let questionText = questionsDict["Question_Text"]?.databaseValue.description ?? ""
                                let responseDisplayType = questionsDict["Response_Display_Type"]?.databaseValue.description ?? ""
                                let level = "1"
                                let sliderValue = "0"
                                let surveyTypeCode = questionsDict["Survey_Type_Code"]?.databaseValue.description ?? ""
                                let questionID = questionsDict["Question_ID"]?.databaseValue.description ?? ""
                                let surveyID = questionsDict["Survey_ID"]?.databaseValue.description ?? ""

                                let surveyQuestion = SurveyQuestion(currentSurveyID: surveyID, currentQuestionText: questionText, currentResponseDisplayType: responseDisplayType, currentLevel: level, isQuestion: "Y", currentSliderValue: sliderValue, currentSurveyTypeCode: surveyTypeCode, currentQuestionID: questionID)
                                questionsArray.append(surveyQuestion)
                            }
                            currentSubSectionQuestionGroup.surveyQuestionsArray = questionsArray
                        }
                        currentQuestionGroup.subSectionsArray?.append(currentSubSectionQuestionGroup)
                    }
                }
                else{
                    //there are no sub sections for this parent group, just questions
                    var questionsArray = [SurveyQuestion]()
                    //fetch questions
                    let questionsQry = "select * from TBL_Survey_Questions where Question_Group_ID = '\(parentGroupID)' order by Display_Sequence"
                    let questionsContentArray = fetchDataForQuery(queryString: questionsQry)
                    if questionsArray.count>0{
                        for k in 0..<questionsContentArray.count{
                            
                            let questionsDict = questionsContentArray[k]
                            let questionText = questionsDict["Question_Text"]?.databaseValue.description ?? ""
                            let responseDisplayType = questionsDict["Response_Display_Type"]?.databaseValue.description ?? ""
                            let level = "1"
                            let sliderValue = "0"
                            let surveyTypeCode = questionsDict["Survey_Type_Code"]?.databaseValue.description ?? ""
                            let questionID = questionsDict["Question_ID"]?.databaseValue.description ?? ""
                            let surveyID = questionsDict["Survey_ID"]?.databaseValue.description ?? ""
                            
                            let surveyQuestion = SurveyQuestion(currentSurveyID: surveyID, currentQuestionText: questionText, currentResponseDisplayType: responseDisplayType, currentLevel: level, isQuestion: "Y", currentSliderValue: sliderValue, currentSurveyTypeCode: surveyTypeCode, currentQuestionID: questionID)
                            questionsArray.append(surveyQuestion)
                        }
                        currentQuestionGroup.surveyQuestionsArray = questionsArray
                }
                
            }
                questionGroupsArray.append(currentQuestionGroup)
        }

 
        /*{
         NSMutableArray* structureArray=[[NSMutableArray alloc]init];
         NSMutableDictionary* structureDict=[[NSMutableDictionary alloc]init];
         /*
         1.Those with parent group id 0 are treated as parents,fetch parents first,
         now fetch sub sections
         
         */
         
         NSString* parentsQry=[NSString stringWithFormat:@"select * from TBL_Survey_Question_Groups where survey_id = '%@' and parent_group_id = '0' order by Display_Sequence ",selectedSurvey.Survey_ID];
         NSLog(@"parents qry %@",parentsQry);
         NSMutableArray* parentsArray=[self fetchDataForQuery:parentsQry];
         if (parentsArray.count>0) {
         for (NSInteger i=0; i<parentsArray.count; i++) {
         NSMutableDictionary* currentParentDictionary = [parentsArray objectAtIndex:i];
         NSMutableDictionary* currentDictionary=[[NSMutableDictionary alloc]init];
         
         NSInteger level=0;
         //fetching sections
         NSString* sectionQry=[NSString stringWithFormat:@"select * from TBL_Survey_Question_Groups where parent_group_id = '%@' order by Display_Sequence",[currentParentDictionary valueForKey:@"Question_Group_ID"]];
         NSMutableArray* sectionArray=[self fetchDataForQuery:sectionQry];
         if (sectionArray.count>0) {
         [currentDictionary setValue:[NSString getValidStringValue:[currentParentDictionary valueForKey:@"Description"]] forKey:@"name"];
         [currentDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)level]] forKey:@"level"];
         
         NSMutableArray* objectsArray=[[NSMutableArray alloc]init];
         
         //there are sub sections for this parent group
         for (NSInteger j=0;j<sectionArray.count;j++) {
         
         NSInteger sectionLevel=1;
         NSMutableDictionary* dataDict=[sectionArray objectAtIndex:j];
         NSMutableDictionary*  currentSubSectionDictionary=[[NSMutableDictionary alloc]init];
         [currentSubSectionDictionary setValue:[NSString getValidStringValue:[dataDict valueForKey:@"Description"]] forKey:@"name"];
         [currentSubSectionDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)sectionLevel]] forKey:@"level"];
         
         NSMutableArray* subsectionObjectsArray=[[NSMutableArray alloc]init];
         
         //fetch questions
         NSString* questionsString=[NSString stringWithFormat:@"select * from TBL_Survey_Questions where Question_Group_ID = '%@' order by Display_Sequence  ",[dataDict valueForKey:@"Question_Group_ID"]];
         NSMutableArray* questionsArray=[self fetchDataForQuery:questionsString];
         if (questionsArray.count>0) {
         NSInteger questionLevel=1;
         
         NSLog(@"questions text is %@",[questionsArray valueForKey:@"Question_Text"]);
         
         for (NSInteger k=0; k<questionsArray.count; k++) {
         NSMutableDictionary* currentQuestionDictionary=[[NSMutableDictionary alloc]init];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Question_Text"]] forKey:@"name"];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Response_Display_Type"]] forKey:@"Response_Display_Type"];
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)questionLevel]] forKey:@"level"];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:KAppControlsYESCode] forKey:@"isQuestion"];
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:@"0"] forKey:@"sliderValue"];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:selectedSurvey.Survey_Type_Code] forKey:@"Survey_Type_Code"];
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Question_ID"] ] forKey:@"Question_ID"];
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Survey_ID"] ] forKey:@"Survey_ID"];
         
         [subsectionObjectsArray addObject:currentQuestionDictionary];
         }
         
         [currentSubSectionDictionary setValue:subsectionObjectsArray forKey:@"Objects"];
         }
         [objectsArray addObject:currentSubSectionDictionary];
         
         }
         [currentDictionary setValue:objectsArray forKey:@"Objects"];
         
         
         
         
         }
         else{
         //there are no sub sections for this parent group
         
         [currentDictionary setValue:[NSString getValidStringValue:[currentParentDictionary valueForKey:@"Description"]] forKey:@"name"];
         [currentDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)level]] forKey:@"level"];
         NSMutableArray* objectsArray=[[NSMutableArray alloc]init];
         
         
         NSString* questionsString=[NSString stringWithFormat:@"select * from TBL_Survey_Questions where Question_Group_ID = '%@' order by Display_Sequence ",[currentParentDictionary valueForKey:@"Question_Group_ID"]];
         NSMutableArray* questionsArray=[self fetchDataForQuery:questionsString];
         
         if (questionsArray.count>0) {
         
         for (NSInteger i=0; i<questionsArray.count; i++) {
         NSMutableDictionary* currentQuestionDictionary=[[NSMutableDictionary alloc]init];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Question_Text"]] forKey:@"name"];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)level]] forKey:@"level"];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:KAppControlsYESCode] forKey:@"isQuestion"];
         [currentQuestionDictionary setValue:[NSString getValidStringValue:@"0"] forKey:@"sliderValue"];
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:selectedSurvey.Survey_Type_Code] forKey:@"Survey_Type_Code"];
         
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Response_Display_Type"]] forKey:@"Response_Display_Type"];
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Survey_ID"] ] forKey:@"Survey_ID"];
         
         
         [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Question_ID"] ] forKey:@"Question_ID"];
         
         //add responses
         //                            NSMutableArray* responsesArray=[[NSMutableArray alloc]init];
         //                            NSString* questionID=[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Question_ID"] ];
         //                            NSString* questionString=[NSString stringWithFormat:@"select IFNULL(Response_ID,'N/A') AS Response_ID, IFNULL(Response_Text,'N/A') AS Response_Text from TBL_Survey_Responses where question_id = '%@' ",questionID];
         //                            NSMutableArray* questionResponsesArray=[self fetchDataForQuery:questionString];
         //                            if (questionResponsesArray.count>0) {
         //
         //                                NSLog(@"question response array is %@", questionResponsesArray);
         //                            }
         
         
         
         [objectsArray addObject:currentQuestionDictionary];
         }
         
         }
         else{
         
         }
         [currentDictionary setValue:objectsArray forKey:@"Objects"];
         
         }
         
         [structureArray addObject:currentDictionary];
         
         }
         
         }
         
         
         
         
         [structureDict setValue:structureArray forKey:@"Objects"];
         
         return structureDict;
         }*/
    }
        return questionGroupsArray

    }*/
    func fetchValidStringValue(sourceString:String) -> String{
       let  refinedString=sourceString.replacingOccurrences(of: "\"", with: "")
        let trimmedString = refinedString.replacingOccurrences(of: "\\t", with: "")  

        return trimmedString
    }
 }

extension String{
    
    

    func validString() -> String{
        var dataString = self
        //if dataString.contains("\""){
        dataString=dataString.replacingOccurrences(of: "\"", with: "")
            dataString = dataString.trimmingCharacters(in: CharacterSet.whitespaces)
       // }

        return dataString
    }
}
