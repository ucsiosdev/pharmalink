//
//  SurveySwiftConfirmationTextViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/21/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
class SurveySwiftConfirmationTextViewCell: UITableViewCell {

    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var txtView: UITextView!
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!
    var node:YSTreeTableViewNode?{
        didSet{
            
            indentationLevel = node?.depth ?? 8 // 缩进层次
            indentationWidth = questionCellIndentationWidth // 每次缩进宽度
            contentViewLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
            titleLbl?.font = surveyTitleFont
            if let questionText = node?.nodeName{
                if questionText.contains("\\\\n"){
                    let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
                    if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
                        var firstLine = ""
                        var secondLine = ""
                        if lines.count > 0 {
                            firstLine = lines[0]
                            for i in 1..<lines.count {
                                secondLine = secondLine + (lines[i])
                            }
                        }
                        let range1: NSRange = (currentString! as NSString).range(of: firstLine)
                        let range2: NSRange = (currentString! as NSString).range(of: secondLine)
                        let attributedText = NSMutableAttributedString(string: currentString!)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: surveyTextColor], range: range2)
                        titleLbl?.attributedText = attributedText
                    }
                }else{
                    titleLbl?.textColor = surveyTextColor
                    titleLbl?.text = node?.nodeName
                }
            }
            
            if let remarksContent = node?.remarksText{
                if remarksContent.isEmpty == true {
                    txtView.text = ""
                }else{
                    txtView.text = node?.remarksText
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //KeyboardAvoiding.avoidingView = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
