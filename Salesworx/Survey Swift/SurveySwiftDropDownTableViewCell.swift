//
//  SurveySwiftDropDownTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 1/9/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

import UIKit

class SurveySwiftDropDownTableViewCell: UITableViewCell,SalesWorxPopOverDelegate,UIPopoverPresentationControllerDelegate,UITextFieldDelegate {
    
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var remarksTextField: UITextField!
    @IBOutlet var remarksTextFieldWidthConstraint: NSLayoutConstraint!
    @IBOutlet var remarksTextFieldTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabelLeftConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var dropDownTextField: SalesWorxTextField!
    var responseTextArray = [String]()
    var node:YSTreeTableViewNode?{
        didSet{
            indentationLevel = node?.depth ?? 0 // 缩进层次
            indentationWidth = questionCellIndentationWidth // 每次缩进宽度
            contentViewLeadingConstraint.constant = CGFloat(self.indentationLevel) * self.indentationWidth
            selectionStyle = .none
            titleLbl?.font = surveyTitleFont
            if let questionText = node?.nodeName{
                if questionText.contains("\\\\n"){
                    let currentString = node?.nodeName.replacingOccurrences(of: "\\\\n", with: "\n")
                    if let lines = currentString?.components(separatedBy: CharacterSet.newlines){
                        var firstLine = ""
                        var secondLine = ""
                        if lines.count > 0 {
                            firstLine = lines[0]
                            for i in 1..<lines.count {
                                secondLine = secondLine + (lines[i])
                            }
                        }
                        let range1: NSRange = (currentString! as NSString).range(of: firstLine)
                        let range2: NSRange = (currentString! as NSString).range(of: secondLine)
                        let attributedText = NSMutableAttributedString(string: currentString!)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 70.0 / 255.0, green: 144.0 / 255.0, blue: 210.0 / 255.0, alpha: 1)], range: range1)
                        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: surveyTextColor], range: range2)
                        titleLbl.attributedText = attributedText
                    }
                }else{
                    titleLbl?.textColor = surveyTextColor
                    titleLbl?.text = node?.nodeName
                }
            }
            
            let sampleArray = node?.selectedResponseTypeIDs.filter({$0 != ""})
            if let responseTypeArray = node?.responseTypesContentArray{
                if responseTypeArray.count>0{
                    let updatedArray = responseTypeArray.filter({$0.isUpdated == "Y"})
                    if updatedArray.count > 0  {
                        if let selectedResponses = sampleArray{
                            print("selected responses is \(selectedResponses)")
                            let responseType = updatedArray[0]
                            dropDownTextField.text =  responseType.responseText
                        }
                    }else{
                       dropDownTextField.text =  ""
                    }
                    
                    for i in 0..<responseTypeArray.count{
                        let responseType = responseTypeArray[i]
                        if responseTextArray.contains(responseType.responseText){
                        }
                        else{
                        responseTextArray.append(responseType.responseText)
                        }
                    }
                }
            }
            
            /*
            if let responseTypeArray = node?.responseTypesContentArray{
                if responseTypeArray.count>1{
                    for i in 0..<responseTypeArray.count{
                        let responseType = responseTypeArray[i]
                        responseTextArray.append(responseType.responseText)
                        if responseType.isUpdated == "Y" {
                            dropDownTextField.text = responseType.responseText
                        }else{
                            //currentButton.setImage(UIImage(named: "SurveyCheckboxUnSelected"), for: .normal)
                            //dropDownTextField.text =  ""
                        }
                    }
                }else{

                }
            }
            */
            
            if let showRemarksTextField = node?.remarksRequired{
                print("remark required? \(showRemarksTextField)")
                if showRemarksTextField == "Y"{
                    remarksTextField.isHidden = false
                    remarksTextFieldWidthConstraint.constant = 250
                    remarksTextFieldTrailingConstraint.constant = 8.0
                    
                }else{
                    remarksTextField.isHidden = true
                    remarksTextFieldTrailingConstraint.constant = 0.0
                    remarksTextFieldWidthConstraint.constant = 0.0
                }
            }
            //remarksTextField.delegate=self
            remarksTextField.text = node?.remarksText
            layoutIfNeeded()
            layoutSubviews()
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // KeyboardAvoiding.avoidingView = self
        dropDownTextField.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //delegate method
        showPopOver()

        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    func showPopOver(){
        let listVC = PopOverListViewController()
        
        listVC.delegate=self
        listVC.hideSearchBar=true
        listVC.popOverType = .listPopOver
        listVC.stringsArray = responseTextArray
        listVC.hideSaveButton=true
        listVC.isStringArray = true
        listVC.titleText = "Options"
        let controller = UINavigationController(rootViewController: listVC)
        //controller.navigationBar.tintColor = SalesWorxColors().kNavigationBarBackgroundColor
        controller.preferredContentSize = CGSize(width: 250, height: 300)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = dropDownTextField.rightView
            if let rightViewFrame = dropDownTextField.rightView?.bounds{
                popover.sourceRect = CGRect(x: rightViewFrame.width-30, y: 0, width: rightViewFrame.width, height: rightViewFrame.height)
            }
            popover.backgroundColor = UIColor.lightGray
            popover.permittedArrowDirections = .any
            //self.showGradiedntViewForPopOver()
            self.window?.rootViewController!.present(controller, animated: true, completion: nil)
           
    }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func didSelectPopOver(selectedContent: AnyObject) {
        
    }
    
    func didCancelPopOver() {
        
    }
    
    func didSelectStringinPopOver(selectedString: String) {
        
        
        dropDownTextField.text = selectedString
        if let currentNode = node {
            
            let tempResponseTypeArray = currentNode.responseTypesContentArray.filter{$0.isUpdated == "Y"}
            if tempResponseTypeArray.count > 0 {
                
                for objResponseType in tempResponseTypeArray {
                    objResponseType.isUpdated = "N"
                    let indexOfResponseType = currentNode.selectedResponseTypeIDs.index{$0 == objResponseType.responseID}
                    
                    if currentNode.selectedResponseTypeIDs.count > 0 {
                        currentNode.selectedResponseTypeIDs.remove(at: indexOfResponseType!)
                    }
                }
            }
            
            
            let matchedResponseType = currentNode.responseTypesContentArray.filter{$0.responseText == selectedString}.first
            if let selectedResponseType = matchedResponseType{
                
                selectedResponseType.isUpdated = "Y"
                if currentNode.selectedResponseTypeIDs.count > 0 {
                    currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
                } else {
                    currentNode.selectedResponseTypeIDs = [String]()
                    currentNode.selectedResponseTypeIDs.append(selectedResponseType.responseID)
                }
            }
        }
    }
    
    func didSelectTableViewIndexPath(selectedIndex: NSInteger) {
        
    }
}
