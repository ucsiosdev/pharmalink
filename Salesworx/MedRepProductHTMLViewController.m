//
//  MedRepProductHTMLViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 6/29/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepProductHTMLViewController.h"
#import "MedRepProductsHomeCollectionViewCell.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "HTMLViewerViewController.h"
#import "SalesworxDocumentDisplayManager.h"
#import "AppControl.h"
#import "MedRepQueries.h"

@interface MedRepProductHTMLViewController ()
{
    SalesworxDocumentDisplayManager * docDispalayManager;

}
@end

@implementation MedRepProductHTMLViewController

@synthesize productHTMLCollectionView, webView, productDataArray, selectedIndexPath, HTMLViewedDelegate, currentImageIndex,showAllProducts,staticData,isObjectData,isCurrentUserPharmacy,isCurrentUserDoctor;;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self fetchEmailID];
    
    docDispalayManager=[[SalesworxDocumentDisplayManager alloc] init];
    cancelButton.hidden=YES;
    
    demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_PPT_DEFAULT_DEMO_TIME.integerValue;

    viewedFiles=[[NSMutableArray alloc]init];
    if (productDataArray.count>0)
    {
        [productDataArray setValue:@"N" forKey:@"isSelected"];
//        [productHTMLCollectionView reloadData];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingProductPresentations];
    }
    if ([[AppControl retrieveSingleton].DISABLE_EMAIL_PRODUCT_MEDIA isEqualToString:KAppControlsYESCode]){
        
        emailButton.hidden = YES;
    }else{
        emailButton.hidden = NO;
    }
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    webView.scrollView.delegate=self;
    
    [productHTMLCollectionView registerClass:[MedRepProductsHomeCollectionViewCell class] forCellWithReuseIdentifier:@"productHomeCell"];

    selectedIndexPathArray=[[NSMutableArray alloc]init];
    selectedCellIndex=0;
    
    productHTMLCollectionView.backgroundColor = [UIColor clearColor];
    productHTMLCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    tap.numberOfTouchesRequired = 1;
    tap.delegate = self;
    [webView addGestureRecognizer:tap];
}

-(void)viewDidAppear:(BOOL)animated
{
    for (UIView *view in webView.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView *scrollView = (UIScrollView *)view;
            scrollView.scrollEnabled = NO;
        }
    }
    
    //webView.contentMode = UIViewContentModeScaleAspectFit;
//    webView.scrollView.scrollEnabled = YES;
//    webView.userInteractionEnabled=YES;
//    webView.scalesPageToFit = YES;
    
    if ([productHTMLCollectionView.delegate respondsToSelector:@selector(collectionView:didSelectItemAtIndexPath:)])
    {
        if (selectedIndexPath) {
            [productHTMLCollectionView selectItemAtIndexPath:selectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
            [productHTMLCollectionView.delegate collectionView:productHTMLCollectionView didSelectItemAtIndexPath:selectedIndexPath];
            
        }
    }
}

-(void)fetchEmailID{
    
    emailIDForAutoInsert = @"";
    
    if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
        
        if (isCurrentUserDoctor){
            emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaDoctorID:self.doctor_ID];
        }else  if (isCurrentUserPharmacy){
            emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaContactID:self.contact_ID];
        }
        
    }
}



#pragma mark - UIGestureRecognizer methods

- (void)singleTap:(UITapGestureRecognizer *)recognizer
{
    /** Power point slide show files will display in external apps*/
    if([[req.URL.path lowercaseString] hasSuffix:KppsxFileExtentionStr]){
        MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell*)[productHTMLCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:selectedCellIndex inSection:0]];

        [docDispalayManager ShowUserActivityShareViewInView:productHTMLCollectionView SourceRect:cell.frame AndFilePath:req.URL.path];
    }else{
        
        HTMLViewerViewController *obj = [[HTMLViewerViewController alloc] initWithNibName:@"HTMLViewerViewController" bundle:nil];
        obj.req = req;
        obj.htmlDelegate=self;
        obj.isFromProductsScreen=self.isFromProductsScreen;
        [self presentViewController:obj animated:YES completion:^{
            viewedTime=[NSDate date];
        }];
    }
    selectedIndexPath = nil;

}
-(void)willCloseWebPage
{
    NSDate *currentTime = [NSDate date];
    NSTimeInterval viewedTime1 = [currentTime timeIntervalSinceDate:viewedTime];
    NSLog(@"viewed Time for html = %f", viewedTime1);
    long ti = lroundf(viewedTime1);
    int viewedTimeInterval = ti % 60;
    
    
    ProductMediaFile* currentMedia=[productDataArray objectAtIndex:currentImageIndex];
    if (currentMedia.viewedTime.length == 0) {
        currentMedia.viewedTime=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%ld",(long)viewedTimeInterval]];
    } else {
        currentMedia.viewedTime = [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%0.2f",(viewedTimeInterval + currentMedia.viewedTime.doubleValue)]];
    }
    if (currentMedia.discussedAt.length == 0) {
        currentMedia.discussedAt = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    }
    [productDataArray replaceObjectAtIndex:currentImageIndex withObject:currentMedia];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark button action methods

- (IBAction)closeButtontapped:(id)sender
{
    
    if(self.isFromProductsScreen)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
    if ([HTMLViewedDelegate respondsToSelector:@selector(fullScreenPDFDidCancel)])
    {
        [HTMLViewedDelegate fullScreenHTMLDidCancel];
    }
    
    if ([self.HTMLViewedDelegate respondsToSelector:@selector(htmlViewedinDemo:)]) {
        
        [self.HTMLViewedDelegate htmlViewedinDemo:viewedFiles];
    }
    
        if ([self.HTMLViewedDelegate respondsToSelector:@selector(setBorderOnLastViewedImage:)]) {
            [self.HTMLViewedDelegate setBorderOnLastViewedImage:currentImageIndex];
        }
        
    [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)emailButtonTapped:(id)sender
{
    isEmailTapped=YES;
    cancelButton.hidden=NO;
    
    NSString *buttonTitle = [sender titleForState:UIControlStateNormal];
    
//    [productHTMLCollectionView setNeedsUpdateConstraints];
//    
//    [UIView animateWithDuration:2.0f animations:^{
//        [productHTMLCollectionView layoutIfNeeded];
//    }];
    
    if (buttonTitle.length>0 && sendEmail==YES)
    {
        [self sendEmail];
    }
    else
    {
        UIAlertView * emailAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Email", nil) message:NSLocalizedString(@"Please select the media you would like to send as an Email, you can select multiple media at a time", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [emailAlert show];
        
    }
}

- (IBAction)cancelButtonTapped:(id)sender {
    isEmailTapped=NO;
    cancelButton.hidden=YES;
    sendEmail=NO;
    for (NSMutableDictionary * currentDict in productDataArray) {
        NSLog(@"dict being removed is %@", currentDict);
        
        
    }
    [productDataArray setValue:@"N" forKey:@"isSelected"];
    
    [selectedIndexPathArray removeAllObjects];
    [productHTMLCollectionView reloadData];
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];
}

#pragma mark UICOllectionview methods

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(147, 150);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  productDataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productHomeCell";
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.selectedImgView.image=nil;
    cell.productImageView.image=nil;
    
    
    if (showAllProducts==YES) {
        
        ProductMediaFile *currentMediaFile=[productDataArray objectAtIndex:indexPath.row];
        cell.captionLbl.text = [MedRepDefaults getDefaultStringForEmptyString:currentMediaFile.Caption];

    }
    else
    {
        if (isObjectData==NO) {
            cell.captionLbl.text = [[productDataArray objectAtIndex:indexPath.row] valueForKey:@"Caption"];

        }
        else{
            cell.captionLbl.text = [productDataArray objectAtIndex:indexPath.row];

        }
        
    }
    cell.placeHolderImageView.image = [UIImage imageNamed:@"MedRep_PPT_Thumbnail"];

    if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"N"]) {
        
        cell.selectedImgView.image=nil;
    } else {
        cell.selectedImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
    }
    
    if (indexPath.row== selectedCellIndex)
    {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else
    {
        cell.layer.borderWidth=0;
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    currentImageIndex=indexPath.row;
    ProductMediaFile *currentMediaFile;
    NSString* urlString;
    [demoTimer invalidate];
    demoTimer=nil;

    if (isObjectData==NO) {

        if (staticData==YES||[[[productDataArray objectAtIndex:indexPath.row] valueForKey:@"isStaticData"]isEqualToString:@"Y"]) {
            //this is for neha to test html5 with js
            NSString *htmlFile = [[NSBundle mainBundle] pathForResource:currentMediaFile.Caption ofType:@"html"];
            urlString=htmlFile;
        }
        else{
        urlString=[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:[[[productDataArray objectAtIndex:indexPath.row] valueForKey:@"File_Name"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    else
    {
        currentMediaFile=[productDataArray objectAtIndex:indexPath.row];
        
        if (staticData==YES||[currentMediaFile.isStaticData isEqualToString:@"Y"]) {
            //this is for neha to test html5 with js
            NSString *htmlFile = [[NSBundle mainBundle] pathForResource:currentMediaFile.Caption ofType:@"html"];
            urlString=htmlFile;
        }
        else
        {
            urlString=[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:[currentMediaFile.File_Name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"file path is %@", urlString);
            
        }
    }
    
    
    
    

    if (isEmailTapped==YES)
    {
        MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        
        if (showAllProducts==YES) {
            
            if ([currentMediaFile.isSelected isEqualToString:@"Y"]) {
                currentMediaFile.isSelected=@"N";
            }
            else
            {
                currentMediaFile.isSelected=@"Y";
            }
            
        }
        else
        {
            if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"Y"]) {
                
                [[productDataArray objectAtIndex:indexPath.row]setValue:@"N" forKey:@"isSelected"];
            }
            else{
                [[productDataArray objectAtIndex:indexPath.row]setValue:@"Y" forKey:@"isSelected"];
            }
        }
        
        
        
        if ([selectedIndexPathArray containsObject:indexPath])
        {
            NSLog(@"check subviews of already existing cell %@", [cell.contentView.subviews description]);
            [selectedIndexPathArray removeObject:indexPath];
        }
        
        else
        {
            [selectedIndexPathArray addObject:indexPath];
            sendEmail=YES;
            [emailButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
        }
       
        selectedCellIndex=indexPath.row;
        [productHTMLCollectionView reloadData];
    }
    else
    {
        [collectionView setAllowsMultipleSelection:NO];
        selectedCellIndex=indexPath.row;
        [productHTMLCollectionView reloadData];
        
        if (showAllProducts==YES) {
            
            NSString* fileTempPath= [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentMediaFile.File_Name];
            
            NSLog(@"temp file path in visit %@", fileTempPath);

//            NSURLRequest *req;
            @try {
                
                req = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:fileTempPath]];
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
            [webView loadRequest:req];
        }
        else
        {
//            NSURLRequest *req;
            NSString* fileTempPath;
            @try {
                
                if ([[[productDataArray objectAtIndex:indexPath.row] valueForKey:@"isStaticData"]isEqualToString:@"Y"]) {
                   
                    NSURL *url = [[NSBundle mainBundle] URLForResource:[[productDataArray objectAtIndex:indexPath.row] valueForKey:@"Caption"] withExtension:@"html"];
                    req = [NSURLRequest requestWithURL:url];


                }
                else{
                    fileTempPath= [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]stringByAppendingPathComponent:[[productDataArray objectAtIndex:indexPath.row] valueForKey:@"File_Name"]];
                    NSLog(@"temp file path in products %@", fileTempPath);
                    

                }
                

                
                req = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:fileTempPath]];
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
            [webView loadRequest:req];
        }
    }
    
    if (selectedIndexPathArray.count==0)
    {
        sendEmail=NO;
        cancelButton.hidden = YES;
        [emailButton setTitle:@"Email" forState:UIControlStateNormal];
        isEmailTapped=NO;
    }
    if (demoTimer)
    {
        demoTimer=nil;
    }
    
    //start the timer to check if images for displayed for more than 5 secs
    demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:NO];
    if (showAllProducts) {
        ProductMediaFile *currentMediaFile=[productDataArray objectAtIndex:indexPath.row];
        _lblTitle.text = [MedRepDefaults getDefaultStringForEmptyString:currentMediaFile.Caption];
  
    }
    else{
        if (isObjectData==NO) {
            _lblTitle.text = [[productDataArray objectAtIndex:indexPath.row] valueForKey:@"Caption"];

        }
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webview finished loading");
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"webview failed to load with error %@", error.debugDescription);
}

-(void)startFaceTimeTimer
{
    [viewedFiles addObject:[productDataArray objectAtIndex:currentImageIndex]];
    NSLog(@"ppt viewed for %ld seconds", demoTimeInterval);

}

-(void)sendEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
       // NSString *emailAddres = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:self.currentVisit.visitOptions.customer.Customer_ID];
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        
        [mailController setSubject:@"MedRep"];
        [mailController setToRecipients:@[emailIDForAutoInsert]];
        [mailController.navigationBar setTintColor:[UIColor blackColor]];
        
        NSString* emailBodyString=[[NSString alloc]init];
        for (NSInteger i=0; i<selectedIndexPathArray.count; i++)
        {
            NSIndexPath * currentIndexPath=[selectedIndexPathArray objectAtIndex:i];
            
            NSString *pdfName= [NSString stringWithFormat:@"%@",[[productDataArray objectAtIndex:currentIndexPath.row]valueForKey:@"File_Name"] ];
            NSArray *items = [pdfName componentsSeparatedByString:@"M_"];   //take the one array for split the string
            
            NSLog(@"splitted items are %@", [items lastObject]);
            
            NSString* refinedFileName=[NSString stringWithFormat:@"File Name : %@",[items lastObject]];
            
            NSString* downloadUrl=[NSString stringWithFormat:@"Download URL : %@",[[productDataArray objectAtIndex:currentIndexPath.row]valueForKey:@"Download_Url"] ];
            
            NSLog(@"download url is %@", downloadUrl);
            emailBodyString=[[[[emailBodyString stringByAppendingString:refinedFileName] stringByAppendingString:@"\n \n"] stringByAppendingString:downloadUrl] stringByAppendingString:@"\n \n"];
            
            NSLog(@"email url is %@", emailBodyString);
            
            
        }
        [mailController setMessageBody:emailBodyString isHTML:NO];
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        
    }
}

#pragma mark Mail Composer Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            
            [emailButton setTitle:@"Email" forState:UIControlStateNormal];
            
            for (NSInteger i=0; i<selectedIndexPathArray.count; i++) {
                NSIndexPath * selectedIndexPathforEmail=[selectedIndexPathArray objectAtIndex:i];
                MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell*)[productHTMLCollectionView cellForItemAtIndexPath:selectedIndexPathforEmail];
                //already seelected cell
                [[cell.contentView.subviews lastObject] removeFromSuperview];
                NSLog(@"check subviews in mail delegate %@", [cell.contentView.subviews description]);
            }
            isEmailTapped=NO;
            cancelButton.hidden=YES;
            
            sendEmail=NO;
            [productDataArray setValue:@"N" forKey:@"isSelected"];
            
            [selectedIndexPathArray removeAllObjects];
            [productHTMLCollectionView reloadData];
            
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
