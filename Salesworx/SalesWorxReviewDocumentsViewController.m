//
//  SalesWorxReviewDocumentsViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReviewDocumentsViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxReviewDocumentsTableViewCell.h"
#import "ReviewDocumentReportsViewController.h"
#import "SWCustomerOrderHistoryDescriptionViewController.h"
#import "SWCustomerReturnHistoryDescriptionViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxDateHelper.h"

@interface SalesWorxReviewDocumentsViewController (){
    SalesWorxDateHelper * dateHelper;
}
@end

@implementation SalesWorxReviewDocumentsViewController
@synthesize arrReviewDocuments;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    viewFooter.backgroundColor = MedRepMenuTitleUnSelectedCellTextColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kReviewDocumentsTitle localizedCapitalizedString], nil)];
    
    dateHelper=[[SalesWorxDateHelper alloc] init];
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    
    tblReviewDocuments.estimatedRowHeight=45;
    tblReviewDocuments.rowHeight=UITableViewAutomaticDimension;
    
    
    [DateRangeTextField SetCustomDateRangeText];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    lblTotalAmount.font=kSWX_FONT_SEMI_BOLD(20);
    
    
    
    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
    pieChartPlaceholderImage.hidden = NO;
    parentViewOfPieChart.hidden = YES;
    
    arrCustomerName = [[NSMutableArray alloc]init];
    arrCustomersDetails = [[NSMutableArray alloc]init];
    
    NSMutableArray *customerListArray = [[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
    for (NSMutableDictionary *currentDict in customerListArray)
    {
        NSMutableDictionary *currentCustDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
        Customer * currentCustomer = [[Customer alloc] init];
        currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
        currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
        currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
        currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
        currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
        currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
        currentCustomer.City=[currentCustDict valueForKey:@"City"];
        currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
        currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
        currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
        currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
        currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
        currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
        currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
        currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
        currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
        currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
        currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
        currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
        currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
        currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
        currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
        currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
        currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
        currentCustomer.Ship_Site_Use_ID = [currentCustDict valueForKey:@"Ship_Site_Use_ID"];
        
        [arrCustomersDetails addObject:currentCustomer];
        if ([currentCustomer.Customer_Name containsString:currentCustomer.Customer_No]) {
            [arrCustomerName addObject:[currentCustDict valueForKey:@"Customer_Name"]];
        } else {
            [arrCustomerName addObject:[[currentCustDict valueForKey:@"Customer_Name"] stringByAppendingString:[NSString stringWithFormat:@" [%@]",[currentCustDict valueForKey:@"Customer_No"]]]];
        }
    }
    arrDocumentType = [NSMutableArray arrayWithObjects:@"All", @"Collection", @"Sales Returns", @"Sales Order", nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController)
    {
        customerPieChart.backgroundColor = [UIColor clearColor];
        customerPieChart.labelFont = [UIFont boldSystemFontOfSize:14];
        self.slicesForCustomerPotential = [[NSMutableArray alloc]init];
        
        customerPieChart.labelRadius = 30;
        [customerPieChart setDelegate:self];
        [customerPieChart setDataSource:self];
        [customerPieChart setPieCenter:CGPointMake(70, 65)];
        [customerPieChart setShowPercentage:YES];
        [customerPieChart setLabelColor:[UIColor whiteColor]];
        [customerPieChart setUserInteractionEnabled:NO];
        
        self.sliceColorsForCustomerPotential = [NSArray arrayWithObjects:
                                                [UIColor colorWithRed:74.0/255.0 green:145.0/255.0 blue:207.0/255.0 alpha:1],
                                                [UIColor colorWithRed:62.0/255.0 green:193.0/255.0 blue:194.0/255.0 alpha:1],
                                                [UIColor colorWithRed:252.0/255.0 green:166.0/255.0 blue:88.0/255.0 alpha:1],
                                                nil];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            NSMutableArray *reviewDocumentsArray = [[[SWDatabaseManager retrieveManager]fetchReviewDocumentsData] mutableCopy];
            arrReviewDocuments = [[NSMutableArray alloc]init];
            arrCollectionDocuments = [[NSMutableArray alloc]init];
            arrOrderAndReturnDocuments = [[NSMutableArray alloc]init];
            if (reviewDocumentsArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary * currentDict in reviewDocumentsArray) {
                    
                    NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Review_Documents *currentCustomer = [[Review_Documents alloc] init];
                    currentCustomer.Amount = [currentCustDict valueForKey:@"Amount"];
                    currentCustomer.Customer_Name = [currentCustDict valueForKey:@"Customer_Name"];
                    currentCustomer.Doc_Date = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"yyyy-MM-dd" scrString:[currentCustDict valueForKey:@"DocDate"]];
                    currentCustomer.Doc_DateWithTime = [currentCustDict valueForKey:@"DocDate"];
                    currentCustomer.Doc_No = [currentCustDict valueForKey:@"DocNo"];
                    currentCustomer.Doc_Type = [currentCustDict valueForKey:@"Doctype"];
                    currentCustomer.ERP_Status = [currentCustDict valueForKey:@"ERP_Status"];
                    currentCustomer.Start_Time = [currentCustDict valueForKey:@"Start_Time"];
                    currentCustomer.Shipping_Instructions=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Shipping_Instructions"]];
                    currentCustomer.Ship_To_Customer_Id = [currentCustDict valueForKey:@"Customer_ID"];
                    currentCustomer.Ship_To_Site_Id = [currentCustDict valueForKey:@"Site_Use_ID"];
                    
                    [arrReviewDocuments addObject:currentCustomer];
                    if ([currentCustomer.Doc_Type isEqualToString:@"Collection"]) {
                        [arrCollectionDocuments addObject:currentCustomer];
                    } else {
                        [arrOrderAndReturnDocuments addObject:currentCustomer];
                    }
                }
            }
            unfilteredReviewDocumentsArray = arrReviewDocuments;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(Doc_Date >= %@) AND (Doc_Date <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]];
                NSMutableArray* filteredArray = [[unfilteredReviewDocumentsArray filteredArrayUsingPredicate:predicate] mutableCopy];
                
                arrReviewDocuments = filteredArray;
                [self loadPieChart:arrReviewDocuments];
            });
        });
    }
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrReviewDocuments.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"reviewDocuments";
    SalesWorxReviewDocumentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxReviewDocumentsTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    
    cell.lblDocNo.isHeader=YES;
    cell.lblDocType.isHeader=YES;
    cell.lblDocDate.isHeader=YES;
    cell.lblName.isHeader=YES;
    cell.lblAmount.isHeader=YES;
    cell.lblStatus.isHeader=YES;
    
    cell.lblDocNo.text = NSLocalizedString(@"Doc No", nil);
    cell.lblDocType.text = NSLocalizedString(@"Doc Type", nil);;
    cell.lblDocDate.text = NSLocalizedString(@"Doc Date", nil);;
    cell.lblName.text = NSLocalizedString(@"Name", nil);;
    cell.lblAmount.text = NSLocalizedString(@"Amount", nil);;
    cell.lblStatus.text = NSLocalizedString(@"Status", nil);;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"reviewDocuments";
    SalesWorxReviewDocumentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxReviewDocumentsTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Review_Documents *data = [arrReviewDocuments objectAtIndex:indexPath.row];
    cell.lblDocNo.text = data.Doc_No;
    cell.lblDocType.text = data.Doc_Type;
    cell.lblDocDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:data.Doc_DateWithTime];
    cell.lblName.text = data.Customer_Name;
    cell.lblAmount.text = [[NSString stringWithFormat:@"%@",data.Amount] currencyString];
    NSString* shippingInstructions=[NSString getValidStringValue:data.Shipping_Instructions];
    
    if ([shippingInstructions isEqualToString:@"N/A"]) {
        cell.lblStatus.text=kNewStatus;
    }
    else if ([shippingInstructions isEqualToString:@""]) {
        cell.lblStatus.text= KNotApplicable;
    }
    else
    {
        cell.lblStatus.text=shippingInstructions;
    }
    
   // cell.lblStatus.text = data.ERP_Status;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Review_Documents *selectedReportDict = [arrReviewDocuments objectAtIndex:indexPath.row];
    if ([selectedReportDict.Doc_Type isEqualToString:@"Order"])
    {
        [self fetchOrderLineItems:selectedReportDict];
    }
    else if ([selectedReportDict.Doc_Type isEqualToString:@"Return"])
    {
        [self fetchReturnLineItems:selectedReportDict];
    }
}
-(void)fetchReturnLineItems:(Review_Documents*)orderHistory
{
    NSMutableArray * currentOrderHistoryLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderItems:orderHistory.Doc_No] mutableCopy];
    NSMutableArray *unfilteredOrderedLineItems = [[NSMutableArray alloc]init];
    
    Customer *selectedCustomer = [[Customer alloc]init];
    selectedCustomer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc]init];
    selectedCustomer.customerOrderHistoryInvoiceLineItemsArray=[[NSMutableArray alloc]init];
    
    if (currentOrderHistoryLineItemsArray.count>0) {
        
        NSLog(@"order line item dict %@", currentOrderHistoryLineItemsArray);
        
        selectedCustomer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc] init];
        for (NSMutableDictionary * currentLineItemDict in currentOrderHistoryLineItemsArray)
        {
            CustomerOrderHistoryLineItems * custOrderHistoryLineItems=[[CustomerOrderHistoryLineItems alloc]init];
            custOrderHistoryLineItems.Calc_Price_Flag=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Calc_Price_Flag"]];
            custOrderHistoryLineItems.Description=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Description"]];
            custOrderHistoryLineItems.Inventory_Item_ID=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Inventory_Item_ID"]];
            custOrderHistoryLineItems.Item_Code=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Item_Code"]];
            custOrderHistoryLineItems.Order_Quantity_UOM=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Order_Quantity_UOM"]];
            custOrderHistoryLineItems.Ordered_Quantity=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Ordered_Quantity"]];
            custOrderHistoryLineItems.Unit_Selling_Price=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Unit_Selling_Price"]];
            custOrderHistoryLineItems.Item_Value=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Item_Value"]];
            
            [selectedCustomer.customerOrderHistoryLineItemsArray addObject:custOrderHistoryLineItems];
            
            [unfilteredOrderedLineItems addObject:custOrderHistoryLineItems];
        }
    }
    
    SWCustomerReturnHistoryDescriptionViewController * orderHistoryDescVC=[[SWCustomerReturnHistoryDescriptionViewController alloc]init];
    orderHistoryDescVC.orderLineItems=unfilteredOrderedLineItems;
    orderHistoryDescVC.status=orderHistory.Shipping_Instructions;
    orderHistoryDescVC.orderNumber=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",orderHistory.Doc_No]];
    orderHistoryDescVC.selectedCustomer=selectedCustomer;
    orderHistoryDescVC.view.backgroundColor = KPopUpsBackGroundColor;
    orderHistoryDescVC.modalPresentationStyle = UIModalPresentationCustom;
    [self.navigationController presentViewController:orderHistoryDescVC animated:NO completion:nil];
}
-(void)fetchOrderLineItems:(Review_Documents*)orderHistory
{
    NSMutableArray * currentOrderHistoryLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderItems:orderHistory.Doc_No] mutableCopy];
    NSMutableArray *unfilteredOrderedLineItems = [[NSMutableArray alloc]init];
    
    Customer *selectedCustomer = [[Customer alloc]init];
    
    selectedCustomer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc]init];
    selectedCustomer.customerOrderHistoryInvoiceLineItemsArray=[[NSMutableArray alloc]init];
    
    if (currentOrderHistoryLineItemsArray.count>0) {
        
        NSLog(@"order line item dict %@", currentOrderHistoryLineItemsArray);

        selectedCustomer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc] init];
        for (NSMutableDictionary * currentLineItemDict in currentOrderHistoryLineItemsArray) {
            
            CustomerOrderHistoryLineItems * custOrderHistoryLineItems=[[CustomerOrderHistoryLineItems alloc]init];
            custOrderHistoryLineItems.Calc_Price_Flag=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Calc_Price_Flag"]];
            custOrderHistoryLineItems.Description=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Description"]];
            custOrderHistoryLineItems.Inventory_Item_ID=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Inventory_Item_ID"]];
            custOrderHistoryLineItems.Item_Code=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Item_Code"]];
            custOrderHistoryLineItems.Order_Quantity_UOM=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Order_Quantity_UOM"]];
            custOrderHistoryLineItems.Ordered_Quantity=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Ordered_Quantity"]];
            custOrderHistoryLineItems.Unit_Selling_Price=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Unit_Selling_Price"]];
            custOrderHistoryLineItems.Item_Value=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Item_Value"]];
            
            [selectedCustomer.customerOrderHistoryLineItemsArray addObject:custOrderHistoryLineItems];
            
            [unfilteredOrderedLineItems addObject:custOrderHistoryLineItems];
        }
    }
    
    
    NSLog(@"history line items are %@", selectedCustomer.customerOrderHistoryLineItemsArray);
    
    NSLog(@"fetching line items for customer %@", selectedCustomer.Customer_Name);
    //fetch invoiced items
    
    NSMutableArray* invoicedLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderInvoice:orderHistory.Doc_No] mutableCopy];
    NSLog(@"invoices line items are %@", invoicedLineItemsArray);
    
    
    NSMutableArray *unfilteredInvoiceLineItems = [[NSMutableArray alloc]init];
    selectedCustomer.customerOrderHistoryInvoiceLineItemsArray=[[NSMutableArray alloc]init];
    
    for (NSMutableDictionary * tempDict in invoicedLineItemsArray) {
        
        NSMutableDictionary*  invoiceItemsDict=[NSMutableArray nullFreeDictionaryWithDictionary:tempDict];
        
        CustomerOrderHistoryInvoiceLineItems * invoiceLineItems=[[CustomerOrderHistoryInvoiceLineItems alloc]init];
        invoiceLineItems.Calc_Price_Flag=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Calc_Price_Flag"]];
        invoiceLineItems.Description=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Description"]];
        invoiceLineItems.Expiry_Date=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Expiry_Date"]];
        invoiceLineItems.Inventory_Item_ID=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Inventory_Item_ID"]];
        invoiceLineItems.Item_Code=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Item_Code"]];
        invoiceLineItems.Item_Value=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Item_Value"]];
        invoiceLineItems.Order_Quantity_UOM=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Order_Quantity_UOM"]];
        invoiceLineItems.Ordered_Quantity=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Ordered_Quantity"]];
        invoiceLineItems.Unit_Selling_Price=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Unit_Selling_Price"]];
        invoiceLineItems.ERP_Ref_No=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"ERP_Ref_No"]];
        
        
        //fetch creation date
        
        NSString* creationDate=[[SWDatabaseManager retrieveManager]fetchCreationDateforInvoice:invoiceLineItems.ERP_Ref_No];
        
        NSLog(@"creation Date is %@", creationDate);
        
        invoiceLineItems.Order_Date=creationDate;
        [selectedCustomer.customerOrderHistoryInvoiceLineItemsArray addObject:invoiceLineItems];
        [unfilteredInvoiceLineItems addObject:invoiceLineItems];
    }
    
    SWCustomerOrderHistoryDescriptionViewController * orderHistoryDescVC=[[SWCustomerOrderHistoryDescriptionViewController alloc]init];
    orderHistoryDescVC.orderLineItems=unfilteredOrderedLineItems;
    orderHistoryDescVC.invoiceLineItems=unfilteredInvoiceLineItems;
    orderHistoryDescVC.status=orderHistory.Shipping_Instructions;
    orderHistoryDescVC.orderNumber=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",orderHistory.Doc_No]];
    orderHistoryDescVC.selectedCustomer=selectedCustomer;
    orderHistoryDescVC.view.backgroundColor = KPopUpsBackGroundColor;
    orderHistoryDescVC.modalPresentationStyle = UIModalPresentationCustom;
    [self.navigationController presentViewController:orderHistoryDescVC animated:NO completion:nil];
}

#pragma  mark textfield methods
-(BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{
    if(textField == txtCustomer)
    {
        [self textFieldTapped:textField :kCustomerNamePopOverTitle :arrCustomerName :400];
        return NO;
    }
    else if(textField == DateRangeTextField)
    {
        SalesWorxReportsDateSelectorViewController * popOverVC=[[SalesWorxReportsDateSelectorViewController alloc]init];
        popOverVC.delegate=self;
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(450, 570);
        popover.delegate = self;
        popover.sourceView = DateRangeTextField.rightView;
        popover.sourceRect = DateRangeTextField.dropdownImageView.frame;
        popover.permittedArrowDirections = UIPopoverArrowDirectionLeft;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
        return NO;
    }
    else if(textField == txtDocumentType)
    {
        [self textFieldTapped:textField :kDocumentTypePopOverTitle :arrDocumentType :266];
        return NO;
    }
    return YES;
}

#pragma  mark textfield tapped method

-(void)textFieldTapped:(MedRepTextField *)txtField :(NSString *)strPop :(NSMutableArray *)arr :(CGFloat )width
{
    txtCustomer.enabled = YES;
    popString = strPop;
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=arr;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    if([popString isEqualToString:kCustomerNamePopOverTitle])
    {
        popOverVC.disableSearch=NO;
    }
    
    if([strPop isEqualToString:kDocumentTypePopOverTitle])
    {
        popOverVC.enableArabicTranslation=YES;
    }
    
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(width, 273);
    popover.delegate = self;
    popOverVC.titleKey=popString;
    popover.sourceView = txtField.rightView;
    popover.sourceRect =txtField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    
    [self presentViewController:nav animated:YES completion:^{
    }];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popString isEqualToString:kCustomerNamePopOverTitle])
    {
        txtCustomer.text = [NSString stringWithFormat:@"%@",[arrCustomerName objectAtIndex:selectedIndexPath.row]];
    }
    else if ([popString isEqualToString:kDocumentTypePopOverTitle])
    {
        txtDocumentType.text = [NSString stringWithFormat:@"%@",[arrDocumentType objectAtIndex:selectedIndexPath.row]];
    }
}
#pragma mark Pie chart delegate methods

-(void)loadPieChart:(NSArray *)arr
{
    if ([arr count] == 0) {
        pieChartPlaceholderImage.hidden = NO;
        parentViewOfPieChart.hidden = YES;
    } else {
        pieChartPlaceholderImage.hidden = YES;
        parentViewOfPieChart.hidden = NO;
    }
    
    [_slicesForCustomerPotential removeAllObjects];
    [customerPieChart reloadData];
    [tblReviewDocuments reloadData];
    
    int countOrder = 0, countCollection = 0, countReturn = 0;
    double salesAmount = 0.0f;
    for (Review_Documents *currentCustomer in arr) {
        salesAmount = salesAmount + [currentCustomer.Amount doubleValue];
        if ([currentCustomer.Doc_Type isEqualToString:@"Order"]){
            countOrder++;
        }
        else if ([currentCustomer.Doc_Type isEqualToString:@"Collection"]){
            countCollection++;
        }
        else{
            countReturn++;
        }
    }
    lblTotalAmount.text = [[NSString stringWithFormat:@"%f",salesAmount] currencyString];
    
    [_slicesForCustomerPotential removeAllObjects];
    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:countCollection]];
    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:countOrder]];
    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:countReturn]];
    
    [customerPieChart reloadData];
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    if (pieChart == customerPieChart && self.slicesForCustomerPotential.count>0) {
        count = self.slicesForCustomerPotential.count;
    }
    return count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    if (pieChart == customerPieChart) {
        count = [[NSString stringWithFormat:@"%@",[self.slicesForCustomerPotential objectAtIndex:index]]floatValue];
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == customerPieChart) {
        colour = [self.sliceColorsForCustomerPotential objectAtIndex:(index % self.sliceColorsForCustomerPotential.count)];
    }
    return colour;
}

#pragma  mark button tapped method

- (IBAction)btnSearch:(id)sender {
    NSMutableArray *predicateArrayForCollection = [NSMutableArray array];
    NSMutableArray *predicateArrayForOrderAndReturn = [NSMutableArray array];
    
    if (![txtCustomer.text isEqualToString:@"N/A"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF.Customer_Name",txtCustomer.text];
        NSMutableArray *filteredArray = [[arrCustomersDetails filteredArrayUsingPredicate:predicate] mutableCopy];
        
        if ([filteredArray count] > 0) {
            Customer *selectdCustomer = [filteredArray objectAtIndex:0];
            [predicateArrayForOrderAndReturn addObject:[NSPredicate predicateWithFormat:@"Ship_To_Customer_Id == %d and Ship_To_Site_Id == %d",[selectdCustomer.Customer_ID intValue], [selectdCustomer.Ship_Site_Use_ID intValue]]];
            [predicateArrayForCollection addObject:[NSPredicate predicateWithFormat:@"Ship_To_Customer_Id == %d",[selectdCustomer.Customer_ID intValue]]];
        }
    }
    
    [predicateArrayForOrderAndReturn addObject:[NSPredicate predicateWithFormat:@"(Doc_Date >= %@) AND (Doc_Date <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]]];
    [predicateArrayForCollection addObject:[NSPredicate predicateWithFormat:@"(Doc_Date >= %@) AND (Doc_Date <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]]];
    if (![txtDocumentType.text isEqualToString:@"N/A"])
    {
        if ([txtDocumentType.text isEqualToString:@"All"]){
            [predicateArrayForOrderAndReturn addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Collection' or SELF.Doc_Type ==[cd] 'Order' or SELF.Doc_Type ==[cd] 'Return'"]];
            [predicateArrayForCollection addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Collection' or SELF.Doc_Type ==[cd] 'Order' or SELF.Doc_Type ==[cd] 'Return'"]];
        }
        else if ([txtDocumentType.text isEqualToString:@"Collection"]) {
            [predicateArrayForOrderAndReturn addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Collection'"]];
            [predicateArrayForCollection addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Collection'"]];
        }
        else if ([txtDocumentType.text isEqualToString:@"Sales Order"]) {
            [predicateArrayForOrderAndReturn addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Order'"]];
            [predicateArrayForCollection addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Order'"]];
        }
        else if ([txtDocumentType.text isEqualToString:@"Sales Returns"]){
            [predicateArrayForOrderAndReturn addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Return'"]];
            [predicateArrayForCollection addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'Return'"]];
        }
    }
    
    NSPredicate *predicateForOrderAndReturn = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArrayForOrderAndReturn];
    NSLog(@"predicate for Order And Return is %@", [predicateForOrderAndReturn description]);
    
    NSPredicate *predicateForCollection = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArrayForCollection];
    NSLog(@"predicate for collection is %@", [predicateForCollection description]);
    
    NSMutableArray* filteredArray = [[arrOrderAndReturnDocuments filteredArrayUsingPredicate:predicateForOrderAndReturn] mutableCopy];
    NSArray *filteredCollectionDocuments = [arrCollectionDocuments filteredArrayUsingPredicate:predicateForCollection];
    
    [filteredArray addObjectsFromArray:filteredCollectionDocuments];
    if (filteredArray.count == 0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
    }
    arrReviewDocuments = filteredArray;
    [self loadPieChart:arrReviewDocuments];
}
- (IBAction)btnReset:(id)sender {
    txtCustomer.text = @"N/A";
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:KUSLOCALE];
    [df setDateFormat:kDateFormatWithoutTime];

    txtDocumentType.text = @"N/A";

    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    DateRangeTextField.SelecetdCustomDateRangeText = nil;
    [DateRangeTextField SetCustomDateRangeText];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(Doc_Date >= %@) AND (Doc_Date <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]];
    NSMutableArray* filteredArray = [[unfilteredReviewDocumentsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    arrReviewDocuments = filteredArray;
    [self loadPieChart:arrReviewDocuments];
}

-(void)DidUserSelectDateRange:(NSDate *)StartDate EndDate:(NSDate *)EndDate AndCustomRangePeriodText:(NSString *)customPeriodText{
    
    DateRangeTextField.StartDate = StartDate;
    DateRangeTextField.EndDate = EndDate;
    DateRangeTextField.SelecetdCustomDateRangeText = customPeriodText;
    
    [DateRangeTextField SetCustomDateRangeText];
    
    NSLog(@"StartDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate]);
    NSLog(@"EndDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
