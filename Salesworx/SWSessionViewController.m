//
//  SWSessionViewController.m
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWSessionViewController.h"
#import "SWFoundation.h"
#import "SWPlatform.h"
#import "NewActivationViewController.h"
#import "SWSessionConstants.h"
#import "UIDevice+IdentifierAddition.h"


#define kLoginField 1
#define kPasswordField 2

@interface SWSessionViewController ()
- (void)showActivateNewUserView;
- (void)doLogin;
@end
@implementation SWSessionViewController


- (id)init {
    self = [super init];
    
    if (self) {
        
        AppControl *appCtrl=[AppControl retrieveSingleton];
        if ([appCtrl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
            
            //[self setTitle:@"Saleswars"];
            [self setTitle:@"MedRep"];
            
        }
        else
        {
            [self setTitle:@"SalesWorx"];
            
        }
        
        //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [Flurry logEvent:@"Login View"];
    //[Crittercism leaveBreadcrumb:@"<Login View>"];

    //NSLog(@"GUID %@",[NSString createGuid]);
    loadingView=[[SWLoadingView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] ;
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    loadingView.loadingLabel.text = @"Please Wait..";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
    self.view.backgroundColor = [UIColor whiteColor];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
//    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    NSString * avid = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];

    
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    
    
    NSLog(@"updating client version %@", [NSString stringWithFormat:@"%@(%@)", version,build]);
    
    ClientVersion =[NSString stringWithFormat:@"%@(%@)", version,build];
    
   
    NSLog(@"Client Version in session %@",ClientVersion);
    
   tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped ];
    
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [tableView setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 200)] ];
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tableView];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Activate New User", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showActivateNewUserView)] ];
    
    if ([SWDefaults username].length > 0) {
        login = [SWDefaults username];
        password = [SWDefaults password];
    }
}



- (void)showActivateNewUserView {
    NewActivationViewController *newActivationViewController = [[NewActivationViewController alloc] init] ;
    [self.navigationController pushViewController:newActivationViewController animated:YES];
}

- (void)doLogin {

}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *IDENT = @"SessionCellIdent";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:IDENT];
    
    if (!cell) {
        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        
        if (indexPath.row == 0) {
            ((SWTextFieldCell *)cell).textField.placeholder = @"username";
            ((SWTextFieldCell *)cell).textField.tag = kLoginField;
            ((SWTextFieldCell *)cell).textField.text = login;
        } else {
            ((SWTextFieldCell *)cell).textField.placeholder = @"password";
            ((SWTextFieldCell *)cell).textField.secureTextEntry = YES;
            ((SWTextFieldCell *)cell).textField.tag = kPasswordField;
            ((SWTextFieldCell *)cell).textField.text = password;
        }

        ((SWTextFieldCell *)cell).textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        ((SWTextFieldCell *)cell).textField.returnKeyType = UIReturnKeyGo;
        ((SWTextFieldCell *)cell).textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        ((SWTextFieldCell *)cell).textField.delegate = self;
    }
    
    return cell;
}
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
   
    return  [NSString stringWithFormat:@"Powered by: Unique Computer Systems        Version : %@ ",ClientVersion];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"Login", nil);
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
  GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:NSLocalizedString(@"Login", nil)] ;
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)s {
    GroupSectionFooterView *footerView = [[GroupSectionFooterView alloc] initWithWidth:tv.bounds.size.width text:[NSString stringWithFormat:@"Powered by: Unique Computer Systems \n Version : %@ ",ClientVersion]] ;
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 50.0f;
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField.tag == kLoginField) {
        login=textField.text;
    } else {
       password=textField.text;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (login.length > 0 && password.length > 0) {
        // login
       NSString *temp  = [[SWDatabaseManager retrieveManager] verifyLogin:login andPassword:password];
        if ([temp isEqualToString:@"Done"])
        {
            [SWDefaults setUsername:login];
            [SWDefaults setUsernameForActivate:login];
            
            [SWDefaults setPassword:password];
            [SWDefaults setPasswordForActivate:password];
            
            SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
            if([syncObject licenseVerification])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kSessionLoginDoneNotification object:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"License validation error", nil) message:@"You license has been expired or contact your administrator for further assistance " delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil)      otherButtonTitles:nil];
                [alert show];
                
//                [ILAlertView showWithTitle:@"License validation error"
//                                   message:@"You license has been expired or contact your administrator for further assistance "
//                          closeButtonTitle:NSLocalizedString(@"OK", nil)
//                         secondButtonTitle:nil
//                       tappedButtonAtIndex:^(NSInteger buttonIndex) {
//                           [[NSNotificationCenter defaultCenter] postNotificationName:kSessionActivationErrorNotification object:nil];
//                           
//                       }];
                
            }
    
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:temp delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil)      otherButtonTitles:nil];
            [alert show];
            
//            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                               message:temp
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:nil];
        }
        return YES;
    }
    
    // if the e-mail address has a value but the password doesn't, select the password
    if ([login length] > 0) {
        [[(SWTextFieldCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]] textField] becomeFirstResponder];
    }
    // if the password has a value but the email doesn't...
    else {
        [[(SWTextFieldCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] textField] becomeFirstResponder];
    }

    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == [alertView cancelButtonIndex])
    {
         [[NSNotificationCenter defaultCenter] postNotificationName:kSessionActivationErrorNotification object:nil];

    }
    // else do your stuff for the rest of the buttons (firstOtherButtonIndex, secondOtherButtonIndex, etc)
}


-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end