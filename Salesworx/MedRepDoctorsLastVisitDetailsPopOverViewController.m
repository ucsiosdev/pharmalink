//
//  MedRepDoctorsLastVisitDetailsPopOverViewController.m
//  MedRep
//
//  Created by Unique Computer Systems on 12/7/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "MedRepDoctorsLastVisitDetailsPopOverViewController.h"

@interface MedRepDoctorsLastVisitDetailsPopOverViewController ()

@end

@implementation MedRepDoctorsLastVisitDetailsPopOverViewController
@synthesize previousVisitNotesTextView,notesString,lastVisitDetailsDict,lastVisitNotesLabel,nextVisitObjectiveLabel,lastVisitDetailsSegmentController,contentTextView;
- (void)viewDidLoad {
    [super viewDidLoad];
    previousVisitNotesTextView.text=notesString;
    contentTextView.text = notesString;
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Previous Visit Details"];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
    lastVisitNotesLabel.text=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Notes"]];
    nextVisitObjectiveLabel.text=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Next_Visit_Objective"]];
    
    UIBarButtonItem * closebutton = [[UIBarButtonItem alloc ]initWithTitle:NSLocalizedString(kCloseTitle,nil) style:UIBarButtonItemStylePlain target:self action:@selector(CloseButtonTapped)];
    [closebutton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:kSWX_FONT_SEMI_BOLD(14),NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = closebutton;
    
    
    if (self.isMovingToParentViewController) {
        
        nextVisitObjectiveTitle = [MedRepQueries fetchDescriptionForAppCode:kNextVisitObjectiveTitleAppCode];
        if ([NSString isEmpty:[SWDefaults getValidStringValue:nextVisitObjectiveTitle]])
        {
            nextVisitObjectiveTitle = kNextVisitObjective;
        }
        lastVisitDetailsSegmentController.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
        lastVisitDetailsSegmentController.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
        if(self.isVisitScreen)
        {
            lastVisitDetailsSegmentController.sectionTitles = @[NSLocalizedString(@"Last Visit Notes", nil)];
        }else{
        lastVisitDetailsSegmentController.sectionTitles = @[NSLocalizedString(@"Last Visit Notes", nil),NSLocalizedString(kNextVisitObjective, nil)];
        }
        
        lastVisitDetailsSegmentController.selectionIndicatorBoxOpacity=0.0f;
        lastVisitDetailsSegmentController.verticalDividerEnabled=YES;
        lastVisitDetailsSegmentController.verticalDividerWidth=1.0f;
        lastVisitDetailsSegmentController.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
        
        lastVisitDetailsSegmentController.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
        lastVisitDetailsSegmentController.selectionStyle = HMSegmentedControlSelectionStyleBox;
        lastVisitDetailsSegmentController.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        lastVisitDetailsSegmentController.tag = 3;
        lastVisitDetailsSegmentController.selectedSegmentIndex = 0;
        
        lastVisitDetailsSegmentController.layer.shadowColor = [UIColor blackColor].CGColor;
        lastVisitDetailsSegmentController.layer.shadowOffset = CGSizeMake(2, 2);
        lastVisitDetailsSegmentController.layer.shadowOpacity = 0.1;
        lastVisitDetailsSegmentController.layer.shadowRadius = 1.0;
        
        contentTextView.text=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Notes"]];

        
        [lastVisitDetailsSegmentController setIndexChangeBlock:^(NSInteger index) {

            if (index==0) {
                contentTextView.text=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Notes"]];
            }
            else{
                contentTextView.text=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Next_Visit_Objective"]];
            }
            
        }];
    }
    
    contentTextView.text = notesString;
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}
-(void)viewWillLayoutSubviews{
    
    [super viewWillLayoutSubviews];

}

-(void)CloseButtonTapped{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
