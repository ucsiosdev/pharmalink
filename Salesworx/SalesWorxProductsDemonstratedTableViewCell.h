//
//  SalesWorxProductsDemonstratedTableViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/15/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxProductsDemonstratedTableViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblProductName;

@end
