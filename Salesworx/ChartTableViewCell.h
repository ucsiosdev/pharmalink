//
//  ChartTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/11/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UUChart.h"

@interface ChartTableViewCell : UITableViewCell<UUChartDataSource>

{
    NSIndexPath *path;
    UUChart *chartView;
    NSMutableArray* agencyArray;
    NSMutableArray* agencyDetails;

}

@property(strong,nonatomic)NSString* customerDetail;
@property(strong,nonatomic)NSMutableDictionary* customerDict;
- (void)configUI:(NSIndexPath *)indexPath;

@end
