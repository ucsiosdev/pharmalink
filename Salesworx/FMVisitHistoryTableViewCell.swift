//
//  FMVisitHistoryTableViewCell.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/6/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit


class FMVisitHistoryTableViewCell: UITableViewCell {

    @IBOutlet var doctorNameLabel: UILabel!
    @IBOutlet var locationNameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeSpentLabel: UILabel!
    @IBOutlet var productsDemonstratedLabel: UILabel!
    @IBOutlet var samplesGivenLabel: UILabel!
    @IBOutlet var tasksLabel: UILabel!
    @IBOutlet var notesLabel: UILabel!
    @IBOutlet var previousVisitNotesLabel: UILabel!
    @IBOutlet var nextVisitObjectiveLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //[UIFont fontWithName:@"WeblySleekUISemibold" size:s]
        
        doctorNameLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        locationNameLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        dateLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        timeSpentLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        productsDemonstratedLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        samplesGivenLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        tasksLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        notesLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        previousVisitNotesLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)
        nextVisitObjectiveLabel.font = UIFont(name: "WeblySleekUISemibold", size: 16)

        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
