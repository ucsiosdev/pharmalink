//
//  MedRepVisitOptionsViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 2/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "MedRepVisitOptionsViewController.h"
#import "MedRepVisitOptionsCollectionViewCell.h"
#import "MedRepVisitsStartMultipleCallsViewController.h"
#import "MedRepDistributionCheckViewController.h"

@interface MedRepVisitOptionsViewController ()

@end

@implementation MedRepVisitOptionsViewController
@synthesize isFromDashboard, visitDetails, visitDetailsDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Visit Options", nil)];
    
    [visitOptionsCollectionView registerClass:[MedRepVisitOptionsCollectionViewCell class] forCellWithReuseIdentifier:@"visitOptionsCell"];
    visitOptionsCollectionViewArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *eDetailingDict = [[NSMutableDictionary alloc]init];
    [eDetailingDict setValue:kEDetailingTitle forKey:@"Title"];
    [eDetailingDict setValue:@"FM_VisitOptions_eDetailing_Active" forKey:@"ThumbNailImage"];
    [visitOptionsCollectionViewArray addObject:eDetailingDict];
    
    NSMutableDictionary *distributionCheckDict = [[NSMutableDictionary alloc]init];
    [distributionCheckDict setValue:kStockCheckTitle forKey:@"Title"];
    [distributionCheckDict setValue:@"VisitOptions_DC_Active" forKey:@"ThumbNailImage"];
    [visitOptionsCollectionViewArray addObject:distributionCheckDict];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingVisitOptions];
        
        
        UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close Visit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeVisitTapped)];
        [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem=closeVisitButton;
    }
    
    [visitOptionsCollectionView reloadData];
}

-(void)closeVisitTapped
{
    if (isEdetailingDone) {
        BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
        if (!deviceTimeisValid)
        {
            [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
        }
        else
        {
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       }];
            
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)] ,nil];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Close Visit" andMessage:@"Would you like to close this visit?" andActions:actionsArray withController:self];
        }
    } else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"eDetailing Mandatory" andMessage:@"please complete eDetailing before closing visit." withController:self];
    }
}

#pragma mark UICOllectionview methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  visitOptionsCollectionViewArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(190, 190);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 20.0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"visitOptionsCell";
    
    MedRepVisitOptionsCollectionViewCell *cell = (MedRepVisitOptionsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.optionTitleLbl.text = [[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"Title"] ;
    cell.optionImageView.image = [UIImage imageNamed:[[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"ThumbNailImage"]];
    cell.optionCompletionImageView.image=nil;
    
    cell.optionTitleLbl.textColor=[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1];
    cell.layer.borderColor=[kVisitOptionsBorderColor CGColor];
    cell.layer.shadowColor = [kVisitOptionsBorderColor CGColor];

    
    if(([cell.optionTitleLbl.text isEqualToString:kEDetailingTitle] && isEdetailingDone) || ([cell.optionTitleLbl.text isEqualToString:kStockCheckTitle] && [[visitDetailsDict valueForKey:@"StockCheckDone"] isEqualToString:@"Yes"])) {
        cell.optionTitleLbl.textColor = [UIColor colorWithRed:(170.0/255.0) green:(170.0/255.0) blue:(170.0/255.0) alpha:1.0];
        cell.layer.borderColor = [kVisitOptionsDisbaleBorderColor CGColor];
        cell.layer.shadowColor = [kVisitOptionsDisbaleBorderColor CGColor];
        cell.userInteractionEnabled = NO;
        cell.optionCompletionImageView.image=[UIImage imageNamed:@"VisitOptions_TickMark"];
        
        if([cell.optionTitleLbl.text isEqualToString:kEDetailingTitle]){
            cell.layerView.backgroundColor=[UIColor clearColor];
            cell.optionImageView.image=[UIImage imageNamed:@"FM_VisitOptions_eDetailing_InActive"];
        } else {
            cell.layerView.backgroundColor=[UIColor clearColor];
            cell.optionImageView.image=[UIImage imageNamed:@"VisitOptions_DC_InActive"];
        }
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedCellTitle = [[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"Title"];
    

    if ([selectedCellTitle isEqualToString:kStockCheckTitle]) {
        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString *StockCheck_StartTime = [formatterTime stringFromDate:[NSDate date]];
        
        MedRepDistributionCheckViewController *startDC = [[MedRepDistributionCheckViewController alloc]init];
        [visitDetailsDict setObject:StockCheck_StartTime forKey:@"StockCheck_StartTime"];
        startDC.visitDetailsDict = visitDetailsDict;
        startDC.visitDetails = visitDetails;
        [self.navigationController pushViewController:startDC animated:YES];
    }
    else if ([selectedCellTitle isEqualToString:kEDetailingTitle])
    {
        isEdetailingDone = YES;
        MedRepVisitsStartMultipleCallsViewController *startEdetailing = [[MedRepVisitsStartMultipleCallsViewController alloc]init];
        startEdetailing.visitDetailsDict = visitDetailsDict;
        startEdetailing.visitDetails = visitDetails;
        startEdetailing.isFromDashboard = isFromDashboard;
        [self.navigationController pushViewController:startEdetailing animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
