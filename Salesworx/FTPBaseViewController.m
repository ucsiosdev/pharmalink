//
//  FTPBaseViewController.m
//  FastTrackParking
//
//  Created by Saqib Khan on 10/30/13.
//  Copyright (c) 2013 Saqib Khan. All rights reserved.
//

#import "FTPBaseViewController.h"
#import "SWFoundation.h"
@interface FTPBaseViewController ()

@end

@implementation FTPBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        //
        //        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        //
        //        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    

    
 
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
   
}

- (BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
