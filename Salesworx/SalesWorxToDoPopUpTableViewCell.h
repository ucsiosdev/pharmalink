//
//  SalesWorxToDoPopUpTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 10/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTitleLabel2.h"
#import "SalesWorxImageView.h"
@interface SalesWorxToDoPopUpTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *cellDividerImg;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *accessoryImageView;

@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) IBOutlet SalesWorxTitleLabel2 *customerLabel;

@end
