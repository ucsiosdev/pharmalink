//
//  SWCustomersViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/6/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "SalesWorxCustomClass.h"
#import "SWViewController.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "XYPieChart/XYPieChart.h"
#import "MedRepElementDescriptionLabel.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepTextField.h"
#import "NSObject+NullDictionary.h"
//#import <BNPieChart.h>
#import <PCPieChart.h>
#import <MapKit/MapKit.h>
#import "SWAppDelegate.h"
#import "SWCustomersFilterViewController.h"
#import "SWCustomerPriceListFilterViewController.h"
#import "SWDefaults.h"
#import "ZUUIRevealController.h"
#import <AVFoundation/AVFoundation.h>
#import "SalesWorxTableView.h"
#import "SalesWorxSingleLineLabel.h"
#import "MedRepPanelTitle.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxImageView.h"
#import "SWCustomerOrderHistoryDescriptionViewController.h"
#import "SalesWorxImageBarButtonItem.h"
#import "SalesWorxiBeaconManager.h"
#import "SalesWorxItemDividerLabel.h"
#import "LastVisitDetailsViewController.h"
#import "SalesWorxCustomerOnsiteVisitOptionsReasonViewController.h"
#import "OpenVisitAlertViewController.h"
#import "LGAlertView.h"

@interface SWCustomersViewController : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,SWFilteredCustomersDelegate,CustomerPriceListDelegate,XYPieChartDataSource,XYPieChartDelegate,ZUUIRevealControllerDelegate,ZBarReaderViewDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate,UIPopoverPresentationControllerDelegate,LGAlertViewDelegate>
{
    AppControl *appControl;
    
    NSIndexPath * selectedDoctorIndexPath;
    IBOutlet UIView *customerHeaderView;
    
    NSMutableArray * pieChartDataArray;
    NSMutableArray * openVisitsArray;
    NSInteger selectedReason;
    Customer* openVisitCustomer;
    UIAlertController * openVisitAlertController;
    
    NSString* orderStatus;
    IBOutlet UIView *creditLimitView;
    IBOutlet SalesWorxSingleLineLabel *statusLbl;
    
    NSMutableArray * categoriesArray;
    
    IBOutlet MKMapView *customerLocationMapView;
    Customer * selectedCustomer;
    
    NSDate * startTime;
    
    NSIndexPath * selectedIndexPath;
    
    IBOutlet UILabel *customerCreditLimitLbl;
    IBOutlet MedRepElementDescriptionLabel *customeraddressLbl;
    IBOutlet UILabel *customerAvailableBalanceLbl;
    
    IBOutlet MedRepElementDescriptionLabel *customerCityLbl;
    IBOutlet UILabel *customerOverDueLbl;
    
    
    IBOutlet SalesWorxImageView *customerStatusImageView;
    NSString* openVisitID;
    
    UIAlertView * openVisitAlert;
    
    NSString * openPlannedVisitID;
    
    NSString * openPlannedDetailID;
    
    SalesWorxVisit * salesWorxVisit;
    
    IBOutlet UILabel *customerTotalOutStandingLbl;
    IBOutlet MedRepElementDescriptionLabel *customerPoBoxLbl;
    
    
    IBOutlet MedRepElementDescriptionLabel *customerTargetValueLbl;
    
    
    IBOutlet MedRepElementDescriptionLabel *customerSalesValueLbl;
    
    
    IBOutlet MedRepElementDescriptionLabel *customerBalanceToGoLbl;
    
    BOOL hideDivider;
    
    ZBarReaderViewController * reader;
    
    IBOutlet MedRepTextField *brandCategoryTextField;
    
    UIPopoverController* popOverController;
    
    StringPopOverViewController_ReturnRow* locationPopOver;
    
    NSString * popString;
    
    IBOutlet SalesWorxSingleLineLabel *totalOrdersLbl;
    BOOL isSearching;
    
    IBOutlet SalesWorxSingleLineLabel *totalOrdervalueLbl;
    NSMutableArray * filteredCustomers;
    
    UIImageView * blurredBgImage;
    NSMutableDictionary * previousFilteredParameters;
    UIPopoverController *filterPopOverController;
    
    NSMutableArray * unfilteredCustomersArray;
    NSMutableArray * customerOrderHistoryArray;
    
    NSMutableArray* customerPriceListArray;

    NSMutableArray* filteredCustomerPriceListArray;

    NSMutableDictionary * previousFilterParameters;
    NSMutableDictionary * previousPriceListFilterParameters;
    
    IBOutlet UISegmentedControl *lineItemsSegment;
    
    IBOutlet UIView *orderHistoryDetailsView;
    
    IBOutlet UITableView *customerOrderHistoryTableView;
    
    IBOutlet UITableView *priceListTableView;
    
    IBOutlet UITableView *orderHistoryDescriptionTableView;
    IBOutlet MedRepPanelTitle *orderHistoryLineItemsOrderNumberLbl;
    IBOutlet UIView *targetInfoView;
    
    
    NSMutableArray * unfilteredOrderedLineItems;
    NSMutableArray* unfilteredInvoiceLineItems;
    
    NSInteger invoicedItemsIndex;
    
    IBOutlet MedRepElementDescriptionLabel *telephoneNumberLbl;
    UIPopoverController * invoicePopOverController;
    IBOutlet MedRepElementTitleLabel *customerNameTitle;
    
    IBOutlet MedRepElementTitleLabel *brandCategoryTitleLbl;
    NSDate *methodStart;
    SalesWorxImageBarButtonItem* startVisitButton;
    
    IBOutlet MedRepElementDescriptionLabel *areaNameLabel;
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *segmentViewTopConstraint;
    IBOutlet MedRepElementTitleLabel *lastVisitedOnHeaderLabel;
    IBOutlet MedRepElementDescriptionLabel *lastVisitedDateLabel;
    IBOutlet UIImageView *NoTargetDataImageView;
    
    
    IBOutlet HMSegmentedControl *targetInformationSegment;
    IBOutlet SalesWorxItemDividerLabel *dividerImageofTargetInformation;
    IBOutlet MedRepPanelTitle *lblTargetInformation;
    IBOutlet MedRepElementDescriptionLabel *lblCollectionTargetMonth;
    IBOutlet MedRepElementTitleLabel *salesValueLabel;
    
    IBOutlet MedRepElementTitleLabel *TargetValueTitleLable;
    NSMutableArray *collectionTargetArrayForSelectedCustomer;
    IBOutlet UIButton *lastvisitDetailsButton;
}
@property (strong, nonatomic) IBOutlet PCPieChart *customerTargetPieChart;
@property (strong, nonatomic) IBOutlet MedRepHeader *detailsCustomerNameLbl;
@property (strong, nonatomic) IBOutlet MedRepProductCodeLabel *detailsCustomerCodeLbl;
@property (strong, nonatomic) IBOutlet MedRepProductCodeLabel *detailsCustomerClassLbl;
@property (strong, nonatomic) IBOutlet UITableView *customersTableView;

@property (strong, nonatomic) IBOutlet UIButton *customersFilterButton;
@property (strong, nonatomic) IBOutlet UIView *tempPieView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *orderHistoryLineItemsSegment;
- (IBAction)orderHistoryLineitemsSegmenttapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *priceListFilterButton;
- (IBAction)orderHistoryBackButtontapped:(id)sender;

- (IBAction)customersFilterButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UISearchBar *customersSearchBar;
@property(strong,nonatomic) IBOutlet HMSegmentedControl * segmentControl;
@property (nonatomic, strong) IBOutlet UIScrollView *customersScrollView;

@property (strong, nonatomic) IBOutlet UIView *OrderHistoryView;
@property (strong, nonatomic) IBOutlet UIView *priceListView;
@property (strong, nonatomic) IBOutlet UIView *detailsView;

@property(strong,nonatomic) NSMutableArray * customersArray;

@property(strong,nonatomic) NSMutableArray * indexPathArray;

@property(nonatomic) BOOL isFromDashBoard;
@property(nonatomic) BOOL isFromBeacon;
@property(strong,nonatomic) NSString * matchedCustomerIdFromBeacon;


@property(strong,nonatomic) Customer * selectedCustomerFromDashBoard;

@property (strong, nonatomic) IBOutlet XYPieChart *pieChartForCustomerPotential;
@property(nonatomic, strong) NSMutableArray *slicesForCustomerPotential;
@property(nonatomic, strong) NSArray        *sliceColorsForCustomerPotential;
- (IBAction)lastVisitDetailsButtonTapped:(id)sender;


@end
