//
//  CommunicationPopoverViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/21/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "CommunicationPopoverViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SWPlatform.h"
#import "MedRepQueries.h"
@interface CommunicationPopoverViewController ()

@end

@implementation CommunicationPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    recipientDetailsArray=[[NSMutableArray alloc]init];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"New Message"];
    self.navigationController.navigationBar.topItem.title = @"";
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=closeButton;
    
    
//    _btnSend.layer.cornerRadius = 4.0;
//    [_btnSend.layer setMasksToBounds:YES];
    
    _lblDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[self getCurrentDate]];
    alertType=nil;
    alertType =[[NSString alloc] init];
    
    //Get Incoming Mesage Count
    MessageID =[[SWDatabaseManager retrieveManager] dbGetIncomingMessageCount];
    MessageID ++;
    
    [self.txtMessage setTextContainerInset:UIEdgeInsetsMake(0, 3, 0, 0)];
}

-(void)closeTapped
{
    if ([_txtTo.text length]==0 && [_txtTitle.text length]==0 && [_txtMessage.text length]==0)
    {
        [_popOverController dismissPopoverAnimated:YES];
        
        if ([self.Delegate respondsToSelector:@selector(updateValues)])
        {
            [self.Delegate updateValues];
        }
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Would you like to cancel sending this message?", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes", nil),NSLocalizedString(@"No", nil), nil];
        alert.tag = 0;
        [alert show];
    }
}

- (IBAction)btnTo:(id)sender
{
    [_txtTitle resignFirstResponder];
    [_txtMessage resignFirstResponder];
    
    _txtTo.text=@"";
    
    _recipientPicker = [[RecipientPopoverViewController alloc]init];
    _recipientPicker.delegate = self;
    NSArray *arr = [[SWDatabaseManager retrieveManager] dbGetUsersList];
    if (arr.count>0)
    {
        [self.navigationController pushViewController:_recipientPicker animated:YES];
    }
    else
    {
        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unavailableAlert show];
    }
}
- (IBAction)btnSend:(id)sender
{
    
    if([_txtTo.text length]==0 ||[_txtTitle.text length]==0 || [_txtMessage.text length]==0)
    {
        alertType = @"InfoFailure";
        [self UserAlert:NSLocalizedString(@"Please enter all details", nil)];
        return;
    }
    
    
    if([[AppControl retrieveSingleton].MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        
        NSDictionary *messageDetailsDic=[[NSDictionary alloc]initWithObjectsAndKeys:_txtTitle.text,@"Msg_Title",_txtMessage.text,@"Msg_Body",recipientDetailsArray,@"recipientDetailsArray", nil];
        BOOL insertionstatus=[[SWDatabaseManager retrieveManager]InsertV2MessagesIntoDb:messageDetailsDic];
        if(insertionstatus)
        {
           // alertType=@"Success";
            
           // [self UserAlert:@"Message sent successfully"];
            

            [_popOverController dismissPopoverAnimated:YES];
            if ([self.Delegate respondsToSelector:@selector(updateValues)])
            {
                [self.Delegate updateValues];
                [SWDefaults showAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Message sent successfully" withController:self.Delegate];
                
            }
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to save data" withController:self];
        }
    }
    else{
        
        NSString *strDate = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
          for (int i=0;i<[recipientIDArray count]; i++)
        {
            NSInteger RcptUserID =[[recipientIDArray objectAtIndex:i] integerValue];
            NSString *strQuery = [NSString stringWithFormat:@"insert into TBL_Incoming_Messages(Message_ID,SalesRep_ID,Emp_Code,Rcpt_User_ID,Message_title,Message_Content,Message_date) values('%@',%d,'%@',%d,'%@','%@','%@')" ,[NSString createGuid],[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue],[[SWDefaults userProfile] stringForKey:@"Emp_Code"],RcptUserID,_txtTitle.text,_txtMessage.text,strDate];
            
            [[SWDatabaseManager retrieveManager] InsertdataCustomerResponse:strQuery];
        }
        alertType=@"Success";
        
        [self UserAlert:@"Message sent successfully"];
        [_popOverController dismissPopoverAnimated:YES];
        if ([self.Delegate respondsToSelector:@selector(updateValues)])
        {
            [self.Delegate updateValues];
        }
     }
    

}
-(void)PopOverDonePressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    _recipientPicker = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}
#pragma mark -
#pragma mark Recipient PopOverViewDelegate
- (void)RecipientSelected:(NSString *)Recipient RecipientId:(NSArray*)RecipientId RecipientDetails:(NSMutableArray *)RecipientDetails
{
    if ( [Recipient length] > 0)
        Recipient = [Recipient substringToIndex:[Recipient length] - 1];
    
    _txtTo.text=Recipient;
    recipientIDArray =[RecipientId copy];
    recipientDetailsArray=[RecipientDetails mutableCopy];
}

#pragma mark
#pragma mark TextView delegate method

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=100){
        return  NO;
    }
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == _txtMessage)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.view cache:YES];
    
    if( [self.txtMessage isFirstResponder]){
        self.view.frame = CGRectMake(self.view.frame.origin.x, -120, self.view.frame.size.width, self.view.frame.size.height+250);
    }
    
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.view cache:YES];
    self.view.frame = CGRectMake(0,0,400,500);
    [UIView commitAnimations];
}

#pragma mark TextField delegate method
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 100) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark
#pragma mark Notification method
- (void) receiveNotificationForAddResponse:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"InsertSurveyNotification"])
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InsertSurveyNotification" object:nil];
        alertType=@"Success";
        [self UserAlert:@"Message sent successfully"];
    }
}
#pragma mark
#pragma mark Alertview method
-(void)UserAlert:(NSString *)Message
{
    UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(Message, nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
    [ErrorAlert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertType isEqualToString:@"Success"])
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
    
    if (alertView.tag == 0) {
        if (buttonIndex == 0) {
            [_popOverController dismissPopoverAnimated:YES];
            if ([self.Delegate respondsToSelector:@selector(updateValues)])
            {
                [self.Delegate updateValues];
            }
        }
    }
}
#pragma mark
#pragma mark Orientation method
- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

#pragma mark
-(NSString *)getCurrentDate
{
    //Update MessageReply and datetime
    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];
    
    formater=nil;
    usLocale=nil;
    
    return dateString;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
