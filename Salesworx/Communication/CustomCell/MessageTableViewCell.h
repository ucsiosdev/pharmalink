//
//  MessageTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxImageView.h"
@interface MessageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet SalesWorxImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;


@end
