//
//  SalesWorxCommunicationsParentViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCommunicationsParentViewController.h"

@interface SalesWorxCommunicationsParentViewController ()

@end

@implementation SalesWorxCommunicationsParentViewController
@synthesize communicationsSwipeView,communicationsSegmentController;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Communication"];
    
    self.view.backgroundColor=UIViewBackGroundColor;
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
            [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
            
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
            
        } else {
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    communicationsSegmentController.sectionTitles = @[@"Messages", @"Push Notifications"];
    communicationsSegmentController.backgroundColor = [UIColor whiteColor];
    communicationsSegmentController.titleTextAttributes = @{NSForegroundColorAttributeName : MedRepDescriptionLabelFontColor};
    communicationsSegmentController.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1]};
    
    communicationsSegmentController.selectionIndicatorBoxOpacity=0.0f;
    communicationsSegmentController.verticalDividerEnabled=YES;
    communicationsSegmentController.verticalDividerWidth=1.0f;
    communicationsSegmentController.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    communicationsSegmentController.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    communicationsSegmentController.selectionStyle = HMSegmentedControlSelectionStyleBox;
    communicationsSegmentController.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    communicationsSegmentController.tag = 3;
    communicationsSegmentController.selectedSegmentIndex = 0;
    
    [communicationsSegmentController addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    self.view.backgroundColor=UIViewBackGroundColor;
    
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    
}

#pragma mark swipe view methods


- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return 2;
}


- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    NSLog(@"swipe view index did changed called current indec is %ld", (long)swipeView.currentPage);
    
    
}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView
{
    NSLog(@"swipe view did end decelerating called");
}


-(void)constrainViewEqual:(UIView *) view {
//    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
//    NSLayoutConstraint *con1 = [NSLayoutConstraint constraintWithItem:communicationsSwipeView attribute:NSLayoutAttributeTopMargin relatedBy:0 toItem:view attribute:NSLayoutAttributeBottomMargin multiplier:1 constant:0];
//    NSLayoutConstraint *con2 = [NSLayoutConstraint constraintWithItem:communicationsSwipeView attribute:NSLayoutAttributeLeftMargin relatedBy:0 toItem:view attribute:NSLayoutAttributeRightMargin multiplier:1 constant:0];
//    NSLayoutConstraint *con3 = [NSLayoutConstraint constraintWithItem:communicationsSwipeView attribute:NSLayoutAttributeWidth relatedBy:0 toItem:view attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
//    NSLayoutConstraint *con4 = [NSLayoutConstraint constraintWithItem:communicationsSwipeView attribute:NSLayoutAttributeHeight relatedBy:0 toItem:view attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
//    NSArray *constraints = @[con1,con2,con3,con4];
//    [self.view addConstraints:constraints];
    
    
    NSMutableArray * constraints = [NSMutableArray array];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:view.superview
                                                        attribute:NSLayoutAttributeLeftMargin
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:view
                                                        attribute:NSLayoutAttributeLeft
                                                       multiplier:1.0
                                                         constant:0]];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:view.superview
                                                        attribute:NSLayoutAttributeRightMargin 
                                                        relatedBy:NSLayoutRelationEqual 
                                                           toItem:view
                                                        attribute:NSLayoutAttributeRight
                                                       multiplier:1.0
                                                         constant:0]];
   
    [constraints addObject:[NSLayoutConstraint constraintWithItem:view.superview
                                                        attribute:NSLayoutAttributeTopMargin
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:view
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0]];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:view.superview
                                                        attribute:NSLayoutAttributeBottomMargin
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:view
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0]];
    [view addConstraints:constraints];
    
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    //create new view if no view is available for recycling
    
    
    if (controllerView==nil) {
        controllerView=[[UIView alloc]initWithFrame:communicationsSwipeView.frame];

    }

        if (index==0) {
            
            CommunicationViewControllerNew * communicationsVC=[[CommunicationViewControllerNew alloc]init];
            [self addChildViewController:communicationsVC];
            controllerView.tag=100;
            [controllerView addSubview:communicationsVC.view];

        }
        else
        {
            SalesWorxPushNotificationMessagesViewController* pushNotificationsVC=[[SalesWorxPushNotificationMessagesViewController alloc]init];
            [self addChildViewController:pushNotificationsVC];
            controllerView.tag=101;

            [controllerView addSubview:pushNotificationsVC.view];
        }
    
    NSLog(@"SWIPE VIEW SUBVIEWS ARE %@", communicationsSwipeView.subviews);
    
    NSString* frame=[NSString stringWithFormat:@"%@",NSStringFromCGRect(communicationsSwipeView.frame)];
    
    
    
   // [self constrainViewEqual:controllerView];

        return controllerView;
}
    
   -(void)viewDidAppear:(BOOL)animated
{
    for(UIView * x in communicationsSwipeView.subviews)
    {
        if([x isKindOfClass:[UIView class]] )
        {
            
            if(x.tag==100 || x.tag==101)
            {
                
                [self constrainViewEqual:x];

            }
        }
            
    }

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
