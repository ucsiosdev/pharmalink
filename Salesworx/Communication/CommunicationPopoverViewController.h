//
//  CommunicationPopoverViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/21/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipientPopoverViewController.h"
#import "FTPBaseViewController.h"
#import "MedRepButton.h"
@protocol dismissDelegate <NSObject>

-(void)updateValues;

@end


@interface CommunicationPopoverViewController : FTPBaseViewController <RecipientPickerDelegate,UITextViewDelegate,UITextFieldDelegate,UIPopoverControllerDelegate>
{
    RecipientPopoverViewController *_recipientPicker;
    UIPopoverController *_colorPickerPopover;
    
    int MessageID;
    
    NSString *alertType;
    NSArray *recipientIDArray;
    NSMutableArray *recipientDetailsArray;

    CGRect popoverFrame;
}
@property(nonatomic) id Delegate;

//@property (nonatomic, strong) RecipientPopoverViewController *recipientPicker;
//@property (nonatomic, strong) UIPopoverController *colorPickerPopover;
@property(strong,nonatomic) UIPopoverController * popOverController;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UITextField *txtTo;
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtMessage;

@property (weak, nonatomic) IBOutlet UIButton *btnTo;
@property (weak, nonatomic) IBOutlet MedRepButton *btnSend;

@property(strong,nonatomic)UINavigationController * filterNavController;

@end
