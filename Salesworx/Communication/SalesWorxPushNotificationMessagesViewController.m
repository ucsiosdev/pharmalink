//
//  SalesWorxPushNotificationMessagesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxPushNotificationMessagesViewController.h"
#import "SalesWorxPushNotificationMessagesTableViewCell.h"
@interface SalesWorxPushNotificationMessagesViewController ()

@end

@implementation SalesWorxPushNotificationMessagesViewController
@synthesize pushNotificationsContentArray,messagestableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
}

-(void)viewWillAppear:(BOOL)animated
{
    pushNotificationsContentArray=[[NSMutableArray alloc]init];
    
    pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];

    
    messagestableView.allowsMultipleSelectionDuringEditing=NO;
    [messagestableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"messageCell";
    SalesWorxPushNotificationMessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPushNotificationMessagesTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];

    cell.titleLbl.text=@"Message";
    cell.dateLbl.text=@"Date";
    return cell.contentView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
//        
//    
//    }
//    
//    cell.textLabel.text=[pushNotificationsContentArray objectAtIndex:indexPath.row];
//    cell.detailTextLabel.text=@"TEST";
//    return cell;
    
    
    static NSString* identifier=@"messageCell";
    SalesWorxPushNotificationMessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPushNotificationMessagesTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }

    FSRMessagedetails * currentMessage=[pushNotificationsContentArray objectAtIndex:indexPath.row];
    cell.titleLbl.text=currentMessage.Message_Content;
    cell.dateLbl.text=currentMessage.Message_date;
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [pushNotificationsContentArray removeObjectAtIndex:indexPath.row];
        
        NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushNotificationsContentArray];
        [defaults setObject:data forKey:@"PUSHED_MESSAGES"];
        [defaults synchronize];

        
        [messagestableView reloadData];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return UITableViewCellEditingStyleDelete;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return  pushNotificationsContentArray.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView

{
    NSLog(@"table scrolled");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
