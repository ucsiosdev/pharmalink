//
//  SalesWorxPushNotificationMessagesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "FSRMessagedetails.h"


@interface SalesWorxPushNotificationMessagesViewController : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *messagestableView;
@property(strong,nonatomic)NSMutableArray * pushNotificationsContentArray;
@end
