//
//  CommunicationHeaderTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunicationHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfUnread;
@property (weak, nonatomic) IBOutlet UILabel *lblUnread;


@end
