//
//  RecipientPopoverViewController.m
//  DataSyncApp
//
//  Created by sapna on 2/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "RecipientPopoverViewController.h"
#import "AllUserList.h"
#import "SWFoundation.h"
#import "SWDatabaseManager.h"
#import "MedRepDefaults.h"
#import "SalesWorxImageBarButtonItem.h"


@implementation RecipientPopoverViewController
@synthesize RecipientArray,filteredRecipientArray;
@synthesize delegate;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=false;
    // Do any additional setup after loading the view from its nib.
    
    RecipientArray=[[NSMutableArray alloc]initWithArray:[[SWDatabaseManager retrieveManager] dbGetUsersList]];
    filteredRecipientArray=[RecipientArray mutableCopy];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Select Recipient"];
    self.navigationController.navigationBar.topItem.title = @"";
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
    }
    else
    {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
    
    SalesWorxImageBarButtonItem* closeButton = [[SalesWorxImageBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnDonePressed)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = closeButton;
    
    tblResponse=[[UITableView alloc] initWithFrame:CGRectMake(0,0,400,456) style:UITableViewStyleGrouped];
    tblResponse.bounces=NO;
    tblResponse.tag=111;
    tblResponse.delegate=self;
    tblResponse.dataSource=self;
    tblResponse.backgroundView = nil;
    tblResponse.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tblResponse];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[[SWDefaults alloc]init]AddBackButtonToViewcontroller:self];
    
    UIView *searchbarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 400, 44)]; //This adds a container that will hold the search bar.
    userSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
    userSearchBar.delegate = self;
    [searchbarView addSubview:userSearchBar];
    tblResponse.tableHeaderView = searchbarView;
    
    if(selectedIndexes)
    {
        [selectedIndexes removeAllObjects];
        selectedIndexes =nil;
    }
    selectedIndexes=[NSMutableArray array];
    
    if(strRecipient)
    {
        strRecipient =nil;
    }
    strRecipient =[[NSString alloc] init];
    if(RecipientIDArray){
        [RecipientIDArray removeAllObjects];
    }
    RecipientIDArray =[NSMutableArray array];
    [tblResponse reloadData];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [filteredRecipientArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }

        
        AllUserList *info = [filteredRecipientArray objectAtIndex:indexPath.row];
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.User_ID==%d",
                                        info.User_ID];
        NSMutableArray * FilteredArray = [[selectedIndexes filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        
        if(FilteredArray.count>0)
        {
            [cell  setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell  setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        
        
        cell.textLabel.font=MedRepSingleLineLabelFont;
        cell.textLabel.textColor=MedRepDescriptionLabelFontColor;
    
        cell.textLabel.text = info.Username;
        
        return cell;
    }
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate != nil)
    {
        AllUserList *info = [filteredRecipientArray objectAtIndex:indexPath.row];
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.User_ID==%d",
                                        info.User_ID];
        NSMutableArray * FilteredArray = [[selectedIndexes filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        
        if(FilteredArray.count==0)
        {
            [selectedIndexes addObject:info];
        }
        else
        {
            [selectedIndexes removeObject:info];
        }
    }
    [tblResponse reloadData];
}
-(void)btnDonePressed
{
    NSMutableArray *RecipientDetailsArray=[[NSMutableArray alloc]init];
    [self.view endEditing:YES];
    for (int i=0; i<[selectedIndexes count]; i++) {
        @autoreleasepool{
            AllUserList *info = [selectedIndexes objectAtIndex:i];
            strRecipient =[strRecipient stringByAppendingFormat:@"%@,",info.Username];
            [RecipientIDArray addObject:[NSString stringWithFormat:@"%d",info.User_ID]];
           
            NSDictionary *tempDic=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",info.User_ID],@"RecipientID",info.Username,@"RecipientName", nil];
            
            [RecipientDetailsArray addObject:tempDic];
        }
    }
    [self.delegate RecipientSelected:strRecipient RecipientId:RecipientIDArray RecipientDetails:RecipientDetailsArray];
    [self.delegate PopOverDonePressed];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([[searchBar.text trimString] isEqualToString:@""])
    {
        filteredRecipientArray=RecipientArray;
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Username CONTAINS[cd] %@",
                                  searchBar.text];
        filteredRecipientArray = [[RecipientArray filteredArrayUsingPredicate:predicate] mutableCopy];
        
    }
    [tblResponse reloadData];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

@end
