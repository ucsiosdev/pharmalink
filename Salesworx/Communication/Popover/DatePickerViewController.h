//
//  DatePickerViewController.h
//  DataSyncApp
//
//  Created by sapna on 1/29/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTPBaseViewController.h"
@protocol DatePickerDelegate
@optional
- (void)dateSelected:(NSString *)Date andFuture:(NSString *)isFuture;
@end

@interface DatePickerViewController : FTPBaseViewController
{
    UIDatePicker *datePicker;
    NSMutableArray *dateArray;
    NSDate *selecteddate;
    id<DatePickerDelegate> _delegate;
}
@property(nonatomic,strong)NSString *strdate;
@property (nonatomic, strong) NSMutableArray *dateArray;
@property (unsafe_unretained) id<DatePickerDelegate> delegate;
@end
