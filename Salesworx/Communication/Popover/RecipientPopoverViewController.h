//
//  RecipientPopoverViewController.h
//  DataSyncApp
//
//  Created by sapna on 2/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//
#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import "FTPBaseViewController.h"
@protocol RecipientPickerDelegate
@optional
- (void)RecipientSelected:(NSString *)Recipient RecipientId:(NSArray*)RecipientId RecipientDetails:(NSMutableArray *)RecipientDetails;
- (void)PopOverDonePressed;
@end


@interface RecipientPopoverViewController : FTPBaseViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
   // NSArray *RecipientArray;
    NSString *strRecipient;
    NSMutableArray *RecipientIDArray,*selectedIndexes;
    UITableView * tblResponse;
    id<RecipientPickerDelegate> _delegate;
    UISearchBar *userSearchBar;
}

@property (nonatomic, strong) NSMutableArray *RecipientArray;
@property (nonatomic, strong) NSMutableArray *filteredRecipientArray;

@property (nonatomic,unsafe_unretained) id<RecipientPickerDelegate> delegate;
@end
