//
//  SalesWorxCommunicationsParentViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "HMSegmentedControl.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import <UIKit/UIKit.h>
#import "FSRMessagedetails.h"
#import <NSDictionary+Additions.h>
#import "CommunicationViewControllerNew.h"
#import "SalesWorxPushNotificationMessagesViewController.h"

@interface SalesWorxCommunicationsParentViewController : UIViewController<SwipeViewDelegate,SwipeViewDataSource>
{
    UIView * controllerView;
}

@property (strong, nonatomic) IBOutlet SwipeView *communicationsSwipeView;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *communicationsSegmentController;
@end
