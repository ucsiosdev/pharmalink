//
//  SalesWorxPushNotificationMessagesTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxPushNotificationMessagesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *titleLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *dateLbl;
@property(nonatomic)BOOL isHeader;

@end
