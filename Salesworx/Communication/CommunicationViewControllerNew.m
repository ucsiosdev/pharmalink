

////
////  CommunicationViewControllerNew.m
////  DataSyncApp
////
////  Created by sapna on 1/28/13.
////  Copyright (c) 2013 Sapna.Shah. All rights reserved.
////
//
//#import "CommunicationViewControllerOld.h"
//#import "MessageTableViewCell.h"
//#import  "ComposeMessageViewController.h"
//#import <QuartzCore/QuartzCore.h>
//#import <CoreText/CoreText.h>
//#import "FSRMessagedetails.h"
//#import "ReadMessageViewController.h"
//#import "CustomerShipAddressDetails.h"
//#import "PlannedVisitDetails.h"
//#import "SWPlatform.h"
//#import "ReadSentMessagesViewController.h"
//
//@interface CommunicationViewControllerOld ()
//
//@end
//
//@implementation CommunicationViewControllerOld
//
//@synthesize customer;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
////- (void)dealloc
////{
////    NSLog(@"Dealloc %@",self.title);
////    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    
//    //customerSer.delegate = self;
//    self.title=NSLocalizedString(@"Communication", nil);
//    self.view.backgroundColor = [UIColor whiteColor];
//    //self.navigationItem.hidesBackButton = YES;
//    //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//    
//    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
//        
//        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
//            UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
//            [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
//            
//            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon" cache:NO] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
//            
//        } else {
//        }
//    }
//    
//    //Set Border
//    
//    //    [messageView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
//    //    [messageView.layer setBorderWidth:1.0];
//    //
//    //
//    //    [UpcomingVisistView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
//    //    [UpcomingVisistView.layer setBorderWidth:1.0];
//    
//    //
//    searchMessageArray = [NSMutableArray array];
//    searchSentMessageArray = [NSMutableArray array];
//    
//    CustomerShipDetailsArray =[NSMutableArray array];
//    tblMessages.backgroundColor = [UIColor whiteColor];
//    tblSentMessages.backgroundColor = [UIColor whiteColor];
//    
//    tblUpcomingVisit.backgroundColor = [UIColor whiteColor ];
//    
//    
//    
//    self.customer = [NSMutableDictionary dictionary];
//    
//    [addButton setTitle:NSLocalizedString(@"Add", nil) forState: UIControlStateNormal];
//    [searchButton setTitle:NSLocalizedString(@"Search", nil) forState: UIControlStateNormal];
//    
//    addButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
//    searchButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
//    dateLabel.font = BoldSemiFontOfSize(21);
//    dateLabel.textColor = UIColorFromRGB(0x4790D2);
//    
//    messageHeading.text = NSLocalizedString(@"Messages", nil);
//    visistHeading.text = NSLocalizedString(@"Upcoming Visits", nil);
//    messageHeading.font = RegularFontOfSize(21);
//    visistHeading.font = RegularFontOfSize(21);
//    
//    
//    
//}
//-(void)viewWillAppear:(BOOL)animated
//{
//    //Get FSR Messages
//    [Flurry logEvent:@"Cummunication  View"];
//    //[Crittercism leaveBreadcrumb:@"<Cummunication  View>"];
//    //    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//    //    [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
//    messageArray = [[SWDatabaseManager retrieveManager] dbGetFSRMessages];
//    sentMessages = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetFSRSentMessages]];
//    [tblMessages reloadData];
//    [tblSentMessages reloadData];
//    
//    //Get cureent date
//    NSDate *date =[NSDate date];
//    NSDateFormatter *formater =[NSDateFormatter new];
//    [formater setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formater setLocale:usLocale];
//    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:date]];
//    dateLabel.text = [formater stringFromDate:date];
//    
//    formater=nil;
//    usLocale=nil;
//}
//
//#pragma mark
//#pragma mark Button Action Method
//-(void)btnBackPressed
//{
//    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
//    
//    //if (revealController.currentFrontViewPosition !=0) {
//    [revealController revealToggle:self];
//    //}
//}
//-(IBAction)btnAddMessagePressed:(id)sender
//{
//    ComposeMessageViewController *objComposeMessageVC =[[ComposeMessageViewController alloc] initWithNibName:@"ComposeMessageViewController" bundle:nil];
//    [self.navigationController pushViewController:objComposeMessageVC animated:YES];
//}
//-(IBAction)btnSerachMessagePressed:(id)sender
//{
//    //Show search bar
//    tblMessages.frame=CGRectMake(5,95 ,500 , 253);
//    tblSentMessages.frame=CGRectMake(510,95 ,500 , 253);
//    
//    searchBar.hidden=FALSE;
//}
//-(IBAction)btnDatePressed:(id)sender
//{
//    if (_colorPicker == nil) {
//        _colorPicker = [[DatePickerViewController alloc]init];
//        _colorPicker.delegate = self;
//        datePickerPopover=nil;
//        datePickerPopover= [[UIPopoverController alloc]
//                            initWithContentViewController:_colorPicker];
//        datePickerPopover.delegate=self;
//    }
//    [datePickerPopover presentPopoverFromRect:[sender frame] inView:UpcomingVisistView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//}
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
//{
//    _colorPicker=nil;
//    datePickerPopover=nil;
//}
//
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    return YES;
//}
//#pragma mark -
//#pragma mark Table view data source
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 44;
//}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//
//{
//    if (tableView.tag ==111) {
//        return NSLocalizedString(@"Inbox", nil);
//    }
//    else if (tableView.tag ==222)
//    {
//        return NSLocalizedString(@"Sent", nil);
//    }
//    else
//    {
//        return nil;
//    }
//}
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    return 1;
//}
//
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if(tableView.tag==111)
//    {
//        if(searching)
//            return [searchMessageArray count];
//        else
//            return [messageArray count];
//    }
//    else if(tableView.tag==222)
//    {
//        if(searching)
//            return [searchSentMessageArray count];
//        else
//            return [sentMessages count];
//    }
//    else
//    {
//        return [CustomerShipDetailsArray count];
//    }
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//// Customize the appearance of table view cells.
//{
//    @autoreleasepool
//    {
//        if(tableView.tag ==111)
//        {
//            MessageTableViewCell *cell = (MessageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PKMessageTableviewCell"];
//            
//            if (cell == nil)
//            {
//                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MessageTableViewCell" owner:self options:nil];
//                for(id currentObject in topLevelObjects)
//                {
//                    if([currentObject isKindOfClass:[MessageTableViewCell class]])
//                    {
//                        cell = (MessageTableViewCell *)currentObject;
//                        break;
//                    }
//                }
//            }
//            cell.selectionStyle =UITableViewCellSelectionStyleNone;
//            //            cell.btnSelection.tag=indexPath.row;
//            //            [cell.btnSelection addTarget:self action:@selector(btnSelectionPressed:) forControlEvents:UIControlEventTouchUpInside];
//            
//            FSRMessagedetails *info;
//            if(searching)
//                info =[searchMessageArray objectAtIndex:indexPath.row];
//            else
//                info =[messageArray objectAtIndex:indexPath.row];
//            cell.lblMessage.font=RegularFontOfSize(16.0);
//            
//            cell.lblMessage.text=info.Message_Title;
//            
//            if([info.Message_Read isEqualToString:@"Y"])
//            {
//                //[cell.btnSelection setImage:[UIImage imageNamed:@"communication_inbox.png" cache:NO] forState:UIControlStateNormal];
//                NSString *string = cell.lblMessage.text;
//                
//                
//                
//                
//                CGSize stringSize=[SWDefaults fetchSizeofLabelWithText:string andFont:cell.lblMessage.font];
//                
//                
//                
//                
//                
//                // CGSize stringSize = [string sizeWithFont:cell.lblMessage.font];
//                CGRect buttonFrame = cell.lblMessage.frame;
//                CGRect labelFrame = CGRectMake(buttonFrame.origin.x ,
//                                               buttonFrame.origin.y + stringSize.height/2,
//                                               stringSize.width, 2);
//                UILabel *lineLabel = [[UILabel alloc] initWithFrame:labelFrame];
//                lineLabel.backgroundColor = [UIColor blackColor];
//                [cell.contentView addSubview:lineLabel];
//            }
//            
//            
//            return cell;
//        }
//        else  if(tableView.tag ==222)
//        {
//            MessageTableViewCell *cell = (MessageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PKMessageTableviewCell"];
//            
//            NSMutableDictionary *info = nil;
//            
//            if (cell == nil)
//            {
//                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MessageTableViewCell" owner:self options:nil];
//                for(id currentObject in topLevelObjects)
//                {
//                    if([currentObject isKindOfClass:[MessageTableViewCell class]])
//                    {
//                        cell = (MessageTableViewCell *)currentObject;
//                        //[cell.btnSelection setImage:[UIImage imageNamed:@"communication_send.png" cache:NO] forState:UIControlStateNormal];
//                        
//                        break;
//                    }
//                }
//                
//                
//            }
//            cell.selectionStyle =UITableViewCellSelectionStyleNone;
//            if(searching)
//            {
//                info = [NSMutableDictionary dictionaryWithDictionary:[searchSentMessageArray objectAtIndex:indexPath.row]];
//            }
//            else
//            {
//                info = [NSMutableDictionary dictionaryWithDictionary:[sentMessages objectAtIndex:indexPath.row]];
//            }
//            cell.lblMessage.font=RegularFontOfSize(16.0);
//            cell.lblMessage.text=[info stringForKey:@"Message_Title"];
//            
//            
//            return cell;
//        }
//        else
//        {
//            NSString *CellIdentifier = @"RouteViewCell";
//            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//            
//            if (!cell) {
//                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier]  ;
//            }
//            
//            NSDictionary *row = [CustomerShipDetailsArray objectAtIndex:indexPath.row];
//            
//            [cell.textLabel setText:[row objectForKey:@"Customer_Name"]];
//            cell.textLabel.font =RegularFontOfSize(16);
//            cell.textLabel.textColor =[UIColor blackColor];
//            cell.textLabel.backgroundColor = [UIColor clearColor];
//            cell.detailTextLabel.backgroundColor = [UIColor clearColor];
//            if ([row objectForKey:@"Visit_Start_Time"] != [NSNull null])
//                //if (![[row stringForKey:@"Visit_Start_Time"] isEqualToString:@""])
//            {
//                NSString *timeString = [row stringForKey:@"Visit_Start_Time"];
//                //NSString *newStr = [timeString substringFromIndex:11];
//                //NSString *newStr = @"";
//                [cell.detailTextLabel setText:[NSString stringWithFormat:@"Time:%@",timeString]];
//            }
//            
//            
//            
//            if([[row stringForKey:@"Visit_Status"] isEqualToString:@"Y"])
//            {
//                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
//            }
//            else
//            {
//                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//            }
//            
//            
//            
//            return cell;
//        }
//    }
//}
//
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    [cell.textLabel setFont:LightFontOfSize(14.0f)];
//    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
//    
//}
//
//#pragma mark - Table view delegate
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if(tableView.tag==111){
//        
//        //Add strickthrough
//        MessageTableViewCell *cell=(MessageTableViewCell*)[tblMessages cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
//        //[cell.btnSelection setImage:[UIImage imageNamed:@"communication_inbox.png" cache:NO] forState:UIControlStateNormal];
//        NSString *string = cell.lblMessage.text;
//        
//        
//        
//        CGSize stringSize =[SWDefaults fetchSizeofLabelWithText:string andFont:cell.lblMessage.font];
//        
//        // CGSize stringSize = [string sizeWithFont:cell.lblMessage.font];
//        CGRect buttonFrame = cell.lblMessage.frame;
//        CGRect labelFrame = CGRectMake(buttonFrame.origin.x ,
//                                       buttonFrame.origin.y + stringSize.height/2,
//                                       stringSize.width, 2);
//        UILabel *lineLabel = [[UILabel alloc] initWithFrame:labelFrame];
//        lineLabel.backgroundColor = [UIColor blackColor];
//        [cell.contentView addSubview:lineLabel];
//        
//        
//        //Go to ReadMesageView
//        ReadMessageViewController *objReadMessageVC =[[ReadMessageViewController alloc] initWithNibName:@"ReadMessageViewController" bundle:nil];
//        FSRMessagedetails *info;
//        if(searching)
//            info =[searchMessageArray objectAtIndex:indexPath.row];
//        else
//            info =[messageArray objectAtIndex:indexPath.row];
//        
//        objReadMessageVC.messageDeatils=info;
//        
//        [self.navigationController pushViewController:objReadMessageVC animated:YES];
//    }
//    
//    else if(tableView.tag==222){
//        
//        ReadSentMessagesViewController *objReadMessageVC =[[ReadSentMessagesViewController alloc] initWithNibName:@"ReadSentMessagesViewController" bundle:nil];
//        NSMutableDictionary *info = nil;
//        if(searching)
//            info = [NSMutableDictionary dictionaryWithDictionary:[searchSentMessageArray objectAtIndex:indexPath.row]];
//        else
//            info = [NSMutableDictionary dictionaryWithDictionary:[sentMessages objectAtIndex:indexPath.row]];
//        
//        objReadMessageVC.messageDict=info;
//        [self.navigationController pushViewController:objReadMessageVC animated:YES];
//        objReadMessageVC = nil;
//    }
//    else
//    {
//        NSDictionary *row = [CustomerShipDetailsArray objectAtIndex:indexPath.row];
//        
//        Singleton *single = [Singleton retrieveSingleton];
//        single.planDetailId = [row stringForKey:@"FSR_Plan_Detail_ID"];
//        
//        // CHECK IF CASH CUSTOMER THEN SHOW FORM
//        if([[row stringForKey:@"Cash_Cust"]isEqualToString:@"Y"])
//        {
//            single.isCashCustomer = @"Y";
//            CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:row];
//            [self.navigationController pushViewController:cashCustomer animated:YES];
//        }
//        else
//        {
//            
//            if(!isFurtureDate)
//            {
//                //customerSer.delegate=self;
//                //[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]];
//                
//                
//                self.customer=[[NSMutableDictionary alloc]init];
//                
//                self.customer=[row mutableCopy];
//                
//                
//                [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]]];
//                
//                
//                //[self setCustomer:[row mutableCopy]];
//            }
//            else
//            {
//                //                [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                //                                   message:@"You can not start visit for future date."
//                //                          closeButtonTitle:NSLocalizedString(@"OK", nil)
//                //                         secondButtonTitle:nil
//                //                       tappedButtonAtIndex:nil];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"You can not start visit for future date." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                [alert show];
//            }
//        }
//        [tblUpcomingVisit deselectRowAtIndexPath:tblUpcomingVisit.indexPathForSelectedRow animated:YES];
//        
//        
//    }
//    
//    
//}
//
//#pragma mark
//#pragma mark Upcoming date selection method
//- (void)dateSelected:(NSString *)Date andFuture:(NSString *)isFuture
//{
//    
//    
//    NSDateFormatter* df = [NSDateFormatter new];
//    [df setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [df setLocale:usLocale];
//    NSDate *selectedDate =[df dateFromString:Date];
//    
//    dateLabel.text = [df stringFromDate:selectedDate];
//    
//    //selectedDate = [df dateFromString:Date];
//    
//    NSDate* enteredDate = selectedDate;
//    //routeSer.delegate=self;
//    //[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate];
//    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
//    
//    [tblUpcomingVisit reloadData];
//    NSDate * today = [NSDate date];
//    NSComparisonResult result = [today compare:enteredDate];
//    switch (result)
//    {
//        case NSOrderedAscending:
//            ////NSLog(@"Future Date");
//            isFurtureDate = YES;
//            break;
//        case NSOrderedDescending:
//            ////NSLog(@"Earlier Date");
//            isFurtureDate = NO;
//            break;
//            
//        case NSOrderedSame:
//            ////NSLog(@"Today/Null Date Passed");
//            isFurtureDate = NO;
//            break;
//            //Not sure why This is case when null/wrong date is passed
//        default:
//            ////NSLog(@"Error Comparing Dates");
//            isFurtureDate = NO;
//            break;
//            
//    }
//    df=nil;
//    usLocale=nil;
//}
//
////- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
//- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
//    
//    [SWDefaults setCustomer:self.customer];
//    
//    // self.customer = [SWDefaults validateAvailableBalance:orderAmmount];
//    
//    Singleton *single = [Singleton retrieveSingleton];
//    single.isCashCustomer = @"N";
//    SWVisitManager *visitManager = [[SWVisitManager alloc] init];
//    [visitManager startVisitWithCustomer:self.customer parent:self andChecker:@"R"];
//    orderAmmount=nil;
//}
//
//#pragma mark SWRouteService Delegate
//- (void)getRouteServiceDidGetRoute:(NSArray *)r{
//    
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Visit_Start_Time" ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    r = [r sortedArrayUsingDescriptors:sortDescriptors];
//    
//    CustomerShipDetailsArray = [NSMutableArray arrayWithArray:r] ;
//    [tblUpcomingVisit reloadData];
//    //[self PlaceAnnotationOnMapView];
//    [datePickerPopover dismissPopoverAnimated:YES];
//    r=nil;
//}
//#pragma mark Search method
//- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
//    
//    [self searchTableView];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
//                                              initWithBarButtonSystemItem:UIBarButtonSystemItemDone
//                                              target:self action:@selector(doneButtonPressed:)];
//}
//
//- (void) searchTableView {
//    
//    searching = YES;
//    NSString *searchText = searchBar.text;
//    if(searchMessageArray)
//        [searchMessageArray removeAllObjects];
//	   
//    if(searchSentMessageArray)
//        [searchSentMessageArray removeAllObjects];
//    
//    
//    for(int i =0;i<[messageArray count];i++)
//    {
//        FSRMessagedetails *info =[messageArray objectAtIndex:i];
//        NSString *strTemp = info.Message_Title;
//        
//        NSRange titleResultsRange = [strTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
//        
//        if (titleResultsRange.length > 0)
//            [searchMessageArray addObject:info];
//        
//    }
//    
//    for(int i =0;i<[sentMessages count];i++)
//    {
//        NSMutableDictionary *info =[NSMutableDictionary dictionaryWithDictionary:[sentMessages objectAtIndex:i]] ;
//        NSString *strTemp = [info stringForKey:@"Message_Title"];
//        
//        NSRange titleResultsRange = [strTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
//        
//        if (titleResultsRange.length > 0)
//            [searchSentMessageArray addObject:info];
//        
//    }
//    [tblMessages reloadData];
//    [tblSentMessages reloadData];
//    [searchBar resignFirstResponder];
//}
//- (void) doneButtonPressed:(id)sender {
//    
//    searchBar.hidden=TRUE;
//    tblMessages.frame=CGRectMake(5,50,500,253);
//    tblSentMessages.frame=CGRectMake(510,50 ,500 , 253);
//    searchBar.text = @"";
//    [searchBar resignFirstResponder];
//    
//    
//    searching = NO;
//    self.navigationItem.rightBarButtonItem = nil;
//    
//    [tblMessages reloadData];
//    [tblSentMessages reloadData];
//    
//}
//- (void)searchBarCancelButtonClicked:(UISearchBar *) SearchBar
//{
//    // searchBar.hidden=TRUE;
//    // tblMessages.frame=CGRectMake(5,50,500,253);
//    //tblSentMessages.frame=CGRectMake(510,50 ,500 , 253);
//    searchBar.text = @"";
//    [searchBar resignFirstResponder];
//    searching = NO;
//    self.navigationItem.rightBarButtonItem = nil;
//    
//    [tblMessages reloadData];
//    [tblSentMessages reloadData];
//    
//}
//- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar1 {
//    searchBar1.showsScopeBar = YES;
//    [searchBar1 sizeToFit];
//    [searchBar1 setShowsCancelButton:YES animated:YES];
//    
//    return YES;
//}
//
//- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar1 {
//    searchBar1.showsScopeBar = NO;
//    [searchBar1 sizeToFit];
//    [searchBar1 setShowsCancelButton:NO animated:YES];
//    
//    return YES;
//}
//#pragma mark
//#pragma mark TableviewCell Button mehtod
//
//-(void)btnDetailVisitPressed:(id)sender
//{
//    // add the data to go  Route page using upcoming visits table
//}
//-(void)btnSelectionPressed:(id)sender
//{
//    // add the data if require check box pressed event
//}
//#pragma mark
//#pragma mark Alertview method
//-(void)UserAlert:(NSString *)Message
//{
//    if (Message.length!=0)
//    {
//        //        [ILAlertView showWithTitle:NSLocalizedString(@"Message", nil)
//        //                           message:Message
//        //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//        //                 secondButtonTitle:nil
//        //               tappedButtonAtIndex:nil];
//        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
//        [ErrorAlert show];
//        
//        
//    }
//    
//    
//}
//#pragma mark
//#pragma mark orientation method
//- (BOOL)shouldAutorotateToInterfaceOrientation:
//(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
//            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
//}
//#pragma mark
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        self.view = nil;
//    // Dispose of any resources that can be recreated.
//}
//
//@end


//
//  CommunicationViewControllerNew.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "CommunicationViewControllerNew.h"
#import "MessageTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import "FSRMessagedetails.h"
#import "CustomerShipAddressDetails.h"
#import "PlannedVisitDetails.h"
#import "SWPlatform.h"
#import "CommunicationHeaderTableViewCell.h"
#import "MedRepDefaults.h"
#import "CommunicationPopoverViewController.h"

@interface CommunicationViewControllerNew ()<UITableViewDataSource,UITableViewDelegate>
{
    AppControl *appControl;
}
@end

@implementation CommunicationViewControllerNew

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(pushNotificationArrived)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushNotificationArrived) name:@"PUSHNOTIFICATIONARRIVED" object:nil];


    
    appControl=[AppControl retrieveSingleton];
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Communication"];

    self.view.backgroundColor=UIViewBackGroundColor;
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
            [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
            
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
            
        } else {
        }
    }
}

-(void)pushNotificationArrived
{
    NSLog(@"push notification arrived in foreground and delegate called");
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        messageArray = [[[SWDatabaseManager retrieveManager] dbGetV2FSRMessages] mutableCopy];
    }
    else
    {
        messageArray = [[[SWDatabaseManager retrieveManager] dbGetFSRMessages] mutableCopy];
    }
    NSMutableArray*  pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
    if (pushNotificationsContentArray.count>0) {
        
        pushNotificationsContentArray=[self sortMessages:pushNotificationsContentArray isPushNotification:YES];
        
        [messageArray addObjectsFromArray:pushNotificationsContentArray];
    }
    [_tblInbox reloadData];

    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)becomeActive
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        messageArray = [[[SWDatabaseManager retrieveManager] dbGetV2FSRMessages] mutableCopy];
    }
    else
    {
        messageArray = [[[SWDatabaseManager retrieveManager] dbGetFSRMessages] mutableCopy];
    }
    NSMutableArray*  pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
    if (pushNotificationsContentArray.count>0) {
        
        pushNotificationsContentArray=[self sortMessages:pushNotificationsContentArray isPushNotification:YES];
        
        [messageArray addObjectsFromArray:pushNotificationsContentArray];
    }
    [_tblInbox reloadData];
    [MBProgressHUD hideHUDForView:self.view animated:YES];


}
-(void)removeKeyBoardObserver
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    
}


-(void)onKeyboardHide:(NSNotification *)notification
{
    
    [searchBar setShowsCancelButton:NO animated:YES];
}

-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self removeKeyBoardObserver];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesCommunicationsScreenName];
    }
    
    [self addKeyBoardObserver];
   // pushNotificationImageView.hidden=YES;
    

    messagesScrollView.delegate=self;
    
    
    
    


    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        sentMessages = [[SWDatabaseManager retrieveManager] dbGetV2FSRSentMessages];
        messageArray = [[[SWDatabaseManager retrieveManager] dbGetV2FSRMessages] mutableCopy];

    }
    else
    {
        sentMessages = [[SWDatabaseManager retrieveManager] dbGetFSRSentMessages];
        messageArray = [[[SWDatabaseManager retrieveManager] dbGetFSRMessages] mutableCopy];

    }
    
    
    
    NSMutableArray*  pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
    if (pushNotificationsContentArray.count>0) {
        
        pushNotificationsContentArray=[self sortMessages:pushNotificationsContentArray isPushNotification:YES];
        
        [messageArray addObjectsFromArray:pushNotificationsContentArray];
    }

    
    
    
    NSMutableArray *arrOfReplyMessage = [self fetchReplyMessages];
    if ([arrOfReplyMessage count] > 0)
    {
        sentMessages = [sentMessages arrayByAddingObjectsFromArray:arrOfReplyMessage];
    }
    sentMessages=[self sortMessages:sentMessages isPushNotification:NO];

    [_tblInbox reloadData];
    [_tblSent reloadData];
    
    [_tblInbox.layer setBorderWidth: 1.0];
    [_tblInbox.layer setMasksToBounds:YES];
    [_tblInbox.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
    
    [_tblSent.layer setBorderWidth: 1.0];
    [_tblSent.layer setMasksToBounds:YES];
    [_tblSent.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
    
    [_txtViewMessage.layer setBorderWidth: 1.0];
    [_txtViewMessage.layer setMasksToBounds:YES];
    [_txtViewMessage.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
    
    [_txtViewReply.layer setBorderWidth: 1.0];
    [_txtViewReply.layer setMasksToBounds:YES];
    [_txtViewReply.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
    
    _lblName.hidden = YES;
    _lblDate.hidden = YES;
    _txtViewMessage.hidden = YES;
    _btnReply.hidden = YES;
    _lblMsg.hidden = NO;
    noMessageSelectedImage.hidden=NO;
    _txtViewReply.hidden = YES;
    _lblReply.hidden = YES;
    _btnSend.hidden = YES;
    
    searchMessageArray = [NSMutableArray array];
    searchSentMessageArray = [NSMutableArray array];
    
    messagesSegment.sectionTitles = @[@"Messages", @"Push Notifications"];
    messagesSegment.backgroundColor = [UIColor whiteColor];
    messagesSegment.titleTextAttributes = @{NSForegroundColorAttributeName : MedRepDescriptionLabelFontColor};
    messagesSegment.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1]};
    
    messagesSegment.selectionIndicatorBoxOpacity=0.0f;
    messagesSegment.verticalDividerEnabled=YES;
    messagesSegment.verticalDividerWidth=1.0f;
    messagesSegment.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    messagesSegment.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    messagesSegment.selectionStyle = HMSegmentedControlSelectionStyleBox;
    messagesSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    messagesSegment.tag = 3;
    messagesSegment.selectedSegmentIndex = 0;
    
    [messagesSegment addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];

    
    NSLog(@"messages scrollview before tbl %@", messagesScrollView.subviews);
    messagesScrollView.contentOffset=CGPointMake(0, 60);
    messagesScrollView.contentSize=CGSizeMake(2048, messagesScrollView.frame.size.height);

    
    SalesWorxPushNotificationMessagesViewController * messagesVC=[[SalesWorxPushNotificationMessagesViewController alloc]init];
    messagesVC.view.backgroundColor=[UIColor clearColor];
    messagesVC.view.frame=CGRectMake(1032, 0, 1024, messagesScrollView.frame.size.height);
    
    NSLog(@"push view frame is %@", NSStringFromCGRect(messagesVC.view.frame));
    
   // [self addChildViewController:messagesVC];
   // [messagesScrollView addSubview:messagesVC.view];
    

    
    messagesScrollView.backgroundColor=[UIColor clearColor];
    messagesScrollView.clipsToBounds = NO;
    messagesScrollView.contentInset = UIEdgeInsetsZero;
    messagesScrollView.pagingEnabled = YES;
    
    //self.view.backgroundColor=UIViewBackGroundColor;
    
    NSLog(@"messages scrollview after tbl %@", messagesScrollView.subviews);

}

-(NSMutableArray*)fetchReplyMessages
{
    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        
          NSString * outStandingQry=@"select  A.Sender_ID AS SalesRep_ID,A.Msg_Title AS Message_Title,A.Msg_Body As Message_Reply,A.Sent_At AS Reply_Date,A.Expires_At AS Message_Expiry_Date,B.msg_body AS Message_Content,B.Msg_ID AS Message_ID,A.Logged_At As Message_date,A.Logged_At As Logged_At,B.Sender_Name As Sender_Name from TBL_msg AS A INNER join tbl_msg As B on A.Parent_Msg_ID=B.Msg_ID where A.Parent_Msg_ID is not Null AND A.Expires_At>date('now')";
        NSMutableArray *temp=  [[[SWDatabaseManager retrieveManager] fetchDataForQuery:outStandingQry] mutableCopy];
        if (temp.count>0) {
            
            return temp;
        }
        else
        {
            return nil;
        }
        return nil;
    }
    else
    {
        NSString * outStandingQry=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Messages where Message_Reply is not Null"]];
        
        NSLog(@"total outstanding qry %@", outStandingQry);
        
        NSMutableArray *temp=  [[[SWDatabaseManager retrieveManager] fetchDataForQuery:outStandingQry] mutableCopy];
        
        if (temp.count>0) {
            
            return temp;
        }
        else
        {
            return nil;
        } 
    }

}
#pragma mark button action
- (IBAction)btnComposeMessage:(id)sender
{
    if (messageDetails.isFromPushNotifications ==YES) {
        
    }
    
    else
    {
    if (_lblMsg.hidden == NO || checkWhichTableIsSelected == true)
    {
        _btnReply.hidden = YES;
    }
    else if ((checkWhichTableIsSelected == false) && _txtViewReply.hidden ==  NO)
    {
        _btnReply.hidden = YES;
    }
    else
    {
        [self checkButtonReplyIsHiddenOrNot];
    }
    }
    [_txtViewReply resignFirstResponder];

    CommunicationPopoverViewController * popOverVC=[[CommunicationPopoverViewController alloc]init];
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    popOverForNewMessage=nil;
    popOverForNewMessage=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    popOverForNewMessage.delegate=self;
    popOverVC.popOverController=popOverForNewMessage;
    popOverVC.Delegate = self;
    CGRect relativeFrame = [_btnComposeMessage convertRect:_btnComposeMessage.bounds toView:self.view];

    
    [popOverForNewMessage setPopoverContentSize:CGSizeMake(400, 480) animated:NO];
    [popOverForNewMessage presentPopoverFromRect:relativeFrame inView:self.view  permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown| UIPopoverArrowDirectionLeft|UIPopoverArrowDirectionRight) animated:YES];
}
//- (IBAction)btnSearch:(id)sender
//{
//    if (_lblMsg.hidden == NO || checkWhichTableIsSelected == true|| noMessageSelectedImage.hidden==NO)
//    {
//        _btnReply.hidden = YES;
//    }
//    else if ((checkWhichTableIsSelected == false) && _txtViewReply.hidden ==  NO)
//    {
//        _btnReply.hidden = YES;
//    }
//    else
//    {
//        [self checkButtonReplyIsHiddenOrNot];
//    }
//    [_txtViewReply resignFirstResponder];
//    [searchBar removeFromSuperview];
//    [doneButton removeFromSuperview];
//    
//   // searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(8, 60, 400, 44)];
//    searchBar.delegate = self;
//    
//    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [doneButton addTarget:self action:@selector(Done:) forControlEvents:UIControlEventTouchUpInside];
//    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
//    [doneButton setTitleColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1] forState:UIControlStateNormal];
//    doneButton.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:19];
//    doneButton.frame = CGRectMake(408.0, 60.0, 84.0, 44.0);
//    doneButton.layer.borderWidth = 1.0;
//    doneButton.layer.borderColor = UITextFieldDarkBorderColor.CGColor;
//   // [_inboxView addSubview:doneButton];
//    //[_inboxView addSubview:searchBar];
//    
//   // [_tblInbox setFrame:CGRectMake(8, 104, 484, 284)];
//    //[_tblSent setFrame:CGRectMake(8, 396, 484, 284)];
//}

- (IBAction)btnReply:(id)sender
{
    _txtViewReply.text = @"";
    _txtViewReply.editable = YES;
    _txtViewReply.hidden = NO;
    _lblReply.hidden = NO;
    _btnSend.hidden = NO;
    _btnReply.hidden = YES;
}
-(void)checkButtonReplyIsHiddenOrNot
{
    
  /*  Version 1: Message_ID
    version 2: msg_Id*/
    
    
    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        if(messageDetails.Reply_Date==nil || [messageDetails.Reply_Date isEqualToString:@""])
        {
            _btnReply.hidden = NO;

        }
        else
        {
            _btnReply.hidden = YES;

        }
    }
    else
    {
        if (searching)
        {
            for (int i = 0; i<[searchSentMessageArray count]; i++)
            {
                
                if ([[[searchSentMessageArray objectAtIndex:i] valueForKey:@"Message_ID"]intValue] == messageDetails.Message_ID) {
                    _btnReply.hidden = YES;
                    break;
                }
                
                else
                {
                    _btnReply.hidden = NO;
                }
            }
        }
        else
        {
            for (int i = 0; i<[sentMessages count]; i++)
            {
                if ([[[sentMessages objectAtIndex:i] valueForKey:@"Message_ID"]intValue] == messageDetails.Message_ID) {
                    _btnReply.hidden = YES;
                    break;
                }
                else
                {
                    _btnReply.hidden = NO;
                }
            }
        }
    }

}
- (IBAction)btnSend:(id)sender
{
    
    
    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        
        BOOL replySuccess;
        

        if([messageDetails.is_Replied_Message isEqualToString:@"Y"])
        {
            NSDictionary *sentUserDetsilsDic=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",messageDetails.SalesRep_ID],@"RecipientID",messageDetails.SalesRep_Name,@"RecipientName", nil];
            
            ;
            
            NSDictionary *messageDetailsDic=[[NSDictionary alloc]initWithObjectsAndKeys:messageDetails.Message_Title,@"Msg_Title",_txtViewReply.text,@"Msg_Body",[[NSMutableArray alloc] initWithObjects:sentUserDetsilsDic, nil],@"recipientDetailsArray", nil];
             replySuccess=[[SWDatabaseManager retrieveManager]InsertV2MessagesIntoDb:messageDetailsDic];
        }
        else
        {
            NSDictionary *replyMessageDetails=[[NSDictionary alloc]initWithObjectsAndKeys:messageDetails.Message_Title,@"Msg_Title",_txtViewReply.text, @"Msg_Body",messageDetails.msg_Id,@"Parent_Msg_ID",[NSNumber numberWithInt:messageDetails.SalesRep_ID],@"Reciepient_Id",messageDetails.SalesRep_Name,@"Reciepient_Name", nil];
            replySuccess= [[SWDatabaseManager retrieveManager]InsertV2ReplyMessagesIntoDb:replyMessageDetails];
            

        }
        if(replySuccess)
        {
             [SWDefaults showAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Message sent successfully" withController:self];
            _txtViewReply.editable=NO;
            _btnSend.hidden = YES;
            if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
            {
                sentMessages = [[SWDatabaseManager retrieveManager] dbGetV2FSRSentMessages];
            }
            else
            {
                sentMessages = [[SWDatabaseManager retrieveManager] dbGetFSRSentMessages];
            }
            NSMutableArray *arrOfReplyMessage = [self fetchReplyMessages];
            
            NSLog(@"sent messages array response is %@", arrOfReplyMessage);
            
            if ([arrOfReplyMessage count] > 0)
            {
                sentMessages = [sentMessages arrayByAddingObjectsFromArray:arrOfReplyMessage];
                
            }
            sentMessages=[self sortMessages:sentMessages isPushNotification:NO];
            [_tblSent reloadData];
            [self pushNotificationArrived];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to save data" withController:self];
        }

          }
    else{
        
        //Submit Event
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotificationForAddResponse:) name:@"AddResponseNotification" object:nil];
        
        NSString *trimmedString = [_txtViewReply.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([_txtViewReply.text length]==0 || [trimmedString isEqualToString:@""])
        {
            alertType =@"Failure";
            [self UserAlert:NSLocalizedString(@"Please enter reply", nil)];
            return;
        }
        else
        {
            NSString *dateString = [self getCurrentDate];
            
            NSString *strQuery = [NSString stringWithFormat:@"update TBL_FSR_Messages Set Message_Reply ='%@' ,Reply_Date=\"%@\" ,Emp_Code='%@' where Message_ID =%d and salesRep_ID=%d",_txtViewReply.text,dateString,[[SWDefaults userProfile] stringForKey:@"Emp_Code"],messageDetails.Message_ID,messageDetails.SalesRep_ID ];
            [[SWDatabaseManager retrieveManager] updateReadMessagestatus:strQuery];
        }

        
    }
    
    
    
    
}
#pragma mark
#pragma mark Notification method
- (void) receiveNotificationForAddResponse:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"AddResponseNotification"])
    {
        alertType =@"Success";
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AddResponseNotification" object:nil];
        [self UserAlert:@"Message sent successfully"];
        _txtViewReply.editable=NO;
        _btnSend.hidden = YES;
        if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
        {
            sentMessages = [[SWDatabaseManager retrieveManager] dbGetV2FSRSentMessages];
        }
        else
        {
            sentMessages = [[SWDatabaseManager retrieveManager] dbGetFSRSentMessages];
        }
        NSMutableArray *arrOfReplyMessage = [self fetchReplyMessages];
        
        NSLog(@"sent messages array response is %@", arrOfReplyMessage);
        
        if ([arrOfReplyMessage count] > 0)
        {
            sentMessages = [sentMessages arrayByAddingObjectsFromArray:arrOfReplyMessage];
            
        }
        sentMessages=[self sortMessages:sentMessages isPushNotification:NO];
        
        [_tblSent reloadData];
    }
}

#pragma mark
-(void)updateValues
{
    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        sentMessages = [[SWDatabaseManager retrieveManager] dbGetV2FSRSentMessages];
    }
    else
    {
        sentMessages = [[SWDatabaseManager retrieveManager] dbGetFSRSentMessages];
    }
    NSMutableArray *arrOfReplyMessage = [self fetchReplyMessages];
    if ([arrOfReplyMessage count] > 0)
    {
        sentMessages = [sentMessages arrayByAddingObjectsFromArray:arrOfReplyMessage];
    }
    sentMessages=[self sortMessages:sentMessages isPushNotification:NO];
    [_tblSent reloadData];
}


#pragma mark
#pragma mark Alertview method

-(void)UserAlert:(NSString *)Message
{
    //    [ILAlertView showWithTitle:NSLocalizedString(@"Message", nil)
    //                       message:Message
    //              closeButtonTitle:NSLocalizedString(@"OK", nil)
    //             secondButtonTitle:nil
    //           tappedButtonAtIndex:nil];
    UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(Message, nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
    [ErrorAlert show];
    
}
#pragma mark
#pragma mark TextView delegate method
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        
        return NO;
    }
    return textView.text.length + (text.length - range.length) <= 100;
    
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView:textView up:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:textView up:NO];
}
- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    NSInteger movementDistance=250;
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"communicationHeaderCell";
    
    CommunicationHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"CommunicationHeaderTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.layer.borderWidth = 1.0;
    cell.layer.borderColor = kUITableViewBorderColor.CGColor;
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.lblUnread.text=NSLocalizedString(@"Unread", nil);
    if (tableView == _tblInbox)
    {
        cell.imgView.image = [UIImage imageNamed:@"Message_Inbox"];

        if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) {
            cell.imgView.image = [[UIImage imageNamed:@"Message_Inbox"] imageFlippedForRightToLeftLayoutDirection];
        }
        cell.lblTitle.text = NSLocalizedString(@"Inbox", nil);
        cell.lblTitle.textColor = [UIColor colorWithRed:(17.0/255.0) green:(176.0/255.0) blue:(101.0/255.0) alpha:1];
        
        if ([messageArray count] == 0)
        {
            cell.lblNumberOfUnread.hidden = YES;
            cell.lblUnread.hidden = YES;
        } else
        {
            int totalUnreadMessage = 0;
            for (int i = 0; i<[messageArray count]; i++)
            {
                if ([[[messageArray objectAtIndex:i]valueForKey:@"Message_Read"] isEqualToString:@"N"])
                {
                    totalUnreadMessage++;
                } else {
                }
            }
            cell.lblNumberOfUnread.text = [NSString stringWithFormat:@"(%d)",totalUnreadMessage];
        }
        
        return cell.contentView;
    }
    else
    {
        cell.imgView.image = [UIImage imageNamed:@"Message_SentIcon"];
        if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) 
            {
                cell.imgView.image = [[UIImage imageNamed:@"Message_SentIcon"] imageFlippedForRightToLeftLayoutDirection];

            }
        cell.lblTitle.text = NSLocalizedString(@"Sent", nil);
        cell.lblTitle.textColor = [UIColor colorWithRed:(0.0/255.0) green:(128.0/255.0) blue:(255.0/255.0) alpha:1];
        cell.lblNumberOfUnread.hidden = YES;
        cell.lblUnread.hidden = YES;
        return cell.contentView;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView isEqual:_tblInbox])
    {
        if(searching)
            return [searchMessageArray count];
        else
            return [messageArray count];
    }
    else
    {
        if(searching)
            return [searchSentMessageArray count];
        else
            return [sentMessages count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
// Customize the appearance of table view cells.
{
    @autoreleasepool
    {
        static NSString* identifier=@"PKMessageTableviewCell";
        MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MessageTableViewCell" owner:nil options:nil] firstObject];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        if([tableView isEqual:_tblInbox])
        {
            FSRMessagedetails *info;
            if(searching)
                info =[searchMessageArray objectAtIndex:indexPath.row];
            else
                info =[messageArray objectAtIndex:indexPath.row];

            cell.lblDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"MMM dd" scrString:[SWDefaults getValidStringValue:info.Message_date]];
            cell.lblDate.font = kSWX_FONT_SEMI_BOLD(16);

            if([info.Message_Read isEqualToString:@"Y"])
            {
                cell.imgView.frame = CGRectMake(8, 22, 20, 20);
                cell.lblTitle.font = kSWX_FONT_SEMI_BOLD(18);
                cell.lblMessage.font = kSWX_FONT_SEMI_BOLD(16);
            } else {
                cell.imgView.frame = CGRectMake(8, 22, 10, 10);
                cell.lblTitle.font = kSWX_FONT_SEMI_BOLD(16);
                cell.lblMessage.font = kSWX_FONT_SEMI_BOLD(14);;
            }
            
            if(selectedTable==tableView && selectedIndexPath.row==indexPath.row){
                cell.lblTitle.textColor=MedRepMenuTitleSelectedCellTextColor;
                [cell.lblMessage setTextColor:MedRepMenuTitleSelectedCellTextColor];
                cell.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
            }
            else{
                cell.lblTitle.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.lblMessage.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;

            }
            
            cell.lblTitle.text=info.Message_Title;
            cell.lblMessage.text=info.Message_Content;

            if (info.isFromPushNotifications==YES) {
                cell.imgView.image = [UIImage imageNamed:@"Message_Read"];

            }
            else
            {
                cell.imgView.image = [UIImage imageNamed:@"Message_Unread"];

            }
            return cell;
        }
        else
        {
            NSMutableDictionary *info = nil;
            if(searching)
            {
                info = [NSMutableDictionary dictionaryWithDictionary:[searchSentMessageArray objectAtIndex:indexPath.row]];
            }
            else
            {
                info = [NSMutableDictionary dictionaryWithDictionary:[sentMessages objectAtIndex:indexPath.row]];
            }
            
            cell.lblTitle.text = [info stringForKey:@"Message_Title"];
            NSString* reply=[SWDefaults getValidStringValue:[info stringForKey:@"Message_Reply"]];
            
            if ([NSString isEmpty:reply]==NO) {
                cell.lblMessage.text = [info stringForKey:@"Message_Reply"];
            }
            else
            {
                cell.lblMessage.text = [info stringForKey:@"Message_Content"];
            }
            
            cell.imgView.image = [UIImage imageNamed:@"Message_Read"];
            cell.imgView.frame = CGRectMake(8, 22, 20, 20); 
        
            cell.lblDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"MMM dd" scrString:[SWDefaults getValidStringValue:[info stringForKey:@"Message_date"]]];
            if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
            {
                cell.lblDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"MMM dd" scrString:[SWDefaults getValidStringValue:[info stringForKey:@"Logged_At"]]];
            }
            cell.lblTitle.font = kSWX_FONT_SEMI_BOLD(16);
            cell.lblMessage.font = kSWX_FONT_SEMI_BOLD(14);
            cell.lblDate.font = kSWX_FONT_SEMI_BOLD(16);
            if([[info stringForKey:@"Message_Read"] isEqualToString:@"Y"])
            {
                cell.imgView.image = [UIImage imageNamed:@"Message_Sent"];
  
                
            } else {
                cell.imgView.image = [UIImage imageNamed:@"Message_Waiting"];
            }
            if(selectedTable==tableView && selectedIndexPath.row==indexPath.row){
                cell.lblTitle.textColor=MedRepMenuTitleSelectedCellTextColor;
                [cell.lblMessage setTextColor:MedRepMenuTitleSelectedCellTextColor];
                cell.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
            }
            else{
                cell.lblTitle.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.lblMessage.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
                
            }
            return cell;
        }
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Table view delegate


-(NSMutableArray*)sortMessages:(NSMutableArray*)contentArray isPushNotification:(BOOL)isPushMessage
{
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Message_date" ascending:NO];

    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value] && isPushMessage == NO)
    {
        valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Logged_At" ascending:NO];
    }
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *sortedArray = [contentArray sortedArrayUsingDescriptors:descriptors];
    return [sortedArray mutableCopy];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    _lblReply.hidden = YES;
    _txtViewReply.hidden = YES;
    _btnSend.hidden = YES;
    _lblName.hidden = NO;
    _lblDate.hidden = NO;
    _txtViewMessage.hidden = NO;
    _lblMsg.hidden = YES;
    noMessageSelectedImage.hidden=YES;
    
    @try {
        [self tableView:selectedTable didDeselectRowAtIndexPath:selectedIndexPath];
    }
    @catch (NSException *exception) {
        
    }
    
    selectedTable = tableView;
    selectedIndexPath = indexPath;
    
    if(tableView == _tblInbox)
    {
        
        
        
        checkWhichTableIsSelected = false;
        
        if(searching)
            messageDetails =[searchMessageArray objectAtIndex:indexPath.row];
        else
            messageDetails =[messageArray objectAtIndex:indexPath.row];
        
        
       
        
        _lblName.text = messageDetails.Message_Title;
        _txtViewMessage.text = messageDetails.Message_Content;
        @try
        {
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.timeZone = [NSTimeZone systemTimeZone];
            
            [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
            NSDate* myDate = [dateFormatter dateFromString:messageDetails.Message_date];
            [dateFormatter setDateFormat:kDateFormatWithTime];
            
            if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
            {
                
                
                NSMutableAttributedString *fromAttributedString=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@: ",@"From"] ];
                
                [fromAttributedString addAttribute:NSForegroundColorAttributeName
                               value:MedRepElementTitleLabelFontColor
                               range:NSMakeRange(0, fromAttributedString.length)];
                
                [fromAttributedString addAttribute:NSFontAttributeName
                                             value:MedRepElementTitleLabelFont
                                             range:NSMakeRange(0, fromAttributedString.length)];
                

                
                NSMutableAttributedString *DateAttributedString=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@: ",@"Date"]];
                
                [DateAttributedString addAttribute:NSForegroundColorAttributeName
                                             value:MedRepElementTitleLabelFontColor
                                             range:NSMakeRange(0, DateAttributedString.length)];
                
                [DateAttributedString addAttribute:NSFontAttributeName
                                             value:MedRepElementTitleLabelFont
                                             range:NSMakeRange(0, DateAttributedString.length)];
                
                
                NSMutableAttributedString *senderNameAttributedstring=[[NSMutableAttributedString alloc]initWithString:messageDetails.SalesRep_Name ];
                
                [senderNameAttributedstring addAttribute:NSForegroundColorAttributeName
                                             value:MedRepDescriptionLabelFontColor
                                             range:NSMakeRange(0, senderNameAttributedstring.length)];
                
                [senderNameAttributedstring addAttribute:NSFontAttributeName
                                             value:kSWX_FONT_REGULAR(16)
                                             range:NSMakeRange(0, senderNameAttributedstring.length)];
                
                NSMutableAttributedString *sentDateAttributedstring=[[NSMutableAttributedString alloc]initWithString:[dateFormatter stringFromDate:myDate] ];
                
                [sentDateAttributedstring addAttribute:NSForegroundColorAttributeName
                                                   value:MedRepDescriptionLabelFontColor
                                                   range:NSMakeRange(0, sentDateAttributedstring.length)];
                
                [sentDateAttributedstring addAttribute:NSFontAttributeName
                                                   value:kSWX_FONT_REGULAR(16)
                                                   range:NSMakeRange(0, sentDateAttributedstring.length)];

                
                [fromAttributedString appendAttributedString:senderNameAttributedstring];
                [fromAttributedString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"\n"]];
                [fromAttributedString appendAttributedString:DateAttributedString];
                [fromAttributedString appendAttributedString:sentDateAttributedstring];

                
            _lblDate.attributedText = fromAttributedString;
            }
            else
            {
                _lblDate.text =[dateFormatter stringFromDate:myDate];

            }
        }
        @catch (NSException *exception) {
            
        }

        NSString *dateString = [self getCurrentDate];
        
        if ([messageDetails.Message_Read isEqualToString:@"N"]) {
            
            if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
            {
                [[SWDatabaseManager retrieveManager]updateV2MessageReadStatus:messageDetails.msg_Id];
            }
            else
            {
                NSString *strQuery = [NSString stringWithFormat:@"update TBL_FSR_Messages Set Message_Read = 'Y' , Message_Date ='%@' where Message_ID =%d and salesRep_ID=%d",dateString,messageDetails.Message_ID,messageDetails.SalesRep_ID ];
                [[SWDatabaseManager retrieveManager] updateReadMessagestatus:strQuery];
            }

            
            if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
            {
                messageArray = [[[SWDatabaseManager retrieveManager] dbGetV2FSRMessages] mutableCopy];
            }
            else
            {
                messageArray = [[[SWDatabaseManager retrieveManager] dbGetFSRMessages] mutableCopy];
            }


           
            
            
            //add pushnotification content
            NSMutableArray*  pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
            if (pushNotificationsContentArray.count>0) {
                
                //indexpath might change if we add contents to message array sp keep an instance of current indexpath and update that object after adding push messages
                pushNotificationsContentArray=[self sortMessages:pushNotificationsContentArray isPushNotification:YES];

                
                [messageArray addObjectsFromArray:pushNotificationsContentArray];
                
                messageDetails=[messageArray objectAtIndex:indexPath.row];
                messageDetails.Message_Read=@"Y";
                [messageArray replaceObjectAtIndex:indexPath.row withObject:messageDetails];
                
                //update the flag in push messages
            
                NSPredicate * pushNotificationMessagesPredicate=[NSPredicate predicateWithFormat:@"SELF.isFromPushNotifications == 1"];
                NSMutableArray* pushMessageContentArray=[[messageArray filteredArrayUsingPredicate:pushNotificationMessagesPredicate] mutableCopy];
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushMessageContentArray];
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"PUSHED_MESSAGES"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                

            }
            
            //sort messages by message date
            
           // messageArray=[self sortMessages:messageArray];
           // NSLog(@"check message date order %@", [messageArray valueForKey:@"Message_date"]);
            
            
            [_tblInbox reloadData];
        }
        MessageTableViewCell *cell = [_tblInbox cellForRowAtIndexPath:indexPath];
        cell.lblTitle.textColor = [UIColor whiteColor];
        cell.lblMessage.textColor = [UIColor whiteColor];
        cell.lblDate.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        
        
        [self checkButtonReplyIsHiddenOrNot];
        
        if (messageDetails.isFromPushNotifications==YES) {
            
            pushNotificationImageView.hidden=NO;
            
            UIImage *thumbNail=[UIImage imageWithContentsOfFile:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:messageDetails.Push_Notification_ID]];
            
            BOOL fileExists=[[NSFileManager defaultManager] fileExistsAtPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:messageDetails.Push_Notification_ID]];
            if (fileExists==YES) {
                pushNotificationImageView.image=thumbNail;
  
            }
            else{
                pushNotificationImageView.image=[UIImage imageNamed:@""];

            }
            
            
            
            _btnReply.hidden=YES;
            
        }
        else
        {
            pushNotificationImageView.hidden=YES;
            pushNotificationImageView.image=nil;
            if ([NSString isEmpty:messageDetails.Message_Reply]==NO ) {
                _btnReply.hidden=YES;
                
            }
            else
            {
                _btnReply.hidden=NO;
                
            }
            
        }
        
        
    }
    else
    {
        checkWhichTableIsSelected = true;
        
        pushNotificationImageView.hidden=YES;
        pushNotificationImageView.image=nil;
        
        MessageTableViewCell *cell = [_tblSent cellForRowAtIndexPath:indexPath];
        cell.lblTitle.textColor = [UIColor whiteColor];
        cell.lblMessage.textColor = [UIColor whiteColor];
        cell.lblDate.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
    
        NSMutableDictionary *info = nil;
        if(searching)
            info = [NSMutableDictionary dictionaryWithDictionary:[searchSentMessageArray objectAtIndex:indexPath.row]];
        else
            info = [NSMutableDictionary dictionaryWithDictionary:[sentMessages objectAtIndex:indexPath.row]];
        
        _lblName.text = [info stringForKey:@"Message_Title"];
        
        @try
        {
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.timeZone = [NSTimeZone systemTimeZone];
            
            [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
            NSDate* myDate = [dateFormatter dateFromString:[info stringForKey:@"Message_date"]];
            if (myDate == nil) {
                myDate = [dateFormatter dateFromString:[info stringForKey:@"Message_Date"]];
            }
            if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
            {
                NSDate* myDate = [dateFormatter dateFromString:[info stringForKey:@"Logged_At"]];
            }

            [dateFormatter setDateFormat:kDateFormatWithTime];
            
            
            
            _lblDate.text = [dateFormatter stringFromDate:myDate];
            if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
            {
                NSMutableAttributedString *ToAttributedString=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@: ",@"To"] ];

                if([info stringForKey:@"SentUserNames"]==nil || [[SWDefaults getValidStringValue:[info stringForKey:@"SentUserNames"]] length]==0)
                {
                    ToAttributedString=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@: ",@"From"] ];
                }
                
                [ToAttributedString addAttribute:NSForegroundColorAttributeName
                                             value:MedRepElementTitleLabelFontColor
                                             range:NSMakeRange(0, ToAttributedString.length)];
                
                [ToAttributedString addAttribute:NSFontAttributeName
                                             value:MedRepElementTitleLabelFont
                                             range:NSMakeRange(0, ToAttributedString.length)];
                
                
                
                NSMutableAttributedString *DateAttributedString=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@: ",@"Date"]];
                
                [DateAttributedString addAttribute:NSForegroundColorAttributeName
                                             value:MedRepElementTitleLabelFontColor
                                             range:NSMakeRange(0, DateAttributedString.length)];
                
                [DateAttributedString addAttribute:NSFontAttributeName
                                             value:MedRepElementTitleLabelFont
                                             range:NSMakeRange(0, DateAttributedString.length)];
                
                
                NSMutableAttributedString *reciepientNameAttributedstring=[[NSMutableAttributedString alloc]initWithString:[info stringForKey:@"SentUserNames"] ];
                
                
                if([info stringForKey:@"SentUserNames"]==nil || [[SWDefaults getValidStringValue:[info stringForKey:@"SentUserNames"]] length]==0)
                {
                    reciepientNameAttributedstring=[[NSMutableAttributedString alloc]initWithString:[info stringForKey:@"Sender_Name"] ];
                }
                
                [reciepientNameAttributedstring addAttribute:NSForegroundColorAttributeName
                                                   value:MedRepDescriptionLabelFontColor
                                                   range:NSMakeRange(0, reciepientNameAttributedstring.length)];
                
                [reciepientNameAttributedstring addAttribute:NSFontAttributeName
                                                   value:kSWX_FONT_REGULAR(16)
                                                   range:NSMakeRange(0, reciepientNameAttributedstring.length)];
                
                NSMutableAttributedString *sentDateAttributedstring=[[NSMutableAttributedString alloc]initWithString:[dateFormatter stringFromDate:myDate] ];
                
                [sentDateAttributedstring addAttribute:NSForegroundColorAttributeName
                                                 value:MedRepDescriptionLabelFontColor
                                                 range:NSMakeRange(0, sentDateAttributedstring.length)];
                
                [sentDateAttributedstring addAttribute:NSFontAttributeName
                                                 value:kSWX_FONT_REGULAR(16)
                                                 range:NSMakeRange(0, sentDateAttributedstring.length)];
                
                
                [ToAttributedString appendAttributedString:reciepientNameAttributedstring];
                [ToAttributedString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"\n"]];
                [ToAttributedString appendAttributedString:DateAttributedString];
                [ToAttributedString appendAttributedString:sentDateAttributedstring];
                
                _lblDate.attributedText = ToAttributedString;/*[NSString stringWithFormat:@"To: %@\nDate: %@",[info stringForKey:@"SentUserNames"],[dateFormatter stringFromDate:myDate]]*/;
            }
        }
        @catch (NSException *exception) {
            
        }
        @try {
            if ([NSString isEmpty:[info valueForKey:@"Message_Reply"]]==NO) {
                _txtViewReply.hidden = NO;
                _txtViewReply.text = [info stringForKey:@"Message_Reply"];
                _txtViewReply.editable=NO;
                _lblReply.hidden = NO;
                
                

            }
        }
        @catch (NSException *exception) {
            
        }
        
        _btnReply.hidden = YES;
        _txtViewMessage.text = [info stringForKey:@"Message_Content"];
    }
    [tableView reloadData];
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.lblTitle.textColor = [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1];
    cell.lblMessage.textColor = [UIColor colorWithRed:(70.0/255.0) green:(90.0/255.0) blue:(120.0/255.0) alpha:1];
    cell.lblDate.textColor = [UIColor colorWithRed:(70.0/255.0) green:(90.0/255.0) blue:(120.0/255.0) alpha:1];
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
}


#pragma mark Search Delegate Methods

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    [self.view endEditing:YES];
    
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    searching = NO;
    
    [_tblInbox reloadData];
    [_tblSent reloadData];
    
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar1
{
    searchBar1.showsScopeBar = YES;
    [searchBar1 setShowsCancelButton:YES animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    if([searchText length] != 0)
    {
        searching = YES;
        if(searchMessageArray)
            [searchMessageArray removeAllObjects];
        
        if(searchSentMessageArray)
            [searchSentMessageArray removeAllObjects];
        
        for(int i =0;i<[messageArray count];i++)
        {
            FSRMessagedetails *info =[messageArray objectAtIndex:i];
            NSString *strTemp = info.Message_Title;
            
            NSRange titleResultsRange = [strTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (titleResultsRange.length > 0)
                [searchMessageArray addObject:info];
        }
        
        for(int i =0;i<[sentMessages count];i++)
        {
            NSMutableDictionary *info =[NSMutableDictionary dictionaryWithDictionary:[sentMessages objectAtIndex:i]] ;
            NSString *strTemp = [info stringForKey:@"Message_Title"];
            
            NSRange titleResultsRange = [strTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (titleResultsRange.length > 0)
                [searchSentMessageArray addObject:info];
        }
    }
    else {
        searching = NO;
    }
    [_tblInbox reloadData];
    [_tblSent reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [self.view endEditing:YES];
    
    searchBar1.showsScopeBar = NO;
    [searchBar1 setShowsCancelButton:NO animated:YES];
    searching=YES;
}

#pragma mark Search method
//- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
//{
//    searching = YES;
//    NSString *searchText = searchBar.text;
//    if(searchMessageArray)
//        [searchMessageArray removeAllObjects];
//	   
//    if(searchSentMessageArray)
//        [searchSentMessageArray removeAllObjects];
//    
//    for(int i =0;i<[messageArray count];i++)
//    {
//        FSRMessagedetails *info =[messageArray objectAtIndex:i];
//        NSString *strTemp = info.Message_Title;
//        
//        NSRange titleResultsRange = [strTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
//        
//        if (titleResultsRange.length > 0)
//            [searchMessageArray addObject:info];
//    }
//    
//    for(int i =0;i<[sentMessages count];i++)
//    {
//        NSMutableDictionary *info =[NSMutableDictionary dictionaryWithDictionary:[sentMessages objectAtIndex:i]] ;
//        NSString *strTemp = [info stringForKey:@"Message_Title"];
//        
//        NSRange titleResultsRange = [strTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
//        
//        if (titleResultsRange.length > 0)
//            [searchSentMessageArray addObject:info];
//    }
//    [_tblInbox reloadData];
//    [_tblSent reloadData];
//}
- (void)Done:(id)sender
{
    [searchBar removeFromSuperview];
    [doneButton removeFromSuperview];
    
    _tblInbox.frame=CGRectMake(8,60,484,308);
    _tblSent.frame=CGRectMake(8,376,484,308);
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
    searching = NO;
    [_tblInbox reloadData];
    [_tblSent reloadData];
}
//- (void)searchBarCancelButtonClicked:(UISearchBar *) SearchBar
//{
//    searchBar.text = @"";
//    [searchBar resignFirstResponder];
//    searching = NO;
//    
//    [_tblInbox reloadData];
//    [_tblSent reloadData];
//}
//- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar1
//{
//    searchBar1.showsScopeBar = YES;
//    //[searchBar1 sizeToFit];
//    [searchBar1 setShowsCancelButton:YES animated:YES];
//    
//    return YES;
//}
//
//- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar1
//{
//    searchBar1.showsScopeBar = NO;
//    //[searchBar1 sizeToFit];
//    [searchBar1 setShowsCancelButton:NO animated:YES];
//    
//    return YES;
//}

#pragma mark
-(NSString *)getCurrentDate
{
    //Update MessageReply and datetime
    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];
    
    formater=nil;
    usLocale=nil;
    
    return dateString;
}
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}
#pragma mark
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIScrollView Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==messagesScrollView) {
        
    
     
     
            [scrollView setContentOffset: CGPointMake(scrollView.contentOffset.x,0)];

      
        NSLog(@"content offset set to %@",NSStringFromCGPoint(scrollView.contentOffset));
        
        
    static NSInteger previousPage = 0;
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    NSLog(@" current page in did scroll %ld",(long)page);
    if (previousPage != page) {
        previousPage = page;
        /* Page did change */
    }
    [messagesSegment setSelectedSegmentIndex:page];
        
//        if (page==1) {
//            
//            [messagesScrollView setScrollEnabled:NO];
//        }
//        else
//        {
//            [messagesScrollView setScrollEnabled:YES];
//
//        }
    }
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

   
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    
    CGRect frame = messagesScrollView.frame;
    frame.origin.x = frame.size.width * segmentedControl.selectedSegmentIndex;
    frame.origin.y = 0;
    [messagesScrollView scrollRectToVisible:frame animated:YES];
    
        
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [messageArray removeObjectAtIndex:indexPath.row];
        
        
        
        
        NSPredicate * pushNotificationMessagesPredicate=[NSPredicate predicateWithFormat:@"SELF.isFromPushNotifications == 1"];
        NSMutableArray* pushMessageContentArray=[[messageArray filteredArrayUsingPredicate:pushNotificationMessagesPredicate] mutableCopy];
        
        pushMessageContentArray=[self sortMessages:pushMessageContentArray isPushNotification:YES];

        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushMessageContentArray];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"PUSHED_MESSAGES"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
        
        
        [_tblInbox deselectRowAtIndexPath:indexPath animated:YES];
        
        [_tblInbox reloadData];
        
        _lblName.hidden = YES;
        _lblDate.hidden = YES;
        _txtViewMessage.hidden = YES;
        _btnReply.hidden = YES;
        _lblMsg.hidden = NO;
        noMessageSelectedImage.hidden=NO;
        _txtViewReply.hidden = YES;
        _lblReply.hidden = YES;
        _btnSend.hidden = YES;
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewCellEditingStyleDelete;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblInbox) {
    FSRMessagedetails * currentMessage=[messageArray objectAtIndex:indexPath.row];
    
    if (currentMessage.isFromPushNotifications==YES) {
        
        return YES;
    }
    else
    {
        return NO;
    }
    }
    
    else
    {
        return NO;
    }
    
}



@end
