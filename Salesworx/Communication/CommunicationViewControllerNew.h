////
////  CommunicationViewControllerNew.h
////  DataSyncApp
////
////  Created by sapna on 1/28/13.
////  Copyright (c) 2013 Sapna.Shah. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "DatePickerViewController.h"
//#import "CustomerShipAddressDetails.h"
//#import "SWPlatform.h"
//@interface CommunicationViewControllerOld : FTPBaseViewController<DatePickerDelegate,UISearchBarDelegate,UIPopoverControllerDelegate>
//{
//    IBOutlet UITableView *tblMessages,*tblUpcomingVisit , *tblSentMessages;
//    IBOutlet UISearchBar *searchBar;
//    IBOutlet UIView *messageView,*UpcomingVisistView;
//    IBOutlet UILabel *messageHeading,*visistHeading,*dateLabel;
//    IBOutlet UIButton *addButton , *searchButton;
//    
//    
//    UIPopoverController *datePickerPopover;
//    DatePickerViewController *_colorPicker;
//    CustomerShipAddressDetails *customerInfo;
//    
//    NSMutableArray *sentMessages;
//    NSArray *messageArray;
//    NSArray *planeedVisitArray;
//    NSMutableArray *CustomerShipDetailsArray;
//    NSMutableArray *searchMessageArray;
//    NSMutableArray *searchSentMessageArray;
//    
//    
//    
//    BOOL searching;
//    
//    NSMutableDictionary *customer;
//    BOOL isFurtureDate;
//    UIView *myBackgroundView;
//}
//
//@property (nonatomic, strong) NSMutableDictionary *customer;
//-(IBAction)btnAddMessagePressed:(id)sender;
//-(IBAction)btnSerachMessagePressed:(id)sender;
//-(IBAction)btnDatePressed:(id)sender;
//@end


//
//  CommunicationViewControllerNew.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSRMessagedetails.h"
#import <SalesWorxCommunicationMessagesSyncManager.h>
#import "HMSegmentedControl.h"
#import "SalesWorxPushNotificationMessagesViewController.h"
#import "SalesWorxCommunicationsScrollView.h"
#import "SalesWorxDefaultButton.h"
#import "MedRepElementTitleDescriptionLabel.h"
#import "SalesWorxDefaultButton.h"
#import "SalesWorxImageView.h"
@interface CommunicationViewControllerNew : UIViewController<UISearchBarDelegate,UIPopoverControllerDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    BOOL checkWhichTableIsSelected;
    NSArray *sentMessages;
    NSMutableArray *messageArray;
    
    NSMutableArray *searchMessageArray;
    NSMutableArray *searchSentMessageArray;
    
    UITableView *selectedTable;
    NSIndexPath *selectedIndexPath;
    IBOutlet HMSegmentedControl *messagesSegment;
    
    IBOutlet SalesWorxCommunicationsScrollView *messagesScrollView;
    BOOL searching;
   // UISearchBar *searchBar;
    UIButton *doneButton;
    IBOutlet SalesWorxImageView *noMessageSelectedImage;
    
    IBOutlet SalesWorxImageView *pushNotificationImageView;
    NSString *alertType;
    FSRMessagedetails *messageDetails;
    UIView *NewMessagePopoverView;
    UIPopoverController *popOverForNewMessage;
    
    IBOutlet UISearchBar *searchBar;
    
}
@property (strong, nonatomic) IBOutlet UIView *inboxView;
@property (strong, nonatomic) IBOutlet UIView *detailView;

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblReply;

@property (strong, nonatomic) IBOutlet UITextView *txtViewMessage;
@property (strong, nonatomic) IBOutlet UITextView *txtViewReply;

@property (strong, nonatomic) IBOutlet UITableView *tblInbox;
@property (strong, nonatomic) IBOutlet UITableView *tblSent;

@property (strong, nonatomic) IBOutlet SalesWorxDefaultButton *btnComposeMessage;
//@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet SalesWorxDefaultButton *btnReply;
@property (strong, nonatomic) IBOutlet UIButton *btnSend;


@end
