//
//  FSRMessagedetails.h
//  DataSyncApp
//
//  Created by sapna on 1/29/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSRMessagedetails : NSObject<NSCoding>
{
    int _Message_ID;
    int _SalesRep_ID;
    NSString *_Message_Title;
    NSString *_Message_Content;
    NSString *_Message_date;
    NSString *_Message_Expiry_date;
    NSString *_Message_Read;
    NSString *_Message_Reply;
    NSString *_Reply_Date;
    NSString *_Emp_Code;
    NSString *_SalesRep_Name;
    NSString *_is_Replied_Message;

}
@property(nonatomic,assign)int Message_ID;
@property(nonatomic,assign)int SalesRep_ID;
@property(nonatomic,strong)NSString* SalesRep_Name;
@property(nonatomic,strong)NSString* is_Replied_Message;

@property(nonatomic,copy) NSString *Message_Title;
@property(nonatomic,copy) NSString *Message_Content;
@property(nonatomic,copy) NSString *Message_date;
@property(nonatomic,copy) NSString *Message_Expiry_date;
@property(nonatomic,copy) NSString *Message_Read;
@property(nonatomic,copy) NSString *Message_Reply;
@property(nonatomic,copy) NSString *Emp_Code;
@property(nonatomic,copy) NSString *Reply_Date;
@property(nonatomic) BOOL isFromPushNotifications;
@property(nonatomic,strong) NSString *imageURL;
@property(nonatomic,strong) NSString *imageID;
@property(nonatomic,strong) NSString *Push_Notification_ID;
@property(nonatomic,strong) NSString *msg_Id;


-(id)initWithMessage_Id:(int)Message_ID SalesRep_ID:(int)SalesRep_ID Message_Title:(NSString*)Message_Title Message_Content:(NSString*)Message_Content Message_date:(NSString*)Message_date Message_Expiry_date:(NSString*)Message_Expiry_date Message_Read:(NSString*)Message_Read Message_Reply:(NSString*)Message_Reply Emp_Code:(NSString*)Emp_Code Reply_Date:(NSString*)Reply_Date;

-(id)initWithMessage_Id:(int)Message_ID SalesRep_ID:(int)SalesRep_ID Message_Title:(NSString*)Message_Title Message_Content:(NSString*)Message_Content Message_date:(NSString*)Message_date Message_Expiry_date:(NSString*)Message_Expiry_date Message_Read:(NSString*)Message_Read Message_Reply:(NSString*)Message_Reply Emp_Code:(NSString*)Emp_Code Reply_Date:(NSString*)Reply_Date msg_Id:(NSString *)msg_ID SalesRep_Name:(NSString*)senderName isRepliedMessage:(NSString *)isRepliedMessage;
@end
