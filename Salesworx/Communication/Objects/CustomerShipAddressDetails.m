//
//  CustomerShipAddressDetails.m
//  DataSyncApp
//
//  Created by sapna on 1/30/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "CustomerShipAddressDetails.h"

@implementation CustomerShipAddressDetails
@synthesize Site_Use_ID=_Site_Use_ID;
@synthesize Customer_ID =_Customer_ID;
@synthesize Customer_Name =_Customer_Name;

-(id)initWithCustomerID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID Customer_Name:(NSString*)Customer_Name
{
    if ((self = [super init])) {
        self.Customer_ID=Customer_ID;
        self.Site_Use_ID=Site_Use_ID;
        self.Customer_Name =Customer_Name;
    }
    return self;
}

@end
