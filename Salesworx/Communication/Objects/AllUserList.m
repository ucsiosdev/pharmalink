//
//  AllUserList.m
//  DataSyncApp
//
//  Created by sapna on 1/29/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "AllUserList.h"

@implementation AllUserList
@synthesize User_ID=_User_ID;
@synthesize  Username=_Username;
-(id)initWithUserId:(int)User_ID User:(NSString*)Username
{
    if ((self = [super init])) {
        
        self.User_ID=User_ID;
        self.Username=Username;
    }
    return self;
}

@end
