//
//  FSRMessagedetails.m
//  DataSyncApp
//
//  Created by sapna on 1/29/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "FSRMessagedetails.h"

@implementation FSRMessagedetails
@synthesize Message_ID =_Message_ID;
@synthesize SalesRep_ID =_SalesRep_ID;
@synthesize Message_Title =_Message_Title;
@synthesize Message_Content =_Message_Content;
@synthesize Message_date=_Message_date;
@synthesize Message_Expiry_date =_Message_Expiry_date;
@synthesize Message_Read =_Message_Read;
@synthesize Reply_Date =_Reply_Date;
@synthesize Emp_Code =_Emp_Code;
@synthesize SalesRep_Name =_SalesRep_Name;
@synthesize msg_Id =_msg_Id;
@synthesize is_Replied_Message=_is_Replied_Message;

-(id)initWithMessage_Id:(int)Message_ID SalesRep_ID:(int)SalesRep_ID Message_Title:(NSString*)Message_Title Message_Content:(NSString*)Message_Content Message_date:(NSString*)Message_date Message_Expiry_date:(NSString*)Message_Expiry_date Message_Read:(NSString*)Message_Read Message_Reply:(NSString*)Message_Reply Emp_Code:(NSString*)Emp_Code Reply_Date:(NSString*)Reply_Date
{
    if ((self = [super init])) {
        
        self.Message_ID =Message_ID;
        self.SalesRep_ID =SalesRep_ID;
        self.Message_Title =Message_Title;
        self.Message_Content =Message_Content;
        self.Message_date =Message_date;
        self.Message_Expiry_date=Message_Expiry_date;
        self.Message_Read=Message_Read;
        self.Message_Reply =Message_Reply;
        self.Emp_Code=Emp_Code;
        self.Reply_Date=Reply_Date;
    
    }
    return self;
}
-(id)initWithMessage_Id:(int)Message_ID SalesRep_ID:(int)SalesRep_ID Message_Title:(NSString*)Message_Title Message_Content:(NSString*)Message_Content Message_date:(NSString*)Message_date Message_Expiry_date:(NSString*)Message_Expiry_date Message_Read:(NSString*)Message_Read Message_Reply:(NSString*)Message_Reply Emp_Code:(NSString*)Emp_Code Reply_Date:(NSString*)Reply_Date msg_Id:(NSString *)msg_ID SalesRep_Name:(NSString*)senderName isRepliedMessage:(NSString *)isRepliedMessage
{
    if ((self = [super init])) {
        
        self.Message_ID =Message_ID;
        self.SalesRep_ID =SalesRep_ID;
        self.Message_Title =Message_Title;
        self.Message_Content =Message_Content;
        self.Message_date =Message_date;
        self.Message_Expiry_date=Message_Expiry_date;
        self.Message_Read=Message_Read;
        self.Message_Reply =Message_Reply;
        self.Emp_Code=Emp_Code;
        self.Reply_Date=Reply_Date;
        self.msg_Id=msg_ID;
        self.SalesRep_Name=senderName;
        self.is_Replied_Message=isRepliedMessage;
    }
    return self;
}
//this is done becuase we are saving push notification content in nsuserdefaults as message info objects so using nscoding protocol
- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.Message_Title = [decoder decodeObjectForKey:@"Message_Title"];
        self.Message_Content = [decoder decodeObjectForKey:@"Message_Content"];
        self.Message_date = [decoder decodeObjectForKey:@"Message_date"];
        self.isFromPushNotifications=[decoder decodeBoolForKey:@"isFromPushNotifications"];
        self.Message_Read=[decoder decodeObjectForKey:@"Message_Read"];
        self.imageURL=[decoder decodeObjectForKey:@"imageURL"];
        self.imageID=[decoder decodeObjectForKey:@"imageID"];
        self.Push_Notification_ID =[decoder decodeObjectForKey:@"Push_Notification_ID"];
        self.SalesRep_Name=@"";
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.Message_Title forKey:@"Message_Title"];
    [encoder encodeObject:self.Message_Content forKey:@"Message_Content"];
    [encoder encodeObject:self.Message_date forKey:@"Message_date"];
    [encoder encodeBool:self.isFromPushNotifications forKey:@"isFromPushNotifications"];
    [encoder encodeObject:self.Message_Read forKey:@"Message_Read"];
    [encoder encodeObject:self.imageURL forKey:@"imageURL"];
    [encoder encodeObject:self.imageID forKey:@"imageID"];
    [encoder encodeObject:self.Push_Notification_ID forKey:@"Push_Notification_ID"];
    
}

@end
