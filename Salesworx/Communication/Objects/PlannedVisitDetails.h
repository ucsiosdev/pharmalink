//
//  PlannedVisitDetails.h
//  DataSyncApp
//
//  Created by sapna on 1/30/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlannedVisitDetails : NSObject
{
    int _FSR_Plan_Detail_ID;
    int _Customer_ID;
    int _Site_Use_ID;
    int _SalesRep_ID;
    NSString * _Planned_Visit_ID;
    NSString * _Visit_Date;
    NSString * _Start_Time;
    NSString * _End_Time;
    NSString * _Visit_Status;
}
@property (nonatomic,assign)int FSR_Plan_Detail_ID;
@property (nonatomic,assign)int Customer_ID;
@property (nonatomic,assign)int Site_Use_ID;
@property (nonatomic,assign) int SalesRep_ID;
@property (nonatomic,copy) NSString * Planned_Visit_ID;
@property (nonatomic,copy) NSString * Visit_Date;
@property (nonatomic,copy) NSString * Start_Time;
@property (nonatomic,copy)NSString * End_Time;
@property (nonatomic,copy)NSString * Visit_Status;

-(id)initWithPlannedVisitID:(NSString*)Planned_Visit_ID FSR_Plan_Detail_ID:(int)FSR_Plan_Detail_ID Visit_Date:(NSString*)Visit_Date Customer_ID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID Start_Time:(NSString*)Start_Time End_Time:(NSString*)End_Time SalesRep_ID:(int)SalesRep_ID Visit_Status:(NSString*)Visit_Status;
@end

