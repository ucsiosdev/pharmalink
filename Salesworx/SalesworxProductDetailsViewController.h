//
//  SalesworxProductDetailsViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/16/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesworxProductDetailsViewController : UIViewController
{
    NSMutableArray *arrProduct;
}

@property (weak, nonatomic) IBOutlet UITableView *tblProduct;
-(void)updateProductData:(NSMutableArray *)arrayProduct;

@end
