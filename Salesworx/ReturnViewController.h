//
//  ReturnViewController.h
//  SWCustomer
//
//  Created by msaad on 1/2/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "ProductReturnViewController.h"
#import "ReturnAdditionalViewController.h"

@protocol OldReturnControllerDelegate <NSObject>
- (void)selectedProduct:(NSMutableDictionary *)product;
@end

@interface ReturnViewController : SWGridViewController < UIAlertViewDelegate , GridViewDataSource , GridViewDelegate>
{
    NSMutableDictionary *customer;
    NSDictionary *category;
    UILabel *totalPriceLabel;
    //
    ProductListViewController *productListViewController;
    UINavigationController *navigationControllerS;
    ProductReturnViewController *orderViewController;
    ReturnAdditionalViewController *orderAdditionalInfoViewController ;
    float totalOrderAmount;

    UIBarButtonItem *flexibleSpace  ;
    UIBarButtonItem *totalLabelButton1 ;
    
    UIBarButtonItem *totalLabelButton ;
    UIButton *saveButton  ;
    NSMutableDictionary *productDict;
    UILabel *totalProduct;

}

@property (unsafe_unretained) id <OldReturnControllerDelegate> delegate;

- (id)initWithCustomer:(NSDictionary *)customerDict andCategory:(NSDictionary *)categoryDict;
- (void)productAdded:(NSMutableDictionary *)q;
@end
