//
//  SalesWorxTitleLabel5_SemiBold.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxTitleLabel5_SemiBold.h"
#import "MedRepDefaults.h"

@implementation SalesWorxTitleLabel5_SemiBold

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    self.font=kSWX_FONT_SEMI_BOLD(16);
    self.textColor=MedRepElementTitleLabelFontColor;
    if(self.RTLSupport)
        super.text=NSLocalizedString(super.text, nil);
}

-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
    
    
}

@end
