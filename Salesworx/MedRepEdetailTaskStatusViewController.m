//
//  MedRepEdetailTaskStatusViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/26/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepEdetailTaskStatusViewController.h"
#import "MedRepDefaults.h"
@interface MedRepEdetailTaskStatusViewController ()

@end

@implementation MedRepEdetailTaskStatusViewController
@synthesize contentArray,selectedIndex,savedTaskString,taskStatusTblView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    //contentArray=[[NSMutableArray alloc]initWithObjects:@"Open",@"Cancelled",@"Completed", nil];
    NSLog(@"saved task String is %@", [savedTaskString description]);
    
    
    NSLog(@"content array in task status %@", [contentArray description]);
    
    //set the previous selection for saved tasks
    
    if (savedTaskString.length>0) {
        
        for (NSInteger i=0; i<contentArray.count; i++) {
            
            if ([[contentArray objectAtIndex:i]isEqualToString:savedTaskString]) {
                
                
                NSLog(@"array contnt %@", [contentArray objectAtIndex:i]);
                
                
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];

                
                self.lastIndexPath=indexPath;
                

                
            }
            
        }
        
    }

    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return contentArray.count;;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {

    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    }
    
    
    
   
    
    cell.textLabel.text=[NSString stringWithFormat:@"%@", [contentArray objectAtIndex:indexPath.row]];
    
    cell.textLabel.font=MedRepTitleFont;
    
    if ([indexPath compare:self.lastIndexPath] == NSOrderedSame)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
   
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
//    UITableViewCell * selectedCell=[tableView cellForRowAtIndexPath:indexPath];
//    
//    if (selectedCell.accessoryType==UITableViewCellAccessoryCheckmark) {
//        selectedCell.accessoryType = UITableViewCellAccessoryNone;
//
//    }
    
    
    if([self.delegate respondsToSelector:@selector(selectedStatus:)])
    {
        
        
        NSMutableDictionary* selectedDataDict=[[NSMutableDictionary alloc]init];
               [selectedDataDict setObject:[contentArray objectAtIndex:indexPath.row] forKey:@"Selected_Value"];
        [selectedDataDict setObject:[NSNumber numberWithInteger:selectedIndex] forKey:@"Selected_Index"];
        
        [self.delegate selectedStatus:selectedDataDict];

        self.lastIndexPath = indexPath;
        
        [tableView reloadData];
        
        [self.navigationController popViewControllerAnimated:YES];
        
        
    }
    
}

@end
