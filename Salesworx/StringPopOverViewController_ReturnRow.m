//
//  CustomPopOverViewController.m
//  MathMonsters
//
//  Created by Transferred on 1/12/13.
//  Copyright (c) 2013 Designated Nerd Software. All rights reserved.
//

#import "StringPopOverViewController_ReturnRow.h"
#import "filterPopupPushVC.h"


@implementation StringPopOverViewController_ReturnRow


#pragma mark - Init

-(id)initwithStyle:(UITableViewStyle)style andContentSize:(CGSize)popoverContentSize
{
    if ([super initWithStyle:style] != nil) {
        
        _colorNames = [NSMutableArray array];
        _colorNamesFiltered = [[NSMutableArray alloc]init];
        
        
        self.clearsSelectionOnViewWillAppear = NO;
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        }
       
        else {
            self.preferredContentSize = popoverContentSize;
        }
        
        searching = NO;
    }
    
    return self;
}

-(id)initWithStyle:(UITableViewStyle)style
{
    if ([super initWithStyle:style] != nil) {
        
        _colorNames = [NSMutableArray array];
        _colorNamesFiltered = [[NSMutableArray alloc]init];
        
        
        self.clearsSelectionOnViewWillAppear = NO;
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        }
        
        else if (self.isProductsUOM==YES)
        {
            self.preferredContentSize = CGSizeMake(200  , 200);

        }
        else {
            
            
            self.preferredContentSize = CGSizeMake(200  , 344);
        }
        
        searching = NO;
    }
    
    return self;
}

-(id)initWithStyle:(UITableViewStyle)style withWidth:(int)withWidth
{
    if ([super initWithStyle:style] != nil) {
        
        
        _colorNames = [NSMutableArray array];
        _colorNamesFiltered = [[NSMutableArray alloc]init];
        
        
        self.clearsSelectionOnViewWillAppear = NO;
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        } else {
            self.preferredContentSize = CGSizeMake(withWidth  , 344);
        }
        
        searching = NO;
    }
    
    return self;
}

-(id)initWithStyle:(UITableViewStyle)style withWidthForFilter:(int)withWidth
{
    if ([super initWithStyle:style] != nil) {
        
        _colorNames = [NSMutableArray array];
        _colorNamesFiltered = [[NSMutableArray alloc]init];
        
        
        self.clearsSelectionOnViewWillAppear = NO;
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        } else {
            self.preferredContentSize = CGSizeMake(withWidth  , 400);
        }

        searching = NO;
        forFilter = YES;
    }
    
    return self;
}



- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [searchBar resignFirstResponder];
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar

{
    [searchBar resignFirstResponder];
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    searchBar.delegate =self;
    
    
    if ([_colorNames count]>1 ) {
        self.tableView.tableHeaderView = searchBar;
    }
    if (forFilter) {
        self.tableView.tableHeaderView = nil;
        self.navigationController.navigationBarHidden = YES;
    }
    self.tableView.backgroundColor = [UIColor clearColor];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0) {
        searching = NO;
        [self.tableView reloadData];
    }else{
        searching = YES;
        
        [_colorNamesFiltered removeAllObjects];
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF contains[c] %@",
                                  searchText];
        _colorNamesFiltered = [[_colorNames filteredArrayUsingPredicate:predicate]mutableCopy];
        [self.tableView reloadData];
    }
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar1{
    
    NSString* newText = searchBar1.text;
    if (newText.length >0) {
        
    }else{
        searching = NO;
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count;
    if (searching) {
        count = [_colorNamesFiltered count];
    }else {
        count =[_colorNames count];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        

        
        cell.textLabel.font=kSWX_FONT_SEMI_BOLD(14);
        cell.textLabel.textColor=MedRepElementDescriptionLabelColor;


               // cell.textLabel.textColor = [UIColor redColor];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    if (searching) {
        cell.textLabel.text = [_colorNamesFiltered objectAtIndex:indexPath.row];
    }else {
        cell.textLabel.text = [_colorNames objectAtIndex:indexPath.row];
    }

    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    NSString *selectedColorName;
   
    if (searching) {
        
        selectedColorName = [_colorNamesFiltered objectAtIndex:indexPath.row];
        
        NSUInteger fooIndex = [_colorNames indexOfObject: selectedColorName];

        NSIndexPath * newPath = [NSIndexPath indexPathForRow:fooIndex inSection:0];
        if (_delegate != nil) {
            [_delegate selectedStringValue:newPath];
        }
        
    }else {
        if (_delegate != nil) {
            if (forFilter) {
                
                if (indexPath.row == 0) {
                    selectedParentIndex = 0;
                    [_delegate selectedIndex:0 andChildIndex:0];

                }else{
                    selectedParentIndex = indexPath.row;
                    NSMutableArray * childArray = [self.childArray objectAtIndex:selectedParentIndex-1];
    
                    
                    filterPopupPushVC *object = [[filterPopupPushVC alloc]initWithNibName:@"filterPopupPushVC" bundle:nil];
                    object.delegate = self;
                    object.selectedPopover = self.selectedPopover;
                    object.tableArrayFrom = childArray;
                    
                    [self.navigationController pushViewController:object animated:YES];
                    NSIndexPath*    selection = [self.tableView indexPathForSelectedRow];
                    if (selection) {
                        [self.tableView deselectRowAtIndexPath:selection animated:YES];
                    }
                }

            }else{
                [_delegate selectedStringValue:indexPath];
            }
        }
    }
}

-(void)selectedChild:(NSInteger)childIndex{
    if (_delegate != nil) {
        [_delegate selectedIndex:selectedParentIndex andChildIndex:childIndex];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
