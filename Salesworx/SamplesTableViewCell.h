//
//  SamplesTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SamplesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *expiryLbl;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *lotNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *itemTypeLbl;
@end
