//
//  MedRepEdetailingTaskInformationViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/22/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepEdetailingTaskInformationViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@interface MedRepEdetailingTaskInformationViewController ()

@end

@implementation MedRepEdetailingTaskInformationViewController

@synthesize isVisitCompleted;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Task Description", nil)];

    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title=@"";
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    
    
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal]
    ;
    if (isVisitCompleted==YES) {
        self.customTextView.userInteractionEnabled=NO;
    }
    else
    {
        self.customTextView.userInteractionEnabled=YES;
        self.navigationItem .rightBarButtonItem=saveBtn;
    }

}

-(void)saveTapped

{
    
    if ([self.taskInformationDelegate respondsToSelector:@selector(taskDetailsInformation:)]) {
        
        [self.taskInformationDelegate taskDetailsInformation:self.customTextView.text];
        
        [self.navigationController popViewControllerAnimated:YES];

    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self saveTapped]
    ;

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    
    if(textView)
    {
        return (newString.length<textViewMaximumSize);
    }
    
    
    return YES;

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
