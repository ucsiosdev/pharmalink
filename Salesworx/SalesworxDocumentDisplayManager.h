//
//  SalesworxDocumentDisplayManager.h
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 11/15/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesworxDocumentDisplayManager : UIViewController<UIDocumentInteractionControllerDelegate>
{
    UIDocumentInteractionController *docIntController ;
}
- (void)ShowUserActivityShareViewInView:(UIView *)SourceView SourceRect:(CGRect)SourceFrame AndFilePath:(NSString *)filePath;
@end
