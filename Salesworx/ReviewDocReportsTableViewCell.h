//
//  ReviewDocReportsTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 1/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewDocReportsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *itemCodeLbl;
@property (strong, nonatomic) IBOutlet UILabel *quantityLbl;
@property (strong, nonatomic) IBOutlet UILabel *unitPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalLbl;

@property (strong, nonatomic) IBOutlet UILabel *itemDescLbl;
@end
