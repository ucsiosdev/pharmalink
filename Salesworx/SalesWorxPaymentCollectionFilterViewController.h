//
//  SalesWorxPaymentCollectionFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/1/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepButton.h"
#import "MedRepTextField.h"

//@protocol PaymentCollectionFilterDelegate <NSObject>
//-(void)didFinishFilter:(NSMutableArray*)filteredArray;
//-(void)filteredPaymentCollectionParameters:(NSMutableDictionary*)parameters;
//-(void)didCancelFilter:(BOOL)selection;
//@end


@protocol PaymentCollectionFilterDelegate <NSObject>

-(void)filteredProducts:(NSMutableArray*)filteredArray;
-(void)productsFilterDidClose;
-(void)productsFilterdidReset;
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict;

@end


@interface SalesWorxPaymentCollectionFilterViewController : UIViewController<UITextFieldDelegate>

{
    id paymentFilterDelegate;
    
    NSString* titleString;
    
    NSMutableDictionary* filterParametersDictionary;
    
}
- (IBAction)resetButtonTapped:(id)sender;

@property(nonatomic) id <PaymentCollectionFilterDelegate> paymentFilterDelegate;

@property(strong,nonatomic) NSString* filterTitle;

@property (strong, nonatomic) IBOutlet MedRepTextField *customerNameFilter;
@property (strong, nonatomic) IBOutlet MedRepTextField *tradeChannelFilter;

@property (strong, nonatomic) IBOutlet MedRepButton *customerNumberButton;
- (IBAction)customerNameButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepButton *addressButton;
- (IBAction)addressButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepButton *tradeChannelButton;
- (IBAction)tradeChannelButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *cashCustomerSwitch;
- (IBAction)cashCustomerSwitch:(UISegmentedControl *)sender ;

- (IBAction)cashCustomerSwitchTapped:(id)sender;

@property(strong,nonatomic) UIPopoverController * filterPopOverController;

@property(strong,nonatomic) NSMutableArray* customerListArray;

@property(strong,nonatomic)UINavigationController * filterNavController;
@property(strong,nonatomic)NSMutableDictionary * previousFilterParametersDict;

@end
