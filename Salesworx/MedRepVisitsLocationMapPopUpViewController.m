//
//  MedRepVisitsLocationMapPopUpViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepVisitsLocationMapPopUpViewController.h"

@interface MedRepVisitsLocationMapPopUpViewController ()

@end

@implementation MedRepVisitsLocationMapPopUpViewController
@synthesize visitLocationMapView,VisitDetailsDictionary,isBrandAmbassadorVisit,currentBAVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)viewWillAppear:(BOOL)animated
{
    if(self.isMovingToParentViewController)
    {
        if (isBrandAmbassadorVisit==YES) {
            coord=CLLocationCoordinate2DMake([currentBAVisit.selectedLocation.Latitude doubleValue], [currentBAVisit.selectedLocation.Longitude doubleValue]);
            [self setLocationMapViewWithCoordinates:coord];
        }
        else
        {
            NSString* locationID=[VisitDetailsDictionary valueForKey:@"Location_ID"];
            NSMutableArray* coordsArray=[MedRepQueries fetchCoordinatesforLocationID:locationID];
            coord=CLLocationCoordinate2DMake([[coordsArray valueForKey:@"Latitude"] doubleValue], [[coordsArray valueForKey:@"Longitude"] doubleValue]);
            [self setLocationMapViewWithCoordinates:coord];
        }
    }
    
}

-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:KAlertOkButtonTitle
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}


#pragma mark MapViewMethods

-(void)setLocationMapViewWithCoordinates:(CLLocationCoordinate2D)coordinates
{
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (coordinates, 800, 800);
    [visitLocationMapView setRegion:regionCustom animated:NO];
    [visitLocationMapView removeAnnotations:[visitLocationMapView annotations]];
    // Create Annotation point
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = coordinates;
    [visitLocationMapView addAnnotation:Pin];
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    if (canHandle) {
        // Google maps installed
        
        NSString* url = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,coord.latitude,coord.longitude];
        NSURL *googleMapsUrl=[NSURL URLWithString:url];
        [[UIApplication sharedApplication] openURL:googleMapsUrl];
    }
    else
    {
        NSString* url = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,coord.latitude,coord.longitude];
        NSURL *googleMapsUrl=[NSURL URLWithString:url];

        [[UIApplication sharedApplication] openURL:googleMapsUrl];
    }
}

#pragma mark buttonActions
- (IBAction)doneButtontapped:(id)sender {
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

@end
