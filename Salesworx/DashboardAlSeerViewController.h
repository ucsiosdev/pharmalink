//
//  DashboardAlSeerViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIMBarGraph.h"
#import "MIMColor.h"
#import "UUChart.h"
#import "AlSeerWelcomeViewController.h"


@interface DashboardAlSeerViewController : UIViewController<BarGraphDelegate,UITabBarControllerDelegate,UITableViewDataSource,UITableViewDelegate>

{
    NSDictionary *barProperty;
    NSMutableArray * arrayForFirstMonth;
    NSMutableArray * arrayForSecondMonth;
    NSMutableArray * arrayForThirdMonth;
    NSArray *yValuesArray,*xValuesArray;
    NSArray *xTitlesArray;
    NSMutableArray* dsgArr;    
    NSString *dateString;
    
    AlSeerWelcomeViewController* welcomeVC;

}
@property (strong, nonatomic) IBOutlet UITableView *dashTblView;

@property(strong,nonatomic)NSMutableArray* dashBoardtblArray,*agencyArray,*targetArray;

@property(strong,nonatomic)UUChart* chartView;


//@property (strong, nonatomic) IBOutlet MIMBarGraph *myBarChartLarge;
@property (strong, nonatomic) IBOutlet MIMBarGraph *myBarChart;
@property (strong, nonatomic) IBOutlet UIImageView *targetAndAcheivmentImageView;
@property (strong, nonatomic) IBOutlet UIView *dummyView;
@property (strong, nonatomic) IBOutlet UILabel *syncStatusLbl;
@property (strong, nonatomic) IBOutlet UILabel *lastSyncDateLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalTargetLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalInvoicesLbl;
@property (strong, nonatomic) IBOutlet UILabel *previousDaySaleLbl;
@property (strong, nonatomic) IBOutlet UILabel *balancetoGoLbl;
@property (strong, nonatomic) IBOutlet UILabel *dailysalestoGoLbl;
@property (strong, nonatomic) IBOutlet UILabel *achLbl;

@property (strong, nonatomic) IBOutlet UILabel *achievementPercentLbl;

@property(strong,nonatomic)NSMutableArray* salesTargetItemsArray;



//top level lables
@property (strong, nonatomic) IBOutlet UILabel *plannedLbl;
@property (strong, nonatomic) IBOutlet UILabel *completedLbl;
@property (strong, nonatomic) IBOutlet UILabel *outofRouteLbl;
@property (strong, nonatomic) IBOutlet UILabel *adherenceLbl;


@property (strong, nonatomic) IBOutlet UILabel *orderCount;
@property (strong, nonatomic) IBOutlet UILabel *returnCount;
@property (strong, nonatomic) IBOutlet UILabel *dailyTargetValue;
@property (strong, nonatomic) IBOutlet UILabel *targetAchievedLbl;


@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;














@end
