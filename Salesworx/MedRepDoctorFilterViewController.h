//
//  MedRepDoctorFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/3/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepTextField.h"
#import "MedRepCustomClass.h"
#import "MedRepElementTitleLabel.h"
@protocol FilteredDoctorDelegate <NSObject>



-(void)filteredDoctorDelegate:(NSMutableArray*)filteredArray;

-(void)filteredParameters:(NSMutableDictionary*)filterParameters;

-(void)CustomPopOverCancelDelegate;

@end


@interface MedRepDoctorFilterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SelectedFilterDelegate,UIPopoverControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate>
{
    NSString * selectedDataString;
    IBOutlet UILabel *specilizationLbl;
    
    IBOutlet UILabel *tradeChannelLbl;
    IBOutlet UILabel *classLbl;
    
    IBOutlet UISegmentedControl *specificationSegment;
    IBOutlet UISegmentedControl *statusSegment;
    NSMutableDictionary* filterDict;
    
    IBOutlet MedRepElementTitleLabel *classtitleLbl;
    
    
    
    
    
    
    
    id <FilteredDoctorDelegate>filteredDoctorDelegate;
    
    
    
    IBOutlet MedRepTextField *classTextfield;
    IBOutlet MedRepTextField *tradeChannelTextfield;
    IBOutlet MedRepTextField *specializationTextfield;
    
    
    IBOutlet MedRepTextField *accountNameTextField;

    
    IBOutlet MedRepTextField *locationTextField;
    
    IBOutlet MedRepTextField *clinicTextField;

    IBOutlet MedRepTextField *territoryTextField;
    
    IBOutlet UIScrollView *doctorFilterScrollView;
}

@property(strong,nonatomic)     IBOutlet UIButton *classButton;

@property(strong,nonatomic) IBOutlet UIButton *tradeChannelButton;

@property(strong,nonatomic) IBOutlet UIButton *specilizationButton;

- (IBAction)statusSwitchTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *statusSwitch;
- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)specificationSegmentTapped:(id)sender;
@property(strong,nonatomic) id filteredDoctorDelegate;
@property(strong,nonatomic) IBOutlet UITableView* filterTblView;
@property(strong,nonatomic) NSMutableArray* filterArray;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;
@property(strong,nonatomic)UINavigationController * filterNavController;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) NSMutableArray * contentArray;
@property(strong,nonatomic) NSMutableDictionary* previouslyFilteredContent;


@property (strong, nonatomic) IBOutlet UITextField *classTxtFld;
- (IBAction)classButtonTapped:(id)sender;
- (IBAction)tradeChannelButtonTapped:(id)sender;

- (IBAction)specilizationButtonTapped:(id)sender;
@property(strong,nonatomic) UIBlurEffect* blurEffect;
- (IBAction)accountNameButtonTapped:(id)sender;
- (IBAction)locationButtonTapped:(id)sender;

- (IBAction)clinicButtonTapped:(id)sender;
- (IBAction)territoryButtonTapped:(id)sender;

@end
