//
//  MedRepTextField.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepTextField.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@implementation MedRepTextField
@synthesize dropdownImageView,DropDownImage,readOnlyLayer,EnableTextTransLation,bottonBorder;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    
    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];
//    self.leftView = paddingView;
//    paddingView.userInteractionEnabled=NO;
//    self.leftViewMode = UITextFieldViewModeAlways;
    
    
    if (bottonBorder) {
        [self addBottomBorder];
    }
    else{
        
        self.layer.borderWidth=1.0f;
        self.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius=5.0;

    }
    
    self.autocorrectionType=UITextAutocorrectionTypeNo;

    
    self. font=kSWX_FONT_SEMI_BOLD(15);

    self.textColor=MedRepElementDescriptionLabelColor;

    if (self.isMandatory==YES) {
        UILabel *mandatoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
        mandatoryLabel.textColor=[UIColor redColor];
        mandatoryLabel.font=MedRepElementDescriptionLabelFont;
        mandatoryLabel.text=@"*";
        self.rightView = mandatoryLabel;
        self.rightViewMode = UITextFieldViewModeAlways;
    }

    UIView *leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, self.frame.size.height)];
    if (_isCollectionViewTextField) {
        leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    }
    self.leftView = leftPaddingView;
    leftPaddingView.userInteractionEnabled=NO;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    if(self.textAlignment==NSTextAlignmentRight)
    {
        UIView *rightPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, self.frame.size.height)];/* 26 is padding +imageView Width*/
        self.rightView = rightPaddingView;
        rightPaddingView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    self.textAlignment=NSTextAlignmentNatural;
    
//    if(self.EnableRTLTranslation)
//    {
//        if(isTextEndingWithExtraSpecialCharacter && super.text.length>2)
//        {
//            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[super.text characterAtIndex:super.text.length-1]];
//            NSString *nString=[super.text stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
//            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
//            
//        }
//        else
//        {
//            super.text=NSLocalizedString(super.text, nil);
//            
//        }
//    }
    
//    if(self.EnableRTLTextAlignment)
//    {
//        super.textAlignment=NSTextAlignmentRight;
//    }
//    else
//    {
//        super.textAlignment=NSTextAlignmentLeft;
//    }
    
    
    
}

-(void)setisMandatory:(BOOL)status
{
    
}



-(void)setDropDownImage:(NSString *)ImageName
{
    if(ImageName.length>0)
    {
        UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 28, 16)];/* 26 is padding +imageView Width*/
        dropdownImageView=[[UIImageView alloc]initWithFrame:CGRectMake(6,0,16,16)];
        [dropdownImageView setImage:[UIImage imageNamed:ImageName/*@"Arrow_DropDown"*/]];
        [dropDownView addSubview:dropdownImageView];
        self.rightView = dropDownView;
        dropDownView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else
    {
      
    }
   
}
-(void)setIsReadOnly:(BOOL)status
{
    if(status==YES)
    {
        [self setEnabled:NO];
        self.textColor=SalesWorxReadOnlyTextFieldTextColor;
        self.backgroundColor=SalesWorxReadOnlyTextFieldColor;
    }
    else
    {
        self.textColor=MedRepElementDescriptionLabelColor;
       
            self.backgroundColor=[UIColor whiteColor];
        
        [self setEnabled:YES];
    }
}
-(void)showTextfieldAsLabel
{
    UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];/* 26 is padding +imageView Width*/
    self.rightView = dropDownView;
    self.rightViewMode = UITextFieldViewModeAlways;
    self.leftView = dropDownView;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.layer.borderColor=(__bridge CGColorRef _Nullable)([UIColor clearColor]);
    self.textAlignment=NSTextAlignmentLeft;
}
-(void)showTextfieldAsBox
{
    self. font=kSWX_FONT_SEMI_BOLD(15);

    self.textColor=MedRepElementDescriptionLabelColor;
    
    self.layer.borderWidth=1.0f;
    self.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    self.layer.masksToBounds = NO;
    
    
    self.layer.cornerRadius=5.0;
    
    if (self.isMandatory==YES) {
        UILabel *mandatoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
        mandatoryLabel.textColor=[UIColor redColor];
        mandatoryLabel.font=MedRepElementDescriptionLabelFont;
        mandatoryLabel.text=@"*";
        self.rightView = mandatoryLabel;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    
    UIView *leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];
    self.leftView = leftPaddingView;
    leftPaddingView.userInteractionEnabled=NO;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    if(self.textAlignment==NSTextAlignmentRight)
    {
        UIView *rightPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];/* 26 is padding +imageView Width*/
        self.rightView = rightPaddingView;
        rightPaddingView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    
    if(DropDownImage.length>0)
    {
        UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 16)];/* 26 is padding +imageView Width*/
        dropdownImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,16,16)];
        [dropdownImageView setImage:[UIImage imageNamed:DropDownImage/*@"Arrow_DropDown"*/]];
        [dropDownView addSubview:dropdownImageView];
        self.rightView = dropDownView;
        dropDownView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }

}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (self.enableCopyPaste) {
        if (action == @selector(copy:) || action == @selector(paste:) || action == @selector(select:) || action == @selector(selectAll:)||action == @selector(cut:))
            return YES;
    }
    return NO;
}
//-(NSString*)text{
//    return super.text;
//}
//
-(void)setText:(NSString*)newText {
    
    if(self.EnableTextTransLation)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
}


-(void)setPlaceHolder:(NSString *)placeHolderText
{
    NSTextAlignment alignment = NSTextAlignmentLeft;
    NSMutableParagraphStyle* alignmentSetting = [[NSMutableParagraphStyle alloc] init];
    alignmentSetting.alignment = alignment;
    NSDictionary *attributes = [[NSDictionary alloc]initWithObjectsAndKeys:kSWX_FONT_REGULAR(8),NSFontAttributeName,alignmentSetting,NSParagraphStyleAttributeName, nil];
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:placeHolderText attributes: attributes];
    //self.attributedPlaceholder = str;

    self.leftView=[[UIView alloc]initWithFrame:CGRectZero];
    self.rightView=[[UIView alloc]initWithFrame:CGRectZero];

    
    

    
}
- (void)addBottomBorder {
    
    
   
    self.textAlignment=NSTextAlignmentRight;

    
   // self.backgroundColor = [UIColor colorWithRed:(248.0/255.0) green:(249.0/255.0) blue:(251.0/255.0) alpha:1];
    self.backgroundColor = [UIColor clearColor];
    self.separatorView = [UIView new];
    self.separatorView.backgroundColor = UITextFieldDarkBorderColor;
    [self addSubview:self.separatorView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat height = CGRectGetHeight(self.frame);
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat onePixelHeight = 1.0f / [[UIScreen mainScreen] scale];
    
    self.separatorView.frame = CGRectMake(0, height - 1, width, onePixelHeight);
}
@end
