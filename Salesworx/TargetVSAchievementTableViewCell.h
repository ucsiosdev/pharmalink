//
//  TargetVSAchievementTableViewCell.h
//  MedRep
//
//  Created by Nethra on 9/12/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"
NS_ASSUME_NONNULL_BEGIN

@interface TargetVSAchievementTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblProduct;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblTarget;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblSalesValue;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPercentageArch;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPercentage;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblIdeal;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblActual;

@property(nonatomic) BOOL isHeader;
@end

NS_ASSUME_NONNULL_END
