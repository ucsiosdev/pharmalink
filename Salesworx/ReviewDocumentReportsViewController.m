//
//  ReviewDocumentReportsViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 1/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "ReviewDocumentReportsViewController.h"
#import "SWDatabaseManager.h"
#import "ReviewDocReportsTableViewCell.h"
#import "ReviewDocumentsHeaderTableViewCell.h"
#import "MedRepDefaults.h"
@interface ReviewDocumentReportsViewController ()

@end

@implementation ReviewDocumentReportsViewController
@synthesize orderDateLbl,orderValueLbl,orderStatusLbl,invoiceDateLbl,invoiceValueLbl,fsrLbl,CustomerNameLbl,ordersArray,grandTotal,quantity,unitPrice,itemCode,itemDesc,appendedStr,appendedUnitPrice,orderNumberLbl,dataDictionary,reportsTableView,totalContentDataArray,selectedIndex,customSegment,prevOrderButton,nextOrderButton,orderStatusStaticLbl,invDateStaticLbl,invvalueStaticLbl,fsrStaticLbl,orderDateStaticLbl,orderValueStatic;
- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Report Description", nil)];
    
    
    
    
    NSLog(@"review document reports called");
    
    dataDictionary=[[NSMutableDictionary alloc]init];
    
    invoicedProductDataArray=[[NSMutableArray alloc]init];
    
    documentNumber=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex] ;

    if ([self.reportType isEqualToString:@"Order"]) {
        [self fetchOrdersData];

    }
    
    else if ([self.reportType isEqualToString:@"Return"])
        
    {
        CustomerNameLbl.text =[[ordersArray valueForKey:@"Customer_Name"] objectAtIndex:selectedIndex];
        orderDateLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:[[ordersArray valueForKey:@"DocDate"] objectAtIndex:selectedIndex]];
        

        orderNumberLbl.text=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex];
        orderValueStatic.text=@"Return Value";
        orderDateStaticLbl.text=@"Return Date";
        
    
        
    [self fetchDataforReturns];
    }
    
    

    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated
{
    //setting button background images
    
    [self loadButtonImages];
    
    [self hideControlsforReturn];
    
    
}


-(void)hideControlsforReturn

{
    if ([self.reportType isEqualToString:@"Return"]) {
        
        [customSegment setHidden:YES];
        [orderStatusLbl setHidden:YES];
        [invoiceDateLbl setHidden:YES];
        [invoiceValueLbl setHidden:YES];
        [invvalueStaticLbl setHidden:YES];
        [invDateStaticLbl setHidden:YES];
        [fsrStaticLbl setHidden:YES];
        [orderStatusStaticLbl setHidden:YES];

        
        [prevOrderButton setHidden:YES];
        [nextOrderButton setHidden:YES];
        
        [fsrLbl setHidden:YES];
        
        
        
    }

}


-(void)loadButtonImages
{
    
    [self.nextOrderButton setBackgroundImage:[UIImage imageNamed:@"Old_DownArrow"] forState:UIControlStateNormal];
    [self.prevOrderButton setBackgroundImage:[UIImage imageNamed:@"Old_UpArrow"] forState:UIControlStateNormal];

}

-(void)fetchOrdersData
{
    NSLog(@"selected index is %d", self.selectedIndex);
    
    
    CustomerNameLbl.text =[[ordersArray valueForKey:@"Customer_Name"] objectAtIndex:selectedIndex];
    
    orderNumberLbl.text=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex];
    orderDateLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:[[ordersArray valueForKey:@"DocDate"] objectAtIndex:selectedIndex]];
    
    
    //for returns
    
    
    NSMutableArray* inverntoryReturnIdArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Inventory_Item_ID from Tbl_RMA_Line_Items where Orig_Sys_Document_Ref='%@' ",documentNumber]];
    
    
    if (inverntoryReturnIdArray.count>0) {
        
        
        
    }
    
    
    
    //get itemCode from doc number
    
    documentNumber=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex] ;
    
    
    NSMutableArray* inverntoryIdArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Inventory_Item_ID from Tbl_order_History_Line_items where Orig_Sys_Document_Ref='%@' ",documentNumber]];
    
    inVoicesInventoryArray=inverntoryIdArray;
    
    //get invoiced total here to diaplay on invoiced value
    
  NSMutableArray*  invoicedTotalValueArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Unit_Selling_Price, Ordered_Quantity,Item_Value from Tbl_Sales_history_Line_Items where Orig_Sys_Document_Ref='%@'",documentNumber]];
    

    
    
    
    float tempInvoiceTotal=0.0;
    
    
    if (invoicedTotalValueArray.count>0) {
        
    
    for (NSInteger i=0; i<invoicedTotalValueArray.count; i++) {
        
        float usp=[[[invoicedTotalValueArray valueForKey:@"Unit_Selling_Price"] objectAtIndex:i] floatValue];
        float qty=[[[invoicedTotalValueArray valueForKey:@"Ordered_Quantity"] objectAtIndex:i]floatValue];
        
        float totalInvVal=usp*qty;
        
        tempInvoiceTotal=tempInvoiceTotal+totalInvVal;
        
    }
    
    }
    NSLog(@"invoice total is %f", tempInvoiceTotal);
    
    
    
    
    
    
    if (inverntoryIdArray.count>0) {
        
        self.inventoryID=[[inverntoryIdArray valueForKey:@"Inventory_Item_ID"] objectAtIndex:0];
        
        
        // NSString* inventoryDataQuery=
        
        //        NSMutableArray* inverntoryDataArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Item_code,Description  FROM TBL_Product  where Inventory_item_ID ='%@']",inventoryID]];
        
        dataArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Unit_Selling_Price, Ordered_Quantity ,Item_Value from Tbl_Order_History_line_items where Orig_Sys_Document_Ref='%@'",documentNumber]];
        
        
        
        
        
        
        float tempTotal=0.0;
        
        for (NSInteger i=0; i<dataArray.count; i++) {
            
            float usp=[[[dataArray valueForKey:@"Item_Value"] objectAtIndex:i] floatValue];
           // float qty=[[[dataArray valueForKey:@"Ordered_Quantity"] objectAtIndex:i]floatValue];
            
            
            tempTotal=tempTotal+usp;
            
        }
        
        
        NSLog(@"orders total is %f", tempTotal);
        
        
        
        
        productDataArray=[[NSMutableArray alloc]init];
        
        
        if (inverntoryIdArray.count>0) {
            
       
        
        for (NSInteger j=0; j<inverntoryIdArray.count; j++) {
            
            invIDforQuery=[[inverntoryIdArray valueForKey:@"Inventory_Item_ID"] objectAtIndex:j];
            
            NSString *itemCodeQuery=[NSString stringWithFormat:@"SELECT Item_code,Description  FROM TBL_Product  where Inventory_item_ID ='%@'",invIDforQuery];
            
            
            NSArray* itemCodeArr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:itemCodeQuery];
            
            
            if (itemCodeArr.count>0) {
                [productDataArray addObject:[itemCodeArr objectAtIndex:0]];

            }
            
            
            //[dataArray setValue:[[itemCodeArr valueForKey:@"Item_Code"] objectAtIndex:0] forKey:@"Item_Code"];
            //[dataArray setValue:[[descArr valueForKey:@"Description"] objectAtIndex:j] forKey:@"Desc"];
            
            // [dataArray insertObject:[NSDictionary dictionaryWithObject:[itemCodeArr valueForKey:@"Item_Code"] forKey:@"Item_Code"] atIndex:1];
            
            
            //            [dataArray setValue:[[SWDatabaseManager retrieveManager]fetchDataForQuery:itemCodeQuery] forKey:@"Item_Code"];
            //            [dataArray setValue:[[SWDatabaseManager retrieveManager]fetchDataForQuery:descQuery] forKey:@"Desc"];
            
            
        }
        
        }
        
        NSLog(@"check data array after adding %@", [dataArray description]);
        NSLog(@"check product data array %@", [productDataArray description]);
        
        
        //get customer name
        
        
        // NSMutableArray *recentOrderHistory=[SWDatabaseManager retrieveManager]dbGetRecentOrders:<#(NSString *)#>
        
        
        //
        //        NSLog(@"data dic desc %@", [[inverntoryDataArray valueForKey:@"Ordered_Quantity"]objectAtIndex:0]);
        //
        //
        //        if (inverntoryDataArray.count>0) {
        //
        //            itemCode=[[inverntoryDataArray valueForKey:@"Item_Code"] objectAtIndex:0];
        //            itemDesc=[[inverntoryDataArray valueForKey:@"Description"] objectAtIndex:0];
        //        }
        //
        //get order quantity
        
        
        //        NSString* unitPriceQuery=[NSString stringWithFormat:@"SELECT Unit_Selling_Price, Ordered_Quantity from Tbl_Order_History_line_items where Orig_Sys_Document_Ref='%@'",documentNumber];
        //
        //
        //
        //        NSMutableArray* UnitPriceQuantity=[[SWDatabaseManager retrieveManager]fetchDataForQuery:unitPriceQuery];
        //
        //        if (UnitPriceQuantity.count>0) {
        //
        //            quantity=[[[UnitPriceQuantity valueForKey:@"Ordered_Quantity"] objectAtIndex:0] stringValue];
        //            unitPrice=[[[UnitPriceQuantity valueForKey:@"Unit_Selling_Price"] objectAtIndex:0] stringValue];
        //            grandTotal=   [quantity floatValue]*[unitPrice floatValue];
        //
        //        }
        
        //        dataArray=[NSMutableArray arrayWithObjects:itemCode,itemDesc,unitPrice,[NSString stringWithFormat:@"%f",grandTotal],quantity, nil];
        //        NSLog(@"data array desc is %@", [dataArray description]);
        
       // NSString *prefixStr=@"AED";
        NSString* roundedSTr=[NSString stringWithFormat:@"%f", grandTotal];
        NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

        
        appendedStr=[roundedSTr currencyString];
        
        if ([orderType isEqualToString:@"Return"]) {
            
            
            
        }
        else
        {
            orderValueLbl.text=[[NSString stringWithFormat:@"%f",tempTotal] currencyString];

        }
        
        
        appendedUnitPrice=[NSString stringWithFormat:@"%@%@",userCurrencyCode,unitPrice];
        
        orderStatusLbl.text=[[ordersArray valueForKey:@"ERP_Status"]objectAtIndex:selectedIndex];
        fsrLbl.text=[SWDefaults username];
        
        
        invoiceValueLbl.text=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,tempInvoiceTotal];
        invoiceDateLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:[[ordersArray valueForKey:@"DocDate"] objectAtIndex:selectedIndex]];
        
        
        
        
    }
    

}

-(void)fetchInvoicedItems
{
    
    
    invoicedDataArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Unit_Selling_Price, Ordered_Quantity,Item_Value from Tbl_Sales_history_Line_Items where Orig_Sys_Document_Ref='%@'",documentNumber]];
    
//     NSMutableArray* inverntoryIdArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Inventory_Item_ID from Tbl_Sales_history_Line_Items where Orig_Sys_Document_Ref='%@' ",documentNumber]];
    
    
    
    
    NSString* invoiceInventryQuery;
    
    for (NSInteger j=0; j<inVoicesInventoryArray.count; j++) {
        
        
        
        invoiceInventryQuery=[[inVoicesInventoryArray valueForKey:@"Inventory_Item_ID"] objectAtIndex:j];
        
        NSString *itemCodeQuery=[NSString stringWithFormat:@"SELECT Item_code,Description  FROM TBL_Product  where Inventory_item_ID ='%@'",invoiceInventryQuery];
        
        NSArray* itemCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:itemCodeQuery];

        [invoicedProductDataArray addObject:[itemCodeArray objectAtIndex:0]];
    
    
    }
    
    NSLog(@"invoiced data array %@", invoicedDataArray);
    NSLog(@"invoiced product array %@", invoicedProductDataArray);

    
    
    
    //populate tables
    
    
    CustomerNameLbl.text =[[ordersArray valueForKey:@"Customer_Name"] objectAtIndex:selectedIndex];
    
    orderNumberLbl.text=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex];
    orderDateLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:[[ordersArray valueForKey:@"DocDate"] objectAtIndex:selectedIndex]];
    
    
    float tempTotal=0.0;
    
    for (NSInteger i=0; i<invoicedDataArray.count; i++) {
        
        float usp=[[[invoicedDataArray valueForKey:@"Item_Value"] objectAtIndex:i] floatValue];
        //float qty=[[[invoicedDataArray valueForKey:@"Ordered_Quantity"] objectAtIndex:i]floatValue];
        
       // float totalVal=usp*qty;
        
        tempTotal=tempTotal+usp;
        
    }
    
    
    NSLog(@"orders total is %f", tempTotal);

   // NSString *prefixStr=@"AED";
    NSString* roundedSTr=[NSString stringWithFormat:@"%f", grandTotal];
    NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

    
    appendedStr=[roundedSTr currencyString];
    
    orderValueLbl.text=[[NSString stringWithFormat:@"%f",tempTotal] currencyString];
    
    appendedUnitPrice=[unitPrice currencyString];
    
    orderStatusLbl.text=[[ordersArray valueForKey:@"ERP_Status"]objectAtIndex:selectedIndex];
    
    
    
    
    NSMutableArray*  invoicedTotalValueArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Unit_Selling_Price, Ordered_Quantity from Tbl_Sales_history_Line_Items where Orig_Sys_Document_Ref='%@'",documentNumber]];
    
    
    
    
    
    float tempInvoiceTotal=0.0;
    
    for (NSInteger i=0; i<invoicedTotalValueArray.count; i++) {
        
        float usp=[[[invoicedTotalValueArray valueForKey:@"Unit_Selling_Price"] objectAtIndex:i] floatValue];
        float qty=[[[invoicedTotalValueArray valueForKey:@"Ordered_Quantity"] objectAtIndex:i]floatValue];
        
        float totalInvVal=usp*qty;
        
        tempInvoiceTotal=tempInvoiceTotal+totalInvVal;
        
    }
    
    
    NSLog(@"invoice total is %f", tempInvoiceTotal);
    

    
    
    fsrLbl.text=[SWDefaults username];
    
    invoiceValueLbl.text=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,tempInvoiceTotal];
    invoiceDateLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:[[ordersArray valueForKey:@"DocDate"] objectAtIndex:selectedIndex]];
    
}


-(void)fetchDataforReturns

{
    
    NSString * lineItemQry=[NSString stringWithFormat:@"select Unit_Selling_Price, Ordered_Quantity  from TBL_order_history_line_items where Orig_Sys_Document_Ref ='%@'", documentNumber];
    NSLog(@"lineItemQry --> %@",lineItemQry);
    
    returnLineItems=[[SWDatabaseManager retrieveManager]fetchDataForQuery:lineItemQry];
    NSLog(@" returnLineItems --> %@",returnLineItems);
    if (returnLineItems.count>0) {
        
        NSLog(@"returned line itmes are %@", [returnLineItems description]);
        
        //calculate total for returns
        
        totalReturns=0.0;
        
        double tempReturns=0.0;
        
        
        for (NSInteger i=0; i<returnLineItems.count; i++) {
            
            double usp=[[[returnLineItems valueForKey:@"Unit_Selling_Price"] objectAtIndex:i] doubleValue];
            
            double qty=[[[returnLineItems valueForKey:@"Ordered_Quantity"] objectAtIndex:i] doubleValue];
            
            tempReturns=usp*qty;
            
            
            totalReturns=totalReturns+tempReturns;
            
            
        }
        
    

        orderValueLbl.text=[[NSString stringWithFormat:@"%f",totalReturns] currencyString];

        
        
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount=0.0;
    
    if ([orderType isEqualToString:@"Invoice"]) {
        rowCount=invoicedDataArray.count;
    }
    else if ([orderType isEqualToString:@"Return"]|| [self.reportType isEqualToString:@"Return"])
        
    {
        rowCount=returnLineItems.count;
    }
    
    else
    {
        rowCount=dataArray.count;
    }
    return rowCount;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"reportCell";


    ReviewDocReportsTableViewCell* cell=(ReviewDocReportsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell==nil) {
        
        NSArray* topLvel=[[NSBundle mainBundle]loadNibNamed:@"ReviewDocReportsTableViewCell" owner:self options:nil];
        cell=[topLvel objectAtIndex:0];
    }
    
//    
//    ReviewDocReportsTableViewCell *cell = (ReviewDocReportsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//    
//    if (cell == nil) {
//        cell = [[ReviewDocReportsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
//    }
    
    float roundedTotal = 0.0;
    
    if ([orderType isEqualToString:@"Invoice"]) {
        
        
        cell.itemCodeLbl.text=[[invoicedProductDataArray valueForKey:@"Item_Code"] objectAtIndex:indexPath.row];
        cell.itemDescLbl.text=[[invoicedProductDataArray valueForKey:@"Description"] objectAtIndex:indexPath.row];
        cell.unitPriceLbl.text=[[[[invoicedDataArray valueForKey:@"Unit_Selling_Price"] objectAtIndex:indexPath.row] stringValue] currencyString];
        cell.quantityLbl.text=[[[invoicedDataArray valueForKey:@"Ordered_Quantity"] objectAtIndex:indexPath.row] stringValue];
        finalTot=[cell.unitPriceLbl.text floatValue]*[cell.quantityLbl.text floatValue];
        roundedTotal= [[[invoicedDataArray valueForKey:@"Item_Value"] objectAtIndex:indexPath.row] floatValue];

       // NSString *prefixStr=@"AED";

        
        appendedStr=[NSString stringWithFormat:@"%f",roundedTotal];
        cell.totalLbl.text=[appendedStr currencyString];
    }
    
    else if ([orderType isEqualToString:@"Ordered Items"]||[self.reportType isEqualToString:@"Order"])
    {
        
        cell.itemCodeLbl.text=[[productDataArray valueForKey:@"Item_Code"] objectAtIndex:indexPath.row];
        cell.itemDescLbl.text=[[productDataArray valueForKey:@"Description"] objectAtIndex:indexPath.row];
        cell.unitPriceLbl.text=[[[[dataArray valueForKey:@"Unit_Selling_Price"] objectAtIndex:indexPath.row] stringValue] currencyString];
        cell.quantityLbl.text=[[[dataArray valueForKey:@"Ordered_Quantity"] objectAtIndex:indexPath.row] stringValue];
        finalTot=[cell.unitPriceLbl.text floatValue]*[cell.quantityLbl.text floatValue];
        
        roundedTotal= [[[dataArray valueForKey:@"Item_Value"] objectAtIndex:indexPath.row] floatValue];

       // NSString *prefixStr=@"AED";
        
        
        appendedStr=[NSString stringWithFormat:@"%f",roundedTotal];
        cell.totalLbl.text=[appendedStr currencyString];
    }
    else if ([orderType isEqualToString:@"Return"]|| [self.reportType isEqualToString:@"Return"])
    {
        //this is for returns
   
   
        cell.unitPriceLbl.text=[[[[returnLineItems valueForKey:@"Unit_Selling_Price"] objectAtIndex:indexPath.row] stringValue] currencyString];
        cell.quantityLbl.text=[[[returnLineItems valueForKey:@"Ordered_Quantity"] objectAtIndex:indexPath.row] stringValue];
        
        
        double unitPriceVal=[[[returnLineItems valueForKey:@"Unit_Selling_Price"] objectAtIndex:indexPath.row] doubleValue];
        
        double QtyVal=[[[returnLineItems valueForKey:@"Ordered_Quantity"] objectAtIndex:indexPath.row] doubleValue];

        double totalReturn=unitPriceVal*QtyVal;
        
        

        cell.totalLbl.text=[[NSString stringWithFormat:@"%f",totalReturn] currencyString];
        
        
        //fetchItemCode and description from tbl_Product
        
        NSString* inventoryIDQuery=[NSString stringWithFormat:@"Select Inventory_Item_ID from TBL_order_history_line_items where Orig_Sys_Document_Ref ='%@'", documentNumber];

        NSLog(@"inventoryIDQuery-->%@",inventoryIDQuery);
        if (inventoryIDQuery!=nil) {
            
            
            NSMutableArray* InvIDArr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:inventoryIDQuery];
            NSLog(@"InvIDArr-->%@",InvIDArr);
            if (InvIDArr.count>0) {
                
                NSString* invIDforReports=[[InvIDArr valueForKey:@"Inventory_Item_ID"] objectAtIndex:indexPath.row];
                
                NSString* inventoryIDProductQry=[NSString stringWithFormat:@"Select Description, Item_Code from TBL_Product where Inventory_Item_ID='%@'", invIDforReports];
                
                NSMutableArray* IVVDescArr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:inventoryIDProductQry];
                
                NSLog(@"final details %@", [IVVDescArr description]);
                if (IVVDescArr.count>0) 
                    {
                        cell.itemCodeLbl.text=[[IVVDescArr valueForKey:@"Item_Code"] objectAtIndex:0];
                        cell.itemDescLbl.text=[[IVVDescArr valueForKey:@"Description"] objectAtIndex:0];
                    }
                    else
                     {
                        cell.itemCodeLbl.text=@"";
                        cell.itemDescLbl.text=@"";

                    }
                
                
            }
            
            
        }
        
        
        
        
        //fetch total from Order_Amt of TBL_RMA
        
//        NSString* orderAmtQry=[NSString stringWithFormat:@"Select Order_Amt from TBL_RMA where Orig_Sys_Document_Ref ='%@'", documentNumber];
//        
//        if (orderAmtQry!=nil) {
//            
//            
//            NSMutableArray* orderAmtArr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:orderAmtQry];
//            
//            NSLog(@"%@", [orderAmtArr valueForKey:@"Order_Amt"]);
//            
//            if (orderAmtArr.count>0) {
//                NSString *prefixStr=@"AED";
//                
//                double returnTotal=[[[orderAmtArr valueForKey:@"Order_Amt"] objectAtIndex:0] doubleValue];
//                
//                
//                appendedStr=[NSString stringWithFormat:@"%@%0.2f",prefixStr,returnTotal];
//                cell.totalLbl.text=appendedStr;
//                orderValueLbl.text=appendedStr;
//                
//
//                
//            }
//            
//            
//        }
//        
//        double returnTotal=[[[orderAmtArr valueForKey:@"Order_Amt"] objectAtIndex:0] doubleValue];
//        
//        
//        appendedStr=[NSString stringWithFormat:@"%@%0.2f",prefixStr,returnTotal];
//        cell.totalLbl.text=appendedStr;
//        orderValueLbl.text=appendedStr;
//
        
        finalTot=[cell.unitPriceLbl.text floatValue]*[cell.quantityLbl.text floatValue];

    
    }
    
    else
    {
        
    }
   
    
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *simpleTableIdentifier = @"headerCell";

    ReviewDocumentsHeaderTableViewCell* cell=(ReviewDocumentsHeaderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell==nil) {
        
        NSArray* topLvel=[[NSBundle mainBundle]loadNibNamed:@"ReviewDocumentsHeaderTableViewCell" owner:self options:nil];
        cell=[topLvel objectAtIndex:0];
    }
    return cell;
    
    
    
    
}// custom view for header. will be adjusted to default or specified header height

#pragma mark segmentedControl Method

- (IBAction)segmentSwitch:(id)sender {
    
    
    self.customSegment=(UISegmentedControl*)sender;
    NSInteger selectedSegment =self.customSegment.selectedSegmentIndex;
    
    if (selectedSegment==0) {
        orderType=@"Ordered Items";

        NSLog(@"ordered items segment tapped");
        [self fetchOrdersData];
        [reportsTableView reloadData];

    }
    else
    {
        
        
        
        NSLog(@"invoiced items segment tapped");
        
        orderType=@"Invoice";
        [self fetchInvoicedItems];
        [reportsTableView reloadData];
    

        
    }
}
- (IBAction)prevOrderButton:(id)sender {
    
    
    
   
    
    NSNumber * num=[NSNumber numberWithInt:selectedIndex];
    
    
    
    
    if (ordersArray.count>1 && selectedIndex<ordersArray.count+1 && selectedIndex>0&& num!=nil) {
        
        
        [self loadButtonImages];
        
        
        
        
        
        selectedIndex=selectedIndex-1;
        
        
        if ([[[ordersArray valueForKey:@"Doctype"] objectAtIndex:selectedIndex] isEqualToString:@"Return"]) {
            
            [self.prevOrderButton setBackgroundImage:[UIImage imageNamed:@"Old_UpArrowInActive"] forState:UIControlStateNormal];

//            orderType=@"Return";
//
//            [self hideControlsforReturn];
//            
//            documentNumber=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex] ;
//
//            [self fetchDataforReturns];
//            [reportsTableView reloadData];
            
            
        }
        
        
        else
        {
        
        if (self.customSegment.selectedSegmentIndex==0) {
            
            [self fetchOrdersData];
            [reportsTableView reloadData];
        }
        else
        {
            documentNumber=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex] ;

            [self fetchInvoicedItems];
            [reportsTableView reloadData];
        }
        
        }
    }
    
    else
    {
        [self.prevOrderButton setBackgroundImage:[UIImage imageNamed:@"Old_UpArrowInActive"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)nextOrderButton:(id)sender {
    
    
   
    
    
    if (ordersArray.count>1 && selectedIndex<ordersArray.count-1) {
        [self loadButtonImages];

        selectedIndex=selectedIndex+1;
        
        
        
        if ([[[ordersArray valueForKey:@"Doctype"] objectAtIndex:selectedIndex] isEqualToString:@"Return"]) {
            
            //[self.prevOrderButton setBackgroundImage:[UIImage imageNamed:@"Old_DownArrowInActive"] forState:UIControlStateNormal];
            
            
        }
        
        else
        {
        
        
        if (self.customSegment.selectedSegmentIndex==0)
        {
            
            [self fetchOrdersData];
        }
        else
        {
            documentNumber=[[ordersArray valueForKey:@"DocNo"] objectAtIndex:selectedIndex] ;

            [self fetchInvoicedItems];
        }        [reportsTableView reloadData];

    }
    }
    
    else
    {
        [self.nextOrderButton setBackgroundImage:[UIImage imageNamed:@"Old_DownArrowInActive"] forState:UIControlStateNormal];
        
        if ([[self.nextOrderButton backgroundImageForState:UIControlStateNormal]isEqual:[UIImage imageNamed:@"Old_DownArrowInActive"]]) {
            NSLog(@"arrow changed");
            
        }

    }
    
    
}
@end
