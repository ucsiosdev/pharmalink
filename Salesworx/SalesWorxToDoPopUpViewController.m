//
//  SalesWorxToDoPopUpViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 10/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxToDoPopUpViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxToDoPopUpTableViewCell.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxToDoDetailPopUpViewController.h"
@interface SalesWorxToDoPopUpViewController ()

@end

@implementation SalesWorxToDoPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    ToDoArray=[[NSMutableArray alloc]init];
    filteredToDoArray=[[NSMutableArray alloc]init];
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    ToDoArray=[[SWDatabaseManager retrieveManager] fetchTodoList];
    if(self.isMovingToParentViewController)
    {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
        filteredToDoArray=ToDoArray;

    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        [TopContentView setHidden:NO];
    }

    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UITableView Methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    tableView.separatorColor=kUITableViewSaperatorColor;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filteredToDoArray.count;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95.0f;

}
-(void)viewDidAppear:(BOOL)animated
{
    [toDoListTableView reloadData];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];

        static NSString* identifier=@"todoCell";
        SalesWorxToDoPopUpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxToDoPopUpTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        
        ToDo * currentToDoContent=[[ToDo alloc]init];
        
        currentToDoContent=[filteredToDoArray objectAtIndex:indexPath.row];

        NSString* statusString=currentToDoContent.Status;
        if ([NSString isEmpty:statusString]==NO) {
            if ([statusString isEqualToString:kNewStatus]) {
                cell.statusView.backgroundColor=kNewToDoColor;
            }
            else if ([statusString isEqualToString:kClosedStatisticsTitle]) {
                cell.statusView.backgroundColor=kClosedToDoColor;
            }
            else if ([statusString isEqualToString:kCancelledStatisticsTitle]) {
                cell.statusView.backgroundColor=kCancelledToDoColor;
            }
            else if ([statusString isEqualToString:kDeferedStatisticsTitle]) {
                cell.statusView.backgroundColor=kDefferedToDoColor;
            }
            else if ([statusString isEqualToString:kCompletedStatisticsTitle]) {
                cell.statusView.backgroundColor=kCompletedColor;
            }
        }
        
        
        cell.titleLbl.text=[SWDefaults getValidStringValue:currentToDoContent.Title];
        cell.descLbl.text=[SWDefaults getValidStringValue:currentToDoContent.Description];
        //cell.statisticsDescLbl.text=[SWDefaults getValidStringValue:[currentDict valueForKey:kStatisticsTitle]];
//       cell.statusView.layer.cornerRadius = cell.statusView.frame.size.width / 2.0;
    
    cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    cell.titleLbl.textColor=MedRepMenuTitleFontColor;
    cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
    cell.customerLabel.text=currentToDoContent.customerName;
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    
    cell.cellDividerImg.hidden=NO;
    
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   ToDo *  currentToDo=[filteredToDoArray objectAtIndex:indexPath.row];
    SalesWorxToDoDetailPopUpViewController *toDoDetailPopUpViewController=[[SalesWorxToDoDetailPopUpViewController alloc]initWithNibName:@"SalesWorxToDoDetailPopUpViewController" bundle:[NSBundle mainBundle]];
    [TopContentView setHidden:YES];
    toDoDetailPopUpViewController.currentToDoItem=currentToDo;
    [self.navigationController pushViewController:toDoDetailPopUpViewController animated:NO];

    
}

- (IBAction)doneButtontapped:(id)sender {
    
    
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];
}
- (void)CloseSyncPopOver
{
    //do what you need to do when animation ends...
    [self dismissViewControllerAnimated:NO completion:^{
 
    }];
    
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = TopContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}



- (IBAction)filterButtonTapped:(id)sender
    {
        
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        //blurredBgImage.image = [self blurWithImageEffects:[self takeSnapshotOfView:[self view]]];
        
        //[self.view addSubview:blurredBgImage];
        SalesWorxVisitOptionsToDoFilterViewController * popOverVC=[[SalesWorxVisitOptionsToDoFilterViewController alloc]init];
        // popOverVC.popOverContentArray=contentArray;
        //popOverVC.salesWorxPopOverControllerDelegate=self;
        //popOverVC.disableSearch=YES;
        popOverVC.delegate = self;
        popOverVC.previousFilterParametersDict=previousFilterParameters;
        
        popOverVC.toDoArray=ToDoArray;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(376, 250);
        // popOverVC.titleKey=popOverString;
        self.modalInPopover=YES;
        
        
        popover.sourceView = filterButton;
        popover.sourceRect =filterButton.bounds;
        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
        //[self presentViewController:nav animated:YES completion:nil];
        toDoSearchBar.text=@"";
        [self searchContent];
        
        
        [self presentViewController:nav animated:YES completion:^{
            popover.passthroughViews=nil;
            self.modalInPopover=YES;
            
        }];
        
    }
    
    
    -(void)toDoFilterdidReset
    {
        [blurredBgImage removeFromSuperview];
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        previousFilterParameters=[[NSMutableDictionary alloc]init];
        filteredToDoArray=[ToDoArray mutableCopy];
        [toDoListTableView reloadData];        
    }
    -(void)toDoFilterDidClose
    {
        [blurredBgImage removeFromSuperview];
        
    }
    
    
    
    -(void)filteredToDoList:(NSMutableArray*)filteredContent
    {
        NSLog(@"filtered todo list is %@", filteredContent);
        [blurredBgImage removeFromSuperview];
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
        [toDoListTableView reloadData];
    }
    -(void)filteredToDoParameters:(NSMutableDictionary*)parametersDict
    {
        previousFilterParameters=[[NSMutableDictionary alloc]init];
        previousFilterParameters=parametersDict;
        NSLog(@"filtered parameters dict %@", previousFilterParameters);
        [self searchContent];
        
    }
    - (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
    {
        return NO;
    }
    
#pragma mark Search Methods
    
    
    - (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
        [toDoSearchBar setShowsCancelButton:YES animated:YES];

    }
    
    - (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
        [self searchContent];

    }

    
    - (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
        
        [toDoSearchBar setText:@""];
        [toDoSearchBar setShowsCancelButton:NO animated:YES];
        filteredToDoArray=[[NSMutableArray alloc]init];
        if ([toDoSearchBar isFirstResponder]) {
            [toDoSearchBar resignFirstResponder];
        }
        
        [self.view endEditing:YES];
        [self searchContent];
    }
    
    
    
    
    - (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
        NSLog(@"Search Clicked with text %@",searchBar.text);
        [self.view endEditing:YES];
        [toDoSearchBar setShowsCancelButton:NO animated:YES];
    }
    
    -(void)searchContent
    {
        
        NSLog(@"searching with text in todo popup %@",toDoSearchBar.text);

        NSMutableArray *predicateArray = [NSMutableArray array];
        NSLog(@"filter parameters dict is %@",previousFilterParameters);
        
        
        NSString* dateString=[previousFilterParameters valueForKey:@"Todo_Date"];
        NSString* statusString=[previousFilterParameters valueForKey:@"Status"];
        if([NSString isEmpty:dateString]==NO) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Todo_Date ==[cd] %@",dateString]];
        }
        
        if([NSString isEmpty:statusString]==NO) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Status ==[cd] %@",statusString]];
        }
        
        NSString *searchString = toDoSearchBar.text;
        NSString* filter = @"%K CONTAINS[cd] %@";
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Title", searchString];
        if(searchString.length>0)
        [predicateArray addObject:predicate1];

        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        NSLog(@"predicate is %@", [compoundpred description]);
        filteredToDoArray=[[NSMutableArray alloc]init];
        filteredToDoArray=[[ToDoArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
        
        [toDoListTableView reloadData];

}
@end
