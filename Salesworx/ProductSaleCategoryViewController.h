//
//  ProductSaleCategoryViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/12/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "SalesOrderViewController.h"

@interface ProductSaleCategoryViewController : SWTableViewController  {
//       id target;
    SEL action;
    
    NSDictionary *customer;
    NSArray *items;
    BOOL isPopover;
    //
    SalesOrderViewController *salesOrderViewController;
    
    UIView *myBackgroundView;
    //UITableView *templateTV;
    NSMutableArray *totalOrderArray;
    int indexPathTemplate;
    BOOL isFromVisit;
    NSString *orderID;
}




@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


- (id)initWithCustomer:(NSDictionary *)customer;

@end