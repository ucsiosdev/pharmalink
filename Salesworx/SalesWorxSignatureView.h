//
//  SalesWorxSignatureView.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 2/2/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxImageAnnimateButton.h"
#import "TESignatureView.h"
#import "SWDefaults.h"


@protocol SalesWorxSignatureViewDelegate<NSObject>
@optional
    -(void)SalesWorxSignatureViewSaveButtonTapped:(UIView *)signView;
    -(void)SalesWorxSignatureViewClearButtonTapped:(UIView *)signView;
@end

@interface SalesWorxSignatureView : UIView<TESignatureViewDelegate>
{
    IBOutlet SalesWorxImageAnnimateButton *signatureClearButton;
    IBOutlet SalesWorxImageAnnimateButton *signatureSaveButton;
}
@property (nonatomic,setter=setStatus:) SignatureViewStatusType status;
@property (strong,nonatomic) IBOutlet id <SalesWorxSignatureViewDelegate>delegate;
-(void)performSignatureSaveAnimation;
@property (strong,nonatomic) IBOutlet TESignatureView *signatureArea;

@end
