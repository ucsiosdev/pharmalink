//
//  SWViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWViewController.h"
#import "SWFoundation.h"
#import "SWDefaults.h"


@interface SWViewController ()

@end

@implementation SWViewController
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   
    
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [[self.navigationController navigationBar] setTintColor:[UIColor whiteColor]];
    [[self.navigationController toolbar] setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
//    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                                                                          RegularFontOfSize(22.0f), UITextAttributeFont, 
//                                                                                                          nil]];
//    
//    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                          BoldSemiFontOfSize(13.0f), UITextAttributeFont, 
//                                                          nil] forState:UIControlStateNormal];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
//            UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
//            [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
//            
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"cache:NO] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
            
        } else {
        }
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    // if we're operating within a navigation controller ...
    if (self.navigationController != nil)
    {
        // set the tint colors for both the nav bar and toolbar -- these can be set back to nil for
        // controllers that might want to have a different color, like black
//        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
        
       
    }
}

- (void)changeUISegmentFont:(UIView *)aView {
    NSString *className = NSStringFromClass(aView.class);
	if ([className isEqualToString:@"UISegmentLabel"]) {
		UILabel* label = (UILabel*)aView;
		[label setTextAlignment:NSTextAlignmentCenter];
		[label setFont:RegularFontOfSize(13.0f)];
	}

	NSArray* subs = [aView subviews];
	NSEnumerator* iter = [subs objectEnumerator];
	UIView* subView;
	while (subView = [iter nextObject]) {
		[self changeUISegmentFont:subView];
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
    {
        return YES;
    }
    else {
        return NO;

    }
}

- (void)setupToolbar:(NSArray *)buttons {    
    [self setToolbarItems:nil];
    
    if (buttons) {
        [self setToolbarItems:buttons];
        [self.navigationController setToolbarHidden:NO animated:YES];
    } else {
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}

#pragma mark - Table view data source

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 70.0f;
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
        
    }
    
    [cell setBackgroundColor:[UIColor whiteColor]];

    [cell.contentView setBackgroundColor:[UIColor whiteColor]];

    cell.textLabel.font=UITableViewCellFont;
    cell.detailTextLabel.font=UITableViewCellDescFont;
    
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=[UIColor clearColor];
    
//    [cell.textLabel setFont:LightFontOfSize(14.0f)];
//    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
