//
//  MedRepProductDescriptionCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/12/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepProductDescriptionCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *selectedImgView;

@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) IBOutlet UIImageView *selectedTickImageView;

@end
