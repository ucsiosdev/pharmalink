//
//  SalesWorxFieldSalesDailyVisitSummaryViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldSalesDailyVisitSummaryViewController.h"
#import "DailyVisitSummaryTableViewCell.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SalesWorxDailyVisitSummaryNotesPopoverViewController.h"

@interface SalesWorxFieldSalesDailyVisitSummaryViewController ()<UIPopoverPresentationControllerDelegate>

@end

@implementation SalesWorxFieldSalesDailyVisitSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrDailyVisit = [[NSMutableArray alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                   {
                       NSMutableArray *arrOfVisits = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT Customer_ID FROM TBL_FSR_Actual_Visits where date(Visit_End_Date) = date('now')"];
                       
                       for (int i =0; i<[arrOfVisits count]; i++){
                           NSMutableDictionary *gpsDict = [NSMutableDictionary dictionary];
                           [dict setObject:gpsDict forKey:[[arrOfVisits objectAtIndex:i]valueForKey:@"Customer_ID"]];
                       }
                       
                       NSMutableArray *arrOfCollection = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT sum(Amount) As total,count(*), Customer_ID FROM TBL_Collection where date(Collected_On) = date('now') group by Customer_ID"];
                       
                       for (int i = 0; i<[arrOfCollection count]; i++)
                       {
                           NSMutableDictionary *gpsDict = [NSMutableDictionary dictionary];
                           [gpsDict setObject:[[arrOfCollection objectAtIndex:i]valueForKey:@"count(*)"] forKey:@"Payment"];
                           [gpsDict setObject:[[arrOfCollection objectAtIndex:i]valueForKey:@"total"] forKey:@"PaymentValue"];
                           
                           [dict setObject:gpsDict forKey:[[arrOfCollection objectAtIndex:i]valueForKey:@"Customer_ID"]];
                       }
                       
                       NSMutableArray *arrOfDistributionCheck = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT count(*), Customer_ID FROM TBL_Distribution_Check where date(Checked_On) = date('now') group by Customer_ID"];
                       
                       for (int i = 0; i<[arrOfDistributionCheck count]; i++)
                       {
                           if ([dict objectForKey:[[arrOfDistributionCheck objectAtIndex:i]valueForKey:@"Customer_ID"]])
                           {
                               NSMutableDictionary *dictionaryNewRecords = [dict objectForKey:[[arrOfDistributionCheck objectAtIndex:i]valueForKey:@"Customer_ID"]];
                               [dictionaryNewRecords setObject:@"Y" forKey:@"DC"];
                               
                               [dict setObject:dictionaryNewRecords forKey:[[arrOfDistributionCheck objectAtIndex:i]valueForKey:@"Customer_ID"]];
                           }
                           else
                           {
                               NSMutableDictionary *gpsDict = [NSMutableDictionary dictionary];
                               [gpsDict setObject:@"Y" forKey:@"DC"];
                               
                               [dict setObject:gpsDict forKey:[[arrOfDistributionCheck objectAtIndex:i]valueForKey:@"Customer_ID"]];
                           }
                       }
                       
                       NSMutableArray *arrOfSurvey = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT count(*), Customer_ID FROM TBL_Survey_Cust_Responses where date(Survey_Timestamp) = date('now') group by Customer_ID"];
                       
                       for (int i = 0; i<[arrOfSurvey count]; i++)
                       {
                           if ([dict objectForKey:[[arrOfSurvey objectAtIndex:i]valueForKey:@"Customer_ID"]])
                           {
                               NSMutableDictionary *dictionaryNewRecords = [dict objectForKey:[[arrOfSurvey objectAtIndex:i]valueForKey:@"Customer_ID"]];
                               [dictionaryNewRecords setObject:@"Y" forKey:@"Survey"];
                               
                               [dict setObject:dictionaryNewRecords forKey:[[arrOfSurvey objectAtIndex:i]valueForKey:@"Customer_ID"]];
                           }
                           else
                           {
                               NSMutableDictionary *gpsDict = [NSMutableDictionary dictionary];
                               [gpsDict setObject:@"Y" forKey:@"Survey"];
                               
                               [dict setObject:gpsDict forKey:[[arrOfSurvey objectAtIndex:i]valueForKey:@"Customer_ID"]];
                           }
                       }
                       
                       NSMutableArray *arrOfOrder = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT count(*),sum(Transaction_Amt) As total, Ship_To_Customer_ID FROM TBL_Order_History where date(Creation_Date) = date('now') AND Doc_Type='I' group by Ship_To_Customer_ID"];
                       
                       for (int i = 0; i<[arrOfOrder count]; i++)
                       {
                           if ([dict objectForKey:[[arrOfOrder objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]])
                           {
                               NSMutableDictionary *dictionaryNewRecords = [dict objectForKey:[[arrOfOrder objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]];
                               [dictionaryNewRecords setObject:[[arrOfOrder objectAtIndex:i]valueForKey:@"count(*)"] forKey:@"Order"];
                               [dictionaryNewRecords setObject:[[arrOfOrder objectAtIndex:i]valueForKey:@"total"] forKey:@"OrderValue"];
                               
                               [dict setObject:dictionaryNewRecords forKey:[[arrOfOrder objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]];
                           }
                           else
                           {
                               NSMutableDictionary *gpsDict = [NSMutableDictionary dictionary];
                               [gpsDict setObject:[[arrOfOrder objectAtIndex:i]valueForKey:@"count(*)"] forKey:@"Order"];
                               [gpsDict setObject:[[arrOfOrder objectAtIndex:i]valueForKey:@"total"] forKey:@"OrderValue"];
                               
                               [dict setObject:gpsDict forKey:[[arrOfOrder objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]];
                           }
                       }
                       
                       NSMutableArray *arrOfReturns = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT count(*),sum(Transaction_Amt) As total, Ship_To_Customer_ID FROM TBL_Order_History where date(Creation_Date) = date('now') AND Doc_Type='R' group by Ship_To_Customer_ID"];
                       
                       for (int i = 0; i<[arrOfReturns count]; i++)
                       {
                           if ([dict objectForKey:[[arrOfReturns objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]])
                           {
                               NSMutableDictionary *dictionaryNewRecords = [dict objectForKey:[[arrOfReturns objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]];
                               [dictionaryNewRecords setObject:[[arrOfReturns objectAtIndex:i]valueForKey:@"count(*)"] forKey:@"Return"];
                               [dictionaryNewRecords setObject:[[arrOfReturns objectAtIndex:i]valueForKey:@"total"] forKey:@"ReturnValue"];
                               
                               [dict setObject:dictionaryNewRecords forKey:[[arrOfReturns objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]];
                           }
                           else
                           {
                               NSMutableDictionary *gpsDict = [NSMutableDictionary dictionary];
                               [gpsDict setObject:[[arrOfReturns objectAtIndex:i]valueForKey:@"count(*)"] forKey:@"Return"];
                               [gpsDict setObject:[[arrOfReturns objectAtIndex:i]valueForKey:@"total"] forKey:@"ReturnValue"];
                               
                               [dict setObject:gpsDict forKey:[[arrOfReturns objectAtIndex:i]valueForKey:@"Ship_To_Customer_ID"]];
                           }
                       }
                       
                       NSArray *allKeys = [dict allKeys];
                       for (id key in allKeys)
                       {
                           NSMutableArray *arrOfCustomer = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_No,Customer_Name FROM TBL_Customer where Customer_ID = '%@'",key]];
                           
                           if ([dict objectForKey:key])
                           {
                               NSMutableDictionary *dictionaryNewRecords = [dict objectForKey:key];
                               [dictionaryNewRecords setObject:[[arrOfCustomer objectAtIndex:0]valueForKey:@"Customer_No"] forKey:@"Customer_No"];
                               [dictionaryNewRecords setObject:[[arrOfCustomer objectAtIndex:0]valueForKey:@"Customer_Name"] forKey:@"Customer_Name"];
                               [dictionaryNewRecords setObject:key forKey:@"Customer_ID"];
                               [dict setObject:dictionaryNewRecords forKey:key];
                           }
                       }
                       
                       dispatch_async(dispatch_get_main_queue(), ^(void){
                           
                           for (id key in dict) {
                               [arrDailyVisit addObject:[dict objectForKey:key]];
                           }
                           [tblDailyVisitSummary reloadData];
                       });
                   });
}

-(void)viewWillAppear:(BOOL)animated {
    
    [tblDailyVisitSummary setContentInset:UIEdgeInsetsMake(0, 0, 0, 228)];
    [tblDailyVisitSummary setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

-(void)viewDidAppear:(BOOL)animated
{
//    [tblDailyVisitSummary scrollsToTop];
}

#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // custom view for header. will be adjusted to default or specified header height
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1238, 44.0)];
    dailyVisitSummaryTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:dailyVisitSummaryTableHeaderView];
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDailyVisit.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DailyVisitSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DailyVisitSummaryTableCell"];
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DailyVisitSummaryTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    @try {
        cell.lblCustomerCode.text = [NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Customer_No"]];
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Customer_Name"]];
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]valueForKey:@"DC"]] isEqualToString:@"(null)"]) {
            cell.lblDC.text = @"N";
        }
        else
        {
            cell.lblDC.text = [NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"DC"]];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Survey"]] isEqualToString:@"(null)"]) {
            cell.lblSurvey.text = @"N";
        }
        else
        {
            cell.lblSurvey.text = [NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Survey"]];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Order"]] isEqualToString:@"(null)"]) {
            cell.lblOrders.text = @"0";
        }
        else
        {
            cell.lblOrders.text = [NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Order"]];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"OrderValue"]] isEqualToString:@"(null)"]) {
            cell.lblOrderValue.text = [@"0" currencyString];
        }
        else
        {
            cell.lblOrderValue.text = [[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"OrderValue"]] currencyString];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Return"]] isEqualToString:@"(null)"]) {
            cell.lblReturns.text = @"0";
        }
        else
        {
            cell.lblReturns.text = [NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Return"]];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"ReturnValue"]] isEqualToString:@"(null)"]) {
            cell.lblReturnsValue.text = [@"0" currencyString];
        }
        else
        {
            cell.lblReturnsValue.text = [[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"ReturnValue"]] currencyString];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Payment"]] isEqualToString:@"(null)"]) {
            cell.lblPayments.text = @"0";
        }
        else
        {
            cell.lblPayments.text = [NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Payment"]];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"PaymentValue"]] isEqualToString:@"(null)"]) {
            cell.lblPaymentValue.text = [@"N" currencyString];
        }
        else
        {
            cell.lblPaymentValue.text = [[NSString stringWithFormat:@"%@",[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"PaymentValue"]] currencyString];
        }
        
        cell.btnVisitNotes.tag = indexPath.row;
        [cell.btnVisitNotes addTarget:self action:@selector(showVisitNotesPopover:) forControlEvents:UIControlEventTouchUpInside];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    cell.lblCustomerCode.textColor = MedRepMenuTitleFontColor;
    cell.lblCustomerName.textColor = MedRepMenuTitleFontColor;
    cell.lblDC.textColor = MedRepMenuTitleFontColor;
    cell.lblOrders.textColor = MedRepMenuTitleFontColor;
    cell.lblOrderValue.textColor = MedRepMenuTitleFontColor;
    cell.lblReturns.textColor = MedRepMenuTitleFontColor;
    cell.lblReturnsValue.textColor = MedRepMenuTitleFontColor;
    cell.lblPayments.textColor = MedRepMenuTitleFontColor;
    cell.lblPaymentValue.textColor = MedRepMenuTitleFontColor;
    cell.lblSurvey.textColor = MedRepMenuTitleFontColor;
    cell.contentView.backgroundColor = UITableviewUnSelectedCellBackgroundColor;
    return cell;
}

-(void)showVisitNotesPopover:(id)Sender
{
    UIButton *tappedButton = Sender;
    
    SalesWorxDailyVisitSummaryNotesPopoverViewController *notesVC = [[SalesWorxDailyVisitSummaryNotesPopoverViewController alloc]init];
    notesVC.selectedCustomerID = [[arrDailyVisit objectAtIndex:tappedButton.tag]valueForKey:@"Customer_ID"];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:notesVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    notesVC.preferredContentSize = CGSizeMake(450, 570);
    popover.delegate = self;
    popover.sourceView = tappedButton.superview;
    popover.sourceRect = tappedButton.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
