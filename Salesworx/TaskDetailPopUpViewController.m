//
//  TaskDetailPopUpViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "TaskDetailPopUpViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"

#define kTitle @"Title"
#define kDescription @"Information"
#define kMessageTitle @"Message"
#define kDateTitle @"Date"
#define kStatus @"Status"
#define kSubmitTitle @"Submit"
#define kNameTitle @"Name"

@interface TaskDetailPopUpViewController ()

@end

@implementation TaskDetailPopUpViewController

@synthesize selectedTask;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setHiddenAnimated:NO duration:1.25];
    
    titleTxtFld.text = selectedTask.Title;
    InformationTxtFld.text = selectedTask.Description;
    statusTxtFld.text = selectedTask.Status;
    categoryTxtFld.text = selectedTask.Category;
    priorityTxtFld.text = selectedTask.Priority;
    dueDateTxtFld.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:selectedTask.Start_Time]];
    
    statusArray = [MedRepQueries fetchTaskAppCodes:@"Task_Status"];
    categoryArray = [MedRepQueries fetchTaskAppCodes:@"Task_Category"];
    priorityArray = [MedRepQueries fetchTaskAppCodes:@"Task_Priority"];

    
    if([selectedTask.Status isEqualToString:kCompletedStatisticsTitle] || [selectedTask.Status isEqualToString:kClosedStatisticsTitle] || [selectedTask.Status isEqualToString:kCancelledStatisticsTitle])
    {
        [InformationTxtFld setIsReadOnly:YES];
        [titleTxtFld setIsReadOnly:YES];
        [dueDateTxtFld setIsReadOnly:YES];
        [statusTxtFld setIsReadOnly:YES];
        [categoryTxtFld setIsReadOnly:YES];
        [priorityTxtFld setIsReadOnly:YES];
        
        [btnSave setHidden:YES];
    }
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = TopContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark UITextField Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == dueDateTxtFld) {
        
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = NSLocalizedString(@"Date", nil);
        popOverVC.datePickerMode=kTodoTitle;
        popOverVC.setMinimumDateCurrentDate=YES;
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate=self;
        popOverVC.datePickerPopOverController=datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:dueDateTxtFld.dropdownImageView.frame inView:dueDateTxtFld.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        return NO;
    }
    else if (textField == statusTxtFld)
    {
        popoverString = @"Status";
        
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray = statusArray;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        statusPopOverController=nil;
        statusPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        statusPopOverController.delegate=self;
        popOverVC.popOverController=statusPopOverController;
        popOverVC.titleKey = NSLocalizedString(@"Status", nil);
        [statusPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
        [statusPopOverController presentPopoverFromRect:statusTxtFld.dropdownImageView.frame inView:statusTxtFld.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        return NO;
    }
    else if (textField == priorityTxtFld)
    {
        popoverString = @"Priority";
        
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray = priorityArray;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        statusPopOverController=nil;
        statusPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        statusPopOverController.delegate=self;
        popOverVC.popOverController=statusPopOverController;
        popOverVC.titleKey = NSLocalizedString(@"Priority", nil);
        [statusPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
        [statusPopOverController presentPopoverFromRect:priorityTxtFld.dropdownImageView.frame inView:priorityTxtFld.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        return NO;
    }
    else if (textField == categoryTxtFld)
    {
        popoverString = @"Category";
        
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray = categoryArray;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        statusPopOverController=nil;
        statusPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        statusPopOverController.delegate=self;
        popOverVC.popOverController=statusPopOverController;
        popOverVC.titleKey = NSLocalizedString(@"Category", nil);
        [statusPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
        [statusPopOverController presentPopoverFromRect:categoryTxtFld.dropdownImageView.frame inView:categoryTxtFld.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        return NO;
    }
    else
        return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == titleTxtFld) {
        NSString* titleText=[SWDefaults getValidStringValue:textField.text];
        selectedTask.Title=titleText;
        [titleTxtFld resignFirstResponder];
//        [DescriptionTextView becomeFirstResponder];
        
    }
}

#pragma mark UITextView Delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == titleTxtFld) {
        [titleTxtFld resignFirstResponder];
//        [DescriptionTextView becomeFirstResponder];
        return NO;
    }
    else
        return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView == InformationTxtFld) {
        NSString* messageText = [SWDefaults getValidStringValue:textView.text];
        selectedTask.Description = messageText;
    }
}

#pragma mark Selected Date Delegate
-(void)didSelectDate:(NSString*)selectedDate
{
    if ([NSString isEmpty:selectedDate]==NO) {
        
        dueDateTxtFld.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:selectedDate]];
        selectedTask.Start_Time = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDatabseDefaultDateFormat scrString:[SWDefaults getValidStringValue:selectedDate]];;
    }
}
#pragma mark Status Delegate
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popoverString isEqualToString:@"Status"]) {
        NSString *selectedStatus = [statusArray objectAtIndex:selectedIndexPath.row];
        if ([NSString isEmpty:selectedStatus]==NO) {
            statusTxtFld.text = [SWDefaults getValidStringValue:selectedStatus];
            selectedTask.Status = selectedStatus;
        }
    }else if ([popoverString isEqualToString:@"Priority"]) {
        NSString *selectedPriority = [priorityArray objectAtIndex:selectedIndexPath.row];
        if ([NSString isEmpty:selectedPriority]==NO) {
            priorityTxtFld.text = [SWDefaults getValidStringValue:selectedPriority];
            selectedTask.Priority = selectedPriority;
        }
    } else {
        NSString *selectedCategory = [categoryArray objectAtIndex:selectedIndexPath.row];
        if ([NSString isEmpty:selectedCategory]==NO) {
            categoryTxtFld.text = [SWDefaults getValidStringValue:selectedCategory];
            selectedTask.Category = selectedCategory;
        }
    }
}


- (IBAction)saveButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    //validations
    NSString* Title = selectedTask.Title;
    NSString* Description = selectedTask.Description;
    NSString* Date = selectedTask.Created_At;
    NSString* Status = selectedTask.Status;
    
    NSString* missingContent=[[NSString alloc]init];
    
    if ([NSString isEmpty:Title]) {
        missingContent = kTitle;
    }
    else if ([NSString isEmpty:Description]) {
        missingContent = kDescription;
    }
    else if ([NSString isEmpty:Date]) {
        missingContent = kDateTitle;
    }
    else if ([NSString isEmpty:Status]) {
        missingContent = kStatus;
    }
    else
    {
        
    }
    
    if ([NSString isEmpty:missingContent]==NO) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please enter %@",missingContent] withController:self];
    }
    
    else
    {
        selectedTask.Last_Updated_At = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        BOOL insertStatus=[MedRepQueries UpdateTask:selectedTask];
        
        if (insertStatus==YES) {
            
            NSLog(@"insert successfull in Task");
            [self cancelButtonTapped:btnCancel];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
