//
//  MedRepEdetailNotesDetailViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepEdetailNotesDetailViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"

@interface MedRepEdetailNotesDetailViewController ()

@end

@implementation MedRepEdetailNotesDetailViewController
@synthesize notesDetailsDict,visitDetailsDict,notesArray,titleTxtFld,notesDescTxtView,isUpdatingNotes,selectedNoteIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([SWDefaults isRTL]) {
        titleTxtFld.textAlignment=NSTextAlignmentRight;
    }
    else{
        titleTxtFld.textAlignment=NSTextAlignmentLeft;
    }
    titleTxtFld.placeholder=NSLocalizedString(@"Title", nil);

    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingNotesDetails];
    }
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Notes"];

 
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    
    if (selectedNoteIndex) {
        notes=[notesArray objectAtIndex:selectedNoteIndex.row];
        
        
      
        
    }
    
    else
    {
        notes=[[VisitNotes alloc]init];
        

    }
    
    
     UIBarButtonItem* saveBtn;
    
    if (isUpdatingNotes==YES) {
        saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Update", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    }
    
    else
    {
        saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    }
   
   
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    
//    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
//    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
//    
//    self.navigationItem .leftBarButtonItem=cancelBtn;
    
    self.navigationController.navigationBar.topItem.title=@"";

    
    notesDetailsDict=[[NSMutableDictionary alloc]init];
    @try {
        [notesDetailsDict setValue:[visitDetailsDict valueForKey:@"Planned_Visit_ID"] forKey:@"Visit_ID"];
    }
    @catch (NSException *exception) {
        
    }

    //check visit completion status
    
    
    
    
    
    if (self.isVisitCompleted || self.dashBoardViewing|| self.isInMultiCalls) {
        
        titleTxtFld.userInteractionEnabled=NO;
        notesDescTxtView.editable=NO;
        notesDescTxtView.selectable = NO;
    }
    
    else
    {
         self.navigationItem .rightBarButtonItem=saveBtn;
    }
    
    
   
    
    
    if (notesArray.count>0) {
        NSLog(@"notes array in desc %@", [notesArray description]);
        
            if (selectedNoteIndex) {
            titleTxtFld.text=[NSString stringWithFormat:@"%@", notes.Title];
            notesDescTxtView.text=[NSString stringWithFormat:@"%@", notes.Description];
            
        }
        
        else
        {
            
        }
        
        
       }
    
    
    

    if (self.isMovingToParentViewController) {
        
        [self updateTextFieldData];
        
        if (self.isInMultiCalls==YES) {
            
            [titleTxtFld becomeFirstResponder];
        }
    }
    
    if (isUpdatingNotes==YES) {
        
        doctorTextField.isReadOnly=YES;
    }
    else
    {
        doctorTextField.isReadOnly=NO;
    }
    
    

}

- (void)viewDidLayoutSubviews {
    [notesDescTxtView setContentOffset:CGPointZero animated:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([titleTxtFld isFirstResponder]) {
        
        [titleTxtFld resignFirstResponder];
        
        [notesDescTxtView becomeFirstResponder];
        
        return NO;

    }
    
    else
        
    {
        return YES;

    }
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)cancelTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveTapped
{
    
    self.navigationItem.rightBarButtonItem.enabled=NO;
    
    [self.view endEditing:YES];
   
    [self performSelector:@selector(saveNotes) withObject:self afterDelay:0.9];
    
    self.view.userInteractionEnabled=NO;
    
}
-(void)saveNotes
{
    self.view.userInteractionEnabled=YES;
    
    //    if ([titleTxtFld isFirstResponder]) {
    //
    //        [titleTxtFld resignFirstResponder];
    //    }
    //
    //    else if ([notesDescTxtView isFirstResponder])
    //
    //    {
    //        [notesDescTxtView resignFirstResponder];
    //    }
    
    if (titleTxtFld.text.length>0 && notesDescTxtView.text>0 ) {
        
        
        NSString* actualVisitID=[NSString createGuid];

        
        NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
        NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
        
        notes.Title=titleTxtFld.text;
        notes.Description=notesDescTxtView.text;
        notes.Visit_ID=actualVisitID;
        notes.Category=@"VISIT";
        notes.Emp_ID=empID;
        notes.Location_ID=self.visitDetails.Location_ID;
        
        
        
        notes.Doctor_ID=self.visitDetails.Doctor_ID;
        notes.Contact_ID=self.visitDetails.Contact_ID;
        notes.Created_At=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        notes.Created_By=createdBy;
        
        
//        [notesDetailsDict setValue:titleTxtFld.text forKey:@"Title"];
//        [notesDetailsDict setValue:notesDescTxtView.text forKey:@"Description"];
        
        NSLog(@"notes dict before saving is %@", [notesDetailsDict description]);
        
        
        //we need actual visit id in notes so creting it here and storing in defaults
        
        
        [[NSUserDefaults standardUserDefaults]setValue:actualVisitID forKey:@"Actual_Visit_ID"];
        
//        [notesArray setValue:actualVisitID forKey:@"Actual_Visit_ID"];
//        
//        [notesDetailsDict setValue:actualVisitID forKey:@"Actual_Visit_ID"];
        
        
        BOOL success=NO;
        if (notesArray.count>0) {
            
            //this note already exists user editing it
            
//            [notesArray setValue:titleTxtFld.text forKey:@"Title"];
//            [notesArray setValue:notesDescTxtView.text forKey:@"Description"];
//            
            
            
                  }
        
        else
        {
            //success= [MedRepQueries InsertNotes:notesDetailsDict andVisitDetails:visitDetailsDict];
            
        }
        
        
        if ([self.delegate respondsToSelector:@selector(savedNoteDetails:)]) {
            
            
            if (selectedNoteIndex) {
                [notesArray replaceObjectAtIndex:selectedNoteIndex.row withObject:notes];
                
            }
            else if (isUpdatingNotes==NO)
                
            {
                
                if (notesArray!=nil) {
                    
                    
                [notesArray addObject:notes];
                }
                
                else
                {
                    notesArray=[[NSMutableArray alloc]init];
                    [notesArray addObject:notes];
                    
                }
                
                notes.Note_ID=[NSString createGuid];


            }
            
            else
            {
                notesArray=[[NSMutableArray alloc]init];
                [notesArray addObject:notes];
                
            }
            
            
            
                      
            
            [self.delegate savedNoteDetails:notesArray];
            
            
            NSString* succesAlertText;
            if (isUpdatingNotes==YES) {
                
                succesAlertText=@"Notes updated successfully";
            }
            
            
            else
            {
                succesAlertText=@"Note created successfully";
                
            }
            
            UIAlertView* successAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString(succesAlertText, nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
            [successAlert show];
            
           // [SWDefaults showAlertAfterHidingKeyBoard:@"Success" andMessage:succesAlertText withController:self];

            
            self.navigationItem.rightBarButtonItem.enabled=YES;
            
            
            
        }
        
        // success=[MedRepQueries InsertNoteObject:notes andVisitDetails:self.visitDetails];
        
        //success= [MedRepQueries InsertNotes:notesArray andVisitDetails:visitDetailsDict];
        

        
        
        if (success==YES) {
            
            
            
        }
        
        
    }
    
    else
    {
        self.navigationItem.rightBarButtonItem.enabled=YES;

        UIAlertView* successAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please enter notes details", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        successAlert.tag=100;
        [successAlert show];
        
    }
    

}
#pragma  mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag==100) {
        
    }
    
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
        
        

        
        //[self.navigationController performSelector:@selector(popViewControllerAnimated:) withObject:self afterDelay:0.7];
    }

    
}


#pragma mark UIPopOver Controller delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

#pragma mark UITextField Delegate Methods


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= NoteTitleTextFieldLimit;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [notesDetailsDict setValue:textField.text forKey:@"Title"];
    notes.Title=textField.text;
    notes.isUpdated=@"YES";
    
}



#pragma mark UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= NoteInformationTextFieldLimit;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [notesDetailsDict setValue:textView.text forKey:@"Description"];
    notes.Description=textView.text;
    notes.isUpdated=@"YES";
    
    

}

-(void)showDoctorsList
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=locationType;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    
    NSLog(@"doctor location array in notes %@",self.visitDetails.doctorsforLocationArray);
    
    if ([self.visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        filterDescVC.filterDescArray=[self.visitDetails.doctorsforLocationArray valueForKey:@"Doctor_Name"];
        
    }
    else if ([self.visitDetails.Location_Type isEqualToString:kPharmacyLocation])
    {
        filterDescVC.filterDescArray=self.visitDetails.locationContactsArray ;
        
    }
    
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
}

-(void)updateTextFieldData
{
    if ([self.visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        doctorTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchDoctorwithID:notes.Doctor_ID]];
        
    }
    else if ([self.visitDetails.Location_Type isEqualToString:kPharmacyLocation])
    {
        doctorTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchContactwithContactID:notes.Contact_ID]];
        
    }

}

-(void)selectedValueforCreateVisit:(NSIndexPath*)indexPath
{
    
    
    
    
    
    if ([self.visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        
        Doctors * selectedDoctor=[self.visitDetails.doctorsforLocationArray objectAtIndex:indexPath.row];

        notes.Doctor_ID=selectedDoctor.Doctor_ID;
        
        doctorTextField.text=[MedRepDefaults getDefaultStringForEmptyString:selectedDoctor.Doctor_Name];
    }
    else if ([self.visitDetails.Location_Type isEqualToString:kPharmacyLocation])
    {
        Contact * selectedContact=[self.visitDetails.doctorsforLocationArray objectAtIndex:indexPath.row];
        
        notes.Contact_ID=selectedContact.Contact_ID;
        
        doctorTextField.text=[MedRepDefaults getDefaultStringForEmptyString:selectedContact.Contact_Name];
    }
    
    
    [titleTxtFld becomeFirstResponder];

    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==doctorTextField) {
        
        if ([self.visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
            locationType=kDoctorName;

        }
        else if ([self.visitDetails.Location_Type isEqualToString:kPharmacyLocation])
        {
            locationType=kContactName;

        }
        [self showDoctorsList];
        
        return NO;
    }
    else
    {
        return YES;
    }
}

@end
