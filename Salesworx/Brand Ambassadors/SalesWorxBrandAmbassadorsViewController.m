//
//  SalesWorxBrandAmbassadorsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorsViewController.h"

@interface SalesWorxBrandAmbassadorsViewController ()

@end

@implementation SalesWorxBrandAmbassadorsViewController
@synthesize blurredBgImage,visitsTblView,locationLbl,objectiveTxtView,plannedVisitTimeLbl,lastVisitedOnLbl,searchBarVisit, selectedVisit, isFromDashboard;

- (void)viewDidLoad {
    [super viewDidLoad];
    testDate=[NSDate date];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveVisitDidFinishNotification:)
                                    name:@"BrandAmbassadorVisitDidFinishNotification"
                                               object:nil];
    
    

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
        [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
    
    [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];

    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
        } else {
        }
    }
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    createVisitButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"PlusIcon_White"] style:UIBarButtonItemStylePlain target:self action:@selector(plusButtonTapped:)];
    
    startVisitButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartVisit"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitTapped)];
    
    
    editVisitButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NoteIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(editVisitButtonTapped:)];
    
    [self.navigationItem setRightBarButtonItems:@[startVisitButton,createVisitButton,editVisitButton]];
    // Do any additional setup after loading the view from its nib.
    
    tasksButton.layer.borderWidth=1.0f;
    notesButton.layer.borderWidth=1.0f;
    tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    
    
    
    notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    notesButton.layer.shadowOffset = CGSizeMake(2, 2);
    notesButton.layer.shadowOpacity = 0.1;
    notesButton.layer.shadowRadius = 1.0;
    
    
    
    tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
    tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
    tasksButton.layer.shadowOpacity = 0.1;
    tasksButton.layer.shadowRadius = 1.0;
    
    //brandAmbassadorVisitsArray=[self fetchFromDefaults];
    //[visitsTblView reloadData];
    
    [self UpdateScreentitle];
    
    
   
    
   
    
    [self fetchAllPlannedVisits];
  
    

}



-(void)fetchAllPlannedVisits
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        allLocationsArray=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchLocationsForBrandAmbassadorVisit];
        allBrandAmbassadorsArrayforFilter=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchAllBrandAmbassadorVisitsforFilter];
        
        NSLog(@"all visits are %@",[allBrandAmbassadorsArrayforFilter valueForKey:@"Date"]);
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:testDate];
            NSLog(@"executionTime = %f", executionTime);
        });
        
    });
}

-(void)viewWillDisappear:(BOOL)animated
{

}
-(void)willRevealCalled
{
    NSLog(@"will reveal delegate called");
    
    [searchBarVisit setShowsCancelButton:NO animated:YES];
    [self searchBarCancelButtonClicked:searchBarVisit];
}

-(void)receiveVisitDidFinishNotification:(NSNotification*)visitFinishNotidication
{
    
    NSLog(@"visit did finish notification received");
    
    SalesWorxBrandAmbassadorVisit * finishedVisit=visitFinishNotidication.object;
    

    NSLog(@"finished visit planned visit id %@", finishedVisit.Planned_Visit_ID);
    
    NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];
    allBrandAmbassadorsArrayforFilter =[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchAllBrandAmbassadorVisitsforFilter];

    brandAmbassadorVisitsArray=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchBrandAmbassadorVisitforSelectedDate:[MedRepQueries fetchDatabaseDateFormat]];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [previousFilteredParameters setValue:currentDateinDisplayFormat forKey:@"Visit_Date"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Planned_Visit_ID == %@",finishedVisit.Planned_Visit_ID];
    
    NSUInteger indexObj=[brandAmbassadorVisitsArray indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [predicate evaluateWithObject:obj];

    }];
    NSLog(@"index of object is %lu", (unsigned long)indexObj);
    if(indexObj==NSIntegerMax)
    {
        if(filteredPlannedVisitsArray.count>0)
        {
            [self tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [visitsTblView reloadData];
        }
    }
    else
    {
        NSLog(@"current completed visit status %@",[[brandAmbassadorVisitsArray objectAtIndex:indexObj]valueForKey:@"Visit_Status"] );
        
        [visitsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]
                                   animated:YES
                             scrollPosition:UITableViewScrollPositionNone];
        [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [visitsTblView reloadData];
        

        dispatch_async(dispatch_get_main_queue(), ^{
            [visitsTblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        });
        
    }
}

-(void)showNoVisitErrorMessageView
{
    [NoVisitMessageView setHidden:NO];
    [startVisitButton setEnabled:NO];
}
-(void)hideNoVisitErrorMessageView
{
    [NoVisitMessageView setHidden:YES];
}

-(void)disableNavigationBarOptionsOnPopOverPresentation
{
    
    [startVisitButton setEnabled:NO];
    [editVisitButton setEnabled:NO];
    [createVisitButton setEnabled:NO];
    
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
}
-(void)EnableNavigationBarOptionsOnPopOverDismissal
{
    [startVisitButton setEnabled:YES];
    [editVisitButton setEnabled:YES];
    [createVisitButton setEnabled:YES];
    
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
    
    if(brandAmbassadorVisitsArray.count>0)
    {
        if(selectedVisitIndexPath.row>=brandAmbassadorVisitsArray.count)
            [self tableView:visitsTblView didSelectRowAtIndexPath:selectedVisitIndexPath];
        else
            [self SetFirstIndexVisitSelected];
        
    }
    else
    {
        [startVisitButton setEnabled:NO];
        [editVisitButton setEnabled:NO];
        
        
    }
}



-(void)fetchBrandAmbassadorVisitsforDate:(NSString*)selectedDate
{
    brandAmbassadorVisitsArray=[[NSMutableArray alloc]init];
    brandAmbassadorVisitsArray=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchBrandAmbassadorVisitforSelectedDate:selectedDate];
    if(brandAmbassadorVisitsArray.count>0)
    {
        unfilteredVisitsArray=brandAmbassadorVisitsArray;
        [visitsTblView reloadData];
        [self SetFirstIndexVisitSelected];
    }
    else if (brandAmbassadorVisitsArray.count==0)
    {
        unfilteredVisitsArray=[[NSMutableArray alloc]init];
        [visitsTblView reloadData];
        [self resetLabels];

    }
    else
    {
        
    }
    
}
-(void)resetLabels
{
    locationLbl.text=KNotApplicable;
    plannedVisitTimeLbl.text=KNotApplicable;
    objectiveTxtView.text=KNotApplicable;
    
}

-(void)updatedLatVisitedOnLabel:(SalesWorxBrandAmbassadorVisit *)currentVisit
{
    NSMutableArray* visitEndDate=[MedRepQueries fetchVisitEndDateFromActualVisits:currentVisit.selectedLocation.Location_ID];
    if (visitEndDate.count>0) {
        NSString* endDateStr=[[visitEndDate objectAtIndex:0] valueForKey:@"Visit_End_Date"];
        NSString* refinedEndDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:endDateStr];
        lastVisitedOnLbl.text=refinedEndDate;
    }
    
    else
    {
        lastVisitedOnLbl.text=KNotApplicable;
    }
    
    [self refreshTaskandNotesStatus:currentVisit];
}
-(void)viewWillAppear:(BOOL)animated
{
    visitsTblView.rowHeight = UITableViewAutomaticDimension;
    visitsTblView.estimatedRowHeight = 70.0;


    if (self.isMovingToParentViewController) {
        [self fetchBrandAmbassadorVisitsforDate:[MedRepQueries fetchDatabaseDateFormat]];
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kBrandAmbassadorVisits];

    }
    
    
    if (self.isMovingToParentViewController) {
        
//        selectedBAVisit = selectedVisit;
        if ([brandAmbassadorVisitsArray count] >0 && selectedBAVisit != nil)
        {
            NSString *selectedPlannedVisitID;
            
            if (![NSString isEmpty:[SWDefaults getValidStringValue:selectedBAVisit.Planned_Visit_ID]]) {
                selectedPlannedVisitID = [SWDefaults getValidStringValue:selectedBAVisit.Planned_Visit_ID];
                NSArray *filtered = [brandAmbassadorVisitsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Planned_Visit_ID == '%@'",selectedPlannedVisitID]]];
                
                NSInteger indexObj = [brandAmbassadorVisitsArray indexOfObject:[filtered objectAtIndex:0]];
                NSLog(@"selected index %@", [brandAmbassadorVisitsArray objectAtIndex:indexObj]);
                
                if ([visitsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
                {
                    [visitsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]
                                               animated:YES
                                         scrollPosition:UITableViewScrollPositionNone];
                    
                    [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]];
                }
            }
        }
    }
    visitsTblView.allowsMultipleSelectionDuringEditing = NO;

}

-(void)UpdateScreentitle
{
    CGRect frame = CGRectMake(0, 0, 400, 44);
    label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];
    
    NSString * titleText = [[NSString alloc]init];
    
    
    if([previousFilteredParameters valueForKey:@"Date"]!=nil)
    {
        NSString* refinedCurrentDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[previousFilteredParameters valueForKey:@"Date"]];
        titleText = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(kBrandAmbassadorVisits, nil),refinedCurrentDate];
        if([[previousFilteredParameters valueForKey:@"Date"] isEqualToString:currentDateinDisplayFormat])
        {
            [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        }
        else
        {
            [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
        }
    }
    else
    {
        NSString* refinedCurrentDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[MedRepQueries fetchDatabaseDateFormat]];

        titleText = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(kBrandAmbassadorVisits, nil),refinedCurrentDate];
        [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    }
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:titleText];
    
}


-(void)startVisitTapped
{
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"Start Visit?", nil)
                                      message:NSLocalizedString(@"Would you like to start the visit?", nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KConfirmationAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        [self startVisit:YES];
                                    }];
        [alert addAction:yesAction];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KConfirmationAlertNOButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
        [alert addAction:noAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
}

-(void)startVisit:(BOOL)flag
{
    if (flag==YES) {
        [MedRepDefaults clearVisitStartTime];
        [MedRepDefaults setVisitStartTime:[self fetchVisitStartTime]];
        
        //clearing timers and doctor signatures if any
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"faceTimeTimer"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"waitTimer"];
        [MedRepDefaults clearDoctorSignature];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_Start_Date"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_End_Date"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Updated_Tasks"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Updated_Notes"];
        
        
        //as suggested by manish, all tasks should be based on doctor id for hospital and contact id for pharmacy
        //29/02/2016
       // selectedBAVisit.tasksArray=[[NSMutableArray alloc]init];
        //selectedBAVisit.notesArray=[[NSMutableArray alloc]init];
        
        //visitDetails.taskArray=currentVisitTasksArray;
        
        NSMutableArray * tempNotesArray=[[NSMutableArray alloc]init];

        tempNotesArray=[MedRepQueries fetchNoteObjectwithLocationID:selectedBAVisit.selectedLocation.Location_ID andDoctorID:@""];
        
        visitDetails.notesArray=tempNotesArray;
       

        
        //capture call start date and time
        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
        
        SalesWorxBrandAmbassadorMultipleVisitsViewController * startVisitVC=[[SalesWorxBrandAmbassadorMultipleVisitsViewController alloc]init];
       
        startVisitVC.currentVisit=selectedBAVisit;
        if (isFromDashboard) {
            startVisitVC.isFromDashboard = YES;
        }
        
        [self.navigationController pushViewController:startVisitVC animated:YES];
    }
}

-(NSString*)fetchVisitStartTime

{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    return formattedDateString;
}
-(void)plusButtonTapped:(id)sender

{
    [self.view endEditing:YES];

    
    SalesWorxBrandAmbassadorsCreateVisitViewController * viewController=[[SalesWorxBrandAmbassadorsCreateVisitViewController alloc]init];
    viewController.brandAmbassadorDelegate=self;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 400);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.barButtonItem = sender;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;

    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;

    }];
    
    

}
- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    return NO;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    
}



-(void)editVisitButtonTapped:(id)sender
{
    //[self.view endEditing:YES];
//    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
//    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
//    SalesWorxBrandAmbassadorsCreateVisitViewController * baCreateVisitVC=[[SalesWorxBrandAmbassadorsCreateVisitViewController alloc]init];
//   // medRepVisitsVC.createVisitNavigationController=self.navigationController;
//    baCreateVisitVC.baCreateVisit=selectedBAVisit;
//    baCreateVisitVC.brandAmbassadorDelegate=self;
//    baCreateVisitVC.isEditingVisit=YES;
//
//    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:baCreateVisitVC];
//    createVisitPopOverController=nil;
//    createVisitPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
//    
//    createVisitPopOverController.delegate=self;
//   // medRepVisitsVC.popOverController=createVisitPopOverController;
//    /** editing future visit*/
//   // medRepVisitsVC.selectedEditVisitDetails= selectedVisitDetailsDict;
//    //medRepVisitsVC.isUpdatingVisit=YES;
//    
//    [createVisitPopOverController setPopoverContentSize:CGSizeMake(345, 436) animated:YES];
//    [createVisitPopOverController presentPopoverFromBarButtonItem:editVisitButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
    
    SalesWorxBrandAmbassadorsCreateVisitViewController * baCreateVisitVC=[[SalesWorxBrandAmbassadorsCreateVisitViewController alloc]init];
    baCreateVisitVC.brandAmbassadorDelegate=self;
    baCreateVisitVC=[[SalesWorxBrandAmbassadorsCreateVisitViewController alloc]init];
    baCreateVisitVC.baCreateVisit=selectedBAVisit;
    baCreateVisitVC.brandAmbassadorDelegate=self;
    baCreateVisitVC.isEditingVisit=YES;

    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:baCreateVisitVC];
    baNavController.preferredContentSize=CGSizeMake(376, 400);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.barButtonItem = sender;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    [self disableNavigationBarOptionsOnPopOverPresentation];

    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
        
    }];
    

    
    
    
    
    
    
}

-(void)createVisitDidCancel
{
    if ([NSString isEmpty:filterParameterDate]==NO) {
        [self fetchBrandAmbassadorVisitsforDate:filterParameterDate];
    }
    [self EnableNavigationBarOptionsOnPopOverDismissal];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark UITableView Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    if (tableView==visitsTblView) {
        return UITableViewAutomaticDimension;
    }
    else
    {
        return 45.0f;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==visitsTblView) {
        if (brandAmbassadorVisitsArray.count>0) {
            return brandAmbassadorVisitsArray.count;
        }
        else if (brandAmbassadorVisitsArray.count==0)
        {
            [self AllowVisitEditing:NO];
            [self showNoVisitErrorMessageView];
            return 0;
        }
    }
    else
    {
        if (walkCustomersArray.count>0) {
            
            return walkCustomersArray.count;
        }
        else{
            return 0;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==VisitCallsTableView) {
        return 45.0f;
    }
    else{
        return 0.0f;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==VisitCallsTableView) {
    static NSString* identifier=@"walkinCell";
    SalesWorxBrandambassadorWalkCustomersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandambassadorWalkCustomersTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];

    cell.isHeader=YES;
        
        cell.customerNameLbl.isHeader=YES;
        cell.timeofMeetingLbl.isHeader=YES;
        cell.samplesLbl.isHeader=YES;
        cell.selloutLbl.isHeader=YES;
        cell.facetimeLbl.isHeader=YES;
        
        
    cell.customerNameLbl.text=@"Customer Name";
    cell.timeofMeetingLbl.text=@"Time of Meeting";
    cell.samplesLbl.text=@"Samples";
    cell.selloutLbl.text=@"Sellout";
    cell.facetimeLbl.text=@"Face Time";
    
    return cell.contentView;
    }
    else
    {
        return [[UIView alloc]initWithFrame:CGRectZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==visitsTblView) {
        
        SalesWorxBrandAmbassadorVisit * baVisit=[[SalesWorxBrandAmbassadorVisit alloc]init];
        baVisit=[brandAmbassadorVisitsArray objectAtIndex:indexPath.row];
        
       static NSString* identifier=@"updatedDesignCell";
    MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
        cell.cellStatusViewType=KCustomColorStatusView;
    
        
        if (isSearching && !NoVisitMessageView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([selectedVisitIndexPath isEqual:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
        
    
        NSString* visitStatusString=baVisit.Visit_Status;
        NSString* currentDateTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        NSDate* visitDate=[MedRepQueries convertNSStringtoNSDate:baVisit.Date];
        NSDate* currentDate=[MedRepQueries convertNSStringtoNSDate:currentDateTime];

        if ([visitStatusString isEqualToString:@"Y"])
        {
            [cell.statusView setBackgroundColor:KCompletedVisitColor];
            [cell setEditing:NO animated:NO];
        }
        else if ([visitDate compare:currentDate]==NSOrderedAscending)
        {
            [cell setEditing:YES animated:NO];
            [cell.statusView setBackgroundColor:KLateVisitColor];
        }
        else
        {
            [cell setEditing:YES animated:NO];
            [cell.statusView setBackgroundColor:KFutureVisitColor];
        }
        
        
    cell.titleLbl.text=[SWDefaults getValidStringValue:baVisit.selectedLocation.Location_Name];
    
        NSString* refinedVisitDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:baVisit.Date];
        
    cell.descLbl.text=[SWDefaults getValidStringValue:refinedVisitDate];
    
    return cell;
    }
    else
    {
        static NSString* identifier=@"walkinCell";
        SalesWorxBrandambassadorWalkCustomersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandambassadorWalkCustomersTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        SalesWorxBrandAmbassadorWalkinCustomerVisit* currentCustomer=[walkCustomersArray objectAtIndex:indexPath.row];
        cell.customerNameLbl.text=[MedRepDefaults getDefaultStringForEmptyString:currentCustomer.Customer_Name];
        NSString*refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"h:mm a" scrString:[SWDefaults getValidStringValue:currentCustomer.Start_Time]];
        cell.timeofMeetingLbl.text=[SWDefaults getValidStringValue:refinedDate];
        cell.samplesLbl.text=[MedRepDefaults getDefaultStringForEmptyString:currentCustomer.samplesGiven];
        cell.selloutLbl.text=[MedRepDefaults getDefaultStringForEmptyString:currentCustomer.sellOutGiven];
        
        cell.facetimeLbl.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults timeBetweenSatrtTime:[MedRepDefaults getValidStringValue:currentCustomer.Start_Time] andEndTime:[MedRepDefaults getValidStringValue:currentCustomer.End_Time]]];
        
        

        return cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==visitsTblView) {
        NSLog(@"did select called");
    
    [self hideNoVisitErrorMessageView];
    selectedBAVisit= [brandAmbassadorVisitsArray objectAtIndex:indexPath.row];
    
    selectedBAVisit.tasksArray=[MedRepQueries fetchTaskObjectsforLocationID:selectedBAVisit.selectedLocation.Location_ID andDoctorID:@""];
    
    
    selectedBAVisit.notesArray=[MedRepQueries fetchNotesObjectswithLocationID:selectedBAVisit.selectedLocation.Location_ID andContactID:@""];
    
    
    [self updatedLatVisitedOnLabel:selectedBAVisit];
    [self populateVisitData:[brandAmbassadorVisitsArray objectAtIndex:indexPath.row]];
    
    
    NSString * visitStatusString=selectedBAVisit.Visit_Status;
    NSLog(@"current visit status is %@", selectedBAVisit.Visit_Status);


    NSDate* visitDate=[MedRepQueries convertNSStringtoNSDate:selectedBAVisit.Date];
   // NSDate* currentDate=[MedRepQueries convertNSStringtoNSDate:currentDateTime];

    
    NSString* currentDateTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    
    // NSLog(@"visit date and current date is %@ %@", visitDateStr,currentDateTime);
    
    
    NSDate* currentDate=[MedRepQueries convertNSStringtoNSDate:currentDateTime];
    
    
    
    if (brandAmbassadorVisitsArray.count>0)
    {
        if ([visitStatusString isEqualToString:@"Y"]) {
            [visitStatusImageView setBackgroundColor:KCompletedVisitColor];
        }
        else if ([visitDate compare:currentDate]==NSOrderedAscending)
        {
            [visitStatusImageView setBackgroundColor:KLateVisitColor];
        }
        else
        {
            [visitStatusImageView setBackgroundColor:KFutureVisitColor];
        }
    }
    else
    {
        [visitStatusImageView setBackgroundColor:KNoVisitColor];
    }

    
    
    NSDateComponents *visitDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:visitDate];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    
    if ([visitStatusString isEqualToString:@"Y"]) {
        [startVisitButton setEnabled:NO];
        [self AllowVisitEditing:NO];
    }
    else if([today day] == [visitDay day] &&
            [today month] == [visitDay month] &&
            [today year] == [visitDay year]) {
        [startVisitButton setEnabled:YES];
        [self AllowVisitEditing:NO];
    }
    else if(([today year] < [visitDay year]) || (([today year] <= [visitDay year]) && ([today month] < [visitDay month])) || (([today year] <= [visitDay year]) && ([today month] <= [visitDay month]) && ([today day] < [visitDay day])))
    {
        [startVisitButton setEnabled:NO];
        [self AllowVisitEditing:YES];
    }
    else
    {
        [startVisitButton setEnabled:NO];
        [self AllowVisitEditing:NO];
    }
    
    if (indexPathArray.count==0) {
        [indexPathArray addObject:indexPath];

    }
    else
    {
        indexPathArray=[[NSMutableArray alloc]init];
        [indexPathArray addObject:indexPath];
    //[indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
    }
    
    selectedVisitIndexPath=indexPath;
    [visitsTblView reloadData];

    }
}

-(void)SetFirstIndexVisitSelected
{
    selectedVisitIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [self deselectPreviousCellandSelectFirstCell];
}





-(void)deselectPreviousCellandSelectFirstCell
{
    if (brandAmbassadorVisitsArray.count>0 && [visitsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[visitsTblView cellForRowAtIndexPath:selectedVisitIndexPath];
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            cell.cellDividerImg.hidden=NO;
        }
        [visitsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                   animated:YES
                             scrollPosition:UITableViewScrollPositionNone];
        [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}


-(void)populateVisitData:(SalesWorxBrandAmbassadorVisit*)currentVisit
{
    locationLbl.text=[MedRepDefaults getDefaultStringForEmptyString:currentVisit.selectedLocation.Location_Name];
    objectiveTxtView.text=[MedRepDefaults getDefaultStringForEmptyString:currentVisit.Objective];
    
    NSString* visitTime=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:currentVisit.Date];

    plannedVisitTimeLbl.text=[MedRepDefaults getDefaultStringForEmptyString:visitTime];
    
    walkCustomersArray=[self fetchWalkInCustomers:currentVisit];
    if (walkCustomersArray.count>0) {
        [VisitCallsTableView reloadData];
    }
    else{
        walkCustomersArray=[[NSMutableArray alloc]init];
        [VisitCallsTableView reloadData];

    }
    
}

#pragma mark Visit Editing

-(void)AllowVisitEditing:(BOOL)allowEdit
{
    if(allowEdit)
    {
        [editVisitButton setEnabled:YES];
    }
    else
    {
        [editVisitButton setEnabled:NO];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==visitsTblView)
    {
        NSString* visitDateStr=[NSString stringWithFormat:@"%@",selectedBAVisit.Date];
        
      NSString*  visitStatusString=selectedBAVisit.Visit_Status;

        
        NSDate* visitDate=[MedRepQueries convertNSStringtoNSDate:visitDateStr];
        
        NSDateComponents *visitDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:visitDate];
        NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
        
        
        
        if ([visitStatusString isEqualToString:@"Y"]) {
            return NO;
        }
        else if ([today day] == [visitDay day])
        {
            return YES;
        }
        else
        {
            return NO;
        }
        
        return YES;
        
    }
    else
    {
        return NO;
    }
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                        {
                                            //insert your editAction here
                                            @try {
                                                [self editTableRow:indexPath];
                                            }
                                            @catch (NSException *exception) {
                                                NSLog(@"%@",exception);
                                            }
                                            
                                        }];
    editAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              //insert your deleteAction here
                                              
                                              @try {
                                                  [brandAmbassadorVisitsArray removeObjectAtIndex:indexPath.row];
                                                  
                                                  
                                                  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                                  
                                                  NSMutableDictionary * tempDict=[[NSMutableDictionary alloc]init];
                                                  [tempDict setValue:selectedBAVisit.Planned_Visit_ID forKey:@"Planned_Visit_ID"];
                                                  
                                                  BOOL success = [MedRepQueries deleteFutureVisit:tempDict];
                                                  if (success == YES)
                                                  {
                                                      [self fetchBrandAmbassadorVisitsforDate:[MedRepQueries fetchDatabaseDateFormat]];
                                                  }
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"%@",exception);
                                              }
                                          }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction,editAction];
}
-(void)editTableRow:(NSIndexPath *)indexPath
{
    
    
    
    NSMutableDictionary *dic;
    
    dic = [filteredPlannedVisitsArray objectAtIndex:indexPath.row];
    
    CGRect rectOfCellInTableView = [visitsTblView rectForRowAtIndexPath:indexPath];
    // CGRect rectOfCellInSuperview = [visitsTblView convertRect:rectOfCellInTableView toView:[visitsTblView superview]];
    CGRect rectOfCellInSuperview = [visitsTblView convertRect:rectOfCellInTableView toView:self.view];
    
    //blur the view here
    
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    
    [self.view addSubview:blurredBgImage];

   
    //
    
    MedRepRescheduleViewController * viewController=[[MedRepRescheduleViewController alloc]init];
    viewController.isBrandAmbassadorVisit=YES;
    viewController.dismissDelegate=self;
    viewController.brandAmbassadorVisit=selectedBAVisit;
    viewController.popOverController = createVisitPopOverController;
    viewController.visitDetailsDict = dic;

    
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 270);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = self.view;
    presentationController.sourceRect =rectOfCellInSuperview;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;

    

    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;

    }];
    
    
    
       
}
-(void)closeReschedulePopover
{
    [blurredBgImage removeFromSuperview];
    
    [self fetchAllPlannedVisits];
    
    
    [self fetchBrandAmbassadorVisitsforDate:[MedRepQueries fetchDatabaseDateFormat]];
   // [self searchContent];
    NSLog(@"selected visit indexpath in reschedule is %ld", (long)selectedVisitIndexPath.row);
    [self EnableNavigationBarOptionsOnPopOverDismissal];

    [self tableView:visitsTblView didSelectRowAtIndexPath:selectedVisitIndexPath];

}

#pragma mark popUpViewMethods
-(void)displayMapViewPopUpView
{
    MedRepVisitsLocationMapPopUpViewController *presentingView=   [[MedRepVisitsLocationMapPopUpViewController alloc]init];
    presentingView.isBrandAmbassadorVisit=YES;
    presentingView.currentBAVisit=selectedBAVisit;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    mapPopUpViewController.navigationBarHidden=YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}
-(void)displayContactPopUpView
{
    MedRepVisitContactDetailsPopUpViewController *presentingView=   [[MedRepVisitContactDetailsPopUpViewController alloc]init];
    presentingView.isObjectData=YES;
    presentingView.selectedLocation=selectedBAVisit.selectedLocation;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    mapPopUpViewController.navigationBarHidden=YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)contactButtonTapped:(id)sender {
    [self displayContactPopUpView];
}

- (IBAction)locationButtonTapped:(id)sender {
    [self displayMapViewPopUpView];
}

#pragma mark Search Methods



-(void)brandAmbassadorVisitCreated:(BOOL)status
{
   
    
    
    
    
    if (status==YES) {
        
        
        [self fetchAllPlannedVisits];
        
        [self EnableNavigationBarOptionsOnPopOverDismissal];

        NSString* refinedCurrentDate;
        //check if visit is editing in case of future visit show future visits not current visits

        if ([NSString isEmpty:filterParameterDate]==NO) {
            [self fetchBrandAmbassadorVisitsforDate:filterParameterDate];
            refinedCurrentDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:filterParameterDate];
        }
        else{
            [self fetchBrandAmbassadorVisitsforDate:[MedRepQueries fetchDatabaseDateFormat]];
            refinedCurrentDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[MedRepQueries fetchDatabaseDateFormat]];
        }
        
        
        
        label.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(kBrandAmbassadorVisits, nil),refinedCurrentDate];
        
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(kBrandAmbassadorVisits, nil),refinedCurrentDate]];
        
        
        
//        brandAmbassadorVisitsArray=[[NSMutableArray alloc]init];
//        brandAmbassadorVisitsArray=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchBrandAmbassadorVisitforSelectedDate:[MedRepQueries fetchDatabaseDateFormat]];
//        unfilteredVisitsArray=brandAmbassadorVisitsArray;
//        [visitsTblView reloadData];

    }
    /*
    if (brandAmbassadorVisitsArray.count==0) {
        brandAmbassadorVisitsArray=[[NSMutableArray alloc]init];
        [brandAmbassadorVisitsArray addObject:brandAmbassadorVisit];
        [visitsTblView reloadData];
        
        
    }
    else if (brandAmbassadorVisitsArray.count>0)
    {
        [brandAmbassadorVisitsArray addObject:brandAmbassadorVisit];
        [visitsTblView reloadData];
        
    }
    
    unfilteredVisitsArray=brandAmbassadorVisitsArray;*/

    
}



#pragma mark UISearch Bar Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBarVisit setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
        
    } else {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if([searchText length] != 0) {
        
        [self showNoVisitErrorMessageView];
        
        isSearching = YES;
        [self searchContent:searchText];
    }
    else {
        isSearching = NO;
        [self hideNoVisitErrorMessageView];
        
        [self searchContent:searchText];
        [self deselectPreviousCellandSelectFirstCell];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBarVisit setText:@""];
    [searchBarVisit setShowsCancelButton:NO animated:YES];
    [self.view endEditing:YES];
    
    isSearching=NO;
    [self hideNoVisitErrorMessageView];
    
    if (isFiltered) {
        
        brandAmbassadorVisitsArray= filteredBrandAmbassadorVisitsArray;
    }
    else{
        brandAmbassadorVisitsArray=unfilteredVisitsArray;
    }
    
    [visitsTblView reloadData];
    [self SetFirstIndexVisitSelected];

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    isSearching=YES;
}

-(void)searchContent:(NSString*)searchString
{
    if ([NSString isEmpty:searchString]==NO) {
      
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"selectedLocation.Location_Name CONTAINS[cd] %@", searchString];
        NSMutableArray * filteredContentArray;
        if (isFiltered) {

            filteredContentArray=[[filteredBrandAmbassadorVisitsArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        }
        else{
            filteredContentArray=[[unfilteredVisitsArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        }
        if (filteredContentArray.count>0) {
            brandAmbassadorVisitsArray=filteredContentArray;
            [visitsTblView reloadData];
//            [self populateVisitData:[brandAmbassadorVisitsArray objectAtIndex:0]];
//            [self SetFirstIndexVisitSelected];
        }
        else if (filteredContentArray.count==0)
        {
            brandAmbassadorVisitsArray=filteredContentArray;
            [visitsTblView reloadData];
//            [self SetFirstIndexVisitSelected];
        }
    }
    else
    {
        brandAmbassadorVisitsArray=unfilteredVisitsArray;
        [visitsTblView reloadData];
        [self SetFirstIndexVisitSelected];
    }
}



#pragma mark Task and Notes methods

- (IBAction)taskButtontapped:(id)sender
{
    
    currentVisitTasksArray=[MedRepQueries fetchTaskObjectswithLocationID:selectedBAVisit.selectedLocation.Location_ID andContactID:@""];
    if (currentVisitTasksArray.count>0) {
        selectedBAVisit.tasksArray=currentVisitTasksArray;

    }
    
    SalesWorxBrandAmbassadorsTaskListViewController * viewController=[[SalesWorxBrandAmbassadorsTaskListViewController alloc]init];
    viewController.tasksListDelegate=self;
    viewController.isViewing=YES;
    viewController.currentVisit=selectedBAVisit;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 570);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = tasksButton;
    presentationController.sourceRect =tasksButton.bounds;
    
    
    if ([SWDefaults isRTL]) {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
    }
    else
    {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionRight;
    }

    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
  
    }];
}

-(void)salesWorxBrandAmbassadorsTasksList:(NSMutableArray*)tasksList
{
    if (tasksList.count>0) {
        selectedBAVisit.tasksArray=tasksList;

    }
    [self refreshTaskandNotesStatus:selectedBAVisit];
}

-(void)refreshTaskandNotesStatus:(SalesWorxBrandAmbassadorVisit*)currentVisitDetails
{
    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodesForBrandAmbassadorTasksandNotes:currentVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }
    
}


- (IBAction)notesButtonTapped:(id)sender
{
    SalesWorxBrandAmbassadorNotesListViewController * viewController=[[SalesWorxBrandAmbassadorNotesListViewController alloc]init];
    viewController.currentVisit=selectedBAVisit;
    viewController.isViewing=YES;
    viewController.notesListDelegate=self;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 350);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = notesButton;
    presentationController.sourceRect =notesButton.bounds;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
  
    }];
}
-(void)salesWorxBrandAmbassadorsNotesList:(NSMutableArray*)notesList
{
    if (notesList.count>0) {
        selectedBAVisit.notesArray=notesList;
        
    }
    [self refreshTaskandNotesStatus:selectedBAVisit];
}


#pragma mark Filter Method

- (IBAction)visitsFilterButtontapped:(id)sender
{
    
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    [self.view addSubview:blurredBgImage];
    SalesWorxBrandAmbassadorsVisitsFilterViewController * viewController=[[SalesWorxBrandAmbassadorsVisitsFilterViewController alloc]init];
    
    
    NSLog(@"content array being passed to visits filter %@",[allBrandAmbassadorsArrayforFilter  valueForKey:@"Date"]);

    
    viewController.contentArray=allBrandAmbassadorsArrayforFilter;
    viewController.brandAmbassadorFilterDelegate=self;
    viewController.previousFilterParametersDict=previousFilteredParameters;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 239);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = visitsFilterbtn;
    presentationController.sourceRect =visitsFilterbtn.bounds;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;

    }];
}

-(void)filteredBrandAmbassadorVisits:(NSMutableArray*)filteredArray
{
    isFiltered=YES;
    [blurredBgImage removeFromSuperview];
    
    //this is for search
    filteredBrandAmbassadorVisitsArray=filteredArray;
    
    brandAmbassadorVisitsArray=filteredArray;
    [visitsTblView reloadData];
    [self SetFirstIndexVisitSelected];
    [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];

}

-(void)filteredBrandAmbassadorDidClose
{
    [blurredBgImage removeFromSuperview];

}

-(void)filteredBrandAmbassadorDidReset
{
    [blurredBgImage removeFromSuperview];
    isFiltered=NO;
    [self fetchBrandAmbassadorVisitsforDate:[MedRepQueries fetchDatabaseDateFormat]];
    [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];

    
}
-(void)filteredParameters:(NSMutableDictionary*)parameters
{
    previousFilteredParameters=parameters;
    
    NSLog(@"filter parameters are %@", previousFilteredParameters);
    
    filterParameterDate=[previousFilteredParameters valueForKey:@"Date"];
    
    [self UpdateScreentitle];
}


#pragma mark WalkIn Customers Data
-(NSMutableArray*)fetchWalkInCustomers:(SalesWorxBrandAmbassadorVisit*)selectedVisit
{
    walkCustomersArray=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchWalkInCustomers:selectedVisit];
    if (walkCustomersArray.count>0) {
        
        return walkCustomersArray;
    }
    else{
        return nil;
    }
}
@end
