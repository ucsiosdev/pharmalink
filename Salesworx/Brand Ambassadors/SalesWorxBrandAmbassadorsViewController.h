//
//  SalesWorxBrandAmbassadorsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import <MapKit/MapKit.h>
#import "MedRepVisitsViewController.h"
#import "MedRepQueries.h"
#import "MedRepMenuTableViewCell.h"
#import <MedRepUpdatedDesignCell.h>
#import "MedRepVisitsFilterViewController.h"
#import "MedRepCustomClass.h"
#import "NSString+Additions.h"
#import "MedRepVisitsStartMultipleCallsViewController.h"
#import "SalesWorxTableView.h"
#import "MedRepPlannedVisitsMultipleCallsTableViewCell.h"
#import "SalesWorxTableViewHeaderView.h"
#import "MedRepDefaults.h"
#import "MedRepVisitsLocationMapPopUpViewController.h"
#import "MedRepVisitContactDetailsPopUpViewController.h"
#import "SalesWorxImageView.h"
#import "MedRepPanelTitle.h"
#import "SalesWorxDefaultLabel.h"
#import "SalesWorxBrandAmbassadorsCreateVisitViewController.h"
#import "SalesWorxBrandAmbassadorsVisitsFilterViewController.h"
#import "MedRepRescheduleViewController.h"
#import "SalesWorxBrandAmbassadorsTaskListViewController.h"
#import "SalesWorxBrandAmbassadorNotesListViewController.h"
#import "SalesWorxBrandAmbassadorMultipleVisitsViewController.h"
#import "SalesWorxBrandambassadorWalkCustomersTableViewCell.h"
#import "SalesWorxImageBarButtonItem.h"

@interface SalesWorxBrandAmbassadorsViewController : UIViewController<UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate,UITableViewDelegate,UITableViewDataSource,BrandAmbassadorVisitFilterDelegate>


{
    UIPopoverController * createVisitPopOverController,* rescheduleVisitPopOverController,*calenderPopOverController;
    SalesWorxImageBarButtonItem* createVisitButton;
    SalesWorxImageBarButtonItem* editVisitButton;
    
    UILabel *label;
    
    BOOL isFilteredContent;
    
    IBOutlet SalesWorxImageView *visitStatusImageView;
    IBOutlet MedRepPanelTitle *noVisitSelectedLbl;
    NSMutableArray* plannedVisitsArray;
    NSMutableArray* filteredPlannedVisitsArray;
    NSMutableArray* visitDetailsArray;
    
    UIDatePicker* datepicker;
    IBOutlet UIView *tableHeaderView;
    
    IBOutlet UIButton *visitStatusBtn;
    UIDatePicker* visitDatePicker;
    IBOutlet SalesWorxImageView *notesStatusImageView;
    
    IBOutlet SalesWorxImageView *taskStatusImageView;
    
    NSString* visitStatus;
    
    SalesWorxImageBarButtonItem* startVisitButton;
    
    NSString* doctorNameStr;
    
    NSInteger selectedDateValue
    ;
    UISearchBar *customSearchBar;
    
    CLLocationCoordinate2D  coord;
    BOOL visitCompleted;
    
    BOOL isSearching;
    
    BOOL calenderUsedtopickDate;
    
    NSString* futureVisitDate;
    
    VisitDetails * visitDetails;
    
    NSMutableArray * currentVisitTasksArray;
    
    IBOutlet UIButton *visitsFilterbtn;
    UIPopoverController* popOverController,*filterPopOverController;
    StringPopOverViewController_ReturnRow* locationPopOver;
    
    IBOutlet UIImageView *visitTypeImageView;
    IBOutlet UIButton *notesButton;
    
    IBOutlet UIButton *tasksButton;
    NSIndexPath *selectedVisitIndexPath;
    
    
    IBOutlet MedRepElementDescriptionLabel *demoPlanDescLbl;
    IBOutlet SalesWorxDropShadowView *tableContainerView;
    
    
    IBOutlet UIButton *pdfFileButton;
    IBOutlet UIButton *imageFileButton;
    IBOutlet UIButton *vedioFileButton;
    NSIndexPath *reschudeleSelectedIndexpath;
    SWAppDelegate * appDel;
    IBOutlet UIImageView *LocationTypeImageView;
    IBOutlet SalesWorxDefaultLabel *LocationTypeLabel;
    
    NSMutableDictionary* previousFilteredParameters;
    
    NSMutableDictionary*   currentVisitDict;
    
    NSMutableArray * filteredBrandAmbassadorVisitsArray;
    
    
    NSMutableArray* productPDFFilesArray;
    NSMutableArray* productImageFilesArray;
    
    NSMutableArray* productVideoFilesArray;
    
    NSMutableDictionary* selectedVisitDetailsDict;
    
    
    NSInteger selectedIndexforDemoPlan;
    
    BOOL cellSelected;
    
    IBOutlet UIButton *taskStatusButton;
    IBOutlet UIButton *notesStatusButton;
    
    BOOL isFiltered;
    
    NSMutableArray* unfilteredVisitsArray;
    
    BOOL visitFinished;
    
    BOOL isFirstAppearance;
    
    
    NSInteger imagesCount;
    NSInteger VideosCount;
    NSInteger brochuresCount;
    
    NSMutableArray * indexPathArray;
    
    IBOutlet SalesWorxTableView *VisitCallsTableView;
    IBOutlet SalesWorxTableViewHeaderView *VisitCallsTableHeaderView;
    IBOutlet UIImageView *NotesImageView;
    
    IBOutlet UIImageView *TaksImageView;
    IBOutlet UIView *notesandTaksButtonContainerView;
    IBOutlet UIView *NoVisitMessageView;
    
    IBOutlet SalesWorxDefaultLabel *LocaionMapLabel;
    IBOutlet UIButton *LocationContactButton;
    IBOutlet UIButton *LocationMapButton;
    NSMutableArray *selectedVisitCallsArray;
    
    NSMutableArray * brandAmbassadorVisitsArray, * unfilteredBrandAmbassadorVisitsArray;
    
    NSMutableArray * customersArray, *unfilteredCustomersArray;
    IBOutlet UIButton *contactButton;
    IBOutlet UIButton *locationButton;
    
    NSMutableDictionary * filterParametersDict;
    
    SalesWorxBrandAmbassadorVisit * selectedBAVisit;
    
    NSMutableArray* allLocationsArray;
    
    NSMutableArray* allBrandAmbassadorsArrayforFilter;
    
    
    NSDate * testDate;
    
    NSMutableArray * walkCustomersArray;
    NSString* filterParameterDate;
    
}
- (IBAction)contactButtonTapped:(id)sender;
- (IBAction)locationButtonTapped:(id)sender;
- (IBAction)visitsFilterButtontapped:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *visitEndDateLbl;
@property (strong, nonatomic) IBOutlet UILabel *visitEndDateValueLbl;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBarVisit;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;

@property (strong, nonatomic) IBOutlet UILabel *doctorLbl;

@property (weak, nonatomic) IBOutlet UILabel *plannedVisitTimeLbl;

@property (strong, nonatomic) IBOutlet UILabel *demonstrationPlanLbl;

@property (strong, nonatomic) IBOutlet UILabel *visitTypeLbl;

@property (strong, nonatomic) IBOutlet UITextView *objectiveTxtView;

@property (strong, nonatomic) IBOutlet UILabel *classLbl;

@property (strong, nonatomic) IBOutlet UILabel *tradeChannelLbl;

@property (strong, nonatomic) IBOutlet UILabel *idLbl;

@property (strong, nonatomic) IBOutlet UILabel *otcRxLbl;

@property (strong, nonatomic) IBOutlet UITextView *addressTxtView;

@property (strong, nonatomic) IBOutlet UILabel *emirateLbl;

@property (strong, nonatomic) IBOutlet MKMapView *visitLocationMapView;

@property (strong, nonatomic) IBOutlet UILabel *contactNumberLbl;

@property (strong, nonatomic) IBOutlet UILabel *emailLbl;

@property (strong, nonatomic) IBOutlet UILabel *lastVisitedOnLbl;

@property (strong, nonatomic) IBOutlet UILabel *nextPlannedVisitLbl;

@property (strong, nonatomic) IBOutlet UITableView *visitsTblView;

@property(strong,nonatomic) NSMutableDictionary * startedVisitDict;


@property(strong,nonatomic) NSString* startedVisitIndexStr;

@property(nonatomic) NSInteger startedVisitIndex;

@property(strong,nonatomic) NSMutableDictionary* currentVisitDetailsDict;

- (IBAction)taskButtontapped:(id)sender;

- (IBAction)notesButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *tblBgView;

@property (strong, nonatomic) IBOutlet UILabel *visitDateLbl;


@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit *selectedVisit;
@property(nonatomic) BOOL isFromDashboard;

- (void)showVisitforSelectedDate:(id)sender;

- (IBAction)calenderBtnTapped:(id)sender;


-(void)loadVisitDetailswithIndex:(NSInteger)arrayIndex;


@property(nonatomic,retain)UIPopoverPresentationController *dateTimePopover8;


@property UIView *blurMask;
@property UIImageView *blurredBgImage;


//-(void)fetchProductMediaFilesforDemoPlanID:(NSString*)demoPlanID;

@end
