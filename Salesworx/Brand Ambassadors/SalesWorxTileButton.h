//
//  SalesWorxTileButton.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"
@interface SalesWorxTileButton : UIButton

@end
