//
//  SalesWorxWebViewViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxWebViewViewController.h"

@interface SalesWorxWebViewViewController ()

@end

@implementation SalesWorxWebViewViewController
@synthesize salesWorxWebView,sourceUrl;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *targetURL = [NSURL fileURLWithPath:sourceUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [salesWorxWebView loadRequest:request];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)emailButtonTapped:(id)sender {
}
@end
