//
//  SalesWorxBrandAmbassadorNotesListViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxDefaultTableViewCell.h"
#import "SalesWorxBrandAmbassadorCreateNotesViewController.h"

@protocol SalesWorxBrandAmbassadorNotesListDelegate <NSObject>

-(void)salesWorxBrandAmbassadorsNotesList:(NSMutableArray*)notesList;

@end
@interface SalesWorxBrandAmbassadorNotesListViewController : UIViewController

{
    NSIndexPath *selectedNoteIndexPath;
    id notesListDelegate;

}
@property(nonatomic) BOOL isViewing;
@property (strong, nonatomic) IBOutlet UITableView *notesListTableView;
@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * currentVisit;
@property(nonatomic)     id notesListDelegate;



@end
