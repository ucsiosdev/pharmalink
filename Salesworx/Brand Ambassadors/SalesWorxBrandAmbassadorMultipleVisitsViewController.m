//
//  SalesWorxBrandAmbassadorMultipleVisitsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorMultipleVisitsViewController.h"
#import "SalesWorxImageBarButtonItem.h"
#import "SalesworxDocumentDisplayManager.h"
@interface SalesWorxBrandAmbassadorMultipleVisitsViewController ()
{
    SalesworxDocumentDisplayManager * docDispalayManager;

}
@end

@implementation SalesWorxBrandAmbassadorMultipleVisitsViewController
@synthesize currentVisit,receivedVisit, isFromDashboard;

- (void)viewDidLoad {
    [super viewDidLoad];
    docDispalayManager=[[SalesworxDocumentDisplayManager alloc] init];
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Start Visit"];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
        [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
   
    receivedVisit=currentVisit;
    


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(walkInCustomerVisitDidFinish:)
                                                 name:@"WalkInCustomerVisitDidFinish"
                                               object:nil];
    
    tasksButton.layer.borderWidth=1.0f;
    notesButton.layer.borderWidth=1.0f;
    tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    
    
    
    notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    notesButton.layer.shadowOffset = CGSizeMake(2, 2);
    notesButton.layer.shadowOpacity = 0.1;
    notesButton.layer.shadowRadius = 1.0;
    
    
    
    tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
    tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
    tasksButton.layer.shadowOpacity = 0.1;
    tasksButton.layer.shadowRadius = 1.0;
    
    UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close Visit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeVisitTapped)];
    [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=closeVisitButton;

    // Do any additional setup after loading the view from its nib.
    
    SalesWorxImageBarButtonItem* startVisitBarButtonItem=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartCall"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitBarButtonTapped)];
    
    self.navigationItem.rightBarButtonItem=startVisitBarButtonItem;
    
    
}

-(void)walkInCustomerVisitDidFinish:(NSNotification*)visitFinishNotification
{
    NSLog(@"walkin did finish notification called");
    
    currentVisit=visitFinishNotification.object;
    NSLog(@"current visit walks customers array is %@", currentVisit.walkCustomerVisitsArray);
    
    currentVisit.selectedLocation=receivedVisit.selectedLocation;
    currentVisit.tasksArray=receivedVisit.tasksArray;
    currentVisit.notesArray=receivedVisit.notesArray;
    
    currentVisit.surveyResponses=receivedVisit.surveyResponses;
    
    
    timerDate=[NSDate date];
    waitTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
    currentVisit.currentCallStartTime=[self fetchCallStartDate];
    currentVisit.Call_Start_Date=[self setCallStartDate];
    currentVisit.samplesGivenArray=[[NSMutableArray alloc]init];
    currentVisit.demoedArray=[[NSMutableArray alloc]init];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [self fetchMediaFilesData];
    });
    
    [self refreshTaskandNotesStatus:currentVisit];

    

}

-(NSString*)fetchCallStartDate
{
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"hh:mm a"];
    [formatterTime setLocale:usLocaleq];
    NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
    return callStartDate;

}

-(void)startVisitBarButtonTapped
{
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
        
        currentVisit.Call_Start_Date=[self setCallStartDate];
        [[NSUserDefaults standardUserDefaults]setObject:[self setCallStartDate] forKey:@"Call_Start_Date"];
        
        //stop wait timer and update value
        
        [[NSUserDefaults standardUserDefaults]setObject:waitTimeLbl.text forKey:@"waitTimer"];
        
        NSLog(@"WAIT TIME WHILE MOVING TO NEXT SCREEN %@", [[NSUserDefaults standardUserDefaults]objectForKey:@"waitTimer"]);
        
        currentVisit.waitTime=waitTimeLbl.text;
        
        [waitTimer invalidate];
        waitTimer=nil;
        
        
    currentVisit.walkinCustomer=[[SalesWorxBrandAmbassadorWalkinCustomerVisit alloc]init];
    currentVisit.walkinCustomer.Walkin_Session_ID=[NSString createGuid];
    SalesWorxBrandAmbassadorEdetailingStartCallViewController * startEdetailingVC=[[SalesWorxBrandAmbassadorEdetailingStartCallViewController alloc]init];
    startEdetailingVC.currentVisit=currentVisit;
        [self fetchMediaFilesData];
        if (isFromDashboard) {
            startEdetailingVC.isFromDashboard = YES;
        }
    startEdetailingVC.productMediaFilesArray=productMediaFilesArray;
    [self.navigationController pushViewController:startEdetailingVC animated:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kBrandAmbassadorStartVisit];
        waitTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];

    }
    NSLog(@"recived visit location id is %@",receivedVisit.selectedLocation.Location_ID);

    waitTimeLbl.textColor = KWaitTimeColor;
    waitTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    
    
    if (self.isMovingToParentViewController) {
    
        [[NSUserDefaults standardUserDefaults]setObject:[self setCallStartDate] forKey:@"Call_Start_Date"];
    
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"hh:mm a"];
        [formatterTime setLocale:usLocaleq];
        NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
        NSLog(@"call start time is %@", callStartDate);
        currentVisit.currentCallStartTime=callStartDate;
        currentVisit.Call_Start_Date=[self setCallStartDate];
        


  
    locationNameLbl.text=[MedRepDefaults getDefaultStringForEmptyString:currentVisit.selectedLocation.Location_Name];
        
    objectiveTextView.text=[MedRepDefaults getDefaultStringForEmptyString:currentVisit.Objective];
    demoPlanTextFied.text=[MedRepDefaults getDefaultStringForEmptyString:currentVisit.selectedDemoPlan.Description];
    timeOfArrivalLbl.text= [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[SWDefaults getValidStringValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]];
    timerDate=[NSDate date];
        
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [self fetchMediaFilesData];
        
        demoPlanObjectsArray=[MedRepQueries fetchDemoPlanObjects];

        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (productMediaFilesArray.count>0) {
                
                productImagesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
                NSLog(@"product images array in did load %@", productImagesArray);
                
                productVideoFilesArray= [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
                
                productPdfFilesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
                
                presentationsArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
                
                
                mediaContentArray=[[NSMutableArray alloc]init];
                [mediaContentArray addObject:productImagesArray];
                [mediaContentArray addObject:productVideoFilesArray];
                [mediaContentArray addObject:productPdfFilesArray];
                [mediaContentArray addObject:presentationsArray];
                [demoPlanMediaCollectionView reloadData];
                [mediaSwipeView reloadData];
                
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });

    
    
    [[NSUserDefaults standardUserDefaults]setObject:[self setCallStartDate] forKey:@"Call_Start_Date"];
    
    NSLog(@"setting call start date %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"Call_Start_Date"]);
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_End_Date"];
    
        segmentMenuArray=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"IMAGE", nil),NSLocalizedString(@"VIDEO", nil),NSLocalizedString(@"PDF", nil),NSLocalizedString(@"PRESENTATION", nil), nil];

    segmentControl.sectionTitles = segmentMenuArray;
    //    segmentControl.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    segmentControl.backgroundColor = [UIColor clearColor];
    
        segmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
        segmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    
    segmentControl.selectionIndicatorBoxOpacity=0.0f;
    segmentControl.verticalDividerEnabled=YES;
    segmentControl.verticalDividerWidth=1.0f;
    segmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    segmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    segmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentControl.tag = 3;
    segmentControl.selectedSegmentIndex = 0;
    
    
    
    
    segmentContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    segmentContainerView.layer.shadowOffset = CGSizeMake(0, -2);
    segmentContainerView.layer.shadowOpacity = 0.1;
    segmentContainerView.layer.shadowRadius = 2.0;
    
    
    [segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
        NSString* createActualVisit=[[SalesWorxBrandAmbassadorQueries retrieveManager] createBrandAmbassadorActualVisit:currentVisit];
        
        if ([NSString isEmpty:createActualVisit]==NO) {
            NSLog(@"New actual visit id created for brand ambassadors visit in multiple calls");
            currentVisit.Actual_Visit_ID=createActualVisit;
            [SWDefaults setCurrentVisitID:currentVisit.Actual_Visit_ID];
        }
     

        [self refreshTaskandNotesStatus:receivedVisit];
        
        
       

    }
}


-(void)closeVisitTapped
{
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self displayEndVisit];
                               }];
    UIAlertAction* CancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];

    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Close Visit" andMessage:@"Would you like to close this visit?" andActions:actionsArray withController:self];
}
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

-(void)fetchMediaFilesData
{
    NSString* demoPlanID=currentVisit.selectedDemoPlan.Demo_Plan_ID;
    if (demoPlanID!=nil)
    {
        productMediaFilesArray=[MedRepQueries fetchMediaFileswithDemoPlanID:demoPlanID];
        
    }
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

-(void)startTimer{
    NSDate * currentTimer=timerDate;
    NSDate *now = [NSDate date];
    NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
    // NSLog(@"wait time counter ticking %@",[self stringFromTimeInterval:elapsedTimeDisplayingPoints]);
    waitTimeLbl.text=[self stringFromTimeInterval:elapsedTimeDisplayingPoints];
}

-(NSString*)setCallStartDate
{
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
    return callStartDate;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (mediaContentArray.count>0) {
        NSMutableArray *tempMediaarray= [mediaContentArray objectAtIndex:collectionView.tag];
        return  tempMediaarray.count;
    }
    return 0;}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(150, 150);
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productCell";
    
    
    NSMutableArray *currentMediaArray= [mediaContentArray objectAtIndex:collectionView.tag];
    
    NSString* mediaType = [[currentMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.selectedImgView.hidden=YES;
    
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    if (indexPath==selectedCellIndexPath) {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else{
        cell.layer.borderWidth=0;
    }

    //make a file name to write the data to using the documents directory:
    
    if ([mediaType isEqualToString:@"Image"])
    {
//        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
        
        NSString* thumbnailImageName = [[currentMediaArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
        
        cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];

        cell.captionLbl.text=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
        
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        cell.captionLbl.text=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        cell.captionLbl.text=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
        cell.captionLbl.text=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    
    
    //adding check mark to selected cell
    
//    MedRepCollectionViewCheckMarkViewController* checkMarkVC=[[MedRepCollectionViewCheckMarkViewController alloc]init];
//    if (demoedMediaFiles.count>0)
//    {
//        for (NSInteger i=0; i<demoedMediaFiles.count; i++)
//        {
//            NSString* selectedFileName=[demoedMediaFiles objectAtIndex:i];
//            NSString * currentFileName=[[collectionViewDataArray valueForKey:@"Media_File_ID"] objectAtIndex:indexPath.row] ;
//            
//            NSLog(@"selected file name : %@  current file name : %@", selectedFileName,currentFileName);
//            
//            if ([selectedFileName isEqualToString:currentFileName])
//            {
//                cell.selectedImgView.hidden=NO;
//            }
//            else
//            {
//            }
//        }
//    }
    
    cell.productImageView.backgroundColor=[UIColor clearColor];
//    if (indexPath.row== selectedCellIndex)
//    {
//        cell .layer.borderWidth=2.0;
//        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
//    }
//    else
//    {
//        cell.layer.borderWidth=0;
//    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCellIndex=indexPath.row;
    //    NSMutableDictionary * tempDict=[collectionViewDataArray objectAtIndex:indexPath.row];
    //
    //    [tempDict setObject:@"Yes" forKey:@"Demoed"];
    //    [collectionViewDataArray replaceObjectAtIndex:indexPath.row withObject:tempDict];
    //
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];

    selectedCellIndexPath = indexPath;
    
    NSMutableArray *currentMediaArray;
    
    @try {
        
        currentMediaArray= [mediaContentArray objectAtIndex:collectionView.tag];
        SelectedProductMediaFile=[currentMediaArray objectAtIndex:indexPath.row];

        
    } @catch (NSException *exception)
    {
        NSLog(@"index catch : %ld array: %@", (long)indexPath.row,collectionViewDataArray);
        NSLog(@"%ld",(long)segmentControl.selectedSegmentIndex);
        [self reloadCollectionViewWithIndex:segmentControl.selectedSegmentIndex];
        
        SelectedProductMediaFile=[collectionViewDataArray objectAtIndex:indexPath.row];
    }
    
    //SelectedProductMediaFile=[collectionViewDataArray objectAtIndex:indexPath.row];
    
    SelectedProductMediaFile.isDemoed=YES;
    
    NSLog(@"data after replacing %@", [currentMediaArray objectAtIndex:indexPath.row]);
    
    NSString* selectedFileID=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID"];
    if ([demoedMediaFiles containsObject:selectedFileID])
    {
        
    }
    else
    {
        [demoedMediaFiles addObject:selectedFileID];
    }
    
    NSLog(@"demoed file id %@", selectedFileID);
    
    NSString* mediaType = [[currentMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row];
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
    if ([mediaType isEqualToString:@"Image"])
    {
        [imagesPathArray removeAllObjects];
        for (NSInteger i=0; i<productImagesArray.count; i++)
        {
            [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i] ]];
        }
        
        NSLog(@"product images array before selecting %@", [productImagesArray description]);
        
        MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
        imagesVC.productImagesArray=imagesPathArray;
        imagesVC.imagesViewedDelegate=self;
        imagesVC.productDataArray=productImagesArray;
        imagesVC.currentImageIndex=indexPath.row;
        imagesVC.selectedIndexPath=indexPath;
        [self presentViewController:imagesVC animated:YES completion:nil];
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
        videosVC.videosViewedDelegate=self;
        videosVC.currentImageIndex=indexPath.row;
        videosVC.selectedIndexPath=indexPath;
        videosVC.productDataArray=productVideoFilesArray;
        [self.navigationController presentViewController:videosVC animated:YES completion:nil];
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        if (productPdfFilesArray.count>0)
        {
            MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
            pdfVC.pdfViewedDelegate=self;

            pdfVC.productDataArray=productPdfFilesArray;
            pdfVC.selectedIndexPath=indexPath;
            [self presentViewController:pdfVC animated:YES completion:nil];
        }
        else
        {
            UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No PDF files are available for the selected demo plan" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [noDataAvblAlert show];
        }
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        if (presentationsArray.count>0) {
            
            NSString * selectedFilePath=[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:[[presentationsArray objectAtIndex:indexPath.row]valueForKey:@"File_Name"]];
            
            /** Power point slide show files will display in external apps*/
            if([[selectedFilePath lowercaseString] hasSuffix:KppsxFileExtentionStr]){
                [docDispalayManager ShowUserActivityShareViewInView:collectionView SourceRect:cell.frame AndFilePath:selectedFilePath];
            }else{
                MedRepProductHTMLViewController *obj = [[MedRepProductHTMLViewController alloc]init];
                obj.HTMLViewedDelegate = self;
                obj.productDataArray = presentationsArray;
                obj.showAllProducts=YES;
                obj.isObjectData=YES;
                obj.selectedIndexPath = indexPath;
                obj.currentImageIndex=indexPath.row;
                [self presentViewController:obj animated:YES completion:nil];
            }
            
        }
        else
        {
            UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No presentation files are available " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [noDataAvblAlert show];
        }
    }
    //[mediaSwipeView reloadData];
}


#pragma mark Swipe View Methods

#pragma mark swipe view methods


- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return 4;
}

-(void)updateSegmentIndex
{
    [segmentControl setSelectedSegmentIndex:selectedSegmentIndex animated:YES];

}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    NSLog(@"swipe page number is %ld", (long)swipeView.currentPage);
  
    
    
}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView
{
    NSLog(@"swipe view did end decelerating called");
    selectedSegmentIndex=swipeView.currentPage;
    
    [self performSelector:@selector(updateSegmentIndex) withObject:self afterDelay:0.5];
    
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    demoPlanMediaCollectionView = nil;
    view = [[UIView alloc] initWithFrame:mediaSwipeView.bounds];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(150, 150);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *demoPlanCollectionView = [[UICollectionView alloc] initWithFrame:view.bounds collectionViewLayout:flowLayout];
    demoPlanCollectionView.delegate=self;
    demoPlanCollectionView.dataSource=self;
    demoPlanCollectionView.tag = index;
    
    demoPlanCollectionView.backgroundColor=[UIColor whiteColor];
    [demoPlanCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    
    
    [view addSubview:demoPlanCollectionView];
    
    
    [self reloadCollectionViewWithIndex:index];
    
    return view;
}

-(void)reloadCollectionViewWithIndex:(NSInteger)index
{
        selectedSegmentIndex=index;
        if (mediaContentArray.count>0) {
        demoPlanMediaCollectionView.contentOffset=CGPointZero;
    }
    [self performSelector:@selector(updateSegmentIndex) withObject:self afterDelay:0.5];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark Task and Notes methods

- (IBAction)tasksButtonTapped:(id)sender
{
   // +(NSMutableArray*)fetchTaskObjectsforLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID
    
    SalesWorxBrandAmbassadorsTaskListViewController * viewController=[[SalesWorxBrandAmbassadorsTaskListViewController alloc]init];
    viewController.tasksListDelegate=self;
    viewController.isViewing=YES;
    viewController.currentVisit=currentVisit;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 570);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = tasksButton;
    presentationController.sourceRect =tasksButton.bounds;
    
    
    if ([SWDefaults isRTL]) {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
    }
    else
    {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionRight;
    }
    
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
    }];
}

-(void)salesWorxBrandAmbassadorsTasksList:(NSMutableArray*)tasksList
{
    if (tasksList.count>0) {
        currentVisit.tasksArray=tasksList;
        
    }
    [self refreshTaskandNotesStatus:currentVisit];
}

-(void)refreshTaskandNotesStatus:(SalesWorxBrandAmbassadorVisit*)currentVisitDetails
{
    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodesForBrandAmbassadorTasksandNotes:currentVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }
    
}


- (IBAction)notesButtonTapped:(id)sender
{
    SalesWorxBrandAmbassadorNotesListViewController * viewController=[[SalesWorxBrandAmbassadorNotesListViewController alloc]init];
    viewController.currentVisit=currentVisit;
    viewController.isViewing=YES;
    viewController.notesListDelegate=self;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 350);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = notesButton;
    presentationController.sourceRect =notesButton.bounds;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
    }];
}
-(void)salesWorxBrandAmbassadorsNotesList:(NSMutableArray*)notesList
{
    if (notesList.count>0) {
        currentVisit.notesArray=notesList;
        
    }
    [self refreshTaskandNotesStatus:currentVisit];
}

#pragma mark UITextField Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=[demoPlanObjectsArray valueForKey:@"Description"];
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    popOverVC.titleKey=@"Demo Plan";
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    popOverNavigationCroller.preferredContentSize=CGSizeMake(300, 250);
    popOverNavigationCroller.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = popOverNavigationCroller.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = demoPlanTextFied;
    presentationController.sourceRect =demoPlanTextFied.bounds;
    
    [self presentViewController:popOverNavigationCroller animated:YES completion:^{
        
    }];
    return NO;
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    selectedDemoPlan=[demoPlanObjectsArray objectAtIndex:selectedIndexPath.row];
    
    demoPlanTextFied.text=selectedDemoPlan.Description;
    
    currentVisit.selectedDemoPlan=selectedDemoPlan;
    
    NSMutableArray* tempArray=[MedRepQueries fetchMediaFileswithDemoPlanID:selectedDemoPlan.Demo_Plan_ID];
    
    productImagesArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
    
    NSLog(@"product images array in did select popover %@", productImagesArray);
    
    productVideoFilesArray= [[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
    productPdfFilesArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
    presentationsArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
    
    mediaContentArray=[[NSMutableArray alloc]init];
    [mediaContentArray addObject:productImagesArray];
    [mediaContentArray addObject:productVideoFilesArray];
    [mediaContentArray addObject:productPdfFilesArray];
    [mediaContentArray addObject:presentationsArray];
    [mediaSwipeView reloadData];

    
    NSLog(@"selected segment index in demo plan popover title is %ld",(long)mediaSwipeView.currentPage);
    
    //[self reloadCollectionViewWithIndex:mediaSwipeView.currentPage];
}

#pragma mark Segment Control method

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    
    [mediaSwipeView scrollToItemAtIndex:segmentedControl.selectedSegmentIndex duration:0.0];

    
    
}


#pragma mark End Visit Tapped
-(void)displayEndVisit
{
    
    NSInteger callsCount=[[SalesWorxBrandAmbassadorQueries retrieveManager]fetchCurrentVisitCallsCount:currentVisit];
    
    NSLog(@"closing the current visit with palnned visit id %@", currentVisit.Planned_Visit_ID);
    
    if (callsCount>0) {
     
        BOOL visitCompletionStatus=[[SalesWorxBrandAmbassadorQueries retrieveManager] updateVisitCompletionStatus:currentVisit];
        BOOL visitendDate=[[SalesWorxBrandAmbassadorQueries retrieveManager] updateVisitEndDateToActualVisits:currentVisit];
        NSLog(@"visit end date status %hhd", visitendDate);
        if (visitCompletionStatus==YES) {
            
            NSLog(@"posting brand ambassador did finish visit notification with object %@", currentVisit.selectedLocation.Location_Name);
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"BrandAmbassadorVisitDidFinishNotification" object:currentVisit];

            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else{
            NSLog(@"error updating visit completion status");
        }
        
    }
    else{
    
    MedRepVisitsEndCallViewController *presentingView=   [[MedRepVisitsEndCallViewController alloc]init];
    presentingView.isBrandAmbassadorVisit=YES;
    presentingView.currentBrandAmbassadorVisit=currentVisit;
    presentingView.visitCompletedDelegate=self;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    mapPopUpViewController.navigationBarHidden=YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
    }
}

-(void)visitCompletedWithOutCalls:(BOOL)status
{
    if (status==YES) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


@end
