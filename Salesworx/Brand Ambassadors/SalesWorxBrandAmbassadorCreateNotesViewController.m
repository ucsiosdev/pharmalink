//
//  SalesWorxBrandAmbassadorCreateNotesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorCreateNotesViewController.h"

@interface SalesWorxBrandAmbassadorCreateNotesViewController ()

@end

@implementation SalesWorxBrandAmbassadorCreateNotesViewController
@synthesize titleTextField,descriptionTextView,currentNote;
- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem .rightBarButtonItem=saveBtn;
    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem .leftBarButtonItem=cancelBtn;
    
    if (currentNote) {
        
        titleTextField.text=currentNote.Title;
        descriptionTextView.text=currentNote.Description;
    }
    else
    {
        currentNote=[[salesWorxBrandAmbassadorNotes alloc]init];
        currentNote.Note_ID=[NSString createGuid];
    }

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Notes Details"];
}

-(void)cancelTapped
{
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)saveTapped
{
    [self.view endEditing:YES];
    
    
    if ([NSString isEmpty:currentNote.Title]) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter note title" withController:self];
    }
    else if ([NSString isEmpty:currentNote.Description])
    {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter note description" withController:self];
    }
    else
    {
    if ([self.createNoteDelegate respondsToSelector:@selector(updatedNote:)]) {
        
        [self.createNoteDelegate updatedNote:currentNote];
    }
    [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([titleTextField isFirstResponder]) {
        
        [titleTextField resignFirstResponder];
        
        [descriptionTextView becomeFirstResponder];
        
        return NO;
        
    }
    
    else
        
    {
        return YES;
        
    }
    
    
}

#pragma mark UITextField Delegate Methods


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= NoteTitleTextFieldLimit;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    currentNote.Title=textField.text;
    currentNote.isUpdated=@"YES";
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    currentNote.Description=textView.text;
    currentNote.isUpdated=@"YES";
    
    
    
}


#pragma mark UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= NoteInformationTextFieldLimit;
}



@end
