//
//  SalesWorxBrandAmbassadorEndCallViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SalesWorxBrandAmbassadorQueries.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell.h"
#import "SalesWorxTableView.h"
#import "SalesWorxBrandAmbassadorsTaskListViewController.h"
#import "SalesWorxBrandAmbassadorNotesListViewController.h"
#import "SalesWorxBrandAmbassadorQueries.h"
#import "TGPCamelLabels7.h"
#import "TGPDiscreteSlider7.h"

#define kCustomerNametextFieldLimit 250
#define kCustomerEmailtextFieldLimit 100
#define kCustomerPhonetextFieldLimit 50


@interface SalesWorxBrandAmbassadorEndCallViewController : UIViewController<UIPopoverPresentationControllerDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    
    IBOutlet MedRepElementDescriptionLabel *locationLbl;
    IBOutlet MedRepElementDescriptionLabel *locationNameLbl;
    IBOutlet SalesWorxImageView *notesStatusImageView;
    IBOutlet SalesWorxImageView *taskStatusImageView;
    IBOutlet MedRepTextField *customerNameTextField;
    IBOutlet MedRepTextField *mobileNumberTextField;
    IBOutlet MedRepTextField *emailAddressTextField;
    IBOutlet UITextView *visitConclusionNotesTextView;
    IBOutlet UILabel *waitTimeLbl;
    IBOutlet UILabel *visitStartTimeLbl;
    IBOutlet UILabel *visitEndTimeLbl;
    IBOutlet SalesWorxTableView *samplesGivenTableView;
    IBOutlet UIButton *tasksButton;
    IBOutlet UIButton *notesButton;

    NSMutableArray * currentCustomerSamplesGivenArray;
    
    NSString *visitRating;
    
    NSTimer * endTimeTimer;
    
    
    
    IBOutlet TGPCamelLabels7 *camelLabelsView;
    IBOutlet TGPDiscreteSlider7 *descreteSliderView;
}
- (IBAction)tasksButtonTapped:(id)sender;
- (IBAction)notesButtonTapped:(id)sender;

@property(nonatomic) BOOL isFromDashboard;

@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * currentVisit;
@property(strong,nonatomic) NSMutableArray* samplesandSellOutArray;
@end
