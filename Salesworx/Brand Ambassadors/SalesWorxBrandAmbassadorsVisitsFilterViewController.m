//
//  SalesWorxBrandAmbassadorsVisitsFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorsVisitsFilterViewController.h"

@interface SalesWorxBrandAmbassadorsVisitsFilterViewController ()

@end

@implementation SalesWorxBrandAmbassadorsVisitsFilterViewController

@synthesize locationTextField,visitDateTextField,filterParametersDict,previousFilterParametersDict,contentArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    filterParametersDict=[[NSMutableDictionary alloc]init];
    
    NSLog(@"content array being passed to visits filter %@",[self.contentArray valueForKey:@"Date"]);
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:kFilterTitle];
    
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearTapped)];
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem .rightBarButtonItem=saveBtn;


    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem .leftBarButtonItem=cancelBtn;

    if (previousFilterParametersDict) {
        
        filterParametersDict=previousFilterParametersDict;
    
    NSString * visitDate=[previousFilterParametersDict valueForKey:@"Date"];

    if ([NSString isEmpty:visitDate]==NO) {
        
        visitDateTextField.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:visitDate];;
    }
    
    
    SalesWorxBrandAmbassadorLocation * selectedLocation=[previousFilterParametersDict objectForKey:@"Location"];
    if (selectedLocation) {
        
        locationTextField.text=selectedLocation.Location_Name;
    }
    }
    

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingBrandAmbassadorVisitsFilter];
    }
}
-(void)clearTapped
{
    filterParametersDict=[[NSMutableDictionary alloc]init];
    visitDateTextField.text=@"";
    locationTextField.text=@"";
    
}

-(void)closeTapped
{
    
    if ([self.brandAmbassadorFilterDelegate respondsToSelector:@selector(filteredBrandAmbassadorDidClose)]) {

        [self.brandAmbassadorFilterDelegate filteredBrandAmbassadorDidClose];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark UITextFieldMethods


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==locationTextField) {
        [self textfieldDidTap:locationTextField withTitle:kSelectedLocation withContentArray:contentArray];
    }
    else
    {
        [self textfieldDidTap:visitDateTextField withTitle:kSelectedLocation withContentArray:[[NSMutableArray alloc]init]];
    }
    return NO;
}
-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withContentArray:(NSMutableArray*)contentArray
{
    
    if (tappedTextField == visitDateTextField) {
        MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        dateFilterVC.isVisitFilter=YES;
        dateFilterVC.visitDateDelegate=self;
        [self.navigationController pushViewController:dateFilterVC animated:YES];
    }
    else
    {
        
        SalesWorxBrandAmbassadorContentFilterViewController * contentVC=[[SalesWorxBrandAmbassadorContentFilterViewController alloc]init];
        contentVC.predicateType=kPredicateTypeContains;
        contentVC.isCreateVisit=YES;
        contentVC.selectedObjectDelegate=self;
        
        NSString * date=[filterParametersDict valueForKey:kSelectedDate];
        
        
        NSLog(@"selected date is %@", date);
        
        NSLog(@"date in content %@", [self.contentArray valueForKey:@"Date"]);
        
        
        NSMutableArray * locationsForSelectedDate=[[NSMutableArray alloc]init];
        if ([NSString isEmpty:date]==NO) {
            
            
            
            
            NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"SELF.Date contains[cd] %@", [filterParametersDict valueForKey:@"Date"]];
            
            NSLog(@"predicate is %@", datePredicate);
            locationsForSelectedDate=[[self.contentArray filteredArrayUsingPredicate:datePredicate] mutableCopy];
            NSLog(@"filtered locations with date is %@", locationsForSelectedDate);
            
            contentVC.contentArray= [locationsForSelectedDate valueForKey:@"selectedLocation"];

        }
        else{
            
//            NSPredicate * datePredicate=[NSPredicate predicateForDistinctWithProperty:@"selectedLocation"];
//            NSMutableArray* distinctLocations=[[self.contentArray filteredArrayUsingPredicate:datePredicate] mutableCopy];
//            

            
//            NSArray *uniqueStates;
//            uniqueStates = [self.contentArray valueForKeyPath:@"@distinctUnionOfObjects.selectedLocation"];
//            
//            
//            NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:@"Location_ID"];
//            
//            
//            NSArray * refinedArray=[self.contentArray filteredArrayUsingPredicate:refinedPred];
//        
//            
//            NSMutableArray* unfilteredArray=[self.contentArray valueForKey:@"selectedLocation"];
//            
//            NSMutableArray * filteredArray=[[unfilteredArray filteredArrayUsingPredicate:refinedPred] ];
            
           NSMutableArray* temp=
             [self.contentArray valueForKey:@"selectedLocation"];
            
            NSPredicate * testPred=[NSPredicate predicateForDistinctWithProperty:@"Location_ID"];
            NSMutableArray * distinctArray=[[temp filteredArrayUsingPredicate:testPred] mutableCopy];
            
            NSLog(@"distinct array is %@",[distinctArray valueForKeyPath:@"Location_Name"]);
            
            
            contentVC.contentArray=distinctArray;
            
            
 
        }
        if (tappedTextField==locationTextField) {
            contentVC.keyString=@"Location_Name";
            contentVC.titleString=@"Location";
        }
        NSLog(@"content VC is %@",contentVC.contentArray);
        
        [self.navigationController pushViewController:contentVC animated:YES];
        
    }
}

#pragma mark Selected Delegate Methods

-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    
    

    
    NSLog(@"selected date is %@", selectedDate);
    visitDateTextField.text= [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[MedRepDefaults getDefaultStringForEmptyString:selectedDate]];
    [filterParametersDict setValue:[SWDefaults getValidStringValue:selectedDate] forKey:kSelectedDate];
}

-(void)selectedObject:(id)selectedItem
{
    NSLog(@"selected object is %@", selectedItem);
    
    SalesWorxBrandAmbassadorLocation * selectedLocation=selectedItem;
    locationTextField.text=[MedRepDefaults getDefaultStringForEmptyString:selectedLocation.Location_Name];

    [filterParametersDict setObject:selectedLocation forKey:kSelectedLocation];

    
}


- (IBAction)resetButtonTapped:(id)sender {
    
    if ([self.brandAmbassadorFilterDelegate respondsToSelector:@selector(filteredBrandAmbassadorDidReset)]) {
        
        [self.brandAmbassadorFilterDelegate filteredBrandAmbassadorDidReset];
        [self.brandAmbassadorFilterDelegate filteredParameters:[[NSMutableDictionary alloc]init]];

    }
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)searchButtonTapped:(id)sender {
    
    NSLog(@"filter parameters are %@", filterParametersDict);
    
    NSString * date=[filterParametersDict valueForKey:kSelectedDate];
    SalesWorxBrandAmbassadorLocation * selectedLocation=[filterParametersDict objectForKey:@"Location"];
    NSString* missingString=[[NSString alloc]init];
    
    
    
    if ([NSString isEmpty:date] && !selectedLocation) {
        
        if ([NSString isEmpty:date]) {
            
            missingString=@"visit date";
        }
        
        else if (!selectedLocation)
        {
            missingString=kSelectedLocation;
        }
        
        [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please enter %@",missingString] withController:self];
    }
    else
    {
    
    
    NSMutableArray * predicateArray=[[NSMutableArray alloc]init];

    previousFilterParametersDict=filterParametersDict;
    

    if (selectedLocation) {
        
        
        NSPredicate *customerPredicate = [NSPredicate predicateWithFormat:@"selectedLocation.Location_ID == %@", [NSString stringWithFormat:@"%@",selectedLocation.Location_ID]];
        
        [predicateArray addObject:customerPredicate];
    }
    
    if ([NSString isEmpty:[filterParametersDict valueForKey:@"Date"]]==NO) {
//        NSString* refinedDateFormat=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[filterParametersDict valueForKey:@"Date"]]
        
        NSLog(@"selected date si %@",[filterParametersDict valueForKey:@"Date"]);
        
       // NSString* refinedDateforfilter=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
        

        NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"SELF.Date contains[cd] %@", [filterParametersDict valueForKey:@"Date"]];
        
        [predicateArray addObject:datePredicate];
    }
    
    NSPredicate * refinedPredicate=[NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    NSMutableArray * filteredArray=[[self.contentArray filteredArrayUsingPredicate:refinedPredicate] mutableCopy];
    
    NSLog(@"filtered array is %@",filteredArray);

        if (filteredArray.count>0) {
            
            if ([self.brandAmbassadorFilterDelegate respondsToSelector:@selector(filteredBrandAmbassadorVisits:)]) {
                [self.brandAmbassadorFilterDelegate filteredParameters:previousFilterParametersDict];
                [self.brandAmbassadorFilterDelegate filteredBrandAmbassadorVisits:filteredArray];

                
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }

        
        
        else{
              [SWDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
        }
    }
}




























@end
