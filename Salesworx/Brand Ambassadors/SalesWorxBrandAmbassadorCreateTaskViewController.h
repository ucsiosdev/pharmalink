//
//  SalesWorxBrandAmbassadorCreateTaskViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "NSString+Additions.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxCustomClass.h"

@protocol SalesWorxBrandAmbassadorUpdatedtaskDelegate <NSObject>

-(void)updatedtask:(SalesWorxBrandAmbassadorTask*)currentTask;

@end


@interface SalesWorxBrandAmbassadorCreateTaskViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    NSString* buttonTitleString;
    
    NSMutableArray * statusArray;
    NSMutableArray* categoryArray;
    NSMutableArray* priorityArray;
    id createTaskDelegate;
}
@property (strong, nonatomic) IBOutlet MedRepTextField *titleTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *dueDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextView *informationTextView;
@property (strong, nonatomic) IBOutlet MedRepTextField *statusTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *categoryTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *priorityTextField;
@property(strong,nonatomic) SalesWorxBrandAmbassadorTask * currentTask;
@property(nonatomic)     id createTaskDelegate;
@property(nonatomic) BOOL isUpdatingTask;


@end
