//
//  SalesWorxBrandAmbassadorsTaskListViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxBrandAmbassadorCreateTaskViewController.h"
#import "SalesWorxDefaultTableViewCell.h"
@protocol SalesWorxBrandAmbassadorTasksListDelegate <NSObject>

-(void)salesWorxBrandAmbassadorsTasksList:(NSMutableArray*)tasksList;

@end

@interface SalesWorxBrandAmbassadorsTaskListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

{
    id tasksListDelegate;
    
    NSIndexPath *selectedtaskIndexPath;
}

@property(nonatomic) BOOL isViewing;
@property(nonatomic) id tasksListDelegate;
@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * currentVisit;
@property (strong, nonatomic) IBOutlet UITableView *taskListTableView;


@end
