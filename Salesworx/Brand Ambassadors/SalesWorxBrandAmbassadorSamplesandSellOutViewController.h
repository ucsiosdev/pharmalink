//
//  SalesWorxBrandAmbassadorSamplesandSellOutViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell.h"

#import "SWDefaults.h"

@interface SalesWorxBrandAmbassadorSamplesandSellOutViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(strong,nonatomic) IBOutlet UITableView *samplesTableView;

@property(strong,nonatomic) NSMutableArray * samplesandSellOutArray;
@end
