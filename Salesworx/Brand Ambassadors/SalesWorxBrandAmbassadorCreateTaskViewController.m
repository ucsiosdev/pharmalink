//
//  SalesWorxBrandAmbassadorCreateTaskViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorCreateTaskViewController.h"

@interface SalesWorxBrandAmbassadorCreateTaskViewController ()

@end

@implementation SalesWorxBrandAmbassadorCreateTaskViewController
@synthesize titleTextField,dueDateTextField,statusTextField,categoryTextField,priorityTextField,informationTextView,currentTask,isUpdatingTask;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Tasks Details"];

    statusArray=[[NSMutableArray alloc]init];
    categoryArray=[[NSMutableArray alloc]init];
    priorityArray=[[NSMutableArray alloc]init];
    
    
    statusArray=[MedRepQueries fetchTaskAppCodes:@"Task_Status"];
    categoryArray=[MedRepQueries fetchTaskAppCodes:@"Task_Category"];
    priorityArray=[MedRepQueries fetchTaskAppCodes:@"Task_Priority"];
    
    // Do any additional setup after loading the view from its nib.
    
    if (currentTask) {
        
        if ([currentTask.Status isEqualToString:@"Closed"] || [currentTask.Status isEqualToString:@"Cancelled"]) {
            
            self.view.userInteractionEnabled=NO;
        }
        else
        {
            self.view.userInteractionEnabled=YES;

        }
        
    }
    else
    {
        currentTask =[[SalesWorxBrandAmbassadorTask alloc]init];
        currentTask.Task_ID=[NSString createGuid];
        statusTextField.isReadOnly=YES;
        statusTextField.text=kNewStatus;
        currentTask.Status=kNewStatus;
    }
    
    
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];

    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem .leftBarButtonItem=cancelBtn;
    
    if (isUpdatingTask) {
        titleTextField.text=[SWDefaults getValidStringValue:currentTask.Title];
        dueDateTextField.text= [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:currentTask.Start_Time]];
        statusTextField.text=[SWDefaults getValidStringValue:currentTask.Status];
        categoryTextField.text=[SWDefaults getValidStringValue:currentTask.Category];
        priorityTextField.text=[SWDefaults getValidStringValue:currentTask.Priority];
        informationTextView.text=[SWDefaults getValidStringValue:currentTask.Description];
        [saveBtn setTitle:@"Update"];
    }
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem .rightBarButtonItem=saveBtn;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)saveTapped
{
    NSLog(@"***UPDATED BA TASK DETAILS before saving ARE %@  %@ %@ %@ %@ %@ %@ ***",currentTask.Title,currentTask.Description,currentTask.Status,currentTask.Priority,currentTask.Category,currentTask.Start_Time,currentTask.isUpdated );
    
    [self.view endEditing:YES];
    
    if ([NSString isEmpty:currentTask.Title]) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter task title" withController:self];
        }
    else if ([NSString isEmpty:currentTask.Start_Time])
    {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter task due date" withController:self];
    }
    else{
    
    if ([self.createTaskDelegate respondsToSelector:@selector(updatedtask:)]) {
        [self.createTaskDelegate updatedtask:currentTask];
    }
    [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)cancelTapped
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    currentTask.Description=textView.text;
}

#pragma mark UITextField Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==titleTextField) {
        currentTask.Title=textField.text;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [self.view endEditing:YES];
    
    if(textField==dueDateTextField)
    {
        [self dueDateDropDownTapped];
        return NO;
    }
    else if(textField==statusTextField)
    {
        [self statusDropDownTapped];
        return NO;
        
    }
    else if(textField==categoryTextField)
    {
        [self categoryDropDownTapped];
        return NO;
        
    }
    else if(textField==priorityTextField)
    {
        [self priorityDropDownTapped];
        return NO;
        
    }
    
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if([newString length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    if([newString isEqualToString:@" "])
    {
        return NO;
        
    }
    
    if(textField==titleTextField)
    {
        //        NSString *expression = @"^[a-zA-Z ]+$";
        //        NSError *error = nil;
        //        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        //        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return ( newString.length<TaskTitleTextFieldLimit);
    }
    
    
    
    return YES;
    
    
    
    
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= TaskInformationTextFieldLimit;
}


#pragma mark Button Action Methods


//- (IBAction)dueDateTapped:(id)sender {
-(void)dueDateDropDownTapped
{
    buttonTitleString=@"Date";
    MedRepVisitsDateFilterViewController * visitDateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
    visitDateFilterVC.titleString=@"Task Date";
    visitDateFilterVC.visitDateDelegate=self;
    if ([NSString isEmpty:dueDateTextField.text]==NO) {
        NSString* refinedStr=[MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:@"yyyy-MM-dd" scrString:dueDateTextField.text];
        visitDateFilterVC.previouslySelectedDate=refinedStr;
        
    }
    
    [self.navigationController pushViewController:visitDateFilterVC animated:YES];
    
}

//- (IBAction)priorityTapped:(id)sender {
-(void)priorityDropDownTapped
{
    
    buttonTitleString=@"Priority";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=buttonTitleString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=priorityArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
    
}




//- (IBAction)statusTapped:(id)sender {

-(void)statusDropDownTapped
{
    buttonTitleString=@"Status";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=buttonTitleString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=statusArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
    
}

//- (IBAction)categoryTapped:(id)sender {
-(void)categoryDropDownTapped
{
    buttonTitleString=@"Category";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=buttonTitleString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=categoryArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
}

#pragma Description Delegate Methods

-(void)selectedValueforCreateVisit:(NSIndexPath*)indexPath
{
    
    currentTask.isUpdated=@"YES";
    
    
    if ([buttonTitleString isEqualToString:@"Priority"]) {
        
        // [priorityButton setTitle:[priorityArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        priorityTextField.text=[priorityArray objectAtIndex:indexPath.row];
        currentTask.Priority=[priorityArray objectAtIndex:indexPath.row];
        
    }
    
    
    
    else if ([buttonTitleString isEqualToString:@"Category"]) {
        
        //[categoryButton setTitle:[categoryArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        categoryTextField.text=[categoryArray objectAtIndex:indexPath.row];
        currentTask.Category=[categoryArray objectAtIndex:indexPath.row];
        
        
    }
    
    
    else  if ([buttonTitleString isEqualToString:@"Status"]) {
        
        // [statusButton setTitle:[statusArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        statusTextField.text=[statusArray objectAtIndex:indexPath.row];
        currentTask.Status=[statusArray objectAtIndex:indexPath.row];
        
        if ([currentTask.Status isEqualToString:@"Closed"]) {
            
            
            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [formatterTime setLocale:usLocaleq];
            NSString *taskCloseTime =  [formatterTime stringFromDate:[NSDate date]];
            
            currentTask.End_Time=taskCloseTime;
        }
        
        
    }
        
}
-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    
    NSLog(@"selected date in date filter %@", selectedDate);
    
    NSString* selectedDateValue=selectedDate;
    
    currentTask.isUpdated=@"YES";
    
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:mm:ss" destFormat:kDateFormatWithoutTime scrString:selectedDateValue];
    
    NSLog(@"refined date in date picker %@", refinedDate);
    if ([buttonTitleString isEqualToString:@"Date"]) {
        dueDateTextField.text=refinedDate;
        NSString* refinedDate=[MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:dueDateTextField.text];
        currentTask.Start_Time=refinedDate;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
