//
//  SalesWorxBrandAmbassadorsTaskListViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorsTaskListViewController.h"

@interface SalesWorxBrandAmbassadorsTaskListViewController ()

@end

@implementation SalesWorxBrandAmbassadorsTaskListViewController
@synthesize isViewing,currentVisit,taskListTableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Tasks"];
    
    if (isViewing) {
        
    }
    else
    {
        UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Add", nil) style:UIBarButtonItemStylePlain target:self action:@selector(addTapped)];
        [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        
        self.navigationItem .rightBarButtonItem=saveBtn;

    }
    
    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem .leftBarButtonItem=cancelBtn;
    
    if (currentVisit.tasksArray.count>0) {
        
        [taskListTableView reloadData];
    }
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)addTapped
{
    SalesWorxBrandAmbassadorCreateTaskViewController * createTaskVC=[[SalesWorxBrandAmbassadorCreateTaskViewController alloc]init];
    createTaskVC.createTaskDelegate=self;
    [self.navigationController pushViewController:createTaskVC animated:YES];
}
-(void)updatedtask:(SalesWorxBrandAmbassadorTask*)currentTask
{
    NSLog(@"created task is %@", currentTask.Title);
    
    if (currentVisit.tasksArray.count==0) {
        
        currentVisit.tasksArray=[[NSMutableArray alloc]init];
        [currentVisit.tasksArray addObject:currentTask];
    }
    else if (currentVisit.tasksArray.count>0)
    {
        if ([currentVisit.tasksArray containsObject:currentTask]) {
            
            [currentVisit.tasksArray replaceObjectAtIndex:selectedtaskIndexPath.row withObject:currentTask];
        }
        else{
        [currentVisit.tasksArray addObject:currentTask];
        }
    }
    else
    {
        
    }
    [taskListTableView reloadData];
}

-(void)cancelTapped
{
    if ([self.tasksListDelegate respondsToSelector:@selector(salesWorxBrandAmbassadorsTasksList:)]) {
        
        [self.tasksListDelegate salesWorxBrandAmbassadorsTasksList:currentVisit.tasksArray];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITableView Methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    tableView.separatorColor=kUITableViewSaperatorColor;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (currentVisit.tasksArray.count>0) {
        
        return currentVisit.tasksArray.count;
    }
    else
    {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"defaultCell";
    SalesWorxDefaultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDefaultTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    SalesWorxBrandAmbassadorTask * currentTask=[[SalesWorxBrandAmbassadorTask alloc]init];
    currentTask=[currentVisit.tasksArray objectAtIndex:indexPath.row];
    cell.titleLbl.text=[SWDefaults getValidStringValue:currentTask.Title];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedtaskIndexPath=indexPath;
    SalesWorxBrandAmbassadorCreateTaskViewController * createTaskVC=[[SalesWorxBrandAmbassadorCreateTaskViewController alloc]init];
    createTaskVC.createTaskDelegate=self;
    createTaskVC.isUpdatingTask=YES;
    createTaskVC.currentTask=[currentVisit.tasksArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:createTaskVC animated:YES];
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
