//
//  SalesWorxBrandAmbassadorContentFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorContentFilterViewController.h"

@interface SalesWorxBrandAmbassadorContentFilterViewController ()

@end

@implementation SalesWorxBrandAmbassadorContentFilterViewController
@synthesize contentArray,keyString,predicateType,titleString,hideBackButton,showCloseButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.extendedLayoutIncludesOpaqueBars = YES;

    self.definesPresentationContext = YES;

    salesWorxSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    salesWorxSearchController.hidesNavigationBarDuringPresentation = YES;

    [salesWorxSearchController setDimsBackgroundDuringPresentation:NO];
    
    // The searchcontroller's searchResultsUpdater property will contain our tableView.
    salesWorxSearchController.searchResultsUpdater = self;
    
    // The searchBar contained in XCode's storyboard is a leftover from UISearchDisplayController.
    // Don't use this. Instead, we'll create the searchBar programatically.
    salesWorxSearchController.searchBar.frame = CGRectMake(salesWorxSearchController.searchBar.frame.origin.x,
                                                       salesWorxSearchController.searchBar.frame.origin.y,
                                                       salesWorxSearchController.searchBar.frame.size.width, 44.0);
    
    
    salesWorxSearchController.searchBar.delegate = self;
    
    

    
    contentTableView.tableHeaderView = salesWorxSearchController.searchBar;

    unfilteredArray=contentArray;
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:titleString];


    // Do any additional setup after loading the view from its nib.
    
    if (_isCreateVisit) {

    }

}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)closeTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (!hideBackButton) {
        [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    }
    
    if (showCloseButton) {
        UIBarButtonItem * closeButton=[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(kCloseTitle, nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
        [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem=closeButton;
    }
    
    
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (contentArray.count>0) {
        return contentArray.count;
    }
    else
    {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.font=MedRepElementDescriptionLabelFont;
    cell.textLabel.textColor=MedRepTableViewCellTitleColor;
    
    cell.textLabel.text=[[contentArray objectAtIndex:indexPath.row] valueForKey:keyString];
    
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self
    || (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController)
    || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
 
    
    id selectedContent=[contentArray objectAtIndex:indexPath.row];
    
    if ([self.selectedObjectDelegate respondsToSelector:@selector(selectedObject:)]) {
        
        [self.selectedObjectDelegate selectedObject:selectedContent];
        
    }
    
    if (_isCreateVisit) {
        [self.navigationController popViewControllerAnimated:YES];

    }
    
    else
    {
    if ([self isModal]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([NSString isEmpty:searchText]) {
        contentArray=unfilteredArray;
        [contentTableView reloadData];
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    contentArray=unfilteredArray;
    [contentTableView reloadData];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    
    if ([NSString isEmpty:searchString]==NO) {
       // NSPredicate*  searchPredicate = [NSPredicate predicateWithFormat:@"SELF.Doctor_Name contains[cd] %@",searchText];

        NSPredicate * searchPredicate =[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.%@ %@ '%@'",keyString,predicateType,searchString]];
        
        NSMutableArray * filteredContentArray=[[unfilteredArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        if (filteredContentArray.count>0) {
            contentArray=filteredContentArray;
            [contentTableView reloadData];
        }
        else if (filteredContentArray.count==0)
        {
            contentArray=unfilteredArray;
            [contentTableView reloadData];
        }
    }

}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
