//
//  SalesWorxBrandAmbassadorMultipleVisitsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "SalesWorxBrandAmbassadorQueries.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxImageView.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepTextView.h"
#import "MedRepTextField.h"
#import "MedRepProductsCollectionViewCell.h"
#import "MedRepProductImagesViewController.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepProductHTMLViewController.h"
#import "SalesWorxBrandAmbassadorsTaskListViewController.h"
#import "SalesWorxBrandAmbassadorNotesListViewController.h"
#import "HMSegmentedControl.h"
#import "SwipeView.h"
#import "MedRepVisitsEndCallViewController.h"
#import "SalesWorxBrandAmbassadorEdetailingStartCallViewController.h"


@interface SalesWorxBrandAmbassadorMultipleVisitsViewController : UIViewController<UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,SwipeViewDelegate,SwipeViewDataSource,UITextFieldDelegate>

{
    
    IBOutlet SwipeView *mediaSwipeView;

    IBOutlet HMSegmentedControl *segmentControl;
    IBOutlet UIButton *notesButton;
    IBOutlet UIButton *tasksButton;
    IBOutlet SalesWorxImageView *notesStatusImageView;
    IBOutlet SalesWorxImageView *taskStatusImageView;
    IBOutlet MedRepElementDescriptionLabel *waitTimeLbl;
    IBOutlet MedRepElementDescriptionLabel *timeOfArrivalLbl;
    IBOutlet MedRepElementDescriptionLabel *locationNameLbl;
    IBOutlet MedRepTextView *objectiveTextView;
    IBOutlet MedRepTextField *demoPlanTextFied;
    IBOutlet UICollectionView *demoPlanMediaCollectionView;
    DemoPlan * selectedDemoPlan;

    IBOutlet UIView *segmentContainerView;

    
    ProductMediaFile* SelectedProductMediaFile;
    NSMutableArray * demoedMediaFiles;
    
    NSTimer *waitTimer;
    NSDate *timerDate;
    NSMutableArray * collectionViewDataArray;
    NSMutableArray * productMediaFilesArray;
    NSMutableArray *productImagesArray;
    NSMutableArray * productVideoFilesArray;
    NSMutableArray *productPdfFilesArray;
    NSMutableArray * presentationsArray;
    
    NSMutableArray * mediaContentArray;
    
    NSInteger selectedCellIndex;
    NSInteger selectedSegmentIndex;
    NSMutableArray * segmentMenuArray;
    
    NSIndexPath * selectedCellIndexPath;

    NSMutableArray * imagesPathArray;
    NSMutableArray * demoPlanObjectsArray;

}
@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * currentVisit;
@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * receivedVisit;

@property(nonatomic) BOOL isFromDashboard;

- (IBAction)notesButtonTapped:(id)sender;

- (IBAction)tasksButtonTapped:(id)sender;
@end
