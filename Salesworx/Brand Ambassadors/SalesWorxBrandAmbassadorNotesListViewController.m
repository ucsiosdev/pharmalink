//
//  SalesWorxBrandAmbassadorNotesListViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorNotesListViewController.h"

@interface SalesWorxBrandAmbassadorNotesListViewController ()

@end

@implementation SalesWorxBrandAmbassadorNotesListViewController
@synthesize isViewing,notesListTableView,currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Notes"];
    
    if (isViewing) {
        
    }
    else
    {
        UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Add", nil) style:UIBarButtonItemStylePlain target:self action:@selector(addTapped)];
        [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        
        self.navigationItem .rightBarButtonItem=saveBtn;
        
    }
    
    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem .leftBarButtonItem=cancelBtn;
    
    if (currentVisit.notesArray.count>0) {
        
        [notesListTableView reloadData];
    }
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancelTapped
{
    if ([self.notesListDelegate respondsToSelector:@selector(salesWorxBrandAmbassadorsNotesList:)]) {
        
        [self.notesListDelegate salesWorxBrandAmbassadorsNotesList:currentVisit.notesArray];
        
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)addTapped
{
    SalesWorxBrandAmbassadorCreateNotesViewController * createTaskVC=[[SalesWorxBrandAmbassadorCreateNotesViewController alloc]init];
    createTaskVC.createNoteDelegate=self;
    [self.navigationController pushViewController:createTaskVC animated:YES];
}





-(void)updatedNote:(salesWorxBrandAmbassadorNotes*)currentNote;
{
    NSLog(@"created task is %@", currentNote.Title);
    
    if (currentVisit.notesArray.count==0) {
        
        currentVisit.notesArray=[[NSMutableArray alloc]init];
        [currentVisit.notesArray addObject:currentNote];
    }
    else if (currentVisit.notesArray.count>0)
    {
        if ([currentVisit.notesArray containsObject:currentNote]) {
            
            [currentVisit.notesArray replaceObjectAtIndex:selectedNoteIndexPath.row withObject:currentNote];
        }
        else{
            [currentVisit.notesArray addObject:currentNote];
        }
    }
    else
    {
        
    }
    [notesListTableView reloadData];
}
#pragma  mark UITableView Methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    tableView.separatorColor=kUITableViewSaperatorColor;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (currentVisit.notesArray.count>0) {
        
        return currentVisit.notesArray.count;
    }
    else
    {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"defaultCell";
    SalesWorxDefaultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDefaultTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    salesWorxBrandAmbassadorNotes * currentNote=[[salesWorxBrandAmbassadorNotes alloc]init];
    currentNote=[currentVisit.notesArray objectAtIndex:indexPath.row];
    cell.titleLbl.text=[SWDefaults getValidStringValue:currentNote.Title];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedNoteIndexPath=indexPath;
    SalesWorxBrandAmbassadorCreateNotesViewController * createNotesVC=[[SalesWorxBrandAmbassadorCreateNotesViewController alloc]init];
    createNotesVC.createNoteDelegate=self;
    createNotesVC.isUpdatingTask=YES;
    createNotesVC.currentNote=[currentVisit.notesArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:createNotesVC animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
