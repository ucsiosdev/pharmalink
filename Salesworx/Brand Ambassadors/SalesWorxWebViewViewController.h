//
//  SalesWorxWebViewViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxWebViewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *salesWorxWebView;
@property(strong,nonatomic) NSString* sourceUrl;
- (IBAction)closeButtonTapped:(id)sender;
- (IBAction)emailButtonTapped:(id)sender;

@end
