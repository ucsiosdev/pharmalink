//
//  SalesWorxDefaultTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxDefaultTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *titleLbl;
@end
