//
//  SalesWorxBrandAmbassadorEndCallViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorEndCallViewController.h"
#import "SalesWorxImageBarButtonItem.h"
@interface SalesWorxBrandAmbassadorEndCallViewController ()

@end

@implementation SalesWorxBrandAmbassadorEndCallViewController
@synthesize currentVisit,samplesandSellOutArray, isFromDashboard;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"End Call"];
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    tasksButton.layer.borderWidth=1.0f;
    notesButton.layer.borderWidth=1.0f;
    tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    
    
    notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    notesButton.layer.shadowOffset = CGSizeMake(2, 2);
    notesButton.layer.shadowOpacity = 0.1;
    notesButton.layer.shadowRadius = 1.0;
    
    
    
    tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
    tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
    tasksButton.layer.shadowOpacity = 0.1;
    tasksButton.layer.shadowRadius = 1.0;
    
    visitStartTimeLbl.text=currentVisit.currentCallStartTime;
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"hh:mm a"];
    [formatterTime setLocale:usLocaleq];
    NSString *callEndDate =  [formatterTime stringFromDate:[NSDate date]];
    
    visitEndTimeLbl.text=callEndDate;
    
    endTimeTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startEndTimeTimer) userInfo:nil repeats:YES];

        waitTimeLbl.text=[SWDefaults getValidStringValue:currentVisit.waitTime];

    // Do any additional setup after loading the view from its nib.
    
    SalesWorxImageBarButtonItem* endCallButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_EndVisit"] style:UIBarButtonItemStylePlain target:self action:@selector(endCallTapped)];
    self.navigationItem.rightBarButtonItem=endCallButton;
    
    
    camelLabelsView.tickCount=10;
    camelLabelsView.ticksDistance=75;
    camelLabelsView.value=0;
    camelLabelsView.upFontName=@"WeblySleekUISemibold";
    camelLabelsView.upFontSize=20;
    camelLabelsView.upFontColor=kNavigationBarBackgroundColor;
    camelLabelsView.downFontName=@"WeblySleekUISemibold";
    camelLabelsView.downFontSize=20;
    camelLabelsView.downFontColor=[UIColor lightGrayColor];
    camelLabelsView.backgroundColor=[UIColor clearColor];
    
    
    descreteSliderView.tickStyle=1;
    descreteSliderView.tickSize = CGSizeMake(1, 8);
    descreteSliderView.tickCount=10;
    descreteSliderView.trackThickness=2;
    descreteSliderView.incrementValue=0;
    descreteSliderView.minimumValue=0;
    descreteSliderView.value=0;
    [descreteSliderView addTarget:self action:@selector(customSliderValueChanged) forControlEvents:UIControlEventValueChanged];
    
    descreteSliderView.tintColor=kNavigationBarBackgroundColor;
    descreteSliderView.ticksListener=camelLabelsView;
}

#pragma mark Slider Methods
-(void)customSliderValueChanged {
    
    visitRating = [NSString stringWithFormat:@"%d",[[NSNumber numberWithFloat:descreteSliderView.value+1] intValue]];
    currentVisit.Visit_Rating = [SWDefaults getValidStringValue:visitRating];
}

#pragma mark
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    locationLbl.text=[MedRepDefaults getDefaultStringForEmptyString:currentVisit.selectedLocation.Location_Name];
    
    waitTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    visitStartTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    visitEndTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);

    
    
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kBrandAmbassadorEndVisit];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    //refine samples and sellout given
    
     currentCustomerSamplesGivenArray=[[currentVisit.samplesGivenArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.samplesGiven >0 OR SELF.sellOutGiven >0"]] mutableCopy];
    
    [self refreshTaskandNotesStatus:currentVisit];

    
    
    // setting default rating 5
    if (currentVisit.Visit_Rating.length>0) {
        visitRating = currentVisit.Visit_Rating;
    } else {
        visitRating = @"5";
        currentVisit.Visit_Rating = [SWDefaults getValidStringValue:visitRating];
    }
    camelLabelsView.value = [visitRating floatValue]-1;
    descreteSliderView.value = [visitRating floatValue]-1;
}
-(void)endCallTapped
{
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
    
    currentVisit.Call_End_Date=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    
    NSString* rating=currentVisit.Visit_Rating;
    
    
    NSPredicate * updatedNotesPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated=='YES'"];
    NSLog(@"note predicate is %@ \n notes array is %@", updatedNotesPredicate,currentVisit.notesArray);
    
    NSMutableArray*  updatedNotesArray=[[currentVisit.notesArray filteredArrayUsingPredicate:updatedNotesPredicate] mutableCopy];
    
    NSLog(@"test notes array %@", updatedNotesArray);
    if ([NSString isEmpty:rating]) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select rating" withController:self];
    }
    else{
    BOOL status=[[SalesWorxBrandAmbassadorQueries retrieveManager] saveBrandAmbassadorVisit:currentVisit];
    if (status==YES) {
        
        NSLog(@"view controllers are %@", self.navigationController.viewControllers);
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       if (currentVisit.walkCustomerVisitsArray.count>0) {
                                           [currentVisit.walkCustomerVisitsArray addObject:currentVisit.walkinCustomer];
                                           
                                       }
                                       else
                                       {
                                           currentVisit.walkCustomerVisitsArray=[[NSMutableArray alloc]init];
                                       }
                                       
                                       NSLog(@"walkin customers array  before popping is %@",currentVisit.walkCustomerVisitsArray);
                                       
                                       if (isFromDashboard) {
                                           [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
                                       } else {
                                           [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
                                       }
                                       
                                       [[NSNotificationCenter defaultCenter]postNotificationName:@"WalkInCustomerVisitDidFinish" object:currentVisit];
                                       NSLog(@"BA visit insert successfull");

                                   }];
       
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Success" andMessage:@"Completed successfully" andActions:actionsArray withController:self];
        

        
              
        
    }
    else{
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:KAlertYESButtonTitle
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //[self displayEndVisit];
                                       [self endCallTapped];
                                   }];
        UIAlertAction* CancelAction = [UIAlertAction
                                       actionWithTitle:KAlertNoButtonTitle
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           if (isFromDashboard) {
                                               [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
                                           } else {
                                               [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
                                           }
                                           
                                           [[NSNotificationCenter defaultCenter]postNotificationName:@"WalkInCustomerVisitDidFinish" object:currentVisit];
                                       }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Failed" andMessage:@"Could not close this visit at the moment, would you like to try again" andActions:actionsArray withController:self];
    }
    }
    
}
}
-(void)startEndTimeTimer
{
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"hh:mm a"];
    [formatterTime setLocale:usLocaleq];
    NSString *callEndDate =  [formatterTime stringFromDate:[NSDate date]];
    
    visitEndTimeLbl.text=callEndDate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark UITableView Methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *CellIdentifier = @"samplesCell";
    
    
    SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    cell.productTitleLbl.text=NSLocalizedString(@"Product Name", nil);
    cell.samplesGivesLbl.text=NSLocalizedString(@"Samples", nil);
    cell.sellOutLbl.text=NSLocalizedString(@"Sellout", nil);
    return cell.contentView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (currentCustomerSamplesGivenArray.count>0) {
        return currentCustomerSamplesGivenArray.count;
    }
    else
    {
        return 0;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    SampleProductClass* currentSelectedProduct=[[SampleProductClass alloc]init];
    //    currentSelectedProduct=[samplesGivenArray objectAtIndex:indexPath.row];
    //    [self selectedObject:currentSelectedProduct];
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"samplesCell";
    SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
    }
    
    SampleProductClass* currentProduct=[[SampleProductClass alloc]init];
    currentProduct=[currentCustomerSamplesGivenArray objectAtIndex:indexPath.row];
    
    tableView.allowsMultipleSelectionDuringEditing = NO;
    cell.productTitleLbl.text=currentProduct.productName;
    cell.samplesGivesLbl.text=[NSString stringWithFormat:@"%ld", (long)currentProduct.samplesGiven];
    cell.sellOutLbl.text=[NSString stringWithFormat:@"%ld", (long)currentProduct.sellOutGiven];
    return cell;
    
}

#pragma mark Task and Notes methods

- (IBAction)tasksButtonTapped:(id)sender
{
    
    SalesWorxBrandAmbassadorsTaskListViewController * viewController=[[SalesWorxBrandAmbassadorsTaskListViewController alloc]init];
    viewController.tasksListDelegate=self;
    viewController.currentVisit=currentVisit;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 570);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = tasksButton;
    presentationController.sourceRect =tasksButton.bounds;
    
    
    if ([SWDefaults isRTL]) {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
    }
    else
    {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionRight;
    }
    
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
    }];
}

-(void)salesWorxBrandAmbassadorsTasksList:(NSMutableArray*)tasksList
{
    if (tasksList.count>0) {
        currentVisit.tasksArray=tasksList;
    }
    [self refreshTaskandNotesStatus:currentVisit];
}

-(void)refreshTaskandNotesStatus:(SalesWorxBrandAmbassadorVisit*)currentVisitDetails
{
    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodesForBrandAmbassadorTasksandNotes:currentVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }
    
}


- (IBAction)notesButtonTapped:(id)sender
{
    SalesWorxBrandAmbassadorNotesListViewController * viewController=[[SalesWorxBrandAmbassadorNotesListViewController alloc]init];
    viewController.currentVisit=currentVisit;
    viewController.notesListDelegate=self;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 350);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = notesButton;
    presentationController.sourceRect =notesButton.bounds;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
    }];
}
-(void)salesWorxBrandAmbassadorsNotesList:(NSMutableArray*)notesList
{
    if (notesList.count>0) {
        currentVisit.notesArray=notesList;
        
    }
    [self refreshTaskandNotesStatus:currentVisit];
}

#pragma mark UITextView Methods
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==visitConclusionNotesTextView) {
        currentVisit.Visit_Conclusion_Notes=[SWDefaults getValidStringValue:textView.text];
    }
}

#pragma mark UITextField Method

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==customerNameTextField) {
        currentVisit.walkinCustomer.Customer_Name=[SWDefaults getValidStringValue:textField.text];
    }
    else if (textField==emailAddressTextField)
    {
        
        BOOL emailValid=[MedRepDefaults validateEmail:emailAddressTextField.text];
        
        if (emailValid==YES) {
            currentVisit.walkinCustomer.Customer_Email=[SWDefaults getValidStringValue:textField.text];
        }
        else
        {
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {

                                       }];
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
            [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:KInvalidEmailAlertTitleStr andMessage:KInvalidEmailAlertMessageStr andActions:actionsArray withController:self];
        }
    }
    else if (textField==mobileNumberTextField)
    {
        currentVisit.walkinCustomer.Customer_Phone=[SWDefaults getValidStringValue:textField.text];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([newString length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if([newString isEqualToString:@" "])
    {
        return NO;
    }
    if(textField==customerNameTextField)
    {
        return ( newString.length<kCustomerNametextFieldLimit);
    }
    else if (textField==mobileNumberTextField)
    {
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=kCustomerPhonetextFieldLimit);
    }
    else if (textField==emailAddressTextField)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:EMAIL_ACCEPTABLE_CHARECTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (![string isEqualToString:filtered]) {
            return NO;
        }
        else
        {
            return (newString.length<kCustomerEmailtextFieldLimit);
        }
    }
    return YES;
}



@end
