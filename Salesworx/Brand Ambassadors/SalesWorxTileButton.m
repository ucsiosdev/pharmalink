//
//  SalesWorxTileButton.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxTileButton.h"

@implementation SalesWorxTileButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    self.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(2, 2);
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowRadius = 1.0;
}

@end
