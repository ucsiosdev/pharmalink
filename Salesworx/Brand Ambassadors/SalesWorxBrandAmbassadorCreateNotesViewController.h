//
//  SalesWorxBrandAmbassadorCreateNotesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import <NSString+Additions.h>

@protocol SalesWorxBrandAmbassadorUpdatedNoteDelegate <NSObject>

-(void)updatedNote:(salesWorxBrandAmbassadorNotes*)currentNote;

@end
@interface SalesWorxBrandAmbassadorCreateNotesViewController : UIViewController
{
    id createNoteDelegate;

}
@property (strong, nonatomic) IBOutlet MedRepTextField *titleTextField;
@property (strong, nonatomic) IBOutlet MedRepTextView *descriptionTextView;

@property(strong,nonatomic)salesWorxBrandAmbassadorNotes * currentNote;


@property(nonatomic)     id createNoteDelegate;
@property(nonatomic) BOOL isUpdatingTask;


@end
