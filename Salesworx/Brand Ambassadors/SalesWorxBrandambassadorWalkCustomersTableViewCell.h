//
//  SalesWorxBrandambassadorWalkCustomersTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWDefaults.h>

#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxBrandambassadorWalkCustomersTableViewCell : UITableViewCell
@property(nonatomic) BOOL isHeader;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *timeofMeetingLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *samplesLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *selloutLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *facetimeLbl;

@end
