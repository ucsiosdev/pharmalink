//
//  SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepProductsCollectionViewCell.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "SalesWorxBrandAmbassadorQueries.h"
#import "SalesWorxCustomClass.h"
#import "SwipeView.h"
#import "SalesWorxCustomClass.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
#import <AVFoundation/AVFoundation.h>
#import "ReaderDocument.h"
#import "ReaderViewController.h"
#import "SalesWorxWebViewViewController.h"
#import "YStepperView.h"
#import "SalesWorxBrandAmbassadorSamplesandSellOutViewController.h"
#import <MessageUI/MessageUI.h>
#import "SalesWorxItemDividerLabel.h"


#define VIEW_FOR_ZOOM_TAG (1)
#define kSelectedMediaTypeImage @"Image"
#define kSelectedMediaTypeVideo @"Video"
#define kSelectedMediaTypeBrochure @"Brochure"
#define kSelectedMediaTypePowerpoint @"Powerpoint"



@protocol SalesWorxBrandAmbassadorDemoedMediaDelegate <NSObject>

-(void)demoedMedia:(NSMutableArray*)demonstratedMedia withSamplesGiven:(NSMutableArray*)samplesandSellOutGivenArray;

@end

@interface SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController : UIViewController<UIScrollViewDelegate,ReaderViewControllerDelegate,UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,YStepperViewDelegate,UIPopoverPresentationControllerDelegate,MFMailComposeViewControllerDelegate>

{
    id demoedMediaDelegate;
    
    IBOutlet UIView *samplesView;
    IBOutlet UIView *samplesStepperContainerView;
    
    IBOutlet UIView *sellOutView;
    
    IBOutlet UIView *mediaContainerView;
    
    IBOutlet UIView *sellOutStepperContainerView;
    IBOutlet UITextView *productDescriptionTextView;
    NSString*documentsDirectory;
    NSString*firstFileName;
    MPMoviePlayerViewController * moviePlayer;
    IBOutlet UIView *moviePlayerView;
    IBOutlet UIView *imagesView;
    IBOutlet UIImageView *moviePlayerThumbnailImageView;
    NSString * selectedMediaType;
    IBOutlet UIImageView *movieThumbnailImageView;
    NSMutableArray *viewedVideos;
    IBOutlet UIView *pdfView;
    
    IBOutlet UIImageView *nomediaImageView;
    IBOutlet UIView *presentationView;
    IBOutlet UIWebView *pdfWebView;
    IBOutlet SalesWorxItemDividerLabel *sampleSellOutDividerLabel;
    IBOutlet UILabel *titleTextView;
    IBOutlet UIView *segmentContainerView;
    NSInteger selectedCellIndex;
    NSInteger selectedSegmentIndex;
    NSMutableArray * segmentMenuArray;
    IBOutlet SwipeView *mediaSwipeView;
    IBOutlet HMSegmentedControl *segmentControl;
    
    NSMutableArray *demoPlanObjectsArray;
    
    NSMutableArray* collectionViewDataArray;
    NSMutableArray *imagesPathArray;
    
    IBOutlet UICollectionView *demoPlanMediaCollectionView;
    
    NSMutableArray * mediaContentArray;
    
    UIBarButtonItem *closeButton;
    DemoPlan * selectedDemoPlan;
    ProductMediaFile* SelectedProductMediaFile;
    NSMutableArray * demoedMediaFiles;
    
    NSMutableArray * viewedImages;
    UIView * zoomableView;
    UIImageView *imageForZooming;
    UIScrollView *pageScrollView;
    UIScrollView *currentPageScrollView;
    NSIndexPath * selectedCellIndexPath;
    BOOL isEmailTapped;
    NSMutableArray* selectedIndexPathArray;
    BOOL sendEmail;
    NSTimer* demoTimer;
    NSString* mediaType;
    UILabel *titleLabel;
    NSString* pdfFileName;
    NSString* pdfFilePath;
    IBOutlet UIWebView *presentationWebView;
    
    ProductMediaFile* currentMediaFile;
    
    BOOL comingFromSegment;
    
    YStepperView* samplesStepperView,*selloutStepperView;

    NSInteger samplesGivenCount;
    NSInteger sellOutGivenCount;
    
    SampleProductClass *selectedProduct;
    
    NSInteger selectedProductIndexforSamples;
    
    NSString* previousSelectedProductID;
    
    UIBarButtonItem * emailBarButtonItem;
    UIBarButtonItem * viewSellOutBarButtonItem;
    UIBarButtonItem * sendbarButtonItem;
    UIBarButtonItem * cancelBarButtonItem;
    
    NSMutableArray* emailableContentArray;
    
    
    
    //Textfields
    
    
    




}
@property(strong,nonatomic) NSMutableArray* VisitSamplesProductsArray;
@property(nonatomic) id demoedMediaDelegate;
@property(strong,nonatomic)NSMutableArray * productMediaFilesArray;
@property(strong,nonatomic)     ProductMediaFile* initialSelectedMediaFile;
@property (strong, nonatomic) IBOutlet UIPageControl *customPageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *imagesScrollView;
@property(strong,nonatomic) NSIndexPath * selectedImageIndexPath;
@property(nonatomic)    NSInteger currentImageIndex;

@property(nonatomic,strong)    NSString *selectedMediaType;
@property(nonatomic)    NSInteger selectedMediaFileIndex;

@property(nonatomic,strong)    NSString *selectedMediaFileID;



@property(strong,nonatomic)NSMutableArray *productImagesArray;
@property(strong,nonatomic)NSMutableArray * productVideoFilesArray;
@property(strong,nonatomic)NSMutableArray *productPdfFilesArray;
@property(strong,nonatomic)NSMutableArray * presentationsArray;




@end
