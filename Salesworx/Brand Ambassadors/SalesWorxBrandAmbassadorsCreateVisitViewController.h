//
//  SalesWorxBrandAmbassadorsCreateVisitViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWDefaults.h>
#import "NSObject+NullDictionary.h"
#import "SWDatabaseManager.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SalesWorxBrandAmbassadorContentFilterViewController.h"
#import "MedRepQueries.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "SalesWorxBrandAmbassadorQueries.h"

#define kLocationTextField @"Location"
#define kDemoPlanTextField @"Demo Plan"
#define kDateTextField @"Date"
#define kObjectiveTextView @"Objective"


@protocol CreateBrandAmbassadorVisit <NSObject>

-(void)brandAmbassadorVisitCreated:(BOOL)status;
-(void)createVisitDidCancel;

@end

@interface SalesWorxBrandAmbassadorsCreateVisitViewController : UIViewController<UITextFieldDelegate,SalesWorxSelectedObjectDelegate,UITextViewDelegate>
{
    NSMutableArray *unfilteredCustomersArray, *customersArray,*demoPlanArray;
    NSString* selectedTextField;
    IBOutlet MedRepTextField *locationTextField;
    IBOutlet MedRepTextField *dateTextField;
    IBOutlet MedRepTextField *demoPlanTextField;
    IBOutlet MedRepTextView *objectiveTextView;
    
    CGRect oldFrame;
    id brandAmbassadorDelegate;
    NSMutableArray * locationsArray;
}

@property(nonatomic) id brandAmbassadorDelegate;
- (IBAction)datePickerButtonTapped:(id)sender;
@property(nonatomic) BOOL isEditingVisit;

@property(nonatomic,strong)SalesWorxBrandAmbassadorVisit * baCreateVisit;

@end
