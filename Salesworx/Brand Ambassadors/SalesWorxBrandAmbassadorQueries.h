//
//  SalesWorxBrandAmbassadorQueries.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <sqlite3.h>
#import "SWQuery.h"
#import "SWSection.h"
#import "Singleton.h"
#import "CustomerShipAddressDetails.h"
#import <CoreLocation/CoreLocation.h>
#import "XMLWriter.h"
#import "SWPlatformDatabaseConstants.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "SWPlatform.h"
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"
#import <NSDictionary+Additions.h>
#import "NSObject+NullDictionary.h"
#import "FMDatabase.h"
#import "FMDBHelper.h"
#import "SalesWorxCustomClass.h"
#import "MedRepQueries.h"

@interface SalesWorxBrandAmbassadorQueries : NSObject

+ (SalesWorxBrandAmbassadorQueries *)retrieveManager;
- (NSMutableArray *)fetchDataForQuery:(NSString *)sql;
- (FMDatabase *)getDatabase;
-(NSMutableArray*)fetchLocationsForBrandAmbassadorVisit;
-(NSMutableArray*)fetchAllBrandAmbassadorVisitsforFilter;
- (BOOL)executeQuery:(NSString *)sqlQuery;


//create brand Ambassador visit
-(BOOL)createBrandAmbassadorVisit:(SalesWorxBrandAmbassadorVisit*)visitDetails;
-(NSMutableArray*)fetchBrandAmbassadorVisitforSelectedDate:(NSString*)date;
-(NSString*)rescheduleBrandAmbassadorVisit:(SalesWorxBrandAmbassadorVisit *)visitDetails;
-(BOOL)saveBrandAmbassadorVisitWithOutCalls:(SalesWorxBrandAmbassadorVisit*)currentVisit;
-(BOOL)saveBrandAmbassadorVisit:(SalesWorxBrandAmbassadorVisit*)currentVisitDetails;
-(BOOL)updateVisitCompletionStatus:(SalesWorxBrandAmbassadorVisit*)currentVisit;
-(NSString*)fetchIssueNotewithVisitID:(NSString*)visitID  andSessionID:(NSString*)sessionID andLotType:(NSString*)lotType;

-(NSInteger)fetchCurrentVisitCallsCount:(SalesWorxBrandAmbassadorVisit*)currentVisit;
-(NSMutableArray*)fetchWalkInCustomers:(SalesWorxBrandAmbassadorVisit*)currentVisit;

-(NSString*)createBrandAmbassadorActualVisit:(SalesWorxBrandAmbassadorVisit*)currentVisit;

-(BOOL)updateVisitEndDateToActualVisits:(SalesWorxBrandAmbassadorVisit*)currentVisit;

@end
