//
//  SalesWorxBrandAmbassadorEdetailingStartCallViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxBrandAmbassadorQueries.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"
#import "SalesWorxBrandAmbassadorsTaskListViewController.h"
#import "SalesWorxBrandAmbassadorNotesListViewController.h"
#import "SalesWorxImageView.h"
#import "SalesWorxTileButton.h"
#import "MedRepProductsCollectionViewCell.h"
#import "MedRepCollectionViewCheckMarkViewController.h"
#import "MedRepProductImagesViewController.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepProductHTMLViewController.h"
#import "MedRepEDetailingProductsViewController.h"
#import "SalesWorxBrandAmbassadorContentFilterViewController.h"
#import "YStepperView/YStepperView.h"
#import "SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell.h"
#import "SalesWorxSurveyViewController.h"
#import "SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController.h"
#import "SalesWorxBrandAmbassadorEndCallViewController.h"
#import "SalesWorxCoachReportPharmacyDetailsViewController.h"


@interface SalesWorxBrandAmbassadorEdetailingStartCallViewController : UIViewController<UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate,YStepperViewDelegate,SalesWorxBrandAmbassadorDemoedMediaDelegate>
{
    IBOutlet MedRepTextField *productTextField;
    IBOutlet UILabel *faceTimeLbl;
    IBOutlet MedRepElementDescriptionLabel *locationLbl;
    IBOutlet SalesWorxImageView *notesStatusImageView;
    IBOutlet SalesWorxImageView *taskStatusImageView;
    IBOutlet SalesWorxTileButton *notesButton;
    IBOutlet SalesWorxTileButton *tasksButton;
    IBOutlet SalesWorxTileButton *surveyButton;
    IBOutlet UICollectionView *demoPlanProductsCollectionView;
   
    IBOutlet UIView *noProductSelectedView;
    IBOutlet UIView *samplesStepperContainerView;
     YStepperView *samplesStepperView;
    
    IBOutlet UIView *sellOutStepperContainerView;
    YStepperView *selloutStepperView;
    
    SampleProductClass *selectedProduct;
    
    NSTimer *faceTimeTimer;
    NSDate* timerDate;
    
    IBOutlet UITableView * samplesGivenTableView;

    UIPopoverController * baPopOverController;
    
    NSMutableArray* productVideoFilesArray;
    NSMutableArray* productPdfFilesArray;
    NSMutableArray* productDetailsArray;
    NSMutableArray* presentationsArray;
    NSMutableArray* productImagesArray;
    NSMutableArray* demoPlanMediaFiles;
    NSMutableArray* collectionViewDataArray;
    NSMutableArray* imagesPathArray;
    NSMutableArray* demoedMediaFiles;
    NSMutableArray* sampleProductsArray;
    NSMutableArray* VisitSamplesProductsArray;
    NSMutableArray* ProductIdsArray;
    NSMutableArray* ProductNamesArray;
    NSMutableArray* samplesGivenArray;
    
    NSInteger selectedCellIndex;
    
    NSInteger samplesGivenCount;
    NSInteger sellOutGivenCount;
    
    ProductMediaFile* SelectedProductMediaFile;
    IBOutlet MedRepElementDescriptionLabel *samplesGivenCountLabel;
    
    
    
    

}
- (IBAction)samplesButtonTapped:(id)sender;


- (IBAction)surveyButtonTapped:(id)sender;
- (IBAction)notesButtonTapped:(id)sender;
- (IBAction)tasksButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *imagesButton;
@property (strong, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *btnProduct;
@property (weak, nonatomic) IBOutlet UIButton *btnPresentation;
@property (strong, nonatomic) IBOutlet UIButton *pptButton;

@property(strong,nonatomic)SalesWorxBrandAmbassadorVisit * currentVisit;
@property(strong,nonatomic) NSMutableArray * productMediaFilesArray;

@property(nonatomic) BOOL isFromDashboard;

@end
