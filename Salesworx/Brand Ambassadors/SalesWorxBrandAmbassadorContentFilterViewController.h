//
//  SalesWorxBrandAmbassadorContentFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDatabaseManager.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import <NSString+Additions.h>

@protocol SalesWorxSelectedObjectDelegate <NSObject>

-(void)selectedObject:(id)selectedItem;

@end

@interface SalesWorxBrandAmbassadorContentFilterViewController : UIViewController<UISearchControllerDelegate,UISearchResultsUpdating,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

{
    UISearchController * salesWorxSearchController;
    id selectedObjectDelegate;
    IBOutlet UITableView *contentTableView;
    NSMutableArray* unfilteredArray;
}
@property(strong,nonatomic)NSMutableArray * contentArray;
@property(strong,nonatomic)NSString *keyString;
@property(strong,nonatomic) NSString* predicateType;
@property(strong,nonatomic) NSString* titleString;
@property(strong,nonatomic) id selectedObjectDelegate;
@property(nonatomic) BOOL isCreateVisit;
@property (nonatomic) BOOL hideBackButton;
@property (nonatomic) BOOL showCloseButton;

@end
