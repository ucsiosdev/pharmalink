//
//  SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *productTitleLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *samplesGivesLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *sellOutLbl;
@property(nonatomic) BOOL isHeader;


@end
