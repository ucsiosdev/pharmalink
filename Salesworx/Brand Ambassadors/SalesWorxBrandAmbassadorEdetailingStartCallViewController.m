//
//  SalesWorxBrandAmbassadorEdetailingStartCallViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorEdetailingStartCallViewController.h"
#import "SalesWorxImageBarButtonItem.h"
@interface SalesWorxBrandAmbassadorEdetailingStartCallViewController ()

@end

@implementation SalesWorxBrandAmbassadorEdetailingStartCallViewController
@synthesize currentVisit,productMediaFilesArray,imagesButton,videoButton,btnProduct,pptButton,btnPresentation, isFromDashboard;

-(void)endCallTapped
{
    
    if (samplesGivenCount>0||sellOutGivenCount>0||demoedMediaFiles.count>0) {
        
    SalesWorxBrandAmbassadorEndCallViewController * endCallVC=[[SalesWorxBrandAmbassadorEndCallViewController alloc]init];
    endCallVC.currentVisit=currentVisit;
        if (isFromDashboard) {
            endCallVC.isFromDashboard = YES;
        }
    currentVisit.EDetailing_End_Time=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    [self.navigationController pushViewController:endCallVC animated:YES];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:@"Please demo atleast one media file or give atleast one sample/sellout to continue" withController:self];
    }
}

-(void)surveyCompleted:(NSNotification*)surveyFinishNotification
{
    NSLog(@"survey did finish notification  called with object %@", surveyFinishNotification.object);
    currentVisit.walkinSurveyResponsesArray=[[NSMutableArray alloc]initWithArray:surveyFinishNotification.object];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(surveyCompleted:) name:@"SurveyDidFinishNotification" object:nil];

    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    currentVisit.EDetailing_Start_Time=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    
    UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close visit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeVisitTapped)];
    [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
   // self.navigationItem.leftBarButtonItem=closeVisitButton;
    
    // Do any additional setup after loading the view from its nib.
    
    SalesWorxImageBarButtonItem* startVisitBarButtonItem=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartCall"] style:UIBarButtonItemStylePlain target:self action:@selector(endCallTapped)];
    
    
    self.navigationItem.rightBarButtonItem=startVisitBarButtonItem;
    
    
    productVideoFilesArray =[[NSMutableArray alloc]init];
    productPdfFilesArray =[[NSMutableArray alloc]init];
    productDetailsArray=[[NSMutableArray alloc]init];
    presentationsArray=[[NSMutableArray alloc]init];
    
    demoPlanMediaFiles=[[NSMutableArray alloc]init];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (productMediaFilesArray.count>0) {
                
                productImagesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
                
                
                productVideoFilesArray= [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
                
                productPdfFilesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
                
                presentationsArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
                
                collectionViewDataArray=[[NSMutableArray alloc]init];
                //                collectionViewDataArray=productImagesArray;
                //                [demoPlanProductsCollectionView reloadData];
                
                if (productImagesArray.count==0) {
                    [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
                }
                else
                {
                    [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
                }
                if (productVideoFilesArray.count==0) {
                    [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
                }
                else
                {
                    [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
                }
                if (productPdfFilesArray.count==0) {
                    [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
                }
                
                else
                {
                    [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
                }
                
                if (presentationsArray.count==0) {
                    [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
                }
                
                else
                {
                    [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
                }
                
                [btnProduct setBackgroundImage:[UIImage imageNamed:@"MedRep_AllMediaBtnActive"] forState:UIControlStateNormal];
                btnProduct.layer.borderWidth=0.0;
                [self imagesButtonTapped:nil];
                
            }
            else
            {
                
                [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
                [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
                [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
                [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
                [btnProduct setBackgroundImage:[UIImage imageNamed:@"MedRep_AllMediaBtnInActive"] forState:UIControlStateNormal];
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
    
    imagesPathArray=[[NSMutableArray alloc]init];
    productImagesArray=[[NSMutableArray alloc]init];
    
    
    [demoPlanProductsCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
   
    tasksButton.layer.borderWidth=1.0f;
    notesButton.layer.borderWidth=1.0f;
    surveyButton.layer.borderWidth=1.0f;
    tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    surveyButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    
    
    
    notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    notesButton.layer.shadowOffset = CGSizeMake(2, 2);
    notesButton.layer.shadowOpacity = 0.1;
    notesButton.layer.shadowRadius = 1.0;
    
    
    surveyButton.layer.shadowColor = [UIColor blackColor].CGColor;
    surveyButton.layer.shadowOffset = CGSizeMake(2, 2);
    surveyButton.layer.shadowOpacity = 0.1;
    surveyButton.layer.shadowRadius = 1.0;
    

    
 
    
    
    tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
    tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
    tasksButton.layer.shadowOpacity = 0.1;
    tasksButton.layer.shadowRadius = 1.0;

    // Do any additional setup after loading the view from its nib.
}
-(void)startTimer{
    NSDate * currentTimer=timerDate;
    NSDate *now = [NSDate date];
    NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
    // NSLog(@"wait time counter ticking %@",[self stringFromTimeInterval:elapsedTimeDisplayingPoints]);
    faceTimeLbl.text=[self stringFromTimeInterval:elapsedTimeDisplayingPoints];
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kBrandAmbassadorStartCall];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    faceTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    
    
    locationLbl.text=[MedRepDefaults getDefaultStringForEmptyString:currentVisit.selectedLocation.Location_Name];
    
    if(self.isMovingToParentViewController)
    {
        timerDate=[NSDate date];
        faceTimeTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];

        sampleProductsArray=[MedRepQueries fetchSampelsForBrandAmbassadorVisit];
        if (sampleProductsArray.count==0) {
            NSLog(@"No samples");
        }
        else
            
        {
            VisitSamplesProductsArray=[self ConvertSampleProductsDataToObjects:sampleProductsArray];
            
            
            
        }
        samplesGivenArray=[[NSMutableArray alloc]init];
        [self refreshTaskandNotesStatus:currentVisit];
 
    }
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"eDetailing"];
    

}
-(void)viewDidAppear:(BOOL)animated{
    
    if (self.isMovingToParentViewController) {
        
    if (VisitSamplesProductsArray.count==1) {
        SampleProductClass *currentProduct=[[SampleProductClass alloc]init];
        currentProduct=[VisitSamplesProductsArray objectAtIndex:0];
        [self selectedObject:currentProduct];
    }
    }

}

-(NSMutableArray *)ConvertSampleProductsDataToObjects:(NSMutableArray *)sampleProducts
{
    ProductIdsArray=[[NSMutableArray alloc]init];
    ProductNamesArray=[[NSMutableArray alloc]init];
    NSMutableArray *ProductsArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<sampleProducts.count; i++) {
        NSDictionary *tempProduct=[sampleProducts objectAtIndex:i];
        SampleProductClass *product=[[SampleProductClass alloc]init];
        product.Product_Code=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Product_Code"]];
        product.productName=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Product_Name"]];
        product.QuantityEntered=@"0";
        product.ExpiryDate=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Expiry_Date"]];
        product.Used_Qty=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Used_Qty"]];
        NSString *lotQty=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Lot_Qty"]];
        NSString *usedQuantity=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Used_Qty"]];
        product.ProductId=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Product_ID"]];
        product.samplesGiven=0;
        product.sellOutGiven=0;
        
        //qty already updated while updating stock no need to do lot-used here
        
        product.productAvailbleQty=[NSString stringWithFormat:@"%ld",[lotQty integerValue]-[usedQuantity integerValue]];
        
        //product.productAvailbleQty=[NSString stringWithFormat:@"%@", lotQty];
        
        product.productLotNumber=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Lot_No"]];
        product.DisplayExpiryDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:mm:ss" destFormat:@"dd/MM/yyyy" scrString:product.ExpiryDate];
        
        
        
        
        if([product.productAvailbleQty integerValue]>0)
        {
            if(![ProductIdsArray containsObject:product.ProductId])
            {
                [ProductIdsArray addObject:product.ProductId];
                [ProductNamesArray addObject:product.productName];
                
            }
            [ProductsArray addObject:product];
            
        }
    }
    
    
    return ProductsArray;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)samplesButtonTapped:(id)sender {
    UIButton * selectedButton=sender;
    if (samplesGivenCount<0) {
        samplesGivenCount=0;
    }
    else if (samplesGivenCount==0 && selectedButton.tag==0)
    {
        selectedButton.enabled=NO;
    }
    else if (samplesGivenCount==0 && selectedButton.tag==1)
    {
        selectedButton.enabled=NO;
    }
   else if (selectedButton.tag==1) {
        
        samplesGivenCount++;
    }
    else if (selectedButton.tag==2)
    {
        samplesGivenCount--;
    }
    samplesGivenCountLabel.text= [NSString stringWithFormat:@"%ld",(long)samplesGivenCount];
}

- (IBAction)surveyButtonTapped:(id)sender {
    
    SalesWorxSurveyViewController * surveyVC=[[SalesWorxSurveyViewController alloc]init];
    //surveyVC.currentVisit=salesWorxVisit;
    surveyVC.brandAmbassadorVisit=currentVisit;
    surveyVC.isBrandAmbassadorVisit=YES;
    surveyVC.isFromVisit=YES;
    [self.navigationController pushViewController:surveyVC animated:YES];
}



#pragma mark Task and Notes methods

- (IBAction)tasksButtonTapped:(id)sender
{
    
    SalesWorxBrandAmbassadorsTaskListViewController * viewController=[[SalesWorxBrandAmbassadorsTaskListViewController alloc]init];
    viewController.tasksListDelegate=self;
    viewController.currentVisit=currentVisit;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 570);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = tasksButton;
    presentationController.sourceRect =tasksButton.bounds;
    
    
    if ([SWDefaults isRTL]) {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
    }
    else
    {
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionRight;
    }
    
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
    }];
}

-(void)salesWorxBrandAmbassadorsTasksList:(NSMutableArray*)tasksList
{
    if (tasksList.count>0) {
        currentVisit.tasksArray=tasksList;
        
    }
    [self refreshTaskandNotesStatus:currentVisit];
}

-(void)refreshTaskandNotesStatus:(SalesWorxBrandAmbassadorVisit*)currentVisitDetails
{
    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodesForBrandAmbassadorTasksandNotes:currentVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }
    
}


- (IBAction)notesButtonTapped:(id)sender
{
    SalesWorxBrandAmbassadorNotesListViewController * viewController=[[SalesWorxBrandAmbassadorNotesListViewController alloc]init];
    viewController.currentVisit=currentVisit;
    viewController.notesListDelegate=self;
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(376, 350);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = notesButton;
    presentationController.sourceRect =notesButton.bounds;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
    }];
}
-(void)salesWorxBrandAmbassadorsNotesList:(NSMutableArray*)notesList
{
    if (notesList.count>0) {
        currentVisit.notesArray=notesList;
        
    }
    [self refreshTaskandNotesStatus:currentVisit];
}


#pragma Button Actions

- (IBAction)productsButtonTapped:(id)sender {
    
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnProduct setSelected:YES];
    [btnPresentation setSelected:NO];
    
    if ([btnPresentation isSelected]) {
        
        btnPresentation .layer.borderWidth=2.0;
        btnPresentation.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnPresentation.layer.borderWidth=0.0f;
    
    
    
    MedRepEDetailingProductsViewController * edetailingProducts= [[MedRepEDetailingProductsViewController alloc]init];
    edetailingProducts.delegate=self;
    
    UINavigationController * mediaNavController=[[UINavigationController alloc]initWithRootViewController:edetailingProducts];
    
    
    [self.navigationController presentViewController:mediaNavController animated:YES completion:nil];
}

- (IBAction)btnPresentationTapped:(id)sender {
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnProduct setSelected:NO];
    [btnPresentation setSelected:YES];
    
    if ([btnPresentation isSelected]) {
        
        btnPresentation .layer.borderWidth=2.0;
        btnPresentation.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnProduct.layer.borderWidth=0.0f;
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    collectionViewDataArray=presentationsArray;
    
    
    [demoPlanProductsCollectionView reloadData];
    
}

- (IBAction)videoImagesButtonTapped:(id)sender {
    
    
    
    
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:YES];
    
    [btnPresentation setSelected:NO];
    btnPresentation.layer.borderWidth=0.0;
    
    if ([videoButton isSelected]) {
        
        videoButton .layer.borderWidth=2.0;
        videoButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    
    imagesButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productVideoFilesArray;
    
    
    // clearSelectedCells=YES;
    
    
    //                if (cell.subviews.count>1) {
    //
    //                    [[cell.subviews objectAtIndex:1]removeFromSuperview];
    //                }
    
    [demoPlanProductsCollectionView reloadData];
    
    
}

- (IBAction)pdfImagesButtonTapped:(id)sender {
    
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:YES];
    [videoButton setSelected:NO];
    
    [btnPresentation setSelected:NO];
    btnPresentation.layer.borderWidth=0.0;
    
    
    if ([pptButton isSelected]) {
        
        pptButton .layer.borderWidth=2.0;
        pptButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    
    
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productPdfFilesArray;
    
    
    [demoPlanProductsCollectionView reloadData];
}

- (IBAction)imagesButtonTapped:(id)sender {
    
    
    // clearSelectedCells=YES;
    
    
    [imagesButton setSelected:YES];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnPresentation setSelected:NO];
    
    
    
    if ([imagesButton isSelected]) {
        
        imagesButton .layer.borderWidth=2.0;
        imagesButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnPresentation.layer.borderWidth=0.0;
    
    
    
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productImagesArray;
    
    
    [demoPlanProductsCollectionView reloadData];
}


#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return  collectionViewDataArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(200, 185);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"productCell";
    
    NSString* mediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    
    
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    

    
    
    
   NSString* selectedMediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    ProductMediaFile*currentMediaFile=[[ProductMediaFile alloc]init];
    if ([selectedMediaType isEqualToString:@"Image"]) {
        currentMediaFile=[productImagesArray objectAtIndex:indexPath.row];
        
    }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypeVideo])
    {
        currentMediaFile=[productVideoFilesArray objectAtIndex:indexPath.row];
    }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypeBrochure])
    {
        currentMediaFile=[productPdfFilesArray objectAtIndex:indexPath.row];
        
    }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypePowerpoint])
    {
        currentMediaFile=[presentationsArray objectAtIndex:indexPath.row];
        
    }
    
    if (currentMediaFile.isDemoed==YES) {
        cell.selectedImgView.hidden=NO;

    }
    else{
        cell.selectedImgView.hidden=YES;

    }
    
    
    
    
    
    
    
    
    
    if (cell.subviews.count>1) {
        //[[cell.subviews objectAtIndex:1]removeFromSuperview];
    }
    
    
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    
    
    //  NSLog(@"caption is %@",[[productMediaFilesArray objectAtIndex:indexPath.row] valueForKey:@"Caption"]);
    
    
    
    //NSLog(@"collectionview data array in cell for item is %@", [collectionViewDataArray description]);
    
    // NSLog(@"collection view data array file name is %@" ,[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row] );
    
    
    
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row] ] ];
    
    //  NSLog(@"file name is %@", fileName);
    
    
    
    if ([mediaType isEqualToString:@"Image"]) {
        
        
        NSString * thumbnailImagePath=[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:[SWDefaults getValidStringValue:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]]];
        
        cell.productImageView.image=[UIImage imageWithContentsOfFile:thumbnailImagePath];
        
        
//        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
        
        cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        cell.placeHolderImageView.hidden=NO;
        
        // cell.captionBgView.hidden=NO;
        
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
        
        
        
    }
    
    else if ([mediaType isEqualToString:@"Video"])
    {
        // UIImage* thumbNailforVideo=[self generateThumbImage:fileName];
        
        
        // cell.placeHolderImageView.hidden=YES;
        
        
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        
        //cell.captionBgView.hidden=YES;
        
        
        // cell.captionLbl=[[productVideoFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        cell.captionLbl.text=[[productVideoFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        
        
    }
    
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        
        // cell.placeHolderImageView.hidden=YES;
        // cell.captionBgView.hidden=YES;
        
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        
        // cell.captionLbl=[[productPdfFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        //        UIImage* thumbImageforPdf=[self generateThumbImageforPdf:fileName];
        //
        //        cell.productImageView.image=thumbImageforPdf;
        
        cell.captionLbl.text=[[productPdfFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
        cell.captionLbl.text=[[presentationsArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    
    
    
    
    //adding check mark to selected cell
    
    MedRepCollectionViewCheckMarkViewController* checkMarkVC=[[MedRepCollectionViewCheckMarkViewController alloc]init];
    
    
   
    
    if (demoedMediaFiles.count>0) {
        
        
        
        if ([[collectionViewDataArray objectAtIndex:indexPath.row] valueForKey:@"Demoed"]!=nil) {
            
            //[cell addSubview:checkMarkVC.view];
        }
        
        /*
        for (NSInteger i=0; i<demoedMediaFiles.count; i++) {
            
            
            
            
            
            NSString* selectedFileName=[demoedMediaFiles objectAtIndex:i];
            NSString * currentFileName=[[collectionViewDataArray valueForKey:@"Media_File_ID"] objectAtIndex:indexPath.row] ;
            
            
            
            NSLog(@"selected file name : %@  current file name : %@", selectedFileName,currentFileName);
            
            
            
            
            if ([selectedFileName isEqualToString:currentFileName]) {
                
                
                cell.selectedImgView.hidden=NO;
                
                //                [cell addSubview:checkMarkVC.view];
                //                checkMarkVC.view.tag=1000;
            }
            
            else
            {
                
                // [[cell viewWithTag:1000]removeFromSuperview];
                
                //                if (clearSelectedCells==YES) {
                //                    if (cell.subviews.count>1) {
                //                    [[cell.subviews objectAtIndex:1]removeFromSuperview];
                //                    }
                //                }
                //
                //                if (cell.subviews.count>1) {
                //
                //                    [[cell.subviews objectAtIndex:1]removeFromSuperview];
                //                }
                //[checkMarkVC.view removeFromSuperview];
            }
            
        }*/
        
        
        
        
    }
    
    
    
    
    cell.productImageView.backgroundColor=[UIColor clearColor];
    
    
    if (indexPath.row== selectedCellIndex) {
        
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        
    }
    
    else
    {
        cell.layer.borderWidth=0;
        
    }
    
    return cell;
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    selectedCellIndex=indexPath.row;
    
    //    NSMutableDictionary * tempDict=[collectionViewDataArray objectAtIndex:indexPath.row];
    //
    //    [collectionViewDataArray replaceObjectAtIndex:indexPath.row withObject:tempDict];
    //    [tempDict setObject:@"Yes" forKey:@"Demoed"];
    //
    
    SelectedProductMediaFile=[collectionViewDataArray objectAtIndex:indexPath.row];
    
    SelectedProductMediaFile.isDemoed=YES;
    
    NSLog(@"data after replacing %@", [collectionViewDataArray objectAtIndex:indexPath.row]);
    
    NSString* selectedFileID=[[collectionViewDataArray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID" ] ;
    
    
    
    NSLog(@"demoed file id %@", selectedFileID);
    
    NSString* mediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row] ;

    
    SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController * productDescVC=[[SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController alloc]init];
   // productDescVC.initialSelectedMediaFile=SelectedProductMediaFile;
    //NSLog(@"product media files are %@", productMediaFilesArray);
    productDescVC.demoedMediaDelegate=self;
    productDescVC.productImagesArray=productImagesArray;
    productDescVC.productVideoFilesArray=productVideoFilesArray;
    productDescVC.productPdfFilesArray=productPdfFilesArray;
    productDescVC.presentationsArray=presentationsArray;
    productDescVC.selectedMediaFileID=selectedFileID;
    
    
    NSLog(@"sent media file index is %ld",(long)selectedCellIndex);

    productDescVC.selectedMediaFileIndex=selectedCellIndex;
    productDescVC.selectedMediaType=mediaType;
    productDescVC.productMediaFilesArray=productMediaFilesArray;
    productDescVC.VisitSamplesProductsArray=VisitSamplesProductsArray;
    
   // [self.navigationController presentViewController:productDescVC animated:YES completion:nil];
    [self.navigationController pushViewController:productDescVC animated:YES];
    
    
    
//    NSString* mediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row] ;
//    NSString*documentsDirectory;
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
//        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
//    }
//    
//    else
//    {
//        NSArray *paths = NSSearchPathForDirectoriesInDomains
//        (NSDocumentDirectory, NSUserDomainMask, YES);
//        documentsDirectory = [paths objectAtIndex:0];
//    }
//    
//    
//    
//    //make a file name to write the data to using the documents directory:
//    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row] ];
//    
//    // NSLog(@"file name is %@", fileName);
//    
//    
//    
//    if ([mediaType isEqualToString:@"Image"]) {
//        
//        
//        [imagesPathArray removeAllObjects];
//        
//        
//        
//        for (NSInteger i=0; i<productImagesArray.count; i++) {
//            
//            // [imagesPathArray addObject:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i]];
//            
//            [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i] ]];
//            
//            
//        }
//        
//        NSLog(@"product images array before selecting %@", [productImagesArray description]);
//        
//        // NSLog(@"images path array is %@", [imagesPathArray description]);
//        
//        
//        
//        MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
//        imagesVC.productImagesArray=imagesPathArray;
//        imagesVC.imagesViewedDelegate=self;
//        
//        imagesVC.productDataArray=productImagesArray;
//        imagesVC.currentImageIndex=indexPath.row;
//        imagesVC.selectedIndexPath=indexPath;
//        [self presentViewController:imagesVC animated:YES completion:nil];
//        
//        
//        
//        
//        
//        
//        
//    }
//    
//    else if ([mediaType isEqualToString:@"Video"])
//    {
//        
//        
//        
//        
//        MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
//        videosVC.videosViewedDelegate=self;
//        videosVC.productDataArray=productVideoFilesArray;
//        
//        [self.navigationController presentViewController:videosVC animated:YES completion:nil];
//        
//        
//        
//        /*
//         NSURL* documentUrl=[NSURL fileURLWithPath:fileName];
//         
//         
//         moviePlayer = [[MPMoviePlayerViewController
//         alloc]initWithContentURL:documentUrl];
//         
//         [[NSNotificationCenter defaultCenter] addObserver:self
//         selector:@selector(moviePlayBackDidFinish:)
//         name:MPMoviePlayerPlaybackDidFinishNotification
//         object:moviePlayer];
//         
//         
//         
//         
//         [self presentViewController:moviePlayer animated:YES completion:nil];*/
//        
//        
//        
//        
//    }
//    
//    else if ([mediaType isEqualToString:@"Brochure"])
//    {
//        
//        
//        
//        
//        if (productPdfFilesArray.count>0) {
//            
//            MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
//            pdfVC.pdfViewedDelegate=self;
//            
//            pdfVC.productDataArray=productPdfFilesArray;
//            
//            pdfVC.selectedIndexPath=indexPath;
//            [self presentViewController:pdfVC animated:YES completion:nil];
//            
//        }
//        
//        else
//        {
//            
//            UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No PDF files are available for the selected demo plan" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [noDataAvblAlert show];
//        }
//        
//        
//        
//        
//        
//        /*
//         MedRepProductMediaDisplayViewController * mediaDisplayVC=[[MedRepProductMediaDisplayViewController alloc]init];
//         mediaDisplayVC.filePath=fileName;
//         
//         [self presentViewController:mediaDisplayVC animated:YES completion:nil];*/
//        
//        
//        
//    }
//    
//    else if ([mediaType isEqualToString:@"Powerpoint"])
//    {
//        if (presentationsArray.count>0) {
//            MedRepProductHTMLViewController *obj = [[MedRepProductHTMLViewController alloc]init];
//            obj.HTMLViewedDelegate = self;
//            obj.productDataArray = presentationsArray;
//            obj.showAllProducts=YES;
//            obj.isObjectData=YES;
//            
//            obj.selectedIndexPath = indexPath;
//            obj.currentImageIndex=indexPath.row;
//            [self presentViewController:obj animated:YES completion:nil];
//        }
//        else
//        {
//            UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No presentation files are available " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [noDataAvblAlert show];
//        }
//    }
//    
//    
//    //  [demoPlanProductsCollectionView reloadData];
//    
    
    
}


#pragma mark Select Product Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
 
        [self textfieldDidTap:productTextField withTitle:@"Product" withContentArray:[[NSMutableArray alloc]init]];

    return NO;
}
-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withContentArray:(NSMutableArray*)contentArray
{
   
        
        SalesWorxBrandAmbassadorContentFilterViewController * contentVC=[[SalesWorxBrandAmbassadorContentFilterViewController alloc]init];
        contentVC.predicateType=kPredicateTypeContains;
        contentVC.selectedObjectDelegate=self;
    contentVC.hideBackButton=YES;
    contentVC.showCloseButton=YES;
            contentVC.contentArray= VisitSamplesProductsArray;
            contentVC.keyString=@"productName";
    contentVC.titleString=@"Samples";
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:contentVC];
    baNavController.preferredContentSize=CGSizeMake(376, 239);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = tappedTextField;
    presentationController.sourceRect =tappedTextField.bounds;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    [self presentViewController:baNavController animated:YES completion:^{
        
    }];
    
}
-(void)selectedObject:(id)selectedItem
{
    [samplesStepperView removeFromSuperview];
    [selloutStepperView removeFromSuperview];
    selectedProduct=[[SampleProductClass alloc]init];
    selectedProduct=selectedItem;
    NSLog(@"selected product name is %@",selectedProduct.productName);
    productTextField.text=[SWDefaults getValidStringValue:selectedProduct.productName];
    
    
    NSInteger selectedProductStock=[selectedProduct.productAvailbleQty integerValue];
    
    samplesStepperView=[[YStepperView alloc]initWithFrame:CGRectMake(0, 0, samplesStepperContainerView.frame.size.width, samplesStepperContainerView.frame.size.height)];
    [samplesStepperView setStepperColor:[UIColor colorWithRed:(18.0/255.0) green:(184.0/255.0) blue:(118.0/255.0) alpha:1] withDisableColor:nil];
    [samplesStepperView setTextColor:MedRepDescriptionLabelFontColor];
    samplesStepperView.yStepperViewDelegate=self;

    [samplesStepperView setTextLabelFont:MedRepElementDescriptionLabelFont];
    if (selectedProduct.samplesGiven>0) {
        [samplesStepperView setValue:(int)selectedProduct.samplesGiven];
    }
    else{
        [samplesStepperView setValue:0];
    }
    
    [samplesStepperView setSteppedTypeSamples:YES];
    [samplesStepperView setStepperRange:0 andMaxValue:(int)selectedProductStock];
    [samplesStepperContainerView addSubview:samplesStepperView];

    
    selloutStepperView=[[YStepperView alloc]initWithFrame:CGRectMake(0, 0, sellOutStepperContainerView.frame.size.width, sellOutStepperContainerView.frame.size.height)];
    [selloutStepperView setStepperColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1] withDisableColor:nil];
    [selloutStepperView setTextColor:MedRepDescriptionLabelFontColor];
    [selloutStepperView setTextLabelFont:MedRepElementDescriptionLabelFont];
    if (selectedProduct.sellOutGiven>0) {
        [selloutStepperView setValue:(int)selectedProduct.sellOutGiven];
    }
    else
    {
        [selloutStepperView setValue:0];
    }
    selloutStepperView.yStepperViewDelegate=self;
    [selloutStepperView setSteppedTypeSamples:NO];
    [selloutStepperView setStepperRange:0 andMaxValue:(int)selectedProductStock];
    [sellOutStepperContainerView addSubview:selloutStepperView];
    
    noProductSelectedView.hidden=YES;
}

-(void)updatedSamplesGivenData
{
    samplesGivenCount=[samplesStepperView getValue];
    sellOutGivenCount=[selloutStepperView getValue];
    selectedProduct.samplesGiven=samplesGivenCount;
    selectedProduct.sellOutGiven=sellOutGivenCount;

    BOOL removeProduct=NO;
    if (samplesGivenCount==0 && sellOutGivenCount==0) {
        removeProduct=YES;
    }
    
    if ([samplesGivenArray containsObject:selectedProduct]) {
        if (removeProduct==YES) {
            [samplesGivenArray removeObject:selectedProduct];
        }
    }
    else{
        [samplesGivenArray addObject:selectedProduct];
    }
    currentVisit.samplesGivenArray=samplesGivenArray;
    [samplesGivenTableView reloadData];

}
-(void)valueIncreased:(NSInteger)value sender:(id)sender
{
    [self updatedSamplesGivenData];
}
-(void)valueDecreased:(NSInteger)value sender:(id)sender
{
    [self updatedSamplesGivenData];
}


#pragma mark UITableView Methods

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *CellIdentifier = @"samplesCell";
    
    
    SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    
    cell.productTitleLbl.isHeader=YES;
    cell.samplesGivesLbl.isHeader=YES;
    cell.sellOutLbl.isHeader=YES;
    
    cell.productTitleLbl.text=NSLocalizedString(@"Product Name", nil);
    cell.samplesGivesLbl.text=NSLocalizedString(@"Samples", nil);
    cell.sellOutLbl.text=NSLocalizedString(@"Sellout", nil);
    
    
    return cell.contentView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (samplesGivenArray.count>0) {
        return samplesGivenArray.count;
    }
    else
    {
        return 0;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SampleProductClass* currentSelectedProduct=[[SampleProductClass alloc]init];
    currentSelectedProduct=[samplesGivenArray objectAtIndex:indexPath.row];
    [self selectedObject:currentSelectedProduct];


}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"samplesCell";
    SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
    }
    
  SampleProductClass* currentProduct=[[SampleProductClass alloc]init];
    currentProduct=[samplesGivenArray objectAtIndex:indexPath.row];

    tableView.allowsMultipleSelectionDuringEditing = NO;
    cell.productTitleLbl.text=currentProduct.productName;
    cell.samplesGivesLbl.text=[NSString stringWithFormat:@"%ld", (long)currentProduct.samplesGiven];
    cell.sellOutLbl.text=[NSString stringWithFormat:@"%ld", (long)currentProduct.sellOutGiven];
    return cell;
    
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [samplesGivenArray removeObjectAtIndex:indexPath.row];
    [samplesGivenTableView reloadData];
    noProductSelectedView.hidden=NO;
    productTextField.text=@"";
    
    
}

#pragma mark Demoed Media files delegate

-(void)demoedMedia:(NSMutableArray *)demonstratedMedia withSamplesGiven:(NSMutableArray *)samplesandSellOutGivenArray
{
    demoedMediaFiles=demonstratedMedia;

   // productMediaFilesArray
    
    
    NSPredicate * demoedFilesPredicate=[NSPredicate predicateWithFormat:@"SELF.isDemoed==YES"];
    NSMutableArray* demoedFilesPredicateArray=[[demonstratedMedia filteredArrayUsingPredicate:demoedFilesPredicate] mutableCopy];
    if (demoedFilesPredicateArray.count>0) {
        
        if (currentVisit.demoedArray.count>0) {
            [currentVisit.demoedArray addObjectsFromArray:demoedFilesPredicateArray];
  
        }
        else
        {
        currentVisit.demoedArray=[[NSMutableArray alloc]init];
        currentVisit.demoedArray=demoedFilesPredicateArray;
        }
        
        NSLog(@"demoed files count is %ld",(long)demoedFilesPredicateArray.count );

    }
    
    
    NSPredicate * samplesandSellOutPredicate=[NSPredicate predicateWithFormat:@"SELF.samplesGiven >0 OR SELF.sellOutGiven>0"];
    NSMutableArray* arraywithSamplesorSellout=[[samplesandSellOutGivenArray filteredArrayUsingPredicate:samplesandSellOutPredicate] mutableCopy];
    if (arraywithSamplesorSellout.count>0) {
        VisitSamplesProductsArray=samplesandSellOutGivenArray;
        currentVisit.samplesGivenArray=[[NSMutableArray alloc]init];
        samplesGivenArray=arraywithSamplesorSellout;
        currentVisit.samplesGivenArray=VisitSamplesProductsArray;


    }
    
    
    [samplesGivenTableView reloadData];
    [demoPlanProductsCollectionView reloadData];
    
    
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    NSLog(@"content array being passed %@",contentArray);
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=NO;
    if ([popOverString isEqualToString:kDoctorTitle]) {
        popOverVC.isDoctorFilter=YES;
    }
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    baPopOverController=nil;
    baPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    baPopOverController.delegate=self;
    popOverVC.popOverController=baPopOverController;
    popOverVC.titleKey=popOverString;
    
    [baPopOverController setPopoverContentSize:CGSizeMake(400, 273) animated:YES];
    [baPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
}
@end
