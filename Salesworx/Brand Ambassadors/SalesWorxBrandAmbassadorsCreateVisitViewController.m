//
//  SalesWorxBrandAmbassadorsCreateVisitViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorsCreateVisitViewController.h"

@interface SalesWorxBrandAmbassadorsCreateVisitViewController ()

@end

@implementation SalesWorxBrandAmbassadorsCreateVisitViewController

@synthesize isEditingVisit,baCreateVisit;
#pragma mark View Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self fetchCustomers];
    
    
    locationsArray=[[SalesWorxBrandAmbassadorQueries retrieveManager] fetchLocationsForBrandAmbassadorVisit];
    NSLog(@"locations are %@",[locationsArray valueForKey:@"Location_Name"]);
    
    demoPlanArray=[MedRepQueries fetchDemoPlanObjects];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Create Visit"];
    
    UIBarButtonItem * saveButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    [saveButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    

    UIBarButtonItem * cancelButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    [cancelButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=cancelButton;
    self.navigationItem.rightBarButtonItem=saveButton;
    if (isEditingVisit) {
        
        NSString* visitDate=baCreateVisit.Date;
        
        if ([NSString isEmpty:visitDate]==NO) {
            
            //this is if user dont chnage anything and tries to save
             NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:visitDate];
            baCreateVisit.Date=refinedDate;
            
        }
        
    }
    else{
        baCreateVisit=[[SalesWorxBrandAmbassadorVisit alloc]init];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kBrandAmbassadorCreateVisits];
    }
    self.navigationController.preferredContentSize=CGSizeMake(376, 400);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (isEditingVisit) {
        locationTextField.text=[SWDefaults getValidStringValue:baCreateVisit.selectedLocation.Location_Name];
        dateTextField.text=[SWDefaults getValidStringValue:baCreateVisit.Date];
        demoPlanTextField.text=[SWDefaults getValidStringValue:baCreateVisit.selectedDemoPlan.Description];
        objectiveTextView.text=[SWDefaults getValidStringValue:baCreateVisit.Objective];

        
    }
    else{
        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    
    //[popOverController dismissPopoverAnimated:YES];
    
}

#pragma mark KeyBoard methods

- (void)keyboardDidShow:(NSNotification *)notification
{
    oldFrame=self.view.frame;
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, -100, self.view.frame.size.width, self.view.frame.size.height+500);
    } completion:^(BOOL finished) {
        NSLog(@"animation completion called");
        
        [self.view layoutIfNeeded];
        NSLog(@"view frame is %@", NSStringFromCGRect(self.view.frame));
    }];
}

-(void)removeKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.view cache:YES];
    self.view.frame = oldFrame;
    [UIView commitAnimations];
}

#pragma mark db data
-(void)fetchCustomers
{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        NSMutableArray * customerListArray=[[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
        // NSLog(@"check status %@", [[customerListArray objectAtIndex:0]valueForKey:@"Cash_Cust"]);
        
        customersArray=[[NSMutableArray alloc]init];
        if (customerListArray.count>0) {
            //doing fast enumeration and modifying the dict so added __strong
            for (NSMutableDictionary   * currentDict in customerListArray) {
                
                NSMutableDictionary   * currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                Customer * currentCustomer=[[Customer alloc] init];
                currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
                currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
                currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
                currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
                currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
                currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
                currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
                currentCustomer.City=[currentCustDict valueForKey:@"City"];
                currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
                currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
                currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
                currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
                currentCustomer.Cust_Lat=[currentCustDict valueForKey:@"Cust_Lat"];
                currentCustomer.Cust_Long=[currentCustDict valueForKey:@"Cust_Long"];
                currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
                currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
                currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
                currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
                currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
                currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
                currentCustomer.Customer_Segment_ID=[currentCustDict valueForKey:@"Customer_Segment_ID"];
                currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
                currentCustomer.Dept=[currentCustDict valueForKey:@"Dept"];
                currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
                currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
                currentCustomer.Postal_Code=[currentCustDict valueForKey:@"Postal_Code"];
                currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
                currentCustomer.SalesRep_ID=[currentCustDict valueForKey:@"SalesRep_ID"];
                currentCustomer.Sales_District_ID=[currentCustDict valueForKey:@"Sales_District_ID"];
                currentCustomer.Ship_Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]];
                currentCustomer.Ship_Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]];
                currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
                currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
                
                
                currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:@"CUST_LOC_RNG"];
                
                [customersArray addObject:currentCustomer];
                
            }
            
        }
        unfilteredCustomersArray=[[NSMutableArray alloc]init];
        unfilteredCustomersArray=customersArray;
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            
        });
    });
}

#pragma mark UITextView Methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    baCreateVisit.Objective=textView.text;
}

#pragma mark UITextField Methods


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==locationTextField) {
        
        selectedTextField=kLocationTextField;
        
        if (isEditingVisit) {
            
            NSMutableArray * selectedLocationArray=[[NSMutableArray alloc]initWithObjects:baCreateVisit.selectedLocation, nil];
            
            [self textfieldDidTap:locationTextField withTitle:kLocationTextField withContentArray:selectedLocationArray];

        }
        else{
        [self textfieldDidTap:locationTextField withTitle:kLocationTextField withContentArray:locationsArray];
        }
        
    }
    else if (textField==demoPlanTextField)
    {
        selectedTextField=kDemoPlanTextField;
        [self textfieldDidTap:demoPlanTextField withTitle:kDemoPlanTextField withContentArray:demoPlanArray];
        
    }
    else if (textField==dateTextField)
    {
        selectedTextField=kDateTextField;
        [self textfieldDidTap:dateTextField withTitle:kDateTextField withContentArray:[[NSMutableArray alloc]init]];
    }
    return NO;
}


-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withContentArray:(NSMutableArray*)contentArray
{
    
    if (tappedTextField == dateTextField) {
        MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        dateFilterVC.datePickerMode=@"dateandTime";
        dateFilterVC.isBrandAmbassadorVisit=YES;
        dateFilterVC.visitDateDelegate=self;
        [self.navigationController pushViewController:dateFilterVC animated:YES];
    }
    else
    {
        SalesWorxBrandAmbassadorContentFilterViewController * contentVC=[[SalesWorxBrandAmbassadorContentFilterViewController alloc]init];
        contentVC.isCreateVisit=YES;
        contentVC.predicateType=kPredicateTypeContains;
        contentVC.selectedObjectDelegate=self;
        if (tappedTextField==locationTextField) {
            contentVC.keyString=@"Location_Name";
            if (isEditingVisit) {
                contentVC.contentArray=contentArray;
            }
            else{
            contentVC.contentArray=locationsArray;
            }
        }
        else if (tappedTextField==demoPlanTextField)
        {
            contentVC.contentArray=demoPlanArray;
            contentVC.keyString=@"Description";
        }
        [self.navigationController pushViewController:contentVC animated:YES];

    }
}

#pragma mark Delegate methods

-(void)selectedObject:(id)selectedItem
{
    if ([selectedTextField isEqualToString:kLocationTextField]) {
        
        SalesWorxBrandAmbassadorLocation * selectedLocation=selectedItem;
        NSLog(@"selected location is %@", selectedLocation.Location_Name);
        locationTextField.text=[SWDefaults getValidStringValue:selectedLocation.Location_Name];
        baCreateVisit.selectedLocation=selectedLocation;
        
    }
    else if ([selectedTextField isEqualToString:kDemoPlanTextField])
    {
        DemoPlan * selectedDemoPlan=selectedItem;
        demoPlanTextField.text=[SWDefaults getValidStringValue:selectedDemoPlan.Description];
        baCreateVisit.selectedDemoPlan=selectedDemoPlan;
        
    }
}
-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    NSString * selectedVisitDate= [MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDateFormatWithTime scrString:[SWDefaults getValidStringValue:selectedDate]];
   
    if ([NSString isEmpty:selectedVisitDate]==NO) {
        dateTextField.text=selectedVisitDate;
        baCreateVisit.Date=selectedVisitDate;
        
    }
}


#pragma mark Save and Cancel Methods

-(void)saveTapped
{
    [self.view endEditing:YES];
    
    NSString* missingString=[[NSString alloc]init];
    
    if ([NSString isEmpty:locationTextField.text]) {
        
        missingString=kLocationTextField;
    }
    else if ([NSString isEmpty:dateTextField.text]) {
        
        missingString=kDateTextField;
    }
   else if ([NSString isEmpty:demoPlanTextField.text]) {
        
        missingString=kDemoPlanTextField;
    }
   else if ([NSString isEmpty:objectiveTextView.text]) {
       
       missingString=kObjectiveTextView;
   }
    
    
    if ([NSString isEmpty:missingString]==NO) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please enter %@",missingString] withController:self];
        
    }
    else
    {
    
    
    if ([self.brandAmbassadorDelegate respondsToSelector:@selector(brandAmbassadorVisitCreated:)]) {
        
        //save data to database
        
        BOOL updateOldVisitDetails=NO;
        if(isEditingVisit)
        {
            
            updateOldVisitDetails=[MedRepQueries updateVisitDetailsToOldvisit:baCreateVisit.Planned_Visit_ID];
            baCreateVisit.Old_Visit_ID=baCreateVisit.Planned_Visit_ID;
            
        }

        
        
        BOOL visitStatus=[[SalesWorxBrandAmbassadorQueries retrieveManager] createBrandAmbassadorVisit:baCreateVisit];
        if (visitStatus==YES) {
            
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [self.brandAmbassadorDelegate brandAmbassadorVisitCreated:visitStatus];
                                           [self dismissViewControllerAnimated:YES completion:nil];

                                       }];
       
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,nil];
        
            if (isEditingVisit) {
                [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Success" andMessage:@"Visit Updated Successfully" andActions:actionsArray withController:self];

            }
            else{
                [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Success" andMessage:@"Visit Created Successfully" andActions:actionsArray withController:self];
 
            }
            
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Unable to create visit at the moment,please try again" withController:self];
        }
        
    }
    }

}

-(void)cancelTapped
{
    
    if ([self.brandAmbassadorDelegate respondsToSelector:@selector(createVisitDidCancel)]) {
        
        [self.brandAmbassadorDelegate createVisitDidCancel];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)datePickerButtonTapped:(id)sender {
    MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
    dateFilterVC.datePickerMode=@"dateandTime";
    dateFilterVC.visitDateDelegate=self;
    [self.navigationController pushViewController:dateFilterVC animated:YES];
}
@end
