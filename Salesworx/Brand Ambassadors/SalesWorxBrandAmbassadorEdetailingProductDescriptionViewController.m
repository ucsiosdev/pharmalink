//
//  SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController.h"

@interface SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController ()

@end

@implementation SalesWorxBrandAmbassadorEdetailingProductDescriptionViewController
@synthesize productMediaFilesArray,initialSelectedMediaFile,imagesScrollView,customPageControl,selectedImageIndexPath,currentImageIndex,VisitSamplesProductsArray,productImagesArray,productVideoFilesArray,productPdfFilesArray,presentationsArray,selectedMediaFileID;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    emailableContentArray=[[NSMutableArray alloc]init ];
    demoedMediaFiles=[[NSMutableArray alloc]init];
    for (UIView * containerView in mediaContainerView.subviews) {
        
        if ([containerView isKindOfClass:[UIView class]]) {
        if (containerView.tag==101||containerView.tag==102||containerView.tag==103||containerView.tag==104) {
            containerView.layer.borderColor = [UIColor colorWithRed:(238.0/255.0) green:(238.0/255.0) blue:(238.0/255.0) alpha:1].CGColor;
            containerView.layer.borderWidth = 2.0;
            containerView.layer.cornerRadius = 1.0;
            containerView.layer.masksToBounds=YES;
        }
        }
        
    }
    
    closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    [closeButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=closeButton;
    
    
    float delay = 0.1;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC),  dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].statusBarHidden = NO;
        
    });
    
    UITapGestureRecognizer * movieTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(movieTappedWithFullScreen:)];
    [moviePlayerView addGestureRecognizer:movieTap];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.delegate=self;
    [pdfWebView addGestureRecognizer:singleTap];
    
  
    UITapGestureRecognizer *presentationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePresentationTap:)];
    presentationTap.delegate=self;
    [presentationWebView addGestureRecognizer:presentationTap];
    
    
   
    
    
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Product Description", nil)];
    
    
    //NSLog(@"product media files are %@",productMediaFilesArray);
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    
    segmentMenuArray=[[NSMutableArray alloc]initWithObjects:@"IMAGE",@"VIDEO",@"PDF",@"PRESENTATION", nil];
    
    segmentControl.sectionTitles = segmentMenuArray;
    //    segmentControl.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    segmentControl.backgroundColor = [UIColor clearColor];
    
    segmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    segmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    
    segmentControl.selectionIndicatorBoxOpacity=0.0f;
    segmentControl.verticalDividerEnabled=YES;
    segmentControl.verticalDividerWidth=1.0f;
    segmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    segmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    segmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentControl.tag = 3;
    segmentControl.selectedSegmentIndex = 0;
    
    [demoPlanMediaCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];

    
    
    segmentContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    segmentContainerView.layer.shadowOffset = CGSizeMake(0, -2);
    segmentContainerView.layer.shadowOpacity = 0.1;
    segmentContainerView.layer.shadowRadius = 2.0;
    
    
    [segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        
       // demoPlanObjectsArray=[MedRepQueries fetchDemoPlanObjects];
        
       /*
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (productMediaFilesArray.count>0) {
                
                productImagesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
                NSLog(@"product images array in did load %@", productImagesArray);
                
                productVideoFilesArray= [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
                
                productPdfFilesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
                
                presentationsArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
                
                
                
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        
        */
    });
    
    


}




-(void)viewWillAppear:(BOOL)animated{
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    

    
    
    emailBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BA_Mail"] style:UIBarButtonItemStylePlain target:self action:@selector(sendEmail)];

    viewSellOutBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BA_Viewsellout"] style:UIBarButtonItemStylePlain target:self action:@selector(showPopOver)];

    NSMutableArray * barButtonItemsArray=[[NSMutableArray alloc]initWithObjects:emailBarButtonItem,viewSellOutBarButtonItem, nil];
    self.navigationItem.rightBarButtonItems=barButtonItemsArray;
    
    
     sendbarButtonItem=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Send", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showMailClient)];
    [sendbarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    
     cancelBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelEmail)];
    [cancelBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    
    productDescriptionTextView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0);

    if (self.isMovingToParentViewController) {
        mediaContentArray=[[NSMutableArray alloc]init];
        [mediaContentArray addObject:productImagesArray];
        [mediaContentArray addObject:productVideoFilesArray];
        [mediaContentArray addObject:productPdfFilesArray];
        [mediaContentArray addObject:presentationsArray];
        [mediaSwipeView reloadData];

    }
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:YES];
    [UIApplication sharedApplication].statusBarHidden = NO;


    if (self.isMovingToParentViewController) {
 
    viewedImages=[[NSMutableArray alloc]init];
    
    //  NSLog(@"product data array is %@",[productDataArray description]);
    
    NSInteger numberOfPages = productImagesArray.count;
    
    customPageControl.numberOfPages=productImagesArray.count;
    
    if (![customPageControl respondsToSelector:@selector(semanticContentAttribute)] && [UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
        //customPageControl.transform = CGAffineTransformMakeScale(-1, 1);
    }
    
    
    imagesScrollView.pagingEnabled = YES;
    imagesScrollView.backgroundColor=[UIColor whiteColor];
    
    
    
    
    imagesScrollView.delegate=self;
    
    imagesScrollView.contentSize = CGSizeMake(numberOfPages * imagesScrollView.frame.size.width, imagesScrollView.frame.size.height);
    

    NSInteger imageX = 0;
    
    zoomableView=[[UIView alloc]initWithFrame:imagesScrollView.frame];
    
    // NSLog(@"number of pages are %d", numberOfPages);
    
    NSString* imagePathStr;
    
     imagesScrollView.delegate=self;
  
        
        imagesScrollView.backgroundColor=[UIColor clearColor];
        
        // [imagesScrollView addGestureRecognizer:rotation];
        
        imagesScrollView.pagingEnabled = YES;
        
        imagesScrollView.showsHorizontalScrollIndicator = NO;
        imagesScrollView.showsVerticalScrollIndicator = NO;
        
        CGRect innerScrollFrame = imagesScrollView.bounds;
        
        for (NSInteger i = 0; i < numberOfPages; i++) {
            
            NSString* imagePath=[productImagesArray objectAtIndex:i];
            // NSLog(@"image path is %@", imagePath);
            
            
            
            imagePathStr=[documentsDirectory stringByAppendingPathComponent:[[productImagesArray  objectAtIndex:i]valueForKey:@"File_Name"] ];
            
            // NSLog(@"image file path %@", imagePathStr);
            
            UIImage* currentImage=[UIImage imageWithContentsOfFile:imagePathStr];
            
            
            imageForZooming = [[UIImageView alloc] initWithImage:currentImage];
            
            NSLog(@"subviews of images scroll view is %@", [imagesScrollView.subviews description]);
            
            
            
            
            [imageForZooming setFrame:CGRectMake(8, 0, imagesScrollView.frame.size.width, imagesScrollView.frame.size.height)];
            
            imageForZooming.tag = VIEW_FOR_ZOOM_TAG;
            imageForZooming.contentMode=UIViewContentModeScaleAspectFit;
            imageForZooming.backgroundColor=[UIColor clearColor];
            
            
            
            pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
            pageScrollView.minimumZoomScale = 1.0f;
            
            pageScrollView.backgroundColor=[UIColor clearColor];
            
            pageScrollView.maximumZoomScale = 6.0f;
            pageScrollView.zoomScale = 1.0f;
            pageScrollView.tag=i;
            pageScrollView.contentSize = imageForZooming.bounds.size;
            pageScrollView.delegate = self;
            pageScrollView.showsHorizontalScrollIndicator = NO;
            pageScrollView.showsVerticalScrollIndicator = NO;
            [pageScrollView addSubview:imageForZooming];
            [imagesScrollView addSubview:pageScrollView];
            
            if (i < numberOfPages-1) {
                innerScrollFrame.origin.x += innerScrollFrame.size.width;
            }
            
        }
    
    
    
    imagesScrollView.contentOffset = CGPointMake(imagesScrollView.frame.size.width * selectedImageIndexPath.row, imagesScrollView.frame.origin.y);
    
    collectionViewDataArray=[[NSMutableArray alloc]init];

    if ([self.selectedMediaType isEqualToString:kSelectedMediaTypeImage]) {
        [self reloadContentWithHeaderMedia:0];
    }
    else if ([self.selectedMediaType isEqualToString:kSelectedMediaTypeVideo])
    {
        [self reloadContentWithHeaderMedia:1];
    }
    else if ([self.selectedMediaType isEqualToString:kSelectedMediaTypeBrochure])
    {
        [self reloadContentWithHeaderMedia:2];
    }
    else if ([self.selectedMediaType isEqualToString:kSelectedMediaTypePowerpoint])
    {
        [self reloadContentWithHeaderMedia:3];
    }
    
        
        NSLog(@"received product id %@",selectedMediaFileID);
        
        /** selection*/
        for (NSInteger i=0; i<4; i++) {
            NSUInteger firstSelectedIndex = [[[mediaContentArray objectAtIndex:i] valueForKey:@"Media_File_ID"]indexOfObject:selectedMediaFileID];
            if(NSNotFound==firstSelectedIndex)
            {
                
            }
            else{
                NSLog(@"selecting segment in else");
                [mediaSwipeView scrollToItemAtIndex:i duration:0.0];
                    for (id x in [[mediaSwipeView itemViewAtIndex:i]subviews]) {
                        if([x isKindOfClass:[UICollectionView class]])
                        {
                            UICollectionView *selectedCollectionView=x;
                            [self collectionView:selectedCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:firstSelectedIndex inSection:0]];
                        }
                    
                }
            }
            
        }
        
        
    }
    
    

}


-(void)loadHmSegment
{
    
}

#pragma mark UIScrollView Methods


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (mediaContentArray.count>0) {
        NSMutableArray *tempMediaarray= [mediaContentArray objectAtIndex:collectionView.tag];
        return  tempMediaarray.count;
    }
    return 0;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(150, 150);
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)resetZoomScale
{
    for (UIScrollView *i in imagesScrollView.subviews){
        if([i isKindOfClass:[UIScrollView class]]){
            UIScrollView *zoomableScrollView = (UIScrollView *)i;
            zoomableScrollView.minimumZoomScale = 1.0f;
            zoomableScrollView.backgroundColor=[UIColor clearColor];
            zoomableScrollView.maximumZoomScale = 6.0f;
            zoomableScrollView.zoomScale = 1.0f;
            zoomableScrollView.contentSize = imageForZooming.bounds.size;
            zoomableScrollView.delegate = self;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productCell";
    
    if (selectedCellIndex>0) {
        NSLog(@"selected cell index in cell for row %ld", (long)selectedCellIndex);

    }
    
    NSMutableArray *currentMediaArray= [mediaContentArray objectAtIndex:collectionView.tag];
    
     mediaType = [[currentMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];

    
    
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    
   
    
    //make a file name to write the data to using the documents directory:
    
    ProductMediaFile*   currentFile;

    currentFile=[currentMediaArray objectAtIndex:indexPath.row];
    if (currentFile.isEmailed==YES) {
        cell.selectedImgView.hidden=NO;
        
    }
    else{
        cell.selectedImgView.hidden=YES;
        
    }
    
    if ([mediaType isEqualToString:kSelectedMediaTypeImage])
    {
       // cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
       
        NSString * thumbnailImagePath=[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:[SWDefaults getValidStringValue:[[currentMediaArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]]];
        
        cell.productImageView.image=[UIImage imageWithContentsOfFile:thumbnailImagePath];
        
        
        cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
        
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
        if (indexPath.row==currentImageIndex) {
            
        }
        else
        {
            [self resetZoomScale];
        }
        if (indexPath.row== selectedCellIndex) {
            
            cell.layer.borderWidth=2.0;
            cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        }
        else
        {
            cell.layer.borderWidth=0;
        }
        
        }
    else if ([mediaType isEqualToString:kSelectedMediaTypeVideo])
    {
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        cell.captionLbl.text=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        currentFile=[currentMediaArray objectAtIndex:indexPath.row];

        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        if (indexPath.row== selectedCellIndex) {
            
            cell.layer.borderWidth=2.0;
            cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        }
        else
        {
            cell.layer.borderWidth=0;
        }


    }
    else if ([mediaType isEqualToString:kSelectedMediaTypeBrochure])
    {
        currentFile=[currentMediaArray objectAtIndex:indexPath.row];

        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        cell.captionLbl.text=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        if (indexPath.row== selectedCellIndex) {
            
            cell.layer.borderWidth=2.0;
            cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        }
        else
        {
            cell.layer.borderWidth=0;
        }

    }
    else if ([mediaType isEqualToString:kSelectedMediaTypePowerpoint])
    {
        currentFile=[currentMediaArray objectAtIndex:indexPath.row];
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
        cell.captionLbl.text=[[currentMediaArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        if (indexPath.row== selectedCellIndex) {
            
            cell.layer.borderWidth=2.0;
            cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        }
        else
        {
            cell.layer.borderWidth=0;
        }

    }
    

    cell.productImageView.backgroundColor=[UIColor clearColor];
    
    return cell;
}


-(void)updatedDemoedStatustoCurrentMediaFile:(ProductMediaFile*)mediaFileToUpdate
{
    
    NSInteger matchedIndex;
    NSPredicate *demoedFilePredicate=[NSPredicate predicateWithFormat:@"SELF.Entity_ID_1 ==%@",mediaFileToUpdate.Entity_ID_1];
    matchedIndex=[productMediaFilesArray indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [demoedFilePredicate evaluateWithObject:obj];
        
    }];
    
    NSLog(@"replacing medi file %@", mediaFileToUpdate);
    [productMediaFilesArray replaceObjectAtIndex:matchedIndex withObject:mediaFileToUpdate];
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (mediaContentArray.count>0) {
     selectedCellIndexPath=indexPath;
    
        nomediaImageView.hidden=YES;
        
        
        NSMutableArray *currentMediaArray= [mediaContentArray objectAtIndex:collectionView.tag];
        
        
        
        selectedMediaType = [[currentMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];

    if ([selectedMediaType isEqualToString:@"Image"]) {
        currentMediaFile=[currentMediaArray objectAtIndex:indexPath.row];
        currentImageIndex=indexPath.row;

    }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypeVideo])
    {
        currentMediaFile=[currentMediaArray objectAtIndex:indexPath.row];
    }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypeBrochure])
    {
        currentMediaFile=[currentMediaArray objectAtIndex:indexPath.row];

    }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypePowerpoint])
    {
        currentMediaFile=[currentMediaArray objectAtIndex:indexPath.row];

    }
    
    if (sendEmail==YES&&comingFromSegment==NO) {
        
        if (currentMediaFile.isEmailed==YES) {
            currentMediaFile.isEmailed=NO;
            [emailableContentArray removeObject:currentMediaFile];

        }
        else{
            [emailableContentArray addObject:currentMediaFile];

        currentMediaFile.isEmailed=YES;
            
        }
    }
    else{
        comingFromSegment=NO;
    }

    currentMediaFile.isDemoed=YES;

    
    [self updatedDemoedStatustoCurrentMediaFile:currentMediaFile];
    
    
    
    [self addSteppers];
        
        MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];

        
        if ([selectedMediaType isEqualToString:@"Image"])
        {
            imagesView.hidden=NO;
            moviePlayerView.hidden=YES;
            pdfView.hidden=YES;
            presentationView.hidden=YES;

            
            if ([selectedIndexPathArray containsObject:indexPath]) {
            }
            else
            {
                [selectedIndexPathArray addObject:indexPath];
                //sendEmail=YES;
                //[emailButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
            }

  
        }
        else if ([selectedMediaType isEqualToString:kSelectedMediaTypeVideo])
        {
            
            imagesView.hidden=YES;
            moviePlayerView.hidden=NO;
            pdfView.hidden=YES;
            presentationView.hidden=YES;
            
            [collectionView setAllowsMultipleSelection:NO];
            
            NSString*filePathStr;
            
            
            filePathStr=[documentsDirectory stringByAppendingPathComponent:currentMediaFile.Filename ];
            firstFileName=currentMediaFile.File_Path;
            
            moviePlayerThumbnailImageView.image=nil;
            moviePlayerThumbnailImageView.hidden=NO;
            movieThumbnailImageView.hidden=NO;
            [moviePlayer.moviePlayer stop];
            [moviePlayer.view removeFromSuperview];
        
            [self reloadHeaderMoviewView:firstFileName];

            
            // [self playMoviewithFileName:selectedFileName];
            
        }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypeBrochure])
    {
        
        imagesView.hidden=YES;
        moviePlayerView.hidden=YES;
        pdfView.hidden=NO;
        presentationView.hidden=YES;
        
        
        NSURL *targetURL = [NSURL fileURLWithPath:currentMediaFile.File_Path];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [pdfWebView loadRequest:request];
        pdfFilePath=currentMediaFile.File_Path;
        pdfFileName=currentMediaFile.File_Name;
    }
    else if ([selectedMediaType isEqualToString:kSelectedMediaTypePowerpoint])
    {
        imagesView.hidden=YES;
        moviePlayerView.hidden=YES;
        pdfView.hidden=YES;
        presentationView.hidden=NO;


        NSURL *targetURL = [NSURL fileURLWithPath:currentMediaFile.File_Path];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [presentationWebView loadRequest:request];
    }
    
        if (selectedIndexPathArray.count==0) {
            
           // [emailButton setTitle:@"Email" forState:UIControlStateNormal];
            //isEmailTapped=NO;
            
        }

        [collectionView setAllowsMultipleSelection:NO];
        
        selectedCellIndex=indexPath.row;
        
        
        if (demoTimer) {
            
            demoTimer=nil;
        }
        
        //start the timer to check if images for displayed for more than 5 secs

        [self loadProductDetailsWithSelectedIndex:indexPath.row];
        
        [self loadSelectedCellDetails:selectedCellIndex];
        customPageControl.currentPage = indexPath.row;
       
        NSLog(@"selected cell index before reloading %ld", (long)selectedCellIndex);
        
        [mediaSwipeView reloadData];
    
        
    
    
    if (selectedIndexPathArray.count==0) {
        
       // [emailButton setTitle:@"Email" forState:UIControlStateNormal];
        //isEmailTapped=NO;
        
    }
    
    
    //[self sendEmail];
    
    if (demoTimer) {
    
        demoTimer=nil;
    }

    //start the timer to check if images for displayed for more than 5 secs
    demoTimer=[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:NO];

    }
    else
    {
        
    }

}

-(void)handlePresentationTap:(UITapGestureRecognizer*)recognizer
{
    NSLog(@"presentation tapped");
    SalesWorxWebViewViewController * webView=[[SalesWorxWebViewViewController alloc]init];
    webView.sourceUrl=currentMediaFile.File_Path;
    [self.navigationController presentViewController:webView animated:YES completion:nil];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:pdfFilePath password:phrase];
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        readerViewController.fileName = pdfFileName;
    #if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
        
        [self.navigationController pushViewController:readerViewController animated:YES];
        
    #else // present in a modal view controller
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:readerViewController animated:YES completion:NULL];
        
    #endif // DEMO_VIEW_CONTROLLER_PUSH
    }
    else // Log an error so that we know that something went wrong
    {
    }
}

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    #if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
    
    [self.navigationController popViewControllerAnimated:YES];
    
    #else // dismiss the modal view controller
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    #endif // DEMO_VIEW_CONTROLLER_PUSH
}

-(void)moviePlayBackDidFinish

{
    NSLog(@"selected movie delegate called");
    
    
    [viewedVideos addObject:[productVideoFilesArray objectAtIndex:currentImageIndex]];
    [self reloadHeaderMoviewView:firstFileName];
}
-(void)movieTappedWithFullScreen:(BOOL)flag
{
    
    moviePlayerThumbnailImageView.image=nil;
    moviePlayerThumbnailImageView.hidden=YES;
    movieThumbnailImageView.hidden=YES;

    flag=YES;
    
    NSLog(@"movie player tapped");
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer.moviePlayer];
    
    NSURL* documentUrl=[NSURL fileURLWithPath:firstFileName];
    
    NSLog(@"document url is %@", documentUrl);
    
    
    moviePlayer = [[MPMoviePlayerViewController
                    alloc]initWithContentURL:documentUrl];
    
    if (flag==YES) {
        [moviePlayer.view setFrame: CGRectMake(0, 0, 554, 420)];
        moviePlayer.view.backgroundColor=[UIColor blackColor];
        
    
    }
    
    NSLog(@"movie player bounds are %@", NSStringFromCGRect(moviePlayer.view.frame));
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    
    moviePlayer.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;

    [moviePlayerView addSubview:moviePlayer.view];
    [moviePlayer.moviePlayer play];

    moviePlayerThumbnailImageView.hidden=NO;
    movieThumbnailImageView.hidden=NO;

    [UIApplication sharedApplication].statusBarHidden = NO;

}

-(void)reloadHeaderMoviewView:(NSString*)fileName
{
    NSLog(@"file name is %@", fileName);
    
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:fileName]];
    
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    
    CMTime thumbnailTime = [asset duration];
    
    thumbnailTime.value=10000;
    
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbnailTime actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    
    moviePlayerThumbnailImageView.image=thumbnail;
    CGImageRelease(imageRef);
    
}
-(void)startFaceTimeTimer
{
    currentMediaFile.isDemoed=YES;
    if ([demoedMediaFiles containsObject:currentMediaFile])
    {
        
    }
    else
    {
        [demoedMediaFiles addObject:currentMediaFile];
    }
    
    [demoTimer invalidate];
    
    demoTimer=nil;
}
-(void)loadSelectedCellDetails:(NSInteger)selectedIndex

{
       [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        
        imagesScrollView.contentOffset = CGPointMake(imagesScrollView.frame.origin.x+imagesScrollView.frame.size.width*selectedCellIndex, 0);
        
      } completion:NULL];
    
}





-(void)loadProductDetailsWithSelectedIndex:(NSInteger)selectedIndex
{
    NSString* productCodeStr=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",currentMediaFile.Product_Code]];
    NSString*productName=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",currentMediaFile.Product_Name]];
    
   // titleLabel.text=[NSString stringWithFormat:@"%@", currentMediaFile.Product_Name ];

    titleTextView.font=MedRepElementTitleLabelFont;
    titleTextView.textColor=MedRepElementTitleLabelFontColor;

    titleTextView.text=currentMediaFile.Product_Name;
    
    
    NSMutableAttributedString* productNameAttrStr =
    [[NSMutableAttributedString alloc] initWithString:productName];
    [productNameAttrStr addAttribute:NSFontAttributeName
                               value:MedRepElementTitleLabelFont
                               range:NSMakeRange(0, productNameAttrStr.length)];
    
    [productNameAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepElementTitleLabelFontColor range:NSMakeRange(0, productNameAttrStr.length)];
    
    
    NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@"\n" attributes:@{NSFontAttributeName : kSWX_FONT_SEMI_BOLD(8)}];
    [productNameAttrStr appendAttributedString:atrStr];
    
    
    NSMutableAttributedString* productAttrStr =
    [[NSMutableAttributedString alloc] initWithString:productCodeStr];
    [productAttrStr addAttribute:NSFontAttributeName
                           value:MedRepElementDescriptionLabelFont
                           range:NSMakeRange(0, productAttrStr.length)];
    
    [productAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepElementDescriptionLabelColor range:NSMakeRange(0, productAttrStr.length)];
    
    if (productCodeStr.length>0) {
        
        [productNameAttrStr appendAttributedString:productAttrStr];
        
        titleTextView.attributedText=productNameAttrStr;
        
        NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@"\n" attributes:@{NSFontAttributeName : kSWX_FONT_SEMI_BOLD(8)}];
        [productNameAttrStr appendAttributedString:atrStr];
        
        //static Description text
        
        
        NSMutableAttributedString* productNameAttrStr =
        [[NSMutableAttributedString alloc] initWithString:@"Description"];
        [productNameAttrStr addAttribute:NSFontAttributeName
                                   value:MedRepElementDescriptionLabelFont
                                   range:NSMakeRange(0, productNameAttrStr.length)];
        
        [productNameAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepElementTitleLabelFontColor range:NSMakeRange(0, productNameAttrStr.length)];
        
        //add new line
        
        NSAttributedString *atrnewLineStr = [[NSAttributedString alloc]initWithString:@"\n" attributes:@{NSFontAttributeName : kSWX_FONT_SEMI_BOLD(8)}];
        [productNameAttrStr appendAttributedString:atrnewLineStr];
        
        //add description text
        NSAttributedString *descLineStr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@", currentMediaFile.Detailed_Info]
                                                                         attributes:@{NSFontAttributeName : MedRepElementDescriptionLabelFont}];
        
        
        [productNameAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepPanelTitleLabelFontColor range:NSMakeRange(0, productNameAttrStr.length)];
        
        [productNameAttrStr appendAttributedString:descLineStr];
        
        }
    else
    {
        titleTextView.text=[NSString stringWithFormat:@"%@", currentMediaFile.Product_Name];
    }
    
    productDescriptionTextView.text=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@", currentMediaFile.Detailed_Info]];
}
#pragma mark Swipe View Methods

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return 4;
}
- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    NSLog(@"swipe page number is %ld", (long)swipeView.currentPage);
}

-(void)updateSegmentIndex
{
    NSLog(@"updating segment index");
    
    [segmentControl setSelectedSegmentIndex:selectedSegmentIndex animated:YES];
    
    
    if (mediaContentArray.count>0) {
    [demoPlanMediaCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionTop];
    
    [demoPlanMediaCollectionView.delegate collectionView:demoPlanMediaCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    else{
        nomediaImageView.hidden=NO;
    }
    
}

//-(void)updateSegmentIndex
//{
//    NSLog(@"performing selector after delay in mult visits");
//    [segmentControl setSelectedSegmentIndex:selectedSegmentIndex animated:YES];
//    
//}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView
{
    NSLog(@"swipe view did end decelerating called in visit multiple options");
    selectedSegmentIndex=swipeView.currentPage;
    
    [self performSelector:@selector(updateSegmentIndex) withObject:self afterDelay:1.0];
    
    
    
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    demoPlanMediaCollectionView = nil;
    view = [[UIView alloc] initWithFrame:mediaSwipeView.bounds];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(150, 150);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *demoPlanCollectionView = [[UICollectionView alloc] initWithFrame:view.bounds collectionViewLayout:flowLayout];
    demoPlanCollectionView.delegate=self;
    demoPlanCollectionView.dataSource=self;
    demoPlanCollectionView.tag = index;
    
    demoPlanCollectionView.backgroundColor=[UIColor whiteColor];
    [demoPlanCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    
    
    [view addSubview:demoPlanCollectionView];
    
    
    [self reloadCollectionViewWithIndex:index];
    
    return view;
}

-(void)reloadCollectionViewWithIndex:(NSInteger)index
{
    [segmentControl setSelectedSegmentIndex:index];
    
    if (mediaContentArray.count>0) {
        demoPlanMediaCollectionView.contentOffset=CGPointZero;
    }
}

#pragma mark Segment Controller Methods



-(void)reloadContentWithHeaderMedia:(NSInteger)updatedIndex
{
    
    NSLog(@"segment control delegate called");
    if (updatedIndex==0) {
        
        imagesView.hidden=NO;
        moviePlayerView.hidden=YES;
        pdfView.hidden=YES;
        presentationView.hidden=YES;
    }
    else if (updatedIndex==1) {
        imagesView.hidden=YES;
        moviePlayerView.hidden=NO;
        pdfView.hidden=YES;
        presentationView.hidden=YES;
    }
    else if (updatedIndex==2)
    {
        imagesView.hidden=YES;
        moviePlayerView.hidden=YES;
        pdfView.hidden=NO;
        presentationView.hidden=YES;
        
    }
    else if (updatedIndex==3)
    {
        imagesView.hidden=YES;
        moviePlayerView.hidden=YES;
        pdfView.hidden=YES;
        presentationView.hidden=NO;
        
    }
}


- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    
    NSLog(@"segment control delegate called");
    comingFromSegment=YES;
    if (segmentedControl.selectedSegmentIndex==0) {
        
        imagesView.hidden=NO;
        moviePlayerView.hidden=YES;
        pdfView.hidden=YES;
        presentationView.hidden=YES;
//        collectionViewDataArray=[[NSMutableArray alloc]init];
//        collectionViewDataArray=productImagesArray;
//        [demoPlanMediaCollectionView reloadData];
    }
    else if (segmentedControl.selectedSegmentIndex==1) {
        imagesView.hidden=YES;
        moviePlayerView.hidden=NO;
        pdfView.hidden=YES;
        presentationView.hidden=YES;

//        collectionViewDataArray=[[NSMutableArray alloc]init];
//        collectionViewDataArray=productVideoFilesArray;
//        [demoPlanMediaCollectionView reloadData];

        
    }
    else if (segmentedControl.selectedSegmentIndex==2)
    {
        imagesView.hidden=YES;
        moviePlayerView.hidden=YES;
        pdfView.hidden=NO;
        presentationView.hidden=YES;


//        collectionViewDataArray=[[NSMutableArray alloc]init];
//        collectionViewDataArray=productPdfFilesArray;
//        [demoPlanMediaCollectionView reloadData];
        
    }
    else if (segmentedControl.selectedSegmentIndex==3)
    {
        imagesView.hidden=YES;
        moviePlayerView.hidden=YES;
        pdfView.hidden=YES;
        presentationView.hidden=NO;
        
//        collectionViewDataArray=[[NSMutableArray alloc]init];
//        collectionViewDataArray=presentationsArray;
//        [demoPlanMediaCollectionView reloadData];
        
    }
    [mediaSwipeView reloadData];
    [mediaSwipeView scrollToItemAtIndex:segmentedControl.selectedSegmentIndex duration:0.0];

    if (mediaContentArray.count>0) {
        
      [demoPlanMediaCollectionView.delegate collectionView:demoPlanMediaCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    
//    [demoPlanMediaCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:selectedSegmentIndex inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
//    [demoPlanMediaCollectionView.delegate collectionView:demoPlanMediaCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:selectedSegmentIndex inSection:0]];
//
    }
    else{
        imagesView.hidden=YES;
        moviePlayerView.hidden=YES;
        pdfView.hidden=YES;
        presentationView.hidden=YES;
        
        sellOutStepperContainerView.hidden=YES;
        samplesStepperContainerView.hidden=YES;
        
        
        samplesView.hidden=YES;
        sellOutView.hidden=YES;
        sampleSellOutDividerLabel.hidden=YES;
        
        
        [samplesStepperView removeFromSuperview];
        [selloutStepperView removeFromSuperview];
        
       // titleLabel.text=@"N/A";
        
        nomediaImageView.hidden=NO;
 
    }
    
}

#pragma mark Stepper Methods
-(void)addSteppers
{
    
    
    sellOutStepperContainerView.hidden=YES;
    samplesStepperContainerView.hidden=YES;
    
    
    samplesView.hidden=YES;
    sellOutView.hidden=YES;
    sampleSellOutDividerLabel.hidden=YES;

        [samplesStepperView removeFromSuperview];
        [selloutStepperView removeFromSuperview];
        

    NSPredicate *stockPredicate=[NSPredicate predicateWithFormat:@"SELF.ProductId ==%@",currentMediaFile.Entity_ID_1];
    
    NSMutableArray* filteredArray=[[VisitSamplesProductsArray filteredArrayUsingPredicate:stockPredicate] mutableCopy];
    
    if (filteredArray.count>0) {
    
        sellOutStepperContainerView.hidden=NO;
        samplesStepperContainerView.hidden=NO;

        samplesView.hidden=NO;
        sellOutView.hidden=NO;
        sampleSellOutDividerLabel.hidden=NO;

        
    selectedProduct=[[SampleProductClass alloc]init];

    selectedProduct=[filteredArray objectAtIndex:0];
    
    previousSelectedProductID=selectedProduct.ProductId;
    NSLog(@"filtered product name is %@", selectedProduct.productName);
        
        NSLog(@"filtered products count %lu",(unsigned long)VisitSamplesProductsArray.count);
        
        
        
        selectedProductIndexforSamples=[VisitSamplesProductsArray indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return [stockPredicate evaluateWithObject:obj];
            
        }];
        NSLog(@"selected product index is %ld",(long)selectedProductIndexforSamples);
        
       // ProductMediaFile* currentProduct=[[ProductMediaFile alloc]init];

    
    
    NSInteger selectedProductStock=[selectedProduct.productAvailbleQty integerValue];
    
    samplesStepperView=[[YStepperView alloc]initWithFrame:CGRectMake(0, 0, samplesStepperContainerView.frame.size.width, samplesStepperContainerView.frame.size.height)];
    [samplesStepperView setStepperColor:[UIColor colorWithRed:(18.0/255.0) green:(184.0/255.0) blue:(118.0/255.0) alpha:1] withDisableColor:nil];
    [samplesStepperView setTextColor:MedRepDescriptionLabelFontColor];
    samplesStepperView.yStepperViewDelegate=self;
    
    [samplesStepperView setTextLabelFont:MedRepElementDescriptionLabelFont];
    if (selectedProduct.samplesGiven>0) {
        [samplesStepperView setValue:(int)selectedProduct.samplesGiven];
    }
    else{
        [samplesStepperView setValue:0];
    }
    
    [samplesStepperView setSteppedTypeSamples:YES];
    [samplesStepperView setStepperRange:0 andMaxValue:(int)selectedProductStock];
    [samplesStepperContainerView addSubview:samplesStepperView];
    
    
    selloutStepperView=[[YStepperView alloc]initWithFrame:CGRectMake(0, 0, sellOutStepperContainerView.frame.size.width, sellOutStepperContainerView.frame.size.height)];
    [selloutStepperView setStepperColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1] withDisableColor:nil];
    [selloutStepperView setTextColor:MedRepDescriptionLabelFontColor];
    [selloutStepperView setTextLabelFont:MedRepElementDescriptionLabelFont];
    if (selectedProduct.sellOutGiven>0) {
        [selloutStepperView setValue:(int)selectedProduct.sellOutGiven];
    }
    else
    {
        [selloutStepperView setValue:0];
    }
    selloutStepperView.yStepperViewDelegate=self;
    [selloutStepperView setSteppedTypeSamples:NO];
    [selloutStepperView setStepperRange:0 andMaxValue:(int)selectedProductStock];
    [sellOutStepperContainerView addSubview:selloutStepperView];
    }
    else
    {
        [samplesStepperView removeFromSuperview];
        [selloutStepperView removeFromSuperview];

    }
    
}

-(void)updatedSamplesGivenData
{
    samplesGivenCount=[samplesStepperView getValue];
    sellOutGivenCount=[selloutStepperView getValue];
    selectedProduct.samplesGiven=samplesGivenCount;
    selectedProduct.sellOutGiven=sellOutGivenCount;
   
    
    
    [VisitSamplesProductsArray replaceObjectAtIndex:selectedProductIndexforSamples withObject:selectedProduct];
    
    
}
-(void)valueIncreased:(NSInteger)value sender:(id)sender
{
    [self updatedSamplesGivenData];
}
-(void)valueDecreased:(NSInteger)value sender:(id)sender
{
    [self updatedSamplesGivenData];
}
-(void)closeTapped
{
    NSLog(@"close demo plan tapped");
    
    if ([self.demoedMediaDelegate respondsToSelector:@selector(demoedMedia:withSamplesGiven:)]) {
        
        [self.demoedMediaDelegate demoedMedia:productMediaFilesArray withSamplesGiven:VisitSamplesProductsArray];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

#pragma mark Bar Button email and popover methods

-(void)cancelEmail
{
    NSMutableArray * barButtonItemsArray=[[NSMutableArray alloc]initWithObjects:emailBarButtonItem,viewSellOutBarButtonItem, nil];
    self.navigationItem.leftBarButtonItem=closeButton;
    self.navigationItem.rightBarButtonItems=barButtonItemsArray;
    
}
-(void)showMailClient
{
    NSLog(@"sending email");
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        mailController.mailComposeDelegate = self;
        
        [mailController setSubject:@"SalesWorx"];
        
        NSString* updatedEmailBodyString;
        
        if (emailableContentArray.count>0) {
            
            for (NSInteger i=0; i<emailableContentArray.count; i++) {
                
                NSString*  currentMediaType = [[emailableContentArray valueForKey:@"Media_Type"] objectAtIndex:i ];
                if ([currentMediaType isEqualToString:@"Image"]) {
                    NSString *fileNameString = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[emailableContentArray valueForKey:@"File_Name"] objectAtIndex:i] ] ];
                    UIImage * currentImage=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileNameString]];
                    NSData *currentImageData = UIImageJPEGRepresentation(currentImage ,1.0);
                    [mailController addAttachmentData:currentImageData mimeType:@"image/jpeg" fileName:fileNameString];
                    
                }
                else if ([currentMediaType isEqualToString:kSelectedMediaTypeVideo])
                {
                    NSString* emailBodyString=[[NSString alloc]init];
                    NSString* imageName= [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[emailableContentArray valueForKey:@"File_Name"] objectAtIndex:i] ] ];
                    NSArray *items = [imageName componentsSeparatedByString:@"M_"];   //take the one array for split the string
                    NSString* refinedFileName=[NSString stringWithFormat:@"File Name : %@",[items lastObject]];
                    NSString* downloadUrl=[NSString stringWithFormat:@"Download URL : %@",[[emailableContentArray objectAtIndex:i]valueForKey:@"Download_Url"] ];
                    NSLog(@"download url is %@", downloadUrl);
                    emailBodyString=[[[[emailBodyString stringByAppendingString:refinedFileName] stringByAppendingString:@"\n \n"] stringByAppendingString:downloadUrl] stringByAppendingString:@"\n \n"];
                    if ([NSString isEmpty:updatedEmailBodyString]) {
                        updatedEmailBodyString=emailBodyString;
                    }
                    else{
                        updatedEmailBodyString=[[updatedEmailBodyString stringByAppendingString:@"\n"] stringByAppendingString:emailBodyString];
                    }
                }
                else if ([currentMediaType isEqualToString:kSelectedMediaTypeBrochure])
                {
                    NSString* pdfName= [NSString stringWithFormat:@"%@",[[emailableContentArray objectAtIndex:i]valueForKey:@"File_Name"] ];
                    NSString *fileNameString=[[MedRepDefaults fetchDocumentsDirectory] stringByAppendingPathComponent:[[emailableContentArray  objectAtIndex:i]valueForKey:@"File_Name"]];
                    NSData *currentImageData = [NSData dataWithContentsOfFile:fileNameString];
                    [mailController addAttachmentData:currentImageData mimeType:@"application/pdf" fileName:pdfName];
                    
                    
                }
                else if ([currentMediaType isEqualToString:kSelectedMediaTypePowerpoint])
                {
                    NSString* emailBodyString=[[NSString alloc]init];
                    NSString* imageName= [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[emailableContentArray valueForKey:@"File_Name"] objectAtIndex:i] ] ];
                    NSArray *items = [imageName componentsSeparatedByString:@"M_"];   //take the one array for split the string
                    NSString* refinedFileName=[NSString stringWithFormat:@"File Name : %@",[items lastObject]];
                    NSString* downloadUrl=[NSString stringWithFormat:@"Download URL : %@",[[emailableContentArray objectAtIndex:i]valueForKey:@"Download_Url"] ];
                    emailBodyString=[[[[emailBodyString stringByAppendingString:refinedFileName] stringByAppendingString:@"\n \n"] stringByAppendingString:downloadUrl] stringByAppendingString:@"\n \n"];
                    if ([NSString isEmpty:updatedEmailBodyString]) {
                        updatedEmailBodyString=emailBodyString;
                    }
                    else{
                        updatedEmailBodyString=[[updatedEmailBodyString stringByAppendingString:@"\n"] stringByAppendingString:emailBodyString];
                    }
                }
                
                [mailController setMessageBody:updatedEmailBodyString isHTML:NO];
            }
            
            [self presentViewController:mailController animated:YES completion:nil];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select atleast one file to email" withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        
    }
    
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    emailableContentArray=[[NSMutableArray alloc]init];
    [productImagesArray setValue:@NO forKey:@"isEmailed"];
    [productVideoFilesArray setValue:@NO forKey:@"isEmailed"];
    [productPdfFilesArray setValue:@NO forKey:@"isEmailed"];
    [presentationsArray setValue:@NO forKey:@"isEmailed"];
    [demoPlanMediaCollectionView reloadData];
    [self cancelEmail];
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)sendEmail
{
    sendEmail=YES;
    self.navigationItem.leftBarButtonItem=nil;
    NSMutableArray * barButtonItemsArray=[[NSMutableArray alloc]initWithObjects:sendbarButtonItem,cancelBarButtonItem, nil];
    
    self.navigationItem.rightBarButtonItems=barButtonItemsArray;
    
}
-(void)showPopOver
{
    
    SalesWorxBrandAmbassadorSamplesandSellOutViewController * viewController=[[SalesWorxBrandAmbassadorSamplesandSellOutViewController alloc]init];
    NSPredicate * samplesandSellOutPredicate=[NSPredicate predicateWithFormat:@"SELF.samplesGiven >0 OR SELF.sellOutGiven>0"];
    NSMutableArray* arraywithSamplesorSellout=[[VisitSamplesProductsArray filteredArrayUsingPredicate:samplesandSellOutPredicate] mutableCopy];
    if (arraywithSamplesorSellout.count>0) {
        viewController.samplesandSellOutArray=arraywithSamplesorSellout;
    }
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:viewController];
    baNavController.preferredContentSize=CGSizeMake(538, 332);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.barButtonItem = viewSellOutBarButtonItem;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    [self presentViewController:baNavController animated:YES completion:^{
        
    }];
    
}

@end
