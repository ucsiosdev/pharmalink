//
//  SalesWorxBrandAmbassadorQueries.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorQueries.h"

@implementation SalesWorxBrandAmbassadorQueries

static SalesWorxBrandAmbassadorQueries *sharedSingleton=nil;
//static sqlite3 *db;

+ (SalesWorxBrandAmbassadorQueries *)retrieveManager{
    @synchronized(self)
    {
        if (!sharedSingleton)
        {
            //NSLog(@"DATABASE MANAGER ALLOCS");
            sharedSingleton = [[SalesWorxBrandAmbassadorQueries alloc] init];
        }
        return sharedSingleton;
    }
    
}

#pragma mark Database Methods

- (NSMutableArray *)fetchDataForQuery:(NSString *)sql
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSMutableArray *newsItems = [NSMutableArray new];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            [newsItems addObject:[results resultDictionary]];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        
        [db close];
    }
    
    return newsItems;
    
}
- (BOOL)executeQuery:(NSString *)sqlQuery
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:sqlQuery];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return success;
}

- (FMDatabase *)getDatabase
{
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    
    //    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}

#pragma mark BrandAmbassador Locations

-(NSMutableArray*)fetchLocationsForBrandAmbassadorVisit
{
    NSString* locationsQry=@"select IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Location_Name,'N/A') AS Location_Name, IFNULL(Latitude,'N/A') AS Latitude,IFNULL(Longitude,'N/A') AS Longitude,IFNULL(City,'N/A') AS City,IFNULL(Phone,'N/A') AS Phone,IFNULL(Address_2,'N/A') AS Address from PHX_TBL_locations where Location_Type ='p'";
    NSMutableArray *locationsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray *contentArray=[self fetchDataForQuery:locationsQry];
    
    if (contentArray.count>0) {
        
        
        for (NSMutableDictionary * currentDict in contentArray) {
            SalesWorxBrandAmbassadorLocation * location=[[SalesWorxBrandAmbassadorLocation alloc]init];

            location.Location_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Location_ID"]];
            location.Location_Name=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Location_Name"]];
            location.Latitude=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Latitude"]];
            location.Longitude=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Longitude"]];
            location.City=[SWDefaults getValidStringValue:[currentDict valueForKey:@"City"]];
            location.Phone=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Phone"]];
            location.Address=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Address"]];
            [locationsArray addObject:location];
        }
    }
    
    if (locationsArray.count>0) {
        
        return locationsArray;
    }
    else
    {
        return  nil;
    }
}

#pragma mark Create Brand Ambassador Visit

-(NSString*)rescheduleBrandAmbassadorVisit:(SalesWorxBrandAmbassadorVisit *)visitDetails
{
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    NSLog(@"location id while reschduling visit %@", visitDetails.selectedLocation.Location_ID);
    
    [database open];
    NSString* plannedVisitID = [NSString createGuid];
    
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:visitDetails.Date];
    //NSLog(@"refined date is %@", refinedDate);
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    BOOL success = NO;
    @try {
        {
            success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Planned_Visits  (Planned_Visit_ID,Visit_Date,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Visit_Status,Comments,Created_At,Created_By,Visit_Type,Demo_Plan_ID,Custom_Attribute_2,Custom_Attribute_3,Old_Visit_ID)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",plannedVisitID,visitDetails.Date,empID,visitDetails.selectedLocation.Location_ID,[NSNull null], [NSNull null],@"N",visitDetails.Objective,createdAt,createdBy,@"N",visitDetails.selectedDemoPlan.Demo_Plan_ID,[NSNull null],@"1",visitDetails.Planned_Visit_ID,nil];
            
        }
        
    }@catch (NSException *exception)
    {
        //NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    
    if (success==YES) {
        
        NSLog(@"brand ambassador visit updated successfully with new planned visit ID %@",plannedVisitID);
        return plannedVisitID;
    }
    else
    {
        return nil;
    }
 
}

-(BOOL)createBrandAmbassadorVisit:(SalesWorxBrandAmbassadorVisit *)visitDetails
{
    
        FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
        
        [database open];
        NSString* plannedVisitID = [NSString createGuid];
        
        NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:visitDetails.Date];
        //NSLog(@"refined date is %@", refinedDate);
        NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
        NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
        
        BOOL success = NO;
        @try {
            {
                if ([NSString isEmpty:visitDetails.Old_Visit_ID]==NO) {
                      success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Planned_Visits  (Planned_Visit_ID,Visit_Date,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Visit_Status,Comments,Created_At,Created_By,Visit_Type,Demo_Plan_ID,Custom_Attribute_2,Custom_Attribute_3,Old_Visit_ID)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",plannedVisitID,refinedDate,empID,visitDetails.selectedLocation.Location_ID,[NSNull null], [NSNull null],@"N",visitDetails.Objective,createdAt,createdBy,@"N",visitDetails.selectedDemoPlan.Demo_Plan_ID,[NSNull null],@"1",visitDetails.Old_Visit_ID,nil];
                }
                else{
                
                success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Planned_Visits  (Planned_Visit_ID,Visit_Date,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Visit_Status,Comments,Created_At,Created_By,Visit_Type,Demo_Plan_ID,Custom_Attribute_2,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",plannedVisitID,refinedDate,empID,visitDetails.selectedLocation.Location_ID,[NSNull null], [NSNull null],@"N",visitDetails.Objective,createdAt,createdBy,@"N",visitDetails.selectedDemoPlan.Demo_Plan_ID,[NSNull null],@"1",nil];
                }
            }
        
        }@catch (NSException *exception)
        {
            //NSLog(@"Exception while obtaining data from database: %@", exception.reason);
        }
        @finally
        {
            [database close];
        }
        
        if (success==YES) {
            
            NSLog(@"brand ambassador visit created successfully");
            return YES;
        }
        else
        {
            return NO;
        }
    
}

#pragma mark Fetch Visit Information
-(NSMutableArray*)fetchBrandAmbassadorVisitforSelectedDate:(NSString*)date
{
    NSString* baVisits=[NSString stringWithFormat:@"select IFNULL(L.Location_Name,'N/A') AS Location_Name,IFNULL(L.Address_2,'N/A') AS Address,IFNULL(L.City,'N/A') AS City,IFNULL(L.Phone,'0') AS Phone,IFNULL(L.Latitude,'0') AS Latitude,IFNULL(L.Longitude,'0') AS Longitude,IFNULL(V.Planned_Visit_ID,'N/A')AS Planned_Visit_ID, IFNULL(V.Visit_Date,'N/A') AS Visit_Date, IFNULL(V.Emp_ID,'0') AS Emp_ID,IFNULL(V.Demo_plan_ID,'0') AS Demo_Plan_ID,IFNULL(D.Description,'0') AS Description, IFNULL(V.Visit_Status,'N/A') AS Visit_Status, IFNULL(V.Location_ID,'N/A')Location_ID, IFNULL(V.Comments,'N/A') AS Objective from PHX_TBL_Planned_visits AS V inner join PHX_TBL_Locations AS L on V.Location_ID=L.Location_ID left join PHX_TBL_Demo_Plan AS D on V.Demo_plan_ID =D.Demo_plan_ID where V.visit_date like '%%%@%%'  and V.Visit_Status!='X'  order by V.Visit_Status  , V.Visit_date ASC  ",date];
    
    NSLog(@"query for fetching ba visit for selected date %@",baVisits);
    
    NSMutableArray * baVisitsArray=[self fetchDataForQuery:baVisits];
    if (baVisitsArray.count>0) {
        
        NSMutableArray * contentArray=[[NSMutableArray alloc]init];
        
        for (NSMutableDictionary *currentDictionary in baVisitsArray) {
            
            SalesWorxBrandAmbassadorVisit * currentvisit=[[SalesWorxBrandAmbassadorVisit alloc]init];
            
            currentvisit.selectedLocation=[[SalesWorxBrandAmbassadorLocation alloc]init];
            currentvisit.selectedDemoPlan=[[DemoPlan alloc]init];
            currentvisit.selectedLocation.Location_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Location_ID"]];
            currentvisit.selectedLocation.Location_Name=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Location_Name"]];
            currentvisit.selectedLocation.Latitude=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Latitude"]];
            currentvisit.selectedLocation.Longitude=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Longitude"]];
            currentvisit.selectedLocation.City=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"City"]];
            currentvisit.selectedLocation.Phone=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Phone"]];
            currentvisit.selectedLocation.Address=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Address"]];
            currentvisit.selectedDemoPlan.Demo_Plan_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Demo_Plan_ID"]];
            currentvisit.selectedDemoPlan.Description=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Description"]];
            currentvisit.Date=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Visit_Date"]];
            currentvisit.Objective=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Objective"]];
            currentvisit.Planned_Visit_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Planned_Visit_ID"]];
            currentvisit.Emp_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Emp_ID"]];
            currentvisit.Visit_Status=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Visit_Status"]];
            

            [contentArray addObject:currentvisit];
            
        }
        
        return contentArray;
    }
    else
    {
        return nil;
    }
}

-(NSMutableArray*)fetchAllBrandAmbassadorVisitsforFilter
{
    NSString* baVisits=[NSString stringWithFormat:@"select IFNULL(L.Location_Name,'N/A') AS Location_Name,IFNULL(L.Address_2,'N/A') AS Address,IFNULL(L.City,'N/A') AS City,IFNULL(L.Phone,'0') AS Phone,IFNULL(L.Latitude,'0') AS Latitude,IFNULL(L.Longitude,'0') AS Longitude,IFNULL(V.Planned_Visit_ID,'N/A')AS Planned_Visit_ID, IFNULL(V.Visit_Date,'N/A') AS Visit_Date, IFNULL(V.Emp_ID,'0') AS Emp_ID,IFNULL(V.Demo_plan_ID,'0') AS Demo_Plan_ID,IFNULL(D.Description,'0') AS Description, IFNULL(V.Visit_Status,'N/A') AS Visit_Status, IFNULL(V.Location_ID,'N/A')Location_ID, IFNULL(V.Comments,'N/A') AS Objective from PHX_TBL_Planned_visits AS V inner join PHX_TBL_Locations AS L on V.Location_ID=L.Location_ID left join PHX_TBL_Demo_Plan AS D on V.Demo_plan_ID =D.Demo_plan_ID where  V.Visit_Status!='X' and L.Location_Type='P'   group by V.Planned_Visit_ID order by V.visit_date ASC"];
    
    NSLog(@"query for fetching bs visit %@",baVisits);
    
    NSMutableArray * baVisitsArray=[self fetchDataForQuery:baVisits];
    if (baVisitsArray.count>0) {
        
        NSMutableArray * contentArray=[[NSMutableArray alloc]init];
        
        for (NSMutableDictionary *currentDictionary in baVisitsArray) {
            
            SalesWorxBrandAmbassadorVisit * currentvisit=[[SalesWorxBrandAmbassadorVisit alloc]init];
            
            currentvisit.selectedLocation=[[SalesWorxBrandAmbassadorLocation alloc]init];
            currentvisit.selectedDemoPlan=[[DemoPlan alloc]init];
            currentvisit.selectedLocation.Location_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Location_ID"]];
            currentvisit.selectedLocation.Location_Name=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Location_Name"]];
            currentvisit.selectedLocation.Latitude=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Latitude"]];
            currentvisit.selectedLocation.Longitude=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Longitude"]];
            currentvisit.selectedLocation.City=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"City"]];
            currentvisit.selectedLocation.Phone=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Phone"]];
            currentvisit.selectedLocation.Address=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Address"]];
            currentvisit.selectedDemoPlan.Demo_Plan_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Demo_Plan_ID"]];
            currentvisit.selectedDemoPlan.Description=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Description"]];
            currentvisit.Date=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Visit_Date"]];
            currentvisit.Objective=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Objective"]];
            currentvisit.Planned_Visit_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Planned_Visit_ID"]];
            currentvisit.Emp_ID=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Emp_ID"]];
            currentvisit.Visit_Status=[SWDefaults getValidStringValue:[currentDictionary valueForKey:@"Visit_Status"]];
            
            
            [contentArray addObject:currentvisit];
            
        }
        
        return contentArray;
    }
    else
    {
        return nil;
    }
}

#pragma mark Save Brand Ambassador Visit Details without call

-(NSString*)createBrandAmbassadorActualVisit:(SalesWorxBrandAmbassadorVisit*)currentVisit
{
    
    BOOL insertIntoActualVisits = NO;
    NSString* actualVisitID=[NSString createGuid];
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    NSString* plannedVisit_ID=currentVisit.Planned_Visit_ID;
    NSString* callStartDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_Start_Date"];
    NSString* callEndDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_End_Date"];
    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* latitide=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude];
    if (latitide.length>0) {
        
    }
    else
    {
        latitide=@"0";
        longitude=@"0";
    }
    NSString* notesStr=currentVisit.Visit_Conclusion_Notes;
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    @try {
        insertIntoActualVisits =  [database executeUpdate:@"INSERT INTO PHX_TBL_Actual_Visits (Actual_Visit_ID,EMP_ID, Location_ID,Doctor_ID,Contact_ID,Planned_Visit_ID,Visit_Start_Date,Latitude,Longitude,Notes,Accompanied_By,Custom_Attribute_2,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",actualVisitID,empID,currentVisit.selectedLocation.Location_ID,[NSNull null],[NSNull null],plannedVisit_ID,callStartDateandTime,latitide,longitude,[NSNull null],[NSNull null],[SWDefaults getValidStringValue:appDelegate.currentLocationCapturedTime], currentVisit.selectedDemoPlan.Demo_Plan_ID,nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        // [database close];
    }

    if (!insertIntoActualVisits  ) {
        //something went wrong rollback
        BOOL transactionActive=[database inTransaction];
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        else
        {
            NSLog(@"transaction is in active");
            
        }
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        [database close];
        return nil;
    }
    
    else
    {
        //everything alright commit
        [database commit];
        [database close];
        return actualVisitID;
    }
    
}

-(BOOL)updateVisitCompletionStatus:(SalesWorxBrandAmbassadorVisit*)currentVisit
{
    
    BOOL insertIntoPlannedVisits=NO;
    BOOL updateActualVisits=NO;
    
    NSString* plannedVisit_ID=currentVisit.Planned_Visit_ID;
    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* latitide=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude];
    
    if ([NSString isEmpty:latitide]==NO) {
        
    }
    else
    {
        latitide=@"0";
        longitude=@"0";
    }
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    @try {
        insertIntoPlannedVisits =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Status ='Y' ,Last_Updated_At= ?, Last_Updated_By=? where Planned_Visit_ID=?",createdAt,createdBy,plannedVisit_ID, nil];
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        // [database close];
        
    }
    
    
    @try {
        updateActualVisits= [database executeUpdate:@"Update  PHX_TBL_Actual_Visits set Visit_End_Date ='%@'  where Planned_Visit_ID= ?",createdAt, plannedVisit_ID,nil];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    if (!insertIntoPlannedVisits|| !updateActualVisits ) {
        //something went wrong rollback
        BOOL transactionActive=[database inTransaction];
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        else
        {
            NSLog(@"transaction is in active");
        }
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        [database close];
        return NO;
    }
    
    else
    {
        //everything alright commit
        [database commit];
        [database close];
        return YES;
        
        
    }
    
}


-(BOOL)saveBrandAmbassadorVisitWithOutCalls:(SalesWorxBrandAmbassadorVisit*)currentVisit
{
    
    BOOL insertIntoPlannedVisits=NO;

    NSString* plannedVisit_ID=currentVisit.Planned_Visit_ID;
    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* latitide=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude];
    
    if ([NSString isEmpty:latitide]==NO) {
        
    }
    else
    {
        latitide=@"0";
        longitude=@"0";
    }
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
        @try {
            insertIntoPlannedVisits =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Status ='Y' ,Last_Updated_At= ?, Last_Updated_By=? where Planned_Visit_ID=?",createdAt,createdBy,plannedVisit_ID, nil];
            
        }@catch (NSException *exception)
        {
            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
        }
        @finally
        {
            // [database close];
            
        }

    if (!insertIntoPlannedVisits ) {
        //something went wrong rollback
        BOOL transactionActive=[database inTransaction];
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        else
        {
            NSLog(@"transaction is in active");
        }
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        [database close];
        return NO;
    }
    
    else
    {
        //everything alright commit
        [database commit];
        [database close];
        return YES;
        
        
    }
    
}


-(BOOL)updateVisitEndDateToActualVisits:(SalesWorxBrandAmbassadorVisit*)currentVisit
{
    
    BOOL updateActualVisitNotes=NO;
    NSString* actual_Visit_ID=currentVisit.Actual_Visit_ID;
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
    }
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    @try {
        updateActualVisitNotes =  [database executeUpdate:@"Update  PHX_TBL_Actual_Visits set Visit_End_Date =?  where Actual_Visit_ID=?",[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],actual_Visit_ID, nil];
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        // [database close];
    }
    if (!updateActualVisitNotes ) {
        //something went wrong rollback
        BOOL transactionActive=[database inTransaction];
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        else
        {
            NSLog(@"transaction is in active");
        }
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        [database close];
        return NO;
    }
    else
    {
        //everything alright commit
        NSLog(@"Notes updated to actual visits successfully");
        [database commit];
        [database close];
        return YES;
    }
}

-(BOOL)updateStock:(NSMutableArray*)productsArray
{
    NSLog(@"update stock quantity called %@", [productsArray description]);
    
    BOOL success = NO;
    
        return success;
    
    
    
}

-(BOOL)saveBrandAmbassadorVisit:(SalesWorxBrandAmbassadorVisit*)currentVisitDetails
{
    //NSLog(@"visit details are %@", [visitDetails description]);
    //NSLog(@"products demonstrated are %@", [productsDemonstrated description]);
    // NSLog(@"samples given are %@", [samplesGiven description]);
    //NSLog(@"additional info is %@", [additionalInfo description]);
    
    NSLog(@"selected location location id is %@", currentVisitDetails.selectedLocation.Location_ID);
    
    NSLog(@"samples given are %@", currentVisitDetails.samplesGivenArray);
    
    
    
    NSLog(@"notes array is %@",[currentVisitDetails.notesArray valueForKey:@"isUpdated"]);
    
    //table flags
    
    BOOL insertIntoPlannedVisits = NO;
    BOOL insertIntoWalkinCustomers = NO;
    BOOL insertIntoIssueNote = NO;
    BOOL insertIntoIssueNoteLineItems=NO;
    BOOL insertIntoEDetailing=NO;
    BOOL insertIntoVisitAddlInfoRating=NO;
    BOOL insertIntoVisitAdditionalInfo=NO;
    BOOL insertIntoVisitAdditionalInfoNotes=NO;
    BOOL updatedTasksSuccessfully=NO;
    BOOL insertIntoSurveyResponses=NO;
    BOOL updatedNotesSuccessfully = NO;
    BOOL updateStock=NO;
    
    /*saving the visit details, data to be inserted to the follwoing tables
     
     PHX_TBL_Actual_Visits
     PHX_Tbl_Edetailing- for products demonstarated
     
     PHX_TBL_Issue_Note-for samples given
     PHX_TBL_Issue_Note_Line_Items-for samples given (more like order and line items)
     
     
     TBL_Visit_Addl_Info- attribute name “TASK”
     ATTRIB VALUE TASK-ID
     CA1 Type of Action C-Created/M-modified, and meeting attendies Attrib name and value,and custom attribute_1 for rating
     
     
     */
    
    
    //check if a note has been created , if yes there it will have actual visit id use the same
    NSString* userID=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];

    NSString *lastOrderReference=[[NSString alloc]init];
    
    NSString* plannedVisit_ID=currentVisitDetails.Planned_Visit_ID;
    
    NSString* callStartDateandTime=currentVisitDetails.Call_Start_Date;
    
    NSString* callEndDateandTime=currentVisitDetails.Call_End_Date;
    
    
    NSString* edetailingStartTime=currentVisitDetails.EDetailing_Start_Time;
    NSString* edetailingEndTime =currentVisitDetails.EDetailing_End_Time;
    
    NSString* assignedTo=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSMutableArray * updatedTasksArray=[[NSMutableArray alloc]init];
    
    NSString* latitide=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude];
    
    currentVisitDetails.walkinCustomer.Walkin_Session_ID=[NSString createGuid];
    NSLog(@"walkin session id is %@", currentVisitDetails.walkinCustomer.Walkin_Session_ID);
    
    
    if (latitide.length>0) {
        
    }
    else
    {
        latitide=@"0";
        longitude=@"0";
    }
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    @try {
        
        insertIntoWalkinCustomers =  [database executeUpdate:@"INSERT INTO PHX_TBL_Walkin_Customers (Walkin_Session_ID,Actual_Visit_ID,Start_Time, End_Time,Latitude,Longitude,SalesRep_ID,Customer_Name,Customer_Email,Customer_Phone,Custom_Attribute_1)  VALUES (?,?,?,?,?,?,?,?,?,?,?)",currentVisitDetails.walkinCustomer.Walkin_Session_ID,currentVisitDetails.Actual_Visit_ID,currentVisitDetails.Call_Start_Date,currentVisitDetails.Call_End_Date,latitide,longitude,empID,currentVisitDetails.walkinCustomer.Customer_Name,currentVisitDetails.walkinCustomer.Customer_Email,currentVisitDetails.walkinCustomer.Customer_Phone,currentVisitDetails.Visit_Conclusion_Notes,nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        // [database close];
    }
    
    if (insertIntoWalkinCustomers==YES) {
        NSPredicate * updatedTasksPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES'"];
        updatedTasksArray=[[currentVisitDetails.tasksArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
        
        NSLog(@"note predicate is %@ \n notes array is %@", updatedTasksPredicate,currentVisitDetails.notesArray);

        NSLog(@"updated tasks are %@", updatedTasksArray);
        if (updatedTasksArray.count>0) {
            for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                VisitTask * currentTask=[updatedTasksArray objectAtIndex:i];
                NSMutableDictionary * updationInfoDict=[[NSMutableDictionary alloc]init];
                [updationInfoDict setValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"Last_Updated_At"];
                [updationInfoDict setValue:userID forKey:@"Last_Updated_By"];
                if ([currentTask.Status isEqualToString:@"New"]) {
                    [updationInfoDict setValue:[NSNull null] forKey:@"Last_Updated_At"];
                    [updationInfoDict setValue:[NSNull null] forKey:@"Last_Updated_By"];
                }
                if ([currentTask.Status isEqualToString:@"Closed"]) {
                    [updationInfoDict setValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"End_Time"];
                }
                else
                {
                    [updationInfoDict setValue:[NSNull null] forKey:@"End_Time"];
                }
                @try {
                    
                    updatedTasksSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Tasks (Task_ID,Title, Description,Category,Priority,Start_Time,Status,Assigned_To,Emp_ID,Location_ID,Show_Reminder,IS_Active,Is_deleted,Created_At,Created_By,Doctor_ID,Contact_ID,End_Time,Last_Updated_At,Last_Updated_By,Custom_Attribute_1,End_Time)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",currentTask.Task_ID, currentTask.Title==nil?@"":currentTask.Title,currentTask.Description==nil?@"":currentTask.Description,currentTask.Category==nil?@"":currentTask.Category,currentTask.Priority==nil?@"":currentTask.Priority, currentTask.Start_Time,currentTask.Status==nil?@"":currentTask.Status,assignedTo,assignedTo,currentVisitDetails.selectedLocation.Location_ID,@"N",@"Y",@"N",currentTask.Start_Time,createdBy,[NSNull null],[NSNull null],currentTask.End_Time,[updationInfoDict valueForKey:@"Last_Updated_At"],[updationInfoDict valueForKey:@"Last_Updated_By"],@"IPAD",[updationInfoDict valueForKey:@"End_Time"],nil];
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
            }
        }
        else
        {
            updatedTasksSuccessfully=YES;
        }
        NSPredicate * updatedNotesPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated=='YES'"];
        NSLog(@"note predicate is %@ \n notes array is %@", updatedNotesPredicate,currentVisitDetails.notesArray);
        
        NSMutableArray*  updatedNotesArray=[[currentVisitDetails.notesArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
        NSLog(@"updated notes are %@", updatedNotesArray);
        if (updatedNotesArray.count>0) {
     for (NSInteger i=0; i<updatedNotesArray.count; i++) {
            VisitNotes * currentNote=[updatedNotesArray objectAtIndex:i];
            @try {
                updatedNotesSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_EMP_NOTES  (Note_ID,Title, Description,Category,Emp_ID,Visit_ID,Location_ID,Doctor_ID,Contact_ID,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?,?,?,?)",currentNote.Note_ID,currentNote.Title,currentNote.Description,@"Visit",empID,currentVisitDetails.Actual_Visit_ID,currentVisitDetails.selectedLocation.Location_ID,[NSNull null],[NSNull null],createdAt,createdBy, nil];
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                //[database close];
            }
        }
        }
        else{
            updatedNotesSuccessfully=YES;
        }
        //now insert samples given to issue notes
        NSString* rowID=[NSString createGuid];
        Singleton *single = [Singleton retrieveSingleton];
        NSString *nUserId = [NSString stringWithFormat:@"%@",[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];
        if(nUserId.length == 1)
        {nUserId = [NSString stringWithFormat:@"00%@",nUserId];}
        else if(nUserId.length == 2)
        {nUserId = [NSString stringWithFormat:@"0%@",nUserId];}
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
        NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];

        NSMutableArray * currentCustomerSamplesGivenArray=[[currentVisitDetails.samplesGivenArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.samplesGiven >0"]] mutableCopy];
        NSMutableArray* currentCustomerSellOutGiven=[[currentVisitDetails.samplesGivenArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.sellOutGiven >0"]] mutableCopy];
        
        
         if(currentCustomerSellOutGiven.count==0&&
                currentCustomerSamplesGivenArray.count==0)
        {
            insertIntoIssueNoteLineItems=YES;
            insertIntoIssueNote=YES;
        }
        else
        {
        
        if(currentCustomerSamplesGivenArray.count>0)
        {
            //insert only if samples given
            lastOrderReference = [MedRepDefaults lastIssueNoteReference];
            unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 10] longLongValue];
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
            if (![lastOrderReference isEqualToString:[MedRepDefaults lastIssueNoteReference]])
            {
                [MedRepDefaults setIssueNoteReference:lastOrderReference];
            }
            else
            {
                lastRefId = lastRefId + 1;
                lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
                if (![lastOrderReference isEqualToString:[MedRepDefaults lastIssueNoteReference]])
                {
                    [MedRepDefaults setIssueNoteReference:lastOrderReference];
                }
                else
                {
                    lastRefId = lastRefId + 1;
                    lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
                    [MedRepDefaults setIssueNoteReference:lastOrderReference];
                }
            }
            NSInteger  lastVisitRefInt=[[NSString stringWithFormat:@"%@", lastOrderReference] integerValue];
            @try {
                insertIntoIssueNote =  [database executeUpdate:@"INSERT INTO PHX_TBL_Issue_Note (Row_ID,Doc_No, Doc_Type,Emp_ID,Location_ID,Doctor_ID,Contact_ID,Visit_ID,Start_Time,End_Time,Status,Created_At,Created_By,Custom_Attribute_2)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",rowID,lastOrderReference,@"S",empID,currentVisitDetails.selectedLocation.Location_ID,[NSNull null],[NSNull null],currentVisitDetails.Actual_Visit_ID,callStartDateandTime,callEndDateandTime,@"N",createdAt,createdBy,currentVisitDetails.walkinCustomer.Walkin_Session_ID, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
            }
            
        }
        
        else
        {
            insertIntoIssueNote=YES;
        }
        if (insertIntoIssueNote==YES) {
            
            NSLog(@"INSERTED SUCCESSFULLY TO ISSUE NOTES");
            //now insert into issue note line items
            //segregate samples given and sell out given insert saperte records
            if(currentCustomerSamplesGivenArray.count>0)
            {
                NSInteger line;
                for (NSInteger i = 0; i <currentCustomerSamplesGivenArray.count; i++)
                {
                    NSDictionary *row = [currentCustomerSamplesGivenArray objectAtIndex:i];
                    NSString* qty=[NSString stringWithFormat:@"%@", [row valueForKey:@"samplesGiven"]];
                    if ([qty integerValue]>0) {
                    line=i+1;
                    NSString *rowID=[NSString createGuid];
                    NSString * docNo=[NSString stringWithFormat:@"%@", lastOrderReference];
                    NSString* lineNumber=[NSString stringWithFormat:@"%ld",(long)line];
                    NSString* productIDStr=[NSString stringWithFormat:@"%@", [row valueForKey:@"ProductId"]];
                    NSString* lotNumber=@"Samples";
                    NSString* qty=[NSString stringWithFormat:@"%@", [row valueForKey:@"samplesGiven"]];
                    //TO BE DONE NO UOM IN UI CHECK WITH HARPAL
                    NSString* uom=@"EA";
                    // [database open];
                    @try {
                        insertIntoIssueNoteLineItems =  [database executeUpdate:@"INSERT INTO PHX_TBL_Issue_Note_line_Items (Row_ID,Doc_No, Line_Number,Product_ID,Lot_No,Issue_Qty,Issue_UOM)  VALUES (?,?,?,?,?,?,?)",rowID,docNo,lineNumber,productIDStr,lotNumber,qty,uom, nil];
                        if (insertIntoIssueNoteLineItems==YES) {
                            NSLog(@"Issue note for Samples create with lot number: \n %@ \n qty : %@ \n %@",lotNumber,qty,docNo);
                        }
                    }@catch (NSException *exception)
                    {
                        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                    }
                    @finally
                    {
                        // [database close];
                    }
                    }
                    else{
                        insertIntoIssueNoteLineItems=YES;
                    }
                }
            }
            
            if (currentCustomerSellOutGiven.count>0)
            {
                NSInteger line;
                for (NSInteger i = 0; i < currentCustomerSellOutGiven.count; i++)
                {
                    NSDictionary *row = [currentCustomerSellOutGiven objectAtIndex:i];
                    NSString* qty=[NSString stringWithFormat:@"%@", [row valueForKey:@"sellOutGiven"]];
                    if ([qty integerValue]>0) {
                        line=i+1;
                        NSString *rowID=[NSString createGuid];
                        NSString * docNo=[NSString stringWithFormat:@"%@", lastOrderReference];
                        NSString* lineNumber=[NSString stringWithFormat:@"%ld", (long)line];
                        NSString* productIDStr=[NSString stringWithFormat:@"%@", [row valueForKey:@"ProductId"]];
                        NSString* lotNumber=@"Sellout";
                        //TO BE DONE NO UOM IN UI CHECK WITH HARPAL
                        NSString* uom=@"EA";
                        // [database open];
                        @try {
                            
                            insertIntoIssueNoteLineItems =  [database executeUpdate:@"INSERT INTO PHX_TBL_Issue_Note_line_Items (Row_ID,Doc_No, Line_Number,Product_ID,Lot_No,Issue_Qty,Issue_UOM)  VALUES (?,?,?,?,?,?,?)",rowID,docNo,lineNumber,productIDStr,lotNumber,qty,uom, nil];
                            
                            if (insertIntoIssueNoteLineItems==YES) {
                                NSLog(@"Issue note for Sell out create with lot number: \n %@ \n qty : %@ \n doc no %@" ,lotNumber,qty,docNo);
                            }

                        }@catch (NSException *exception)
                        {
                            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                        }
                        @finally
                        {
                            // [database close];
                        }
                    }
                    else{
                        insertIntoIssueNoteLineItems=NO;
                    }
                    
                }
            }
            
            
        }
        
        
    }
    
            
            if (insertIntoIssueNoteLineItems==YES) {
                NSLog(@"Issue Line Item success");
                //now insert PHX_Tbl_Edetailing- for products demonstarated
                for (NSInteger i=0; i<currentVisitDetails.demoedArray.count; i++) {
                    ProductMediaFile* currentProduct=[[ProductMediaFile alloc]init];
                    currentProduct=[currentVisitDetails.demoedArray objectAtIndex:i];
                    NSString *rowID=[NSString createGuid];
                    NSString* mediaFileID=currentProduct.Media_File_ID;
                    NSString* productID=currentProduct.Entity_ID_1;
                    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
                    // [database open];
                    @try {
                        
                        insertIntoEDetailing =  [database executeUpdate:@"INSERT INTO PHX_TBL_EDetailing (Row_ID,Visit_ID, Product_ID,Media_File_ID,Discussed_At,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Custom_Attribute_2)  VALUES (?,?,?,?,?,?,?,?,?,?)",rowID,currentVisitDetails.Actual_Visit_ID,productID,mediaFileID,callStartDateandTime,empID,currentVisitDetails.selectedLocation.Location_ID,[NSNull null],[NSNull null],currentVisitDetails.walkinCustomer.Walkin_Session_ID, nil];
                        
                    }@catch (NSException *exception)
                    {
                        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                    }
                    @finally
                    {
                        // [database close];
                    }
                }
                if (currentVisitDetails.demoedArray.count==0) {
                    insertIntoEDetailing=YES;
                }
                if (insertIntoEDetailing==YES) {
                    
                    //now insert visit addl info details
                    /*
                     TBL_Visit_Addl_Info- attribute name “TASK”
                     ATTRIB VALUE TASK-ID
                     CA1 Type of Action C-Created/M-modified, and meeting attendies Attrib name and value,and custom attribute_1 for rating
                     */
                    
                    NSString* taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where Location_ID='%@' and  Is_Active='Y' and Is_Deleted='N'  ",currentVisitDetails.selectedLocation.Location_ID ];
                    
                    FMResultSet* resultSet=[database executeQuery:taskdetailsQry];
                    NSMutableArray* tasksArray=[[NSMutableArray alloc]init];
                    while ([resultSet next]) {
                        [tasksArray addObject:[resultSet resultDictionary]];
                    }
                    if (updatedTasksArray.count>0) {
                        for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                            VisitTask * task=[updatedTasksArray objectAtIndex:i];
                            NSString* rowID=[NSString createGuid];
                            NSString* attributeName=@"Task";
                            NSString* attributeValue=task.Task_ID;
                            NSString* taskStatus=task.Status;
                            @try {
                                insertIntoVisitAdditionalInfo =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Custom_Attribute_1,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?)",rowID,currentVisitDetails.Actual_Visit_ID,attributeName,attributeValue,taskStatus,createdAt,createdBy, nil];
                                
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        }
                        
                        //after updating in visit addl info update the status in task table as well.
                        
                        BOOL updatetaskStatus=NO;
                        for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                            VisitTask * task=[updatedTasksArray objectAtIndex:i];
                            @try {
                                updatetaskStatus =  [database executeUpdate:@"Update  PHX_TBL_Tasks set Status = ?, Last_Updated_At=?, Last_Updated_By=?  where Task_ID='%@'",task.Status,createdAt,createdBy,task.Task_ID, nil];
                                
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                                
                            }
                        }
                    }
                    else
                    {
                        insertIntoVisitAdditionalInfo=YES;
                    }
                    //update notes data same as task
                    NSMutableArray* notesArray=[[NSMutableArray alloc]init];
                    NSString* queryForFetchingNotes=[NSString stringWithFormat:@"select IFNULL(Note_ID,'0') AS Note_ID from PHX_TBL_EMP_NOTES where Location_ID='%@' ",currentVisitDetails.selectedLocation.Location_ID ];
                    FMResultSet* resultSetNotes=[database executeQuery:queryForFetchingNotes];
                    while ([resultSetNotes next]) {
                        [notesArray addObject:[resultSetNotes resultDictionary]];
                    }
                    if (updatedNotesArray.count>0) {
                        for (NSInteger i=0; i<updatedNotesArray.count; i++) {
                            VisitNotes * note=[[VisitNotes alloc]init];
                            note=[updatedNotesArray objectAtIndex:i];
                            NSString* rowID=[NSString createGuid];
                            NSString* attributeName=@"Notes";
                            NSString* attributeValue=note.Note_ID;
                            @try {
                                insertIntoVisitAdditionalInfoNotes =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,currentVisitDetails.Actual_Visit_ID,attributeName,attributeValue,createdAt,createdBy, nil];
                                
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        }
                        
                    }
                    else
                    {
                        //if no notes are available
                        insertIntoVisitAdditionalInfoNotes=YES;
                    }
                    if ( insertIntoVisitAdditionalInfo==YES && insertIntoVisitAdditionalInfoNotes==YES) {
                        
                        if (insertIntoVisitAdditionalInfoNotes==YES) {
                            
                            NSLog(@"Notes array insert successful in visit");
                            
                        }
                            NSString* ratingStr=currentVisitDetails.Visit_Rating;
                            NSString* rowID=[NSString createGuid];
                            NSString* attribName=@"Visit_Rating";
                            //  [database open];
                            @try {
                                
                                insertIntoVisitAddlInfoRating =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,currentVisitDetails.Actual_Visit_ID,attribName,ratingStr,createdAt,createdBy, nil];
                                
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                            
                            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
                            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            [formatterTime setLocale:usLocaleq];
                            NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
                            if (insertIntoVisitAddlInfoRating==YES) {
                                
                                NSLog(@"RATING INSERTED SUCCESSFULLY");
                                //this to be updated while closing the visit
                                insertIntoPlannedVisits=YES;
                            }
                    }
                }
                
            }
        }
        
        else
        {
            //issue note insertion failed
            
            //check if transaction is active
            
        }
    
    if (currentVisitDetails.walkinSurveyResponsesArray.count>0) {
    
        for (NSInteger i=0; i<currentVisitDetails.walkinSurveyResponsesArray.count; i++) {
        SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses * currentResponse=[[SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses alloc]init];
            currentResponse=[currentVisitDetails.walkinSurveyResponsesArray objectAtIndex:i];
            @try {
                insertIntoSurveyResponses =  [database executeUpdate:@"INSERT OR REPLACE into TBL_Survey_Walkin_Responses(Walkin_Response_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Survey_Timestamp,Walkin_Session_ID,Actual_Visit_ID) Values(?,?,?,?,?,?,?,?)",currentResponse.Walkin_Response_ID,currentResponse.Survey_ID,currentResponse.Question_ID,currentResponse.Response,currentResponse.SalesRep_ID,currentResponse.Survey_Timestamp,currentVisitDetails.walkinCustomer.Walkin_Session_ID,currentVisitDetails.Actual_Visit_ID];
            }
            @catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                // [database close];
            }
            
        }
        
       }
    else{
        insertIntoSurveyResponses=YES;
    }
    
    if(currentVisitDetails.samplesGivenArray.count>0)
    {
        
        for (NSInteger i=0; i<currentVisitDetails.samplesGivenArray.count; i++) {
            
            SampleProductClass* currentProduct=[currentVisitDetails.samplesGivenArray objectAtIndex:i];
            NSLog(@"Updating stock for product : %@ %ld", currentProduct.productName, (long)currentProduct.samplesGiven);
            NSString* productID=currentProduct.ProductId;
           // NSString* LotNo=currentProduct.productLotNumber;
            NSInteger qtyToUpdate=[currentProduct.Used_Qty integerValue]+currentProduct.samplesGiven;
            NSString *userQtyStr = [NSString stringWithFormat:@"%zd", qtyToUpdate];
            NSString* userId=[[SWDefaults userProfile] stringForKey:@"User_ID"];
            @try {
                //add last updated by
                updateStock =  [database executeUpdate:@"Update  PHX_TBL_EMP_Stock set Last_Updated_By=?, Used_Qty = ? ,   Last_Updated_At=?  where Product_ID=?",userId,userQtyStr,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],productID, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
            }
        }

    }
    else{
        updateStock=YES;
    }
    
    
    
    /* insertIntoActualVisits
     insertIntoIssueNote
     insertIntoIssueNoteLineItems
     insertIntoEDetailing
     insertIntoVisitAdditionalInfo
     insertIntoVisitAddlInfoMeetingAttendees
     insertIntoVisitAddlInfoRating
     insertIntoPlannedVisits*/
    
    if (!insertIntoWalkinCustomers || !insertIntoIssueNote || !insertIntoIssueNoteLineItems|| !insertIntoEDetailing|| !insertIntoVisitAdditionalInfo||!insertIntoVisitAddlInfoRating||!insertIntoPlannedVisits || !insertIntoVisitAdditionalInfoNotes||!insertIntoSurveyResponses||!updatedNotesSuccessfully||!updateStock) {
        
        //something went wrong rollback
        BOOL transactionActive=[database inTransaction];
        
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        else
        {
            NSLog(@"transaction is in active");
            
        }
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        [database close];
        return NO;
    }
    else
    {
        //everything alright commit
        [database commit];
        [database close];
        return YES;
    }
}


-(NSInteger)fetchCurrentVisitCallsCount:(SalesWorxBrandAmbassadorVisit*)currentVisit
{
    NSString* countQry=[NSString stringWithFormat:@"select Count(*) Number from PHX_TBL_Walkin_Customers where Actual_Visit_ID ='%@' ", currentVisit.Actual_Visit_ID];
    NSMutableArray * countArray=[FMDBHelper executeQuery:countQry];
    if (countArray.count>0) {
        
        return countArray.count;
    }
    else
    {
        return 0;
    }
    
}

-(NSMutableArray*)fetchWalkInCustomers:(SalesWorxBrandAmbassadorVisit*)currentVisit
{
    NSString* walkCustomersQry=[NSString stringWithFormat:@"select IFNULL(Walkin_Session_ID,'N/A') AS Walkin_Session_ID, IFNULL(A.Actual_Visit_ID,'N/A') AS Actual_Visit_ID, IFNULL(C.Start_Time,'N/A')AS Start_Time, IFNULL(Customer_Name,'N/A') AS Customer_Name ,IFNULL(C.End_Time,'N/A') AS End_Time, IFNULL(Customer_Name,'N/A') AS Customer_Name   ,IFNULL(P.Planned_Visit_ID,'N/A') AS Planned_Visit_ID from PHX_TBL_Walkin_Customers AS C inner join PHX_TBL_Actual_Visits AS A on  A.Actual_Visit_ID=C.Actual_Visit_ID inner join  PHX_TBL_Planned_Visits AS P on A.Planned_Visit_ID=P.Planned_Visit_ID where P.Planned_Visit_ID='%@'",currentVisit.Planned_Visit_ID];
    NSMutableArray *contentArray=[[NSMutableArray alloc]init];

    NSMutableArray * walkinCustomersArray=[FMDBHelper executeQuery:walkCustomersQry];
    if (walkinCustomersArray.count>0) {
        for (NSMutableDictionary * currentDict in walkinCustomersArray) {
            
            SalesWorxBrandAmbassadorWalkinCustomerVisit * currentCustomer=[[SalesWorxBrandAmbassadorWalkinCustomerVisit alloc]init];
            currentCustomer.Walkin_Session_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Walkin_Session_ID"]];
            currentCustomer.Actual_Visit_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Actual_Visit_ID"]];
            
            NSLog(@"actual visit id is %@", currentCustomer.Actual_Visit_ID);
            
            
            currentCustomer.Start_Time=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Start_Time"]];
            currentCustomer.End_Time=[SWDefaults getValidStringValue:[currentDict valueForKey:@"End_Time"]];

            currentCustomer.Customer_Name=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Customer_Name"]];
            currentCustomer.Planned_Visit_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Planned_Visit_ID"]];
            
            //fetch samples and sell out given for current visit
            NSString* samplesGivenQantity=[self fetchIssueNotewithVisitID:currentCustomer.Actual_Visit_ID andSessionID:currentCustomer.Walkin_Session_ID andLotType:@"Samples"];
            
            currentCustomer.samplesGiven=samplesGivenQantity;
            NSString* sellOutGivenQty=[self fetchIssueNotewithVisitID:currentCustomer.Actual_Visit_ID andSessionID:currentCustomer.Walkin_Session_ID andLotType:@"Sellout"];
            currentCustomer.sellOutGiven=sellOutGivenQty;
            [contentArray addObject:currentCustomer];
        }
        
    }
    return contentArray;
}

-(NSString*)fetchIssueNotewithVisitID:(NSString*)visitID  andSessionID:(NSString*)sessionID andLotType:(NSString*)lotType
{
    NSString* quantityQry=[NSString stringWithFormat:@"select SUM(A.Issue_Qty) AS Qty from PHX_TBL_Issue_Note_Line_Items AS A left join PHX_TBL_Issue_Note As B on A.Doc_no=B.Doc_NO left join PHX_TBL_Walkin_Customers AS C on B.Custom_attribute_2= C.Walkin_Session_ID where C.Actual_Visit_ID='%@' and  C.Walkin_Session_ID ='%@' and A.Lot_No='%@'",visitID,sessionID,lotType];
    
    
    NSMutableArray* issueNoteDocNoArray=[self fetchDataForQuery:quantityQry];
    
    if (issueNoteDocNoArray.count>0) {
        NSString* qtyStr=[[issueNoteDocNoArray objectAtIndex:0] valueForKey:@"Qty"];

        return [SWDefaults getValidStringValue:qtyStr];
    }
    else{
        return nil;
    }

//    NSString* issueNoteDocNumber;
//    NSString* issueDocNoQry=[NSString stringWithFormat:@"select IFNULL(Doc_No,'N/A') AS Doc_No,IFNULL(Visit_ID,'N/A') AS Visit_ID from PHX_TBL_Issue_Note where Visit_ID='%@' ",visitID];
//    
//    NSMutableArray* issueNoteDocNoArray=[self fetchDataForQuery:issueDocNoQry];
//    if (issueNoteDocNoArray.count>0) {
//        issueNoteDocNumber=[[issueNoteDocNoArray objectAtIndex:0] valueForKey:@"Doc_No"];
//        NSString* issueNoteLineItemsQry=[NSString stringWithFormat:@"select IFNULL(Doc_No,'N/A') AS Doc_No,IFNULL(Issue_Qty,'N/A') AS Issue_Qty from PHX_TBL_Issue_Note_Line_Items where Doc_No='%@' and Lot_No='%@'",issueNoteDocNumber,type];
//        NSLog(@"issue note line item qry %@", issueNoteLineItemsQry);
//        NSMutableArray* issueNoteLineItemsArray=[self fetchDataForQuery:issueNoteLineItemsQry];
//        if (issueNoteLineItemsArray.count>0) {
//            for (NSInteger i=0; i<issueNoteLineItemsArray.count; i++) {
//                quantity=quantity+[[[issueNoteLineItemsArray objectAtIndex:i] valueForKey:@"Issue_Qty"] integerValue];
//            }
//        }
//    }
}



























@end
