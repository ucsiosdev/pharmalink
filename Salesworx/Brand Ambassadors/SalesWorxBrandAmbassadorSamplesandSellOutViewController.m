//
//  SalesWorxBrandAmbassadorSamplesandSellOutViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBrandAmbassadorSamplesandSellOutViewController.h"

@interface SalesWorxBrandAmbassadorSamplesandSellOutViewController ()

@end

@implementation SalesWorxBrandAmbassadorSamplesandSellOutViewController
@synthesize samplesTableView,samplesandSellOutArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    self.navigationItem.rightBarButtonItem=closeButton;
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Samples & Sellout"];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)closeTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableView Methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *CellIdentifier = @"samplesCell";
    
    
    SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    cell.productTitleLbl.text=@"Product Name";
    cell.samplesGivesLbl.text=@"Samples";
    cell.sellOutLbl.text=@"Sellout";
    return cell.contentView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (samplesandSellOutArray.count>0) {
        return samplesandSellOutArray.count;
    }
    else
    {
        return 0;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    SampleProductClass* currentSelectedProduct=[[SampleProductClass alloc]init];
//    currentSelectedProduct=[samplesGivenArray objectAtIndex:indexPath.row];
//    [self selectedObject:currentSelectedProduct];
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"samplesCell";
    SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBrandAmbassadorSamplesandSellOutTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
    }
    
    SampleProductClass* currentProduct=[[SampleProductClass alloc]init];
    currentProduct=[samplesandSellOutArray objectAtIndex:indexPath.row];
    
    tableView.allowsMultipleSelectionDuringEditing = NO;
    cell.productTitleLbl.text=currentProduct.productName;
    cell.samplesGivesLbl.text=[NSString stringWithFormat:@"%ld", (long)currentProduct.samplesGiven];
    cell.sellOutLbl.text=[NSString stringWithFormat:@"%ld", (long)currentProduct.sellOutGiven];
    return cell;
    
}

// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    [samplesGivenArray removeObjectAtIndex:indexPath.row];
//    [samplesGivenTableView reloadData];
//    noProductSelectedView.hidden=NO;
//    productTextField.text=@"";
//    
//    
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
