//
//  SalesWorxBrandAmbassadorsVisitsFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepTextField.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "SalesWorxBrandAmbassadorContentFilterViewController.h"
#import "NSPredicate+Distinct.h"
#define kSelectedDate @"Date"
#define kSelectedLocation @"Location"


@protocol BrandAmbassadorVisitFilterDelegate <NSObject>

-(void)filteredBrandAmbassadorVisits:(NSMutableArray*)filteredArray;
-(void)filteredBrandAmbassadorDidReset;
-(void)filteredParameters:(NSMutableDictionary*)parameters;
-(void)filteredBrandAmbassadorDidClose;
@end

@interface SalesWorxBrandAmbassadorsVisitsFilterViewController : UIViewController<UITextFieldDelegate>

{
    id brandAmbassadorFilterDelegate;
}
@property (strong, nonatomic) IBOutlet MedRepTextField *visitDateTextField;

@property(strong,nonatomic) id brandAmbassadorFilterDelegate;

- (IBAction)resetButtonTapped:(id)sender;
- (IBAction)searchButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepTextField *locationTextField;
@property(strong,nonatomic) NSMutableArray * contentArray;
@property(strong,nonatomic) NSMutableDictionary * filterParametersDict;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;


@end
