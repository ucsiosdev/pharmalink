////
////  StatementReportViewController.m
////  Salesworx
////
////  Created by msaad on 6/13/13.
////  Copyright (c) 2013 msaad. All rights reserved.
////
//
//#import "StatementReportViewController.h"
//
//@interface StatementReportViewController ()
//
//@end
//
//@implementation StatementReportViewController
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
////- (void)dealloc
////{
////    NSLog(@"Dealloc %@",self.title);
////    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//        self.view.backgroundColor = [UIColor whiteColor];
//    gridView = [[GridView alloc] initWithFrame:CGRectMake(302, 27, 695, 600)];
//    [gridView setDataSource:self];
//    [gridView setDelegate:self];
//    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [gridView.layer setBorderWidth:1.0f];
//    [self.view addSubview:gridView];
//    
//    filterTableView.backgroundView=nil;[filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [filterTableView.layer setBorderWidth:1.0f];
//    
//    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
//    datePickerViewController.isRoute=NO;
//    datePickerViewController.forReports=YES;
//    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
//    navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    navigationController.navigationBar.translucent = NO;
//    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
//    datePickerPopOver.delegate=self;
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//    [offsetComponents setMonth:-1]; // note that I'm setting it to -1
//    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
//    toDate=[formatter stringFromDate:[NSDate date]];
//    
//    formatter=nil;
//    usLocale=nil;
//    gregorian=nil;
//    offsetComponents=nil;
//}
//-(void)viewWillAppear:(BOOL)animated
//{
//    //NSLog(@"Statement Documents");
//     //[self viewReportAction];
//}
//- (void)typeChanged:(NSString *)sender
//{
//    [customPopOver dismissPopoverAnimated:YES];
//    if(isCustType)
//    {
//        custType=sender;
//    }
//    else
//    {
//        DocType = sender;
//    }
//    [filterTableView reloadData];
//    [self viewReportAction];
//
//}
//- (void)dateChanged:(SWDatePickerViewController *)sender
//{
//    [datePickerPopOver dismissPopoverAnimated:YES];
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    if(isFromDate)
//    {
//        fromDate=[formatter stringFromDate:sender.selectedDate];
//    }
//    else
//    {
//        toDate = [formatter stringFromDate:sender.selectedDate];
//    }
//    [filterTableView reloadData];
//    formatter=nil;
//    usLocale=nil;
//    [self viewReportAction];
//
//}
//- (void)currencyTypeChanged:(NSDictionary *)customer {
//    [currencyTypePopOver dismissPopoverAnimated:YES];
//    if ([customer count]!=0) {
//        customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//        [self viewReportAction];
//        [filterTableView reloadData];
//
//
//    }
//    else
//    {
//        dataArray = nil;
//        customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//        [gridView reloadData];
//        [filterTableView reloadData];
//
//    }
//
//}
//
//#pragma TableView Delegate
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 44;
//}
//- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
//    return 3;
//    
//}
//- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 44;
//}
//- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
//    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
//    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
//    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
//    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
////    [companyNameLabel setShadowColor:[UIColor whiteColor]];
////    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
//    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
//    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
//
//    [headerView addSubview:companyNameLabel];
//    return headerView;
//    
//}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//	return NSLocalizedString(@"Filter", nil);
//}
//- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    @autoreleasepool
//    {
//        NSString *CellIdentifier = @"CustomerDetailViewCell";
//        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
//        //try fix
//        if (!cell)
//        {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
//        }
//
//        if(indexPath.row==0)
//        {
//            if(customerDict.count!=0)
//            {
//                cell.textLabel.text = [customerDict stringForKey:@"Customer_Name"];
//            }
//            else
//            {
//                cell.textLabel.text = NSLocalizedString(@"Select Customer", nil);
//            }
//        }
//        else if(indexPath.row==1)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"From Date", nil)];
//            NSLog(@"From date %@", fromDate);
//            [cell.detailTextLabel setText:fromDate];
//        }
//        else if(indexPath.row==2)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"To Date", nil)];
//            NSLog(@"to date %@", toDate);
//            [cell.detailTextLabel setText:toDate];
//        }
////        else if(indexPath.row==3)
////        {
////            [cell.textLabel setText:@"Cust Type"];
////            [cell.detailTextLabel setText:custType];
////        }
////        else if(indexPath.row==4)
////        {
////            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
////            [cell.detailTextLabel setText:DocType];
////        }
//        else
//        {
//            [cell.textLabel setText:@"View Report"];
//            
//        }
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        cell.textLabel.font=LightFontOfSize(14.0f);
//        cell.detailTextLabel.font=LightFontOfSize(14.0);
//
//        return cell;
//    }
//}
//#pragma mark UITableView Delegate
//- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row==0)
//    {
//        currencyTypeViewController=nil;
//        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
//        [currencyTypeViewController setTarget:self];
//        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
//        [currencyTypeViewController setContentSizeForViewInPopover:CGSizeMake(400,600)];
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
//        navigationController.navigationBar.barStyle = UIBarStyleBlack;
//        navigationController.navigationBar.translucent = NO;
//        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
//        currencyTypePopOver.delegate=self;
//        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
//    }
//    else if(indexPath.row==1)
//    {
//        isFromDate=YES;
//        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else if(indexPath.row==2)
//    {
//        isFromDate=NO;
//        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
////    else if(indexPath.row==3)
////    {
////        customVC = [[CustomerPopOverViewController alloc] init] ;
////        [customVC setTarget:self];
////        [customVC setAction:@selector(typeChanged:)];
////        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
////        isCustType=YES;
////        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Cash",@"Credit" , nil]];
////        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
////    }
////    else if(indexPath.row==4)
////    {
////        customVC = [[CustomerPopOverViewController alloc] init] ;
////        [customVC setTarget:self];
////        [customVC setAction:@selector(typeChanged:)];
////        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
////        isCustType=NO;
////        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil]];
////        [customPopOver presentPopoverFromRect:CGRectMake(280,235, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
////    }
//    else
//    {
// 
//        
//    }
//    [tv deselectRowAtIndexPath:indexPath animated:YES];
//    
//}
//
//#pragma GridView Delegate
//- (int)numberOfRowsInGrid:(GridView *)gridView {
//    return dataArray.count;
//}
//
//- (int)numberOfColumnsInGrid:(GridView *)gridView {
//    return 4;
//}
//
//- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
//    NSString *title = @"";
//    
//    if (column == 0)
//    {
//        title = [NSString stringWithFormat:@"    %@",NSLocalizedString(@"Document No", nil)];
//    } else if (column == 1) {
//        title = NSLocalizedString(@"Invoice Date", nil);
//    } else if (column == 2) {
//        title = NSLocalizedString(@"Net Amount", nil);
//    } else if (column == 3) {
//        title = NSLocalizedString(@"Paid Amount", nil);
//    } else {
//        title =NSLocalizedString(@"Settled Amount", nil);
//    }
//    
//    return title;
//}
//
//- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
//    NSDictionary *data = [dataArray objectAtIndex:row];
//    
//    NSString *value = @"";
//    if (column == 0) {
//        value = [NSString stringWithFormat:@"     %@", [data objectForKey:@"C.Invoice_Ref_No"]];
//    } else if (column == 1) {
//        value = [data dateStringForKey:@"InvDate" withDateFormat:NSDateFormatterMediumStyle];
//    } else if (column == 2) {
//        value = [data currencyStringForKey:@"NetAmount"];
//    } else if (column == 3) {
//        value = [data currencyStringForKey:@"PaidAmt"];
//    } else if (column == 4) {
//        if ([data objectForKey:@"Paid"]) {
//            value = [data currencyStringForKey:@"Paid"];
//        } else {
//            value = [NSString stringWithFormat:@"%@0.00",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
//        }
//    }
//    
//    return value;
//    
//}
//- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
//    if (columnIndex == 0) {
//        return 40;
//    } else if (columnIndex == 1) {
//        return 20;
//    } else if (columnIndex == 2) {
//        return 20;
//    }
//    else if (columnIndex == 3) {
//        return 20;
//    }
//    else
//        return 0;
//}
//
//-(IBAction)viewReportAction
//{
//    if(customerDict.count!=0)
//    {
//        
//        NSString *q=    @"SELECT CAST('0' AS Bit) AS Selected, C.Invoice_Ref_No, C.Pending_Amount AS NetAmount,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate, C.Due_Date as DueDate,C.Customer_ID  FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID   LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No   WHERE invDate >= '{4}' AND invDate <='{5}' AND (C.Customer_ID={0})   UNION ALL  SELECT CAST('0' AS Bit) AS Selected, B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount,   CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate,  B.Due_Date as DueDate,B.Inv_To_Customer_ID AS Customer_ID  FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = B.Inv_To_Customer_ID AND  B.Inv_To_Site_ID = N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No   WHERE (B.ERP_Status = 'X') AND (B.Inv_To_Customer_ID={0}) AND (B.Inv_To_Site_ID={1}) AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID={0} AND Site_Use_ID={1}) ORDER BY InvDate ASC";
//        
//
//
//        q = [q stringByReplacingOccurrencesOfString:@"{0}" withString:[customerDict stringForKey:@"Customer_ID"]];
//        q = [q stringByReplacingOccurrencesOfString:@"{1}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
//        q = [q stringByReplacingOccurrencesOfString:@"{2}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
//        q = [q stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
//        q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
//        q = [q stringByReplacingOccurrencesOfString:@"{5}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
//        NSMutableArray *temp = [[SWDatabaseManager retrieveManager] dbGetDataForReport:q];
////        NSMutableArray *filteredResultSet = [NSMutableArray array];
//        
////        for (NSDictionary *row in temp){
////            @autoreleasepool{
////                int foundat = -1;
////                for (int i = 0; i < filteredResultSet.count; i++)
////                {
////                    @autoreleasepool{
////                        NSDictionary *r = [filteredResultSet objectAtIndex:i];
////
////                        if ([[r stringForKey:@"C.Invoice_Ref_No"] isEqualToString:[row stringForKey:@"C.Invoice_Ref_No"]]) {
////                            double netamount = [[r stringForKey:@"PaidAmt"] doubleValue];
////                            double paid = [[row stringForKey:@"PaidAmt"] doubleValue];
////                            netamount = netamount + paid;
////                            [r setValue:[NSNumber numberWithDouble:netamount] forKey:@"PaidAmt"];
////                            [filteredResultSet replaceObjectAtIndex:i withObject:r];
////                            foundat = i;
////                            break;
////                        }
////                    }
////                }
////                
////                if (foundat > -1) {
////                } else {
////                    [filteredResultSet addObject:row];
////                }
////                
////            }
////        }
////        
////        for (int i = filteredResultSet.count - 1; i >= 0; i--){
////            @autoreleasepool
////            {
////                NSDictionary *r = [filteredResultSet objectAtIndex:i];
////                double netamount = [[r stringForKey:@"NetAmount"] doubleValue];
////                double paid = [[r stringForKey:@"PaidAmt"] doubleValue];
////                
////                
////                if (netamount <= paid ) {
////                    [filteredResultSet removeObjectAtIndex:i];
////                }
////            }
////        }
//        
//        dataArray = nil;
//        dataArray =[NSMutableArray arrayWithArray:temp];
//        
//        [gridView reloadData];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select customer." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alert show];
//        
//
//    }
//}
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
//{
//    currencyTypePopOver=nil;
////    datePickerPopOver=nil;
////    customPopOver=nil;
////    
//    currencyTypeViewController=nil;
//    customVC=nil;
//}
//
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    
//    return YES;
//}
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        self.view = nil;
//    // Dispose of any resources that can be recreated.
//}
//
//
//
//@end





//
//  StatementReportViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "StatementReportViewController.h"
#import "MedRepDefaults.h"


@interface StatementReportViewController ()

@end

@implementation StatementReportViewController
@synthesize datePickerViewControllerDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:[kCustomerStatementTitle localizedCapitalizedString]];

    
    gridView = [[GridView alloc] initWithFrame:CGRectMake(283, 8, 733, 689)];
    [gridView setDataSource:self];
    [gridView setDelegate:self];
    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [gridView.layer setBorderWidth:1.0f];
    [self.view addSubview:gridView];
    
    filterTableView.backgroundView=nil;
    [filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [filterTableView.layer setBorderWidth:1.0f];
    
    
    filterTableView.backgroundColor=[UIColor whiteColor];

    
    datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=NO;
    
    
        self.datePickerViewControllerDate=datePickerViewController;
    
    
    
    datePickerViewController.forReports=YES;
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    navigationController.navigationBar.translucent = NO;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-1]; // note that I'm setting it to -1
    //[offsetComponents setDay:-1];
    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
    
    NSLog(@"from date in did load %@", fromDate);
    toDate=[formatter stringFromDate:[NSDate date]];
    
    NSLog(@"from date is %@", fromDate);
    
    formatter=nil;
    usLocale=nil;
    gregorian=nil;
    offsetComponents=nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"Statement Documents");
    //[self viewReportAction];
}
- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
    
}
- (void)dateChanged:(SWDatePickerViewController *)sender
{
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        NSString * sampleDate=fromDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            fromDate=sampleDate;
        }
        else
        {
            fromDate=[formatter stringFromDate:sender.selectedDate];
            
        }
        
        
    }
    else
    {
        NSString* sampleToDate=toDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            toDate=sampleToDate;
        }
        else
        {
            toDate=[formatter stringFromDate:sender.selectedDate];
            
            
        }
        
        
    }
    [filterTableView reloadData];
    

    if ([formatter stringFromDate:sender.selectedDate]==nil) {

    }
    else
    {
        [self viewReportAction];

    }
    formatter=nil;
    usLocale=nil;
    
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    if ([customer count]!=0) {
        customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
        [self viewReportAction];
        [filterTableView reloadData];
        
        
    }
    else
    {
        dataArray = nil;
        customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
        NSLog(@"customer dic description %@", [customerDict description]);
        [gridView reloadData];
        [filterTableView reloadData];
        
    }
    
}

#pragma TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
    return 3;
    
}
- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
    //    [companyNameLabel setShadowColor:[UIColor whiteColor]];
    //    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:companyNameLabel];
    return headerView;
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return NSLocalizedString(@"Filter", nil);
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool
    {
        NSString *CellIdentifier = @"CustomerDetailViewCell";
        
        
        //test fix by syed
        
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
        //try fix
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
        }
        
        if(indexPath.row==0)
        {
            if(customerDict.count!=0)
            {
                NSLog(@"customer dic description %@", [customerDict description]);
                
                cell.textLabel.text = [customerDict stringForKey:@"Customer_Name"];
            }
            else
            {
                cell.textLabel.text = NSLocalizedString(@"Select Customer", nil);
            }
        }
        else if(indexPath.row==1)
        {
            [cell.textLabel setText:NSLocalizedString(@"From Date", nil)];
            
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:fromDate]];
            NSLog(@"from date in cell %@", cell.detailTextLabel.text);
        }
        else if(indexPath.row==2)
        {
            [cell.textLabel setText:NSLocalizedString(@"To Date", nil)];
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:toDate]];
        }
        //        else if(indexPath.row==3)
        //        {
        //            [cell.textLabel setText:@"Cust Type"];
        //            [cell.detailTextLabel setText:custType];
        //        }
        //        else if(indexPath.row==4)
        //        {
        //            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
        //            [cell.detailTextLabel setText:DocType];
        //        }
        else
        {
            [cell.textLabel setText:@"View Report"];
            
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font=LightFontOfSize(14.0f);
        cell.detailTextLabel.font=LightFontOfSize(14.0);
        
        NSLog(@"Customer dic desc is %@", [customerDict description]);
        
        return cell;
    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0)
    {
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else if(indexPath.row==1)
    {
        isFromDate=YES;
        
     
        
        
//        NSDateFormatter *formatter = [NSDateFormatter new]  ;
//        
//        [formatter setDateFormat:@"yyyy-MM-dd"];
//        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//        [formatter setLocale:usLocale];
//        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        offsetComponents = [[NSDateComponents alloc] init];
//        [offsetComponents setDay:-1]; // note that I'm setting it to -1
//        //[offsetComponents setDay:-1];
//        
//        NSString * sampleDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
//        
//        
//        NSDate *sample=[formatter dateFromString:sampleDate];
//        
//        
//        NSLog( @"sample date is %@", sample);
//     
//        
//        datePickerViewControllerDate.picker.date=sample;

        NSLog(@"From Date in did select row 1 is %@", fromDate);
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:fromDate];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;

        
        
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==2)
    {
        isFromDate=NO;
        
        
        
        NSLog(@"to Date is %@", fromDate);
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:fromDate];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;
        

        
        
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    //    else if(indexPath.row==3)
    //    {
    //        customVC = [[CustomerPopOverViewController alloc] init] ;
    //        [customVC setTarget:self];
    //        [customVC setAction:@selector(typeChanged:)];
    //        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
    //        isCustType=YES;
    //        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Cash",@"Credit" , nil]];
    //        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //    }
    //    else if(indexPath.row==4)
    //    {
    //        customVC = [[CustomerPopOverViewController alloc] init] ;
    //        [customVC setTarget:self];
    //        [customVC setAction:@selector(typeChanged:)];
    //        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
    //        isCustType=NO;
    //        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil]];
    //        [customPopOver presentPopoverFromRect:CGRectMake(280,235, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //    }
    else
    {
        
        
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return dataArray.count;
    
    NSLog(@"statement report vc count %d", [dataArray count]);
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0)
    {
        title = [NSString stringWithFormat:@"    %@",NSLocalizedString(@"Document No", nil)];
    } else if (column == 1) {
        title = NSLocalizedString(@"Invoice Date", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Net Amount", nil);
    } else if (column == 3) {
        title = NSLocalizedString(@"Paid Amount", nil);
    } else {
        title =NSLocalizedString(@"Settled Amount", nil);
    }
    
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSDictionary *data = [dataArray objectAtIndex:row];
    
    NSString *value = @"";
    if (column == 0) {
        value = [NSString stringWithFormat:@"     %@", [data objectForKey:@"Invoice_Ref_No"]];
    } else if (column == 1) {
        //value = [data dateStringForKey:@"InvDate" withDateFormat:NSDateFormatterMediumStyle];
        value = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd/MM/yyyy" scrString:[data stringForKey:@"InvDate"]];
        
    } else if (column == 2) {
        value = [data currencyStringForKey:@"NetAmount"];
    } else if (column == 3) {
        value = [data currencyStringForKey:@"PaidAmt"];
    } else if (column == 4) {
        if ([data objectForKey:@"Paid"]) {
            value = [data currencyStringForKey:@"Paid"];
        } else {
            value = [NSString stringWithFormat:@"%@0.00",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
        }
    }
    NSLog(@"Customer dic desc is %@", [customerDict description]);
    return value;
    
}
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 40;
    } else if (columnIndex == 1) {
        return 20;
    } else if (columnIndex == 2) {
        return 20;
    }
    else if (columnIndex == 3) {
        return 20;
    }
    else
        return 0;
}

-(IBAction)viewReportAction
{
    if(customerDict.count!=0)
    {
        temp=nil;
        
        NSLog(@"temp is nill %@", [temp description]);
        
        //try distinct 1
        
        //       NSString *q=    @"SELECT  CAST('0' AS Bit) AS Selected, C.Invoice_Ref_No, C.Pending_Amount AS NetAmount,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate, C.Due_Date as DueDate,C.Customer_ID  FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID   LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No   WHERE invDate >= '{4}' AND invDate <='{5}' AND (C.Customer_ID={0})   UNION ALL  SELECT CAST('0' AS Bit) AS Selected, B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount,   CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate,  B.Due_Date as DueDate,B.Inv_To_Customer_ID AS Customer_ID  FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = B.Inv_To_Customer_ID AND  B.Inv_To_Site_ID = N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No   WHERE (B.ERP_Status = 'X') AND (B.Inv_To_Customer_ID={0}) AND (B.Inv_To_Site_ID={1}) AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID={0} AND Site_Use_ID={1}) ORDER BY InvDate ASC";
        
        
        //modified query to avoid duplicates
        NSString *q=@"select Invoice_Ref_No,max(NetAmount)as NetAmount,sum(PaidAmt) as PaidAmt,max(InvDate) as InvDate from ( SELECT   C.Invoice_Ref_No, C.Pending_Amount AS NetAmount,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate  FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID   LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No   WHERE invDate >= '{4}' AND invDate <='{5}' AND (C.Customer_ID={0})   UNION ALL  SELECT  B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount,   CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = B.Inv_To_Customer_ID AND  B.Inv_To_Site_ID = N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No   WHERE (B.ERP_Status = 'X') AND (B.Inv_To_Customer_ID={0}) AND (B.Inv_To_Site_ID={1}) AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID={0} AND Site_Use_ID={1}))  as X group by Invoice_Ref_No";
        
        
        
        q = [q stringByReplacingOccurrencesOfString:@"{0}" withString:[customerDict stringForKey:@"Customer_ID"]];
        q = [q stringByReplacingOccurrencesOfString:@"{1}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
        q = [q stringByReplacingOccurrencesOfString:@"{2}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
        q = [q stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
        NSLog(@"from date sent to query is %@", fromDate);
        
        q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
        q = [q stringByReplacingOccurrencesOfString:@"{5}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        
        NSLog(@"query in customer statement %@", q);
        
        
        
        temp = [[SWDatabaseManager retrieveManager] dbGetDataForReport:q];
        
        
        NSLog(@"db response of docs %@", [temp description]);
        
        NSLog(@"customer statement response count %d",[temp count] );
        
        
        //        NSLog(@"paid amount value key %@", [temp valueForKey:@"PaidAmt"]);
        //
        //
        //
        //
        //        //clubbing the paid amount
        //        NSMutableArray * paidAmountArray=[NSMutableArray arrayWithArray:[temp valueForKey:@"PaidAmt"]];
        //
        //        NSLog(@"paid amount array is %@", [paidAmountArray description]);
        //
        //        [paidAmountArray removeObjectIdenticalTo:[NSNull null]];
        //
        //        NSLog(@"array after removing nulls %@", [paidAmountArray description]);
        
        
        
        
        
        
        //
        //        int total=0;
        //
        //        for (int i=0; i<[paidAmountArray count]; i++) {
        //
        //                total= total+[[paidAmountArray objectAtIndex:i]intValue];
        //
        //        }
        //
        //        NSLog(@"total is %d", total);
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //        NSMutableArray *filteredResultSet = [NSMutableArray array];
        
        //        for (NSDictionary *row in temp){
        //            @autoreleasepool{
        //                int foundat = -1;
        //                for (int i = 0; i < filteredResultSet.count; i++)
        //                {
        //                    @autoreleasepool{
        //                        NSDictionary *r = [filteredResultSet objectAtIndex:i];
        //
        //                        if ([[r stringForKey:@"C.Invoice_Ref_No"] isEqualToString:[row stringForKey:@"C.Invoice_Ref_No"]]) {
        //                            double netamount = [[r stringForKey:@"PaidAmt"] doubleValue];
        //                            double paid = [[row stringForKey:@"PaidAmt"] doubleValue];
        //                            netamount = netamount + paid;
        //                            [r setValue:[NSNumber numberWithDouble:netamount] forKey:@"PaidAmt"];
        //                            [filteredResultSet replaceObjectAtIndex:i withObject:r];
        //                            foundat = i;
        //                            break;
        //                        }
        //                    }
        //                }
        //
        //                if (foundat > -1) {
        //                } else {
        //                    [filteredResultSet addObject:row];
        //                }
        //
        //            }
        //        }
        //
        //        for (int i = filteredResultSet.count - 1; i >= 0; i--){
        //            @autoreleasepool
        //            {
        //                NSDictionary *r = [filteredResultSet objectAtIndex:i];
        //                double netamount = [[r stringForKey:@"NetAmount"] doubleValue];
        //                double paid = [[r stringForKey:@"PaidAmt"] doubleValue];
        //
        //
        //                if (netamount <= paid ) {
        //                    [filteredResultSet removeObjectAtIndex:i];
        //                }
        //            }
        //        }
        
        dataArray = nil;
        dataArray =[NSMutableArray arrayWithArray:temp];
        
        [gridView reloadData];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select customer." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    //    datePickerPopOver=nil;
    //    customPopOver=nil;
    //    
    currencyTypeViewController=nil;
    customVC=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}



@end

