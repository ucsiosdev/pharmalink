//
//  SalesWorxReportsDateSelectorViewController.m
//  CustomReportDateSelector
//
//  Created by Pavan Kumar Singamsetti on 11/1/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "SalesWorxReportsDateSelectorViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxDateHelper.h"
//#import "MedRepQueries.h"
#import "SalesWorxReportsDateSelectorCollectionViewCell.h"
@interface SalesWorxReportsDateSelectorViewController (){
    SalesWorxDateHelper *dateHelper;
}
@end

@implementation SalesWorxReportsDateSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:YES];
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([@"Date Range" localizedCapitalizedString], nil)];

    
    // Do any additional setup after loading the view from its nib.
    DatePickerRangesArray = [[NSMutableArray alloc] initWithObjects:@"Today",@"Last 3 days",@"Last 7 days",@"This Month",@"This Year", nil];
    dateHelper=[[SalesWorxDateHelper alloc] init];
    
    UIBarButtonItem* DoneButton=[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(DoneButtonTapped)];
    self.view.backgroundColor=UIViewBackGroundColor;
    // [DoneButton setTitleTextAttributes:[MedRepQueries fetchBarAttributes] forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem: DoneButton];
    [dateRangeCollectionView registerClass:[SalesWorxReportsDateSelectorCollectionViewCell class] forCellWithReuseIdentifier:@"DateSelectorCellIdentifier"];
    
    
    FromDatePicker.maximumDate=[NSDate date];
    ToDatePicker.maximumDate=[NSDate date];

    FromDatePicker.datePickerMode=UIDatePickerModeDate;
    ToDatePicker.datePickerMode=UIDatePickerModeDate;
    FromDateValueLabel.text=[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:FromDatePicker.date];
    ToDateValueLabel.text=[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:FromDatePicker.date];

}
-(void)DoneButtonTapped{
    
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate DidUserSelectDateRange:FromDatePicker.date EndDate:ToDatePicker.date AndCustomRangePeriodText:nil];

    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)DatePickerValueChanged:(UIDatePicker *)sender{
    FromDateValueLabel.text=[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:FromDatePicker.date];
    ToDateValueLabel.text=[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:ToDatePicker.date];
    

    
    if ([FromDatePicker.date compare:ToDatePicker.date] == NSOrderedDescending) {
        ToDatePicker.date=FromDatePicker.date;
        ToDateValueLabel.text=[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:FromDatePicker.date];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark UICollectionview Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  DatePickerRangesArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width/3 -5, 50);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"DateSelectorCellIdentifier";
    
    SalesWorxReportsDateSelectorCollectionViewCell *cell = (SalesWorxReportsDateSelectorCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.TitleLabel.text=[DatePickerRangesArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDate * startDate = [NSDate date];
    NSDate * endDate = [NSDate date];

    if(indexPath.row==0){
    }else if(indexPath.row==1){
        startDate = [dateHelper addToDate:[NSDate date] days:-2];
    }else if(indexPath.row==2){
        startDate = [dateHelper addToDate:[NSDate date] days:-6];
    }else if(indexPath.row==3){
        startDate = [dateHelper firstDayOfMonth:[NSDate date]];
    }else if(indexPath.row==4){
        startDate = [dateHelper firstDayOfYear:[NSDate date]];
    }
    
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate DidUserSelectDateRange:startDate EndDate:endDate AndCustomRangePeriodText:[DatePickerRangesArray objectAtIndex:indexPath.row]];
    }];
}

@end
