//
//  MenuViewController.m
//  SWDashboard
//
//  Created by Irfan Bashir on 5/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
//UINavigationController *navigationControllerFront
#import "MenuViewController.h"
#import "SWFoundation.h"
#import "MenuTableViewCell.h"
#import "MenuItem.h"
#import <objc/runtime.h>
#import "SWPlatform.h"
#import "SWDashboardViewController.h"
#import "SalesWorxRouteViewController.h"
#import "AboutUsViewController.h"
#import "SWProductListViewController.h"
#import "CustomersListViewController.h"
#import "SalesWorxReportsViewController.h"
#import "SWCollectionCustListViewController.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "ReturnNewViewController.h"
#import "DashboardAlSeerViewController.h"
#import "DashboardAlSeerViewController.h"
#import "Singleton.h"
#import "LoginViewController.h"
#import "SalesWorxPaymentCollectionViewController.h"
#import "SWCustomersViewController.h"
#import "SalesWorxProductsViewController.h"
@interface MenuViewController ()
- (void)configureToolbar;
@end

@implementation MenuViewController


- (id)init
{
    self = [super init];
    if (self)
    {
        AppControl *appControl = [AppControl retrieveSingleton];
        isCollectionEnable = appControl.ENABLE_COLLECTION; //[SWDefaults appControl]count]
        
        
        //check the custom details screen(its a dashboard in case of al seer)
        
        NSString* custDetailsScreen=appControl.CUST_DTL_SCREEN;
        
    
        customerIsCollapsed=YES;
        siteIsCollapsed=YES;
        
        
        
        //customerSer.delegate = self;
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            
            
                            modules=[NSArray arrayWithObjects:
                         [MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"SalesWorxFieldSalesDashboardViewController" imageName:@"Menu_Dashboard"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SalesWorxRouteViewController" imageName:@"Menu_Route"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"],
                     //   [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"CustomersListViewController" imageName:@"Menu_Customer"],
                             [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"SWCustomersViewController" imageName:@"Menu_Customer"],
                        
                        [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SalesWorxProductsViewController" imageName:@"Menu_Products"],
                        
//                         [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SWProductListViewController" imageName:@"Menu_Products"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Collection", nil) className:@"SWCollectionCustListViewController" imageName:@"Menu_Collection"],
                                     
                         //[MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"GenCustListViewController" imageName:@"Menu_Survey"],
                                     [MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"SalesWorxSurveyViewController" imageName:@"Menu_Survey"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"],nil];
                
//                         [MenuItem itemWithTitle:NSLocalizedString(@"Logout", nil) className:@"Logout" imageName:@"Menu_Logout"],nil];
                
            }

            
            
                  
        else
        {
            
            
            
            
         
          
                modules=[NSArray arrayWithObjects:
                         [MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"SalesWorxFieldSalesDashboardViewController" imageName:@"Menu_Dashboard"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SalesWorxRouteViewController" imageName:@"Menu_Route"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"],
                      //  [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"CustomersListViewController" imageName:@"Menu_Customer"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"SWCustomersViewController" imageName:@"Menu_Customer"],
                        
                         [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SalesWorxProductsViewController" imageName:@"Menu_Products"],
                         
                        // [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SWProductListViewController" imageName:@"Menu_Products"],
//                         [MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"GenCustListViewController" imageName:@"Menu_Survey"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"SalesWorxSurveyViewController" imageName:@"Menu_Survey"],
                         [MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"],nil];
                
//                         [MenuItem itemWithTitle:NSLocalizedString(@"Logout", nil) className:@"Logout" imageName:@"Menu_Logout"],nil];
 
            
            
                   }
        

        
//        if ([[SWDefaults checkTgtField]isEqualToString:@"Y"]) {
//            SWDashboardViewController1 = (UIViewController *)[[SWDashboardViewController alloc] init];
//
//            
//        }
//        else
//        {
//            dashboardAlSeerViewController1 = (UIViewController *)[[DashboardAlSeerViewController alloc] init];
//
//        }
        SWDashboardViewController1 = (UIViewController *)[[SalesWorxFieldSalesDashboardViewController alloc] init];
        dashboardAlSeerViewController1 = (UIViewController *)[[DashboardAlSeerViewController alloc] init];

        ReportViewController1 =  (UIViewController *)[[SalesWorxReportsViewController alloc]init];
        aboutVController = [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil] ;
        synch = [[SynViewController alloc] init] ;
        settingVController = [[SettingViewController alloc] init] ;
        SalesWorxRouteViewController1 = (UIViewController *)[[SalesWorxRouteViewController alloc] init];
        CommunicationViewControllerNew1 = (UIViewController *)[[CommunicationViewControllerNew alloc]initWithNibName:@"CommunicationViewControllerNew" bundle:nil] ;
//        SWCollectionCustListViewController1 =  (UIViewController *)[[SWCollectionCustListViewController alloc]init];

        SWCollectionCustListViewController1 =  (UIViewController *)[[SalesWorxPaymentCollectionViewController alloc]init];
       // SWCustomerListViewController1 =  (UIViewController *)[[CustomersListViewController alloc]init];
        
        //add here
        SWCustomerListViewController1 =  (UIViewController *)[[SWCustomersViewController alloc]init];
        
//        SWProductListViewController1  =  (UIViewController *)[[SWProductListViewController alloc]init];
        SWProductListViewController1  =  (UIViewController *)[[SalesWorxProductsViewController alloc]init];
        
//        SurveyFormViewController1 =  (UIViewController *)[[GenCustListViewController alloc] init] ;
        SurveyFormViewController1 =  (UIViewController *)[[SalesWorxSurveyViewController alloc] init] ;
        newSalesOrder1 = (UIViewController *)[[ReturnNewViewController alloc]initWithNibName:@"ReturnNewViewController" bundle:nil] ;

    }
    
    return self;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Menu";
    [self.view setBackgroundColor:UIColorFromRGB(0x1A2635)];
    self.view.backgroundColor = [UIColor whiteColor];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }


    
    //self.navigationController.navigationBar
    
    tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height-20) style:UITableViewStylePlain ];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tableView setScrollEnabled:YES];
    
    [self.view addSubview:tableView];
    
    [self configureToolbar];
    //customerSer.delegate = self;
    //[SWDatabaseManager retrieveManager] dbGetCashCutomerList];
   // [self getServiceDidGetCasCutomerList:[[SWDatabaseManager retrieveManager] dbGetCashCutomerList]];
}

- (void)configureToolbar {
    
    UIButton* infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoButton addTarget:self action:@selector(infoButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    
    // UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_Sync" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(synchronizeAction:)] ;
    
    
    UIBarButtonItem *syncButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_Sync"] style:UIBarButtonItemStylePlain target:self action:@selector(displaySyncPopover)] ;
    
  //  UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SettingIcon" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(settingAction:)] ;
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_Logout"] style:UIBarButtonItemStylePlain target:self action:@selector(logoutTapped)] ;
    
    
    
    [self.navigationController setToolbarHidden:NO animated:YES];
    [self setToolbarItems:[NSArray arrayWithObjects:modalButton,flexibleSpace,syncButton,flexibleSpace,logoutButton, nil] animated:YES];
    
    
    
}
-(void)configureBackOfficeURLView{
    
}

-(void)displaySyncPopover
{
    SalesWorxSyncPopoverViewController *presentingView=   [[SalesWorxSyncPopoverViewController alloc]init];
    UINavigationController * syncNavController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    syncNavController.navigationBarHidden=YES;
    syncNavController.view.backgroundColor = [UIColor colorWithRed:(86.0/255.0) green:(86.0/255.0)  blue:(86.0/255.0)  alpha:0.5];
    syncNavController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = [UIColor colorWithRed:(86.0/255.0) green:(86.0/255.0)  blue:(86.0/255.0)  alpha:0.5];
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:syncNavController animated:NO completion:nil];
}

-(void)logoutTapped

{
    
    NSLog(@"log out tapped");
    
    
    UINavigationController *navigationControllerFront;
    
    
    
    Singleton *single = [Singleton retrieveSingleton];
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    
    LoginViewController * loginVC=[[LoginViewController alloc]init];
    
    
    dashBViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init] ;
//        navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:dashBViewController] ;
    
    navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
    
    
    
    
    if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[LoginViewController class]])
    {
        [revealController setFrontViewController:navigationControllerFront animated:YES];
        
    } else {
        [revealController revealToggle:self];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionLogout" object:nil];
}

-(void)infoButtonAction
{
   //Singleton *single = [Singleton retrieveSingleton];
    //revealController=nil;
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
//    single.splitViewObject=nil;
//    single.splitViewObject = revealController;
    
    if (modules != nil)
    {
        UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:aboutVController] ;
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[aboutVController class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
            
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
}
- (void)synchronizeAction:(id)sender
{
    //single=nil;
  // Singleton *single = [Singleton retrieveSingleton];
    //revealController=nil;
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
//    single.splitViewObject=nil;
//    single.splitViewObject = revealController;
    
    if (modules != nil)
    {
        if(synch)
        {
            synch=nil;
        }
        synch = [[SynViewController alloc] init] ;
        [synch setTarget:nil];
        [synch setAction:nil];

        UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:synch] ;
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[synch class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
    
}
- (void)settingAction:(id)sender
{

    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;

    if (modules != nil)
    {
        UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:settingVController] ;
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[settingVController class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
}
#pragma mark UITableView Datasource
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 2;
//}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    
//    return [modules count];
//}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
//    if(section == 0)
//    {
//        if(customerIsCollapsed)
//            return 0;
//        else
//            return modules.count;
//    }
//    else if (section == 1)
//    {
//        if(siteIsCollapsed)
//            return 0;
//        else
//            return modules.count;
//        
//    }
    return modules.count;
}


#pragma mark Med Rep Methods

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 44;
//}



- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section>0) return YES;
    
    return NO;
}

/*

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 35)];
    
    UILabel *lblSection = [UILabel new];
    [lblSection setFrame:CGRectMake(0, 0, 300, 30)];
    [lblSection setBackgroundColor:[UIColor clearColor]];
    lblSection.alpha = 0.5;
    if(section == 0)
    {
        if(!customerIsCollapsed)
            [lblSection setText:@"Sales Order"];
        else
            [lblSection setText:@"Sales Order"];
    }
    else
    {
        if(!siteIsCollapsed)
            [lblSection setText:@"Med Rep"];
        else
            [lblSection setText:@"Med Rep"];    }
    
    UIButton *btnCollapse = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCollapse setFrame:CGRectMake(0, 0,260, 35)];
    [btnCollapse setBackgroundColor:[UIColor clearColor]];
    [btnCollapse addTarget:self action:@selector(touchedSection:) forControlEvents:UIControlEventTouchUpInside];
    btnCollapse.tag = section;
    
    
    [headerView addSubview:lblSection];
    [headerView addSubview:btnCollapse];
    
    return headerView;
}

 */
 

- (IBAction)touchedSection:(id)sender
{
    UIButton *btnSection = (UIButton *)sender;
    
    if(btnSection.tag == 0)
    {
        NSLog(@"Touched Customers header");
        if(!customerIsCollapsed)
            customerIsCollapsed = YES;
        else
            customerIsCollapsed = NO;
        
    }
    else if(btnSection.tag == 1)
    {
        NSLog(@"Touched Site header");
        if(!siteIsCollapsed)
            siteIsCollapsed = YES;
        else
            siteIsCollapsed = NO;
        
       }
   // [tableView reloadData];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:btnSection.tag] withRowAnimation:UITableViewRowAnimationFade];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"CellIdentifier";
    MenuTableViewCell *cell = [tv dequeueReusableCellWithIdentifier:identifier];
    
    if (nil == cell)
    {
        cell = [[MenuTableViewCell alloc] initWithReuseIdentifier:identifier] ;
        
    }
    
    MenuItem *menuItem = [modules objectAtIndex:indexPath.row];
    [cell setImageName:menuItem.imageName];
    [cell.textLabel setText:menuItem.title];
    return cell;
}

#pragma mark UITableView delegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
    
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    synch=nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    Singleton *single = [Singleton retrieveSingleton];
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;

    MenuItem *menuItem = [modules objectAtIndex:indexPath.row];
    
    [tableView1 deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *className = menuItem.className;
    
    
    if([className isEqualToString:@"Logout"])
    {
        
        UINavigationController *navigationControllerFront;
        
        
        
        
        
        if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
            
            /*alseerDashBoard=[[DashboardAlSeerViewController alloc] init];
            navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:alseerDashBoard] ;*/
            dashBViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init] ;
            navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:dashBViewController] ;

            
        }
        else
        {
            dashBViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init] ;
            navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:dashBViewController] ;
        }
        
        
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[dashBViewController class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
            
        } else {
            [revealController revealToggle:self];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionLogout" object:nil];
    }
    
    else if (modules != nil)
    {
        UIViewController *viewController = nil;
        
        
        
//        if ([[SWDefaults checkTgtField]isEqualToString:@"Y"]) {
//            
//          
//        }
//        
//
//        
//        else
//        {
//            
//        }
        
        if([className isEqualToString:@"SalesWorxFieldSalesDashboardViewController"])
        {
            if (single.isFullSyncDashboard) {
                SWDashboardViewController1 = nil;
                SWDashboardViewController1 = (UIViewController *)[[SalesWorxFieldSalesDashboardViewController alloc] init];
                single.isFullSyncDashboard=NO;
            }
            
            
            
            
            viewController = SWDashboardViewController1;
        }
        
        else if([className isEqualToString:@"DashboardAlSeerViewController"])
        {
            if (single.isFullSyncMessage) {
                dashboardAlSeerViewController1=nil;
                dashboardAlSeerViewController1 = (UIViewController *)[[DashboardAlSeerViewController alloc]initWithNibName:@"DashboardAlSeerViewController" bundle:nil] ;
                single.isFullSyncMessage=NO;
            }
            viewController = dashboardAlSeerViewController1;
        }

        
        else if([className isEqualToString:@"CommunicationViewControllerNew"])
        {
            if (single.isFullSyncMessage) {
                CommunicationViewControllerNew1=nil;
                CommunicationViewControllerNew1 = (UIViewController *)[[CommunicationViewControllerNew alloc]initWithNibName:@"CommunicationViewControllerNew" bundle:nil] ;
                single.isFullSyncMessage=NO;
            }
            viewController = CommunicationViewControllerNew1;
        }
//        else if([className isEqualToString:@"GenCustListViewController"])
        else if([className isEqualToString:@"SalesWorxSurveyViewController"])
        {
            if (single.isFullSyncSurvey)
            {
                SurveyFormViewController1=nil;
//                SurveyFormViewController1 =  (UIViewController *)[[GenCustListViewController alloc] init] ;
                SurveyFormViewController1 =  (UIViewController *)[[SalesWorxSurveyViewController alloc] init] ;
                single.isFullSyncSurvey=NO;
            }
            viewController = SurveyFormViewController1;
        }
        else if([className isEqualToString:@"SalesWorxRouteViewController"])
        {
            
            viewController = SalesWorxRouteViewController1;
        }
        
        
        
        
        else if([className isEqualToString:@"SalesWorxProductsViewController"])
        {
            if (single.isFullSyncProduct) {
                SWProductListViewController1=nil;
                SWProductListViewController1  =  (UIViewController *)[[SalesWorxProductsViewController alloc]init];
                single.isFullSyncProduct=NO;
            }
            viewController = SWProductListViewController1;

        }

//        else if([className isEqualToString:@"SWProductListViewController"])
//        {
//            if (single.isFullSyncProduct) {
//                SWProductListViewController1=nil;
//                SWProductListViewController1  =  (UIViewController *)[[SWProductListViewController alloc]init];
//                single.isFullSyncProduct=NO;
//            }
//            viewController = SWProductListViewController1;
//        }
        
        
        
        else if([className isEqualToString:@"SWCustomersViewController"])
            {
                if (single.isFullSyncCustomer) {
                    SWCustomerListViewController1 =nil;
                    SWCustomerListViewController1 =  (UIViewController *)[[SWCustomersViewController alloc]init];
                    single.isFullSyncCustomer=NO;
                }
                
                viewController = SWCustomerListViewController1;
            }

        
        
//        else if([className isEqualToString:@"CustomersListViewController"])
//       {
//            if (single.isFullSyncCustomer) {
//                SWCustomerListViewController1 =nil;
//                SWCustomerListViewController1 =  (UIViewController *)[[CustomersListViewController alloc]init];
//                single.isFullSyncCustomer=NO;
//            }
//           
//            viewController = SWCustomerListViewController1;
//        }
        
        else if([className isEqualToString:@"SalesWorxReportsViewController"])
        {
            viewController = ReportViewController1;
        }
        
        else if([className isEqualToString:@"SWCollectionCustListViewController"])
        {
            if (single.isFullSyncCollection)
            {
                SWCollectionCustListViewController1=nil;
                SWCollectionCustListViewController1 =  (UIViewController *)[[SalesWorxPaymentCollectionViewController alloc]init];
                single.isFullSyncCollection=NO;
            }
            viewController = SWCollectionCustListViewController1;
            
            
//            SalesWorxPaymentCollectionViewController* collectionVC=[[SalesWorxPaymentCollectionViewController alloc]init];
//            
//            if (single.isFullSyncCollection)
//                
//            {
//                single.isFullSyncCollection=NO;
//                
//            }
//            [self setSliderRootViewController:collectionVC];
            
        }

        UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:viewController] ;
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[viewController class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else { // Seems the user attempts to 'switch' to exactly the same controller he came from!
            [revealController revealToggle:self];
            
        }
    }
}


-(void)setSliderRootViewController:(UIViewController*)viewController

{
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    
    
    UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:viewController] ;
    if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[viewController class]])
    {
        [revealController setFrontViewController:navigationControllerFront animated:YES];
    }
    else { // Seems the user attempts to 'switch' to exactly the same controller he came from!
        [revealController revealToggle:self];
        
    }
    
    
    
}

- (void) report_memory {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        //NSLog(@"Memory in use (in MB): %f", info.resident_size/(1024*1024.f));
    } else {
        //NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
    }
}
//- (void)customerServiceDidGetCasCutomerList:(NSArray *)customerList
- (void)getServiceDidGetCasCutomerList:(NSArray *)customerList
{
   Singleton *single = [Singleton retrieveSingleton];
    single.cashCustomerList = customerList;
    customerList=nil;
}

- (void)lastSychDelegate:(NSString *)synchDate
{
    dateString = synchDate;
    [self configureToolbar];
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
