//
//  SWOrderConfirmationViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/3/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AlSeerSignatureViewController.h"
@interface SWOrderConfirmationViewController : UIViewController<UIPopoverControllerDelegate,UITextFieldDelegate,UITextViewDelegate>


{
    NSString *ENABLE_ORDER_CONSOLIDATION;
    NSString *ALLOW_SKIP_CONSOLIDATION;
    BOOL isWholeSalesOrder;
    BOOL isEOConsolidation;
    BOOL isASConsolidation;
    NSString* checkWholesaleOrder;
    CGRect oldFrame;
    
    
    BOOL wholesaleOrder;
    
    BOOL alertDisplayed;
    
    BOOL skipConsolidation;
    
    
    
    UIDatePicker* datepicker;
    UIPopoverController* popoverControllerForDate;
    
    
    NSMutableDictionary* orderAdditionalInfoDict;
    BOOL animateViewUp;
    BOOL textViewTapped;
    NSString*  isSignatureOptional;
    
    UIPopoverController *shipDatePopOver;

}

@property (strong, nonatomic) IBOutlet UITextField *txtFldShipDate;
@property(nonatomic,strong)NSString *proformaOrderRef;
@property(strong,nonatomic)NSMutableArray * warehouseDataArray;
@property(strong,nonatomic)NSMutableArray * itemsSalesOrder;

@property(strong,nonatomic)AlSeerSignatureViewController* signVC;

@property(strong,nonatomic)NSMutableDictionary* salesOrderDict,*customerDict;


@property(strong,nonatomic)NSNumber* wholesaleNumber, *skipConsolidationNumber;

@property (strong, nonatomic) IBOutlet UIButton *shipdateBtn;

- (IBAction)datePickerBtnTapped:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *customerIDLbl;
@property (strong, nonatomic) IBOutlet UITextField *txtfldCustomerDocRef;
@property (strong, nonatomic) IBOutlet UITextView *commentsTxtView;
@property (strong, nonatomic) IBOutlet UITextField *nameTxtFld;
@property (strong, nonatomic) IBOutlet UIView *customSignatureView;
@property (strong, nonatomic) IBOutlet UITextField *customerNameTxtFld;

- (IBAction)saveButtonTapped:(id)sender;
- (IBAction)confirmButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *wholesaleOrderLbl;

@property (strong, nonatomic) IBOutlet UISegmentedControl *wholesaleOrderSegment;


@property (strong, nonatomic) IBOutlet UILabel *skipConsolidationLbl;

@property (strong, nonatomic) IBOutlet UISegmentedControl *skipConsolidationSegment;



- (IBAction)wholesaleSegmentTapped:(UISegmentedControl *)sender;

- (IBAction)skipConsolidationTapped:(UISegmentedControl *)sender;




@end
