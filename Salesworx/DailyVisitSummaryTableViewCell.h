//
//  DailyVisitSummaryTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface DailyVisitSummaryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblCustomerCode;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDC;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblOrders;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblOrderValue;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblReturns;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblReturnsValue;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPayments;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPaymentValue;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblSurvey;
@property (weak, nonatomic) IBOutlet UIButton *btnVisitNotes;


@end
