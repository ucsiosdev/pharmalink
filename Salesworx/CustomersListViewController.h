//
//  SWCustomersListViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "SWLoadingView.h"
#import "ZBarSDK.h"
#import "Singleton.h"





@interface CustomersListViewController : SWViewController < UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, UIPopoverControllerDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate> {
    
   
    
    NSMutableArray *customerList;
    
    UITableView *tableView;
    UISearchDisplayController *searchController;
    UISearchBar *searchBar;
    NSMutableArray *searchData;
    NSMutableDictionary *customerDict;

    UIPopoverController *locationFilterPopOver;
    int totalRecords;
//       id target;
    SEL action;
    //SWSection *section;
    NSMutableDictionary *sectionList;
    NSMutableDictionary *sectionSearch;

    SWLoadingView *loadingView;
    UILabel *infoLabel;
    ZBarReaderViewController *reader;
    //

    UIBarButtonItem *flexibleSpace ;
    UIBarButtonItem *totalLabelButton;
    UIBarButtonItem *displayActionBarButton ;
    UIBarButtonItem *displayMapBarButton ;
    UIView* myBackgroundView;
    
}


//@property (nonatomic, strong)  NSMutableDictionary *customerDict;

//@property (nonatomic, strong) NSMutableArray *customerList;
//@property (nonatomic, strong) NSMutableArray *searchData;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@end