//
//  SWQuery.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWQuery : NSObject {
    NSString *query;
    int tag;
    NSArray *result;
    BOOL isSectionedQuery;
    NSDictionary *params;
}

@property (nonatomic, strong) NSString *query;
@property (nonatomic, assign) int tag;
@property (nonatomic, strong) NSArray *result;
@property (nonatomic, assign) BOOL isSectionedQuery;
@property (nonatomic, strong) NSDictionary *params;

@end