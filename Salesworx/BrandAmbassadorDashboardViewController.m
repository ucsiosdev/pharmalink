//
//  BrandAmbassadorDashboardViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 1/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "BrandAmbassadorDashboardViewController.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepMenuViewController.h"
#import "TaskPopoverViewController.h"
#import "SalesWorxBrandAmbassadorsViewController.h"

#define kMovetKey @"moveViewAnimation"


@interface BrandAmbassadorDashboardViewController ()

@end

@implementation BrandAmbassadorDashboardViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIViewBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Dashboard"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view layoutIfNeeded];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        } else {
        }
    }
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingDashboard];
    }
    
    // get Current month
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    df.dateFormat=@"yyyy-MM";
    monthString = [[df stringFromDate:[NSDate date]] capitalizedString];
    
    
    NSString* currentDateinDisplayFormat = [MedRepQueries fetchDatabaseDateFormat];
    NSMutableArray *VisitsArray = [MedRepQueries fetchPlannedVisitsforSelectedDate:currentDateinDisplayFormat];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    NSDate *currentDate = [dateFormatter dateFromString:dateString];
    
    @try {
        NSMutableArray *plannedVisit = [[NSMutableArray alloc]init];
        for (int i = 0; i<[VisitsArray count]; i++)
        {
            NSString *dateStr = [[VisitsArray objectAtIndex:i] objectForKey:@"Visit_Date"];
            NSDate *date = [dateFormatter dateFromString:dateStr];
            
            if (![[[VisitsArray objectAtIndex:i]valueForKey:@"Visit_Status"] isEqualToString:@"Y"] && ([date compare:currentDate] == NSOrderedDescending)) {
                [plannedVisit addObject:[VisitsArray objectAtIndex:i]];
            }
        }
        if ([plannedVisit count] == 0)
        {
            self.viewUpcomingAppointment.hidden = YES;
        } else
        {
            
            NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"Visit_Date" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
            sortedPlannedVisitArray = [plannedVisit sortedArrayUsingDescriptors:sortDescriptors];
            
            if ([[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Doctor_Name"] isEqualToString:@"N/A"])
            {
                _lblDoctorName.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Pharmacy_Contact_Name"];
            }
            else
            {
                _lblDoctorName.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Doctor_Name"];
            }
            _lblLocation.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Location_Name"];
            _lblTelephoneNo.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Phone"];
            if (_lblTelephoneNo.text.length == 0) {
                _lblTelephoneNo.text = @"--";
            }
            
            _lblTime.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"hh:mm" scrString:[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Visit_Date"]];
            
            _lblATime.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"a" scrString:[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Visit_Date"]];
            
            _lblDate.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"dd MMMM" scrString:[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Visit_Date"]];
        }
    }
    @catch (NSException *exception) {
        
    }
    
    if (_viewTasks.layer.sublayers.count>0) {
        
        [_viewTasks.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    }
    
    // Load Tasks data of Doctors and Pharmacies
    NSMutableArray *arrTasks = [MedRepQueries fetchTotalTasksForDoctorAndLocation];
    int tasksOpen = 0;
    int tasksCompleted = 0;
    
    for (int i = 0; i<[arrTasks count]; i++)
    {
        NSString * task = [[arrTasks objectAtIndex:i]valueForKey:@"Status"];
        
        if ([task isEqualToString:@"Closed"])
        {
            tasksCompleted++;
        }
        else if ([task isEqualToString:@"New"] || [task isEqualToString:@"Deffered"])
        {
            tasksOpen++;
        }
        
    }
    _lblTasksOpen.text = [NSString stringWithFormat:@"%d",tasksOpen];
    _lblTasksCompleted.text = [NSString stringWithFormat:@"%d",tasksCompleted];
    
    if (tasksOpen+tasksCompleted == 0 && tasksCompleted > 0)
    {
        [self loadPercentageChart:circleChartForTasks :_viewTasks :@100 :@100 :[UIColor colorWithRed:(38.0/255.0) green:(196.0/255.0) blue:(165.0/255.0) alpha:1]];
    }else
    {
        [self loadPercentageChart:circleChartForTasks :_viewTasks :[NSNumber numberWithInt:(tasksOpen+tasksCompleted)]  :[NSNumber numberWithInt:tasksCompleted] :[UIColor colorWithRed:(38.0/255.0) green:(196.0/255.0) blue:(165.0/255.0) alpha:1]];
    }
    
    btnTask = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"TaskIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnTask)];
    [btnTask setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = btnTask;
}

- (void)btnTask{
    
    TaskPopoverViewController *presentingView = [[TaskPopoverViewController alloc]init];
    UINavigationController *toDoPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    toDoPopUpViewController.navigationBarHidden=YES;
    toDoPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    toDoPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:toDoPopUpViewController animated:NO completion:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadVisitFrequency];
    [self loadViewForDemonstratedProducts];
    [self loadViewForFaceTimeVsWaitTime];
    [self loadViewForProductiveVisits];
}

#pragma mark Load PercentageChart
-(void)loadPercentageChart:(PNCircleChart *)circleChart :(UIView *)view :(NSNumber *)totalPercentage :(NSNumber *)percentage :(UIColor *)gradientColor
{
    circleChart = [[PNCircleChart alloc] initWithFrame:CGRectMake(52, 0, 160, 160) total:totalPercentage current:percentage clockwise:YES shadow:NO shadowColor:[UIColor colorWithRed:(232.0f/255.0f) green:(243.0f/255.0f) blue:(246.0f/255.0f) alpha:1] check:NO lineWidth:@8.0f];
    
    circleChart.backgroundColor = [UIColor clearColor];
    [circleChart setStrokeColor:[UIColor clearColor]];
    [circleChart setStrokeColorGradientStart:gradientColor];
    [circleChart strokeChart];
    
    [view addSubview:circleChart];
}


#pragma mark Load Chart for Demonstrated Products
-(void)loadViewForDemonstratedProducts
{
    [_chart reset];

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSMutableArray *arrValue = [[NSMutableArray alloc]init];
        arrproductName = [[NSMutableArray alloc]init];
        
        NSMutableArray *CountMedialFiles = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT  IFNULL(p.Product_Name,'') AS Product_Name, e.Product_ID, IFNULL(COUNT(*),0) AS ProductCount   FROM PHX_TBL_EDetailing AS e left join phx_tbl_products as p on e.Product_ID=p.Product_ID   where e.Discussed_At like '%%%@%%' and  p.Product_Name not null GROUP BY e.Product_ID ORDER BY e.Product_ID ASC limit 5",monthString]];
        
        if ([CountMedialFiles count] == 0) {
            viewDemonstratedGraph.hidden = YES;
        }
        else
        {
            arrproductName=[CountMedialFiles valueForKey:@"Product_Name"];
            arrValue=[CountMedialFiles valueForKey:@"ProductCount"];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                _values = (NSArray *)arrValue;
                _barColors						= @[[UIColor colorWithRed:(33.0/255.0) green:(186.0/255.0) blue:(164.0/255.0) alpha:1]];
                _currentBarColor				= 0;
                
                _chart							= [[SimpleBarChart alloc] initWithFrame:CGRectMake(0,40, _viewDemonstratedProducts.frame.size.width, _viewDemonstratedProducts.frame.size.height-80)];
                _chart.center					= CGPointMake((_viewDemonstratedProducts.frame.size.width / 2.0), (_viewDemonstratedProducts.frame.size.height / 2.0)+15);
                _chart.delegate					= self;
                _chart.dataSource				= self;
                _chart.barShadowOffset			= CGSizeMake(2.0, 1.0);
                _chart.animationDuration		= 1.0;
                _chart.barShadowColor			= [UIColor grayColor];
                _chart.barShadowAlpha			= 0.5;
                _chart.barShadowRadius			= 1.0;
                _chart.barWidth					= 35.0;
                _chart.xLabelType				= SimpleBarChartXLabelTypeHorizontal;
                _chart.gridColor				= [UIColor whiteColor];
                [_viewDemonstratedProducts addSubview:_chart];
                [_chart reloadData];
                
            });
        }
    });
}

#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return _values.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] floatValue];
}
- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    return [arrproductName objectAtIndex:index];
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
    return [_barColors objectAtIndex:_currentBarColor];
}

#pragma mark Face Time Vs. Wait Time

-(void)loadViewForFaceTimeVsWaitTime
{
    if (_viewFaceTimeVsWaitTime.layer.sublayers.count>0) {
        
        [_viewFaceTimeVsWaitTime.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    }

    NSArray *arrFaceTime = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"Select round (sum ((JulianDay(End_Time) - JulianDay(Start_Time)) * 24 * 60)) As faceTime FROM PHX_TBL_Walkin_Customers where Start_Time like '%%%@%%'",monthString]];
    
    NSArray *arrWaitTime = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"Select round (sum ((JulianDay(B.Start_Time) - JulianDay(A.Visit_Start_Date)) * 24 * 60)) As waitTime FROM PHX_TBL_Actual_Visits As A inner join PHX_TBL_Walkin_Customers as B where A.Actual_Visit_ID = B.Actual_Visit_ID and Visit_Start_Date like '%%%@%%'",monthString]];
    
    int faceTime = 0;
    int waitTime = 0;
    if (arrFaceTime >0)
    {
        faceTime = [[[arrFaceTime objectAtIndex:0]stringForKey:@"faceTime"]intValue];
    }
    if (arrWaitTime >0)
    {
        waitTime = [[[arrWaitTime objectAtIndex:0]stringForKey:@"waitTime"]intValue];
    }
    
    
    self.lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(10, 0, 460, 95)];
    self.lineChart.yLabelFormat = @"%1.1f";
    self.lineChart.backgroundColor = [UIColor clearColor];
    //[self.lineChart setXLabels:@[@"SEP 1",@"SEP 2"]];
    self.lineChart.showCoordinateAxis = YES;
    
    //Use yFixedValueMax and yFixedValueMin to Fix the Max and Min Y Value
    //Only if you needed
    self.lineChart.yFixedValueMax = 60.0;
    self.lineChart.yFixedValueMin = 0.0;
    
    [self.lineChart setYLabels:@[@"W.T.",@"F.T.",]];
    
    // Line Chart #1
    NSArray * data01Array = @[@50.0, @50.0];
    PNLineChartData *data01 = [PNLineChartData new];
    data01.dataTitle = @"Alpha";
    data01.color = KSalesWorxFaceTimeColor;
    data01.alpha = 1.0f;
    data01.itemCount = data01Array.count;
    data01.inflexionPointStyle = PNLineChartPointStyleTriangle;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    // Line Chart #2
    NSArray * data02Array = @[@0.0, @0.0];
    PNLineChartData *data02 = [PNLineChartData new];
    data02.dataTitle = @"Beta";
    data02.color = KSalesWorxWaitTimeColor;
    data02.alpha = 1.0f;
    data02.itemCount = data02Array.count;
    data02.inflexionPointStyle = PNLineChartPointStyleCircle;
    data02.getData = ^(NSUInteger index) {
        CGFloat yValue = [data02Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    self.lineChart.chartData = @[data01, data02];
    
    if (waitTime < 0) {
        waitTime = 0;
    }
    else if (faceTime < 0) {
        faceTime = 0;
    }
    
    if(faceTime>350 || waitTime>350)
        [self.lineChart strokeChart:faceTime*350/(faceTime+waitTime) :waitTime*350/(faceTime+waitTime)];
    else
        [self.lineChart strokeChart:faceTime :waitTime];
    
    self.lineChart.delegate = self;
    [_viewFaceTimeVsWaitTime addSubview:self.lineChart];
    
    self.lineChart.legendStyle = PNLegendItemStyleStacked;
    self.lineChart.legendFont = kSWX_FONT_SEMI_BOLD(12);
    
    NSString *FT = [NSString stringWithFormat:@"%d Min",faceTime];
    NSString *WT = [NSString stringWithFormat:@"%d Min",waitTime];
    
    UIView *legend = [self.lineChart getLegendWithMaxWidth:100 :FT :WT];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language containsString:@"ar"])
    {
        [legend setFrame:CGRectMake(330, 5, 70, legend.frame.size.height)];
    }
    else
    {
        [legend setFrame:CGRectMake(380, 5, 70, legend.frame.size.height)];
    }
    [_viewFaceTimeVsWaitTime addSubview:legend];
}

- (void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex pointIndex:(NSInteger)pointIndex{
    NSLog(@"Click Key on line %f, %f line index is %d and point index is %d",point.x, point.y,(int)lineIndex, (int)pointIndex);
}

- (void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
    NSLog(@"Click on line %f, %f, line index is %d",point.x, point.y, (int)lineIndex);
}

#pragma mark Productive Visits

-(void)loadViewForProductiveVisits
{
    if (_viewProductiveVisits.layer.sublayers.count>0) {
        
        [_viewProductiveVisits.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    }
    
    NSArray *arrTotalVisits = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select count(*) from PHX_TBL_Actual_Visits where Visit_Start_Date like '%%%@%%'",monthString]];

    
    NSArray *arrProductiveVisits = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select count(distinct Planned_Visit_ID) As count from PHX_TBL_Actual_Visits  where Visit_Start_Date like '%%%@%%' and Call_Started_At is not null",monthString]];
    
    
    int totalVisits = 0;
    int productiveVisits = 0;
    if (arrTotalVisits >0)
    {
        totalVisits = [[[arrTotalVisits objectAtIndex:0]stringForKey:@"count(*)"]intValue];
    }
    
    if (arrProductiveVisits >0)
    {
        productiveVisits = [[[arrProductiveVisits objectAtIndex:0]stringForKey:@"count"]intValue];
    }
    
    NSArray *arrProductDemonstrated = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select count(*) from PHX_TBL_EDetailing where Discussed_At like '%%%@%%'",monthString]];
    NSArray *arrSamplesGiven = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select sum(A.Issue_Qty) As sum from PHX_TBL_Issue_Note_Line_Items AS A left join PHX_TBL_Issue_Note As B  where A.Doc_No = B.Doc_No and B.Start_Time like '%%%@%%'",monthString]];
    
    
    int productDemonstrated = 0;
    int samplesGiven = 0;
    if (arrProductDemonstrated >0)
    {
        productDemonstrated = [[[arrProductDemonstrated objectAtIndex:0]stringForKey:@"count(*)"]intValue];
    }
    
    if (arrSamplesGiven >0)
    {
        samplesGiven = [[[arrSamplesGiven objectAtIndex:0]stringForKey:@"sum"]intValue];
    }
    
    _lblProductsDemonstrated.text = [NSString stringWithFormat:@"%d",productDemonstrated];
    _lblSamplesGiven.text = [NSString stringWithFormat:@"%d",samplesGiven];
    
    [self loadPercentageChart:circleChartForProductiveVisits :_viewProductiveVisits :[NSNumber numberWithInt:totalVisits] :[NSNumber numberWithInt:productiveVisits] :[UIColor colorWithRed:(88.0/255.0) green:(116.0/255.0) blue:(152.0/255.0) alpha:1]];
}

#pragma mark Load Vist Frequency Graph

-(void)loadVisitFrequency
{
    [self.gkLineGraph reset];
    
    arrVisitFrequency = [[NSMutableArray alloc]init];
    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        
        NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
        NSString *output = @"0.00";
        NSString *newOutput = [NSString stringWithFormat:@"%@", output];
        [dummyDict setObject:newOutput forKey:@"Ammount"];
        [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
        
        NSArray *temp = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) AS Total FROM PHX_TBL_Planned_Visits where Visit_Date Like '%%%@%%' AND Visit_Status = 'Y'", currentString]];
        [dummyDict setObject:[[temp objectAtIndex:0]valueForKey:@"Total"] forKey:@"Total"];
        
        if ([[[temp objectAtIndex:0]valueForKey:@"Total"]intValue] >0) {
            [arrVisitFrequency addObject:dummyDict];
        }
    }
    
    if (arrVisitFrequency.count==0) {
        isEmptyGraph=YES;
        
        [self displayEmptyDataGraph];
    }
    else{
        
        //adding missing date data
        
        NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
        
        for (NSInteger i=0; i<tempDates.count; i++) {
            NSString* currentString=[tempDates objectAtIndex:i];
            if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
                
            }
            else{
                NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
                NSString *output = @"0.00";
                NSString *newOutput = [NSString stringWithFormat:@"%@", output];
                [dummyDict setObject:newOutput forKey:@"Ammount"];
                [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
                [dummyDict setObject:@0 forKey:@"Total"];
                [arrVisitFrequency insertObject:dummyDict atIndex:i];
            }
        }
        
        NSLog(@"sales data %@" ,arrVisitFrequency);
        
        isEmptyGraph=NO;
        
        [self testDataGraph];
    }
}

-(void)testDataGraph
{
    if (arrVisitFrequency.count>0)
    {
        NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
        currentMonthFormatter.dateFormat=@"MMMM";
        NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
        NSLog(@"current month is %@", currentMonth);
        monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];
        
        NSMutableArray * refinedSalesArray=[[NSMutableArray alloc]init];
        
        if (arrVisitFrequency.count>0) {
            //this is for first of every month
            
            if (arrVisitFrequency.count==1) {
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:0]];
            }
            for (NSString * amountStr in [arrVisitFrequency valueForKey:@"Total"]) {
                
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
            }
        }
        
        NSMutableArray * testArray=[[NSMutableArray alloc]init];
        [testArray addObject:refinedSalesArray];
        
        NSLog(@" test array %@",testArray);
        self.data =testArray;
        
        
        NSLog(@"Vist frequency data array is %@",arrVisitFrequency);
        
        
        NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
        
        NSLog(@"labels array is %@",labalesArray);
        
        self.labels = labalesArray;
        
        NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));
        
        self.gkLineGraph.dataSource = self;
        self.gkLineGraph.lineWidth = 3.0;
        self.gkLineGraph.startFromZero=YES;
        self.gkLineGraph.valueLabelCount = 5;
        
        testArray = [[testArray objectAtIndex:0] valueForKeyPath:@"@distinctUnionOfObjects.self"];
        self.gkLineGraph.valueLabelCount = [testArray count];
        
        self.gkLineGraph.isMedRepDashboard = YES;
        
        [_gkLineGraph draw];
    }
}

-(void)displayEmptyDataGraph
{
    NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
    currentMonthFormatter.dateFormat=@"MMMM";
    NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
    NSLog(@"current month is %@", currentMonth);
    monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];
    
    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    
    NSMutableArray * testSalesDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * testReturnsDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * emptyGraphDataArray=[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testSalesDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testReturnsDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    NSMutableArray * testArray=[[NSMutableArray alloc]init];
    NSMutableArray * tempSalesDataArray=[[NSMutableArray alloc]init];
    NSMutableArray * tempReturnsDataArray=[[NSMutableArray alloc]init];
    
    
    for (NSString * amountStr in [testSalesDataArray valueForKey:@"Ammount"]) {
        [tempSalesDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        [emptyGraphDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        
    }
    for (NSString * amountStr in [testReturnsDataArray valueForKey:@"Ammount"]) {
        
        [tempReturnsDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        
    }
    
    [testArray addObject:tempSalesDataArray];
    [testArray addObject:tempReturnsDataArray];
    
    NSLog(@"test array for dummy graph %@ \n%@ \n%@",tempSalesDataArray,tempSalesDataArray,emptyGraphDataArray);
    
    
    [emptyGraphDataArray replaceObjectAtIndex:0 withObject:[NSDecimalNumber numberWithInteger:[@"100" integerValue]]];
    [testArray addObject:emptyGraphDataArray];
    
    
    self.data =testArray;
    
    
    NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
    self.labels = labalesArray;
    
    
    NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));
    
    self.gkLineGraph.dataSource = self;
    self.gkLineGraph.lineWidth = 3.0;
    
    self.gkLineGraph.valueLabelCount = 5;
    NSLog(@"trying to draw graph %@", self.gkLineGraph.layer.sublayers);
    [self.gkLineGraph draw];
}

#pragma mark - GKLineGraphDataSource

- (NSInteger)numberOfLines {
    return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index {
    
    if (isEmptyGraph==YES) {
        
        id colors = @[[UIColor clearColor],
                      [UIColor colorWithRed:(75.0/255.0) green:(143.0/255.0) blue:(249.0/255.0) alpha:1],
                      [UIColor clearColor],
                      ];
        return [colors objectAtIndex:index];
        
    }
    else{
        id colors = @[[UIColor colorWithRed:(13.0/255.0) green:(133.0/255.0) blue:(248.0/255.0) alpha:1],
                      [UIColor clearColor],
                      ];
        return [colors objectAtIndex:index];
        
    }
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index {
    
    if (isEmptyGraph==YES) {
        return [[@[@1, @1.6,@1.6] objectAtIndex:index] doubleValue];
        
    }
    else{
        return [[@[@1, @1.6] objectAtIndex:index] doubleValue];
        
    }
    
}

- (NSString *)titleForLineAtIndex:(NSInteger)index {

    @try {
        return [self.labels objectAtIndex:index];
        
    } @catch (NSException *exception) {
        return @"";
        
    } @finally {
        
    }
}

-(NSMutableArray*)fetchDatesofCurrentMonthinDBFormat
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    [fmt setDateFormat:@"yyyy-MM-dd"]; // you can set this to whatever you like
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
        [dates sortUsingSelector:@selector(compare:)];
        
    }
    return dates;
}

-(NSMutableArray*)fetchDatesofCurrentMonth
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"dd"]; // you can set this to whatever you like
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
        // [dates insertObject:@"0" atIndex:0];
        
       // NSLog(@"dates are %@",dates);
        
        [dates sortUsingSelector:@selector(compare:)];
    }
    return dates;
}

#pragma mark IBAction Visit
- (IBAction)btnVisit:(id)sender {
    
    SalesWorxBrandAmbassadorVisit * currentvisit = [[SalesWorxBrandAmbassadorVisit alloc]init];
    
    currentvisit.selectedLocation = [[SalesWorxBrandAmbassadorLocation alloc]init];
    currentvisit.selectedDemoPlan = [[DemoPlan alloc]init];
    currentvisit.selectedLocation.Location_ID   = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Location_ID"]];
    currentvisit.selectedLocation.Location_Name = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Location_Name"]];
    currentvisit.selectedLocation.Latitude      = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Latitude"]];
    currentvisit.selectedLocation.Longitude     = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Longitude"]];
    currentvisit.selectedLocation.City          = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"City"]];
    currentvisit.selectedLocation.Phone         = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Phone"]];
    currentvisit.selectedLocation.Address       = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Address"]];
    currentvisit.selectedDemoPlan.Demo_Plan_ID  = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Demo_Plan_ID"]];
    currentvisit.selectedDemoPlan.Description   = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Description"]];
    currentvisit.Date                           = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Visit_Date"]];
    currentvisit.Objective                      = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Objective"]];
    currentvisit.Planned_Visit_ID               = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Planned_Visit_ID"]];
    currentvisit.Emp_ID                         = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Emp_ID"]];
    currentvisit.Visit_Status                   = [SWDefaults getValidStringValue:[[sortedPlannedVisitArray objectAtIndex:0] valueForKey:@"Visit_Status"]];
   
    
    SalesWorxBrandAmbassadorsViewController *visitVC = [[SalesWorxBrandAmbassadorsViewController alloc]init];
    visitVC.selectedVisit = currentvisit;
    visitVC.isFromDashboard = YES;
    [self.navigationController pushViewController:visitVC animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
