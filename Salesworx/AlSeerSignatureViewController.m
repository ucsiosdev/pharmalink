//
//  AlSeerSignatureViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/18/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerSignatureViewController.h"
#import "SignatureViewController.h"
#import "OrderAdditionalInfoViewController.h"
#import "MJPopupBackgroundView.h"
#import "UIViewController+MJPopupViewController.h"
#define kImagesFolder @"Signature"

@interface AlSeerSignatureViewController ()

@end

@implementation AlSeerSignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(0, 40, 600, 120)];
    
    [self.view addSubview:pjrSignView];
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)closeButtonTapped:(id)sender {
    
    
    //    self.glkView=nil;
    //
    //    [self.glkView erase];
    //
    //    [self.view removeFromSuperview];
    
    
    // [self.navigationController popViewControllerAnimated:YES];
    
    //    OrderAdditionalInfoViewController * orderInfoVC=[[OrderAdditionalInfoViewController alloc]init];
    //
    //    [orderInfoVC closeButtonTapped];
    
    // [(id)self.view.superview closeButtonTapped];
    
    //    NSLog(@"check navigation stack %@", [self.navigationController.viewControllers description]);
    //
    //[self dismissViewControllerAnimated:YES completion:nil];
    //
    
    //[self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    //
    //    self.delegate=nil;
    
    
    
}




- (NSString*) dbGetImagesDocumentPath
{
    
    //     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}


- (IBAction)saveButtonTapped:(id)sender {
    
    
    NSLog(@"save button tapped in signature");
    
    //if signature already captured display the signature
    
    
    //pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(0, 55, 827, 211)];
    
    
    
    if (pjrSignView.lblSignature.hidden==NO) {
        
        UIAlertView* signatureAlert=[[UIAlertView alloc]initWithTitle:@"Signature Missing" message:@"Please enter the signature" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        signatureAlert.tag=907;
        [signatureAlert show];
    }
    
    
    else
    {
    
    
    UIImage* signImage=[pjrSignView getSignatureImage];
    NSData *imageData = UIImagePNGRepresentation(signImage);
   
    
    
//    UIImageView* testImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 500, 200)];
//    [testImgView setImage:signImage];
//    
//    [self.view addSubview:testImgView];
    
    
    
    [SWDefaults setSignature:imageData];
    
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSObject * object = [prefs objectForKey:@"signature"];
    if(object != nil){
        //object is there
        
        UIAlertView* signatureAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Signature saved successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        signatureAlert.tag=906;
        [signatureAlert show];
        
    }
    
    
   // [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    
    }
    
}

- (IBAction)eraseButtonTapped:(id)sender {
    
    
    [pjrSignView.lblSignature setHidden:NO];
    
    if ([pjrSignView.previousSignature superview] || [pjrSignView.previousSignature superview]){
        //[pjrSignView.lblSignature removeFromSuperview];
        
        [SWDefaults clearSignature];
        [pjrSignView.previousSignature removeFromSuperview];
        
    }
    
    signImageView.image=nil;
    
    [signImageView removeFromSuperview];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signature"];
    
    [SWDefaults clearSignature];
    
    
    [pjrSignView clearSignature];
}
@end
