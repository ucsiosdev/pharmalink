//
//  SWCustomerPriceListTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementTitleDescriptionLabel.h"
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"
@interface SWCustomerPriceListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *dividerImageView;

@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * descriptionLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * isGenricLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * itemCodeLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * unitListPriceLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * unitSellingPriceLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * uomLbl;

@property(nonatomic) BOOL isHeader;

@end
