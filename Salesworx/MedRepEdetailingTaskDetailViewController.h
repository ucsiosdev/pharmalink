//
//  MedRepEdetailingTaskDetailViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "MedRepEdetailTaskStatusViewController.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "MedRepEdetailingTaskInformationViewController.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "MedRepCustomClass.h"


@protocol VisitTaskDelegate <NSObject>

-(void)updatedTasks:(NSMutableArray*)updatedTasksArray;

@end


@interface MedRepEdetailingTaskDetailViewController : SWViewController<TaskStatusDelete,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,UIPickerViewDelegate,SelectedFilterDelegate,VisitDatePickerDelegate,TaskInformationDelegate,UITextFieldDelegate>
{
    //This is the index of the cell which will be expanded
    NSInteger selectedIndex;
    IBOutlet UITextField *taskTitleTxtFld;
    
    IBOutlet MedRepTextField *doctorNameTitleLbl;
    IBOutlet MedRepTextField *doctorTextField;
    NSMutableArray* pickerDataArray;
    
    NSString* selectedDate;
    
    VisitTask *currentTask;
    
    NSInteger selectedRow;
    
    BOOL hideDatePicker;
    
    NSString *selectedStatus, *taskTitle,*taskDesc;
    
    NSMutableArray* statusArray,*categoryArray,*priorityArray;
    
    NSString* statusString,*categoryString,*priorityString;
    
    NSInteger selectedDelegateIndex;
    
    BOOL isTaskforCurrentDoctor;
    
    
    
    NSMutableDictionary* taskContentDictionary;
    
    
    NSString* buttonTitleString;
    
    IBOutlet MedRepTextField *titleTxtFld;
    
    IBOutlet MedRepTextField *dueDateTxtFld;

    IBOutlet MedRepTextView *InformationTxtFld;
    
    IBOutlet MedRepTextField *statusTxtFld;
    IBOutlet MedRepTextField *categoryTxtFld;
    IBOutlet MedRepTextField *priorityTxtFld;


    IBOutlet UIButton *dueDateButton;
    
    
    
    IBOutlet UIButton *informationButton;
    
    
    IBOutlet UIButton *statusButton;
    
    
    IBOutlet UIButton *categoryButton;
    
    
    VisitTask * visitTask;
    
    IBOutlet UIButton *priorityButton;
    
    
    id <VisitTaskDelegate>updatesTasksDelegate;

    

}
- (IBAction)dueDateTapped:(id)sender;
- (IBAction)priorityTapped:(id)sender;


- (IBAction)informationTapped:(id)sender;

- (IBAction)statusTapped:(id)sender;

- (IBAction)categoryTapped:(id)sender;
@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;

@property(nonatomic,strong) id updatesTasksDelegate;

@property(strong,nonatomic) NSMutableArray* taskObjectsArray;

@property(nonatomic) BOOL isVisitCompleted;

@property(nonatomic) BOOL isViewing;

@property(nonatomic) BOOL isUpdatingTask;

@property(nonatomic) BOOL isTaskClosed;

@property(nonatomic) BOOL isInMultiCalls;

@property (strong, nonatomic) IBOutlet UITableView *taskDetailTblView;

@property(strong,nonatomic) NSMutableDictionary* savedTaskDictionary;

@property(nonatomic) BOOL dashBoardViewing;

@property(strong,nonatomic) NSIndexPath * selectedTaskIndexPath;

@property(nonatomic,strong)NSMutableArray* tasksArray;

@property(strong,nonatomic) VisitDetails * currentVisit;


@property(strong,nonatomic) VisitTask * selectedTask;

@end
