//
//  SalesWorxReturnMatchedInvoicesTableViewCell.h
//  MedRep
//
//  Created by Neha Gupta on 05/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxReturnMatchedInvoicesTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *invoiceDateLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *invoiceNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *quantityLbl;

@property(nonatomic) BOOL isHeader;

@end

