//
//  SalesWorxReturnsViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/15/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxReturnLineItemsTableViewCell.h"
#import "SWDefaults.h"
#import "SalesWorxTableView.h"
#import "SalesWorxSingleLineLabel.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SWDatabaseManager.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "MedRepButton.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxProductsFilterViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesOrderDescriptionLabel.h"
#import "SalesWorxTitleLabel1.h"
#import "SalesOrderTitleLabel2.h"
#import "SalesOrderDescriptionLabel2.h"
#import <MBProgressHUD.h>
#import "SalesWorxSalesOrderProductsTableViewBrandcodeCell.h"
#import "MedRepUpdatedDesignCell.h"
#import "SalesOrderStockInfoViewController.h"
#import "SalesOrderBonusInfoViewController.h"
#import "SalesOrderSalesHistoryInfoViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "SalesWorxReturnConfirmationViewController.h"
#import "SalesWorxImageView.h"
#import "SalesWorxProductNameSearchPopOverViewController.h"
#import "SalesWorxDescriptionLabel1.h"
#import "MedRepElementTitleDescriptionLabel.h"
#import "SalesWorxDefaultButton.h"
#import "ZBarSDK.h"
#import "SalesWorxBarCodeScannerManager.h"
#import "SalesWorxReturnMatchedInvoicesViewController.h"
#import "SalesWorxSalesOrderProductLongDescriptionViewController.h"

@interface SalesWorxReturnsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SalesWorxProductsFilterViewControllerDelegate,SalesWorxPopoverControllerDelegate,MGSwipeTableCellDelegate,SalesWorxDatePickerDelegate,UIPopoverPresentationControllerDelegate,SalesWorxProductNameSearchPopOverViewControllerDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate>
{
    IBOutlet SalesWorxTableView *ReturnsTableView;
    IBOutlet SalesWorxTableViewHeaderView *ReturnsTableHeaderView;

    IBOutlet UIView *NoSelectionView;
    
    IBOutlet UITableView *productsTableView;
    IBOutlet SalesWorxSingleLineLabel *NoSelectionMessageLabel;
    IBOutlet UIImageView *NoSelectionImageView;
    BOOL isSearching;
    NSMutableArray* filteredProductsArray;
    NSMutableArray*productsArray;
    NSMutableArray* filteredProductsArrayGroupedByBrandCode;
    NSMutableArray* productsArrayGroupedByBrandCode;
    
    NSIndexPath *selectedProductIndexpath;
    
    IBOutlet MedRepHeader *customerNameLabel;
    IBOutlet MedRepProductCodeLabel *customerCodeLabel;
    IBOutlet UILabel *customerAvailableBalanceLabel;
    IBOutlet SalesWorxImageView *customerstatusImageView;
    IBOutlet UILabel *productNameLabel;
    IBOutlet MedRepElementTitleLabel *productCodeLabel;
    
    /* product Details View UI Elements*/
    IBOutlet SalesWorxDropShadowView *productDetailsView;
    IBOutlet SalesOrderDescriptionLabel *productRetailPriceLabel;
    IBOutlet SalesOrderDescriptionLabel *productWholeSalePriceLabel;
    IBOutlet SalesOrderDescriptionLabel *productDefaultDiscountLabel;
    IBOutlet SalesOrderDescriptionLabel *productSpecialDiscountLabel;
    IBOutlet SalesOrderDescriptionLabel *productBonusItemLabel;
    IBOutlet SalesOrderDescriptionLabel *productDefaultBonusLabel;
    IBOutlet SalesWorxDefaultButton *productDescriptionInfoButton;
    IBOutlet SalesWorxDefaultButton *productStockInfoButton;
    IBOutlet SalesWorxDefaultButton *productBonusInfoButton;
    IBOutlet SalesWorxDefaultButton *productSalesHistoryInfoButton;
    
    IBOutlet SalesWorxTitleLabel1 *specialDiscountStringLabel;
    IBOutlet SalesWorxTitleLabel1 *bonusItemStringLabel;
    IBOutlet SalesWorxTitleLabel1 *defaultBonusItemStringLabel;
    NSString *enableProductRank;
    NSString *showProductLongDescription;
    NSString *popOverTitle;
    /* product Order Details View UI Elements*/
    IBOutlet SalesWorxDropShadowView *returnProductDetailsView;
    IBOutlet MedRepTextField *returnQtyTextField;
    IBOutlet MedRepTextField *UOMTextField;
    IBOutlet MedRepTextField *recievingBonusTextField;
    IBOutlet MedRepTextField *ReasonForReturnTextfield;
    IBOutlet MedRepTextField *ExpiryDateTextField;
    IBOutlet MedRepTextField *lotNumberTextField;
    IBOutlet MedRepTextField *invoiceTextField;
    IBOutlet SalesWorxTitleLabel2 *invoiceTitleLabel;
    

    IBOutlet SalesOrderDescriptionLabel2 *OrderItemTotalAmountLabel;
    IBOutlet UIButton *addItemButton;
    
    
    /**Product TableView UI elements*/
    IBOutlet UIButton *productsFilterButton;
    IBOutlet UISearchBar *productsSearchBar;
    
    NSMutableArray *productsBrandCodesArray;
    NSMutableArray *ProductTableViewExpandedSectionsArray;
    
    Products * selectedProduct;
    ReturnsLineItem *selectedReturnLineItem;
    
    
    /*Layout Constarints*/
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *NoSelectionTopMarginConstraint;
    IBOutlet NSLayoutConstraint *productDetailsViewHeightConstraiint;
    IBOutlet NSLayoutConstraint *productDetailsViewTopConstraiint;
    IBOutlet NSLayoutConstraint *returnProductDetailsViewHeightConstraiint;
    IBOutlet NSLayoutConstraint *returnProductDetailsViewTopConstraiint;
    
    IBOutlet NSLayoutConstraint *productSalesHistoryInfoButtonHeightContraint;
    IBOutlet NSLayoutConstraint *productSalesHistoryInfoButtonWidthContraint;
    IBOutlet NSLayoutConstraint *productSalesHistoryInfoButtontarilingMarginContraint;

    
    UIBarButtonItem *saveBarButtonItem;
    UIBarButtonItem *closeVisitButton;
    
    NSMutableDictionary* previousFilteredParameters;
    UIImageView *blurredBgImage;
    NSMutableArray* returnLineItemsArray;
    
    UIPopoverController * ReasonForReturnPopOverController;
    
    AppControl *appControl;
    NSIndexPath *selectedReturnItemIndexpath;
    BOOL isUpdatingReturn;
    IBOutlet MedRepElementTitleDescriptionLabel*returnsTotalAmountLbl;
    BOOL isKeyBoardShowing;
    
    
    
    NSMutableArray *returnTypes;
    IBOutlet UIPopoverController *returnPagePopOverController;
    BOOL showReturnsTableViewNewItemAnimationColor;
    BOOL allowSearchBarEditing;
    UIActivityIndicatorView *searchBarActivitySpinner;
    
    BOOL isSearchBarClearButtonTapped; /* do not use this BOOL other than for product search popover */
    UIPopoverController * FOCPopOverController;

    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    IBOutlet SalesWorxTitleLabel2 *bonusStringLabel;
    
    ZBarReaderViewController *reader;
    ProductSearchType proSearchType;
    IBOutlet NSLayoutConstraint *xSearchBarTrailingspaceToFilterButton;
    IBOutlet SalesWorxDefaultButton *barcodeScanButton;
    
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelLeadingMarginConstraint;
    IBOutlet MedRepElementTitleLabel * ReturnsTableHeaderTotalLabel;

    NSMutableArray *matchedInvoicesArray;
}
- (void)BarCodeScannerButtonBackPressed:(id)sender;

-(IBAction)productDescriptionInfoButtonTapped:(id)sender;
-(IBAction)productStockInfoButtonTapped:(id)sender;
-(IBAction)productBonusInfoButtonTapped:(id)sender;
-(IBAction)productSalesHistoryInfoButtonTapped:(id)sender;
-(IBAction)addToReturnsButtonTapped:(id)sender;
-(IBAction)clearButtonTapped:(id)sender;
- (IBAction)ReturnsTextFieldEditingChanged:(id)sender;
-(IBAction)barCodeScanButtonTapped:(id)sender;



@property (strong,nonatomic) NSString *parentViewString;
@property (strong,nonatomic) NSString *orderTypeTitle;
@property(strong,nonatomic) SalesWorxVisit * currentVisit;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;
-(id)fetchProductPriceListData:(Products*)Product;
@end
