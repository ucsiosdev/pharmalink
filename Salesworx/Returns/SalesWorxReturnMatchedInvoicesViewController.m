//
//  SalesWorxReturnMatchedInvoicesViewController.m
//  MedRep
//
//  Created by Neha Gupta on 05/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReturnMatchedInvoicesViewController.h"

@interface SalesWorxReturnMatchedInvoicesViewController ()

@end

@implementation SalesWorxReturnMatchedInvoicesViewController
@synthesize matchedInvoicesArray, previousSelectedInvoice, invoiceDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    invoicesTableView.estimatedRowHeight=44.0f;
    invoicesTableView.rowHeight=UITableViewAutomaticDimension;
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

-(void)viewWillAppear:(BOOL)animated{
    
    if(self.isMovingToParentViewController)
    {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
    }
    [invoicesTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeView
{
    //do what you need to do when animation ends...
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)closeTapped
{
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(closeView) userInfo:nil repeats:NO];
}

- (IBAction)closeButtonTapped:(id)sender {

    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(callReasonViewDelegate) userInfo:nil repeats:NO];
}
- (void)callReasonViewDelegate
{
    //do what you need to do when animation ends...
    [self dismissViewControllerAnimated:NO completion:nil];
    if (!previousSelectedInvoice || previousSelectedInvoice.invoiceNumber.length == 0) {
        if ([self.invoiceDelegate respondsToSelector:@selector(closeInvoicePopOver:)]) {
            [self.invoiceDelegate closeInvoicePopOver:matchedInvoicesArray];
        }
    }
}

#pragma mark UITableView Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SalesWorxReturnMatchedInvoicesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"invoiceCell"];
    if (cell==nil) {
        NSArray * bundleArray=[[NSBundle mainBundle] loadNibNamed:@"SalesWorxReturnMatchedInvoicesTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    
    cell.invoiceDateLbl.isHeader=YES;
    cell.invoiceNumberLbl.isHeader=YES;
    cell.quantityLbl.isHeader=YES;
    
    cell.invoiceDateLbl.text=@"Invoice Date";
    cell.invoiceNumberLbl.text=@"Invoice Number";
    cell.quantityLbl.text=@"Quantity";

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return matchedInvoicesArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SalesWorxReturnMatchedInvoicesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"invoiceCell"];
    if (cell==nil) {
        NSArray * bundleArray=[[NSBundle mainBundle] loadNibNamed:@"SalesWorxReturnMatchedInvoicesTableViewCell" owner:self options:nil];
        cell=[bundleArray objectAtIndex:0];
    }
    
    SalesWorxReturnsMatchedInVoice *currentInvoice = [matchedInvoicesArray objectAtIndex:indexPath.row];

    if (currentInvoice==previousSelectedInvoice) {
        cell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
    }
    else{
        cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
    }

    cell.invoiceDateLbl.text=[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:currentInvoice.invoiceDate];
    cell.invoiceNumberLbl.text=currentInvoice.invoiceNumber;
    cell.quantityLbl.text=currentInvoice.quantity;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SalesWorxReturnsMatchedInVoice *selectedInVoice = [matchedInvoicesArray objectAtIndex:indexPath.row];
    if ([self.invoiceDelegate respondsToSelector:@selector(didSelectInvoice:andAllMatchedInvoices:)]) {
        [self.invoiceDelegate didSelectInvoice:selectedInVoice andAllMatchedInvoices:matchedInvoicesArray];
    }
    [self closeTapped];
}

- (IBAction)downloadButtonTapped:(id)sender {
    [self sendRequestToFetchInvoices:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecGetInvoiceInfo"];
}

- (void)sendRequestToFetchInvoices:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    NSString *strParams =[[NSString alloc] init];
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRepID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"CustomerID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[self.customerID intValue]];
    
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"ShipSiteID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[self.shipSiteID intValue]];
    
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"InventoryItemID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[self.selectedProduct.Inventory_Item_ID intValue]];
    
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"MaxInvoiceDate"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
    

    NSString *strDeviceID = [[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion = [[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString = [NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Return_GetInvoice",[testServerDict stringForKey:@"name"],procName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"fetch invoice Response %@",resultArray);
         
         matchedInvoicesArray = [[NSMutableArray alloc]init];
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             for (int i = 0; i < resultArray.count; i++) {
                 SalesWorxReturnsMatchedInVoice *currentInvoice = [[SalesWorxReturnsMatchedInVoice alloc]init];
                 NSMutableArray *objDict = [resultArray objectAtIndex:i];
                 currentInvoice.invoiceDate = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd'T'HH:mm:ss" destFormat:kDatabseDefaultDateFormat scrString:[NSString getValidStringValue:[objDict valueForKey:@"Invoice_Date"]]];
                 currentInvoice.invoiceNumber = [NSString getValidStringValue:[objDict valueForKey:@"Invoice_No"]];
                 currentInvoice.quantity = [NSString getValidStringValue:[objDict valueForKey:@"Invoice_Qty"]];
                 
                 [matchedInvoicesArray addObject:currentInvoice];
             }
         }
         
         dispatch_async(dispatch_get_main_queue(), ^(void){
             //Run UI Updates
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [invoicesTableView reloadData];
             if (matchedInvoicesArray.count == 0) {
                 [SWDefaults showAlertAfterHidingKeyBoard:@"" andMessage:@"No invoice found" withController:self];
             }
         });
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"sync error in sync_MC_ExecGetInvoiceInfo %@",error);
     }];
    
    //call start on your request operation
    [operation start];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
