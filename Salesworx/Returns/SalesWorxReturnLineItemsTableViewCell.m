//
//  SalesWorxReturnLineItemsTableViewCell.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReturnLineItemsTableViewCell.h"
#import "AppControl.h"
#import "SWDefaults.h"
@implementation SalesWorxReturnLineItemsTableViewCell
@synthesize xVATChargeLabelWidthConstraint,xVATChargeLabelLeadingMarginConstraint,xNetAmountLabelWidthConstraint,xNetAmountLabelLeadingMarginConstraint;
- (void)awakeFromNib {
    [super awakeFromNib];
    
    AppControl * appcontrol=[AppControl retrieveSingleton];
    if(![appcontrol.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
        xVATChargeLabelWidthConstraint.constant = 0;
        xVATChargeLabelLeadingMarginConstraint.constant=0;
        xNetAmountLabelWidthConstraint.constant=0;
        xNetAmountLabelLeadingMarginConstraint.constant=0;
        self.ToatlLbl.textAlignment = NSTextAlignmentRight;
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
