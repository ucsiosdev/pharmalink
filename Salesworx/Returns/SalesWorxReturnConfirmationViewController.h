//
//  SalesWorxReturnConfirmationViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJRSignatureView.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "CurrencyViewController.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SalesWorxTableViewHeaderView.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxSalesOrdersTableCellTableViewCell.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "SWDatabaseManager.h"
#import "FMDatabase.h"
#import <FMDBHelper.h>
#import "MedRepQueries.h"
#import "SalesWorxVisitOptionsViewController.h"
#import "AppControl.h"
#import "SalesWorxTitleLabel2.h"
#import "SalesWorxDescriptionLabel4.h"
#import "SalesWorxImageAnnimateButton.h"
#import "SalesWorxImageView.h"
#import "SalesWorxDescriptionLabel1.h"
#import "SalesWorxDefaultButton.h"
#import "SalesWorxDefaultSegmentedControl.h"
#import "SalesWorxSignatureView.h"

@interface SalesWorxReturnConfirmationViewController : UIViewController<SalesWorxDatePickerDelegate,SalesWorxPopoverControllerDelegate,SalesOrderTemplateNameViewControllerDelegate,SalesWorxSignatureViewDelegate, UIPopoverControllerDelegate>
{
    IBOutlet UIView *signatureViewContinerView;
    IBOutlet MedRepHeader *customerNameLabel;
    IBOutlet MedRepProductCodeLabel *customerCodeLabel;
    IBOutlet UILabel *customerAvailableBalanceLabel;
    IBOutlet SalesWorxImageView *customerstatusImageView;
    
    IBOutlet SalesWorxDescriptionLabel4 *ReturnsTotalAmountLabel;
    IBOutlet MedRepTextField *DocReferanceTexField;
    IBOutlet MedRepTextField *nameTextField;
    IBOutlet MedRepTextView *commentsTextView;
    
    IBOutlet SalesWorxTitleLabel2 *ReturnsTotalAmountStringLabel;
    
    IBOutlet UITableView *ReturnsTableView;
    IBOutlet SalesWorxTableViewHeaderView *ReturnsTableHeaderView;
    NSMutableArray *returnLineItemsArray;
    NSString *popOverTitle;
    UIPopoverController *confirmationPagePopOverController;
    ReturnsConfirmationDetails *confirmationDetails;
    
    IBOutlet SalesWorxDefaultSegmentedControl *returnTypeSegmentedControl;
    IBOutlet SalesWorxDefaultSegmentedControl *GoodsCollectedSegmentedControl;
    IBOutlet MedRepTextView *InvoicesNumbersTextView;

    BOOL isKeyBoardShowing;
    AppControl *appControl;
    
    IBOutlet UIView *returnsTypeView;
    IBOutlet UIView *GoodsCollectedView;
//    IBOutlet NSLayoutConstraint *skipConslidationViewHeightConstraint;
//    IBOutlet NSLayoutConstraint *wholeSaleOrderViewHeightConstraint;
//    IBOutlet NSLayoutConstraint *OrderAmountDetailsViewTopConstarint;
    
    UIBarButtonItem *confirmBarButtonItem;
//    BOOL isWholeSaleOrderViewHidden;
//    BOOL isSkipConsolidationViewHidden;
    NSString *shipDate;
    NSMutableArray *rmaReasonsCodesObjectsArray;
    IBOutlet SalesWorxDefaultButton *signatureClearButton;
    IBOutlet SalesWorxImageAnnimateButton *signatureSaveButton;
    IBOutlet SalesWorxSignatureView *customerSignatureTopContainerView;

    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    
    IBOutlet MedRepTextField *shipDateTextField;
    IBOutlet SalesWorxTitleLabel2 *lblShipDate;
    
    IBOutlet NSLayoutConstraint *DocRefTopLayout;
    IBOutlet NSLayoutConstraint *SigneeNameBottomLayout;
    
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelWidthConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderVATChargeLabelLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint * xOrderTableHeaderNetAmountLabelLeadingMarginConstraint;
    IBOutlet MedRepElementTitleLabel * ReturnsTableHeaderTotalLabel;
}
@property (strong,nonatomic) SalesWorxVisit *currentVisit;
@end
