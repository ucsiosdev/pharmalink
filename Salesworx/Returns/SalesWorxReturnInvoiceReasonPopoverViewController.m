//
//  SalesWorxReturnInvoiceReasonPopoverViewController.m
//  MedRep
//
//  Created by Neha Gupta on 05/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReturnInvoiceReasonPopoverViewController.h"

@interface SalesWorxReturnInvoiceReasonPopoverViewController ()

@end

@implementation SalesWorxReturnInvoiceReasonPopoverViewController
@synthesize invoiceReasonDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

-(void)viewWillAppear:(BOOL)animated{
    
    if(self.isMovingToParentViewController)
    {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
    }
}

- (IBAction)saveButtonTapped:(id)sender {
    
    [self.view endEditing:true];
    if(reasonTextView.text.length == 0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter reason" withController:self];
    }
    else
    {
        if ([self.invoiceReasonDelegate respondsToSelector:@selector(didSaveInvoiceReason:)]) {
            [self.invoiceReasonDelegate didSaveInvoiceReason:reasonTextView.text];
        }
        [self closeTapped];
    }
}

- (void)closeView
{
    //do what you need to do when animation ends...
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)closeTapped
{
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(closeView) userInfo:nil repeats:NO];
}

#pragma mark UITextViewMethods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextField:textView up:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextField:textView up:NO];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textView.text.length==0 && ([text isEqualToString:@" "] || [text isEqualToString:@"\n"]))
    {
        return NO;
    }

    return (newString.length <= KReturnsScreenInvoiceReasonMaximumDigitsLimit);
}

- (void) animateTextField: (UITextView*) textView up: (BOOL) up
{
    NSInteger movementDistance=150;
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
