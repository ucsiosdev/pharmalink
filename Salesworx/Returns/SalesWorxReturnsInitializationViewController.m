//
//  SalesWorxReturnsInitializationViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/15/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReturnsInitializationViewController.h"

@interface SalesWorxReturnsInitializationViewController ()

@end

@implementation SalesWorxReturnsInitializationViewController
@synthesize currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [categoriesCollectionView registerClass:[OrderInitializationCollectionViewCell class] forCellWithReuseIdentifier:@"orderCell"];
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesReturnsInitialisationScreenName];
    }


    [self showCustomerDetails];
    [self showNavigationBarTitle];
    
    
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
        
    
    proceedBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Proceed",nil) style:UIBarButtonItemStylePlain target:self action:@selector(proceedButtonTapped:)];
    proceedBarButtonItem.tintColor=[UIColor whiteColor];
    [proceedBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                      forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=proceedBarButtonItem;
    
    [proceedBarButtonItem setEnabled:NO];

}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    tempCategoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:currentVisit];
    [categoriesCollectionView reloadData];
    
    
    if ([[AppControl retrieveSingleton].ENABLE_MULTI_CAT_TRX isEqualToString:@"N"] && tempCategoriesArray.count==1) {
        
        if(self.isMovingToParentViewController)
        {
        [categoriesCollectionView.delegate collectionView:categoriesCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [self proceedButtonTapped:nil];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
    }
    if (tempCategoriesArray.count==0) {

        [proceedBarButtonItem setEnabled:NO];
    }
    else
    {
        [proceedBarButtonItem setEnabled:YES];
    }
}
-(void)showNavigationBarTitle
{
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:kReturnsInitializationScreenTitle];
    
}
-(void)showCustomerDetails
{
    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    customerAvailableBalanceLabel.text=[currentVisit.visitOptions.customer.Avail_Bal currencyString];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerstatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerstatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
}
#pragma mark UICollectionview Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  tempCategoriesArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(180, 100);
}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"filtered predicate array %@", [tempCategoriesArray objectAtIndex:indexPath.row]);
    
    [self updateSelectedCellForIndexPath:indexPath];
    
}
-(void)updateSelectedCellForIndexPath:(NSIndexPath*)indexPath
{
    if(selectedCategoryIndexpath!=nil)
    {
        /** Deselect previous selected cell*/
        OrderInitializationCollectionViewCell * previousCell=(OrderInitializationCollectionViewCell*)[categoriesCollectionView cellForItemAtIndexPath:selectedCategoryIndexpath];
        [previousCell setDeSelectedCellStatus];

    }
    
    /**selected cell*/
    OrderInitializationCollectionViewCell * cell=(OrderInitializationCollectionViewCell*)[categoriesCollectionView cellForItemAtIndexPath:indexPath];
    [cell setSelectedCellStatus];

    selectedCategoryIndexpath=indexPath;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"orderCell";
    ProductCategories * selectedCategories=[tempCategoriesArray objectAtIndex:indexPath.row];
    OrderInitializationCollectionViewCell *cell = (OrderInitializationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectedImageView.image=[UIImage imageNamed:@""];
    if ([selectedCategoryIndexpath isEqual:indexPath]) {
        //cell.selectedImageView.image=[UIImage imageNamed:@"Tick_CollectionView"];
        [cell setSelectedCellStatus];
    }
    else{
        //cell.selectedImageView.image=[UIImage imageNamed:@""];
        [cell setDeSelectedCellStatus];

        
    }
    cell.titleLbl.text=selectedCategories.Item_No;
    cell.descLbl.text=selectedCategories.Description;
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(IBAction)proceedButtonTapped:(id)sender
{
    if(selectedCategoryIndexpath!=nil)
    {
        SalesWorxReturnsViewController *returnsView=[[SalesWorxReturnsViewController alloc]initWithNibName:@"SalesWorxReturnsViewController" bundle:[NSBundle mainBundle]];
        currentVisit.visitOptions.returnOrder=[[SalesWorxReturn alloc]init];
        currentVisit.visitOptions.returnOrder.selectedCategory=[tempCategoriesArray objectAtIndex:selectedCategoryIndexpath.row];
        currentVisit.visitOptions.returnOrder.productCategoriesArray=tempCategoriesArray;
        currentVisit.visitOptions.returnOrder.returnLineItemsArray=[[NSMutableArray alloc]init];
        currentVisit.visitOptions.returnOrder.ReturnStartTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        currentVisit.visitOptions.returnOrder.confrimationDetails=[[ReturnsConfirmationDetails alloc]init];
        returnsView.currentVisit=currentVisit;
        [self.navigationController pushViewController:returnsView animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Selection" andMessage:@"Please select a category to proceed" withController:self];
    }
}



@end
