//
//  SalesWorxReturnsViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/15/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReturnsViewController.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"
#import <AVFoundation/AVFoundation.h>
#import "SalesWorxAssortmentBonusManager.h"
#import "SalesWorxAssortmentBonusInfoViewController.h"
#import "SaleWorxVATChargesCalculationsManager.h"

@interface SalesWorxReturnsViewController ()
{
    SaleWorxVATChargesCalculationsManager * VATChargesManager;

}
@end

@implementation SalesWorxReturnsViewController
@synthesize currentVisit;
@synthesize filterPopOverController;
- (void)viewDidLoad {
    [super viewDidLoad];
    proSearchType=ProductSerachNormal;
    VATChargesManager=[[SaleWorxVATChargesCalculationsManager alloc] init];
    // Do any additional setup after loading the view from its nib.
    self.title= @"";
    isKeyBoardShowing=NO;
    productsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    filteredProductsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
    returnTypes=[[NSMutableArray alloc]init];
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    [self hideNoSelectionView];
    NoSelectionMessageLabel.text=KNOProductSelectedLabel;
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    
    selectedProductIndexpath=nil;
    // longDescriptionButton.hidden=YES;
    
    //fetching array of products query has been modified by harpal on 24.01.2016 as per aki requirement to add product description and sorting products based on ranking
    
    
    enableProductRank= [NSString stringWithFormat:@"%@",appControl.ENABLE_PROD_RANK];
    showProductLongDescription=[NSString stringWithFormat:@"%@",[[[SWDefaults appControl] objectAtIndex:0]valueForKey:@"SHOW_PROD_LONG_DESC"]];
    
    NSLog(@"test string flags %@ %@", enableProductRank,showProductLongDescription);
    [self clearAllProductLabelsAndTextFields];
    appControl = [AppControl retrieveSingleton];
    
    isUpdatingReturn=NO;
    allowSearchBarEditing=YES;
    [self updateAddToReturnButtonTitle];
    returnLineItemsArray=[[NSMutableArray alloc]init];
    ReturnsTableView.allowsMultipleSelectionDuringEditing = NO;
    [self AddActivityIndicatorInProductSearchBar];
    if ([appControl.ENABLE_INVOICE_ATTACHMENT_IN_RETURNS isEqualToString:KAppControlsYESCode]) {
        invoiceTextField.hidden = NO;
        invoiceTitleLabel.hidden = NO;
    }else{
        invoiceTextField.hidden = YES;
        invoiceTitleLabel.hidden = YES;
    }
}
-(void)AddActivityIndicatorInProductSearchBar
{
    searchBarActivitySpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //set frame for activity indicator
    [searchBarActivitySpinner setFrame:CGRectMake(100, 200, 100, 100)];
    [productsTableView addSubview: searchBarActivitySpinner];
}
-(void)showActivityIndicatorInSearchBar
{
    [productsTableView setUserInteractionEnabled:NO];
    [productsFilterButton setUserInteractionEnabled:NO];
    [searchBarActivitySpinner startAnimating];
}
-(void)hideActivityIndicatorInSearchBar
{
    [productsTableView setUserInteractionEnabled:YES];
    [productsFilterButton setUserInteractionEnabled:YES];
    [searchBarActivitySpinner stopAnimating];
}

-(void)viewWillAppear:(BOOL)animated
{
    productsTableView.estimatedRowHeight=70;
    productsTableView.rowHeight=UITableViewAutomaticDimension;
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesReturnsScreenName];
    }

    showReturnsTableViewNewItemAnimationColor=NO;
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    
    NSMutableAttributedString* returnsAttributeString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Returns", nil),[NSString stringWithFormat:@"[ Category: %@, W/H: %@ ]",currentVisit.visitOptions.returnOrder.selectedCategory.Category,currentVisit.visitOptions.returnOrder.selectedCategory.Org_ID]]];
    [returnsAttributeString addAttribute:NSForegroundColorAttributeName
                                      value:[UIColor whiteColor]
                                      range:NSMakeRange(0, returnsAttributeString.length)];
    
    [returnsAttributeString addAttribute:NSFontAttributeName
                                      value:headerTitleFont
                                      range:NSMakeRange(0, returnsAttributeString.length)];
    NSMutableAttributedString* orderDetailsAttributedString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"[ Category: %@, W/H: %@ ]",currentVisit.visitOptions.returnOrder.selectedCategory.Category,currentVisit.visitOptions.returnOrder.selectedCategory.Org_ID]];
    [orderDetailsAttributedString addAttribute:NSForegroundColorAttributeName
                                         value:[UIColor whiteColor]
                                         range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    [orderDetailsAttributedString addAttribute:NSFontAttributeName
                                         value:kSWX_FONT_REGULAR(16)
                                         range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    
    //[returnsAttributeString appendAttributedString:orderDetailsAttributedString];
    
    label.attributedText=returnsAttributeString;
    self.navigationItem.titleView = label;
    
    
    saveBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    saveBarButtonItem.tintColor=[UIColor whiteColor];
    [saveBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                     forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=saveBarButtonItem;
    closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeReturnsOrderTapped)];
    closeVisitButton.tintColor=[UIColor whiteColor];
    [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=closeVisitButton;
    
    
    if(self.isMovingToParentViewController)
    {

        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [saveBarButtonItem setEnabled:NO];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            if ([enableProductRank isEqualToString:KAppControlsYESCode] || [showProductLongDescription isEqualToString:KAppControlsYESCode]) {
                productsArray=[[[SWDatabaseManager retrieveManager]fetchProductsforCategoryWithRanking:currentVisit.visitOptions.returnOrder.selectedCategory AndCustomer:currentVisit.visitOptions.customer andTransactionType:kReturnTransaction] mutableCopy];
            }
            
            else
            {
                productDescriptionInfoButton.hidden=YES;
                productsArray=[[[SWDatabaseManager retrieveManager]fetchProductsforCategory:currentVisit.visitOptions.returnOrder.selectedCategory AndCustomer:currentVisit.visitOptions.customer andTransactionType:kReturnTransaction] mutableCopy];
            }
            
            
            
            if(!currentVisit.visitOptions.customer.isBonusAvailable){
                [productsArray setValue:@"0" forKey:@"bonus"];
            }
            
            currentVisit.visitOptions.salesOrder.AssortmentPlansArray=[[NSMutableArray alloc]init];
            if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode] && currentVisit.visitOptions.customer.isBonusAvailable){
                SalesWorxAssortmentBonusManager *manager=[[SalesWorxAssortmentBonusManager alloc]init];
                currentVisit.visitOptions.returnOrder.AssortmentPlansArray=[manager FetchAssortmentBonusPlansForCategoryProducts:productsArray ForCustomer:currentVisit.visitOptions.customer];
            }
            

            filteredProductsArray=productsArray;
            productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsArray valueForKeyPath:@"@distinctUnionOfObjects.Brand_Code"]];
            productsBrandCodesArray= [[NSMutableArray alloc]initWithArray:[productsBrandCodesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];

            /** removing invalid brandcodes*/
            //[productsBrandCodesArray removeObject:@""];
            /*filtering products by brand code*/
            for (NSString *productBrandCode in productsBrandCodesArray) {
                //            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
                //                                           @"Brand_Code=%@",productBrandCode];
                //            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:brandCodePredicate]];
                [productsArrayGroupedByBrandCode addObject:[[NSMutableArray alloc]init]];
                
            }
            filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if(filteredProductsArrayGroupedByBrandCode.count==1)
                {
                    [self ExpandFirstProductsSection];
                }
                else
                {
                    ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
                }
                [productsTableView reloadData];
                [self hideProductDetailsView];
                [self hideProductOrderDetailsView];
                [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
                [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];
                [self showCustomerDetails];
                [saveBarButtonItem setEnabled:YES];
                [MBProgressHUD hideHUDForView:self.view animated:YES];


            });
        });
        
        returnTypes=[[[SWDatabaseManager retrieveManager]DBGetRMALotTypes] mutableCopy];

        UOMTextField.isReadOnly=YES;
        // Listen for keyboard appearances and disappearances
        if([appControl.SHOW_SALES_HISTORY isEqualToString:KAppControlsNOCode])
        {
            [productSalesHistoryInfoButton setHidden:YES];
            productSalesHistoryInfoButtonHeightContraint.constant=0;
            productSalesHistoryInfoButtonWidthContraint.constant=0;
            productSalesHistoryInfoButtontarilingMarginContraint.constant=0;
        }

    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];

    ReturnsTableView.delegate=self;
    ReturnsTableView.dataSource=self;
    
    [self updateSalesOrderTotalAmount];
    
    if(self.isMovingToParentViewController){
        if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode]){
            xSearchBarTrailingspaceToFilterButton.constant=47;
            [barcodeScanButton setHidden:NO];
        }
        else{
            xSearchBarTrailingspaceToFilterButton.constant=8;
            [barcodeScanButton setHidden:YES];
        }
        
        /** Hiding VAT Columns*/
        if(![appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            xOrderTableHeaderNetAmountLabelLeadingMarginConstraint.constant=0;
            xOrderTableHeaderNetAmountLabelWidthConstraint.constant=0;
            xOrderTableHeaderVATChargeLabelLeadingMarginConstraint.constant=0;
            xOrderTableHeaderVATChargeLabelWidthConstraint.constant=0;
            ReturnsTableHeaderTotalLabel.textAlignment = NSTextAlignmentRight;
        }
    }

}
-(void)showCustomerDetails
{
    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    customerAvailableBalanceLabel.text=[currentVisit.visitOptions.customer.Avail_Bal currencyString];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerstatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerstatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    
}

-(void)closeReturnsOrderTapped
{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"Cancel return", nil)
                                      message:NSLocalizedString(@"Would you like to cancel the return?", nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                    }];
        [alert addAction:yesAction];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
        [alert addAction:noAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    
}

-(void)clearAllProductLabelsAndTextFields
{
    productRetailPriceLabel.text=@"N/A";
    productWholeSalePriceLabel.text=@"N/A";
    productDefaultDiscountLabel.text=@"N/A";
    productSpecialDiscountLabel.text=@"N/A";
    productBonusItemLabel.text=@"N/A";
    productDefaultBonusLabel.text=@"N/A";
    
    returnQtyTextField.text=@"";
    UOMTextField.text=@"";
    recievingBonusTextField.text=@"";
    ReasonForReturnTextfield.text=@"";
    invoiceTextField.text=@"";
    ExpiryDateTextField.text=@"";
    lotNumberTextField.text=@"";
    OrderItemTotalAmountLabel.text=@"N/A";
}
-(void)updateAddToReturnButtonTitle
{
    if(isUpdatingReturn)
    {
        [addItemButton setTitle:KReturnProductUpdateButtonTitle forState:UIControlStateNormal];
    }
    else
    {
        [addItemButton setTitle:KReturnProductAddButtonTitle forState:UIControlStateNormal];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark LayOutConstarinsMethods
-(void)showProductDetailsView
{
    [productDetailsView setHidden:NO];
    productDetailsViewTopConstraiint.constant=KSalesOrderScreenProductDetailsViewTopMargin;
    productDetailsViewHeightConstraiint.constant=KSalesOrderScreenProductDetailsViewHeight;
    
}
-(void)hideProductDetailsView
{
    [productDetailsView setHidden:YES];
    productDetailsViewTopConstraiint.constant=KZero;
    productDetailsViewHeightConstraiint.constant=KZero;
}
-(void)showProductOrderDetailsView
{
       [returnProductDetailsView setHidden:NO];
    returnProductDetailsViewTopConstraiint.constant=KSalesOrderScreenProductOrderDetailsViewTopMargin;
    returnProductDetailsViewHeightConstraiint.constant=KSalesOrderScreenProductOrderDetailsViewHeight;
    
}
-(void)hideProductOrderDetailsView
{

    [returnProductDetailsView setHidden:YES];
    returnProductDetailsViewTopConstraiint.constant=KZero;
    returnProductDetailsViewHeightConstraiint.constant=KZero;
}
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    NoSelectionTopMarginConstraint.constant=KZero;
}
-(void)showNoSelectionViewWithMessage:(NSString *)message WithHeight:(NSInteger)height
{
    [NoSelectionView setHidden:NO];
    NoSelectionMessageLabel.text=message;
    NoSelectionHeightConstraint.constant=height;
    NoSelectionTopMarginConstraint.constant=KSalesOrderScreenNoSelectionsViewTopMargin;
    
}
#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==ReturnsTableView) {
        if(section==0)
            return 2.0f;
        return 5.0f;
    }
    if (tableView==productsTableView) {
        
        return 2.0f;
    }
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==ReturnsTableView) {
        return 44;
    }
    if (tableView==productsTableView) {
        if(indexPath.row==0)
        {
            return 44.0;
        }
        return UITableViewAutomaticDimension;
    }
    return 0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==ReturnsTableView) {
        //        if(section==0)
        //        {
        //
        //            UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, OrdersTableHeaderView.frame.size.width, OrdersTableHeaderView.frame.size.height)];
        //            OrdersTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
        //            [view addSubview:OrdersTableHeaderView];
        //            return view;
        //        }
        //        else
        //        {
        //            UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 5.0)];
        //            view.backgroundColor=UIViewBackGroundColor;
        //            return view;
        //        }
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 5.0)];
        view.backgroundColor=UIViewBackGroundColor;
        return view;
        return nil;
    }
    else
    {
        return nil;
    }
    
}   // custom view for header. will be adjusted to default or specified header height


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==productsTableView) {
        return productsBrandCodesArray.count;
    }
    else if (tableView==ReturnsTableView)
    {
        return returnLineItemsArray.count;
    }
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==productsTableView) {
        
        if(![ProductTableViewExpandedSectionsArray containsObject:[NSString stringWithFormat:@"%ld",(long)section]])
            return 1;
        else
            return [(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:section]count]+1;
    }
    else if (tableView==ReturnsTableView)
    {
        ReturnsLineItem *returnLineItem=[returnLineItemsArray objectAtIndex:section];
        NSInteger numberOfrows=0;
        if(returnLineItem.returnProductQty>0)
            numberOfrows++;
        if(returnLineItem.recievedBonusQty>0)
            numberOfrows++;
        return numberOfrows;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView==productsTableView) {
        
        
        if(indexPath.row==0)
        {
            static NSString* identifier=@"BrandCodeCell";
            SalesWorxSalesOrderProductsTableViewBrandcodeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesOrderProductsTableViewBrandcodeCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.brandCodeLabel.text=[productsBrandCodesArray objectAtIndex:indexPath.section];
            cell.brandCodeLabel.font=MedRepTitleFont;
            if(![ProductTableViewExpandedSectionsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section ]])
            {
                [self updateBrandCodeCell:cell isExpanded:NO];
            }
            else
            {
                [self updateBrandCodeCell:cell isExpanded:YES];
            }
            return cell;
        }
        else
        {
            Products *currentProduct;
            currentProduct=[[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1];
            
            
            
            static NSString* identifier=@"updatedDesignCell";
            MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            if(![selectedProductIndexpath isEqual:indexPath] || selectedProductIndexpath==nil)
            {
                [cell setDeSelectedCellStatus];
                
            }
            else
            {
                [cell setSelectedCellStatus];
            }
            
            
            
            cell.titleLbl.text=[[NSString stringWithFormat:@"%@",currentProduct.Description] capitalizedString];
            cell.descLbl.text=[NSString stringWithFormat:@"%@",currentProduct.Item_Code];
            
            
            return cell;
            
        }
        
    }
    else
    {
        
        static NSString* identifier=@"returnLineItemCell";
        SalesWorxReturnLineItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxReturnLineItemsTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        ReturnsLineItem *currentLineItem=[returnLineItemsArray objectAtIndex:indexPath.section];
        
        
        if(indexPath.row==0)
        {
            
            NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.returnProduct.selectedUOM];
            
            cell.QunatutyLbl.text=[NSString stringWithFormat:@"%@ %@",currentLineItem.returnProductQty,uomStr];
            cell.UnitPriceLbl.text=[[NSString
                                     stringWithFormat:@"%@",currentLineItem.returnProduct.productPriceList.Unit_Selling_Price] floatString];
            cell.ProductNameLbl.text=currentLineItem.returnProduct.Description;
            cell.NetAmountLbl.text=[[NSString
                                     stringWithFormat:@"%@",currentLineItem.returnItemNetamount] floatString];
            cell.ToatlLbl.text=[[NSString
                                 stringWithFormat:@"%@",currentLineItem.returnItemTotalamount] floatString];
            cell.DiscountLbl.text=[@"0.00" floatString];
            cell.VATChargesLbl.text=currentLineItem.VATChargeObj!=nil?[[NSString
                                                                        stringWithFormat:@"%.2f",currentLineItem.VATChargeObj.VATCharge.floatValue]stringByAppendingString:@"%"]:@"0.00%" ;
            
            if(selectedReturnItemIndexpath!=nil && selectedReturnItemIndexpath.section==indexPath.section)
            {
                /*seclecting the present selected cell*/
                [cell.statusView setHidden:YES];
                cell.ProductNameLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.QunatutyLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.UnitPriceLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.ToatlLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.DiscountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.NetAmountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
                cell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
                
            }
            else
            {
                [cell.statusView setHidden:YES];
                cell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
                cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
                
            }
            
            NSMutableArray *rightSwipeButtons=[[NSMutableArray alloc]init];
            [rightSwipeButtons addObject:[MGSwipeButton buttonWithTitle:NSLocalizedString(@"Delete", nil) backgroundColor:[UIColor redColor]]];
            cell.rightButtons = rightSwipeButtons;
            cell.rightSwipeSettings.transition = MGSwipeTransition3D;
            cell.delegate=self;
            if(indexPath.section==0 && showReturnsTableViewNewItemAnimationColor)
            {
                cell.lineItemRowAnimationView.backgroundColor=KNewRowAnimationColor;
            }
            else
            {
                cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
            }
        }
        else if(indexPath.row==1)
        {
            [cell.statusView setHidden:NO];
            
            NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.bonusProduct.selectedUOM];
            
            cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.recievedBonusQty,uomStr];
            
            cell.ProductNameLbl.text=currentLineItem.bonusProduct.Description;

            
            cell.UnitPriceLbl.text=[@"0.0" floatString];
            cell.NetAmountLbl.text=[@"0.0" floatString];
            cell.ToatlLbl.text=[@"0.0" floatString];
            cell.DiscountLbl.text=[@"0.0" floatString];
            cell.VATChargesLbl.text=@"0.00%";
            
            cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
            
            cell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
            cell.rightButtons = [[NSArray alloc]init];
            cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];


        }
            return cell;
        
    }
    
}
-(void)updateBrandCodeCell:(SalesWorxSalesOrderProductsTableViewBrandcodeCell*)cell isExpanded:(BOOL)isExpanded
{
    if(!isExpanded)
    {
        cell.expandCollapseArrowImageView.image=[UIImage imageNamed:KSalesOrderProductTableViewDownArrowImageName];
        cell.backgroundColor=KSalesOrderProductTableViewBrandCodeCellCollapsedColor;
        cell.brandCodeLabel.textColor=KSalesOrderProductTableViewBrandCodeCellCollapsedTextColor;
    }
    else
    {
        cell.expandCollapseArrowImageView.image=[UIImage imageNamed:KSalesOrderProductTableViewUpArrowImageName];
        cell.backgroundColor=KSalesOrderProductTableViewBrandCodeCellExpandColor;
        cell.brandCodeLabel.textColor=KSalesOrderProductTableViewBrandCodeCellExpandTextColor;
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==productsTableView)
    {
        if(indexPath.row==0)
        {
            [self.view endEditing:YES];

            SalesWorxSalesOrderProductsTableViewBrandcodeCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            NSMutableArray *productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
            
            if(ProductTableViewExpandedSectionsArray.count>0)
            {
                NSInteger expnadedSection=[[ProductTableViewExpandedSectionsArray objectAtIndex:0]integerValue];
                cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:expnadedSection]];
                
                for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:expnadedSection]count]; i++) {
                    NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:expnadedSection];
                    [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                }
                [tableView beginUpdates];
                [ProductTableViewExpandedSectionsArray removeAllObjects];
                [tableView deleteRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                [self updateBrandCodeCell:cell isExpanded:NO];
                [tableView endUpdates];
                
                if(expnadedSection!=indexPath.section)
                {
                    cell = [tableView cellForRowAtIndexPath:indexPath];
                    
                    productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
                    [self AddProductObjectsToGroupByArrayAtIndex:indexPath.section];

                    for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section]count]; i++) {
                        NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:indexPath.section];
                        [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                    }
                    [tableView beginUpdates];
                    [ProductTableViewExpandedSectionsArray addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
                    [tableView insertRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self updateBrandCodeCell:cell isExpanded:YES];
                    [tableView endUpdates];
                    
                }
                
            }
            else
            {
                cell = [tableView cellForRowAtIndexPath:indexPath];
                
                productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
                [self AddProductObjectsToGroupByArrayAtIndex:indexPath.section];

                for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section]count]; i++) {
                    NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:indexPath.section];
                    [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                }
                [tableView beginUpdates];
                [ProductTableViewExpandedSectionsArray addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
                [tableView insertRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                [self updateBrandCodeCell:cell isExpanded:YES];
                [tableView endUpdates];
            }
            
        }
        else
        {
            
            [self.view endEditing:YES];
            [self showProductDetailsView];
            [self showProductOrderDetailsView];
            [self hideNoSelectionView];
            [self clearAllProductLabelsAndTextFields];
            [self updateTheProductTableViewSelectedCellIndexpath:indexPath];
            matchedInvoicesArray = [[NSMutableArray alloc]init];
            selectedProduct=[(Products *)[[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1] copy];
            
            //check if item is restricted.
            if ([appControl.FS_ENABLE_RETURN_RESTRICTION isEqualToString:KAppControlsYESCode]) {
                
                NSString* isItemRestricted=[[SWDatabaseManager retrieveManager]isItemRestrictedForReturn:selectedProduct.Inventory_Item_ID];
                if ([isItemRestricted isEqualToString:KAppControlsYESCode]) {
                    
                    [self hideProductDetailsView];
                    [self hideProductOrderDetailsView];
                    [self showNoSelectionViewWithMessage:KReturnRestrictedErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
                    NoSelectionImageView.image=[[UIImage alloc]init];
                    [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];
                    
                    return;

                }
                else
                {
                    [self showProductDetailsView];
                    [self showProductOrderDetailsView];
                    [self hideNoSelectionView];
                }
                
            }
            
            selectedProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:selectedProduct];
            /* Price list for Primary UOM*/
            if([self fetchProductPriceListData:selectedProduct]!=nil)
            {
                selectedProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedProduct];
                productWholeSalePriceLabel.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString] ;
                productRetailPriceLabel.text=[selectedProduct.productPriceList.Unit_List_Price floatString];
                
            }
            else if([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode] && selectedProduct.ProductUOMArray.count>0 )
                // checking for other UOM price list availability
            {
                NSMutableArray *priceListAvailableUOMsArray=[[SWDatabaseManager retrieveManager] getPriceListAvailableUOMSForSalesOrderProduct:selectedProduct AndCustomer:currentVisit.visitOptions.customer];
                if (priceListAvailableUOMsArray.count==0)
                {
                    productWholeSalePriceLabel.text=KNotApplicable;
                    productRetailPriceLabel.text=KNotApplicable;
                    [self showProductOrderDetailsView];
                    [self hideProductOrderDetailsView];
                    [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
                    [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];
                }
                else
                {
                    matchedInvoicesArray = [[NSMutableArray alloc]init];
                    selectedProduct=[self getUpdatedProductDetails:selectedProduct ToSelectedUOM:[priceListAvailableUOMsArray objectAtIndex:0]];
                    selectedProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedProduct];
                    productWholeSalePriceLabel.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString] ;
                    productRetailPriceLabel.text=[selectedProduct.productPriceList.Unit_List_Price floatString];
                    
                }
                
            }
            else
            {
                productWholeSalePriceLabel.text=KNotApplicable;
                productRetailPriceLabel.text=KNotApplicable;
                [self showProductOrderDetailsView];
                [self hideProductOrderDetailsView];
                [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
                [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];
                
            }
          
            productCodeLabel.text=selectedProduct.Item_Code;
            productNameLabel.text=selectedProduct.Description;

            productDefaultDiscountLabel.text=[[NSString stringWithFormat:@"%0.2f", [selectedProduct.Discount doubleValue]] stringByAppendingString:@"%"];
            
            selectedReturnLineItem=[[ReturnsLineItem alloc]init];
            selectedReturnLineItem.returnProduct=selectedProduct;
           selectedReturnLineItem.VATChargeObj=[VATChargesManager getVATChargeForProduct:selectedProduct AndCustomer:currentVisit.visitOptions.customer];
            selectedReturnLineItem.returnProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:selectedReturnLineItem.returnProduct];
            [self updateUOMTextfield];
            [self getProductSpecailDisount];
            if([selectedReturnLineItem.returnProduct.specialDiscount doubleValue]>0.0)
                productSpecialDiscountLabel.text=[NSString stringWithFormat:@"%0.2f", [selectedReturnLineItem.returnProduct.specialDiscount doubleValue]];
            else
                productSpecialDiscountLabel.text=@"N/A";
            [self getBonusDetails];
            

            
            isUpdatingReturn=NO;
            [self updateAddToReturnButtonTitle];
            selectedReturnItemIndexpath=nil;
            [ReturnsTableView reloadData];
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            NSString *todayString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:[NSDate date]]];
            [self didSelectDate:todayString];
        }
    }
    else
    {
        
        [addItemButton setTitle:@"Update" forState:UIControlStateNormal];
        selectedReturnLineItem=[(ReturnsLineItem*)[returnLineItemsArray objectAtIndex:indexPath.section] copy];
        
        
        [self showProductDetailsView];
        [self showProductOrderDetailsView];
        [self hideNoSelectionView];
        [self clearAllProductLabelsAndTextFields];
        [self updateTheOrderTableViewSelectedCellIndexpath:indexPath];
        matchedInvoicesArray = [[NSMutableArray alloc]init];
        selectedProduct=selectedReturnLineItem.returnProduct;
        if([self fetchProductPriceListData:selectedReturnLineItem.returnProduct]!=nil)
        {
            selectedReturnLineItem.returnProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedReturnLineItem.returnProduct];
            productWholeSalePriceLabel.text=[selectedReturnLineItem.returnProduct.productPriceList.Unit_Selling_Price floatString];
            productRetailPriceLabel.text=[selectedReturnLineItem.returnProduct.productPriceList.Unit_List_Price floatString];
            [self showProductDetailsView];
            [self showProductOrderDetailsView];
            [self hideNoSelectionView];
        }
        else
        {
            productWholeSalePriceLabel.text=KNotApplicable;
            productRetailPriceLabel.text=KNotApplicable;
            [self showProductOrderDetailsView];
            [self hideProductOrderDetailsView];
            [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
            [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];

        }
        productCodeLabel.text=selectedReturnLineItem.returnProduct.Item_Code;
        productNameLabel.text=selectedReturnLineItem.returnProduct.Description;
        productDefaultDiscountLabel.text=[[NSString stringWithFormat:@"%0.2f", [selectedReturnLineItem.returnProduct.Discount doubleValue]] stringByAppendingString:@"%"];
        selectedReturnLineItem.returnProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:selectedReturnLineItem.returnProduct];
        [self updateUOMTextfield];
        [self getProductSpecailDisount];
        if([selectedReturnLineItem.returnProduct.specialDiscount doubleValue]>0.0)
            productSpecialDiscountLabel.text=[NSString stringWithFormat:@"%0.2f", [selectedReturnLineItem.returnProduct.specialDiscount doubleValue]];
        else
            productSpecialDiscountLabel.text=@"N/A";
        
        [self getBonusDetails];
        
        [self updateBonusFields];
        recievingBonusTextField.text=[NSString stringWithFormat:@"%0.0f",selectedReturnLineItem.recievedBonusQty];
        returnQtyTextField.text=[NSString stringWithFormat:@"%@",selectedReturnLineItem.returnProductQty];
        [self updateProductAmountLabels];
        NSString * refinedDatetoDisplay=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:selectedReturnLineItem.expiryDate];
        ExpiryDateTextField.text=refinedDatetoDisplay;
        ReasonForReturnTextfield.text=selectedReturnLineItem.reasonForReturnDescription;
        invoiceTextField.text=selectedReturnLineItem.selectedInvoice.invoiceNumber;
        lotNumberTextField.text=selectedReturnLineItem.lotNumber;
        isUpdatingReturn=YES;
        [self updateAddToReturnButtonTitle];
        selectedReturnItemIndexpath=indexPath;
        selectedProductIndexpath=nil;
        if(showReturnsTableViewNewItemAnimationColor)
        {
            showReturnsTableViewNewItemAnimationColor=NO;
            
            [ReturnsTableView beginUpdates];
            [ReturnsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
            [ReturnsTableView endUpdates];
        }
        [productsTableView reloadData];
    }
    
}
-(void)updateTheProductTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    if(selectedProductIndexpath!=nil)
    {
        MedRepUpdatedDesignCell *previousSelectedCell=[productsTableView cellForRowAtIndexPath:selectedProductIndexpath];
        [previousSelectedCell setDeSelectedCellStatus];
        previousSelectedCell.cellDividerImg.hidden=NO;
    }
    /*seclecting the present selected cell*/
    MedRepUpdatedDesignCell *currentSelectedcell=[productsTableView cellForRowAtIndexPath:indexpath];
    [currentSelectedcell setSelectedCellStatus];
    currentSelectedcell.cellDividerImg.hidden=YES;
    selectedProductIndexpath=indexpath;

}
-(void)updateTheOrderTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    
    /* deselecting the previous selected cell*/
    if(selectedReturnItemIndexpath!=nil)
    {
        SalesWorxReturnLineItemsTableViewCell *previousSelectedCell=[ReturnsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedReturnItemIndexpath.section]];
        previousSelectedCell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
        previousSelectedCell.ProductNameLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.QunatutyLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.UnitPriceLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.ToatlLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.DiscountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.NetAmountLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    }
    
    /*seclecting the present selected cell*/
    SalesWorxReturnLineItemsTableViewCell *currentSelectedcell=[ReturnsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexpath.section]];
    currentSelectedcell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
    currentSelectedcell.ProductNameLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.QunatutyLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.UnitPriceLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.ToatlLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.DiscountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.NetAmountLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    selectedReturnItemIndexpath=indexpath;
}
-(void)getProductSpecailDisount
{
    if ([appControl.ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsYESCode])
    {
        NSString* specialDiscount;
        specialDiscount=[[SWDatabaseManager retrieveManager] dbGetSpecialDiscount:selectedProduct.Inventory_Item_ID];
        selectedReturnLineItem.returnProduct.specialDiscount=specialDiscount;
    }
    else
    {
        selectedReturnLineItem.returnProduct.specialDiscount=@"0.0";
        
    }
}
-(void)getBonusDetails
{

    NSMutableArray *bonusDetailsArray=[[NSMutableArray alloc]init];
    
    /** checking availability of assortment bonus*/
    BOOL isAssortmentBonusPlanAvailable=NO;
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
        SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
        if([bnsManager IsAnyAssortmentBonusAvailableForTheProduct:[selectedReturnLineItem.returnProduct copy] AssortmentPlans:currentVisit.visitOptions.returnOrder.AssortmentPlansArray])
            isAssortmentBonusPlanAvailable=YES;
    }
    if(currentVisit.visitOptions.customer.isBonusAvailable)
    {
        NSArray * bonusInfo=[[SWDatabaseManager retrieveManager] dbGetBonusInfo:selectedReturnLineItem.returnProduct.Item_Code];
        for (NSInteger i=0; i<bonusInfo.count; i++) {
            NSMutableDictionary *bonusItemInfo=[bonusInfo objectAtIndex:i];
            ProductBonusItem * bonusItem=[[ProductBonusItem alloc]init];
            bonusItem.Description=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Description"]];
            bonusItem.Get_Add_Per=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Add_Per"]];
            bonusItem.Get_Item=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Item"]];
            bonusItem.Get_Qty=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Qty"]];
            bonusItem.Price_Break_Type_Code=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Price_Break_Type_Code"]];
            bonusItem.Prom_Qty_From=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Prom_Qty_From"]];
            bonusItem.Prom_Qty_To=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Prom_Qty_To"]];
            [bonusDetailsArray addObject:bonusItem];
        }
    }
    selectedReturnLineItem.bonusDetailsArray=bonusDetailsArray;
    
}
-(void)updateBonusFields
{
    if(selectedReturnLineItem.bonusProduct!=nil)
    {
        productBonusItemLabel.text=selectedReturnLineItem.bonusProduct.Description;
        productDefaultBonusLabel.text=[NSString stringWithFormat:@"%0.0f",selectedReturnLineItem.defaultBonusQty];
    }
    else
    {
        productBonusItemLabel.text=KNotApplicable;
        productDefaultBonusLabel.text=KNotApplicable;
    }
}
-(void)updateProductAmountLabels
{
    OrderItemTotalAmountLabel.text=[[[NSString alloc]initWithFormat:@"%@",selectedReturnLineItem.returnItemNetamount] currencyString];
 }

#pragma mark ButtonActions
-(void)saveButtonTapped
{
    NSLog(@"Save Button tapped");
    
    if(returnLineItemsArray.count>0)
    {
        SalesWorxReturnConfirmationViewController *salesWorxReturnConfirmationViewController=[[SalesWorxReturnConfirmationViewController alloc]initWithNibName:@"SalesWorxReturnConfirmationViewController" bundle:[NSBundle mainBundle]];
        currentVisit.visitOptions.returnOrder.returnLineItemsArray=returnLineItemsArray;
        salesWorxReturnConfirmationViewController.currentVisit=currentVisit;
        [self.navigationController pushViewController:salesWorxReturnConfirmationViewController animated:YES];
        
    }
    else
    {

        [self showAlertAfterHidingKeyBoard:@"No products" andMessage:@"Please add at least one product to proceed" withController:nil];
    }

}
#pragma mark ProductDetailsMethods

-(IBAction)productDescriptionInfoButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    NSString *longDescription=@"";
    longDescription=[[SWDatabaseManager retrieveManager] getLongDescriptionForInventoryItemId:selectedProduct.Inventory_Item_ID];
    SalesWorxSalesOrderProductLongDescriptionViewController *salesOrderProductLongDescriptionViewController=[[SalesWorxSalesOrderProductLongDescriptionViewController alloc]initWithNibName:@"SalesWorxSalesOrderProductLongDescriptionViewController" bundle:[NSBundle mainBundle]];
    salesOrderProductLongDescriptionViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderProductLongDescriptionViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderProductLongDescriptionViewController.Description=longDescription;
    [self.navigationController presentViewController:salesOrderProductLongDescriptionViewController animated:NO completion:nil];

}
-(IBAction)productStockInfoButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    SalesOrderStockInfoViewController *salesOrderStockInfoViewController=[[SalesOrderStockInfoViewController alloc]initWithNibName:@"SalesOrderStockInfoViewController" bundle:[NSBundle mainBundle]];
    salesOrderStockInfoViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderStockInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderStockInfoViewController.returnLineItem=selectedReturnLineItem;
    salesOrderStockInfoViewController.isFromReturnsView=YES;
    salesOrderStockInfoViewController.wareHouseId=currentVisit.visitOptions.returnOrder.selectedCategory.Org_ID;
    [self.navigationController presentViewController:salesOrderStockInfoViewController animated:NO completion:nil];
}
-(IBAction)productBonusInfoButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    
    if(selectedReturnLineItem.returnProduct.bonus>0)
    {
        NSMutableArray *tempProductAsstBonusPlans=[[NSMutableArray alloc]init];;
        if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
            NSMutableArray  *ProductAssortmentPlans=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B on A.Item_Code=B.Item_Code Where A.Item_Code='%@' and Is_Get_Item='N'",selectedReturnLineItem.returnProduct.Item_Code]];
            if(ProductAssortmentPlans.count>0)
            {
                ProductAssortmentPlans= [ProductAssortmentPlans valueForKey:@"Assortment_Plan_ID"];
                
                tempProductAsstBonusPlans= [[currentVisit.visitOptions.returnOrder.AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId IN %@", ProductAssortmentPlans]] mutableCopy];
            }
            
        }
        if(tempProductAsstBonusPlans.count>0){
            SalesWorxAssortmentBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesWorxAssortmentBonusInfoViewController alloc]initWithNibName:@"SalesWorxAssortmentBonusInfoViewController" bundle:[NSBundle mainBundle]];
            salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
            salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
            salesOrderBonusInfoViewController.plan=[tempProductAsstBonusPlans objectAtIndex:0];
            [self.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
        }
        else{
            SalesOrderBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesOrderBonusInfoViewController alloc]initWithNibName:@"SalesOrderBonusInfoViewController" bundle:[NSBundle mainBundle]];
            salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
            salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
            [self getBonusDetails];
            salesOrderBonusInfoViewController.isFromReturnsView=YES;
            salesOrderBonusInfoViewController.productsArray=productsArray;
            salesOrderBonusInfoViewController.returnLineItem=selectedReturnLineItem;
            salesOrderBonusInfoViewController.bonusArray=selectedReturnLineItem.bonusDetailsArray;
            [self.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"Bonus not available" withController:self];
    }
    
    
}
-(IBAction)productSalesHistoryInfoButtonTapped:(id)sender
{
    
    [self.view endEditing:YES];

    SalesOrderSalesHistoryInfoViewController *salesOrderSalesHistoryInfoViewController=[[SalesOrderSalesHistoryInfoViewController alloc]initWithNibName:@"SalesOrderSalesHistoryInfoViewController" bundle:[NSBundle mainBundle]];
    salesOrderSalesHistoryInfoViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderSalesHistoryInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderSalesHistoryInfoViewController.returnLineItem=selectedReturnLineItem;
    salesOrderSalesHistoryInfoViewController.isFromReturnsView=YES;
    salesOrderSalesHistoryInfoViewController.customerVisit=currentVisit;
    [self.navigationController presentViewController:salesOrderSalesHistoryInfoViewController animated:NO completion:nil];
    
    
}


-(id)fetchProductPriceListData:(Products*)Product
{
    NSMutableArray *  productPriceListArray=[[NSMutableArray alloc]init];
    PriceList* priceList=[[PriceList alloc ]init];
    productPriceListArray=[[SWDatabaseManager retrieveManager]fetchProductPriceList:Product withCustomer:currentVisit.visitOptions.customer];
    if (productPriceListArray.count>0) {
        
        NSLog(@"product price list response is %@", productPriceListArray);
        
        
        //now refine using a predicate and display the price based on selected uom
        NSPredicate * uomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM ==[cd] %@",Product.selectedUOM];
        
        NSMutableArray* filteredPredicateArray=[[productPriceListArray filteredArrayUsingPredicate:uomPredicate] mutableCopy];
        NSLog(@"filtered UOM data is %@", filteredPredicateArray);
        if (filteredPredicateArray.count>0) {
            priceList=[filteredPredicateArray objectAtIndex:0];
            if([priceList.Unit_Selling_Price doubleValue]>0)
            return priceList;
            else
                return nil;
            
        }
        
    }
    return nil;
    
}

#pragma UITextField DelegateMethods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if(textField==returnQtyTextField)
    {
        NSString *expression = ([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode]&&
                                ![selectedReturnLineItem.returnProduct.primaryUOM isEqualToString:selectedReturnLineItem.returnProduct.selectedUOM]&&
                                ([SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[selectedReturnLineItem.returnProduct copy]]<1))?KDecimalQuantityRegExpression:KWholeNumberQuantityRegExpression;
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KReturnsScreenQuantityTextFieldMaximumDigitsLimit);
    }

    else if(textField==recievingBonusTextField)
    {
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KReturnsOrderScreenBonusQuantityTextFieldMaximumDigitsLimit);
        
    }
    else if(textField==lotNumberTextField)
    {
        return (newString.length<=KReturnsOrderScreenLotNumberTextFieldMaximumDigitsLimit);
    }
    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==ReasonForReturnTextfield) {
        [self.view endEditing:true];
        popOverTitle=kReturnsScreenReasonForReturnPopOverTitle;
        [self presentPopoverfor:ReasonForReturnTextfield withTitle:kReturnsScreenReasonForReturnPopOverTitle withContent:[returnTypes valueForKey:@"Description"]];
        return NO;
    }
    else if(textField==ExpiryDateTextField)
    {
        [self.view endEditing:true];
        [self showDatePicker];
        return NO;

    }
    else if (textField==UOMTextField) {
        [self.view endEditing:true];
        popOverTitle=KSalesOrderUOMPopOverTitle;
        
        NSMutableArray *priceListAvailableUOMsArray=[[SWDatabaseManager retrieveManager] getPriceListAvailableUOMSForSalesOrderProduct:selectedProduct AndCustomer:currentVisit.visitOptions.customer];
        [self presentPopoverfor:UOMTextField withTitle:KSalesOrderUOMPopOverTitle withContent:[priceListAvailableUOMsArray valueForKey:@"Item_UOM"]];
        return NO;
    }
    else if (textField == invoiceTextField) {
        [self.view endEditing:true];
        [self displayMatchedInvoicesVC];
        return NO;
    }
    
    return YES;
}

-(void)displayMatchedInvoicesVC
{
    SalesWorxReturnMatchedInvoicesViewController *presentingView = [[SalesWorxReturnMatchedInvoicesViewController alloc]init];
    presentingView.selectedProduct=selectedProduct;
    presentingView.customerID = currentVisit.visitOptions.customer.Customer_ID;
    presentingView.shipSiteID = currentVisit.visitOptions.customer.Ship_Site_Use_ID;
    presentingView.matchedInvoicesArray= matchedInvoicesArray;
    if (selectedReturnLineItem.selectedInvoice) {
        presentingView.previousSelectedInvoice = selectedReturnLineItem.selectedInvoice;
    }
    presentingView.invoiceDelegate=self;
    UINavigationController *toDoPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    toDoPopUpViewController.navigationBarHidden=YES;
    toDoPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    toDoPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:toDoPopUpViewController animated:NO completion:nil];
}

-(void)didSelectInvoice:(SalesWorxReturnsMatchedInVoice *)inVoice andAllMatchedInvoices:(NSMutableArray *)arrMatchedInvoice
{
    matchedInvoicesArray = arrMatchedInvoice;
    
    selectedReturnLineItem.selectedInvoice = inVoice;
    selectedReturnLineItem.invoiceReason = nil;
    invoiceTextField.text = inVoice.invoiceNumber;
}

-(void)closeInvoicePopOver:(NSMutableArray *)arrMatchedInvoice {
    
    matchedInvoicesArray = arrMatchedInvoice;
    
    SalesWorxReturnInvoiceReasonPopoverViewController *presentingView = [[SalesWorxReturnInvoiceReasonPopoverViewController alloc]init];
    presentingView.invoiceReasonDelegate = self;
    
    UINavigationController *toDoPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    toDoPopUpViewController.navigationBarHidden=YES;
    toDoPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    toDoPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:toDoPopUpViewController animated:NO completion:nil];
}

-(void)didSaveInvoiceReason:(NSString *)reason
{
    selectedReturnLineItem.selectedInvoice = [[SalesWorxReturnsMatchedInVoice alloc]init];
    selectedReturnLineItem.invoiceReason = reason;
    invoiceTextField.text = reason;
}

-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==returnQtyTextField)
    {
        [recievingBonusTextField becomeFirstResponder];
    }
    else if (textField==recievingBonusTextField)
    {
        [lotNumberTextField becomeFirstResponder];
    }
    else if (textField==lotNumberTextField)
    {
        [invoiceTextField becomeFirstResponder];
    }
    
    return NO;
}

- (IBAction)ReturnsTextFieldEditingChanged:(id)sender
{
    if (sender==returnQtyTextField)
    {

        selectedReturnLineItem.returnProductQty=[NSDecimalNumber ValidDecimalNumberWithString:returnQtyTextField.text];
        [self calculateBonus];
        [self updateBonusFields];
        
        NSDecimalNumber  * productSelleingPriceDecimalNumber=[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.4f",selectedReturnLineItem.returnProduct.productPriceList.Unit_Selling_Price.doubleValue]];
        
        
        if([appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode] &&
           selectedReturnLineItem.VATChargeObj!=nil){
            
            selectedReturnLineItem.returnItemVATChargeamount=[[[selectedReturnLineItem.returnProductQty decimalNumberByMultiplyingBy:productSelleingPriceDecimalNumber] decimalNumberByMultiplyingBy:selectedReturnLineItem.VATChargeObj.VATCharge] decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"100"]];
            
        }else{
            selectedReturnLineItem.returnItemVATChargeamount=[NSDecimalNumber zero];
        }
        selectedReturnLineItem.returnItemTotalamount=[selectedReturnLineItem.returnProductQty decimalNumberByMultiplyingBy:productSelleingPriceDecimalNumber];

        selectedReturnLineItem.returnItemNetamount=[[selectedReturnLineItem.returnProductQty decimalNumberByMultiplyingBy:productSelleingPriceDecimalNumber] decimalNumberByAdding:selectedReturnLineItem.returnItemVATChargeamount];
        [self updateProductAmountLabels];


    }

}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}





- (void)calculateBonus {
    
    double bonus = 0;
    double quantity=selectedReturnLineItem.returnProductQty.doubleValue*[SWDefaults getConversionRateWithRespectToPrimaryUOMCodeForProduct:[selectedReturnLineItem.returnProduct copy]];
    
    /** checking availability of assortment bonus*/
    BOOL isAssortmentBonusPlanAvailable=NO;
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
        SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
        if([bnsManager IsAnyAssortmentBonusAvailableForTheProduct:[selectedReturnLineItem.returnProduct copy] AssortmentPlans:currentVisit.visitOptions.returnOrder.AssortmentPlansArray])
            isAssortmentBonusPlanAvailable=YES;
    }

    
    ProductBonusItem *selectedBonusItem;
    
    if(currentVisit.visitOptions.customer.isBonusAvailable && !isAssortmentBonusPlanAvailable)
     {
        
        for(NSInteger i=0 ; i<[selectedReturnLineItem.bonusDetailsArray count] ; i++ )
        {
            ProductBonusItem *bonusItem = [selectedReturnLineItem.bonusDetailsArray objectAtIndex:i];
            double rangeStart = [bonusItem.Prom_Qty_From doubleValue];
            double rangeEnd = [bonusItem.Prom_Qty_To doubleValue];
            if (quantity >= rangeStart && quantity <=rangeEnd)
            {
                
                if ([appControl.BONUS_MODE isEqualToString:@"DEFAULT"])
                {
                    if([bonusItem.Price_Break_Type_Code isEqualToString:@"RECURRING"])
                    {
                        double dividedValue = quantity / rangeStart ;
                        bonus = [bonusItem.Get_Qty integerValue] * floor(dividedValue) ;
                        selectedBonusItem=bonusItem;
                    }
                    
                    else if([bonusItem.Price_Break_Type_Code  isEqualToString:@"PERCENT"])
                    {
                        double dividedValue = [bonusItem.Get_Qty integerValue]* (quantity / rangeStart) ;
                        bonus = floor(dividedValue) ;
                        selectedBonusItem=bonusItem;
                        
                    }
                    else if([bonusItem.Price_Break_Type_Code  isEqualToString:@"POINT"])
                    {
                        bonus = [bonusItem.Get_Qty integerValue];
                        selectedBonusItem=bonusItem;
                        
                    }
                }
                else
                {
                    bonus = [bonusItem.Get_Qty integerValue] + floorf(([bonusItem.Get_Qty integerValue] - rangeStart) *[bonusItem.Get_Add_Per  floatValue]);
                    selectedBonusItem=bonusItem;
                }
                break;
            }
        }
    }
    
    if (bonus > 0)
    {
        NSPredicate *bonusProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",selectedBonusItem.Get_Item];
        NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:bonusProductPredicate]];
        
        
        if(tempProductsArray.count>0)
        {
            selectedReturnLineItem.bonusProduct=[tempProductsArray objectAtIndex:0];
            selectedReturnLineItem.defaultBonusQty=bonus;
           // selectedReturnLineItem.recievedBonusQty=bonus;
        }
    }
    else
    {
        selectedReturnLineItem.bonusProduct=nil;
        selectedReturnLineItem.defaultBonusQty=0;
       // selectedReturnLineItem.recievedBonusQty=0;
    }
}


#pragma mark Filter Button Action



- (IBAction)filterButtonTapped:(id)sender {
    isSearching=NO;
    if (productsArray.count>0) {
        [productsSearchBar setShowsCancelButton:NO animated:YES];
        productsSearchBar.text=@"";
        [productsSearchBar resignFirstResponder];
        [self.view endEditing:YES];
        [self searchProductsContent:@""];
        selectedProductIndexpath=nil;
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        // Blurred with UIImage+ImageEffects
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        
        [self.view addSubview:blurredBgImage];
        
        UIButton* searchBtn=(id)sender;
        SalesWorxProductsFilterViewController * popOverVC=[[SalesWorxProductsFilterViewController alloc]init];
        popOverVC.delegate=self;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict=previousFilteredParameters;
            
        }
        popOverVC.productsArray=productsArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 580) animated:YES];
        CGRect btnFrame=searchBtn.frame;
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        btnFrame.origin.x=btnFrame.origin.x+3;
        CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];

        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
    else{
        [self showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later"  withController:nil];
    }
    
}


-(void)filteredProducts:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    filteredProductsArray=filteredArray;
    [self updateProductTableViewOnFilter];
}
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    previousFilteredParameters=parametersDict;
}
-(void)productsFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [productsFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    filteredProductsArray=productsArray;
    [self updateProductTableViewOnFilter];
    
}
-(void)productsFilterDidClose
{
    [blurredBgImage removeFromSuperview];
    
}
-(void)updateProductTableViewOnFilter
{
    productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsArray valueForKeyPath:@"@distinctUnionOfObjects.Brand_Code"]];
    productsBrandCodesArray= [[NSMutableArray alloc]initWithArray:[productsBrandCodesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    
    
    
    /** removing invalid brandcodes*/
    //[productsBrandCodesArray removeObject:@""];
    filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
    allowSearchBarEditing=NO;
    [self showActivityIndicatorInSearchBar];
    [productsTableView reloadData];
    
    
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        /*filtering products by brand code*/
        //        for (NSString *productBrandCode in productsBrandCodesArray) {
        //            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
        //                                                @"Brand_Code=%@",productBrandCode];
        //            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[filteredProductsArray filteredArrayUsingPredicate:brandCodePredicate]];
        //            [filteredProductsArrayGroupedByBrandCode addObject:tempProductsArray];
        //        }
        filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            selectedProductIndexpath=nil;
            allowSearchBarEditing=YES;
            [self hideActivityIndicatorInSearchBar];
            if(productsBrandCodesArray.count==1)
            {
                [self ExpandFirstProductsSection];
            }
            else
            {
                ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
                
            }
            
            [productsTableView reloadData];
            
            /** if only one product is there in table view , selecting the first product*/
            /** if only one product is there in table view , selecting the first product*/
            if(filteredProductsArray.count==1 && /*&& [appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode]*/ (proSearchType==ProductSearchByPopOver || proSearchType==ProductSearchByBarCode))
            {
                [self tableView:productsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                //[orderQtyTextField becomeFirstResponder];
                proSearchType=ProductSerachNormal;

            }
        });
    });
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0); // called before text changes
{
    if(allowSearchBarEditing)
        return YES;
    else
        return NO;
    
}
#pragma mark SearchBarDelegate Methods

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if([appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode])
    {
        
        
        if(!isSearchBarClearButtonTapped)
        {
            
            NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:@""];
            NSMutableArray *popOverProductsArray;
            if(predicateArray.count>0)
            {
                NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
                NSMutableArray *tempProductsArray=[productsArray mutableCopy];
                popOverProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
            }
            else{
                popOverProductsArray=productsArray;
            }
            
            SalesWorxProductNameSearchPopOverViewController * popOverVC=[[SalesWorxProductNameSearchPopOverViewController alloc]init];
            popOverVC.popOverContentArray=popOverProductsArray;
            popOverVC.salesWorxPopOverControllerDelegate=self;
            popOverVC.disableSearch=NO;
            popOverVC.popoverType=KVisitOptionsProductNameSearchPopOverTitle;
            // popOverVC.filterText=searchBar.text;
            popOverTitle=KVisitOptionsProductNameSearchPopOverTitle;
            
            UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
            FOCPopOverController=nil;
            FOCPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
            FOCPopOverController.delegate=self;
            popOverVC.popOverController=FOCPopOverController;
            popOverVC.titleKey=KVisitOptionsProductNameSearchPopOverTitle;
            [FOCPopOverController setPopoverContentSize:CGSizeMake(300, 500) animated:YES];
            [FOCPopOverController presentPopoverFromRect:searchBar.frame inView:searchBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            [self.view resignFirstResponder];
        }
        //[self filterButtonTapped:searchBar];
        isSearchBarClearButtonTapped=NO;
        
        return NO;
        
    }
    else{
        return YES;
    }
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [productsSearchBar setShowsCancelButton:YES animated:YES];
    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode])
    {
        xSearchBarTrailingspaceToFilterButton.constant=16;
        [barcodeScanButton setHidden:YES];
    }

    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [productsSearchBar setShowsCancelButton:NO animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if([appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode])
    {
        if([searchText length] == 0) {
            // clearButton Tapped
            isSearchBarClearButtonTapped=YES;
        }
    }

    if([searchText length] != 0) {
        isSearching = YES;
        selectedProductIndexpath=nil;
        
        [self searchProductsContent:searchBar.text];
    }
    else {
        isSearching = NO;
        if (productsArray.count>0) {
            [self searchProductsContent:@""];
        }
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [productsSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    [self.view endEditing:YES];
    if (productsArray.count>0) {
        [self searchProductsContent:@""];
    }
    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode])
    {
        xSearchBarTrailingspaceToFilterButton.constant=47;
        [barcodeScanButton setHidden:NO];
    }

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(NSMutableArray *)getSearchFieldPredicateArray:(NSString *)searchString
{
    NSString* filter = @"%K CONTAINS[cd] %@";
    
    
    NSMutableArray *predicateArray=[[NSMutableArray alloc]init];
    if([[previousFilteredParameters valueForKey:@"Brand_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[previousFilteredParameters valueForKey:@"Brand_Code"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Description"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[previousFilteredParameters valueForKey:@"Description"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Item_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[previousFilteredParameters valueForKey:@"Item_Code"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Stock"] length]>0) {
        
        if ([[previousFilteredParameters valueForKey:@"Stock"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue == 0"]];
        }
    }
    if([[previousFilteredParameters valueForKey:@"Bonus"] length]>0) {
        
        if ([[previousFilteredParameters valueForKey:@"Bonus"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue == 0"]];
        }
    }
    
    NSString* promoItemStatus=[previousFilteredParameters valueForKey:@"Promo_Item"];
    
    if ([NSString isEmpty:promoItemStatus]==NO) {
        
        if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsYESCode]];
        }
        
        else if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kUnAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsNOCode]];
        }
        else
        {
            
        }
        
    }
    
    NSPredicate *BrandCodeSearchPredicate = [NSPredicate predicateWithFormat:filter, @"Brand_Code", searchString];
    NSPredicate *DescriptionSearchPredicate = [SWDefaults fetchMultipartSearchPredicate:searchString withKey:@"Description"];
    NSPredicate *BarCodeSearchPredicate = [NSPredicate predicateWithFormat:filter, @"ProductBarCode", searchString];

    NSMutableArray *searchBarPredicatesArray=[[NSMutableArray alloc]init];
    if(![searchString isEqualToString:@""])
    {
        [searchBarPredicatesArray addObject:BrandCodeSearchPredicate];
        [searchBarPredicatesArray addObject:DescriptionSearchPredicate];
        [searchBarPredicatesArray addObject:BarCodeSearchPredicate];

    }
    if(predicateArray.count>0 || searchBarPredicatesArray.count>0)
    {
        
        if(searchBarPredicatesArray.count>0)
        {
            NSPredicate *searchBarPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:searchBarPredicatesArray];
            [predicateArray addObject:searchBarPredicate];
        }
    }
    
    return predicateArray;
}

-(void)searchProductsContent:(NSString*)searchString
{
    
        NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:searchString];
        if(predicateArray.count>0)
        {
            NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
            NSMutableArray *tempProductsArray=[productsArray mutableCopy];
            filteredProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
            NSLog(@"filtered Products count %lu", (unsigned long)filteredProductsArray.count);
            if (filteredProductsArray.count>0) {
                [self updateProductTableViewOnFilter];
            }
            else if (filteredProductsArray.count==0)
            {
                isSearching=YES;
                [self updateProductTableViewOnFilter];
            }
            
        }
        else
        {
            filteredProductsArray=productsArray;
            [self updateProductTableViewOnFilter];
            
        }
        
        if(!isUpdatingReturn)
        {
            selectedProductIndexpath=nil;
            [self hideProductDetailsView];
            [self hideProductOrderDetailsView];
            [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
            [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];
        }
}
#pragma UIPopoverPresentationController DelegateMethods

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController;
{
    return NO;
}
#pragma mark UIPopOver delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController == filterPopOverController) {
        
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;

    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.titleKey=popOverString;
    if([popOverString isEqualToString:KSalesOrderUOMPopOverTitle])
    {
        popOverVC.preferredContentSize = CGSizeMake(250, 250);
        popOverVC.disableSearch=YES;
        
    }
    else{
        popOverVC.preferredContentSize = CGSizeMake(300, 300);
        popOverVC.disableSearch=NO;
    }

    popover.delegate = self;
    popOverVC.titleKey=popOverString;
    popover.sourceView = selectedTextField.rightView;
    popover.sourceRect =selectedTextField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{
        if(isKeyBoardShowing)
        {
            [UIView animateWithDuration:0.2 animations: ^{
                [self.view endEditing:YES];
    
            } completion:^(BOOL finished) {
            }];
            
        }

    }];
    
}


#pragma mark POPOVER DELEGATE METHODS
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath
{
    
}
-(void)didDismissPopOverController
{
    
}

-(void)didSelectProductNameSearchPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:@""];
    NSMutableArray *popOverProductsArray;
    if(predicateArray.count>0)
    {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        NSMutableArray *tempProductsArray=[productsArray mutableCopy];
        popOverProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    else{
        popOverProductsArray=productsArray;
    }
    
    productsSearchBar.text=[[popOverProductsArray valueForKey:@"Description"]objectAtIndex:selectedIndexPath.row];
    proSearchType=ProductSearchByPopOver;
    [self searchProductsContent:productsSearchBar.text];
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popOverTitle isEqualToString:kReturnsScreenReasonForReturnPopOverTitle]) {
        RMALotType *lotType=[returnTypes objectAtIndex:selectedIndexPath.row];
        ReasonForReturnTextfield.text=lotType.Description;
        selectedReturnLineItem.reasonForReturn=lotType.lotType;
        selectedReturnLineItem.reasonForReturnDescription=lotType.Description;
        [ReasonForReturnPopOverController dismissPopoverAnimated:YES];
        ReasonForReturnPopOverController=nil;

    }
    else if ([popOverTitle isEqualToString:KSalesOrderUOMPopOverTitle]) {
        
        
        NSMutableArray *priceListAvailableUOMsArray=[[SWDatabaseManager retrieveManager] getPriceListAvailableUOMSForSalesOrderProduct:selectedReturnLineItem.returnProduct AndCustomer:currentVisit.visitOptions.customer];
        [self updateProductAndOrderDetailsUIOnUOMSelection:[priceListAvailableUOMsArray objectAtIndex:selectedIndexPath.row]];
        
        [FOCPopOverController dismissPopoverAnimated:YES];
        FOCPopOverController=nil;
    }

    
}
-(IBAction)addToReturnsButtonTapped:(id)sender
{
    
    if(returnQtyTextField.text.length==0 || [returnQtyTextField.text doubleValue]==0)
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter quantity" withController:nil];
    }
    else if ([appControl.ENABLE_LOT_MANDATORY_RETURN isEqualToString:KAppControlsYESCode] && lotNumberTextField.text.length == 0)
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter lot number" withController:nil];
    }
    else if (invoiceTextField.text.length==0 && [appControl.ENABLE_INVOICE_ATTACHMENT_IN_RETURNS isEqualToString:KAppControlsYESCode])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select invoice" withController:nil];
    }
    else if (ReasonForReturnTextfield.text.length==0)
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select reason for return" withController:nil];
    }
    else if (selectedReturnLineItem.expiryDate.length==0)
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select expiry date" withController:nil];
    }
    else
    {
        if(isKeyBoardShowing)
        {
            [UIView animateWithDuration:0 animations: ^{
                [self.view endEditing:YES];
                
            }  completion: ^(BOOL finished) {
                
            }];
        }
        selectedReturnLineItem.recievedBonusQty=[recievingBonusTextField.text doubleValue];
        if (selectedReturnLineItem.bonusProduct==nil && selectedReturnLineItem.recievedBonusQty>0)
        {
            selectedReturnLineItem.bonusProduct=selectedReturnLineItem.returnProduct;
        }
        selectedReturnLineItem.lotNumber=lotNumberTextField.text;
        [self addOrUpdateProduct];
       
        
    }
}
-(void)addOrUpdateProduct
{
    
    if(selectedReturnLineItem.returnProductQty<=0)
    {
        [self showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please enter quantity."  withController:nil];
        return;
        
    }
    else if([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode]&&
            ![selectedReturnLineItem.returnProduct.primaryUOM isEqualToString:selectedReturnLineItem.returnProduct.selectedUOM]&&
            ([SWDefaults getConversionRateWithRespectToSelectedUOMCodeForProduct:[selectedReturnLineItem.returnProduct copy]]<1)&&
            ![self isReturnItemWithValidDecimalReturnQuantity:selectedReturnLineItem]
            )
    {
        [self showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Please enter valid quantity." withController:self];
        return;

    }

    if(isUpdatingReturn)
    {
        [returnLineItemsArray replaceObjectAtIndex:selectedReturnItemIndexpath.section withObject:selectedReturnLineItem];
    }
    else
    {
        [returnLineItemsArray insertObject:selectedReturnLineItem atIndex:0];
    }
    [self updateSalesOrderTotalAmount];
    
    
    if(isUpdatingReturn)
    {
        [self DeselectTableViewsAndUpdate];
        
    }
    else
    {
        [self clearAllProductLabelsAndTextFields];
        [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
        showReturnsTableViewNewItemAnimationColor=NO;
        [ReturnsTableView reloadData];
        [NSTimer scheduledTimerWithTimeInterval:0.1
                                         target:self
                                       selector:@selector(showOrderTableViewNewItemAddAnimation)
                                       userInfo:nil
                                        repeats:NO];
        
    }
    
}
-(void)showOrderTableViewNewItemAddAnimation
{
    [ReturnsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    SalesWorxSalesOrdersTableCellTableViewCell *Cell=[ReturnsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    Cell.lineItemRowAnimationView.layer.sublayers=nil;
    //create the initial and the final path.
    UIBezierPath *fromPath = [UIBezierPath bezierPathWithRect:CGRectMake(Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height, 1, Cell.lineItemRowAnimationView.bounds.size.height)];
    UIBezierPath *toPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height)];
    //create the shape layer that will be animated
    CAShapeLayer *fillingShape = [CAShapeLayer layer];
    fillingShape.path = fromPath.CGPath;
    fillingShape.fillColor = [UIColor colorWithRed:(30.0/255.0) green:(216.0/255.0) blue:(255.0/255.0) alpha:1.0].CGColor;
    //create the animation for the shape layer
    CABasicAnimation *animatedFill = [CABasicAnimation animationWithKeyPath:@"path"];
    animatedFill.duration = 1.0f;
    animatedFill.fromValue = (id)fromPath.CGPath;
    animatedFill.toValue = (id)toPath.CGPath;
    
    
    //create the initial and the final path.
    UIBezierPath *fromPath1 = [UIBezierPath bezierPathWithRect:CGRectMake(Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height, 1, Cell.lineItemRowAnimationView.bounds.size.height)];
    UIBezierPath *toPath1 = [UIBezierPath bezierPathWithRect:CGRectMake(Cell.lineItemRowAnimationView.bounds.size.width/2, 0,Cell.lineItemRowAnimationView.bounds.size.width/2, Cell.lineItemRowAnimationView.bounds.size.height)];
    //create the shape layer that will be animated
    CAShapeLayer *fillingShape1= [CAShapeLayer layer];
    fillingShape1.path = fromPath1.CGPath;
    fillingShape1.fillColor = [UIColor colorWithRed:(30.0/255.0) green:(216.0/255.0) blue:(255.0/255.0) alpha:1.0].CGColor;
    //create the animation for the shape layer
    CABasicAnimation *animatedFill1 = [CABasicAnimation animationWithKeyPath:@"path"];
    animatedFill1.duration = 1.0f;
    animatedFill1.fromValue = (id)fromPath1.CGPath;
    animatedFill1.toValue = (id)toPath1.CGPath;
    
    //using CATransaction like this we can set a completion block
    [CATransaction begin];
    
    [CATransaction setCompletionBlock:^{
        //when the animation ends, we want to set the proper color itself!
        Cell.lineItemRowAnimationView.backgroundColor = KNewRowAnimationColor;
        showReturnsTableViewNewItemAnimationColor=YES;
        // [ReturnsTableView reloadData];
    }];
    
    //add the animation to the shape layer, then add the shape layer as a sublayer on the view changing color
    [fillingShape addAnimation:animatedFill forKey:@"path"];
    [fillingShape1 addAnimation:animatedFill1 forKey:@"path"];
    
    [Cell.lineItemRowAnimationView.layer addSublayer:fillingShape];
    [Cell.lineItemRowAnimationView.layer addSublayer:fillingShape1];
    
    [CATransaction commit];
}

-(void)updateSalesOrderTotalAmount
{
    NSNumber* sum = [returnLineItemsArray valueForKeyPath: @"@sum.returnItemNetamount"];
    returnsTotalAmountLbl.text=[[NSString stringWithFormat:@"%@",sum] currencyString];
}
-(void)DeselectTableViewsAndUpdate
{
    [ReturnsTableView reloadData];
    selectedProductIndexpath=nil;
    [productsTableView reloadData];
    
    selectedReturnItemIndexpath=nil;
    matchedInvoicesArray = [[NSMutableArray alloc]init];
    selectedProduct=nil;
    [self hideProductDetailsView];
    [self hideProductOrderDetailsView];
    [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
    [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];

    [self updateSalesOrderTotalAmount];
}


#pragma mark expirydateMethods
-(void)showDatePicker
{
    popOverTitle=@"Expiry Date";
    SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
    popOverVC.didSelectDateDelegate=self;
    popOverVC.titleString = popOverTitle;
    popOverVC.setMaximumDateCurrentDate=NO;
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    returnPagePopOverController=nil;
    returnPagePopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    returnPagePopOverController.delegate=self;
    popOverVC.datePickerPopOverController=returnPagePopOverController;
    
    [returnPagePopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
    
    
    if(isKeyBoardShowing)
    {
        [UIView animateWithDuration:0.2 animations: ^{
            [self.view endEditing:YES];
            
        } completion:^(BOOL finished) {
        }];
        
    }
    [returnPagePopOverController presentPopoverFromRect:ExpiryDateTextField.dropdownImageView.frame inView:ExpiryDateTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}
-(void)didSelectDate:(NSString *)selectedDate
{
    //refine date
    selectedReturnLineItem.expiryDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
    NSString * refinedDatetoDisplay=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:kDateFormatWithoutTime scrString:selectedDate];
    [ExpiryDateTextField setText:refinedDatetoDisplay];
    
}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    isKeyBoardShowing=YES;
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    isKeyBoardShowing=NO;
}


-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark ClearButton Method
-(IBAction)clearButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    if(selectedProductIndexpath!=nil)/** product tableview selected*/
    {
        [self tableView:productsTableView didSelectRowAtIndexPath:selectedProductIndexpath];
    }
    else
    {
        selectedReturnItemIndexpath=nil;
        [ReturnsTableView reloadData];
        selectedProductIndexpath=nil;
        [productsTableView reloadData];
        
        selectedReturnLineItem=nil;
        matchedInvoicesArray = [[NSMutableArray alloc]init];
        selectedProduct=nil;
        [self hideProductDetailsView];
        [self hideProductOrderDetailsView];
        [self showNoSelectionViewWithMessage:KSalesOrderNoProductSelectionMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight+KSalesOrderScreenProductDetailsViewHeight+KSalesOrderScreenProductDetailsViewTopMargin+KSalesOrderScreenProductOrderDetailsViewTopMargin-KSalesOrderScreenNoSelectionsViewTopMargin];
        [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoProductSelect"]];

        [self updateSalesOrderTotalAmount];
    }
}



#pragma mark swipeTableCell Delegate methods
-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    
    if (direction == MGSwipeDirectionRightToLeft) {
        
        if(index==0)
        {
            //delete button
            NSIndexPath * path = [ReturnsTableView indexPathForCell:cell];
            [returnLineItemsArray removeObjectAtIndex:path.section];
            [UIView beginAnimations:@"myAnimationId" context:nil];
            
            [UIView setAnimationDuration:0.5]; // Set duration here
            [CATransaction begin];
            [CATransaction setCompletionBlock:^{
                NSLog(@"Complete!");
            }];
            
            [ReturnsTableView beginUpdates];
            [ReturnsTableView deleteSections:[NSIndexSet indexSetWithIndex:path.section]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [ReturnsTableView endUpdates];
            
            [CATransaction commit];
            [UIView commitAnimations];
            
            if(selectedReturnItemIndexpath==nil)
            {
                [self updateSalesOrderTotalAmount];
                [ReturnsTableView reloadData];
            }
            else
            {
                [self DeselectTableViewsAndUpdate];
            }
            return NO; //Don't autohide to improve delete expansion animation
        }
    }

return YES;
}
-(void)ExpandFirstProductsSection
{
    ProductTableViewExpandedSectionsArray =[[NSMutableArray alloc]init];
    [ProductTableViewExpandedSectionsArray addObject:@"0"];
    filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
    
    [self AddProductObjectsToGroupByArrayAtIndex:0];
}
-(void)AddProductObjectsToGroupByArrayAtIndex:(NSInteger)brandCodeLocation
{
    NSPredicate *brandCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[productsBrandCodesArray objectAtIndex:brandCodeLocation]];
    NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[filteredProductsArray filteredArrayUsingPredicate:brandCodeProductPredicate]];
    
    [filteredProductsArrayGroupedByBrandCode replaceObjectAtIndex:brandCodeLocation  withObject:tempProductsArray];
}

#pragma mark Multi_UOM
-(Products *)getUpdatedProductDetails:(Products *)product ToSelectedUOM:(ProductUOM *)proUOM
{
    NSPredicate *ProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@ && SELF.OrgID==[cd] %@",product.Item_Code,product.OrgID];
    NSMutableArray *tempProducts= [[productsArray filteredArrayUsingPredicate:ProductPredicate] mutableCopy];
    
    if(tempProducts.count==1)
    {
        Products *pro=[(Products *)[tempProducts objectAtIndex:0] copy];
        NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
        NSMutableArray *UOMarray=[product.ProductUOMArray mutableCopy];
        NSMutableArray *filtredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
        
        if(filtredArray.count==1)
        {
            ProductUOM *primaryUOMObj=[filtredArray objectAtIndex:0];
            
            pro.stock=[NSString stringWithFormat:@"%ld",(long)[[NSString stringWithFormat:@"%f",([pro.stock doubleValue]*[primaryUOMObj.Conversion doubleValue])/[proUOM.Conversion doubleValue]] integerValue]];
            pro.selectedUOM=proUOM.Item_UOM;
            pro.ProductUOMArray=[product.ProductUOMArray mutableCopy];
            return  pro;
        }
        else
        {
            return nil;
        }
        
    }
    else
    {
        return  nil;
    }
    
}

-(void)updateProductAndOrderDetailsUIOnUOMSelection:(ProductUOM *)proUOM
{
    
    [self clearAllProductLabelsAndTextFields];
    matchedInvoicesArray = [[NSMutableArray alloc]init];
    selectedProduct=[self getUpdatedProductDetails:selectedReturnLineItem.returnProduct ToSelectedUOM:proUOM];
    selectedReturnLineItem=[[ReturnsLineItem alloc]init];
    selectedReturnLineItem.returnProduct=selectedProduct;
    selectedReturnLineItem.VATChargeObj=[VATChargesManager getVATChargeForProduct:selectedProduct AndCustomer:currentVisit.visitOptions.customer];

    
    if([self fetchProductPriceListData:selectedProduct]!=nil)
    {
        selectedProduct.productPriceList=(PriceList*)[self fetchProductPriceListData:selectedProduct];
        productWholeSalePriceLabel.text=[selectedProduct.productPriceList.Unit_Selling_Price floatString] ;
        productRetailPriceLabel.text=[selectedProduct.productPriceList.Unit_List_Price floatString];
        [self showProductDetailsView];
        [self showProductOrderDetailsView];
        [self hideNoSelectionView];
    }
    else
    {
        productWholeSalePriceLabel.text=KNotApplicable;
        productRetailPriceLabel.text=KNotApplicable;
        [self showProductOrderDetailsView];
        [self hideProductOrderDetailsView];
        [self showNoSelectionViewWithMessage:KSalesOrderNoPricesAvailableErrorMessage WithHeight:KSalesOrderScreenProductOrderDetailsViewHeight];
        [NoSelectionImageView setImage:[UIImage imageNamed:@"SalesOrder_NoPriceSelect"]];
        
    }
    
    productCodeLabel.text=selectedProduct.Item_Code;
    productNameLabel.text=selectedProduct.Description;
    
    productDefaultDiscountLabel.text=[[NSString stringWithFormat:@"%0.2f", [selectedProduct.Discount doubleValue]] stringByAppendingString:@"%"];
    
    [self updateUOMTextfield];
    [self getProductSpecailDisount];
    if([selectedReturnLineItem.returnProduct.specialDiscount doubleValue]>0.0)
        productSpecialDiscountLabel.text=[NSString stringWithFormat:@"%0.2f", [selectedReturnLineItem.returnProduct.specialDiscount doubleValue]];
    else
        productSpecialDiscountLabel.text=@"N/A";
    [self getBonusDetails];
    
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
    [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *todayString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:[NSDate date]]];
    [self didSelectDate:todayString];
}

-(void)updateUOMTextfield
{
    UOMTextField.text=selectedReturnLineItem.returnProduct.selectedUOM;
    
    if([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode])
    {
        [UOMTextField setIsReadOnly:NO];
        [UOMTextField setDropDownImage:@"Arrow_DropDown"];
    }
    else
    {
        [UOMTextField setIsReadOnly:YES];
        
    }
}

#pragma mark order_quantity_decimals
-(BOOL)isReturnItemWithValidDecimalReturnQuantity:(ReturnsLineItem *)item
{
    
    
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",item.returnProduct.primaryUOM];
    NSPredicate *selecetdUOMPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",item.returnProduct.selectedUOM];
    
    NSMutableArray *UOMarray=[item.returnProduct.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    NSMutableArray *selecetdUOMFilterArray=[[UOMarray filteredArrayUsingPredicate:selecetdUOMPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    ProductUOM *selectedUOMObj=[selecetdUOMFilterArray objectAtIndex:0];
    
    NSDecimalNumber *OrderProductQtyInPrimaryUOM= [[item.returnProductQty decimalNumberByMultiplyingBy:[NSDecimalNumber ValidDecimalNumberWithString:selectedUOMObj.Conversion]]decimalNumberByDividingBy:[NSDecimalNumber ValidDecimalNumberWithString:primaryUOMObj.Conversion]];
    ;
    if([[NSString stringWithFormat:@"%0.2f",OrderProductQtyInPrimaryUOM.doubleValue] integerValue]==OrderProductQtyInPrimaryUOM.integerValue)
    {
        return YES;
    }
    else{
        return NO;
    }
    
    
    return YES;
}
#pragma mark Barcode_Scan
-(IBAction)barCodeScanButtonTapped:(id)sender
{
    
    [self.view endEditing:YES];
    SalesWorxBarCodeScannerManager *barcodeManager=[[SalesWorxBarCodeScannerManager alloc]init];
    barcodeManager.swxBCMdelegate=self;
    [barcodeManager ScanBarCode];
}

- (void)BarCodeScannerButtonBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    [reader dismissViewControllerAnimated:YES completion:^{
        
        NSLog(@"scanned bar code is %@", symbol.data);
        NSLog(@"symbol is %@", symbol.typeName);
        
        NSPredicate *barCodePredicate=[NSPredicate predicateWithFormat:@"SELF.ProductBarCode ==[cd] %@",symbol.data];
        if([[productsArray filteredArrayUsingPredicate:barCodePredicate] count]>0)
        {
            previousFilteredParameters=[[NSMutableDictionary alloc]init];
            productsSearchBar.text=symbol.data;
            proSearchType=ProductSearchByBarCode;
            [self searchProductsContent:productsSearchBar.text];
        }
        else
        {
            NSMutableArray *tempProductsArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Product where EANNO='%@'",symbol.data]];
            if(tempProductsArray.count>0)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Product with scanned barcode not available in selected warehouse/category" withController:self];
                
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"No product found" withController:self];
            }
        }
    }];
}


@end
