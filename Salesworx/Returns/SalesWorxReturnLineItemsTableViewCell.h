//
//  SalesWorxReturnLineItemsTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "MedRepView.h"
#import "MGSwipeTableCell.h"
@interface SalesWorxReturnLineItemsTableViewCell : MGSwipeTableCell

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ProductNameLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *QunatutyLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *UnitPriceLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ToatlLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *DiscountLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *NetAmountLbl;
@property (strong, nonatomic) IBOutlet MedRepView *statusView;
@property (strong, nonatomic) IBOutlet MedRepView *lineItemRowAnimationView;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *VATChargesLbl;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xVATChargeLabelWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xVATChargeLabelLeadingMarginConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xNetAmountLabelWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * xNetAmountLabelLeadingMarginConstraint;
@end
