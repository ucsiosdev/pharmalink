//
//  SalesWorxReturnsInitializationViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/15/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SalesWorxPaymentCollectionInvoicesTableViewCell.h"
#import "SalesWorxSalesOrdersTableCellTableViewCell.h"
#import "SWDefaults.h"
#import "SalesWorxTableView.h"
#import "SalesWorxSingleLineLabel.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SWDatabaseManager.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "MedRepButton.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxProductsFilterViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "OrderInitializationCollectionViewCell.h"
#import "SalesWorxReturnsViewController.h"
#import "MedRepQueries.h"
#import "AppControl.h"
#import "SalesWorxImageView.h"
#import "SalesWorxDescriptionLabel1.h"

@interface SalesWorxReturnsInitializationViewController : UIViewController
{
    IBOutlet MedRepHeader *customerNameLabel;
    IBOutlet MedRepProductCodeLabel *customerCodeLabel;
    IBOutlet UILabel *customerAvailableBalanceLabel;
    IBOutlet SalesWorxImageView *customerstatusImageView;
    NSMutableArray * tempCategoriesArray;
    NSIndexPath *selectedCategoryIndexpath;
    UIBarButtonItem *closeReturnsButton;
    UIBarButtonItem *proceedBarButtonItem;

    IBOutlet UICollectionView *categoriesCollectionView;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
}
@property (strong,nonatomic) SalesWorxVisit *currentVisit;
-(IBAction)proceedButtonTapped:(id)sender;
@end
