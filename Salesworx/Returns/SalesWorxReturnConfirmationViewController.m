//
//  SalesWorxReturnConfirmationViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxReturnConfirmationViewController.h"

@interface SalesWorxReturnConfirmationViewController ()

@end

@implementation SalesWorxReturnConfirmationViewController

@synthesize currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=UIViewBackGroundColor;
    isKeyBoardShowing=NO;
    appControl=[AppControl retrieveSingleton];
    customerSignatureTopContainerView.delegate=self;
    shipDate=@"";
}
-(void)ApplyStylesToSubviews
{
    
    /** segmentedControl styles*/
    NSDictionary *segmentControlAttributes = [NSDictionary dictionaryWithObject:KOpenSans15
                                                                         forKey:NSFontAttributeName];
    [returnTypeSegmentedControl setTitleTextAttributes:segmentControlAttributes
                                              forState:UIControlStateNormal];
    [GoodsCollectedSegmentedControl setTitleTextAttributes:segmentControlAttributes
                                                  forState:UIControlStateNormal];
    [GoodsCollectedSegmentedControl setTintColor:KSegmentedControltintColor];
    [returnTypeSegmentedControl setTintColor:KSegmentedControltintColor];
    
    [returnTypeSegmentedControl removeAllSegments];
    rmaReasonsCodesObjectsArray=[[SWDatabaseManager retrieveManager]DBGetTBLReasonCodesForPurpose:@"Returns"];
    
    for (NSString *segment in [rmaReasonsCodesObjectsArray valueForKey:@"Description"]) {
        [returnTypeSegmentedControl insertSegmentWithTitle:NSLocalizedString(segment, nil) atIndex:returnTypeSegmentedControl.numberOfSegments animated:NO];
    }

    [GoodsCollectedSegmentedControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    /** textview styles*/

}
-(void)viewDidAppear:(BOOL)animated
{
    if(self.isMovingToParentViewController)
    {
        if([currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
        {
            [customerSignatureTopContainerView setStatus:KSignatureDisabled];
            [nameTextField setIsReadOnly:YES];
        }
    }
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{

    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesReturnsConfirmationScreenName];
    }
    

    [self showCustomerDetails];
    returnLineItemsArray=currentVisit.visitOptions.returnOrder.returnLineItemsArray;
    [ReturnsTableView reloadData];
    [self orderLineItemsData];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];

    
    confirmBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Confirm",nil) style:UIBarButtonItemStylePlain target:self action:@selector(confirmButtonTapped)];
    confirmBarButtonItem.tintColor=[UIColor whiteColor];
    [confirmBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                        forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=confirmBarButtonItem;
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    
    NSMutableAttributedString* returnsAttributeString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Returns", nil),[NSString stringWithFormat:@"[ Category: %@, W/H: %@ ]",currentVisit.visitOptions.returnOrder.selectedCategory.Category,currentVisit.visitOptions.returnOrder.selectedCategory.Org_ID]]];    [returnsAttributeString addAttribute:NSForegroundColorAttributeName
                                   value:[UIColor whiteColor]
                                   range:NSMakeRange(0, returnsAttributeString.length)];
    
    [returnsAttributeString addAttribute:NSFontAttributeName
                                   value:headerTitleFont
                                   range:NSMakeRange(0, returnsAttributeString.length)];
    NSMutableAttributedString* orderDetailsAttributedString =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"[ Category: %@, W/H: %@ ]",currentVisit.visitOptions.returnOrder.selectedCategory.Category,currentVisit.visitOptions.returnOrder.selectedCategory.Org_ID]];
    [orderDetailsAttributedString addAttribute:NSForegroundColorAttributeName
                                         value:[UIColor whiteColor]
                                         range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    [orderDetailsAttributedString addAttribute:NSFontAttributeName
                                         value:kSWX_FONT_REGULAR(16)
                                         range:NSMakeRange(0, orderDetailsAttributedString.length)];
    
    
   // [returnsAttributeString appendAttributedString:orderDetailsAttributedString];
    
    label.attributedText=returnsAttributeString;
    self.navigationItem.titleView = label;

    if(self.isMovingToParentViewController)
    {
        [self ApplyStylesToSubviews];
        
        /** Hiding VAT Columns*/
        if(![appControl.ENABLE_VAT isEqualToString:KAppControlsYESCode]){
            xOrderTableHeaderNetAmountLabelLeadingMarginConstraint.constant=0;
            xOrderTableHeaderNetAmountLabelWidthConstraint.constant=0;
            xOrderTableHeaderVATChargeLabelLeadingMarginConstraint.constant=0;
            xOrderTableHeaderVATChargeLabelWidthConstraint.constant=0;
            ReturnsTableHeaderTotalLabel.textAlignment = NSTextAlignmentRight;
        }

    }
   // if ([appControl.FS_IS_RETURN_DATE_MANDATORY isEqualToString:KAppControlsYESCode]) {
        
        shipDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
        NSString * refinedDatetoDisplay=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
        [shipDateTextField setText:refinedDatetoDisplay];
        
//    } else {
//        lblShipDate.hidden = YES;
//        shipDateTextField.hidden = YES;
//        
//        SigneeNameBottomLayout.constant = 24.0f;
//        DocRefTopLayout.constant = 24.0f;
//    }
}
-(void)showCustomerDetails
{
    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    customerAvailableBalanceLabel.text=[currentVisit.visitOptions.customer.Avail_Bal currencyString];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerstatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerstatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    
}
-(void)orderLineItemsData
{
    NSNumber* Totalamount = [returnLineItemsArray valueForKeyPath: @"@sum.returnItemNetamount"];
    ReturnsTotalAmountLabel.text=[[NSString stringWithFormat:@"%@",Totalamount] floatString];
    ReturnsTotalAmountStringLabel.text=[NSString stringWithFormat:@"Total(%@):",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
    
    ReturnsTotalAmountStringLabel.text=[NSString stringWithFormat:@"%@(%@):",NSLocalizedString(@"Total", nil),[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];

}
-(void)SalesWorxSignatureViewClearButtonTapped:(UIView *)signView
{
    [customerSignatureTopContainerView setStatus:KEmptySignature];
}

-(void)SalesWorxSignatureViewSaveButtonTapped:(UIView *)signView
{
    if(customerSignatureTopContainerView.status==KEmptySignature)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please sign to save" withController:self];
    }
    else
    {
        [self.view endEditing:YES];
        [customerSignatureTopContainerView performSignatureSaveAnimation];
    }
}

-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
-(void)confirmButtonTapped
{
    [self.view endEditing:YES];
    if([appControl.IS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsNOCode] &&  nameTextField.text.length==0 && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please provide 'Signature' with 'Name'." withController:nil];
        
    }
    else if([appControl.IS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsNOCode] &&  customerSignatureTopContainerView.status==KEmptySignature && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please provide signature" withController:nil];
        
    }
    else if([appControl.IS_SIGNATURE_OPTIONAL isEqualToString:KAppControlsNOCode] && customerSignatureTopContainerView.status==KSignatureNotSaved && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please save the signature to continue" withController:nil];
        
    }
    else if(customerSignatureTopContainerView.status==KSignatureSaved && nameTextField.text.length==0 && ![currentVisit.Visit_Type isEqualToString:kTelephonicVisitTitle])
    {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please provide the signee name" withController:nil];
        
    }
    else if([returnTypeSegmentedControl selectedSegmentIndex]==UISegmentedControlNoSegment)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select a Return Type option" withController:self];

    }
    else if([GoodsCollectedSegmentedControl selectedSegmentIndex]==UISegmentedControlNoSegment)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please indicate if goods were collected" withController:self];
        
    }
    else
    {
        confirmationDetails=[[ReturnsConfirmationDetails alloc]init];
        confirmationDetails.signeeName=[customerSignatureTopContainerView.status==KSignatureSaved?nameTextField.text:@"" trimString];
        confirmationDetails.DocReferenceNumber=[DocReferanceTexField.text trimString];
        confirmationDetails.shipDate = shipDate;
        confirmationDetails.InvoicesNumbers=[InvoicesNumbersTextView.text trimString];
        confirmationDetails.comments=[commentsTextView.text trimString];
        SalesWorxReasonCode *returnTypeCode=[rmaReasonsCodesObjectsArray objectAtIndex:[returnTypeSegmentedControl selectedSegmentIndex]];
        confirmationDetails.returnType=returnTypeCode.Reasoncode;
        
        if([GoodsCollectedSegmentedControl selectedSegmentIndex]==1)
        {
            confirmationDetails.isGoodsCollected=@"NO";
        }
        else
        {
            confirmationDetails.isGoodsCollected=@"YES";
        }
        
        
        if(customerSignatureTopContainerView.status==KSignatureSaved)
            confirmationDetails.isCustomerSigned=YES;
        
        currentVisit.visitOptions.returnOrder.confrimationDetails=confirmationDetails;
        
        [self saveConfirmedReturnDetails];
    }

}


-(void)saveConfirmedReturnDetails
{
    [self saveReturnOrder:currentVisit.visitOptions.returnOrder WithcustomerDetails:currentVisit.visitOptions.customer andVisitID:currentVisit.Visit_ID];
}
-(void)saveReturnsInfo
{

    
    
}
- (void)saveReturnOrder:(SalesWorxReturn*)returnOrder WithcustomerDetails:(Customer *)customerDetails andVisitID:(NSString *)visitID{
    
    NSString *orderID = [NSString createGuid];
    NSString *lastReturnOrderReferenceStr = [SWDefaults lastReturnOrderReference];
    NSDictionary * userInfo=[SWDefaults userProfile];
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    

    
    unsigned long long lastRefId = [[lastReturnOrderReferenceStr substringFromIndex: [lastReturnOrderReferenceStr length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    
    
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    
    
    
    
  NSString *  returnOrderReferenceStr = [NSString stringWithFormat:@"M%@R%010lld", nUserId, lastRefId];
    //[SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
    
    if (![returnOrderReferenceStr isEqualToString:[SWDefaults lastReturnOrderReference]])
    {
        [SWDefaults setLastReturnOrderReference:returnOrderReferenceStr];
    }
    else
    {
        lastRefId = lastRefId + 1;
        returnOrderReferenceStr = [NSString stringWithFormat:@"M%@R%010lld", nUserId, lastRefId];
        if (![returnOrderReferenceStr isEqualToString:[SWDefaults lastReturnOrderReference]])
        {
            [SWDefaults setLastReturnOrderReference:returnOrderReferenceStr];
        }
        else
        {
            lastRefId = lastRefId + 1;
            returnOrderReferenceStr = [NSString stringWithFormat:@"M%@R%010lld", nUserId, lastRefId];
            [SWDefaults setLastReturnOrderReference:returnOrderReferenceStr];
        }
    }
    
    returnOrder=currentVisit.visitOptions.returnOrder;
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    
    NSString *dateString =  [MedRepQueries fetchDatabaseDateFormat];
    NSString *dateTimeString =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString *startTime = returnOrder.ReturnStartTime;
    

    
    NSString *InsertReturnOrderQuery;
    InsertReturnOrderQuery = @"INSERT INTO TBL_RMA (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID,Internal_Notes, Customer_Ref_No, Order_Status, Start_Time, End_Time,Last_Update_Date ,Last_Updated_By, Signee_Name, Order_Amt, Visit_ID ,Emp_Code,Credit_Customer_ID , Credit_Site_ID ,Invoice_Ref_No ,Reason_Code , Custom_Attribute_2,Custom_Attribute_4,Custom_Attribute_1,Custom_Attribute_3,Custom_Attribute_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    NSNumber* Totalamount = [returnOrder.returnLineItemsArray valueForKeyPath: @"@sum.returnItemNetamount"];
    
    NSMutableArray *insertReturnParameters=[[NSMutableArray alloc]initWithObjects:
                                           orderID,
                                           returnOrderReferenceStr,
                                           dateString,
                                           [userInfo stringForKey:@"SalesRep_ID"],
                                           customerDetails.Ship_Customer_ID ,
                                           customerDetails.Ship_Site_Use_ID ,
                                           customerDetails.Customer_ID ,
                                           customerDetails.Site_Use_ID ,
                                           returnOrder.confrimationDetails.comments,
                                           returnOrder.confrimationDetails.DocReferenceNumber,
                                           @"N",
                                           startTime,
                                           dateTimeString,
                                           dateString,
                                           [userInfo stringForKey:@"SalesRep_ID"],
                                           returnOrder.confrimationDetails.signeeName,
                                           Totalamount,
                                           visitID,
                                           [userInfo stringForKey:@"Emp_Code"],
                                           [currentVisit.visitOptions.customer.Cash_Cust isEqualToString:KAppControlsNOCode]?@"":currentVisit.visitOptions.customer.Customer_ID,
                                           [currentVisit.visitOptions.customer.Cash_Cust isEqualToString:KAppControlsNOCode]?@"":currentVisit.visitOptions.customer.Site_Use_ID,
                                           returnOrder.confrimationDetails.InvoicesNumbers,
                                           [SWDefaults getValidStringValue:returnOrder.confrimationDetails.returnType],
                                           returnOrder.selectedCategory.Category,
                                           returnOrder.confrimationDetails.isGoodsCollected,
                                           returnOrder.confrimationDetails.isCustomerSigned?@"[SIGN:Y]":@"[SIGN:N]",
                                           returnOrder.selectedCategory.Org_ID,
                                            returnOrder.confrimationDetails.shipDate,
                                           nil];
    
    
    BOOL isReturnDetailsSaved= [database executeUpdate:InsertReturnOrderQuery withArgumentsInArray:insertReturnParameters];
    
    if(isReturnDetailsSaved)
    {
        NSLog(@"Saved success");
    }
    NSArray *returnArray=returnOrder.returnLineItemsArray;
    NSInteger lineNumber=0;
    BOOL orderLineItemsSaved=NO;
    for (NSInteger i = 0; i < returnArray.count; i++)
    {
        lineNumber=lineNumber+1;
        NSInteger orderLineNumber=lineNumber;
        NSString *orderLineItemId=[NSString createGuid];
        
        ReturnsLineItem *orderItem=[returnArray objectAtIndex:i];
        NSString *OrderLineItemQuery =/* kSQLSaveOrderLineItem*/@"INSERT INTO TBL_RMA_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Return_Quantity_UOM, Display_UOM, Returned_Quantity, Inventory_Item_ID, Unit_Selling_Price, Custom_Attribute_2, Custom_Attribute_1, VAT_Amount, VAT_Percentage, VAT_Level, VAT_Code, Custom_Attribute_4, Custom_Attribute_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        
        
        NSMutableArray *insertOrderLineItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                       [NSString createGuid],
                                                       returnOrderReferenceStr,
                                                       [NSString stringWithFormat:@"%ld",(long)orderLineNumber],
                                                       orderItem.returnProduct.selectedUOM,
                                                       orderItem.returnProduct.selectedUOM,
                                                       orderItem.returnProductQty ,
                                                       orderItem.returnProduct.Inventory_Item_ID ,
                                                       orderItem.returnProduct.productPriceList.Unit_Selling_Price ,
                                                       @"N",
                                                       orderLineItemId,
                                                       orderItem.VATChargeObj!=nil?orderItem.returnItemVATChargeamount:[NSNull null],
                                                       orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCharge:[NSNull null],
                                                       orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATLevel:[NSNull null],
                                                       orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCode:[NSNull null],
                                                       orderItem.invoiceReason!=nil?orderItem.invoiceReason:[NSNull null],
                                                       orderItem.selectedInvoice.invoiceNumber!=nil?orderItem.selectedInvoice.invoiceNumber:[NSNull null],
                                                       nil];
        
        BOOL orderLineItemSaved= [database executeUpdate:OrderLineItemQuery withArgumentsInArray:insertOrderLineItemParameters];
        if(!orderLineItemSaved)
        {
            orderLineItemsSaved=NO;
            break;
        }
        else
        {
            orderLineItemsSaved=YES;
        }
        
        if(orderLineItemSaved)
        {
            if(orderItem.recievedBonusQty>0)
            {
                lineNumber=lineNumber+1;
                
                NSMutableArray *insertOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                                    [NSString createGuid],
                                                                    returnOrderReferenceStr,
                                                                    [NSString stringWithFormat:@"%ld",(long)lineNumber],
                                                                    orderItem.bonusProduct.selectedUOM,
                                                                    orderItem.bonusProduct.selectedUOM,
                                                                    [NSString stringWithFormat:@"%0.0f",orderItem.recievedBonusQty] ,
                                                                    orderItem.bonusProduct.Inventory_Item_ID ,
                                                                    @"0.00" ,
                                                                    @"F",
                                                                    orderLineItemId,
                                                                    [NSNull null],
                                                                    [NSNull null],
                                                                    [NSNull null],
                                                                    [NSNull null],
                                                                    [NSNull null],
                                                                    [NSNull null],
                                                                    nil];
                
                BOOL orderLineItemSaved= [database executeUpdate:OrderLineItemQuery withArgumentsInArray:insertOrderLineItemBonusParameters];
                if(!orderLineItemSaved)
                {
                    orderLineItemsSaved=NO;
                    break;
                }
                else
                {
                    orderLineItemsSaved=YES;
                }
                
                
            }
        }
        BOOL insertLotStatus=YES;
                if([NSString isEmpty:orderItem.lotNumber])
                {
                    orderItem.lotNumber=@"";
                }
                NSString *InsertLotQuery = /*kSQLSaveReturnLotItem */@"INSERT INTO TBL_RMA_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Inventory_Item_ID,Returned_Quantity , Expiry_Dt ,Lot_Type) VALUES (?,?,?,?,?,?,?,? )";
                NSMutableArray *InsertLotItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                         [NSString createGuid],
                                                         returnOrderReferenceStr,
                                                         [NSString stringWithFormat:@"%ld",(long)orderLineNumber],
                                                         [SWDefaults getValidStringValue:orderItem.lotNumber],
                                                         orderItem.returnProduct.Inventory_Item_ID ,
                                                         orderItem.returnProductQty ,
                                                         [SWDefaults getValidStringValue:orderItem.expiryDate] ,
                                                         orderItem.reasonForReturn,
                                                         nil];
                insertLotStatus= [database executeUpdate:InsertLotQuery withArgumentsInArray:InsertLotItemParameters];
                if(!insertLotStatus)
                {
                    orderLineItemsSaved=NO;
                    break;
                }
        
        
    }

    if(isReturnDetailsSaved && orderLineItemsSaved){
        [self saveReturnsHistory:returnOrder WithCustomerInfo:customerDetails andVisitID:visitID andOrderID:orderID andOrderRef:returnOrderReferenceStr ToDatabase:database];
    }
    else{
        [database rollback];
        [database close];
        [SWDefaults showAlertAfterHidingKeyBoard:KDBErrorAlertTitleStr andMessage:KReturnsDBErrorAlertMessageStr withController:self];
    }
    
}

- (void)saveReturnsHistory:(SalesWorxReturn*)returnOrder WithCustomerInfo:(Customer *)customerDetails  andVisitID:(NSString *)visitID andOrderID:(NSString *)OrderRowID andOrderRef:(NSString *)lastReturnReference ToDatabase:(FMDatabase*)database{
    [self.view endEditing:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDictionary *userInfo=[SWDefaults userProfile];
    
    NSString * dummyTimeStr=@" 00:00:00";
    NSString *dateString =  [MedRepQueries fetchDatabaseDateFormat] ;
    dateString=[dateString stringByAppendingString:dummyTimeStr];
    NSString *dateTimeString =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString *startTime =returnOrder.ReturnStartTime;
    
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] ;
    NSDateComponents *components=[gregCalendar components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSDate *lastDateofMonth= [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    NSString *lastDateMonthString =  [formatter stringFromDate:lastDateofMonth];
    
    
    NSArray *orderArray = returnOrder.returnLineItemsArray ;
    
    
    
    
    
    
    
    NSString *OrderHistoryInsertQuery = @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp,Custom_Attribute_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
//    if(appControl.ENABLE_REORDER_OPTION)
//    {
//        /* Reorder requires  orgid and category to fetch products data*/
//
//        OrderHistoryInsertQuery = @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp,Custom_Attribute_2,Custom_Attribute_3) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
//
//    }
    
    NSNumber* Totalamount = [returnOrder.returnLineItemsArray valueForKeyPath: @"@sum.returnItemNetamount"];
    
    NSMutableArray *insertOrderParameters=[[NSMutableArray alloc]initWithObjects:
                                           OrderRowID,
                                           lastReturnReference,
                                           @"R",
                                           dateString,
                                           [userInfo stringForKey:@"SalesRep_ID"],
                                           dateString,
                                           dateString ,
                                           returnOrder.confrimationDetails.shipDate,
                                           customerDetails.Ship_Customer_ID ,
                                           customerDetails.Ship_Site_Use_ID,
                                           customerDetails.Customer_ID,
                                           customerDetails.Site_Use_ID ,
                                           @"N",
                                           startTime,
                                           dateTimeString,
                                           [SWDefaults getValidStringValue:returnOrder.confrimationDetails.comments],
                                           [userInfo stringForKey:@"Emp_Code"],
                                           @"N",
                                           Totalamount,
                                           lastDateMonthString,
                                           [customerDetails.Cash_Cust isEqualToString:KAppControlsNOCode]?@"":customerDetails.Customer_ID,
                                           [customerDetails.Cash_Cust isEqualToString:KAppControlsNOCode]?@"":customerDetails.Site_Use_ID,
                                           currentVisit.Visit_ID,
                                           dateString,
                                           returnOrder.confrimationDetails.DocReferenceNumber,
                                           nil];
    
    
//    if(appControl.ENABLE_REORDER_OPTION)
//    {
//        [insertOrderParameters addObject:currentVisit.visitOptions.salesOrder.selectedCategory.Category];
//        [insertOrderParameters addObject:currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID];
//        
//    }
    BOOL isOrderHistoryDetailsSaved= [database executeUpdate:OrderHistoryInsertQuery withArgumentsInArray:insertOrderParameters];
    

    
    NSInteger lineNumber=0;
    BOOL orderHistoryLineItemsSaved=NO;
    for (NSInteger i = 0; i < orderArray.count; i++)
    {
        lineNumber=lineNumber+1;
        NSInteger orderLineNumber=lineNumber;
        NSString *orderHistoryLineItemId=[NSString createGuid];
        
        ReturnsLineItem *orderItem=[orderArray objectAtIndex:i];
        
        
        NSString *OrderHistoryLineItemQuery = /*kSQLSaveOrderHistoryLineItem*/@"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date,VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code,Custom_Attribute_4,Custom_Attribute_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        
        
        NSMutableArray *insertOrderLineItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                       orderHistoryLineItemId,
                                                       lastReturnReference,
                                                       [NSString stringWithFormat:@"%ld",(long)orderLineNumber],
                                                       [NSString stringWithFormat:@"%@",orderItem.returnProduct.Inventory_Item_ID] ,
                                                       orderItem.returnProduct.selectedUOM,
                                                       orderItem.returnProductQty,
                                                       @"N",
                                                       [[[orderItem.returnProduct.productPriceList.Unit_Selling_Price AmountStringWithOutComas] floatString] AmountStringWithOutComas] ,
                                                       [[[[NSString stringWithFormat:@"%@",orderItem.returnItemNetamount] AmountStringWithOutComas] floatString] AmountStringWithOutComas] ,
                                                       [SWDefaults getValidStringValue:orderItem.lotNumber],
                                                       [SWDefaults getValidStringValue:orderItem.expiryDate],
                                                       orderItem.VATChargeObj!=nil?orderItem.returnItemVATChargeamount:[NSNull null],
                                                       orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCharge:[NSNull null],
                                                       orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATLevel:[NSNull null],
                                                       orderItem.VATChargeObj!=nil?orderItem.VATChargeObj.VATCode:[NSNull null],
                                                       orderItem.invoiceReason!=nil?orderItem.invoiceReason:[NSNull null],
                                                       orderItem.selectedInvoice.invoiceNumber!=nil?orderItem.selectedInvoice.invoiceNumber:[NSNull null],
                                                       nil];
        
        BOOL orderHistoryLineItemSaved= [database executeUpdate:OrderHistoryLineItemQuery withArgumentsInArray:insertOrderLineItemParameters];
        
        if(!orderHistoryLineItemSaved)
        {
            orderHistoryLineItemsSaved=NO;
            break;
        }
        else
        {
            orderHistoryLineItemsSaved=YES;
        }
        
        if(orderItem.recievedBonusQty>0)
        {
            lineNumber=lineNumber+1;
            
            NSMutableArray *insertOrderLineItemBonusParameters=[[NSMutableArray alloc]initWithObjects:
                                                                [NSString createGuid],
                                                                lastReturnReference,
                                                                [NSString stringWithFormat:@"%ld",(long)orderLineNumber],
                                                                [NSString stringWithFormat:@"%@",orderItem.bonusProduct.Inventory_Item_ID] ,
                                                                [SWDefaults getValidStringValue:orderItem.bonusProduct.selectedUOM],
                                                                [NSString stringWithFormat:@"%0.0f",orderItem.recievedBonusQty],
                                                                @"F",
                                                                @"0" ,
                                                                @"0",
                                                                @"",
                                                                @"",
                                                                [NSNull null],
                                                                [NSNull null],
                                                                [NSNull null],
                                                                [NSNull null],
                                                                [NSNull null],
                                                                [NSNull null],
                                                                nil];
            
            
            orderHistoryLineItemSaved= [database executeUpdate:OrderHistoryLineItemQuery withArgumentsInArray:insertOrderLineItemBonusParameters];
            if(!orderHistoryLineItemSaved)
            {
                orderHistoryLineItemsSaved=NO;
                break;
            }
            
        }
      }
    if(orderHistoryLineItemsSaved && isOrderHistoryDetailsSaved)
    {
        [database commit];
        [database close];
        if (returnOrder.confrimationDetails.isCustomerSigned) {
            [self signatureSaveImage:[customerSignatureTopContainerView.signatureArea getSignatureImage] withName:[NSString stringWithFormat:@"%@.jpg",lastReturnReference] ];
        }
        
       // [SWDefaults UpdateGoogleAnalyticsforScreenName:kReturnsTakenEventName];

        [SWDefaults updateGoogleAnalyticsEvent:kReturnsTakenEventName];
        
        
        [self popToVisitOptionsScreenAfterShowingAlertWithTitle:KSuccessAlertTitleStr andMessage:KReturnDocCreateSuccessAlertMesssage];
    }
    else
    {
        [database rollback];
        [database close];
       
        [SWDefaults showAlertAfterHidingKeyBoard:KDBErrorAlertTitleStr andMessage:KReturnsDBErrorAlertMessageStr withController:self];
    }
    [MBProgressHUD hideAllHUDsForView:self.viewIfLoaded animated:YES];
}
#pragma mark saving signature methods

- (NSString*) dbGetImagesDocumentPath
{
    
    //iOS 8 support
    NSString *path;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}



-(void)popToVisitOptionsScreenAfterShowingAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kReturnsTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                        object:self
                                                      userInfo:visitOptionDict];
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle: NSLocalizedString(title, nil)
                                      message:NSLocalizedString(message, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 for (NSInteger i=0; i<self.navigationController.viewControllers.count; i++) {
                                     if( [[self.navigationController.viewControllers objectAtIndex:i] isKindOfClass:[SalesWorxVisitOptionsViewController class]])
                                     {
                                         [self.navigationController popToViewController:(SalesWorxVisitOptionsViewController*)[self.navigationController.viewControllers objectAtIndex:i] animated:YES];
                                         break;
                                     }
                                 }
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==ReturnsTableView) {
        if(section==0)
            return 0.0f;
        
        return 5.0f;
    }
    
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==ReturnsTableView) {
        
        return 44.0f;
    }
    return 0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==ReturnsTableView) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 5.0)];
        view.backgroundColor=UIViewBackGroundColor;
        return view;
    }
    else
    {
        return nil;
    }
    
}   // custom view for header. will be adjusted to default or specified header height



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return returnLineItemsArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView==ReturnsTableView)
    {
        ReturnsLineItem *returnLineItem=[returnLineItemsArray objectAtIndex:section];
        NSInteger numberOfrows=0;
        if(returnLineItem.returnProductQty>0)
            numberOfrows++;
        if(returnLineItem.recievedBonusQty>0)
            numberOfrows++;
        return numberOfrows;

    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"returnLineItemCell";
    SalesWorxReturnLineItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxReturnLineItemsTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    ReturnsLineItem *currentLineItem=[returnLineItemsArray objectAtIndex:indexPath.section];
    
    cell.lineItemRowAnimationView.backgroundColor=[UIColor clearColor];
    if(indexPath.row==0)
    {
        NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.returnProduct.selectedUOM];
        
        cell.QunatutyLbl.text=[NSString stringWithFormat:@"%@ %@",currentLineItem.returnProductQty,uomStr];
        cell.UnitPriceLbl.text=[[NSString
                                 stringWithFormat:@"%@",currentLineItem.returnProduct.productPriceList.Unit_Selling_Price] floatString];
        cell.ProductNameLbl.text=currentLineItem.returnProduct.Description;
        cell.NetAmountLbl.text=[[NSString
                                 stringWithFormat:@"%@",currentLineItem.returnItemNetamount] floatString];
        cell.ToatlLbl.text=[[NSString
                             stringWithFormat:@"%@",currentLineItem.returnItemNetamount] floatString];
        cell.DiscountLbl.text=[@"0.00" currencyString];
        
        cell.VATChargesLbl.text=currentLineItem.VATChargeObj!=nil?[[NSString
                                                                    stringWithFormat:@"%.2f",currentLineItem.VATChargeObj.VATCharge.floatValue]stringByAppendingString:@"%"]:@"0.00%" ;
        [cell.statusView setHidden:YES];
        cell.ProductNameLbl.textColor=MedRepMenuTitleFontColor;
        cell.QunatutyLbl.textColor=MedRepMenuTitleFontColor;
        cell.UnitPriceLbl.textColor=MedRepMenuTitleFontColor;
        cell.ToatlLbl.textColor=MedRepMenuTitleFontColor;
        cell.DiscountLbl.textColor=MedRepMenuTitleFontColor;
        cell.NetAmountLbl.textColor=MedRepMenuTitleFontColor;
        cell.VATChargesLbl.textColor=MedRepMenuTitleFontColor;

        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;

        
//        NSMutableArray *rightSwipeButtons=[[NSMutableArray alloc]init];
//        [rightSwipeButtons addObject:[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
//        cell.rightButtons = rightSwipeButtons;
//        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
//        cell.delegate=self;
    }
    else if(indexPath.row==1)
    {
        [cell.statusView setHidden:NO];
        
        NSString *uomStr=[NSString stringWithFormat:@"%@",currentLineItem.bonusProduct.selectedUOM];
        
        cell.QunatutyLbl.text=[NSString stringWithFormat:@"%0.0f %@",currentLineItem.recievedBonusQty,uomStr];
        cell.ProductNameLbl.text=currentLineItem.bonusProduct.Description;
        
        
        cell.UnitPriceLbl.text=[@"0.0" floatString];
        cell.NetAmountLbl.text=[@"0.0" floatString];
        cell.ToatlLbl.text=[@"0.0" floatString];
        cell.DiscountLbl.text=[@"0.0" floatString];
        cell.VATChargesLbl.text=@"0.00%";
        cell.statusView.backgroundColor=KSalesOrderBonusCellBackgroundColor;
        
        cell.ProductNameLbl.textColor=MedRepMenuTitleFontColor;
        cell.QunatutyLbl.textColor=MedRepMenuTitleFontColor;
        cell.UnitPriceLbl.textColor=MedRepMenuTitleFontColor;
        cell.ToatlLbl.textColor=MedRepMenuTitleFontColor;
        cell.DiscountLbl.textColor=MedRepMenuTitleFontColor;
        cell.NetAmountLbl.textColor=MedRepMenuTitleFontColor;
        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
        
    }
    return cell;
    
    
}
#pragma mark uitextfield methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==shipDateTextField) {
        [self showDatePicker];
        return NO;
    }
    return YES;
}

-(void)showDatePicker
{
    popOverTitle=@"Ship Date";
    SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
    popOverVC.didSelectDateDelegate=self;
    popOverVC.titleString = popOverTitle;
    popOverVC.setMaximumDateCurrentDate=NO;
    popOverVC.salesWorxDatePicker.minimumDate=[NSDate date];
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    confirmationPagePopOverController=nil;
    confirmationPagePopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    confirmationPagePopOverController.delegate=self;
    popOverVC.datePickerPopOverController=confirmationPagePopOverController;
    
    [confirmationPagePopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
    
    
    if(isKeyBoardShowing)
    {
        [UIView animateWithDuration:0.2 animations: ^{
            [self.view endEditing:YES];
            
        } completion:^(BOOL finished) {
        }];
        
    }
    [confirmationPagePopOverController presentPopoverFromRect:shipDateTextField.dropdownImageView.frame inView:shipDateTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}
-(void)didSelectDate:(NSString *)selectedDate
{
    //refine date
    shipDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
    NSString * refinedDatetoDisplay=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:kDateFormatWithoutTime scrString:selectedDate];
    [shipDateTextField setText:refinedDatetoDisplay];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString *trimmedString = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    textField.text=trimmedString;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if(textField==DocReferanceTexField)
    {
        NSString *expression = @"^[0-9a-z ]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=KReturnsScreenDocReferenceTextFieldMaximumDigitsLimit);
    }
    else if(textField==nameTextField)
    {
//        NSString *expression = @"^[0-9a-z ]*$";
//        NSError *error = nil;
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
//        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (newString.length<KReturnsOrderScreenSigneeNameTextFieldMaximumDigitsLimit);
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==DocReferanceTexField)
    {
        [shipDateTextField becomeFirstResponder];
    }
    if(textField==shipDateTextField)
    {
        [nameTextField becomeFirstResponder];
    }
    if(textField==nameTextField)
    {
        [commentsTextView becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return NO;
}

#pragma mark UITextView methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textView.text.length==0 && [text isEqualToString:@" "])
    {
        return NO;
    }
    if(textView==commentsTextView)
    {
        if([text isEqualToString:@"\n"])
        {
            [commentsTextView endEditing:YES];
            return NO;
        }
        
        return ( newString.length<=KReturnsScreenCommentsTextViewMaximumDigitsLimit);
    }
    if(textView==InvoicesNumbersTextView)
    {
        return ( newString.length<=KReturnsScreenInvoicesNumbersMaximumDigitsLimit);
    }
    
    return YES;

}



- (FMDatabase *)getDatabase
{
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    
    //    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    isKeyBoardShowing=YES;
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    isKeyBoardShowing=NO;
}


-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

@end
