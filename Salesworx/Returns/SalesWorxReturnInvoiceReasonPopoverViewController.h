//
//  SalesWorxReturnInvoiceReasonPopoverViewController.h
//  MedRep
//
//  Created by Neha Gupta on 05/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextView.h"

@protocol InvoiceReasonDelegate <NSObject>

-(void)didSaveInvoiceReason:(NSString *)reason;

@end


@interface SalesWorxReturnInvoiceReasonPopoverViewController : UIViewController
{
    id invoiceReasonDelegate;
    IBOutlet UIView * contentView;
    IBOutlet MedRepTextView *reasonTextView;
}

@property(nonatomic) id invoiceReasonDelegate;

@end
