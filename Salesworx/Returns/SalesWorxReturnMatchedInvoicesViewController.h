//
//  SalesWorxReturnMatchedInvoicesViewController.h
//  MedRep
//
//  Created by Neha Gupta on 05/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxReturnMatchedInvoicesTableViewCell.h"
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"
#import "XMLWriter.h"
#import "CJSONDeserializer.h"
#import "AFHTTPClient.h"
#import "HttpClient.h"
#import "NSDictionary+Additions.h"
#import "DataSyncManager.h"
#import "AFHTTPRequestOperation.h"
#import "NSString+SBJSON.h"
#import "MedRepQueries.h"
#import "SalesWorxReturnInvoiceReasonPopoverViewController.h"

@protocol SelectedInvoiceDelegate <NSObject>

-(void)didSelectInvoice:(SalesWorxReturnsMatchedInVoice *)inVoice andAllMatchedInvoices:(NSMutableArray *)arrMatchedInvoice;
-(void)closeInvoicePopOver:(NSMutableArray *)arrMatchedInvoice;

@end


@interface SalesWorxReturnMatchedInvoicesViewController : UIViewController
{
    IBOutlet UITableView *invoicesTableView;
    IBOutlet UIView * contentView;
    id invoiceDelegate;
}

@property(strong,nonatomic) NSMutableArray *matchedInvoicesArray;
@property(strong,nonatomic) Products *selectedProduct;
@property(strong,nonatomic) SalesWorxReturnsMatchedInVoice *previousSelectedInvoice;
@property(nonatomic) id invoiceDelegate;
@property(strong,nonatomic) NSString *customerID;
@property(strong,nonatomic) NSString *shipSiteID;

@end

