//
//  FCWGalleryCollectionDetailViewController.h
//  FalconCityofWonders
//
//  Created by Unique Computer Systems on 9/24/14.
//  Copyright (c) 2014 UCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"

@protocol ContactUsImageViewControllerDelegate <NSObject>


-(void)updateImagesArray:(NSMutableArray*)imagesArray;


@end
@interface FCWGalleryCollectionDetailViewController : UIViewController<SwipeViewDelegate, SwipeViewDataSource,UICollectionViewDelegate>
{
 NSInteger imageIndex;
   IBOutlet UIPageControl *imagePageControl;
    IBOutlet UIButton *shareButton;
    IBOutlet UIButton *closeButton;

}
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
- (IBAction)shareImage:(id)sender ;
@property (nonatomic, strong) IBOutlet SwipeView *swipeView;
@property (nonatomic,strong)NSMutableString* imageURL;
@property (nonatomic,strong) IBOutlet UILabel* imageURLLabel;
@property (nonatomic, strong) IBOutlet UIImageView *swipeItemImage;
@property (nonatomic, strong) NSMutableArray *swipImagesUrls;
@property (nonatomic, strong) NSString *selectedImageIndex;
@property (nonatomic, strong) NSString *isImageFromMainBundle;

@property (nonatomic, weak) id<ContactUsImageViewControllerDelegate> delegate;
-(IBAction)DeleteButtonTapped:(id)sender;
-(IBAction)CloseButtonTapped:(id)sender;

-(IBAction)shareButtonTapped:(id)sender;


@property (nonatomic, retain) IBOutlet UIPopoverController *poc;

@end
