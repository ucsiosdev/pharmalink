//
//  MedRepButton.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepButton.h"
#import "MedRepDefaults.h"
#import <QuartzCore/QuartzCore.h>
@implementation MedRepButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    
//    self.titleLabel. font=MedRepElementDescriptionLabelFont;
//    self.titleLabel.textColor=MedRepElementTitleLabelFontColor;
    [super awakeFromNib];
  if(self.tag==1881)
  {
      
  }
else if (self.ShadowColor==NO) {
        [self.titleLabel setFont:MedRepElementDescriptionLabelFont];
        [self.titleLabel setTextColor:[UIColor redColor]];
        
        
        self.layer.borderWidth=1.0f;
        self.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.1f;
        self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        self.layer.shadowRadius = 1.0f;
        //self.layer.shouldRasterize = YES;
    }
    
    
//    else if (self.isTextField==YES)
//        
//    {
//        self.titleLabel. font=MedRepElementDescriptionLabelFont;
//        self.titleLabel.textColor=MedRepElementDescriptionLabelColor;
//        
//        self.layer.borderWidth=1.0f;
//        self.layer.borderColor=[[UIColor redColor] CGColor];
//        self.layer.masksToBounds = NO;
//        //   self.layer.shadowColor = [UIColor blackColor].CGColor;
//        //   self.layer.shadowOpacity = 0.1f;
//        //   self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//        //   self.layer.shadowRadius = 1.0f;
//        // self.layer.shouldRasterize = YES;
//        self.layer.cornerRadius=5.0;
//
//    }
    else
    {
        //    self.titleLabel. font=MedRepElementDescriptionLabelFont;
        //    self.titleLabel.textColor=MedRepElementTitleLabelFontColor;
        
        NSLog(@"shadow color on");
        
        [self.titleLabel setFont:MedRepButtonFont];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        self.backgroundColor=[UIColor colorWithRed:(87.0/255.0) green:(190.0/255.0) blue:(135.0/255.0) alpha:1];
  
        self.layer.cornerRadius=5.0f;
        
        CALayer *bottomBorder = [CALayer layer];
        
        bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height, self.frame.size.width, 4.0f);
        
        bottomBorder.cornerRadius=5.0f;

        bottomBorder.backgroundColor = [UIColor yellowColor].CGColor;
        
        //[self.layer addSublayer:bottomBorder];
        
        
        
        
        self.layer.shadowColor = [[UIColor colorWithRed:(13.0/255.0) green:(136.0/255.0) blue:(96.0/255.0) alpha:1] CGColor];
        
        if (self.frame.size.height==40) {
            self.layer.shadowOffset = CGSizeMake(0.0, 3.0);

        }
        
        else
        {
            self.layer.shadowOffset = CGSizeMake(0.0, 3.0);

        }
        self.layer.shadowOpacity = 1.0;
        self.layer.shadowRadius = 0.0;
        
        
        /*self.layer.cornerRadius=5.0f;
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [UIColor colorWithRed:(14/255.0) green:(146/255.0) blue:(103/255.0) alpha:1].CGColor;
        self.layer.shadowOpacity = 2.0f;
        self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        self.layer.shadowRadius = 1.0f;
        //self.layer.shouldRasterize = YES;*/
    }

    if (self.isClearButton == YES) {
        self.backgroundColor = [UIColor colorWithRed:(81.0/255.0) green:(102.0/255.0) blue:(136.0/255.0) alpha:1];
        self.layer.shadowColor = [[UIColor colorWithRed:(53.0/255.0) green:(79.0/255.0) blue:(111.0/255.0) alpha:1] CGColor];
    }
 }

@end
