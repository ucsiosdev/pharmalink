//
//  SWOrderConfirmationViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/3/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "SWOrderConfirmationViewController.h"
#import "AppControl.h"
#import "Singleton.h"
#import "SWDefaults.h"
#import "AlSeerSignatureViewController.h"
#import "SWDatabaseManager.h"
#import <QuartzCore/QuartzCore.h>


@interface SWOrderConfirmationViewController ()

@end

@implementation SWOrderConfirmationViewController

@synthesize proformaOrderRef,warehouseDataArray,salesOrderDict,customerDict,itemsSalesOrder,customSignatureView,signVC,txtfldCustomerDocRef,txtFldShipDate,commentsTxtView,customerNameTxtFld,customerIDLbl,customerNameLbl,skipConsolidationLbl,skipConsolidationSegment,wholesaleOrderLbl,wholesaleOrderSegment,wholesaleNumber,skipConsolidationNumber;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Sales Order Confirmation";
    self.navigationItem.titleView = label;

    
    // Do any additional setup after loading the view from its nib.
}



-(void)viewWillAppear:(BOOL)animated
{
    
    /***if ENABLE_ORDER_CONSOLIDATION and ALLOW_SKIP_CONSOLIDATION flags are YES then show skip consolidation segment,
     if ENABLE_WHOLESALE_ORDER is YES then show wholesale order segment
     
     **/
    orderAdditionalInfoDict=[[NSMutableDictionary alloc]init];
    
    AppControl *appControl = [AppControl retrieveSingleton];
    
    ENABLE_ORDER_CONSOLIDATION = appControl.ENABLE_ORDER_CONSOLIDATION;
    ALLOW_SKIP_CONSOLIDATION = appControl.ALLOW_SKIP_CONSOLIDATION;
    checkWholesaleOrder = appControl.ENABLE_WHOLESALE_ORDER;
    if([checkWholesaleOrder isEqualToString:@"Y"])//ENABLE_WHOLESALE_ORDER
    {
        isWholeSalesOrder=YES;
        
        wholesaleOrderSegment.hidden=NO;
        wholesaleOrderLbl.hidden=NO;
        
        
    }
    else
    {
        
        wholesaleOrderSegment.hidden=YES;
        wholesaleOrderLbl.hidden=YES;
        isWholeSalesOrder = NO;
    }
    if([ENABLE_ORDER_CONSOLIDATION isEqualToString:@"Y"])//
    {
        isEOConsolidation=YES;
    }
    else
    {
        isEOConsolidation = NO;
    }
    if([ALLOW_SKIP_CONSOLIDATION isEqualToString:@"Y"])//
    {
        isASConsolidation=YES;
    }
    else
    {
        isASConsolidation = NO;
    }

    
    
    if (isEOConsolidation==YES && isASConsolidation==YES) {
        skipConsolidationSegment.hidden=NO;
        skipConsolidationLbl.hidden=NO;
    }
    
    else
    {
        skipConsolidationSegment.hidden=YES;
        skipConsolidationLbl.hidden=YES;
    }
    
    
    
    
    NSLog(@"customer dict in will appear %@", [customerDict description]);
    
    
    customerNameLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_Name"]];
    customerIDLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_ID"]];
    
    customSignatureView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    customSignatureView.layer.borderWidth = 1.0;
    
    signVC=[[AlSeerSignatureViewController alloc]init];
    
    [customSignatureView addSubview:signVC.view];
    
    
    
    txtfldCustomerDocRef.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    txtfldCustomerDocRef.layer.borderWidth = 1.0;
    
    
    
    txtFldShipDate.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    txtFldShipDate.layer.borderWidth = 1.0;
    
    
    commentsTxtView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    commentsTxtView.layer.borderWidth = 1.0;
    
    
    customerNameTxtFld.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    customerNameTxtFld.layer.borderWidth = 1.0;
    
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    txtfldCustomerDocRef.leftView=paddingView;
    txtfldCustomerDocRef.leftViewMode=UITextFieldViewModeAlways;
    
    
    UIView *paddingViewShipDate = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];

    txtFldShipDate.leftView=paddingViewShipDate;
    txtFldShipDate.leftViewMode=UITextFieldViewModeAlways;
    
    
    
    UIView *paddingViewCustName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];

    customerNameTxtFld.leftView=paddingViewCustName;
    customerNameTxtFld.leftViewMode=UITextFieldViewModeAlways;
    
    
    
    
}

-(void)cancel{
    datepicker = nil;
    [popoverControllerForDate dismissPopoverAnimated:YES];
    popoverControllerForDate = nil;
}

-(void)done{
    
    NSLog(@"dat is %@", datepicker.date);
    
    
    NSDateFormatter  *customDateFormatter = [[NSDateFormatter alloc] init];
    [customDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [customDateFormatter setDateFormat:@"dd/MM/yyyy"];
    txtFldShipDate.text = [customDateFormatter stringFromDate:datepicker.date];
    
    //[self.shipdateBtn setTitle:[customDateFormatter stringFromDate:datepicker.date] forState:UIControlStateNormal];
    
   
    
    
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString* datetoSave=[formatter stringFromDate:datepicker.date];
    NSLog(@"date to save is %@", datetoSave);
    
    [orderAdditionalInfoDict setObject:datetoSave forKey:@"ship_date"];
    
    
    datepicker = nil;
    [popoverControllerForDate dismissPopoverAnimated:YES];
    popoverControllerForDate = nil;
    //selectedDateFromPicker = datepicker.date;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)checkappControlFlags

{
    AppControl *appControl = [AppControl retrieveSingleton];
    
    ENABLE_ORDER_CONSOLIDATION = appControl.ENABLE_ORDER_CONSOLIDATION;
    ALLOW_SKIP_CONSOLIDATION = appControl.ALLOW_SKIP_CONSOLIDATION;
    checkWholesaleOrder = appControl.ENABLE_WHOLESALE_ORDER;
    
    
    
    
    
    if([checkWholesaleOrder isEqualToString:@"Y"])//ENABLE_WHOLESALE_ORDER
    {
        isWholeSalesOrder=YES;
    }
    else
    {
        isWholeSalesOrder = NO;
    }
    if([ENABLE_ORDER_CONSOLIDATION isEqualToString:@"Y"])//
    {
        isEOConsolidation=YES;
    }
    else
    {
        isEOConsolidation = NO;
    }
    if([ALLOW_SKIP_CONSOLIDATION isEqualToString:@"Y"])//
    {
        isASConsolidation=YES;
    }
    else
    {
        isASConsolidation = NO;
    }
    

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)datePickerBtnTapped:(id)sender {
    
    UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
    
    UIView *popoverView = [[UIView alloc] init];   //view
    popoverView.backgroundColor = [UIColor clearColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchDown];
    button.tintColor=[UIColor blackColor];
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    button.frame = CGRectMake(10.0, 15.0, 86, 30.0);
    [popoverView addSubview:button];
    
    UIButton *buttonDone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonDone addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchDown];
    buttonDone.tintColor=[UIColor blackColor];
    [buttonDone setTitle:@"Done" forState:UIControlStateNormal];
    buttonDone.frame = CGRectMake(230.0, 15.0, 86, 30.0);
    [popoverView addSubview:buttonDone];
    
    datepicker=[[UIDatePicker alloc]init];//Date picker
    datepicker.frame=CGRectMake(0,40,320, 216);
    
    datepicker.datePickerMode = UIDatePickerModeDate;
    [datepicker setMinimumDate:[NSDate date]];
    [datepicker setMinuteInterval:1];
    [datepicker setTag:10];
    //[datepicker addTarget:self action:@selector(result) forControlEvents:UIControlEventValueChanged];
    [popoverView addSubview:datepicker];
    
    popoverContent.view = popoverView;
    popoverControllerForDate = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    popoverControllerForDate.delegate=self;
    
    [popoverControllerForDate setPopoverContentSize:CGSizeMake(320, 275) animated:NO];
    [popoverControllerForDate presentPopoverFromRect:self.shipdateBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    

}

- (IBAction)saveButtonTapped:(id)sender
{
    Singleton *single = [Singleton retrieveSingleton];
    
    if([single.visitParentView isEqualToString:@"MO"])
    {
        
        
        
        NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
        
        
        
    }
    else
    {
        
        
        
        NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
        NSLog(@"check view controllers on the stack %@", [self.navigationController.viewControllers description]);
        
        Singleton *single = [Singleton retrieveSingleton];
        
        if ([[SWDefaults Over_Due] intValue] > 0 || [[SWDefaults availableBalance] intValue] <= 0)
        {
            
            
            
            if ([SWDefaults Over_Due].length>0 ||[SWDefaults availableBalance].length>0) {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
                
            }
            
            else
            {
                
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
            }
            
        }
        
        else
        {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
        }
        
        
    }

}
- (IBAction)confirmButtonTapped:(id)sender
{
    // include form dictionary in infodictionary
    [self.view endEditing:YES];
    
    
    
    //saving signature to documents directory
    
    
    //fetch image from defaults
    
    NSData* savedImageData=[SWDefaults fetchSignature];
    
  //  NSLog(@"check the saved image tyoe %@", savedImageData);
    
    UIImage *image ;
    
    if (savedImageData) {
        
        
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        image = [UIImage imageWithData:savedImageData scale:screenScale];
        
        
        // NSData *imageData = UIImagePNGRepresentation(savedImageData);
        [orderAdditionalInfoDict setObject:savedImageData forKey:@"signature"];
        
    }
    
    if([self validateInput])
    {
        //salesSer.delegate=self;
        //NSLog(@"performar %@",performaOrderRef);
        [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:proformaOrderRef];
        NSMutableDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[salesOrderDict objectForKey:@"info"]] ;
        [infoOrder setValue:orderAdditionalInfoDict forKey:@"extraInfo"];
        [infoOrder setValue:@"I" forKey:@"order_Status"];
        
        [salesOrderDict setValue:infoOrder forKey:@"info"];
        
        
        NSLog(@"check order additional info dict %@", orderAdditionalInfoDict);
        
        NSLog(@"sales order dict with all details %@", [salesOrderDict description]);
        
        
        
        
        
        NSLog(@"Current Visit ID = %@",[SWDefaults currentVisitID]);
        
        //salesSer.delegate=self;
        
        
        //NSLog(@"sales order being saved %@", salesOrderDict);
        
        NSString *orderRef = [[SWDatabaseManager retrieveManager] saveOrderWithCustomerInfo:customerDict andOrderInfo:salesOrderDict andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
        Singleton *single = [Singleton retrieveSingleton];
        
        
        
        NSString* attributeVal;

        
        if (wholesaleOrder)
        {
            if (wholesaleOrder==YES) {
                attributeVal=@"Y";
            }
            else
            {
                attributeVal=@"N";
            }
            
            
            [[SWDatabaseManager retrieveManager] saveDocAdditionalInfoWithDoc:orderRef andDocType:@"I" andAttName:@"WO" andAttValue:attributeVal andCust_Att_5:@""];
        }
        if (skipConsolidation)
        {
            
            if (skipConsolidation==YES) {
                attributeVal=@"Y";
            }
            
            else
            {
                attributeVal=@"N";
            }
            
            [[SWDatabaseManager retrieveManager] saveDocAdditionalInfoWithDoc:orderRef andDocType:@"I" andAttName:@"SC" andAttValue:attributeVal andCust_Att_5:@""];
        }
        

        
        
        
        
        if (image) {
            [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",orderRef] ];

        }
        
        
        
        
        if([single.visitParentView isEqualToString:@"MO"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Order Created Successfully", nil) message:[NSString stringWithFormat:@"%@: \n %@",NSLocalizedString(@"Reference No", nil),orderRef] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            alert.tag = 30;
            [alert show];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Order Created Successfully", nil) message:[NSString stringWithFormat:@"%@: \n %@. \n",NSLocalizedString(@"Reference No", nil),orderRef] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            
            
            
            
            
            
            
            
            alert.tag = 40;
            [alert show];
        }
        
        
    }
    else {
        
        
        
        if (isWholeSalesOrder==YES) {
            
            
            if (!wholesaleNumber) {
                
                
                UIAlertView* wholesaleAlert=[[UIAlertView alloc]initWithTitle:@"Missing Wholesale Type" message:@"Please choose an option from wholesale" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                wholesaleAlert.tag=999;
                alertDisplayed=YES;
                 [wholesaleAlert show];
                
                NSLog(@"wholesale segment not selected");
                
            }
            
            
        }
        
        
        if (isEOConsolidation==YES && isASConsolidation==YES) {
            
            
            if (!skipConsolidationNumber) {
                
                
                UIAlertView* wholesaleAlert=[[UIAlertView alloc]initWithTitle:@"Missing Skip Consolidation Type" message:@"Please choose an option from skip consolidation" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                alertDisplayed=YES;

                wholesaleAlert.tag=9999;
                 [wholesaleAlert show];
                NSLog(@"skip consolidation not selected");
            }
            
        }

        
        if (alertDisplayed==NO) {
            
        
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please provide 'Signature' with 'Name'." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
        }
        
    }

}


- (BOOL)validateInput
{
    // [self hideKeyboard];
    AppControl *appControl = [AppControl retrieveSingleton];
    
    isSignatureOptional = appControl.IS_SIGNATURE_OPTIONAL;
    
    
    if (isWholeSalesOrder==YES) {
        
        
        if (!wholesaleNumber) {
            
            
            UIAlertView* wholesaleAlert=[[UIAlertView alloc]initWithTitle:@"Missing Wholesale Type" message:@"Please choose an option from wholesale" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            wholesaleAlert.tag=999;
            // [wholesaleAlert show];
            
            NSLog(@"wholesale segment not selected");
            
            return NO;
        }
        
        
    }
    
    
    if (isEOConsolidation==YES && isASConsolidation==YES) {
        
        
        if (!skipConsolidationNumber) {
            
            
            UIAlertView* wholesaleAlert=[[UIAlertView alloc]initWithTitle:@"Missing Skip Consolidation Type" message:@"Please choose an option from skip consolidation" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            wholesaleAlert.tag=9999;
            // [wholesaleAlert show];
            NSLog(@"skip consolidation not selected");
            return NO;
        }
        
    }
    
    
    
    
    
    NSLog(@"is signature optional %@", appControl.IS_SIGNATURE_OPTIONAL);
    
    
    /*BOOL isOnsiteVisit=[SWDefaults fetchOnsiteVisitStatus];
     
     if (isOnsiteVisit==YES) {
     
     
     if (![orderAdditionalInfoDict objectForKey:@"name"] || ![orderAdditionalInfoDict objectForKey:@"signature"])
     {
     return NO;
     }
     
     
     }*/
    
    
    
    //if ([[SWDefaults appControl]count]==0)//IS_SIGNATURE_OPTIONAL
    if([isSignatureOptional isEqualToString:@"Y"])
    {
        if ([orderAdditionalInfoDict objectForKey:@"signature"])
        {
            if (![orderAdditionalInfoDict objectForKey:@"name"])
            {
                return NO;
            }
            else if ([[orderAdditionalInfoDict objectForKey:@"name"] length] < 1)
            {
                return NO;
            }
        }
        
        
        /** if signature optional is yes then how come we can check signature here, so commenting this out
         else if ([orderAdditionalInfoDict objectForKey:@"name"])
         {
         if (![orderAdditionalInfoDict objectForKey:@"signature"])
         {
         return NO;
         }
         else if ([[orderAdditionalInfoDict objectForKey:@"signature"] length] < 1)
         {
         return NO;
         }
         }
         **/
        
        return YES;
    }
    else
    {
        
        //        if (![orderAdditionalInfoDict objectForKey:@"name"])
        //        {
        //            return NO;
        //        }
        //        else if ([[orderAdditionalInfoDict objectForKey:@"name"] length] < 1)
        //        {
        //            return NO;
        //        }
        //
        //
        //
        //        if (![orderAdditionalInfoDict objectForKey:@"signature"])
        //        {
        //            return NO;
        //        }
        //        else if ([[orderAdditionalInfoDict objectForKey:@"signature"] length] < 1)
        //        {
        //            return NO;
        //        }
        
        
        
        if ([orderAdditionalInfoDict objectForKey:@"signature"])
        {
            if (![orderAdditionalInfoDict objectForKey:@"name"])
            {
                alertDisplayed=NO;
                
                return NO;
            }
            else if ([[orderAdditionalInfoDict objectForKey:@"name"] length] < 1)
            {
                return NO;
            }
        }
        
        else if (![orderAdditionalInfoDict objectForKey:@"signature"])
        {
            
            alertDisplayed=NO;
            return NO;
        }
        
        return YES;
    }
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    

    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==customerNameTxtFld) {
        
        [orderAdditionalInfoDict setObject:textField.text forKey:@"name"];
        
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: 0.2];
        self.view.frame =oldFrame;
        [UIView commitAnimations];
        
        
        
    }
    
    
    else if (textField==txtfldCustomerDocRef)
    {
        [orderAdditionalInfoDict setObject:textField.text forKey:@"reference"];
    }
}



- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==commentsTxtView) {
        
        [orderAdditionalInfoDict setObject:textView.text forKey:@"comments"];
        
    }
}


#pragma mark saving signature methods

- (NSString*) dbGetImagesDocumentPath
{
    
    //     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==10)
    {
        NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
    }
    else if(alertView.tag==20)
    {
        if (buttonIndex==[alertView cancelButtonIndex])
        {
            
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            NSLog(@"check view controllers on the stack %@", [self.navigationController.viewControllers description]);
            
            
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
        }
        
        else if (buttonIndex==1)
            
        {
            
        }
        else
        {
            Singleton *single = [Singleton retrieveSingleton];
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            
            if(single.isDueScreen)
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-5)] animated:YES];
            }
            else
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
            }
            
        }
    }
    
    else if (alertView.tag==30)
    {
        NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
        
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
        //[self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        //[self.navigationController  popViewControllerAnimated:YES];
        
    }
    else if(alertView.tag==40)
    {
        
        Singleton *single = [Singleton retrieveSingleton];
        
        
        if (buttonIndex==[alertView cancelButtonIndex])
        {
            
            
            BOOL duesScreen=single.isDueScreen;
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            
            if (duesScreen==YES) {
                NSLog(@"coming from dues");
                
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
            }
            else
            {
                
                
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
            }
            
        }
        else
        {
            
            
            
            NSLog(@"check view controllers on the stack %@", [self.navigationController.viewControllers description]);
            
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            
            if(single.isDueScreen)
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-5)] animated:YES];
            }
            else
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
            }
            
        }
        
    }
}


- (IBAction)wholesaleSegmentTapped:(UISegmentedControl *)sender {
    
    
    if (sender.selectedSegmentIndex==0) {
        wholesaleOrder=YES;
        wholesaleNumber=[NSNumber numberWithBool:wholesaleOrder];
        
        
        
    }
    else
    {
        wholesaleOrder=NO;
        wholesaleNumber=[NSNumber numberWithBool:wholesaleOrder];

    }
    
    
    
}

- (IBAction)skipConsolidationTapped:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex==0) {
        skipConsolidation=YES;
        
        skipConsolidationNumber=[NSNumber numberWithBool:skipConsolidation];

    }
    else
    {
        skipConsolidation=NO;
        skipConsolidationNumber=[NSNumber numberWithBool:skipConsolidation];

    }
}


#pragma mark UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==customerNameTxtFld) {
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: 0.2];
         oldFrame=self.view.frame;
        
        self.view.frame = CGRectMake(0,-100,1024,768);
        [UIView commitAnimations];
    }
}

@end
