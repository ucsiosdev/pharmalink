//
//  SalesWorxVisitOptionOrderHistoryFilterViewController.m
//  MedRep
//
//  Created by Prashannajeet kumar on 10/04/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxVisitOptionOrderHistoryFilterViewController.h"
#import "NSPredicate+Distinct.h"
#import "SWDatabaseManager.h"

@interface SalesWorxVisitOptionOrderHistoryFilterViewController ()

@end

@implementation SalesWorxVisitOptionOrderHistoryFilterViewController
@synthesize filterPopOverController, previousFilterParametersDict, selectedCustomer;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];

    if (previousFilterParametersDict.count > 0) {
        filterParametersDict = previousFilterParametersDict;
        itemNameTextField.text = [filterParametersDict valueForKey:@"Description"];
        ItemCodeTextField.text = [filterParametersDict valueForKey:@"Item_Code"];
        lpoNumberTextField.text = [filterParametersDict valueForKey:@"LPO_NO"];
        
    } else {
        filterParametersDict = [[NSMutableDictionary alloc]init];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    
    
}

#pragma mark CLose Filter

-(void)closeFilter
{
    if ([self.delegate respondsToSelector:@selector(orderHistoryFilterDidClose)]) {
        [self.delegate orderHistoryFilterDidClose];
    }
    [filterPopOverController dismissPopoverAnimated:YES];
}

-(void)clearFilter
{
    itemNameTextField.text=@"";
    ItemCodeTextField.text=@"";
    lpoNumberTextField.text=@"";
    filterParametersDict = [[NSMutableDictionary alloc]init];
}

#pragma mark UITextField Delegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == itemNameTextField)
    {
        selectedTextField=@"Item Name";
        selectedPredicateString=@"Description";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    else if (textField == ItemCodeTextField)
    {
        selectedTextField=@"Item Code";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"Item_Code"];
        selectedPredicateString=@"Item_Code";
        return NO;
    }
    else if (textField == lpoNumberTextField)
    {
        selectedTextField=@"LPO Number";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"LPO_NO"];
        selectedPredicateString=@"LPO_NO";
        return NO;
    }
    
    return YES;
}

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedTextField;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    NSMutableArray *filterDescArray=[[NSMutableArray alloc]init];
    NSMutableArray *unfilteredArray=[[NSMutableArray alloc]init];
    
    //get distinct keys
    NSPredicate * refinedPred = [NSPredicate predicateForDistinctWithProperty:predicateString];
    if ([title isEqualToString:@"LPO Number"]) {
        unfilteredArray=[[selectedCustomer.customerOrderHistoryArray filteredArrayUsingPredicate:refinedPred] mutableCopy];

    }else{
        unfilteredArray=[[selectedCustomer.customerPriceListArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    }
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:predicateString];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
    }
}

#pragma mark Selected Filter Data

-(void)selectedFilterValue:(NSString*)selectedString
{
    
    if ([selectedTextField isEqualToString:@"Item Name"]) {
        itemNameTextField.text= selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"Item Code"])
    {
        ItemCodeTextField.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    
    else if ([selectedTextField isEqualToString:@"LPO Number"])
    {
        lpoNumberTextField.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    
}


#pragma mark Button Actions

- (IBAction)resetButtonTapped:(id)sender {
    previousFilterParametersDict=[[NSMutableDictionary alloc]init];
    filterParametersDict=[[NSMutableDictionary alloc]init];
    if ([self.delegate respondsToSelector:@selector(orderHistoryFilterDidReset)]) {
        [self.delegate orderHistoryFilterDidReset];
        [self.filterPopOverController dismissPopoverAnimated:YES];
    }
}
- (IBAction)searchButtonTapped:(id)sender {
   
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filter parameters dict is %@",filterParametersDict);
    
    if([[filterParametersDict valueForKey:@"Description"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[filterParametersDict valueForKey:@"Description"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[filterParametersDict valueForKey:@"Item_Code"]]];
    }
    
    if([[filterParametersDict valueForKey:@"LPO_NO"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.LPO_NO ==[cd] %@",[filterParametersDict valueForKey:@"LPO_NO"]]];
    }
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);

    
    if (predicateArray.count==0) {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Missing Data", nil) andMessage:NSLocalizedString(@"Please select your filter criteria and try again", nil) withController:self];
        return;
    }
    
    
    // first check LPO_No exist in Order History arry or not
    
    if([[filterParametersDict valueForKey:@"LPO_NO"] length] > 0) {
        NSPredicate *LPOPredicate = [NSPredicate predicateWithFormat:@"SELF.LPO_NO ==[cd] %@",[filterParametersDict valueForKey:@"LPO_NO"]];
        NSMutableArray *filteredOrderHistoryArray = [[self.orderHistoryArray filteredArrayUsingPredicate:LPOPredicate] mutableCopy];
        
        if (filteredOrderHistoryArray.count > 0) {
            predicateArray = [[NSMutableArray alloc]init];
            if([[filterParametersDict valueForKey:@"Description"] length]>0) {
                [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[filterParametersDict valueForKey:@"Description"]]];
            }
            
            if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
                [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[filterParametersDict valueForKey:@"Item_Code"]]];
            }
            NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
            
            NSMutableArray *temp = [[NSMutableArray alloc]init];
            for (int i = 0; i <filteredOrderHistoryArray.count; i++) {
                NSMutableDictionary *currentCustOrderHistoryDict = [filteredOrderHistoryArray objectAtIndex:i];
                NSMutableArray *currentOrderHistoryLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderItems:[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"]] mutableCopy];
                
                
                // filter currentOrderHistoryLineItemsArray for Item_Name and Item_Code
                NSMutableArray *filteredOrderHistoryArray = [[currentOrderHistoryLineItemsArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
                
                if (filteredOrderHistoryArray.count > 0) {
                    [temp addObject:currentCustOrderHistoryDict];
                }
            }
            
            if (temp.count > 0) {
                if ([self.delegate respondsToSelector:@selector(filteredOrderHistoryData:WithParameterDict:)]) {
                    [self.delegate filteredOrderHistoryData:temp WithParameterDict:filterParametersDict];
                    [self.filterPopOverController dismissPopoverAnimated:YES];
                }
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
            }
            
        } else {
            [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
        }
    } else {
        predicateArray = [[NSMutableArray alloc]init];
        if([[filterParametersDict valueForKey:@"Description"] length]>0) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[filterParametersDict valueForKey:@"Description"]]];
        }
        
        if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[filterParametersDict valueForKey:@"Item_Code"]]];
        }
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        
        for (int i = 0; i <self.orderHistoryArray.count; i++) {
            NSMutableDictionary *currentCustOrderHistoryDict = [self.orderHistoryArray objectAtIndex:i];
            NSMutableArray *currentOrderHistoryLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderItems:[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"]] mutableCopy];
            
            
            // filter currentOrderHistoryLineItemsArray for Item_Name and Item_Code
            NSMutableArray *filteredOrderHistoryArray = [[currentOrderHistoryLineItemsArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
            
            if (filteredOrderHistoryArray.count > 0) {
                if ([self.delegate respondsToSelector:@selector(filteredOrderHistoryData:WithParameterDict:)]) {
                    [self.delegate filteredOrderHistoryData:[NSMutableArray arrayWithObject:currentCustOrderHistoryDict] WithParameterDict:filterParametersDict];
                    [self.filterPopOverController dismissPopoverAnimated:YES];
                }
                break;
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
            }
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
