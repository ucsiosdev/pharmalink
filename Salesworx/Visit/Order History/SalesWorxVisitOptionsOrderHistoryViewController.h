//
//  SalesWorxVisitOptionsOrderHistoryViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SWCustomerOrderHistoryTableViewCell.h"
#import "SalesWorxDropShadowView.h"
#import "SWCustomerOrderHistoryDescriptionTableViewCell.h"
#import "SWDatabaseManager.h"
#import "SalesWorxFooterTableViewCell.h"
#import "SalesWorxImageView.h"
#import "SWCustomerOrderHistoryDescriptionViewController.h"
#import "SalesWorxDescriptionLabel1.h"


@interface SalesWorxVisitOptionsOrderHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate>
{
    NSMutableArray* unfilteredOrderedLineItems;
    IBOutlet NSLayoutConstraint *orderLineItemsViewLeadingConstraint;
    NSMutableArray* unfilteredInvoiceLineItems;
    IBOutlet NSLayoutConstraint *orderLineItemsViewTrailingConstraint;
    IBOutlet NSLayoutConstraint *orderLineItemsViewWidthConstraint;
    NSString* orderStatus;
    IBOutlet MedRepElementTitleDescriptionLabel *totalOrdersLbl;
    IBOutlet MedRepElementTitleDescriptionLabel *statusLbl;
    IBOutlet MedRepElementTitleDescriptionLabel *totalOrdersValueLbl;
    BOOL hideDividerImageView;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    IBOutlet UIButton *filterButtonTapped;
    UIPopoverController *filterPopOverController;
    NSMutableDictionary * previousOrderHistoryFilterParameters;
    NSMutableArray* filteredOrderHistoryArray;
    Customer * selectedCustomer;

    BOOL isSearching;
}
@property (strong, nonatomic) IBOutlet UISegmentedControl *lineItemsSegment;
@property (strong, nonatomic) IBOutlet MedRepHeader *customerNameLbl;
@property (strong, nonatomic) IBOutlet MedRepProductCodeLabel *customerNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *availableBalanceLbl;
@property(strong,nonatomic) SalesWorxVisit * currentVisit;
@property (strong, nonatomic) IBOutlet UITableView *orderHistoryTableView;
@property (strong, nonatomic) IBOutlet  SalesWorxImageView *statusImageView;
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *orderHistoryLineItemsView;
@property (strong, nonatomic) IBOutlet UITableView *orderhistoryLineItemsTableView;
- (IBAction)orderHistoryLineItemsViewBackButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *orderNumberLbl;
- (IBAction)lineItemSegmentTapped:(id)sender;

@end
