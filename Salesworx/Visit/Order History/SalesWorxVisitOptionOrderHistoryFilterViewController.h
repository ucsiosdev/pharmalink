//
//  SalesWorxVisitOptionOrderHistoryFilterViewController.h
//  MedRep
//
//  Created by Prashannajeet kumar on 10/04/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepButton.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CustomerPriceListDelegate <NSObject>

-(void)orderHistoryFilterDidCancel;
-(void)orderHistoryFilterDidClose;
-(void)orderHistoryFilterDidReset;
-(void)filteredOrderHistoryData:(NSMutableArray *)orderHistorydata WithParameterDict:(NSMutableDictionary *)ParametersDictionary;

@end

@interface SalesWorxVisitOptionOrderHistoryFilterViewController : UIViewController{
    id delegate;
    IBOutlet MedRepTextField *itemNameTextField;
    IBOutlet MedRepTextField *ItemCodeTextField;
    IBOutlet MedRepTextField *lpoNumberTextField;
    NSMutableDictionary *filterParametersDict;
    
    NSString* selectedTextField;    
    NSString* selectedPredicateString;
}
@property (strong,nonatomic) UIPopoverController* filterPopOverController;
@property (nonatomic) id delegate;
@property (strong, nonatomic) IBOutlet MedRepButton *resetButtonTapped;
@property (strong, nonatomic) IBOutlet MedRepButton *searchButtonTapped;
@property (strong, nonatomic) UINavigationController *filterNavController;
@property (strong, nonatomic) NSString *filterTitle;
@property (strong, nonatomic) Customer *selectedCustomer;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@property(strong,nonatomic) NSMutableArray *orderHistoryArray;


@end

NS_ASSUME_NONNULL_END
