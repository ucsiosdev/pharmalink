//
//  SalesWorxVisitOptionsOrderHistoryViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxVisitOptionsOrderHistoryViewController.h"
#import "NSString+Additions.h"
#import "SalesWorxVisitOptionOrderHistoryFilterViewController.h"
@interface SalesWorxVisitOptionsOrderHistoryViewController ()

@end

@implementation SalesWorxVisitOptionsOrderHistoryViewController
@synthesize currentVisit,customerNameLbl,customerNumberLbl,availableBalanceLbl,orderHistoryTableView,statusImageView,orderHistoryLineItemsView,orderhistoryLineItemsTableView,lineItemsSegment,orderNumberLbl;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=UIViewBackGroundColor;
    // Do any additional setup after loading the view from its nib.
    customerNameLbl.text=[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Customer_Name];
    customerNumberLbl.text=[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Customer_No];
    availableBalanceLbl.text=[[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Avail_Bal] currencyString];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [statusImageView setBackgroundColor:CustomerActiveColor];
    }
    else
    {
        [statusImageView setBackgroundColor:[UIColor redColor]];
    }

    
   // NSLog(@"order history is %@", currentVisit.visitOptions.customer.customerOrderHistoryArray);
//    orderHistoryTableView.layer.borderColor = UITextViewBorderColor.CGColor;
//    orderHistoryTableView.layer.borderWidth = 2.0;
//    orderHistoryTableView.layer.cornerRadius = 1.0;
    
//    orderhistoryLineItemsTableView=[SWDefaults fetchUITableViewAttributes:orderhistoryLineItemsTableView];
   
//    orderhistoryLineItemsTableView.layer.borderColor = UITextViewBorderColor.CGColor;
//    orderhistoryLineItemsTableView.layer.borderWidth = 2.0;
//    orderhistoryLineItemsTableView.layer.cornerRadius = 1.0;
    

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Order History"];
    orderHistoryLineItemsView.frame = CGRectMake(1022, 131, 1008, 629);

    
    hideDividerImageView=YES;
    
    [filterButtonTapped setImage:[UIImage imageNamed:@"Customer_FilterEmpty"] forState:UIControlStateNormal];

    isSearching = false;
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    //currentVisit=[[SWDatabaseManager retrieveManager]fetchRecentOrdersforCustomer:currentVisit];
    NSMutableArray * tempCustomerOrderHistoryArray=[[[SWDatabaseManager retrieveManager] dbGetRecentOrdersForCustomer:currentVisit.visitOptions.customer] mutableCopy];
    currentVisit.visitOptions.customer.customerOrderHistoryArray=tempCustomerOrderHistoryArray;
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesOrderHistoryScreenName];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    NSLog(@"current visit order history count %lu", (unsigned long)currentVisit.visitOptions.customer.customerOrderHistoryArray.count);
    
    
    
    @try {
       currentVisit.visitOptions.customer.totalOrderAmount=[[NSString stringWithFormat:@"%@",[[currentVisit.visitOptions.customer.customerOrderHistoryArray valueForKey:@"Transaction_Amt"] valueForKeyPath:@"@sum.self"]] doubleValue];
    }@catch (id e) {
        // Ignored
        NSLog(@"Exception transaction amout sum is %@",e);
        currentVisit.visitOptions.customer.totalOrderAmount=0;
    }

    
    
    totalOrdersLbl.text=[NSString stringWithFormat:@"%lu",(unsigned long)currentVisit.visitOptions.customer.customerOrderHistoryArray.count];
    totalOrdersValueLbl.text=[[NSString stringWithFormat:@"%0.2f",currentVisit.visitOptions.customer.totalOrderAmount] currencyString];
    NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kOrderHistoryTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                        object:self
                                                      userInfo:visitOptionDict];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==orderHistoryTableView) {
        if (isSearching) {
            return filteredOrderHistoryArray.count;
        } else {
            return currentVisit.visitOptions.customer.customerOrderHistoryArray.count;
        }
    }
    
    else if (tableView==orderhistoryLineItemsTableView)
    {
        return currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray.count;
    }
    else{
        return 0;
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    static NSString* identifier=@"footerCell";
    SalesWorxFooterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFooterTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    cell.footerLbl.textColor=[UIColor whiteColor];
    cell.footerLbl.text=[NSString stringWithFormat:@"Order Status: %@",orderStatus];
    
    return cell;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==orderHistoryTableView) {
        static NSString* identifier=@"customerOrderHistoryCell";
        SWCustomerOrderHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell=nil;

        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        
        cell.isHeader=YES;
        cell.dividerImageView.hidden=YES;
        
        cell.docRefNumberLbl.isHeader=YES;
        cell.orderStatusLbl.isHeader=YES;
        cell.orderDateLbl.isHeader=YES;
        cell.orderValueLbl.isHeader=YES;
        
        
        cell.docRefNumberLbl.text=@"Order No";
        cell.orderStatusLbl.text=@"Order Status";
        cell.orderDateLbl.text=@"Order Date";
        cell.orderValueLbl.text=@"Order Value";
        return cell;
    }
    else if (tableView==orderhistoryLineItemsTableView)
    {
        static NSString* identifier=@"orderHistoryDescription";
        SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell=nil;
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        cell.dividerImageView.hidden=YES;

        cell.isHeader=YES;
        cell.itemCodeLbl.isHeader=YES;
        cell.descriptionLbl.isHeader=YES;
        cell.itemCodeLbl.isHeader=YES;
        cell.qtyLbl.isHeader=YES;
        cell.totalLbl.isHeader=YES;
        cell.unitPriceLbl.isHeader=YES;
        
        
        cell.itemCodeLbl.text=@"Item Code";
        cell.descriptionLbl.text=@"Description";
        cell.qtyLbl.text=@"Qty";
        cell.unitPriceLbl.text=@"Unit Price";
        cell.totalLbl.text=@"Total";
        
        return cell;
    }
    else{
        return nil;
    }
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (hideDividerImageView==YES) {
        return 45.0f;
    }
    else{
    return 49.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView==orderhistoryLineItemsTableView) {
        return 0.0f;
 
    }
    else
    {
        return 0.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (tableView==orderHistoryTableView)
    {
        //        Customer * currentCust;
        //        if (isSearching==YES) {
        //            currentCust=[filteredCustomers objectAtIndex:indexPath.row];
        //        }
        //        else
        //        {
        //            currentCust=[customersArray objectAtIndex:indexPath.row];
        //        }
        static NSString* identifier=@"customerOrderHistoryCell";
        SWCustomerOrderHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        //cell.dividerImageView.hidden=YES;
        
        cell.hideDividerImage=YES;
        
    
        
        
       // CustomerOrderHistory * currentCustomerOrderHistory=[currentVisit.visitOptions.customer.customerOrderHistoryArray objectAtIndex:indexPath.row];
        
        CustomerOrderHistory * currentCustOrderHistoryDict;
        if (isSearching) {
            currentCustOrderHistoryDict=[filteredOrderHistoryArray objectAtIndex:indexPath.row];
        } else {
            currentCustOrderHistoryDict=[currentVisit.visitOptions.customer.customerOrderHistoryArray  objectAtIndex:indexPath.row];
        }
        
        CustomerOrderHistory* currentCustomerOrderHistory=[[CustomerOrderHistory alloc]init];
        currentCustomerOrderHistory.Creation_Date=[currentCustOrderHistoryDict valueForKey:@"Creation_Date"];
        currentCustomerOrderHistory.Customer_Name=[currentCustOrderHistoryDict valueForKey:@"Customer_Name"];
        currentCustomerOrderHistory.Shipping_Instructions=[currentCustOrderHistoryDict valueForKey:@"Shipping_Instructions"];
        currentCustomerOrderHistory.ERP_Status=[currentCustOrderHistoryDict valueForKey:@"ERP_Status"];
        currentCustomerOrderHistory.FSR=[currentCustOrderHistoryDict valueForKey:@"FSR"];
        currentCustomerOrderHistory.ERP_Ref_Number=[currentCustOrderHistoryDict valueForKey:@"ERP_Ref_No"];
        currentCustomerOrderHistory.Orig_Sys_Document_Ref=[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"];
        currentCustomerOrderHistory.Transaction_Amt=[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"];
        currentCustomerOrderHistory.End_Time=[currentCustOrderHistoryDict valueForKey:@"End_Time"];
        

        
        
        cell.docRefNumberLbl.text=[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Orig_Sys_Document_Ref];
        cell.orderStatusLbl.text=[[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Shipping_Instructions]isEqualToString:@"N/A"]?@"New":[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Shipping_Instructions];
        cell.orderDateLbl.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.End_Time]];
        cell.orderValueLbl.text=[[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Transaction_Amt] currencyString];
        return cell;
    }
    else if (tableView==orderhistoryLineItemsTableView)
    {
        static NSString* identifier=@"orderHistoryDescription";
        SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        orderhistoryLineItemsTableView.separatorColor=kUITableViewSaperatorColor;
        cell.dividerImageView.hidden=YES;
        CustomerOrderHistoryLineItems * currentCustomerHistoryLineItems=[currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray objectAtIndex:indexPath.row];
        cell.itemCodeLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Item_Code];
        cell.descriptionLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Description];
        cell.qtyLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Ordered_Quantity];
        cell.unitPriceLbl.text=[[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Unit_Selling_Price] currencyString];
        double totalPrice=[currentCustomerHistoryLineItems.Item_Value doubleValue];
        cell.totalLbl.text=[[NSString stringWithFormat:@"%f", totalPrice] currencyString];
        return cell;
    }
    
    else{
        return nil;
    }
   

}


-(void)fetchOrderLineItems:(CustomerOrderHistory*)orderHistory
{
    
   
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
//    [UIView animateWithDuration:0.3 animations:^{
//        orderHistoryLineItemsView.frame = CGRectMake(8, 131, 1008, 629);
//    } completion:^(BOOL finished) {
//        NSLog(@"customer order history line items before reloading data %@", currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray);
//        
//        //[orderhistoryLineItemsTableView reloadData];
//    }];
    

//    orderHistoryLineItemsView=[[SalesWorxDropShadowView alloc]initWithFrame:CGRectMake(1024, 131, 1008, 629)];
    
    CustomerOrderHistory * currentCustOrderHistoryDict;
    if (isSearching) {
        currentCustOrderHistoryDict=[filteredOrderHistoryArray objectAtIndex:indexPath.row];
    } else {
        currentCustOrderHistoryDict=[currentVisit.visitOptions.customer.customerOrderHistoryArray  objectAtIndex:indexPath.row];
    }
    
    
    CustomerOrderHistory* currentCustomerOrderHistory=[[CustomerOrderHistory alloc]init];
    currentCustomerOrderHistory.Creation_Date=[currentCustOrderHistoryDict valueForKey:@"Creation_Date"];
    currentCustomerOrderHistory.Customer_Name=[currentCustOrderHistoryDict valueForKey:@"Customer_Name"];
    currentCustomerOrderHistory.Shipping_Instructions=[currentCustOrderHistoryDict valueForKey:@"Shipping_Instructions"];
    currentCustomerOrderHistory.ERP_Status=[currentCustOrderHistoryDict valueForKey:@"ERP_Status"];
    currentCustomerOrderHistory.FSR=[currentCustOrderHistoryDict valueForKey:@"FSR"];
    currentCustomerOrderHistory.ERP_Ref_Number=[currentCustOrderHistoryDict valueForKey:@"ERP_Ref_No"];
    currentCustomerOrderHistory.Orig_Sys_Document_Ref=[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"];
    currentCustomerOrderHistory.Transaction_Amt=[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"];
    currentCustomerOrderHistory.End_Time=[currentCustOrderHistoryDict valueForKey:@"End_Time"];

    
    CustomerOrderHistory * orderHistory=currentCustomerOrderHistory;
    
    orderStatus= orderHistory.Shipping_Instructions;
    
    
    lineItemsSegment.selectedSegmentIndex=0;
    
    
    NSMutableArray * currentOrderHistoryLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderItems:orderHistory.Orig_Sys_Document_Ref] mutableCopy];
    
    unfilteredOrderedLineItems=[[NSMutableArray alloc]init];
    
    
    if (currentOrderHistoryLineItemsArray.count>0) {
        
        currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc] init];
        
        for (NSMutableDictionary * currentLineItemDict in currentOrderHistoryLineItemsArray) {
            
            CustomerOrderHistoryLineItems * custOrderHistoryLineItems=[[CustomerOrderHistoryLineItems alloc]init];
            custOrderHistoryLineItems.Calc_Price_Flag=[currentLineItemDict valueForKey:@"Calc_Price_Flag"];
            custOrderHistoryLineItems.Description=[currentLineItemDict valueForKey:@"Description"];
            custOrderHistoryLineItems.Inventory_Item_ID=[currentLineItemDict valueForKey:@"Inventory_Item_ID"];
            custOrderHistoryLineItems.Item_Code=[currentLineItemDict valueForKey:@"Item_Code"];
            custOrderHistoryLineItems.Order_Quantity_UOM=[currentLineItemDict valueForKey:@"Order_Quantity_UOM"];
            custOrderHistoryLineItems.Ordered_Quantity=[currentLineItemDict valueForKey:@"Ordered_Quantity"];
            custOrderHistoryLineItems.Unit_Selling_Price=[currentLineItemDict valueForKey:@"Unit_Selling_Price"];
            custOrderHistoryLineItems.Item_Value=[currentLineItemDict valueForKey:@"Item_Value"];
            
            [currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray addObject:custOrderHistoryLineItems];
            
            [unfilteredOrderedLineItems addObject:custOrderHistoryLineItems];
            
        }
        
    }
    
    NSLog(@"check count %lu",(unsigned long)currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray.count);
    
    
    
    NSMutableArray* invoicedLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderInvoice:orderHistory.Orig_Sys_Document_Ref] mutableCopy];
    NSLog(@"invoices line items are %@", invoicedLineItemsArray);
    
    
    unfilteredInvoiceLineItems=[[NSMutableArray alloc]init];
    
    
    currentVisit.visitOptions.customer.customerOrderHistoryInvoiceLineItemsArray=[[NSMutableArray alloc]init];
    
    for (NSMutableDictionary * tempDict in invoicedLineItemsArray) {
        
        NSMutableDictionary*  invoiceItemsDict=[NSMutableArray nullFreeDictionaryWithDictionary:tempDict];
        
        CustomerOrderHistoryInvoiceLineItems * invoiceLineItems=[[CustomerOrderHistoryInvoiceLineItems alloc]init];
        invoiceLineItems.Calc_Price_Flag=[invoiceItemsDict valueForKey:@"Calc_Price_Flag"];
        invoiceLineItems.Description=[invoiceItemsDict valueForKey:@"Description"];
        invoiceLineItems.Expiry_Date=[invoiceItemsDict valueForKey:@"Expiry_Date"];
        invoiceLineItems.Inventory_Item_ID=[invoiceItemsDict valueForKey:@"Inventory_Item_ID"];
        invoiceLineItems.Item_Code=[invoiceItemsDict valueForKey:@"Item_Code"];
        invoiceLineItems.Item_Value=[invoiceItemsDict valueForKey:@"Item_Value"];
        invoiceLineItems.Order_Quantity_UOM=[invoiceItemsDict valueForKey:@"Order_Quantity_UOM"];
        invoiceLineItems.Ordered_Quantity=[invoiceItemsDict valueForKey:@"Ordered_Quantity"];
        invoiceLineItems.Unit_Selling_Price=[invoiceItemsDict valueForKey:@"Unit_Selling_Price"];
        invoiceLineItems.ERP_Ref_No=[invoiceItemsDict valueForKey:@"ERP_Ref_No"];
        
        
        
        NSString* creationDate=[[SWDatabaseManager retrieveManager]fetchCreationDateforInvoice:invoiceLineItems.ERP_Ref_No];
        
        NSLog(@"creation Date is %@", creationDate);
        
        invoiceLineItems.Order_Date=creationDate;
        
        NSLog(@"line item count %lu",(unsigned long)currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray.count);
        
        [currentVisit.visitOptions.customer.customerOrderHistoryInvoiceLineItemsArray addObject:invoiceLineItems];
        
        [unfilteredInvoiceLineItems addObject:invoiceLineItems];
        
        
    }
    
    orderNumberLbl.text=[NSString stringWithFormat:@"Order No: %@",orderHistory.Orig_Sys_Document_Ref];
    
    NSLog(@"order history view frame is %@", NSStringFromCGRect(orderHistoryLineItemsView.frame ));
    statusLbl.text=[NSString stringWithFormat:@"%@",orderStatus];

    
    
    
    SWCustomerOrderHistoryDescriptionViewController * orderHistoryDescVC=[[SWCustomerOrderHistoryDescriptionViewController alloc]init];
    orderHistoryDescVC.orderLineItems=unfilteredOrderedLineItems;
    orderHistoryDescVC.invoiceLineItems=unfilteredInvoiceLineItems;
    orderHistoryDescVC.status=orderStatus;
    orderHistoryDescVC.orderNumber=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",orderHistory.Orig_Sys_Document_Ref]];
    orderHistoryDescVC.selectedCustomer=currentVisit.visitOptions.customer;
    orderHistoryDescVC.view.backgroundColor = KPopUpsBackGroundColor;
    orderHistoryDescVC.modalPresentationStyle = UIModalPresentationCustom;
    [self.navigationController presentViewController:orderHistoryDescVC animated:NO completion:nil];
    
    
    
    
//    [UIView animateWithDuration:0.3 animations:^{
//        
//        orderLineItemsViewLeadingConstraint.constant=8.0f;
//        orderLineItemsViewTrailingConstraint.constant=8.0f;
//        [self.view layoutIfNeeded];
//        statusLbl.text=[NSString stringWithFormat:@"%@",orderStatus];
//        NSLog(@"order history view frame after animation %@", NSStringFromCGRect(orderHistoryLineItemsView.frame ));
//        
//
//        
//    } completion:^(BOOL finished) {
//        NSLog(@"customer order history line items before reloading data %@", currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray);
//        
//        [orderhistoryLineItemsTableView reloadData];
//    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)orderHistoryLineItemsViewBackButtonTapped:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        orderLineItemsViewLeadingConstraint.constant=1025.0f;
        [self.view layoutIfNeeded];

    } completion:^(BOOL finished) {
    }];
}
- (IBAction)lineItemSegmentTapped:(id)sender {
    
    UISegmentedControl *lineItemsSegmentController=(id)sender;
    
    if (lineItemsSegmentController.selectedSegmentIndex==1) {
        
        NSLog(@"invoiced items tapped ");
        
        if (unfilteredInvoiceLineItems.count>0) {
            
            currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray=unfilteredInvoiceLineItems;
            
            [orderhistoryLineItemsTableView reloadData];
        }
        else
        {
            currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc]init];
            [orderhistoryLineItemsTableView reloadData];
        }
    }
    
    else if (lineItemsSegmentController.selectedSegmentIndex==0)
        
    {
        if (unfilteredOrderedLineItems.count>0) {
            
            currentVisit.visitOptions.customer.customerOrderHistoryLineItemsArray=unfilteredOrderedLineItems;
            [orderhistoryLineItemsTableView reloadData];
            
            
        }
    }
    
}
- (IBAction)filterButtonTapped:(id)sender {
    
    if (currentVisit.visitOptions.customer.customerOrderHistoryArray.count>0) {
        
        SalesWorxVisitOptionOrderHistoryFilterViewController * popOverVC=[[SalesWorxVisitOptionOrderHistoryFilterViewController alloc]init];
        popOverVC.delegate=self;
        
        UIButton* searchBtn=(id)sender;
        if (previousOrderHistoryFilterParameters.count>0) {
            popOverVC.previousFilterParametersDict=previousOrderHistoryFilterParameters;
        }
        popOverVC.orderHistoryArray = currentVisit.visitOptions.customer.customerOrderHistoryArray;
        popOverVC.selectedCustomer = currentVisit.visitOptions.customer;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController = nil;
        filterPopOverController = [[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate = self;
        popOverVC.filterPopOverController = filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 430) animated:YES];
        CGRect btnFrame=searchBtn.frame;
        popOverVC.previousFilterParametersDict = previousOrderHistoryFilterParameters;
        btnFrame.origin.x=btnFrame.origin.x+3;
        CGRect currentFrame=CGRectMake(695, 8, 32, 32);
        
        
        
        CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];
        
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Data", nil) message:NSLocalizedString(@"Please try later", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [unavailableAlert show];
    }
}

-(void)orderHistoryFilterDidReset
{
    isSearching = false;
    previousOrderHistoryFilterParameters = [[NSMutableDictionary alloc]init];
    filteredOrderHistoryArray = selectedCustomer.customerOrderHistoryArray;
    [filterButtonTapped setImage:[UIImage imageNamed:@"Customer_FilterEmpty"] forState:UIControlStateNormal];
    [orderHistoryTableView reloadData];
    totalOrdersLbl.text=[NSString stringWithFormat:@"%lu",(unsigned long)currentVisit.visitOptions.customer.customerOrderHistoryArray.count];
    totalOrdersValueLbl.text=[[NSString stringWithFormat:@"%0.2f",currentVisit.visitOptions.customer.totalOrderAmount] currencyString];
}

-(void)filteredOrderHistoryData:(NSMutableArray *)orderHistorydata WithParameterDict:(NSMutableDictionary *)ParametersDictionary {
    isSearching = true;
    
    previousOrderHistoryFilterParameters = ParametersDictionary;
    filteredOrderHistoryArray = orderHistorydata;
    [filterButtonTapped setImage:[UIImage imageNamed:@"Customer_FilterFilled"] forState:UIControlStateNormal];
    [orderHistoryTableView reloadData];
    totalOrdersLbl.text=[NSString stringWithFormat:@"%lu",(unsigned long)filteredOrderHistoryArray.count];
    
    double transactionAmount = 0.0;
    for (NSMutableDictionary *dicObj in filteredOrderHistoryArray) {
        
        transactionAmount = transactionAmount + [[dicObj valueForKey:@"Transaction_Amt"]doubleValue];
        totalOrdersValueLbl.text = [[NSString stringWithFormat:@"%0.2f",transactionAmount] currencyString];
    }
}

@end
