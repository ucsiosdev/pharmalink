//
//  SalesWorxFooterTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxFooterTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *footerLbl;

@end
