//
//  SalesWorxCreateNotePopoverViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/14/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextView.h"
#import "SalesWorxDropShadowView.h"
#import "MedRepQueries.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@protocol VisitOptionsNotesDelegate

-(void)didSaveNote;
-(void)cancelPopover;

@end
@interface SalesWorxCreateNotePopoverViewController : SalesWorxPresentControllerBaseViewController
{    
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *saveButton;
    id notesDelegate;
}
@property (strong, nonatomic) IBOutlet MedRepTextView *notesTxtView;
@property (strong, nonatomic) NSString *visitText;

@property(nonatomic)  id notesDelegate;
- (IBAction)SaveButtontapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@property(strong,nonatomic) SalesWorxVisit *currentVisit;

@end
