//
//  SalesWorxVisitOptionsContactDetailsPopOverViewController.m
//  MedRep
//
//  Created by Unique Computer Systems on 1/22/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxVisitOptionsContactDetailsPopOverViewController.h"

@interface SalesWorxVisitOptionsContactDetailsPopOverViewController ()

@end

@implementation SalesWorxVisitOptionsContactDetailsPopOverViewController
@synthesize notesTxtView,currentVisit,visitText,contactNameTextField,contactNumberTextField,contactDetailsDelegate;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    topContentView.backgroundColor=UIViewControllerBackGroundColor;

    
    NSMutableDictionary* contactDetailsDict = [[SWDatabaseManager retrieveManager]fetchFieldSalesVisitContactDetailsForCustomerinCurrentVisit:currentVisit];
    
    NSString* contactName=[NSString getValidStringValue:[contactDetailsDict valueForKey:@"Contact_Name"]];
    
    NSString* contactNumber=[NSString getValidStringValue:[contactDetailsDict valueForKey:@"Contact_Number"]];
    
    contactNameTextField.text=[NSString isEmpty:contactName]?@"":contactName;
    
    contactNumberTextField.text=[NSString isEmpty:contactNumber]?@"":contactNumber;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if(self.isMovingToParentViewController)
    {
        notesTxtView.backgroundColor=[UIColor whiteColor];
        
        [saveButton.titleLabel setFont:NavigationBarButtonItemFont];
        [cancelButton.titleLabel setFont:NavigationBarButtonItemFont];
    }
    notesTxtView.text=visitText;
    
   

    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    oldFrame=self.view.bounds;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message: NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    BOOL iskeyboardShowing = false;
    if([contactNumberTextField isFirstResponder] ||
       [contactNameTextField isFirstResponder]){
        iskeyboardShowing = YES;
    }
    [self.view endEditing:YES];
    topContentView.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, iskeyboardShowing?0.3 * NSEC_PER_SEC:0), dispatch_get_main_queue(), ^{
        [self dimissViewControllerWithSlideDownAnimation:YES];
    });
        
    
        
}

- (void) animateTextField: (MedRepTextField*)textFld Viewup:(BOOL) up
{
    NSInteger movementDistance=110;
    const float movementDuration = 0.3f; // tweak as needed
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:contactNameTextField Viewup:YES];

}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self animateTextField:contactNameTextField Viewup:NO];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == contactNameTextField){
        [contactNumberTextField becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if( [string length] == 0){
        if([textField.text length] != 0){
            return YES;
        }
        else {
            return NO;
        }
    }
    
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    
    if(textField==contactNameTextField){
        NSString *expression = @"^[a-zA-Z ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<50);
    }else if(textField == contactNumberTextField){
        NSString *expression = @"^\\+?[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<14);
    }
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveButtonTapped:(id)sender {
    
    BOOL iskeyboardShowing;
    if([contactNumberTextField isFirstResponder] ||
       [contactNameTextField isFirstResponder]){
        iskeyboardShowing = YES;
    }
    [self.view endEditing:YES];
    
    if(contactNumberTextField.text.length != 0 &&
       contactNumberTextField.text.length < 8){
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Plase enter valid contact number" withController:self];
    }else{
        topContentView.userInteractionEnabled = NO;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, iskeyboardShowing?0.3 * NSEC_PER_SEC:0), dispatch_get_main_queue(), ^{

        currentVisit.visitOptions.contact=[[FieldSalesContact alloc]init];
        
        NSString* contactName=[NSString getValidStringValue:contactNameTextField.text];
        
        NSString* contactNumber=[NSString getValidStringValue:contactNumberTextField.text];
        currentVisit.visitOptions.contact.contactName=contactName;
        currentVisit.visitOptions.contact.contactNumber=contactNumber;

        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if ([self.contactDetailsDelegate respondsToSelector:@selector(didSaveContactDetails:)]) {
                [self.contactDetailsDelegate didSaveContactDetails:currentVisit];
            }
        });
        [self dimissViewControllerWithSlideDownAnimation:YES];
        });

    }

}
@end
