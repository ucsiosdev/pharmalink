//
//  SalesWorxCashCustomerOnsiteVisitOptionsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/5/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+MJPopupViewController.h"

#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxCustomClass.h"
#import "MedRepElementTitleLabel.h"
#import "SWDatabaseManager.h"
#import "NSString+Additions.h"
#import "SWAppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "SalesWorxCustomerOnsiteVisitOptionsReasonViewController.h"
#import "MedRepMultiCallsCloseVisitReasonCodeViewController.h"
#import "MedRepTextField.h"
#import "SalesWorxServersListViewController.h"
#import "SalesWorxiBeaconManager.h"

@protocol CashCustomerOnsiteVisitOptionsDelegate <NSObject>

-(void)cashCustomerSelectedVisitOption:(NSString*)visitOption customer:(SalesWorxVisit*)currentVisit;




@end

@interface SalesWorxCashCustomerOnsiteVisitOptionsViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,SalesWorxServersListViewControllerDelegate>
{
    IBOutlet UIView *accompaniedByView;
    IBOutlet NSLayoutConstraint *accompaniedByViewHeightConstraint;
    IBOutlet NSLayoutConstraint *accompaniedByViewTopConstraint;
    IBOutlet NSLayoutConstraint *accompaniedByViewBottomConstraint;
    IBOutlet MedRepTextField *accompaniedByTextField;

    IBOutlet NSLayoutConstraint *cashCustomerDetailsViewBottomConstraint;

    BOOL beaconValidationStatus;
    BOOL validateBeacon ;
    
    IBOutlet UIView *cashCustomerDetailsView;
    
    IBOutlet NSLayoutConstraint *cashCustDetailsViewTopConstraint;
    id delegate;
    IBOutlet SalesWorxDropShadowView *customerDetailsContainerView;
    
    IBOutlet UIView *titleView;

    IBOutlet MedRepElementTitleLabel *phoneTitleLbl;

    IBOutlet MedRepElementTitleLabel *nameTitleLbl;
    IBOutlet NSLayoutConstraint *detailsViewHeightConstraint;
    NSString* selectedSegmentTitle;
    BOOL saveTapped;
    CGRect oldFrame;
    UIAlertController * alertController;
    NSMutableDictionary * cashCustomerDetailsDictionary;
    
    IBOutlet UIButton *cancelButton;
    IBOutlet MedRepTextView *addressTxtView;
    
    IBOutlet UIButton *saveButton;
    
    BOOL isCustomerLocationValid;
    
    AppControl * appControl;
}

@property(strong,nonatomic) IBOutlet MedRepTextField *nameTxtFld;
@property(strong,nonatomic) IBOutlet MedRepTextField *phoneTxtFld;
@property(strong,nonatomic) IBOutlet MedRepTextField *addressTxtFld;
@property(strong,nonatomic) IBOutlet MedRepTextField *contactTxtFld;
@property(strong,nonatomic) IBOutlet MedRepTextField *faxTxtFld;
@property(strong,nonatomic) SalesWorxVisit * currentVisit;
@property(strong,nonatomic) UINavigationController * navController;
- (IBAction)backgroundTapped:(id)sender;

- (IBAction)segmentSwitch:(UISegmentedControl *)sender ;
- (IBAction)saveButtonTapped:(id)sender;

@property (nonatomic, assign) id delegate;
- (IBAction)cancelButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
-(void)displayAlertWithTitle:(NSString*)Title andMessage:(NSString*)message withButtonTitles:(NSArray*)buttonTitlesArray andTag:(NSInteger)alertTag;
@end

