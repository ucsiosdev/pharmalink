//
//  SalesWorxCustomerDuesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCustomerDuesViewController.h"
#import "SWDatabaseManager.h"
#import "MedRepDefaults.h"
#import "SalesWorxCustomerDuesTableViewCell.h"
#import "ProductSaleCategoryViewController.h"
@interface SalesWorxCustomerDuesViewController ()

@end

@implementation SalesWorxCustomerDuesViewController
@synthesize currentVisit,customerDuesArray,duesTableView,customerNameLbl,customerNumberLbl,availableBalanceLbl,totalDueLbl,pdcDueLbl,creditLimitLbl,creditPeriodLbl;
- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Customer Dues"];
    //self.navigationItem.title = NSLocalizedString(back, <#comment#>);
    footerNoteLbl.text=NSLocalizedString(@"Note: All new orders for this customer will be held for approval on the backoffice", nil);
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)checkAppControlFlags
{
        AppControl*  appControl=[AppControl retrieveSingleton];
         NSMutableArray*  tempCategoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:currentVisit];
    
    //in case of bin sina there is only one category there are no warehouses in that case save org id as w/h
    NSMutableArray * OrderTempleteArray=[[NSMutableArray alloc]init];
    
    if([appControl.ENABLE_TEMPLATES isEqualToString:KAppControlsYESCode])
    {
        OrderTempleteArray=[[SWDatabaseManager retrieveManager]fetchOrderTempletesForCustomer:currentVisit.visitOptions.customer];
        
    }

    if (/*[appControl.ENABLE_MULTI_CAT_TRX isEqualToString:@"N"] &&*/
        ![appControl.ENABLE_FOC_ORDER isEqualToString:KAppControlsYESCode] &&
        (![appControl.ENABLE_TEMPLATES isEqualToString:KAppControlsYESCode] || OrderTempleteArray.count==0) &&
        ![appControl.ENABLE_RECOMMENDED_ORDER isEqualToString:KAppControlsYESCode] &&
        ![appControl.ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode] &&
        tempCategoriesArray.count==1) {
        NSLog(@"temp categories array is %@", tempCategoriesArray);
        
        
        currentVisit.visitOptions.salesOrder.selectedCategory=[tempCategoriesArray objectAtIndex:0];
        
        currentVisit.visitOptions.salesOrder.selectedOrderType=kDefaultOrderPopOverTitle;
        
        NSLog(@"check cat org id,cat and desc %@ %@ %@", currentVisit.visitOptions.salesOrder.selectedCategory.Org_ID,currentVisit.visitOptions.salesOrder.selectedCategory.Category,currentVisit.visitOptions.salesOrder.selectedCategory.Description);
        
        SalesWorxSalesOrderViewController* salesOrderVC=[[SalesWorxSalesOrderViewController alloc]init];
        salesOrderVC.currentVisit=currentVisit;
        currentVisit.visitOptions.salesOrder.OrderStartTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        [self.navigationController pushViewController:salesOrderVC animated:YES];
        
    }
    
    else
    {
        SalesWorxOrderInitializationViewController* salesOrderInitilizationVC=[[SalesWorxOrderInitializationViewController alloc]init];
        salesOrderInitilizationVC.currentVisit=currentVisit;
        [self.navigationController pushViewController:salesOrderInitilizationVC animated:YES];
        
        
    }
  

    
    
}

-(void)startSalesOrder
{
    
    /*ProductSaleCategoryViewController *productSaleCategoryViewController = [[ProductSaleCategoryViewController alloc] initWithCustomer:[SWDefaults fetchCurrentCustDict:currentVisit.visitOptions.customer]] ;
    [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];*/
    
    
    
    [self checkAppControlFlags];
   
    

}
-(void)becomeActive
{
    [duesTableView reloadData];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    

    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
   
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesCustomerDuesScreenName];
    }
    
    
    UIBarButtonItem* salesOrderButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Continue", nil) style:UIBarButtonItemStylePlain target:self action:@selector(startSalesOrder)];
    [salesOrderButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=salesOrderButton;
    
    customerNumberLbl.text=currentVisit.visitOptions.customer.Customer_No;
    customerNameLbl.text=currentVisit.visitOptions.customer.Customer_Name;
    availableBalanceLbl.text=[[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Avail_Bal] currencyString];
    creditLimitLbl.text=[[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Credit_Limit ] currencyString];
    creditPeriodLbl.text=[NSString stringWithFormat:@"%@ Days",currentVisit.visitOptions.customer.Bill_Credit_Period];
    
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);

    
    
    customerDuesArray=[[SWDatabaseManager retrieveManager] fetchDuesForCustomer:currentVisit.visitOptions.customer];
    
    NSLog(@"test");
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerStatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerStatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    if (customerDuesArray.count>0) {
         duesDict=[customerDuesArray objectAtIndex:0];
        currentVisit.visitOptions.customer.Total_Due=[duesDict valueForKey:@"Total_Due"];
        currentVisit.visitOptions.customer.PDC_Due=[duesDict valueForKey:@"PDC_Due"];
        CustomerDues * customerDues=[[CustomerDues alloc]init];
        customerDues.duesDictionary=[NSMutableDictionary dictionaryWithDictionary:duesDict];
        currentVisit.visitOptions.customer.customerDues=customerDues;
        NSLog(@"customer dues dict is %@",currentVisit.visitOptions.customer.customerDues.duesDictionary);
        
        totalDueLbl.text=[[NSString stringWithFormat:@"%@",[duesDict valueForKey:@"Total_Due"]] currencyString];
        pdcDueLbl.text=[[NSString stringWithFormat:@"%@",[duesDict valueForKey:@"PDC_Due"]] currencyString];
        duesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [duesTableView reloadData];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString* identifier=@"duesCell";
    SalesWorxCustomerDuesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerDuesTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.isHeader=YES;
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.titleLbl.isHeader=YES;
    cell.descLbl.isHeader=YES;
    cell.titleLbl.RTLSupport=YES;
    cell.descLbl.RTLSupport=YES;
    cell.titleLbl.text= @"Period";
    cell.descLbl.text=@"Outstanding";
   
    return cell;
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (customerDuesArray.count>0) {
        return 7;
    }
    else{
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"duesCell";
    SalesWorxCustomerDuesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerDuesTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSMutableArray * prevoiusMonthsArray=[[NSMutableArray alloc]init];
    
    BOOL ageingReportDisplayFormatMonthly=[SWDefaults isAgeingReportDisplayFormatMonthly];
    if (ageingReportDisplayFormatMonthly) {
        prevoiusMonthsArray=[SWDefaults fetchPreviousMonthsforAgeingReport];
    }
    
        if(indexPath.row==0)
        {
            if (ageingReportDisplayFormatMonthly) {
                [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:0]];
            }
            else{
                [cell.titleLbl setText:NSLocalizedString(@"30 Days", nil)];
            }
            [cell.descLbl setText:[duesDict currencyStringForKey:@"M0_Due"]];
        }
        else  if(indexPath.row==1)
        {
            if (ageingReportDisplayFormatMonthly) {
                [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:1]];
            }
            else{
                [cell.titleLbl setText:NSLocalizedString(@"60 Days", nil)];
            }
            [cell.descLbl setText:[duesDict currencyStringForKey:@"M1_Due"]];
        }
        else  if(indexPath.row==2)
        {
            if (ageingReportDisplayFormatMonthly) {
                [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:2]];
            }
            else{
                [cell.titleLbl setText:NSLocalizedString(@"90 Days", nil)];
            }
            [cell.descLbl setText:[duesDict currencyStringForKey:@"M2_Due"]];
        }
        else  if(indexPath.row==3)
        {
            if (ageingReportDisplayFormatMonthly) {
                [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:3]];
            }
            else{
                [cell.titleLbl setText:NSLocalizedString(@"120 Days", nil)];
            }
            [cell.descLbl setText:[duesDict currencyStringForKey:@"M3_Due"]];
        }
        else  if(indexPath.row==4)
        {
            if (ageingReportDisplayFormatMonthly) {
                [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:4]];
            }
            else{
                [cell.titleLbl setText:NSLocalizedString(@"150 Days", nil)];
            }
            [cell.descLbl setText:[duesDict currencyStringForKey:@"M4_Due"]];
        }
        else if (indexPath.row==5)
        {
            if (ageingReportDisplayFormatMonthly) {
                [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:5]];
            }
            else{
                [cell.titleLbl setText:NSLocalizedString(@"180 Days", nil)];
            }
            [cell.descLbl setText:[duesDict currencyStringForKey:@"M5_Due"]];

        }
        else  if(indexPath.row==6)
        {
            [cell.titleLbl setText:NSLocalizedString(@"Prior Due", nil)];
            [cell.descLbl setText:[duesDict currencyStringForKey:@"Prior_Due"]];
        }
    return cell;
    
    }

@end
