//
//  SalesWorxCustomerDuesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxOrderInitializationViewController.h"
#import "SalesWorxDescriptionLabel1.h"

@interface SalesWorxCustomerDuesViewController : UIViewController
{
    NSMutableDictionary* duesDict;
    IBOutlet SalesWorxImageView *customerStatusImageView;
    IBOutlet UILabel *footerNoteLbl;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
}
@property (strong, nonatomic) IBOutlet MedRepHeader *customerNameLbl;
@property (strong, nonatomic) IBOutlet MedRepProductCodeLabel *customerNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *availableBalanceLbl;
@property(strong,nonatomic) SalesWorxVisit* currentVisit;
@property(strong,nonatomic) NSMutableArray* customerDuesArray;
@property (strong, nonatomic) IBOutlet UITableView *duesTableView;
@property (strong, nonatomic) IBOutlet UILabel *totalDueLbl;
@property (strong, nonatomic) IBOutlet UILabel *pdcDueLbl;
@property (strong, nonatomic) IBOutlet UILabel *creditPeriodLbl;
@property (strong, nonatomic) IBOutlet UILabel *creditLimitLbl;

@end
