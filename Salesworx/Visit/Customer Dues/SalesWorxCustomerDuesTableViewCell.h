//
//  SalesWorxCustomerDuesTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineDarkColorLabel.h"
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxCustomerDuesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *titleLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *descLbl;
@property(nonatomic) BOOL isHeader;

@end
