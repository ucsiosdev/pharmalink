//
//  SalesWorxCreateNotePopoverViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/14/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxCreateNotePopoverViewController.h"
#import "FMDBHelper.h"

@interface SalesWorxCreateNotePopoverViewController ()

@end

@implementation SalesWorxCreateNotePopoverViewController
@synthesize notesTxtView,currentVisit,visitText;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.view.backgroundColor=UIViewControllerBackGroundColor;
    topContentView.backgroundColor=UIViewControllerBackGroundColor;
}
-(void)viewWillAppear:(BOOL)animated
{
    if(self.isMovingToParentViewController)
    {
        notesTxtView.backgroundColor=[UIColor whiteColor];
        
        [saveButton.titleLabel setFont:NavigationBarButtonItemFont];
        [cancelButton.titleLabel setFont:NavigationBarButtonItemFont];
    }
    notesTxtView.text=visitText;
}

-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message: NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

#pragma mark buttopnActions

- (IBAction)SaveButtontapped:(id)sender {
    
    BOOL iskeyboardShowing = false;
    if([notesTxtView isFirstResponder]){
        iskeyboardShowing = YES;
    }
    [self.view endEditing:YES];
    
    if (notesTxtView.text.length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter note" withController:self];
    } else {
        topContentView.userInteractionEnabled = NO;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, iskeyboardShowing?0.3 * NSEC_PER_SEC:0), dispatch_get_main_queue(), ^{
                        
            BOOL status = NO;
            
            @try {
                //iOS 8 support
                NSString *documentDir;
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                    documentDir=[SWDefaults applicationDocumentsDirectory];
                }else{
                    NSArray *paths = NSSearchPathForDirectoriesInDomains
                    (NSDocumentDirectory, NSUserDomainMask, YES);
                    documentDir = [paths objectAtIndex:0];
                }
                FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
                
                [database open];
                [database beginTransaction];
                
                NSString *notesUpdateQuery = @"Update TBL_FSR_Actual_Visits SET Text_Notes=? Where Actual_Visit_ID=?";
                
                NSMutableArray *notesParameters=[[NSMutableArray alloc]initWithObjects:
                                                 [[[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Visit_Note] trimString] length]==0?[NSNull null]:currentVisit.visitOptions.customer.Visit_Note,
                                                 currentVisit.Visit_ID,
                                                 nil];
                
                status = [database executeUpdate:notesUpdateQuery withArgumentsInArray:notesParameters];
                
                if(status)
                {
                    [database commit];
                    [database close];
                }
                else
                {
                    [database rollback];
                    [database close];
                }
            } @catch (NSException *exception) {
                NSLog(@"Exception while saving notes - %@",exception);
            }
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                if (status == YES) {
                    if ([self.notesDelegate respondsToSelector:@selector(didSaveNote)]) {
                        [self.notesDelegate didSaveNote];
                    }
                }
            });
            [self dimissViewControllerWithSlideDownAnimation:YES];
        });
        
    }
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    BOOL iskeyboardShowing = false;
    if([notesTxtView isFirstResponder]){
        iskeyboardShowing = YES;
    }
    [self.view endEditing:YES];
    topContentView.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, iskeyboardShowing?0.3 * NSEC_PER_SEC:0), dispatch_get_main_queue(), ^{
        if ([self.notesDelegate respondsToSelector:@selector(cancelPopover)]) {
            [self.notesDelegate cancelPopover];
        }
        [self dimissViewControllerWithSlideDownAnimation:YES];
    });
}

#pragma mark UITextViewMethods

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    currentVisit.visitOptions.customer.Visit_Note = trimmedString;
    [self animateTextField:textView up:NO];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextField:textView up:YES];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    NSLog(@"Note textview length %d", newString.length);
    
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }

    if([newString isEqualToString:@" "] || [newString isEqualToString:@"\n"])
    {
        return NO;
    }
    
    if(textView == notesTxtView)
    {
        return (newString.length<=NotesTextViewLimit);
    }
    
    return YES;
}

- (void) animateTextField: (UITextView*) textView up: (BOOL) up
{
    NSInteger movementDistance=50;
    if(textView==notesTxtView)
    {
        movementDistance=130;
    }
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
