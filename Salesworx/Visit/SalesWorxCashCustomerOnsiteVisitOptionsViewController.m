//
//  SalesWorxCashCustomerOnsiteVisitOptionsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/5/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCashCustomerOnsiteVisitOptionsViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "Singleton.h"
@interface SalesWorxCashCustomerOnsiteVisitOptionsViewController ()

@end

@implementation SalesWorxCashCustomerOnsiteVisitOptionsViewController
@synthesize segmentControl,nameTxtFld,phoneTxtFld,addressTxtFld,contactTxtFld,faxTxtFld,currentVisit;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"cash customer onsite class called");
    nameTxtFld.delegate=self;
    phoneTxtFld.delegate=self;
    addressTxtFld.delegate=self;
    contactTxtFld.delegate=self;
    faxTxtFld.delegate=self;
    addressTxtView.delegate=self;
    
   
    
    
    NSLog(@"cash customer onsite visit options called");
    
    
    cashCustomerDetailsView.layer.cornerRadius=4.0f;
    cashCustomerDetailsView.layer.shadowColor = [UIColor blackColor].CGColor;
    cashCustomerDetailsView.layer.shadowOffset = CGSizeMake(2, 2);
    cashCustomerDetailsView.layer.shadowOpacity = 0.1;
    cashCustomerDetailsView.layer.shadowRadius = 1.0;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:titleView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(4.0, 4.0)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = titleView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    titleView.layer.mask = maskLayer;
    
    UIFont *font = MedRepSegmentControlFont;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    
    
    [segmentControl setTitleTextAttributes:attributes
                               forState:UIControlStateNormal];
    
    
    NSLog(@"cash cust details %@",currentVisit.visitOptions.customer.Cash_Cust);
    
    NSLog(@"check planned visit in visit type %hhd",currentVisit.isPlannedVisit);
   
    
//    
//    
    cashCustomerDetailsView.backgroundColor=UIViewControllerBackGroundColor;
    cashCustomerDetailsView.hidden=YES;
    [segmentControl setTitle:kOnsiteVisitTitle forSegmentAtIndex:0];
    [segmentControl setTitle:kTelephonicVisitTitle forSegmentAtIndex:1];
    
    [segmentControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    oldFrame=self.view.frame;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    // Do any additional setup after loading the view from its nib.
    
     validateBeacon = YES;

}




-(void)hideAccompaniedBy:(BOOL)flag
{
    
    
    if (flag==YES) {
        accompaniedByViewHeightConstraint.constant=0.0;
        accompaniedByViewTopConstraint.constant=0.0;
        accompaniedByViewBottomConstraint.constant=8.0;
        accompaniedByView.hidden=YES;
        accompaniedByTextField.text=@"";
        currentVisit.Accompanied_By_User_Name=@"";
        currentVisit.Accompanied_By_User_ID=@"";
        
    }
    else{
        accompaniedByView.hidden=NO;
        accompaniedByViewHeightConstraint.constant=77;
        accompaniedByViewTopConstraint.constant=8.0;
        accompaniedByViewBottomConstraint.constant=8.0;

    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    
     beaconValidationStatus = [self validateFSRLocationwithiBeacon];

    
    if (self.isMovingToParentViewController) {
    }

    
    [cancelButton.titleLabel setFont:NavigationBarButtonItemFont];
    saveButton.titleLabel.font=NavigationBarButtonItemFont;
    
    cashCustomerDetailsDictionary=[[NSMutableDictionary alloc]init];
  
    if (self.isMovingToParentViewController) {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        [cashCustomerDetailsView setHidden:NO];
    }
    cashCustomerDetailsView.layer.cornerRadius=5.0f;
   
    self.view.backgroundColor=KPopUpsBackGroundColor;

    
    NSString * accompaniedByFlag=[[AppControl retrieveSingleton] ENABLE_FS_ACCOMPANIED_BY];
    //accompaniedByFlag=KAppControlsNOCode;
    
    if ([accompaniedByFlag isEqualToString:KAppControlsYESCode]) {
        [self hideAccompaniedBy:NO];
    }
    else{
        [self hideAccompaniedBy:YES];
    }


    if ([currentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"N"]) {
        //this is not a cash customer, hide cash customer container view
        customerDetailsContainerView.hidden=YES;
        detailsViewHeightConstraint.constant=0.0;
        cashCustDetailsViewTopConstraint.constant=0.0;
        cashCustomerDetailsViewBottomConstraint.constant=0.0;
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    if ([currentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"N"]) {
        //this is not a cash customer, hide cash customer container view
        customerDetailsContainerView.hidden=YES;
        detailsViewHeightConstraint.constant=0.0;
        cashCustDetailsViewTopConstraint.constant=0.0;
        cashCustomerDetailsViewBottomConstraint.constant=0.0;
    }
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = cashCustomerDetailsView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backgroundTapped:(id)sender {
    
    [self closeTapped];
}

- (BOOL)location:(CLLocation *)location isNearCoordinate:(CLLocationCoordinate2D)coordinate withRadius:(CLLocationDistance)radius
{
    CLCircularRegion *circularRegion = [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius identifier:@"radiusCheck"];
    
    return [circularRegion containsCoordinate:coordinate];
}


-(void)validateLocationRangeforOnSiteVisit
{
    NSString* customerLatitude=[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Cust_Lat];
    NSString* customerLongitude=[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Cust_Long];
    
    NSLog(@"customer lat in validate %@ long %@", customerLatitude,customerLongitude);
    
    NSLog(@"current customer is %@", currentVisit.Visit_ID);
    
    
    //check if customer location is available
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString* fsrLatitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude] ;
    NSString* fsrLongitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude] ;
    
    if ([NSString isEmpty:customerLatitude]==NO && [NSString isEmpty:customerLongitude]==NO && [NSString isEmpty:fsrLatitude]==NO && [NSString isEmpty:fsrLongitude]==NO ) {
        
        NSLog(@"customer location and fsr location available to validate for onsite visit \ncustomer latitude %@: \n customer longitude :%@\n fsr latitude : %@\n fsr longitude : %@", customerLatitude,customerLongitude,fsrLatitude,fsrLongitude);
        
    }
    
    
    NSLog(@"CUST_LOC_RNG is %@", currentVisit.visitOptions.customer.CUST_LOC_RNG);
    
    
    //fetch range from tbl_addl_info
    NSString * rangeStr=[[SWDatabaseManager retrieveManager]fetchCustomerLocationRange:currentVisit.visitOptions.customer];
    double validationRange=100;
    
    if ([NSString isEmpty:rangeStr]==NO) {
        validationRange=[rangeStr doubleValue];
        
    }
    NSLog(@"location range is %f",validationRange);
    
    
    
    CLLocationCoordinate2D customerLocation= CLLocationCoordinate2DMake([customerLatitude doubleValue], [customerLongitude doubleValue]);
    
    
    NSLog(@"current location lat: %f  long: %f and customer location  lat %@ long %@", appDel.currentLocation.coordinate.latitude,appDel.currentLocation.coordinate.longitude,customerLatitude,customerLongitude );

    
    if (CLLocationCoordinate2DIsValid(customerLocation)) {
        NSLog(@" customer Coordinate valid");
        
        
        BOOL isInLocation=[self location:appDel.currentLocation isNearCoordinate:customerLocation withRadius:validationRange];
        if (isInLocation) {
            
            NSLog(@"user is in customer location");
            
            isCustomerLocationValid=YES;
            
            currentVisit.isFSRinCustomerLocation=YES;
            
            [self saveButtonTapped:saveButton];
            
        }
        else
        {
            
            //validate beacon
            
            
            NSString * validateBeaconStr =[[AppControl retrieveSingleton] VALIDATE_CUST_IBEACON];
            
            
            
            NSLog(@"customer iBeacon validation FLag %@", validateBeaconStr);
            
             if ([validateBeaconStr isEqualToString:KAppControlsYESCode] &&  [NSString isEmpty:[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon].Beacon_UUID]==NO )
            {
                //this is sample test for beacon
                beaconValidationStatus=[self validateFSRLocationwithiBeacon];
                
                if (beaconValidationStatus==NO) {
                    currentVisit.isFSRinCustomerLocation=NO;

                    SalesWorxCustomerOnsiteVisitOptionsReasonViewController *reasonVC= [[SalesWorxCustomerOnsiteVisitOptionsReasonViewController alloc]init];
                    reasonVC.selectedReason=self;
                    reasonVC.currentVisit=currentVisit;
                    currentVisit.beaconValidationSuccessfull=NO;
                    [cashCustomerDetailsView setHidden:YES];
                    
                    [self.navigationController pushViewController:reasonVC animated:NO];
                    
                }
                else{
                    
                    NSLog(@"customer location failed but beacon location succeeded");
                    isCustomerLocationValid=YES;

                    currentVisit.beaconValidationSuccessfull=YES;
                    currentVisit.isFSRinCustomerLocation=YES;

                    validateBeacon=NO;
                    
                    currentVisit.detectedBeacon=[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon];
                    
                    
                    [self saveButtonTapped:saveButton];
                }
                
            }
            
            
            
            else
            {
            
            
            NSLog(@"user is not in customer location displaying reason VC");
            currentVisit.isFSRinCustomerLocation=NO;

            SalesWorxCustomerOnsiteVisitOptionsReasonViewController *reasonVC= [[SalesWorxCustomerOnsiteVisitOptionsReasonViewController alloc]init];
            reasonVC.selectedReason=self;
                currentVisit.beaconAvailable=NO;
            reasonVC.currentVisit=currentVisit;
            
            //reasonVC.view.backgroundColor=KPopUpsBackGroundColor;
            [cashCustomerDetailsView setHidden:YES];

            [self.navigationController pushViewController:reasonVC animated:NO];
            
            }
//            id temp;
//            NSMutableArray * contentArray=temp;
//            [[contentArray objectAtIndex:0] valueForKey:@"test"];
//            
            

        }
        
    } else {
        NSLog(@"FSR is not at customer location but visit is onsite, no validation done due to invalid coordinates of the customer %@", currentVisit.visitOptions.customer.Customer_ID);
        isCustomerLocationValid=YES;
        currentVisit.isFSRinCustomerLocation=YES;

    }

}

- (IBAction)segmentSwitch:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex==0)
    {
        selectedSegmentTitle=kOnsiteVisitTitle;
    }
    else if(sender.selectedSegmentIndex==1)
    {
        selectedSegmentTitle=kTelephonicVisitTitle;
    }
    NSLog(@"selected title is %@", selectedSegmentTitle);
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        //onsite  clicked
        
        //check if user is actually onsite,check if we have user coordinates, if yes then check whether he is within the range
        
        //create region with userlocation
        
       
        [SWDefaults setOnsiteVisitStatus:YES];
        
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesOnsiteVisitTypeScreenName];
        
        if ([[[AppControl retrieveSingleton] ENABLE_FS_ACCOMPANIED_BY] isEqualToString:KAppControlsYESCode]) {
            
            [self hideAccompaniedBy:NO];

        }
        currentVisit.visitOptions.customer.Visit_Type=selectedSegmentTitle;

    }
    else{
       
        //telephonic  clicked
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesTelephonicVisitTypeScreenName];

        [SWDefaults setOnsiteVisitStatus:NO];
        
        if ([[[AppControl retrieveSingleton] ENABLE_FS_ACCOMPANIED_BY] isEqualToString:KAppControlsYESCode]) {
            //deliberately hiding for ids based on segment selection

            [self hideAccompaniedBy:YES];
            
        }
        currentVisit.visitOptions.customer.Visit_Type=selectedSegmentTitle;
        [self saveButtonTapped:nil];

    }
    
    
}

-(void)selectedReasonDelegate:(id)salesworxVisit
{
    currentVisit=salesworxVisit;

    NSString * accompaniedBy=[SWDefaults getValidStringValue:currentVisit.Accompanied_By_User_Name];
    NSString * onsiteExceptionReason=[SWDefaults getValidStringValue:currentVisit.onSiteExceptionReason];
    
    if ([NSString isEmpty:currentVisit.Accompanied_By_User_Name]==NO) {
        accompaniedByTextField.text=accompaniedBy;
    }
    
    
    NSString * accompaniedByFlag=[[AppControl retrieveSingleton] ENABLE_FS_ACCOMPANIED_BY];
    //accompaniedByFlag=KAppControlsNOCode;
    
    if ([accompaniedByFlag isEqualToString:KAppControlsYESCode]) {

    if ([NSString isEmpty:currentVisit.Accompanied_By_User_Name]==NO && [NSString isEmpty:onsiteExceptionReason]==NO) {
        
        //reason code selected to move to start visit
        [self saveButtonTapped:nil];
    }
    }
    else
    {
        [self saveButtonTapped:nil];

    }
    NSLog(@"selected reason for onsite is %@",currentVisit.onSiteExceptionReason);
}


- (IBAction)saveButtonTapped:(id)sender
{
    BOOL isOnsiteVisit=[SWDefaults fetchOnsiteVisitStatus];
    
    
    NSString* validateCustomerLocationAppControl=[[AppControl retrieveSingleton] VALIDATE_CUST_LOC];
    
     NSString * accompaniedByFlag=[[AppControl retrieveSingleton] ENABLE_FS_ACCOMPANIED_BY];
    
    if ([NSString isEmpty:selectedSegmentTitle]==YES) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select visit type" withController:self];
    }
    
//    else if ([accompaniedByFlag isEqualToString:KAppControlsYESCode] && [NSString isEmpty:currentVisit.Accompanied_By_User_ID])
//    {
//         [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select accompanied by" withController:self];
//    }
    
    
   
    else if ([NSString isEmpty:currentVisit.onSiteExceptionReason]==YES && isOnsiteVisit==YES && isCustomerLocationValid ==NO && [validateCustomerLocationAppControl isEqualToString:KAppControlsYESCode] ) {
       
        [self validateLocationRangeforOnSiteVisit];
        
    }
    
//    else if ([self validateFSRLocationwithiBeacon]==NO)
//    {
//        //this is for ibeacon validation
//    }
    else
    {
        NSLog(@"cash customer dict in save %@",cashCustomerDetailsDictionary);
        NSLog(@"selected visit title %@ ",selectedSegmentTitle);
        currentVisit.Visit_Type=selectedSegmentTitle;
        
        if ([currentVisit.visitOptions.customer.Cash_Cust  isEqualToString:@"N"]) {
            if (selectedSegmentTitle.length==0) {
                [self displayAlertWithTitle:@"Missing Data" andMessage:[NSString stringWithFormat:@"Please choose visit type"] withButtonTitles:[NSArray arrayWithObjects:NSLocalizedString(KAlertOkButtonTitle, nil),nil] andTag:200];
            }
            else{
                [self closeTapped];
                
                if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
                {
                    currentVisit.visitOptions.customer.cashCustomerDictionary=[[NSMutableDictionary alloc]initWithDictionary:cashCustomerDetailsDictionary];
                    Singleton* singleTon=[Singleton retrieveSingleton];
                    singleTon.cashCustomerDictionary=cashCustomerDetailsDictionary;
                    if ([currentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"Y"]) {
                        singleTon.isCashCustomer=@"Y";
                    }
                    
                    
                    if ([[[AppControl retrieveSingleton] ENABLE_FS_VISIT_TIMER] isEqualToString:@"Y"]) {
                        [SWDefaults startVisitTimerWithController:self];
                    }
                    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(StartVisitwithInformation) userInfo:nil repeats:NO];
                    
                    
                }
            }
        }
        
        else
        {
            NSString* missingTextField;
            
            if (nameTxtFld.text.length==0) {
                missingTextField=NSLocalizedString(@"Please enter name", nil);
                
            }
            else if (phoneTxtFld.text.length==0) {
                missingTextField=NSLocalizedString(@"Please enter Telephone Number", nil);
            }
            
            else{
                
            }
            //    if (addressTxtView.text.length==0) {
            //        missingTextField=@"address";
            //    }
            //    if (faxTxtFld.text.length==0) {
            //        missingTextField=@"fax";
            //    }
            if (selectedSegmentTitle.length==0) {
                [self displayAlertWithTitle:@"Missing Data" andMessage:[NSString stringWithFormat:@"Please choose visit type"] withButtonTitles:[NSArray arrayWithObjects:NSLocalizedString(KAlertOkButtonTitle, nil),nil] andTag:200];
            }
            
            else if (missingTextField.length>0) {
                [self displayAlertWithTitle:@"Missing Data" andMessage:[NSString stringWithFormat:@"%@",missingTextField] withButtonTitles:[NSArray arrayWithObjects:NSLocalizedString(KAlertOkButtonTitle, nil),nil] andTag:100];
            }
            else{
                
                
                Singleton* singleTon=[Singleton retrieveSingleton];
                singleTon.cashCustomerDictionary=cashCustomerDetailsDictionary;
                singleTon.isCashCustomer=@"Y";
                NSLog(@"inof dict %@", cashCustomerDetailsDictionary);
                
                currentVisit.visitOptions.customer.cashCustomerDictionary=[[NSMutableDictionary alloc]initWithDictionary:cashCustomerDetailsDictionary];
                
                saveTapped=YES;
                
                
                [self closeTapped];
                
                if ([[[AppControl retrieveSingleton] ENABLE_FS_VISIT_TIMER] isEqualToString:@"Y"]) {
                    [SWDefaults startVisitTimerWithController:self];
                }
                
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(StartVisitwithInformation) userInfo:nil repeats:NO];
            }
        }
    }
}

-(void)StartVisitwithInformation
{
    if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
    {
        NSDictionary *plannedVisitDetails=[self fetchSelectedCustomerPlanFSRDetailId];
        if(plannedVisitDetails!=nil)
        {
            currentVisit.isPlannedVisit=YES;
            currentVisit.FSR_Plan_Detail_ID=[plannedVisitDetails valueForKey:@"FSR_Plan_Detail_ID"];
            currentVisit.visitOptions.customer.Planned_visit_ID=[plannedVisitDetails valueForKey:@"Planned_Visit_ID"];
        }
        
        [self.delegate cashCustomerSelectedVisitOption:selectedSegmentTitle customer:currentVisit];
        
    }
}
-(NSDictionary *)fetchSelectedCustomerPlanFSRDetailId
{
   NSDictionary *visitDetails= [[SWDatabaseManager retrieveManager]FetchTodayIncompletedPlannedVisitDetailsForCustomer:currentVisit.visitOptions.customer];
    return visitDetails;
}

- (void)CloseCashCustomerInfo
{
    //do what you need to do when animation ends...
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}

-(void)closeTapped
{
    
    
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseCashCustomerInfo) userInfo:nil repeats:NO];
  
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    [self closeTapped];
    
}

#pragma mark Accompanied By Textfield Data

-(void)accompaniedByTapped
{
    SalesWorxCustomerOnsiteVisitOptionsReasonViewController *reasonVC= [[SalesWorxCustomerOnsiteVisitOptionsReasonViewController alloc]init];
    reasonVC.selectedReason=self;
    reasonVC.isAccompaniedBy=YES;
    reasonVC.currentVisit=currentVisit;
    //reasonVC.view.backgroundColor=KPopUpsBackGroundColor;
    [cashCustomerDetailsView setHidden:YES];

    [self.navigationController pushViewController:reasonVC animated:NO];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==accompaniedByTextField) {
        [self accompaniedByTapped];
        return NO;
    }
    else{
        return YES;
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"should change called with text %@", textField.text);
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([newString length] == 0)
    {

        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField==nameTxtFld)
    {
        
        NSString *expression = @"^[a-zA-Z0-9 ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=150);
    }
    else if(textField==phoneTxtFld)
    {
        NSString *expression = @"^[0-9]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=30);
    }
//    else if(textField==addressTxtFld)
//    {
//        NSString *expression = @"^[a-zA-Z0-9 ]+$";
//        NSError *error = nil;
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
//        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
//        return (numberOfMatches != 0 && newString.length<=100);
//        
//    }
    else if(textField==faxTxtFld)
    {
        NSString *expression = @"^[0-9]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=100);
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"did begin called");
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"did end editing called");
    
    if (textField==nameTxtFld) {
        [cashCustomerDetailsDictionary setValue:nameTxtFld.text forKey:@"cash_Name"];
        currentVisit.visitOptions.customer.cash_Name=nameTxtFld.text;
       
    }
    else if (textField==phoneTxtFld)
    {
        [cashCustomerDetailsDictionary setValue:phoneTxtFld.text forKey:@"cash_Phone"];
        currentVisit.visitOptions.customer.cash_Phone=phoneTxtFld.text;

        
    }
    else if (textField==contactTxtFld)
    {
        [cashCustomerDetailsDictionary setValue:contactTxtFld.text forKey:@"cash_Contact"];
        currentVisit.visitOptions.customer.cash_Contact=contactTxtFld.text;

    }
       else if (textField==faxTxtFld)
    {
        [cashCustomerDetailsDictionary setValue:faxTxtFld.text forKey:@"cash_Fax"];
        currentVisit.visitOptions.customer.cash_Fax=faxTxtFld.text;

        
    }
        
   
    
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{

    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        
        if ([phoneTxtFld isFirstResponder]) {
            [addressTxtView becomeFirstResponder];
        }
        else{
        [textField resignFirstResponder];
        }
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark UIAlertView Delegate Methods
-(void)displayAlertWithTitle:(NSString*)Title andMessage:(NSString*)message withButtonTitles:(NSArray*)buttonTitlesArray andTag:(NSInteger)alertTag
{
    if ([UIAlertController class]) {
        // Use new API to create alert controller, add action button and display it
        alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(Title, nil) message:NSLocalizedString(message, nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle: NSLocalizedString([buttonTitlesArray objectAtIndex:0], nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            //yes button action
            if ([UIAlertController class]) {
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }        }];
        [alertController addAction: ok];
        
        if (buttonTitlesArray.count>1) {
            UIAlertAction* cancel = [UIAlertAction actionWithTitle: NSLocalizedString([buttonTitlesArray objectAtIndex:1], nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                //no button action
                if ([UIAlertController class]) {
                    [alertController dismissViewControllerAnimated:YES completion:nil];
                }            }];
            [alertController addAction: cancel];
        }
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        // We are running on old SDK as the new class is not available
        // Hide the compiler errors about deprecation and use the class available on older SDK
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIAlertView *alert;
        if (buttonTitlesArray.count>1) {
            alert = [[UIAlertView alloc] initWithTitle:Title
                                               message:message
                                              delegate:self
                                     cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                     otherButtonTitles:[buttonTitlesArray objectAtIndex:1],nil];
        }
        else{
            alert = [[UIAlertView alloc] initWithTitle:Title
                                               message:message
                                              delegate:self
                                     cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                     otherButtonTitles:nil];
        }
        
        [alert show];
#pragma clang diagnostic pop
    }
    
}

#pragma mark KeyBoard Animation Methods
- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView animateWithDuration:5.0 animations:^{
        
        self.view.frame=CGRectMake(oldFrame.origin.x, oldFrame.origin.y-270, oldFrame.size.width, oldFrame.size.height);
    }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:5.0 animations:^{
        
        self.view.frame=oldFrame;
    }];

}


#pragma mark TextView methods

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==addressTxtView) {
        [cashCustomerDetailsDictionary setValue:textView.text forKey:@"cash_Address"];
        currentVisit.visitOptions.customer.cash_Address=textView.text;

    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    NSString *expression = @"^[a-zA-Z0-9 \n./-]+$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
            return (numberOfMatches != 0 && newString.length<=100);
    
}

#pragma mark ibeacon Methods

-(BOOL)validateFSRLocationwithiBeacon
{
    BOOL status=NO;
    
    
    
    
    currentVisit.detectedBeacon=[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon];

    NSLog(@"detected beacon in visit options %@", currentVisit.detectedBeacon.Beacon_Major);
    
    if (![NSString isEmpty:currentVisit.detectedBeacon.Beacon_Major]) {
        return YES;
    }
    
    return status;
}

@end
