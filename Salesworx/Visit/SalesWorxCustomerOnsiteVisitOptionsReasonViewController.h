//
//  SalesWorxCustomerOnsiteVisitOptionsReasonViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/24/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import <MedRepDefaults.h>
#import "SalesWorxSingleLineLabel.h"
#import <MedRepQueries.h>
#import "MedRepHeader.h"
#import "SalesWorxCustomClass.h"

#define kOnsiteExceptionReason @"ONSITE_EXCEPTION_REASON"

@protocol SelectedReasonCodeDelegate <NSObject>

-(void)selectedReasonDelegate:(id)salesworxVisit;
-(void)selectedReasonDidCancel:(id)salesWorxVisit;

@end

@interface SalesWorxCustomerOnsiteVisitOptionsReasonViewController : UIViewController
{
    NSMutableArray * contentArray;
    IBOutlet MedRepHeader *headerTitleLbl;
    NSMutableArray * selectedReasonArray;
    
    NSInteger selectedIndex;
    
    NSIndexPath * selectedIndexPath;
    
    NSNumber *selectedIndexNumber;
    
    id selectedReason;
}
- (IBAction)saveTapped:(id)sender;
@property(nonatomic) id selectedReason;

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIView *contentView;
- (IBAction)canCelButtonTapped:(id)sender;
@property(strong,nonatomic) UINavigationController * navController;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *reasonLbl;
@property (strong, nonatomic) IBOutlet UITableView *reasonTblView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) SalesWorxVisit * currentVisit;
@property(nonatomic) BOOL isAccompaniedBy;

// this is only for market visit
@property(nonatomic) BOOL isMarketVisit;
@property(nonatomic) BOOL isMerchandisingVisit;
@property(nonatomic) BOOL isOpenVisit;

@property(strong,nonatomic) VisitDetails *currentVisitDetails;

@end
