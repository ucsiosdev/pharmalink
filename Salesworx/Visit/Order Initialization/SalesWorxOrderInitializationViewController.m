//
//  SalesWorxOrderInitializationViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxOrderInitializationViewController.h"
#import "OrderInitializationCollectionViewCell.h"
#import "MedRepDefaults.h"


@interface SalesWorxOrderInitializationViewController ()

@end

@implementation SalesWorxOrderInitializationViewController
@synthesize orderTempleteView,orderTypeView,currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=UIViewBackGroundColor;
    enableOrderType=YES;
    enableOrderTemplate=YES;
    appControl=[AppControl retrieveSingleton];

    tempParentViewString=@"SO";
    
    
    UIBarButtonItem* spacebutton=[[UIBarButtonItem alloc]initWithTitle:@"     " style:UIBarButtonItemStylePlain target:self action:nil];
    
    [spacebutton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Reset", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelBarButtonTapped)];

    [cancelBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    UIBarButtonItem* proceedBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Proceed", nil) style:UIBarButtonItemStylePlain target:self action:@selector(proceedBarButtonTapped)];
    [proceedBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];

    
    self.navigationItem.rightBarButtonItems=@[proceedBtn,spacebutton,cancelBtn];
    

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Order Initialization"];

   [categoryCollectionView registerClass:[OrderInitializationCollectionViewCell class] forCellWithReuseIdentifier:@"orderCell"];
    
    /*
    
     
    // Do any additional setup after loading the view from its nib.

        /* Initializing sales order options*/

    currentVisit.visitOptions.salesOrder.productCategoriesArray=[[NSMutableArray alloc]init];
    currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=[[NSMutableArray alloc]init];
    currentVisit.visitOptions.salesOrder.selectedTemplete=nil;
    currentVisit.visitOptions.salesOrder.selectedCategory=nil;
    currentVisit.visitOptions.salesOrder.selectedOrderType=nil;
    currentVisit.visitOptions.salesOrder.focParentOrderNumber=nil;
    currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
    currentVisit.visitOptions.salesOrder.confrimationDetails=nil;
    currentVisit.visitOptions.salesOrder.isManagedOrder=NO;
    currentVisit.visitOptions.salesOrder.isTemplateOrder=NO;

    templateLineItemsArray=[[NSMutableArray alloc]init];
    ordertempletesArray=[[NSMutableArray alloc]init];
}

-(void)proceedBarButtonTapped
{
    [self proceedButtonTapped];
}
-(void)cancelBarButtonTapped
{
    [self resetButtonTapped:nil];
}


-(void)proceedButtonTapped
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kRecommendedOrderPopOverTitle])
        {
            currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[[SWDatabaseManager retrieveManager]fetchRecommendedOrderTemplateLineItemsForcustomer:currentVisit.visitOptions.customer andCategory:currentVisit.visitOptions.salesOrder.selectedCategory AndCurrentVisitDcItems:[self getRecommendedOrderDCItemsForCurrentVisit]];
        }
        if ([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kDCReOrderPopOverTitle]) {
            currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[[SWDatabaseManager retrieveManager]fetchDCReOrderItemsForCustomer:currentVisit.visitOptions.customer andCategory:currentVisit.visitOptions.salesOrder.selectedCategory AndCurrentVisitDcItems:[self getDCReOrderItemsForCurrentVisit]];
        }

        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (currentVisit.visitOptions.salesOrder.selectedCategory==nil&&tempWareHouseArray.count==0) {
                [self displayAlertWithTitle:KMissingData andMessage:KOrderInitializationscreen_CategoryNotSelectedAlertMessage withButtonTitles:@[KAlertOkButtonTitle] andTag:101];
            }
            else if ( [appControl.FOC_ORDER_MODE isEqualToString:@"L"] && parentOrderID==nil && [selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
            {
                [self displayAlertWithTitle:KMissingData andMessage:@"Please select parent order and try again" withButtonTitles:@[KAlertOkButtonTitle] andTag:102];
            }
            
            else if (orderTypeTextField.text.length==0&& [appControl.ENABLE_FOC_ORDER isEqualToString:@"Y"])
            {
                [self displayAlertWithTitle:KMissingData andMessage:@"Please select order type and try again" withButtonTitles:@[KAlertOkButtonTitle] andTag:110];
            }
            else if(currentVisit.visitOptions.salesOrder.isTemplateOrder && currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray.count==0)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:KOrderInitializationscreen_NoTemplateOrderLineItemsAlertMessage withController:self];
            }

            else if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kRecommendedOrderPopOverTitle] && currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray.count==0)
            {
                if(isRecommendedOrderAvailableForTheCustomer)
                {
                      [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:KOrderInitializationscreen_NoRecommendedOrderAlertMessage withController:self];
                }
                else
                {

                    [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:[NSString stringWithFormat:@"%@ %@",KOrderInitializationscreen_NoRecommendedOrderForCustomerAlertMessage,currentVisit.visitOptions.customer.Customer_Name] withController:self];
                }
                
            }
            else if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kReOrderPopOverTitle] && [[SWDefaults getValidStringValue:currentVisit.visitOptions.salesOrder.reOrderDetails.Orig_Sys_Document_Ref] isEqualToString:@""])
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"No orders available for selected category" withController:self];
            }
            else if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kDCReOrderPopOverTitle] && currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray.count == 0) {
                [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:KOrderInitializationscreen_NoDCReOrderAlertMessage withController:self];
            }
            else
            {
                SalesWorxSalesOrderViewController* salesOrderVC=[[SalesWorxSalesOrderViewController alloc]init];
                salesOrderVC.currentVisit=currentVisit;
                currentVisit.visitOptions.salesOrder.OrderStartTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
                [self.navigationController pushViewController:salesOrderVC animated:YES];
            }

        });
    });
  
    

}


-(void)applyAppControlFlagsforViews
{
    
    
    NSLog(@"multi category transaction is %@", appControl.ENABLE_MULTI_CAT_TRX);
    
    
    //update views based on app control flags
    
    if ([appControl.ENABLE_MULTI_CAT_TRX isEqualToString:KAppControlsYESCode]) {
        categoriesView.hidden=NO;
        categoryViewTopConstraint.constant=kAllViewsTopConstant;
        categoryViewHeightConstraint.constant=kCategoryViewHeightConstant;
    }
    else
    {
        if(currentVisit.visitOptions.salesOrder.productCategoriesArray.count==1)
        {
            currentVisit.visitOptions.salesOrder.selectedCategory=[currentVisit.visitOptions.salesOrder.productCategoriesArray objectAtIndex:0];
            [categoryCollectionView.delegate collectionView:categoryCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
    
    if ([appControl.ENABLE_TEMPLATES isEqualToString:KAppControlsYESCode]&&currentVisit.visitOptions.salesOrder.salesOrderTempleteArray.count>0 ) {
        orderTempleteView.hidden=NO;
        templateViewHeightConstraint.constant=kOrdertemplateViewHeightConstant;
        templateViewTopConstraint.constant=kAllViewsTopConstant;
    }
    else
    {
        orderTempleteView.hidden=YES;
        templateViewHeightConstraint.constant=0.0f;
        templateViewTopConstraint.constant=0.0f;
    }
    
    if (orderTypeArray.count>1) { /** if Foc order or recommnded order enabled*/
        orderTypeView.hidden=NO;
        orderTypeViewHeightConstraint.constant=kOrderTypeViewheightConstant;
        orderTypeViewTopConstraint.constant=kAllViewsTopConstant;
    }
    else
    {
        orderTypeView.hidden=YES;
        orderTypeViewHeightConstraint.constant=0.0f;
        orderTypeViewTopConstraint.constant=0.0f;
    }
    


    [self.view layoutIfNeeded];
    
  
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
   
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesOrderInitilisationScreenName];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    
    if(self.isMovingToParentViewController)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            
             NSMutableArray *recOrderData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from  tbl_customer_ro where customer_id='%@' and site_use_id='%@'",currentVisit.visitOptions.customer.Ship_Customer_ID,currentVisit.visitOptions.customer.Ship_Site_Use_ID]];
            
            NSLog(@"RO order qry %@", [NSString stringWithFormat:@"Select * from  tbl_customer_ro where customer_id='%@' and site_use_id='%@'",currentVisit.visitOptions.customer.Ship_Customer_ID,currentVisit.visitOptions.customer.Ship_Site_Use_ID]);
            
            
            NSMutableArray *DCReOrderData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.* FROM TBL_Distribution_Check_Items as A inner join TBL_Distribution_Check as B where A.DistributionCheck_ID = B.DistributionCheck_ID and B.Visit_ID = '%@' and B.Customer_ID = '%@' and B.Site_Use_ID = '%@' and A.Reorder ='Y'",currentVisit.Visit_ID, currentVisit.visitOptions.customer.Ship_Customer_ID, currentVisit.visitOptions.customer.Ship_Site_Use_ID]];
            
            NSLog(@"DC RO order qry %@", [NSString stringWithFormat:@"SELECT A.* FROM TBL_Distribution_Check_Items as A inner join TBL_Distribution_Check as B where A.DistributionCheck_ID = B.DistributionCheck_ID and B.Visit_ID = '%@' and B.Customer_ID = '%@' and B.Site_Use_ID = '%@' and A.Reorder ='Y'",currentVisit.Visit_ID, currentVisit.visitOptions.customer.Ship_Customer_ID, currentVisit.visitOptions.customer.Ship_Site_Use_ID]);
            
            
            if(recOrderData.count>0)
                isRecommendedOrderAvailableForTheCustomer=YES;
            else
                isRecommendedOrderAvailableForTheCustomer=NO;


            
            
            ordertempletesArray=[[SWDatabaseManager retrieveManager]fetchOrderTempletesForCustomer:currentVisit.visitOptions.customer];
            currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=[[NSMutableArray alloc]initWithArray:ordertempletesArray];
            unfilteredTemplateArray=ordertempletesArray;
            
            orderTypeArray=[[NSMutableArray alloc]initWithObjects:kDefaultOrderPopOverTitle,nil];
            
        //    NSArray *arrOrder = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_Order_History where Ship_To_Customer_ID = '%@' and Visit_ID is not '%@'",currentVisit.visitOptions.customer.Customer_ID,currentVisit.Visit_ID]];
            if([appControl.ENABLE_FOC_ORDER isEqualToString:KAppControlsYESCode])
            {
                [orderTypeArray addObject:kFOCOrderPopOverTitle];
            }
            if([appControl.ENABLE_RECOMMENDED_ORDER isEqualToString:KAppControlsYESCode] && isRecommendedOrderAvailableForTheCustomer)
            {
                [orderTypeArray addObject:kRecommendedOrderPopOverTitle];
            }
            if([appControl.ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode] /*&& [arrOrder count] >0*/)
            {
                [orderTypeArray addObject:kReOrderPopOverTitle];
            }
            if(DCReOrderData.count > 0) {
                [orderTypeArray addObject:kDCReOrderPopOverTitle];
            }
            
            selectedOrderType=[orderTypeArray objectAtIndex:0];
            currentVisit.visitOptions.salesOrder.selectedOrderType=selectedOrderType;
            
            currentVisit.visitOptions.salesOrder.productCategoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:currentVisit];
            tempCategoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:currentVisit];
            unfilteredCategories=tempCategoriesArray;
            if (tempCategoriesArray.count>0) {
                currentVisit.visitOptions.salesOrder.productCategoriesArray=tempCategoriesArray;
            }
            parentOrdersArray=[[SWDatabaseManager retrieveManager]fetchParentOrdersforVisitID:currentVisit];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                orderTypeTextField.text=selectedOrderType;
                
                if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
                    
                    [customerStatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
                }
                else
                {
                    [customerStatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
                }
                customerNameLbl.text=currentVisit.visitOptions.customer.Customer_Name;
                customerNumberLbl.text=currentVisit.visitOptions.customer.Customer_No;
                availableBalanceLbl.text=[[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Avail_Bal] currencyString];
                
                lblVisitType.layer.cornerRadius = 8.0f;
                [lblVisitType.layer setMasksToBounds:YES];
                lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
                
                [self updateOrderTypeDescriptionView];
                parentOrderViewHeightConstraint.constant=0.0f;
                [categoryCollectionView reloadData];
                [self applyAppControlFlagsforViews];
                
            });
        });
    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            ordertempletesArray=[[SWDatabaseManager retrieveManager]fetchOrderTempletesForCustomer:currentVisit.visitOptions.customer];
            currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=[[NSMutableArray alloc]initWithArray:ordertempletesArray];
            unfilteredTemplateArray=ordertempletesArray;
            parentOrdersArray=[[SWDatabaseManager retrieveManager]fetchParentOrdersforVisitID:currentVisit];

            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self applyAppControlFlagsforViews];
                
            });
        });

    }
    

    

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIPopOver delegate methods

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;

    if([popOverTitle isEqualToString:kOrderTypePopOverTitle])
    {
        popOverVC.enableArabicTranslation=YES;
    }
    
     if ([popOverTitle isEqualToString:kOrderTempletePopOverTitle]&& selectedTextField==orderTempleteTextField) {
        
        popOverVC.enableSwipeToDelete=YES;
        popOverVC.disableSearch=NO;
        popOverVC.popOverContentArray=[currentVisit.visitOptions.salesOrder.salesOrderTempleteArray mutableCopy];
        popOverVC.popoverType=kOrderTempletePopOverTitle;

    }
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    categoryPopOverController=nil;
    categoryPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    categoryPopOverController.delegate=self;
    popOverVC.popOverController=categoryPopOverController;
    popOverVC.titleKey=popOverString;
    [categoryPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [categoryPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


-(void)didSelectOrderType:(id)selectedOrderType
{
    NSLog(@"selected order type %@", selectedOrderType);
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popOverTitle isEqualToString:kCategoryPopOverTitle]) {
        ProductCategories* selectedCategory=[currentVisit.visitOptions.salesOrder.productCategoriesArray objectAtIndex:selectedIndexPath.row];
        categoryTextField.text=selectedCategory.Category;
        //currentVisit.visitOptions.salesOrder.selectedCategory=selectedCategory.Category;
    }
    else if ([popOverTitle isEqualToString:kWarehousePopOverTitle])
    {
        
        NSLog(@"product category set %@", [SWDefaults productCategory]);
    }
    else if ([popOverTitle isEqualToString:kOrderTempletePopOverTitle])
    {
        indexPathTemplate=selectedIndexPath.row;
        currentVisit.visitOptions.salesOrder.isTemplateOrder=YES;
        currentTemplate=[[SalesOrderTemplete alloc]init];
        currentTemplate=[currentVisit.visitOptions.salesOrder.salesOrderTempleteArray objectAtIndex:selectedIndexPath.row];
        orderTempleteTextField.text=currentTemplate.Template_Name;
        currentVisit.visitOptions.salesOrder.selectedTemplete=currentTemplate;
        
        //fetchTemplateLineItems
        templateLineItemsArray=[[SWDatabaseManager retrieveManager] fetchOrderTemplateLineItems:currentTemplate];
        NSLog(@"template line items array is %@", templateLineItemsArray);
        if (templateLineItemsArray.count>0) {
            currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=templateLineItemsArray;
        }
        
        //refine and select the category based on selected template
        //check if custom attribute 2 gets null in some cases
        NSPredicate* templatePredicate=[NSPredicate predicateWithFormat:@"SELF.Category == [cd] %@ AND SELF.Org_ID == [cd] %@",currentTemplate.Custom_Attribute_1,currentTemplate.Custom_Attribute_2];
        NSLog(@"template predicate is %@", templatePredicate);
        NSMutableArray* filteredCategoriesArray=[[unfilteredCategories filteredArrayUsingPredicate:templatePredicate] mutableCopy];
        
        
        if(filteredCategoriesArray.count>0)
        {
        
        ProductCategories * filteredCategory=[filteredCategoriesArray objectAtIndex:0];
        
        NSLog(@"filtered categories %@", filteredCategory.Category);
        
        [tempCategoriesArray setValue:@"N" forKey:@"isSelected"];
   
        filteredCategory.isSelected=@"Y";

        [categoryCollectionView reloadData];
        
        currentVisit.visitOptions.salesOrder.selectedCategory=filteredCategory;
        
        
        NSLog(@"filterec categories array is %@",filteredCategoriesArray);
        Singleton * single=[Singleton retrieveSingleton];
        single.orderStartDate=[NSDate date];
        conventionalTemplateDataArray=[[SWDatabaseManager retrieveManager] fetchConventionalTemplate:unfilteredTemplateArray];
        NSLog(@"conventional data %@", conventionalTemplateDataArray);
        single.isTemplateOrder=YES;
        //comment it out to be done
        
      //  NSLog(@"temp id is %@",[[conventionalTemplateDataArray objectAtIndex:selectedIndexPath.row] stringForKey:@"Order_Template_ID"]);
        
      //  [self getSalesOrderServiceDidGetTemplateItem:[[SWDatabaseManager retrieveManager] dbGetTemplateOrderItem:currentTemplate.Order_Template_ID]];
        }
        else
        {

            [self resetButtonTapped:nil];
        }

        
    }
    else if ([popOverTitle isEqualToString:kOrderTypePopOverTitle])
    {
        
        selectedOrderType=[orderTypeArray objectAtIndex:selectedIndexPath.row];
        orderTypeTextField.text=selectedOrderType;
        currentVisit.visitOptions.salesOrder.selectedOrderType=selectedOrderType;
        [orderTempleteTextField setIsReadOnly:NO];
        

        [self updateOrderTypeDescriptionView];

        if ([selectedOrderType isEqualToString:kFOCOrderPopOverTitle]) {
            //show parent order view
        }
        else  if ([selectedOrderType isEqualToString:kRecommendedOrderPopOverTitle])
        {
            [orderTempleteTextField setIsReadOnly:YES];
            recommendedOrderNotesTextView.text=@"";
            recommendedOrderNotesTextView.font=KOpenSans13;
            recommendedOrderNotesTextView.textColor=MedRepElementDescriptionLabelColor;
            recommendedOrderNotesTextView.editable=NO;
            if(isRecommendedOrderAvailableForTheCustomer)
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    NSMutableArray *itemsArray=[[SWDatabaseManager retrieveManager]fetchRecommendedOrderTemplateLineItemsForcustomer:currentVisit.visitOptions.customer andCategory:currentVisit.visitOptions.salesOrder.selectedCategory AndCurrentVisitDcItems:[self getRecommendedOrderDCItemsForCurrentVisit]];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if(itemsArray.count>0)
                        {
                            recommendedOrderNotesTextView.text=[NSString stringWithFormat:@"(%@)",appControl.RO_DESCRIPTION];
                        }
                        else
                        {
                            recommendedOrderNotesTextView.textColor=[UIColor redColor];
                            recommendedOrderNotesTextView.text=[NSString stringWithFormat:@"(%@)",NSLocalizedString(KOrderInitializationscreen_NoRecommendedOrderAlertMessage, nil)];
                            
                        }
                        
                    });
                });
            }
            else
            {
                recommendedOrderNotesTextView.textColor=[UIColor redColor];

                recommendedOrderNotesTextView.text=[NSString stringWithFormat:@"%@ %@",KOrderInitializationscreen_NoRecommendedOrderForCustomerAlertMessage,currentVisit.visitOptions.customer.Customer_Name];

            }

            
        }
        else  if ([selectedOrderType isEqualToString:kReOrderPopOverTitle])
        {
            NSArray *temp = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Orig_Sys_Document_Ref,IFNULL(A.Shipping_Instructions,'N/A') AS Shipping_Instructions, A.Creation_Date,A.End_Time, A.Transaction_Amt, CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status], B.Customer_Name ,(SELECT C.Username FROM TBL_User AS C WHERE C.SalesRep_ID=A.Created_By) As [FSR] FROM TBL_Order_History As A INNER JOIN TBL_Customer_Ship_Address As B ON B.Customer_ID=A.Ship_To_Customer_ID AND B.Site_Use_ID=A.Ship_To_Site_ID WHERE A.Doc_Type='I' AND B.Customer_No = '%@' AND A.Ship_To_Site_ID='%@' AND A.Visit_ID is not '%@' AND A.Custom_Attribute_2 = '%@' AND A.Custom_Attribute_6='%@' ORDER BY A.Orig_Sys_Document_Ref DESC", currentVisit.visitOptions.customer.Customer_No, currentVisit.visitOptions.customer.Ship_Site_Use_ID, currentVisit.Visit_ID, currentVisit.visitOptions.salesOrder.selectedCategory.Category,@"0"]];
            
            NSString* reorderQry = [NSString stringWithFormat:@"SELECT A.Orig_Sys_Document_Ref,IFNULL(A.Shipping_Instructions,'N/A') AS Shipping_Instructions, A.Creation_Date,A.End_Time, A.Transaction_Amt, CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status], B.Customer_Name ,(SELECT C.Username FROM TBL_User AS C WHERE C.SalesRep_ID=A.Created_By) As [FSR] FROM TBL_Order_History As A INNER JOIN TBL_Customer_Ship_Address As B ON B.Customer_ID=A.Ship_To_Customer_ID AND B.Site_Use_ID=A.Ship_To_Site_ID WHERE A.Doc_Type='I' AND B.Customer_No = '%@' AND A.Ship_To_Site_ID='%@' AND A.Visit_ID is not '%@' AND A.Custom_Attribute_2 = '%@' AND A.Custom_Attribute_6='%@' ORDER BY A.Orig_Sys_Document_Ref DESC", currentVisit.visitOptions.customer.Customer_No, currentVisit.visitOptions.customer.Ship_Site_Use_ID, currentVisit.Visit_ID, currentVisit.visitOptions.salesOrder.selectedCategory.Category,@"0"];
            NSLog(@"re order qry %@", reorderQry);
            [orderTempleteTextField setIsReadOnly:YES];


            SalesWorxReOrder *reOrder=[[SalesWorxReOrder alloc]init];
            if ([temp count] != 0) {
                lblOrderNo.text = [NSString stringWithFormat:@"%@",[[temp objectAtIndex:0]valueForKey:@"Orig_Sys_Document_Ref"]];
                lblOrderDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[NSString stringWithFormat:@"%@",[[temp objectAtIndex:0]valueForKey:@"End_Time"]]];
                lblOrderValue.text = [[NSString stringWithFormat:@"%@",[[temp objectAtIndex:0]valueForKey:@"Transaction_Amt"]] currencyString];
                
                reOrder.Orig_Sys_Document_Ref=[NSString stringWithFormat:@"%@",[[temp objectAtIndex:0]valueForKey:@"Orig_Sys_Document_Ref"]];
                reOrder.OrderAmount=[NSString stringWithFormat:@"%@",[[temp objectAtIndex:0]valueForKey:@"Transaction_Amt"]];
                reOrder.Creation_Date=[NSString stringWithFormat:@"%@",[[temp objectAtIndex:0]valueForKey:@"End_Time"]];
                
            }
            else
            {
                lblOrderNo.text = @"N/A";
                lblOrderDate.text = @"N/A";
                lblOrderValue.text = @"N/A";
            }
            currentVisit.visitOptions.salesOrder.reOrderDetails=reOrder;
        }

        
        [self ClearTheTemplateVisitTemplateDetails];

    }
    [categoryPopOverController dismissPopoverAnimated:YES];
    categoryPopOverController=nil;

}

-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath
{
    SalesOrderTemplete * deletedTemplate=[currentVisit.visitOptions.salesOrder.salesOrderTempleteArray objectAtIndex:deletedIndexPath.row];
    
    [[SWDatabaseManager retrieveManager]deleteTemplateOrderWithRef:deletedTemplate.Order_Template_ID];
    orderTempleteTextField.text=@"";
    
    
    NSMutableArray *tempTemplatesarray=[currentVisit.visitOptions.salesOrder.salesOrderTempleteArray mutableCopy];
    
    currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=tempTemplatesarray;
    
    [tempTemplatesarray removeObjectAtIndex:deletedIndexPath.row];
    
    currentVisit.visitOptions.salesOrder.selectedTemplete=nil;
    currentVisit.visitOptions.salesOrder.isTemplateOrder=NO;

}


#pragma mark UITextField Delegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==categoryTextField) {
        popOverTitle=kCategoryPopOverTitle;
        [self presentPopoverfor:categoryTextField withTitle:kCategoryPopOverTitle withContent:[currentVisit.visitOptions.salesOrder.productCategoriesArray valueForKey:kCategoryPopOverTitle]];
        return NO;
    }
    else if (textField==warehouseTextField)
    {
        popOverTitle=kWarehousePopOverTitle;
        
        //refine warehouse 
        [self presentPopoverfor:warehouseTextField withTitle:kWarehousePopOverTitle withContent:[currentVisit.visitOptions.salesOrder.productCategoriesArray valueForKey:@"Org_ID"]];
        return NO;
    }
    else if (textField==orderTempleteTextField)
    {
        popOverTitle=kOrderTempletePopOverTitle;
        if (currentVisit.visitOptions.salesOrder.salesOrderTempleteArray.count>0) {
          [self presentPopoverfor:orderTempleteTextField withTitle:kOrderTempletePopOverTitle withContent:[[currentVisit.visitOptions.salesOrder.salesOrderTempleteArray valueForKey:@"Template_Name"] mutableCopy]];
        }
        else{
            
            [self displayAlertWithTitle:@"No Data" andMessage:@"No template available for selected category/warehouse, please select another category/warehouse and try again" withButtonTitles:@[KAlertOkButtonTitle] andTag:100];
            
        }
        
        return NO;
    }
    else if (textField==orderTypeTextField)
    {
        popOverTitle=kOrderTypePopOverTitle;
        [self presentPopoverfor:orderTypeTextField withTitle:kOrderTypePopOverTitle withContent:orderTypeArray];
        return NO;
        
    }
    else if (textField==parentOrderTextField)
    {
//        if ([selectedOrderType isEqualToString:kFOCOrderPopOverTitle] && parentOrdersArray.count==0&& [appControl.FOC_ORDER_MODE isEqualToString:@"L"]) {
//            
//            [self displayAlertWithTitle:@"No orders to link" andMessage:@"Please create an order to link it to foc order" withButtonTitles:@[KAlertOkButtonTitle] andTag:105];
//            
//        }
//        else if ([selectedOrderType isEqualToString:kFOCOrderPopOverTitle] && parentOrdersArray.count==0&& [appControl.FOC_ORDER_MODE isEqualToString:@"B"])
//        {
//            [self displayAlertWithTitle:@"No orders to link" andMessage:@"Please create an order to link it to foc order" withButtonTitles:@[KAlertOkButtonTitle] andTag:106];
//        }
//        
//        else
//        {
        popOverTitle =kParentOrderPopOverTitle;
        SalesWorxOrderInitializationParentOrderViewController * popOverVC=[[SalesWorxOrderInitializationParentOrderViewController alloc]init];
        popOverVC.popOverContentArray=parentOrdersArray;
        popOverVC.parentOrderDelegate=self;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        categoryPopOverController=nil;
        categoryPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        categoryPopOverController.delegate=self;
        popOverVC.popOverController=categoryPopOverController;
        popOverVC.titleKey=kParentOrderPopOverTitle;
        [categoryPopOverController setPopoverContentSize:CGSizeMake(474, 273) animated:YES];
        [categoryPopOverController presentPopoverFromRect:parentOrderTextField.dropdownImageView.frame inView:parentOrderTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        //}
        return NO;
    }
    
    return YES;
}

#pragma mark Parent Order Methods
-(void)parentOrderDidSelectRow:(NSIndexPath*)selectedIndexPath;
{
    NSLog(@"selected parent order %@",[parentOrdersArray objectAtIndex:selectedIndexPath.row]);
    parentOrderTextField.text=[NSString stringWithFormat:@"%@",[[parentOrdersArray objectAtIndex:selectedIndexPath.row] valueForKey:@"Orig_Sys_Document_Ref"]];
    parentOrderID=parentOrderTextField.text;
    
    if ( [appControl.FOC_ORDER_MODE isEqualToString:@"L"] && parentOrderID.length>0)
    {
        currentVisit.visitOptions.salesOrder.focParentOrderNumber=parentOrderID;
    }
    
    else if ( [appControl.FOC_ORDER_MODE isEqualToString:@"B"] && parentOrderID.length>0)
    {
        currentVisit.visitOptions.salesOrder.focParentOrderNumber=parentOrderID;

    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)resetButtonTapped:(id)sender {
    
    categoryTextField.text=@"";
    warehouseTextField.text=@"";
    orderTempleteTextField.text=@"";
    orderTypeTextField.text=@"";
    parentOrderTextField.text=@"";
    currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=unfilteredTemplateArray;
    [tempCategoriesArray setValue:@"N" forKey:@"isSelected"];
    [categoryCollectionView reloadData];
    tempWareHouseArray=[[NSMutableArray alloc]init];
    
    
    currentVisit.visitOptions.salesOrder.selectedTemplete=nil;
    currentVisit.visitOptions.salesOrder.selectedCategory=nil;
    currentVisit.visitOptions.salesOrder.selectedOrderType=nil;
    currentVisit.visitOptions.salesOrder.focParentOrderNumber=nil;
    currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
    currentVisit.visitOptions.salesOrder.confrimationDetails=nil;
    currentVisit.visitOptions.salesOrder.isManagedOrder=NO;
    currentVisit.visitOptions.salesOrder.isTemplateOrder=NO;
    currentVisit.visitOptions.salesOrder.reOrderDetails=nil;


    selectedOrderType=[orderTypeArray objectAtIndex:0];
    orderTypeTextField.text=selectedOrderType;
    currentVisit.visitOptions.salesOrder.focParentOrderNumber=nil;
    parentOrderTextField.text=@"";
    
    ordertempletesArray=[[SWDatabaseManager retrieveManager]fetchOrderTempletesForCustomer:currentVisit.visitOptions.customer];
    currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=[[NSMutableArray alloc]initWithArray:ordertempletesArray];
    unfilteredTemplateArray=ordertempletesArray;
    
    [self updateOrderTypeDescriptionView];

}

- (IBAction)proceedButtonTapped:(id)sender {
    
    [self proceedButtonTapped];
}

#pragma mark UICollectionview Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  tempCategoriesArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(180, 100);
}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"did select called");
    
    [orderTempleteTextField setIsReadOnly:NO];
    [self ClearTheTemplateVisitTemplateDetails];
    
    popOverTitle=@"";
    selectedOrderType=[orderTypeArray objectAtIndex:0];
    orderTypeTextField.text=selectedOrderType;
    currentVisit.visitOptions.salesOrder.selectedOrderType=selectedOrderType;
    [self updateOrderTypeDescriptionView];
    
    selectedCategoryIndexPath=indexPath;
    ProductCategories * selectedCategories=[tempCategoriesArray objectAtIndex:indexPath.row];
    tempWareHouseArray=[[NSMutableArray alloc]init];
    currentVisit.visitOptions.salesOrder.selectedCategory=selectedCategories;
    
     parentOrdersArray=[[SWDatabaseManager retrieveManager]fetchParentOrdersforVisitID:currentVisit];
    
    NSLog(@"warehouse in did select %@",tempWareHouseArray);
    
    [tempCategoriesArray setValue:@"N" forKey:@"isSelected"];
    if([selectedCategories.isSelected isEqualToString: @"Y"])
    {
        selectedCategories.isSelected=@"N";
    }
    else{
        selectedCategories.isSelected=@"Y";

    }
    [tempCategoriesArray replaceObjectAtIndex:indexPath.row withObject:selectedCategories];
    [categoryCollectionView reloadData];
    
    //refined templates based on selected category/warehouse which are stored in custom attribute 1 and 2 in templates
    NSPredicate* templatePredicate=[NSPredicate predicateWithFormat:@"SELF.Custom_Attribute_1 == [cd] %@ AND SELF.Custom_Attribute_2 == [cd] %@",selectedCategories.Category,selectedCategories.Org_ID];
    NSLog(@"template predicate is %@", templatePredicate);
    NSMutableArray* filteredTemplateArray=[[unfilteredTemplateArray filteredArrayUsingPredicate:templatePredicate] mutableCopy];
    if (filteredTemplateArray.count>0) {
        
        currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=filteredTemplateArray;
    }
    else{
        currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=[[NSMutableArray alloc]init];
    }
    NSLog(@"filtered predicate array %@", filteredTemplateArray);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"orderCell";
    
    ProductCategories * selectedCategories=[tempCategoriesArray objectAtIndex:indexPath.row];
    
    NSLog(@"check image status %@", selectedCategories.isSelected);
    
    OrderInitializationCollectionViewCell *cell = (OrderInitializationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.selectedImageView.image=[UIImage imageNamed:@""];
    
    if ([selectedCategories.isSelected isEqualToString:@"Y"]) {
        
        [cell setSelectedCellStatus];
    }
    else{
        [cell setDeSelectedCellStatus];
    }
    cell.titleLbl.text=selectedCategories.Item_No;
    cell.descLbl.text=selectedCategories.Description;
    
    return cell;

}

#pragma mark UIAlertView Methods


-(void)displayAlertWithTitle:(NSString*)Title andMessage:(NSString*)message withButtonTitles:(NSArray*)buttonTitlesArray andTag:(NSInteger)alertTag
{
    
    
    
    
    if ([UIAlertController class]) {
        // Use new API to create alert controller, add action button and display it
        alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(Title, nil) message:NSLocalizedString(message, nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle: NSLocalizedString([buttonTitlesArray objectAtIndex:0], nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            //yes button action
            [self alertViewYesbuttonClickedwithTag:alertTag];
        }];
        [alertController addAction: ok];
        
        if (buttonTitlesArray.count>1) {
            
            
            UIAlertAction* cancel = [UIAlertAction actionWithTitle: NSLocalizedString([buttonTitlesArray objectAtIndex:1], nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                //no button action
                [self alertViewNobuttonClickedwithTag:alertTag];
            }];
            [alertController addAction: cancel];
        }
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        // We are running on old SDK as the new class is not available
        // Hide the compiler errors about deprecation and use the class available on older SDK
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIAlertView *alert;
        if (buttonTitlesArray.count>1) {
            alert = [[UIAlertView alloc] initWithTitle:Title
                                               message:message
                                              delegate:self
                                     cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                     otherButtonTitles:[buttonTitlesArray objectAtIndex:1],nil];
            
        }
        else{
            alert = [[UIAlertView alloc] initWithTitle:Title
                                               message:message
                                              delegate:self
                                     cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                     otherButtonTitles:nil];
            
        }
        [alert show];
#pragma clang diagnostic pop
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [self alertViewYesbuttonClickedwithTag:alertView.tag];
    }
    else if(buttonIndex==1)
    {
        [self alertViewNobuttonClickedwithTag:alertView.tag];
    }
}
-(void)alertViewYesbuttonClickedwithTag:(NSInteger)tag
{
    if ([UIAlertController class]) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }
    if (tag==100) {
        //yes tapped in close visit alert check if its from route
        
    }
}

-(void)alertViewNobuttonClickedwithTag:(NSInteger)tag
{
    
    if ([UIAlertController class]) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)ClearTheTemplateVisitTemplateDetails
{
    /* reset the template details*/
    currentVisit.visitOptions.salesOrder.selectedTemplete=nil;
    currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
    currentVisit.visitOptions.salesOrder.salesOrderTempleteArray=unfilteredTemplateArray;
    currentVisit.visitOptions.salesOrder.isTemplateOrder=NO;
    orderTempleteTextField.text=@"";
}


-(void)updateOrderTypeDescriptionView
{
    [self hideOrderTypeAllDetailViews];
    
    parentOrdersView.hidden=YES;
    parentOrderTextField.text=@"";
    parentOrderID=nil;
    currentVisit.visitOptions.salesOrder.focParentOrderNumber=nil;
    
    
    if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kRecommendedOrderPopOverTitle])
    {
        if(![appControl.RO_DESCRIPTION isEqualToString:@""] )
        {
            [RecommandedOrderDescriptionView setHidden:NO];
        }
    }
    else if([currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kReOrderPopOverTitle])
    {
        [OrderDetailView setHidden:NO];
    }
     else if ( [appControl.ENABLE_FOC_ORDER isEqualToString:@"Y"] && ![appControl.FOC_ORDER_MODE isEqualToString:@"I"] && [currentVisit.visitOptions.salesOrder.selectedOrderType isEqualToString:kFOCOrderPopOverTitle])
    {
        [parentOrdersView setHidden:NO];
    }

}
-(void)hideOrderTypeAllDetailViews
{
    [parentOrdersView setHidden:YES];
    [OrderDetailView setHidden:YES];
    [RecommandedOrderDescriptionView setHidden:YES];

}
-(NSMutableArray *)getRecommendedOrderDCItemsForCurrentVisit
{

        NSMutableArray *dcItems  =[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT X.Customer_ID ,X.Site_Use_ID ,X.Inventory_Item_ID AS DCItemID ,X.Organization_ID AS DCOrgID, X.TotalQty AS DCQty, Y.Inventory_Item_ID AS RoItemID,Y.Organization_ID AS ROOrgID, Y.Qty AS ROQty,Y.Qty-X.TotalQty AS BalQty,X.Item_Code FROM (SELECT A.Customer_ID ,A.Site_Use_ID , B.Inventory_Item_ID ,C.Organization_ID , SUM(B.Qty) AS TotalQty,C.Item_Code ,C.Description  FROM TBL_Distribution_Check AS A INNER JOIN TBL_Distribution_Check_Items AS B ON A.DistributionCheck_ID =B.DistributionCheck_ID INNER JOIN TBL_Product AS C On C.Inventory_Item_ID =B.Inventory_Item_ID WHERE A.Visit_ID ='%@' AND B.Is_Available ='Y' GROUP BY A.Customer_ID ,A.Site_Use_ID, B.Inventory_Item_ID ,C.Organization_ID,C.Item_Code ,C.Description)AS x INNER JOIN TBL_Customer_RO AS Y ON X.Customer_ID =Y.Customer_ID AND X.Site_Use_ID =Y.Site_Use_ID AND X.Organization_ID =Y.Organization_ID AND X.Inventory_Item_ID =Y.Inventory_Item_ID",currentVisit.Visit_ID]];
    return  dcItems;

}

-(NSMutableArray *)getDCReOrderItemsForCurrentVisit
{
    NSMutableArray *dcItems  =[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Customer_ID, A.Site_Use_ID, B.Inventory_Item_ID, C.Organization_ID, SUM(B.Qty) AS TotalQty, C.Item_Code, C.Description FROM TBL_Distribution_Check AS A INNER JOIN TBL_Distribution_Check_Items AS B ON A.DistributionCheck_ID = B.DistributionCheck_ID INNER JOIN TBL_Product AS C On C.Inventory_Item_ID = B.Inventory_Item_ID WHERE A.Visit_ID = '%@' AND B.Reorder ='Y' GROUP BY A.Customer_ID, A.Site_Use_ID, B.Inventory_Item_ID, C.Organization_ID, C.Item_Code, C.Description",currentVisit.Visit_ID]];
    return dcItems;
}

@end
