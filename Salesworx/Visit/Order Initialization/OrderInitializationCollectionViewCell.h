//
//  OrderInitializationCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SalesWorxSingleLineLabel.h"
#import "MedRepElementTitleLabel.h"

@interface OrderInitializationCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageView;

@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *descLbl;
-(void)setSelectedCellStatus;
-(void)setDeSelectedCellStatus;
@end
