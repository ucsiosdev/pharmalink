//
//  OrderInitializationCollectionViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "OrderInitializationCollectionViewCell.h"
#import "SWDefaults.h"

@implementation OrderInitializationCollectionViewCell



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"OrderInitializationCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
    
}

- (void)awakeFromNib {
    // Shadow and Radius
    self.layer.borderWidth=0.5;
    self.layer.cornerRadius=4.0f;
    self.layer.borderColor=[kVisitOptionsBorderColor CGColor];
    self.layer.shadowColor = [kVisitOptionsBorderColor CGColor];
//    self.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.layer.shadowOpacity = 1.0f;
//    self.layer.shadowRadius = 0.0f;
//    self.layer.masksToBounds = NO;
//    self.layer.cornerRadius = 4.0f;
    
}

-(void)setSelectedCellStatus
{
    self.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    [self.descLbl setTextColor:MedRepMenuTitleSelectedCellTextColor];
    [self.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
}
-(void)setDeSelectedCellStatus
{
    
    self.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    self.descLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    [self.contentView setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
}

@end
