//
//  SalesWorxOrderInitializationParentOrderViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxOrderInitializationParentOrderTableViewCell.h"

@protocol ParentOrderDelegate <NSObject>

-(void)parentOrderDidSelectRow:(NSIndexPath*)selectedIndexPath;

@end

@interface SalesWorxOrderInitializationParentOrderViewController : UIViewController

{
    id parentOrderDelegate;
    NSMutableDictionary * currentDict;
}

@property (strong, nonatomic) IBOutlet UITableView *parentOrderTableView;
@property(strong,nonatomic) NSMutableArray* popOverContentArray;
@property(nonatomic) id parentOrderDelegate;
@property(strong,nonatomic) UIPopoverController *popOverController;
@property(strong,nonatomic) NSString* titleKey;
@end
