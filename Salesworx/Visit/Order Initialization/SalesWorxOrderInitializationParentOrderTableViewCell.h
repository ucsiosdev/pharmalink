//
//  SalesWorxOrderInitializationParentOrderTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxOrderInitializationParentOrderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *orderReferenceNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *orderAmountLbl;
@property(nonatomic) BOOL isHeader;
@end
