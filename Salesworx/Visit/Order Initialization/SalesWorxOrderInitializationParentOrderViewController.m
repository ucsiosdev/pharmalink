//
//  SalesWorxOrderInitializationParentOrderViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxOrderInitializationParentOrderViewController.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
@interface SalesWorxOrderInitializationParentOrderViewController ()

@end

@implementation SalesWorxOrderInitializationParentOrderViewController
@synthesize popOverContentArray,popOverController,titleKey;
- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"FOC Parent Order"];
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=closeButton;
    // Do any additional setup after loading the view from its nib.
}
-(void)closeTapped
{
    [popOverController dismissPopoverAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString* identifier=@"parentCell";
    SalesWorxOrderInitializationParentOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxOrderInitializationParentOrderTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.isHeader=YES;
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.orderReferenceNumberLbl.isHeader=YES;
    cell.orderAmountLbl.isHeader=YES;
    cell.orderReferenceNumberLbl.text=NSLocalizedString(@"Order Number", nil);
    cell.orderAmountLbl.text=NSLocalizedString(@"Order Amount", nil);
    
    return cell;
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (popOverContentArray.count>0) {
        return popOverContentArray.count;
    }
    else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorColor=kUITableViewSaperatorColor;
    
    static NSString* identifier=@"parentCell";
    SalesWorxOrderInitializationParentOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxOrderInitializationParentOrderTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
     currentDict=[popOverContentArray objectAtIndex:indexPath.row];
    cell.orderReferenceNumberLbl.text=[NSString stringWithFormat:@"%@",[currentDict valueForKey:@"Orig_Sys_Document_Ref"]];
    cell.orderAmountLbl.text=[[NSString stringWithFormat:@"%@",[currentDict valueForKey:@"Order_Amt"]] currencyString];
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.parentOrderDelegate respondsToSelector:@selector(parentOrderDidSelectRow:)]) {
        
        [self.parentOrderDelegate parentOrderDidSelectRow:indexPath];
    }
    
    
    [popOverController dismissPopoverAnimated:YES];
    
//    [[NSNotificationCenter defaultCenter]postNotificationName:kFOCOrderNotificationName object:currentDict];
//    [popOverController dismissPopoverAnimated:YES];
    
}

@end
