//
//  SalesWorxOrderInitializationOrderTypeViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SalesWorxTableView.h"
#import "SalesWorxOrderInitializationParentOrderViewController.h"

@protocol SalesWorxOrderInitializationOrderTypeDelegate <NSObject>

-(void)didSelectOrderType:(id)selectedOrderType;

@end

@interface SalesWorxOrderInitializationOrderTypeViewController : UIViewController
{
    id selectedOrderTypeDelegate;
}
@property(strong,nonatomic)NSMutableArray* orderTypeArray;
@property(strong,nonatomic) NSMutableArray* parentOrdersArray;
@property(strong,nonatomic) NSString* titleKey;
@property (strong, nonatomic) IBOutlet UITableView *orderTypeTableView;
@property(strong,nonatomic) UIPopoverController *popOverController;
@property(nonatomic) id selectedOrderTypeDelegate;
@end
