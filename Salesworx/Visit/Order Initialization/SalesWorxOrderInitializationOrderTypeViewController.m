//
//  SalesWorxOrderInitializationOrderTypeViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxOrderInitializationOrderTypeViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"


@interface SalesWorxOrderInitializationOrderTypeViewController ()

@end

@implementation SalesWorxOrderInitializationOrderTypeViewController
@synthesize orderTypeArray,titleKey,popOverController,orderTypeTableView,parentOrdersArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:titleKey];
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=closeButton;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)closeTapped
{
    [popOverController dismissPopoverAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView Methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return orderTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.font=MedRepSingleLineLabelFont;
    
    cell.textLabel.textColor=MedRepDescriptionLabelFontColor;
    
    cell.textLabel.text=[orderTypeArray objectAtIndex:indexPath.row];
    
    return cell;
    
    }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[orderTypeArray objectAtIndex:indexPath.row]isEqualToString:kFOCOrderPopOverTitle]) {
        SalesWorxOrderInitializationParentOrderViewController * popOverVC=[[SalesWorxOrderInitializationParentOrderViewController alloc]init];
        popOverVC.popOverContentArray=parentOrdersArray;
        popOverVC.popOverController=popOverController;
        [self.navigationController pushViewController:popOverVC animated:YES];
  
    }
    
   
}


@end
