//
//  SalesWorxOrderInitializationViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepDefaults.h"
#import "SalesWorxSingleLineLabel.h"
#import "SalesWorxCustomClass.h"
#import "MedRepProductCodeLabel.h"
#import "MedRepHeader.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepTextField.h"
#import "SalesOrderNewViewController.h"
#import "SalesWorxOrderInitializationParentOrderViewController.h"
#import "SalesWorxOrderInitializationOrderTypeViewController.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxSalesOrderViewController.h"
#import "SalesWorxDescriptionLabel1.h"

#define kCategoryViewHeightConstant 149
#define kOrdertemplateViewHeightConstant 113
#define kOrderTypeViewheightConstant 113
#define kAllViewsTopConstant 8



@interface SalesWorxOrderInitializationViewController : UIViewController<UIPopoverControllerDelegate,UITextFieldDelegate,ParentOrderDelegate,SalesWorxOrderInitializationOrderTypeDelegate>
{
    
    IBOutlet NSLayoutConstraint *categoryViewHeightConstraint;
    
    IBOutlet NSLayoutConstraint *categoryViewTopConstraint;
    
    IBOutlet NSLayoutConstraint *templateViewHeightConstraint;
    
    IBOutlet NSLayoutConstraint *templateViewTopConstraint;
    
    IBOutlet NSLayoutConstraint *orderTypeViewHeightConstraint;
    
    IBOutlet NSLayoutConstraint *orderTypeViewTopConstraint;
    
    IBOutlet SalesWorxDropShadowView *categoriesView;
    IBOutlet NSLayoutConstraint *parentOrderViewHeightConstraint;
    IBOutlet UIView *parentOrdersView;
    IBOutlet MedRepTextField *categoryTextField;
    
    IBOutlet MedRepTextField *warehouseTextField;
    
    IBOutlet MedRepTextField *orderTempleteTextField;
    
    IBOutlet MedRepTextField *parentOrderTextField;
    IBOutlet MedRepTextField *orderTypeTextField;
    
    IBOutlet UICollectionView * categoryCollectionView;
    
    NSMutableArray* unfilteredTemplateArray;
    
    NSMutableArray* parentOrdersArray;
    
    NSMutableDictionary* tempWareHouseDict;
    NSMutableArray * tempWareHouseArray;
    NSMutableArray* tempCategoriesArray;
    NSMutableArray* unfilteredCategories;
    NSInteger selectedTemplateIndexPath;
    
    NSMutableArray* ordertempletesArray;
    NSMutableArray* templateLineItemsArray;
    NSMutableArray* orderTypeArray;
    AppControl * appControl;
    
    NSString* parentOrderID;
    NSString* selectedOrderType;
    
    NSIndexPath * selectedCategoryIndexPath;
    
    IBOutlet SalesWorxImageView *customerStatusImageView;
    IBOutlet UILabel *availableBalanceLbl;
    IBOutlet MedRepProductCodeLabel *customerNumberLbl;
    IBOutlet MedRepHeader *customerNameLbl;
    IBOutlet NSLayoutConstraint *orderTempleteViewHeightConstraint;
    BOOL enableOrderTemplate;
    BOOL enableOrderType;
    UIPopoverController * categoryPopOverController;
    NSString* popOverTitle;
    
    UIAlertController * alertController;
    
    NSMutableArray* conventionalTemplateDataArray;
    
    NSInteger indexPathTemplate;
    
    SalesOrderTemplete* currentTemplate;
    
    
    NSString* tempParentViewString;
    
    //app control flag bools
    
    IBOutlet MedRepButton *recommendOrderButton;
    
    IBOutlet UIView *OrderDetailView;
    IBOutlet NSLayoutConstraint *orderDetailViewLeadingConstraint;
    IBOutlet UILabel *lblOrderNo;
    IBOutlet UILabel *lblOrderDate;
    IBOutlet UILabel *lblOrderValue;
    
    IBOutlet UIView *RecommandedOrderDescriptionView;
    IBOutlet UITextView *recommendedOrderNotesTextView;
    BOOL isRecommendedOrderAvailableForTheCustomer;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
}
@property(strong,nonatomic) SalesWorxVisit* currentVisit;
- (IBAction)resetButtonTapped:(id)sender;
- (IBAction)proceedButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *orderTempleteView;

@property (strong, nonatomic) IBOutlet UIView *orderTypeView;

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString Content:(NSMutableArray*)contentArray;
@end
