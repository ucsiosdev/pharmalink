//
//  OpenVisitAlertViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 11/7/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SalesWorxTableView.h"
#import "MedRepCustomClass.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@protocol OpenVisitDelegate <NSObject>

-(void)selectedOpenVisitReason:(NSInteger)selectedReasonCode;
@end
@interface OpenVisitAlertViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id selectedReasonDelegate;

    IBOutlet UILabel *messageLabel;
    NSMutableArray* buttonTitlesArray;
}
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *headerTitleLabel;
@property (strong, nonatomic) IBOutlet SalesWorxTableView *contentTableView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) Customer * openVisitCustomer;
@property (strong, nonatomic) Customer * selectedCustomer;
@property(nonatomic) id selectedReasonDelegate;


- (IBAction)cancelButtonTapped:(id)sender;
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType;


@end


