//
//  SalesWorxVisitOptionsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxVisitOptionsViewController.h"
#import "SWDuesView.h"
#import "SalesWorxCustomerDuesViewController.h"
#import "ProductSaleCategoryViewController.h"
#import "SalesWorxAllSurveyViewController.h"
#import "SalesWorxSurveyViewController.h"
#import "SalesWorxCreateNotePopoverViewController.h"
#import "SalesWorxVisitOptionsNearByCustomersViewController.h"
#import "SalesWorxReportsViewController.h"
#import "SalesWorxBackgroundSyncManager.h"

@interface SalesWorxVisitOptionsViewController ()

@end

@implementation SalesWorxVisitOptionsViewController
@synthesize visitOptionsCollectionViewArray,visitOptionsCollectionView,selectedCustomer,salesWorxVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    [visitOptionsCollectionView registerClass:[SalesWorxVisitOptionsCollectionViewCell class] forCellWithReuseIdentifier:@"visitOptionsCell"];
    
//    if (![SWDefaults currentVisitID]) {
//        salesWorxVisit.Visit_ID=[NSString createGuid];
//        
//        [SWDefaults setCurrentVisitID:salesWorxVisit.Visit_ID];
//    }
    appControl = [AppControl retrieveSingleton];
    isUserSeletedAnyOption=NO;
    // Do any additional setup after loading the view from its nib.
    appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveVisitOtionCompletiontNotification:)
                                                 name:KVisitOtionCompletionNotificationNameStr
                                               object:nil];
    
    if (!_timer) {
        [_timer fire];
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(performBackgroundTask) userInfo:nil repeats:YES];

    }
    
    NSLog(@"ExpiryDate=%@",salesWorxVisit.visitOptions.customer.Trade_License_Expiry);
    if([[AppControl retrieveSingleton].SHOW_ALERT_FOR_TRD_LIC_EXP isEqualToString:KAppControlsYESCode]){
        [self checkTradeLicenseExpiry];
    }
}

- (IBAction)stopTimer {
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}

-(void)checkTradeLicenseExpiry{
    NSString *dateString = self.salesWorxVisit.visitOptions.customer.Trade_License_Expiry; //@"2019-08-15T07:29:55.850Z";

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    NSDate *todayDate = [NSDate date];
    
    NSDateComponents *components;
    NSInteger days;
    if(date!=nil){
        components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                 fromDate: todayDate toDate: date options: 0];
    }
    NSInteger positiveDay = labs([components day]);
    days = positiveDay+1;
    if(date!=nil){
        if(days<=30 && ([date compare:todayDate] == NSOrderedAscending)){
            [SWDefaults showAlertAfterHidingKeyBoard:@"Warning" andMessage:@"Trade license as expired" withController:self];
            NSLog(@"ShowAlert");
        }
        else if(days<=30 && ([date compare:todayDate] == NSOrderedDescending)){
            [SWDefaults showAlertAfterHidingKeyBoard:@"Warning" andMessage:@"Trade license will expire soon" withController:self];
        }else{
            NSLog(@"Trade license not expired");
    }
    }
}

- (void)performBackgroundTask
{
    [appDel reminderToDo :salesWorxVisit.visitOptions.customer.Ship_Customer_ID];
}

- (void) receiveVisitOtionCompletiontNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    NSString* option = [[notification userInfo] valueForKey:KVisitOtionNotification_DictionaryKeyStr] ;

    if ([[notification name] isEqualToString:KVisitOtionCompletionNotificationNameStr])
    {
        if([option isEqualToString:kSalesOrderTitle])
            salesWorxVisit.isSalesOrderCompleted=YES;
        else if ([option isEqualToString:kDistributionCheckTitle])
            salesWorxVisit.isDistributionCheckCompleted=YES;
        else if([option isEqualToString:kManageOrdersTitle])
            salesWorxVisit.isManageOrderCompleted=YES;
        else if([option isEqualToString:kReturnsTitle])
            salesWorxVisit.isReturnsCompleted=YES;
        else if([option isEqualToString:kSurveyTitle])
            salesWorxVisit.isSurveyCompleted=YES;
        else if([option isEqualToString:kCollectionTitle])
            salesWorxVisit.isCollectionCompleted=YES;
        else if([option isEqualToString:kOrderHistoryTitle])
            salesWorxVisit.isOrderHistoryAccessed=YES;
        else if([option isEqualToString:kTodoTitle])
            salesWorxVisit.isTODOCompleted=YES;
        else if ([option isEqualToString:kReportsTitle])
            salesWorxVisit.isReportsAccessed=YES;
        else if ([option isEqualToString:kProductMediaTitle])
            salesWorxVisit.isProductMediaAccessed=YES;
    }
}

- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
#ifdef DEBUG
   // [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(samplePush) userInfo:nil repeats:NO];
#endif

    self.view.backgroundColor=UIViewControllerBackGroundColor;
   // if (self.isMovingToParentViewController) {
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Visit Options", nil)];
    
    visitOptionsCollectionViewArray=[[NSMutableArray alloc]init];
    
    NSLog(@"visit type in options is %@", self.salesWorxVisit.visitOptions.customer.Visit_Type);
    
    
    
    BOOL customerLevelVisitOptions=[SWDefaults isCustomerLevelVisitOptionsFlagEnabled];
    
    NSMutableArray * customerLevelVisitOptionsArray=[[NSMutableArray alloc]init];
    
    if (customerLevelVisitOptions == YES) {
        
        customerLevelVisitOptionsArray=[[SWDatabaseManager retrieveManager]fetchVisitOptionsFlagsforCustomer:salesWorxVisit.visitOptions.customer.Ship_Customer_ID];
        NSLog(@"customer level visitoptions array is %@", customerLevelVisitOptionsArray);
        
    }
    
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]];
        PDAComponents=[PDARIGHTS componentsSeparatedByString:@","];
    }
    
    visitOptionsFlagsArray=[[SalesWorxVisitOptionsManager retrieveSingleton] fetchVisitOptionsforCustomer:salesWorxVisit];
    
    if (visitOptionsFlagsArray.count>0) {
        
            if ([visitOptionsFlagsArray containsObject:KDistributionCheckDisplayLevelCode]) {
                NSMutableDictionary * distributionCheckDict=[[NSMutableDictionary alloc]init];
                [distributionCheckDict setValue:kDistributionCheckTitle forKey:@"Title"];
                [distributionCheckDict setValue:@"VisitOptions_DC_Active" forKey:@"ThumbNailImage"];
                    [visitOptionsCollectionViewArray addObject:distributionCheckDict];
                if ([self.salesWorxVisit.visitOptions.customer.Visit_Type isEqualToString:kTelephonicVisitTitle]) {
                    [distributionCheckDict setValue:@"Y" forKey:@"Disabled"];
                }
            }
            
            if ([visitOptionsFlagsArray containsObject:KSalesOrderDisplayLevelCode]) {
                NSMutableDictionary * salesOrderDict=[[NSMutableDictionary alloc]init];
                [salesOrderDict setValue:kSalesOrderTitle forKey:@"Title"];
                [salesOrderDict setValue:@"VisitOptions_SO_Active" forKey:@"ThumbNailImage"];
                if([appControl.DISABLE_BLOCKED_CUSTOMER_SALES_ORDER_ENTRY isEqualToString:KAppControlsYESCode] &&
                   ![salesWorxVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]){
                    [salesOrderDict setValue:@"Y" forKey:@"Disabled"];
                    [salesOrderDict setValue:@"VisitOptions_SO_InActive" forKey:@"ThumbNailImage"];

                }
                [visitOptionsCollectionViewArray addObject:salesOrderDict];
                
            }
          
            
            if ([visitOptionsFlagsArray containsObject:kManageOrderDisplayLevelCode]) {
                NSMutableDictionary * manageOrdersDict=[[NSMutableDictionary alloc]init];
                [manageOrdersDict setValue:kManageOrdersTitle forKey:@"Title"];
                [manageOrdersDict setValue:@"VisitOptions_MO_Active" forKey:@"ThumbNailImage"];
                if([appControl.DISABLE_BLOCKED_CUSTOMER_SALES_ORDER_ENTRY isEqualToString:KAppControlsYESCode] &&
                   ![salesWorxVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]){
                    [manageOrdersDict setValue:@"Y" forKey:@"Disabled"];
                    [manageOrdersDict setValue:@"VisitOptions_MO_InActive" forKey:@"ThumbNailImage"];
                }
                [visitOptionsCollectionViewArray addObject:manageOrdersDict];
            }
            
            if ([visitOptionsFlagsArray containsObject:kOrderHistoryDisplayLevelCode]) {
                NSMutableDictionary * orderHistoryDict=[[NSMutableDictionary alloc]init];
                [orderHistoryDict setValue:kOrderHistoryTitle forKey:@"Title"];
                [orderHistoryDict setValue:@"VisitOptions_OH_Active" forKey:@"ThumbNailImage"];
                    [visitOptionsCollectionViewArray addObject:orderHistoryDict];
                }
            
            if ([visitOptionsFlagsArray containsObject:KReturnsDisplayLevelCode]) {
                NSMutableDictionary * returnDict=[[NSMutableDictionary alloc]init];
                [returnDict setValue:kReturnsTitle forKey:@"Title"];
                [returnDict setValue:@"VisitOptions_Return_Active" forKey:@"ThumbNailImage"];
                [visitOptionsCollectionViewArray addObject:returnDict];
            }

            if ([visitOptionsFlagsArray containsObject:KPaymentCollectionDisplayLevelCode]) {
                NSMutableDictionary * collectionsDict=[[NSMutableDictionary alloc]init];
                [collectionsDict setValue:kCollectionTitle forKey:@"Title"];
                [collectionsDict setValue:@"VisitOptions_COLLE_Active" forKey:@"ThumbNailImage"];
                    [visitOptionsCollectionViewArray addObject:collectionsDict];
                if ([self.salesWorxVisit.visitOptions.customer.Visit_Type isEqualToString:kTelephonicVisitTitle]) {
                    [collectionsDict setValue:@"Y" forKey:@"Disabled"];
                    [collectionsDict setValue:@"VisitOptions_COLLE_InActive" forKey:@"ThumbNailImage"];

                }
            }
            
            if ([visitOptionsFlagsArray containsObject:KSurveyDisplayLevelCode]) {
                NSMutableDictionary * surveyDict=[[NSMutableDictionary alloc]init];
                [surveyDict setValue:kSurveyTitle forKey:@"Title"];
                [surveyDict setValue:@"VisitOptions_Survey_Active" forKey:@"ThumbNailImage"];
                [visitOptionsCollectionViewArray addObject:surveyDict];
                
                if ([self.salesWorxVisit.visitOptions.customer.Visit_Type isEqualToString:kTelephonicVisitTitle]) {
                    [surveyDict setValue:@"Y" forKey:@"Disabled"];
                }
            }
            
            if ([visitOptionsFlagsArray containsObject:kTODODisplayLevelCode]) {
                NSMutableDictionary * toDoDict=[[NSMutableDictionary alloc]init];
                [toDoDict setValue:kTodoTitle forKey:@"Title"];
                [toDoDict setValue:@"VisitOptions_ToDo_Active" forKey:@"ThumbNailImage"];
                [visitOptionsCollectionViewArray addObject:toDoDict];
            }
        
        //Reports
            if ([visitOptionsFlagsArray containsObject:kReportsDisplayLevelCode]) {
                NSMutableDictionary * reportsDict=[[NSMutableDictionary alloc]init];
                [reportsDict setValue:kReportsTitle forKey:@"Title"];
                [reportsDict setValue:@"VisitOptions_ToDo_Active" forKey:@"ThumbNailImage"];
                [visitOptionsCollectionViewArray addObject:reportsDict];
            }
        
        
        
//            if ([visitOptionsFlagsArray containsObject:kMerchandisingDisplayLevelCode]) {
//                NSMutableDictionary * toDoDict=[[NSMutableDictionary alloc]init];
//                [toDoDict setValue:kMerchandisingTitle forKey:@"Title"];
//                [toDoDict setValue:@"VisitOptions_Merch_Active" forKey:@"ThumbNailImage"];
//                [visitOptionsCollectionViewArray addObject:toDoDict];
//            }
        
            /**Visit Options near by customers*/
            NSMutableDictionary * toDoDict=[[NSMutableDictionary alloc]init];
            [toDoDict setValue:kNearByCustomersTitle forKey:@"Title"];
            [toDoDict setValue:@"VistOptions_NearByLocations_Active" forKey:@"ThumbNailImage"];
       //     [visitOptionsCollectionViewArray addObject:toDoDict];
        
        if ([appControl.FS_ENABLE_PRODUCT_MEDIA isEqualToString:KAppControlsYESCode]) {
            NSMutableDictionary *productMediaDict = [[NSMutableDictionary alloc]init];
            [productMediaDict setValue:kProductMediaTitle forKey:@"Title"];
            [productMediaDict setValue:@"VisitOptions_Product_Media" forKey:@"ThumbNailImage"];
            [visitOptionsCollectionViewArray addObject:productMediaDict];
        }
        
    }
        
        
        
        
    
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
   
    
    
    customerNameLbl.text=salesWorxVisit.visitOptions.customer.Customer_Name ;
    customerCodeLbl.text=salesWorxVisit.visitOptions.customer.Customer_No;
    lblVisitType.text = NSLocalizedString(salesWorxVisit.visitOptions.customer.Visit_Type, nil);

    __block NSString *lastVisitedDateStr=[[NSString alloc] init];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:salesWorxVisit.visitOptions.customer];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            lblLastVisitedOn.text=lastVisitedDateStr.length==0?KNotApplicable:lastVisitedDateStr;
        });
    });
    

    
   
//    if ([appControl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
//        OnsiteOptionsViewController* onsiteOptions=[[OnsiteOptionsViewController alloc]init];
//        onsiteOptions.delegate=self;
//        [self presentPopupViewController:onsiteOptions animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
//    }
        customerDict=[SWDefaults fetchCurrentCustDict:salesWorxVisit.visitOptions.customer];

   // }
    
    single = [Singleton retrieveSingleton];

    
   
    [visitOptionsCollectionView reloadData];
    
    
        UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close Visit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeVisitTapped)];
        closeVisitButton.tintColor=[UIColor whiteColor];
        [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
        forState:UIControlStateNormal];

        self.navigationItem.leftBarButtonItem=closeVisitButton;
    
    if ([appControl.ENABLE_FSR_VISIT_NOTES isEqualToString:KAppControlsYESCode] && [appControl.SHOW_FS_VISIT_CUSTOMER_CONTACT
                                                    isEqualToString:KAppControlsYESCode]) {
        createNoteButton = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NoteIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowVisitNotes)];
        [createNoteButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        
        contactDetailsButton = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ContactIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(showContactDetailsPoPover)];
        [contactDetailsButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

        self.navigationItem.rightBarButtonItems=@[createNoteButton,contactDetailsButton];
    }
   else if ([appControl.ENABLE_FSR_VISIT_NOTES isEqualToString:KAppControlsYESCode])
    {
        createNoteButton = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NoteIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowVisitNotes)];
        [createNoteButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = createNoteButton;


    }
  else  if ([appControl.SHOW_FS_VISIT_CUSTOMER_CONTACT
         isEqualToString:KAppControlsYESCode]) {
        contactDetailsButton = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ContactIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(showContactDetailsPoPover)];
        [contactDetailsButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = contactDetailsButton;
    
    }
    
    
    
    if ([salesWorxVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerStatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerStatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    
    NSLog(@"avbl is %@",salesWorxVisit.visitOptions.customer.Avail_Bal);
    
    availableBalanceLbl.textColor=[UIColor colorWithRed:(59.0/255.0) green:(142.0/255.0) blue:(210.0/255.0) alpha:1];
    
    availableBalanceLbl.text=[[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Avail_Bal] currencyString];
    
    
    if(!self.isMovingToParentViewController)
    {
        
       __block NSInteger ordersCount=0;
        __block NSString *ordersVlaue=@"0";

        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
             ordersCount= [[SWDatabaseManager retrieveManager]dbGetOrderCountForVisit:salesWorxVisit.Visit_ID];
            ordersVlaue=[[SWDatabaseManager retrieveManager]dbGetOrdersValueForVisit:salesWorxVisit.Visit_ID];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if(ordersCount>0)
                {
                    [OrderDetailsView setHidden:NO];
                    OrdersTakenNumberLabel.text=[NSString stringWithFormat:@"%ld",(long)ordersCount];
                    OrdersValueNumberLabel.text=[ordersVlaue currencyString];
                }
                else
                {
                    [OrderDetailsView setHidden:YES];
                }

            });
        });

    }
    else
    {
        [OrderDetailsView setHidden:YES];
    }
    if(self.isMovingToParentViewController){
        appDel.CurrentRunningSalesWorxVisit=salesWorxVisit;
        
        NSLog(@"appControl.START_VISIT_ON_ACTIVITY value is %@",appControl.START_VISIT_ON_ACTIVITY);
        if([appControl.START_VISIT_ON_ACTIVITY isEqualToString:KAppControlsNOCode]){
            [self AddVisitDetailsToTheDatabaseIfNotAdded];
        }

    }
}

-(void)showContactDetailsPoPover
{
    isUserSeletedAnyOption=YES;
    SalesWorxVisitOptionsContactDetailsPopOverViewController *createNoteVC = [[SalesWorxVisitOptionsContactDetailsPopOverViewController alloc]init];
    createNoteVC.currentVisit = salesWorxVisit;
    createNoteVC.contactDetailsDelegate=self;
    UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:createNoteVC];
    mapPopUpViewController.navigationBarHidden = YES;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    createNoteVC.view.backgroundColor = [UIColor clearColor];
    createNoteVC.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}
-(void)viewDidAppear:(BOOL)animated
{
    /** checking bonus group*/
    salesWorxVisit.visitOptions.customer.isBonusAvailable = YES;
    if ([appControl.ENABLE_BONUS_GROUPS isEqualToString:KAppControlsYESCode])
    {
        NSMutableArray *asstBnsPlans=[[NSMutableArray alloc]init];
        NSMutableArray *sampleBnsPlans=[[NSMutableArray alloc]init];
        
        if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
            NSString *asstBnsPlansQuery = [NSString stringWithFormat:@"Select A.* from TBL_BNS_Assortment_Slabs AS A INNER JOIN TBL_Customer_Bonus_Map As B ON B.Plan_type='Assortment' AND B.Bonus_Plan_ID=A.Assortment_Plan_ID Where B.Customer_ID = '%@' AND B.Site_Use_ID = '%@'",salesWorxVisit.visitOptions.customer.Customer_ID,salesWorxVisit.visitOptions.customer.Site_Use_ID];
           asstBnsPlans= [[[SWDatabaseManager retrieveManager]fetchDataForQuery:asstBnsPlansQuery] mutableCopy];

        }
       NSString * sampleBnsPlansQuery = [NSString stringWithFormat:@"Select A.* from TBL_BNS_Promotion AS A INNER JOIN TBL_Customer_Bonus_Map As B ON B.Plan_type='Simple' AND B.Bonus_Plan_ID=A.BNS_Plan_ID Where B.Customer_ID = '%@' AND B.Site_Use_ID = '%@'",salesWorxVisit.visitOptions.customer.Customer_ID,salesWorxVisit.visitOptions.customer.Site_Use_ID];
        sampleBnsPlans= [[[SWDatabaseManager retrieveManager]fetchDataForQuery:sampleBnsPlansQuery] mutableCopy];
        
        if (asstBnsPlans.count==0 && sampleBnsPlans.count==0)
        {
            salesWorxVisit.visitOptions.customer.isBonusAvailable = NO;
        }
        else
        {
            salesWorxVisit.visitOptions.customer.isBonusAvailable = YES;
        }
    }
    /*bonus group checking completed*/
}

//-(void)closeVisitTapped1
//{
//    
//    
//    NSString* currentVisitID=[SWDefaults currentVisitID];
//    
//    
//    
//
//    if (!currentVisitID ) {
//        
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else if([appControl.SURVEY_MANDATORY isEqualToString:KAppControlsYESCode] &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KSurveyPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode])) /** TO -DO condition will be replace with the app control flag*/
//    {
//        
//        
//        
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        [self.navigationItem.leftBarButtonItem setEnabled:NO];
//        __block BOOL isSurveysCompleted=YES;
//        
//        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//            /** checking for pending surveys*/
//            NSArray *surveyArray=[[SWDatabaseManager retrieveManager]selectSurveyWithCustomerID:salesWorxVisit.visitOptions.customer.Customer_ID andSiteUseID:salesWorxVisit.visitOptions.customer.Site_Use_ID];
//            for (NSInteger i=0; i<surveyArray.count; i++) {
//                SurveyDetails *tempsurvey=[surveyArray objectAtIndex:i];
//                isSurveysCompleted=[self checkSurveyStatusForSurveyID:tempsurvey.Survey_ID];
//                if(!isSurveysCompleted)
//                {
//                    break;
//                }
//            }
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                [MBProgressHUD hideHUDForView:self.view animated:NO];
//                
//                [self.navigationItem.leftBarButtonItem setEnabled:YES];
//                if(!isSurveysCompleted)
//                {
//                    [SWDefaults showAlertAfterHidingKeyBoard:@"Survey Mandatory" andMessage:@"please complete survey before closing visit." withController:self];
//
//                }
//                else
//                {
//                    [self showConfirmationAlertForCloseVisit];
//                    
//                }
//            });
//        });
//        
//        
//    }
//    
//    else if([appControl.DISTRIBUTIONCHECK_MANDATORY isEqualToString:KAppControlsYESCode] &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KSurveyPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode])) /** TO -DO condition will be replace with the app control flag*/
//    {
//        
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        [self.navigationItem.leftBarButtonItem setEnabled:NO];
//        if(salesWorxVisit.isDistributionCheckCompleted)
//        {
//            [self showConfirmationAlertForCloseVisit];
//
//        }
//        else
//        {
//            [SWDefaults showAlertAfterHidingKeyBoard:@"Distribution Check Mandatory" andMessage:@"please complete Distribution Check before closing visit." withController:self];
//
//        }
//       
//    }
//    //pavan data
//    else if([appControl.SHOW_SURVEY_ALERT isEqualToString:KAppControlsYESCode] &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KSurveyPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode])) /** TO -DO condition will be replace with the app control flag*/
//    {
//        
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        [self.navigationItem.leftBarButtonItem setEnabled:NO];
//       __block BOOL isSurveysCompleted=YES;
//        
//        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//            /** checking for pending surveys*/
//            NSArray *surveyArray=[[SWDatabaseManager retrieveManager]selectSurveyWithCustomerID:salesWorxVisit.visitOptions.customer.Customer_ID andSiteUseID:salesWorxVisit.visitOptions.customer.Site_Use_ID];
//            for (NSInteger i=0; i<surveyArray.count; i++) {
//                SurveyDetails *tempsurvey=[surveyArray objectAtIndex:i];
//                isSurveysCompleted=[self checkSurveyStatusForSurveyID:tempsurvey.Survey_ID];
//                if(!isSurveysCompleted)
//                {
//                    break;
//                }
//            }
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                [MBProgressHUD hideHUDForView:self.view animated:NO];
//
//                [self.navigationItem.leftBarButtonItem setEnabled:YES];
//                if(!isSurveysCompleted)
//                {
//                    UIAlertAction* noAction = [UIAlertAction
//                                               actionWithTitle:KAlertNoButtonTitle
//                                               style:UIAlertActionStyleDefault
//                                               handler:^(UIAlertAction * action)
//                                               {
//                                                   [self closeVisitWithEndDate];
//                                               }];
//                    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:[SWDefaults GetDefaultCancelAlertActionWithTitle:KAlertYESButtonTitle],noAction ,nil];
//                    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KVisitOptionsPendingSurveyAlertTitleStr andMessage:KVisitOptionsPendingSurveyAlertMessageStr andActions:actionsArray withController:self];
//                }
//                else
//                {
//                    [self showConfirmationAlertForCloseVisit];
//
//                }
//            });
//        });
//    }
//    else
//    {
//        [self showConfirmationAlertForCloseVisit];
//        
//    }
//
//}

-(void)closeVisitTapped
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    
    [SalesWorxiBeaconManager destroyBeaconSingleton];
    
    [FIRAnalytics logEventWithName:@"Close Visit Tapped From Visit Options" parameters:nil];
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    else
    {
        
        
        NSString* currentVisitID=[SWDefaults currentVisitID];
        //    __block BOOL isSurveysCompleted=YES;
        __block BOOL isDChaveValidItems=YES;
        __block BOOL isAnySurveyCompleted=NO;
        
        BOOL isOnsiteVisit= NO;//[SWDefaults fetchOnsiteVisitStatus];
        
        NSString * visitTypeString = [SWDefaults getValidStringValue:salesWorxVisit.visitOptions.customer.Visit_Type];
        
        if ([visitTypeString isEqualToString:kOnsiteVisitTitle])
        {
            isOnsiteVisit = YES;
        }
        
        NSLog(@"is survey mandatory? %@, is distribution check mandatory? %@",appControl.SURVEY_MANDATORY,appControl.DISTRIBUTIONCHECK_MANDATORY);
        
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            if (!currentVisitID ) {
                
            }
            else
            {
                if(([appControl.SURVEY_MANDATORY isEqualToString:KAppControlsYESCode]||[appControl.SHOW_SURVEY_ALERT isEqualToString:KAppControlsYESCode]) &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KSurveyPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode]) && [visitOptionsFlagsArray containsObject:KSurveyDisplayLevelCode] && isOnsiteVisit ==YES) /** TO -DO condition will be replace with the app control flag*/
                {
                    /** checking for pending surveys*/
                    NSArray *surveyArray=[[SWDatabaseManager retrieveManager]selectSurveyWithCustomerID:salesWorxVisit.visitOptions.customer.Customer_ID andSiteUseID:salesWorxVisit.visitOptions.customer.Ship_Site_Use_ID];
                    if(surveyArray.count==0)
                    {
                        isAnySurveyCompleted=YES;
                    }
                    else{
                        for (NSInteger i=0; i<surveyArray.count; i++) {
                            SurveyDetails *tempsurvey=[surveyArray objectAtIndex:i];
                            BOOL isSurveysCompleted=[self checkSurveyStatusForSurveyID:tempsurvey.Survey_ID];
                            if(isSurveysCompleted)
                            {
                                isAnySurveyCompleted = YES;
                                break;
                            }
                        }
                    }
                }
                
                if([appControl.DISTRIBUTIONCHECK_MANDATORY isEqualToString:KAppControlsYESCode] &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KDistributionCheckPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode]) && isOnsiteVisit ==YES &&  [visitOptionsFlagsArray containsObject:KDistributionCheckDisplayLevelCode]) /** TO -DO condition will be replace with the app control flag*/
                {
                    isDChaveValidItems=[[SWDatabaseManager retrieveManager] isDistributionCheckAvailable];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [self.navigationItem.leftBarButtonItem setEnabled:YES];
                
                NSString *notesQuery = [NSString stringWithFormat:@"Select IFNULL(Text_Notes,'') AS Text_Notes From TBL_FSR_Actual_Visits  Where Actual_Visit_ID='%@'",salesWorxVisit.Visit_ID];
                
                NSString *VisitNotes=@"";
                NSMutableArray *tempVisitArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:notesQuery];
                if(tempVisitArray.count>0){
                    VisitNotes=[[[[SWDatabaseManager retrieveManager] fetchDataForQuery:notesQuery] objectAtIndex:0] valueForKey:@"Text_Notes"];
                }
                
                if (!currentVisitID ) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else if ([appControl.ENABLE_VISIT_NOTE_ON_END_VISIT isEqualToString:KAppControlsYESCode] && [VisitNotes isEqualToString:@""] && !(salesWorxVisit.isSalesOrderCompleted || salesWorxVisit.isDistributionCheckCompleted || salesWorxVisit.isManageOrderCompleted || salesWorxVisit.isReturnsCompleted || salesWorxVisit.isSurveyCompleted || salesWorxVisit.isCollectionCompleted || salesWorxVisit.isOrderHistoryAccessed || salesWorxVisit.isTODOCompleted || salesWorxVisit.isReportsAccessed || salesWorxVisit.isProductMediaAccessed)) {
                    UIAlertAction *okAction = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   visitShouldCloseAfterSavingNotes = true;
                                                   [self ShowVisitNotes];
                                               }];
                    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)], okAction, nil];
                    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Visit Notes Required" andMessage:@"Please enter visit notes to end non-productive visit." andActions:actionsArray withController:self];
                } else if ([appControl.ENABLE_VISIT_NOTE_ON_END_VISIT isEqualToString:KAppControlsNOCode] && [VisitNotes isEqualToString:@""] && !(salesWorxVisit.isSalesOrderCompleted || salesWorxVisit.isDistributionCheckCompleted || salesWorxVisit.isManageOrderCompleted || salesWorxVisit.isReturnsCompleted || salesWorxVisit.isSurveyCompleted || salesWorxVisit.isCollectionCompleted || salesWorxVisit.isOrderHistoryAccessed || salesWorxVisit.isTODOCompleted || salesWorxVisit.isReportsAccessed || salesWorxVisit.isProductMediaAccessed)) {
                    
                    
                    UIAlertAction *skipAction = [UIAlertAction
                    actionWithTitle:NSLocalizedString(@"Skip", nil)
                    style:UIAlertActionStyleDefault
                    handler:^(UIAlertAction * action)
                    {
                        [self closeVisitWithEndDate];
                    }];
                    
                    UIAlertAction *okAction = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   visitShouldCloseAfterSavingNotes = true;
                                                   [self ShowVisitNotes];
                                               }];
                    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:skipAction, [SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)], okAction, nil];
                    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Visit Notes Required" andMessage:@"Please enter visit notes to end non-productive visit." andActions:actionsArray withController:self];
                }
                else if([appControl.SURVEY_MANDATORY isEqualToString:KAppControlsYESCode] &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KSurveyPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode])&& !isAnySurveyCompleted && [visitOptionsFlagsArray containsObject:KSurveyDisplayLevelCode] && isOnsiteVisit==YES) /** TO -DO condition will be replace with the app control flag*/
                {
                    NSLog(@"Displaying survey mandatory alert in visit options");
                    [SWDefaults showAlertAfterHidingKeyBoard:@"Survey Mandatory" andMessage:@"Please complete survey before closing visit." withController:self];
                }
                
                else if([appControl.DISTRIBUTIONCHECK_MANDATORY isEqualToString:KAppControlsYESCode] &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KDistributionCheckPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode]) && !salesWorxVisit.isDistributionCheckCompleted && isDChaveValidItems && isOnsiteVisit==YES && [visitOptionsFlagsArray containsObject:KDistributionCheckDisplayLevelCode]) /** TO -DO condition will be replace with the app control flag*/
                {
                    NSLog(@"Displaying distribution check mandatory alert in visit options");
                    [SWDefaults showAlertAfterHidingKeyBoard:@"Distribution Check Mandatory" andMessage:@"please complete Distribution Check before closing visit" withController:self];
                    
                }
                //pavan data
                else if([appControl.SHOW_SURVEY_ALERT isEqualToString:KAppControlsYESCode] &&  (([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] && [PDAComponents containsObject:KSurveyPDA_RightsCode])|| [appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsNOCode]) && !isAnySurveyCompleted && isOnsiteVisit==YES && [visitOptionsFlagsArray containsObject:KSurveyDisplayLevelCode]) /** TO -DO condition will be replace with the app control flag*/
                {
                    
                    UIAlertAction* noAction = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   [self closeVisitWithEndDate];
                                               }];
                    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)],noAction ,nil];
                    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KVisitOptionsPendingSurveyAlertTitleStr andMessage:KVisitOptionsPendingSurveyAlertMessageStr andActions:actionsArray withController:self];
                }
                else
                {
                    NSLog(@"Displaying confirmation alert in visit options");
                    [self showConfirmationAlertForCloseVisit];
                }
            });
        });
    }
}

-(void)showConfirmationAlertForCloseVisit
{
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   NSLog(@"Yes tap in close visit in visit options");
                                   [self closeVisitWithEndDate];
                               }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KVisitOptionCloseVisitConfirmationAlertTitleStr andMessage:KVisitOptionCloseVisitConfirmationAlertMessageStr andActions:actionsArray withController:self];
}

-(void)validateLocationCoOrdinateforOnSiteVisit
{
    NSString *customerLatitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Lat];
    NSString *customerLongitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Long];
    
    //check if customer location is available
    
    NSString *fsrLatitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude];
    NSString *fsrLongitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
    
    NSLog(@"CLOSE_VISIT_LOC_UPDATE_LIMIT is %@",appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT);
    
    //fetch range from tbl_addl_info
    double distanceLimit = [appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT doubleValue];
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[customerLatitude doubleValue] longitude:[customerLongitude doubleValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[fsrLatitude doubleValue] longitude:[fsrLongitude doubleValue]];
    CLLocationDistance distance = [location1 distanceFromLocation:location2];
    
    NSLog(@"Distance between Locations in meters: %f", distance);
    
    if (distance > distanceLimit)
    {
        //just update the current coordinates dont chnage customer coordinates, doing so, if we again start the onsite visit for same customer current coordinates and fsr coordinates so validatation fails
        
//        salesWorxVisit.visitOptions.customer.Cust_Lat = fsrLatitude;
//        salesWorxVisit.visitOptions.customer.Cust_Long = fsrLongitude;
        
        [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@',Custom_Attribute_6='Y' Where Actual_Visit_ID='%@'",fsrLatitude,fsrLongitude,[SWDefaults currentVisitID]]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UiCollection View Methods

#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  visitOptionsCollectionViewArray.count;
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(visitOptionsCollectionViewArray.count>8){
        return CGSizeMake(170, 170);
    }
    return CGSizeMake(190, 190);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if(visitOptionsCollectionViewArray.count>8){
        return 10.0;
    }
    return 20.0;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"visitOptionsCell";
    
    SalesWorxVisitOptionsCollectionViewCell *cell = (SalesWorxVisitOptionsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString* cellTitle=[[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"Title"];
    NSDictionary *visitOptionDict=[visitOptionsCollectionViewArray objectAtIndex:indexPath.row];
    NSString *visitOptionTitle=[visitOptionDict valueForKey:@"Title"];
    
    cell.optionTitleLbl.text=[[[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"Title"] uppercaseString] ;
    cell.optionImageView.image=[UIImage imageNamed:[[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"ThumbNailImage"]];
   
    if([cellTitle isEqualToString:kDistributionCheckTitle]){
        if (single.isDistributionChecked==YES ) {
            cell.layerView.backgroundColor=[UIColor clearColor];
            cell.optionImageView.image=[UIImage imageNamed:@"VisitOptions_DC_InActive"];
        }
        else{
            cell.optionImageView.image=[UIImage imageNamed:@"VisitOptions_DC_Active"];
        }
    }

    
    
    NSString* isDisabled=[[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"Disabled"];
    
    if(([visitOptionTitle isEqualToString:kSalesOrderTitle] && salesWorxVisit.isSalesOrderCompleted)
       ||([visitOptionTitle isEqualToString:kDistributionCheckTitle]   && salesWorxVisit.isDistributionCheckCompleted)
       ||([visitOptionTitle isEqualToString:kManageOrdersTitle] && salesWorxVisit.isManageOrderCompleted)
       ||([visitOptionTitle isEqualToString:kReturnsTitle] && salesWorxVisit.isReturnsCompleted)
       ||([visitOptionTitle isEqualToString:kSurveyTitle] && salesWorxVisit.isSurveyCompleted)
       ||([visitOptionTitle isEqualToString:kCollectionTitle] && salesWorxVisit.isCollectionCompleted)
       ||([visitOptionTitle isEqualToString:kOrderHistoryTitle] & salesWorxVisit.isOrderHistoryAccessed)
       ||([visitOptionTitle isEqualToString:kTodoTitle] & salesWorxVisit.isTODOCompleted)
       ||([visitOptionTitle isEqualToString:kReportsTitle] & salesWorxVisit.isReportsAccessed)
       ||([visitOptionTitle isEqualToString:kProductMediaTitle] & salesWorxVisit.isProductMediaAccessed))
    {
        cell.optionCompletionImageView.image=[UIImage imageNamed:@"VisitOptions_TickMark"];
    }
    else {
        cell.optionCompletionImageView.image=nil;
    }
    
    if ([isDisabled isEqualToString:@"Y"]) {
        cell.optionTitleLbl.textColor=[UIColor colorWithRed:(170.0/255.0) green:(170.0/255.0) blue:(170.0/255.0) alpha:1.0];
       // cell.optionImageView.layer.opacity=0.3;
        cell.layer.borderColor=[kVisitOptionsDisbaleBorderColor CGColor];
        cell.layer.shadowColor = [kVisitOptionsDisbaleBorderColor CGColor];
    }
    else{
        cell.optionTitleLbl.textColor=[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1];
       // cell.optionImageView.layer.opacity=1.0;
        cell.layer.borderColor=[kVisitOptionsBorderColor CGColor];
        cell.layer.shadowColor = [kVisitOptionsBorderColor CGColor];
    }
   
    if(visitOptionsCollectionViewArray.count>8){
        cell.optionLabelHeightConstraint.constant = 45.0f;
    } else {
        cell.optionLabelHeightConstraint.constant = 25.0f;
    }
    
    return cell;

}


-(void)checkAppControlFlagsandStartVisit
{
      appControl=[AppControl retrieveSingleton];
        NSMutableArray*  tempCategoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:salesWorxVisit];
    //in case of bin sina there is only one category there are no warehouses in that case save org id as w/h
    
    NSMutableArray * OrderTempleteArray=[[NSMutableArray alloc]init];
    
    if([appControl.ENABLE_TEMPLATES isEqualToString:KAppControlsYESCode])
    {
        OrderTempleteArray=[[SWDatabaseManager retrieveManager]fetchOrderTempletesForCustomer:salesWorxVisit.visitOptions.customer];
        
    }
    
    if (/*[appControl.ENABLE_MULTI_CAT_TRX isEqualToString:@"N"] &&*/
        ![appControl.ENABLE_FOC_ORDER isEqualToString:KAppControlsYESCode] &&
        (![appControl.ENABLE_TEMPLATES isEqualToString:KAppControlsYESCode] || OrderTempleteArray.count==0) &&
        ![appControl.ENABLE_RECOMMENDED_ORDER isEqualToString:KAppControlsYESCode] &&
        ![appControl.ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode] &&
        tempCategoriesArray.count==1) {
        NSLog(@"temp categories array is %@", tempCategoriesArray);
        
        //assuming there is only one category if enable multi cat trx is N
        salesWorxVisit.visitOptions.salesOrder.selectedCategory=[tempCategoriesArray objectAtIndex:0];
        
        salesWorxVisit.visitOptions.salesOrder.selectedOrderType=kDefaultOrderPopOverTitle;
        
        NSLog(@"check cat org id,cat and desc %@ %@ %@", salesWorxVisit.visitOptions.salesOrder.selectedCategory.Org_ID,salesWorxVisit.visitOptions.salesOrder.selectedCategory.Category,salesWorxVisit.visitOptions.salesOrder.selectedCategory.Description);
        
        SalesWorxSalesOrderViewController* salesOrderVC=[[SalesWorxSalesOrderViewController alloc]init];
        salesOrderVC.currentVisit=salesWorxVisit;
        salesWorxVisit.visitOptions.salesOrder.OrderStartTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        [self.navigationController pushViewController:salesOrderVC animated:YES];
        
    }
    else if(tempCategoriesArray.count==0)
    {
        [self showAlertAfterHidingKeyBoard:@"No Categories" andMessage:@"Categories are not available" withController:nil];
    }

    else
    {
        SalesWorxOrderInitializationViewController* salesOrderInitilizationVC=[[SalesWorxOrderInitializationViewController alloc]init];
        salesOrderInitilizationVC.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:salesOrderInitilizationVC animated:YES];
        
        
    }
    
    
    
    
}

-(void)samplePush{
    SalesWorxPaymentCollectionViewController *collectionViewController = [[SalesWorxPaymentCollectionViewController alloc] initWithCustomer:customerDict] ;
    collectionViewController.currentVisit=salesWorxVisit;
    [self.navigationController pushViewController:collectionViewController animated:YES];
    collectionViewController=nil;
    isUserSeletedAnyOption=YES;
}
-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:KAlertOkButtonTitle
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(void)checkAppControlForReturnScreen
{
  NSMutableArray*  tempCategoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:salesWorxVisit];
    
    
    if (/*[[AppControl retrieveSingleton].ENABLE_MULTI_CAT_TRX isEqualToString:KAppControlsNOCode] &&*/
        tempCategoriesArray.count==1) {
        SalesWorxReturnsViewController *returnsView=[[SalesWorxReturnsViewController alloc]initWithNibName:@"SalesWorxReturnsViewController" bundle:[NSBundle mainBundle]];
        salesWorxVisit.visitOptions.returnOrder=[[SalesWorxReturn alloc]init];
        salesWorxVisit.visitOptions.returnOrder.selectedCategory=[tempCategoriesArray objectAtIndex:0];
        salesWorxVisit.visitOptions.returnOrder.productCategoriesArray=tempCategoriesArray;
        salesWorxVisit.visitOptions.returnOrder.returnLineItemsArray=[[NSMutableArray alloc]init];
        salesWorxVisit.visitOptions.returnOrder.ReturnStartTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        salesWorxVisit.visitOptions.returnOrder.confrimationDetails=[[ReturnsConfirmationDetails alloc]init];
        returnsView.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:returnsView animated:YES];
    }
    else if(tempCategoriesArray.count==0)
    {
        [self showAlertAfterHidingKeyBoard:@"No Categories" andMessage:@"Categories are not available" withController:nil];
    }
    else
    {
        SalesWorxReturnsInitializationViewController *returnsVC=[[SalesWorxReturnsInitializationViewController alloc]initWithNibName:@"SalesWorxReturnsInitializationViewController" bundle:[NSBundle mainBundle]];
        returnsVC.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:returnsVC animated:YES];

    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
    NSString* selectedCellTitle=[[visitOptionsCollectionViewArray objectAtIndex:indexPath.row ] valueForKey:@"Title"];
    NSString* alertDescription;
    if ([selectedCellTitle isEqualToString:kDistributionCheckTitle]) {
        alertDescription=@"Distribution check not allowed for visits via telephone";
    }else if ([selectedCellTitle isEqualToString:kCollectionTitle]){
        alertDescription=@"Collections not allowed for visits via telephone";
    }else if ([selectedCellTitle isEqualToString:kSurveyTitle]){
        alertDescription=@"Survey not allowed for visits via telephone";
    }else if ([selectedCellTitle isEqualToString:kSalesOrderTitle]||
              [selectedCellTitle isEqualToString:kManageOrdersTitle]){
        alertDescription=@"Sales Order not allowed for blocked customers";
    }
  
    NSString* isDisabled=[[visitOptionsCollectionViewArray objectAtIndex:indexPath.row] valueForKey:@"Disabled"];
    if ([isDisabled isEqualToString:@"Y"]) {
        
        [self displayAlertWithTitle:@"" andMessage:alertDescription withButtonTitles:@[KAlertOkButtonTitle] andTag:indexPath.row];
    }
    else
    {

        /** adding visit to the database*/
        [self AddVisitDetailsToTheDatabaseIfNotAdded];
        
        
    if ([selectedCellTitle isEqualToString:kDistributionCheckTitle]) {
        if (single.isDistributionChecked==YES) {
            
            [self displayAlertWithTitle:kDistributionCheckedAlertTitle andMessage:@"" withButtonTitles:@[KAlertOkButtonTitle] andTag:indexPath.row];
        }
        else{
//        SWDistributionCheckViewController* distributionCheckVC=[[SWDistributionCheckViewController alloc]initWithCustomer:customerDict];
        SalesWorxDistributionCheckNewViewController* distributionCheckVC=[[SalesWorxDistributionCheckNewViewController alloc]initWithNibName:@"SalesWorxDistributionCheckNewViewController" bundle:[NSBundle mainBundle]];
        distributionCheckVC.currentVisit=salesWorxVisit;
        isUserSeletedAnyOption=YES;

        [self.navigationController pushViewController:distributionCheckVC animated:YES];
        }
    }
    else if ([selectedCellTitle isEqualToString:kSalesOrderTitle])
    {
        single.visitParentView = @"SO";
        NSLog(@"check over due %ld", (long)[[SWDefaults Over_Due] integerValue]);
        salesWorxVisit.visitOptions.salesOrder=[[SalesOrder alloc] init];
        isUserSeletedAnyOption=YES;

        
        if ([salesWorxVisit.visitOptions.customer.Over_Due integerValue] > 0 || [salesWorxVisit.visitOptions.customer.Avail_Bal integerValue] < 0)
        {
            single.isDueScreen=YES;
            SalesWorxCustomerDuesViewController * duesVC=[[SalesWorxCustomerDuesViewController alloc]init];
            duesVC.currentVisit=salesWorxVisit;
            [self.navigationController pushViewController:duesVC animated:YES];
            /*
            SWDuesView *duesView = [[SWDuesView alloc] initWithCustomer:customerDict] ;
            [self.navigationController pushViewController:duesView animated:YES];
            duesView=nil;*/
            
        }
        else
        {
            NSLog(@"navigating to product sale category");
            
            
            [self checkAppControlFlagsandStartVisit];
            
//            
//            SalesWorxOrderInitializationViewController* salesOrderInitilizationVC=[[SalesWorxOrderInitializationViewController alloc]init];
//            salesOrderInitilizationVC.currentVisit=salesWorxVisit;
//            [self.navigationController pushViewController:salesOrderInitilizationVC animated:YES];
//            

            
            /*
            single.isDueScreen=NO;
            ProductSaleCategoryViewController *productSaleCategoryViewController = [[ProductSaleCategoryViewController alloc] initWithCustomer:customerDict] ;
            [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
            productSaleCategoryViewController=nil;*/
        }
    }
    else if ([selectedCellTitle isEqualToString:kManageOrdersTitle])
    {
//        single.visitParentView = @"MO";
//        ManageOrderViewController *viewController = [[ManageOrderViewController alloc] initWithCustomer:customerDict] ;
//        [self.navigationController pushViewController:viewController animated:YES];
//        viewController=nil;
        isUserSeletedAnyOption=YES;

        
        ManageOrdersListViewController *manageOrdersListViewController=[[ManageOrdersListViewController alloc]initWithNibName:@"ManageOrdersListViewController" bundle:[NSBundle mainBundle]];
        manageOrdersListViewController.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:manageOrdersListViewController animated:YES];
    }
    else if ([selectedCellTitle isEqualToString:kOrderHistoryTitle])
    {
        
             /*   single.visitParentView = @"MO";
                ManageOrderViewController *viewController = [[ManageOrderViewController alloc] initWithCustomer:customerDict] ;
                [self.navigationController pushViewController:viewController animated:YES];
                viewController=nil;*/
        isUserSeletedAnyOption=YES;

        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"hideOrderHistoryHeader"];
        
        SalesWorxVisitOptionsOrderHistoryViewController * orderHistoryVC=[[SalesWorxVisitOptionsOrderHistoryViewController alloc]init];
        orderHistoryVC.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:orderHistoryVC animated:YES];

       /* SWOrderHistoryViewController *orderHistoryViewController = [[SWOrderHistoryViewController alloc] initWithCustomer:customerDict] ;
        [self.navigationController pushViewController:orderHistoryViewController animated:YES];
        orderHistoryViewController=nil;*/
        
    }
    else if ([selectedCellTitle isEqualToString:kReturnsTitle])
    {
        isUserSeletedAnyOption=YES;

//        single.visitParentView = @"RO";
//        NSArray* categoriesArray=  [[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customerDict];
//        if (categoriesArray.count==1) {
//            single.orderStartDate = [NSDate date];
//            [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
//            if ([[[[SWDefaults appControl] objectAtIndex:0] valueForKey:@"ENABLE_MULTI_UOM"] isEqualToString:@"Y"]) {
//                AlseerReturnViewController *returnView = [[AlseerReturnViewController alloc] initWithNibName:@"AlseerReturnViewController" bundle:nil] ;
//                returnView.customerDict = customerDict;
//                [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
//                [self.navigationController pushViewController:returnView animated:YES];
//            }
//            else
//            {
//                single.visitParentView = @"RO";
//                ReturnProductCategoryViewController *productSaleCategoryViewController = [[ReturnProductCategoryViewController alloc] initWithCustomer:customerDict] ;
//                [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
//                productSaleCategoryViewController=nil;
//            }
//        }
//        else
//        {
//            
//            SalesWorxReturnsInitializationViewController *returnsVC=[[SalesWorxReturnsInitializationViewController alloc]initWithNibName:@"SalesWorxReturnsInitializationViewController" bundle:[NSBundle mainBundle]];
//            returnsVC.currentVisit=salesWorxVisit;
//            [self.navigationController pushViewController:returnsVC animated:YES];
//            
//           /* ReturnProductCategoryViewController *productSaleCategoryViewController = [[ReturnProductCategoryViewController alloc] initWithCustomer:customerDict] ;
//            [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
//            productSaleCategoryViewController=nil;*/
//        }
        [self checkAppControlForReturnScreen];
    }
    else if ([selectedCellTitle isEqualToString:kCollectionTitle])
    {
        
        //check if there are any open invoices for customers
        
        
//       NSMutableArray*  invoicesArray=[[[SWDatabaseManager retrieveManager] dbGetInvoicesOfCustomer:[customerDict stringForKey:@"Customer_ID"] withSiteId:[customerDict stringForKey:@"Site_Use_ID"]andShipCustID:[customerDict stringForKey:@"Ship_Customer_ID"] withShipSiteId:[customerDict stringForKey:@"Ship_Site_Use_ID"] andIsStatement:NO] mutableCopy];
        
//        if (invoicesArray.count>0) {
        
        SalesWorxPaymentCollectionViewController *collectionViewController = [[SalesWorxPaymentCollectionViewController alloc] initWithCustomer:customerDict] ;
        collectionViewController.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:collectionViewController animated:YES];
        collectionViewController=nil;
        isUserSeletedAnyOption=YES;
//        }
//        else{
//            [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"Cannot take collection,there are no open invoices for the customer" withController:self];
//            
//        }
        
        /*ReturnProductCategoryViewController *productSaleCategoryViewController = [[ReturnProductCategoryViewController alloc] initWithCustomer:customerDict] ;
        [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
        productSaleCategoryViewController=nil;*/
    }
    else if ([selectedCellTitle isEqualToString:kSurveyTitle])
    {
//        SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
       
        
        /*
        SalesWorxAllSurveyViewController *objSurveyVC =[[SalesWorxAllSurveyViewController alloc] initWithNibName:@"SalesWorxAllSurveyViewController" bundle:nil];
        objSurveyVC.customer=customerDict;
        objSurveyVC.surveyParentLocation=@"Visit";
        [self.navigationController pushViewController:objSurveyVC animated:YES];
        objSurveyVC=nil;
        isUserSeletedAnyOption=YES;
        */
        
        
        
        //update this to survey
        isUserSeletedAnyOption=YES;

        SalesWorxSurveyViewController * surveyVC=[[SalesWorxSurveyViewController alloc]init];
        surveyVC.currentVisit=salesWorxVisit;
        surveyVC.isFromVisit=YES;
        [self.navigationController pushViewController:surveyVC animated:YES];

    }
    else if ([selectedCellTitle isEqualToString:kTodoTitle])
    {
        popOverController=nil;
        isUserSeletedAnyOption=YES;
        SalesWorxVisitOptionsToDoViewController * toDoVC = [[SalesWorxVisitOptionsToDoViewController alloc]init];
        toDoVC.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:toDoVC animated:YES];
        
    }
    else if ([selectedCellTitle isEqualToString:kMerchandisingTitle]) {
        
        //delete survey images if they exist
        NSString* surveyPicturesPath=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:KMerchandisingSurveyPicturesFolderName] stringByAppendingPathComponent:@"temp"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:surveyPicturesPath]) {
            NSError* deleteError;
            for (NSString * file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:surveyPicturesPath error:&deleteError]) {
                NSError* deleteFileError;
                NSString* filePath= [surveyPicturesPath stringByAppendingPathComponent:file];
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:&deleteFileError];
            }
            
        }
        else{
            NSString* directoryPath=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:KMerchandisingSurveyPicturesFolderName] stringByAppendingPathComponent:@"temp"];
            NSError *error;
            
            if (![[NSFileManager defaultManager]fileExistsAtPath:directoryPath]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
            }
        }
        
        
        
        
        //delete temp images for competitor info
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSString* tempPath=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingCompetitorInfoImages] stringByAppendingPathComponent:@"temp"];
        if ([fileManager fileExistsAtPath:tempPath]) {
            NSError* deleteError;
            for (NSString * file in [fileManager contentsOfDirectoryAtPath:tempPath error:&deleteError]) {
                NSError* deleteFileError;
                NSString* filePath= [tempPath stringByAppendingPathComponent:file];
                [fileManager removeItemAtPath:filePath error:&deleteFileError];
            }
            
        }
        else{
            NSString* directoryPath=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingCompetitorInfoImages] stringByAppendingPathComponent:@"temp"];
            NSError *error;
            
            if (![[NSFileManager defaultManager]fileExistsAtPath:directoryPath]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
            }
        }
        
        
        //delete temp images for planogram 
        NSFileManager * fileManagerPlanogram = [NSFileManager defaultManager];
        NSString* tempPathPlanogram=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingPlanogramImages] stringByAppendingPathComponent:@"temp"];
        if ([fileManagerPlanogram fileExistsAtPath:tempPathPlanogram]) {
            NSError* deleteError;
            for (NSString * file in [fileManager contentsOfDirectoryAtPath:tempPathPlanogram error:&deleteError]) {
                NSError* deleteFileError;
                NSString* filePath= [tempPathPlanogram stringByAppendingPathComponent:file];
                [fileManagerPlanogram removeItemAtPath:filePath error:&deleteFileError];
            }
        }
        else{
            NSString* directoryPath=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingPlanogramImages] stringByAppendingPathComponent:@"temp"];
            NSError *error;

            if (![[NSFileManager defaultManager]fileExistsAtPath:directoryPath]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
            }
        }

        
        popOverController=nil;
        
        
        SalesWorxMerchandising*  salesWorxMerchandising=[[SalesWorxMerchandising alloc]init];
        salesWorxMerchandising.Session_ID=[NSString createGuid];
        salesWorxMerchandising.Start_Time=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        
        salesWorxMerchandising.merchandisingArray=[[NSMutableArray alloc]init];
        salesWorxVisit.visitOptions.salesWorxMerchandising=salesWorxMerchandising;

        
        
        isUserSeletedAnyOption=YES;
        
        
        SalesWorxMerchandisingHomeViewController *merchVC=[[SalesWorxMerchandisingHomeViewController alloc]init];
        merchVC.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:merchVC animated:YES];
        
        

        
    }
    else if ([selectedCellTitle isEqualToString:kNearByCustomersTitle])
    {
        popOverController=nil;
        isUserSeletedAnyOption=YES;
        SalesWorxVisitOptionsNearByCustomersViewController *nearBYCustomersVC = [[SalesWorxVisitOptionsNearByCustomersViewController alloc]init];
        nearBYCustomersVC.currentVisit=salesWorxVisit;
        [self.navigationController pushViewController:nearBYCustomersVC animated:YES];
        
    }
    else if ([selectedCellTitle isEqualToString:kReportsTitle])
    {
        popOverController=nil;
        isUserSeletedAnyOption=YES;
        SalesWorxReportsViewController *reportsViewController = [[SalesWorxReportsViewController alloc]init];
        reportsViewController.currentVisit = salesWorxVisit;
        [self.navigationController pushViewController:reportsViewController animated:YES];
        
    }
    else if ([selectedCellTitle isEqualToString:kProductMediaTitle])
    {
        popOverController=nil;
        isUserSeletedAnyOption=YES;
        SalesWorxProductMediaViewController *productMediaViewController = [[SalesWorxProductMediaViewController alloc]init];
        productMediaViewController.currentVisit = salesWorxVisit;
        [self.navigationController pushViewController:productMediaViewController animated:YES];
    }
    else{
        
    }
        
    }
}
}

#pragma mark Close Visit Method

-(void)displayAlertWithTitle:(NSString*)Title andMessage:(NSString*)message withButtonTitles:(NSArray*)buttonTitlesArray andTag:(NSInteger)alertTag
{
    
    
    
    
    if ([UIAlertController class]) {
        // Use new API to create alert controller, add action button and display it
        alertController = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle: [buttonTitlesArray objectAtIndex:0] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            //yes button action
            [self alertViewYesbuttonClickedwithTag:alertTag];
        }];
        [alertController addAction: ok];
        
        if (buttonTitlesArray.count>1) {
            
       
        UIAlertAction* cancel = [UIAlertAction actionWithTitle: [buttonTitlesArray objectAtIndex:1] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            //no button action
            [self alertViewNobuttonClickedwithTag:alertTag];
        }];
        [alertController addAction: cancel];
        }
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        // We are running on old SDK as the new class is not available
        // Hide the compiler errors about deprecation and use the class available on older SDK
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIAlertView *alert;
        if (buttonTitlesArray.count>1) {
            alert = [[UIAlertView alloc] initWithTitle:Title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                                  otherButtonTitles:[buttonTitlesArray objectAtIndex:1],nil];

        }
        else{
            alert = [[UIAlertView alloc] initWithTitle:Title
                                               message:message
                                              delegate:self
                                     cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                     otherButtonTitles:nil];

        }
                [alert show];
#pragma clang diagnostic pop
}

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [self alertViewYesbuttonClickedwithTag:alertView.tag];
    }
    else if(buttonIndex==1)
    {
        [self alertViewNobuttonClickedwithTag:alertView.tag];
    }
}
-(void)alertViewYesbuttonClickedwithTag:(NSInteger)tag
{
    NSLog(@"yes button clicked %d",tag);
    if ([UIAlertController class]) {
    [alertController dismissViewControllerAnimated:YES completion:nil];
    }
    if (tag==100) {
        //yes tapped in close visit alert check if its from route
//        if (salesWorxVisit.isPlannedVisit==YES) {
//        }
//        else{
//            NSLog(@"closing visit without end date");
//            [self closeVisitwithoutEndDate];
//        }
        
        [self closeVisitWithEndDate];

    }
}

-(void)alertViewNobuttonClickedwithTag:(NSInteger)tag
{

    if ([UIAlertController class]) {
    [alertController dismissViewControllerAnimated:YES completion:nil];
    }
}



- (void)closeVisitWithEndDate
{
    
    [SWDefaults invalidateCurrentFieldSalesVisitTimer];

    if ([SWDefaults currentVisitID]) {
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:[NSDate date]]];
        
        if ([appControl.ENABLE_VISIT_LOC_UPDATE_ON_CLOSE isEqualToString:@"Y"] && [salesWorxVisit.visitOptions.customer.Visit_Type isEqualToString:@"Onsite"]) {
            [self validateLocationCoOrdinateforOnSiteVisit];
        }
        [SWDefaults clearCurrentVisitID];
    }
    else
    {
        [self stopTimer];
        NSLog(@"popping back after visit completion");
        [self.navigationController popViewControllerAnimated:YES];
    }
    if([SWDefaults isVTRXBackgroundSyncRequired] && ![NSString isEmpty:[SWDefaults username]] && [appDel. currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_None])
    {
        SalesWorxBackgroundSyncManager *salesWorxBackgroundSyncManager=[[SalesWorxBackgroundSyncManager alloc]init];
        [salesWorxBackgroundSyncManager postVisitDataToserver];
    }
    else{
        //No auto sync
    }
    
    
}

- (void)getRouteServiceDidGetRoute:(NSArray *)r{
    
    
    NSLog(@"planned visit for customer from route %@",salesWorxVisit.visitOptions.customer.Planned_visit_ID);
    
    
    
//    [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:salesWorxVisit.visitOptions.customer.Planned_visit_ID];
//    
//    NSString* tempVisitID=[SWDefaults currentVisitID];
//    if (tempVisitID) {
//        [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
//    
//    

    
//2018-11-17, client planet pharmacy has reported an issue where in user started a visit from route/customer went on to visit options did not do anything tapped on close visit and navigated back to route screen, the play button was still visible, intially i thought the start visit on activity flag is enabled for the customer, however the bool isUserSeletedAnyOption is being set on did select and if user closed a visit without selecting any cell, close visit database inseetion is not hapenning ideally it should happen, we updating the flow here, if start visit on activity is not enabled and visit has started and user tapped on close it shall close
    
    if ([[SWDefaults getValidStringValue:[AppControl retrieveSingleton].START_VISIT_ON_ACTIVITY] isEqualToString:KAppControlsNOCode]){
        isUserSeletedAnyOption=YES;
    }
        
    if (salesWorxVisit.visitOptions.customer.Planned_visit_ID)
    {
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID && isUserSeletedAnyOption) {
            [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:salesWorxVisit.visitOptions.customer.Planned_visit_ID];
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    
    
    
    
    
    NSString* tempVisitID=[SWDefaults currentVisitID];
    
    if (tempVisitID) {
        
        
        
        
        NSMutableArray *locationDict = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select Latitude , Longitude from TBL_FSR_Actual_Visits where Actual_Visit_ID='%@'",[SWDefaults currentVisitID]]];
        
        
        NSLog(@"check the location dict %@", [locationDict description]);
        
        
        if (locationDict.count>0) {
            
            
            
            if ([[[locationDict objectAtIndex:0]stringForKey:@"Latitude"]isEqualToString:@"0"] || [[[locationDict objectAtIndex:0]stringForKey:@"Longitude"]isEqualToString:@"0"])
            {
                // #define kSQLUpdateEndDate @"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='{0}' WHERE Actual_Visit_ID='{1}'; "

                NSString *latitudePoint,*longtitudePoint;
                latitudePoint=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude] ;
                longtitudePoint = [NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
            
                
      
                [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@' Where Actual_Visit_ID='%@'",latitudePoint,longtitudePoint,[SWDefaults currentVisitID]]];
            }
            
        }
        
        
        
        SWVisitManager *visitManager = [[SWVisitManager alloc] init];
        [visitManager closeVisit];
        
        if ([single.isCashCustomer isEqualToString:@"Y"]) {
            single.popToCash = @"Y";
        }
        else
        {
            single.popToCash = @"N";
            
        }
        r=nil;
        [self.navigationController  popViewControllerAnimated:YES];
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
    
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"went to visit options no activity done");
    }


}
-(void)closeVisitwithoutEndDate
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Visit Options Delegate
-(void)selectedVisitOption:(NSString*)visitOption
{
    NSLog(@"selected visit option is %@", visitOption);
    salesWorxVisit.Visit_Type=visitOption;
    
    if ([visitOption isEqualToString:@"Telephonic"]) {
       
    //disable distribution check and collection if its telephonic visit
    
    NSLog(@"visit options %@", visitOptionsCollectionViewArray);
    
    NSPredicate * dCPredicate=[NSPredicate predicateWithFormat:@"SELF.Title == [cd] %@", kDistributionCheckTitle];
    NSPredicate * collectionPredicate=[NSPredicate predicateWithFormat:@"SELF.Title == [cd] %@",kCollectionTitle];
    
    NSCompoundPredicate * tempPred=[NSCompoundPredicate orPredicateWithSubpredicates:@[dCPredicate,collectionPredicate]];
    NSLog(@"filter in visit option %@", [visitOptionsCollectionViewArray filteredArrayUsingPredicate:tempPred]);
    
    NSMutableArray* disabledArray=[[visitOptionsCollectionViewArray filteredArrayUsingPredicate:tempPred] mutableCopy];
    
    NSMutableArray * refinedDisabledArray=[[NSMutableArray alloc]init];
    
    for (NSInteger i=0; i<disabledArray.count;i++ ) {
        NSMutableDictionary * currentDict=[disabledArray objectAtIndex:i];
        [currentDict setValue:@"Y" forKey:@"Disabled"];
        [refinedDisabledArray addObject:currentDict];
    }
    
    
    NSLog(@"refined array is %@", visitOptionsCollectionViewArray);
    [visitOptionsCollectionView reloadData];
    }
}
-(BOOL)checkSurveyStatusForSurveyID:(NSInteger)surveyID
{
    BOOL completed =NO;
    NSArray *tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:salesWorxVisit.visitOptions.customer.Customer_ID andSiteUseID:salesWorxVisit.visitOptions.customer.Ship_Site_Use_ID ansSurveyID:[NSString stringWithFormat:@"%ld",(long)surveyID]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
    
    if (tempArray.count>0) {
        NSMutableDictionary *currentSurveyDict =[tempArray objectAtIndex:0];
        NSDateFormatter* df = [NSDateFormatter new];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [df setLocale:usLocale];
        NSDate *selectedDate = [df dateFromString:[currentSurveyDict stringForKey:@"Start_Time"]];
        NSInteger noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
        df=nil;
        usLocale=nil;
        if(noOfDays <= 0)
        {
            completed=YES;
        }
        else
        {
            completed=NO;
        }
    }
    
    return completed;
    
}
- (NSInteger)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2
{
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return [components day];
}


#pragma mark Save Contact details

-(void)didSaveContactDetails:(SalesWorxVisit*)swxVisit
{
    isUserSeletedAnyOption=YES;
    [self AddVisitDetailsToTheDatabaseIfNotAdded];
    
    BOOL contactDetailsInserted=[[SWDatabaseManager retrieveManager]saveVisitContactDetails:swxVisit];
    
}

#pragma mark Create Note
-(void)AddVisitDetailsToTheDatabaseIfNotAdded{
    
    if (!salesWorxVisit.Visit_ID) {
        salesWorxVisit.Visit_ID=[NSString createGuid];
        [SWDefaults setCurrentVisitID:salesWorxVisit.Visit_ID];
        NSLog(@"current visit id in sales order is %@", [SWDefaults currentVisitID]);
    }
    NSLog(@"customer dict in save visit %@", customerDict);
    if (insertFSRVisitData==NO && salesWorxVisit.isOpenVisit==NO) { /** inserting start viist if its  not already added to database*/
        insertFSRVisitData=  [[SWDatabaseManager retrieveManager]saveVisitDetails:salesWorxVisit];
    }
}
-(void)didSaveNote
{
    isUserSeletedAnyOption=YES;
    
    if (visitShouldCloseAfterSavingNotes) {
        [self closeVisitWithEndDate];
    }
}

-(void)cancelPopover {
    visitShouldCloseAfterSavingNotes = false;
}

-(void)ShowVisitNotes
{
//    if (insertFSRVisitData==NO) { /** inserting start viist if its  not already added to database*/
//        insertFSRVisitData=  [[SWDatabaseManager retrieveManager]saveVisitDetails:salesWorxVisit];
//    }

    [self AddVisitDetailsToTheDatabaseIfNotAdded];
  //  currentVisit.Visit_ID
    
     NSString *notesQuery = [NSString stringWithFormat:@"Select IFNULL(Text_Notes,'') AS Text_Notes From TBL_FSR_Actual_Visits  Where Actual_Visit_ID='%@'",salesWorxVisit.Visit_ID];
    
    NSString *VisitNotes=@"";
    NSMutableArray *tempVisitArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:notesQuery];
    if(tempVisitArray.count>0){
        VisitNotes=[[[[SWDatabaseManager retrieveManager] fetchDataForQuery:notesQuery] objectAtIndex:0] valueForKey:@"Text_Notes"];
    }
    
    
    SalesWorxCreateNotePopoverViewController *createNoteVC = [[SalesWorxCreateNotePopoverViewController alloc]init];
    createNoteVC.currentVisit = salesWorxVisit;
    createNoteVC.visitText=VisitNotes;
    createNoteVC.notesDelegate=self;
    UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:createNoteVC];
    mapPopUpViewController.navigationBarHidden = YES;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    createNoteVC.view.backgroundColor = [UIColor clearColor];
    createNoteVC.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}

@end
