//
//  SalesWorxVisitOptionsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"
#import "SalesWorxVisitOptionsCollectionViewCell.h"
#import "SalesWorxCustomClass.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "UIViewController+MJPopupViewController.h"
#import "OnsiteOptionsViewController.h"
#import "SalesWorxDistributionCheckNewViewController.h"
#import "SalesOrderNewViewController.h"
#import "SalesOrderViewController.h"
#import "ManageOrderViewController.h"
#import "SWOrderHistoryViewController.h"
#import "ReturnNewViewController.h"
#import "SalesWorxPaymentCollectionViewController.h"
#import "SurveyViewController.h"
#import "AlseerReturnViewController.h"
#import "ReturnProductCategoryViewController.h"
#import "SalesWorxPaymentCollectionViewController.h"
#import "SalesWorxVisitOptionsOrderHistoryViewController.h"
#import "SalesWorxCustomClass.h"
#import "ManageOrdersListViewController.h"
#import "SalesWorxReturnsInitializationViewController.h"
#import "SWAppDelegate.h"
#import "SalesWorxVisitOptionsToDoViewController.h"
#import "SWDistributionCheckViewController.h"
#import "SalesWorxImageView.h"
#import "SalesWorxVisitOptionsManager.h"
#import "SalesWorxDescriptionLabel1.h"
#import "SalesWorxiBeaconManager.h"
#import "SalesWorxMerchandisingHomeViewController.h"
#import "SalesWorxVisitOptionsContactDetailsPopOverViewController.h"
#import "SalesWorxProductMediaViewController.h"

@interface SalesWorxVisitOptionsViewController : UIViewController<updateVisitOptionButtons,UIAlertViewDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate>

{
    BOOL insertFSRVisitData;
    NSMutableArray * visitOptionsFlagsArray;
    IBOutlet MedRepHeader *customerNameLbl;
    IBOutlet MedRepProductCodeLabel *customerCodeLbl;
    IBOutlet SalesWorxImageView *customerStatusImageView;
    Singleton *single;
    NSMutableDictionary* customerDict;
    IBOutlet MedRepHeader *availableBalanceLbl;
    UIAlertController *alertController;
    AppControl *appControl;
    
    IBOutlet SalesWorxDropShadowView *OrderDetailsView;
    IBOutlet UILabel *OrdersValueNumberLabel;
    IBOutlet UILabel *OrdersTakenNumberLabel;
    BOOL isUserSeletedAnyOption;
    SWAppDelegate *appDel;
    NSArray *PDAComponents;
    
    UIPopoverController * popOverController;
    IBOutlet MedRepElementTitleLabel *lblLastVisitedOn;
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    
    SalesWorxImageBarButtonItem *createNoteButton;
    SalesWorxImageBarButtonItem *contactDetailsButton;
    NSTimer *_timer;
    BOOL visitShouldCloseAfterSavingNotes;
}
@property (strong, nonatomic) IBOutlet UICollectionView *visitOptionsCollectionView;
@property(strong,nonatomic)NSMutableArray* visitOptionsCollectionViewArray;
@property(strong,nonatomic)Customer * selectedCustomer;
@property(strong,nonatomic)SalesWorxVisit * salesWorxVisit;
- (void)closeVisitWithEndDate;

-(void)closeVisitTapped;

@end
