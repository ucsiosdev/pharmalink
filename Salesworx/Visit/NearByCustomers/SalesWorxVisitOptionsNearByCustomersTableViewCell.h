//
//  SalesWorxVisitOptionsNearByCustomersTableViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 1/16/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"

@interface SalesWorxVisitOptionsNearByCustomersTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *distanceLbl;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *customerNumberLbl;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *outStandingLbl;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *lastVisitedOnLbl;
@property (strong, nonatomic) IBOutlet UIButton *infoButton;

@end
