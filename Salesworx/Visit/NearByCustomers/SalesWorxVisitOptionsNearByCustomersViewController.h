//
//  SalesWorxVisitOptionsNearByCustomersViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 1/16/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"
#import <MapKit/MapKit.h>
#import "NSString+Additions.h"
#import "SWDatabaseManager.h"
#import "SWAppDelegate.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxTableView.h"

@interface SalesWorxVisitOptionsNearByCustomersViewController : UIViewController
{
    IBOutlet SalesWorxTableView *nearByCustomersTableView;
    NSMutableArray *nearByCustomersArray;
    
    IBOutlet SalesWorxDropShadowView *noContentView;
    IBOutlet MedRepElementDescriptionLabel *lblNoCustomerMessage;
    
    
    IBOutlet MedRepElementTitleLabel *customerTitleLbl;
    IBOutlet MedRepHeader *customerNameLbl;
    IBOutlet MedRepProductCodeLabel *customerCodeLbl;
    IBOutlet SalesWorxImageView *customerStatusImageView;
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
}

@property (strong,nonatomic) IBOutlet MKMapView *nearByCustomersMapView;
@property(strong,nonatomic) SalesWorxVisit *currentVisit;


@end
