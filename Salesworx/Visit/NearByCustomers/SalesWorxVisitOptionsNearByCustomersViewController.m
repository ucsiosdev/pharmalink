//
//  SalesWorxVisitOptionsNearByCustomersViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 1/16/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxVisitOptionsNearByCustomersViewController.h"
#import "MedRepDefaults.h"
#import "SalesWorxVisitOptionsNearByCustomersTableViewCell.h"
#import "SalesWorxCustomerStatementViewController.h"

@interface SalesWorxVisitOptionsNearByCustomersViewController ()

@end

@implementation SalesWorxVisitOptionsNearByCustomersViewController
@synthesize nearByCustomersMapView, currentVisit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:kFieldNearByCustomersScreenName];
    
    customerTitleLbl.text = NSLocalizedString(@"Customer Name", nil);
    customerNameLbl.text = [SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Customer_Name];
    customerCodeLbl.text = [SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Customer_No];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        [customerStatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else {
        [customerStatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    lblNoCustomerMessage.text = @"";
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldNearByCustomersScreenName];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
        [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
    
    if (self.isMovingToParentViewController) {
        
        [self fetchNearByCustomers];
        [nearByCustomersMapView removeAnnotations:nearByCustomersMapView.annotations];
        nearByCustomersArray = [[NSMutableArray alloc]init];
        [self updateMapData];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
//    if (self.isMovingToParentViewController) {
//        [nearByCustomersMapView removeAnnotations:nearByCustomersMapView.annotations];
//        nearByCustomersArray = [[NSMutableArray alloc]init];
//        [self updateMapData];
//        [self fetchNearByCustomers];
//    }
}
-(void)updateMapData
{
    double selectedCustLat = [[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Cust_Lat] doubleValue];
    double selectedCustLong = [[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Cust_Long] doubleValue];
    
    CLLocationCoordinate2D selectedCustomerCoordinate= CLLocationCoordinate2DMake(selectedCustLat, selectedCustLong);
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = selectedCustomerCoordinate;
    Pin.title=[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Customer_Name];
    
    [nearByCustomersMapView removeAnnotations:nearByCustomersMapView.annotations];
    [nearByCustomersMapView addAnnotation:Pin];
    
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (selectedCustomerCoordinate, 500, 500);
    [nearByCustomersMapView setRegion:regionCustom animated:YES];
}

-(void)fetchNearByCustomers
{
    if(self.parentViewController != nil){
        [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        double selectedCustLat = [[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Cust_Lat] doubleValue];
        double selectedCustLong = [[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Cust_Long] doubleValue];
        
         NSMutableArray * customerListArray=[[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
        if (customerListArray.count>0) {
            
            for (NSMutableDictionary *currentDict in customerListArray) {
                NSMutableDictionary *currentCustDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                Customer * currentCustomer=[[Customer alloc] init];
                currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
                currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
                currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
                currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
                currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
                currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
                currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
                currentCustomer.City=[currentCustDict valueForKey:@"City"];
                currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
                currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
                currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
                currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
                currentCustomer.Cust_Lat=[currentCustDict valueForKey:@"Cust_Lat"];
                currentCustomer.Cust_Long=[currentCustDict valueForKey:@"Cust_Long"];
                currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
                currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
                currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
                currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
                currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
                currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
                currentCustomer.Customer_Segment_ID=[currentCustDict valueForKey:@"Customer_Segment_ID"];
                currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
                currentCustomer.Dept=[currentCustDict valueForKey:@"Dept"];
                currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
                currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
                currentCustomer.Postal_Code=[currentCustDict valueForKey:@"Postal_Code"];
                currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
                currentCustomer.SalesRep_ID=[currentCustDict valueForKey:@"SalesRep_ID"];
                currentCustomer.Sales_District_ID=[currentCustDict valueForKey:@"Sales_District_ID"];
                currentCustomer.Ship_Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]];
                currentCustomer.Ship_Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]];
                currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
                currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
                currentCustomer.isDelegatedCustomer=[currentCustDict valueForKey:@"isDelegatedCustomer"];
                currentCustomer.Area=[currentCustDict valueForKey:@"Area"];
                
                currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:@"CUST_LOC_RNG"];
                
                CLLocationCoordinate2D customerLocation= CLLocationCoordinate2DMake([currentCustomer.Cust_Lat doubleValue], [currentCustomer.Cust_Long doubleValue]);
                CLLocation *selectedCustomerLocation=[[CLLocation alloc]initWithLatitude:selectedCustLat longitude:selectedCustLong];
                BOOL isInLocation=[self location:selectedCustomerLocation isNearCoordinate:customerLocation withRadius:1000];
                if (isInLocation) {
                    NSLog(@"customer %@ is in range with coordinate %f %f", currentCustomer.Customer_Name,customerLocation.latitude,customerLocation.longitude );
                    
                    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
                    CLLocation *custLocation = [[CLLocation alloc] initWithLatitude:[currentCustomer.Cust_Lat doubleValue] longitude:[currentCustomer.Cust_Long doubleValue]];
                    CLLocationDistance distance = [appDelegate.currentLocation distanceFromLocation:custLocation];
                    currentCustomer.distanceFromCurrentLocation=[NSString stringWithFormat:@"%f",distance];
   
                    
                    NSString *lastVisitedDateStr=[[NSString alloc] init];
                    lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:currentCustomer];
                    currentCustomer.lastVisitedOn=[NSString getValidStringValue:lastVisitedDateStr];
                    
                    NSMutableArray* outStandingArray=[self fetchTotalOutStandingforCustomer:currentCustomer.Customer_ID];
                    if (outStandingArray.count>0) {
                        NSMutableDictionary* outstandingDict=[outStandingArray objectAtIndex:0];
                        NSString* outStandingString=[NSString getValidStringValue:[outstandingDict valueForKey:@"Total"]];
                        currentCustomer.totalOutStanding=outStandingString;
                    }
                    
                    [nearByCustomersArray addObject:currentCustomer];
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            for (Customer * tempCustomer in nearByCustomersArray) {
                
                CLLocationCoordinate2D tempCustomerLocation= CLLocationCoordinate2DMake([tempCustomer.Cust_Lat doubleValue], [tempCustomer.Cust_Long doubleValue]);
                
                MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
                Pin.coordinate = tempCustomerLocation;
                Pin.title = tempCustomer.Customer_Name;
                [nearByCustomersMapView addAnnotation:Pin];
            }
            
            
            
            [nearByCustomersTableView reloadData];
            if (nearByCustomersArray.count > 0) {
                noContentView.hidden = YES;
            } else {
                noContentView.hidden = NO;
                lblNoCustomerMessage.text = @"No Nearby Customers";
            }
            
            if(self.parentViewController != nil){
                [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
            }else{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
        });
    });
}

-(NSMutableArray*)fetchTotalOutStandingforCustomer:(NSString*)selectedCustomerID
{
    NSString * outStandingQry=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"Select IFNULL(Total_Due,'N/A')  AS Total, IFNULL(Over_Due,'N/A') AS Over_Due, IFNULL(PDC_Due,'N/A') AS PDC_Due from TBL_Customer_Dues where Customer_ID = '%@'",selectedCustomerID]];
    
    NSLog(@"total outstanding qry %@", outStandingQry);
    
    NSMutableArray *temp=  [[[SWDatabaseManager retrieveManager] fetchDataForQuery:outStandingQry] mutableCopy];
    
    if (temp.count>0) {
        
        return temp;
    }
    else
    {
        return nil;
    }
}
- (BOOL)location:(CLLocation *)location isNearCoordinate:(CLLocationCoordinate2D)currentCoordinate withRadius:(CLLocationDistance)radius
{
    CLCircularRegion *circularRegion = [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius identifier:@"radiusCheck"];
    
    return [circularRegion containsCoordinate:currentCoordinate];
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKPinAnnotationView *mapPin = nil;
    if(annotation != map.userLocation)
    {
        static NSString *defaultPinID = @"defaultPin";
        mapPin = (MKPinAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if (mapPin == nil )
        {
            mapPin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                     reuseIdentifier:defaultPinID];
            mapPin.canShowCallout = YES;
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [infoButton addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            mapPin.rightCalloutAccessoryView = infoButton;
        }
        else
        {
            mapPin.annotation = annotation;
        }
        
        if([[annotation title] isEqualToString:currentVisit.visitOptions.customer.Customer_Name])
        {
            mapPin.pinColor = MKPinAnnotationColorGreen;
        } else {
            mapPin.pinColor = MKPinAnnotationColorRed;
        }
    }
    return mapPin;
}

-(void)infoButtonTapped:(id)sender
{
    
}

- (void)mapView:(MKMapView *)currentMapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* custLatValue;
    NSString* custLongValue;
    
    if (currentMapView==nearByCustomersMapView) {
        
        if (currentMapView.selectedAnnotations.count>0) {
            MKPointAnnotation * selectedannotation= (MKPointAnnotation*)[currentMapView.selectedAnnotations objectAtIndex:0];
            custLatValue=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%f",selectedannotation.coordinate.latitude]];
            custLongValue=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%f",selectedannotation.coordinate.longitude]];
        }
        else{
            
        }
        NSLog(@"selected annotations are %@", currentMapView.selectedAnnotations);
    }
    else{
        if (![NSString isEmpty:currentVisit.visitOptions.customer.Cust_Lat]) {
            custLatValue =[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Cust_Lat];
            custLongValue=[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Cust_Long];
        }
    }
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        NSString* googleMapsUrlString;
        
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication]openURL:googleMapsUrl];
            
        }
    }
    
    else
    {
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
        else
        {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
    }
}

#pragma mark UITableView Methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    tableView.separatorColor=kUITableViewSaperatorColor;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return nearByCustomersArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"nearByCell";
    SalesWorxVisitOptionsNearByCustomersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxVisitOptionsNearByCustomersTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    Customer *currentCustomer =  [nearByCustomersArray objectAtIndex:indexPath.row];
    
    cell.customerNameLbl.text=[NSString getValidStringValue:currentCustomer.Customer_Name];
    cell.customerNumberLbl.text=[NSString getValidStringValue:currentCustomer.Customer_No];
    cell.outStandingLbl.text=[[NSString getValidStringValue:currentCustomer.totalOutStanding] currencyString];
    
    NSString* distanceString=[NSString getValidStringValue:currentCustomer.distanceFromCurrentLocation];
    
    cell.distanceLbl.text=[NSString isEmpty:distanceString] || [distanceString integerValue]==0?@"":[NSString stringWithFormat:@"%ldm",(long)[distanceString integerValue]];
    
    cell.lastVisitedOnLbl.text=currentCustomer.lastVisitedOn;
    cell.infoButton.tag=indexPath.row;
    
    [cell.infoButton addTarget:self action:@selector(cellInfoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SalesWorxCustomerStatementViewController *statementVC = [[SalesWorxCustomerStatementViewController alloc]init];
    statementVC.selectedCustomer = [nearByCustomersArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:statementVC animated:YES];
}

-(void)cellInfoButtonTapped:(id)sender
{
    UIButton* selectedButton=sender;
    
    Customer * selectedCustomer=[nearByCustomersArray objectAtIndex:selectedButton.tag];
    
    NSLog(@"selected customer is %@",selectedCustomer.Customer_Name);
}

-(void)updateCustomerLastVisitDetails:(Customer*)selectCust{
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
