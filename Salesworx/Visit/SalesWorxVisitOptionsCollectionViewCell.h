//
//  SalesWorxVisitOptionsCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDescriptionLabel5_SemiBold.h"
#import "SalesWorxImageView.h"
@interface SalesWorxVisitOptionsCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *optionImageView;
@property (strong, nonatomic) IBOutlet UIView *layerView;
@property (strong, nonatomic) IBOutlet SalesWorxDescriptionLabel5_SemiBold *optionTitleLbl;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *optionCompletionImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *optionLabelHeightConstraint;
@end
