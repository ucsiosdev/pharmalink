//
//  SalesWorxVisitOptionsContactDetailsPopOverViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 1/22/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxPresentControllerBaseViewController.h"
#import "MedRepTextView.h"
#import "SalesWorxCustomClass.h"
#import "MedRepTextField.h"
#import "NSString+Additions.h"
#import "SWDatabaseManager.h"

#define topContentViewTopConstant 282

@protocol FieldSalesContactDetailsDelegate

-(void)didSaveContactDetails:(SalesWorxVisit*)swxVisit;

@end

@interface SalesWorxVisitOptionsContactDetailsPopOverViewController : SalesWorxPresentControllerBaseViewController<UITextFieldDelegate>
{
    IBOutlet NSLayoutConstraint *XtopContentViewTopConstraint;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *saveButton;
    CGRect oldFrame;
    BOOL isKeypadOpen;
    id contactDetailsDelegate;
    BOOL viewMovedUp;
}
@property (strong, nonatomic) IBOutlet MedRepTextView *notesTxtView;
@property (strong, nonatomic) NSString *visitText;
@property ( nonatomic) id contactDetailsDelegate;;

- (IBAction)SaveButtontapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@property(strong,nonatomic) SalesWorxVisit *currentVisit;
@property (strong, nonatomic) IBOutlet MedRepTextField *contactNameTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *contactNumberTextField;
- (IBAction)saveButtonTapped:(id)sender;

@end
