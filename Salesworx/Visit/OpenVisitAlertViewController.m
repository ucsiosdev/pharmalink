//
//  OpenVisitAlertViewController.m
//  MedRep
//
//  Created by Unique Computer Systems on 11/7/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "OpenVisitAlertViewController.h"

@interface OpenVisitAlertViewController ()

@end

@implementation OpenVisitAlertViewController
@synthesize contentView,openVisitCustomer,selectedCustomer,headerTitleLabel,contentTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    headerTitleLabel.text = [NSString stringWithFormat:@"You already have an open visit for \n%@\nPlease confirm how would you like to proceed?",openVisitCustomer.Customer_Name];
    contentTableView.estimatedRowHeight = 60;
    contentTableView.rowHeight= UITableViewAutomaticDimension;
    messageLabel.text = [NSString stringWithFormat:@"You already have an existing open visit for %@\n\nAnd you have attempted to start a new visit for %@\n\nPlease confirm how would you like to proceed?",openVisitCustomer.Customer_Name,selectedCustomer.Customer_Name];
    
    buttonTitlesArray= [[NSMutableArray alloc]initWithObjects:@"Close the existing visit and start new visit",@"Continue with the existing open visit",@"Cancel", nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    if (self.isMovingToParentViewController) {
       // [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return buttonTitlesArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.numberOfLines = 1;
    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(15);
    cell.textLabel.textColor = kNavigationBarBackgroundColor;
    cell.textLabel.text=buttonTitlesArray[indexPath.row];
    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.selectedReasonDelegate respondsToSelector:@selector(selectedOpenVisitReason:)]){
        [self.selectedReasonDelegate selectedOpenVisitReason:indexPath.row];
    }
    [self dismissViewControllerAnimated:true completion:nil];

}


- (IBAction)cancelButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
@end
