//
//  SalesWorxVisitOptionsToDoViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SalesWorxCustomClass.h"
#import "MedRepQueries.h"
#import "SWDatabaseManager.h"
#import "MedRepPanelTitle.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepTextView.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxVisitOptionsToDoStaticticsTableViewCell.h"
#import "SalesWorxVisitOptionsToDoTableViewCell.h"
#import <math.h>
#import "SalesWorxDatePickerPopOverViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxVisitOptionsToDoFilterViewController.h"
#import "PNChartDelegate.h"
#import "PNCircleChart.h"
#import "PNChart.h"
#import "SalesWorxDescriptionLabel1.h"


#define kTitle @"Title"
#define kDescription @"Description"
#define kMessageTitle @"Message"
#define kDateTitle @"Date"
#define kStatus @"Status"
#define kReminder @"Reminder"
#define kSubmitTitle @"Submit"
#define kNameTitle @"Name"



#define kStatisticsTitle @"Title"
#define kStatisticsColor @"Color"
#define kStatisticsCount @"Count"
#define kNewStatisticsColor [UIColor greenColor]
#define kClosedStatisticsColor [UIColor greenColor]
#define kCompletedStatisticsColor [UIColor greenColor]
#define kDeferredStatisticsColor [UIColor greenColor]



@interface SalesWorxVisitOptionsToDoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate,UITextViewDelegate,UIPopoverPresentationControllerDelegate,FilteredToDoDelegate,UITextFieldDelegate,PNChartDelegate>
{
    IBOutlet MedRepElementTitleLabel *titleLbl;
    IBOutlet SalesWorxImageView *customerStatusImageView;
    IBOutlet MedRepTextField *titleTextField;
    IBOutlet MedRepElementTitleLabel *messagelbl;
    IBOutlet MedRepTextView *messageTxtView;
    IBOutlet MedRepElementTitleLabel *dateLbl;
    IBOutlet MedRepTextField *dateTextField;
    IBOutlet MedRepElementTitleLabel *statusLbl;
    IBOutlet MedRepTextField *statusTextField;
    IBOutlet MedRepButton *cancelButton;
    IBOutlet MedRepButton *submitButton;
    IBOutlet MedRepElementTitleLabel *reminderLbl;
    IBOutlet MedRepTextField *reminderTextField;
    
    UIImageView * blurredBgImage;
    NSMutableDictionary * previousFilterParameters;
    NSString* selectedDateTextField;

    NSMutableArray * unFilteredArray;
    NSIndexPath * selectedIndexPath;
    
    
    IBOutlet SalesWorxDropShadowView *noSelctionView;
    IBOutlet UITableView *todoTblView;
    
    IBOutlet UILabel *availableBalanceLbl;
    IBOutlet UISearchBar *toDoSearchBar;
    
    BOOL isSearching;
    IBOutlet MedRepElementTitleLabel *customerTitleLbl;
    IBOutlet MedRepHeader *customerNameLbl;
    IBOutlet MedRepProductCodeLabel *customerCodeLbl;
    
    UIPopoverController * datePickerPopOverController,*statusPopOverController;
    NSMutableArray * toDoStatusArray;
    ToDo * currentToDo;

    UIBarButtonItem* createToDoButton;
    NSMutableArray * indexPathArray;
    
    NSInteger selectedIndex;
    IBOutlet UIButton *filterButton;
    NSMutableArray * filteredToDoArray;
    
    IBOutlet UILabel *lblCountNewTask;
    IBOutlet UILabel *lblCountClosedTask;
    IBOutlet UILabel *lblCountDeferredTask;
    IBOutlet UILabel *lblCountCancelledTask;
    
    IBOutlet UILabel *lblCountCompletedTask;
    IBOutlet UILabel *lblCountTotalTask;
    
    IBOutlet UIView *chartView;
    PNCircleChart *circleChartForTasks;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
}
@property(strong,nonatomic)     UIPopoverController * popOverController;
@property(strong,nonatomic) SalesWorxVisit * currentVisit;
- (IBAction)submitButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

- (IBAction)filterButtonTapped:(id)sender;

@end
