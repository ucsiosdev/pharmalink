//
//  SalesWorxVisitOptionsToDoFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxVisitOptionsToDoFilterViewController.h"

@interface SalesWorxVisitOptionsToDoFilterViewController ()

@end

@implementation SalesWorxVisitOptionsToDoFilterViewController

@synthesize dateTextField,statusTextField,dateTitleLbl,statusTitleLbl,popOverPresentationController,previousFilterParametersDict,filterNavController,toDoArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    filterParametersDict=[[NSMutableDictionary alloc]init];

    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldTodoFilterScreenName];
    }
    
    if (self.isMovingToParentViewController) {
        
        dateTitleLbl.text=NSLocalizedString(@"Date", nil);
        statusTitleLbl.text=NSLocalizedString(@"Status", nil);
    
        NSString* statusString=[previousFilterParametersDict valueForKey:@"Status"];
        NSString* dateSting=[previousFilterParametersDict valueForKey:@"Todo_Date"];
        filterParametersDict=previousFilterParametersDict;
        if (![NSString isEmpty:statusString]) {
            
            statusTextField.text=[SWDefaults getValidStringValue:statusString];
        }
    
        if (![NSString isEmpty:dateSting]) {
            
            dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:dateSting]];
        }
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    }
    
}


-(void)closeFilter
{
    if ([self.delegate respondsToSelector:@selector(toDoFilterDidClose)]) {
        [self.delegate toDoFilterDidClose];
        //[self.delegate filteredProductParameters:filterParametersDict];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)clearFilter
{
    dateTextField.text=@"";
    statusTextField.text=@"";
    filterParametersDict=[[NSMutableDictionary alloc]init];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITextField Methods

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedTextField;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];
    
    //get distinct keys
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray=[[toDoArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:predicateString];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        
    }
}

-(void)selectedFilterValue:(NSString*)selectedString
{
    if ([selectedTextField isEqualToString:kStatusTextField]) {
        statusTextField.text=[SWDefaults getValidStringValue:selectedString];
        [filterParametersDict setValue:selectedString forKey:@"Status"];
    }
}


-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    NSLog(@"selected visit date is %@", selectedDate);
    dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:selectedDate]];
    [filterParametersDict setValue:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:selectedDate] forKey:@"Todo_Date"];

    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if(textField==dateTextField)
    {
        selectedTextField=kDateTextField;
        selectedPredicateString=@"Todo_Date";
        //[self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        
        MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        dateFilterVC.datePickerMode=kTodoTitle;
        dateFilterVC.titleString=@"Date";
        dateFilterVC.visitDateDelegate=self;
        [self.navigationController pushViewController:dateFilterVC animated:YES];
        return NO;
    }
    
    else if (textField==statusTextField)
    {
        selectedTextField=kStatusTextField;
        selectedPredicateString=@"Status";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    else
    {
        return YES;
    }
    
   
}
- (IBAction)searchButtonTapped:(id)sender {
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filter parameters dict is %@",filterParametersDict);
    
    NSMutableArray * filteredToDoArray=[[NSMutableArray alloc]init];
    
    NSString* dateString=[filterParametersDict valueForKey:@"Todo_Date"];
    NSString* statusString=[filterParametersDict valueForKey:@"Status"];
    if([NSString isEmpty:dateString]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Todo_Date ==[cd] %@",dateString]];
    }
    
    if([NSString isEmpty:statusString]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Status ==[cd] %@",statusString]];
    }
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredToDoArray=[[toDoArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    if (predicateArray.count==0) {
        
        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please select your filter criteria and try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [noFilterAlert show];
    }
    else  if (filteredToDoArray.count>0)
    {
        if ([self.delegate respondsToSelector:@selector(filteredToDoList:)]) {
            [self.delegate filteredToDoList:filteredToDoArray];
            [self.delegate filteredToDoParameters:filterParametersDict];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        [MedRepDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
        
    }

}

- (IBAction)resetButtonTapped:(id)sender {
    
    previousFilterParametersDict=[[NSMutableDictionary alloc]init];
    
    if ([self.delegate respondsToSelector:@selector(toDoFilterdidReset)]) {
        [self.delegate toDoFilterdidReset];
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}
@end
