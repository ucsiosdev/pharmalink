//
//  SalesWorxVisitOptionsToDoStaticticsTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementTitleLabel.h"


@interface SalesWorxVisitOptionsToDoStaticticsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *statisticsTitleLbl;
@property (strong, nonatomic) IBOutlet UILabel *statisticsDescLbl;

@end
