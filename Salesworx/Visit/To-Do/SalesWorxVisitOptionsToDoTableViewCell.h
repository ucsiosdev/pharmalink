//
//  SalesWorxVisitOptionsToDoTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepView.h"
#import "SalesWorxImageView.h"
@interface SalesWorxVisitOptionsToDoTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *accessoryImageView;
@property (strong, nonatomic) IBOutlet UIView *statusView;

@end
