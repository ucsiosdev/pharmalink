//
//  SalesWorxVisitOptionsToDoFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/20/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementTitleLabel.h"
#import "MedRepTextField.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "NSPredicate+Distinct.h"
#import "MedRepVisitsDateFilterViewController.h"
#define kStatusTextField @"Status"
#define kDateTextField @"Date"
#import "NSString+Additions.h"

@protocol FilteredToDoDelegate <NSObject>

-(void)filteredToDoList:(NSMutableArray*)filteredContent;
-(void)toDoFilterDidClose;
-(void)toDoFilterdidReset;
-(void)filteredToDoParameters:(NSMutableDictionary*)parametersDict;

@end

@interface SalesWorxVisitOptionsToDoFilterViewController : UIViewController<UITextFieldDelegate,SelectedFilterDelegate,VisitDatePickerDelegate>

{
    id delegate;
    NSMutableDictionary * filterParametersDict;
    
    NSString* selectedTextField;
    NSString* selectedPredicateString;

}
@property(nonatomic) id delegate;

@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *dateTitleLbl;
@property (strong, nonatomic) IBOutlet MedRepTextField *dateTextField;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *statusTitleLbl;
@property (strong, nonatomic) IBOutlet MedRepTextField *statusTextField;
- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)resetButtonTapped:(id)sender;
@property(strong,nonatomic)NSMutableDictionary * previousFilterParametersDict;
@property(strong,nonatomic)UINavigationController * filterNavController;
@property(strong,nonatomic) UIPopoverPresentationController * popOverPresentationController;
@property(strong,nonatomic) NSMutableArray* toDoArray;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;

@end
