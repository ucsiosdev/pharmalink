//
//  SalesWorxVisitOptionsToDoViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxVisitOptionsToDoViewController.h"

@interface SalesWorxVisitOptionsToDoViewController ()

@end

@implementation SalesWorxVisitOptionsToDoViewController
@synthesize currentVisit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    indexPathArray=[[NSMutableArray alloc]init];
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"To Do"];
    
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(addTapped)];
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    
    
    createToDoButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"PlusIcon_White"] style:UIBarButtonItemStylePlain target:self action:@selector(addTapped)];
    
    self.navigationItem .rightBarButtonItem=createToDoButton;
    
    customerNameLbl.text=[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Customer_Name];
    customerCodeLbl.text=[SWDefaults getValidStringValue:currentVisit.visitOptions.customer.Customer_No];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        
        [customerStatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }
    else
    {
        [customerStatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    availableBalanceLbl.text=[[NSString stringWithFormat:@"%@",currentVisit.visitOptions.customer.Avail_Bal] currencyString];
}

-(void)removeKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    [toDoSearchBar setShowsCancelButton:NO animated:YES];
}

-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self removeKeyBoardObserver];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    todoTblView.delegate=self;
    todoTblView.rowHeight = UITableViewAutomaticDimension;
    todoTblView.estimatedRowHeight = 70.0;

    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldTodoScreenName];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
        [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        
        [self addKeyBoardObserver];
        
        customerTitleLbl.text=NSLocalizedString(@"Customer Name", nil);
        titleLbl.text=NSLocalizedString(@"Title", nil);
        messagelbl.text=NSLocalizedString(@"Description", nil);
        
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        dateLbl.text=kDateTitle;
        statusLbl.text=kStatus;
        reminderLbl.text = kReminder;
        
        NSMutableArray * statusArray= [MedRepQueries fetchTaskAppCodes:@"Todo_Status"];
        
        NSMutableArray* toDoArray=[[SWDatabaseManager retrieveManager] fetchTodoforCustomer:currentVisit.visitOptions.customer];
        if (toDoArray.count>0) {
            
            currentVisit.visitOptions.toDoArray=toDoArray;
            unFilteredArray=toDoArray;
            [self updatedStatistics];
        }
        
        if (statusArray.count>0) {
            
            NSPredicate * refinedPred=[NSPredicate predicateWithFormat:@"SELF != %@",kNewStatus];
            toDoStatusArray=[[statusArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
        }
        
        NSLog(@"status array is %@",toDoStatusArray);
        
        //fetch todo
        
        if (currentVisit.visitOptions.toDoArray.count>0) {
            [self deselectPreviousCellandSelectFirstCell];
            
        }
        else{
            currentVisit.visitOptions.toDoArray=[[NSMutableArray alloc]init];
            [self updateViewEditingStatus:NO];
        }
    }
}

-(void)cancelTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)addTapped
{
    [submitButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    
    [indexPathArray removeAllObjects];
    [todoTblView reloadData];
    
    currentToDo=[[ToDo alloc]init];
    //invoice to customer id should be customer id
    currentToDo.Inv_Customer_ID=currentVisit.visitOptions.customer.Customer_ID;
    currentToDo.Inv_Site_Use_ID=currentVisit.visitOptions.customer.Site_Use_ID;
    currentToDo.Ship_Customer_ID=currentVisit.visitOptions.customer.Ship_Customer_ID;
    currentToDo.Ship_Site_Use_ID=currentVisit.visitOptions.customer.Ship_Site_Use_ID;
    currentToDo.Created_At=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    currentToDo.Created_By=[[SWDefaults userProfile] valueForKey:@"User_ID"];
    currentToDo.Last_Updated_At=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    currentToDo.Last_Updated_By=[[SWDefaults userProfile] valueForKey:@"User_ID"];
    
    cancelButton.hidden=NO;
    submitButton.hidden=NO;
    titleTextField.isReadOnly=NO;
    messageTxtView.isReadOnly=NO;
    dateTextField.isReadOnly=NO;
    statusTextField.isReadOnly=NO;
    reminderTextField.isReadOnly = NO;
    
    
    noSelctionView.hidden=NO;
    titleTextField.text=@"";
    messageTxtView.text=@"";
    statusTextField.text=@"";
    dateTextField.text=@"";
    reminderTextField.text = @"";
    currentToDo.isNew=kYesTitle;
    currentToDo.isUpdated=kNoTitle;
    currentToDo.Status=kNewStatus;
    statusTextField.text=kNewStatus;
    if (![titleTextField isFirstResponder]) {
        [titleTextField becomeFirstResponder];
    }
    
    [self updateViewEditingStatus:YES];
}

-(void)updatedStatistics
{
    //new
    NSPredicate * newStatisticsPredicate=[NSPredicate predicateWithFormat:@"Self.Status == [cd] %@",kNewStatisticsTitle];
    NSArray * newStatsArray=[currentVisit.visitOptions.toDoArray filteredArrayUsingPredicate:newStatisticsPredicate];
    lblCountNewTask.text = [NSString stringWithFormat:@"%lu",(unsigned long)[newStatsArray count]];

    
    //Closed
    NSPredicate * closedStatisticsPredicate=[NSPredicate predicateWithFormat:@"Self.Status == [cd] %@",kClosedStatisticsTitle];
    NSArray * closedStatsArray=[currentVisit.visitOptions.toDoArray filteredArrayUsingPredicate:closedStatisticsPredicate];
    lblCountClosedTask.text = [NSString stringWithFormat:@"%lu",(unsigned long)[closedStatsArray count]];
    
    
    //Cancelled
    NSPredicate * cancelledStatisticsPredicate=[NSPredicate predicateWithFormat:@"Self.Status == [cd] %@",kCancelledStatisticsTitle];
    NSArray * cancelledStatsArray=[currentVisit.visitOptions.toDoArray filteredArrayUsingPredicate:cancelledStatisticsPredicate];
    lblCountCancelledTask.text = [NSString stringWithFormat:@"%lu",(unsigned long)[cancelledStatsArray count]];
    
    
    //deferred
    NSPredicate * deferredStatisticsPredicate=[NSPredicate predicateWithFormat:@"Self.Status == [cd] %@",kDeferedStatisticsTitle];
    NSArray * deferredStatsArray=[currentVisit.visitOptions.toDoArray filteredArrayUsingPredicate:deferredStatisticsPredicate];
    lblCountDeferredTask.text = [NSString stringWithFormat:@"%lu",(unsigned long)[deferredStatsArray count]];
    
 
    //Completed
    NSPredicate * completedStatisticsPredicate=[NSPredicate predicateWithFormat:@"Self.Status == [cd] %@",kCompletedStatisticsTitle];
    NSArray * completedStatsArray=[currentVisit.visitOptions.toDoArray filteredArrayUsingPredicate:completedStatisticsPredicate];
    lblCountCompletedTask.text = [NSString stringWithFormat:@"%lu",(unsigned long)[completedStatsArray count]];
    
    lblCountTotalTask.text = [NSString stringWithFormat:@"%lu",(unsigned long)[currentVisit.visitOptions.toDoArray count]];
    
    
    [circleChartForTasks removeFromSuperview];
    circleChartForTasks = [[PNCircleChart alloc] initWithFrame:CGRectMake(0, 0, chartView.frame.size.width, chartView.frame.size.height) total:[NSNumber numberWithInt:[currentVisit.visitOptions.toDoArray count]] current:[NSNumber numberWithInt:[completedStatsArray count]] clockwise:YES shadow:NO shadowColor:[UIColor lightGrayColor] check:NO lineWidth:@4.0f];
    
    circleChartForTasks.backgroundColor = [UIColor clearColor];
    [circleChartForTasks setStrokeColor:[UIColor clearColor]];
    [circleChartForTasks setStrokeColorGradientStart:kCompletedColor];
    [circleChartForTasks strokeChart];
    [chartView addSubview:circleChartForTasks];
}

-(void)updateToDoData:(ToDo*)selectedToDo
{
    titleTextField.text=[SWDefaults getValidStringValue:selectedToDo.Title];
    messageTxtView.text=[SWDefaults getValidStringValue:selectedToDo.Description];
    dateTextField.text = [NSString stringWithFormat:@"%@", [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:selectedToDo.Todo_Date]]];
    reminderTextField.text = [NSString stringWithFormat:@"%@", [MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDatabseDefaultDateFormat scrString:[SWDefaults getValidStringValue:selectedToDo.remindAt]]];

    if ([dateTextField.text isEqualToString:@"(null)"]) {
        dateTextField.text = [SWDefaults getValidStringValue:selectedToDo.Todo_Date];
    }
    if ([reminderTextField.text isEqualToString:@"(null)"]) {
        reminderTextField.text = [SWDefaults getValidStringValue:selectedToDo.remindAt];
    }
    statusTextField.text=[SWDefaults getValidStringValue:selectedToDo.Status];
    
    if ([selectedToDo.isUpdated isEqualToString:kYesTitle]) {
        
        [submitButton setTitle:kUpdateTitle forState:UIControlStateNormal];
    }
    else
    {
        [submitButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];

    }
    
    if ([currentToDo.Status isEqualToString:kClosedStatisticsTitle]||[currentToDo.Status isEqualToString:kCompletedStatisticsTitle]) {
        
        submitButton.hidden=YES;
        cancelButton.hidden=YES;
        titleTextField.isReadOnly=YES;
        messageTxtView.isReadOnly=YES;
        dateTextField.isReadOnly=YES;
        statusTextField.isReadOnly=YES;
        reminderTextField.isReadOnly = YES;
    }
    else
    {
        submitButton.hidden=NO;
        cancelButton.hidden=NO;
        titleTextField.isReadOnly=NO;
        messageTxtView.isReadOnly=NO;
        dateTextField.isReadOnly=NO;
        statusTextField.isReadOnly=NO;
        reminderTextField.isReadOnly = NO;

    }
    
    noSelctionView.hidden=YES;
    
    
}
-(void)updateViewEditingStatus:(BOOL)isEditing
{
    if (isEditing==YES) {
        noSelctionView.hidden=YES;
        if ([currentToDo.isNew isEqualToString:kYesTitle]) {
            statusTextField.isReadOnly=YES;
        }
        else{
            statusTextField.isReadOnly=NO;
        }
    }
    else
    {
        noSelctionView.hidden=NO;
        titleTextField.text=@"";
        messageTxtView.text=@"";
        statusTextField.text=@"";
        dateTextField.text=@"";
        currentToDo=nil;
        reminderTextField.text = @"";
    }
}

#pragma mark UITableView Methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    tableView.separatorColor=kUITableViewSaperatorColor;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES) {
        if (filteredToDoArray.count>0) {
            return filteredToDoArray.count;
        }
        else
        {
            return 0;
        }
    }
    else{
        return currentVisit.visitOptions.toDoArray.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    static NSString* identifier=@"todoCell";
    SalesWorxVisitOptionsToDoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxVisitOptionsToDoTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    ToDo * currentToDoContent=[[ToDo alloc]init];
    
    if (isSearching==YES) {
        currentToDoContent=[filteredToDoArray objectAtIndex:indexPath.row];
    }
    else
    {
        currentToDoContent=[currentVisit.visitOptions.toDoArray objectAtIndex:indexPath.row];
    }
    NSString* statusString=currentToDoContent.Status;
    if ([NSString isEmpty:statusString]==NO) {
        if ([statusString isEqualToString:kNewStatus]) {
            cell.statusView.backgroundColor=kNewToDoColor;
        }
        else if ([statusString isEqualToString:kClosedStatisticsTitle]) {
            cell.statusView.backgroundColor=kClosedToDoColor;
        }
        else if ([statusString isEqualToString:kCancelledStatisticsTitle]) {
            cell.statusView.backgroundColor=kCancelledToDoColor;
        }
        else if ([statusString isEqualToString:kDeferedStatisticsTitle]) {
            cell.statusView.backgroundColor=kDefferedToDoColor;
        }
        else if ([statusString isEqualToString:kCompletedStatisticsTitle]) {
            cell.statusView.backgroundColor=kCompletedColor;
        }
    }
    cell.titleLbl.text=[SWDefaults getValidStringValue:currentToDoContent.Title];
    
    if ([indexPathArray containsObject:indexPath]) {
        
        cell.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
    }
    
    else
    {
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        cell.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==todoTblView)
    {
        if (indexPathArray.count==0) {
            [indexPathArray addObject:indexPath];
        }
        else
        {
            indexPathArray=[[NSMutableArray alloc]init];
            [indexPathArray addObject:indexPath];
        }
        [todoTblView reloadData];
        
        selectedIndex=indexPath.row;
        selectedIndexPath=indexPath;
        if (isSearching==YES) {
            currentToDo=[filteredToDoArray objectAtIndex:indexPath.row];
        }
        else
        {
            currentToDo=[currentVisit.visitOptions.toDoArray objectAtIndex:indexPath.row];
        }
        currentToDo.isUpdated=kYesTitle;
        [self updateToDoData:currentToDo];
    }
}


#pragma mark UITextField Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //currentToDo.isUpdated=kYesTitle;
    
    if (textField ==dateTextField) {
        
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = @"Date";
        popOverVC.datePickerMode=kTodoTitle;
        selectedDateTextField = @"Date";
//        if (_txtExpiryDate.text == nil || [_txtExpiryDate.text isEqualToString:@""])
//        {
//        } else {
//            popOverVC.setMinimumDateCurrentDate=NO;
//            popOverVC.previousSelectedDate = previousSelectedDate;
//        }
        popOverVC.setMinimumDateCurrentDate=YES;

        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate=self;
        popOverVC.datePickerPopOverController=datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:dateTextField.dropdownImageView.frame inView:dateTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        return NO;
    }
    else if (textField==statusTextField)
    {
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray=toDoStatusArray;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        statusPopOverController=nil;
        statusPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        statusPopOverController.delegate=self;
        popOverVC.popOverController=statusPopOverController;
        popOverVC.titleKey=kStatus;
        [statusPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
        [statusPopOverController presentPopoverFromRect:statusTextField.dropdownImageView.frame inView:statusTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        return NO;
    } else if (textField ==reminderTextField) {
        
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = @"Reminder";
        popOverVC.datePickerMode = @"dateandTime";
        popOverVC.setMinimumDateCurrentDate=YES;
        selectedDateTextField = @"Reminder";

        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate=self;
        popOverVC.datePickerPopOverController=datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:reminderTextField.dropdownImageView.frame inView:reminderTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        return NO;
    }
    else
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==titleTextField) {
        NSString* titleText=[SWDefaults getValidStringValue:textField.text];
        currentToDo.Title=titleText;
        [titleTextField resignFirstResponder];
        [messageTxtView becomeFirstResponder];
        
    }
}

#pragma mark UITextView Delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == titleTextField) {
        [titleTextField resignFirstResponder];
        [messageTxtView becomeFirstResponder];
        return NO;
    }
    else
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
   // currentToDo.isUpdated=kYesTitle;

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==messageTxtView) {
        NSString* messageText=[SWDefaults getValidStringValue:textView.text];
        currentToDo.Description=messageText;
    }
}

#pragma mark Selected Date Delegate
-(void)didSelectDate:(NSString*)selectedDate
{
    if ([NSString isEmpty:selectedDate]==NO) {
        
        if ([selectedDateTextField isEqualToString:@"Reminder"]) {
            reminderTextField.text = [MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDatabseDefaultDateFormat scrString:selectedDate];
            currentToDo.remindAt = [MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDatabseDefaultDateFormat scrString:selectedDate];
        }else{
            dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:selectedDate]];
            currentToDo.Todo_Date = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:selectedDate];
        }

    }
}
#pragma mark Status Delegate
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSString* selectedStatus=[toDoStatusArray objectAtIndex:selectedIndexPath.row];
    if ([NSString isEmpty:selectedStatus]==NO) {
        statusTextField.text=[SWDefaults getValidStringValue:selectedStatus];
        currentToDo.Status=selectedStatus;
    }
}

#pragma mark Save Button Action

- (IBAction)submitButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    //validations
    NSString* Title=currentToDo.Title;
    NSString* Description=currentToDo.Description;
    NSString* Date=currentToDo.Todo_Date;
    NSString* Status=currentToDo.Status;
    NSString *reminder = currentToDo.remindAt;
    
    NSString* missingContent=[[NSString alloc]init];
    
    if ([NSString isEmpty:Title]) {
        missingContent=kTitle;
    }
    else if ([NSString isEmpty:Description]) {
        missingContent=kDescription;
    }
    else if ([NSString isEmpty:Date]) {
        missingContent=kDateTitle;
    }
    else if ([NSString isEmpty:Status]) {
        missingContent=kStatus;
    }
    else if ([NSString isEmpty:reminder]) {
        missingContent=kReminder;
    }
    else
    {
        
    }
    
    if ([NSString isEmpty:missingContent]==NO) {
      
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please enter %@",missingContent] withController:self];
    }
    
    else
    {
    
    if ([currentToDo.isUpdated isEqualToString:kYesTitle] && currentVisit.visitOptions.toDoArray.count>0) {
        
        [currentVisit.visitOptions.toDoArray replaceObjectAtIndex:selectedIndex withObject:currentToDo];
    }
    else{
    [currentVisit.visitOptions.toDoArray addObject:currentToDo];
    }
    
    currentToDo.Last_Updated_At=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];

    
    //save data to database
    if ([currentToDo.isUpdated isEqualToString:kYesTitle]) {
       
        NSLog(@"already existing row_ID %@", currentToDo.Row_ID);
        
    }
    else if([currentToDo.isNew isEqualToString:kYesTitle])
    
    {
        currentToDo.Row_ID=[NSString createGuid];

    }
    currentToDo.Visit_ID=currentVisit.Visit_ID;
        

    BOOL insertStatus=[SWDatabaseManager InsertToDoList:currentToDo];
    
    if (insertStatus==YES) {
        
        NSLog(@"insert successfull in To DO");
       
        NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kTodoTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                            object:self
                                                          userInfo:visitOptionDict];
        
        NSMutableArray* toDoArray=[[SWDatabaseManager retrieveManager] fetchTodoforCustomer:currentVisit.visitOptions.customer];
        if (toDoArray.count>0) {
            
            currentVisit.visitOptions.toDoArray=[[NSMutableArray alloc]init];
            currentVisit.visitOptions.toDoArray=toDoArray;
            unFilteredArray=toDoArray;
            
        }
        
    }
    
    NSLog(@"to do content added to todoarray %@", currentVisit.visitOptions.toDoArray);
    if (currentVisit.visitOptions.toDoArray.count>0) {
        //removing selection
        [indexPathArray removeAllObjects];
        [todoTblView reloadData];
        [self updatedStatistics];
        [self updateViewEditingStatus:NO];
    }
    }
}

- (IBAction)cancelButtonTapped:(id)sender {
    

    [self updateViewEditingStatus:NO];
    [indexPathArray removeAllObjects];

    [todoTblView reloadData];

}

- (IBAction)filterButtonTapped:(id)sender {
    
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    //blurredBgImage.image = [self blurWithImageEffects:[self takeSnapshotOfView:[self view]]];
    
    //[self.view addSubview:blurredBgImage];
            SalesWorxVisitOptionsToDoFilterViewController * popOverVC=[[SalesWorxVisitOptionsToDoFilterViewController alloc]init];
           // popOverVC.popOverContentArray=contentArray;
            //popOverVC.salesWorxPopOverControllerDelegate=self;
            //popOverVC.disableSearch=YES;
        popOverVC.delegate = self;
    popOverVC.previousFilterParametersDict=previousFilterParameters;
    
        popOverVC.toDoArray=unFilteredArray;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
            nav.modalPresentationStyle = UIModalPresentationPopover;
            UIPopoverPresentationController *popover = nav.popoverPresentationController;
            popover.backgroundColor=[UIColor whiteColor];
            popOverVC.preferredContentSize = CGSizeMake(376, 250);
           // popOverVC.titleKey=popOverString;
    self.modalInPopover=YES;
    
    
            popover.sourceView = filterButton;
            popover.sourceRect =filterButton.bounds;
            popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
            //[self presentViewController:nav animated:YES completion:nil];
    
    
    
    
    [self presentViewController:nav animated:YES completion:^{
        popover.passthroughViews=nil;
        self.modalInPopover=YES;

    }];
    
}

-(void)deselectPreviousCellandSelectFirstCell
{
    
    if ([todoTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        currentToDo=[currentVisit.visitOptions.toDoArray objectAtIndex:0];
        SalesWorxVisitOptionsToDoTableViewCell *cell = (SalesWorxVisitOptionsToDoTableViewCell *)[todoTblView cellForRowAtIndexPath:selectedIndexPath];
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [todoTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                        animated:YES
                                  scrollPosition:UITableViewScrollPositionNone];
        [todoTblView.delegate tableView:todoTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

-(void)toDoFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    currentVisit.visitOptions.toDoArray=unFilteredArray;
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    
    if (currentVisit.visitOptions.toDoArray.count>0) {
        [todoTblView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
        
 
    }
    else
    {
        [self updateViewEditingStatus:NO];

    }

}


-(void)toDoFilterDidClose
{
    [blurredBgImage removeFromSuperview];

}
-(void)filteredToDoList:(NSMutableArray*)filteredContent
{
    NSLog(@"filtered todo list is %@", filteredContent);
    [blurredBgImage removeFromSuperview];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    currentVisit.visitOptions.toDoArray=filteredContent;
    [todoTblView reloadData];
   // NSLog(@"customers array count in filter %d",productsArray.count);
    if (filteredContent.count>0) {
        [self deselectPreviousCellandSelectFirstCell];

    }

}
-(void)filteredToDoParameters:(NSMutableDictionary*)parametersDict
{
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    previousFilterParameters=parametersDict;
    NSLog(@"filtered parameters dict %@", previousFilterParameters);
    
}
- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}


#pragma mark Search Methods


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [toDoSearchBar setShowsCancelButton:YES animated:YES];
    if ([toDoSearchBar isFirstResponder]) {
    }
    else
    {
        [toDoSearchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchContent];
    }
    else {
        isSearching = NO;
        if (currentVisit.visitOptions.toDoArray.count>0) {
            [todoTblView reloadData];
            [todoTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                            animated:YES
                                      scrollPosition:UITableViewScrollPositionNone];
            NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            [todoTblView.delegate tableView:todoTblView didSelectRowAtIndexPath:firstIndexPath];
        }
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [toDoSearchBar setText:@""];
    [toDoSearchBar setShowsCancelButton:NO animated:YES];
    filteredToDoArray=[[NSMutableArray alloc]init];
    isSearching=NO;
    if ([toDoSearchBar isFirstResponder]) {
        [toDoSearchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (currentVisit.visitOptions.toDoArray.count>0) {
        [todoTblView reloadData];
        //[self deselectPreviousCellandSelectFirstCell];
    }
    
}




- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [toDoSearchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(void)searchContent
{
    NSString *searchString = toDoSearchBar.text;
    NSLog(@"searching with text in Todo %@",searchString);

    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Title", searchString];
    //NSPredicate *predicate2 = [NSPredicate predicateWithFormat:filter, @"Description", searchString];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1]];
    filteredToDoArray=[[currentVisit.visitOptions.toDoArray filteredArrayUsingPredicate:predicate] mutableCopy];
    if (filteredToDoArray.count>0) {
        [todoTblView reloadData];
        [todoTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                        animated:YES
                                  scrollPosition:UITableViewScrollPositionNone];
        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [todoTblView.delegate tableView:todoTblView didSelectRowAtIndexPath:firstIndexPath];
        
    }
    else if (filteredToDoArray.count==0)
    {
        isSearching=YES;
        [todoTblView reloadData];
        //[self deselectPreviousCellandSelectFirstCell];
        [self updateViewEditingStatus:NO];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
