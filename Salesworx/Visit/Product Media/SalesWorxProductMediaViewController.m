//
//  SalesWorxProductMediaViewController.m
//  MedRep
//
//  Created by Neha Gupta on 20/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxProductMediaViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepProductsHomeCollectionViewCell.h"
#import "MedRepUpdatedDesignCell.h"
#import <MediaPlayer/MPMoviePlayerController.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import "MedRepProductImagesViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepProductHTMLViewController.h"
#import "FMDatabase.h"

@interface SalesWorxProductMediaViewController ()

@end

@implementation SalesWorxProductMediaViewController

@synthesize productsPopoverController, selectedProductID;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.view.backgroundColor = UIViewControllerBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Product Media"];
    [productFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory = [SWDefaults applicationDocumentsDirectory];
    } else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    thumbnailFolderPath = [documentsDirectory stringByAppendingPathComponent:kThumbnailsFolderName];
    
    
    selectedIndexPathArray = [[NSMutableArray alloc]init];
    productsArray = [[NSMutableArray alloc]init];
    filteredProducts = [[NSMutableArray alloc]init];
    unFilteredProducts = [[NSMutableArray alloc]init];
    selectedProductDetails = [[NSMutableDictionary alloc]init];
    imagesPathArray = [[NSMutableArray alloc]init];
    productImagesArray = [[NSMutableArray alloc]init];
    productHTMLFilesArray = [[NSMutableArray alloc]init];
    demoedMediaFiles = [[NSMutableArray alloc]init];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    closeButton.tintColor = [UIColor whiteColor];
    [closeButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = closeButton;
}

-(void)viewWillAppear:(BOOL)animated
{
    productsTblView.rowHeight = UITableViewAutomaticDimension;
    productsTblView.estimatedRowHeight = 70.0;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    if (self.isMovingToParentViewController) {
        
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesProductMedia];
        
        if(self.currentVisit){
            NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kProductMediaTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                                object:self
                                                              userInfo:visitOptionDict];
        }
        
        UITapGestureRecognizer * productImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped)];
        productImageView.userInteractionEnabled=YES;
        [productImageView addGestureRecognizer:productImageGesture];
        
        if (selectedProductID == nil) {
            productsArray = [[SWDatabaseManager retrieveManager] fetchProductsWithMedia:@""];
        } else {
            xProductDetailLeadingConstraint.constant = 0.0;
            xProductTableViewWidthConstraint.constant = 0.0;
            xProductCategoryLeadingConstraint.constant = 370.0;
            productsArray = [[SWDatabaseManager retrieveManager] fetchProductsWithMedia:selectedProductID];
        }
        unFilteredProducts = productsArray;
        [self updateMediaButtonsState];
    } else {
        return;
    }
    
    if (@available(iOS 13, *)) {
        productSearchBar.searchTextField.background = [[UIImage alloc] init];
        productSearchBar.searchTextField.backgroundColor = MedRepSearchBarBackgroundColor;
    }else{
        productSearchBar.backgroundImage = [[UIImage alloc] init];
        productSearchBar.backgroundColor = MedRepSearchBarBackgroundColor;
    }
    
    
    
    
    
    if (productsArray.count>0) {
        [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
    if (selectedIndexPath!=nil) {
        
    } else {
        if (productsArray.count>0) {
            [productsTblView reloadData];
            [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES  scrollPosition:UITableViewScrollPositionBottom];
            [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            
        }
    }
    
    [productMediaCollectionView registerClass:[MedRepProductsHomeCollectionViewCell class] forCellWithReuseIdentifier:@"productHomeCell"];

    if ([videoButton isSelected]==YES) {
        collectionViewDataArray=productVideoFilesArray;
        [productMediaCollectionView reloadData];
    }
    
    else if ([pptButton isSelected]==YES)
    {
        collectionViewDataArray=productPdfFilesArray;
        [productMediaCollectionView reloadData];
    }
    else if ([imagesButton isSelected]==YES)
    {
        collectionViewDataArray=productImagesArray;
        [productMediaCollectionView reloadData];
    }
    else if ([btnHTML isSelected]==YES)
    {
        collectionViewDataArray = productHTMLFilesArray;
        [productMediaCollectionView reloadData];
    }

    
    if ([selectedButton isEqualToString:@"PPT"]) {
        collectionViewDataArray=productPdfFilesArray;
        [productMediaCollectionView reloadData];
    }
    else if ([selectedButton isEqualToString:@"Image"])
    {
        [self imagesButtonTapped:nil];
    }
    else if ([selectedButton isEqualToString:@"Video"])
    {
        [self videoImagesButtonTapped:nil];
    }
    else if ([selectedButton isEqualToString:@"HTML"])
    {
        [self btnHTML:nil];
    }
    
    for (UIView *borderView in self.view.subviews) {
        
        if ([borderView isKindOfClass:[UIView class]]) {
            
            if (borderView.tag==100 || borderView.tag==101|| borderView.tag==103||borderView.tag==1010 || borderView.tag==10001) {
                
                borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                borderView.layer.shadowOffset = CGSizeMake(2, 2);
                borderView.layer.shadowOpacity = 0.1;
                borderView.layer.shadowRadius = 1.0;
            }
        }
    }
    [self hideNoSelectionView];
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    
}

-(void)onKeyboardShow:(NSNotification *)notification
{
    [productSearchBar becomeFirstResponder];
    [productSearchBar setShowsCancelButton:YES animated:YES];
}

-(void)imageTapped
{
    if(productImagesArray.count>0)
    {
        MedRepProductImagesViewController *imagesVC = [[MedRepProductImagesViewController alloc]init];
        imagesVC.productImagesArray=imagesPathArray;
        imagesVC.imagesViewedDelegate=self;
        imagesVC.currentImageIndex=0;
        imagesVC.productDataArray=productImagesArray;
        imagesVC.selectedProductDetails=selectedProductDetails;
        imagesVC.selectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [self presentViewController:imagesVC animated:YES completion:nil];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"No product images available" withController:self];
    }
}

- (FMDatabase *)getDatabase
{
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}

-(void)closeButtonTapped {
    if (!self.isFromProductsScreen) {
        if (demoedMediaFiles.count > 0) {
            
            FMDatabase *database = [self getDatabase];
            [database open];
            [database beginTransaction];
            NSDictionary *FSR = [SWDefaults userProfile];
            
            for (ProductMediaFile *objMedia in demoedMediaFiles) {
                
                NSString *insertEdetailingQuery = @"INSERT INTO TBL_EDetailing (Row_ID, Visit_ID, Product_ID, Media_File_ID, Discussed_At, SalesRep_ID, Customer_ID, Site_Use_ID) VALUES (?,?,?,?,?,?,?,?)";
                NSMutableArray *insertEdetailingParameters = [[NSMutableArray alloc]initWithObjects:
                                                              [NSString createGuid],
                                                              self.currentVisit.Visit_ID,
                                                              [selectedProductDetails valueForKey:@"Inventory_Item_ID"],
                                                              objMedia.Media_File_ID,
                                                              objMedia.discussedAt,
                                                              [FSR stringForKey:@"SalesRep_ID"],
                                                              self.currentVisit.visitOptions.customer.Customer_ID,
                                                              self.currentVisit.visitOptions.customer.Site_Use_ID,
                                                              nil];
                
                [database executeUpdate:insertEdetailingQuery withArgumentsInArray:insertEdetailingParameters];
            }
            [database commit];
            [database close];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView DataSource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES) {
        return filteredProducts.count;
    } else {
        return productsArray.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"updatedDesignCell";
    MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (isSearching && !NoSelectionView.hidden) {
        [cell setDeSelectedCellStatus];
    } else {
        if ([selectedIndexPathArray containsObject:indexPath]) {
            [cell setSelectedCellStatus];
        }
        else {
            [cell setDeSelectedCellStatus];
        }
    }
    
    if (isSearching==YES && filteredProducts.count>0) {
        cell.titleLbl.text=[[[filteredProducts valueForKey:@"Product_Name"]objectAtIndex:indexPath.row] capitalizedString];
        cell.descLbl.text=[[[filteredProducts valueForKey:@"Brand_Code"]objectAtIndex:indexPath.row]capitalizedString];
    }
    else {
        if (productsArray.count>0) {
            cell.titleLbl.text=[[[productsArray valueForKey:@"Product_Name"]objectAtIndex:indexPath.row] capitalizedString];
            cell.descLbl.text=[[productsArray valueForKey:@"Brand_Code"]objectAtIndex:indexPath.row];
        }
    }
    
    return cell;
}

-(void)updateMediaButtonsState
{
    [imagesButton setSelected:YES];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnHTML setSelected:NO];
    
    if ([imagesButton isSelected])
    {
        imagesButton .layer.borderWidth=2.0;
        imagesButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    pptButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideNoSelectionView];
    selectedIndexPath=indexPath;
    
    if (selectedIndexPathArray.count==0) {
        [selectedIndexPathArray addObject:indexPath];
    }
    else {
        selectedIndexPathArray=[[NSMutableArray alloc]init];
        [selectedIndexPathArray addObject:indexPath];
    }
    
    [imagesButton setSelected:YES];
    [videoButton setSelected:NO];
    [pptButton setSelected:NO];
    [btnHTML setSelected:NO];
    [productsTblView reloadData];

    if (isSearching==YES) {
        selectedProductDetails = [filteredProducts objectAtIndex:indexPath.row];
    }
    else {
        selectedProductDetails = [productsArray objectAtIndex:indexPath.row];
    }

    productNameLbl.text = [selectedProductDetails valueForKey:@"Product_Name"];
    brandLbl.text = [selectedProductDetails valueForKey:@"Brand_Code"];
    categoryLbl.text = [selectedProductDetails valueForKey:@"Agency"];
    typeLbl.text = @"N/A";
    skuLbl.text = [selectedProductDetails valueForKey:@"Product_Code"];
    
    NSMutableArray* tempStockArray = [[SWDatabaseManager retrieveManager] fetchStockWithProductID:[selectedProductDetails valueForKey:@"Inventory_Item_ID"]];
    NSInteger lotQty=0;
    
    for (NSInteger i=0; i<tempStockArray.count; i++) {
        NSInteger tempLotQty = [[NSString stringWithFormat:@"%@",[[tempStockArray objectAtIndex:i] valueForKey:@"Lot_Qty"]] integerValue];
        lotQty = lotQty + tempLotQty;
    }
    
    if (lotQty==0) {
        stockLbl.text=@"N/A";
    } else {
        stockLbl.text=[NSString stringWithFormat:@"%ld", (long)lotQty];
    }    
    
    productVideoFilesArray =[[NSMutableArray alloc]init];
    productPdfFilesArray =[[NSMutableArray alloc]init];
    productImagesArray=[[NSMutableArray alloc]init];
    productHTMLFilesArray=[[NSMutableArray alloc]init];
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    [self fetchProductDetails:[selectedProductDetails valueForKey:@"Inventory_Item_ID"]];
    [self updateMediaButtonsState];
}


#pragma mark Search Bar Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [productSearchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
        
    } else {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [filteredProducts removeAllObjects];
    
    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        
        isSearching = YES;
        [self searchContent];
    }
    else {
        isSearching = NO;
        [self hideNoSelectionView];
        
        if (productsArray.count>0) {
            [productsTblView reloadData];
            [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [productSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    
    if (productsArray.count>0) {
        [productsTblView reloadData];
        [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    
    isSearching=YES;
}

-(void)searchContent
{
    NSPredicate *predicate = [SWDefaults fetchMultipartSearchPredicate:productSearchBar.text withKey:@"Product_Name"];
    filteredProducts = [[productsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    if (filteredProducts.count>0) {
        [productsTblView reloadData];
    }
    else if (filteredProducts.count==0)
    {
        isSearching=YES;
        [productsTblView reloadData];
    }
}

-(void)resetProductData
{
    productNameLbl.text=@"";
    brandLbl.text=@"";
    categoryLbl.text=@"";
    otcRxLbl.text=@"";
    skuLbl.text=@"";
    typeLbl.text=@"";
    stockLbl.text=@"";
    
    
    productImageView.image=[UIImage imageNamed:@""];
    
    imagesButton.selected=NO;
    videoButton.selected=NO;
    pptButton.selected=NO;
    [btnHTML setSelected:NO];
    
    [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
    [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
    [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
    [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
    
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
}


#pragma mark UICOllectionview methods

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(237, 240);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return collectionViewDataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productHomeCell";
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    ProductMediaFile *objMedia = [collectionViewDataArray objectAtIndex:indexPath.row];
    NSString *mediaType = objMedia.Media_Type;
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName;
    NSString* fileTempPath;
    
    if ([objMedia.isStaticData isEqualToString:@"Y"]) {
        mediaType = @"Powerpoint";
    }
    else {
        fileName = [documentsDirectory stringByAppendingPathComponent:objMedia.File_Name];
        fileTempPath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:objMedia.File_Name];
    }
    cell.selectedImgView.image=nil;
    
    if ([mediaType isEqualToString:@"Image"])
    {
        cell.placeHolderImageView.hidden = NO;
        cell.productImageView.image = [UIImage imageWithContentsOfFile:[thumbnailFolderPath stringByAppendingPathComponent:[SWDefaults getValidStringValue:objMedia.File_Name]]];
        
        cell.placeHolderImageView.image = [UIImage imageNamed:@"MedRepPlaceHolderEmpty"];
        cell.captionLbl.text = objMedia.Caption;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        
        cell.captionLbl.text = objMedia.Caption;
        cell.placeHolderImageView.image=[UIImage imageNamed:@"MedRep_VideoThumbnail"];
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        
        cell.captionLbl.text = objMedia.Caption;
        cell.placeHolderImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumb"];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        cell.selectedImgView.image = nil;
        
        cell.captionLbl.text = objMedia.Caption;
        cell.placeHolderImageView.image = [UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
    }
    
    cell.backgroundColor=[UIColor clearColor];
    if (indexPath.row== selectedCellIndex)
    {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else {
        cell.layer.borderWidth=0;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    selectedCellIndex = indexPath.row;
    ProductMediaFile *objMedia = [collectionViewDataArray objectAtIndex:indexPath.row];
    NSString *mediaType = objMedia.Media_Type;

    //make a file name to write the data to using the documents directory:
    
    if ([mediaType isEqualToString:@"Image"])
    {
        [imagesPathArray removeAllObjects];
        for (ProductMediaFile *obj in productImagesArray) {
            [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:obj.File_Name]];
        }
        MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
        imagesVC.productImagesArray=imagesPathArray;
        imagesVC.imagesViewedDelegate=self;
        imagesVC.currentVisit = self.currentVisit;
        imagesVC.currentImageIndex=indexPath.row;
        imagesVC.productDataArray=productImagesArray;
        imagesVC.selectedProductDetails=selectedProductDetails;
        imagesVC.selectedIndexPath=indexPath;
        [self presentViewController:imagesVC animated:YES completion:nil];
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
        videosVC.videosViewedDelegate=self;
        videosVC.currentVisit = self.currentVisit;
        videosVC.currentImageIndex=indexPath.row;
        videosVC.selectedIndexPath=indexPath;
        videosVC.productDataArray=productVideoFilesArray;
        [self.navigationController presentViewController:videosVC animated:YES completion:nil];
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
        pdfVC.pdfViewedDelegate=self;
        pdfVC.currentVisit = self.currentVisit;
        pdfVC.productDataArray=productPdfFilesArray;
        pdfVC.currentImageIndex=indexPath.row;
        pdfVC.selectedIndexPath=indexPath;
        [self presentViewController:pdfVC animated:YES completion:nil];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        NSString *selectedFilePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:objMedia.File_Name];
        
        /** Power point slide show files will display in external apps*/
        if([[selectedFilePath lowercaseString] hasSuffix:KppsxFileExtentionStr]){
            SalesworxDocumentDisplayManager *docDispalayManager = [[SalesworxDocumentDisplayManager alloc] init];
            [docDispalayManager ShowUserActivityShareViewInView:collectionView SourceRect:cell.frame AndFilePath:selectedFilePath];
        }else{
            MedRepProductHTMLViewController *obj = [[MedRepProductHTMLViewController alloc]init];
            obj.HTMLViewedDelegate = self;

            //TO BE REMOVED AFTER TESTING
            obj.isObjectData=NO;
            obj.currentVisit = self.currentVisit;
            obj.productDataArray = productHTMLFilesArray;
            obj.selectedIndexPath = indexPath;
            obj.currentImageIndex=indexPath.row;
            [self presentViewController:obj animated:YES completion:nil];
        }
    }
    [productMediaCollectionView reloadData];
}

#pragma mark MP Movie Player Notification

-(void)moviePlayerDonedoneButtonClicked:(NSNotification*)doneNotification{
    
    NSLog(@"done button animation clicked");
    
    NSNumber *reason = [doneNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited) {
        // Your done button action here
    }
}
- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    NSLog(@"finished playing");
    
    selectedButton=@"Video";
}

#pragma button action methods

- (IBAction)imagesButtonTapped:(id)sender {
    
    [self updateMediaButtonsState];
    collectionViewDataArray = productImagesArray;
    [productMediaCollectionView reloadData];
}

- (IBAction)videoImagesButtonTapped:(id)sender {
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:YES];
    [btnHTML setSelected:NO];
    
    if ([videoButton isSelected]) {
        videoButton.layer.borderWidth=2.0;
        videoButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;

    collectionViewDataArray = productVideoFilesArray;
    [productMediaCollectionView reloadData];
}

- (IBAction)pdfImagesButtonTapped:(id)sender {
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:YES];
    [videoButton setSelected:NO];
    [btnHTML setSelected:NO];
    
    if ([pptButton isSelected]) {
        pptButton .layer.borderWidth=2.0;
        pptButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
    
    collectionViewDataArray = productPdfFilesArray;
    [productMediaCollectionView reloadData];
}

- (IBAction)btnHTML:(id)sender
{
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnHTML setSelected:YES];
    
    if ([btnHTML isSelected]) {
        btnHTML.layer.borderWidth=2.0;
        btnHTML.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    imagesButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    
    collectionViewDataArray = productHTMLFilesArray;
    [productMediaCollectionView reloadData];
}

-(UIImage *)generateThumbImage : (NSString *)filepath
{
    NSURL *url = [NSURL fileURLWithPath:filepath];
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = 2000;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

-(UIImage*)generateThumbImageforPdf:(NSString*)filePath
{
    NSURL* pdfFileUrl = [NSURL fileURLWithPath:filePath];
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
    CGPDFPageRef page;
    
    CGRect aRect = CGRectMake(0, 0, 160, 160); // thumbnail size
    UIGraphicsBeginImageContext(aRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIImage* thumbnailImage;
    NSUInteger totalNum = CGPDFDocumentGetNumberOfPages(pdf);
    
    for(int i = 0; i < totalNum; i++ ) {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0.0, aRect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        CGContextSetGrayFillColor(context, 1.0, 1.0);
        CGContextFillRect(context, aRect);
        
        
        // Grab the first PDF page
        page = CGPDFDocumentGetPage(pdf, i + 1);
        CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFMediaBox, aRect, 0, true);
        // And apply the transform.
        CGContextConcatCTM(context, pdfTransform);
        CGContextDrawPDFPage(context, page);
        
        // Create the new UIImage from the context
        thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
        
        //Use thumbnailImage (e.g. drawing, saving it to a file, etc)
        CGContextRestoreGState(context);
    }
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(pdf);
    
    return thumbnailImage;
}

#pragma mark Top Level Data Methods

-(void)displayUpdatedData:(NSIndexPath*)selectedIndex
{
    NSMutableDictionary* currentDict=[[NSMutableDictionary alloc]init];
    if (isSearching==YES) {
        currentDict = [filteredProducts objectAtIndex:selectedIndex.row];
    }
    else {
        currentDict = [productsArray objectAtIndex:selectedIndex.row];
    }
}

-(void)fetchProductDetails:(NSString*)productID
{
    //now fetch the media files associated with this product
    NSMutableArray *productMediaFilesArray = [[SWDatabaseManager retrieveManager] fetchMediaFilesforProduct:productID];
    
    if (productMediaFilesArray.count > 0) {
        
        productImagesArray = [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
        
        productVideoFilesArray = [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
        
        productPdfFilesArray = [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
        
        productHTMLFilesArray = [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
        
        if (productImagesArray.count==0) {
            [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
        } else {
            [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
        }
        
        if (productVideoFilesArray.count==0) {
            [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
        }
        else {
            [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
        }
        
        if (productPdfFilesArray.count==0) {
            [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
        }
        else {
            [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
        }
        
        if (productHTMLFilesArray.count==0) {
            [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
        }
        else {
            [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
        }
        
        productImageView.contentMode=UIViewContentModeScaleAspectFit;
        if (productImagesArray.count>0) {
            ProductMediaFile *obj = [productImagesArray objectAtIndex:0];
            productImageView.image=[UIImage imageWithContentsOfFile:[thumbnailFolderPath stringByAppendingPathComponent:[SWDefaults getValidStringValue:obj.File_Name]]];
        }
        else {
            productImageView.image=[UIImage imageNamed:@"Default_ImgIcon"];
        }
        collectionViewDataArray = productImagesArray;
        [productMediaCollectionView reloadData];
    }
    else {
        productImageView.image=nil;
        
        [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
        [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
        [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
        [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
        
        productImageView.image=[UIImage imageNamed:@"Default_ImgIcon"];
        collectionViewDataArray=productMediaFilesArray;
        [productMediaCollectionView reloadData];
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        productMediaCollectionView.collectionViewLayout = layout;
    }
    if (productMediaFilesArray.count > 0) {
        ProductMediaFile *obj = [productMediaFilesArray objectAtIndex:0];
        brandLbl.text = [NSString stringWithFormat:@"%@", obj.Brand_Code];
        categoryLbl.text = [NSString stringWithFormat:@"%@", obj.Agency];
    }
}

-(void)removeKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (IBAction)filterButtonTapped:(id)sender
{
    if (productsArray.count > 0) {
        [self removeKeyBoardObserver];
        
        if (selectedIndexPathArray.count>0) {
            [selectedIndexPathArray replaceObjectAtIndex:0 withObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        blurredBgImage.image = [UIImage imageNamed:@"BlurView"];
        [self.view addSubview:blurredBgImage];
        
        
        [productSearchBar setShowsCancelButton:NO animated:YES];
        productSearchBar.text=@"";
        
        [self.view endEditing:YES];
        isSearching=NO;
        [self hideNoSelectionView];
        
        [productsTblView reloadData];
        [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        
        
        SalesWorxProductMediaFilterViewController *popOverVC = [[SalesWorxProductMediaFilterViewController alloc]init];
        popOverVC.contentArray=unFilteredProducts;
        popOverVC.navController=self.navigationController;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        productsPopoverController=nil;
        productsPopoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        productsPopoverController.delegate=self;
        popOverVC.filterPopOverController=productsPopoverController;
        popOverVC.filteredProductMediaDelegate=self;
        popOverVC.previouslyFilteredContent=previousFilteredParameters;
        [productsPopoverController setPopoverContentSize:CGSizeMake(366, 400) animated:YES];
        
        
        UIButton *filterBtn = (UIButton*)sender;
        CGRect btnFrame=filterBtn.frame;
        btnFrame.origin.x=btnFrame.origin.x+3;
        CGRect relativeFrame = [filterBtn convertRect:filterBtn.bounds toView:self.view];
        
        [productsPopoverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
    }
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    if (popoverController == productsPopoverController) {
        return NO;
    }
    else {
        return YES;
    }
}

-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
}

#pragma mark Filter Product delegate methods

-(void)productFilterDidCancel
{
    [self addKeyBoardObserver];
    
    [blurredBgImage removeFromSuperview];
    [productFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    [productsPopoverController dismissPopoverAnimated:YES];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    productsArray = unFilteredProducts;
    [productsTblView reloadData];
    [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

-(void)filteredProduct:(NSMutableArray *)filteredContent
{
    [self addKeyBoardObserver];
    if (filteredContent.count > 0) {
        
        isFiltered=YES;
        [productsPopoverController dismissPopoverAnimated:YES];
        [blurredBgImage removeFromSuperview];
      
        productsArray = filteredContent;
        
        [productsTblView reloadData];
        [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [productFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    }
}

#pragma mark ImagesViewed Delegate

-(void)imagesViewedinFullScreenDemo:(NSMutableArray*)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        
        NSString* selectedFileID=[[imagesArray objectAtIndex:i]valueForKey:@"Media_File_ID"] ;
        
        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else {
            [demoedMediaFiles addObject:[imagesArray objectAtIndex:i]];
        }
    }
}
-(void)fullScreenImagesDidCancel
{
    selectedButton=@"Image";
}

#pragma mark Viewed Videos delegate

-(void)videosViewedinFullScreenDemo:(NSMutableArray *)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        
        ProductMediaFile* currentFile=[imagesArray objectAtIndex:i];
        
        NSString* selectedFileID=[NSString getValidStringValue:currentFile.Media_File_ID] ;
        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else {
            [demoedMediaFiles addObject:[imagesArray objectAtIndex:i]];
        }
    }
}
-(void)fullScreenVideosDidCancel
{
    selectedButton=@"Video";
}

#pragma mark PDF's Viewed delegate

-(void)PDFViewedinFullScreenDemo:(NSMutableArray*)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        ProductMediaFile* selectedMedia=[imagesArray objectAtIndex:i];
        
        NSString* selectedFileID=[NSString getValidStringValue:selectedMedia.Media_File_ID] ;
        
        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else {
            [demoedMediaFiles addObject:[imagesArray objectAtIndex:i]];
        }
    }
}
-(void)fullScreenPDFDidCancel
{
    selectedButton=@"PPT";
}

#pragma mark HTML's Viewed delegate

-(void)htmlViewedinDemo:(NSMutableArray*)htmlArray
{
    for (NSInteger i=0; i<htmlArray.count; i++) {
        ProductMediaFile* selectedMedia=[htmlArray objectAtIndex:i];
        
        NSString* selectedFileID=[NSString getValidStringValue:selectedMedia.Media_File_ID] ;
        
        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else {
            [demoedMediaFiles addObject:[htmlArray objectAtIndex:i]];
        }
    }
}
-(void)fullScreenHTMLDidCancel
{
    selectedButton=@"HTML";
}

-(void)filteredProductParameters:(NSMutableDictionary*)filterParameters
{
    self.view.alpha=1.0;
    
    if ([self.view.subviews containsObject:blurredBgImage]) {
        [blurredBgImage removeFromSuperview];
    }
    previousFilteredParameters=filterParameters;
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    productDetailViewTopConstraint.constant = 8;
}
-(void)showNoSelectionView
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    productDetailViewTopConstraint.constant = 1000;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"application did receive memory warning in field sales product media");
}

@end
