//
//  SalesWorxProductMediaFilterViewController.h
//  MedRep
//
//  Created by Ravinder Kumar on 25/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepTextField.h"

@protocol FilteredProductMediaDelegate <NSObject>

-(void)filteredProduct:(NSMutableArray*)filteredContent;
-(void)filteredProductParameters:(NSMutableDictionary*)filterParameters;
-(void)productFilterDidCancel;

@end

@interface SalesWorxProductMediaFilterViewController : UIViewController<UITextFieldDelegate>
{
    NSMutableDictionary *filterDict;
    NSMutableArray *brandsArray, *categoryArray;
    
    id filteredProductMediaDelegate;
    NSString *selectedDataString;
    
    IBOutlet MedRepTextField *productBrandTextfield;
    IBOutlet MedRepTextField *productCategoryTextfield;
}

@property(strong,nonatomic) NSMutableDictionary *previouslyFilteredContent;

@property(strong,nonatomic) id filteredProductMediaDelegate;
@property(strong,nonatomic) UIPopoverController *filterPopOverController;
@property(strong,nonatomic) NSMutableArray *contentArray;
@property(strong,nonatomic) UINavigationController *navController;


@end
