//
//  SalesWorxProductMediaViewController.h
//  MedRep
//
//  Created by Neha Gupta on 20/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesworxDocumentDisplayManager.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxProductMediaFilterViewController.h"

@interface SalesWorxProductMediaViewController : UIViewController<FilteredProductMediaDelegate, UIPopoverControllerDelegate>
{    
    IBOutlet UITableView *productsTblView;
    IBOutlet UISearchBar *productSearchBar;
    IBOutlet UIButton *productFilterButton;
    
    
    IBOutlet UILabel *productNameLbl;
    IBOutlet UILabel *brandLbl;
    IBOutlet UILabel *categoryLbl;
    IBOutlet UILabel *otcRxLbl;
    IBOutlet UILabel *skuLbl;
    IBOutlet UILabel *typeLbl;
    IBOutlet UILabel *stockLbl;
    
    
    IBOutlet UICollectionView *productMediaCollectionView;
    IBOutlet UIButton *imagesButton;
    IBOutlet UIButton *videoButton;
    IBOutlet UIButton *pptButton;
    IBOutlet UIButton *btnHTML;
    
    
    IBOutlet UIView *productImageBGView;
    IBOutlet UIImageView *productImageView;
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *productDetailViewTopConstraint;
    IBOutlet NSLayoutConstraint *xProductDetailLeadingConstraint;
    IBOutlet NSLayoutConstraint *xProductTableViewWidthConstraint;
    IBOutlet NSLayoutConstraint *xProductCategoryLeadingConstraint;
    
    
    NSMutableArray *productsArray;
    NSMutableArray *demoedMediaFiles;
    
    
    
    
    
    
    NSMutableArray* filteredProducts;    
    
    NSMutableArray* productVideoFilesArray;
    NSMutableArray* productPdfFilesArray;
    NSMutableArray* productImagesArray;
    NSMutableArray* productHTMLFilesArray;
    
    BOOL isSearching;
    
    NSMutableArray * imagesPathArray;
    
    
    NSIndexPath *selectedIndexPath;
    
    NSMutableArray* collectionViewDataArray;
    
    UIImageView* blurredBgImage;
    
    NSString * selectedButton;
    
    NSInteger selectedCellIndex;
    
    NSMutableArray* selectedIndexPathArray;
    NSMutableDictionary* selectedProductDetails;
    
    NSMutableDictionary * previousFilteredParameters;
    
    NSString*documentsDirectory;
    
    
    
    
    BOOL isFiltered;
    NSMutableArray* unFilteredProducts;
    NSString * thumbnailFolderPath;
}

@property(strong,nonatomic) SalesWorxVisit* currentVisit;
@property(strong,nonatomic) UIPopoverController *productsPopoverController;

@property(strong,nonatomic) NSString *selectedProductID;
@property(nonatomic) BOOL isFromProductsScreen;

@end

