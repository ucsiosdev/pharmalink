//
//  SalesWorxProductMediaFilterViewController.m
//  MedRep
//
//  Created by Ravinder Kumar on 25/06/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxProductMediaFilterViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepQueries.h"

@interface SalesWorxProductMediaFilterViewController ()

@end

@implementation SalesWorxProductMediaFilterViewController
@synthesize filterPopOverController,contentArray,previouslyFilteredContent, filteredProductMediaDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
    filterDict = [[NSMutableDictionary alloc]init];
    
    if ([previouslyFilteredContent valueForKey:@"Brand_Code"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Brand_Code"] forKey:@"Brand_Code"];
        productBrandTextfield.text=[previouslyFilteredContent valueForKey:@"Brand_Code"];
    }

    if ([previouslyFilteredContent valueForKey:@"Agency"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Agency"] forKey:@"Agency"];
        productCategoryTextfield.text=[previouslyFilteredContent valueForKey:@"Agency"];
    }

    
    brandsArray = [[NSMutableArray alloc]init];
    categoryArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in contentArray) {
        if (![brandsArray containsObject:[dic valueForKey:@"Brand_Code"]]) {
            [brandsArray addObject:[dic valueForKey:@"Brand_Code"]];
        }
        if (![categoryArray containsObject:[dic valueForKey:@"Agency"]]) {
            [categoryArray addObject:[dic valueForKey:@"Agency"]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kProductMediaFilterTitle];
    }
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear",nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
}

-(void)closeFilter
{
    if ([self.filteredProductMediaDelegate respondsToSelector:@selector(productFilterDidCancel)]) {
        [self.filteredProductMediaDelegate filteredProductParameters:previouslyFilteredContent];
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];
}

-(void)clearFilter
{
    filterDict=[[NSMutableDictionary alloc]init];
    [filterDict setValue:@"Active" forKey:@"Is_Active"];
    
    productBrandTextfield.text=@"";
    productCategoryTextfield.text=@"";
}

-(void)selectedFilterValue:(NSString*)selectedString
{
    NSLog(@"selected string %@", selectedString);

    if ([selectedDataString isEqualToString:@"Agency"]) {
        [filterDict setValue:selectedString forKey:selectedDataString];
        productCategoryTextfield.text=selectedString;
        
    }
    if ([selectedDataString isEqualToString:@"Brand_Code"]) {
        [filterDict setValue:selectedString forKey:selectedDataString];
        productBrandTextfield.text=selectedString;
    }
}
- (IBAction)searchButtonTapped:(id)sender
{
    NSMutableArray *filteredProductsArray = [[NSMutableArray alloc]init];

    NSString *filter = @"%K == [c] %@";
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    
    if([[filterDict valueForKey:@"Agency"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Agency", [filterDict valueForKey:@"Agency"]]];
    }
    
    
    if([[filterDict valueForKey:@"Brand_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Brand_Code", [filterDict valueForKey:@"Brand_Code"]]];
    }
 
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    filteredProductsArray=[[self.contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    if (predicateArray.count==0) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Missing Data", nil) andMessage:NSLocalizedString(@"Please change your filter criteria and try again", nil) withController:self];
    }
    else if (filteredProductsArray.count>0 ) {
        if ([self.filteredProductMediaDelegate respondsToSelector:@selector(filteredProduct:)]) {
            
            [self.filteredProductMediaDelegate filteredProduct:filteredProductsArray];
            [self.filteredProductMediaDelegate filteredProductParameters:filterDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
    }
}

- (IBAction)resetButtonTapped:(id)sender {
    
    if ([self.filteredProductMediaDelegate respondsToSelector:@selector(productFilterDidCancel)]) {
        [self.filteredProductMediaDelegate productFilterDidCancel];
    }
    if ([self.filteredProductMediaDelegate respondsToSelector:@selector(filteredProductParameters:)]) {
        [self.filteredProductMediaDelegate filteredProductParameters:nil];
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];
}

#pragma mark UITextFieldMethods

-(void)textFieldDidTap:(MedRepTextField*)tappedTextField withTitle:(NSString*)titleString withFilterType:(NSString*)textFieldFilterType withContentArray:(NSMutableArray*)textFieldContentArray
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=textFieldFilterType;
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=titleString;
    filterDescVC.filterDescArray=textFieldContentArray;
    [self.navigationController pushViewController:filterDescVC animated:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==productBrandTextfield)
    {
        selectedDataString = @"Brand_Code";
        if (brandsArray.count>0) {
            [self textFieldDidTap:productBrandTextfield withTitle:@"Brand" withFilterType:@"Brand" withContentArray:brandsArray];
        }
        else {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
    }
    else if (textField==productCategoryTextfield)
    {
        selectedDataString=@"Agency";
        if (categoryArray.count>0) {
            [self textFieldDidTap:productCategoryTextfield withTitle:@"Category" withFilterType:@"Category" withContentArray:categoryArray];
        }
        else {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
    }
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
