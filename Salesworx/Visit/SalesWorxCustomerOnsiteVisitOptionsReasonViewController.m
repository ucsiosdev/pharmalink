//
//  SalesWorxCustomerOnsiteVisitOptionsReasonViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/24/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCustomerOnsiteVisitOptionsReasonViewController.h"

@interface SalesWorxCustomerOnsiteVisitOptionsReasonViewController ()

@end

@implementation SalesWorxCustomerOnsiteVisitOptionsReasonViewController
@synthesize contentView,reasonLbl,headerView,reasonTblView,currentVisit,isAccompaniedBy, isMarketVisit, currentVisitDetails,isOpenVisit;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(Save)]  animated:YES];
//    
   [ self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(Cancel)]  animated:YES];
    

    // Do any additional setup after loading the view from its nib.
}




-(void)viewDidAppear:(BOOL)animated
{
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
    
    //contentViewHeightConstraint.constant=contentViewHeight;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    contentView.layer.cornerRadius=5.0f;
    
    
    self.view.backgroundColor=KPopUpsBackGroundColor;

    
    if (self.isMovingToParentViewController) {
        selectedReasonArray=[[NSMutableArray alloc]init];
        
        if (isAccompaniedBy) {
            contentArray=[[SWDatabaseManager retrieveManager] fetchAccompaniedByForFieldSalesVisit];
            headerTitleLbl.text=NSLocalizedString(@"Accompanied By", nil);
            reasonLbl.text=@"Please select accompanied by";
            
        }
        else if(currentVisit.beaconValidationSuccessfull==NO && currentVisit.beaconAvailable==YES)
        {
            contentArray=[[MedRepQueries getTBLAppCodesDataForCodeType:@"BEACON_EXCEPTION_REASON"] mutableCopy];
            headerTitleLbl.text=NSLocalizedString(@"Reason", nil);
            reasonLbl.text=@"You are not at customer beacon location, please select a reason.";
        }
        else{
            contentArray=[[MedRepQueries getTBLAppCodesDataForCodeType:kOnsiteExceptionReason] mutableCopy];
            headerTitleLbl.text=NSLocalizedString(@"Reason", nil);
            reasonLbl.text=@"You are not at customer location, please select a reason.";
        }
        //NSLog(@"content array is %@", contentArray);
        if (contentArray.count>0) {
            
            //selectedIndex=0;
            selectedIndexNumber=[[NSNumber alloc]init];
            
            //[reasonTblView reloadData];
        }
        
    }
    
    contentView.backgroundColor=UIViewControllerBackGroundColor;
    
    if (self.isMerchandisingVisit) {
        
    if (self.isMovingToParentViewController) {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        //[cashCustomerDetailsView setHidden:NO];
    }
    }
    else{
    [self setHiddenAnimated:NO duration:1.25];
    }

//    if (self.isMovingToParentViewController) {
//        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
//
//    }
//    else{
//        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
//    }
    
}


- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}



- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return contentArray.count;
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(16);
    
    
    if (isAccompaniedBy) {
        SalesWorxFieldSalesAccompaniedBy *accompaniedBy=[[SalesWorxFieldSalesAccompaniedBy alloc]init];
        accompaniedBy=[contentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[MedRepDefaults getDefaultStringForEmptyString:accompaniedBy.Username];
   
    }
    else{
        TBLAppCode *appcode=[[TBLAppCode alloc]init];
        appcode=[contentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[MedRepDefaults getDefaultStringForEmptyString:appcode.Code_Value];
    }
    
//    BOOL isTheObjectThere = [selectedReasonArray containsObject: appcode.Code_Value];
//    
//    
//    
//    if ( isTheObjectThere==YES)
//    {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }
//    else
//    {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        
//    }
    
    
    
    
    if(indexPath.row == [selectedIndexNumber integerValue] && selectedIndexNumber!=nil)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.font=MedRepTitleFont;
    cell.textLabel.textColor=MedRepTableViewCellTitleColor;
    
    
  
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([self.reasonCodeDelegate respondsToSelector:@selector(selectedReasonCode:)]) {
//        
//        
//        [self.navigationController popViewControllerAnimated:NO];
//        
//        [self.reasonCodeDelegate selectedReasonCode:[MedRepDefaults getValidStringValue:[contentArray objectAtIndex:indexPath.row]]];
//    }
    
    selectedIndex=indexPath.row;
    
    selectedIndexNumber=[NSNumber numberWithInteger:indexPath.row];
    
    [reasonTblView reloadData];

//  TBLAppCode*  appcode=[contentArray objectAtIndex:indexPath.row];
//
//    
//    if ([selectedReasonArray containsObject:appcode.Code_Value]) {
//        
//        
//        [selectedReasonArray removeObject:appcode.Code_Value];
//        
//        
//    }
//    else
//    {
//        [selectedReasonArray addObject:appcode.Code_Value];
//    }
//    [reasonTblView reloadData];
    
    
    if ([self.selectedReason respondsToSelector:@selector(selectedReasonDelegate:)]) {
        
        
        if (isAccompaniedBy==YES) {
            SalesWorxFieldSalesAccompaniedBy *accompaniedBy=[[SalesWorxFieldSalesAccompaniedBy alloc]init];
            accompaniedBy=[contentArray objectAtIndex:[selectedIndexNumber integerValue]];
            currentVisit.Accompanied_By_User_ID=accompaniedBy.User_ID;
            currentVisit.Accompanied_By_User_Name=accompaniedBy.Username;
            
            [self.selectedReason selectedReasonDelegate:currentVisit];
            
        }
        else{
            TBLAppCode *appcode=[[TBLAppCode alloc]init];
            appcode=[contentArray objectAtIndex:[selectedIndexNumber integerValue]];
            
            if(isMarketVisit==YES) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    currentVisitDetails.onSiteExceptionReason=appcode.Code_Value;
                    [self.selectedReason selectedReasonDelegate:currentVisitDetails];
                });
            }
            else if (self.isMerchandisingVisit)
            {
                currentVisit.onSiteExceptionReason=appcode.Code_Value;
                [self.selectedReason selectedReasonDelegate:currentVisit];

                [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseReasonViewForMarketVisit) userInfo:nil repeats:NO];

            }
            else {
                currentVisit.onSiteExceptionReason=appcode.Code_Value;
                [self.selectedReason selectedReasonDelegate:currentVisit];
            }
        }
    }
    if(isMarketVisit==YES) {
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseReasonViewForMarketVisit) userInfo:nil repeats:NO];
    } else {
        [self.navigationController popViewControllerAnimated:NO];
    }
}


- (void)CloseReasonViewForMarketVisit
{
    //do what you need to do when animation ends...
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)Save
{
    
}
-(void)Cancel
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)canCelButtonTapped:(id)sender {
    if(isMarketVisit==YES || self.isMerchandisingVisit) {
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseReasonViewForMarketVisit) userInfo:nil repeats:NO];
    } else {
        [self.navigationController popViewControllerAnimated:NO];
    }
}
- (IBAction)saveTapped:(id)sender {
    
    if (selectedIndexNumber==nil) {
        
        if (isAccompaniedBy) {
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select Accompanied by" withController:self];
            
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select a reason" withController:self];
        }
    }
    else
    {
        
        if ([self.selectedReason respondsToSelector:@selector(selectedReasonDelegate:)]) {
            
            if (isAccompaniedBy==YES) {
                SalesWorxFieldSalesAccompaniedBy *accompaniedBy=[[SalesWorxFieldSalesAccompaniedBy alloc]init];
                accompaniedBy=[contentArray objectAtIndex:[selectedIndexNumber integerValue]];
                currentVisit.Accompanied_By_User_ID=accompaniedBy.User_ID;
                currentVisit.Accompanied_By_User_Name=accompaniedBy.Username;
                
                [self.selectedReason selectedReasonDelegate:currentVisit];
            }
            else{
                TBLAppCode *appcode=[[TBLAppCode alloc]init];
                appcode=[contentArray objectAtIndex:[selectedIndexNumber integerValue]];
                
                if(isMarketVisit==YES) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        currentVisitDetails.onSiteExceptionReason=appcode.Code_Value;
                        [self.selectedReason selectedReasonDelegate:currentVisitDetails];
                    });
                } else {
                    currentVisit.onSiteExceptionReason=appcode.Code_Value;
                    [self.selectedReason selectedReasonDelegate:currentVisit];
                }
            }
        }
        if(isMarketVisit==YES) {
            [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseReasonViewForMarketVisit) userInfo:nil repeats:NO];
        } else {
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
}
@end
