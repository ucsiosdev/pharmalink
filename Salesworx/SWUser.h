//
//  SWUser.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWUser : NSObject {
    NSInteger userId;
    NSString *username;
    NSString *password;
    NSInteger salesRepId;
    NSInteger salesRepWsId;
    NSString *organizationId;
    NSString *pdaRights;
    bool isSS;
    NSString *site;
    NSString *controlParams;
    NSString *masOrganizationId;
    NSString *employeeCode;
    NSString *employeeName;
    NSString *employeePhone;
    NSString *currencyCode;
    int decimalDigits;
    bool isDCOptional;
}

@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, assign) NSInteger salesRepId;
@property (nonatomic, assign) NSInteger salesRepWsId;
@property (nonatomic, strong) NSString *organizationId;
@property (nonatomic, strong) NSString *pdaRights;
@property (nonatomic, assign) bool isSS;
@property (nonatomic, strong) NSString *site;
@property (nonatomic, strong) NSString *controlParams;
@property (nonatomic, strong) NSString *masOrganizationId;
@property (nonatomic, strong) NSString *employeeCode;
@property (nonatomic, strong) NSString *employeeName;
@property (nonatomic, strong) NSString *employeePhone;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, assign) int decimalDigits;
@property (nonatomic, assign) bool isDCOptional;

@end