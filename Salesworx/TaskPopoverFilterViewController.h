//
//  TaskPopoverFilterViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MedRepElementTitleLabel.h"
#import "MedRepTextField.h"
//#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "NSPredicate+Distinct.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "NSString+Additions.h"

#define kStatusTextField @"Status"
#define kDateTextField @"Date"

@protocol FilteredTaskDelegate <NSObject>

-(void)filteredTaskList:(NSMutableArray*)filteredContent;
-(void)taskFilterDidClose;
-(void)taskFilterdidReset;
-(void)filteredTaskParameters:(NSMutableDictionary*)parametersDict;

@end

@interface TaskPopoverFilterViewController : UIViewController<UITextFieldDelegate,SelectedFilterDelegate,VisitDatePickerDelegate>
{
    id delegate;
    NSMutableDictionary *filterParametersDict;
    
    NSString *selectedTextField;
    NSString *selectedPredicateString;
}

@property(nonatomic) id delegate;

@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *dateTitleLbl;
@property (strong, nonatomic) IBOutlet MedRepTextField *dateTextField;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *statusTitleLbl;
@property (strong, nonatomic) IBOutlet MedRepTextField *statusTextField;

@property(strong,nonatomic) NSMutableDictionary *previousFilterParametersDict;
@property(strong,nonatomic) UINavigationController *filterNavController;
@property(strong,nonatomic) UIPopoverPresentationController *popOverPresentationController;
@property(strong,nonatomic) NSMutableArray *taskArray;
@property(strong,nonatomic) UIPopoverController *filterPopOverController;


@end


