//
//  SalesWorxDescriptionLabel5.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDescriptionLabel5.h"
#import "MedRepDefaults.h"

@implementation SalesWorxDescriptionLabel5

-(void)awakeFromNib
{
    self.font=kSWX_FONT_SEMI_BOLD(16);
    self.textColor=[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1];
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(super.text, nil);
    
}
-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
    
    
}

@end
