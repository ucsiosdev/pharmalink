//
//  MedRepUpdatedDesignCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/8/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepView.h"
#import "SalesWorxImageView.h"
typedef enum {
    KSelectionColorStatusView,
    KCustomColorStatusView,
    KNoColorStatusView
} StausViewType;

@interface MedRepUpdatedDesignCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *cellDividerImg;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *accessoryImageView;
@property (nonatomic) BOOL isCellSelected;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet MedRepView *statusView;
@property  StausViewType cellStatusViewType;
@property (strong, nonatomic) IBOutlet UIImageView *cellSpecialIndicationImageView;

-(void)setSelectedCellStatus;
-(void)setDeSelectedCellStatus;

@end
