//
//  SWCustomersListViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "SWViewController.h"
#import "SWLoadingView.h"


@interface SWCollectionCustListViewController : SWViewController < UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, UIPopoverControllerDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate>
{
    NSArray *customerList;
    UITableView *tableView;
    UISearchDisplayController *searchController;
    UISearchBar *searchBar;
    NSMutableArray *searchData;
    UIPopoverController *locationFilterPopOver;
    SWLoadingView *loadingView;
    UILabel *infoLabel;
    

    ZBarReaderViewController *reader;
    //
    int totalRecords;
    //       id target;
    SEL action;
    UIView *myBackgroundView;
    
    NSMutableDictionary *sectionList;
    NSMutableDictionary *sectionSearch;
    
    NSMutableArray*refinedCustomerListArray;
    

}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@end