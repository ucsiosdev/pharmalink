//
//  SalesWorxDropShadowView.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxDropShadowView : UIView
@property (nonatomic,strong,setter=setDropDownImage:) IBInspectable NSString *DropShadowPosition;
@property (nonatomic,strong) IBInspectable UIColor *shadowColor;
@property (nonatomic) IBInspectable BOOL setCustomColor;

@end
