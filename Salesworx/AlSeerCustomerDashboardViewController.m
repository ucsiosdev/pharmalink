//
//  AlSeerCustomerDashboardViewController.m
//  Salesworx
//
//  Created by Unique Computer Systems on 3/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerCustomerDashboardViewController.h"
#import "SWDefaults.h"
#import "DataHeaderTableViewCell.h"
#import "DataTableViewCell.h"
#import "ChartTableViewCell.h"
#import "NullFreeDictionary.h"
#import "GraphHeaderCustomerDashTableViewCell.h"
#import "GraphHeaderTableViewCell.h"
#import "SalesWorxPaymentCollectionViewController.h"
#import "SalesWorxAllSurveyViewController.h"



@interface AlSeerCustomerDashboardViewController ()

@end

@implementation AlSeerCustomerDashboardViewController

@synthesize customerOrderHistoryCustomView,customerIdLbl,customerNameLbl,customerTypeLbl,custMapView,addressLbl,cityLbl,poBoxLbl,locationLbl,pdcDueLbl,overDueLbl,avblBalanceLbl,creditLimitLbl,overallTargetLbl,totalSalesLbl,totalReturnsLbl,balanceToGoLbl,achievementLbl,agencyLbl,targetValueCatLbl,salesValueCatLbl,prevdaySalesCatLbl,achievementCatLbl,customerPieChart,slices,sliceColorsForActivities,userCurrencyCode,customerDetailsDictionary,salesStatisticsTblView,monthBtgLbl,monthSalesLbl,monthTargetLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    slices=[[NSMutableArray alloc]initWithObjects:@"50",@"25",@"25", nil];
    
    
    
    
    
   // customerPieChart.labelRadius =0;
    [customerPieChart setDelegate:self];
    [customerPieChart setDataSource:self];
    [customerPieChart setPieCenter:CGPointMake(150, 110)];
   // customerPieChart setPieRadius:<#(CGFloat)#>
    [customerPieChart setShowPercentage:YES];
    [customerPieChart setLabelColor:[UIColor blackColor]];
    customerPieChart.showPercentage=YES;
    

//    [customerPieChart setDataSource:self];
//    [customerPieChart setStartPieAngle:M_PI_2];
//    [customerPieChart setAnimationSpeed:5.0];
//    [customerPieChart setLabelFont:[UIFont fontWithName:@"ARIAL" size:12]];
//    [customerPieChart setLabelRadius:40];
//    [customerPieChart setShowPercentage:YES];
//    [customerPieChart setPieBackgroundColor:[UIColor redColor]];
//    [customerPieChart setPieCenter:CGPointMake(50, 60)];
//    [customerPieChart setUserInteractionEnabled:YES];
//    [customerPieChart setLabelShadowColor:[UIColor blackColor]];

    sliceColorsForActivities=[NSArray arrayWithObjects:
                              [UIColor redColor],
                              [UIColor blackColor],[UIColor blueColor],nil];
    
    
    
    
    [customerPieChart reloadData];
    
    [custMapView setShowsUserLocation:YES];
    [self loadCustomerDetailsData];
    
    // ** Don't forget to add NSLocationWhenInUseUsageDescription in MyApp-Info.plist and give it a string

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
}


-(void)targetDetailsLabels

{
    //fetch total Target
    
    
    
//    @property (strong, nonatomic) IBOutlet UILabel *overallTargetLbl;
//    @property (strong, nonatomic) IBOutlet UILabel *totalSalesLbl;
//    @property (strong, nonatomic) IBOutlet UILabel *totalReturnsLbl;
//    @property (strong, nonatomic) IBOutlet UILabel *balanceToGoLbl;
//    @property (strong, nonatomic) IBOutlet UILabel *achievementLbl;
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items";
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    
    
    if (targetdatArry.count>0) {
        
        
        //        NSInteger testInt =[[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]] integerValue];
        //
        //       // NSString* test=[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]];
        //
        //
        //        totalTargetLbl.text=[[NSNumber numberWithInt:test] descriptionWithLocale:[NSLocale currentLocale]];
        //
        //        //[SWDefaults formatWithThousandSeparator:testInt];
        //        [@1234567 descriptionWithLocale:[NSLocale currentLocale]];  // 1,234,567
        
        
        
        NSInteger tempTarget=[[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]] integerValue];
        
        
        NSString* strNumberOfUsers = [self formatWithThousandSeparator:tempTarget];
        
        //totalTargetLbl.text=strNumberOfUsers;
        
        overallTargetLbl.text= [[NSNumber numberWithInt:tempTarget] descriptionWithLocale:[NSLocale currentLocale]];
        
        
        
        //        totalTargetLbl.text=[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]];
        
        //calculate balance
        
        
        
        if ([[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] isEqual:[NSNull null]]) {
            
            
        }
        
        else
        {
            
            float totalTarget=[[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] floatValue];
            
            float achievedTarget=[[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] floatValue];
            //
            float balanceTogo= totalTarget-achievedTarget;
            
            float achievementPercent=(achievedTarget/totalTarget)*100;
            
            achievementLbl.text=[[NSString stringWithFormat:@"%0.2f",achievementPercent] stringByAppendingString:@"%"];
            
            

            
            int totalSales= (int) achievedTarget;
            
            NSString* salesStr = [self formatWithThousandSeparator:totalSales];

            totalSalesLbl.text=salesStr;
            
            
            
            int myInt = (int) balanceTogo;
            
            
            
            NSString* strNumber = [self formatWithThousandSeparator:myInt];
            
            balanceToGoLbl.text=strNumber;
            
            
        }
        
        
        
        

}
}

-(void)loadCustomerTargetPieChart

{
//    customerTargetView=[[CustomerTargetView alloc] initWithCustomer:customerDict] ;
//    [customerTargetView setFrame:CGRectMake(8, 198, 320, 200)];
//    [customerTargetView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
//    
//    [self.view addSubview:customerTargetView];
    
    
    
    
    NSString *achieve = @"50";//[pie valueForKey:@"Achieved_Target"] ;
    NSString *remain = @"25";//[pie valueForKey:@"Remainning_Value"] ;
    NSString* test=@"25";
    
    //        [pieChartNew removeFromSuperview];
    //
    //        pieChartNew=nil;
    
   // [self.backGroundView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    pieChartNew = [[PCPieChart alloc] initWithFrame:CGRectMake(8, 198, 320, 200)] ;
    [pieChartNew setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin];
    
    [pieChartNew setDiameter:140];
    [pieChartNew setSameColorLabel:YES];
    components = [NSMutableArray array] ;
    
    PCPieComponent *component = [PCPieComponent pieComponentWithTitle:@"Target" value:[remain floatValue]];
    PCPieComponent *component2 = [PCPieComponent pieComponentWithTitle:@"Previous Day Sales" value:[achieve floatValue]];
    PCPieComponent *component3 = [PCPieComponent pieComponentWithTitle:@"Sales to Date" value:[test floatValue]];
    
    [component2 setColour:PCColorBlue];
    [component setColour:PCColorOrange];
    [component3 setColour:PCColorGreen];
    
    [components addObject:component2];
    [components addObject:component];
    [components addObject:component3];
    [pieChartNew setComponents:components];
    
    
    
    NSLog(@"pie chart being added ");
    
    
   // [self.view addSubview: pieChartNew];
    
    
    
}

-(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}

/**- (void)displayActions:(id)sender {
    if (actions == nil)
    {
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Collection", nil),NSLocalizedString(@"Survey", nil), nil]  ;
        }
        else{
            //            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Survey", nil), nil]  ;
            
            
            //check this action sheet
            
            
            actions = [[UIActionSheet alloc]
                       initWithTitle:@"Select Folder"
                       delegate:self
                       cancelButtonTitle:nil
                       destructiveButtonTitle:nil
                       otherButtonTitles:nil];
            
            NSMutableArray* buttons=[[NSMutableArray alloc] initWithObjects:@"Start Visit",@"Survey", nil];
            
            //[actions addButtonWithTitle:@"Start Visit"];
            
            for (int nb=0; nb<2; nb++)
            {
                
                
                [actions addButtonWithTitle:@"Start Visit"];
                
            }
            
            
            
            [actions addButtonWithTitle:@"Survey"];
            
            
            
            //actions.cancelButtonIndex = [actions addButtonWithTitle: @"Cancel"];
            
            
        }
        
    }
    [actions showFromBarButtonItem:sender animated:YES];
}
**/




-(void)loadCustomerDetailsData
{
    
   // customerDict = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults customer]  ] ;
    
    
    [self fetchCategories];

    //set header labels
    
    if (customerDetailsDictionary) {
        
        
        
        
        customerNameLbl.text=[NSString stringWithFormat:@"%@",[customerDetailsDictionary valueForKey:@"Customer_Name"]];
        
        
        NSString* customerTypeStr=[customerDetailsDictionary valueForKey:@"Customer_ID"];
        
       // [SWDatabaseManager retrieveManager]fetchDataForQuery:@""
        
        
        customerIdLbl.text= [ NSString stringWithFormat:@"%@",[customerDetailsDictionary valueForKey:@"Customer_ID"]];
        customerTypeLbl.text=[NSString stringWithFormat:@"%@",[customerDetailsDictionary valueForKey:@"Customer_Type"]];
        
        
        //initial values for labels
        
        if (categoriesArray.count>0) {
            
       
        targetValueCatLbl.text=[NSString stringWithFormat:@"%@%@",userCurrencyCode,[[categoriesArray objectAtIndex:0] valueForKey:@"Target_Value"]];
        
        salesValueCatLbl.text=[NSString stringWithFormat:@"%@%@",userCurrencyCode,[[categoriesArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
        prevdaySalesCatLbl.text=[NSString stringWithFormat:@"%@%@",userCurrencyCode,[[categoriesArray objectAtIndex:0] valueForKey:@"Custom_Attribute_6"]];
        
        
            [slices removeAllObjects];
            
            [slices addObject:[[categoriesArray objectAtIndex:0] valueForKey:@"Target_Value"]];
            [slices addObject:[[categoriesArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
            [slices addObject:[[categoriesArray objectAtIndex:0] valueForKey:@"Custom_Attribute_6"]];

            
            
//            [slices addObject:@"50"];
//            [slices addObject:@"25"];
//            [slices addObject:@"25"];

            
            [customerPieChart reloadData];
            
        
        NSString* totalTgt=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:0] valueForKey:@"Target_Value"]];
        
        
        NSString* achTarget=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
        
        
            
            //update graph
            
            
            
            
        
        float totalTarget=[totalTgt floatValue];
        
        
        float achievedTarget=[achTarget floatValue];
        //
        float balanceTogo= totalTarget-achievedTarget;
        
        float achievementPercent=(achievedTarget/totalTarget)*100;
        
        achievementCatLbl.text=[[NSString stringWithFormat:@"%0.2f",achievementPercent] stringByAppendingString:@"%"];
        agencyLbl.text=[[categoriesArray objectAtIndex:0] valueForKey:@"Classification_1"];
        

        }
        
    }
    
    
    
    customerOrderHistoryView=[[CustomerOrderHistoryView alloc] initWithCustomer:customerDetailsDictionary] ;
    [customerOrderHistoryView setFrame:CGRectMake(540, 380, 480, 330)];
    [self.view addSubview:customerOrderHistoryView];
    
    
    
    customerPriceListView=[[CustomerPriceList alloc] initWithCustomer:customerDetailsDictionary] ;
    [customerPriceListView setFrame:CGRectMake(507 ,424, 480 ,276)];
    //[self.view addSubview:customerPriceListView];


    
     [customerOrderHistoryView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    

    if(customerOrderHistoryView.isToggled)
    {
        customerOrderHistoryView.orderItemView.alpha=0.0;
        customerOrderHistoryView.isToggled = NO;
    }
    [customerOrderHistoryView loadOrders];
    [customerPriceListView loadprice];

    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.toolbarHidden=YES;
    

    
    
    AppControl *appControl = [AppControl retrieveSingleton];
    
    //isOrderHistoryEnable = appControl.ENABLE_ORDER_HISTORY; //[SWDefaults appControl]count]
    isCollectionEnable = appControl.ENABLE_COLLECTION;
    
    
    
    
   UIBarButtonItem *displayActionBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(displayActions:)] ;
    
    
    
    UIBarButtonItem *displayMapBarButton  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_filter_geo"cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(displayCustomerMap:)];
    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayActionBarButton, nil];
    
    
     userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];


    
    NSMutableDictionary *refinedDict = customerDetailsDictionary;
    
    
    mapView = [[MapView alloc] init] ;
    
    place = [[Place alloc] init] ;
    
    
    NSString * latStr=[NSString stringWithFormat:@"%@",[customerDetailsDictionary objectForKey:@"Cust_Lat"]];
    NSString * longStr=[customerDetailsDictionary objectForKey:@"Cust_Long"];

    
    if ([latStr isEqualToString:@"0"]) {
        
        latStr=@"25.204490";
        longStr=@"55.2707830";
        
        
        CLLocationCoordinate2D custLocation;
        custLocation.latitude=[latStr floatValue];
        custLocation.longitude=[longStr floatValue];
        
        MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (
                                                                              custLocation, 50000, 50000);
        [self.mapView setRegion:regionCustom animated:NO];
        

    }
    
    else
    {
    
    
    place.latitude= [[customerDetailsDictionary objectForKey:@"Cust_Lat"] floatValue];
    place.longitude =  [[customerDetailsDictionary objectForKey:@"Cust_Long"] floatValue];
    
    }
    
    
    
    
    place.name= [customerDetailsDictionary objectForKey:@"Customer_Name"];
    place.otherDetail = [customerDetailsDictionary objectForKey:@"City"];
    
    [mapView showCurrentLocationAddress:place];
    
    if ([latStr isEqualToString:@"0"]) {

        
    }
    
    
    else
    {
    
    
    Annotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
    [mapView.mapView addAnnotation:Annotation];
        
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapView] ;
    
    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    [locationFilterPopOver setDelegate:self];
    
    CGRect rect = self.view.frame;
    rect.size.width = rect.size.width;
    
//    [locationFilterPopOver presentPopoverFromRect:rect inView:self.view permittedArrowDirections:NO animated:YES];
    
    popoverOriented=NO;
    

    
    NSArray *keysForNullValues = [refinedDict allKeysForObject:[NSNull null]];
    [refinedDict removeObjectsForKeys:keysForNullValues];
    
    addressLbl.text=[refinedDict valueForKey:@"Address"];
    cityLbl.text=[refinedDict valueForKey:@"CITY"];
    
    poBoxLbl.text=[refinedDict valueForKey:@"Postal_Code"];
    locationLbl.text=[refinedDict valueForKey:@"Location"];
    
    if ([[refinedDict valueForKey:@"PDC_Due"] isEqualToString:@"N/A"]) {
        
        pdcDueLbl.text=[refinedDict valueForKey:@"PDC_Due"];
        overDueLbl.text=[refinedDict valueForKey:@"Over_Due"];
    }
    
    else
    {
    
    pdcDueLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[refinedDict valueForKey:@"PDC_Due"]];
    overDueLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[refinedDict valueForKey:@"Over_Due"]];
    }
    
    
    NSString* refinedCreditLimit=[SWDefaults formatStringWithThousandSeparator:[[refinedDict valueForKey:@"Credit_Limit"]integerValue]];
    
    creditLimitLbl.text=[NSString stringWithFormat:@"Credit Limit:  %@%@ ",userCurrencyCode,refinedCreditLimit];
    avblBalanceLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[refinedDict valueForKey:@"Avail_Bal"]];
    
    
    
    [customerOrderHistoryView refreshOrderHistory];
    
    
    
    
    [self testMap];
    
    [self targetDetailsLabels];
    [self loadCustomerTargetPieChart];
    [self fetchSalesStatistics];
   
    
    
    
    
    
    
    
    
    
    
    
    
    /*** Small Chart ****/
    self.myBarChart.delegate=self;
    self.myBarChart.barGraphStyle=BAR_GRAPH_STYLE_GROUPED;
    self.myBarChart.barLabelStyle=BAR_LABEL_STYLE1;
    self.myBarChart.tag=10;
    self.myBarChart.margin=MIMMarginMake(0, -20, -10, 0);
    self.myBarChart.glossStyle=GLOSS_STYLE_1;
    self.myBarChart.xTitleStyle=XTitleStyle2;
    [self.myBarChart drawBarChart];
    self.myBarChart.backgroundColor = [UIColor clearColor];
    
    self.myBarChart.titleLabel.text = @"One";
    self.myBarChart.titleLabel1.text = @"Two";
    self.myBarChart.titleLabel2.text = @"Three";
    //
    self.myBarChart.titleLabel.frame = CGRectMake(65, 190, 100, 20);
    self.myBarChart.titleLabel1.frame = CGRectMake(205, 190, 100, 20);
    self.myBarChart.titleLabel2.frame = CGRectMake(330, 190, 100, 20);
    
    self.myBarChart.plannedViewColor.frame = CGRectMake(380,5, 30, 10);
    self.myBarChart.completedViewColor.frame = CGRectMake(380, 20, 30, 10);
    // self.myBarChart.unPlanedViewColor.frame = CGRectMake(380, 35, 30, 10);
    //self.myBarChart.missedViewColor.frame = CGRectMake(380, 50, 30, 10);
    
    self.myBarChart.plannedViewColorLable.frame = CGRectMake(415,0, 100, 20);
    self.myBarChart.completedViewColorLable.frame = CGRectMake(415, 15, 100, 20);
    //self.myBarChart.unPlanedViewColorLable.frame = CGRectMake(415, 30, 100, 20);
    //self.myBarChart.missedViewColorLable.frame = CGRectMake(415, 45, 100, 20);
    
    self.myBarChart.plannedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    self.myBarChart.completedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    //self.myBarChart.unPlanedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    //self.myBarChart.missedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}


- (void)displayActions:(id)sender {
    if (actions == nil)
    {
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Collection", nil),NSLocalizedString(@"Survey", nil), nil]  ;
        }
        else{
            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Survey", nil), nil]  ;
        }
        
    }
    [actions showFromBarButtonItem:sender animated:YES];
}

//- (void)displayActions:(id)sender {
//    if (actions == nil)
//    {
//        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
//        {
//            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Collection", nil),NSLocalizedString(@"Survey", nil), nil]  ;
//        }
//        else{
//            //            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Survey", nil), nil]  ;
//            
//            
//            //check this action sheet
//            
//            
//            actions = [[UIActionSheet alloc]
//                       initWithTitle:@"Select Folder"
//                       delegate:self
//                       cancelButtonTitle:nil
//                       destructiveButtonTitle:nil
//                       otherButtonTitles:nil];
//            
//            NSMutableArray* buttons=[[NSMutableArray alloc] initWithObjects:@"Start Visit",@"Survey", nil];
//            
//            //[actions addButtonWithTitle:@"Start Visit"];
//            
//            for (int nb=0; nb<2; nb++)
//            {
//                
//                
//                [actions addButtonWithTitle:@"Start Visit"];
//                
//            }
//            
//            
//            
//            [actions addButtonWithTitle:@"Survey"];
//            
//            
//            
//            //actions.cancelButtonIndex = [actions addButtonWithTitle: @"Cancel"];
//            
//            
//        }
//        
//    }
//    [actions showFromBarButtonItem:sender animated:YES];
//}
//



- (void)displayCustomerMap:(id)sender {
    

   

}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    //[self applyMapViewMemoryHotFix];
    popoverOriented = YES;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    //  [self applyMapViewMemoryHotFix];
    
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)testMap
{
    if (mapView == nil)
    {
        self.mapView = [[MKMapView alloc] init];
    }
   // self.mapView.frame = CGRectMake(0, 0, single.frameWidth, single.frameHeight) ;
    
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setZoomEnabled:YES];
    [self.mapView setScrollEnabled:YES];
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setUserInteractionEnabled:YES];
    
    // Singleton *single = [Singleton retrieveSingleton];
    
    
    MKUserLocation *userLocation = self.mapView.userLocation;
    
    
    CLLocationCoordinate2D custLocation;
    custLocation.latitude=[[customerDetailsDictionary objectForKey:@"Cust_Lat"] floatValue];
    custLocation.longitude=[[customerDetailsDictionary objectForKey:@"Cust_Long"] floatValue];
    
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (
                                                                    custLocation, 50000, 50000);
    [self.mapView setRegion:regionCustom animated:NO];

    
//    span.latitudeDelta=0.2;
//    span.longitudeDelta=0.2;
//    region.span=span;
    
    //[self.mapView setRegion:region animated:YES];
   // [self.mapView regionThatFits:region];
    
    
    
    place = [[Place alloc] init] ;
    place.latitude= [[customerDetailsDictionary objectForKey:@"Cust_Lat"] floatValue];
    place.longitude =  [[customerDetailsDictionary objectForKey:@"Cust_Long"] floatValue];
    place.name= [customerDetailsDictionary objectForKey:@"Customer_Name"];
    place.otherDetail = [customerDetailsDictionary objectForKey:@"City"];
    
    
    
    if (place.latitude==0) {
        
        
       NSString* latStr=@"25.204490";
      NSString*  longStr=@"55.2707830";
        
        CLLocationCoordinate2D custLocation;
        custLocation.latitude= [latStr floatValue];
        custLocation.longitude=[longStr floatValue];
        
        MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (
                                                                              custLocation, 50000, 50000);
        [self.mapView setRegion:regionCustom animated:NO];

    }
    
    
    else
    {
    
    Annotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
    [self.mapView addAnnotation:Annotation];
    }
    
    [self.mapView setDelegate:self];
    [self.view addSubview:self.mapView];
}

-(void)displayMap

{
    mapView = [[MapView alloc] init] ;
    mapView.mapView.frame = CGRectMake(710, 65, 306, 120);

    [mapView setPreferredContentSize:CGSizeMake(700,600  )];
    
    place = [[Place alloc] init] ;
    place.latitude= [[customerDetailsDictionary objectForKey:@"Cust_Lat"] floatValue];
    place.longitude =  [[customerDetailsDictionary objectForKey:@"Cust_Long"] floatValue];
    place.name= [customerDetailsDictionary objectForKey:@"Customer_Name"];
    place.otherDetail = [customerDetailsDictionary objectForKey:@"City"];
    
    [mapView showCurrentLocationAddress:place];
    
    
    
    
    
    Annotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
    [mapView.mapView addAnnotation:Annotation];
    
    
   // [self.view addSubview:mapView.mapView];
    
    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapView] ;
//    
//    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
//    [locationFilterPopOver setDelegate:self];
//    
//    CGRect rect = self.view.frame;
//    rect.size.width = rect.size.width;
//    
//    [locationFilterPopOver presentPopoverFromRect:rect inView:self.view permittedArrowDirections:NO animated:YES];
    
    
    
   // popoverOriented=NO;
}


#pragma mark mapview delegate methods

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    
    NSLog(@"user location called");
    
    MKCoordinateSpan spanCustom;
    spanCustom.latitudeDelta = 0.002;
    spanCustom.longitudeDelta = 0.002;
    
    
    CLLocationCoordinate2D customerCoordinates;
    customerCoordinates.latitude =  [[customerDetailsDictionary valueForKey:@"Cust_Lat"]doubleValue];
    
    customerCoordinates.longitude = [[customerDetailsDictionary valueForKey:@"Cust_Long"]doubleValue];
    
    if (customerCoordinates.latitude==0) {
        
        NSString* latStr=@"25.204490";
        NSString*  longStr=@"55.2707830";
        
        CLLocationCoordinate2D custLocation;
        custLocation.latitude= [latStr floatValue];
        custLocation.longitude=[longStr floatValue];
        
        MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (
                                                                              custLocation, 50000,50000);
        [self.mapView setRegion:regionCustom animated:NO];

        
    }
    
    else
    {
    
    
    
    MKCoordinateRegion regionCustom;
    regionCustom.span = span;
    regionCustom.center = customerCoordinates;
    
    
    [self.mapView setRegion:regionCustom animated:YES];

    }
    
   // MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(customerCoordinates, 80, 80);
//    [custMapView setRegion:[custMapView regionThatFits:region] animated:YES];
//    
//    // Add an annotation
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    point.coordinate = userLocation.coordinate;
//    point.title = @"Where am I?";
//    point.subtitle = @"I'm here!!!";
//    
//    [custMapView addAnnotation:point];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    
}

#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation {
    if(annotation == map.userLocation) {
        return nil;
    }
    
    
    static NSString *defaultID=@"MYLoction";
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultID] ;
    [pinView setSelected:YES animated:YES];
    [pinView setEnabled:YES];
    
    if (!pinView) {
        
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultID];
        
        
        pinView.canShowCallout=YES;
        pinView.animatesDrop=YES;
        
        
    }
    return pinView;
}

-(void)selectedStringValue:(id)value
{
   
    NSLog(@"selected value is %@", value);
    agencyLbl.text=[NSString stringWithFormat:@"%@", value];
    
    
    
    [slices removeAllObjects];
    
    

    
    
    for (NSInteger i=0; i<categoriesArray.count; i++) {
        
        
        if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Classification_1"] isEqualToString:value] ) {
            
            //update tables accordingly
            
            
            
            //[NSString stringWithFormat:@"%@",

        
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]isEqual:[NSNull null]]) {
                
                
                targetValueCatLbl.text=@"";
                [slices addObject:[NSString stringWithFormat:@"%d",0]];
                
                
                
            }
            else
            {
                
                
                targetValueCatLbl.text=[NSString stringWithFormat:@"%@%@",userCurrencyCode,[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]];
                
                [slices addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]];
                
            }
            
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]isEqual:[NSNull null]]) {
                
                [slices addObject:[NSString stringWithFormat:@"%d",0]];

                prevdaySalesCatLbl.text=@"";
                
                
            }
            else
            {
                prevdaySalesCatLbl.text=[NSString stringWithFormat:@"%@%@",userCurrencyCode,[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]];
                
                [slices addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]];
                
            }
            
            
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]isEqual:[NSNull null]]) {
                
                [slices addObject:[NSString stringWithFormat:@"%d",0]];

                salesValueCatLbl.text=@"";
                
                
            }
            else
            {
                salesValueCatLbl.text=[NSString stringWithFormat:@"%@%@",userCurrencyCode,[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
                
                [slices addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
                
            }
            
            
            
//            salesValueCatLbl.text=[NSString stringWithFormat:@"AED%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
//            prevdaySalesCatLbl.text=[NSString stringWithFormat:@"AED%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]];
            
            NSLog(@"slice values are %@", [slices description]);
            
            
            
           

            
            [customerPieChart reloadData];
            
            
            
            NSString* totalTgt=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]];
            
            
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]isEqual:[NSNull null]]) {
                achievementCatLbl.text=@"";
                
                
            }
            
            else
            {
                NSString* achTarget=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
                
                
                
                float totalTarget=[totalTgt floatValue];
                
                
                float achievedTarget=[achTarget floatValue];
                //
                float balanceTogo= totalTarget-achievedTarget;
                
                float achievementPercent=(achievedTarget/totalTarget)*100;
                
                achievementCatLbl.text=[[NSString stringWithFormat:@"%0.2f",achievementPercent] stringByAppendingString:@"%"];
                

            }
            
            
        }
        
    }
    
    
    //update lables based on selected category
    
    
    //Dismiss the popover if it's showing.
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    customPopOver.delegate=nil;
    customPopOver=nil;

}


- (IBAction)categoryBtnTapped:(id)sender {
    
    
    
    UIButton *btn  = (UIButton *)sender;
    outletPopOver = nil;
    valuePopOverController=nil;
    
    outletPopOver = [[OutletPopoverTableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    
    
    //display values for pop over
    
 
    NSMutableArray* categoryNamesArray=[[NSMutableArray alloc]init];
    
    
    
    
    for (NSInteger i=0; i<categoriesArray.count; i++) {
        
        [categoryNamesArray addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Classification_1"]];
        
        
    
    }
    
    
    
    
    
    
    outletPopOver.colorNames = categoryNamesArray;// [NSMutableArray arrayWithObjects:@"PERMANENT",@"Evaluation - 30 days", @"Evaluation - 60 days", @"Evaluation - 90 days", nil];
    outletPopOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:outletPopOver];
        [valuePopOverController presentPopoverFromRect:CGRectMake(350, 120, 175, 200) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    

}

-(void)fetchCategories

{
    NSString*categoriesQry=[NSString stringWithFormat:@"select * from TBL_Sales_Target_Items where Classification_2='%@'", [customerDetailsDictionary valueForKey:@"Customer_No"]];
    
    
    categoriesArray=[[NSMutableArray alloc]init];
    
    
    
    categoriesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:categoriesQry];

    
    NSLog(@"categories are %@", [categoriesArray description]);
    
}

-(void)updateCategoryLevelLabels:(NSMutableArray*)labelData

{
    
    
    
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items";
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    
      NSInteger tempTarget=[[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]] integerValue];
    
    if (targetdatArry.count>0) {
        
        targetValueCatLbl.text= [[NSNumber numberWithInt:tempTarget] descriptionWithLocale:[NSLocale currentLocale]];

    }
    
    
    
    
    
    NSLog(@"label data is %@", [labelData description]);
    
    targetValueCatLbl.text=@"";
    salesValueCatLbl.text=@"";
    prevdaySalesCatLbl.text=@"";
    achievementCatLbl.text=@"";
    
    
    
}


#pragma mark - XYPieChart Data Source

//- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
//{
//    //return slices.count;
//}
//
//- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
//{
//    
//  //  NSLog(@"slice val is %d", [[slices objectAtIndex:index] intValue]);
//    
//    
//   // return [[slices objectAtIndex:index] intValue];
//}
//
//- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
//{
////    if(pieChart == self.pieChartRight) return nil;
////    return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
//    
//    return nil;
//}
//
//#pragma mark - XYPieChart Delegate
//- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"will select slice at index %d",index);
//}
//- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"will deselect slice at index %d",index);
//}
//- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"did deselect slice at index %d",index);
//}
//- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"did select slice at index %d",index);
//   // self.selectedSliceLabel.text = [NSString stringWithFormat:@"$%@",[self.slices objectAtIndex:index]];
//}




-(void)fetchSalesStatistics

{
     NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items";
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *currDate = [NSDate date];
    NSDateComponents *dComp = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                          fromDate:currDate];

     month = [dComp month];

    
    NSLog(@"month is %d", month);
    
    
    [self fetchLabelDatafromDB];
    [self fetchCustomerStatistics];


    
    
    NSString* salesTargetsQry=[NSString stringWithFormat:@"SELECT Classification_1 ,Custom_Attribute_8 ,Custom_Attribute_7 ,Target_Value ,Sales_Value ,Classification_2 FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Month='%d' GROUP BY Classification_1 ORDER BY Classification_1",[customerDetailsDictionary valueForKey:@"Customer_No"],month];
    
    
    
    NSLog(@"sales statictics query is %@", salesTargetsQry);
    
    
    
    salesTargetItemsArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesTargetsQry];
    
    
    if (salesTargetItemsArray.count>0) {
        
        NSLog(@"check sales target %@", [salesTargetItemsArray description]);
        
        
        [self.salesStatisticsTblView reloadData];
        
        
        
    }

    
    
}



#pragma mark Table View Delegate Methods


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    static NSString *datacellIdentifier = @"dataTableViewCell";
//
//    DataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
//    
//    if(cell == nil) {
//        cell = [[[NSBundle mainBundle]loadNibNamed:@"DataTableViewCell" owner:nil options:nil] firstObject];
//        
//        //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
//        
//        if ([[salesTargetItemsArray valueForKey:@"Classification_1"]isEqual:[NSNull null]] || [salesTargetItemsArray valueForKey:@"Classification_1"]==nil) {
//            
//            cell.agencyLbl.text=@"-";
//        }
//        
//        else
//        {
//            cell.agencyLbl.text=
//            [[salesTargetItemsArray valueForKey:@"Classification_1"] objectAtIndex:indexPath.row];
//            
//            
//        }
//        
//        
//        
//        
//        
//        
//        
//        NSString* dailyTargetStr=[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row];
//        NSInteger dailyTarget=0;
//        
//        if (!dailyTargetStr) {
//            
//            
//        }
//        
//        else
//        {
//            
//            dailyTarget=[dailyTargetStr integerValue];
//        }
//        
//        
//        if (!dailyTarget) {
//            
//            cell.targetLbl.text=@"0";
//            
//        }
//        
//        else
//        {
//            cell.targetLbl.text= [[NSNumber numberWithInt:dailyTarget] descriptionWithLocale:[NSLocale currentLocale]];
//        }
//        
//        
//        
//        if ([[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] isEqual:[NSNull null]]|| [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]==nil) {
//            
//            
//            
//            
//            cell.MTDsalesLbl.text=@"0";
//            
//            cell.achPercentLbl.text=@"0";
//            cell.dailySalesLbl.text=@"0";
//            
//            cell.btgLbl.text=@"0";
//        }
//        
//        
//        
//        else
//        {
//            NSInteger dailySale=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]integerValue];
//            
//            
//            cell.MTDsalesLbl.text= [[NSNumber numberWithInt:dailySale] descriptionWithLocale:[NSLocale currentLocale]];
//            
//            
//            
//            
//            //cell.targetLbl.text=  [NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] ];
//            
//            
//            // cell.MTDsalesLbl.text=  [NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] ];
//            
//            
//            
//            float totalTarget=[[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] floatValue];
//            
//            float achievedTarget=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] floatValue];
//            //
//            //float balanceTogo= totalTarget-achievedTarget;
//            
//            float achievementPercent=(achievedTarget/totalTarget)*100;
//            
//            
//            if (totalTarget==0) {
//                
//                cell.achPercentLbl.text=@"0.00%";
//            }
//            
//            else
//            {
//                
//                cell.achPercentLbl.text=[[NSString stringWithFormat:@"%0.2f", achievementPercent] stringByAppendingString:@"%"];
//                
//            }
//            
//            
//            NSInteger tempTargetDsg=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row]]integerValue];
//            
//            
//            
//            
//            cell.btgLbl.text= [[NSNumber numberWithInt:tempTargetDsg] descriptionWithLocale:[NSLocale currentLocale]];
//            
//            
//            
//            NSInteger dailySalestoGo=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_8"] objectAtIndex:indexPath.row]]integerValue];
//            
//            
//            cell.dailySalesLbl.text= [[NSNumber numberWithInt:dailySalestoGo] descriptionWithLocale:[NSLocale currentLocale]];
//            
//            
//            
//            //  float btgVal=[[[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row] floatValue];
//            
//            
//            //  cell.btgLbl.text=[NSString stringWithFormat:@"%0.0f",btgVal];
//            
//        }
//        
//        
//    }
//    
//    return cell;
//

    
    
    
    static NSString *cellIdentifier = @"ChartTableViewCell";
    static NSString *datacellIdentifier = @"dataTableViewCell";
    
    
    tableView.allowsSelection = NO;
    
    
    
    
    //    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_graph_bg"]];
    //    [tempImageView setFrame:tableView.frame];
    
    //    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"Old_graph_bg"]];
    //    tableView.backgroundColor = background;
    
    //tableView.backgroundView = tempImageView;
    
    if (tableView.tag==1) {
        DataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DataTableViewCell" owner:nil options:nil] firstObject];
            
            //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
            
            if ([[salesTargetItemsArray valueForKey:@"Classification_1"]isEqual:[NSNull null]] || [salesTargetItemsArray valueForKey:@"Classification_1"]==nil) {
                
                cell.agencyLbl.text=@"-";
            }
            
            else
            {
                cell.agencyLbl.text=
                [[salesTargetItemsArray valueForKey:@"Classification_1"] objectAtIndex:indexPath.row];
                
                
            }
            
            
            
            
            
            
            
            NSString* dailyTargetStr=[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row];
            NSInteger dailyTarget=0;
            
            if (!dailyTargetStr) {
                
                
            }
            
            else
            {
                
                dailyTarget=[dailyTargetStr integerValue];
            }
            
            
            if (!dailyTarget) {
                
                cell.targetLbl.text=@"0";
                
            }
            
            else
            {
                cell.targetLbl.text= [[NSNumber numberWithInt:dailyTarget] descriptionWithLocale:[NSLocale currentLocale]];
            }
            
            
            
            if ([[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] isEqual:[NSNull null]]|| [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]==nil) {
                
                
                
                
                cell.MTDsalesLbl.text=@"0";
                
                cell.achPercentLbl.text=@"0";
                cell.dailySalesLbl.text=@"0";
                
                cell.btgLbl.text=@"0";
            }
            
            
            
            else
            {
                NSInteger dailySale=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]integerValue];
                
                
                cell.MTDsalesLbl.text= [[NSNumber numberWithInt:dailySale] descriptionWithLocale:[NSLocale currentLocale]];
                
                
                
                
                //cell.targetLbl.text=  [NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] ];
                
                
                // cell.MTDsalesLbl.text=  [NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] ];
                
                
                
                float totalTarget=[[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] floatValue];
                
                float achievedTarget=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] floatValue];
                //
                //float balanceTogo= totalTarget-achievedTarget;
                
                float achievementPercent=(achievedTarget/totalTarget)*100;
                
                
                if (totalTarget==0) {
                    
                    cell.achPercentLbl.text=@"0.00%";
                }
                
                else
                {
                    
                    cell.achPercentLbl.text=[[NSString stringWithFormat:@"%0.2f", achievementPercent] stringByAppendingString:@"%"];
                    
                }
                
                
                NSInteger tempTargetDsg=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row]]integerValue];
                
                
                
                
                cell.btgLbl.text= [[NSNumber numberWithInt:tempTargetDsg] descriptionWithLocale:[NSLocale currentLocale]];
                
                
                
                NSInteger dailySalestoGo=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_8"] objectAtIndex:indexPath.row]]integerValue];
                
                
                cell.dailySalesLbl.text= [[NSNumber numberWithInt:dailySalestoGo] descriptionWithLocale:[NSLocale currentLocale]];
                
                
                
                //  float btgVal=[[[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row] floatValue];
                
                
                //  cell.btgLbl.text=[NSString stringWithFormat:@"%0.0f",btgVal];
                
            }
            
            
        }
        
        return cell;
    }
    
    else
    {
        ChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
//        [tableView setContentOffset:CGPointMake(0, tableView.contentSize.height-tableView.frame.size.height) animated:YES];

        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ChartTableViewCell" owner:nil options:nil] firstObject];
        }
        
        cell.customerDetail=@"Dashboard";

        cell.customerDict=customerDetailsDictionary;
        
        [cell configUI:indexPath];
        
        
        return cell;
        
        
    }
    
    
    

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   
    
    if (tableView.tag==0) {
        
        static NSString *datacellIdentifier = @"dataTableViewCell";
        
        GraphHeaderCustomerDashTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GraphHeaderCustomerDashTableViewCell" owner:nil options:nil] firstObject];
            
            //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
        }
        
        return cell;

    }
    
    
    else
    {
    static NSString *datacellIdentifier = @"dataTableViewCell";
    
    DataHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DataHeaderTableViewCell" owner:nil options:nil] firstObject];
        
        //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
    }
    
    return cell;

    
    }
    
}// custom view for header. will be adjusted to default or specified header height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==0) {
        
        return 200;
    }
    
    else
    {
        return 50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView.tag==0) {
        
        return 1;
    }
   
    
    else
    {
    
    
    if (salesTargetItemsArray.count>0) {
        
        
        return salesTargetItemsArray.count;
    }
    
    else
    {
        return 0;
    }
        
    }
}


-(void)fetchLabelDatafromDB

{
    
    
    
    
    NSString* labelDataQry =[NSString stringWithFormat:@"select SUM(Sales_Value) AS Sales_Value , SUM(Target_Value) AS Target_Value, sum(Custom_Attribute_7) AS Balance_To_Go  FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Month='%d'",[customerDetailsDictionary valueForKey:@"Customer_No"],month];
    
    
    //topLevelDataArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:labelDataQry];
    
    
    NSMutableArray* testArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:labelDataQry];
    
    topLevelDataArray=[self nullFreeArray:testArray];
    
    
    if (topLevelDataArray.count>0) {
        
    
    NSLog(@"top level data is %@", [topLevelDataArray description]);
    
    
    NSString * balanceToGo=[NSString stringWithFormat:@"%@",[[topLevelDataArray objectAtIndex:0] valueForKey:@"Balance_To_Go"]];
    monthBtgLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[balanceToGo integerValue]]];
    
    NSString * salesValue=[NSString stringWithFormat:@"%@",[[topLevelDataArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
    monthSalesLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[salesValue integerValue]]];
    
    NSString * targetValue=[NSString stringWithFormat:@"%@",[[topLevelDataArray objectAtIndex:0] valueForKey:@"Target_Value"]];
    monthTargetLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[targetValue integerValue]]];
    }
    
    
}


-(void)fetchCustomerStatistics

{
    
   
    if (customerDetailsDictionary.count>0) {
        
    
    
    NSString* targetQry=[NSString stringWithFormat:@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value],SUM(custom_Attribute_7) AS Balance_To_Go, SUM(Custom_Attribute_6) As [Previous_Day_Sales] FROM TBL_Sales_Target_Items where Classification_2 ='%@' and Month='%d' ",[customerDetailsDictionary valueForKey:@"Customer_No"],month];
    
    NSLog(@"query for customer statistics %@", targetQry);
    
    NSMutableArray* responseArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    
    NSMutableArray* targetdatArry= [self nullFreeArray:responseArray];
    
    
        float totalTarget;
        float achievedTarget;
        float prevDaySales;
        
        
    if (targetdatArry.count>0) {
        
        NSString* checkTargetStr=[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0];
        
        
        if ([[[targetdatArry valueForKey:@"Target_Value"]objectAtIndex:0]isEqual:[NSNull null]]) {
            
            
            totalTarget=0.0;
            
        }
        
        else
        {

     totalTarget=[[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] floatValue];
        }
        
        
        if ([[[targetdatArry valueForKey:@"Sales_Value"]objectAtIndex:0]isEqual:[NSNull null]]) {
            
            
            achievedTarget=0.0;
            
        }
        
        else
        {
        
        
     achievedTarget=[[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] floatValue];
        }
        
        
        if ([[[targetdatArry valueForKey:@"Previous_Day_Sales"]objectAtIndex:0]isEqual:[NSNull null]]) {
            
            
            prevDaySales=0.0;
            
        }
        
        else
        {
        
     prevDaySales=[[[targetdatArry valueForKey:@"Previous_Day_Sales"] objectAtIndex:0] floatValue];
        }

    //
    float balanceTogo= totalTarget-achievedTarget;
    
    float achievementPercent=(achievedTarget/totalTarget)*100;
    
        
        NSString* targetStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]];
        NSString* achTargetStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0]];
        NSString* precDaySaleStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Previous_Day_Sales"] objectAtIndex:0]];
         NSString* btgStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Balance_To_Go"] objectAtIndex:0]];
        
        
        self.totalTargetSalesStatsLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[targetStr integerValue]]];
        
         self.totalSalesStatsLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[achTargetStr integerValue]]];
        
        self.prevDaySalesStatsLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[precDaySaleStr integerValue]]];
        
        self.btgSalesStatsLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[btgStr integerValue]]];
        
        
        if (totalTarget==0) {
            
            self.achSalesStatsLbl.text=@"0.00%";
        }
        
        else
        {
            
            self.achSalesStatsLbl.text=[[NSString stringWithFormat:@"%0.2f", achievementPercent] stringByAppendingString:@"%"];
            
        }
        
        
       
        
        
   
    }

    }
}


-(NSMutableArray*)nullFreeArray:(NSMutableArray*)contentArray

{
    
    
    for(NSInteger i=0;i<contentArray.count;i++)
    {
        if([[contentArray objectAtIndex:i] isKindOfClass:[NSNull class]])
            [contentArray replaceObjectAtIndex:i withObject:@""];
    }
    
    return contentArray;
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [SWDefaults setCustomer:customerDetailsDictionary];
    if (buttonIndex == 0)
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.planDetailId = @"0";
        if([[customerDetailsDictionary stringForKey:@"Cash_Cust"]isEqualToString:@"Y"]){
            single.isCashCustomer = @"Y";
            CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:customerDetailsDictionary] ;
            [self.navigationController pushViewController:cashCustomer animated:YES];
            cashCustomer = nil;
        }
        else{
            single.isCashCustomer = @"N";
            [[[SWVisitManager alloc] init] startVisitWithCustomer:customerDetailsDictionary parent:self andChecker:@"D"];
        }
    }
    else if (buttonIndex == 1)
    {
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            SalesWorxPaymentCollectionViewController *collectionViewController = [[SalesWorxPaymentCollectionViewController alloc] initWithCustomer:customerDetailsDictionary] ;
            [self.navigationController pushViewController:collectionViewController animated:YES];
        }
        else
        {
//            SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
            SalesWorxAllSurveyViewController *objSurveyVC =[[SalesWorxAllSurveyViewController alloc] initWithNibName:@"SalesWorxAllSurveyViewController" bundle:nil];
            [self.navigationController pushViewController:objSurveyVC animated:YES];
        }
        
    }
    else if (buttonIndex == 2)
    {
//        SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
        SalesWorxAllSurveyViewController *objSurveyVC =[[SalesWorxAllSurveyViewController alloc] initWithNibName:@"SalesWorxAllSurveyViewController" bundle:nil];
        [self.navigationController pushViewController:objSurveyVC animated:YES];
        
    }
}

@end
