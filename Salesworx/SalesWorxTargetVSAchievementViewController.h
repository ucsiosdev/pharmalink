//
//  SalesWorxTargetVSAchievementViewController.h
//  MedRep
//
//  Created by Nethra on 9/12/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface SalesWorxTargetVSAchievementViewController : UIViewController<UIPopoverPresentationControllerDelegate>
{
    IBOutlet UITableView *tblTargetVSAchievement;
    IBOutlet MedRepTextField *txtTargetType;
    IBOutlet MedRepTextField *txtYearlyCategory;
    IBOutlet MedRepTextField *txtYPeriodCategory;
    IBOutlet UILabel *lblTotalAmount;
    IBOutlet UILabel *lblSalesAmount;
    IBOutlet UILabel *lblIdealAmount;
    IBOutlet UILabel *lblActaulAmount;
    IBOutlet UILabel *lblSelectMonthOrQuarter;
    IBOutlet UILabel *lblCurrency1;
    IBOutlet UILabel *lblCurrency2;
    IBOutlet UILabel *lblCurrency3;
    IBOutlet UILabel *lblCurrency4;
    IBOutlet UILabel *lblTotalTarget;


    NSString * popString,*targetType;
    NSMutableArray *arrDocumentType,*arrCustomerName,*appcodeValue,*yearlyArr,*quartelyArr,*monthlyArr,*arrDocumentType1,*arrBlockedCustomerData;
    int row;
    IBOutlet UIView *viewFooter;

    
}
@property(strong,nonatomic) NSMutableArray *arrDocumentType;

@end

NS_ASSUME_NONNULL_END
