////
////  ReviewDocumentViewController.h
////  Salesworx
////
////  Created by msaad on 6/13/13.
////  Copyright (c) 2013 msaad. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "SWPlatform.h"
//#import "CustomersListViewController.h"
//#import "CustomerPopOverViewController.h"
//
//@interface ReviewDocumentViewController: SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
//{
//    GridView *gridView;
//    IBOutlet UITableView *filterTableView;
//    
//    UIPopoverController *currencyTypePopOver;
//    CustomersListViewController *currencyTypeViewController;
//    NSMutableDictionary *customerDict;
//    
//    NSDate *selectedDate;
//    UIPopoverController *datePickerPopOver;
//    NSString *fromDate;
//    NSString *toDate;
//    BOOL isFromDate;
//    
//    CustomerPopOverViewController *customVC;
//    UIPopoverController *customPopOver;
//    NSString *custType;
//    NSString *DocType;
//    BOOL isCustType;
//    NSMutableArray *dataArray;
//
//}
//- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict andCustomer : (NSMutableDictionary *)customer;
//@end









//
//  ReviewDocumentViewController.h
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "CustomersListViewController.h"
#import "CustomerPopOverViewController.h"

@interface ReviewDocumentViewController: SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
{
    GridView *gridView;
    IBOutlet UITableView *filterTableView;
    
    UIPopoverController *currencyTypePopOver;
    CustomersListViewController *currencyTypeViewController;
    NSMutableDictionary *customerDict;
    
    NSDate *selectedDate;
    UIPopoverController *datePickerPopOver;
    NSString *fromDate;
    NSString *toDate;
    BOOL isFromDate;
    NSDate * minDate;
    
    CustomerPopOverViewController *customVC;
    UIPopoverController *customPopOver;
    NSString *custType;
    NSString *DocType;
    BOOL isCustType;
    NSMutableArray *dataArray;
    NSDateFormatter* toDateFormatter;
    NSArray* convertedAmountArray;
    
}
- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict andCustomer : (NSMutableDictionary *)customer;

@property(strong,nonatomic)SWDatePickerViewController *datePickerViewControllerDate;
@end

