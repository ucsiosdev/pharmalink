//
//  SWBarButtonItem.h
//  SWPlatform
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (SalesWorx)

+ (UIBarButtonItem *)buttonWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)labelButtonWithTitle:(NSString *)title;
+ (UIBarButtonItem *)labelButtonWithLabel:(UILabel *)label;

@end