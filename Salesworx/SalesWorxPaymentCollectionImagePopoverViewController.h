//
//  SalesWorxPaymentCollectionImagePopoverViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/27/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "SalesWorxPaymentCollectionImagePopOverTableViewCell.h"
#import "SalesWorxTableView.h"
#import "LPOImagesGalleryCollectionDetailViewController.h"

@protocol CollectionImageDelegate <NSObject>

-(void)capturedImages:(NSMutableArray*)imagesArray;


@end


@interface SalesWorxPaymentCollectionImagePopoverViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,LPOImagesGalleryCollectionDetailViewControllerDelegate>
{
    id delegate;
    PaymentImage * selectedImageObject;
    IBOutlet SalesWorxTableView *imagesTableView;
    NSInteger selectedIndex;
    
    
}
@property(strong,nonatomic) UIPopoverPresentationController * popOverController;
@property(strong,nonatomic) NSMutableArray* contentArray;
@property(nonatomic) id delegate;
@end
