//
//  SalesWorxAllSurveyViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 6/2/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxAllSurveyViewController.h"
#import "SurveyViewController.h"
#import "SurveyDetails.h"
#import "DataSyncManager.h"
#import "SWPlatform.h"
#import "NewSurveyViewController.h"
#import "SalesWorxFeedbackViewController.h"
#import "SalesWorxSurveyCollectionViewCell.h"


@interface SalesWorxAllSurveyViewController ()

@end

@implementation SalesWorxAllSurveyViewController
@synthesize SurveyArray,customer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.view.backgroundColor=UIViewBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Survey"];
    
    
    //Set back button
    self.navigationItem.hidesBackButton = NO;
    tblView.backgroundView = nil;
    tblView.backgroundColor = [UIColor whiteColor];
    //Set Page count
    appDelegate=[DataSyncManager sharedManager];
    appDelegate.alertMessageShown=0;
    appDelegate.currentPage=0;
    appDelegate.MaxPages=0;

    SurveyArray =[[SWDatabaseManager retrieveManager] selectSurveyWithCustomerID:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Site_Use_ID"]];
    
    _lblName.text = [[NSString stringWithFormat:@"%@", [customer valueForKey:@"Customer_Name"]] capitalizedString];
    _lblNumber.text = [NSString stringWithFormat:@"%@", [customer valueForKey:@"Customer_No"]];
    
    if ([[customer valueForKey:@"Cust_Status"] isEqualToString:@"Y"]) {
        [_btnStatus setBackgroundColor:CustomerActiveColor];
    } else {
        [_btnStatus setBackgroundColor:[UIColor redColor]];
    }
    
    [self.surveyCollectionView registerClass:[SalesWorxSurveyCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.toolbarHidden=YES;
}
-(void)btnBackPressed
{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
}

#pragma mark Collection Datasource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [SurveyArray count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(147, 150);
}
// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    SalesWorxSurveyCollectionViewCell *cell = (SalesWorxSurveyCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.captionLbl.text = [[SurveyArray objectAtIndex:indexPath.row] valueForKey:@"Survey_Title"];
    cell.surveyNumber.text = [NSString stringWithFormat:@"Survey %d",(indexPath.row + 1)];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SurveyDetails *info =[SurveyArray objectAtIndex:indexPath.row];
    
    NSLog(@"sales rep id is %d", appDelegate.surveyDetails.SalesRep_ID);
    NSLog(@"info %d, %@, %@", info.Survey_ID,info.Survey_Title,info.Survey_Type_Code);
    
    NSArray *tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Site_Use_ID"] ansSurveyID:[NSString stringWithFormat:@"%d",info.Survey_ID]];
    
    NSLog(@"temp array in did select %@", [tempArray description]);
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
    
    if([tempArray count]==0)
    {
        appDelegate.surveyDetails = [SurveyArray objectAtIndex:indexPath.row];
        
        NSLog(@"survey old  id %d", appDelegate.surveyDetails.Survey_ID);
        NSLog(@"sales rep ID %d", appDelegate.surveyDetails.SalesRep_ID);
        NSLog(@"Emp code %@", [[SWDefaults userProfile]stringForKey:@"Emp_Code"]);
        
        NSDate *date =[NSDate date];
        NSDateFormatter *formater =[NSDateFormatter new];
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formater setLocale:usLocale];
        NSString *dateString = [formater stringFromDate:date];
        
        NSLog(@"Survey Time Stamp is %@", dateString);
        
        
        if([info.Survey_Type_Code isEqualToString:@"N"]) {
            NSLog(@"app delegate key %d", appDelegate.key);
        }
        
        SalesWorxFeedbackViewController *newSurveyVC = [[SalesWorxFeedbackViewController alloc]init];
        newSurveyVC.customerID = [customer stringForKey:@"Customer_ID"];
        if (self.surveyParentLocation)
        {
            newSurveyVC.surveyParentLocation=self.surveyParentLocation;
        }
        [self.navigationController pushViewController:newSurveyVC animated:YES];
    }
    else
    {
        NSMutableDictionary *tempDictionary=[tempArray objectAtIndex:0];
        NSDateFormatter* df = [NSDateFormatter new];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [df setLocale:usLocale];
        NSDate *selectedDate = [df dateFromString:[tempDictionary stringForKey:@"Start_Time"]];
        int noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
        df=nil;
        usLocale=nil;
        if(noOfDays <= 0)
        {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Survey has been already submitted for this customer", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
        }
        else
        {
            appDelegate.surveyDetails = [SurveyArray objectAtIndex:indexPath.row];
            NSLog(@"survey old %@", [SurveyArray objectAtIndex:indexPath.row]);
            
            SalesWorxFeedbackViewController *newSurveyVC = [[SalesWorxFeedbackViewController alloc]init];
            newSurveyVC.customerID = [customer stringForKey:@"Customer_ID"];
            [self.navigationController pushViewController:newSurveyVC animated:YES];
        }
    }
}
- (int)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2
{
    NSUInteger unitFlags = NSDayCalendarUnit;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return [components day];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
}


@end
