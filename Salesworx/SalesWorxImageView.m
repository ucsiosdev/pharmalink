//
//  SalesWorxImageView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxImageView.h"

@implementation SalesWorxImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    if(self.RTLSupport)
    {
        if ([UIImageView userInterfaceLayoutDirectionForSemanticContentAttribute:self.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
            self.image= [UIImage imageWithCGImage:self.image.CGImage
                                            scale:1.0 orientation: UIImageOrientationUpMirrored];
        }
    }
    [super awakeFromNib];
}
-(void)setImage:(UIImage *)image
{
    if(self.RTLSupport)
    {
        if ([UIImageView userInterfaceLayoutDirectionForSemanticContentAttribute:self.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
            super.image= [UIImage imageWithCGImage:image.CGImage
                                                     scale:1.0 orientation: UIImageOrientationUpMirrored];
        }
        else
        {
            super.image=image;
        }
        
    }
    else
    {
        super.image=image;
    }
}


@end
