//
//  MedRepProductsHomeCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/27/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepProductsHomeCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productImageViewTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productImageViewLeadingCOnstraint;
@property (strong, nonatomic) IBOutlet UILabel *captionLbl;
@property (strong, nonatomic) IBOutlet UIImageView *captionBackGroundImageView;

@property (strong, nonatomic) IBOutlet UIImageView *placeHolderImageView;


@property (strong, nonatomic) IBOutlet UIImageView *selectedImgView;

@property (strong, nonatomic) IBOutlet UIImageView *productImageView;

@end
