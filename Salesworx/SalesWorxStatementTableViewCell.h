//
//  SalesWorxStatementTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxStatementTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDocumentNo;
@property (weak, nonatomic) IBOutlet UILabel *lblInvoiceDate;
@property (weak, nonatomic) IBOutlet UILabel *lblBalanceAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblPaidAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblSettledAmount;


@end
