//
//  MedRepProductsCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepProductsCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *selectedImgView;
@property (strong, nonatomic) IBOutlet UILabel *captionLbl;
@property (strong, nonatomic) IBOutlet UIImageView *placeHolderImageView;
@property (strong, nonatomic) IBOutlet UIImageView *captionBgView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productImgViewTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productImageViewTrailingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productimageViewLeadingConstraint;

@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@end
