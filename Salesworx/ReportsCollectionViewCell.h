//
//  ReportsCollectionViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepElementDescriptionLabel.h"


@interface ReportsCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UITextView *descTxtView;
@property (strong, nonatomic) IBOutlet UIImageView *optionImageView;
@property (strong, nonatomic) IBOutlet UIView *layerView;
@property(strong,nonatomic) IBOutlet MedRepElementTitleLabel * titleLbl;
@property(strong,nonatomic) IBOutlet MedRepElementDescriptionLabel * descLbl;



@end
