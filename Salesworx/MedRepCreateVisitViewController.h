//
//  MedRepCreateVisitViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/18/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepCreateVisitViewController : UIViewController

{
    id dismissDelegate;

}

@property (strong, nonatomic) IBOutlet UITextField *locationTxtFld;

@property(nonatomic)     id dismissDelegate;

@property (strong, nonatomic) IBOutlet UITextField *doctorTxtFld;

@property (strong, nonatomic) IBOutlet UITextField *dateTimeTxtFld;

- (IBAction)dateTimeButtonTapped:(id)sender;
- (IBAction)addDoctorButtonTapped:(id)sender;

- (IBAction)addLocationButtonTapped:(id)sender;



















@end
