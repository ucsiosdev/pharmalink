//
//  DailyVisitSummaryViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "DailyVisitSummaryViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"

@interface DailyVisitSummaryViewController ()

@end

@implementation DailyVisitSummaryViewController
@synthesize contentScrollView, segmentControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kDailyVisitSummaryTitle localizedCapitalizedString], nil)];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self prepareSegmentControl];
    [self prepareChildViewControllers];
    
    if (![SWDefaults isMedRep]) {
        segmentTopConstraint.constant = -44;
    }
}

-(void)prepareSegmentControl
{
    segmentControl.titleTextAttributes = [SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    segmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    segmentControl.sectionTitles = @[NSLocalizedString(@"Field Sales", nil), NSLocalizedString(@"Field Marketing", nil)];
    
    segmentControl.selectionIndicatorBoxOpacity=0.0f;
    segmentControl.verticalDividerEnabled=YES;
    segmentControl.verticalDividerWidth=1.0f;
    segmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    segmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    segmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentControl.tag = 2;
    segmentControl.selectedSegmentIndex = 0;
    
    segmentControl.layer.shadowColor = [UIColor blackColor].CGColor;
    segmentControl.layer.shadowOffset = CGSizeMake(2, 2);
    segmentControl.layer.shadowOpacity = 0.1;
    segmentControl.layer.shadowRadius = 1.0;
    
    __weak typeof(self) weakSelf = self;
    [segmentControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.contentScrollView setContentOffset:CGPointMake(contentScrollView.frame.size.width * index, 0)];
    }];
}

#pragma mark Child Viewcontrollers

-(void)prepareChildViewControllers
{
    [contentScrollView setPagingEnabled:YES];
    
    AppControl *appControl = [AppControl retrieveSingleton];
    if([appControl.ENABLE_PDARIGHTS_REPORTS isEqualToString:KAppControlsYESCode])
    {
        if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
        {
            NSString *PDARIGHTS = [[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]] trimString];
            NSArray *arrPDARights = [PDARIGHTS componentsSeparatedByString:@","];
            
            if([arrPDARights containsObject:KReportsDailyVisitSummary_FieldSales_PDA_RightsCode] && [arrPDARights containsObject:KReportsDailyVisitSummary_FieldMarketing_PDA_RightsCode]) {
                fieldSalesView = [[SalesWorxFieldSalesDailyVisitSummaryViewController alloc] initWithNibName:@"SalesWorxFieldSalesDailyVisitSummaryViewController" bundle:[NSBundle mainBundle]];
                fieldMarketingView = [[SalesWorxFieldMarketingDailyVisitSummaryViewController alloc] initWithNibName:@"SalesWorxFieldMarketingDailyVisitSummaryViewController" bundle:[NSBundle mainBundle]];
                
                fieldSalesView.view.frame = CGRectMake(0, 0, contentScrollView.frame.size.width, contentScrollView.frame.size.height);
                fieldMarketingView.view.frame = CGRectMake(contentScrollView.frame.size.width, 0, contentScrollView.frame.size.width, contentScrollView.frame.size.height);
                
                [contentScrollView addSubview:fieldSalesView.view];
                [contentScrollView addSubview:fieldMarketingView.view];
                
                [self addChildViewController:fieldSalesView];
                [self addChildViewController:fieldMarketingView];
            }
            else if ([arrPDARights containsObject:KReportsDailyVisitSummary_FieldSales_PDA_RightsCode]) {
                segmentTopConstraint.constant = -44;
                fieldSalesView = [[SalesWorxFieldSalesDailyVisitSummaryViewController alloc] initWithNibName:@"SalesWorxFieldSalesDailyVisitSummaryViewController" bundle:[NSBundle mainBundle]];
                fieldSalesView.view.frame = CGRectMake(0, 0, contentScrollView.frame.size.width, contentScrollView.frame.size.height);
                [contentScrollView addSubview:fieldSalesView.view];
                [self addChildViewController:fieldSalesView];
            }
            else if ([arrPDARights containsObject:KReportsDailyVisitSummary_FieldMarketing_PDA_RightsCode] && [SWDefaults isMedRep]) {
                segmentTopConstraint.constant = -44;
                fieldMarketingView = [[SalesWorxFieldMarketingDailyVisitSummaryViewController alloc] initWithNibName:@"SalesWorxFieldMarketingDailyVisitSummaryViewController" bundle:[NSBundle mainBundle]];
                fieldMarketingView.view.frame = CGRectMake(0, 0, contentScrollView.frame.size.width, contentScrollView.frame.size.height);
                [contentScrollView addSubview:fieldMarketingView.view];
                [self addChildViewController:fieldMarketingView];
            } else {
                segmentTopConstraint.constant = -44;
            }
        }
    }
    else {
        fieldSalesView = [[SalesWorxFieldSalesDailyVisitSummaryViewController alloc] initWithNibName:@"SalesWorxFieldSalesDailyVisitSummaryViewController" bundle:[NSBundle mainBundle]];
        fieldMarketingView = [[SalesWorxFieldMarketingDailyVisitSummaryViewController alloc] initWithNibName:@"SalesWorxFieldMarketingDailyVisitSummaryViewController" bundle:[NSBundle mainBundle]];
        
        fieldSalesView.view.frame = CGRectMake(0, 0, contentScrollView.frame.size.width, contentScrollView.frame.size.height);
        fieldMarketingView.view.frame = CGRectMake(contentScrollView.frame.size.width, 0, contentScrollView.frame.size.width, contentScrollView.frame.size.height);
        
        [contentScrollView addSubview:fieldSalesView.view];
        [contentScrollView addSubview:fieldMarketingView.view];
        
        [self addChildViewController:fieldSalesView];
        [self addChildViewController:fieldMarketingView];
    }
}

#pragma mark UIScrollView methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView == contentScrollView){
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        [segmentControl setSelectedSegmentIndex:page animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
