//
//  SWOrderHistoryViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "CustomerOrderHistoryView.h"


@interface SWOrderHistoryViewController : SWViewController {
    CustomerOrderHistoryView *orderHistoryView;
    NSDictionary *customer;
}



- (id)initWithCustomer:(NSDictionary *)customer;

@end