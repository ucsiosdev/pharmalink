//
//  SalesWorxCustomClass.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCustomClass.h"
#import "NSString+Additions.h"
#import "SWDefaults.h"

@implementation SalesWorxCustomClass

@end

@implementation PriceList

@synthesize Price_List_ID,Inventory_Item_ID,Organization_ID,Item_UOM,Unit_Selling_Price,Unit_List_Price,Is_Generic;

@end

@implementation Stock

@synthesize Org_ID,Lot_No,Lot_Qty,Expiry_Date,Stock_ID,lotNumberWithExpiryDate;

@end

@implementation SalesOrderInitialization



@end

@implementation ProductCategories
@end

@implementation DistributionCheck
@synthesize Item_Code, description, Avl, Qty, LotNo, ExpDate, ItemStock;
@end

@implementation SalesOrderTemplateLineItems



@end

@implementation SalesWorxParentOrder



@end

@implementation SalesOrderTemplete



@end
@implementation ManageOrderDetails
@synthesize manageOrderStatus,Orig_Sys_Document_Ref,Row_ID,Visit_ID,FOCOrderNumber,FOCParentOrderOrderNumber,OrderAmount,Creation_Date,Category,customerDetails;


@end

@implementation SalesWorxReturn
@synthesize productCategoriesArray,selectedCategory,returnLineItemsArray,confrimationDetails,ReturnStartTime,AssortmentPlansArray;
@end

@implementation SalesWorxReturnsMatchedInVoice

@end

@implementation ReturnsLineItem
@synthesize returnProduct,bonusProduct,defaultBonusQty,recievedBonusQty,returnProductQty,bonusDetailsArray,lotNumber,expiryDate,reasonForReturn,returnItemNetamount,reasonForReturnDescription,returnItemTotalamount, selectedInvoice;

- (id)init
{
    if(self = [super init]){
        self.returnItemVATChargeamount=[NSDecimalNumber decimalNumberWithString:@"0"];
        self.returnItemNetamount=[NSDecimalNumber decimalNumberWithString:@"0"];
        self.returnItemTotalamount=[NSDecimalNumber decimalNumberWithString:@"0"];
    }
    return self;
}
- (id)copy
{
    ReturnsLineItem *returnsLineItem = [[ReturnsLineItem alloc] init];
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    returnsLineItem.returnProduct = self.returnProduct;
    returnsLineItem.bonusProduct = self.bonusProduct;
    returnsLineItem.defaultBonusQty = self.defaultBonusQty;
    returnsLineItem.recievedBonusQty = self.recievedBonusQty;
    returnsLineItem.returnProductQty = self.returnProductQty;
    returnsLineItem.bonusDetailsArray = self.bonusDetailsArray;
    returnsLineItem.lotNumber = self.lotNumber;
    returnsLineItem.expiryDate = self.expiryDate;
    returnsLineItem.reasonForReturn = self.reasonForReturn;
    returnsLineItem.returnItemNetamount = self.returnItemNetamount;
    returnsLineItem.reasonForReturnDescription = self.reasonForReturnDescription;
    returnsLineItem.returnItemVATChargeamount = self.returnItemVATChargeamount;
    returnsLineItem.returnItemTotalamount = self.returnItemTotalamount;
    returnsLineItem.VATChargeObj = self.VATChargeObj;
    returnsLineItem.selectedInvoice = self.selectedInvoice;
    
    return returnsLineItem;
}

@end

@implementation ReturnsConfirmationDetails
@synthesize DocReferenceNumber,shipDate, signeeName,comments,returnType,isGoodsCollected,InvoicesNumbers,isCustomerSigned;
@end
@implementation SalesWorxReOrder
//@synthesize productCategoriesArray,salesOrderTempleteArray,selectedTemplete,selectedCategory,selectedOrderType,focParentOrderNumber,SalesOrderLineItemsArray,confrimationDetails,isTemplateOrder,isManagedOrder,OrderStartTime,SelectedManageOrderDetails,reOrderDetails;
@end
@implementation SalesOrder
@synthesize productCategoriesArray,salesOrderTempleteArray,selectedTemplete,selectedCategory,selectedOrderType,focParentOrderNumber,SalesOrderLineItemsArray,confrimationDetails,isTemplateOrder,isManagedOrder,OrderStartTime,SelectedManageOrderDetails,reOrderDetails,AssortmentBonusProductsArray,AssortmentPlansArray;
@end

@implementation SalesWorxVisit

@synthesize visitOptions,Visit_ID,Visit_Type,isPlannedVisit,isTODOCompleted;

@end

@implementation VisitOptions



@end


@implementation ToDo



@end
@implementation BonusItem

@end

@implementation ProductMediaFile



@end
@implementation ProductUOM



@end
@implementation Products

@synthesize Brand_Code,Category,Description,IsMSL,Item_Code,ItemID,OrgID,Sts,productPriceList,nearestExpiryDate,isSellingPriceFieldEditable,specialDiscount,RESTRICTED,primaryUOM,ProductUOMArray,ProductBarCode,Discount,OnOrderQty,maxExpiryDate,TradeName,GenericName,FormDesc;
- (id)copy
{
    Products *Product = [[Products alloc] init];
    
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    Product.Agency = self.Agency;
    Product.Inventory_Item_ID = self.Inventory_Item_ID;
    Product.Brand_Code = self.Brand_Code;
    Product.Category = self.Category;
    Product.Description = self.Description;
    Product.IsMSL = self.IsMSL;

    Product.ItemID = self.ItemID;
    Product.Lot_Qty = self.Lot_Qty;
    Product.Item_Code = self.Item_Code;
    Product.OrgID = self.OrgID;
    Product.Sts = self.Sts;
    Product.Discount = self.Discount;
    Product.selectedUOM = self.selectedUOM;
    Product.primaryUOM = self.primaryUOM;
    Product.conversionValue = self.conversionValue;
    Product.productStockArray = self.productStockArray;
    Product.productBonusArray = self.productBonusArray;
    Product.productPriceListArray = self.productPriceListArray;

    Product.stock = self.stock;
    Product.bonus = self.bonus;

    Product.productImagesArray = self.productImagesArray;
    Product.productMediaFile = self.productMediaFile;
    Product.productStock = self.productStock;
    Product.productPriceList = self.productPriceList;
    Product.nearestExpiryDate = self.nearestExpiryDate;
    Product.isSellingPriceFieldEditable = self.isSellingPriceFieldEditable;
    Product.specialDiscount = self.specialDiscount;

    Product.RESTRICTED = self.RESTRICTED;
    Product.Product_ID = self.Product_ID;
    Product.Product_Type = self.Product_Type;
    Product.Product_Name = self.Product_Name;
    Product.Promo_Item = self.Promo_Item;
    Product.ProductUOMArray = self.ProductUOMArray;
    Product.ProductBarCode=self.ProductBarCode;
    Product.OnOrderQty=self.OnOrderQty;
    Product.maxExpiryDate=self.maxExpiryDate;
    
    Product.GenericName=self.GenericName;
    Product.FormDesc=self.FormDesc;
    Product.TradeName=self.TradeName;
    
    Product.REMOVE_IN_FOC_LIST=self.REMOVE_IN_FOC_LIST;
    Product.RESTRICTED_FOR_RETURN=self.RESTRICTED_FOR_RETURN;

    
    
    return Product;
}
@end

@implementation CustomerCategory

@synthesize Category,Description,Item_No,Org_ID;

@end

@implementation SelectedBrand



@end

@implementation CustomerOrderHistory

@synthesize Customer_Name,Creation_Date,ERP_Status,FSR,Orig_Sys_Document_Ref,Transaction_Amt,ERP_Ref_Number;

@end

@implementation SWCustomerPriceList



@end

@implementation CustomerOrderHistoryLineItems


@end

@implementation CustomerOrderHistoryInvoiceLineItems



@end

@implementation CustomerDues



@end

@implementation Customer





@synthesize Address,Allow_FOC,Avail_Bal,Customer_Name,Bill_Credit_Period,Cash_Cust,Chain_Customer_Code,City,Contact,Creation_Date,Credit_Hold,Credit_Limit,Cust_Lat,Cust_Long,Cust_Status,Customer_Barcode,Customer_Class,Customer_ID,Customer_No,Customer_OD_Status,Customer_Segment_ID,Customer_Type,Dept,Location,Phone,Postal_Code,Price_List_ID,SalesRep_ID,Sales_District_ID,Ship_Customer_ID,Ship_Site_Use_ID,Site_Use_ID,Trade_Classification,categoriesArray,targetValue,salesValue,balanceToGoValue,isBonusAvailable, FSR_Plan_Detail_ID, Visit_Date, Visit_End_Time, Visit_Status, Visit_Start_Time, Visit_Note,isDelegatedCustomer,Area,Trade_License_Expiry;

@end





@implementation SalesOrderLineItemVATCharge
@end

@implementation SalesOrderLineItem
@synthesize OrderProduct,manualFocProduct,manualFOCQty,bonusProduct,defaultBonusQty,requestedBonusQty,productDiscount,productSelleingPrice,OrderProductQty,OrderItemTotalamount,OrderItemDiscountamount,OrderItemNetamount,appliedDiscount,bonusDetailsArray,NotesStr,AssignedLotsArray,AppliedAssortMentPlanID,showApliedAssortMentPlanBonusProducts,isOHDuplicateItemEntryAlertConfirmedByUser,isAssortMentBonusSavedByUser,isDuplicateItemEntryChcekAlertConfirmedByUser;
- (id)copy
{
    SalesOrderLineItem *salesOrderLineItem = [[SalesOrderLineItem alloc] init];
    
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    salesOrderLineItem.Guid = self.Guid;
    salesOrderLineItem.OrderProduct = self.OrderProduct;
    salesOrderLineItem.manualFocProduct = self.manualFocProduct;
    salesOrderLineItem.manualFOCQty = self.manualFOCQty;
    salesOrderLineItem.bonusProduct = self.bonusProduct;
    salesOrderLineItem.defaultBonusQty = self.defaultBonusQty;
    salesOrderLineItem.requestedBonusQty = self.requestedBonusQty;
    salesOrderLineItem.productDiscount = self.productDiscount;
    salesOrderLineItem.productSelleingPrice = self.productSelleingPrice;
    salesOrderLineItem.OrderProductQty = self.OrderProductQty;
    salesOrderLineItem.OrderItemTotalamount = self.OrderItemTotalamount;
    salesOrderLineItem.OrderItemDiscountamount = self.OrderItemDiscountamount;
    salesOrderLineItem.OrderItemNetamount = self.OrderItemNetamount;
    salesOrderLineItem.OrderItemVATChargeamount = self.OrderItemVATChargeamount;
    salesOrderLineItem.appliedDiscount = self.appliedDiscount;
    salesOrderLineItem.bonusDetailsArray = self.bonusDetailsArray;
    salesOrderLineItem.NotesStr = self.NotesStr;
    salesOrderLineItem.AssignedLotsArray = self.AssignedLotsArray;
    salesOrderLineItem.AppliedAssortMentPlanID = self.AppliedAssortMentPlanID;
    salesOrderLineItem.showApliedAssortMentPlanBonusProducts=self.showApliedAssortMentPlanBonusProducts;
    salesOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser=self.isDuplicateItemEntryChcekAlertConfirmedByUser;
    
    
    salesOrderLineItem.isAssortMentBonusSavedByUser=self.isAssortMentBonusSavedByUser;
    salesOrderLineItem.isOHDuplicateItemEntryAlertConfirmedByUser=self.isOHDuplicateItemEntryAlertConfirmedByUser;
    salesOrderLineItem.isDuplicateItemEntryChcekAlertConfirmedByUser=self.isDuplicateItemEntryChcekAlertConfirmedByUser;
    salesOrderLineItem.proximityNotes = self.proximityNotes;
    
    return salesOrderLineItem;
}
- (id)init
{
    if(self = [super init])
    {
        self.AppliedAssortMentPlanID = NSNotFound;
        self.showApliedAssortMentPlanBonusProducts=NO;
        
        self.OrderItemVATChargeamount=[NSDecimalNumber decimalNumberWithString:@"0"];
        
        self.OrderItemNetamount=[NSDecimalNumber decimalNumberWithString:@"0"];
        self.OrderItemDiscountamount=[NSDecimalNumber decimalNumberWithString:@"0"];
        self.OrderItemTotalamount=[NSDecimalNumber decimalNumberWithString:@"0"];
        
        self.Guid=[NSString createGuid];
    }
    return self;
}
@end


@implementation ProductBonusItem
@synthesize Description,Get_Add_Per,Get_Item,Get_Qty,Price_Break_Type_Code,Prom_Qty_From,Prom_Qty_To,bonusProduct,Plan_Name,Get_UOM;
@end

@implementation SalesOrderAssignedLot
@synthesize lot,assignedQuantity;

- (id)copy;
{
    SalesOrderAssignedLot *assignLot = [[SalesOrderAssignedLot alloc] init];
    
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    assignLot.lot = self.lot;
    assignLot.assignedQuantity = self.assignedQuantity;
    
    return assignLot;
}
@end


@implementation SalesOrderConfirmationDetails
@synthesize DocReferenceNumber,shipDate,signeeName,comments,isWholeSaleOrder,SkipConsolidation,isCustomerSigned;
@end

@implementation RMALotType
@synthesize lotType,Description;
@end

@implementation SalesWorxReasonCode
@synthesize Reasoncode,Description,Purpose;
@end

@implementation DailyVisitSummaryReport
@synthesize Customer_ID, Customer_Name, Payment, PaymentValue, DC, Survey, Order, OrderValue, Return, ReturnValue;
@end
@implementation SyncImage
@synthesize imagePath,imageSyncType,isUploaded,imageMIMEType,uploadFailureCount;
- (id)copy
{
    SyncImage *image = [[SyncImage alloc] init];
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    image.imagePath = self.imagePath;
    image.imageSyncType = self.imageSyncType;
    image.isUploaded = self.isUploaded;
    image.imageMIMEType = self.imageMIMEType;
    image.uploadFailureCount = self.uploadFailureCount;
    
    return image;
}

@end

@implementation SalesWorxBrandAmbassadorLocation



@end

@implementation SalesWorxBrandAmbassadorTask



@end

@implementation salesWorxBrandAmbassadorNotes

@end

@implementation SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses



@end


@implementation SalesWorxBrandAmbassadorWalkinCustomerVisit



@end

@implementation SalesWorxBrandAmbassadorVisit



@end

@implementation SalesWorxGoogleAnalytics



@end

@implementation SalesWorxSalesTrend

@end

@implementation SalesWorxProductStockSync



@end


@implementation Customer_SalesSummary

@synthesize Orig_Sys_Document_Ref, Creation_Date, Doc_Type, Transaction_Amt, Customer_No, Customer_Name, Cash_Cust, Phone, Visit_ID, End_Time, ERP_Ref_No, Ship_To_Customer_Id, Ship_To_Site_Id,Creation_DateWithTime;

@end


@implementation Customer_Statement

@synthesize Invoice_Ref_No, NetAmount, PaidAmt, InvDate, Customer_ID,InvDateWithTime;

@end


@implementation Blocked_Statement

@synthesize Contact, Customer_ID, Customer_Name, Phone;

@end


@implementation Review_Documents

@synthesize Amount, Customer_Name, Doc_Date, Doc_No, Doc_Type, ERP_Status, Start_Time,Doc_DateWithTime;

@end


@implementation Payment_Summary

@synthesize Amount, Collected_Amount, Collected_On, Collection_Ref_No, Collection_Type, Customer_ID, Customer_Name, DocDate, Doctype, Start_Time,Collected_OnWithTime;

@end

@implementation TargetVsAchievement

@synthesize product_Family,target,sales_Total,ach,rate,ideal,actual;

@end

@implementation SalesWorxFieldSalesAccompaniedBy



@end
@implementation DistriButionCheckItem
@end

@implementation DistriButionCheckMinStock
@end

@implementation DistriButionCheckItemLot
@end

@implementation DistriButionCheckLocation
@end

@implementation SalesWorxBeacon
//- (void)encodeWithCoder:(NSCoder *)aCoder{
//    [aCoder encodeObject:self.beaconUUID forKey:@"beaconUUID"];
//    [aCoder encodeObject:self.majorID forKey:@"majorID"];
//    [aCoder encodeObject:self.minorID forKey:@"minorID"];
//    [aCoder encodeObject:self.visitStartTime forKey:@"visitStartTime"];
//    [aCoder encodeObject:self.beaconDetectionTime forKey:@"beaconDetectionTime"];
//    [aCoder encodeBool:self.isVisitStarted forKey:@"isVisitStarted"];
//    [aCoder encodeObject:self.proximity forKey:@"proximity"];
//    
//    
//    
//}
//
//-(id)initWithCoder:(NSCoder *)aDecoder{
//    if(self = [super init]){
//        self.beaconUUID = [aDecoder decodeObjectForKey:@"beaconUUID"];
//        self.majorID = [aDecoder decodeObjectForKey:@"majorID"];
//        self.minorID = [aDecoder decodeObjectForKey:@"minorID"];
//        self.visitStartTime = [aDecoder decodeObjectForKey:@"visitStartTime"];
//        self.beaconDetectionTime = [aDecoder decodeObjectForKey:@"beaconDetectionTime"];
//        self.isVisitStarted = [aDecoder decodeBoolForKey:@"isVisitStarted"];
//        self.beaconDetectionTime = [aDecoder decodeObjectForKey:@"proximity"];
//        
//        
//        
//    }
//    return self;
//}

@end
@implementation SalesworxAssortmentBonusProduct
@synthesize PlanId,product,assignedQty,PlanDefaultBnsQty;
- (id)copy
{
    SalesworxAssortmentBonusProduct *asmPro = [[SalesworxAssortmentBonusProduct alloc] init];
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    asmPro.PlanId = self.PlanId;
    asmPro.product = self.product;
    asmPro.assignedQty = self.assignedQty;
    asmPro.PlanDefaultBnsQty=self.PlanDefaultBnsQty;
    return asmPro;
}

@end
@implementation SalesworxAssortmentBonusPlan
-(double)CalculateBonusForQty:(double)quantity
{
    double bonus=0;
    
    for(NSInteger i=0 ; i<[self.SlabsArray count] ; i++ )
    {
        SalesworxAssortmentBonusSlab *bonusItem = [self.SlabsArray objectAtIndex:i];
        double rangeStart = [bonusItem.Prom_Qty_From doubleValue];
        double rangeEnd = [bonusItem.Prom_Qty_To doubleValue];
        if (quantity >= rangeStart && quantity <=rangeEnd)
        {
            if([bonusItem.Price_Break_Type_Code isEqualToString:@"RECURRING"])
            {
                double dividedValue = quantity / rangeStart ;
                bonus = [bonusItem.Get_Qty integerValue] * floor(dividedValue) ;
            }
            
            else if([bonusItem.Price_Break_Type_Code  isEqualToString:@"PERCENT"])
            {
                double dividedValue = [bonusItem.Get_Qty integerValue]* (quantity / rangeStart) ;
                bonus = floor(dividedValue) ;
                
            }
            else if([bonusItem.Price_Break_Type_Code  isEqualToString:@"POINT"])
            {
                bonus = [bonusItem.Get_Qty integerValue];
            }
            break;
        }
    }
    return bonus;
}
@end
@implementation SalesworxAssortmentBonusSlab
@end
@implementation ProductObjectsArrayCustomClass
@end

@implementation CustomerCollectionTarget

@end

@implementation PaymentImage



@end

@implementation SalesWorxMerchandising



@end


@implementation MerchandisingPlanogramObjectDetection



@end



@implementation SalesWorxMerchandisingBrand



@end

@implementation MerchandisingAvailability



@end
@implementation MerchandisingBrandShare



@end
@implementation MerchandisingPlanogram



@end

@implementation MerchandisingCompetitorPictures

@end

@implementation MerchandisingCompetitionInfo



@end
@implementation MerchandisingInStoreActivity
- (id)init
{
    if(self = [super init]){
        self.StoreConditionNotes = @"";
        self.StoreCondition = @"";
        self.managerInteractionNotes= @"";
        self.ManagerInteractionStatus= NO;
        self.NewSKUListingstatus = NO;
        self.NewSKUListingNotes = @"";
        self.NewPromoActivationEndDate = @"";
        self.NewPromoActivationstartDate = @"";
        self.NewPromoActivationStatus = NO;
        self.NewPromoActivationNotes = @"";
    }
    return self;
}


@end
@implementation MerchandisingPOS



@end

@implementation MerchandisingPOSMaterial



@end
@implementation MerchandisingPromotionMaterial



@end
@implementation MerchandisingPositionDetails



@end

@implementation MerchandisingSurveyPicturesMark
@end

@implementation MerchandisingSurveyPictures



@end
@implementation MerchandisingComments



@end

@implementation MerchandisingBrandShareOfSelf



@end

@implementation FieldSalesContact



@end

@implementation CoachSurvey


@end

@implementation CoachQuestion

@end

@implementation CoachSection

@end

@implementation CoachSurveyConfirmationQuestion

@end


@implementation CoachSurveyType

@end

@implementation PaymentSummary
@end

@implementation RouteRescheduleDetails
@end
