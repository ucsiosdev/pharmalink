//
//  SalesWorxPopOverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxPopOverViewController.h"
#import "SWDefaults.h"
#import "MBProgressHUD.h"
#import "MedRepDefaults.h"
@interface SalesWorxPopOverViewController ()

@end

@implementation SalesWorxPopOverViewController
@synthesize enableArabicTranslation;
@synthesize popOverContentArray,titleKey,popOverSearchBar,popOverTableView,popOverController,disableSearch,popoverTableViewTopConstraint,popoverType,enableSwipeToDelete,headerTitle,popOverSize;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (enableSwipeToDelete==YES) {
        popOverTableView.allowsMultipleSelectionDuringEditing=NO;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardVisibleChanged:) name:UIKeyboardWillHideNotification object:nil];


    // Do any additional setup after loading the view from its nib.
    filteredPopOverContentArray=[[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)keyboardVisibleChanged:(NSNotification *)notification {
    self.popOverController.popoverContentSize = self.popOverSize;
}
-(void)viewWillDisappear:(BOOL)animated{
 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:titleKey];

   // self.navigationController.navigationBar.topItem.title = @"";
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }
    
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    if (self.isEdetailingAccompaniedBy) {
        self.navigationItem.leftBarButtonItem=closeButton;
        
        UIBarButtonItem *accompaniedByDoneButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done",nil) style:UIBarButtonItemStylePlain target:self action:@selector(accompaniedByDoneButtonTapped)];
        
        [accompaniedByDoneButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem=accompaniedByDoneButton;
        popOverTableView.allowsMultipleSelection = YES;
        
        filteredPopOverContentArray = [[NSMutableArray alloc] initWithCapacity:popOverContentArray.count];
        for (id element in popOverContentArray) {
            [filteredPopOverContentArray addObject:[element mutableCopy]];
        }
        
    } else {
        if (![titleKey isEqualToString:@"Reason"]) {
            self.navigationItem.rightBarButtonItem=closeButton;
        }
        filteredPopOverContentArray = [popOverContentArray mutableCopy];
    }
    
    
    
    if (disableSearch==YES) {
        
        
        popOverSearchBar.hidden=YES;
       // popoverTableViewTopConstraint.constant=-44;
        
        [self.view layoutIfNeeded];
    }
    
    else
    {
        NSLog(@"testing search");
        
        popOverSearchBar.hidden=NO;
        self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        self.searchController.searchResultsUpdater = self;
        self.searchController.delegate=self;
        self.searchController.dimsBackgroundDuringPresentation = NO;
        self.popOverTableView.tableHeaderView = self.searchController.searchBar;
        
        [popOverTableView reloadData];
        self.definesPresentationContext = NO;
        self.searchController.hidesNavigationBarDuringPresentation = NO;

    }

    popOverTableView.estimatedRowHeight = 44.0;
    popOverTableView.rowHeight = UITableViewAutomaticDimension;
}

-(void)closeTapped
{
    self.searchController.active = NO;
    if (self.isEdetailingAccompaniedBy) {
        filteredPopOverContentArray = [popOverContentArray mutableCopy];
    }
    
    if(popOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [popOverController dismissPopoverAnimated:YES];
}
-(void)accompaniedByDoneButtonTapped
{
    if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverControllerForEdetailingAccompaniedByMultiSelection:)]) {
        
        [self.salesWorxPopOverControllerDelegate didSelectPopOverControllerForEdetailingAccompaniedByMultiSelection:filteredPopOverContentArray];
    }

    if(popOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [popOverController dismissPopoverAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView DataSource Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filteredPopOverContentArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    if (self.isEdetailingAccompaniedBy) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(14);
    
    cell.textLabel.textColor=MedRepElementDescriptionLabelColor;
    
    cell.textLabel.numberOfLines=2;
    
    
    if ([popoverType isEqualToString:@"Activation"]) {
        
        SyncLoctions * synLocation=[filteredPopOverContentArray objectAtIndex:indexPath.row];
            cell.textLabel.text=[NSString stringWithFormat:@"%@",synLocation.name];
    }
    else if ([popoverType isEqualToString:kOrderTempletePopOverTitle]) {
        SalesOrderTemplete * OrderTemplete=[filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",OrderTemplete.Template_Name];
    }
    else  if ([popoverType isEqualToString:KSalesOrderManualFocPopOverTitle])
    {
        Products *product=[filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",product.Description];
        
    }
    else  if ([popoverType isEqualToString:KVisitOptionsProductNameSearchPopOverTitle])
    {
        Products *product=[filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",product.Description];
        
    }
    else  if ([popoverType isEqualToString:kCoachSurveyPopOverTitle])
    {
        CoachSurveyType * currentType=[[CoachSurveyType alloc]init];
        currentType=[filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",currentType.Survey_Title];
        
    }
    else  if (self.isEdetailingAccompaniedBy)
    {
        NSMutableDictionary *selectedDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        
        if ([[selectedDict valueForKey:@"isSelected"] isEqualToString:KAppControlsYESCode]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[[filteredPopOverContentArray objectAtIndex:indexPath.row]valueForKey:@"Username"]];
    }
    
    
    else
        
    {
        
        if(enableArabicTranslation)
            cell.textLabel.text=[NSString stringWithFormat:@"%@",NSLocalizedString([filteredPopOverContentArray objectAtIndex:indexPath.row],nil)];
        else
            cell.textLabel.text=[NSString stringWithFormat:@"%@",[filteredPopOverContentArray objectAtIndex:indexPath.row]];

    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didDeleteItematIndex:)] && enableSwipeToDelete==YES) {
            
            NSIndexPath *selectedIndexPath;
            selectedIndexPath = [NSIndexPath indexPathForRow:[popOverContentArray indexOfObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]] inSection:0];
            [filteredPopOverContentArray removeObjectAtIndex:indexPath.row];
            [popOverContentArray removeObjectAtIndex:selectedIndexPath.row];
            [self.salesWorxPopOverControllerDelegate didDeleteItematIndex:selectedIndexPath];
            [popOverTableView reloadData];
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (enableSwipeToDelete==YES) {
        
        return UITableViewCellEditingStyleDelete;
    }
    else
    {
        return UITableViewCellEditingStyleNone;

    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *matchedindexPath = [NSIndexPath indexPathForRow:[popOverContentArray indexOfObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]] inSection:0];

    NSLog(@"selected indexpath is %ld \n and matched index is %ld", (long)indexPath.row,matchedindexPath.row);
    
    
    if(popOverController==nil){

        if ([titleKey isEqualToString:@"Reason"]) {
            [self.navigationController popViewControllerAnimated:true];
            if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverController:)]) {
                [self.salesWorxPopOverControllerDelegate didSelectPopOverController:matchedindexPath];
            }
        } else {
            tableView.userInteractionEnabled=NO;
            self.searchController.active = NO;
            
            [[self presentingViewController] dismissViewControllerAnimated:YES completion:^{
                if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverController:)]) {
                    [self.salesWorxPopOverControllerDelegate didSelectPopOverController:matchedindexPath];
                }
            }];
        }
    }
    else{
        if (self.isEdetailingAccompaniedBy) {
            
            NSMutableDictionary *selectedDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
            
            if ([[selectedDict valueForKey:@"Username"] isEqualToString:@"No Coach"]) {
                
                // below code is used to remove all checkmarks except No Coach
                for (NSMutableDictionary *dict in popOverContentArray) {
                    [dict setValue:KAppControlsNOCode forKey:@"isSelected"];
                }
                filteredPopOverContentArray = [popOverContentArray mutableCopy];
                NSMutableDictionary *selecDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
                [selecDict setValue:KAppControlsYESCode forKey:@"isSelected"];
                
                if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverControllerForEdetailingAccompaniedByMultiSelection:)]) {
                    [self.salesWorxPopOverControllerDelegate didSelectPopOverControllerForEdetailingAccompaniedByMultiSelection:filteredPopOverContentArray];
                }
                tableView.userInteractionEnabled=NO;
                self.searchController.active=NO;
                [popOverController dismissPopoverAnimated:YES];
                [tableView reloadData];
            } else {
            
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Username = 'No Coach'"];
                NSArray *filteredArray = [filteredPopOverContentArray filteredArrayUsingPredicate:predicate];
                
                if (filteredArray.count > 0) {
                    [[filteredArray objectAtIndex:0] setValue:KAppControlsNOCode forKey:@"isSelected"];
                }
                
                if ([[selectedDict valueForKey:@"isSelected"] isEqualToString:KAppControlsYESCode]) {
                    
                    [selectedDict setValue:KAppControlsNOCode forKey:@"isSelected"];
                } else if ([[selectedDict valueForKey:@"isSelected"] isEqualToString:KAppControlsNOCode]) {
                    [selectedDict setValue:KAppControlsYESCode forKey:@"isSelected"];
                }
                [tableView reloadData];
            }
        } else {
            if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverController:)]) {
                
                [self.salesWorxPopOverControllerDelegate didSelectPopOverController:matchedindexPath];
            }
            tableView.userInteractionEnabled=NO;
            self.searchController.active=NO;
            
            [popOverController dismissPopoverAnimated:YES];
        }
    }
}

-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
    // [textField release] // uncomment if not using ARC
}


#pragma mark Search DisplayController methods


//- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
//    
//    
//    
//    
//    tableView.frame = CGRectMake(0, -20, self.popOverTableView.frame.size.width, self.popOverTableView.frame.size.height);
//    
//}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
    if(searchText.length==0)
    {
        filteredPopOverContentArray = [popOverContentArray mutableCopy];

    }
    else if ([popoverType isEqualToString:@"Activation"]) {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.name contains[cd] %@",
                                        searchText];
        filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    else  if ([popoverType isEqualToString:kOrderTempletePopOverTitle])
   {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Template_Name contains[cd] %@",
                                        searchText];
        filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    else  if ([popoverType isEqualToString:KSalesOrderManualFocPopOverTitle])
    {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Description contains[cd] %@",
                                        searchText];
        filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    else  if ([popoverType isEqualToString:KVisitOptionsProductNameSearchPopOverTitle])
    {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Description contains[cd] %@",
                                        searchText];
        filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    else
    {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF contains[cd] %@",
                                        searchText];
        
        filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    [popOverTableView reloadData];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentForSearchText:searchController.searchBar.text
                               scope:@""];

}
#pragma mark - UISearchDisplayController delegate methods
//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
//shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [self filterContentForSearchText:searchString
//                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[self.searchDisplayController.searchBar
//                                                     selectedScopeButtonIndex]]];
//    
//    return YES;
//}
//
- (void)didDismissSearchController:(UISearchController *)searchController
{
    self.navigationController.navigationBar.translucent = NO;

    filteredPopOverContentArray=[popOverContentArray mutableCopy];
    [popOverTableView reloadData];
}
- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
    self.navigationController.navigationBar.translucent = YES;
}



@end
