//
//  SalesWorxMerchandisingViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 3/23/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxMerchandisingViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SWSplitViewController.h"
#import "MedRepUpdatedDesignCell.h"
#import "SWDatabaseManager.h"
#import "NSString+Additions.h"
#import "SWAppDelegate.h"


@interface SalesWorxMerchandisingViewController ()

@end

@implementation SalesWorxMerchandisingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appControl = [AppControl retrieveSingleton];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Merchandising", nil)];
    
    
    startVisitButton = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartVisit"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitTapped)];
    [startVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = startVisitButton;
    
    
    selectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    filteredCustomers=[[NSMutableArray alloc]init];
    indexPathArray=[[NSMutableArray alloc]init];

    
    if([appControl.ENABLE_FSR_VISIT_NOTES isEqualToString:KAppControlsYESCode] || [appControl.SHOW_FS_VISIT_CUSTOMER_CONTACT isEqualToString:KAppControlsYESCode]){
        [lastvisitDetailsButton setHidden:NO];
    }else{
        [lastvisitDetailsButton setHidden:YES];
    }
    validateBeacon=YES;
}

-(void)closeSplitView
{
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        if (revealController.currentFrontViewPosition !=0)
        {
            [revealController revealToggle:self];
        }
        revealController=nil;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kMerchandisingScreenName];
        beaconValidationStatus = [self validateFSRLocationwithiBeacon];

    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            NSMutableArray *customerListArray = [[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
            
            customersArray=[[NSMutableArray alloc]init];
            if (customerListArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary *currentDict in customerListArray) {
                    
                    NSMutableDictionary *currentCustDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Customer *currentCustomer = [[Customer alloc] init];
                    currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
                    currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
                    currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
                    currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
                    currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
                    currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
                    currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
                    currentCustomer.City=[currentCustDict valueForKey:@"City"];
                    currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
                    currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
                    currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
                    currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
                    currentCustomer.Cust_Lat=[currentCustDict valueForKey:@"Cust_Lat"];
                    currentCustomer.Cust_Long=[currentCustDict valueForKey:@"Cust_Long"];
                    currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
                    currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
                    currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
                    currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
                    currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
                    currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
                    currentCustomer.Customer_Segment_ID=[currentCustDict valueForKey:@"Customer_Segment_ID"];
                    currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
                    currentCustomer.Dept=[currentCustDict valueForKey:@"Dept"];
                    currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
                    currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
                    currentCustomer.Postal_Code=[currentCustDict valueForKey:@"Postal_Code"];
                    currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
                    currentCustomer.SalesRep_ID=[currentCustDict valueForKey:@"SalesRep_ID"];
                    currentCustomer.Sales_District_ID=[currentCustDict valueForKey:@"Sales_District_ID"];
                    currentCustomer.Ship_Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]];
                    currentCustomer.Ship_Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]];
                    currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
                    currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
                    currentCustomer.isDelegatedCustomer=[currentCustDict valueForKey:@"isDelegatedCustomer"];
                    currentCustomer.Area=[currentCustDict valueForKey:@"Area"];
                    currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:@"CUST_LOC_RNG"];

                    [customersArray addObject:currentCustomer];
                }
            }
            unfilteredCustomersArray = customersArray;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                if (customersArray.count>0) {
                    
                    [customersTableView reloadData];
                    [self deselectPreviousCellandSelectFirstCell];
                    self.navigationItem.rightBarButtonItem.enabled=YES;
                }
                else
                {
                    self.navigationItem.rightBarButtonItem.enabled=NO;
                }
            });
        });
        
        
        [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        customerHeaderView.layer.shadowColor = [UIColor blackColor].CGColor;
        customerHeaderView.layer.shadowOffset = CGSizeMake(2, 2);
        customerHeaderView.layer.shadowOpacity = 0.1;
        customerHeaderView.layer.shadowRadius = 1.0;
        
        
        for (UIView *borderView in self.view.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                
                if (borderView.tag==1000 || borderView.tag==1001|| borderView.tag==1002|| borderView.tag==1003|| borderView.tag==1004 || borderView.tag==104 || borderView.tag==1005 || borderView.tag==1008||borderView.tag==1010 ) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                }
            }
        }
        
        
        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
        [self.navigationController.navigationBar setTranslucent:NO];
        
        if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
            
            if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
                UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
                
                [self.navigationItem setLeftBarButtonItem:leftBtn];
                
            } else {
            }
        }
        self.view.backgroundColor=UIViewControllerBackGroundColor;
        customerLocationMapView.showsUserLocation=YES;
        
        
    }
    
    
    
    [self hideNoSelectionView];
    [self updateCustomerLastVisitDetails];
}



#pragma mark Start Visit Methods

-(void)startCustomerVisit
{
    
    VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
    salesWorxVisit=[[SalesWorxVisit alloc]init];
    salesWorxVisit.Visit_Type=kMerchandisingVisitType;
    currentVisitOptions.customer=selectedCustomer;
    salesWorxVisit.visitOptions=currentVisitOptions;

    if (!customerLocationValidationDone) {
        [self validateLocationRangeforOnSiteVisit];
        return;
    }
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    
    
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.isCashCustomer = @"N";
        // [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"D"];
        
        
        [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
    }
}
-(void)startVisitTapped
{
    if ([customersSearchBar isFirstResponder]) {
        [customersSearchBar resignFirstResponder];
    }
    //show an alert if there is any open visit to close that visit.
    NSMutableArray * openVisits=[[SWDatabaseManager retrieveManager]fetchOpenVisitsforOtherCustomers:@"" ]
    ;
    NSLog(@"open visits count is %@", openVisits);
    if (openVisits.count>0) {
        NSString* customerName=[openVisits valueForKey:@"Customer_Name"];
        openVisitID=[openVisits valueForKey:@"Actual_Visit_ID"];
        openPlannedDetailID=[openVisits valueForKey:@"FSR_Plan_Detail_ID"];
        
        if (!openVisitAlert) {
            NSString * firstText=NSLocalizedString(@"There is an open visit for the customer", nil);
            NSString* secondText=NSLocalizedString(@"would you like to close this visit before starting a new visit", nil);
            NSString* finalString=[NSString stringWithFormat:@"%@\n %@ \n %@",firstText,customerName,secondText ];
            //[SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Open Visit", nil) andMessage:finalString withController:secondText];
            openVisitAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Open Visit", nil) message:finalString delegate:self cancelButtonTitle:NSLocalizedString(@"YES", nil) otherButtonTitles:NSLocalizedString(@"NO", nil), nil];
            openVisitAlert.delegate=self;
            openVisitAlert.tag=300;
            [openVisitAlert show];
        }
        
    }
    else
    {
        [self startCustomerVisit];
    }
}

- (BOOL)location:(CLLocation *)location isNearCoordinate:(CLLocationCoordinate2D)coordinate withRadius:(CLLocationDistance)radius
{
    CLCircularRegion *circularRegion = [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius identifier:@"radiusCheck"];
    
    return [circularRegion containsCoordinate:coordinate];
}

-(void)validateLocationRangeforOnSiteVisit
{
    NSString* customerLatitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Lat];
    NSString* customerLongitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Long];
    
    NSLog(@"customer lat in validate %@ long %@", customerLatitude,customerLongitude);
    
    NSLog(@"current customer is %@", salesWorxVisit.Visit_ID);
    
    
    //check if customer location is available
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString* fsrLatitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude] ;
    NSString* fsrLongitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude] ;
    
    if ([NSString isEmpty:customerLatitude]==NO && [NSString isEmpty:customerLongitude]==NO && [NSString isEmpty:fsrLatitude]==NO && [NSString isEmpty:fsrLongitude]==NO ) {
        
        NSLog(@"customer location and fsr location available to validate for onsite visit \ncustomer latitude %@: \n customer longitude :%@\n fsr latitude : %@\n fsr longitude : %@", customerLatitude,customerLongitude,fsrLatitude,fsrLongitude);
        
    }
    
    
    NSLog(@"CUST_LOC_RNG is %@", salesWorxVisit.visitOptions.customer.CUST_LOC_RNG);
    
    
    //fetch range from tbl_addl_info
    NSString * rangeStr=[[SWDatabaseManager retrieveManager]fetchCustomerLocationRange:salesWorxVisit.visitOptions.customer];
    double validationRange=100;
    
    if ([NSString isEmpty:rangeStr]==NO) {
        validationRange=[rangeStr doubleValue];
        
    }
    
    
    
    
    CLLocationCoordinate2D customerLocation= CLLocationCoordinate2DMake([customerLatitude doubleValue], [customerLongitude doubleValue]);
    
    
    NSLog(@"current location lat: %f  long: %f and customer location  lat %@ long %@", appDel.currentLocation.coordinate.latitude,appDel.currentLocation.coordinate.longitude,customerLatitude,customerLongitude );
    
    
    if (CLLocationCoordinate2DIsValid(customerLocation)) {
        NSLog(@" customer Coordinate valid");
        
        BOOL isInLocation=[self location:appDel.currentLocation isNearCoordinate:customerLocation withRadius:validationRange];
        if (isInLocation) {
            
            NSLog(@"user is in customer location");
            
            isCustomerLocationValid=YES;
            
            salesWorxVisit.isFSRinCustomerLocation=YES;
            
            [self startCustomerVisit];
        }
        else
        {
            
            //validate beacon
            
            
            NSString * validateBeaconStr =[[AppControl retrieveSingleton] VALIDATE_CUST_IBEACON];
            
            
            
            NSLog(@"customer iBeacon validation FLag %@", validateBeaconStr);
            
            if ([validateBeaconStr isEqualToString:KAppControlsYESCode] &&  [NSString isEmpty:[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon].Beacon_UUID]==NO )
            {
                //this is sample test for beacon
                beaconValidationStatus=[self validateFSRLocationwithiBeacon];
                
                if (beaconValidationStatus==NO) {
                    salesWorxVisit.isFSRinCustomerLocation=NO;
                    salesWorxVisit.beaconValidationSuccessfull=NO;
                    [self showOnsiteVisitPopOver];
                }
                else{
                    
                    NSLog(@"customer location failed but beacon location succeeded");
                    isCustomerLocationValid=YES;
                    
                    salesWorxVisit.beaconValidationSuccessfull=YES;
                    salesWorxVisit.isFSRinCustomerLocation=YES;
                    
                    validateBeacon=NO;
                    
                    salesWorxVisit.detectedBeacon=[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon];
                    
                    
                    //[self saveButtonTapped:saveButton];
                    [self startCustomerVisit];

                }
                
            }
            else
            {
                
                
                NSLog(@"user is not in customer location displaying reason VC");
                salesWorxVisit.isFSRinCustomerLocation=NO;
                salesWorxVisit.beaconAvailable=NO;

                [self showOnsiteVisitPopOver];
                
            }
            //            id temp;
            //            NSMutableArray * contentArray=temp;
            //            [[contentArray objectAtIndex:0] valueForKey:@"test"];
            //
            
            
        }
        
    } else {
        NSLog(@"FSR is not at customer location but visit is onsite, no validation done due to invalid coordinates of the customer %@", salesWorxVisit.visitOptions.customer.Customer_ID);
        isCustomerLocationValid=YES;
        salesWorxVisit.isFSRinCustomerLocation=YES;
        customerLocationValidationDone=YES;

    }
    
}
-(BOOL)validateFSRLocationwithiBeacon
{
    BOOL status=NO;
    salesWorxVisit.detectedBeacon=[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon];
    
    NSLog(@"detected beacon in visit options %@", salesWorxVisit.detectedBeacon.Beacon_Major);
    
    if (![NSString isEmpty:salesWorxVisit.detectedBeacon.Beacon_Major]) {
        return YES;
    }
    
    return status;
}

-(void)selectedReasonDelegate:(id)salesworxVisit
{
    //first one is current object and second is from delegate
    customerLocationValidationDone=YES;
    salesWorxVisit=salesworxVisit;
    NSString *onsiteExceptionReason = [SWDefaults getValidStringValue:salesWorxVisit.onSiteExceptionReason];
    
    if ([NSString isEmpty:onsiteExceptionReason] == NO) {
        [self startCustomerVisit];

        

    }
    

}
-(void)selectedReasonDidCancel:(id)salesWorxVisit
{
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    openVisitAlert=nil;
    
    if (alertView.tag==300 && buttonIndex==0) {
        //yes tapped
        //we are updating visit end date to tbl_fsr__actual visits similary if its a planned visit we should update status flag of TBL_FSR_Planned_Visits, get plannev visit id of actula visit if based on fsr_plan_detail id, fetch planned visit id of open visit
        
        
        NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Planned_Visit_ID from TBL_FSR_Planned_Visits where FSR_Plan_Detail_ID ='%@'",openPlannedDetailID]];
        
        if (plannedVisitIDArray.count>0) {
            
            openPlannedVisitID=[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Planned_Visit_ID"];
        }
        
        BOOL plannedVisitStatus;
        
        if ([NSString isEmpty:openPlannedVisitID]==NO) {
            plannedVisitStatus=[[SWDatabaseManager retrieveManager]closePlannedVisitwithVisitID:openPlannedVisitID];
        }
        
        BOOL visitEndDateStatus =NO;
        
        if ([NSString isEmpty:openVisitID]==NO) {
            visitEndDateStatus=  [[SWDatabaseManager retrieveManager]updateVisitEndDatewithAlert:openVisitID];
        }
        
        if (visitEndDateStatus==YES || plannedVisitStatus==YES) {
            
            [self startCustomerVisit];
        }
        
        
        NSLog(@"visit end time updated");
    }
    //    else if (alertView.tag==300 && buttonIndex==1)
    //    {
    //        //[self.navigationController popViewControllerAnimated:YES];
    //    }
}


-(void)showOnsiteVisitPopOver
{
    
    SalesWorxCustomerOnsiteVisitOptionsReasonViewController *reasonVC= [[SalesWorxCustomerOnsiteVisitOptionsReasonViewController alloc]init];
    reasonVC.selectedReason=self;
    reasonVC.currentVisit=salesWorxVisit;
    salesWorxVisit.beaconValidationSuccessfull=NO;
    reasonVC.selectedReason=self;
    reasonVC.isMerchandisingVisit=YES;
    VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
    salesWorxVisit=[[SalesWorxVisit alloc]init];
    salesWorxVisit.Visit_Type=kMerchandisingVisitType;
    currentVisitOptions.customer=selectedCustomer;
    salesWorxVisit.visitOptions=currentVisitOptions;

    salesWorxVisit.isPlannedVisit=NO;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:reasonVC];
    mapPopUpViewController.navigationBarHidden=YES;
    //mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    //cashCustOnsiteVC.view.backgroundColor = KPopUpsBackGroundColor;
    reasonVC.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}


#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES && filteredCustomers.count>0) {
        [self EnableCustomerOptions];
        return filteredCustomers.count;
    }
    else if(isSearching ==NO && customersArray.count>0)
    {
        [self EnableCustomerOptions];
        return customersArray.count;
    }
    else
    {
        [self DisableCustomerOptions];
        return 0;
    }
}
-(void)DisableCustomerOptions
{
    [startVisitButton setEnabled:NO];
}
-(void)EnableCustomerOptions
{
    [startVisitButton setEnabled:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Customer *currentCust;
    if (isSearching==YES) {
        currentCust=[filteredCustomers objectAtIndex:indexPath.row];
    }
    else
    {
        currentCust=[customersArray objectAtIndex:indexPath.row];
    }
    
    static NSString* identifier=@"updatedDesignCell";
    MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (isSearching && !NoSelectionView.hidden) {
        [cell setDeSelectedCellStatus];
    } else {
        if ([indexPathArray containsObject:indexPath]) {
            [cell setSelectedCellStatus];
        }
        else
        {
            [cell setDeSelectedCellStatus];
        }
    }
    
    cell.titleLbl.text=[[NSString stringWithFormat:@"%@",currentCust.Customer_Name] capitalizedString];
    cell.descLbl.text=[NSString stringWithFormat:@"%@",currentCust.Customer_No];
    
    
    if([currentCust.isDelegatedCustomer isEqualToString:KAppControlsYESCode]){
        [cell.cellSpecialIndicationImageView setHidden:NO];
        cell.cellSpecialIndicationImageView.image=[UIImage imageNamed:@"DelegatedCustomerFlag"];
    }else{
        [cell.cellSpecialIndicationImageView setHidden:YES];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndexPath = indexPath;

    if (tableView == customersTableView) {
        [self hideNoSelectionView];
        
        if (isSearching==YES && filteredCustomers.count>0) {
            selectedCustomer=[filteredCustomers objectAtIndex:indexPath.row];
        }
        else
        {
            selectedCustomer=[customersArray objectAtIndex:indexPath.row];
        }

        
        // Create Annotation point
        MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
        CLLocationCoordinate2D custCoordinates;
        custCoordinates.latitude=[selectedCustomer.Cust_Lat doubleValue];
        custCoordinates.longitude=[selectedCustomer.Cust_Long doubleValue];
        Pin.coordinate = custCoordinates;
        // add annotation to mapview
        [customerLocationMapView removeAnnotations:customerLocationMapView.annotations];
        [customerLocationMapView addAnnotation:Pin];
        [customerLocationMapView setCenterCoordinate:custCoordinates animated:YES];
        [customerLocationMapView setUserTrackingMode:MKUserTrackingModeNone];
        
        MKCoordinateRegion adjustedRegion = [customerLocationMapView regionThatFits:MKCoordinateRegionMakeWithDistance(custCoordinates, 5000, 5000)];
        [customerLocationMapView setRegion:adjustedRegion animated:NO];
        
        
        if (indexPathArray.count==0) {
            [indexPathArray addObject:indexPath];
        }
        else
        {
            indexPathArray=[[NSMutableArray alloc]init];
            [indexPathArray addObject:indexPath];
        }
        
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.titleLbl.textColor=[UIColor whiteColor];
        [cell.descLbl setTextColor:[UIColor whiteColor]];
        cell.cellDividerImg.hidden=YES;
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        [self populateSelectedCustomerDetails:selectedCustomer];
        
        [customersTableView reloadData];
    }
}

-(void)deselectPreviousCellandSelectFirstCell
{
    if ([customersTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        selectedCustomer = [customersArray objectAtIndex:0];
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[customersTableView cellForRowAtIndexPath:selectedIndexPath];
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [customersTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                        animated:YES
                                  scrollPosition:UITableViewScrollPositionNone];
        [customersTableView.delegate tableView:customersTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

-(void)populateSelectedCustomerDetails:(Customer*)selectedCust
{
    detailsCustomerNameLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Customer_Name];
    detailsCustomerCodeLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Customer_No];
    
    if (![selectedCust isEqual:[NSNull null]]) {
        
        if (![NSString isEmpty:selectedCust.Address]) {
            customeraddressLbl.text = [NSString stringWithFormat:@"%@",selectedCust.Address];
        }else{
            customeraddressLbl.text=KNotApplicable;
        }
        if (![NSString isEmpty:selectedCust.Phone]) {
            telephoneNumberLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Phone];
        }else{
            telephoneNumberLbl.text=KNotApplicable;
            
        }
        if (![NSString isEmpty:selectedCust.Area]) {/**City replace with Area in future*/
            customerCityLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Area];
        }else{
            customerCityLbl.text=KNotApplicable;
            
        }
    }else{
        customeraddressLbl.text=KNotApplicable;
        telephoneNumberLbl.text=KNotApplicable;
        customerCityLbl.text=KNotApplicable;
    }
    

    if ([selectedCust.Cust_Status isEqualToString:@"Y"]) {
        [customerStatusImageView setBackgroundColor:CustomerActiveColor];
    }
    else {
        [customerStatusImageView setBackgroundColor:[UIColor redColor]];
    }

    
    CLLocationCoordinate2D  currentCustCoord;
    currentCustCoord.latitude=[selectedCust.Cust_Lat doubleValue];
    currentCustCoord.latitude=[selectedCust.Cust_Long doubleValue];
    
    [self updateCustomerLastVisitDetails];
}

#pragma mark Search Bar Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self closeSplitView];
    [searchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
    }
    else
    {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        isSearching = YES;
        [self searchContent];
    }
    else {
        [self hideNoSelectionView];
        isSearching = NO;
        if (customersArray.count>0) {
            NSLog(@"reloading in search did change");
            
            [customersTableView reloadData];
            [customersTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                            animated:YES
                                      scrollPosition:UITableViewScrollPositionNone];
            NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            [customersTableView.delegate tableView:customersTableView didSelectRowAtIndexPath:firstIndexPath];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [customersSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    [self hideNoSelectionView];
    
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (customersArray.count>0) {
        [customersTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(void)searchContent
{
    NSString *searchString = customersSearchBar.text;
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Customer_Name", searchString];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:filter, @"Customer_No", searchString];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2]];
    filteredCustomers=[[customersArray filteredArrayUsingPredicate:predicate] mutableCopy];

    if (filteredCustomers.count>0) {

        [customersTableView reloadData];
    }
    else if (filteredCustomers.count==0)
    {
        isSearching=YES;
        [customersTableView reloadData];
    }
}

#pragma mark map Methods

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* custLatValue;
    NSString* custLongValue;
    if (selectedCustomer) {
        custLatValue =[NSString stringWithFormat:@"%@",selectedCustomer.Cust_Lat];
        custLongValue=[NSString stringWithFormat:@"%@",selectedCustomer.Cust_Long];
    }
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        NSString* googleMapsUrlString;
        
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication]openURL:googleMapsUrl];
        }
    }
    else
    {
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
        else
        {
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
    }
}

#pragma mark Filter Button methods

- (IBAction)customersFilterButtonTapped:(id)sender {
    [self closeSplitView];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    if (customersArray.count>0) {
        [customersSearchBar setShowsCancelButton:NO animated:YES];
        
        
        customersSearchBar.text=@"";
        [customersSearchBar resignFirstResponder];
        [self.view endEditing:YES];
        
        NSLog(@"reloading in customersFilterButtonTapped");
        
        [customersTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];

        
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        // Blurred with UIImage+ImageEffects
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        //blurredBgImage.image = [self blurWithImageEffects:[self takeSnapshotOfView:[self view]]];
        
        [self.view addSubview:blurredBgImage];
        
        UIButton* searchBtn=(id)sender;
        
        SWCustomersFilterViewController *popOverVC = [[SWCustomersFilterViewController alloc]init];
        popOverVC.delegate=self;
        popOverVC.customersArray=unfilteredCustomersArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 510) animated:YES];
        CGRect btnFrame=searchBtn.frame;
        popOverVC.previousFilterParametersDict=previousFilterParameters;
        
        CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];
        
        btnFrame.origin.x=btnFrame.origin.x+3;
        
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else{
        
        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Data", nil) message:NSLocalizedString(@"Please try later", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [unavailableAlert show];
    }
}

#pragma  mark Filter Delegate Methods

-(void)customerFilterDidClose
{
    [blurredBgImage removeFromSuperview];
}

-(void)filteredCustomers:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    customersArray=filteredArray;
    [customersTableView reloadData];
    [self deselectPreviousCellandSelectFirstCell];
}
-(void)filteredCustomerParameters:(NSMutableDictionary*)parametersDict
{
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    previousFilterParameters=parametersDict;
}

-(void)customerFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    customersArray=unfilteredCustomersArray;
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    
    [customersTableView reloadData];
    [self deselectPreviousCellandSelectFirstCell];
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    customerViewTopConstraint.constant = 8;
}
-(void)showNoSelectionView
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    customerViewTopConstraint.constant = 1000;
}
#pragma mark customer last visit
-(void)updateCustomerLastVisitDetails{
    __block NSString *lastVisitedDateStr=[[NSString alloc] init];
    lastVisitedDateLabel.text=KNotApplicable;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        if(selectedCustomer!=nil && selectedCustomer.Customer_ID!=nil){
            lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:selectedCustomer];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            lastVisitedDateLabel.text=lastVisitedDateStr.length==0?KNotApplicable:lastVisitedDateStr;
        });
    });    
}

- (IBAction)lastVisitDetailsButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    UIButton *tappedButton=sender;
    
    LastVisitDetailsViewController * popOverVC=[[LastVisitDetailsViewController alloc]init];
    popOverVC.selectedCustomer=selectedCustomer;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(380, 380);
    popover.delegate = self;
    popover.sourceView = tappedButton.superview;
    popover.sourceRect = tappedButton.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
