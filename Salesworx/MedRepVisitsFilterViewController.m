//
//  MedRepVisitsFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepVisitsFilterViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepQueries.h"
#import "NSPredicate+Distinct.h"

@interface MedRepVisitsFilterViewController ()

@end

@implementation MedRepVisitsFilterViewController
@synthesize filterPopOverController,contentArray,previouslyFilteredContent;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    plannedVisitDetailsDict=[[NSMutableDictionary alloc]init];
    
    locationNamesArray=[[NSMutableArray alloc]init];
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = self.filterTitle;
    self.navigationItem.titleView =     self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(self.filterTitle, nil)];
    


    // Do any additional setup after loading the view from its nib.
    
   
    
    if ([previouslyFilteredContent valueForKey:@"Visit_Location"] !=nil) {
        
        [plannedVisitDetailsDict setValue:[previouslyFilteredContent valueForKey:@"Visit_Location"] forKey:@"Visit_Location"];
        
        visitLocationTextField.text=[NSString stringWithFormat:@"%@", [previouslyFilteredContent valueForKey:@"Visit_Location"]];
        
        
        
    }
    
   
    if ([previouslyFilteredContent valueForKey:@"Visit_Date"] !=nil) {
        
        [plannedVisitDetailsDict setValue:[previouslyFilteredContent valueForKey:@"Visit_Date"] forKey:@"Visit_Date"];
        
        
        NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[NSString stringWithFormat:@"%@", [previouslyFilteredContent valueForKey:@"Visit_Date"]]];
        
        
        visitDateTextField.text=refinedDate;
        
        
    }

    
    if ([previouslyFilteredContent valueForKey:@"Visit_Type"] !=nil) {
        
        [plannedVisitDetailsDict setValue:[previouslyFilteredContent valueForKey:@"Visit_Type"] forKey:@"Visit_Type"];
        
        
        if ([[previouslyFilteredContent valueForKey:@"Visit_Type"] isEqualToString:@"N"]) {

            
            visitTypeTextField.text=@"Normal";
            

        }
        
      else  if ([[previouslyFilteredContent valueForKey:@"Visit_Type"] isEqualToString:@"R"]) {
            
            
            visitTypeTextField.text=@"Round Table";
            
            
        }
        
        
        
        
        
    }
    
    if ([previouslyFilteredContent valueForKey:@"Location_Type"] !=nil) {
        
        [plannedVisitDetailsDict setValue:[previouslyFilteredContent valueForKey:@"Location_Type"] forKey:@"Location_Type"];
        
        if ([[previouslyFilteredContent valueForKey:@"Location_Type"] isEqualToString:@"D"]) {
            
            locationTypeTextField.text=@"Hospital";

        }
        
        else
        {
            locationTypeTextField.text=@"Pharmacy";
        }
        
        
        
        
    }
    
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingVisitsFilter];
    }
    
    NSLog(@"previous filter content is %@", previouslyFilteredContent);
    
    
   // NSLog(@"content array in visit filter is %@", [self.contentArray description]);
    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    
    
     
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    
    clearFilterBtn.tintColor=[UIColor whiteColor];
    
    
    
    
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    
    if(self.isMovingToParentViewController)
    {
        /*setting today date as default value*/
        NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
        myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
        [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
        NSString *todayString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:[NSDate date]]];
        //[self saveDate:todayString];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    //contentArray=[[NSMutableArray alloc]init];
   // contentArray=[MedRepQueries fetchPlannedVisits];
    
//    for (NSInteger i=0; i<contentArray.count; i++) {
//        if ([[[contentArray objectAtIndex:i] valueForKey:@"Doctor_ID"] isEqualToString:@"0"]) {
//            //this is a pharmacy
//            [[contentArray objectAtIndex:i]setValue:@"P" forKey:@"Location_Type"];
//        }
// 
//        else
//        {
//            [[contentArray objectAtIndex:i]setValue:@"D" forKey:@"Location_Type"];
//
//        }
//    }
    
    
  //  NSLog(@"content array in did appear %@", [contentArray description]);
  
}

-(void)closeFilter

{
    
    
    if ([self.visitsFilterDelegate respondsToSelector:@selector(visitsFilterDidCancel)]) {
        
        //[self.visitsFilterDelegate visitsFilterDidCancel];
        
        [self.visitsFilterDelegate visitsFilterDidClose];
        
        
        [filterPopOverController dismissPopoverAnimated:YES];
        
    }
}

-(void)clearFilter

{
    filteredVisitsArray=[[NSMutableArray alloc]init];
    visitLocationTextField.text=@"";
    visitDateTextField.text=@"";
    visitTypeTextField.text=@"";
    locationTypeTextField.text=@"";

    plannedVisitDetailsDict=[[NSMutableDictionary alloc]init];
    
        
    /*
    
    if ([self.visitsFilterDelegate respondsToSelector:@selector(visitsFilterDidCancel)]) {
        
        [self.visitsFilterDelegate visitsFilterDidCancel];
        
        [filterPopOverController dismissPopoverAnimated:YES];
        
    }*/
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Visit Date Button Methods

- (IBAction)visitDateButtonTapped:(id)sender {
    
    MedRepVisitsDateFilterViewController * visitDateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
    visitDateFilterVC.visitDateDelegate=self;
    
    NSString * previouslySelectedDate=[plannedVisitDetailsDict valueForKey:@"Visit_Date"];
    
    if (previouslySelectedDate.length>0) {
        
        
        visitDateFilterVC.previouslySelectedDate=previouslySelectedDate;
    }
    else
    {
        
    }
    
    [self.navigationController pushViewController:visitDateFilterVC animated:YES];
}

-(void)selectedVisitDateDelegate:(NSString *)selectedDate
{

    visitDateTextField.text=[self saveDate:selectedDate];
    
    
    NSLog(@"selected visit date is %@ and returned date %@", visitDateTextField.text, selectedDate);
    
    NSString* refinedDateforfilter=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
    
    NSPredicate * refinedPredicate =[NSPredicate predicateWithFormat:@"SELF.Visit_Date contains [cd] %@", refinedDateforfilter];
    
    locationNamesArray=[[contentArray filteredArrayUsingPredicate:refinedPredicate] mutableCopy];
    
    
    
}
-(NSString *)saveDate:(NSString*)selectedDate
{
    NSLog(@"selected visit date in filter VC is %@", selectedDate);
    
    NSString* tempDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
    
    NSString* refinedDateforFilter=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:kDateFormatWithoutTime scrString:selectedDate];
    
    NSLog(@"refined date is %@", refinedDateforFilter);
    
    [plannedVisitDetailsDict setValue:tempDate forKey:@"Visit_Date"];
    
    return refinedDateforFilter;
}

#pragma mark Location Button Methods

- (IBAction)locationButtontapped:(id)sender {
    
    selectedDataString=@"Visit Location";
    
    
    
    NSMutableArray* locationDetailsArray=[[NSMutableArray alloc]init];
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit Location";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    
    
    
    NSString* filter = @"%K == [c] %@";
    NSMutableArray *predicateArray = [NSMutableArray array];
    if([[plannedVisitDetailsDict valueForKey:@"Location_Type"] length]>0) {
        
        // location type may be doctor or pharmacy so filter here by contact ID
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Location_Type", [plannedVisitDetailsDict valueForKey:@"Location_Type"]]];
    }
    
    if([[plannedVisitDetailsDict valueForKey:@"Visit_Type"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Visit_Type", [NSString stringWithFormat:@"%@",[plannedVisitDetailsDict valueForKey:@"Visit_Type"]]]];
    }
    if([[plannedVisitDetailsDict valueForKey:@"Visit_Date"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Visit_Date Contains[cd] %@",[plannedVisitDetailsDict valueForKey:@"Visit_Date"]]];
        
    }

    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    locationNamesArray=[[contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];

    
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:@"Location_ID"];
    NSArray * refinedArray=[locationNamesArray filteredArrayUsingPredicate:refinedPred];
    filterDescArray=[refinedArray mutableCopy];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    else
    {
        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Data", nil) message:NSLocalizedString(@"No data available for selected date, please change visit date and try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [unavailableAlert show];
    }

}


-(void)selectedVisitLocationDetails:(NSMutableArray*)visitDetails
{
   // NSLog(@"details for visit location delegate %@", [visitDetails description]);
    
    [plannedVisitDetailsDict setValue:[visitDetails valueForKey:@"Location_Name"] forKey:@"Visit_Location"];

    [plannedVisitDetailsDict setValue:[visitDetails valueForKey:@"Location_ID"] forKey:@"Location_ID"];

    
    
     
    visitLocationTextField.text=[visitDetails valueForKey:@"Location_Name"];
    
}


- (IBAction)classButtonTapped:(id)sender {
    
    NSMutableArray* classArray=[[NSMutableArray alloc]init];
    
   
    NSLog(@"class array in visit filter %@", [classArray description]);
    
    
}


- (IBAction)visitTypeButtonTapped:(id)sender {
    
    selectedDataString=@"Visit Type";

    
    NSLog(@"visit type in content array %@", [contentArray valueForKey:@"Visit_Type"]);
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit Type";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    
    
      filterDescArray=contentArray;
    
    
    if (filterDescArray.count>0) {
        
   
    NSLog(@"filtered desc is %@", [filterDescArray description]);
    
    
        NSLog(@"filter desc before pushing is %@", [filterDescArray valueForKey:@"Visit_Type"]);
        
        NSMutableArray *distinctVisitTypesArray = [[[NSOrderedSet orderedSetWithArray:[filterDescArray valueForKey:@"Visit_Type"]] array] mutableCopy];

        NSLog(@"distinct filters are %@", [distinctVisitTypesArray description]);
        
        
        
        [distinctVisitTypesArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

        
        NSLog(@"distict visit types are %@", [distinctVisitTypesArray description]);
        
    
    for ( NSInteger i=0; i<distinctVisitTypesArray.count; i++) {
        
        NSString * locationTypeString=[distinctVisitTypesArray objectAtIndex:i];
        
       //fetch description from db
        
        
        
        
        
        if ([locationTypeString isEqualToString:@"R"]) {
            
            [distinctVisitTypesArray replaceObjectAtIndex:i withObject:@"Round Table"];
            
        }
        
         if ([locationTypeString isEqualToString:@"N"])
        {
            [distinctVisitTypesArray replaceObjectAtIndex:i withObject:@"Normal"];
            
        }
        
    }
    
        
        NSUInteger index = [distinctVisitTypesArray indexOfObject:@"N/A"];
        if (index!=NSNotFound) {
            [distinctVisitTypesArray removeObjectAtIndex:index];
        }

        
        
        NSMutableArray* locationAppCode=[MedRepQueries fetchCodeDescriptionfromAppCodes:@"Visit_Type"];

        if (locationAppCode.count>0) {
            filterDescVC.filterDescArray=[distinctVisitTypesArray mutableCopy];

        }
        
        else
        {

    NSLog(@"visit type is %@", [distinctVisitTypesArray mutableCopy]);
    
        filterDescVC.filterDescArray=[distinctVisitTypesArray mutableCopy];

        }
//    filterDescVC.filterDescArray=[[NSMutableArray alloc]initWithObjects:@"ONE", nil];

        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }

}


- (IBAction)locationTypeButtonTapped:(id)sender {
    
    selectedDataString=@"Location Type";
    
    NSMutableArray* locationDetailsArray=[[NSMutableArray alloc]init];
    
    for (NSInteger i=0; i<contentArray.count; i++) {
        
        [locationDetailsArray addObject:[MedRepQueries fetchLocationDetailswithLocationID:@"Location_Type" locationID:[[contentArray objectAtIndex:i] valueForKey:@"Location_ID"]]];
        
    }
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Location Type";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=locationDetailsArray;
    
    if (filterDescArray.count>0) {
        
        NSMutableArray *distinctLocationTypesArray = [[[NSOrderedSet orderedSetWithArray:[filterDescArray valueForKey:@"Location_Type"]] array] mutableCopy];
        
        for ( NSInteger i=0; i<distinctLocationTypesArray.count; i++) {
            
            NSString * locationTypeString=[distinctLocationTypesArray objectAtIndex:i];
            
            if ([locationTypeString isEqualToString:@"D"]) {
                
                
                
                NSString* appCodeStr=[MedRepQueries fetchAppCodeforLocationtype:locationTypeString];
                
              //  [distinctLocationTypesArray replaceObjectAtIndex:i withObject:appCodeStr];
                
                [distinctLocationTypesArray replaceObjectAtIndex:i withObject:@"Hospital"];
                
            }
            
            else if ([locationTypeString isEqualToString:@"P"])
            {
                [distinctLocationTypesArray replaceObjectAtIndex:i withObject:@"Pharmacy"];

            }
        }

        
        filterDescVC.filterDescArray=[distinctLocationTypesArray mutableCopy];
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }

    
}

#pragma Visit Description Delegate

-(void)selectedFilterValue:(NSString*)selectedString;
{
    NSLog(@"selected filter in visits %@", [selectedString description]);
    
    if ([selectedDataString isEqualToString:@"Visit Location"]) {
        
      //  [locationBtn setTitle:selectedString forState:UIControlStateNormal];
        [plannedVisitDetailsDict setValue:selectedString forKey:@"Visit_Location"];

        visitLocationTextField.text=selectedString;
    }
    
    else if ([selectedDataString isEqualToString:@"Location Type"])
    {
       // [locationTypeBtn setTitle:selectedString forState:UIControlStateNormal];
        locationTypeTextField.text=selectedString;

        if ([selectedString isEqualToString:@"Hospital"]) {
            
            [plannedVisitDetailsDict setValue:@"D" forKey:@"Location_Type"];

        }
        
        else if ([selectedString isEqualToString:@"Pharmacy"])
        {
            [plannedVisitDetailsDict setValue:@"P" forKey:@"Location_Type"];

        }
        
    }
    else if ([selectedDataString isEqualToString:@"Visit Type"])
    {
       // [visitTypeBtn setTitle:selectedString forState:UIControlStateNormal];
        visitTypeTextField.text=selectedString;

        if ([selectedString isEqualToString:@"Round Table"]) {
            
            [plannedVisitDetailsDict setValue:@"R" forKey:@"Visit_Type"];
            
        }
        
        else if ([selectedString isEqualToString:@"Normal"])
        {
            [plannedVisitDetailsDict setValue:@"N" forKey:@"Visit_Type"];
            
        }
        

    }
    
    
    
}

- (IBAction)searchButtonTapped:(id)sender {
    
    filteredVisitsArray=[[NSMutableArray alloc]init];
    NSString* filter = @"%K == [c] %@";
    NSMutableArray *predicateArray = [NSMutableArray array];
    if([[plannedVisitDetailsDict valueForKey:@"Location_Type"] length]>0) {
        
        // location type may be doctor or pharmacy so filter here by contact ID
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Location_Type", [plannedVisitDetailsDict valueForKey:@"Location_Type"]]];
    }
    
    
    if([[plannedVisitDetailsDict valueForKey:@"Visit_Location"] length]>0) {
        
        
        //get location ID from predicate
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Location_ID", [NSString stringWithFormat:@"%@",[plannedVisitDetailsDict valueForKey:@"Location_ID"]]]];
        
    }
    
    if([[plannedVisitDetailsDict valueForKey:@"Visit_Type"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Visit_Type", [NSString stringWithFormat:@"%@",[plannedVisitDetailsDict valueForKey:@"Visit_Type"]]]];
    }
    if([[plannedVisitDetailsDict valueForKey:@"Visit_Date"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Visit_Date Contains[cd] %@",[plannedVisitDetailsDict valueForKey:@"Visit_Date"]]];
        
    }
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    filteredVisitsArray=[[contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    NSLog(@"filtered visit in filter %@", [filteredVisitsArray description]);
    
    if (predicateArray.count==0) {
    
        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:@"Please select your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noFilterAlert show];
    }
    
    
    
   else if (filteredVisitsArray.count>0) {
     
    if ([self.visitsFilterDelegate respondsToSelector:@selector(filteredVisitsDelegate:)]) {
        
        
        [self.visitsFilterDelegate filteredVisitParameters:plannedVisitDetailsDict];
        [self.visitsFilterDelegate filteredVisitsDelegate:filteredVisitsArray];

    }
    
    }
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];

    }

    
}



- (IBAction)resetButtonTapped:(id)sender {
    NSMutableDictionary *tempFilteredParameters=[[NSMutableDictionary alloc]init];
    NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];
    [tempFilteredParameters setValue:currentDateinDisplayFormat forKey:@"Visit_Date"];

    if ([self.visitsFilterDelegate respondsToSelector:@selector(visitsFilterDidCancel)]) {
        [self.visitsFilterDelegate filteredVisitParameters:tempFilteredParameters];
        [self.visitsFilterDelegate visitsFilterDidCancel];

        [filterPopOverController dismissPopoverAnimated:YES];
    }
}


#pragma mark UITextField method

-(void)textFieldDidTapped:(MedRepTextField*)tappedTextField withTitle:(NSString*)textFieldTitle withContentArray:(NSMutableArray*)textFieldContentArray
{
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=textFieldTitle;
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    filterDescVC.filterDescArray=textFieldContentArray;
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==visitDateTextField) {
        MedRepVisitsDateFilterViewController * visitDateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        visitDateFilterVC.visitDateDelegate=self;
        NSString * previouslySelectedDate=[plannedVisitDetailsDict valueForKey:@"Visit_Date"];
        if (previouslySelectedDate.length>0) {
            visitDateFilterVC.previouslySelectedDate=previouslySelectedDate;
        }
        else
        {
            
        }
        [self.navigationController pushViewController:visitDateFilterVC animated:YES];
    }
    else if (textField==visitTypeTextField)
    {
        
        selectedDataString=@"Visit Type";
        
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=contentArray;
        if (filterDescArray.count>0) {
            NSMutableArray *distinctVisitTypesArray = [[[NSOrderedSet orderedSetWithArray:[filterDescArray valueForKey:@"Visit_Type"]] array] mutableCopy];
            [distinctVisitTypesArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            for ( NSInteger i=0; i<distinctVisitTypesArray.count; i++) {
                NSString * locationTypeString=[distinctVisitTypesArray objectAtIndex:i];
                //fetch description from db
                if ([locationTypeString isEqualToString:@"R"]) {
                    [distinctVisitTypesArray replaceObjectAtIndex:i withObject:@"Round Table"];
                }
                if ([locationTypeString isEqualToString:@"N"])
                {
                    [distinctVisitTypesArray replaceObjectAtIndex:i withObject:@"Normal"];
                }
            }
            NSUInteger index = [distinctVisitTypesArray indexOfObject:@"N/A"];
            if (index!=NSNotFound) {
                [distinctVisitTypesArray removeObjectAtIndex:index];
            }
            NSMutableArray* locationAppCode=[MedRepQueries fetchCodeDescriptionfromAppCodes:@"Visit_Type"];
            NSMutableArray * contentArrayTapped=[[NSMutableArray alloc]init];
            if (locationAppCode.count>0) {
                contentArrayTapped=[distinctVisitTypesArray mutableCopy];
                
            }
            else
            {
                contentArrayTapped=[distinctVisitTypesArray mutableCopy];
            }
            [self textFieldDidTapped:visitTypeTextField withTitle:@"Visit Type" withContentArray:contentArrayTapped];
        }
        
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
        
    }
    else if (textField==locationTypeTextField)
    {
        
        selectedDataString=@"Location Type";
        NSMutableArray* locationDetailsArray=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<contentArray.count; i++) {
            [locationDetailsArray addObject:[MedRepQueries fetchLocationDetailswithLocationID:@"Location_Type" locationID:[[contentArray objectAtIndex:i] valueForKey:@"Location_ID"]]];
        }
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=locationDetailsArray;
        if (filterDescArray.count>0) {
            NSMutableArray *distinctLocationTypesArray = [[[NSOrderedSet orderedSetWithArray:[filterDescArray valueForKey:@"Location_Type"]] array] mutableCopy];
            for ( NSInteger i=0; i<distinctLocationTypesArray.count; i++) {
                NSString * locationTypeString=[distinctLocationTypesArray objectAtIndex:i];
                if ([locationTypeString isEqualToString:@"D"]) {
                    NSString* appCodeStr=[MedRepQueries fetchAppCodeforLocationtype:locationTypeString];
                    [distinctLocationTypesArray replaceObjectAtIndex:i withObject:@"Hospital"];
                }
                else if ([locationTypeString isEqualToString:@"P"])
                {
                    [distinctLocationTypesArray replaceObjectAtIndex:i withObject:@"Pharmacy"];
                }
            }
            [self textFieldDidTapped:locationTypeTextField withTitle:@"Location Type" withContentArray:[distinctLocationTypesArray mutableCopy]];
            
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
        
        
    }
    else if (textField==visitLocationTextField)
    {
        
        selectedDataString=@"Visit Location";
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        NSString* filter = @"%K == [c] %@";
        NSMutableArray *predicateArray = [NSMutableArray array];
        if([[plannedVisitDetailsDict valueForKey:@"Location_Type"] length]>0) {
            // location type may be doctor or pharmacy so filter here by contact ID
            [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Location_Type", [plannedVisitDetailsDict valueForKey:@"Location_Type"]]];
        }
        if([[plannedVisitDetailsDict valueForKey:@"Visit_Type"] length]>0) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Visit_Type", [NSString stringWithFormat:@"%@",[plannedVisitDetailsDict valueForKey:@"Visit_Type"]]]];
        }
        if([[plannedVisitDetailsDict valueForKey:@"Visit_Date"] length]>0) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Visit_Date Contains[cd] %@",[plannedVisitDetailsDict valueForKey:@"Visit_Date"]]];
            
        }
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        locationNamesArray=[[contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
        NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:@"Location_ID"];
        NSArray * refinedArray=[locationNamesArray filteredArrayUsingPredicate:refinedPred];
        filterDescArray=[refinedArray mutableCopy];
        if (filterDescArray.count>0) {
            [self textFieldDidTapped:locationTypeTextField withTitle:@"Visit Location" withContentArray:filterDescArray];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"No data available for selected date, please change visit date and try again" withController:self];

        }
        
    }
        
    return NO;
}

@end
