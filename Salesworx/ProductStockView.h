//
//  ProductStockView.h
//  SWProducts
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "DetailBaseView.h"

@class ProductStockView;
@protocol ProductStockViewDelegate <NSObject>
@optional
- (void)productStockViewLoaded;
- (void)productStockViewDidFinishedEditingForRowIndex:(int)rowIndex;

@end

@interface ProductStockView : UIView <  GridViewDataSource, GridViewDelegate, EditableCellDelegate > {
    NSMutableArray *items;
    GridView *gridView;
    NSDictionary *product;
    BOOL editMode;
    id < ProductStockViewDelegate > delegate;
    UILabel *discountLable;
    NSString *isOnOrder;
}
//@property (nonatomic, strong) ProductService *serProduct;
-(void)loadStock;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) GridView *gridView;
@property (nonatomic, assign) BOOL editMode;


@property (unsafe_unretained) id < ProductStockViewDelegate > delegate;

- (id)initWithProduct:(NSDictionary *)product;

@end