//
//  MedRepProductMediaDisplayViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PDFViewDismissDelegate <NSObject>

-(void)PDFViewerDidCancel;

@end

@interface MedRepProductMediaDisplayViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    id pdfViewerdelegate;
}

@property(strong,nonatomic) id pdfViewerdelegate;


@property (strong, nonatomic) IBOutlet UIWebView *pdfWebView;

@property(strong,nonatomic)NSString *filePath;

@property (strong,nonatomic)NSMutableArray* productImagesArray;

@property(nonatomic)NSIndexPath* selectedIndexPath;

- (IBAction)closeButtonTapped:(id)sender;

@property (nonatomic)BOOL isImage;

@property (strong, nonatomic) IBOutlet UITableView *imagesTableView;

@end
