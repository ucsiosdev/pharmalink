//
//  ReportsCollectionViewCell.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "ReportsCollectionViewCell.h"

@implementation ReportsCollectionViewCell

@synthesize descTxtView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ReportsCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]])
        {
            return nil;
        }
        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}

- (void)awakeFromNib
{
    // Shadow and Radius
    self.layer.borderWidth=0.5;
    self.layer.borderColor=[kVisitOptionsBorderColor CGColor];
    self.layer.shadowColor = [kVisitOptionsBorderColor CGColor];
    self.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.layer.shadowOpacity = 1.0f;
    self.layer.shadowRadius = 0.0f;
    self.layer.masksToBounds = NO;
    self.layer.cornerRadius = 4.0f;
    
    self.layerView.layer.cornerRadius=4.0f;
    self.descLbl.font=kSWX_FONT_REGULAR(16);
    descTxtView.textContainerInset = UIEdgeInsetsZero;
    descTxtView.textContainer.lineFragmentPadding = 0;
}


@end
