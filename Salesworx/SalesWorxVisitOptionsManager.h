//
//  SalesWorxVisitOptionsManager.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppControl.h"
#import "SalesWorxCustomClass.h"

#define kDisplay









@interface SalesWorxVisitOptionsManager : NSObject
{
    NSMutableArray *customerLevelEnabledVisitOptionsArray;
    AppControl * appControl;
    NSArray *PDAComponents;
    BOOL customerLevelVisitOptionsFlagEnabled;

}
+ (SalesWorxVisitOptionsManager*) retrieveSingleton;
-(NSMutableArray*)fetchVisitOptionsforCustomer:(SalesWorxVisit*)currentvisit;

-(BOOL)isDistributionCheckAvailable;

@end
