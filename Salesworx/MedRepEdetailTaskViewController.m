//
//  MedRepEdetailTaskViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepEdetailTaskViewController.h"
#import "SWDefaults.h"
#import "MedRepEdetailingTaskDetailViewController.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "MedRepTaskListTableViewCell.h"
#import "MedrepPharmacyLocationsTableViewCell.h"

@interface MedRepEdetailTaskViewController ()

@end

@implementation MedRepEdetailTaskViewController
@synthesize tasksArray,dashBoardViewing,visitDetails;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Tasks"];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)viewWillAppear:(BOOL)animated
{

    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingTasks];
    }
    
    if (self.isVisitCompleted==YES|| self.isInMultiCalls==YES) {
        
    }
//    else if(dashBoardViewing==YES)
//    {
//        NSLog(@"this is dashboard viewing");
//    }
    
    else{
    
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Add", nil) style:UIBarButtonItemStylePlain target:self action:@selector(addTapped)];
        [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        
    self.navigationItem .rightBarButtonItem=saveBtn;
    
    }
    UIBarButtonItem* cancelBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    [cancelBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    self.navigationItem .leftBarButtonItem=cancelBtn;
    
    
  //  NSLog(@"check for visit details dict in task %@", [self.visitDetailsDict description]);
    
    
    NSLog(@"current task array in tasks list %@", [tasksArray description]);
    
    //task to be based on location id and contact id
    
   // NSString* contactID=[self.visitDetailsDict valueForKey:@"Contact_ID"];
    
    
//    if ([[self.visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
//        
//        tasksArray=[MedRepQueries fetchTaskswithLocationID:[self.visitDetailsDict valueForKey:@"Location_ID"] andcontactID:[self.visitDetailsDict valueForKey:@"Contact_ID"]];
//    }
//    
//    else
//    {
//        tasksArray=[MedRepQueries fetchTaskswithLocationID:[self.visitDetailsDict valueForKey:@"Location_ID"] andDoctorID:[self.visitDetailsDict valueForKey:@"Doctor_ID"]];
//    }
    
    NSLog(@"task array is %@",[tasksArray description]);
    
    
    
    
   // tasksArray=[MedRepQueries fetchTaskswithLocationID:[self.visitDetailsDict valueForKey:@"Location_ID"]];
    //NSLog(@"Tasks are %@", [tasksArray description]);
    
    if (tasksArray.count>0) {
        
        [self.tasksTblView reloadData];
    }
    
   

}

- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self
    || (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController)
    || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}

-(void)cancelTapped
{
   
    
    
    if ([self.delegate respondsToSelector:@selector(popOverControlDidClose:)]) {
        
        [self.delegate popOverControlDidClose:tasksArray];
        
    }
    
    if ([self isModal]) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.tasksPopOverController dismissPopoverAnimated:YES];

    }
}


-(void)addTapped
{
    MedRepEdetailingTaskDetailViewController* detailVC=[[MedRepEdetailingTaskDetailViewController alloc]init];
    detailVC.visitDetailsDict=_visitDetailsDict;
    
    detailVC.updatesTasksDelegate=self;
    detailVC.tasksArray=tasksArray;
    detailVC.currentVisit=self.visitDetails;

    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isInMultiCalls) {
        
        return 75.0f;
    }
    else
    {
    return 44.0f;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tasksArray.count>0) {
        return tasksArray.count;

    
    }
    
    
    else
    {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TaskListCell";
    
   
    MedRepTaskListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepTaskListTableViewCell" owner:nil options:nil] firstObject];
        
        
        
        
        
    }
    
    
    VisitTask * task=[tasksArray objectAtIndex:indexPath.row];

    
    cell.titleLbl.text=[NSString stringWithFormat:@"%@", task.Title];

    if (self.isInMultiCalls==YES) {
        cell.descriptionLbl.hidden=NO;
        
        cell.statusImageViewTopConstraint.constant=27.0f;

        //cell.statusImageView.frame= CGRectMake(8, 26, 20, 20);

        cell.descriptionLblheightConstraint.constant=25.0;
    
    if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        
        cell.descriptionLbl.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchDoctorwithID:task.Doctor_ID]];
        

    }
    else if([visitDetails.Location_Type isEqualToString:kPharmacyLocation])
    {
        cell.descriptionLbl.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchContactwithContactID:task.Contact_ID]];
        

    }
    }
    else
    {
        cell.descriptionLbl.hidden=YES;
        cell.statusImageViewTopConstraint.constant=8.0f;
        cell.statusImageViewHeightConstraint.constant=20.0f;
        cell.statusImageViewWidthConstraint.constant=20.0f;
        //cell.statusImageView.frame= CGRectMake(8, 8, 20, 20);

        cell.descriptionLblheightConstraint.constant=0.0f;

    }
    
    
    UIImage* statusImage;
    
//    NSString* taskStatus=[[tasksArray objectAtIndex:indexPath.row] valueForKey:@"Status"];
    
    NSString* taskStatus=task.Status;
    
    
    NSLog(@"Status is %@", taskStatus);
    
    if ([taskStatus isEqualToString:@"New"]) {
        
        statusImage=[UIImage imageNamed:@"Task_NewIcon"];
    }
    
  else  if ([taskStatus isEqualToString:@"Closed"]) {
        
        statusImage=[UIImage imageNamed:@"Task_CloseIcon"];
    }
    
    else if ([taskStatus isEqualToString:@"Deffered"])
        
    {        statusImage=[UIImage imageNamed:@"Task_DeferredIcon"];

        
    }
    else if ([taskStatus isEqualToString:@"Cancelled"])
    {
        statusImage=[UIImage imageNamed:@"Task_CancelIcon"];

    }
    
    
    cell.statusImageView.image=statusImage;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    cell.textLabel.font=MedRepTitleFont;
    cell.textLabel.textColor=MedRepMenuTitleFontColor;
    
    //cell.statusImageView.image=nil;
    
    
//    cell.titleLbl.text=[NSString stringWithFormat:@"%@", [[tasksArray valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    
    cell.titleLbl.text=[NSString stringWithFormat:@"%@", task.Title];

    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MedRepEdetailingTaskDetailViewController* detailVC=[[MedRepEdetailingTaskDetailViewController alloc]init];
   // detailVC.savedTaskDictionary=[tasksArray objectAtIndex:indexPath.row];
    
    detailVC.tasksArray=tasksArray;
    

    
    detailVC.updatesTasksDelegate=self;
    
    detailVC.selectedTaskIndexPath=indexPath;
    
    
    detailVC.visitDetailsDict=self.visitDetailsDict;
    detailVC.dashBoardViewing=dashBoardViewing;
    detailVC.currentVisit=self.visitDetails;
    detailVC.selectedTask=[self.visitDetails.taskArray objectAtIndex:indexPath.row];
    NSLog(@"visit details in task %@", self.visitDetailsDict);
    detailVC.isInMultiCalls=self.isInMultiCalls;
    detailVC.isViewing=YES;
    detailVC.isUpdatingTask=YES;
    
    if ([[[tasksArray objectAtIndex:indexPath.row]valueForKey:@"Status"]isEqualToString:@"Closed"] || [[[tasksArray objectAtIndex:indexPath.row]valueForKey:@"Status"]isEqualToString:@"Cancelled"]) {
        
        detailVC.isTaskClosed=YES;
    }
    else
    {
    
    detailVC.isVisitCompleted=self.isVisitCompleted;
    }
    NSLog(@"task array in did select %@", [tasksArray description]);
    
    
    
    
   
[self.navigationController pushViewController:detailVC animated:YES];
    
}


#pragma mark Task Delegate methods

-(void)updatedTasks:(NSMutableArray*)updatedTasksArray
{



    
    tasksArray=updatedTasksArray;
    
    self.visitDetails.taskArray=tasksArray;
    
    
    
    for (NSInteger i=0; i<tasksArray.count; i++) {
        
      VisitTask *  visitTask=[tasksArray objectAtIndex:i];
        
        NSLog(@"task title is in delegate %@", visitTask.Title);
        
    }
    
    
    NSLog(@"task array count in delegate %d", tasksArray.count);
    
    [self.tasksTblView reloadData];
    
}


@end
