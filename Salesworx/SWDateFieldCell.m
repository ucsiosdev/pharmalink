//
//  SWDateFieldCell.m
//  SWFoundation
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDateFieldCell.h"
#import "SWFoundation.h"
@implementation SWDateFieldCell

@synthesize textField;
@synthesize date;
@synthesize datePicker;
@synthesize titleLabel;
@synthesize detailLabel;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (self.textField) {
            self.textField=nil;
        }
        [self setTextField:[[UITextField alloc] initWithFrame:CGRectZero] ];
        self.textField.font = LightFontOfSize(14.0f);
        [self.contentView addSubview:self.textField];
        
        if (self.titleLabel) {
            self.titleLabel=nil;
        }
        if (self.detailLabel) {
            self.detailLabel=nil;
        }
        if (self.datePicker) {
            self.datePicker=nil;
        }
        [self setTitleLabel:[[UILabel alloc] initWithFrame:CGRectZero] ];
        [self setDetailLabel:[[UILabel alloc] initWithFrame:CGRectZero] ];
        
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.detailLabel];
        
        [self setDate:[NSDate date]];
        
        [self setDatePicker:[[SWDatePicker alloc] initWithFrame:CGRectMake(0, 0, 400, 224)] ];
        [self.datePicker setDelegate:self];
    

        [self.textField setInputView:self.datePicker];
        [self.contentView addSubview:self.textField];
        [self.textField setBackgroundColor:[UIColor blueColor]];
        
           }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    [self.titleLabel setFont:LightFontOfSize(14.0f)];
    [self.detailLabel setFont:LightFontOfSize(14.0f)];
    
    if (self.titleLabel.text.length < 1) {
        [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
        [self.titleLabel setTextColor:[UIColor blackColor]];
    } else {
        [self.titleLabel setTextAlignment:NSTextAlignmentRight];
        [self.titleLabel setTextColor:UIColorFromRGB(0x556993)];
    }
    
    if (selected) {
        [self.datePicker.datePicker setDate:self.date animated:YES];
        [self.textField becomeFirstResponder];
    } else {

    }
}


- (void)updateDateLabel {
    NSDateFormatter *df = [NSDateFormatter new] ;
    [df setDateStyle:NSDateFormatterMediumStyle]; // day, Full month and year
    [df setTimeStyle:NSDateFormatterNoStyle];  // nothing
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [df setLocale:usLocale];
    [self.detailLabel setText:[df stringFromDate:self.date]];
    df=nil;
    usLocale=nil;
}

- (void)setDate:(NSDate *)d {
    if (date) {
    }
    
    if ([d isKindOfClass:[NSDate class]]) {
        date = d;
    } else {
        date = [NSDate date] ;
    }
    
    [self updateDateLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    // get the bounding rectangle of the content view
    CGRect bounds = [self.contentView bounds];
    
    // if we have a label to render, then it gets 1/3 the width...
    if ([self.titleLabel.text length] > 0) {
        // label gets 1/3 the width and automatically centers itself
        self.titleLabel.frame = CGRectMake(5, 5,CGRectGetWidth(bounds) / 3, CGRectGetHeight(bounds) - 10);
        
        // the height of the text field is the same as the height of the font used within it
        UIFont *textFieldFont = [self.textField font];
        CGFloat textFieldHeight = textFieldFont.ascender - textFieldFont.descender;
        
        // add 2px to support the (x) clear indicator
        textFieldHeight += 2;
        
        // text field gets remaining width
        self.detailLabel.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + 10, 
                                     (CGRectGetHeight(bounds) - textFieldHeight) / 2, 
                                     2 * CGRectGetWidth(bounds) / 3 - 20,
                                     textFieldHeight);
    }
    // otherwise, we just give the text field the entire content pane
    else {
        [self.titleLabel setFrame:CGRectZero];
        
        [self.detailLabel setFrame:CGRectInset(bounds, 10, 0)];
    }
}

#pragma mark UITableViewCell+Responder override
- (void)resignResponder {
    if ([self.textField isFirstResponder]) {
        [self.textField resignFirstResponder];
    }
}

#pragma mark - FBDatePicker Delegate
- (void)datePickerSelectionDone:(SWDatePicker *)picker withDate:(NSDate *)d {
    [self.textField resignFirstResponder];
    
    [self setDate:d];
    
    if ([self.delegate respondsToSelector:@selector(editableCell:didUpdateValue:forKey:)]) {
        [self.delegate editableCell:self didUpdateValue:self.date forKey:self.key];
    }
}

- (void)datePickerSelectionCancelled:(SWDatePicker *)picker {
    [self.textField resignFirstResponder];
}

#pragma mark TextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(editableCellDidStartUpdate:)]) {
        [self.delegate editableCellDidStartUpdate:self];
    }
}

@end