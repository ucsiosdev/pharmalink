//
//  MedRepQueries.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "SWAppDelegate.h"
#import "MedRep-Swift.h"

@implementation MedRepQueries


#pragma mark Doctors Data Methods


+(NSMutableArray*)fetchDoctorsforCreateLocation

{
    NSString* doctorsQuery=@"";
   /* if ([SWDefaults isDrApprovalAvailable])
    {
        doctorsQuery=[NSString stringWithFormat:@"select IFNULL(Doctor_ID,'N/A') AS Doctor_ID, IFNULL(Doctor_Name,'N/A') AS Contact_Name from PHX_TBL_Doctors where Is_Approved = 'Y' order by Contact_Name ASC"];
    }
    else{
         doctorsQuery=[NSString stringWithFormat:@"select IFNULL(Doctor_ID,'N/A') AS Doctor_ID, IFNULL(Doctor_Name,'N/A') AS Contact_Name from PHX_TBL_Doctors order by Contact_Name ASC"];
    }*/
    doctorsQuery=[NSString stringWithFormat:@"select IFNULL(Doctor_ID,'N/A') AS Doctor_ID, IFNULL(Doctor_Name,'N/A') AS Contact_Name from PHX_TBL_Doctors order by Contact_Name ASC"];
    NSMutableArray* doctorsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorsQuery];
    if (doctorsArray.count>0) {
        return doctorsArray;
    }
    else
    {
        return nil;
    }
}


+(NSMutableArray*)fetchDoctorsData
{
    NSString* doctorsQuery=@"";
    /*if ([SWDefaults isDrApprovalAvailable])
    {
        doctorsQuery=[NSString stringWithFormat:@"select IFNULL(Classification_1, 'N/A') AS Specialization,IFNULL(A.Doctor_ID,'N/A') AS Doctor_ID, IFNULL(A.Doctor_Name,'N/A') AS Contact_Name,IFNULL(B.Location_ID,'N/A') AS Location_ID from PHX_TBL_Doctors AS A inner join PHX_TBL_Doctor_location_Map AS B on A.Doctor_ID=B.Doctor_ID where A.Is_Approved = 'Y'"];
    }
    else{
         doctorsQuery=[NSString stringWithFormat:@"select IFNULL(Classification_1, 'N/A') AS Specialization,IFNULL(A.Doctor_ID,'N/A') AS Doctor_ID, IFNULL(A.Doctor_Name,'N/A') AS Contact_Name,IFNULL(B.Location_ID,'N/A') AS Location_ID from PHX_TBL_Doctors AS A inner join PHX_TBL_Doctor_location_Map AS B on A.Doctor_ID=B.Doctor_ID"];
    }*/
    
    doctorsQuery=[NSString stringWithFormat:@"select IFNULL(Classification_1, 'N/A') AS Specialization,IFNULL(A.Doctor_ID,'N/A') AS Doctor_ID, IFNULL(A.Doctor_Name,'N/A') AS Contact_Name,IFNULL(B.Location_ID,'N/A') AS Location_ID from PHX_TBL_Doctors AS A inner join PHX_TBL_Doctor_location_Map AS B on A.Doctor_ID=B.Doctor_ID"];

    
    NSMutableArray* doctorsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorsQuery];
    
    if (doctorsArray.count>0) {
        
       // NSLog(@"doctors query response is %@", [doctorsArray description]);
        
        return doctorsArray;
        
        
    }
    else
    {
        return nil;
    }
    
    
}
+(NSMutableArray*)fetchDoctorsDataWhenPendingVisitsEnabled
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    dateFormatter.dateFormat=@"yyyy-MM";
    NSString * monthString = [[dateFormatter stringFromDate:[NSDate date]] capitalizedString];
    
//    NSString* doctorsQuery=[NSString stringWithFormat:@"SELECT * FROM (select IFNULL(A.Doctor_ID,'N/A') AS Doctor_ID, IFNULL(A.Doctor_Name,'N/A') AS Contact_Name,IFNULL(B.Location_ID,'N/A') AS Location_ID,IFNULL(Classification_2,'N/A') AS Class from PHX_TBL_Doctors AS A inner join PHX_TBL_Doctor_location_Map AS B on A.Doctor_ID=B.Doctor_ID and B.Created_At like '%%%@%%') t1 INNER JOIN (select IFNULL(Custom_Attribute_1,'N/A') AS Visits_Req, IFNULL(Code_Value,'N/A') AS value from TBL_App_Codes where Code_Type='CLASS') t2 on t1.Class = t2.value INNER JOIN (SELECT count(*) As CompletedVisit,Doctor_ID FROM PHX_TBL_Actual_Visits group by Doctor_ID) t3 on t1.Doctor_ID = t3.Doctor_ID",monthString];
    NSString* doctorsQuery=@"";
   /* if ([SWDefaults isDrApprovalAvailable])
    {
        doctorsQuery =[NSString stringWithFormat:@"Select IFNULL(D.Classification_1, 'N/A') AS Specialization,IFNULL(DL.Doctor_ID,'0') AS Doctor_ID,IFNULL(DL.Location_ID,'N/A') AS Location_ID, IFNULL(D.Doctor_Name,'N/A') AS Contact_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where    D.Doctor_ID NOT IN ( Select X.Doctor_ID from (SELECT A.Doctor_ID,count(A.Doctor_ID) AS Visits_Completed,IFNULL(D.Custom_Attribute_5,C.Custom_Attribute_1) AS Visits_Required   FROM (Select * from PHX_TBL_Actual_Visits WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') and Doctor_ID  NOT NULL GROUP by Doctor_ID|| Planned_Visit_ID) AS A Inner JOIN PHX_TBL_Doctors AS D  ON A.Doctor_ID =D.Doctor_ID inner join TBL_App_Codes AS C ON  D.Classification_2=C.Code_Value WHERE  C.Code_Type='CLASS'  group by  A.Doctor_ID order by  A.Visit_End_Date desc) AS X  where Visits_Completed >= CAST(Visits_Required AS INTEGER)) and D.Is_Approved = 'Y' group by D.Doctor_ID order by Doctor_Name  COLLATE NOCASE ASC"];

    }else{
        doctorsQuery =[NSString stringWithFormat:@"Select IFNULL(D.Classification_1, 'N/A') AS Specialization,IFNULL(DL.Doctor_ID,'0') AS Doctor_ID,IFNULL(DL.Location_ID,'N/A') AS Location_ID, IFNULL(D.Doctor_Name,'N/A') AS Contact_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where    D.Doctor_ID NOT IN ( Select X.Doctor_ID from (SELECT A.Doctor_ID,count(A.Doctor_ID) AS Visits_Completed,IFNULL(D.Custom_Attribute_5,C.Custom_Attribute_1) AS Visits_Required   FROM (Select * from PHX_TBL_Actual_Visits WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') and Doctor_ID  NOT NULL GROUP by Doctor_ID|| Planned_Visit_ID) AS A Inner JOIN PHX_TBL_Doctors AS D  ON A.Doctor_ID =D.Doctor_ID inner join TBL_App_Codes AS C ON  D.Classification_2=C.Code_Value WHERE  C.Code_Type='CLASS'  group by  A.Doctor_ID order by  A.Visit_End_Date desc) AS X  where Visits_Completed >= CAST(Visits_Required AS INTEGER)) group by D.Doctor_ID order by Doctor_Name  COLLATE NOCASE ASC"];

    }*/
    doctorsQuery =[NSString stringWithFormat:@"Select IFNULL(D.Classification_1, 'N/A') AS Specialization,IFNULL(DL.Doctor_ID,'0') AS Doctor_ID,IFNULL(DL.Location_ID,'N/A') AS Location_ID, IFNULL(D.Doctor_Name,'N/A') AS Contact_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where    D.Doctor_ID NOT IN ( Select X.Doctor_ID from (SELECT A.Doctor_ID,count(A.Doctor_ID) AS Visits_Completed,IFNULL(D.Custom_Attribute_5,C.Custom_Attribute_1) AS Visits_Required   FROM (Select * from PHX_TBL_Actual_Visits WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') and Doctor_ID  NOT NULL GROUP by Doctor_ID|| Planned_Visit_ID) AS A Inner JOIN PHX_TBL_Doctors AS D  ON A.Doctor_ID =D.Doctor_ID inner join TBL_App_Codes AS C ON  D.Classification_2=C.Code_Value WHERE  C.Code_Type='CLASS'  group by  A.Doctor_ID order by  A.Visit_End_Date desc) AS X  where Visits_Completed >= CAST(Visits_Required AS INTEGER)) group by D.Doctor_ID order by Doctor_Name  COLLATE NOCASE ASC"];

    
    NSLog(@"doctor qry when no location selected and pending visit on %@", doctorsQuery);
    
    
    NSMutableArray* doctorsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorsQuery];
    

    if (doctorsArray.count>0)
    {
        [doctorsArray setValue:@"D" forKey:@"Location_Type"];
       
       // NSLog(@"doctors when visits enabled %@", doctorsArray);

        return doctorsArray;
        
    }
    else
    {
        return nil;
    }
}

+(NSMutableArray*)fetchDoctorLocations:(NSString*)doctorID

{
//    NSString* doctorLocationQuery=[NSString stringWithFormat:@"select IFNULL( D.Doctor_ID,'N/A')AS Doctor_ID, IFNULL(L.Location_ID,'N/A')AS Location_ID, IFNULL(DL.Location_Name,'N/A') Location_Name, IFNULL(DL.Address_1,'N/A') AS Adress, IFNULL(DL.City,'N/A') AS City, IFNULL(DL.State,'N/A') AS State ,IFNULL(DL.Email,'N/A') AS Email , IFNULL(DL.Phone,'N/A') AS Telephone , IFNULL(DL.Postal_Code,'N/A') AS Postal_Code,IFNULL(DL.Latitude,0) AS Latitude , IFNULL(DL.Longitude,0)AS Longitude, IFNULL(DL.Classification_2,'N/A') as Class ,IFNULL(DL.Classification_3,'N/A') AS OTC ,IFNULL(DL.Custom_Attribute_1,'N/A') AS Trade_Channel from  PHX_TBL_Doctors AS D  left join  PHX_TBL_Doctor_Location_map  L   on  D.Doctor_ID=L.Doctor_ID  left join PHX_TBL_Locations DL on L.Location_ID=DL.Location_ID where D.Doctor_ID ='%@'", doctorID];
    NSString* doctorLocationQuery=@"";
   /* if ([SWDefaults isDrApprovalAvailable])
    {
        doctorLocationQuery=[NSString stringWithFormat:@"select IFNULL( D.Doctor_ID,'N/A')AS Doctor_ID, IFNULL(L.Location_ID,'N/A')AS Location_ID,IFNULL(DL.Custom_Attribute_2,'N/A') AS Account_Name, IFNULL(DL.Location_Name,'N/A') Location_Name, IFNULL(DL.Address_2,'N/A') AS Location, IFNULL(DL.City,'N/A') AS Territory, IFNULL(DL.State,'N/A') AS State ,IFNULL(DL.Email,'N/A') AS Email , IFNULL(DL.Phone,'N/A') AS Telephone , IFNULL(DL.Postal_Code,'N/A') AS Postal_Code,IFNULL(DL.Latitude,0) AS Latitude , IFNULL(DL.Longitude,0)AS Longitude, IFNULL(DL.Classification_2,'N/A') as Class ,IFNULL(DL.Classification_3,'N/A') AS OTC ,IFNULL(DL.Custom_Attribute_1,'N/A') AS Trade_Channel from  PHX_TBL_Doctors AS D  left join  PHX_TBL_Doctor_Location_map  L   on  D.Doctor_ID=L.Doctor_ID  left join PHX_TBL_Locations DL on L.Location_ID=DL.Location_ID where D.Doctor_ID ='%@' and D.Is_Approved = 'Y'", doctorID];

    }else{
        doctorLocationQuery=[NSString stringWithFormat:@"select IFNULL( D.Doctor_ID,'N/A')AS Doctor_ID, IFNULL(L.Location_ID,'N/A')AS Location_ID,IFNULL(DL.Custom_Attribute_2,'N/A') AS Account_Name, IFNULL(DL.Location_Name,'N/A') Location_Name, IFNULL(DL.Address_2,'N/A') AS Location, IFNULL(DL.City,'N/A') AS Territory, IFNULL(DL.State,'N/A') AS State ,IFNULL(DL.Email,'N/A') AS Email , IFNULL(DL.Phone,'N/A') AS Telephone , IFNULL(DL.Postal_Code,'N/A') AS Postal_Code,IFNULL(DL.Latitude,0) AS Latitude , IFNULL(DL.Longitude,0)AS Longitude, IFNULL(DL.Classification_2,'N/A') as Class ,IFNULL(DL.Classification_3,'N/A') AS OTC ,IFNULL(DL.Custom_Attribute_1,'N/A') AS Trade_Channel from  PHX_TBL_Doctors AS D  left join  PHX_TBL_Doctor_Location_map  L   on  D.Doctor_ID=L.Doctor_ID  left join PHX_TBL_Locations DL on L.Location_ID=DL.Location_ID where D.Doctor_ID ='%@'", doctorID];
    }*/
    
    doctorLocationQuery=[NSString stringWithFormat:@"select IFNULL( D.Doctor_ID,'N/A')AS Doctor_ID, IFNULL(L.Location_ID,'N/A')AS Location_ID,IFNULL(DL.Custom_Attribute_2,'N/A') AS Account_Name, IFNULL(DL.Location_Name,'N/A') Location_Name, IFNULL(DL.Address_2,'N/A') AS Location, IFNULL(DL.City,'N/A') AS Territory, IFNULL(DL.State,'N/A') AS State ,IFNULL(DL.Email,'N/A') AS Email , IFNULL(DL.Phone,'N/A') AS Telephone , IFNULL(DL.Postal_Code,'N/A') AS Postal_Code,IFNULL(DL.Latitude,0) AS Latitude , IFNULL(DL.Longitude,0)AS Longitude, IFNULL(DL.Classification_2,'N/A') as Class ,IFNULL(DL.Classification_3,'N/A') AS OTC ,IFNULL(DL.Custom_Attribute_1,'N/A') AS Trade_Channel from  PHX_TBL_Doctors AS D  left join  PHX_TBL_Doctor_Location_map  L   on  D.Doctor_ID=L.Doctor_ID  left join PHX_TBL_Locations DL on L.Location_ID=DL.Location_ID where D.Doctor_ID ='%@'", doctorID];

    
   // NSLog(@"query for doctor locations %@", doctorLocationQuery);
    
    NSMutableArray* doctorsLocationsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorLocationQuery];
    
    if (doctorsLocationsArray.count>0) {
        
        return doctorsLocationsArray;
    }
    
    else
    {
        return nil;
        
    }
}



+(NSMutableArray*)fetchDoctorswithLocationID:(NSString*)locationID
{
    NSString* doctorLocationQry=@"";
    /*if ([SWDefaults isDrApprovalAvailable])
    {
        doctorLocationQry= [NSString stringWithFormat:@"select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where DL.Location_ID='%@' AND D.Is_Approved = 'Y' order by Doctor_Name  COLLATE NOCASE ASC ", locationID];
    }else{
        doctorLocationQry= [NSString stringWithFormat:@"select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where DL.Location_ID='%@' order by Doctor_Name  COLLATE NOCASE ASC ", locationID];
    }*/
    doctorLocationQry= [NSString stringWithFormat:@"select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where DL.Location_ID='%@' order by Doctor_Name  COLLATE NOCASE ASC ", locationID];

    
    //adding the condition fo pending visits
    
    NSLog(@"doctors for selected location %@", doctorLocationQry);
    
    NSMutableArray* demoLocationArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorLocationQry];
    
    if (demoLocationArray.count>0) {
        
        return demoLocationArray;
    }
    
    else{
        return nil;
    }
    
}
+(NSMutableArray*)fetchDoctorswithLocationIDWhenPendingVisitsEnabled:(NSString*)locationID
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    dateFormatter.dateFormat=@"yyyy-MM";
    NSString * monthString = [[dateFormatter stringFromDate:[NSDate date]] capitalizedString];
    
//    NSString* doctorLocationQry= [NSString stringWithFormat:@"SELECT * FROM (select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where DL.Location_ID='%@' and  DL.Created_At like '%%%@%%' order by Doctor_Name  COLLATE NOCASE ASC) t1 INNER JOIN (select IFNULL(Custom_Attribute_1,'N/A') AS Visits_Req, IFNULL(Code_Value,'N/A') AS value from TBL_App_Codes where Code_Type='CLASS') t2 on t1.Class = t2.value INNER JOIN (SELECT count(*) As CompletedVisit,Doctor_ID FROM PHX_TBL_Actual_Visits group by Doctor_ID) t3 on t1.Doctor_ID = t3.Doctor_ID", locationID,monthString];
    
    
    //updating pending visits qry
    NSString* doctorLocationQry;
   /* if ([SWDefaults isDrApprovalAvailable])
    {
        doctorLocationQry=[NSString stringWithFormat:@"select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where  DL.Location_ID='%@'  and  D.Doctor_ID NOT IN ( Select X.Doctor_ID from (SELECT A.Doctor_ID,count(A.Doctor_ID) AS Visits_Completed,IFNULL(D.Custom_Attribute_5,C.Custom_Attribute_1) AS Visits_Required   FROM (Select * from PHX_TBL_Actual_Visits WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') and Doctor_ID  NOT NULL GROUP by Doctor_ID|| Planned_Visit_ID) AS A Inner JOIN PHX_TBL_Doctors AS D  ON A.Doctor_ID =D.Doctor_ID inner join TBL_App_Codes AS C ON  D.Classification_2=C.Code_Value WHERE  C.Code_Type='CLASS'  group by  A.Doctor_ID order by  A.Visit_End_Date desc) AS X where  Visits_Completed >= CAST(Visits_Required AS INTEGER))  and D.Is_Approved = 'Y' order by Doctor_Name  COLLATE NOCASE ASC",locationID];

    }else{
        doctorLocationQry=[NSString stringWithFormat:@"select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where  DL.Location_ID='%@'  and  D.Doctor_ID NOT IN ( Select X.Doctor_ID from (SELECT A.Doctor_ID,count(A.Doctor_ID) AS Visits_Completed,IFNULL(D.Custom_Attribute_5,C.Custom_Attribute_1) AS Visits_Required   FROM (Select * from PHX_TBL_Actual_Visits WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') and Doctor_ID  NOT NULL GROUP by Doctor_ID|| Planned_Visit_ID) AS A Inner JOIN PHX_TBL_Doctors AS D  ON A.Doctor_ID =D.Doctor_ID inner join TBL_App_Codes AS C ON  D.Classification_2=C.Code_Value WHERE  C.Code_Type='CLASS'  group by  A.Doctor_ID order by  A.Visit_End_Date desc) AS X where  Visits_Completed >= CAST(Visits_Required AS INTEGER))  order by Doctor_Name  COLLATE NOCASE ASC",locationID];
    }*/
    
    doctorLocationQry=[NSString stringWithFormat:@"select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where  DL.Location_ID='%@'  and  D.Doctor_ID NOT IN ( Select X.Doctor_ID from (SELECT A.Doctor_ID,count(A.Doctor_ID) AS Visits_Completed,IFNULL(D.Custom_Attribute_5,C.Custom_Attribute_1) AS Visits_Required   FROM (Select * from PHX_TBL_Actual_Visits WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') and Doctor_ID  NOT NULL GROUP by Doctor_ID|| Planned_Visit_ID) AS A Inner JOIN PHX_TBL_Doctors AS D  ON A.Doctor_ID =D.Doctor_ID inner join TBL_App_Codes AS C ON  D.Classification_2=C.Code_Value WHERE  C.Code_Type='CLASS'  group by  A.Doctor_ID order by  A.Visit_End_Date desc) AS X where  Visits_Completed >= CAST(Visits_Required AS INTEGER))  order by Doctor_Name  COLLATE NOCASE ASC",locationID];

    
    NSLog(@"pending visit query %@", doctorLocationQry);
    
    NSMutableArray* demoLocationArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorLocationQry];
    if (demoLocationArray.count>0)
    {
        return demoLocationArray;
    }
    else
    {
        return nil;
    }
}

#pragma mark Pharmacies Data Methods


+(NSMutableArray*)fetchPharmacieswithContacts

{
        NSString* locationsQuery=[NSString stringWithFormat:@"select IFNULL(C.Contact_Name,'N/A')AS Contact_Name,IFNULL(A.Location_ID,'N/A') AS Location_ID,IFNULL(Latitude,'N/A')AS Latitude,IFNULL(Longitude,'N/A')AS Longitude,IFNULL(C.Phone,'N/A')AS Phone,IFNULL(A.Location_Name,'N/A') AS Location_name from PHX_TBL_Locations AS A inner join PHX_TBL_Location_Contact_Map as B on A.Location_ID=B.Location_ID inner join PHX_TBL_Contacts AS C on B.Contact_ID=C.Contact_ID where A.Location_Type='P'  "];
    
    
    
    NSMutableArray* locationsData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationsQuery];
    
  //  NSLog(@"pharmacies data with locations is %@", locationsData);
    
    if (locationsData.count>0) {
        
        return locationsData;
    }
    else
    {
        return nil;
        
    }

}

+(NSMutableArray*)fetchPharmaciesData

{
    NSString* locationsQuery=[NSString stringWithFormat:@"select IFNULL(Location_ID,0) AS Location_ID, IFNULL(Location_Name,'N/A') AS Location_Name, IFNULL(Location_Type,0) AS Location_Type, IFNULL(Location_Code , 'N/A') AS Location_Code, IFNULL(Name_2,0) AS Name_2, IFNULL(Name_3,0) AS Name_3, IFNULL(Address_1,'') AS Address_1,IFNULL(Address_2,'') AS Address_2,IFNULL(Address_3,'') AS Address_3,IFNULL(City,'N/A') AS City,IFNULL(State,'N/A') AS State, IFNULL(Email,'N/A') AS Email, IFNULL(Phone,'N/A') AS Phone, IFNULL(Postal_Code,0) AS Postal_Code, IFNULL(Latitude,0) AS Latitude, IFNULL(Longitude,0) AS Longitude, IFNULL(Classification_2,'N/A') AS Class, IFNULL(Classification_3,'N/A') AS OTCRX, IFNULL(Custom_Attribute_1,'N/A') AS TradeChannel, Case  when IFNULL(Is_Active,'N/A')  ='Y' then 'Active' when IFNULL(Is_Active,'N/A')  ='N' then 'InActive' END AS Is_Active from PHX_TBL_Locations where Location_Type ='P' order by Location_Name ASC"];
    
    //query updated to fetch contacts as well
    
//    NSString* locationsQuery=[NSString stringWithFormat:@"select IFNULL(A.Location_ID,0) AS Location_ID, IFNULL(A.Location_Name,'N/A') AS Location_Name, IFNULL(A.Location_Type,'N/A') AS Location_Type, IFNULL(A.Location_Code,'N/A') AS Location_Code,IFNULL(A.Address_1,'N/A') AS Address_1, IFNULL(A.City,'N/A') AS City, IFNULL(A.State,'N/A') AS State, IFNULL(A.Email,'N/A') AS Email, IFNULL(A.Postal_Code,0) AS Postal_Code, IFNULL(A.Latitude,0)AS Latitude, IFNULL(A.Longitude,0) AS Longitude, IFNULL(A.Classification_2,'N/A')AS Class, IFNULL(A.Classification_3,'N/A')AS OTCRX, Case  when IFNULL(A.Is_Active,'N/A')  ='Y' then 'Active' when IFNULL(A.Is_Active,'N/A')  ='N' then 'InActive' END AS Is_Active, IFNULL(A.Is_Deleted,0) AS Is_Deleted ,B.*,IFNULL(C.Contact_Name,'N/A')AS Contact_Name from PHX_TBL_Locations AS A inner join PHX_TBL_Location_Contact_Map as B on A.Location_ID=B.Location_ID inner join PHX_TBL_Contacts AS C on B.Contact_ID=C.Contact_ID where A.Location_Type='P' order by Location_Name ASC"];
    
    
    
    NSMutableArray* locationsData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationsQuery];
    
   // NSLog(@"pharmacies data with locations is %@", locationsData);
    
    if (locationsData.count>0) {
        
        return locationsData;
    }
    else
    {
        return nil;
        
    }
    
    
    
}

+(NSMutableArray*)fetchContactDetaislwithLocationID:(NSString*)locationID

{
    NSString* contaxtDetaislQry=[NSString stringWithFormat:@"select IFNULL(A.Contact_Name,'N/A')As Contact_Name,IFNULL(A.Contact_ID,'N/A')As Contact_ID,IFNULL(Email,'N/A') AS Email ,IFNULL(A.City,'N/A') AS City ,IFNULL(A.Phone,'N/A') AS Phone  from PHX_TBL_Contacts AS A inner join PHX_TBL_Location_Contact_Map AS B on  A.Contact_ID = B.Contact_ID where B.Location_ID='%@'",locationID];
    
    
    NSMutableArray* contactDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:contaxtDetaislQry];
    
    if (contactDetailsArray.count>0) {
        
        return [contactDetailsArray objectAtIndex:0];
    }
    else
    {
        return nil;
        
    }

    

}

+(NSMutableArray*)fetchLocationsforPharmacy:(NSString*)pharmacyLocationID
{
    NSString* pharmacyLocationsQry=[NSString stringWithFormat:@"select IFNULL(Location_ID,0) AS Location_ID, IFNULL(Location_Name,'N/A') AS Location_Name, IFNULL(Location_Type,'N/A') AS Location_Type, IFNULL(Location_Code,'N/A') AS Location_Code,IFNULL(Address_1,'N/A') AS Address_1, IFNULL(City,'N/A') AS City, IFNULL(State,'N/A') AS State, IFNULL(Email,'N/A') AS Email, IFNULL(Postal_Code,0) AS Postal_Code, IFNULL(Latitude,0)AS Latitude, IFNULL(Longitude,0) AS Longitude, IFNULL(Classification_2,'N/A')AS Class, IFNULL(Classification_3,'N/A')AS OTCRX, Case  when IFNULL(Is_Active,'N/A')  ='Y' then 'Active' when IFNULL(Is_Active,'N/A')  ='N' then 'InActive' END AS Is_Active, IFNULL(Is_Deleted,0) AS Is_Deleted from PHX_TBL_Locations where Location_ID='%@'", pharmacyLocationID];
    
    
    NSMutableArray* pharmacyLocationsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyLocationsQry];
    
    if (pharmacyLocationsArray.count>0) {
        
        return pharmacyLocationsArray;
    }
    else
    {
        return nil;
        
    }
    
    
}


+(NSMutableArray *)fetchProductswithStock

{
    NSString* tempProductsQry=[NSString stringWithFormat:@"select IFNULL(P.Product_ID,'N/A') AS Product_ID, IFNULL(P.Product_Name,'N/A') AS Product_Name, IFNULL(P.Product_Type,'N/A') AS Product_Type,IFNULL(P.Product_Code,'N/A') AS Product_Code, IFNULL(P.Primary_UOM,'N/A') AS Primary_UOM,IFNULL(P.Barcode,'0') AS Barcode,IFNULL(P.Brand_Code,'N/A') AS Brand_Code, IFNULL(P.Agency,'N/A') AS Agency, IFNULL(P.List_price,'0') AS List_Price, IFNULL(P.Net_price,'0') AS Net_Price,IFNULL(P.Cost_price,'0') AS Cost_Price, IFNULL(P.item_Metadata,'N/A') AS item_Metadata, CASE when IFNULL(P.Is_Active,'N/A') = 'Y' then 'Active' when IFNULL(P.Is_Active,'N/A') = 'N' then 'InActive' END AS Status ,IFNULL(S.Product_ID,'0') AS Product_ID,IFNULL(S.Emp_ID,'0') AS Emp_ID ,IFNULL(S.Lot_No,'N/A') AS Lot_No ,IFNULL(S.Expiry_Date,'0') AS Expiry_Date, IFNULL(S.Lot_Qty,'0') AS Lot_Qty, IFNULL(S.Used_Qty,'0') AS Used_Qty from PHX_TBL_EMP_Stock  AS S inner join  PHX_TBL_Products AS P where  P.Product_ID=S.Product_ID and Expiry_Date > '%@'  and EMP_ID='%@'",[self fetchDatabaseDateFormat],[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];
    
    
    
    NSString* querywithUpdatedLotQty=[NSString stringWithFormat:@"select IFNULL(Qty,'0') AS QTY ,EMP_ID, IFNULL(Product_ID,'N/A') AS Product_ID, IFNULL(Product_Name,'N/A') AS Product_Name, IFNULL(Product_Type,'N/A') AS Product_Type,IFNULL(Product_Code,'N/A') AS Product_Code, IFNULL(Primary_UOM,'N/A') AS Primary_UOM,IFNULL(Barcode,'0') AS Barcode,IFNULL(Brand_Code,'N/A') AS Brand_Code, IFNULL(Agency,'N/A') AS Agency, IFNULL(List_price,'0') AS List_Price, IFNULL(Net_price,'0') AS Net_Price,IFNULL(Cost_price,'0') AS Cost_Price, IFNULL(item_Metadata,'') AS item_Metadata, CASE when IFNULL(Is_Active,'N/A') = 'Y' then 'Active' when IFNULL(Is_Active,'N/A') = 'N' then 'InActive' END AS Status from (SELECT B.Qty,B.EMP_ID,PHX_TBL_Products.* FROM PHX_TBL_Products LEFT JOIN ((select Lot_Qty AS Qty,Product_ID,EMP_ID FROM PHX_TBL_EMP_Stock where Expiry_Date>'%@'  group by Product_ID )) AS B ON PHX_TBL_Products.Product_ID=B.Product_ID)  ",[self fetchDatabaseDateFormat]];
    
    
    NSLog(@"products query is %@", querywithUpdatedLotQty);
    
    
    NSMutableArray* productsDataArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:querywithUpdatedLotQty];
    
    if (productsDataArray.count>0) {
        
       // NSLog(@"products with stock %@", [productsDataArray description]);
        
        return productsDataArray;
    }
    
    else
    {
        return nil;
    }

    
}

+(NSMutableArray*)fetchProductsData
{
    NSString* productsDataQry=[NSString stringWithFormat:@"select IFNULL(Product_ID,'N/A') AS Product_ID, IFNULL(Product_Name,'N/A') AS Product_Name, IFNULL(Product_Type,'N/A') AS Product_Type,IFNULL(Product_Code,'N/A') AS Product_Code, IFNULL(Primary_UOM,'N/A') AS Primary_UOM,IFNULL(Barcode,'0') AS Barcode,IFNULL(Brand_Code,'N/A') AS Brand_Code, IFNULL(Agency,'N/A') AS Agency, IFNULL(List_price,'0') AS List_Price, IFNULL(Net_price,'0') AS Net_Price,IFNULL(Cost_price,'0') AS Cost_Price, IFNULL(item_Metadata,'') AS item_Metadata, CASE when IFNULL(Is_Active,'N/A') = 'Y' then 'Active' when IFNULL(Is_Active,'N/A') = 'N' then 'InActive' END AS Status from PHX_TBL_Products"];
    
    
    
    
    
    NSMutableArray* productsDataArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productsDataQry];
    
    if (productsDataArray.count>0) {
        
        
        return productsDataArray;
    }
    
    else
    {
        return nil;
    }
    
}


+(BOOL)insertDoctors:(NSMutableDictionary*)doctorsData

{
    
    NSString *documentDir;
    
    
    
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        
        
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    
    [database open];
    
    //NSString *insertQuestions=[[ NSString alloc]initWithFormat: @"INSERT INTO TBL_Doctors (Custom_Attribute_1, Classification_3,Classification_1,Classification_2)  VALUES (?,?,?,?)"];
    // FMDatabase *database=[[FMDatabase alloc]init];
    
    NSArray *argumentArray=[[NSArray alloc]initWithObjects:
                            
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"TradeChannel"]],
                            [NSString stringWithFormat:@"%@", [doctorsData valueForKey:@"OTCRX"]],
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"Specialization"]],
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"Class"]],
                            
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"DoctorName"]],
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"Email"]],
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"Telephone"]],
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"MOHID"]],
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"VisitRequired"]],
                            [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"City"]],
                            nil];
    
    
   //NSLog(@"argument array is %@", [argumentArray description]);
    
    BOOL mohIdAvbl;
    
    if (![[SWDefaults getValidStringValue:[doctorsData valueForKey:@"MOHID"]] isEqualToString:@""]) {
        mohIdAvbl=YES;
    }
    else
    {
        mohIdAvbl=NO;
    }
    
    
   // NSLog(@"%@",insertQuestions);
    
    NSString *doctorID = [NSString stringWithFormat:@"%@",[doctorsData valueForKey:@"Doctor_ID"]];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *dateTimeString =  [formatterTime stringFromDate:[NSDate date]];
    NSString* userName=[[SWDefaults userProfile] stringForKey:@"Username"];
    
    if (userName) {
        
        
    }
    NSString* userId=[[SWDefaults userProfile] stringForKey:@"User_ID"];

    
    BOOL success = NO;
    @try {
        
        NSString* isApproved = KEmptyString;
        //if app control flag FM_ENABLE_DR_APPROVAL is Y then is_Approved is N
        NSString* doctorApproval= [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_DR_APPROVAL];
        if ([doctorApproval isEqualToString:KAppControlsYESCode])
        {
            isApproved = KAppControlsNOCode;
            if (mohIdAvbl==YES) {
                
                success =  [database executeUpdate:@"INSERT INTO PHX_TBL_Doctors (Doctor_ID,Custom_Attribute_1, Classification_3,Classification_1,Classification_2,Doctor_Name,IS_Active,Is_Deleted,Created_By,Created_At,Email,Phone,Custom_Attribute_2,Custom_Attribute_3,Is_Approved,Custom_Attribute_5,City)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",doctorID,[argumentArray objectAtIndex:0],[argumentArray objectAtIndex:1],[argumentArray objectAtIndex:2],[argumentArray objectAtIndex:3],[argumentArray objectAtIndex:4],@"Y",@"N",userId,dateTimeString,[argumentArray objectAtIndex:5],[argumentArray objectAtIndex:6],[argumentArray objectAtIndex:7],@"IPAD",[NSString isEmpty:isApproved]?[NSNull null]:isApproved,[argumentArray objectAtIndex:8], [NSString isEmpty:[argumentArray objectAtIndex:9]]?[NSNull null]:[argumentArray objectAtIndex:9], nil];
                
            }
            else
            {
                success =  [database executeUpdate:@"INSERT INTO PHX_TBL_Doctors (Doctor_ID,Custom_Attribute_1, Classification_3,Classification_1,Classification_2,Doctor_Name,IS_Active,Is_Deleted,Created_By,Created_At,Email,Phone,Custom_Attribute_3,Is_Approved,Custom_Attribute_5,City)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",doctorID,[argumentArray objectAtIndex:0],[argumentArray objectAtIndex:1],[argumentArray objectAtIndex:2],[argumentArray objectAtIndex:3],[argumentArray objectAtIndex:4],@"Y",@"N",userId,dateTimeString,[argumentArray objectAtIndex:5],[argumentArray objectAtIndex:6],@"IPAD",[NSString isEmpty:isApproved]?[NSNull null]:isApproved,[argumentArray objectAtIndex:8], [NSString isEmpty:[argumentArray objectAtIndex:9]]?[NSNull null]:[argumentArray objectAtIndex:9], nil];
                
            }
        }
        else{
            if (mohIdAvbl==YES) {
                
                success =  [database executeUpdate:@"INSERT INTO PHX_TBL_Doctors (Doctor_ID,Custom_Attribute_1, Classification_3,Classification_1,Classification_2,Doctor_Name,IS_Active,Is_Deleted,Created_By,Created_At,Email,Phone,Custom_Attribute_2,Custom_Attribute_3,Custom_Attribute_5,City)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",doctorID,[argumentArray objectAtIndex:0],[argumentArray objectAtIndex:1],[argumentArray objectAtIndex:2],[argumentArray objectAtIndex:3],[argumentArray objectAtIndex:4],@"Y",@"N",userId,dateTimeString,[argumentArray objectAtIndex:5],[argumentArray objectAtIndex:6],[argumentArray objectAtIndex:7],@"IPAD",[argumentArray objectAtIndex:8], [NSString isEmpty:[argumentArray objectAtIndex:9]]?[NSNull null]:[argumentArray objectAtIndex:9], nil];
                
            }
            
            else
            {
                success =  [database executeUpdate:@"INSERT INTO PHX_TBL_Doctors (Doctor_ID,Custom_Attribute_1, Classification_3,Classification_1,Classification_2,Doctor_Name,IS_Active,Is_Deleted,Created_By,Created_At,Email,Phone,Custom_Attribute_3,Custom_Attribute_5,City)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",doctorID,[argumentArray objectAtIndex:0],[argumentArray objectAtIndex:1],[argumentArray objectAtIndex:2],[argumentArray objectAtIndex:3],[argumentArray objectAtIndex:4],@"Y",@"N",userId,dateTimeString,[argumentArray objectAtIndex:5],[argumentArray objectAtIndex:6],@"IPAD",[argumentArray objectAtIndex:8], [NSString isEmpty:[argumentArray objectAtIndex:9]]?[NSNull null]:[argumentArray objectAtIndex:9], nil];
                
            }
        }
        
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    return success;
    
    
}

+(BOOL)insertDoctorLocationMapwithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID
{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    
    
    
    BOOL success = NO;
    @try {
        
        success =  [database executeUpdate:@"INSERT OR REPLACE INTO PHX_TBL_Doctor_Location_Map (Doctor_ID,Location_ID, Created_By,Created_At)  VALUES (?,?,?,?)",doctorID,locationID,createdBy,createdAt, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    return success;
    
    
    
    
}


+(BOOL)insertPharmacyLocationMap:(NSString*)location_ID withContactID:(NSString*)Contact_ID
{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    

    BOOL success = NO;
    @try {
        
        
        success=[database executeUpdate:@"INSERT OR REPLACE INTO PHX_TBL_Location_Contact_Map (Location_ID,Contact_ID,Created_At,Created_By) VALUES (?,?,?,?)",location_ID,Contact_ID,createdAt,createdBy,nil];
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    return success;
    
}

-(NSString*)docsDir
{
    NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
    
    return [filePath path];
    
}

+(BOOL)isProductActive:(NSString*)productID
{
    NSString* productQry=[NSString stringWithFormat:@"SELECT IFNULL(IS_Active,'N/A') AS Is_Active FROM PHX_TBL_Products where Product_ID='%@'  and IS_Deleted='N'", productID];
    
    NSMutableArray* productsDataArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productQry];
    
    
    if (productsDataArray.count>0) {
        
        if ([[[productsDataArray objectAtIndex:0]valueForKey:@"Is_Active"] isEqualToString:@"Y"]) {
            
            return YES;
        }
        else
        {
            return NO;
        }
        
    }
    
    else
    {
        return NO;
    }
    
}

+(NSMutableArray*)fetchMediaFilesforProduct:(NSString*)productID
{
    
    
    
    
    //    NSString* productQry=[NSString stringWithFormat:@"select IFNULL(Media_Type,'N/A') AS Media_Type, IFNULL(Filename,'N/A') AS File_Name from TBL_Media_Files where Entity_ID_1 ='%@' and is_Deleted='N' and  Download_Flag ='Y'", productID];
    
    
    
//    NSString* productQry=[NSString stringWithFormat:@"select IFNULL(Media_Type,'N/A') AS Media_Type,IFNULL(Caption,'N/A') AS Caption, IFNULL(Filename,'N/A') AS File_Name ,IFNULL(Media_File_ID,'0') AS Media_File_ID from TBL_Media_Files where Entity_ID_1 ='%@' and is_Deleted='N' and  Download_Flag ='N'", productID];
    
    
    NSString *productQry=[NSString stringWithFormat:@"select DISTINCT IFNULL(B.Product_Name ,'N/A') AS Product_Name , IFNULL(B.Product_ID ,'N/A')AS Product_ID,IFNULL(B.Product_Code ,'N/A')AS Product_Code, IFNULL(C.Detailed_Info,'N/A') AS Detailed_Info,IFNULL(A.Media_File_ID,'N/A') AS Media_File_ID ,IFNULL(Media_Type,'N/A') AS Media_Type,IFNULL(Caption,'N/A') AS Caption, IFNULL(Filename,'N/A') AS File_Name from TBL_Media_Files AS A LEFT JOIN PHX_TBL_Products AS B On CAST(A.Entity_ID_1 as nvarchar(100)) =CAST(B.Product_ID as nvarchar(100)) LEFT JOIN PHX_TBL_Product_Details AS C ON B.Product_ID =C.Product_ID where B.Product_ID='%@' and B.is_Deleted='N' and  A.Download_Flag ='N' Order by Caption ASC",productID];
    
    
    
    NSMutableArray* productMediaFiles=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productQry];
    if (productMediaFiles.count>0) {
        
       // NSLog(@"media files for product is %@", [productMediaFiles description]);
        return productMediaFiles;
    }
    
    else
    {
        return 0;
    }
    
    
}


+(NSString*)fetchDetailedInfoforProduct:(NSString*)productID
{
    NSString* productDesc=[NSString stringWithFormat:@"select IFNULL(Detailed_Info,'N/A') AS Detailed_Info from PHX_TBL_Product_Details where Product_ID='%@'", productID];
    
    NSMutableArray* productDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productDesc];
    
    if (productDetailsArray.count>0) {
        
        return [[productDetailsArray objectAtIndex:0]valueForKey:@"Detailed_Info"];
    }
    
    else
    {
        return nil;
        
}
}

+(NSMutableArray*)fetchProductDetaislwithMediaFileID:(NSString*)mediaFileID

{
    NSString* productDetailsQry=[NSString stringWithFormat:@"select DISTINCT IFNULL(B.Product_Name ,'N/A') AS Product_Name , IFNULL(B.Product_ID ,'N/A')AS Product_ID,IFNULL(B.Product_Code ,'N/A')AS Product_Code, IFNULL(C.Detailed_Info,'N/A') AS Detailed_Info,IFNULL(A.Media_File_ID,'N/A') AS Media_File_ID from TBL_Media_Files AS A LEFT JOIN PHX_TBL_Products AS B On CAST(A.Entity_ID_1 as nvarchar(100)) =CAST(B.Product_ID as nvarchar(100)) LEFT JOIN PHX_TBL_Product_Details AS C ON B.Product_ID =C.Product_ID WHERE A.Media_File_ID ='%@'", mediaFileID];
    
                                 
    NSMutableArray* productDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productDetailsQry];
    
    if (productDetailsArray.count>0) {
        
        return [productDetailsArray objectAtIndex:0];
    }
    
    else
    {
        return 0;
    }
    
                                 
                                 
                                 
    

}
+(NSMutableArray*)fetchProductDetails:(NSString*)productID

{
    NSString* productDetailsQuery =[NSString stringWithFormat:@"select IFNULL(Brand_Code,'N/A') AS Brand_Code , IFNULL(Agency,'N/A') AS Agency from PHX_TBL_Products where Product_ID='%@'", productID];
    NSMutableArray* productDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productDetailsQuery];
    
    if (productDetailsArray.count>0) {
        
        return productDetailsArray;
    }
    
    else
    {
        return 0;
    }
    
}


#pragma mark Tasks Methods

+(BOOL)insertTask:(NSMutableDictionary*)taskData
{
    
    
    NSLog(@"task data is %@", [taskData description]);
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    
    NSString *taskID;
    NSString *createdAt;
    
    if ([taskData valueForKey:@"Task_ID"]!=nil) {
        //for saved tasks taskid already exists
        taskID=[taskData valueForKey:@"Task_ID"];
    }
    
    else
    {
        taskID = [NSString createGuid];
    }
    
    
    if ([taskData valueForKey:@"Date"]!=nil) {
        
        createdAt=[taskData valueForKey:@"Date"];
        
        
        
        /*
        NSDateFormatter *datePickerDateFormat = [NSDateFormatter new] ;
        NSLocale *usLocaleCurrent = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [datePickerDateFormat setDateFormat:@"yyyy-MM-dd"];
        [datePickerDateFormat setLocale:usLocaleCurrent];
        NSDate* displayDate=[datePickerDateFormat dateFromString:createdAt];
        
        
        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString  *dateTobeSaved =  [formatterTime stringFromDate:displayDate];
        
        createdAt=dateTobeSaved;*/
        
        NSLog(@"format of date chnaged %@", createdAt);
        
        
        
        
    }
    
    else
    {
        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        createdAt =  [formatterTime stringFromDate:[NSDate date]];
    }
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    //it is unique identifier while full sync its giving error, so use a unique identifier for now later on to changed after discussing with harpal. /* changed backend aiigned to is no more a Uinque identifier */
    
    //TO BE DONE
    
    
    NSString* assignedTo=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    
    NSString* userID=[[SWDefaults userProfile]valueForKey:@"User_ID"];
    
    //NSString* assignedTo=[NSString createGuid];
   
    
    //check for doctor and contact id's
    
    NSString* doctorID=[taskData valueForKey:@"Doctor_ID"];
    
    NSString* contactID=[taskData valueForKey:@"Contact_ID"];
    
    if ([doctorID isEqual:[NSNull null]]) {
        
    }
    
    else if ([doctorID isEqualToString:@"0"])
        
    {
        [taskData setObject:[NSNull null] forKey:@"Doctor_ID"];

    }
    
    if ([contactID isEqual:[NSNull null]]) {
        
    }
    
    else if ([contactID isEqualToString:@"0"])
        
    {
        [taskData setObject:[NSNull null] forKey:@"Contact_ID"];
        
    }
    
    doctorID=[taskData valueForKey:@"Doctor_ID"];
    
    contactID=[taskData valueForKey:@"Contact_ID"];
    
    
    NSLog(@"task data before saving %@", [taskData description]);
    
    NSString* taskCloseTime=[taskData valueForKey:@"Task_Closed_Time"];
    
    
    
        NSMutableDictionary * taskDict=[[NSMutableDictionary alloc]init];
        [taskDict setValue:taskID forKey:@"Task_ID"];
        [taskDict setValue:[taskData valueForKey:@"Status"] forKey:@"Status"];
    
    NSMutableArray* taskDetailsArray=[[NSMutableArray alloc]init];
    
    taskDetailsArray=[[[NSUserDefaults standardUserDefaults]objectForKey:@"Updated_Tasks"] mutableCopy];
    
    if (taskDetailsArray.count>0) {
        
        [taskDetailsArray addObject:taskDict];

    }
    else
    {
        taskDetailsArray=[[NSMutableArray alloc]init];
        [taskDetailsArray addObject:taskDict];

    }
    
    
   
     [[NSUserDefaults standardUserDefaults]setObject:taskDetailsArray forKey:@"Updated_Tasks"];
    
    NSLog(@"task array after inserting %@", [taskDetailsArray description]);
    
    
//    
//    NSMutableArray* taskIdArray=[[NSMutableArray alloc]init];
//    
//    [taskIdArray addObject:taskID];
//    
//    NSMutableDictionary * taskDict=[[NSMutableDictionary alloc]init];
//    [taskDict setValue:taskID forKey:@"Task_ID"];
//    [taskDict setValue:[taskData valueForKey:@"Status"] forKey:@"Status"];
//    
//    
//    [[NSUserDefaults standardUserDefaults]setObject:taskDict forKey:@"Updated_Tasks"];
    
        
    BOOL success = NO;
    @try {
        
        success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Tasks (Task_ID,Title, Description,Category,Priority,Start_Time,Status,Assigned_To,Emp_ID,Location_ID,Show_Reminder,IS_Active,Is_deleted,Created_At,Created_By,Doctor_ID,Contact_ID,End_Time,Last_Updated_At,Last_Updated_By,Custom_Attribute_1)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",taskID, [taskData valueForKey:@"Title"]==nil?@"":[taskData valueForKey:@"Title"],[taskData valueForKey:@"Description"]==nil?@"":[taskData valueForKey:@"Description"],[taskData valueForKey:@"Category"]==nil?@"":[taskData valueForKey:@"Category"],[taskData valueForKey:@"Priority"]==nil?@"":[taskData valueForKey:@"Priority"], createdAt,[taskData valueForKey:@"Status"]==nil?@"":[taskData valueForKey:@"Status"],assignedTo,assignedTo,[taskData valueForKey:@"Location_ID"]==nil?@"":[taskData valueForKey:@"Location_ID"],@"N",@"Y",@"N",createdAt,createdBy,doctorID,contactID,taskCloseTime,createdAt,userID,@"IPAD",nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    return success;
    
    
    
}

+(NSString*)fetchDocumentsDirectory
{
    NSString *documentDir;
    
    
    
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        
        
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    return documentDir;
}



+(NSMutableArray*)fetchCodeDescriptionfromAppCodes:(NSString*)codeType

{
    
    NSString* taskQry=[NSString stringWithFormat:@"select Code_Description from TBL_App_Codes where Code_Type='%@'", codeType];
    NSMutableArray*taskArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskQry];
    
    NSMutableArray* refinedCodesArray=[[NSMutableArray alloc]init];
    
    for (NSInteger i=0; i<taskArray.count; i++) {
        [refinedCodesArray addObject:[[taskArray valueForKey:@"Code_Description"] objectAtIndex:i]];
        
    }
    NSLog(@"app codes for task %@", [refinedCodesArray description]);
    return refinedCodesArray;
    
}

+(NSMutableArray*)fetchTaskAppCodes:(NSString*)code
{
    NSString* taskQry=[NSString stringWithFormat:@"select Code_Value from TBL_App_Codes where Code_Type='%@'", code];
    NSMutableArray*taskArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskQry];
    
    NSMutableArray* refinedCodesArray=[[NSMutableArray alloc]init];
    
    for (NSInteger i=0; i<taskArray.count; i++) {
        [refinedCodesArray addObject:[[taskArray valueForKey:@"Code_Value"] objectAtIndex:i]];
        
    }
    NSLog(@"app codes for task %@", [refinedCodesArray description]);
    return refinedCodesArray;
}

+(NSMutableArray*)fetchTaskswithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID

{
    NSString* taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where Location_ID='%@' and Doctor_ID= '%@' and  Is_Active='Y' and Is_Deleted='N' ",locationID,doctorID];
    
    
   // NSLog(@"quert for fetching tasks %@", taskdetailsQry);
    
    
    NSMutableArray*tasksArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    if (tasksArray.count>0) {
        
        
        //refine the date here itself to display only year month and time
        
        for (NSInteger i=0; i<tasksArray.count; i++) {
            
            NSString* responseDateStr=[[tasksArray valueForKey:@"Created_At"] objectAtIndex:i];
            
            NSLog(@"date from response %@", responseDateStr);
            
            
            
            
            //            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
            //            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            //            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            //            [formatterTime setLocale:usLocaleq];
            //            NSDate  *responseTempDate =  [formatterTime dateFromString:responseDateStr];
            //
            //
            //
            //            NSDateFormatter *datePickerDateFormat = [NSDateFormatter new] ;
            //            NSLocale *usLocaleCurrent = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            //            [datePickerDateFormat setDateFormat:@"yyyy-MM-dd"];
            //            [datePickerDateFormat setLocale:usLocaleCurrent];
            //
            //            NSString * savedTaskDate=[datePickerDateFormat stringFromDate:responseTempDate];
            //            NSDate* displayDate=[datePickerDateFormat dateFromString:savedTaskDate];
            //
            //
            //
            
            
            
            
            
            
            
            //            NSString* finalStr=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:responseDateStr];
            //
            //            NSLog(@"final str is %@", finalStr);
            
            
            
            
        }
        
        return tasksArray;
    }
    
    else
    {
        return nil;
    }
    

    
}





+(NSMutableArray*)fetchTaskswithLocationID:(NSString*)locationID andcontactID:(NSString*)contactID
{
    //NSString* taskQry=[NSString stringWithFormat:@"select * from PHX_TBL_Tasks"];
    
    NSString* taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where Location_ID='%@' and Contact_ID= '%@' and  Is_Active='Y' and Is_Deleted='N' ",locationID,contactID];
    
    
   // NSLog(@"quert for fetching tasks %@", taskdetailsQry);
    
    
    NSMutableArray*tasksArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
   
    if (tasksArray.count>0) {
        
        
        //refine the date here itself to display only year month and time
        
        for (NSInteger i=0; i<tasksArray.count; i++) {
            
            NSString* responseDateStr=[[tasksArray valueForKey:@"Created_At"] objectAtIndex:i];
            
            NSLog(@"date from response %@", responseDateStr);
            
            
                     
            
            
        }
        
        return tasksArray;
    }
    
    else
    {
        return nil;
    }
    
}


#pragma mark Location methods

+(NSMutableArray*)fetchLocationDetailsforDoctorID:(NSString*)doctorID
{
    
    
    NSString* locationsQry= [NSString stringWithFormat:@"select IFNULL(A.Location_ID,'N/A') AS Location_ID,IFNULL(B.Location_Name,'N/A') AS Location_Name,IFNULL(B.Location_Type,'N/A') AS Location_Type from PHX_TBL_Doctor_location_map As A inner join  PHX_TBL_locations as b  where b.location_ID=A.Location_ID and A.Doctor_ID='%@'",doctorID];
    
    NSMutableArray* locationsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationsQry];
    
    if (locationsArray.count>0) {
        
        return locationsArray;
    }
    
    else{
        return nil;
    }
}



+(NSMutableArray*)fetchLocations{
    AppControl *appcontrol=[AppControl retrieveSingleton];
    NSString* locationsQry=[NSString stringWithFormat:@"select IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Location_Name,'N/A') AS Location_Name ,IFNULL(Location_Type,'N/A') AS Location_Type from PHX_TBL_Locations  Where Location_ID!='%@' order by Location_name ASC",appcontrol.MEDREP_NO_VISIT_LOCATION];
    NSMutableArray* locationsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationsQry];
    
    if (locationsArray.count>0) {
        
        return locationsArray;
    }
    
    else{
        return nil;
    }
    
}


#pragma mark Demo Plan Methods


+(NSMutableArray*)fetchMediaFileCountforDemoPlanID:(NSString*)demoPlanID

{
    NSString* mediaFileCountQry=[NSString stringWithFormat:@"select A.Media_type from TBL_Media_Files AS A left join  PHX_TBL_DEMO_PLAN_MEDIA AS B where B.Media_File_ID=A.Media_File_ID and Demo_plan_ID='%@'",demoPlanID];
    
    NSMutableArray* mediaCount=[[SWDatabaseManager retrieveManager]fetchDataForQuery:mediaFileCountQry];
    
    
    
    
            
            
      
    if (mediaCount.count>0) {
        
        
        
        
        return mediaCount ;
    }
    
    else
    {
        return nil;
    }
    
    
}



+(NSMutableArray*)fetchMediaFileswithDemoPlanID:(NSString*)demoPlanID

{
   // NSString* mediaQry=[NSString stringWithFormat:@"select  IFNULL(C.Product_Code,'N/A') AS Product_Code, IFNULL(A.Media_File_ID,'N/A') AS Media_File_ID ,IFNULL(C.Product_Name,'N/A') AS Product_Name, IFNULL(A.Caption,'N/A') AS Caption , IFNULL(A.Filename,'N/A') AS File_Name,IFNULL(A.Media_Type,'N/A') AS Media_Type,IFNULL(A.Entity_ID_1,'N/A') AS Product_ID,IFNULL(D.Detailed_Info,'N/A') AS Detailed_Info from TBL_Media_Files AS A  left join  PHX_TBL_DEMO_PLAN_MEDIA AS B ON B.Media_File_ID=A.Media_File_ID left join PHX_TBL_Products AS C ON  C.Product_ID=A.Entity_ID_1 left join PHX_TBL_Product_Details AS D ON C.Product_ID=D.Product_ID WHERE A.IS_Deleted ='N' and A.Download_Flag='N' and B.Demo_plan_ID='%@' and  date('now') between Valid_From and Valid_To  ",demoPlanID];
    
    
    NSString * mediaQry=[NSString stringWithFormat:@"select  IFNULL(C.Product_Code,'N/A') AS Product_Code, IFNULL(A.Media_File_ID,'N/A') AS Media_File_ID ,IFNULL(C.Product_Name,'N/A') AS Product_Name, IFNULL(A.Caption,'N/A') AS Caption , IFNULL(A.Filename,'N/A') AS File_Name,IFNULL(A.Media_Type,'N/A') AS Media_Type,IFNULL(A.Entity_ID_1,'N/A') AS Product_ID,IFNULL(D.Detailed_Info,'N/A') AS Detailed_Info from TBL_Media_Files AS A  left join  PHX_TBL_DEMO_PLAN_MEDIA AS B ON B.Media_File_ID=A.Media_File_ID left join PHX_TBL_Products AS C ON  C.Product_ID=A.Entity_ID_1 left join PHX_TBL_Product_Details AS D ON C.Product_ID=D.Product_ID left join PHX_TBL_Demo_Plan AS E on B.Demo_Plan_ID=E.Demo_Plan_ID WHERE A.IS_Deleted ='N' and A.Download_Flag='N' and B.Demo_plan_ID='%@' and  date(E.Valid_From)<=date('now') and  date(E.Valid_To)>=date('now') order by Caption ASC",demoPlanID];
    
    
    NSLog(@"query for fetching media files is %@", mediaQry);
    
    
    NSMutableArray* demoPlanMediaArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:mediaQry];
    
    NSMutableArray * contentArray=[[NSMutableArray alloc]init];
    
    if (demoPlanMediaArray.count>0) {
        
        for (NSInteger i=0; i<demoPlanMediaArray.count; i++) {
     
        ProductMediaFile* currentProduct=[[ProductMediaFile alloc]init];
        NSMutableDictionary * currentProductDict=[demoPlanMediaArray objectAtIndex:i];
        currentProduct.Media_File_ID=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Media_File_ID"]];
        currentProduct.Product_Code=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_Code"]];
        currentProduct.Product_Name=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_Name"]];
        currentProduct.Caption=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Caption"]];
        currentProduct.File_Name=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"File_Name"]];
        currentProduct.Media_Type=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Media_Type"]];
        currentProduct.Entity_ID_1=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_ID"]];
        currentProduct.Detailed_Info=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Detailed_Info"]];
        currentProduct.isDemoed=NO;
        
        NSString* downloadUrl=[[[NSUserDefaults standardUserDefaults]valueForKey:@"ServerAPILink"] stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",currentProduct.Media_File_ID,@"M"]];
            currentProduct.Download_Url=downloadUrl;
            

        NSString*documentsDirectory;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        
        currentProduct.File_Path=[documentsDirectory stringByAppendingPathComponent:currentProduct.File_Name];
        [contentArray addObject:currentProduct];
        

        }
        
        return contentArray;
    }
    
    else
    {
        return nil;
    }
}

+(NSMutableArray*)fetchMediaFilesforDemoPlanID:(NSString*)demoPlanID
{
    
    
   NSString* mediaFilesIDQry= @"SELECT IFNULL(Media_File_ID,'0') AS  Media_File_ID FROM TBL_Media_Files LEFT JOIN PHX_TBL_DEMO_PLAN_MEDIA ON PHX_TBL_DEMO_PLAN_MEDIA.Media_file_id=TBL_Media_Files.Media_file_id where DEMO_PLAN_ID='3'";
    
     mediaFilesIDQry=[NSString stringWithFormat:@"Select IFNULL(Media_File_ID,'0') AS  Media_File_ID from PHX_TBL_DEMO_PLAN_MEDIA where DEMO_PLAN_ID='%@'",demoPlanID];
    
   // NSLog(@"query for fetching media files based on demo plan ID is %@", mediaFilesIDQry);
    
    NSMutableArray* mediaFileIDArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:mediaFilesIDQry];
    if (mediaFileIDArray.count>0) {
        //now fetch media files
        
        
        NSMutableArray* mediaFilesArray=[[NSMutableArray alloc]init];
        
        
        for (NSInteger i=0; i<mediaFileIDArray.count; i++) {
            
            NSString*mediaFileID=[[mediaFileIDArray valueForKey:@"Media_File_ID"] objectAtIndex:i];
            
            
            NSString* productQry=[NSString stringWithFormat:@"select IFNULL(Media_Type,'N/A') AS Media_Type, IFNULL(Filename,'N/A') AS File_Name, IFNULL(Caption,'N/A') AS Caption from TBL_Media_Files where Media_File_ID ='%@' and is_Deleted='N'", mediaFileID];
            
           // NSLog(@"product media qry %@", productQry);
            NSMutableArray* productMediaFiles=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productQry];
            if (productMediaFiles.count>0) {
                
                NSMutableDictionary * tempDict=[productMediaFiles objectAtIndex:0];
                
                [tempDict setObject:mediaFileID forKey:@"Media_File_ID"];
                
                
                
                [mediaFilesArray addObject:tempDict];
                
            }
            
        }
        
        return mediaFilesArray;
        
    }
    else
    {
        return nil;
    }
}


+(NSMutableArray*)fetchMediaFileDetailswithMediaFileID:(NSString*)mediaFileID

{
    //TO BE DONE
    
    //IGNORING DOWNLOAD FLAG WHICH IS N EVEN IF FILE GETS DOWNLOADED
    NSString* mediaFileDetailsQry=[NSString stringWithFormat:@"SELECT IFNULL(Entity_ID_1,'N/A') AS Entity_ID_1,IFNULL(Entity_ID_2,'N/A') AS Entity_ID_2,IFNULL(Entity_Type,'N/A') AS Entity_Type,IFNULL(Media_Type,'N/A') AS Media_Type,IFNULL(Media_File_ID,'N/A') AS Media_File_ID, IFNULL(Filename,'N/A') AS Filename,IFNULL(Caption,'N/A') AS Caption,IFNULL(Thumbnail,'N/A') AS Thumbnail,IFNULL(Custom_Attribute_3,'N/A') AS Custom_Attribute_3  FROM TBL_Media_Files where Media_File_ID='%@' and IS_Deleted='N' and Download_Flag='N'",mediaFileID];
    NSMutableArray* fileDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:mediaFileDetailsQry];
    if (fileDetailsArray.count>0) {
        
        
        
        return [fileDetailsArray objectAtIndex:0];
    }
    
    else
    {
        return nil;
    }
    
    
}

+(NSMutableArray*)fetchProductIDforSelectedDemoPlan:(NSString*)demoPlanID
{
    
    NSString* productDetailsforDemoPlanID=[NSString stringWithFormat:@"select IFNULL(A.Demo_Plan_ID,'N/A') AS Demo_Plan_ID,IFNULL(B.Demo_Plan_Item_ID,'0') AS Demo_Plan_Item_ID , IFNULL(B.Media_File_ID,'0') AS Media_File_ID,IFNULL(C.Entity_ID_1,'0') AS Product_ID from PHX_TBL_Demo_plan as A inner join PHX_TBL_Demo_Plan_Media as B  inner join TBL_Media_Files AS C where C.Media_File_ID=B.Media_File_ID and  A.Demo_Plan_ID=B.Demo_Plan_ID and A.Demo_Plan_ID='%@'",demoPlanID];
    
  //  NSLog(@"query for fetching demo plan details based on product ID %@", productDetailsforDemoPlanID);
    
    NSMutableArray* productDetails=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productDetailsforDemoPlanID];
    
    
    
    if (productDetails.count>0) {
        
        return productDetails;
    }
    
    else
    {
        return nil;
    }
    

    
}

+(NSMutableArray*)fetchDemoPlanforProducts:(NSString*)date

{
    NSString* demoPlanQry= [NSString stringWithFormat:@"select IFNULL(Demo_Plan_ID,'0') AS Demo_Plan_ID,IFNULL(Description,'N/A') AS Description,IFNULL(Valid_From,'N/A') AS Valid_From,IFNULL(Valid_To,'N/A') AS Valid_To,IFNULL(IS_Active,'N/A') AS IS_Active,IFNULL(IS_Deleted,'N/A') AS IS_Deleted  from PHX_TBL_Demo_plan where IS_Active ='Y' and IS_Deleted ='N'  and date(Valid_From)<=date('%@') and  date(Valid_To)>=date('%@') ",date,date];
    
  //  NSLog(@"query for demo plan products %@",demoPlanQry);
    
    
    NSMutableArray* demoPlanDetails=[[SWDatabaseManager retrieveManager]fetchDataForQuery:demoPlanQry];
    
    
    
    if (demoPlanDetails.count>0) {
        
        return demoPlanDetails;
    }
    
    else
    {
        return nil;
    }
    
    
}


+(NSString*)fetchDemoPlanNamewithID:(NSString*)demoPlanID

{
    NSString* demoPlanQry=[NSString stringWithFormat:@"select IFNULL(Description,'N/A') AS Description  from PHX_TBL_Demo_Plan where Demo_Plan_ID='%@'", demoPlanID];
    
    NSMutableArray* demoPlanDescArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:demoPlanQry];
    
    if (demoPlanDescArray.count>0) {
        
        
        
        return [[demoPlanDescArray objectAtIndex:0] valueForKey:@"Description"];
        
    }
    
    else
    {
        return nil;
    }
    
}

+(NSString*)fetchDateStringofDemoPlan:(NSString*)fieldName
{
    NSString* dateFieldQry=[NSString stringWithFormat:@"select IFNULL(%@,'N/A') AS %@ from  PHX_TBL_Demo_Plan where IS_Active ='Y' and IS_Deleted ='N'", fieldName,fieldName];
   
    
    
    NSMutableArray* demoPlanFieldNamesArray=[[NSMutableArray alloc]init];
    
    
    demoPlanFieldNamesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:dateFieldQry];
    
    
    
    if (demoPlanFieldNamesArray.count>0) {
        return [[demoPlanFieldNamesArray  objectAtIndex:0]valueForKey:[NSString stringWithFormat:@"%@", fieldName]];
    }
    
    else
    {
        return nil;
    }
    
    
}



+(NSDate*)convertNSStringtoNSDate:(NSString*)dateString

{
 
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* currentDate=[dateFormatter dateFromString:dateString];
    
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    
  //  NSLog(@"db date format is %@", formattedDate);
    
    
   
    return currentDate;
}
+(NSString*)fetchNextDateFromCurrentDateTimeinDatabaseFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    NSTimeInterval MY_EXTRA_TIME = 86400;
    NSDate *nextDate = [[NSDate date] dateByAddingTimeInterval:MY_EXTRA_TIME];
    NSString * formattedDate=[dateFormatter stringFromDate:nextDate];
    return formattedDate;
}

+(NSString*)fetchCurrentDateTimeinDatabaseFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}

+(NSString*)fetchDatabaseDateFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}
+(NSString*)fetchCurrentDateInDatabaseFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}
+(NSString*)convertNSDatetoDataBaseDateTimeFormat:(NSDate *)inputDate
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    //NSDate* currentDate=inputDate;
    NSString * formattedDate=[dateFormatter stringFromDate:inputDate];
    return formattedDate;

    
}
+(NSString*)convertNSDatetoDataBaseDateFormat:(NSDate *)inputDate
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    //NSDate* currentDate=inputDate;
    NSString * formattedDate=[dateFormatter stringFromDate:inputDate];
    return formattedDate;
    
    
}
+(NSDate *)getDateFromString:(NSString *)dateStr Format:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    NSDate *date=[dateFormatter dateFromString:dateStr];
    return date;
}
+(NSString *)getCurrentDateInDeviceDateWithOutTimeDisplayFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:kDateFormatWithoutTime];
    [dateFormatter setLocale:KUSLOCALE];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}

+(NSString *)getCurrentDateInDeviceDateWithTimeDisplayFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:kDateFormatWithTime];
    [dateFormatter setLocale:KUSLOCALE];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}

+(NSMutableArray*)fetchDemoPlansforSelectedDate:(NSDate*)selectedDate

{
    
    // NSLog(@"first and last dates of current month are %@",[MedRepDefaults fetchFirstandLastDatesofCurrentMonth:[NSDate date]]);
    
    
    //BOOL status=[MedRepDefaults isDate:selectedDate inRangeFirstDate:<#(NSDate *)#> lastDate:<#(NSDate *)#>]
    
    
    
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:selectedDate];
//    
//    
//    NSInteger month = [components month];
//    NSInteger year = [components year];
//    
//    NSString* updatedDateStr=[[[[NSString stringWithFormat:@"%d", year]stringByAppendingString:@"-"] stringByAppendingString:@"0"]stringByAppendingString:[NSString stringWithFormat:@"%d",month]];
//    NSLog(@"updated date string is %@", updatedDateStr);
//    
//    
//    
//    NSLog(@"current month are year are %d, %d", month,year);
//    
//    
//    NSLog(@"start and end dates are %@ %@",[MedRepDefaults startOfMonth],[MedRepDefaults endOfMonth]);
//    
//    where   Valid_To>=date('%@') and Is_Active ='Y' and Is_Deleted='N'",[self fetchDatabaseDateFormat]
    
    NSString* enableDemoPlanSpecialization = [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_DEMO_PLAN_SPECIALIZATION];
    NSString* demoPlanQry= @"";
    if([enableDemoPlanSpecialization isEqualToString:KAppControlsYESCode]){
        demoPlanQry=[NSString stringWithFormat:@"Select IFNULL(Specialization, 'N/A') AS Specialization,IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Description,'N/A') AS Description, IFNULL(Valid_From,'N/A')Valid_From, IFNULL(Valid_To,'N/A') AS Valid_To, IFNULL(Is_Active,'N/A') AS Is_Active,IFNULL(Is_Deleted,'N/A') AS Is_Deleted from PHX_TBL_Demo_plan where IS_Active ='Y' and IS_Deleted ='N' and date(Valid_From)<=date('%@') and  date(Valid_To)>=date('%@')  order by Description",[MedRepQueries convertNSDatetoDataBaseDateFormat:selectedDate],[MedRepQueries convertNSDatetoDataBaseDateFormat:selectedDate]];

    }else{
        demoPlanQry=[NSString stringWithFormat:@"Select IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Description,'N/A') AS Description, IFNULL(Valid_From,'N/A')Valid_From, IFNULL(Valid_To,'N/A') AS Valid_To, IFNULL(Is_Active,'N/A') AS Is_Active,IFNULL(Is_Deleted,'N/A') AS Is_Deleted from PHX_TBL_Demo_plan where IS_Active ='Y' and IS_Deleted ='N' and date(Valid_From)<=date('%@') and  date(Valid_To)>=date('%@')  order by Description",[MedRepQueries convertNSDatetoDataBaseDateFormat:selectedDate],[MedRepQueries convertNSDatetoDataBaseDateFormat:selectedDate]];

    }
    
    
    
    NSLog(@"query for fetching demo plans %@", demoPlanQry);
    NSMutableArray* demoPlanArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:demoPlanQry];
    
    if (demoPlanArray.count>0) {
        
        return demoPlanArray;
    }
    
    else{
        return nil;
    }
}



#pragma mark Task Count


+(NSMutableArray*)fetchTaskObjectsforHospital:(NSString*)locationID
{
     NSString* taskdetailsQry=[NSString stringWithFormat:@"select  IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where   Location_ID= '%@'  and Is_Active='Y' and Is_Deleted='N' ",locationID];
    
    NSLog(@"task qry is %@", taskdetailsQry);
    
    NSMutableArray* taskDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* taskObjectsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    NSLog(@"task objects for doc %@", taskObjectsArray);
    
    
    if (taskObjectsArray.count>0) {
        
        VisitTask * task;
        
        for (NSInteger i=0; i<taskObjectsArray.count; i++) {
            
            task=[[VisitTask alloc]init];
            NSMutableDictionary * currentDict=[taskObjectsArray objectAtIndex:i];
            
            task.Title=[currentDict valueForKey:@"Title"];
            task.Task_ID=[currentDict valueForKey:@"Task_ID"];
            task.Doctor_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Doctor_ID"]];
            task.Description=[currentDict valueForKey:@"Description"];
            task.Category=[currentDict valueForKey:@"Category"];
            task.Priority=[currentDict valueForKey:@"Priority"];
            task.Start_Time=[currentDict valueForKey:@"Start_Time"];
            task.Status=[currentDict valueForKey:@"Status"];
            task.Assigned_To=[currentDict valueForKey:@"Assigned_To"];
            task.Created_At=[currentDict valueForKey:@"Created_At"];
            task.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            task.Show_Reminder=[currentDict valueForKey:@"Show_Reminder"];
            task.isUpdated=@"NO";
            
            [taskDataArray addObject:task];
            
            
        }
        
        
        
    }
    
    if (taskDataArray.count>0) {
        return taskDataArray;
    }
    else
    {
        return 0;
    }
    
    

}


+(NSMutableArray*)fetchTaskObjectsforLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID
{
    NSString* taskdetailsQry;
    
    
    
    if([[SWDefaults getValidStringValue:doctorID]length]>0 && [[SWDefaults getValidStringValue:locationID]length]>0 )
    {
         taskdetailsQry=[NSString stringWithFormat:@"select  IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where   Location_ID= '%@' and Doctor_ID ='%@' and Is_Active='Y' and Is_Deleted='N' ",locationID,doctorID];
    }
    
    else if([[SWDefaults getValidStringValue:doctorID]isEqualToString:@""])
    {
        taskdetailsQry=[NSString stringWithFormat:@"select  IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where   Location_ID= '%@' and Is_Active='Y' and Is_Deleted='N' ",locationID];
    }
    else
    {
        taskdetailsQry=[NSString stringWithFormat:@"select  IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where Doctor_ID ='%@' and Is_Active='Y' and Is_Deleted='N' ",doctorID];
    }

    
    NSMutableArray* taskDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* taskObjectsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    if (taskObjectsArray.count>0) {
        
        VisitTask * task;
        
        for (NSInteger i=0; i<taskObjectsArray.count; i++) {
            
             task=[[VisitTask alloc]init];
            NSMutableDictionary * currentDict=[taskObjectsArray objectAtIndex:i];
            
            task.Title=[currentDict valueForKey:@"Title"];
            task.Task_ID=[currentDict valueForKey:@"Task_ID"];
            task.Doctor_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Doctor_ID"]];
            task.Description=[currentDict valueForKey:@"Description"];
            task.Category=[currentDict valueForKey:@"Category"];
            task.Priority=[currentDict valueForKey:@"Priority"];
            task.Start_Time=[currentDict valueForKey:@"Start_Time"];
            task.Status=[currentDict valueForKey:@"Status"];
            task.Assigned_To=[currentDict valueForKey:@"Assigned_To"];
            task.Created_At=[currentDict valueForKey:@"Created_At"];
            task.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            task.Show_Reminder=[currentDict valueForKey:@"Show_Reminder"];
            task.Location_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Location_ID"]];
            task.isUpdated=@"NO";
            
            [taskDataArray addObject:task];
            
            
        }
        

        
    }
    
    if (taskDataArray.count>0) {
        return taskDataArray;
    }
    else
    {
        return 0;
    }

    
}

+(NSMutableArray*)fetchAllTaskObjects
{
    NSString *taskdetailsQry = @"select  Task_ID, Title,Description,Category,IFNULL(Start_Time,datetime('now')) AS Start_Time, Status,Location_ID,IFNULL (Doctor_ID,'') AS Doctor_ID,IFNULL (Contact_ID,'') AS Contact_ID ,Last_Updated_At, IFNULL(Show_Reminder,'N') Show_Reminder,IFNULL(Emp_ID,'') AS Emp_ID,IFNULL(Created_At,datetime('now')) AS Created_At, IFNULL (Priority,'Low') AS Priority, Assigned_To  from PHX_TBL_Tasks where Is_Active='Y' and Is_Deleted='N' AND Task_ID IS NOT NULL AND Location_ID IS NOT NULL order by Last_Updated_At desc";
    
    
    NSMutableArray* taskDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* taskObjectsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    if (taskObjectsArray.count>0) {
        
        VisitTask * task;
        
        for (NSInteger i=0; i<taskObjectsArray.count; i++) {
            
            task=[[VisitTask alloc]init];
            NSMutableDictionary * currentDict=[taskObjectsArray objectAtIndex:i];
            
            task.Title=[currentDict valueForKey:@"Title"];
            task.Task_ID=[currentDict valueForKey:@"Task_ID"];
            task.Doctor_ID=[currentDict valueForKey:@"Doctor_ID"];
            task.Contact_ID=[currentDict valueForKey:@"Contact_ID"];
            task.Description=[currentDict valueForKey:@"Description"];
            task.Category=[currentDict valueForKey:@"Category"];
            task.Priority=[currentDict valueForKey:@"Priority"];
            task.Start_Time=[currentDict valueForKey:@"Start_Time"];
            task.Status=[currentDict valueForKey:@"Status"];
            task.Assigned_To=[currentDict valueForKey:@"Assigned_To"];
            task.Created_At=[currentDict valueForKey:@"Created_At"];
            task.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            task.Show_Reminder=[currentDict valueForKey:@"Show_Reminder"];
            task.Location_ID=[currentDict valueForKey:@"Location_ID"];
            task.isUpdated=@"NO";
            
            [taskDataArray addObject:task];
        }
    }
    
    if (taskDataArray.count>0) {
        return taskDataArray;
    }
    else
    {
        return 0;
    }
}

+(NSMutableArray*)fetchTaskObjectsforPharmacy:(NSString*)locationID
{
    NSString* taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where  Location_ID= '%@'  and  Is_Active='Y' and Is_Deleted='N' ",locationID];
    
    
    // NSLog(@"quert for fetching tasks %@", taskdetailsQry);
    
    NSMutableArray* taskDataArray=[[NSMutableArray alloc]init];
    
    
    NSMutableArray* taskObjectsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    
    NSLog(@"task objects for phar %@", taskObjectsArray);
    
    if (taskObjectsArray.count>0) {
        
        VisitTask * task;
        
        for (NSInteger i=0; i<taskObjectsArray.count; i++) {
            
            task=[[VisitTask alloc]init];
            NSMutableDictionary * currentDict=[taskObjectsArray objectAtIndex:i];
            
            task.Title=[currentDict valueForKey:@"Title"];
            task.Task_ID=[currentDict valueForKey:@"Task_ID"];
            task.Description=[currentDict valueForKey:@"Description"];
            task.Category=[currentDict valueForKey:@"Category"];
            task.Priority=[currentDict valueForKey:@"Priority"];
            task.Start_Time=[currentDict valueForKey:@"Start_Time"];
            task.Status=[currentDict valueForKey:@"Status"];
            task.Assigned_To=[currentDict valueForKey:@"Assigned_To"];
            task.Created_At=[currentDict valueForKey:@"Created_At"];
            task.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            task.Show_Reminder=[currentDict valueForKey:@"Show_Reminder"];
            
            
            [taskDataArray addObject:task];
            
            
        }
        
        if (taskDataArray.count>0) {
            return taskDataArray;
        }
        else
        {
            return 0;
        }
        
        
        
        
    }
    
    else
    {
        return 0;
    }
    

}

+(NSMutableArray*)fetchTaskObjectswithLocationID:(NSString*)locationID andContactID:(NSString*)contactID

{
    NSString* taskdetailsQry;
    
    if([[SWDefaults getValidStringValue:contactID] length]>0 && [[SWDefaults getValidStringValue:locationID] length]>0 )
    {
        taskdetailsQry=[NSString stringWithFormat:@"select Contact_ID,IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where  Location_ID= '%@' and  Contact_ID= '%@' and  Is_Active='Y' and Is_Deleted='N' ",locationID,contactID];
    }
    
    if([[SWDefaults getValidStringValue:contactID]isEqualToString:@""])
    {
       taskdetailsQry=[NSString stringWithFormat:@"select Contact_ID,IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where  Location_ID= '%@' and  Is_Active='Y' and Is_Deleted='N' ",locationID];
    }
    else
    {
        taskdetailsQry=[NSString stringWithFormat:@"select Contact_ID,IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where Contact_ID= '%@' and  Is_Active='Y' and Is_Deleted='N' ",contactID];
    }
    
    
   // NSLog(@"quert for fetching tasks %@", taskdetailsQry);
    
    NSMutableArray* taskDataArray=[[NSMutableArray alloc]init];

    
    NSMutableArray* taskObjectsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    
    NSLog(@"task objects for phar %@", taskObjectsArray);
    
        if (taskObjectsArray.count>0) {
            
            VisitTask * task;
            
            for (NSInteger i=0; i<taskObjectsArray.count; i++) {
                
                task=[[VisitTask alloc]init];
                NSMutableDictionary * currentDict=[taskObjectsArray objectAtIndex:i];
                
                task.Title=[currentDict valueForKey:@"Title"];
                task.Task_ID=[currentDict valueForKey:@"Task_ID"];
                task.Description=[currentDict valueForKey:@"Description"];
                task.Category=[currentDict valueForKey:@"Category"];
                task.Priority=[currentDict valueForKey:@"Priority"];
                task.Start_Time=[currentDict valueForKey:@"Start_Time"];
                task.Status=[currentDict valueForKey:@"Status"];
                task.Assigned_To=[currentDict valueForKey:@"Assigned_To"];
                task.Created_At=[currentDict valueForKey:@"Created_At"];
                task.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
                task.Show_Reminder=[currentDict valueForKey:@"Show_Reminder"];
                task.Contact_ID=[currentDict valueForKey:@"Contact_ID"];
                task.Location_ID=[currentDict valueForKey:@"Location_ID"];

                [taskDataArray addObject:task];
                
                
            }
            
            if (taskDataArray.count>0) {
                return taskDataArray;
            }
            else
            {
                return 0;
            }
            

            
            
        }
        
        else
        {
            return 0;
        }

    
    
 

}



+(NSMutableArray*)fetchTasksCountforLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID
{
    NSString* countQry=[NSString stringWithFormat:@"select Start_Time from PHX_TBL_Tasks where  End_Time IS  NULL and Location_ID='%@' and Doctor_ID='%@'",locationID,doctorID];
    
  //  NSLog(@"task count query is %@", countQry);
    
    NSMutableArray* countArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:countQry];
    if (countArray.count>0) {
        
        

        
        
        return countArray;
        
    }
    
    else
    {
        return nil;
    }
}


+(NSMutableArray*)fetchTaskCountforLocationID:(NSString*)locationID andContactID:(NSString*)contactID
{
    NSString* countQry=[NSString stringWithFormat:@"select Start_Time from PHX_TBL_Tasks where  End_Time IS  NULL and  Location_ID='%@' and Contact_ID='%@'",locationID,contactID];
    NSMutableArray* countArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:countQry];
    if (countArray.count>0) {
        
        return countArray;
        
    }
    
    else
    {
        return nil;
    }
}




+(NSInteger)fetchNotesCountforLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID
{
    NSString* countQry=[NSString stringWithFormat:@"select Count(*)  AS Count from PHX_TBL_EMP_Notes where Location_ID='%@' and Doctor_ID='%@'",locationID,doctorID];
    
   // NSLog(@"query for fetching notes %@", countQry);
    
    NSMutableArray* countArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:countQry];
    if (countArray.count>0) {
        
        return [[NSString stringWithFormat:@"%@",[[countArray objectAtIndex:0] valueForKey:@"Count"]] integerValue];
        
    }
    
    else
    {
        return 0;
    }
}


+(NSInteger)fetchNotesCountforLocationID:(NSString*)locationID andContactID:(NSString*)contactID
{
    NSString* countQry=[NSString stringWithFormat:@"select Count(*)  AS Count from PHX_TBL_EMP_Notes where Location_ID='%@' and Contact_ID='%@'",locationID,contactID];
    NSMutableArray* countArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:countQry];
    if (countArray.count>0) {
        
        return [[NSString stringWithFormat:@"%@",[[countArray objectAtIndex:0] valueForKey:@"Count"]] integerValue];
        
    }
    
    else
    {
        return 0;
    }
}



#pragma mark Create Visit Methods


+(NSMutableArray*)fetchContactIDforPharmacy:(NSString*)locationID
{
    NSString* pharmacyContactsQry=[NSString stringWithFormat:@"select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code, IFNULL(Contact_Name,'N/A') AS Contact_Name  from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID where A.Location_ID='%@'",locationID];
    NSMutableArray* contactsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyContactsQry];
    if (contactsArray.count>0) {
        
        return contactsArray;
    }
    
    else
    {
        return nil;
    }

}


+(NSString*)fetchContactwithContactID:(NSString*)contactID
{
    NSString* pharmacyContactsQry=[NSString stringWithFormat:@"select IFNULL(Contact_Name,'N/A') AS Contact_Name from PHX_TBL_Contacts where Contact_ID='%@'",contactID];
    
    
    NSMutableArray* contactsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyContactsQry];
    if (contactsArray.count>0) {
     
        
        NSString* contactName=[[contactsArray objectAtIndex:0] valueForKey:@"Contact_Name"];
        
        return contactName;
    }
    
    else
    {
        return nil;
    }

}


+(NSMutableArray*)fetchAllPharmacyDetails

{
//    NSString* pharmacyDetailsQry=[NSString stringWithFormat:@"select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code, IFNULL(Contact_Name,'N/A') AS Contact_Name  from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID "];
//    
    
    NSString* pharmacyDetailsQry=[NSString stringWithFormat:@"select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code,C.Location_Type ,IFNULL(Contact_Name,'N/A') AS Contact_Name  from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID inner join PHX_TBL_Locations AS C on C.Location_ID=A.Location_ID where Location_Type='p'"];
    
    
    NSMutableArray* contactsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyDetailsQry];
    if (contactsArray.count>0) {
        
        return contactsArray;
    }
    
    else
    {
        return nil;
    }
    
}

+(NSMutableArray*)fetchAllPharmacyDetailsWhenPendingVisitsEnabled
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    dateFormatter.dateFormat=@"yyyy-MM";
    NSString * monthString = [[dateFormatter stringFromDate:[NSDate date]] capitalizedString];
    
//    NSString* pharmacyDetailsQry=[NSString stringWithFormat:@"SELECT * FROM (select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code,C.Location_Type ,IFNULL(Contact_Name,'N/A') AS Contact_Name, IFNULL(C.Classification_2,'N/A') AS Class  from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID inner join PHX_TBL_Locations AS C on C.Location_ID=A.Location_ID where Location_Type='p' and A.Created_At like '%%%@%%') t1 INNER JOIN (select IFNULL(Custom_Attribute_1,'N/A') AS Visits_Req, IFNULL(Code_Value,'N/A') AS value from TBL_App_Codes where Code_Type='CLASS') t2 on t1.Class = t2.value INNER JOIN (SELECT count(*) As CompletedVisit,Location_ID FROM PHX_TBL_Actual_Visits group by Location_ID) t3 on t1.Location_ID = t3.Location_ID",monthString];
    
    //there is not requirement of completed VS pending visits for pharmacies to updating qry
    
    NSString* pharmacyDetailsQry =@"select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code,C.Location_Type ,IFNULL(Contact_Name,'N/A') AS Contact_Name  from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID inner join PHX_TBL_Locations AS C on C.Location_ID=A.Location_ID where Location_Type='p'";
    
    
    NSMutableArray* contactsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyDetailsQry];
    if (contactsArray.count>0)
    {
        return contactsArray;
    }
    else
    {
        return nil;
    }
}

+(NSMutableArray*)fetchContactsforPharmacywithLocationID:(NSString*)locationID
{
    NSString* pharmacyContactsQry=[NSString stringWithFormat:@"select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code, IFNULL(Contact_Name,'N/A') AS Contact_Name  from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID where A.Location_ID='%@'",locationID];
    
   NSLog(@"pharmacy contacts qry %@", pharmacyContactsQry);
    
    
    NSMutableArray* contactsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyContactsQry];
    
    
   //NSLog(@"pharmacy contacts data %@", contactsArray);

    
    if (contactsArray.count>0) {
        
        return contactsArray;
    }
    
    else
    {
        return nil;
    }
    
}
+(NSMutableArray*)fetchContactsforPharmacywithLocationIdWhenPendingVisitsEnabled:(NSString*)locationID
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    dateFormatter.dateFormat=@"yyyy-MM";
    NSString * monthString = [[dateFormatter stringFromDate:[NSDate date]] capitalizedString];
    
    NSString* pharmacyContactsQry=[NSString stringWithFormat:@"SELECT * FROM (select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code, IFNULL(Contact_Name,'N/A') AS Contact_Name ,IFNULL(B.Classification_2,'N/A') AS Class from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID where A.Location_ID='%@' and A.Created_At like '%%%@%%') t1 INNER JOIN (select IFNULL(Custom_Attribute_1,'N/A') AS Visits_Req, IFNULL(Code_Value,'N/A') AS value from TBL_App_Codes where Code_Type='CLASS') t2 on t1.Class = t2.value INNER JOIN (SELECT count(*) As CompletedVisit,Location_ID FROM PHX_TBL_Actual_Visits group by Location_ID) t3 on t1.Location_ID = t3.Location_ID",locationID,monthString];
    
    NSLog(@"pharmacy contacts qry %@", pharmacyContactsQry);
    
    NSMutableArray* contactsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyContactsQry];
    if (contactsArray.count>0)
    {
        return contactsArray;
    }
    else
    {
        return nil;
    }
}


+(NSString*)createVisitandReturnPlannedVisitID:(NSMutableDictionary*)visitDetailsDict
{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    NSString* plannedVisitID = [NSString createGuid];
    
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[visitDetailsDict valueForKey:@"Visit_Date"]];
   //NSLog(@"refined date is %@", refinedDate);
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    
    NSString* visitLocationType=[visitDetailsDict valueForKey:@"Location_Type"];
    
    
    if ([[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
        
        [visitDetailsDict setObject:[NSNull null] forKey:@"Doctor_ID"];

    }
    
    else if ([[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"D"])
        
    {
        [visitDetailsDict setObject:[NSNull null] forKey:@"Contact_ID"];

    }
    
//    if ([[visitDetailsDict valueForKey:@"Doctor_ID"] isEqual:[NSNull null]]) {
//        
//        
//    }
//    
//    else if ([[visitDetailsDict valueForKey:@"Doctor_ID"]isEqualToString:@"0"])
//        
//    {
//        
//    }
//   
//    
//    if ([[visitDetailsDict valueForKey:@"Contact_ID"] isEqual:[NSNull null]]) {
//        
//        
//    }
//    
//    else if ([[visitDetailsDict valueForKey:@"Contact_ID"]isEqualToString:@"0"])
//        
//    {
//        [visitDetailsDict setObject:[NSNull null] forKey:@"Contact_ID"];
//        
//    }
//    
//    if ([[visitDetailsDict valueForKey:@"Contact_ID"]isEqualToString:@"(null)"]) {
//        
//        [visitDetailsDict setObject:[NSNull null] forKey:@"Contact_ID"];
//
//    }
//    
//    
//    if ([[visitDetailsDict valueForKey:@"Doctor_ID"]isEqualToString:@"(null)"]) {
//        
//        [visitDetailsDict setObject:[NSNull null] forKey:@"Doctor_ID"];
//        
//    }
//    
    NSLog(@"visit details while creating visit %@", visitDetailsDict);

    
    
   //NSLog(@"creating visit with details %@", [visitDetailsDict description]);
    
    BOOL success = NO;
    @try {
        if([visitDetailsDict valueForKey:@"Old_Visit_ID"]==nil)
        {
            success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Planned_Visits  (Planned_Visit_ID,Visit_Date,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Visit_Status,Comments,Created_At,Created_By,Visit_Type,Demo_Plan_ID,Custom_Attribute_2,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",plannedVisitID,refinedDate,empID,[visitDetailsDict valueForKey:@"Location_ID"],[visitDetailsDict valueForKey:@"Doctor_ID"], [visitDetailsDict valueForKey:@"Contact_ID"],[visitDetailsDict valueForKey:@"Status"],[visitDetailsDict valueForKey:@"Objective"],createdAt,createdBy,[visitDetailsDict valueForKey:@"Visit_Type"],[visitDetailsDict valueForKey:@"Demo_Plan_ID"],visitLocationType,@"1",nil];

        }
        else
        {
            success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Planned_Visits  (Planned_Visit_ID,Visit_Date,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Visit_Status,Comments,Created_At,Created_By,Visit_Type,Demo_Plan_ID,Custom_Attribute_2,Custom_Attribute_3,Old_Visit_ID)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",plannedVisitID,refinedDate,empID,[visitDetailsDict valueForKey:@"Location_ID"],[visitDetailsDict valueForKey:@"Doctor_ID"], [visitDetailsDict valueForKey:@"Contact_ID"],[visitDetailsDict valueForKey:@"Status"],[visitDetailsDict valueForKey:@"Objective"],createdAt,createdBy,[visitDetailsDict valueForKey:@"Visit_Type"],[visitDetailsDict valueForKey:@"Demo_Plan_ID"],visitLocationType,@"1",[visitDetailsDict valueForKey:@"Old_Visit_ID"],nil];
        }
        }@catch (NSException *exception)
    {
       //NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }

    if (success==YES) {
        
        return plannedVisitID;
    }
    else
    {
        return nil;
    }
}
+(BOOL)createVisit:(NSMutableDictionary*)visitDetailsDict
{
    
    
    
    
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    NSString* plannedVisitID = [NSString createGuid];
    
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[visitDetailsDict valueForKey:@"Visit_Date"]];
   //NSLog(@"refined date is %@", refinedDate);
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    if ([[visitDetailsDict valueForKey:@"Doctor_ID"] isEqual:[NSNull null]]) {
        
        
    }
    
    else if ([[visitDetailsDict valueForKey:@"Doctor_ID"]isEqualToString:@"0"])
        
    {
        [visitDetailsDict setObject:[NSNull null] forKey:@"Doctor_ID"];
        
    }
    
    
    if ([[visitDetailsDict valueForKey:@"Contact_ID"] isEqual:[NSNull null]]) {
        
        
    }
    
    else if ([[visitDetailsDict valueForKey:@"Contact_ID"]isEqualToString:@"0"])
        
    {
        [visitDetailsDict setObject:[NSNull null] forKey:@"Contact_ID"];
        
    }
    
    
    
    
    
    BOOL success = NO;
    @try {
        
        success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Planned_Visits  (Planned_Visit_ID,Visit_Date,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Visit_Status,Comments,Created_At,Created_By,Visit_Type,Demo_Plan_ID,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",plannedVisitID,refinedDate,empID,[visitDetailsDict valueForKey:@"Location_ID"],[visitDetailsDict valueForKey:@"Doctor_ID"], [visitDetailsDict valueForKey:@"Contact_ID"],[visitDetailsDict valueForKey:@"Status"],[visitDetailsDict valueForKey:@"Comments"],createdAt,createdBy,[visitDetailsDict valueForKey:@"Visit_Type"],[visitDetailsDict valueForKey:@"Demo_Plan_ID"],@"1",nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    return success;
    
    
    
}

+(NSMutableArray*)fetchCoordinatesforLocationID:(NSString*)locationID

{
    NSString* queryforLocationID=[NSString stringWithFormat:@"SELECT IFNULL(Latitude,'0') AS Latitude, IFNULL(Longitude,'0') AS Longitude FROM PHX_TBL_Locations where Location_ID='%@'",locationID];
    
    NSMutableArray* coordArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:queryforLocationID];
    if (coordArray.count>0) {
        
        return [coordArray objectAtIndex:0];
    }
    
    else
    {
        return nil;
    }
    
}


+(NSMutableArray*)fetchPlannedVisits
{
    NSString* plannedVisitsQry=[NSString stringWithFormat: @"select IFNULL(Planned_Visit_ID,'0') AS Planned_Visit_ID, IFNULL(Visit_Date,'N/A') AS Visit_Date, IFNULL(EMP_ID,'0') AS EMP_ID,IFNULL(Location_ID,'0') AS Location_ID, IFNULL(Doctor_ID,'0') AS Doctor_ID ,IFNULL(Contact_ID,'0') AS Contact_ID, IFNULL(Visit_Status,'N/A') AS Visit_Status, IFNULL(Comments, 'N/A') AS Objective , IFNULL(Created_At,'N/A') AS Created_At,IFNULL(Created_By,'N/A') AS Created_By, IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Visit_Type,'N/A') AS Visit_Type from PHX_TBL_Planned_Visits where Visit_Status!='X'"];
    
    
    
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:plannedVisitsQry];
    
    if (plannedVisitsArray.count>0) {
        
        return plannedVisitsArray;
    }
    
    else{
        return nil;
    }

}


+(NSMutableArray*)fetchPlannedVisitsforNextDay:(NSString*)visitDate
{
    
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 1;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    NSString* dateString=[[NSMutableString alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    dateString = [df stringFromDate:nextDate];
   //NSLog(@"nextDate: %@ ...", dateString);
    NSString* plannedVisitsQry=[NSString stringWithFormat: @"select IFNULL(Planned_Visit_ID,'0') AS Planned_Visit_ID, IFNULL(Visit_Date,'N/A') AS Visit_Date, IFNULL(EMP_ID,'0') AS EMP_ID,IFNULL(Location_ID,'0') AS Location_ID, IFNULL(Doctor_ID,'0') AS Doctor_ID ,IFNULL(Contact_ID,'0') AS Contact_ID, IFNULL(Visit_Status,'N/A') AS Visit_Status, IFNULL(Comments, 'N/A') AS Objective , IFNULL(Created_At,'N/A') AS Created_At,IFNULL(Created_By,'N/A') AS Created_By, IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Visit_Type,'N/A') AS Visit_Type from PHX_TBL_Planned_Visits where Visit_date like '%@%%'  and Visit_Status!='X' Order by  Visit_Status  , Visit_date ASC ",dateString];
    
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:plannedVisitsQry];
    
    if (plannedVisitsArray.count>0) {
        
        return plannedVisitsArray;
    }
    
    else{
        return nil;
    }

}


+(NSString*)fetchModifiedDate:(NSInteger)selectedValue
{
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = selectedValue;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    NSString* dateString=[[NSMutableString alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    dateString = [df stringFromDate:nextDate];
   //NSLog(@"previousDate: %@ ...", dateString);

    return dateString;
    
}



+(NSMutableArray*)fetchPlannedVisitswithDate:(NSString*)visitDate withSelection:(NSInteger)selectedValue
{
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = selectedValue;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    NSString* dateString=[[NSMutableString alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    dateString = [df stringFromDate:nextDate];
   //NSLog(@"previousDate: %@ ...", dateString);
    
   //NSLog(@"selected value is %@", visitDate);
    
    
    NSString* plannedVisitsQry=[NSString stringWithFormat: @"select IFNULL(Planned_Visit_ID,'0') AS Planned_Visit_ID, IFNULL(Visit_Date,'N/A') AS Visit_Date, IFNULL(EMP_ID,'0') AS EMP_ID,IFNULL(Location_ID,'0') AS Location_ID, IFNULL(Doctor_ID,'0') AS Doctor_ID ,IFNULL(Contact_ID,'0') AS Contact_ID, IFNULL(Visit_Status,'N/A') AS Visit_Status, IFNULL(Comments, 'N/A') AS Objective , IFNULL(Created_At,'N/A') AS Created_At,IFNULL(Created_By,'N/A') AS Created_By, IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Visit_Type,'N/A') AS Visit_Type from PHX_TBL_Planned_Visits where Visit_date like '%@%%' and  Visit_Status!='X' Order by  Visit_Status  , Visit_date ASC",visitDate];
    
    
   //NSLog(@"query for planned visits %@", plannedVisitsQry);
    
    
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:plannedVisitsQry];
    //NSLog(@"planned visits in asc order %@", [plannedVisitsArray description]);
    
    
    if (plannedVisitsArray.count>0) {
        
        return plannedVisitsArray;
    }
    
    else{
        return nil;
    }

}


+(NSMutableArray*)fetchAllPlannedVisits
{
    NSString* plannedVisitsQry=[NSString stringWithFormat: @"select IFNULL(Planned_Visit_ID,'0') AS Planned_Visit_ID, IFNULL(Visit_Date,'N/A') AS Visit_Date, IFNULL(EMP_ID,'0') AS EMP_ID,IFNULL(Location_ID,'0') AS Location_ID, IFNULL(Doctor_ID,'0') AS Doctor_ID ,IFNULL(Contact_ID,'0') AS Contact_ID, IFNULL(Visit_Status,'N/A') AS Visit_Status, IFNULL(Comments, 'N/A') AS Objective , IFNULL(Created_At,'N/A') AS Created_At,IFNULL(Created_By,'N/A') AS Created_By, IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Visit_Type,'N/A') AS Visit_Type from PHX_TBL_Planned_Visits where Visit_Status!='X' Order by  Visit_Status  , Visit_date ASC "];
    
    
    
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:plannedVisitsQry];
    
    if (plannedVisitsArray.count>0) {
        
        return plannedVisitsArray;
    }
    
    else{
        return nil;
    }

}

+(NSMutableArray*)fetchPlannedVisitswithDate:(NSString*)visitDate

{
    
    //rescheduled visit
    
    NSString* plannedVisitsQry=[NSString stringWithFormat: @"select Actual_visit_id,A.Planned_Visit_ID as ActualPlannedVisitID, P.Planned_Visit_ID AS Planned_Visit_ID,P.Visit_Date AS Visit_Date, P.EMP_ID AS EMP_ID,IFNULL(L.Latitude,'0')AS Latitude,IFNULL(L.Longitude,'0')AS Longitude,P.Location_ID AS Location_ID, P.Doctor_ID AS Doctor_ID,P.Contact_ID AS Contact_ID, IFNULL(P.Visit_Status,'N/A') AS Visit_Status,IFNULL(P.Comments, 'N/A') AS Objective ,P.Visit_Date AS Visit_Date,P.Created_At AS Created_At,P.Created_By AS Created_By, P.Demo_Plan_ID AS Demo_Plan_ID,IFNULL(P.Visit_Type,'N/A') AS Visit_Type ,IFNULL(L.Location_Type,'N/A') AS Location_Type,IFNULL(L.Address_1,'N/A')AS Address,IFNULL(L.Location_name,'N/A') as Location_Name, IFNULL(L.Classification_3,'N/A') AS OTC ,A.Visit_End_Date AS Last_Visited_At, IFNULL(Contact_Name,'N/A')AS Pharmacy_Contact_Name,IFNULL(L.Custom_Attribute_1,'N/A')AS Trade_Channel, IFNULL(L.Classification_2,'N/A') AS Class,IFNULL(D.Doctor_Name,'N/A')AS Doctor_Name ,IFNULL(D.Classification_2,'N/A')AS Doctor_Class, IFNULL(D.Classification_1,'N/A') AS Specialization from PHX_TBL_Planned_Visits  as P left join PHX_TBL_Actual_Visits AS A on A.Planned_Visit_ID =P.Planned_Visit_ID inner join PHX_TBL_Locations L on P.Location_ID=L.Location_ID left join PHX_TBL_Contacts AS C on C.Contact_ID=P.Contact_ID left join PHX_TBL_Doctors  AS D on D.Doctor_ID=P.Doctor_ID where Visit_date like  '%%%@%%' and P.Visit_Status!='X' Order by  Visit_Status, Visit_Date  ASC",visitDate];
    
    
    
    
    
   //NSLog(@"planned visits qry %@", plannedVisitsQry );
    
    
    
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:plannedVisitsQry];
    
    if (plannedVisitsArray.count>0) {
        
        return plannedVisitsArray;
    }
    
    else{
        return nil;
    }
}


+(NSMutableString*)fetchDoctorDetails:(NSString*)doctorID
{
    NSString* plannedVisitsQry=[NSString stringWithFormat:@"select IFNULL(Doctor_Name,'N/A') AS Doctor_Name, IFNULL(Classification_1,'N/A') AS Specialization from PHX_TBL_Doctors where Doctor_ID='%@'",doctorID];
    

    
    NSMutableArray* doctorDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:plannedVisitsQry];
    
    if (doctorDetailsArray.count>0) {
        
        //now check Specialization based on app codes
        

        NSString* trimmedstring =[[[doctorDetailsArray objectAtIndex:0] valueForKey:@"Specialization"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        NSString* specilizationQry=[NSString stringWithFormat:@"Select IFNULL(Code_Description ,'N/A') AS Specialization from TBL_App_Codes where Code_Type='SPECIALIZATION' and Code_Value ='%@'", trimmedstring];
        
        
        
        NSMutableArray* specilizationArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
        

        if (specilizationArray.count>0) {
            
            return [[specilizationArray objectAtIndex:0]valueForKey:@"Specialization"];
            
        }
        
        else
        {
            return nil;
        }

        
    }
    
    else{
        return nil;
    }

}

+(NSString*)fetchDoctorwithID:(NSString *)doctorID
{
    NSString* plannedVisitsQry=@"";
    if ([SWDefaults isDrApprovalAvailable])
    {
     plannedVisitsQry=[NSString stringWithFormat:@"select IFNULL(Doctor_Name,'N/A') AS Doctor_Name, IFNULL(Classification_1,'N/A') AS Specialization from PHX_TBL_Doctors where Doctor_ID='%@' and Is_Approved = 'Y'",doctorID];
    }else
    {
        plannedVisitsQry=[NSString stringWithFormat:@"select IFNULL(Doctor_Name,'N/A') AS Doctor_Name, IFNULL(Classification_1,'N/A') AS Specialization from PHX_TBL_Doctors where Doctor_ID='%@'",doctorID];
    }
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:plannedVisitsQry];
    
    
   // NSLog(@"doctor details query is %@", plannedVisitsQry);
    if (plannedVisitsArray.count>0) {
        
        return [[plannedVisitsArray valueForKey:@"Doctor_Name"]objectAtIndex:0];
    }
    
    else{
        return nil;
    }
}



+(NSMutableArray*)fetchVisitDetailsForDoctorwithDoctorID:(NSString*)doctorID visitID:(NSString*)visitID
{
    
    NSString* visitDetailsQry=[NSString stringWithFormat:@"SELECT IFNULL(V.Visit_Date, 'N/A') AS Visit_Date,IFNULL(V.Demo_Plan_ID,'N/A') AS Demo_Plan_ID,IFNULL(V.Visit_Type,'N/A') AS Visit_Type,IFNULL(V.Comments,'N/A') AS Objective,IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OtcRx,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.City,'N/A') AS City,IFNULL(D.Phone,'N/A') AS Phone,IFNULL(D.Email,'N/A') AS Email FROM PHX_TBL_Planned_Visits V INNER JOIN PHX_TBL_Doctors D ON V.Doctor_ID = D.Doctor_ID WHERE V.Doctor_ID ='%@'and V.Planned_Visit_ID='%@'", doctorID,visitID];
    
    
    
    /*
    //to fetch previous visit end date
    NSString* refinedVisitDetailsQry=[NSString stringWithFormat:@"SELECT IFNULL(V.Visit_Date, 'N/A') AS Visit_Date,IFNULL(V.Demo_Plan_ID,'N/A') AS Demo_Plan_ID,IFNULL(V.Visit_Type,'N/A') AS Visit_Type,IFNULL(V.Comments,'N/A') AS Objective,IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OtcRx,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.City,'N/A') AS City,IFNULL(D.Phone,'N/A') AS Phone,IFNULL(D.Email,'N/A') AS Email , IFNULL(A.Visit_End_Date,'N/A') AS Visit_End_Date  FROM PHX_TBL_Planned_Visits V INNER JOIN PHX_TBL_Doctors D ON V.Doctor_ID = D.Doctor_ID  inner join  PHX_TBL_Actual_Visits  A on A.Doctor_ID=D.Doctor_ID  WHERE V.Doctor_ID ='%@'and V.Planned_Visit_ID='%@'",doctorID,visitID];*/
    
   //NSLog(@"visit details for doctor qry %@",visitDetailsQry);
    
    
    
    NSMutableArray* visitDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitDetailsQry];
    
    if (visitDetailsArray.count>0) {
        
        
       // NSLog(@"refined visits details array %@",visitDetailsArray);
        
        return visitDetailsArray;
    }
    else
    {
        return nil;
    }
    
    
}


+(NSMutableArray*)fetchLocationDetails:(NSString*)locationID loactionType:(NSString*)locationType
{
    
    NSString* locationNameQry=[NSString stringWithFormat:@"select IFNULL(Location_Name,'N/A') AS Location_Name,IFNULL(Address_1,'N/A') AS Address from PHX_TBL_Locations  where Location_Type='%@' and Location_ID='%@'",locationType,locationID];
    NSMutableArray* locationNameArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationNameQry];
    
    if (locationNameArray.count>0) {
        
        return locationNameArray;
    }
    
    else{
        return nil;
    }
}



+(NSString*)fetchLocationNamewithID:(NSString*)locationID locationType:(NSString*)locationType
{
    
    NSString* locationNameQry=[NSString stringWithFormat:@"select IFNULL(Location_Name,'N/A') AS Location_Name from PHX_TBL_Locations  where Location_Type='%@' and Location_ID='%@'",locationType,locationID];
    
   // NSLog(@"location name qry %@", locationNameQry);
    
    
    NSMutableArray* locationNameArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationNameQry];
    
    if (locationNameArray.count>0) {
        
        return [[locationNameArray valueForKey:@"Location_Name"]objectAtIndex:0];
    }
    
    else{
        return nil;
    }
    
}

+(NSString*)fetchDemoPlanwithID:(NSString*)demoPlanID

{
    
    
    NSString* demoPlanQry=[NSString stringWithFormat:@"select IFNULL(Description,'N/A') AS Description from PHX_TBL_Demo_Plan where Demo_Plan_ID='%@'",demoPlanID];
    NSMutableArray* demoPlanArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:demoPlanQry];
    
    if (demoPlanArray.count>0) {
        
        return [[demoPlanArray valueForKey:@"Description"]objectAtIndex:0];
    }
    
    else{
        return nil;
    }
    
}


+(NSString*)fetchLocationTypewithLocationID:(NSString*)locationID
{
    NSString* demoPlanQry=[NSString stringWithFormat:@"select IFNULL(Location_Type,'N/A') AS Location_Type from PHX_TBL_locations where Location_ID='%@'",locationID];
    
    
    
    NSMutableArray* demoPlanArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:demoPlanQry];
    
    

    if (demoPlanArray.count>0) {
        
        return [[demoPlanArray valueForKey:@"Location_Type"]objectAtIndex:0];
    }
    
    else{
        return nil;
    }
    
}


+(NSMutableArray*)fetchVisitDetailsfForPharmacywithLocationID:(NSString*)locationID visitID:(NSString*)visitID

{
    //
    
    
    NSString* pharmacyVisitDetailsQry=[NSString stringWithFormat:@"SELECT IFNULL(L.Location_Name,'N/A') AS Location_Name , IFNULL(L.Classification_2,'N/A') AS Class, IFNULL(L.Classification_3,'N/A') AS OtcRx,IFNULL(L.Custom_Attribute_1,'N/A') AS Trade_Channel, IFNULL(L.Address_1,'N/A') AS Address_1, IFNULL(L.Address_2,'N/A') AS Address_2, IFNULL(L.Address_3,'N/A') AS Address_3, IFNULL(L.State,'N/A') AS City, IFNULL(L.Email,'N/A') AS Email, IFNULL(L.Phone,'N/A') AS Phone, IFNULL(V.Visit_Date,'N/A') AS Visit_Date,IFNULL(V.Visit_Type,'N/A') AS Visit_Type,IFNULL(V.Comments,'N/A') AS Objective,IFNULL(V.Demo_Plan_ID,'N/A') AS Demo_Plan_ID FROM PHX_TBL_Locations L INNER JOIN PHX_TBL_Planned_Visits V ON L.Location_ID = V.Location_ID WHERE V.Location_ID ='%@' and V.Planned_Visit_ID='%@' and L.Location_Type='P'", locationID,visitID];
    
    
    
    
    //refined query to fetch content with visit end date
    
   /*
    NSString* refinedPharmacyVisitDetailsQry=[NSString stringWithFormat:@"SELECT IFNULL(L.Location_Name,'N/A') AS Location_Name , IFNULL(L.Classification_2,'N/A') AS Class, IFNULL(L.Classification_3,'N/A') AS OtcRx,IFNULL(L.Custom_Attribute_1,'N/A') AS Trade_Channel, IFNULL(L.Address_1,'N/A') AS Address_1, IFNULL(L.Address_2,'N/A') AS Address_2, IFNULL(L.Address_3,'N/A') AS Address_3, IFNULL(L.State,'N/A') AS City, IFNULL(L.Email,'N/A') AS Email, IFNULL(L.Phone,'N/A') AS Phone, IFNULL(V.Visit_Date,'N/A') AS Visit_Date,IFNULL(V.Visit_Type,'N/A') AS Visit_Type,IFNULL(V.Comments,'N/A') AS Objective,IFNULL(V.Demo_Plan_ID,'N/A') AS Demo_Plan_ID ,IFNULL(A.Visit_End_Date,'N/A') AS Visit_End_Date FROM PHX_TBL_Locations L INNER JOIN PHX_TBL_Planned_Visits V ON L.Location_ID = V.Location_ID inner join   PHX_TBL_Actual_Visits  A  on V.Location_ID=A.Location_ID WHERE L.Location_Type='P' and  V.Location_ID ='%@' and V.Planned_Visit_ID='%@' ", locationID,visitID];*/
    
    
    
    
    NSMutableArray* pharmacyVisitDetailsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyVisitDetailsQry];
    
    if (pharmacyVisitDetailsArray.count>0) {
        
       // NSLog(@"pharmacy visit details %@", [pharmacyVisitDetailsArray description]);
        
        return pharmacyVisitDetailsArray;
    }
    
    else
    {
        return nil;
    }
    
    
    
    
}



+(BOOL)rescheduleVisitwithVisitDetails:(NSMutableDictionary*)visitDetailsDict
{
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    
    NSString *existingVisitID = [visitDetailsDict valueForKey:@"Planned_Visit_ID"];
    NSString *rescheduledTime = [visitDetailsDict valueForKey:@"Visit_Date"];
    NSString *demoPlanID = [visitDetailsDict valueForKey:@"Demo_Plan_ID"];
    NSString* visitID=[NSString createGuid];

    NSString *doctorID;
    BOOL success = NO;
    @try {
        
        if ([[visitDetailsDict valueForKey:@"Location_Type"] isEqualToString:@"D"])
        {
            doctorID = [visitDetailsDict valueForKey:@"Doctor_ID"];
            success =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Date =? , Doctor_ID= ?, Demo_Plan_ID= ? , Old_Visit_ID= ?,  Planned_Visit_ID=?  where Planned_Visit_ID=?",rescheduledTime, doctorID, demoPlanID, existingVisitID,visitID,existingVisitID, nil];
            
        } else {
            doctorID = [visitDetailsDict valueForKey:@"Contact_ID"];
            success =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Date =? , Contact_ID= ?, Demo_Plan_ID= ? , Old_Visit_ID= ?,  Planned_Visit_ID=?  where Planned_Visit_ID=?",rescheduledTime, doctorID, demoPlanID, existingVisitID,visitID,existingVisitID, nil];
        }
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    return success;
    
    
}



+(NSString*)fetchAppCodeValueforProductType:(NSString*)productType

{
    
    NSString* appCodeDesc;
    
    NSString* appCodeQry=[NSString stringWithFormat:@"select  Code_Value from TBL_App_Codes  where Code_Type='Product_Type' and Code_Description='%@'",productType];
   //NSLog(@"qry for app code %@", appCodeQry);
    
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    
    if (appCodeArray.count>0) {
        appCodeDesc=[[appCodeArray objectAtIndex:0]valueForKey:@"Code_Value"];
        return appCodeDesc;
        
    }
    
    else
    {
        return nil;
    }

}

+(NSString*)fetchAppCOdeforProductType:(NSString*)productType

{
    NSString* appCodeDesc;
    
    NSString* appCodeQry=[NSString stringWithFormat:@"select  Code_Description from TBL_App_Codes  where Code_Type='Product_Type' and Code_Value='%@'",productType];
   //NSLog(@"qry for app code %@", appCodeQry);
    
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    
    if (appCodeArray.count>0) {
        appCodeDesc=[[appCodeArray objectAtIndex:0]valueForKey:@"Code_Description"];
        return appCodeDesc;
        
    }
    
    else
    {
        return nil;
    }
    
}

+(NSMutableArray*)fetchVisitsRequiredwithClass:(NSString*)classStr

{
    NSString* visitsReq=[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_1,'N/A') AS Visits_Req from TBL_App_Codes where Code_Type='CLASS' and Code_Value='%@'",classStr];
    
    
    NSMutableArray* visitsReqArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsReq];
    
    if (visitsReqArray.count>0) {
        
       //NSLog(@"visits req array is %@", [visitsReqArray description]);
        
        return [visitsReqArray objectAtIndex:0];
    }
    else
    {
        
        return nil;
        
    }
    
}


+(NSMutableArray*)fetchCompltedVisitsforCurrentMonth:(NSString*)locationID
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    //[query setTag:kQueryTypeTotalCollection];
    
    
    NSString* compltedVisitsQry=[NSString stringWithFormat:@"SELECT * FROM PHX_TBL_Actual_Visits  WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month')  and Doctor_ID='%@' order by  Visit_End_Date desc ",locationID];
    
   NSLog(@"completed visits qry %@", compltedVisitsQry);
    
    
    
    NSMutableArray* compltedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:compltedVisitsQry];
    if (compltedVisitsArray.count>0) {
       NSLog(@"completed visits count is %@", [compltedVisitsArray objectAtIndex:0]);
        
        return compltedVisitsArray;
    }
    else
    {
        return nil;
    }
    
}


+(NSString*)fetchVisitCompletionStatuswithVisitID:(NSString*)plannedVisitID
{
    
    NSString* status=[[NSString alloc]init];
    NSString* visitCompletionQry=[NSString stringWithFormat:@"select IFNULL(Visit_Status,'N/A') AS Visit_Status from PHX_TBL_Planned_Visits where Planned_Visit_ID='%@'", plannedVisitID];
    NSMutableArray* completionStatusArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitCompletionQry];
    if (completionStatusArray.count>0) {
        
        return   status=[[completionStatusArray objectAtIndex:0] valueForKey:@"Visit_Status"];
        
    }
    
    else
    {
        return nil;
        
    }
    
    
}

+(NSMutableArray*)fetchVisitCompletionData:(NSString*)doctorID
{
    NSString* visitCompleionQry=[NSString stringWithFormat:@"SELECT IFNULL(Visit_End_Date,'N/A') AS Visit_End_Date,  IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Doctor_ID,'N/A') AS Doctor_ID FROM PHX_TBL_Actual_Visits  where Doctor_ID='%@' order by Visit_End_Date desc ",doctorID];
    
    NSMutableArray* completionStatusArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitCompleionQry];
    
    if (completionStatusArray.count>0) {
        return [completionStatusArray objectAtIndex:0];
    }
    
    else
    {
        return nil;
    }
    
}

#pragma mark Create Location Methods

+(BOOL)createLocationWithDetails:(NSMutableDictionary*)locationDetails

{
    
    
    //adding contact creation details, so have to do with transactions so as to rollback if anything goes wrong while creating location or contact
    
    
    //data to to be inserted to TBL_Contacts and TBL_Loction_contact Map
    
   
    
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
   
    [database beginTransaction];
    

    
    
    
   //NSLog(@"location Details are %@", [locationDetails description]);
    
    
   
    BOOL locationCreatedSuccessfully=NO;
    BOOL contactDataInsertedSuccessfully=NO;
    BOOL locationContactMapDataInsertedSuccessfully=NO;
    
    
    
    
    NSString* locationID=  [locationDetails valueForKey:@"Location_ID"];

    NSString* contactID=  [locationDetails valueForKey:@"Contact_ID"];

    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    //for not emirate is dubai in ui we are not capturing city but in db its mandatory check with harpal, to be done
    
    NSString* isActive=@"Y";
    NSString*isDeleted=@"N";
    
    
    
    

    
    
    NSString* locationType;
    
    NSString* locationTypeStr=[locationDetails valueForKey:@"Location_Type"];
    if ([locationTypeStr isEqualToString:@"Clinic"] || [locationTypeStr isEqualToString:@"Hospital"]) {
        locationType=@"D";
        
    }
    
    else
    {
        locationType=@"P";
    }
    
    
    
    
    //if app control flag FM_ENABLE_DR_APPROVAL is Y then is_Approved is N
    NSString* isApproved = @"";
    NSString* locationApproval= [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_LOCATION_APPROVAL];
    if ([locationApproval isEqualToString:KAppControlsYESCode])
    {
        isApproved = KAppControlsNOCode;
        //this is hospital location we need not add contact details for that, beacuse doctor is contact for hospital
        @try {
            locationCreatedSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Locations  (Location_ID,Location_Name,Location_Type,Address_1,City,Phone,Latitude,Longitude,Is_Active,Is_Deleted,Created_By,Created_At,Custom_Attribute_5,Is_Approved)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",locationID,[locationDetails valueForKey:@"Location_Name"],locationType,[locationDetails valueForKey:@"Address"],[locationDetails valueForKey:@"City"],[locationDetails valueForKey:@"Telephone"], [locationDetails valueForKey:@"Latitude"],[locationDetails valueForKey:@"Longitude"],isActive,isDeleted,createdBy,createdAt,@"IPAD",[NSString isEmpty:isApproved]?[NSNull null]:isApproved,nil];
        }@catch (NSException *exception)
        {
            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
        }
        @finally
        {
            //[database close];
        }
    }
    else{
        //this is hospital location we need not add contact details for that, beacuse doctor is contact for hospital
        @try {
            locationCreatedSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Locations  (Location_ID,Location_Name,Location_Type,Address_1,City,Phone,Latitude,Longitude,Is_Active,Is_Deleted,Created_By,Created_At,Custom_Attribute_5)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",locationID,[locationDetails valueForKey:@"Location_Name"],locationType,[locationDetails valueForKey:@"Address"],[locationDetails valueForKey:@"City"],[locationDetails valueForKey:@"Telephone"], [locationDetails valueForKey:@"Latitude"],[locationDetails valueForKey:@"Longitude"],isActive,isDeleted,createdBy,createdAt,@"IPAD",nil];
        }@catch (NSException *exception)
        {
            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
        }
        @finally
        {
            //[database close];
        }
    }
    if ([locationType isEqualToString:@"D"])
    {
        
      

        if (locationCreatedSuccessfully==YES) {
            [database commit];
            
            [database close];
            
            return YES;
        }
        
        

        
        
    }
    
    
    
    else
    {
    
        //if app control flag FM_ENABLE_DR_APPROVAL is Y then is_Approved is N
        NSString* isApproved = @"";
        NSString* locationApproval= [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_LOCATION_APPROVAL];
        if ([locationApproval isEqualToString:KAppControlsYESCode])
        {
            isApproved = KAppControlsNOCode;
            @try {
                
                locationCreatedSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Locations  (Location_ID,Location_Name,Location_Type,Address_1,City,Phone,Latitude,Longitude,Is_Active,Is_Deleted,Created_By,Created_At,Custom_Attribute_5,Is_Approved)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",locationID,[locationDetails valueForKey:@"Location_Name"],locationType,[locationDetails valueForKey:@"Address"],[locationDetails valueForKey:@"City"],[locationDetails valueForKey:@"Telephone"], [locationDetails valueForKey:@"Latitude"],[locationDetails valueForKey:@"Longitude"],isActive,isDeleted,createdBy,createdAt,@"IPAD",[NSString isEmpty:isApproved]?[NSNull null]:isApproved,nil];
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                //[database close];
            }
        }
        else{
            @try {
                
                locationCreatedSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Locations  (Location_ID,Location_Name,Location_Type,Address_1,City,Phone,Latitude,Longitude,Is_Active,Is_Deleted,Created_By,Created_At,Custom_Attribute_5,Is_Approved)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",locationID,[locationDetails valueForKey:@"Location_Name"],locationType,[locationDetails valueForKey:@"Address"],[locationDetails valueForKey:@"City"],[locationDetails valueForKey:@"Telephone"], [locationDetails valueForKey:@"Latitude"],[locationDetails valueForKey:@"Longitude"],isActive,isDeleted,createdBy,createdAt,@"IPAD",nil];
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                //[database close];
            }
        }
    
   

    
    
    
    if (locationCreatedSuccessfully==YES) {
        
        //insert data to TBL_Contacts
        
        contactDataInsertedSuccessfully=YES;
        locationContactMapDataInsertedSuccessfully=YES;
        if([[[locationDetails valueForKey:@"ContactName"] trimString] length]>0)
        {
        @try {
            
            contactDataInsertedSuccessfully=[database executeUpdate:@"INSERT OR REPLACE INTO PHX_TBL_Contacts (Contact_ID,Contact_Name,Title,Address_1,City,Email,Phone,Is_Active,Is_Deleted,Created_At,Created_By,Custom_Attribute_1) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",contactID,[locationDetails valueForKey:@"ContactName"],[locationDetails valueForKey:@"ContactTitle"],[locationDetails valueForKey:@"Address"],[locationDetails valueForKey:@"City"],[locationDetails valueForKey:@"ContactEmail"],[locationDetails valueForKey:@"ContactTelephone"],isActive,isDeleted,createdAt,createdBy,@"IPAD",nil];

        }
        @catch (NSException *exception) {
            NSLog(@"Exception while obtaining data from database: %@", exception.reason);

        }
        @finally {
            
            
        }
        
        
        if (contactDataInsertedSuccessfully==YES) {
            
            // insert into PHX_TBL_Location_Contact_Map
            
            
            @try {
                
                locationContactMapDataInsertedSuccessfully=[database executeUpdate:@"INSERT OR REPLACE INTO PHX_TBL_Location_Contact_Map (Location_ID,Contact_ID,Created_At,Created_By) VALUES (?,?,?,?)",locationID,contactID,createdAt,createdBy,nil];
                
            }
            @catch (NSException *exception) {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                
            }
            @finally {
                
                
            }
            
            
        }
        
        }
        
        
    
        

        
        
        
        if (!locationCreatedSuccessfully || !contactDataInsertedSuccessfully || !locationContactMapDataInsertedSuccessfully) {
            
            //something went wrong rollback
            
            
            
            BOOL transactionActive=[database inTransaction];
            
            if (transactionActive) {
                NSLog(@"transaction is active");
            }
            
            else
            {
                NSLog(@"transaction is in active");
                
            }
            
            
            [database rollback];
            NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
            
            [database close];
            
            
            
            
            return NO;
        }
        
        else
        {
            //everything alright commit
            [database commit];
            
            [database close];
            
            return YES;
            
            
        }
        

        
     
        
        
        
        
    }
     
    
    }
    
}


+(NSMutableArray*)fetchLocationData
{
    NSString* locationsQry=@"select IFNULL(Location_ID,0) AS Location_ID, IFNULL(Location_Name,'N/A') AS Location_Name, IFNULL(Location_Type,'N/A') AS Location_Type, IFNULL(Location_Code,'N/A') AS Location_Code,IFNULL(Address_1,'N/A') AS Address_1, IFNULL(City,'N/A') AS City, IFNULL(State,'N/A') AS State, IFNULL(Email,'N/A') AS Email, IFNULL(Postal_Code,0) AS Postal_Code, IFNULL(Latitude,0)AS Latitude, IFNULL(Longitude,0) AS Longitude, IFNULL(Classification_2,'N/A')AS Class, IFNULL(Classification_3,'N/A')AS OTCRX, Case  when IFNULL(Is_Active,'N/A')  ='Y' then 'Active' when IFNULL(Is_Active,'N/A')  ='N' then 'InActive' END AS Is_Active, IFNULL(Is_Deleted,0) AS Is_Deleted from PHX_TBL_Locations";
    
    
    NSMutableArray* locationsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationsQry];
    
    if (locationsArray.count>0) {
        return locationsArray;
    }
    
    else
    {
        return nil;
    }
    
}


#pragma mark E Detailing product sample methods

+(NSMutableArray*)fetchSampelsForBrandAmbassadorVisit
{
    NSString* samplesQry=[NSString stringWithFormat:@"select SUM(B.Lot_Qty) AS Lot_Qty,ROW_ID AS Stock_Row_ID, IFNULL(A.Product_ID,'0') AS  Product_ID, IFNULL(A.Product_Name,'N/A') AS Product_Name, IFNULL(A.Product_Type,'N/A') AS  Product_Type, IFNULL(A.Product_Code,'N/A') AS Product_Code, IFNULL(A.Primary_UOM,'N/A') AS Primary_UOM, IFNULL(A.BarCode,'N/A') AS BarCode, IFNULL(A.Brand_Code,'N/A') AS Brand_Code,IFNULL(A.Agency,'N/A') AS Agency, IFNULL(A.List_Price,'0') AS List_Price, IFNULL(A.Net_Price,'0') AS Net_Price,IFNULL(A.cost_Price,'0') AS cost_Price, IFNULL(A.Item_Metadata,'') AS item_Metadata, IFNULL(B.Product_ID,'0') AS Product_ID,IFNULL(B.Emp_ID,'0') AS Emp_ID ,IFNULL(B.Lot_No,'N/A') AS Lot_No ,IFNULL(B.Expiry_Date,'0') AS Expiry_Date,  IFNULL(B.Used_Qty,'0') AS Used_Qty from PHX_TBL_EMP_Stock AS B inner join  PHX_TBL_Products  AS A  where A.Product_ID=B.Product_ID  and  A.Product_Type='S' and B. Expiry_Date > '%@'  group by A.Product_ID  order by A.Product_Name ASC",[self fetchDatabaseDateFormat]];
    
    
    
    NSMutableArray* samplesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:samplesQry];
    if (samplesArray.count>0) {
        
        
        return samplesArray;
    }
    
    else
    {
        return nil;
    }

    
}

+(NSMutableArray*)fetchSamplesforEdetailing:(NSString *)productType
{
    //modifying edetailing query to handle expired products in quey level
    
    NSString* currentDateinDBFormat=[self fetchDatabaseDateFormat];
    
    
    //removing and  A.Product_Type='S' to allow all products which has stock, as suggested by harpal on 2017-05-14.removing the expiry as suggested by harpal to allow pos materials as well
    
    NSString *samplesQuery;
    if ([[AppControl retrieveSingleton].ENABLE_ITEM_TYPE isEqualToString:KAppControlsNOCode]) {
        samplesQuery=[NSString stringWithFormat:@"select ROW_ID AS Stock_Row_ID, IFNULL(A.Product_ID,'0') AS  Product_ID, IFNULL(A.Product_Name,'N/A') AS Product_Name, IFNULL(A.Product_Type,'N/A') AS  Product_Type, IFNULL(A.Product_Code,'N/A') AS Product_Code, IFNULL(A.Primary_UOM,'N/A') AS Primary_UOM, IFNULL(A.BarCode,'N/A') AS BarCode, IFNULL(A.Brand_Code,'N/A') AS Brand_Code,IFNULL(A.Agency,'N/A') AS Agency, IFNULL(A.List_Price,'0') AS List_Price, IFNULL(A.Net_Price,'0') AS Net_Price,IFNULL(A.cost_Price,'0') AS cost_Price, IFNULL(A.Item_Metadata,'') AS item_Metadata, IFNULL(B.Product_ID,'0') AS Product_ID,IFNULL(B.Emp_ID,'0') AS Emp_ID ,IFNULL(B.Lot_No,'N/A') AS Lot_No ,IFNULL(B.Expiry_Date,'0') AS Expiry_Date, IFNULL(B.Lot_Qty,'0') AS Lot_Qty, IFNULL(B.Used_Qty,'0') AS Used_Qty from PHX_TBL_EMP_Stock AS B inner join  PHX_TBL_Products  AS A  where A.Product_ID=B.Product_ID  order by A.Product_Name ASC"];
    } else {
        samplesQuery=[NSString stringWithFormat:@"select ROW_ID AS Stock_Row_ID, IFNULL(A.Product_ID,'0') AS  Product_ID, IFNULL(A.Product_Name,'N/A') AS Product_Name, IFNULL(A.Product_Type,'N/A') AS  Product_Type, IFNULL(A.Product_Code,'N/A') AS Product_Code, IFNULL(A.Primary_UOM,'N/A') AS Primary_UOM, IFNULL(A.BarCode,'N/A') AS BarCode, IFNULL(A.Brand_Code,'N/A') AS Brand_Code,IFNULL(A.Agency,'N/A') AS Agency, IFNULL(A.List_Price,'0') AS List_Price, IFNULL(A.Net_Price,'0') AS Net_Price,IFNULL(A.cost_Price,'0') AS cost_Price, IFNULL(A.Item_Metadata,'') AS item_Metadata, IFNULL(B.Product_ID,'0') AS Product_ID,IFNULL(B.Emp_ID,'0') AS Emp_ID ,IFNULL(B.Lot_No,'N/A') AS Lot_No ,IFNULL(B.Expiry_Date,'0') AS Expiry_Date, IFNULL(B.Lot_Qty,'0') AS Lot_Qty, IFNULL(B.Used_Qty,'0') AS Used_Qty from PHX_TBL_EMP_Stock AS B inner join  PHX_TBL_Products  AS A  where A.Product_ID=B.Product_ID and Product_Type = '%@' order by A.Product_Name ASC ",productType];
    }
    NSLog(@"query for fetching samples are %@", samplesQuery);

    NSMutableArray* samplesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:samplesQuery];
    if (samplesArray.count > 0) {
        return samplesArray;
    }
    else {
        return nil;
    }
}

/*
 
 select IFNULL(Product_ID,'0') AS  Product_ID, IFNULL(Product_Name,'N/A') AS Product_Name, IFNULL(Product_Type,'N/A') AS  Product_Type, IFNULL(Product_Code,'N/A') AS Product_Code, IFNULL(Primary_UOM,'N/A') AS Primary_UOM, IFNULL(BarCode,'N/A') AS BarCode, IFNULL(Brand_Code,'N/A') AS Brand_Code,IFNULL(Agency,'N/A') AS Agency, IFNULL(List_Price,'0') AS List_Price, IFNULL(Net_Price,'0') AS Net_Price,IFNULL(cost_Price,'0') AS cost_Price, IFNULL(Item_Metadata,'N/A') AS item_Metadata from PHX_TBL_Products where Product_Type='S'*/


+(NSMutableArray *)fetchEmpStockwithEmpID:(NSString*)empID ProductID:(NSString*)productID

{
    
    //fetch stock only if expiry date is greater then today
    
    NSString* databaseDateFormat=[self fetchCurrentDateTimeinDatabaseFormat];
    
    
    
    //removing emp id link as we are getting stock only for activated emp id
    
    NSString* empStockDataQry=[NSString stringWithFormat:@"select IFNULL(Product_ID,'0') AS Product_ID,IFNULL(Emp_ID,'0') AS Emp_ID ,IFNULL(Lot_No,'N/A') AS Lot_No ,IFNULL(Expiry_Date,'0') AS Expiry_Date, IFNULL(Lot_Qty,'0') AS Lot_Qty, IFNULL(Used_Qty,'0') AS Used_Qty from PHX_TBL_EMP_Stock where  Product_ID='%@' and Expiry_Date > '%@' and  Lot_Qty > '0' ",productID,databaseDateFormat];
    
 //   NSLog(@"emp stock qry %@", empStockDataQry);
    
    NSMutableArray* empStockArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:empStockDataQry];
    
    if (empStockArray.count>0) {
        
        return empStockArray;
    }
    else
    {
        return nil;
    }
    
}



+(NSMutableArray*)fetchAccompaniedByData

{
    NSMutableArray* accompaniesbyArray=[[NSMutableArray alloc]init];
    NSString* accompaniedByQry=@"select IFNULL(User_ID,'N/A') AS User_ID, IFNULL(Username, 'N/A') AS Username, IFNULL(Password,'N/A') AS Password, IFNULL(PDA_Rights,'N/A') AS PDA_Rights, IFNULL(is_SS,'N/A') As is_SS from Tbl_Users_All Where Is_Coach='Y'";
    if ([AppControl.retrieveSingleton.USE_EMPNAME_IN_USERLIST isEqualToString:KAppControlsYESCode]) {
        accompaniedByQry = @"select IFNULL(User_ID,'N/A') AS User_ID, IFNULL(Emp_Name, 'N/A') AS Username, IFNULL(Password,'N/A') AS Password, IFNULL(PDA_Rights,'N/A') AS PDA_Rights, IFNULL(is_SS,'N/A') As is_SS from Tbl_Users_All Where Is_Coach='Y' order by Username COLLATE nocase ASC";
    }
    
    accompaniesbyArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:accompaniedByQry];
    NSMutableDictionary * noCoachDic=[[NSMutableDictionary alloc]init];
    [noCoachDic setObject:@"No Coach" forKey:@"Username"];
    [noCoachDic setObject:[NSString stringWithFormat:@"%d",0] forKey:@"User_ID"];
    

    if (accompaniesbyArray.count==0) {
        accompaniesbyArray=[[NSMutableArray alloc] init];

    }

    if ([[AppControl retrieveSingleton].FM_ENABLE_MULTISEL_ACCOMP_BY isEqualToString:KAppControlsYESCode]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Username = 'WithCoach'"];
        NSArray *filteredArray = [accompaniesbyArray filteredArrayUsingPredicate:predicate];
        
        if (filteredArray.count > 0) {
            [accompaniesbyArray removeObject:[filteredArray objectAtIndex:0]];
        }
    }
    [accompaniesbyArray insertObject:noCoachDic atIndex:0];
    
    NSLog(@"accompanied by array being retruned from db %@",accompaniesbyArray);
    if (accompaniesbyArray.count>0) {
        for (NSMutableDictionary *dictObj in accompaniesbyArray) {
            [dictObj setValue:KAppControlsNOCode forKey:@"isSelected"];
        }
        
        return accompaniesbyArray;
    }
    else
    {
        return nil;
    }
}

#pragma mark Notes



+(BOOL)InsertNoteObject:(VisitNotes*)notes andVisitDetails:(VisitDetails*)visitDetails
{
    
    if ([visitDetails.Location_Type isEqualToString:@"D"]) {
        
        
        [visitDetails.Contact_ID setValue:[NSNull null] forKey:@"Contact_ID"];
        
        
    }
    
    else if ([visitDetails.Location_Type isEqualToString:@"P"])
        
    {
        
        [visitDetails.Doctor_ID setValue:[NSNull null] forKey:@"Doctor_ID"];

    }
    
    
    NSString* noteID;
    
    if (notes.Note_ID!=nil) {
        
        noteID=notes.Note_ID;
        
    }
    else
    {
        
        noteID=[NSString createGuid];
    }
    NSString* title=notes.Title;
    NSString* description=notes.Title;
    NSString* category=@"Visit";
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    NSMutableDictionary * taskDict=[[NSMutableDictionary alloc]init];
    [taskDict setValue:noteID forKey:@"Note_ID"];
    
    NSMutableArray* noteDetailsArray=[[NSMutableArray alloc]init];
    
    noteDetailsArray=[[[NSUserDefaults standardUserDefaults]objectForKey:@"Updated_Notes"] mutableCopy];
    
    if (noteDetailsArray.count>0) {
        [noteDetailsArray addObject:taskDict];
        
    }
    else
    {
        noteDetailsArray=[[NSMutableArray alloc]init];
        [noteDetailsArray addObject:taskDict];
        
    }
    
    
    
    [[NSUserDefaults standardUserDefaults]setObject:noteDetailsArray forKey:@"Updated_Notes"];
    //this should be actual visit id as visits
    
    NSString* actualVisitID=notes.Visit_ID;
    
    //NSLog(@"actual visit id in notes db %@", actualVisitID);
    
    
    NSString* visitID=visitDetails.Planned_Visit_ID;
    
    NSString* location_ID;
    if (notes.Location_ID!=nil) {
        
        location_ID=notes.Location_ID;
    }
    else
    {
        location_ID=@"";
    }
    
    NSString* doctorID=notes.Doctor_ID;
    
    NSString* contactID=notes.Contact_ID;
    
    
    
    if ([doctorID isEqual:[NSNull null]]) {
        
    }
    
    else if ([doctorID isEqualToString:@"0"])
        
    {
       // [visitDetailsDict setObject:[NSNull null] forKey:@"Doctor_ID"];
        
        
        [visitDetails.Doctor_ID setValue:[NSNull null] forKey:@"Doctor_ID"];

        
    }
    
    if ([contactID isEqual:[NSNull null]]) {
        
    }
    
    else if ([contactID isEqualToString:@"0"])
        
    {
        //[visitDetailsDict setObject:[NSNull null] forKey:@"Contact_ID"];
        
        [visitDetails.Contact_ID setValue:[NSNull null] forKey:@"Contact_ID"];

        
    }
    
    
    contactID=visitDetails.Contact_ID;
    
    doctorID=visitDetails.Doctor_ID;;
    
    
    
    //NSLog(@"doctor ID and Contact ID before inserting notes %@ %@", doctorID,contactID);
    
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    
    
    //created by should be user id as suggested by peram on 10.2.2016
    
    
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    
    
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    
    
    BOOL success = NO;
    
    @try {
        
        success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_EMP_NOTES  (Note_ID,Title, Description,Category,Emp_ID,Visit_ID,Location_ID,Doctor_ID,Contact_ID,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?,?,?,?)",noteID,title,description,category,empID,actualVisitID,location_ID,doctorID,[visitDetails valueForKey:@"Contact_ID"],createdAt,createdBy, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    
    
    if (success==YES) {
        NSLog(@"NOTES CREATED SUCCESSFULLY");
        
        
    }
    
    return success;

}

+(BOOL)InsertNotes:(NSMutableDictionary*)notesDetails andVisitDetails:(NSMutableDictionary*)visitDetailsDict

{
    
    
    if ([[notesDetails valueForKey:@"Location_Type"]isEqualToString:@"D"]) {
        
        
        [visitDetailsDict setValue:[NSNull null] forKey:@"Contact_ID"];
        
    }
    
    else if ([[notesDetails valueForKey:@"Location_Type"]isEqualToString:@"P"])
        
    {
        [visitDetailsDict setValue:[NSNull null] forKey:@"Doctor_ID"];

    }
    
    
    NSString* noteID;
    
    if ([notesDetails valueForKey:@"Note_ID"]!=nil) {
        
        noteID=[notesDetails valueForKey:@"Note_ID"];
        
    }
    else
    {
        
        noteID=[NSString createGuid];
    }
    NSString* title=[notesDetails valueForKey:@"Title"];
    NSString* description=[notesDetails valueForKey:@"Description"];
    NSString* category=@"Visit";
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
   
    NSMutableDictionary * taskDict=[[NSMutableDictionary alloc]init];
    [taskDict setValue:noteID forKey:@"Note_ID"];
    
    NSMutableArray* noteDetailsArray=[[NSMutableArray alloc]init];
    
    noteDetailsArray=[[[NSUserDefaults standardUserDefaults]objectForKey:@"Updated_Notes"] mutableCopy];
    
    if (noteDetailsArray.count>0) {
        [noteDetailsArray addObject:taskDict];
        
    }
    else
    {
        noteDetailsArray=[[NSMutableArray alloc]init];
        [noteDetailsArray addObject:taskDict];
        
    }
    
    
    
    [[NSUserDefaults standardUserDefaults]setObject:noteDetailsArray forKey:@"Updated_Notes"];
    //this should be actual visit id as visits
    
    NSString* actualVisitID=[notesDetails valueForKey:@"Actual_Visit_ID"];
    
   //NSLog(@"actual visit id in notes db %@", actualVisitID);
    
    
    NSString* visitID=[visitDetailsDict valueForKey:@"Planned_Visit_ID"];
    NSString* location_ID;
    if ([visitDetailsDict valueForKey:@"Location_ID"]!=nil) {
        
        location_ID=[visitDetailsDict valueForKey:@"Location_ID"];
    }
    else
    {
        location_ID=@"";
    }
    
    NSString* doctorID=[visitDetailsDict valueForKey:@"Doctor_ID"];
    
    NSString* contactID=[visitDetailsDict valueForKey:@"Contact_ID"];
    
    
    
    if ([doctorID isEqual:[NSNull null]]) {
        
    }
    
    else if ([doctorID isEqualToString:@"0"])
        
    {
        [visitDetailsDict setObject:[NSNull null] forKey:@"Doctor_ID"];
        
    }
    
    if ([contactID isEqual:[NSNull null]]) {
        
    }
    
    else if ([contactID isEqualToString:@"0"])
        
    {
        [visitDetailsDict setObject:[NSNull null] forKey:@"Contact_ID"];
        
    }
    
    
    contactID=[visitDetailsDict valueForKey:@"Contact_ID"];
    
    doctorID=[visitDetailsDict valueForKey:@"Doctor_ID"];
    
    
    
   //NSLog(@"doctor ID and Contact ID before inserting notes %@ %@", doctorID,contactID);
  
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    
    
    //created by should be user id as suggested by peram on 10.2.2016
    
    
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    
    
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    
    
    BOOL success = NO;
    
    @try {
        
        success =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_EMP_NOTES  (Note_ID,Title, Description,Category,Emp_ID,Visit_ID,Location_ID,Doctor_ID,Contact_ID,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?,?,?,?)",noteID,title,description,category,empID,actualVisitID,location_ID,doctorID,contactID,createdAt,createdBy, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    
    
    if (success==YES) {
        NSLog(@"NOTES CREATED SUCCESSFULLY");
        
        
    }
    
    return success;
    
}


+(NSMutableArray*)fetchNoteswithLocationID:(NSString*)locationID

{
    
    
    NSString * notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@' ",locationID ];
    
   //NSLog(@"query for notes is %@", notesQry);
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    if (notesArray.count>0) {
        
        return notesArray;
        
    }
    else
    {
        return nil;
    }

}


#pragma mark Note Object Methods


+(NSMutableArray*)fetchNoteObjectsforHospital:(NSString*)locationID
{
    NSString * notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By,IFNULL(Last_Updated_By,'N/A') AS Last_Updated_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@' ",locationID ];
    
    //NSLog(@"query for notes is %@", notesQry);
    
    NSMutableArray* notesDataArray=[[NSMutableArray alloc]init];
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    if (notesArray.count>0) {
        VisitNotes * notes;
        for (NSInteger i=0; i<notesArray.count; i++) {
            
            notes=[[VisitNotes alloc]init];
            NSMutableDictionary * currentDict=[notesArray objectAtIndex:i];
            notes.Note_ID=[currentDict valueForKey:@"Note_ID"];
            notes.Title=[currentDict valueForKey:@"Title"];
            notes.Description=[currentDict valueForKey:@"Description"];
            notes.Category=[currentDict valueForKey:@"Category"];
            notes.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            notes.Visit_ID=[currentDict valueForKey:@"Visit_ID"];
            notes.Doctor_ID=[currentDict valueForKey:@"Doctor_ID"];
            notes.Contact_ID=[currentDict valueForKey:@"Contact_ID"];
            notes.Created_At=[currentDict valueForKey:@"Created_At"];
            notes.Created_By=[currentDict valueForKey:@"Created_By"];
            notes.Last_Updated_By=[currentDict valueForKey:@"Last_Updated_By"];
            [notesDataArray addObject:notes];
            
        }
        return notesDataArray;
        
    }
    else
    {
        return nil;
    }

}

+(NSMutableArray*)fetchNoteObjectwithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID
{
    
    
    NSString * notesQry;
    
    if([[SWDefaults getValidStringValue:doctorID]length]>0 && [[SWDefaults getValidStringValue:locationID]length]>0)
    {
        notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By,IFNULL(Last_Updated_By,'N/A') AS Last_Updated_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@' and Doctor_ID='%@'",locationID ,doctorID];
        
    }
    
    if([[SWDefaults getValidStringValue:doctorID] isEqualToString:@""])
    {
        notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By,IFNULL(Last_Updated_By,'N/A') AS Last_Updated_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@'",locationID];
        
    }
    else
    {
        notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By,IFNULL(Last_Updated_By,'N/A') AS Last_Updated_By FROM PHX_Tbl_Emp_Notes where Doctor_ID='%@'" ,doctorID];
    }

    
    //NSLog(@"query for notes is %@", notesQry);
    
    NSMutableArray* notesDataArray=[[NSMutableArray alloc]init];
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    if (notesArray.count>0) {
        VisitNotes * notes;
        for (NSInteger i=0; i<notesArray.count; i++) {
            
            notes=[[VisitNotes alloc]init];
            NSMutableDictionary * currentDict=[notesArray objectAtIndex:i];
            notes.Note_ID=[currentDict valueForKey:@"Note_ID"];
            notes.Title=[currentDict valueForKey:@"Title"];
            notes.Description=[currentDict valueForKey:@"Description"];
            notes.Category=[currentDict valueForKey:@"Category"];
            notes.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            notes.Visit_ID=[currentDict valueForKey:@"Visit_ID"];
            notes.Doctor_ID=[currentDict valueForKey:@"Doctor_ID"];
            notes.Contact_ID=[currentDict valueForKey:@"Contact_ID"];
            notes.Created_At=[currentDict valueForKey:@"Created_At"];
            notes.Created_By=[currentDict valueForKey:@"Created_By"];
            notes.Last_Updated_By=[currentDict valueForKey:@"Last_Updated_By"];
            [notesDataArray addObject:notes];
            
        }
        return notesDataArray;
        
    }
    else
    {
        return nil;
    }
    
}



+(NSMutableArray*)fetchNotesObjectforPharmacy:(NSString*)locationID
{
    NSString * notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@'  ",locationID ];
    
    //NSLog(@"query for notes is %@", notesQry);
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    NSMutableArray* notesDataArray=[[NSMutableArray alloc]init];
    if (notesArray.count>0) {
        VisitNotes * notes;
        for (NSInteger i=0; i<notesArray.count; i++) {
            
            notes=[[VisitNotes alloc]init];
            NSMutableDictionary * currentDict=[notesArray objectAtIndex:i];
            notes.Note_ID=[currentDict valueForKey:@"Note_ID"];
            notes.Title=[currentDict valueForKey:@"Title"];
            notes.Description=[currentDict valueForKey:@"Description"];
            notes.Category=[currentDict valueForKey:@"Category"];
            notes.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            notes.Visit_ID=[currentDict valueForKey:@"Visit_ID"];
            notes.Doctor_ID=[currentDict valueForKey:@"Doctor_ID"];
            notes.Contact_ID=[currentDict valueForKey:@"Contact_ID"];
            notes.Created_At=[currentDict valueForKey:@"Created_At"];
            notes.Created_By=[currentDict valueForKey:@"Created_By"];
            notes.Last_Updated_By=[currentDict valueForKey:@"Last_Updated_By"];
            [notesDataArray addObject:notes];
            
        }
        return notesDataArray;
        
    }
    else
    {
        return nil;
    }

}

+(NSMutableArray*)fetchNotesObjectswithLocationID:(NSString*)locationID andContactID:(NSString*)contactID

{
    NSString * notesQry;
    
    if([[SWDefaults getValidStringValue:contactID]length]>0 && [[SWDefaults getValidStringValue:locationID]length]>0)
    {
        notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@' and Contact_ID='%@'",locationID ,contactID];
    }
    
    if([[SWDefaults getValidStringValue:contactID]isEqualToString:@""])
    {
        notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@'",locationID];

    }
    else
    {
        notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where  Contact_ID='%@'",contactID];
  
    }
    
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    NSMutableArray* notesDataArray=[[NSMutableArray alloc]init];
    if (notesArray.count>0) {
        VisitNotes * notes;
        for (NSInteger i=0; i<notesArray.count; i++) {
            
            notes=[[VisitNotes alloc]init];
            NSMutableDictionary * currentDict=[notesArray objectAtIndex:i];
            notes.Note_ID=[currentDict valueForKey:@"Note_ID"];
            notes.Title=[currentDict valueForKey:@"Title"];
            notes.Description=[currentDict valueForKey:@"Description"];
            notes.Category=[currentDict valueForKey:@"Category"];
            notes.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            notes.Visit_ID=[currentDict valueForKey:@"Visit_ID"];
            notes.Doctor_ID=[currentDict valueForKey:@"Doctor_ID"];
            notes.Contact_ID=[currentDict valueForKey:@"Contact_ID"];
            notes.Created_At=[currentDict valueForKey:@"Created_At"];
            notes.Created_By=[currentDict valueForKey:@"Created_By"];
            notes.Last_Updated_By=[currentDict valueForKey:@"Last_Updated_By"];
            [notesDataArray addObject:notes];
            
        }
        return notesDataArray;
        
    }
    else
    {
        return nil;
    }
    
}


+(NSMutableArray*)fetchNoteswithLocationID:(NSString*)locationID andDoctorID:(NSString*)doctorID

{
    NSString * notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@' and Doctor_ID='%@'",locationID ,doctorID];
    
   //NSLog(@"query for notes is %@", notesQry);
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    if (notesArray.count>0) {
        
        return notesArray;
        
    }
    else
    {
        return nil;
    }
    
}

+(NSMutableArray*)fetchNoteswithLocationID:(NSString*)locationID andContactID:(NSString*)contactID

{
    NSString * notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where Location_ID='%@' and Contact_ID='%@'",locationID ,contactID];
    
   //NSLog(@"query for notes is %@", notesQry);
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    if (notesArray.count>0) {
        
        return notesArray;
        
    }
    else
    {
        return nil;
    }

}

+(NSMutableArray*)fetchNoteswithVisitID:(NSString*)visitID

{
    NSString * notesQry=[NSString stringWithFormat:@"SELECT IFNULL(Note_ID,'0') AS Note_ID,IFNULL(Title,'N/A') AS Title,IFNULL(Description,'N/A') AS Description,IFNULL(Category,'N/A') AS Category,IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Visit_ID,'N/A') AS Visit_ID,IFNULL(Location_ID,'N/A') AS Location_ID,IFNULL(Doctor_ID,'N/A') AS Doctor_ID,IFNULL(Contact_ID,'N/A') AS Contact_ID,IFNULL(Created_At,'N/A') AS Created_At , IFNULL(Created_By,'N/A') AS Created_By FROM PHX_Tbl_Emp_Notes where Visit_ID='%@'",visitID ];
    NSMutableArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:notesQry];
    if (notesArray.count>0) {
        
        return notesArray;
        
    }
    else
    {
        return nil;
    }
    
}


+(NSMutableArray*)fetchVisitEndDateFromActualVisits:(NSString*)plannedVisitID

{
    NSString* endDateQry=[NSString stringWithFormat:@"Select IFNULL(Visit_End_Date,'0') AS Visit_End_Date from PHX_TBL_Actual_Visits where Location_ID='%@' order by  Visit_End_Date desc", plannedVisitID];
    
    NSMutableArray* endDateArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:endDateQry];
    if (endDateArray.count>0) {
        return endDateArray;
    }
    else
    {
        return nil;
    }
}


+(BOOL)isSurveyCompletedForLocation:(NSString*)locationID andSurveyID:(NSString*)selectedSurveyID
{
    BOOL completed=NO;
    NSString* qryString = [NSString stringWithFormat:@"select Attrib_Value  AS Survey_ID from TBL_Visit_Addl_Info AS A inner join phx_tbl_actual_visits AS B on A.Visit_ID = B.Actual_Visit_ID where A.Attrib_name = 'Survey_ID' and B.Call_Ended_At like '%%%@%%' and B.Location_ID = '%@'",[self fetchDatabaseDateFormat],locationID];
    NSLog(@"location survey qry %@", qryString);
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:qryString];
    if (contentArray.count>0) {
        
        NSPredicate* surveyIDPredicate=[NSPredicate predicateWithFormat:@"SELF.Survey_ID == %@",selectedSurveyID];
        NSArray* filterArray=[contentArray filteredArrayUsingPredicate:surveyIDPredicate];
        if (filterArray.count>0) {
            completed=YES;
        }
    }
    else{
        completed=NO;
    }
    return completed;
}

#pragma mark Save Visit

+(BOOL)saveVisitDetails:(NSMutableDictionary*)visitDetails withProductsDemonstrated:(NSMutableArray*)productsDemonstrated andSamplesGiven:(NSMutableArray*)samplesGiven withAdditionalInfo:(NSMutableDictionary*)additionalInfo andVisitDetails:(VisitDetails*)currentVisitDetails
{
    BOOL insertIntoPlannedVisits = NO;
    BOOL insertIntoActualVisits = NO;
    BOOL insertIntoIssueNote = NO;
    BOOL insertIntoIssueNoteLineItems=NO;
    BOOL insertIntoEDetailing=NO;
    BOOL insertIntoVisitAddlInfoMeetingAttendees=NO;
    BOOL insertIntoVisitAddlInfoRating=NO;
    BOOL insertIntoVisitAdditionalInfoAccopaniedBy=YES;
    BOOL insertIntoVisitAdditionalInfo=NO;
    BOOL insertIntoVisitAdditionalInfoNotes=NO;
    BOOL insertIntoVisitAdditionalInfoLocationReason = NO;
    BOOL insertIntoVisitAdditionalInfoNextVisitObjective = NO;
    
    //coach survey
    BOOL insertSurveySuccessfull = NO;
    BOOL insertSurveyConfirmationSuccessfull = NO;
    BOOL insertIntoSurveyVisitAdditionalInfo = NO;
    BOOL updatedTasksSuccessfully=NO;
    
    /*saving the visit details, data to be inserted to the follwoing tables
     
     PHX_TBL_Actual_Visits
     PHX_Tbl_Edetailing- for products demonstarated
     
     PHX_TBL_Issue_Note-for samples given
     PHX_TBL_Issue_Note_Line_Items-for samples given (more like order and line items)
     
     
     TBL_Visit_Addl_Info- attribute name “TASK”
     ATTRIB VALUE TASK-ID
     CA1 Type of Action C-Created/M-modified, and meeting attendies Attrib name and value,and custom attribute_1 for rating
     */
    
    
    //check if a note has been created , if yes there it will have actual visit id use the same
    [MedRepQueries updateStockQuantity:currentVisitDetails.samplesArray];
    
    NSString* userID=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    NSString* actualVisitID=[NSString createGuid];
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    //in case of visiting pharmacy doctor id will be 0, and while sync unique identifer issue will occur so insert null
    
    [visitDetails setValue:currentVisitDetails.selectedContact.Contact_ID forKey:@"Contact_ID"];
    [visitDetails setValue:currentVisitDetails.selectedDoctor.Doctor_ID forKey:@"Doctor_ID"];
    
    
    if ([[visitDetails valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
    }
    else if ([[visitDetails valueForKey:@"Location_Type"]isEqualToString:@"D"]) {
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
    }
    
    
    if ([[visitDetails valueForKey:@"Doctor_ID"] isEqual:[NSNull null]]) {
    }
    else if ([[visitDetails valueForKey:@"Doctor_ID"] isEqualToString:@""] || [[visitDetails valueForKey:@"Doctor_ID"] isEqualToString:@"0"]) {
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
    }
    
    if ([[visitDetails valueForKey:@"Contact_ID"] isEqual:[NSNull null]]) {
    }
    else if ([[visitDetails valueForKey:@"Contact_ID"] isEqualToString:@"0"]||[[visitDetails valueForKey:@"Contact_ID"] isEqualToString:@""]) {
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
    }

    
    NSString *lastOrderReference=[[NSString alloc]init];
    
    NSString* plannedVisit_ID=[visitDetails valueForKey:@"Planned_Visit_ID"];
    
    NSString* callStartDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_Start_Date"];
    
    NSString* callEndDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_End_Date"];
    
    NSString* edetailingStartTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"EDetailing_Start_Time"];
    
    NSString* edetailingEndTime =[[NSUserDefaults standardUserDefaults]objectForKey:@"EDetailing_End_Time"];
    
    NSString* assignedTo=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    
    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSMutableArray *updatedTasksArray=[[NSMutableArray alloc]init];
    
    BOOL isLocationEnabled = true;
    if ([CLLocationManager locationServicesEnabled]) {
        
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusDenied:
                isLocationEnabled = false;
                break;
            case kCLAuthorizationStatusRestricted:
                isLocationEnabled = false;
                break;
            case kCLAuthorizationStatusAuthorizedAlways:
                
                break;
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                
                break;
            case kCLAuthorizationStatusNotDetermined:
                isLocationEnabled = false;
                break;
            default:
                break;
        }
    } else {
        isLocationEnabled = false;
    }
    NSString* latitide;
    NSString* longitude;
    
    if (isLocationEnabled) {
        latitide=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude];
        longitude=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude];
    } else {
        latitide=@"0";
        longitude=@"0";
    }
    
    if (latitide.length>0) {
        
    }
    else
    {
        latitide=@"0";
        longitude=@"0";
    }
    NSString* notesStr=[visitDetails valueForKey:@"Visit_Conclusion_Notes"];

    
    //accompanies by should be user id as suggested by param on 10/2/2016
    
    NSString *accompaniedByStr=[visitDetails valueForKey:@"Accompanied_By"];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    
    
    //TO BE DONE CHECK WITH HARPAL
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    
    @try {
        if ([[[AppControl retrieveSingleton] FM_ENABLE_MULTISEL_ACCOMP_BY] isEqualToString:KAppControlsYESCode]) {
            insertIntoActualVisits =  [database executeUpdate:@"INSERT INTO PHX_TBL_Actual_Visits (Actual_Visit_ID,EMP_ID, Location_ID,Doctor_ID,Contact_ID,Planned_Visit_ID,Visit_Start_Date,Visit_End_Date,Latitude,Longitude,Notes,Call_Started_At,Call_Ended_At,Custom_Attribute_2,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",actualVisitID,empID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],plannedVisit_ID,callStartDateandTime,callEndDateandTime,latitide,longitude,notesStr,edetailingStartTime,edetailingEndTime,[SWDefaults getValidStringValue:appDelegate.currentLocationCapturedTime],currentVisitDetails.selectedDemoPlan.Demo_Plan_ID,nil];
        } else {
            insertIntoActualVisits =  [database executeUpdate:@"INSERT INTO PHX_TBL_Actual_Visits (Actual_Visit_ID,EMP_ID, Location_ID,Doctor_ID,Contact_ID,Planned_Visit_ID,Visit_Start_Date,Visit_End_Date,Latitude,Longitude,Notes,Accompanied_By,Call_Started_At,Call_Ended_At,Custom_Attribute_2,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",actualVisitID,empID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],plannedVisit_ID,callStartDateandTime,callEndDateandTime,latitide,longitude,notesStr,accompaniedByStr,edetailingStartTime,edetailingEndTime, [SWDefaults getValidStringValue:appDelegate.currentLocationCapturedTime], currentVisitDetails.selectedDemoPlan.Demo_Plan_ID,nil];
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        // [database close];
    }
    
    if (insertIntoActualVisits==YES) {
        
        if ([[[AppControl retrieveSingleton] FM_ENABLE_MULTISEL_ACCOMP_BY] isEqualToString:KAppControlsYESCode]) {
            // add accompanied by in to table TBL_Visit_Addl_Info
            @try {
                
                insertIntoVisitAdditionalInfoAccopaniedBy =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID, Visit_ID, Attrib_Name, Attrib_Value, Created_At, Created_By)  VALUES (?,?,?,?,?,?)",[NSString createGuid], actualVisitID, @"Accompanied_BY", accompaniedByStr, createdAt, createdBy, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                // [database close];
            }
        }
        
        //insert tasks here
        //refined the array to fetch only those which are updated
        
        NSPredicate * updatedTasksPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
        updatedTasksArray=[[currentVisitDetails.taskArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
        NSLog(@"updated tasks are %@", updatedTasksArray);
        
        if (updatedTasksArray.count>0) {
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                VisitTask * currentTask=[updatedTasksArray objectAtIndex:i];
                
                NSMutableDictionary * updationInfoDict=[[NSMutableDictionary alloc]init];
                
                [updationInfoDict setValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"Last_Updated_At"];
                [updationInfoDict setValue:userID forKey:@"Last_Updated_By"];
                
                if ([currentTask.Status isEqualToString:@"New"]) {
                    
                    [updationInfoDict setValue:[NSNull null] forKey:@"Last_Updated_At"];
                    [updationInfoDict setValue:[NSNull null] forKey:@"Last_Updated_By"];
                }
                
                if ([currentTask.Status isEqualToString:@"Closed"]) {
                    [updationInfoDict setValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"End_Time"];
                }
                else
                {
                    [updationInfoDict setValue:[NSNull null] forKey:@"End_Time"];
                }
                
                @try {
                    updatedTasksSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Tasks (Task_ID,Title, Description,Category,Priority,Start_Time,Status,Assigned_To,Emp_ID,Location_ID,Show_Reminder,IS_Active,Is_deleted,Created_At,Created_By,Doctor_ID,Contact_ID,End_Time,Last_Updated_At,Last_Updated_By,Custom_Attribute_1,End_Time)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",currentTask.Task_ID, currentTask.Title==nil?@"":currentTask.Title,currentTask.Description==nil?@"":currentTask.Description,currentTask.Category==nil?@"":currentTask.Category,currentTask.Priority==nil?@"":currentTask.Priority, currentTask.Start_Time,currentTask.Status==nil?@"":currentTask.Status,assignedTo,assignedTo,currentTask.Location_ID==nil?@"":currentTask.Location_ID,@"N",@"Y",@"N",currentTask.Start_Time,createdBy,[NSString isEmpty:[visitDetails valueForKey:@"Doctor_ID"]]?[NSNull null]: [visitDetails valueForKey:@"Doctor_ID"],[NSString isEmpty:[visitDetails valueForKey:@"Contact_ID"]]?[NSNull null]: [visitDetails valueForKey:@"Contact_ID"],currentTask.End_Time,[updationInfoDict valueForKey:@"Last_Updated_At"],[updationInfoDict valueForKey:@"Last_Updated_By"],@"IPAD",[updationInfoDict valueForKey:@"End_Time"],nil];
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
            }
            NSLog(@"TASKS INSERTED SUCCESSFULLY");
        }
        
        NSPredicate *updatedNotesPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
        NSMutableArray *updatedNotesArray=[[currentVisitDetails.notesArray filteredArrayUsingPredicate:updatedNotesPredicate] mutableCopy];
        NSLog(@"updated notes are %@", updatedNotesArray);
        
        for (NSInteger i=0; i<updatedNotesArray.count; i++) {
            
            VisitNotes * currentNote=[updatedNotesArray objectAtIndex:i];
            BOOL updatedNotesSuccessfully = NO;
            
            @try {
                updatedNotesSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_EMP_NOTES  (Note_ID,Title, Description,Category,Emp_ID,Visit_ID,Location_ID,Doctor_ID,Contact_ID,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?,?,?,?)",currentNote.Note_ID,currentNote.Title,currentNote.Description,@"Visit",currentNote.Emp_ID,actualVisitID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],currentNote.Created_At,currentNote.Created_By, nil];
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                //[database close];
            }
        }

        NSLog(@"DATA INSERTED INTO ACTUAL VISITS SUCCESSFULLY");
        
        //now insert samples given to issue notes
        NSString* rowID=[NSString createGuid];

        NSString *nUserId = [NSString stringWithFormat:@"%@",[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];
        if(nUserId.length == 1)
        {nUserId = [NSString stringWithFormat:@"00%@",nUserId];}
        else if(nUserId.length == 2)
        {nUserId = [NSString stringWithFormat:@"0%@",nUserId];}
        
        if([samplesGiven count]>0)
        {
            //insert only if samples given
            
            lastOrderReference = [MedRepDefaults lastIssueNoteReference];
            unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 10] longLongValue];
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
            
            if (![lastOrderReference isEqualToString:[MedRepDefaults lastIssueNoteReference]])
            {
                [MedRepDefaults setIssueNoteReference:lastOrderReference];
            }
            else
            {
                lastRefId = lastRefId + 1;
                lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
                if (![lastOrderReference isEqualToString:[MedRepDefaults lastIssueNoteReference]])
                {
                    [MedRepDefaults setIssueNoteReference:lastOrderReference];
                }
                else
                {
                    lastRefId = lastRefId + 1;
                    lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
                    [MedRepDefaults setIssueNoteReference:lastOrderReference];
                }
            }
            
            @try {
                
                insertIntoIssueNote =  [database executeUpdate:@"INSERT INTO PHX_TBL_Issue_Note (Row_ID,Doc_No, Doc_Type,Emp_ID,Location_ID,Doctor_ID,Contact_ID,Visit_ID,Start_Time,End_Time,Status,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",rowID,lastOrderReference,@"S",empID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],actualVisitID,callStartDateandTime,callEndDateandTime,@"N",createdAt,createdBy, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                //  [database close];
            }
        }
        else {
            insertIntoIssueNote=YES;
        }
        
        
        if (insertIntoIssueNote==YES) {
            
            NSLog(@"INSERTED SUCCESSFULLY TO ISSUE NOTES");
            
            //now insert into issue note line items
            if([samplesGiven count]>0)
            {
                NSInteger line = 0;
                for (SampleProductClass *tempProduct in samplesGiven) {
                    
                    line = line+1;
                    
                    NSString *rowID = [NSString createGuid];
                    NSString *docNo = [NSString stringWithFormat:@"%@", lastOrderReference];
                    NSString *lineNumber = [NSString stringWithFormat:@"%ld", (long)line];
                    
                    NSString *productIDStr = tempProduct.ProductId;
                    NSString *lotNumber = tempProduct.productLotNumber;
                    NSString *qty = tempProduct.QuantityEntered;
                    NSString *itemType = tempProduct.ItemType;
                    
                    //TO BE DONE NO UOM IN UI CHECK WITH HARPAL
                    NSString *uom = @"EA";
                    
                    @try {
                        
                        insertIntoIssueNoteLineItems =  [database executeUpdate:@"INSERT INTO PHX_TBL_Issue_Note_line_Items (Row_ID,Doc_No, Line_Number,Product_ID,Lot_No,Issue_Qty,Issue_UOM, Custom_Attribute_1)  VALUES (?,?,?,?,?,?,?,?)",rowID,docNo,lineNumber,productIDStr, [lotNumber isEqualToString:@"N/A"] ? [NSNull null]:lotNumber,qty,uom, itemType, nil];
                        
                    }@catch (NSException *exception)
                    {
                        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                    }
                    @finally
                    {
                        // [database close];
                    }
                }
            }
            else
            {
                insertIntoIssueNoteLineItems=YES;
            }
            
            
            if (insertIntoIssueNoteLineItems==YES) {
                
                NSLog(@"Issue Line Item success");
                
                //now insert PHX_Tbl_Edetailing- for products demonstarated
                if(productsDemonstrated.count>0){
                    for (NSInteger i=0; i<productsDemonstrated.count; i++) {
                        
                        ProductMediaFile * currentMediaFile=[productsDemonstrated objectAtIndex:i];
                        NSString *rowID=[NSString createGuid];
                        NSString* mediaFileID=[NSString getValidStringValue:currentMediaFile.Media_File_ID];
                        NSString* productID=[NSString getValidStringValue:currentMediaFile.Entity_ID_1];
                        NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
                        NSString* viewedTime =[NSString getValidStringValue:currentMediaFile.viewedTime];
                        NSString* discussedAt =[NSString getValidStringValue:currentMediaFile.discussedAt].length == 0 ? callStartDateandTime : [NSString getValidStringValue:currentMediaFile.discussedAt];
                        
                        @try {
                            
                            insertIntoEDetailing =  [database executeUpdate:@"INSERT INTO PHX_TBL_EDetailing (Row_ID,Visit_ID, Product_ID,Media_File_ID,Discussed_At,EMP_ID,Location_ID,Doctor_ID,Contact_ID,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?)",rowID,actualVisitID,productID,mediaFileID,discussedAt,empID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],viewedTime, nil];
                            
                        }@catch (NSException *exception)
                        {
                            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                        }
                        @finally
                        {
                            // [database close];
                        }
                    }
                }else{
                    insertIntoEDetailing=YES;
                }
                
                
                if (insertIntoEDetailing==YES) {
                    
                    NSLog(@"EDETAILING INSERT SUCCESSFULL");
                    
                    //now insert visit addl info details
                    /*
                     TBL_Visit_Addl_Info- attribute name “TASK”
                     ATTRIB VALUE TASK-ID
                     CA1 Type of Action C-Created/M-modified, and meeting attendies Attrib name and value,and custom attribute_1 for rating
                     */
                    if (updatedTasksArray.count>0) {
                        for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                            
                            VisitTask * task=[updatedTasksArray objectAtIndex:i];
                            NSString* rowID=[NSString createGuid];
                            NSString* attributeName=@"Task";
                            NSString* attributeValue=task.Task_ID;
                            NSString* taskStatus=task.Status;
                            
                            @try {
                                
                                insertIntoVisitAdditionalInfo =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Custom_Attribute_1,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?)",rowID,actualVisitID,attributeName,attributeValue,taskStatus,createdAt,createdBy, nil];
                                
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        }
                        
                        //after updating in visit addl info update the status in task table as well.
                        BOOL updatetaskStatus=NO;
                        
                        
                        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
                        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        [formatterTime setLocale:usLocaleq];
                        NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
                        
                        for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                            VisitTask * task=[updatedTasksArray objectAtIndex:i];
                            
                            @try {
                                
                                updatetaskStatus =  [database executeUpdate:@"Update  PHX_TBL_Tasks set Status = ?, Last_Updated_At=?, Last_Updated_By=?  where Task_ID='%@'",task.Status,createdAt,createdBy,task.Task_ID, nil];
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        }
                    }
                    else
                    {
                        insertIntoVisitAdditionalInfo=YES;
                    }
                    
                    //update notes data same as task
                    if (updatedNotesArray.count>0) {
                        for (NSInteger i=0; i<updatedNotesArray.count; i++) {
                            
                            VisitNotes * note=[[VisitNotes alloc]init];
                            note=[updatedNotesArray objectAtIndex:i];
                            
                            NSString* rowID=[NSString createGuid];
                            NSString* attributeName=@"Notes";
                            NSString* attributeValue=note.Note_ID;
                            
                            @try {
                                
                                insertIntoVisitAdditionalInfoNotes =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,note.Visit_ID,attributeName,attributeValue,createdAt,createdBy, nil];
                                
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        }
                    }
                    else
                    {
                        //if no notes are available
                        insertIntoVisitAdditionalInfoNotes=YES;
                    }
                    
                    if ( insertIntoVisitAdditionalInfo==YES && insertIntoVisitAdditionalInfoNotes==YES) {
                        
                        if (insertIntoVisitAdditionalInfoNotes==YES) {
                            NSLog(@"Notes array insert successful in visit");
                        }
                        
                        if ([[visitDetails valueForKey:@"Visit_Type"] isEqualToString:@"R"]) {
                            NSString* meetingAttendees=[visitDetails valueForKey:@"Meeting_Attendees"];
                            NSString* rowID=[NSString createGuid];
                            NSString* attribName=[NSString stringWithFormat:@"Meeting_Attendees"];
                            
                            @try {
                                insertIntoVisitAddlInfoMeetingAttendees =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,actualVisitID,attribName,meetingAttendees,createdAt,createdBy, nil];
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        }
                        else
                        {
                            //if a meeting is normal then we need not insert data into TBL_Visit_Addl_Info its only for round table visit
                            insertIntoVisitAddlInfoMeetingAttendees=YES;
                        }
                        
                        // add visit location reason
                        if (currentVisitDetails.onSiteExceptionReason.length > 0) {
                            @try {
                                
                                insertIntoVisitAdditionalInfoLocationReason = [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",[NSString createGuid], actualVisitID, @"ONSITE_EXCEPTION_REASON", currentVisitDetails.onSiteExceptionReason, createdAt, createdBy, nil];
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        } else {
                            insertIntoVisitAdditionalInfoLocationReason = YES;
                        }
                        
                        
                        NSString* nextVisitObjectiveString=[NSString getValidStringValue:[visitDetails valueForKey:@"Next_Visit_Objective"]];
                        if (![NSString isEmpty:nextVisitObjectiveString]) {
                            
                            @try {
                                insertIntoVisitAdditionalInfoNextVisitObjective =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",[NSString createGuid], actualVisitID, @"Next_Visit_Objective", nextVisitObjectiveString, createdAt, createdBy, nil];
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                        }
                        else{
                            insertIntoVisitAdditionalInfoNextVisitObjective=YES;
                        }
                        
                        
                        if (insertIntoVisitAddlInfoMeetingAttendees==YES) {
                            
                            NSLog(@"INSERT SUCCESSFULL MEETING ATTENDEES");
                            
                            NSString* ratingStr= [NSString getValidStringValue:[additionalInfo valueForKey:@"Visit_Rating"]];
                            NSString* rowID=[NSString createGuid];
                            NSString* attribName=@"Visit_Rating";
                            
                            @try {
                                insertIntoVisitAddlInfoRating =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,actualVisitID,attribName,ratingStr,createdAt,createdBy, nil];
                                
                            }@catch (NSException *exception)
                            {
                                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                            }
                            @finally
                            {
                                // [database close];
                            }
                            
                            
                            if (insertIntoVisitAddlInfoRating==YES) {
                                NSLog(@"RATING INSERTED SUCCESSFULLY");
                                
                                //this to be updated while closing the visit
                                insertIntoPlannedVisits=YES;
                            }
                        }    //now verify if all transactions were successfull
                    }
                }
            }
        }
        
        if ([[AppControl retrieveSingleton].FM_SURVEY_VERSION isEqualToString:@"2"] ){
            
            NSString* surveyTimeStamp=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            NSString* salesRepID=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];
            NSString* empCode=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];
            
            if (currentVisitDetails.coachSurveyArray.count>0) {
                
                NSLog(@"coach survey details while inserting %ld",currentVisitDetails.coachSurveyArray.count);
                
                for (NSInteger i=0; i<currentVisitDetails.coachSurveyArray.count; i++) {
                    
                    NSString* remarks=[[NSString alloc]init];
                    
                    NSMutableDictionary* currentDictionary=[currentVisitDetails.coachSurveyArray objectAtIndex:i];
                    NSString* response=[[NSString alloc]init];
                    NSString *queryString=[[NSString alloc]init];
                    NSMutableArray* argumentsArray=[[NSMutableArray alloc]init];
                    
                    NSString * auditSurveyID =[NSString createGuid];
                    NSString* surveyID=[NSString getValidStringValue:[currentDictionary valueForKey:@"Survey_ID"]];
                    NSString* questionID=[NSString getValidStringValue:[currentDictionary valueForKey:@"Question_ID"]];
                    
                    NSString* surveyTypeCode=[NSString getValidStringValue:[currentDictionary valueForKey:@"Survey_Type_Code"]];
                    
                    if([surveyTypeCode isEqualToString:@"D"])
                    {
                        //D has slider
                        //if not applicable is selected then insert null in response
                        
                        response=[NSString getValidStringValue:[currentDictionary valueForKey:@"sliderValue"]];
                        remarks=[NSString getValidStringValue:[currentDictionary valueForKey:@"Remarks"]];
                        NSString* status=[NSString getValidStringValue:[currentDictionary valueForKey:@"Status"]];
                        
                        queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By,Custom_Attribute_1,Custom_Attribute_2) Values(?,?,?,?,?,?,?,?,?,?)";
                        argumentsArray= [[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,[status isEqualToString:KAppControlsNOCode]?[NSNull null]:response,salesRepID,empCode,surveyTimeStamp,createdBy,remarks,actualVisitID,nil];
                    }
                    else if ([surveyTypeCode isEqualToString:@"Q"])
                    {
                        //Q has segment
                        response=[NSString getValidStringValue:[currentDictionary valueForKey:@"segmentValue"]];
                        remarks=[NSString getValidStringValue:[currentDictionary valueForKey:@"Remarks"]];
                        queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By,Custom_Attribute_1,Custom_Attribute_2) Values(?,?,?,?,?,?,?,?,?,?)";
                        argumentsArray=[[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,response,salesRepID,empCode,surveyTimeStamp,createdBy,remarks,actualVisitID,nil];
                    }
                    insertSurveySuccessfull = [database executeUpdate:queryString withArgumentsInArray:argumentsArray];
                }
            }
            else
            {
                insertSurveySuccessfull=YES;
                insertSurveyConfirmationSuccessfull=YES;
            }
            
            if (currentVisitDetails.coachSurveyConfirmationArray.count>0) {
                
                for (NSInteger j=0; j<currentVisitDetails.coachSurveyConfirmationArray.count; j++) {
                    
                    CoachSurveyConfirmationQuestion * currentQuestion=[currentVisitDetails.coachSurveyConfirmationArray objectAtIndex:j];
                    NSString * auditSurveyID =[NSString createGuid];
                    NSString* surveyID=[NSString getValidStringValue:currentQuestion.Survey_ID];
                    NSString* questionID=[NSString getValidStringValue:currentQuestion.Question_ID];
                    NSString* response=[[NSString alloc]init];
                    
                    if ([currentQuestion.Response_Display_Type isEqualToString:kSignatureResponseDisplayType]) {
                        response=currentQuestion.image_Name;
                    }
                    else
                    {
                        response = [NSString getValidStringValue:currentQuestion.Response_Text];
                    }
                    
                    
                    NSString *queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By,Custom_Attribute_2) Values(?,?,?,?,?,?,?,?,?)";
                    
                    NSMutableArray* argumentsArray=[[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,response,salesRepID,empCode,surveyTimeStamp,createdBy,actualVisitID,nil];
                    
                    insertSurveyConfirmationSuccessfull = [database executeUpdate:queryString withArgumentsInArray:argumentsArray];
                }
            }
            else
            {
                insertSurveyConfirmationSuccessfull=YES;
            }
            
            if (currentVisitDetails.coachSurveyConfirmationArray.count>0) {
                
                for (NSInteger k=0; k<currentVisitDetails.coachSurveyConfirmationArray.count; k++) {
                    CoachSurveyConfirmationQuestion * currentQuestion=[currentVisitDetails.coachSurveyConfirmationArray objectAtIndex:k];
                    if ([currentQuestion.Response_Display_Type isEqualToString:kSignatureResponseDisplayType]) {
                        
                        //save image to documents directory
                        NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:KCoachSurveySignatureFolderName] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentQuestion.image_Name]];
                        
                        if ([[NSFileManager defaultManager]fileExistsAtPath:tempPath]) {
                            NSError * error;
                            NSString* actualPath = [[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:KCoachSurveySignatureFolderName]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentQuestion.image_Name]];
                            
                            BOOL moveSuccessfull =[[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:actualPath error:&error];
                            
                            if (moveSuccessfull) {
                                //now delete from temp
                                NSError * deleteError;
                                BOOL deleteStatus = NO;
                                deleteStatus=[[NSFileManager defaultManager]removeItemAtPath:tempPath error:&deleteError];
                            }
                        }
                    }
                }
            }
            //visit addl info for coach survey
            if (currentVisitDetails.coachSurveyArray.count>0) {
                
                for (NSInteger i=0; i<currentVisitDetails.coachSurveyArray.count; i++) {
                    
                    NSString* rowID=[NSString createGuid];
                    NSString* attributeName=@"SURVEY_ID";
                    
                    NSMutableDictionary* currentDictionary=[currentVisitDetails.coachSurveyArray objectAtIndex:i];
                    NSString* surveyID=[NSString getValidStringValue:[currentDictionary valueForKey:@"Survey_ID"]];
                    NSString * attributeValue=surveyID;
                    
                    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
                    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    [formatterTime setLocale:usLocaleq];
                    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
                    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
                    
                    insertIntoSurveyVisitAdditionalInfo =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,actualVisitID,attributeName,attributeValue,createdAt,createdBy, nil];
                }
            }
            else{
                insertIntoSurveyVisitAdditionalInfo=YES;
            }
        }
    }
    
    if (!insertIntoActualVisits || !insertIntoIssueNote || !insertIntoIssueNoteLineItems|| !insertIntoEDetailing|| !insertIntoVisitAdditionalInfo||!insertIntoVisitAddlInfoMeetingAttendees||!insertIntoVisitAddlInfoRating||!insertIntoPlannedVisits || !insertIntoVisitAdditionalInfoNotes || !insertIntoVisitAdditionalInfoLocationReason ||!insertIntoVisitAdditionalInfoNextVisitObjective || !insertIntoVisitAdditionalInfoAccopaniedBy) {
        
        //something went wrong rollback
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        
        [database close];
        return NO;
    }
    else
    {
        //everything alright commit
        
        [database commit];
        [database close];
        //survey to swift module
        if ([[AppControl retrieveSingleton].FM_SURVEY_VERSION isEqualToString:@"3"]){
            
            SurveySwiftDetailsViewController *surveyDetailsVC = [[SurveySwiftDetailsViewController alloc]init];
            NSString* surveyTimeStampSwift=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            NSString* salesRepIDSwift=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];
            NSString* empCodeSwift=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];
            NSString* createdBySwift=[[SWDefaults userProfile] stringForKey:@"User_ID"];
            
            NSDictionary * visitDetailsDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:actualVisitID,@"Actual_Visit_ID",salesRepIDSwift,@"SalesRep_ID",empCodeSwift,@"Emp_Code",createdBySwift,@"Created_By",surveyTimeStampSwift,@"Survey_Time_Stamp",@"Y",@"is_From_Visit" ,nil];
            [[NSUserDefaults standardUserDefaults]setObject:visitDetailsDictionary forKey:@"VISIT_DETAILS_SWIFT"];
            [surveyDetailsVC insertSurveyToDatabase];
            
            //clearing defaults used for survey
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"SURVEY_INSERT_STATUS"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"VISIT_DETAILS_SWIFT"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Survey_Nodes"];
        }
        return YES;
    }
}



#pragma mark Save Visit Details without call

+(BOOL)saveVisitDetailsWithOutCall:(NSMutableDictionary*)visitDetails AdditionalInfo:(NSMutableDictionary*)visitAdditionalInfo andVisitDetails:(VisitDetails*)currentVisitDetails

{
    BOOL status=NO;
    
    BOOL insertIntoPlannedVisits = NO;
    BOOL insertIntoActualVisits = NO;
    BOOL insertIntoIssueNote = NO;
    BOOL insertIntoIssueNoteLineItems=NO;
    BOOL insertIntoEDetailing=NO;
    BOOL insertIntoVisitAddlInfoMeetingAttendees=NO;
    BOOL insertIntoVisitAddlInfoRating=NO;
    BOOL visitAddlInfoStatus=NO;
    BOOL insertIntoVisitAdditionalInfo=NO;
    BOOL insertIntoVisitAdditionalInfoNotes=NO;
    BOOL insertIntoVisitAdditionalInfoLocationReason=NO;
    BOOL insertIntoVisitAdditionalInfoAccopaniedBy=YES;
    
    BOOL updatedTasksSuccessfully=NO;
    
   NSLog(@"visit details withput call are %@", [visitDetails description]);
    
   NSLog(@"visit AdditionalInfo without call are %@", [visitAdditionalInfo description]);
    
    NSString* actualVisitID=[NSString createGuid];
    
    
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    NSString*locationID=[visitDetails valueForKey:@"Location_ID"];
    NSString* location_Type=[visitDetails valueForKey:@"Location_Type"];
    
    
    
    
    if ([[visitDetails valueForKey:@"Location_Type"]isEqualToString:@"D"]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
        
    }
    
    
    
    if ([[visitDetails valueForKey:@"Doctor_ID"] isEqual:[NSNull null]]) {
        
    }
    
    else if ([[visitDetails valueForKey:@"Doctor_ID"] isEqualToString:@""]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
        
    }
    else if ([[visitDetails valueForKey:@"Doctor_ID"] isEqualToString:@"0"]) {
        
        //  doctorID=NULL;
        
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
        
        
    }
    
    
    
    
    if ([[visitDetails valueForKey:@"Contact_ID"] isEqual:[NSNull null]]) {
        
    }
    
    else if ([[visitDetails valueForKey:@"Contact_ID"] isEqualToString:@"0"]||[[visitDetails valueForKey:@"Contact_ID"] isEqualToString:@""]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
        
    }
    
    
    
    
    if ([location_Type isEqualToString:@"D"]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];

    }
    
  else  if ([location_Type isEqualToString:@"P"]) {
    
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];

    }
    
    
    else
    {
        
    }

    
    //in case of visiting pharmacy doctor id will be 0, and while sync unique identifer issue will occur so insert null
    
    
    
//    if ([[visitDetails valueForKey:@"Location_Type"]isEqualToString:@"D"]) {
//        
//        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
//        
//    }
//
//    NSString* doctorID=[visitDetails valueForKey:@"Doctor_ID"];
//
//    
//    if ([doctorID isEqual:[NSNull null]]) {
//        
//    }
//    
//   else if ([doctorID isEqualToString:@"0"]) {
//        
//        //doctorID=NULL;
//        
//        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
//        
//        
//    }
//    
//    doctorID=[visitDetails valueForKey:@"Doctor_ID"];
//    
//    
//    
//    NSString * contactID=[visitDetails valueForKey:@"Contact_ID"];
//    
//    if ([contactID isEqual:[NSNull null]]) {
//        
//    }
//    
//    
//   else if ([contactID isEqualToString:@"0"]) {
//       
//        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
//
//    }
//    
    
    
    
    
    
    NSPredicate * updatedTasksPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
    
    NSMutableArray * updatedTasksArray =[[currentVisitDetails.taskArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
    NSLog(@"updated tasks are %@", updatedTasksArray);
    

    
    
    NSString* contactIDStr=[NSString stringWithFormat:@"%@", [visitDetails valueForKey:@"Contact_ID"]];
    
    
    NSString* plannedVisit_ID=[visitDetails valueForKey:@"Planned_Visit_ID"];
    
    NSString* callStartDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_Start_Date"];
    
    NSString* callEndDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_End_Date"];
    
    
    
    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    NSString* latitide=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude];
    
    if (latitide.length>0) {
        
    }
    
    else
    {
        latitide=@"0";
        longitude=@"0";
    }
    NSString* notesStr=[visitDetails valueForKey:@"Visit_Conclusion_Notes"];
    
    
    NSString* assignedTo=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    
    NSString* userID=[[SWDefaults userProfile]valueForKey:@"User_ID"];
    
    
//    NSInteger accompaniedBy=[[visitDetails valueForKey:@"Accompanied_By"] integerValue];
    
    
    NSString *accompaniedByStr = [NSString getValidStringValue:[visitDetails valueForKey:@"Accompanied_By"]];
    
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];

    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    
    
    //TO BE DONE CHECK WITH HARPAL
    // NSString* accompaniedBy=@"";
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    
    
    
    [database open];
    
    
    [database beginTransaction];
    
    @try {
        
        if ([[[AppControl retrieveSingleton] FM_ENABLE_MULTISEL_ACCOMP_BY] isEqualToString:KAppControlsYESCode]) {
            insertIntoActualVisits =  [database executeUpdate:@"INSERT INTO PHX_TBL_Actual_Visits (Actual_Visit_ID,EMP_ID, Location_ID, Doctor_ID, Contact_ID, Planned_Visit_ID, Visit_Start_Date, Visit_End_Date, Latitude, Longitude, Notes, Custom_Attribute_2, Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",actualVisitID,empID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],plannedVisit_ID,callStartDateandTime,callEndDateandTime,latitide,longitude,notesStr,[SWDefaults getValidStringValue:appDelegate.currentLocationCapturedTime], currentVisitDetails.selectedDemoPlan.Demo_Plan_ID,nil];
        } else {
            insertIntoActualVisits =  [database executeUpdate:@"INSERT INTO PHX_TBL_Actual_Visits (Actual_Visit_ID,EMP_ID, Location_ID,Doctor_ID,Contact_ID,Planned_Visit_ID,Visit_Start_Date,Visit_End_Date,Latitude,Longitude,Notes,Accompanied_By,Custom_Attribute_2,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",actualVisitID,empID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],plannedVisit_ID,callStartDateandTime,callEndDateandTime,latitide,longitude,notesStr,accompaniedByStr,[SWDefaults getValidStringValue:appDelegate.currentLocationCapturedTime], currentVisitDetails.selectedDemoPlan.Demo_Plan_ID,nil];
        }

    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        // [database close];
    }
    
    if (insertIntoActualVisits==YES) {
        
        NSLog(@"DATA INSERTED INTO ACTUAL VISITS SUCCESSFULLY");
        
        if ([[[AppControl retrieveSingleton] FM_ENABLE_MULTISEL_ACCOMP_BY] isEqualToString:KAppControlsYESCode]) {
            // add accompanied by in to table TBL_Visit_Addl_Info
            @try {
                
                insertIntoVisitAdditionalInfoAccopaniedBy =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID, Visit_ID, Attrib_Name, Attrib_Value, Created_At, Created_By)  VALUES (?,?,?,?,?,?)",[NSString createGuid], actualVisitID, @"Accompanied_BY", accompaniedByStr, createdAt, createdBy, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                // [database close];
            }
        }

        
        
        //insert tasks here
        
        //VisitTask * currentVisitTasks=[currentVisitDetails.taskArray objectAtIndex:0];
        
        NSLog(@"tasks count in current visit is %d", currentVisitDetails.taskArray.count);
        
        
        if (currentVisitDetails.taskArray.count>0) {
        
        
//        VisitTask * currentTask=[currentVisitDetails.taskArray objectAtIndex:0];
//        
//        NSLog(@"***UPDATED TASK DETAILS ARE %@  %@ %@ %@ %@ %@ %@ ***",currentTask.Title,currentTask.Description,currentTask.Status,currentTask.Priority,currentTask.Category,currentTask.Start_Time,currentTask.isUpdated );
//        
        
        
        //refined the array to fetch only those which are updated
        
        NSPredicate * updatedTasksPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
        
        updatedTasksArray=[[currentVisitDetails.taskArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
        NSLog(@"updated tasks are %@", updatedTasksArray);
        
        
        if (updatedTasksArray.count>0) {
            
            
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                
                VisitTask * currentTask=[updatedTasksArray objectAtIndex:i];
                
//                NSLog(@"***UPDATED TASK DETAILS ARE %@  %@ %@ %@ %@ %@ ***",currentTask.Title,currentTask.Description,currentTask.Status,currentTask.Priority,currentTask.Category,currentTask.Start_Time );
//                
                
                @try {
                    
                    updatedTasksSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Tasks (Task_ID,Title, Description,Category,Priority,Start_Time,Status,Assigned_To,Emp_ID,Location_ID,Show_Reminder,IS_Active,Is_deleted,Created_At,Created_By,Doctor_ID,Contact_ID,End_Time,Last_Updated_At,Last_Updated_By,Custom_Attribute_1)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",currentTask.Task_ID, currentTask.Title==nil?@"":currentTask.Title,currentTask.Description==nil?@"":currentTask.Description,currentTask.Category==nil?@"":currentTask.Category,currentTask.Priority==nil?@"":currentTask.Priority, currentTask.Start_Time,currentTask.Status==nil?@"":currentTask.Status,assignedTo,assignedTo,currentTask.Location_ID==nil?@"":currentTask.Location_ID,@"N",@"Y",@"N",currentTask.Start_Time,createdBy,[NSString isEmpty:[visitDetails valueForKey:@"Doctor_ID"]]?[NSNull null]: [visitDetails valueForKey:@"Doctor_ID"],[NSString isEmpty:[visitDetails valueForKey:@"Contact_ID"]]?[NSNull null]: [visitDetails valueForKey:@"Contact_ID"],currentTask.End_Time,currentTask.Start_Time,userID,@"IPAD",nil];
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
                
                
                
        
            }
        
            
            
            //after updating in visit addl info update the status in task table as well.
            
            
            BOOL updatetaskStatus=NO;
            
            
            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [formatterTime setLocale:usLocaleq];
            NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                
                
                VisitTask * task=[updatedTasksArray objectAtIndex:i];
                
                
                
                @try {
                    
                    updatetaskStatus =  [database executeUpdate:@"Update  PHX_TBL_Tasks set Status = ?, Last_Updated_At=?, Last_Updated_By=?  where Task_ID='%@'",task.Status,createdAt,createdBy,task.Task_ID, nil];
                    
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    // [database close];
                    
                }
                
            }
            
            
        
        
        }
        
        }
        
        
        
        
        
        
        
        NSPredicate * updatedNotesPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
        
        NSMutableArray*  updatedNotesArray=[[currentVisitDetails.notesArray filteredArrayUsingPredicate:updatedNotesPredicate] mutableCopy];
        NSLog(@"updated notes are %@", updatedTasksArray);
        
        
        
        
        for (NSInteger i=0; i<updatedNotesArray.count; i++) {
            
            VisitNotes * currentNote=[updatedNotesArray objectAtIndex:i];
            
            
            
            
            
            BOOL updatedNotesSuccessfully = NO;
            
            @try {
                
                updatedNotesSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_EMP_NOTES  (Note_ID,Title, Description,Category,Emp_ID,Visit_ID,Location_ID,Doctor_ID,Contact_ID,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?,?,?,?)",currentNote.Note_ID,currentNote.Title,currentNote.Description,currentNote.Category,currentNote.Emp_ID,actualVisitID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],currentNote.Created_At,currentNote.Created_By, nil];
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                //[database close];
            }
            
            
            
            
        }
        
        
            
        
        
        //now insert samples given to issue notes
        NSString* rowID=[NSString createGuid];
        
        
        
        
        
        Singleton *single = [Singleton retrieveSingleton];
        
        NSString *nUserId = [NSString stringWithFormat:@"%@",[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];
        
        if(nUserId.length == 1)
        {nUserId = [NSString stringWithFormat:@"00%@",nUserId];}
        else if(nUserId.length == 2)
        {nUserId = [NSString stringWithFormat:@"0%@",nUserId];}
        
        
        /* samples not given if visit is closed without call so no need to increment issue note referenc e number
        NSString *lastOrderReference = [MedRepDefaults lastIssueNoteReference];
        unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 10] longLongValue];
        lastRefId = lastRefId + 1;
        lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
        
        if (![lastOrderReference isEqualToString:[MedRepDefaults lastIssueNoteReference]])
        {
            
            [MedRepDefaults setIssueNoteReference:lastOrderReference];
            
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
            if (![lastOrderReference isEqualToString:[MedRepDefaults lastIssueNoteReference]])
            {
                [MedRepDefaults setIssueNoteReference:lastOrderReference];
            }
            else
            {
                lastRefId = lastRefId + 1;
                lastOrderReference = [NSString stringWithFormat:@"M%@I%010lld", nUserId, lastRefId];
                [MedRepDefaults setIssueNoteReference:lastOrderReference];
            }
        }
        
        
        
        
        
        
        NSLog(@"last order ref is %@", lastOrderReference);*/
        
        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        
        NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
        
        //created by to be user id as sugges by peram on 10/2/2016
        
        NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
        
       // NSInteger  lastVisitRefInt=[[NSString stringWithFormat:@"%@", lastOrderReference] integerValue];
        
        
        
        //samples not given no need of issue note
        
        
//        //[database open];
//        @try {
//            
//            insertIntoIssueNote =  [database executeUpdate:@"INSERT INTO PHX_TBL_Issue_Note (Row_ID,Doc_No, Doc_Type,Emp_ID,Location_ID,Doctor_ID,Contact_ID,Visit_ID,Start_Time,End_Time,Status,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",rowID,lastOrderReference,@"S",empID,[visitDetails valueForKey:@"Location_ID"],[visitDetails valueForKey:@"Doctor_ID"],[visitDetails valueForKey:@"Contact_ID"],actualVisitID,callStartDateandTime,callEndDateandTime,@"N",createdAt,createdBy, nil];
//            
//        }@catch (NSException *exception)
//        {
//            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
//            
//            //[database rollback];
//        }
//        @finally
//        {
//            //  [database close];
//        }
        
        
        
        
        

        
        NSLog(@"EDETAILING INSERT SUCCESSFULL");
        
        
        //now insert visit addl info details
        
        /*
         TBL_Visit_Addl_Info- attribute name “TASK”
         ATTRIB VALUE TASK-ID
         CA1 Type of Action C-Created/M-modified, and meeting attendies Attrib name and value,and custom attribute_1 for rating
         */
        
        
        
        
        NSString* taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where Location_ID='%@' and  Is_Active='Y' and Is_Deleted='N' ",locationID ];
        
        FMResultSet* resultSet=[database executeQuery:taskdetailsQry];
        NSMutableArray* tasksArray=[[NSMutableArray alloc]init];
        
        
        while ([resultSet next]) {
            [tasksArray addObject:[resultSet resultDictionary]];
        }
        
        
        // NSMutableArray* tasksArray=[MedRepQueries fetchTaskswithLocationID:locationID];
        
       //NSLog(@"task array in insert qry %@", [tasksArray description]);
        
        
        NSMutableArray* updatedTaskArray=[[[NSUserDefaults standardUserDefaults]objectForKey:@"Updated_Tasks"] mutableCopy];
        
        
        
        
        
        
       //NSLog(@"updated task Array is %@", [updatedTaskArray description]);
        
        
        if (updatedTasksArray.count>0) {
            
            
            
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++) {
                
                VisitTask * task=[updatedTasksArray objectAtIndex:i];
                
                
                NSMutableDictionary* row=[updatedTasksArray objectAtIndex:i];
                
                NSString* rowID=[NSString createGuid];
                NSString* attributeName=@"Task";
                NSString* attributeValue=task.Task_ID;
                NSString* taskStatus=task.Status;
                
                
                
                //  [database open];
                @try {
                    
                    insertIntoVisitAdditionalInfo =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Custom_Attribute_1,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?)",rowID,actualVisitID,attributeName,attributeValue,taskStatus,createdAt,createdBy, nil];
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    // [database close];
                }
                
                
           
            }
        }
        
        else
        {
            insertIntoVisitAdditionalInfo=YES;
        }
        
        
        //update notes data same as task
        
        
        
        
        NSMutableArray* notesArray=[[NSMutableArray alloc]init];
        
        NSString* queryForFetchingNotes=[NSString stringWithFormat:@"select IFNULL(Note_ID,'0') AS Note_ID from PHX_TBL_EMP_NOTES where Location_ID='%@' ",locationID ];
        
        
        FMResultSet* resultSetNotes=[database executeQuery:queryForFetchingNotes];
        
        
        while ([resultSetNotes next]) {
            [notesArray addObject:[resultSetNotes resultDictionary]];
        }
        
        
        
        
        
        
        
        
        
        
        
        
        if (updatedNotesArray.count>0) {
            
            
            
            
            for (NSInteger i=0; i<updatedNotesArray.count; i++) {
                
                NSMutableDictionary* row=[updatedNotesArray objectAtIndex:i];
                
                NSString* rowID=[NSString createGuid];
                NSString* attributeName=@"Notes";
                NSString* attributeValue=[row valueForKey:@"Note_ID"];
                
                
                
                //  [database open];
                @try {
                    
                    insertIntoVisitAdditionalInfoNotes =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,actualVisitID,attributeName,attributeValue,createdAt,createdBy, nil];
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    // [database close];
                }
                
                
            }
            
        }
        
        else
        {
            //if no notes are available
            insertIntoVisitAdditionalInfoNotes=YES;
        }
        
        
        //task details updated
        
        NSString* visitTypeStr=[visitDetails valueForKey:@"Visit_Type"];
        
        if ([visitTypeStr isEqualToString:@"R"] && [[[SWDefaults getValidStringValue:[NSString stringWithFormat:@"Meeting_Attendees"]] trimString] isEqualToString:@""]) {
            
            NSString* meetingAttendees=[visitDetails valueForKey:@"Meeting_Attendees"];
            NSString* rowID=[NSString createGuid];
            NSString* attribName=[NSString stringWithFormat:@"Meeting_Attendees"];
            @try {
                
                insertIntoVisitAddlInfoMeetingAttendees =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,actualVisitID,attribName,meetingAttendees,createdAt,createdBy, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                // [database close];
            }
            
        }
        
        else
        {
            
            
            //if a meeting is normal then we need not insert data into TBL_Visit_Addl_Info its only for round table visit
            
            insertIntoVisitAddlInfoMeetingAttendees=YES;
            
        }
        
        if (insertIntoVisitAddlInfoMeetingAttendees==YES) {
            
            NSLog(@"INSERT SUCCESSFULL MEETING ATTENDEES");
            
            insertIntoVisitAddlInfoRating=YES;
            
            //rating not displayed as per latest update
            
            /*
             NSString* ratingStr=[visitAdditionalInfo valueForKey:@"Visit_Rating"];
             NSString* rowID=[NSString createGuid];
             NSString* attribName=@"Visit_Rating";
             
             
             //  [database open];
             @try {
             
             insertIntoVisitAddlInfoRating =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,actualVisitID,attribName,ratingStr,createdAt,createdBy, nil];
             
             }@catch (NSException *exception)
             {
             NSLog(@"Exception while obtaining data from database: %@", exception.reason);
             }
             @finally
             {
             // [database close];
             }
             */
            
            
            if (insertIntoVisitAddlInfoRating==YES) {
                
                NSLog(@"RATING INSERTED SUCCESSFULLY");
                
                
                NSDateFormatter *formatterTime = [NSDateFormatter new] ;
                NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [formatterTime setLocale:usLocaleq];
                NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
                
                //update visit status in phx_tbl_planned_visits
                //  [database open];
                
                
                @try {
                    
                    //                    insertIntoPlannedVisits =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Status ='Y'  where Planned_Visit_ID=?",plannedVisit_ID, nil];
                    //
                    
                    
                    
                    insertIntoPlannedVisits =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Status ='Y' ,Last_Updated_At= ?, Last_Updated_By=? where Planned_Visit_ID=?",createdAt,createdBy,plannedVisit_ID, nil];
                    
                    
                    
                    
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    // [database close];
                    
                }
                
            }
            
            
            
        }
        
        
        // add visit location reason
        
        if (currentVisitDetails.onSiteExceptionReason.length > 0) {
            
            @try {
                
                insertIntoVisitAdditionalInfoLocationReason =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",[NSString createGuid], actualVisitID, @"ONSITE_EXCEPTION_REASON", currentVisitDetails.onSiteExceptionReason, createdAt, createdBy, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                // [database close];
            }
        } else {
            insertIntoVisitAdditionalInfoLocationReason = YES;
        }

}
    
    
    if (!insertIntoActualVisits  || !insertIntoVisitAdditionalInfo||!insertIntoVisitAddlInfoMeetingAttendees||!insertIntoVisitAddlInfoRating||!insertIntoPlannedVisits || !insertIntoVisitAdditionalInfoNotes || !insertIntoVisitAdditionalInfoLocationReason || !insertIntoVisitAdditionalInfoAccopaniedBy) {
        
        //something went wrong rollback
        
        
        
        BOOL transactionActive=[database inTransaction];
        
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        
        else
        {
            NSLog(@"transaction is in active");
            
        }
        
        
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        
        [database close];
        
        
        
        
        return NO;
    }
    
    else
    {
        //everything alright commit
        [database commit];
        
        [database close];
        
        return YES;
        
        
    }

}
    

+(BOOL)saveVisitDetailsOnCloseAfterStartCall:(NSMutableDictionary*)visitDetails andVisitDetails:(VisitDetails*)currentVisitDetails
{
    BOOL insertIntoActualVisits = NO;
    BOOL insertIntoVisitAdditionalInfoAccopaniedBy=YES;
    
    NSLog(@"visit details withput call are %@", [visitDetails description]);
    
    NSString* actualVisitID=[NSString createGuid];
    
    
    NSString* empID=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    NSString* location_Type=[visitDetails valueForKey:@"Location_Type"];
 
    
    if ([[visitDetails valueForKey:@"Location_Type"]isEqualToString:@"D"]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
    }
    if ([[visitDetails valueForKey:@"Doctor_ID"] isEqual:[NSNull null]]) {
        
    }
    
    else if ([[visitDetails valueForKey:@"Doctor_ID"] isEqualToString:@""]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
        
    }
    else if ([[visitDetails valueForKey:@"Doctor_ID"] isEqualToString:@"0"]) {
        
        //  doctorID=NULL;
        
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
    }
    
    
    if ([[visitDetails valueForKey:@"Contact_ID"] isEqual:[NSNull null]]) {
        
    }
    else if ([[visitDetails valueForKey:@"Contact_ID"] isEqualToString:@"0"]||[[visitDetails valueForKey:@"Contact_ID"] isEqualToString:@""]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
    }
    
    
    
    
    if ([location_Type isEqualToString:@"D"]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Contact_ID"];
    } else if ([location_Type isEqualToString:@"P"]) {
        
        [visitDetails setObject:[NSNull null] forKey:@"Doctor_ID"];
    }
    else {
        
    }
 
    
    NSString* plannedVisit_ID=[visitDetails valueForKey:@"Planned_Visit_ID"];
    
    NSString* callStartDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_Start_Date"];
    
    NSString* callEndDateandTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"Call_End_Date"];
    
    
    
    SWAppDelegate* appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    NSString* latitide=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",appDelegate.currentLocation.coordinate.longitude];
    
    if (latitide.length>0) {
        
    }
    
    else
    {
        latitide=@"0";
        longitude=@"0";
    }
    NSString* notesStr=[visitDetails valueForKey:@"Visit_Conclusion_Notes"];
    
//    NSInteger accompaniedBy=[[visitDetails valueForKey:@"Accompanied_By"] integerValue];
    
    NSString *accompaniedByStr= [visitDetails valueForKey:@"Accompanied_By"];
    
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
    
    
    
    //TO BE DONE CHECK WITH HARPAL
    // NSString* accompaniedBy=@"";
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    } else {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];

    [database open];
    
    [database beginTransaction];
    
    @try {
        
        if ([[[AppControl retrieveSingleton] FM_ENABLE_MULTISEL_ACCOMP_BY] isEqualToString:KAppControlsYESCode]) {
            insertIntoActualVisits =  [database executeUpdate:@"INSERT INTO PHX_TBL_Actual_Visits (Actual_Visit_ID,EMP_ID, Location_ID,Doctor_ID,Contact_ID,Planned_Visit_ID,Visit_Start_Date,Visit_End_Date,Latitude,Longitude,Notes,Custom_Attribute_2,Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",actualVisitID,empID,[visitDetails valueForKey:@"Location_ID"],currentVisitDetails.selectedDoctor.Doctor_ID,currentVisitDetails.selectedContact.Contact_ID,plannedVisit_ID,callStartDateandTime,callEndDateandTime,latitide,longitude,notesStr,[SWDefaults getValidStringValue:appDelegate.currentLocationCapturedTime], currentVisitDetails.selectedDemoPlan.Demo_Plan_ID,nil];
        } else {
            insertIntoActualVisits =  [database executeUpdate:@"INSERT INTO PHX_TBL_Actual_Visits (Actual_Visit_ID, EMP_ID, Location_ID, Doctor_ID, Contact_ID, Planned_Visit_ID, Visit_Start_Date, Visit_End_Date, Latitude, Longitude, Notes, Accompanied_By, Custom_Attribute_2, Custom_Attribute_3)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",actualVisitID,empID,[visitDetails valueForKey:@"Location_ID"],currentVisitDetails.selectedDoctor.Doctor_ID,currentVisitDetails.selectedContact.Contact_ID,plannedVisit_ID,callStartDateandTime,callEndDateandTime,latitide,longitude,notesStr,accompaniedByStr, [SWDefaults getValidStringValue:appDelegate.currentLocationCapturedTime], currentVisitDetails.selectedDemoPlan.Demo_Plan_ID,nil];
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        // [database close];
    }

    if (insertIntoActualVisits == YES) {
        if ([[[AppControl retrieveSingleton] FM_ENABLE_MULTISEL_ACCOMP_BY] isEqualToString:KAppControlsYESCode]) {
            // add accompanied by in to table TBL_Visit_Addl_Info
            @try {
                
                insertIntoVisitAdditionalInfoAccopaniedBy =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID, Visit_ID, Attrib_Name, Attrib_Value, Created_At, Created_By)  VALUES (?,?,?,?,?,?)",[NSString createGuid], actualVisitID, @"Accompanied_BY", accompaniedByStr, createdAt, createdBy, nil];
                
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            }
            @finally
            {
                // [database close];
            }
        }
    }
    
    if (!insertIntoActualVisits || !insertIntoVisitAdditionalInfoAccopaniedBy) {

        BOOL transactionActive=[database inTransaction];
        
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        
        else
        {
            NSLog(@"transaction is in active");
            
        }
        
        
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        
        [database close];

        return NO;
    }
    
    else
    {
        //everything alright commit
        [database commit];
        
        [database close];
        
        return YES;
    }
}


#pragma mark Stock Quantity updating methods

+(NSInteger)fetchPreviouslyusedQtyforProduct:(NSString*)productID lotNumber:(NSString*)lotNumber

{
    NSString* usedQtyqry=[NSString stringWithFormat:@"select IFNULL(Used_Qty,'0') AS User_Qty from PHX_TBL_Emp_Stock where Product_ID='%@' and lot_no ='%@'",productID,lotNumber];
    
    
    NSLog(@"previosuly used qty qry %@", usedQtyqry);
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:usedQtyqry];
    
    if (filteredContentArray.count>0) {
        
        NSInteger contentVal=[[[filteredContentArray objectAtIndex:0]valueForKey:@"Used_Qty"] integerValue];
        
        if (contentVal>0) {
            
            return contentVal;
        }
        
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
        
    }
    
}




+(NSInteger)fetchColumnsforEmpStock:(NSString*)columnName LotNumber:(NSString*)lotNumber
{
    
    
    NSString* filteredContentQry=[NSString stringWithFormat:@"select IFNULL(%@,'0') AS %@ from PHX_TBL_EMP_Stock where Lot_NO='%@'",columnName,columnName,lotNumber];
    
    NSLog(@"qry for stock %@", filteredContentQry);
    
    
   //NSLog(@"filtered ContentQry %@", filteredContentQry);
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:filteredContentQry];
    
    if (filteredContentArray.count>0) {
        
        NSInteger contentVal=[[filteredContentArray valueForKey:columnName] integerValue];
        
        if (contentVal>0) {
            
            return contentVal;
        }
        
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
        
    }

}

+(BOOL)updateStockQuantity:(NSMutableArray*)productsArray
{
    BOOL success = NO;

    for (SampleProductClass *tempProduct in productsArray) {
        NSString *userId = [[SWDefaults userProfile] stringForKey:@"User_ID"];
        NSString *productID = tempProduct.ProductId;
        NSString *LotNo = tempProduct.productLotNumber;
        
        NSInteger usedQty = tempProduct.Used_Qty.integerValue;
        NSInteger currentUsedQtyIntVal = tempProduct.QuantityEntered.integerValue;
        NSInteger updatedUsedQtyVal = usedQty + currentUsedQtyIntVal;
        NSString *userQtyStr = [NSString stringWithFormat:@"%zd", updatedUsedQtyVal];

        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString *updatedAt =  [formatterTime stringFromDate:[NSDate date]];
        
        FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
        [database open];
        
        @try {
            success =  [database executeUpdate:@"Update  PHX_TBL_EMP_Stock set Last_Updated_By=?, Used_Qty = ? ,   Last_Updated_At=?  where Lot_No=? and Product_ID=?",userId,userQtyStr,updatedAt,LotNo,productID, nil];
        }@catch (NSException *exception)
        {
            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
        }
        @finally
        {
            [database close];
        }
    }
    return success;
}

+(NSMutableArray*)fetchFilteredContent:(NSString*)columnName
{
    NSString* filteredContentQry=[NSString stringWithFormat:@"select Distinct %@ from PHX_TBL_Doctors",columnName];
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:filteredContentQry];
    
    if (filteredContentArray.count>0) {
        
        
        return filteredContentArray;
    }
    else
    {
        return nil;
        
    }

}

+(NSMutableArray*)fetchFilteredContentforPharmacy:(NSString*)columnName
{
    NSString* filteredContentQry=[NSString stringWithFormat:@"select  Distinct IFNULL(%@,'N/A') AS %@ from PHX_TBL_Locations where Location_Type='P' order by %@ COLLATE NOCASE ASC ",columnName,columnName,columnName];
    
   //NSLog(@"query for locations %@", filteredContentQry);
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:filteredContentQry];
    
    if (filteredContentArray.count>0) {
        
        return filteredContentArray;
    }
    else
    {
        return nil;
        
    }
    
}



+(NSMutableArray*)fetchFilteredProductsContent:(NSString*)columnName
{
    NSString* filteredContentQry=[NSString stringWithFormat:@"select  Distinct %@ from PHX_TBL_Products order by %@ COLLATE NOCASE ASC  ",columnName,columnName];
    
   //NSLog(@"query for products is %@", [filteredContentQry description]);
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:filteredContentQry];
    
   //NSLog(@"filtered content in products filter %@", [filteredContentArray description]);
    
    
    if (filteredContentArray.count>0) {
        
        if ([columnName isEqualToString:@"Product_Type"]) {
      
            
        
        for (NSInteger i=0; i<filteredContentArray.count; i++) {
            
            NSMutableDictionary* productAppCodesDict=[[NSMutableDictionary alloc]init];

            
            NSString* productTypeStr=[[filteredContentArray objectAtIndex:i]valueForKey:@"Product_Type"];
            
            NSString* appCodesString=[self fetchAppCOdeforProductType:productTypeStr];
            
            [productAppCodesDict setValue:appCodesString forKey:@"Product_Type"];
            
            [filteredContentArray replaceObjectAtIndex:i withObject:productAppCodesDict];
            
        }
            
            
            return filteredContentArray;
        }
        
        else
        {
        
        return filteredContentArray;
        }
    }
    else
    {
        return nil;
        
    }
    
}


+(NSMutableArray*)fetchFilteredContentforPlannedVisits :(NSString*)columnName

{
    NSString* filteredContentQry=[NSString stringWithFormat:@"select  Distinct IFNULL(%@,'N/A') AS %@ from PHX_TBL_PlannedVisits ",columnName,columnName];
    
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:filteredContentQry];
    
    if (filteredContentArray.count>0) {
        
        return filteredContentArray;
    }
    else
    {
        return nil;
        
    }

}


#pragma mark Planned Visits Methods

+(NSMutableArray*)fetchAllPlannedVisitsforFilter

{
    NSString* updatedQry=[NSString stringWithFormat:@"select A.visit_start_date AS ActualVisitCallStartTime,A.Visit_End_Date AS ActualVisitCallEndTime,Actual_visit_id,A.Planned_Visit_ID as ActualPlannedVisitID, P.Planned_Visit_ID AS Planned_Visit_ID,P.Visit_Date AS Visit_Date, P.EMP_ID AS EMP_ID,IFNULL(L.Latitude,'0')AS Latitude,IFNULL(L.Longitude,'0')AS Longitude,P.Location_ID AS Location_ID, P.Doctor_ID AS Doctor_ID,P.Contact_ID AS Contact_ID, IFNULL(P.Visit_Status,'N/A') AS Visit_Status,IFNULL(P.Comments, 'N/A') AS Objective ,P.Visit_Date AS Visit_Date,P.Created_At AS Created_At,P.Created_By AS Created_By, P.Demo_Plan_ID AS Demo_Plan_ID,IFNULL(P.Visit_Type,'N/A') AS Visit_Type ,IFNULL(L.Location_Type,'N/A') AS Location_Type,IFNULL(L.Address_1,'N/A')AS Address,IFNULL(L.City,'')AS City,IFNULL(L.Phone,'')AS Phone,IFNULL(L.Location_name,'N/A') as Location_Name, IFNULL(L.Classification_3,'N/A') AS OTC ,A.Visit_End_Date AS Last_Visited_At, IFNULL(Contact_Name,'N/A')AS Pharmacy_Contact_Name,IFNULL(L.Custom_Attribute_1,'N/A')AS Trade_Channel, IFNULL(L.Classification_2,'N/A') AS Class,IFNULL(D.Doctor_Name,'N/A')AS Doctor_Name ,IFNULL(D.Classification_2,'N/A')AS Doctor_Class, IFNULL(D.Classification_1,'N/A') AS Specialization from PHX_TBL_Planned_Visits  as P left join PHX_TBL_Actual_Visits AS A on A.Planned_Visit_ID =P.Planned_Visit_ID inner join PHX_TBL_Locations L on P.Location_ID=L.Location_ID left join PHX_TBL_Contacts AS C on C.Contact_ID=P.Contact_ID left join PHX_TBL_Doctors  AS D on D.Doctor_ID=P.Doctor_ID where P.Visit_Status!='X' GROUP by P.Planned_Visit_ID Order by  Visit_Status, Visit_Date  ASC"];
    
    
  // NSLog(@"check query  for all planned visitshere %@", updatedQry);
    
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:updatedQry];
    if (plannedVisitsArray.count>0) {
        
        return plannedVisitsArray;
    }
    
    else
    {
        return nil;
    }

    
}


+(BOOL)contactMatcheswithLocation:(NSString*)locationID andDoctorID:(NSString*)contactID
{
    
    NSString* matchQry=[NSString stringWithFormat:@"select IFNULL(Location_ID,'N/A') AS Location_ID  from phx_tbl_location_contact_map where Contact_ID ='%@' and Location_ID='%@' ",contactID,locationID];
    
    NSLog(@"match qry is %@", matchQry);
    
    NSMutableArray* matchArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:matchQry];
    
    
    if (matchArray.count>0) {
        
        
        //write predicate to match the locationID
        
        NSPredicate * locationIDPredicate=[NSPredicate predicateWithFormat:@"SELF.Location_ID == %@", locationID];
        
        NSLog(@"predicate for matching is %@", locationIDPredicate);
        
        NSArray* predicateArray=[matchArray filteredArrayUsingPredicate:locationIDPredicate];
        
        NSLog(@"predicate array response is %@", predicateArray);
        
        if (predicateArray.count>0) {
            
            return YES;
        }
        else
        {
            return NO;
        }
        
        
    }
    
    else
    {
        return NO;
        
    }
    
    

    
}

+(BOOL)doctorMatcheswithLocation:(NSString*)locationID andDoctorID:(NSString*)doctorID
{
    NSString* matchQry=[NSString stringWithFormat:@"select IFNULL(Location_ID,'N/A') AS Location_ID  from PHX_TBL_Doctor_location_map where Doctor_ID ='%@' and Location_ID='%@' ",doctorID,locationID];
    
    NSLog(@"match qry is %@", matchQry);
    
    NSMutableArray* matchArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:matchQry];
    
    
    if (matchArray.count>0) {
        
        
        //write predicate to match the locationID
        
        NSPredicate * locationIDPredicate=[NSPredicate predicateWithFormat:@"SELF.Location_ID == %@", locationID];
        
        NSLog(@"predicate for matching is %@", locationIDPredicate);
        
        NSArray* predicateArray=[matchArray filteredArrayUsingPredicate:locationIDPredicate];
        
        NSLog(@"predicate array response is %@", predicateArray);
        
        if (predicateArray.count>0) {
            
            return YES;
        }
        else
        {
            return NO;
        }
        
        
    }
    
    else
    {
        return NO;
        
    }
    
    
    
}


#pragma mark Visit Filter methods

+(NSMutableArray*)fetchLocationDetailswithLocationID:(NSString*)columnName locationID:(NSString*)locID

{
    NSString* filteredContentQry=[NSString stringWithFormat:@"SELECT IFNULL(%@,'N/A') AS %@  from PHX_TBL_Locations where Location_ID= '%@' order By Location_name ASC ",columnName,columnName,locID];
    
    
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:filteredContentQry];
    
    if (filteredContentArray.count>0) {
        
        return [filteredContentArray objectAtIndex:0];
    }
    else
    {
        return nil;
        
    }
}

+(NSMutableArray*)fetchLocationIDwithLocationName:(NSString*)locationName
{
    
    NSString* filteredContentQry=[NSString stringWithFormat:@"select IFNULL(Location_ID,'N/A')Location_ID  from  PHX_TBL_Locations where Location_Name='%@'",locationName];
    
    
    
    NSMutableArray* filteredContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:filteredContentQry];
    
    if (filteredContentArray.count>0) {
        
        return [filteredContentArray objectAtIndex:0];
    }
    else
    {
        return nil;
        
    }

}


+(NSMutableArray*)fetchTradeChannelforCustomerNumber:(NSString*)customerNumber
{
    NSString* tradeChannelQry=[NSString stringWithFormat:@"select IFNULL(Trade_Classification,'N/A') AS Trade_Channel from TBL_Customer where Customer_No='%@'",customerNumber];
    
    
    
    NSMutableArray* tradeChannelArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:tradeChannelQry];
    
    if (tradeChannelArray.count>0) {
        
        return tradeChannelArray ;
    }
    else
    {
        return nil;
        
    }

}

#pragma mark Custom Object Methods

+(NSMutableArray*)fetchDoctorObjects
{
    NSString* doctorsQuery=[NSString stringWithFormat:@"select IFNULL( D.Doctor_ID,'N/A')AS Doctor_ID, IFNULL(DL.Location_ID,'N/A') AS Location_ID,IFNULL(D.Email,'N/A') As EMAIL,IFNULL(DL.Location_Type,'N/A') AS Location_Type,IFNULL(DL.Location_Code,'N/A') AS Location_Code, IFNULL( D.Doctor_Name,'N/A')AS Doctor_Name,  IFNULL( D.Doctor_Code,'N/A')AS Doctor_Code,IFNULL(D.Classification_1, 'N/A') AS Specialization, IFNULL(D.Custom_Attribute_4, 'N/A') AS Adoption_Style, IFNULL(D.Classification_5, 'N/A') AS Personality_Style, Case  when IFNULL(D.Is_Active,'N/A')  ='Y' then 'Active' when IFNULL(D.Is_Active,'N/A')  ='N' then 'InActive' END AS Is_Active,IFNULL(D.Phone,'N/A') AS Phone,IFNULL(D.Email,'N/A') AS Email,IFNULL(D.Classification_2,'N/A') AS Class, IFNULL(D.Classification_3,'N/A') as OTC,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(DL.Custom_Attribute_2,'N/A') AS Account_Name,IFNULL(DL.Location_Name,'N/A') AS Location_Name,IFNULL(DL.Address_2,'N/A') AS Location, IFNULL(DL.City,'N/A') AS Territory, IFNULL(DL.State,'N/A') AS State,IFNULL(DL.Email,'N/A') AS Email ,IFNULL(DL.Phone,'N/A') AS Telephone , IFNULL(DL.Postal_Code,'N/A') AS Postal_Code,IFNULL(DL.Latitude,0) AS Latitude ,IFNULL(DL.Longitude,0)AS Longitude from PHX_TBL_Doctors AS D left join  PHX_TBL_Doctor_Location_map L on D.Doctor_ID=L.Doctor_ID  left join PHX_TBL_Locations DL on L.Location_ID=DL.Location_ID order by D.Doctor_name ASC"];
    
    
    
    NSMutableArray *doctorsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorsQuery];
    NSMutableArray * doctorObjectsArray=[[NSMutableArray alloc]init];
    
    NSLog(@"doctor objects count is %lu", (unsigned long)doctorsArray.count);
    
    if (doctorsArray.count>0) {
        
        Doctors * doctor;
        
        for ( NSInteger i=0; i<doctorsArray.count; i++) {
            
            NSMutableDictionary * doctorDict=[[NSMutableDictionary alloc]init];
            doctorDict=[doctorsArray objectAtIndex:i];
            doctor=[Doctors new];
            doctor.Doctor_ID=[doctorDict valueForKey:@"Doctor_ID"];
            doctor.Doctor_Name=[doctorDict valueForKey:@"Doctor_Name"];
            doctor.Display_Name=[doctorDict valueForKey:@"Doctor_Name"];
            doctor.Doctor_Code=[doctorDict valueForKey:@"Doctor_Code"];
            doctor.Classification_1=[doctorDict valueForKey:@"Specialization"];
            doctor.Classification_2=[doctorDict valueForKey:@"Class"];
            doctor.Classification_3=[doctorDict valueForKey:@"OTC"];
            doctor.isActive=[doctorDict valueForKey:@"Is_Active"];
            doctor.isDeleted=[doctorDict valueForKey:@"Is_Deleted"];
            doctor.adoptionStyle=[doctorDict valueForKey:@"Adoption_Style"];
            doctor.personalityStyle=[doctorDict valueForKey:@"Personality_Style"];
            doctor.Email=[doctorDict valueForKey:@"EMAIL"];
            doctor.Custom_Attribute_1=[doctorDict valueForKey:@"Trade_Channel"];
            doctor.Phone=[doctorDict valueForKey:@"Phone"];
            doctor.locations=[Locations new];
            doctor.locations.Location_ID=[doctorDict valueForKey:@"Location_ID"];
           // NSLog(@"doctor dict in db %@", doctor.locations.Location_ID);
            doctor.locations.Location_Name=[doctorDict valueForKey:@"Location_Name"];
            doctor.locations.Location_Type=[doctorDict valueForKey:@"Location_Type"];
            doctor.locations.Location_Code=[doctorDict valueForKey:@"Location_Code"];
            doctor.locations.City=[doctorDict valueForKey:@"Territory"];
            doctor.locations.Phone=[doctorDict valueForKey:@"Telephone"];
            doctor.locations.Postal_Code=[doctorDict valueForKey:@"Postal_Code"];
            doctor.locations.Latitude=[doctorDict valueForKey:@"Latitude"];
            doctor.locations.Longitude=[doctorDict valueForKey:@"Longitude"];
            doctor.locations.Address_2=[doctorDict valueForKey:@"Location"];
            doctor.locations.Custom_Attribute_2=[doctorDict valueForKey:@"Account_Name"];
            [doctorObjectsArray addObject:doctor];
        }
        
        return doctorObjectsArray;
    }
    
    else
    {
        return nil;
    }
}


+(BOOL)createContactforPharmacy:(PharmacyContact*)contact
{
    
    
    
    BOOL contactDataInsertedSuccessfully=YES;
    
    BOOL locationContactMapDataInsertedSuccessfully=NO;
    
    

    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];

    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];

    
    
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    
    
    
    [database open];
    
    
    [database beginTransaction];
    
        //insert data to TBL_Contacts
        
        @try {
            
            contactDataInsertedSuccessfully=[database executeUpdate:@"INSERT OR REPLACE INTO PHX_TBL_Contacts (Contact_ID,Contact_Name,Title,Email,Phone,Is_Active,Is_Deleted,Created_At,Created_By,Custom_Attribute_1) VALUES (?,?,?,?,?,?,?,?,?,?)",contact.contactID,contact.contactName,contact.contactTitle,contact.contactEmail,contact.contactTelephone,@"Y",@"N",createdAt,createdBy,@"IPAD",nil];
            
        }
        @catch (NSException *exception) {
            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
            
        }
        @finally {
            
            
        }
        
        
        if (contactDataInsertedSuccessfully==YES) {
            
            // insert into PHX_TBL_Location_Contact_Map
            
            
            @try {
                
                locationContactMapDataInsertedSuccessfully=[database executeUpdate:@"INSERT OR REPLACE INTO PHX_TBL_Location_Contact_Map (Location_ID,Contact_ID,Created_At,Created_By) VALUES (?,?,?,?)",contact.locationID,contact.contactID,createdAt,createdBy,nil];
                
            }
            @catch (NSException *exception) {
                NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                
            }
            @finally {
                
                
            }
            
            
        }
    
    if (!contactDataInsertedSuccessfully || !locationContactMapDataInsertedSuccessfully ) {
        
        //something went wrong rollback
        
        
        
        BOOL transactionActive=[database inTransaction];
        
        if (transactionActive) {
            NSLog(@"transaction is active");
        }
        
        else
        {
            NSLog(@"transaction is in active");
            
        }
        
        
        [database rollback];
        NSLog(@"Error rolling back  %@ - %d", [database lastErrorMessage], [database lastErrorCode]);
        
        [database close];
        
        
        
        
        return NO;
    }
    
    else
    {
        //everything alright commit
        [database commit];
        
        [database close];
        
        return YES;
        
        
    }
    
    //return insertIntoPlannedVisits;
    
    

        
}

#pragma mark Selected Visit Data queries

+(NSMutableDictionary*)fetchVisitDataforSelectedDoctor:(NSString*)doctorID andDemoPlanID:(NSString*)demoPlanID

{
    NSString* visitDataQry=[NSString stringWithFormat:@"SELECT 'NOTE' As Data_Type, COUNT(*) As Data_Value FROM PHX_TBL_Emp_Notes WHERE Doctor_ID='%@' UNION SELECT 'TASK_OPEN' As Data_Type, COUNT(*) As Data_Value FROM PHX_TBL_Tasks WHERE Doctor_ID='%@' AND End_Time IS NULL UNION SELECT 'TASK_OVERDUE' As Data_Type, COUNT(*) As Data_Value FROM PHX_TBL_Tasks WHERE Doctor_ID='%@' AND End_Time IS NULL AND Start_Time<DATE('NOW') UNION SELECT 'DEMO_PLAN_NAME' As Data_Type, Description As Data_Value FROM PHX_TBL_Demo_Plan WHERE Demo_Plan_ID='%@' UNION SELECT ('MEDIA_' || C.Media_Type) As Data_Type,COUNT(1) As Data_Value FROM PHX_TBL_Demo_Plan_Media As B INNER JOIN TBL_Media_Files As C ON B.Media_File_ID=C.Media_File_ID WHERE B.Demo_Plan_ID='%@' AND C.Download_Flag='N' AND C.Is_Deleted='N' GROUP BY C.Media_Type",doctorID,doctorID,doctorID,demoPlanID,demoPlanID ];
    
    
    NSMutableArray* doctorDescArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitDataQry];
    
    NSLog(@"Doctor data qry is %@", doctorDescArray);

    
    NSMutableDictionary * contentDict=[[NSMutableDictionary alloc]init];
    
    
    if (doctorDescArray.count>0) {
        
        
        
        
        
        for (NSInteger i=0; i<doctorDescArray.count; i++) {
            
            NSMutableDictionary* currentVisitDictionary=[doctorDescArray objectAtIndex:i];
            
            if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"DEMO_PLAN_NAME"]) {
                
                
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
                
            }
            
            else    if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"MEDIA_Brochure"]) {
                
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
            }
            
            else    if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"MEDIA_Image"]) {
                
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
            }
            else   if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"MEDIA_Video"]) {
                  [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];

            }
            else  if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"NOTE"]) {
                  [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
            }
            
            else   if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"TASK_OVERDUE"]) {
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
                
            }
            
            else  if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"TASK_OPEN"]) {
                      [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];

            }
            
            
          
            
            
            else
            {
                
            }
            
        }

    
        
       // NSLog(@"content dict is %@", contentDict);
        
        return contentDict;
    }
    else
    {
        return 0;
    }
    
}



+(NSMutableDictionary*)fetchVisitDataforSelectedPharmacy:(NSString*)contactID andDemoPlanID:(NSString*)demoPlanID
{
    NSString* visitDetailsQry=[NSString stringWithFormat:@"SELECT 'NOTE' As Data_Type, COUNT(*) As Data_Value FROM PHX_TBL_Emp_Notes WHERE Contact_ID='%@' UNION SELECT 'TASK_OPEN' As Data_Type, COUNT(*) As Data_Value FROM PHX_TBL_Tasks WHERE Contact_ID='%@' AND End_Time IS NULL UNION SELECT 'TASK_OVERDUE' As Data_Type, COUNT(*) As Data_Value FROM PHX_TBL_Tasks WHERE Contact_ID='%@' AND End_Time IS NULL AND Start_Time<DATE('NOW') UNION SELECT 'DEMO_PLAN_NAME' As Data_Type, Description As Data_Value FROM PHX_TBL_Demo_Plan WHERE Demo_Plan_ID='%@' UNION SELECT ('MEDIA_' || C.Media_Type) As Data_Type,COUNT(1) As Data_Value FROM PHX_TBL_Demo_Plan_Media As B INNER JOIN TBL_Media_Files As C ON B.Media_File_ID=C.Media_File_ID WHERE B.Demo_Plan_ID='%@' AND C.Download_Flag='N' AND C.Is_Deleted='N' GROUP BY C.Media_Type",contactID,contactID,contactID,demoPlanID ,demoPlanID];
    
    NSLog(@"pharmacy details query %@", visitDetailsQry);
    
   // NSLog(@"visit data for selected pharmacy is %@", visitDetailsQry);
    
    NSMutableArray* pharmacyDescArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitDetailsQry];
    
    NSMutableDictionary* contentDict=[[NSMutableDictionary alloc]init];
    
    
    if (pharmacyDescArray.count>0) {
        
        for (NSInteger i=0; i<pharmacyDescArray.count; i++) {
            
            NSMutableDictionary* currentVisitDictionary=[pharmacyDescArray objectAtIndex:i];
            
            if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"DEMO_PLAN_NAME"]) {
                
                
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
                
            }
            
            else    if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"MEDIA_Brochure"]) {
                
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
            }
            
            else    if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"MEDIA_Image"]) {
                
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
            }
            else   if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"MEDIA_Video"]) {
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
                
            }
            else  if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"NOTE"]) {
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
            }
            
            else   if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"TASK_OVERDUE"]) {
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
                
            }
            
            else  if ([[currentVisitDictionary valueForKey:@"Data_Type"] isEqualToString:@"TASK_OPEN"]) {
                [contentDict setValue:[currentVisitDictionary valueForKey:@"Data_Value"] forKey:[currentVisitDictionary valueForKey:@"Data_Type"]];
                
            }
            
            
            
            
            
            else
            {
                
            }
            
        }
        
        
        
        // NSLog(@"content dict is %@", contentDict);
        
        return contentDict;
    }
    else
    {
        return 0;
    }

    
}


#pragma mark App Codes Query

+(NSString*)fetchAppCodeforLocationtype:(NSString*)codeValue
{
    NSString* appCodeQry=[NSString stringWithFormat:@"select Code_Description from TBL_App_Codes where Code_Type='Location Type' and Code_Value='%@'",codeValue];
    
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    
    if (appCodeArray.count>0) {
        
        return [[appCodeArray objectAtIndex:0] valueForKey:@"Code_Description"];
    }
    
    else
    {
        return nil;
    }
}

#pragma mark get Total Completed Visit for Medrep Dashboard

+(NSMutableArray*)fetchCompltedVisitsOfDoctorAndPharmaforCurrentMonth:(NSString*)str
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    dateFormatter.dateFormat=@"yyyy-MM";
    NSString * monthString = [[dateFormatter stringFromDate:[NSDate date]] capitalizedString];
    
    NSString* compltedVisitsQry=[NSString stringWithFormat:@"SELECT * FROM PHX_TBL_Actual_Visits WHERE Doctor_ID %@ and Visit_Start_Date like '%%%@%%'",str,monthString];
    
    NSMutableArray* compltedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:compltedVisitsQry];
    if (compltedVisitsArray.count>0)
    {
        return compltedVisitsArray;
    }
    else
    {
        return nil;
    }
    
}
+(NSMutableArray*)fetchTotalTasksForDoctorAndLocation
{
    NSString *taskdetailsQry=@"";

    if ([[AppControl retrieveSingleton].INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
        taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(Task_ID,'N/A') AS Task_ID, IFNULL(Title,'N/A') AS Title, IFNULL(Description,'N/A') AS Description, IFNULL(Category,'N/A') AS Category, IFNULL(Priority,'N/A')AS  Priority, IFNULL(Start_Time,'N/A') AS Start_Time, IFNULL(Status,'N/A') AS Status,IFNULL(Assigned_To,'N/A') AS Assigned_To,IFNULL(Created_At,'N/A') AS Created_At, IFNULL(Emp_ID,'N/A') AS Emp_ID,IFNULL(Location_ID,'N/A') AS Location_ID, IFNULL(Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks where Is_Active='Y' and Is_Deleted='N'"];
    } else {
        taskdetailsQry=[NSString stringWithFormat:@"select IFNULL(t.Task_ID,'N/A') AS Task_ID, IFNULL(t.Title,'N/A') AS Title, IFNULL(t.Description,'N/A') AS Description,IFNULL(t.Category,'N/A') AS Category, IFNULL(t.Priority,'N/A')AS  Priority, IFNULL(t.Start_Time,'N/A') AS Start_Time, IFNULL(t.Status,'N/A')AS Status,IFNULL(t.Assigned_To,'N/A') AS Assigned_To,IFNULL(t.Created_At,'N/A') AS Created_At, IFNULL(t.Emp_ID,'N/A') AS Emp_ID,IFNULL(t.Location_ID,'N/A') AS Location_ID, IFNULL(t.Show_Reminder,'N/A') Show_Reminder from PHX_TBL_Tasks as T left Join PHX_TBL_Doctors as D on T.Doctor_ID = D.Doctor_ID left join PHX_TBL_Locations L on T.Location_ID = L.Location_ID where T.Is_Active='Y' and T.Is_Deleted='N'and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y'"];
    }
    
    NSMutableArray* taskDataArray=[[NSMutableArray alloc]init];
    NSMutableArray* taskObjectsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:taskdetailsQry];
    
    if (taskObjectsArray.count>0) {
        
        VisitTask * task;
        
        for (NSInteger i=0; i<taskObjectsArray.count; i++) {
            
            task=[[VisitTask alloc]init];
            NSMutableDictionary * currentDict=[taskObjectsArray objectAtIndex:i];
            
            task.Title=[currentDict valueForKey:@"Title"];
            task.Task_ID=[currentDict valueForKey:@"Task_ID"];
            task.Description=[currentDict valueForKey:@"Description"];
            task.Category=[currentDict valueForKey:@"Category"];
            task.Priority=[currentDict valueForKey:@"Priority"];
            task.Start_Time=[currentDict valueForKey:@"Start_Time"];
            task.Status=[currentDict valueForKey:@"Status"];
            task.Assigned_To=[currentDict valueForKey:@"Assigned_To"];
            task.Created_At=[currentDict valueForKey:@"Created_At"];
            task.Emp_ID=[currentDict valueForKey:@"Emp_ID"];
            task.Show_Reminder=[currentDict valueForKey:@"Show_Reminder"];
            
            
            [taskDataArray addObject:task];
        }
        
        if (taskDataArray.count>0) {
            return taskDataArray;
        }
        else
        {
            return 0;
        }
    }
    
    else
    {
        return 0;
    }
}
+(BOOL)deleteFutureVisit:(NSMutableDictionary *)visitDetailsDict
{
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    BOOL success = NO;
    
    @try
    {
        NSString* userId=[[SWDefaults userProfile] stringForKey:@"User_ID"];

         success =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Status ='X',Last_Updated_At=?,Last_Updated_By=?,Custom_Attribute_1=? where Planned_Visit_ID=?",[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],userId,@"Deleted", [visitDetailsDict valueForKey:@"Planned_Visit_ID"],nil];
       // success =  [database executeUpdate:@"delete from PHX_TBL_Planned_Visits where Planned_Visit_ID=?",[visitDetailsDict valueForKey:@"Planned_Visit_ID"], nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    return success;
}

+(NSMutableArray*)fetchCitiesData
{
    NSMutableArray * citiesArray=[[NSMutableArray alloc]init];
    
    NSString* citiesQry=@"select IFNULL(Code_value,'N/A') AS Code_value from TBL_App_Codes where Code_Type ='MEDREP_CITY' and Code_Value NOT NULL";
    citiesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:citiesQry];
    if (citiesArray.count>0) {
        return [citiesArray valueForKey:@"Code_value"];
    }
    else{
        return nil;
    }
}

+(NSMutableArray*)getTBLAppCodesDataForCodeType:(NSString *)codeType
{
    NSMutableArray * appcodesObjectsArray=[[NSMutableArray alloc]init];
    
    NSString* appCodeQry=[NSString stringWithFormat:@"select * from TBL_App_Codes where Code_Type='%@' and Code_Value NOT NULL",codeType];
   NSMutableArray* tempArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    if (tempArray.count>0) {
        
        for (NSInteger i=0; i<tempArray.count; i++) {
            TBLAppCode *appcode=[[TBLAppCode alloc]init];
            appcode.Code_Type=[[tempArray valueForKey:@"Code_Type"]objectAtIndex:i];
            appcode.Code_Value=[[tempArray valueForKey:@"Code_Value"]objectAtIndex:i];
            appcode.Code_Description=[[tempArray valueForKey:@"Code_Description"]objectAtIndex:i];
            [appcodesObjectsArray addObject:appcode];

        }
        
        //updating the order
    }
    return appcodesObjectsArray;
}
/**mark as old Visit*/
+(BOOL)updateVisitDetailsToOldvisit:(NSString*)oldPlannedVisitId
{
    FMDatabase *database = [FMDatabase databaseWithPath:[[MedRepQueries fetchDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    NSString* userId=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    BOOL success = NO;
    @try {
        success =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Status ='X',Last_Updated_At=?,Last_Updated_By=? where Planned_Visit_ID=?",[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],userId, oldPlannedVisitId,nil];
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [database close];
    }
    if (success==YES) {
        NSLog(@"old visit crossed successfully");

    }
    
    return success;
    
}


#pragma mark Object Methods

+(NSMutableArray*)fetchContactsObjectsforPharmacyWithLocationID:(NSString*)locationID
{
    NSString* pharmacyContactsQry=[NSString stringWithFormat:@"select IFNULL(A.Location_ID,'0') AS Location_ID, IFNULL(A.Contact_ID,'0')AS Contact_ID,IFNULL(Contact_Code, '0') AS Contact_Code, IFNULL(Contact_Name,'N/A') AS Contact_Name  from PHX_TBL_LOCATION_CONTACT_MAP AS A  inner join PHX_TBL_Contacts AS B on  A.Contact_ID=B.contact_ID where A.Location_ID='%@'",locationID];
    
    NSLog(@"pharmacy contacts qry %@", pharmacyContactsQry);
    
    
    NSMutableArray* contactsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:pharmacyContactsQry];
    
    
    //NSLog(@"pharmacy contacts data %@", contactsArray);
    
    
    if (contactsArray.count>0) {
        NSMutableArray * contactsContentsArray=[[NSMutableArray alloc]init];
        
        
        for( NSInteger i=0; i<contactsArray.count; i++) {
            NSMutableDictionary * currentDict=[contactsArray objectAtIndex:i];
            Contact * currentContact=[[Contact alloc]init];
            currentContact.Contact_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Contact_ID"]];
            currentContact.Contact_Name=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Contact_Name"]];
            currentContact.Location_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Location_ID"]];
            [contactsContentsArray addObject:currentContact];
            
            
        }
        
        return contactsContentsArray;
    }
    
    else
    {
        return nil;
    }
    
}


+ (FMDatabase *)getDatabase
{
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}

+(BOOL)updateContactDetails:(VisitDetails*)currentVisit

{
    
    
    
    NSString *selectSQL =[NSString stringWithFormat:@"update PHX_TBL_Planned_visits set Contact_ID ='%@' where Planned_visit_ID ='%@'",currentVisit.selectedContact.Contact_ID,currentVisit.Planned_Visit_ID];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:selectSQL];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    
    
    
    return success;
    
    
}

+(NSMutableArray*)fetchDemoPlanObjects
{
    NSString* enableDemoPlanSpecialization = [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_DEMO_PLAN_SPECIALIZATION];
    NSString* demoPlanQry= @"";
    if([enableDemoPlanSpecialization isEqualToString:KAppControlsYESCode]){
        demoPlanQry=[NSString stringWithFormat:@"Select IFNULL(Specialization,'N/A') AS Specialization,IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Description,'N/A') AS Description, IFNULL(Valid_From,'N/A')Valid_From, IFNULL(Valid_To,'N/A') AS Valid_To, IFNULL(Is_Active,'N/A') AS Is_Active,IFNULL(Is_Deleted,'N/A') AS Is_Deleted from  PHX_TBL_demo_plan where   date(Valid_From)<=date('now') and  date(Valid_To)>=date('now') and Is_Active ='Y' and Is_Deleted='N'"];

    }else{
        demoPlanQry=[NSString stringWithFormat:@"Select IFNULL(Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(Description,'N/A') AS Description, IFNULL(Valid_From,'N/A')Valid_From, IFNULL(Valid_To,'N/A') AS Valid_To, IFNULL(Is_Active,'N/A') AS Is_Active,IFNULL(Is_Deleted,'N/A') AS Is_Deleted from  PHX_TBL_demo_plan where   date(Valid_From)<=date('now') and  date(Valid_To)>=date('now') and Is_Active ='Y' and Is_Deleted='N'"];
    }
    NSMutableArray * demoPlanContentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:demoPlanQry];
    NSMutableArray * demoPlanArray=[[NSMutableArray alloc]init];

    if (demoPlanContentArray.count>0) {
        
        for (NSInteger i=0; i<demoPlanContentArray.count; i++) {
            
            NSMutableDictionary * demoPlanDict=[demoPlanContentArray objectAtIndex:i];
            DemoPlan * currentDemoPlan=[[DemoPlan alloc]init];
            currentDemoPlan.Demo_Plan_ID=[SWDefaults getValidStringValue:[demoPlanDict valueForKey:@"Demo_Plan_ID"]];
            currentDemoPlan.Description=[SWDefaults getValidStringValue:[demoPlanDict valueForKey:@"Description"]];
            currentDemoPlan.Valid_From=[SWDefaults getValidStringValue:[demoPlanDict valueForKey:@"Valid_From"]];
            currentDemoPlan.Valid_To=[SWDefaults getValidStringValue:[demoPlanDict valueForKey:@"Valid_To"]];
            currentDemoPlan.Is_Active=[SWDefaults getValidStringValue:[demoPlanDict valueForKey:@"Is_Active"]];
            currentDemoPlan.Is_Deleted=[SWDefaults getValidStringValue:[demoPlanDict valueForKey:@"Is_Deleted"]];
            if([enableDemoPlanSpecialization isEqualToString:KAppControlsYESCode]){
            currentDemoPlan.Specialization=[SWDefaults getValidStringValue:[demoPlanDict valueForKey:@"Specialization"]];
            }
            
            [demoPlanArray addObject:currentDemoPlan];
            
        }
    }
    if (demoPlanArray.count>0) {
        
        return demoPlanArray;
    }
    else
    {
        return [[NSMutableArray alloc] init];
    }
    
}

+(NSMutableArray*)fetchDoctorObjectswithLocationID:(NSString*)locationID
{
    NSString *doctorsQry=[NSString stringWithFormat:@"select IFNULL(DL.Doctor_ID,'0') AS Doctor_ID, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,IFNULL(D.Doctor_Code,'N/A') AS Doctor_Code,IFNULL(D.Title,'N/A') AS Title,IFNULL(D.Name_2,'N/A') AS Name_2,IFNULL(D.Name_3,'N/A') AS Name_3,IFNULL(D.Address_1,'N/A') AS Address_1,IFNULL(D.Address_2,'N/A') AS Address_2,IFNULL(D.Address_3,'N/A') AS Address_3,IFNULL(D.City,'N/A') AS City,IFNULL(D.State,'N/A') AS State, IFNULL(D.Email,'N/A') AS Email, IFNULL(D.Email,'N/A') AS Phone,IFNULL(D.Postal_Code,'N/A') AS Postal_Code,IFNULL(D.Rating_Info,'N/A') AS Rating_Info,IFNULL(D.Classification_1,'N/A') AS Specialization,IFNULL(D.Classification_2,'N/A') AS Class,IFNULL(D.Classification_3,'N/A') AS OTCRX,IFNULL(D.Custom_Attribute_1,'N/A') AS Trade_Channel,IFNULL(D.IS_Active,'N/A') AS IS_Active,IFNULL(D.IS_Deleted,'N/A') AS IS_Deleted,IFNULL(D.Created_At,'N/A') AS Created_At from PHX_TBL_Doctor_location_Map AS DL inner join PHX_TBL_Doctors AS D on Dl.Doctor_ID=D.Doctor_ID where DL.Location_ID='%@' order by Doctor_Name  COLLATE NOCASE ASC ", locationID];
    
    NSMutableArray * contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:doctorsQry];
    NSMutableArray* doctorObjectsArray=[[NSMutableArray alloc]init];
    
    if (contentArray.count>0) {
       // NSLog(@"content array is %@",contentArray);
        for (NSInteger i=0; i<contentArray.count; i++) {
        Doctors * currentDoctor=[[Doctors alloc]init];
        NSMutableDictionary * currentDict=[contentArray objectAtIndex:i];
            currentDoctor.Doctor_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Doctor_ID"]];
            currentDoctor.Doctor_Name=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Doctor_Name"]];
            currentDoctor.Specialization=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Specialization"]];
            
            [doctorObjectsArray addObject:currentDoctor];
            
        }
    }
    if (doctorObjectsArray.count>0) {
        return doctorObjectsArray;
    }
    else
    {
        return nil;
    }
    
    
    
}


+(NSMutableArray*)fetchPlannedVisitsforSelectedDate:(NSString*)selectedDate
{
    //    NSString* plannedVisitsQry=[NSString stringWithFormat:@"select IFNULL(P.Planned_Visit_ID,'0') AS Planned_Visit_ID, IFNULL(P.Visit_Date,'N/A') AS Visit_Date, IFNULL(P.EMP_ID,'0') AS EMP_ID,IFNULL(L.Latitude,'N/A')AS Latitude,IFNULL(L.Longitude,'N/A')AS Longitude,IFNULL(P.Location_ID,'0') AS Location_ID, IFNULL(P.Doctor_ID,'0') AS Doctor_ID ,IFNULL(P.Contact_ID,'0') AS Contact_ID, IFNULL(P.Visit_Status,'N/A') AS Visit_Status, IFNULL(P.Comments, 'N/A') AS Objective , IFNULL(P.Visit_Date,'N/A')AS Visit_Date,IFNULL(P.Created_At,'N/A') AS Created_At,IFNULL(P.Created_By,'N/A') AS Created_By, IFNULL(P.Demo_Plan_ID,'N/A') AS Demo_Plan_ID, IFNULL(P.Visit_Type,'N/A') AS Visit_Type ,IFNULL(L.Location_Type,'N/A') AS Location_Type,IFNULL(L.Address_1,'N/A')AS Address, IFNULL(L.Location_name,'N/A') as Location_Name,IFNULL(L.Classification_3,'N/A') AS OTC ,IFNULL(A.Visit_End_Date,'N/A') AS Last_Visited_At, IFNULL(Contact_Name,'N/A')AS Pharmacy_Contact_Name,IFNULL(L.Custom_Attribute_1,'N/A')AS Trade_Channel, IFNULL(L.Classification_2,'N/A') AS Class,IFNULL(D.Doctor_Name,'N/A')AS Doctor_Name ,IFNULL(D.Classification_2,'N/A')AS Doctor_Class,IFNULL(D.Classification_1,'N/A') AS Specialization from PHX_TBL_Planned_Visits  as P  inner join PHX_TBL_Locations L on P.Location_ID=L.Location_ID left join PHX_TBL_Actual_Visits AS A on A.Location_ID=P.Location_ID left join PHX_TBL_Contacts AS C on C.Contact_ID=P.Contact_ID left join PHX_TBL_Doctors  AS D on P.Doctor_ID=D.Doctor_ID  where Visit_date like '%%%@%%' Order by  Visit_Status  ASC",selectedDate];
    AppControl *appcontrol=[AppControl retrieveSingleton];
    
    NSString* updatedQry=[NSString stringWithFormat:@"select A.visit_start_date AS ActualVisitCallStartTime,A.Visit_End_Date AS ActualVisitCallEndTime,Actual_visit_id,A.Planned_Visit_ID as ActualPlannedVisitID, P.Planned_Visit_ID AS Planned_Visit_ID,P.Visit_Date AS Visit_Date, P.EMP_ID AS EMP_ID,IFNULL(L.Latitude,'0')AS Latitude,IFNULL(L.Longitude,'0')AS Longitude,P.Location_ID AS Location_ID, P.Doctor_ID AS Doctor_ID,P.Contact_ID AS Contact_ID, IFNULL(P.Visit_Status,'N/A') AS Visit_Status,IFNULL(P.Comments, 'N/A') AS Objective ,P.Visit_Date AS Visit_Date,P.Created_At AS Created_At,P.Created_By AS Created_By, P.Demo_Plan_ID AS Demo_Plan_ID,IFNULL(P.Visit_Type,'N/A') AS Visit_Type ,IFNULL(L.Location_Type,'N/A') AS Location_Type,IFNULL(L.Address_1,'N/A')AS Address,IFNULL(L.City,'')AS City,IFNULL(L.Phone,'')AS Phone,IFNULL(L.Location_name,'N/A') as Location_Name, IFNULL(L.Classification_3,'N/A') AS OTC ,A.Visit_End_Date AS Last_Visited_At, IFNULL(Contact_Name,'N/A')AS Pharmacy_Contact_Name,IFNULL(L.Custom_Attribute_1,'N/A')AS Trade_Channel, IFNULL(L.Classification_2,'N/A') AS Class,IFNULL(D.Doctor_Name,'N/A')AS Doctor_Name ,IFNULL(D.Classification_2,'N/A')AS Doctor_Class, IFNULL(D.Classification_1,'N/A') AS Specialization from PHX_TBL_Planned_Visits  as P left join PHX_TBL_Actual_Visits AS A on A.Planned_Visit_ID =P.Planned_Visit_ID inner join PHX_TBL_Locations L on P.Location_ID=L.Location_ID left join PHX_TBL_Contacts AS C on C.Contact_ID=P.Contact_ID left join PHX_TBL_Doctors  AS D on D.Doctor_ID=P.Doctor_ID where Visit_date like  '%%%@%%' and P.Visit_Status!='X' and P.Location_ID!='%@' GROUP by P.Planned_Visit_ID Order by  Visit_Status, Visit_Date  ASC",selectedDate,appcontrol.MEDREP_NO_VISIT_LOCATION];
    
    
    // NSLog(@"planned visits query %@", updatedQry);
    
    NSMutableArray* plannedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:updatedQry];
    if (plannedVisitsArray.count>0) {
        
        return plannedVisitsArray;
    }
    
    else
    {
        return nil;
    }
    
}
+(NSMutableArray*)fetchCallsInformationFroSelectedVisit:(NSString*)PlannedVisitId
{
    NSString* updatedQry=[NSString stringWithFormat:@"select A.Call_Started_At AS ActualVisitCallStartTime,A.Call_Ended_At AS ActualVisitCallEndTime,Actual_visit_id,A.Planned_Visit_ID as ActualPlannedVisitID, P.Planned_Visit_ID AS Planned_Visit_ID,P.Visit_Date AS Visit_Date, P.EMP_ID AS EMP_ID,IFNULL(L.Latitude,'0')AS Latitude,IFNULL(L.Longitude,'0')AS Longitude,P.Location_ID AS Location_ID, A.Doctor_ID AS Doctor_ID,A.Contact_ID AS Contact_ID, IFNULL(P.Visit_Status,'N/A') AS Visit_Status,IFNULL(P.Comments, 'N/A') AS Objective ,P.Visit_Date AS Visit_Date,P.Created_At AS Created_At,P.Created_By AS Created_By, A.Custom_Attribute_3 AS Demo_Plan_ID,IFNULL(P.Visit_Type,'N/A') AS Visit_Type ,IFNULL(L.Location_Type,'N/A') AS Location_Type,IFNULL(L.Address_1,'N/A')AS Address,IFNULL(L.City,'')AS City,IFNULL(L.Phone,'')AS Phone,IFNULL(L.Location_name,'N/A') as Location_Name, IFNULL(L.Classification_3,'N/A') AS OTC ,A.Visit_End_Date AS Last_Visited_At, IFNULL(Contact_Name,'N/A')AS Pharmacy_Contact_Name,IFNULL(L.Custom_Attribute_1,'N/A')AS Trade_Channel, IFNULL(L.Classification_2,'N/A') AS Class,IFNULL(D.Doctor_Name,'N/A')AS Doctor_Name ,IFNULL(D.Classification_2,'N/A')AS Doctor_Class, IFNULL(D.Classification_1,'N/A') AS Specialization from PHX_TBL_Planned_Visits  as P left join PHX_TBL_Actual_Visits AS A on A.Planned_Visit_ID =P.Planned_Visit_ID inner join PHX_TBL_Locations L on A.Location_ID=L.Location_ID left join PHX_TBL_Contacts AS C on C.Contact_ID=A.Contact_ID left join PHX_TBL_Doctors  AS D on D.Doctor_ID=A.Doctor_ID  where P.Planned_Visit_ID= '%@' and P.Visit_Status!='X' Order by  Visit_Status, Visit_Date  ASC",PlannedVisitId];
    
    NSMutableArray* completedCallsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:updatedQry];
    if (completedCallsArray.count>0) {
        
        return completedCallsArray;
    }
    
    else
    {
        return nil;
    }
    
}

#pragma mark Multi Visit completed calls method

+(NSMutableArray*)fetchCompletedCallsData:(VisitDetails*)currentVisit
{
    NSMutableArray * contentArray=[[NSMutableArray alloc]init];
    NSMutableArray* completedCallsarray =[[NSMutableArray alloc]init];
    
    NSString* contentQry=[[NSString alloc]init];
    
    if ([currentVisit.Location_Type isEqualToString:kDoctorLocation]) {
        
         contentQry=[NSString stringWithFormat:@"select IFNULL(D.Description,'N/A') AS Description, IFNULL(A.Call_Started_At,'N/A') AS Call_Started_At, IFNULL(C.Doctor_Name,'N/A') AS Contact_Name,IFNULL(A.Location_ID,'N/A') AS Location_ID, IFNULL(A.Custom_Attribute_3,'N/A') AS Demo_plan_ID from phx_tbl_actual_visits AS A left join PHX_TBL_Doctors AS C on A.Doctor_ID=C.Doctor_ID  left join phx_tbl_demo_plan AS D on A.Custom_Attribute_3 =D.Demo_Plan_ID where Planned_visit_ID  ='%@'",currentVisit.Planned_Visit_ID];
    }
    else if ([currentVisit.Location_Type isEqualToString:kPharmacyLocation])
    {
        contentQry=[NSString stringWithFormat:@"select IFNULL(D.Description,'N/A') AS Description, IFNULL(A.Call_Started_At,'N/A') AS Call_Started_At, IFNULL(C.Contact_Name,'N/A') AS Contact_Name,IFNULL(A.Location_ID,'N/A') AS Location_ID, IFNULL(A.Custom_Attribute_3,'N/A') AS Demo_plan_ID from phx_tbl_actual_visits AS A left join PHX_TBL_Contacts AS C on A.Contact_ID=C.Contact_ID  left join phx_tbl_demo_plan AS D on A.Custom_Attribute_3 =D.Demo_Plan_ID where Planned_visit_ID ='%@'",currentVisit.Planned_Visit_ID];
        
     
    }
    
    NSLog(@"content qry is %@", contentQry);
    
    contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:contentQry];
    if (contentArray.count>0) {
        
        NSLog(@"current visit calls %@", contentArray);
    
        for (NSInteger i=0; i<contentArray.count; i++) {
            
            NSMutableDictionary * currentCallDict=[contentArray objectAtIndex:i];
            Calls * currentCall=[[Calls alloc]init];
            currentCall.Description=[MedRepDefaults getDefaultStringForEmptyString:[currentCallDict valueForKey:@"Description"]];
            
            
            
            NSString* refinedDate=[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"hh:mm a" scrString:[currentCallDict valueForKey:@"Call_Started_At"]]];
            
           
            
            currentCall.Call_Started_At=refinedDate;

            
            currentCall.Contact_Name=[MedRepDefaults getDefaultStringForEmptyString:[currentCallDict valueForKey:@"Contact_Name"]];
            currentCall.Location_ID=[MedRepDefaults getDefaultStringForEmptyString:[currentCallDict valueForKey:@"Location_ID"]];
            currentCall.Demo_Plan_ID=[MedRepDefaults getDefaultStringForEmptyString:[currentCallDict valueForKey:@"Demo_Plan_ID"]];
            [completedCallsarray addObject:currentCall];
            
        }
        
    }
    
    if (completedCallsarray.count>0) {
        
        return completedCallsarray;
    }
    else
    {
        return nil;
    }
    
    
}

+(BOOL)updateVisitStatus:(VisitDetails*)currentVisit
{
    FMDatabase * database=[self getDatabase];
    
    [database open];
    BOOL insertIntoPlannedVisits=NO;
    
    NSLog(@"updating planned visit status with %@ %@ %@", [MedRepQueries fetchCurrentDateTimeinDatabaseFormat],currentVisit.Created_By,currentVisit.Planned_Visit_ID);

    
    @try {
        
        //currentVisit.Created_At was getting inserted instead of current time, this was causing an issue, even if visit is completed the visit status from backend after sync will still be N,so updating the last_updated_At to current time.
        insertIntoPlannedVisits =  [database executeUpdate:@"Update  PHX_TBL_Planned_Visits set Visit_Status ='Y' ,Last_Updated_At= ?, Last_Updated_By=? where Planned_Visit_ID=?",[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],currentVisit.Created_By,currentVisit.Planned_Visit_ID, nil];
        
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception updating database: %@", exception.reason);
    }
    @finally
    {
         [database close];
        
    }
    return insertIntoPlannedVisits;
    
    
}

+(NSMutableArray*)fetchReasonsforNoCallBrandAmbassadorVisit
{
    NSString* reasonsQry=[NSString stringWithFormat:@"select IFNULL(Code_Value,'N/A') AS Code_Value  from TBL_App_Codes where Code_Type ='REASON_NO_BA_CALL'"];
    
    NSMutableArray * noCallsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:reasonsQry];
    if (noCallsArray.count>0) {
        return [noCallsArray valueForKey:@"Code_Value"];
    }
    else
    {
        return nil;
    }
}

+(NSMutableArray*)fetchReasonsforNoCall
{
    NSString* reasonsQry=[NSString stringWithFormat:@"select IFNULL(Code_Value,'N/A') AS Code_Value  from TBL_App_Codes where Code_Type ='REASON_NO_CALL'"];

    NSMutableArray * noCallsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:reasonsQry];
    if (noCallsArray.count>0) {
        return [noCallsArray valueForKey:@"Code_Value"];
    }
    else
    {
        return nil;
    }
}


+(NSMutableDictionary *)fetchStatusCodesForBrandAmbassadorTasksandNotes:(SalesWorxBrandAmbassadorVisit*)visitDetails
{
    
    NSMutableDictionary * statusCodesDict=[[NSMutableDictionary alloc]init];
    
    
    for (VisitTask * task in visitDetails.tasksArray) {
        
        NSLog(@"task status is %@", task.Status);
        
        NSString* taskDueDate=task.Start_Time;
        
        NSLog(@"task due date is %@", task.Start_Time);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        
        NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setTimeZone:timeZone];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *dateFromString = [[NSDate alloc] init];
        // voila!
        dateFromString = [dateFormatter dateFromString:taskDueDate];
        
        NSLog(@"date being passed %@", dateFromString);
        
        BOOL testTaskDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
        
        NSLog(@"test task date %hhd", testTaskDate);
        
        NSLog(@"task status is %@", task.Status);
        
        if ([task.Status isEqualToString:@"Closed"]) {
            
            [statusCodesDict setValue:KTASKCLOSED forKey:KTASKSTATUSKEY];
            
        }
        else if (testTaskDate==YES ) {
            
            NSLog(@"task status color updated to red");
            [statusCodesDict setValue:KTASKOVERDUE forKey:KTASKSTATUSKEY];
        }
        else
        {
            [statusCodesDict setValue:KTASKPENDING forKey:KTASKSTATUSKEY];
            NSLog(@"task status color updated to green");
            
        }
        
        
    }
    
    if(visitDetails.tasksArray.count==0)
    {
        [statusCodesDict setValue:KTASKUNAVAILABLE forKey:KTASKSTATUSKEY];
        
    }
    
    if (visitDetails.notesArray.count>0) {
        [statusCodesDict setValue:KNOTESAVAILABLE forKey:KNOTESSTATUSKEY];
    }
    else{
        [statusCodesDict setValue:KNOTESUNAVAILABLE forKey:KNOTESSTATUSKEY];
        
    }
    return statusCodesDict;
    
}
+(NSMutableDictionary*)fetchStatusCodeForTaskandNotes:(VisitDetails*)visitDetails
{
    
    NSMutableDictionary * statusCodesDict=[[NSMutableDictionary alloc]init];
    
    
    for (VisitTask * task in visitDetails.taskArray) {
        
        NSLog(@"task status is %@", task.Status);
        
        NSString* taskDueDate=task.Start_Time;
        
        NSLog(@"task due date is %@", task.Start_Time);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        
        NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setTimeZone:timeZone];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *dateFromString = [[NSDate alloc] init];
        // voila!
        dateFromString = [dateFormatter dateFromString:taskDueDate];
        
        NSLog(@"date being passed %@", dateFromString);
        
        BOOL testTaskDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
        
        NSLog(@"test task date %hhd", testTaskDate);
        
        NSLog(@"task status is %@", task.Status);
        
        if ([task.Status isEqualToString:@"Closed"]) {
            
            [statusCodesDict setValue:KTASKCLOSED forKey:KTASKSTATUSKEY];
            
        }
        else if (testTaskDate==YES ) {
            
            NSLog(@"task status color updated to red");
            [statusCodesDict setValue:KTASKOVERDUE forKey:KTASKSTATUSKEY];
        }
        else
        {
            [statusCodesDict setValue:KTASKPENDING forKey:KTASKSTATUSKEY];
            NSLog(@"task status color updated to green");
            
        }
        
        
    }
    
    if(visitDetails.taskArray.count==0)
    {
        [statusCodesDict setValue:KTASKUNAVAILABLE forKey:KTASKSTATUSKEY];

    }
    
    if (visitDetails.notesArray.count>0) {
        [statusCodesDict setValue:KNOTESAVAILABLE forKey:KNOTESSTATUSKEY];
    }
    else{
        [statusCodesDict setValue:KNOTESUNAVAILABLE forKey:KNOTESSTATUSKEY];

    }
    return statusCodesDict;

}

+(NSMutableArray *)getInCompleteVisits{
    AppControl *appcontrol=[AppControl retrieveSingleton];

    /*
    NSString *incompleteVisitsQuery=[NSString stringWithFormat:@"Select Planned_Visit_ID,Visit_Date,L.Location_Name from PHX_TBL_Planned_Visits AS P LEFT JOIN  PHX_TBL_Locations AS L ON P.Location_ID=L.Location_ID Where P.Visit_Date<datetime('now', 'localtime') AND P.Custom_Attribute_1 is NULL AND P.Visit_Status!='X' AND  (P.Visit_Type='R' OR P.Visit_Type='N') AND P.Visit_Status='N' AND P.Location_ID!='%@' AND L.Location_Name IS NOT NULL AND P.Planned_Visit_ID IS NOT NULL Order By Visit_Date DESC",appcontrol.MEDREP_NO_VISIT_LOCATION];*/
    
    
    
    NSString* incompleteVisitsQuery = [NSString stringWithFormat:@"Select IFNULL(C.Contact_Name,'N/A') AS Contact_Name, IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,L.Location_Type, Planned_Visit_ID,Visit_Date,L.Location_Name from PHX_TBL_Planned_Visits AS P LEFT JOIN  PHX_TBL_Locations AS L ON P.Location_ID=L.Location_ID  Left JOIN PHX_TBL_Contacts AS C on P.Contact_ID=C.Contact_ID LEFT JOIN  PHX_TBL_Doctors AS D on P.Doctor_ID=D.Doctor_ID Where P.Visit_Date<datetime('now', 'localtime') AND P.Custom_Attribute_1 is NULL AND P.Visit_Status!='X' AND  (P.Visit_Type='R' OR P.Visit_Type='N') AND P.Visit_Status='N' AND P.Location_ID!='%@' AND L.Location_Name IS NOT NULL AND P.Planned_Visit_ID IS NOT NULL    Order By Visit_Date DESC", appcontrol.MEDREP_NO_VISIT_LOCATION];
    
    NSLog(@"incompleteVisitsQuery--  %@",incompleteVisitsQuery);

    NSMutableArray *incompleteVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:incompleteVisitsQuery];
    
    NSLog(@"incompleteVisits--  %@",incompleteVisitsArray);

    return incompleteVisitsArray;
}


+(NSMutableArray*)fetchMediaFilesforAllProducts
{
    NSString* productsQry=[NSString stringWithFormat:@"select  IFNULL(C.Brand_Code,'N/A') AS Brand_Code,IFNULL(C.Product_Code,'N/A') AS Product_Code, IFNULL(A.Media_File_ID,'N/A') AS Media_File_ID ,IFNULL(C.Product_Name,'N/A') AS Product_Name, IFNULL(A.Caption,'N/A') AS Caption , IFNULL(A.Filename,'N/A') AS File_Name,IFNULL(A.Media_Type,'N/A') AS Media_Type,IFNULL(A.Entity_ID_1,'N/A') AS Product_ID,IFNULL(D.Detailed_Info,'N/A') AS Detailed_Info from TBL_Media_Files AS A   inner join PHX_TBL_Products AS C ON  C.Product_ID=A.Entity_ID_1 left join PHX_TBL_Product_Details AS D ON C.Product_ID=D.Product_ID WHERE A.IS_Deleted ='N' and A.Download_Flag='N'"];
    
    NSMutableArray * productsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * mediaFilesArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:productsQry];
    if (mediaFilesArray.count>0) {
        
        for (NSInteger i=0; i<mediaFilesArray.count; i++) {
            
            ProductMediaFile* currentProduct=[[ProductMediaFile alloc]init];
            NSMutableDictionary * currentProductDict=[mediaFilesArray objectAtIndex:i];
            currentProduct.Media_File_ID=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Media_File_ID"]];
            currentProduct.Product_Code=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_Code"]];
            currentProduct.Product_Name=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_Name"]];
            currentProduct.Caption=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Caption"]];
            currentProduct.File_Name=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"File_Name"]];
            currentProduct.Media_Type=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Media_Type"]];
            currentProduct.Entity_ID_1=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_ID"]];
            currentProduct.Caption=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Caption"]];
            currentProduct.Brand_Code=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.Detailed_Info=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Detailed_Info"]];
            currentProduct.isDemoed=NO;
            
            NSString*documentsDirectory;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                documentsDirectory=[SWDefaults applicationDocumentsDirectory];
            }
            
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains
                (NSDocumentDirectory, NSUserDomainMask, YES);
                documentsDirectory = [paths objectAtIndex:0];
            }
            
            currentProduct.File_Path=[documentsDirectory stringByAppendingPathComponent:currentProduct.File_Name];
            [productsArray addObject:currentProduct];
            
        }
        
        
        
        
        return productsArray;
    }
    else
    {
        return nil;
    }
    
}

+(NSMutableArray*)fetchAllproductswithMediaFiles
{
    NSString*productsQry=[NSString stringWithFormat:@"select IFNULL(P.Product_ID,'N/A') AS Product_ID, IFNULL(P.Product_Name,'N/A') AS Product_Name, IFNULL(P.Product_Type,'N/A') AS Product_Type,IFNULL(P.Product_Code,'N/A') AS Product_Code,IFNULL(Brand_Code,'N/A') AS Brand_Code, IFNULL(Agency,'N/A') AS Agency from TBL_Media_Files  as m inner join PHX_TBL_Products as p on p.Product_ID=m.Entity_ID_1 where  m.Download_Flag ='N' group by m.Entity_ID_1"];
    
    NSMutableArray * productsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productsQry];
    NSMutableArray * contentArray=[[NSMutableArray alloc]init];
    
    if (productsArray.count>0) {
        
        for (NSInteger i=0; i<productsArray.count; i++) {
            
            Products * currentProduct=[[Products alloc]init];
            NSMutableDictionary * currentProductDict=[productsArray objectAtIndex:i];

            currentProduct.Brand_Code=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.Product_Name=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Product_Name"]];
            
            currentProduct.Lot_Qty=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"QTY"]];
            currentProduct.Product_ID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Product_ID"]];
            currentProduct.Product_Type=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Product_Type"]];
            currentProduct.Agency=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Agency"]];
            [contentArray addObject:currentProduct];
            
        }
        
        return contentArray;
    }
    else
    {
        return nil;
    }
}

+(NSString *)getPDARIGHTSForSalesRep
{
    NSString*PDARightsQry=@"Select PDA_Rights from tbl_user";
    NSArray *results=[[SWDatabaseManager retrieveManager]fetchDataForQuery:PDARightsQry] ;
    if(results.count>0)
    {
        NSString * PDARightsstring=[[results objectAtIndex:0] valueForKey:@"PDA_Rights"];
        NSLog(@"PDA RIGHTS ARE %@", PDARightsstring);

        return PDARightsstring;

    }
    else
    {
        return @"";
    }

}
+(NSNumber *)fetchCompletedPharmacyVisitsInCurrentMonth{
    NSString *visitsCompletedQuery =@"Select Count(Distinct Planned_Visit_ID) AS NumberOfVisits from PHX_TBL_Actual_Visits where Contact_ID IS NOT NULL AND Call_Ended_At<=datetime('now','localtime') AND Call_Ended_At>=datetime('now','start of month')";
    NSMutableArray* visitsCompletedArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsCompletedQuery];
    if(visitsCompletedArray.count>0){
        return [NSNumber numberWithInteger:[[[visitsCompletedArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
    }else{
        return [NSNumber numberWithInteger:0];
    }

    
}
+(NSNumber *)fetchCompletedDoctorsCallsInCurrentMonth{
    NSString *visitsCompletedQuery=@"";
    
    if ([[AppControl retrieveSingleton].INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
        visitsCompletedQuery = @"Select Count(Distinct Planned_Visit_ID||A.Doctor_ID) AS NumberOfVisits from PHX_TBL_Actual_Visits AS A LEFT JOIN PHX_TBL_Doctors AS D ON A.Doctor_ID=D.Doctor_ID Where A.Doctor_ID IS NOT NULL AND D.Doctor_ID IS NOT NULL AND A.Call_Ended_At<=datetime('now','localtime') AND A.Call_Ended_At>=datetime('now','start of month')";
    } else {
        visitsCompletedQuery = @"Select Count(Distinct Planned_Visit_ID||A.Doctor_ID) AS NumberOfVisits from PHX_TBL_Actual_Visits AS A LEFT JOIN PHX_TBL_Doctors AS D ON A.Doctor_ID=D.Doctor_ID Where A.Doctor_ID IS NOT NULL AND D.Doctor_ID IS NOT NULL AND A.Call_Ended_At<=datetime('now','localtime') AND A.Call_Ended_At>=datetime('now','start of month') AND IFNULL(D.Is_Approved,'Y') = 'Y'";
    }
    
    NSMutableArray* visitsCompletedArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsCompletedQuery];
    if(visitsCompletedArray.count>0){
        return [NSNumber numberWithInteger:[[[visitsCompletedArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
    }else{
        return [NSNumber numberWithInteger:0];
    }
}




+(NSNumber *)fetchNumberOFVisitsCompletedForaDoctorInCurrentMonth:(Doctors *)doctor{
    NSString* visitsCompletedQuery=[NSString stringWithFormat:@"SELECT Count(Distinct Planned_Visit_ID) AS NumberOfVisits FROM PHX_TBL_Actual_Visits  WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') AND Doctor_ID='%@'",doctor.Doctor_ID];
    NSMutableArray* visitsCompletedArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsCompletedQuery];
    if(visitsCompletedArray.count>0){
        return [NSNumber numberWithInteger:[[[visitsCompletedArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
    }else{
        return [NSNumber numberWithInteger:0];
    }
    
}
+(NSNumber *)fetchNumberOFVisitsCompletedForaPharmcyInCurrentMonth:(NSString *)locationId{
    NSString* visitsCompletedQuery=[NSString stringWithFormat:@"Select Count(Distinct Planned_Visit_ID) AS NumberOfVisits from PHX_TBL_Actual_Visits where Contact_ID IS NOT NULL AND Call_Ended_At<=datetime('now','localtime') AND Call_Ended_At>=datetime('now','start of month') AND Location_ID='%@'",locationId];
    NSMutableArray* visitsCompletedArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsCompletedQuery];
    if(visitsCompletedArray.count>0){
        return [NSNumber numberWithInteger:[[[visitsCompletedArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
    }else{
        return [NSNumber numberWithInteger:0];
    }
    
}
+(NSNumber *)fetchCompletedPharmaciesCallsInCurrentMonth
{
    NSString *visitsCompletedQuery=@"";
    
    if ([[AppControl retrieveSingleton].INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
        visitsCompletedQuery =@"Select Count(Distinct Planned_Visit_ID) AS NumberOfVisits from PHX_TBL_Actual_Visits where Contact_ID IS NOT NULL AND Call_Ended_At<=datetime('now','localtime') AND Call_Ended_At>=datetime('now','start of month') AND Location_ID IN(select Location_ID from PHX_TBL_Locations)";
    } else {
        visitsCompletedQuery =@"Select Count(Distinct Planned_Visit_ID) AS NumberOfVisits from PHX_TBL_Actual_Visits where Contact_ID IS NOT NULL AND Call_Ended_At<=datetime('now','localtime') AND Call_Ended_At>=datetime('now','start of month') AND Location_ID IN(select Location_ID from PHX_TBL_Locations where IFNULL(Is_Approved,'Y') = 'Y')";
    }

    NSMutableArray* visitsCompletedArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsCompletedQuery];
    if(visitsCompletedArray.count>0){
        return [NSNumber numberWithInteger:[[[visitsCompletedArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
    }else{
        return [NSNumber numberWithInteger:0];
    }
}

+(NSMutableArray*)fetchCompltedVisitsforCurrentMonthforPharmacy:(NSString*)locationID
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSString* compltedVisitsQry=[NSString stringWithFormat:@"SELECT * FROM PHX_TBL_Actual_Visits  WHERE Call_Ended_At<=datetime('now', 'localtime') AND Call_Ended_At>=datetime('now','start of month') and Location_ID='%@' group by Planned_Visit_ID order by  Call_Ended_At desc ",locationID];
    
    NSMutableArray* compltedVisitsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:compltedVisitsQry];
    if (compltedVisitsArray.count>0) {
        //NSLog(@"complted visits count for pharmacy is %@", [compltedVisitsArray objectAtIndex:0]);
        
        return compltedVisitsArray;
    }
    else
    {
        return nil;
    }
}

+(NSNumber *)fetchNumberOfVisitsRequiredForADoctor:(Doctors*)doctor{
    NSString* visitsReq=[NSString stringWithFormat:@"Select IFNULL(Custom_Attribute_5,(select Custom_Attribute_1 from TBL_App_Codes where IFNULL(Classification_2 ,'N')=Code_Value and Code_Type = 'CLASS')) AS NumberOfVisits from PHX_TBL_Doctors  where Doctor_ID='%@'",doctor.Doctor_ID];
    NSMutableArray* visitsReqArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsReq];
    if(visitsReqArray.count>0){
        return [NSNumber numberWithInteger:[[[visitsReqArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
    }else{
        return [NSNumber numberWithInteger:0];
    }
}
+(NSNumber *)fetchNumberOfVisitsRequiredForPharmaciesInaMonth{
//    NSString* visitsReq=[NSString stringWithFormat:@"Select IFNULL(Custom_Attribute_5,(select Custom_Attribute_1 from TBL_App_Codes where IFNULL(Classification_2 ,'N')=Code_Value and Code_Type = 'CLASS')) AS NumberOfVisits from PHX_TBL_Doctors  where Doctor_ID='%@'",doctor.Doctor_ID];
//    NSMutableArray* visitsReqArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsReq];
//    if(visitsReqArray.count>0){
//        return [NSNumber numberWithInteger:[[[visitsReqArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
//    }else{
//        return [NSNumber numberWithInteger:0];
//    }
    
    return [NSNumber numberWithInteger:0];
}
+(NSNumber *)fetchNumberOfVisitsRequiredForDoctorsInaMonth{
    NSString* visitsReq=@"";
    
    if ([[AppControl retrieveSingleton].INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
        visitsReq = [NSString stringWithFormat:@"Select IFNULL(SUM(IFNULL(Custom_Attribute_5,(select Custom_Attribute_1 from TBL_App_Codes where IFNULL(Classification_2 ,'N')=Code_Value and Code_Type = 'CLASS'))),'0') AS NumberOfVisits from PHX_TBL_Doctors"];
    } else {
        visitsReq = [NSString stringWithFormat:@"Select IFNULL(SUM(IFNULL(Custom_Attribute_5,(select Custom_Attribute_1 from TBL_App_Codes where IFNULL(Classification_2 ,'N')=Code_Value and Code_Type = 'CLASS'))),'0') AS NumberOfVisits from PHX_TBL_Doctors where IFNULL(Is_Approved,'Y') = 'Y'"];
    }

    NSMutableArray* visitsReqArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:visitsReq];
    if(visitsReqArray.count>0){
        return [NSNumber numberWithInteger:[[[visitsReqArray objectAtIndex:0] valueForKey:@"NumberOfVisits"] integerValue]];
    }else{
        return [NSNumber numberWithInteger:0];
    }
}

+(NSMutableArray*)fetchHolidays
{
    NSString* holidaysQry=@"select DISTINCT IFNULL(Holiday_Date,'N/A') AS Holiday_Date from TBL_Holiday";
    NSMutableArray* holidaysArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:holidaysQry];
    if (holidaysArray.count>0) {
        return [holidaysArray valueForKey:@"Holiday_Date"];
    }
    else{
        return [[NSMutableArray alloc]init];
    }
    
}

#pragma mark Task

+(BOOL)UpdateTask:(VisitTask*)currentTask
{
    BOOL status =NO;
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    [database beginTransaction];
    
    @try {
        
        status = [database executeUpdate:@"update PHX_TBL_Tasks set Title=?,Description=?,start_time=?,Status=?,Category=?,Priority=?,Last_Updated_At=?,Custom_Attribute_1='IPAD' where task_ID=?",  currentTask.Title==nil?@"":currentTask.Title,currentTask.Description==nil?@"":currentTask.Description,currentTask.Start_Time, currentTask.Status==nil?@"":currentTask.Status,  currentTask.Category==nil?@"":currentTask.Category,currentTask.Priority==nil?@"":currentTask.Priority,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],currentTask.Task_ID,nil];
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while inserting to do: %@", exception.reason);
        status=NO;
    }
    @finally
    {
        //[database close];
    }
    
    
    if (status==YES) {
        
        [database commit];
        [database close];
    }
    else
    {
        [database rollback];
        [database close];
    }
    
    
    return status;
}

#pragma mark Prevoious Visit Notes
+(NSMutableDictionary*)fetchLastVisitDetails:(NSString*)doctorID
{
    NSMutableDictionary* previousVisitNotesArray=[[NSMutableDictionary alloc]init];
    
    NSString* previousVisitQry=[NSString stringWithFormat:@"select IFNULL(B.Attrib_Value,'') AS Next_Visit_Objective,IFNULL(L.Location_Name,'N/A') AS Last_Visited_At,IFNULL(A.Notes,'N/A')AS Notes,IFNULL(max(A.Call_Ended_At),'') AS Last_Visited_On from PHX_TBL_Actual_Visits AS A left join  TBL_Visit_Addl_Info AS B on A.Actual_visit_ID = B.Visit_ID left join phx_tbl_locations AS L on A.Location_ID = L.Location_ID where  Doctor_ID = '%@' and B.Attrib_Name = 'Next_Visit_Objective' order by  Call_Ended_At desc",doctorID];
    NSLog(@"previous visit query is %@", previousVisitQry);
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:previousVisitQry];
    if (contentArray.count>0) {
        return [contentArray objectAtIndex:0];
    }
    return previousVisitNotesArray;
}

+(NSString*)fetchLastVisitNote:(NSString*)visitID{
    NSString *noteString = [[NSString alloc]init];
    NSString* qryStr=[NSString stringWithFormat:@"select Notes from PHX_TBL_Actual_Visits where Actual_Visit_ID = '%@'",visitID];
    NSArray* notesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:qryStr];
    if (notesArray.count>0) {
        noteString=[NSString getValidStringValue:[[notesArray objectAtIndex:0] valueForKey:@"Notes"]];
    }
    return noteString
    ;
}

+(NSMutableDictionary*)fetchLastVisitDetailsforLocation:(NSString*)locationID
{
    NSMutableDictionary* previousVisitNotesArray=[[NSMutableDictionary alloc]init];
    
    NSString* previousVisitQry=[NSString stringWithFormat:@"select IFNULL(B.Attrib_Value,'') AS Next_Visit_Objective,IFNULL(L.Location_Name,'N/A') AS Last_Visited_At,IFNULL(A.Notes,'N/A')AS Notes,IFNULL(max(A.Call_Ended_At),'') AS Last_Visited_On from PHX_TBL_Actual_Visits AS A left join  TBL_Visit_Addl_Info AS B on A.Actual_visit_ID = B.Visit_ID left join phx_tbl_locations AS L on A.Location_ID = L.Location_ID where  A.Location_ID = '%@' and B.Attrib_Name = 'Next_Visit_Objective' order by  Call_Ended_At desc",locationID];
    NSLog(@"previous visit query is %@", previousVisitQry);
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:previousVisitQry];
    if (contentArray.count>0) {
        return [contentArray objectAtIndex:0];
    }
    return previousVisitNotesArray;
}

+(NSMutableDictionary*)fetchLastVisitDetailsforContactID:(NSString*)contactID
{
    NSMutableDictionary* previousVisitNotesArray=[[NSMutableDictionary alloc]init];
    
    NSString* previousVisitQry=[NSString stringWithFormat:@"select IFNULL(B.Attrib_Value,'') AS Next_Visit_Objective,IFNULL(L.Location_Name,'N/A') AS Last_Visited_At,IFNULL(A.Notes,'N/A')AS Notes,IFNULL(max(A.Call_Ended_At),'') AS Last_Visited_On from PHX_TBL_Actual_Visits AS A left join  TBL_Visit_Addl_Info AS B on A.Actual_visit_ID = B.Visit_ID left join phx_tbl_locations AS L on A.Location_ID = L.Location_ID where  A.Contact_ID ='%@' and B.Attrib_Name = 'Next_Visit_Objective' order by  Call_Ended_At desc",contactID];
    NSLog(@"previous visit query is %@", previousVisitQry);
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:previousVisitQry];
    if (contentArray.count>0) {
        return [contentArray objectAtIndex:0];
    }
    return previousVisitNotesArray;
}



+(NSString*)fetchPreviousVisitObjective:(NSMutableDictionary*)visitDetailsDict
{
    NSString* objectiveString=[[NSString alloc]init];
    
    NSString* locationType=[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Location_Type"]];
    NSString* locationID=[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Location_ID"]];
    NSString* contactID = [[NSString alloc]init];;
    
    if ([locationType isEqualToString:@"D"]) {
        contactID=[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Doctor_ID"]];
    }
    else{
        contactID=[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Contact_ID"]];
        
    }
    NSString* objectiveQry=[NSString stringWithFormat:@"select A.visit_end_Date, IFNULL(B.Attrib_Value,'') AS Next_Visit_Objective from PHX_TBL_Actual_Visits AS A left join  TBL_Visit_Addl_Info AS B on A.Actual_visit_ID = B.Visit_ID  where ((A.Location_ID = '%@' and A.Doctor_ID = '%@') OR (A.Location_ID = '%@' and A.Contact_ID ='%@')) and B.Attrib_Name = 'Next_Visit_Objective' order by A.visit_end_Date desc",locationID,contactID,locationID,contactID];
    NSLog(@"Objective query is %@",objectiveQry);
    NSMutableArray* dataArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:objectiveQry];
    if (dataArray.count>0) {
        objectiveString=[NSString getValidStringValue:[[dataArray objectAtIndex:0] valueForKey:@"Next_Visit_Objective"]];
    }
    
    return objectiveString;
}

+(NSString *)ConvertDateIntoDeviceDateWithTimeDisplayFormat:(NSDate *)date
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:kDateFormatWithTime];
    [dateFormatter setLocale:KUSLOCALE];
    NSDate* currentDate=date;
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}
+(NSString *)ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:(NSDate *)date
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:kDateFormatWithoutTime];
    [dateFormatter setLocale:KUSLOCALE];
    NSDate* currentDate=date;
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}
+(NSDictionary*)fetchTasksDueForTodayForNotifications
{
    NSString* tasksQry = @"";
    if([SWDefaults isDrApprovalAvailable])
    {
        tasksQry = [NSString stringWithFormat:@"select IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,C.Contact_Name,T.Title from PHX_TBL_Tasks AS T left join PHX_TBL_Doctors AS D on T.Doctor_ID = D.Doctor_ID left join PHX_TBL_Contacts AS C on T.Contact_ID = C.Contact_ID where T.start_time like '%%%@%%' and  T.STATUS IN ('New') and D.Is_Approved = 'Y' ",[self fetchDatabaseDateFormat]];

    }else{
        tasksQry = [NSString stringWithFormat:@"select IFNULL(D.Doctor_Name,'N/A') AS Doctor_Name,C.Contact_Name,T.Title from PHX_TBL_Tasks AS T left join PHX_TBL_Doctors AS D on T.Doctor_ID = D.Doctor_ID left join PHX_TBL_Contacts AS C on T.Contact_ID = C.Contact_ID where T.start_time like '%%%@%%' and  T.STATUS IN ('New')",[self fetchDatabaseDateFormat]];

    }
    NSDictionary * notificationsDict = [[NSDictionary alloc]init];
    NSMutableArray* dataArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:tasksQry];
    if (dataArray.count>0) {
        if (dataArray.count == 1)
        {
            NSMutableDictionary * currentDict = dataArray[0];
            NSString* doctorName = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Doctor_Name"]];
            NSString* taskTitle = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Title"]];
            NSString* contactName = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Contact_Name"]];
            NSString* doctorTitle = [NSString stringWithFormat:@"There is a task %@ due today for Dr.%@,please check Tasks screen",taskTitle,doctorName];
            NSString* contactTitle = [NSString stringWithFormat:@"There is a task %@ due today for %@,please check Tasks screen",taskTitle,contactName];
            NSString* titleString = ([NSString isEmpty:doctorName] || [doctorName isEqualToString:KNotApplicable])?contactTitle:doctorTitle;
            notificationsDict= [[NSDictionary alloc]initWithObjectsAndKeys:titleString,@"Title", nil];
        }
        else{
            notificationsDict= [[NSDictionary alloc]initWithObjectsAndKeys:@"There are tasks due today,please check Tasks screen",@"Title", nil];
        }
    }
    else{
    }
    return notificationsDict;
}

+(NSString*)fetchDescriptionForAppCode:(NSString*)appCode
{
    NSString* descriptionString = @"";
    NSString* appCodeQry=[NSString stringWithFormat:@"select  Code_Description from TBL_App_Codes  where Code_Type='%@'",appCode];
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    if (appCodeArray.count>0) {
        NSMutableDictionary* currentDict =appCodeArray[0];
        descriptionString=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Code_Description"]];
    }
    return descriptionString;
}

+(NSMutableArray*)fetchReasonsforRescheduleVisit
{
    NSString* reasonsQry=[NSString stringWithFormat:@"select IFNULL(Code_Value,'N/A') AS Code_Value  from TBL_App_Codes where Code_Type = 'VISIT_CHANGE_REQUEST_REASON'"];
    
    NSMutableArray * noCallsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:reasonsQry];
    if (noCallsArray.count>0) {
        return [noCallsArray valueForKey:@"Code_Value"];
    }
    else
    {
        return nil;
    }
}

@end
