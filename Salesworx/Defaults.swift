//
//  Defaults.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/7/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import Foundation
import SystemConfiguration
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret

    }
}

class SalesWorxDefaults{
    
    static let retrieveDefaults = SalesWorxDefaults()
    init(){}
    
    func refineDateFormat(sourceFormat:String,sourceString:String,destinationFormat:String) -> String
    {
        var dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.locale = Locale(identifier: "en_US")
        
        dateFormatter.dateFormat = sourceFormat
        var myDate: Date? = dateFormatter.date(from: sourceString)
        
        var formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.system
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = destinationFormat
        var stringFromDate: String? = nil
        var refinedDateString = ""
        if let aDate = myDate {
            stringFromDate = formatter.string(from: aDate)
            refinedDateString = stringFromDate ?? ""
        }
        return refinedDateString
    }
    
}
