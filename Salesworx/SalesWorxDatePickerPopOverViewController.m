//
//  SalesWorxDatePickerPopOverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/1/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxDatePickerPopOverViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"


@interface SalesWorxDatePickerPopOverViewController ()

@end

@implementation SalesWorxDatePickerPopOverViewController

@synthesize salesWorxDatePicker,datePickerMode,datePickerPopOverController,setMaximumDateCurrentDate,setMinimumDateCurrentDate, dateRange;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:_titleString];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDateTapped)];
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    saveBtn.tintColor=[UIColor whiteColor];
    
    
    
    UIBarButtonItem * closeBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    [closeBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                            forState:UIControlStateNormal];
    closeBtn.tintColor=[UIColor whiteColor];
    
    
    self.navigationController.navigationBar.topItem.title = @"";
    if (![self.titleString isEqualToString:@"Reschedule Date"]) {
        self.navigationItem.leftBarButtonItem=closeBtn;
    }
    self.navigationItem.rightBarButtonItem=saveBtn;
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    if (_previousSelectedDate==nil) {
        salesWorxDatePicker.date=[NSDate date];
    }
    else{
        salesWorxDatePicker.date = [dateFormatter dateFromString:_previousSelectedDate];
    }
    
    salesWorxDatePicker.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
    if ([_titleString isEqualToString:@"Status"] || [_titleString isEqualToString:NSLocalizedString(@"Select Date", nil)]) {
        salesWorxDatePicker.datePickerMode=UIDatePickerModeDate;
        
        salesWorxDatePicker.minimumDate=[NSDate date];
    }
    
   else if ([datePickerMode isEqualToString:@"dateandTime"]) {
        
        salesWorxDatePicker.datePickerMode=UIDatePickerModeDateAndTime;
        
        salesWorxDatePicker.minimumDate=[NSDate date];
    }
    else if ([_titleString isEqualToString:@"Cheque"])
    {
        @try {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            // this is imporant - we set our input date format to match our input string
            // if format doesn't match you'll get nil from your string, so be careful
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *dateFromString = [dateFormatter dateFromString:_previousSelectedDate];
            
            NSLog(@"%@",[NSDate date]);
            if (self.setMinimumDateCurrentDate == YES) {
                salesWorxDatePicker.minimumDate = [NSDate date];

            }else{
                
                NSDate *currentDate = [NSDate date];
                NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                
                [dateComponents setDay: - [dateRange integerValue]];
                NSDate *dateRangeValue = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
                
                salesWorxDatePicker.minimumDate = dateRangeValue;
                self.setMinimumDateCurrentDate = YES;
            }
                
            salesWorxDatePicker.date = dateFromString;
        }
        @catch (NSException *exception) {
            
        }
    }
    else if ([_titleString isEqualToString:@"PDC"])
    {
        if (self.setMinimumDateCurrentDate==YES)
        {
            
            NSTimeInterval MY_EXTRA_TIME = 86400; // 10 Seconds
            NSDate *futureDate = [[NSDate date] dateByAddingTimeInterval:MY_EXTRA_TIME];
            
            salesWorxDatePicker.minimumDate=futureDate;
        }
        else if (self.setMinimumDateCurrentDate == NO)
        {
            @try
            {
                NSTimeInterval MY_EXTRA_TIME = 86400; // 10 Seconds
                NSDate *futureDate = [[NSDate date] dateByAddingTimeInterval:MY_EXTRA_TIME];
                
                salesWorxDatePicker.minimumDate=futureDate;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateFromString = [dateFormatter dateFromString:_previousSelectedDate];
                
                salesWorxDatePicker.date = dateFromString;
            }
            @catch (NSException *exception) {
                
            }
        }
    }
    
    
     if (![_titleString isEqualToString:@"Cheque"] || setMaximumDateCurrentDate==NO||salesWorxDatePicker.maximumDate==nil) {
        //set maximum date to 2099, as backend is handling only till 2099
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *maximumDate = [dateFormatter dateFromString:kBackendMaxDate];
        salesWorxDatePicker.maximumDate=maximumDate;
    }



    if (setMinimumDateCurrentDate==NO || salesWorxDatePicker.minimumDate==nil) {
        //set minimum date to 1980, as backend is handling only till 1980
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *maximumDate = [dateFormatter dateFromString:kBackendMinDate];
        salesWorxDatePicker.minimumDate=maximumDate;

    }
    
    if ([self.titleString isEqualToString:@"Reschedule Date"] || [_titleString isEqualToString:@"Ship Date"]) {
        salesWorxDatePicker.datePickerMode = UIDatePickerModeDate;
        
        NSTimeInterval MY_EXTRA_TIME = 86400;
        NSDate *futureDate = [[NSDate date] dateByAddingTimeInterval:MY_EXTRA_TIME];
        salesWorxDatePicker.minimumDate=futureDate;
        
        NSRange daysRange = [NSCalendar.currentCalendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:NSDate.date];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorian components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:NSDate.date];
        components.day = daysRange.length;
        NSDate *maximumDate = [gregorian dateFromComponents:components];
        salesWorxDatePicker.maximumDate=maximumDate;
    }
}
-(void)closeButtonTapped
{
    if(datePickerPopOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [datePickerPopOverController dismissPopoverAnimated:YES];
}
-(void)backButtontapped
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)saveDateTapped
{
    if ([self.didSelectDateDelegate respondsToSelector:@selector(didSelectDate:)]) {
        
        NSString *todayString;
        
        //refine date as per DB format
        
        
        
        if ([datePickerMode isEqualToString:@"dateandTime"]) {
            
            NSDateFormatter  *customDateFormatter = [[NSDateFormatter alloc] init];
            [customDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [customDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
            customDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];

            todayString = [NSString stringWithFormat:@" %@",[customDateFormatter stringFromDate:salesWorxDatePicker.date]];
            
            
            //            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            //            myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
            //            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            //
            //            todayString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:visitsDatePicker.date]];
        }
        
        else if ([datePickerMode isEqualToString:kTodoTitle])
        {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];

            NSString*   dateString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:salesWorxDatePicker.date]];
            todayString=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:dateString];
        }
        else if ([_titleString isEqualToString:NSLocalizedString(@"Select Date", nil)])
        {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd";
            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
            
            todayString = [NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:salesWorxDatePicker.date]];
        }
        else
        {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];

         
             todayString = [NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:salesWorxDatePicker.date]];
        }
        
        if ([_titleString isEqualToString:@"Reschedule Date"])
        {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = kDatabseDefaultDateFormatWithoutTime;
            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
            todayString = [NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:salesWorxDatePicker.date]];
            
            [self.navigationController popViewControllerAnimated:true];
            [self.didSelectDateDelegate didSelectDate:todayString];
            
        } else {
            [self.didSelectDateDelegate didSelectDate:todayString];
            if(datePickerPopOverController==nil)
                [self dismissViewControllerAnimated:YES completion:nil];
            else
                [datePickerPopOverController dismissPopoverAnimated:YES];
        }
        NSLog(@"refined date is %@", todayString);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
