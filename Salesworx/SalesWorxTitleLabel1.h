//
//  SalesWorxTitleLabel1.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"

@interface SalesWorxTitleLabel1 : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
@property (nonatomic) IBInspectable BOOL isTextEndingWithExtraSpecialCharacter;

-(NSString*)text;
-(void)setText:(NSString*)newText;

@end
