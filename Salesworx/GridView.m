//
//  GridView.m
//  SWFoundation
//
//  Created by Irfan Bashir on 5/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "GridView.h"
#import "SWFoundation.h"
#import <QuartzCore/QuartzCore.h>
#import "SWDefaults.h"
@implementation GridView

@synthesize columns;
@synthesize dataSource;
@synthesize tableView;
@synthesize columnWidths;
@synthesize delegate= _delegate;
@synthesize shouldAllowDeleting;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //[self setBackgroundColor:];
        if (self.tableView) {
            self.tableView=nil;
        }
        [self setTableView:[[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain] ];
        [self.tableView setDataSource:self];
        [self.tableView setDelegate:self];
        
        if (background) {
            background=nil;
        }
        //background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_grouptablebackground" cache:NO]] ;
        
        [self.tableView setBackgroundColor:[UIColor clearColor]];
        [self.tableView setBackgroundView:background];
        inputColumnIndex = -1;
        
        [self addSubview:self.tableView];
        shouldAllowDeleting = NO;
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.tableView setFrame:self.bounds];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
        //searchController.searchResultsTableView.cellLayoutMarginsFollowReadableWidth=NO;
        
    }

    
}






- (void)deleteRowAtIndex:(int)rowIndex {
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)reloadRowAtIndex:(int)rowIndex {
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = [self.dataSource numberOfRowsInGrid:self];
    
    [self setColumns:[self.dataSource numberOfColumnsInGrid:self]];
    
    if ([self.dataSource respondsToSelector:@selector(indexOfColumnForInput:)]) {
        inputColumnIndex = [self.dataSource indexOfColumnForInput:self];
    }
    
    if ([self.dataSource respondsToSelector:@selector(gridView:widthForColumn:)]) {
        NSMutableArray *widths = [NSMutableArray array];
        for (int i = 0; i < self.columns; i++) {
            [widths addObject:[NSNumber numberWithFloat:[self.dataSource gridView:self widthForColumn:i]]];
        }
        [self setColumnWidths:widths];
    } else {
        NSMutableArray *widths = [NSMutableArray array];
        for (int i = 0; i < self.columns; i++) {
            [widths addObject:[NSNumber numberWithFloat:((float)100 / (float)columns)]];
        }
        [self setColumnWidths:widths];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"GridViewRow";
    GridCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[GridCell alloc] initWithReuseIdentifier:CellIdentifier columnWidths:self.columnWidths andInputColumnIndex:inputColumnIndex] ;
    }
    
    [cell setKey:@""];
    [cell setDelegate:self];
    if ([self.dataSource respondsToSelector:@selector(gridView:keyForRow:)]) {
        [cell setKey:[self.dataSource gridView:self keyForRow:indexPath.row]];
    }
    
    for (int j = 0; j < self.columns; j ++) {
        if ([self.dataSource respondsToSelector:@selector(gridView:textforRow:andColumn:)]) {
            NSString *text = [self.dataSource gridView:self textforRow:indexPath.row andColumn:j];
            
            if (text) {
                [cell setText:text forColumn:j];
            } else {
                [cell setText:@"" forColumn:j];
            }
        } else {
            [cell setText:@"" forColumn:j];
        }
    }
        //testing grid
        cell.textLabel.highlightedTextColor=[UIColor blueColor];
    return cell;
}
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 44)] ;
    [headerView setBackgroundColor:[UIColor lightGrayColor]];
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    float columnWidth = self.bounds.size.width / columns;
    int x = 0;
    for (int j = 0; j < self.columns; j ++) {
        
        if (self.columnWidths) {
            float cw = [[self.columnWidths objectAtIndex:j] floatValue];
            columnWidth = headerView.bounds.size.width * (cw / 100);
        }
        
        GridCellView *gridCell = [[GridCellView alloc] initWithFrame:CGRectMake(x, 0, columnWidth, 44)] ;
        [gridCell setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
        [gridCell setBorderColor:UIColorFromRGB(0xF6F7FB)];
        //[gridCell setBorderType:GridCellBorderTypeBottom | GridCellBorderTypeTop];
        [gridCell.titleLabel setFont:BoldFontOfSize(14.0f)];
        [gridCell.titleLabel setTextColor:UIColorFromRGB(0x687281)];

        NSString *headerTitle = @"";
        
        if ([self.dataSource respondsToSelector:@selector(gridView:titleForColumn:)]) {
            headerTitle = [self.dataSource gridView:self titleForColumn:j];
        }
        
        [gridCell.titleLabel setText:headerTitle];
        
        [headerView addSubview:gridCell];
        
        x = x + columnWidth;
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 1)] ;;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1f;
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
    
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(gridView:didSelectRow:atIndex:)]) {
        GridCellView *cell = (GridCellView *)[tv cellForRowAtIndexPath:indexPath];
        [self.delegate gridView:self didSelectRow:cell atIndex:indexPath.row];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return shouldAllowDeleting;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([self.delegate respondsToSelector:@selector(gridView:commitDeleteRowAtIndex:)]) {
            [self.delegate gridView:self commitDeleteRowAtIndex:indexPath.row];
        }
    }
}

#pragma mark Functions
- (void)reloadData {
    [self.tableView reloadData];
}

- (void)reloadRow:(int)rowIndex {
    NSIndexPath *path = [NSIndexPath indexPathForRow:rowIndex inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:path] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark EditableCell Delegate
- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key {
    if ([self.delegate respondsToSelector:@selector(gridView:didFinishInputForKey:andValue:)]) {
        [self.delegate gridView:self didFinishInputForKey:key andValue:value];
    }
}

- (void)editableCellDidStartUpdate:(EditableCell *)cell {
    if ([self.delegate respondsToSelector:@selector(gridView:willStartInputForRow:andKey:)]) {
        [self.delegate gridView:self willStartInputForRow:(GridCell *)cell andKey:cell.key];
    }
}
@end
