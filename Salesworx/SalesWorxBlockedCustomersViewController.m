//
//  SalesWorxBlockedCustomersViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBlockedCustomersViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxBlockedCustomersTableViewCell.h"

@interface SalesWorxBlockedCustomersViewController ()

@end

@implementation SalesWorxBlockedCustomersViewController
@synthesize arrBlockedCustomerData, tblBlockedCustomer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kBlockedCustomersTitle localizedCapitalizedString], nil)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            NSMutableArray *customerListArray=[[[SWDatabaseManager retrieveManager]fetchBlockedCustomerData] mutableCopy];
            arrBlockedCustomerData = [[NSMutableArray alloc]init];
            if (customerListArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary * currentDict in customerListArray)
                {
                    NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Blocked_Statement *currentCustomer = [[Blocked_Statement alloc] init];
                    
                    currentCustomer.Contact = [currentCustDict valueForKey:@"Contact"];
                    currentCustomer.Customer_ID = [currentCustDict valueForKey:@"Customer_ID"];
                    currentCustomer.Customer_Name = [currentCustDict valueForKey:@"Customer_Name"];
                    currentCustomer.Phone = [currentCustDict valueForKey:@"Phone"];
                    
                    [arrBlockedCustomerData addObject:currentCustomer];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                
                [tblBlockedCustomer reloadData];
            });
        });
    }
}

#pragma mark UITableView Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrBlockedCustomerData.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"blockedCustomer";
    SalesWorxBlockedCustomersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBlockedCustomersTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    
    cell.lblCode.isHeader=YES;
    cell.lblName.isHeader=YES;
    cell.lblContact.isHeader=YES;
    cell.lblPhone.isHeader=YES;
    
    cell.lblCode.text = @"Code";
    cell.lblName.text = @"Name";
    cell.lblContact.text = @"Contact";
    cell.lblPhone.text = @"Phone";
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"blockedCustomer";
    SalesWorxBlockedCustomersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBlockedCustomersTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Blocked_Statement *data = [arrBlockedCustomerData objectAtIndex:indexPath.row];
    cell.lblCode.text = [ NSString stringWithFormat:@"%@",data.Customer_ID];
    cell.lblName.text = data.Customer_Name;
    cell.lblContact.text = data.Contact;
    cell.lblPhone.text = data.Phone;
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
