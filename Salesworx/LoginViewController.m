//
//  LoginViewController.m
//  Salesworx
//
//  Created by Saad Ansari on 12/8/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "LoginViewController.h"
#import "SWFoundation.h"
#import "SWPlatform.h"
#import "NewActivationViewController.h"
#import "SWSessionConstants.h"
#import "UIDevice+IdentifierAddition.h"
#import "SSKeychain.h"
#import "SWDefaults.h"
#import "AppControl.h"
#import "SWDashboardViewController.h"
#import "MedRepMenuViewController.h"
#import "SWAppDelegate.h"
#import "SWDashboardViewController.h"
#import "SalesWorxActivateNewUserViewController.h"
#import "SalesWorxDashboardViewController.h"
#import "AGPushNoteView.h"
#import "NSData+GZIP.h"
#import "SalesWorxDashboardViewController.h"
#import "BrandAmbassadorDashboardViewController.h"
#import "UIDeviceHardware.h"
#import "SalesWorxNotificationManager.h"

@interface LoginViewController ()

@end


#define kLoginField 1
#define kPasswordField 2

@implementation LoginViewController
@synthesize txtPassword,txtUserName,btnLogin;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
       
        
        
//        AppControl *appCtrl=[AppControl retrieveSingleton];
//        if ([appCtrl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
//            
////            [self setTitle:@"Saleswars"];
//            [self setTitle:@"MedRep"];
//            
//        }
//        else
//        {
//            [self setTitle:@"SalesWorx"];
//            
//        }
//        [self setTitle:@"SalesWorx"];

        
        //[self setTitle:@"SalesWorx"];
        //[SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Login", nil)];
        // Custom initialization
    }
    return self;
}
- (IBAction)sendLogsTapped:(id)sender {
    
//    NSMutableDictionary * testDict=[[NSMutableDictionary alloc]init];
//    NSMutableDictionary * testContentDict=[[NSMutableDictionary alloc]init];
//    [testContentDict setValue:@"Test Notification" forKey:@"alert"];
//    [testContentDict setValue:@"1" forKey:@"badge"];
//    [testContentDict setValue:@"default" forKey:@"sound"];
//    
//    //updating od structure
//    [testDict setValue:@"http://bit.ly/1TaGj22 Test notification content" forKey:@"od"];
//    [testDict setValue:@"http://ucstestapps.ddns.net:10008/SWX/Sync/Media/0F2D6015-58FD-4F83-858D-9202FD22D532?FileType=M Test notification content" forKey:@"od"];
//    
//    
//    [testDict setValue:testContentDict forKey:@"aps"];
//
//    NSLog(@"test content dict is %@", testDict);
//    
//    
//    
//    
//
//    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//    localNotification.alertBody = @"This is local notification!";
//    localNotification.userInfo=testDict;
//    localNotification.timeZone = [NSTimeZone systemTimeZone];
//    localNotification.soundName = UILocalNotificationDefaultSoundName;
//    localNotification.applicationIconBadgeNumber = 1;
//    
//    
//    
//    NSDate *now = [NSDate date];
//    
//    for( int i = 1; i <= 10;i++)
//    {
//        localNotification.fireDate = [NSDate dateWithTimeInterval:i sinceDate:now];
//        [[UIApplication sharedApplication] scheduleLocalNotification: localNotification];
//    }
    
    [self sendEmail];
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SalesWorxNotificationManager registerForLocalNotifications];
   // [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(startVisitWithBeaconDetails:) name:@"StartVisitWithBeacon" object:nil];


//    NSMutableArray * testCrash=[[NSMutableArray alloc]init];
//    NSLog(@"crash here %@",[testCrash objectAtIndex:3]);
    
    
    
     appdel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];

    oldFrame=self.view.frame;
    versionViewFrame=self.versionView.frame;
    
    
    NSLog(@"old frame is %@", NSStringFromCGRect(oldFrame));
    

    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    
    
//    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    

    
    
    
//    UIBarButtonItem* activateNewuserButton=[[UIBarButtonItem alloc] initWithTitle:@"Activate New User" style:UIBarButtonItemStylePlain target:self action:@selector(showActivateNewUserView)];
//    
//    [activateNewuserButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
//
//
//    
//    [self.navigationItem setRightBarButtonItem: activateNewuserButton];
    
    
    
    /*
    txtUserName.font = LightFontOfSize(14);
    txtPassword.font = LightFontOfSize(14);
    lblLogin.font = BoldSemiFontOfSize(20);
    lblPowered.font = LightFontOfSize(14);
    lblVersion.font=LightFontOfSize(14);*/
    
    
    
    //lblLogin.text = NSLocalizedString(@"Login", nil);
    
    

    
    
    lblVersion.text=[NSString stringWithFormat:@"Version  %@",[[[SWDefaults alloc]init] getAppVersionWithBuild]];
    
    lblActivatedUserName.text = [SWDefaults usernameForActivate];
    txtPassword.keyboardType=UIKeyboardTypeASCIICapable;
   // btnLogin.titleLabel.font = BoldSemiFontOfSize(24);
    // Do any additional setup after loading the view from its nib.
}

-(void)sendEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        
        [mailController setMessageBody:@"Hi,\n Please find the attached device logs for SalesWorx." isHTML:NO];
        
        if ([appControl.SET_DEFAULT_EMAIL_FOR_LOG isEqualToString:KAppControlsYESCode]) {
            
            NSString *clientEmailID = appControl.DEFAULT_EMAILID_FOR_LOG;
            
            NSMutableArray *arrControlValue = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT Control_Value FROM 'TBL_App_Control' where Control_Key = 'CLIENT_CODE'"];
            NSMutableArray *arrUserName = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT Username FROM 'TBL_User'"];
            
            NSString *subject = @"SalesWorx Device Logs";
            if (arrUserName.count > 0) {
                subject = [subject stringByAppendingString:[NSString stringWithFormat:@" - %@",[[arrUserName objectAtIndex:0]valueForKey:@"Username"]]];
            }
            if (arrControlValue.count > 0) {
                subject = [subject stringByAppendingString:[NSString stringWithFormat:@" - %@",[[arrControlValue objectAtIndex:0]valueForKey:@"Control_Value"]]];
            }
            
            [mailController setToRecipients:@[@"ucssupport@ucssolutions.com"]];
            [mailController setCcRecipients:@[clientEmailID]];
            [mailController setSubject:subject];
        } else {
            [mailController setToRecipients:@[@"ucssupport@ucssolutions.com"]];
            [mailController setSubject:@"SalesWorx Device Logs"];
        }
        
        
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        
        NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
        NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)[components month]];
        
        NSString *fileNameString;
        fileNameString=[[MedRepDefaults fetchDocumentsDirectory] stringByAppendingPathComponent:documentPathComponent];
        NSError * error;
        NSMutableArray * totalFilesArray =  [[[NSFileManager defaultManager]
                                              contentsOfDirectoryAtPath:[SWDefaults fetchDeviceLogsFolderPath] error:&error] mutableCopy];
        
        for (NSInteger i=0; i<totalFilesArray.count; i++) {
            NSData *noteData = [NSData dataWithContentsOfFile:[[SWDefaults fetchDeviceLogsFolderPath] stringByAppendingPathComponent: [totalFilesArray objectAtIndex:i]]];
            
            NSData *compressedData = [noteData gzipDeflate];
            
            NSString *fileName = [[totalFilesArray objectAtIndex:i] stringByReplacingOccurrencesOfString:@".txt" withString:@".zip"];
            
            [mailController addAttachmentData:compressedData mimeType:@"application/zip" fileName:fileName];
        }
        
        /*
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        NSString * documentDir = [filePath path] ;
        NSData *sqliteData = [NSData dataWithContentsOfFile:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
        [mailController addAttachmentData:sqliteData mimeType:@"text/plain" fileName:@"swx.sqlite"];*/
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
    }
}


/*-(void)sendEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        
        
        [mailController setSubject:@"SalesWorx Device Logs"];
        [mailController setMessageBody:@"Hi,\n Please find the attached device logs for SalesWorx." isHTML:NO];

        [mailController setToRecipients:@[@"ucssupport@ucssolutions.com"]];
        [mailController setCcRecipients:@[@"sahamed@ucssolutions.com"]];
        
        
        
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        
        NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
        NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)[components month]];
        
        
        
        NSString *fileNameString;
        fileNameString=[[MedRepDefaults fetchDocumentsDirectory] stringByAppendingPathComponent:documentPathComponent];
        
        NSError * error;

        NSMutableArray * totalFilesArray =  [[[NSFileManager defaultManager]
                                              contentsOfDirectoryAtPath:[SWDefaults fetchDeviceLogsFolderPath] error:&error] mutableCopy];
        
        for (NSInteger i=0; i<totalFilesArray.count; i++) {
            NSData *noteData = [NSData dataWithContentsOfFile:[[SWDefaults fetchDeviceLogsFolderPath] stringByAppendingPathComponent: [totalFilesArray objectAtIndex:i]]];
            
            [mailController addAttachmentData:noteData mimeType:@"text/plain" fileName:[totalFilesArray objectAtIndex:i]];
        }
        
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        
    }
    
    
}*/

#pragma mark Mail Composer Delegate


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Log email sent.");
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    CLS_LOG(@"%@", [[NSString alloc] initWithFormat:@"Customer_ID:%@ --  Username:%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"],[[SWDefaults userProfile] valueForKey:@"Username"]]);

    
    [SWDefaults UpdateGoogleAnalyticsforScreenName:kLoginScreenName];

    
    UITapGestureRecognizer*  deviceLogsTapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sendEmail)];
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"SalesWorx"];
    
   // [logoImageView addGestureRecognizer:deviceLogsTapGesture];
    
    txtUserName.autocorrectionType = UITextAutocorrectionTypeNo;

    txtPassword.autocorrectionType = UITextAutocorrectionTypeNo;

    
//    if ([SWDefaults username].length > 0)
//    {
//        login = [SWDefaults username];
//        password = [SWDefaults password];
//    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    NSString *customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    if ([customerID isEqualToString:@"C16K82563"])
    {
        txtUserName.text = [SWDefaults username];
        txtPassword.text = [SWDefaults password];
        login = [SWDefaults username];
        password = [SWDefaults password];
    } else
    {
        UIBarButtonItem* activateNewuserButton=[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Activate New User", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showActivateNewUserView)];
        
        [activateNewuserButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        [self.navigationItem setRightBarButtonItem: activateNewuserButton];
        txtPassword.text=@"";
        password=@"";
    }
    
   NSMutableArray *usernamearray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT Username FROM TBL_User"];
    
    if (usernamearray.count > 0) {
        txtUserName.text = [[usernamearray objectAtIndex:0] valueForKey:@"Username"];
        login = [[usernamearray objectAtIndex:0] valueForKey:@"Username"];
    }

    [txtUserName setIsReadOnly:YES];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//    
//    [UIViewController alloc]
//
    
    
    
//    NSString * test=[SSKeychain passwordForService:@"registereduser" account:@"user"];
//    
//    NSLog(@"key chain result is %@", test);
//    
//    if ([test isEqualToString:@"registereduser"])
//    {
//        
//        
//        //TO BE DONE TEXTFIELD NOT BEING CLEARED FOR TESTING PURPOSE
//       
//        
//        
//      /*  txtUserName.text=@"";
//        txtPassword.text= @"";
//        btnLogin.enabled=NO;
//        
//        login=@"";
//        password=@"";*/
//        
//        
//        //Mahmoud
//        login = @"Rashid";
//        password = @"password";
//       
//        
//        
//        btnLogin.enabled=YES;
//        
//    }
//    
//    else{
//        
//   
//    txtUserName.text = login;
//    txtPassword.text = password;
//    }
//    if (login.length > 0 && password.length > 0)
//    {
//        btnLogin.enabled=YES;
//    }
    isApplicationHaveTheValidMenuOptions=YES;
    appControl=[AppControl retrieveSingleton];
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]] trimString];

        if(PDARIGHTS.length==0)
        {
            isApplicationHaveTheValidMenuOptions=NO;
        }
        
    }
    if ( [SWDefaults isVersionUpdateFullSyncRequired])
    {
        appdel.isFullSyncMandatory=YES;
        
    }
    else
    {
        appdel.isFullSyncMandatory=NO;
    }
    appdel.selectedMenuIndexpath=nil;
    
    /** disabling the background sync on log out*/
    [[NSNotificationCenter defaultCenter] postNotificationName:KDISABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:KDISABLE_COMM_BS_NOTIFICATIONNameStr object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kDISABLE_PRODSTK_NOTIFICATIONNameStr object:nil];
    
    UIDeviceHardware *hardware=[[UIDeviceHardware alloc] init];
    if (![[hardware platformString] isEqualToString:@"Simulator"]) {
        [self postDeviceIDDataToGoogleAnalytics];
    }
}


-(void)viewDidAppear:(BOOL)animated{
    
    /*
    pre-filled, just for testing purpose 
    txtPassword.text = @"Alaa@2019";
    password =  txtPassword.text;
    */
    
    /*
   //  pre-filled, just for testing purpose 
   #if DEBUG
    if ([login isEqualToString:@"rashid" ]){
      password=@"password";
    }
    else if ([login isEqualToString:@"demo" ]){
        password=@"demo123";
    }
    
    [self buttonAction:self];
 
   #endif
     */
     
     
    
    
    [self compressProductImageThumbnails];
    
    NSLog(@"did appear called in login");
    
    NSString * beaconFlag = [[AppControl retrieveSingleton] VALIDATE_CUST_IBEACON];
    
    if ([beaconFlag isEqualToString:KAppControlsYESCode]) {
        [[SalesWorxBluetoothManager retrieveSingleton]initializeBluetoothManager];

    }


}

-(void)startVisitWithBeaconDetails:(NSNotification *)notificationObj
{
    
    isComingFromBeacon=YES;
    
    Singleton * currentSingleton=[Singleton retrieveSingleton];
    currentSingleton.startBeaconVisit=YES;
    
    SalesWorxBeacon * salesWorxBeacon =[[SalesWorxBeacon alloc]init];
    salesWorxBeacon=notificationObj.object;
    salesWorxBeacon.isVisitStarted=YES;
    salesWorxBeacon.visitStartTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    appdel.selectedMenuIndexpath=[NSIndexPath indexPathForRow:2 inSection:0];
    
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"matchedBeaconsArray"];
    NSMutableArray * defaultsArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    NSInteger matchedIndex;
    NSPredicate *matchedObjectPredicate=[NSPredicate predicateWithFormat:@"SELF.majorID ==%@",salesWorxBeacon.Beacon_Major];
    
    matchedIndex=[defaultsArray indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [matchedObjectPredicate evaluateWithObject:obj];
        
    }];
    
    [defaultsArray replaceObjectAtIndex:matchedIndex withObject:salesWorxBeacon];
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *defaultsData = [NSKeyedArchiver archivedDataWithRootObject:defaultsArray];
    [currentDefaults setObject:defaultsData forKey:@"matchedBeaconsArray"];

    
    //get customer id for matched beacon from db
    
    
    
    NSMutableArray * dbCustomerData =[[SWDatabaseManager retrieveManager]fetchCustomerIDwithBeacon:salesWorxBeacon];
    
    self.matchedCustomerIDFromBeacon=[SWDefaults getValidStringValue:[dbCustomerData valueForKey:@"Customer_ID"]];
    
    

    NSMutableArray * receivedBeaconDetails = notificationObj.object;
    NSLog(@"received beacon details in login %@", receivedBeaconDetails);
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"BeaconDetailsReceived"];
    [SWDefaults showAlertAfterHidingKeyBoard:[NSString stringWithFormat:@"Welcome to %@",[SWDefaults getValidStringValue:[dbCustomerData valueForKey:@"Customer_Name"]]] andMessage:@"Please login to start a visit" withController:self];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];

}

-(void)navigateToCustomersScreenonLogin
{
    
}

-(void)compressProductImageThumbnails
{
    //compress the uncompressed images

    NSArray * unCompressedImagesArray=[[NSArray alloc]init];
    NSArray *thumbnailFilesArray=[[NSMutableArray alloc] init];


    //fetched images list form db
    NSArray * downloadedImagesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select Filename  from TBL_Media_Files where Media_Type ='Image' and Download_Flag ='N' and Filename not null"];


    if(downloadedImagesArray.count>0)
    {
    
        if ([[NSFileManager defaultManager] fileExistsAtPath:[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName]]) {
            
            thumbnailFilesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kThumbnailsFolderName]  error:nil];
            
        }


        NSMutableSet *downloadedImagesSet = [[NSMutableSet alloc] init];
        [downloadedImagesSet addObjectsFromArray:[downloadedImagesArray valueForKey:@"Filename"]];

        NSMutableSet *thumbnailImagesSet = [[NSMutableSet alloc] init];

        if (thumbnailFilesArray.count>0) {
            [thumbnailImagesSet addObjectsFromArray:thumbnailFilesArray];
            
            [downloadedImagesSet minusSet:thumbnailImagesSet];
            
            unCompressedImagesArray = [downloadedImagesSet allObjects];
            
        }
        else{
            unCompressedImagesArray=[downloadedImagesArray valueForKey:@"Filename"];
            
        }
        
        if (unCompressedImagesArray.count>0) {
            [SWDefaults compressAndMoveImagestoThumbnailDirectory:[unCompressedImagesArray mutableCopy]];
        }
    }
}
- (void)showActivateNewUserView {
    [self.view endEditing:YES];
    NSLog(@"check navigation controller %@", self.navigationController);
    
    BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];

    if (checkInternetConnectivity) {
    
    if (self.navigationController.viewControllers.count == 1) {
        NSLog(@"login is RootViewController");
    }
    
    SalesWorxActivateNewUserViewController *newActivationViewController=[[SalesWorxActivateNewUserViewController alloc]init];
    newActivationViewController.isActivationNewUserFromLogin=YES;
    newActivationViewController.isFromLogin=YES;
    [self.navigationController pushViewController:newActivationViewController animated:YES];
 
    
//    NewActivationViewController *newActivationViewController = [[NewActivationViewController alloc] init] ;
//    [self.navigationController pushViewController:newActivationViewController animated:YES];
    }
    
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Internet Connection" andMessage:@"Please connect to internet and try again" withController:self];
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtPassword) {
        [textField resignFirstResponder];

        return YES;
        
    }
    
    
    if (textField==txtUserName) {
        [textField resignFirstResponder];
        [txtPassword becomeFirstResponder];
        
    }
//    [textField resignFirstResponder];

    return YES;
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField == txtUserName) {
        login=textField.text;
    } else {
        password=textField.text;
    }
    
    if (login.length > 0 && password.length > 0)
    {
        btnLogin.enabled=YES;
    }
    
    if (login.length == 0) {
        txtPassword.text = @"";
        password = @"";
    }
    
    return YES;
}


- (void)keyboardWillShow:(NSNotification *)notification

{
    if ([txtPassword isFirstResponder]) {
        
     //   loginViewTopConstraint.constant=-240;
        
     //   [self.view layoutIfNeeded];
    }
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    
  //  loginViewTopConstraint.constant=240;
  //  [self.view layoutIfNeeded];
    
}


-(IBAction)buttonAction:(id)sender
{

    NSDictionary * notificationsDict = [MedRepQueries fetchTasksDueForTodayForNotifications];
    NSLog(@"Task notificatison dict is %@",notificationsDict);
    NSString* titleString = [SWDefaults getValidStringValue:[notificationsDict valueForKey:@"Title"]];
    if (![NSString isEmpty:titleString])
    {
        [SalesWorxNotificationManager createNotificationRequest:notificationsDict];
    }

    
    [txtPassword resignFirstResponder];
    [txtUserName resignFirstResponder];
    NSCharacterSet *charc=[NSCharacterSet characterSetWithCharactersInString:@" "];
    login= [login stringByTrimmingCharactersInSet:charc];
    #if DEBUG
//   login=@"rashid";
//  password=@"password";
   #endif
    if (login.length > 0 && password.length > 0) {
        // login
        NSString *temp  = [[SWDatabaseManager retrieveManager] verifyLogin:login andPassword:password];
        
        if ([temp isEqualToString:@"Done"])
        {
            
            
            
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"welcomeDisplayed"];
            
            NSArray* arr=[[NSArray alloc]init];
            
            arr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat: @"select * from TBL_User"]];
            
            [SWDefaults setUsername:login];
            NSLog(@"logging in with user name %@", login);

            [SWDefaults setUsernameForActivate:login];
            
//            SalesWorxGoogleAnalytics * gAnalytics=[[SalesWorxGoogleAnalytics alloc]init];
//            gAnalytics.userID
//            [SWDefaults updateGoogleAnalyticsCustomEvents:<#(SalesWorxGoogleAnalytics *)#>]
            
            [SWDefaults setPassword:password];
            [SWDefaults setPasswordForActivate:password];
            
            SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
            
                      
            
            BOOL licenseValid=[SWDefaults validateSalesWorxLicense:[[SWDefaults licenseKey] valueForKey:@"ll"]];
            
            if (licenseValid==YES)
   
            {
                
                
                
                 UINavigationController *menuNavigationController;
                SalesWorxFieldSalesDashboardViewController*     dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
                appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];

                if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
                {
                    NSString *PDARIGHTS=[[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]] trimString];
                    
                    if (isComingFromBeacon&&
                        [PDARIGHTS containsString:KFieldSalesSectionPDA_RightsCode]&&
                        [PDARIGHTS containsString:KFieldSalesCustomersPDA_RightCode]) {
                        [self MoveToCustomerScreenWithDetectedBeaconCustomerDetails];
                    }
                    else if([PDARIGHTS containsString:KFieldSalesSectionPDA_RightsCode])
                    {
                        SalesWorxFieldSalesDashboardViewController*     dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
                        appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
                    }
                    else if([PDARIGHTS containsString:KFieldMarketingSectionPDA_RightsCode] ||
                            [PDARIGHTS containsString:KFieldMarketingVisitsPDA_RightsCode])
                    {
                        SalesWorxDashboardViewController *dashboardViewController = [[SalesWorxDashboardViewController alloc] init];
                        appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
                    }
                    else if([PDARIGHTS containsString:KBrandAmbassadorVisitsPDA_RightsCode])
                    {
                        BrandAmbassadorDashboardViewController *dashboardViewController = [[BrandAmbassadorDashboardViewController alloc] init];
                        appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
                    }
                    
                    
                }
                else if (isComingFromBeacon) {
                    [self MoveToCustomerScreenWithDetectedBeaconCustomerDetails];
                }
                
                

                
                
                
                
                MedRepMenuViewController *medrepMenuViewController;
                medrepMenuViewController = [[MedRepMenuViewController alloc] init] ;
                menuNavigationController = [[UINavigationController alloc] initWithRootViewController:medrepMenuViewController] ;
                
                SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:appdel.mainNavigationController rearViewController:menuNavigationController] ;
                SWSessionManager * sessionManager=[[SWSessionManager alloc] initWithWindow:appdel.window andApplicationViewController:splitViewController];
                [[UINavigationBar appearance]setBarTintColor:kNavigationBarBackgroundColor];
                [[UINavigationBar appearance] setTranslucent:NO];
                [[UINavigationBar appearance]setTintColor:kNavigationBarTitleFontColor];
                [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: kNavigationBarTitleFontColor}];

                medrepMenuViewController=nil;
                splitViewController=nil;
                dashboardViewController=nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:kSessionLoginDoneNotification object:nil];
                [self ValidateFullSyncOnLoginAndShowSyncPopOverOnView:appdel.mainNavigationController.topViewController];
                
                if([appControl.USER_ENABLED_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode])
                {
                    // Post a notification to enableBackGroundSync
                    [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
                }
                if([appControl.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_COMM_BS_NOTIFICATIONNameStr object:nil];
                }
                if ([appControl.ENABLE_FS_STOCK_SYNC isEqualToString:KAppControlsYESCode] ) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kENABLE_PRODSTK_NOTIFICATIONNameStr object:nil];
                    
                }

                [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_ROUTE_BS_NOTIFICATIONNameStr object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_ROUTE_CANCEL_BS_NOTIFICATIONNameStr object:nil];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KLIcenseValidationErrorAlerttitleStr andMessage:KLIcenseValidationErrorAlertMessageStr withController:self];
            }
            
        }
        else
        {
            txtPassword.text = @"";
            password = @"";
            [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Please enter a valid password and try again" withController:self];
            
        }
    }
    
    else
    {
        
        NSString* errorMessageString;
        
        if (txtUserName.text.length==0) {
            
            errorMessageString=@"Please enter username and try again";
        }
        
         if (txtPassword.text.length==0)
        {
            errorMessageString=@"Please enter password and try again";

        }
        
        
        if (txtUserName.text.length==0 && txtUserName.text.length==0) {
            
            errorMessageString=@"Please enter username and password and try again";
        }
    
        [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:errorMessageString withController:self];
        
    }

}
-(void)ValidateFullSyncOnLoginAndShowSyncPopOverOnView:(id)viewcontroller
{
    if (appdel.isFullSyncMandatory) {
        [(ZUUIRevealController*)((UIViewController *)viewcontroller).navigationController.parentViewController revealToggle:nil];
        [(MedRepMenuViewController*)[(UINavigationController*)[(ZUUIRevealController*)((UIViewController *)viewcontroller).navigationController.parentViewController rearViewController]topViewController] displaySyncPopover];
    }
    else if ([SWDefaults isDailyFullSyncRequired])
    {
        
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [(ZUUIRevealController*)((UIViewController *)viewcontroller).navigationController.parentViewController revealToggle:nil];
                                        [(MedRepMenuViewController*)[(UINavigationController*)[(ZUUIRevealController*)((UIViewController *)viewcontroller).navigationController.parentViewController rearViewController]topViewController] displaySyncPopover];
                                    }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
        
        
        NSString * firstString=NSLocalizedString(@"Your last full sync with server was on", nil);
        
        NSString* secondString=NSLocalizedString(@"Would you like to do a full sync now", nil);
        
        NSString* finalString=[NSString stringWithFormat:@"%@\n%@\n%@",firstString,[SWDefaults lastFullSyncDate],secondString];
        

        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Daily Sync", nil) andMessage:finalString andActions:actionsArray withController:viewcontroller];
    }

}
-(void)MoveToCustomerScreenWithDetectedBeaconCustomerDetails
{
    //check app control flags if customers screen access exists
    SWCustomersViewController *customersVC = [[SWCustomersViewController alloc] init];
    customersVC.isFromBeacon=YES;
    NSLog(@"customer id being passed to customers %@",self.matchedCustomerIDFromBeacon );
    
    customersVC.matchedCustomerIdFromBeacon=[NSString stringWithFormat:@"%@",self.matchedCustomerIDFromBeacon];
    appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:customersVC];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1000) {
        
    }
    else
    {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSessionActivationErrorNotification object:nil];
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Text Field Delegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (textField==txtUserName) {
//        
//        [txtUserName resignFirstResponder];
//        [txtPassword becomeFirstResponder];
//    }
   [self animateTextField: textField up: NO];

}




- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    
    if (textField.text.length >= UsernameTextFieldLimit && range.length == 0)
        
        
    {
        
        return NO;
    }
    else
    {
        return YES;
    }

    
    
}


#pragma mark UIKeyboard Notifications



//- (void)keyboardWillHide:(NSNotification *)notification {
//    
//   // [self.versionView setFrame:versionViewFrame];
//    
//    self.customLoginViewTopConstraint.constant=181;
//
//    
//    [loginCustomView setNeedsUpdateConstraints];
//    
//    [UIView animateWithDuration:0.25f animations:^{
//        [loginCustomView layoutIfNeeded];
//    }];
//    
//    
//
//   
//}
//
//- (void)keyboardWillShow:(NSNotification *)notification {
//    
//    
//    
//    self.customLoginViewTopConstraint.constant=80;
//    
//    
//    [loginCustomView setNeedsUpdateConstraints];
//    
//    [UIView animateWithDuration:0.25f animations:^{
//        [loginCustomView layoutIfNeeded];
//    }];
//
//}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self animateTextField: textField up: YES];
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    NSInteger movementDistance=70;
    if(textField==txtPassword)
    {
        movementDistance=140;
    }
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    loginCustomView.frame = CGRectOffset(loginCustomView.frame, 0, movement);
    [UIView commitAnimations];
}
-(void)postDeviceIDDataToGoogleAnalytics
{
    
    NSString *GAIDeviceIDStr=[NSString stringWithFormat:@"%@?%@?%@",[SWDefaults licenseIDForInfo],[SWDefaults getValidStringValue:[[SWDefaults licenseKey] stringForKey:@"sid"]],[SWDefaults getValidStringValue:[[DataSyncManager sharedManager]getDeviceID]]];
    
    NSMutableDictionary * googleAnalyticsDict=[[NSMutableDictionary alloc]init];
    NSData *SelectedSeverData = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
    [googleAnalyticsDict setValue:[NSString stringWithFormat:@"%@",[SelectedServersDetailsjson valueForKey:@"name"]] forKey:@"Server_Name"];
    [googleAnalyticsDict setValue:GAIDeviceIDStr forKey:@"Device_ID"];
    
    NSLog(@"googleAnalyticsDict %@",googleAnalyticsDict);
    NSLog(@"SelectedServersDetailsjson %@",SelectedServersDetailsjson);

    //add an event to track device ID
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Device_ID_Check"     // Event category (required)
                                                          action:[SWDefaults getValidStringValue:[googleAnalyticsDict valueForKey:@"Server_Name"]]  // Event action
                                                           label:[SWDefaults getValidStringValue:[googleAnalyticsDict valueForKey:@"Device_ID"]]          // Event label
                                                           value:nil] build]];    // Event value
}
@end
