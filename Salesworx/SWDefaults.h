//
//  SWDefaults.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/9/12.
//  Copyright (c) 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedRepCustomClass.h"
#import "SalesWorxCustomClass.h"
#import "UIWindow+SalesWorxWindow.h"
#import "MedRepDefaults.h"
#import "KSToastView.h"


#define kMerchandisingTitle @"Merchandising"
#define kMerchandisingCustomerLevelCode @"MERCH"
#define kMerchandisingDisplayLevelCode @"MERCH"
#define kMerchandisingPDA_RightsCode @"MERCH"

#pragma mark AfterMultiUOM
#define KDecimalQuantityRegExpression [NSString stringWithFormat:@"^[0-9]*((\\.|,)[0-9]{0,%0.0f})?$",2.0]
#define KWholeNumberQuantityRegExpression @"^[0-9]*$"
#define  kGoogleAPIKey @"AIzaSyAOpIPxhcIJ4PCHkIAl5ed_eP2cfu5HYsg"
#define kGoogleDistanceAPIURL @"https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=%f,%f&destinations=%f,%f&key=%@"


#define KBackButtonArrowImageName @"back_btn.png"
#define KBackButtonArabicArrowImageName @"back_btn_ar.png"
#define kAllControlStates (UIControlStateNormal|UIControlStateHighlighted| UIControlStateDisabled|UIControlStateSelected)

#define KUSLOCALE [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]

#define KUNSupportMailComposer @"Your device doesn't support the mail composer sheet. Please configure mail box in your device"
#define KFailureAlertTitle  @"Failure"

#define KNewActivationNotificationNameStr @"NewActivationStatusNotification"
#define KSendOrdersNotificationNameStr @"SendOrdersStatusNotification"
#define kNewActivationNotification @"NewActivation"
#define kActivateUrl @"/Activate"
#define KAllFilesUploadedStr @"All Files uploaded successfully"
#define KNoFilesToUploadedStr @"No files to upload"

#define KMissingData @"Missing Data"
#define KInvalidData @"Invalid Data"

#define kActivateNewUserTitle @"New Activation"


#define kUploadDatabaseExistingUserNotificationName @"UploadExistingUserDatabaseNotification"

#define kDatabaseFileName @"swx.sqlite"
#define kDatabaseZipFileName @"swx.sqlite.gz"
#define kFilterTitle @"Filter"


#define KNewSyncNotificationNameStr @"NewSyncStatusNotification"
#define KSendOrdersNotificationNameStr @"SendOrdersStatusNotification"
#define KMissingData @"Missing Data"
#define KNoMatchesStr @"No Matches"
#define kNoData @"No Data"
#define kUsernameKey    @"login"
#define kPasswordKey    @"password"
#define kLocationFilterCustomerList @"location_filter_customerlist"
#define kFilterCustomerList @"filter_customerlist"
#define kLastCollectionRef  @"LastCollectionReference"
#define kPaymentFilterForCustomerList @"Payment_filter_customerlist"
#define kStatusFilterForCustomerList @"Status_filter_customerlist"
#define kNameFilterForCustomerList @"Name_filter_customerlist"
#define kCodeFilterForCustomerList @"Code_filter_customerlist"
#define kBarCodeFilterForCustomerList @"BarCode_filter_customerlist"
#define kProductBrandFilter     @"ProductBrandFilter"
#define kProductNameFilter     @"ProductNameFilter"
#define kProductProductIDFilter     @"ProductProductIDFilter"
#define kUserProfile    @"UserProfile"
#define kImagesFolder @"Signature"
#define kLicenseActivationURL @"http://www.ucssolutions.com/licman/gen-lic.jsp"
#define kLicenseActivationValidationURL @"http://www.ucssolutions.com/licman/url.jsp"
#define kLicenseActivationDateKey @"UCS_Salesworx_ActivationDate"
#define kLicenseActivationAccount @"user"

#define kTextFieldCharacterLimit @"50"
#define kLicenseActivationStatus @"SalesWorxLicenseActivationStatus"

#define kOnsiteVisitTitle @"OnSite"
#define kTelephonicVisitTitle @"Via Telephone"


#define kOnsiteVisitDatabaseTitle @"ONSITE"
#define kTelephonicVisitDatabaseTitle @"TELEPHONIC"

#define kFOCOrderNotificationName @"FOCOrderDidSelectNotification"
#define kTemplateOrder @"Template Order"



#define kNavigationBarBackgroundColor [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]
#define kNavigationBarTitleFontColor [UIColor whiteColor]

#define  kDarkTitleColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]
#define headerFont kSWX_FONT_REGULAR(16)

#define kVisitOptionsDisbaleBorderColor [UIColor colorWithRed:(170.0/255.0) green:(170.0/255.0) blue:(170.0/255.0) alpha:1]

#define kVisitOptionsBorderColor [UIColor colorWithRed:(64.0/255.0) green:(129.0/255.0) blue:(189.0/255.0) alpha:1]

#define kUITableViewHeaderBackgroundColor [UIColor colorWithRed:(248.0/255.0) green:(250.0/255.0) blue:(252.0/255.0) alpha:1]

#define kUITableViewCellHeight 44

#define kBlockedCustomerBackgroundColor [UIColor redColor]

#define kActiveCustomerBackgroundColor [UIColor whiteColor]

#define kUITableViewSaperatorColor [UIColor colorWithRed:(196.0/255.0) green:(204.0/255.0) blue:(210.0/255.0) alpha:1]


#define kUITableViewBorderColor [UIColor colorWithRed:(182.0/255.0) green:(200.0/255.0) blue:(209.0/255.0) alpha:1]

#define kUIElementDividerBackgroundColor  [UIColor colorWithRed:(231.0/255.0) green:(235.0/255.0) blue:(237.0/255.0) alpha:1]

#define bodyFont kSWX_FONT_REGULAR(12)

#define bodySemiBold kSWX_FONT_SEMI_BOLD(14)



#define NavigationBarButtonItemFont kSWX_FONT_SEMI_BOLD(14)


#define textFont kSWX_FONT_REGULAR(14)



#define UITableViewCellFont kSWX_FONT_REGULAR(19)

#define UITableViewCellDescFont kSWX_FONT_SEMI_BOLD(14)


#define UITableviewSelectedCellBackgroundColor [UIColor colorWithRed:(81.0/255.0) green:(102.0/255.0) blue:(136.0/255.0) alpha:1]


#define UITableViewTitleDescriptionLabelFontColor [UIColor colorWithRed:(89.0/255.0) green:(114.0/255.0) blue:(153.0/255.0) alpha:1]

#define UITableViewTitleLabelFontColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]


#define fontColor [UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1]



#define UIViewBackGroundColor [UIColor colorWithRed:(240.0/255.0) green:(240.0/255.0) blue:(240.0/255.0) alpha:1]



#define  SalesWorxTitleFont kSWX_FONT_REGULAR(19)

#define SalesWorxTableViewCellTitleColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]

#define SalesWorxReadOnlyViewColor [UIColor colorWithRed:(248.0/255.0) green:(249.0/255.0) blue:(250.0/255.0) alpha:0.7]

#define SalesWorxReadOnlyTextFieldColor [UIColor colorWithRed:(248.0/255.0) green:(249.0/255.0) blue:(250.0/255.0) alpha:1.0]
#define SalesWorxReadOnlyTextFieldTextColor [UIColor colorWithRed:(105.0/255.0) green:(114.0/255.0) blue:(128.0/255.0) alpha:1.0]
#define SalesWorxReadOnlyTextViewColor [UIColor colorWithRed:(248.0/255.0) green:(249.0/255.0) blue:(250.0/255.0) alpha:1.0]
#define SalesWorxReadOnlyTextViewTextColor [UIColor colorWithRed:(105.0/255.0) green:(114.0/255.0) blue:(128.0/255.0) alpha:1.0]

#define KOpenSans12 kSWX_FONT_REGULAR(12)
#define KOpenSans13 kSWX_FONT_REGULAR(13)
#define KOpenSans14 kSWX_FONT_REGULAR(14)
#define KOpenSans15 kSWX_FONT_REGULAR(14)
#define KOpenSans16 kSWX_FONT_REGULAR(16)



#define KOpenSansSemiBold12 kSWX_FONT_SEMI_BOLD(12)
#define KOpenSansSemiBold13 kSWX_FONT_SEMI_BOLD(13)
#define KOpenSansSemiBold14 kSWX_FONT_SEMI_BOLD(14)
#define KOpenSansSemiBold15 kSWX_FONT_SEMI_BOLD(15)
#define KOpenSansSemiBold16 kSWX_FONT_SEMI_BOLD(16)

#define KSalesWorxTitleLabelColor [UIColor colorWithRed:(89.0/255.0) green:(114.0/255.0) blue:(153.0/255.0) alpha:1]

#define KSalesWorxFaceTimeColor [UIColor colorWithRed:(49.0/255.0) green:(156.0/255.0) blue:(212.0/255.0) alpha:1]
#define KSalesWorxWaitTimeColor [UIColor colorWithRed:(252.0/255.0) green:(166.0/255.0) blue:(88.0/255.0) alpha:1]

#define HAS_DECIMALS(x) (x != (int)x)


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define EMAIL_ACCEPTABLE_CHARECTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@-"


#define kOrderInitializationTitle @"Order Initialization"

#define kProductInfoUnavailable @"Product information not available"

#define kAvailableCode @"Available"

#define kUnAvailableCode @"Unavailable"

#define kCashTitle @"Cash"

#define kCreditTitle @"Credit"

#define kOpenTitle @"Open"

#define kBlockedTitle @"Blocked"

//SW Customers screen color codes


#define CreditLimitLableColor [UIColor colorWithRed:(19.0/255.0) green:(196.0/255.0) blue:(165.0/255.0) alpha:1.0]

//#define CustomerActiveColor  [UIColor colorWithRed:(19.0/255.0) green:(196.0/255.0) blue:(165.0/255.0) alpha:1.0]

#define CustomerActiveColor  [UIColor whiteColor]


#define kNearByCustomersTitle @"NEARBY CUSTOMERS"
#define kDistributionCheckTitle @"DISTRIBUTION CHECK"
#define kSalesOrderTitle @"SALES ORDER"
#define kManageOrdersTitle @"MANAGE ORDERS"
#define kOrderHistoryTitle @"ORDER HISTORY"
#define kReturnsTitle @"RETURNS"
#define kCollectionTitle @"COLLECTION"
#define kSurveyTitle @"SURVEY"
#define kManageOrdersScreenTitle @"Manage Orders"
#define kReturnsInitializationScreenTitle @"Returns Initialization"
#define kTodoTitle @"To-Do"
#define kReportsTitle @"REPORTS"
#define kProductMediaTitle @"PRODUCT MEDIA"
#define kProductMediaFilterTitle @"Product Media Filter"

#define kBackendMaxDate @"2099-12-30"
#define kBackendMinDate @"1980-1-1"


#define KSalesOrderCustomerLevelCode @"O"
#define KReturnsCustomerLevelCode @"R"
#define KPaymentCollectionCustomerLevelCode @"C"
#define KSurveyCustomerLevelCode @"S"
#define KDistributionCheckCustomerLevelCode @"D"
#define kTODOCustomerLevelCode @"TODO"
#define kReportsCustomerLevelCode @"RPT"



#define KSalesOrderDisplayLevelCode @"O"
#define KReturnsDisplayLevelCode @"R"
#define KPaymentCollectionDisplayLevelCode @"C"
#define KSurveyDisplayLevelCode @"S"
#define KDistributionCheckDisplayLevelCode @"D"
#define kTODODisplayLevelCode @"TODO"
#define kManageOrderDisplayLevelCode @"M"
#define kOrderHistoryDisplayLevelCode @"H"
#define kReportsDisplayLevelCode @"RPT"


#define kSalesOrderTransaction @"O"
#define kReturnTransaction @"R"



#define kDistributionCheckedAlertTitle @"Distribution Check Completed"

/* Distribution Check */
#define KDistributionCheckAvailabilityColor [UIColor colorWithRed:(17.0/255.0) green:(176.0/255.0) blue:(101.0/255.0) alpha:1]
#define KDistributionCheckUnAvailabilityColor [UIColor redColor]


/*New sales order Constants*/

#define KNOProductSelectedLabel @"No product selected"
#define KSalesOrderShortName @"SO"
#define KManageOrderShortName @"MO"
#define KTemplateOrderShartName @"TO"
#define KAppControlsYESCode @"Y"
#define KAppControlsNOCode @"N"
#define KAppControlsConfirmCode @"C"

#define KSalesOrderProductTableViewBrandCodeCellCollapsedColor [UIColor colorWithRed:(210.0/255.0) green:(210.0/255.0) blue:(210.0/255.0) alpha:1]
#define KSalesOrderProductTableViewBrandCodeCellExpandColor [UIColor colorWithRed:(182.0/255.0) green:(200.0/255.0) blue:(209.0/255.0) alpha:1]

#define KSalesOrderProductTableViewBrandCodeCellCollapsedTextColor [UIColor colorWithRed:(89.0/255.0) green:(114.0/255.0) blue:(153.0/255.0) alpha:1]
#define KSalesOrderProductTableViewBrandCodeCellExpandTextColor [UIColor whiteColor]



#define surveyCellExpandColor [UIColor colorWithRed:(202.0/255.0) green:(209.0/255.0) blue:(221.0/255.0) alpha:1]
#define surveyCellCollapseColor [UIColor colorWithRed:(247.0/255.0) green:(248.0/255.0) blue:(249.0/255.0) alpha:1]

#define surveyCellSecondLevelColor [UIColor colorWithRed:(231.0/255.0) green:(235.0/255.0) blue:(237.0/255.0) alpha:1]




#define KNOSelectionViewInSalesOrderHeight 36
#define KSalesOrderScreenProductDetailsViewHeight 90
#define KSalesOrderScreenProductOrderDetailsViewHeight 140
#define KSalesOrderScreenProductFOCOrderDetailsViewHeight 90

#define KSalesOrderScreenProductOrderDetailsViewTopMargin 8
#define KSalesOrderScreenProductDetailsViewTopMargin 8
#define KSalesOrderScreenNoSelectionsViewTopMargin 8
#define KZero 0
#define KSalesOrderNoPricesAvailableErrorMessage @"Pricing not available for selected product."

#define KReturnRestrictedErrorMessage @"Return is restricted for selected product"


#define KNotApplicable @"N/A"
#define kNewStatus @"New"
#define KSalesOrderNoProductSelectionMessage @"No product selected"
#define KUITableViewUnSelectedCellrightArrow @"ArrowRight_DarkIcon"
#define KUITableViewSelectedCellrightArrow @"ArrowRight_WhiteIcon"
#define KSalesOrderProductTableViewUpArrowImageName @"SalesOrder_UpArrorw"
#define KSalesOrderProductTableViewDownArrowImageName @"SalesOrder_DownArrorw"
#define KSalesOrderManualFocPopOverTitle @"FOC Items"
#define KVisitOptionsProductNameSearchPopOverType @"Visit options Product Names Search"
#define KVisitOptionsProductNameSearchPopOverTitle @"Search Products"
#define KSalesOrderUOMPopOverTitle @"UOM"

#define KSalesOrderProductAddButtonTitle @"Add To Order"
#define KSalesOrderProductUpdateButtonTitle @"Update"

#define KSalesOrderBonusCellBackgroundColor [UIColor purpleColor]

#define KLineItemSelectLots @"Select Lots"
#define KLineItemNotes @"Notes"
#define KLineItemShowLots @"Show Lots"

#define KSalesOrderScreenOrderQuantityTextFieldMaximumDigitsLimit 6
#define KSalesOrderScreenBonusQuantityTextFieldMaximumDigitsLimit 6
#define KSalesOrderScreenSellingPriceTextFieldMaximumDigitsLimit 9
#define KSalesOrderScreenDiscountPercantageMaximumDigitsLimit 6
#define KSalesOrderScreenFOCQuantityTextFieldMaximumDigitsLimit 6
#define KSalesOrderScreenNotesPopOverTextViewMaximumDigitsLimit 49

#define KSalesOrderScreenDocReferenceTextFieldMaximumDigitsLimit 49
#define KSalesOrderScreenSigneeNameTextFieldMaximumDigitsLimit 49
#define KSalesOrderScreenCommentsTextViewMaximumDigitsLimit 199


/** sales order initialization screen*/
#define kCategoryPopOverTitle @"Category"
#define kWarehousePopOverTitle @"Warehouse"
#define kOrderTempletePopOverTitle @"Order Template"
#define kOrderTypePopOverTitle @"Order Type"
#define kDefaultOrderPopOverTitle @"Normal Order"
#define kFOCOrderPopOverTitle @"FOC Order"
#define kReOrderPopOverTitle @"Re-order"
#define kDCReOrderPopOverTitle @"Re-order from Distribution Check"
#define kParentOrderPopOverTitle @"Parent Order"
#define kRecommendedOrderPopOverTitle @"Recommended Order"
#define kCoachSurveyPopOverTitle @"Coach Survey"


#define KTitleStrAlert @"Alert"

/** manage order screen*/

#define KConfirmOrderStatusString @"Confirmed"
#define KUnConfirmOrderStatusString @"Unconfirmed"

/** Returns Screen*/
#define kReturnsScreenExpiryDatePopOverTitle @"Expiry Date"
#define kReturnsScreenReasonForReturnPopOverTitle @"Reason for Return"
#define KReturnProductAddButtonTitle @"Add To Returns"
#define KReturnProductUpdateButtonTitle @"Update"
#define KSuccessAlertTitleStr @"Success"
#define KReturnDocCreateSuccessAlertMesssage @"RMA Document created successfully"
#define KDBErrorAlertTitleStr @"DB Error"
#define KReturnsDBErrorAlertMessageStr @"Unable to update returns"

/** Payment Collection Screen*/
#define kPaymentCollectionScreenPaymentMethodPopOverTitle @"Payment"
#define kPaymentCollectionScreenCurrencyPopOverTitle @"Currency"
#define kPaymentCollectionScreenChequeDatePopOverTitle @"Cheque Date"
#define kPaymentCollectionScreenBanksPopOverTitle @"Bank"


#define KPaymentCollectionErrorAlertTitlestr @"Invalid Data"
#define KPaymentCollectionMissingDataAlertViewTitleStr @"Missing Data"
#define KPaymentCollectionAfterSignatureSavedAlertTitleStr @"Alert"
#define KPaymentCollectionAfterSignatureSavedAlertViewMessageStr @"Changes are not allowed after the signature is saved"


#define KReturnsScreenQuantityTextFieldMaximumDigitsLimit 6
#define KReturnsOrderScreenBonusQuantityTextFieldMaximumDigitsLimit 6
#define KReturnsOrderScreenLotNumberTextFieldMaximumDigitsLimit 20

#define KReturnsScreenDocReferenceTextFieldMaximumDigitsLimit 49
#define KReturnsOrderScreenSigneeNameTextFieldMaximumDigitsLimit 49
#define KReturnsScreenCommentsTextViewMaximumDigitsLimit 199
#define KReturnsScreenInvoicesNumbersMaximumDigitsLimit 99
#define KReturnsScreenInvoiceReasonMaximumDigitsLimit 49
#define KNewRowAnimationColor [UIColor colorWithRed:(30.0/255.0) green:(216.0/255.0) blue:(255.0/255.0) alpha:1.0];

#define KSegmentedControltintColor [UIColor colorWithRed:(81.0/255.0) green:(102.0/255.0) blue:(136.0/255.0) alpha:1]


#pragma Mark CollectionScreenDefinitions
#define KEmptyString @"Empty"
#define KUnSavedString @"UnSaved"
#define  KSavedString @"Saved"


#define  KAlertOkButtonTitle @"Ok"
#define  KAlertYESButtonTitle @"Yes"
#define  KAlertNoButtonTitle @"No"
#define  KAlertCancelButtonTitle @"Cancel"

#define KVisitOptionsPendingSurveyAlertMessageStr  @"There are one or more surveys pending for this customer. Would you like to complete them now?"
#define KVisitOptionsPendingSurveyAlertTitleStr @"Survey pending"
#define KVisitOptionCloseVisitConfirmationAlertMessageStr @"Would you like to close this visit?"
#define KVisitOptionCloseVisitConfirmationAlertTitleStr @"Close visit?"


/** menu and visit options restriction codes*/
#define KFieldMarketingSectionPDA_RightsCode @"MED"
#define KFieldSalesSectionPDA_RightsCode @"FLS"
#define KCommunicationSectionPDA_RightsCode @"COM"
#define KReportsSectionPDA_RightsCode @"RPT"
#define KFieldSalesDashBoardPDA_RightCode @"SDASH"
#define KFieldSalesProductsPDA_RightCode @"SPROD"
#define KFieldSalesROUTEPDA_RightCode @"ROUTE"
#define KFieldSalesCustomersPDA_RightCode @"CUST"
#define KFieldMarketingdoctorsPDA_RightsCode @"DR"
#define KFieldMarketingProductsPDA_RightsCode @"MPROD"
#define KFieldMarketingDashBoardPDA_RightsCode @"MDASH"
#define KFieldMarketingPharmaciesPDA_RightsCode @"PH"
#define KFieldMarketingVisitsPDA_RightsCode @"VISITS"
#define KBrandAmbassadorVisitsPDA_RightsCode @"BAVISITS"
#define KBrandAmbassadorDashboardPDA_RightsCode @"BADASH"
#define KFieldMarketingCoachSurveyPDA_RightsCode @"VSURVEY"
#define KBO_URLRightsCode @"BOURL"
#define KReportsSalesSummaryPDA_RightsCode @"RSS"
#define KReportsReviewDocumentsPDA_RightsCode @"RRD"
#define KReportsBlockedCustomerPDA_RightsCode @"RBC"
#define KReportsCustomerStatementPDA_RightsCode @"RCS"
#define KReportsPaymentSummaryPDA_RightsCode @"RPS"
#define KReportsDailyVisitSummaryPDA_RightsCode @"RDVS"
#define KReportsDailyVisitSummary_FieldSales_PDA_RightsCode @"RDVS_FS"
#define KReportsDailyVisitSummary_FieldMarketing_PDA_RightsCode @"RDVS_FM"
#define KReportsEdetailingReportPDA_RightsCode @"RER"
#define KReportsTargetVsAchivementPDA_RightsCode @"CRT"



#define KSalesOrderPDA_RightsCode @"O"
#define KReturnsPDA_RightsCode @"R"
#define KPaymentCollectionPDA_RightsCode @"C"
#define KSurveyPDA_RightsCode @"S"
#define KDistributionCheckPDA_RightsCode @"D"
#define kTODORightsCode @"TODO"
#define KCoachReportPDA_RightsCode @"CSURVEY"
#define KReportsPDA_RightsCode @"RPT"


#define KCoachMarketingSurveyType @"M"
#define KCoachDefaultSurveyType @"C"
#define kCoachSurveySliderResponseType @"SLIDER"
#define kCoachSurveySegmentResponseType @"SEGMENT"
#define kStoreAuditReportSurvey @"D"
#define kQualityControlCheckListReportSurvey @"Q"



#define kDefaultDownloadMediaType @"Default"
#define kMerchandisingPOSMDownloadMediaType @"POSM_IMG"
#define kOrgLogoDownloadMediaType @"ORG_LOGO"


#define KLIcenseValidationErrorAlertMessageStr @"Your license has been expired or contact your administrator for further assistance"
#define KLIcenseValidationErrorAlerttitleStr @"License validation error"

#define KVisitOtionCompletionNotificationNameStr @"VisitOtionCompletionNotification"

#define KVisitOtionNotification_DictionaryKeyStr @"VisitOptionName"
#define KVersionUpdate_MandatoryFullSync_AlertMessageStr @"SalesWorx has been updated, please do a full sync with the server to proceed."
#define KDaily_MandatoryFullSync_AlertMessageStr @"Please do a full sync to proceed."




#define kSalesOrderDocType @"I"

#define kReturnsDocType @"R"
//there is no such thing in backedn this is just for db method purpose
#define kCollectionDocType @"C"


#define kSalesReportTitle @"SALES PER AGENCY"
#define kStockReportTitle @"STOCK REPORT BY AGENCY"
#define kSalesSummaryTitle @"SALES SUMMARY"
#define kReviewDocumentsTitle @"REVIEW DOCUMENTS"
#define kBlockedCustomersTitle @"BLOCKED CUSTOMERS"
#define kCustomerStatementTitle @"CUSTOMER STATEMENT"
#define kPaymentSummaryTitle @"PAYMENT SUMMARY"
#define kDailyVisitSummaryTitle @"DAILY VISIT SUMMARY"
#define kEDetailingReportTitle @"eDETAILING REPORT"
#define kTargetVSAchievmentTitle @"Target VS Achievement"


#define kSalesReportDescription @"View comparison of year to date and month to date sales date with last year."
#define kStockReportDescription @"View product stock by agency."
#define kSalesSummaryDescription @"View sales data by date, customer type and sales document type."
#define kReviewDocumentsDescription @"View all transactions created for all customers or filter by a single customer for selected date range."
#define kBlockedCustomersDescription @"Check details of blocked customers."
#define kCustomerStatementDescription @"Review invoices and payment details for customers."
#define kPaymentSummaryDescription @"Review payment collected by type and date range."
#define kDailyVisitSummaryDescription @"Review daily visit summary."
#define kEDetailingReportDescription @"Review products demonstrated and samples given."
#define kTargetVSAhievementDescription @"Target VS Achievement report"



#define KTableViewSectionCollapsedColor [UIColor colorWithRed:(5.0/255.0) green:(114.0/255.0) blue:(198.0/255.0) alpha:1]
#define KTableViewSectionExpandColor [UIColor colorWithRed:(54.0/255.0) green:(193.0/255.0) blue:(195.0/255.0) alpha:1]

#define KCoachHeaderColor [UIColor colorWithRed:(252.0/255.0) green:(251.0/255.0) blue:(255.0/255.0) alpha:1]
#define KCoachContentColor [UIColor colorWithRed:(245.0/255.0) green:(242.0/255.0) blue:(255.0/255.0) alpha:1]



#define KTableViewExapndedSectionArrowImageStr @"ArrowUp_WhiteIcon"
#define KTableViewCollapsedSectionArrowImageStr @"ArrowDown_WhiteIcon"


#define KWarningAlertTitleStr @"Warning"

#pragma mark activationscreen
#define KEnglishLocaleStr @"en_US"

#define kThumbnailsFolderName @"Thumbnails"

#define  KNoNetworkAlertTitle @"Internet Connection Unavailable"
#define  KNoNetworkAlertMessage @"Unable to connect to application server, please verify your network connectivity"
#define KLicenseActivationScreenURLChangeWebServiceErrorAlertMessage @"Unable to change, please verify your URL"
#define KPleaseTryAgainStr @"please try again"
#define KLicenseActivationScreenURLChangeWebServiceServerErrorAlertTitle @"URL Change Failed"
#define KApplicationInitializationErrorAlertMessage @"Unable to initialize application data, please try later"
#define KLicenseFailureErrorTitle @"License Activation Failed"
#define KApplicationDataResetAlertMessage @"All existing application data will be cleared, are you sure you want to continue?"
#define KOrderInitializationscreen_CategoryNotSelectedAlertMessage @"Please select category/warehouse and try again"

#define KOrderInitializationscreen_NoRecommendedOrderAlertMessage @"No Recommended orders available for selected category"
#define KOrderInitializationscreen_NoRecommendedOrderForCustomerAlertMessage @"No Recommended orders available for "

#define KOrderInitializationscreen_NoTemplateOrderLineItemsAlertMessage @"No products available for selected template"

#define KOrderInitializationscreen_NoDCReOrderAlertMessage @"No orders available for selected category"

#define kPushNotificationTitle @"Push Notification"
#define kPushNotificationsMediaDirectory @"PushNotificationImages"

#define kUpdateTitle @"UPDATE"
#define kSaveTitle @"SAVE"
#define kSavePopOverTitle @"Save"
#define kCancelPopOverTitle @"Cancel"

#define kReasonTitle @"Reason"

#define KSignatureImagesFolderName @"Signature"
#define KDCImagesFolderName @"Distribution check images"
#define KLPOImagesFolderName @"LPOImages"
#define KCollectionImagesFolderName @"Collection"
#define kCollectionReceiptFolderName @"RECEIPTS"
#define KMerchandisingSurveyPicturesFolderName @"Merchandising Survey Pictures"

#define kMerchandisingCompetitorInfoImages @"Merchandising Competitor Pictures"

#define kMerchandisingPlanogramImages @"Planogram Pictures"
#define KCoachSurveySignatureFolderName @"Coach Signatures"


#define KJPEGMIMEFormat @"image/jpeg"
#define KLPOImageRoundMaskColor [UIColor colorWithRed:(54.0/255.0) green:(193.0/255.0) blue:(195.0/255.0) alpha:1];
#define KSync_UploadingLPOImagesStatusStr @"Uploading LPO images"
#define KSync_UploadingCollectionImagesStatusStr @"Uploading Collection images"


#define kYesTitle @"Yes"
#define kNoTitle @"No"
#define kNewToDoColor [UIColor colorWithRed:(75.0/255.0) green:(145.0/255.0) blue:(207.0/255.0) alpha:1]
#define kCancelledToDoColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(75.0/255.0) alpha:1]
#define kClosedToDoColor [UIColor colorWithRed:(252.0/255.0) green:(78.0/255.0) blue:(81.0/255.0) alpha:1]
#define kDefferedToDoColor [UIColor colorWithRed:(253.0/255.0) green:(159.0/255.0) blue:(76.0/255.0) alpha:1]
#define kCompletedColor [UIColor colorWithRed:(39.0/255.0) green:(195.0/255.0) blue:(165.0/255.0) alpha:1]

#define kSalesWorxPanelHeaderDividerColor [UIColor colorWithRed:(184.0/255.0) green:(193.0/255.0) blue:(200.0/255.0) alpha:1]

#define  KVisitsAndTrxsSettingsSwitchDisableFlag @"N"
#define  KVisitsAndTrxsSettingsSwitchEnableFlag @"Y"
#define  KCommunicationsSettingsSwitchDisableFlag @"N"
#define  KCommunicationsSettingsSwitchEnableFlag @"Y"

#define KDataUploadProcess_FullSync @"Full Sync"
#define KDataUploadProcess_SendOrders @"Send Orders"
#define KDataUploadProcess_VTRXBS @"V_Trx_BS"
#define KDataUploadProcess_COMMBS @"COMM_BS"
#define KDataUploadProcess_None @"None"

#define kStockSyncProcedureName @"sync_MC_ExecGetFSRProductStock"

#define kDataDownloadProcess_PRODSTK @"Product Stock"
#define kDataDownloadProcess_NONE @"None"


#define KENABLE_V_TRX_BS_NOTIFICATIONNameStr @"ENABLE_V_TRX_BS_NOTIFICATION"
#define KDISABLE_V_TRX_BS_NOTIFICATIONNameStr @"DISABLE_V_TRX_BS_NOTIFICATION"
#define KENABLE_COMM_BS_NOTIFICATIONNameStr @"ENABLE_COMM_BS_NOTIFICATION"
#define KDISABLE_COMM_BS_NOTIFICATIONNameStr @"DISABLE_COMM_BS_NOTIFICATION"
#define kENABLE_PRODSTK_NOTIFICATIONNameStr @"ENABLE_PRODSTK_BS_NOTIFICATION"
#define kDISABLE_PRODSTK_NOTIFICATIONNameStr @"DISABLE_PRODSTK_BS_NOTIFICATION"
#define KENABLE_ROUTE_BS_NOTIFICATIONNameStr @"ENABLE_ROUTE_BS_NOTIFICATION"
#define KENABLE_ROUTE_CANCEL_BS_NOTIFICATIONNameStr @"ENABLE_ROUTE_CANCEL_BS_NOTIFICATION"

#define kPredicateTypeContains @"contains[cd]"
#define kPredicateTypeEquals @"==[cd]"
#define kDataUnavailable @"Data Unavailable"



//Screen Names for Google Analytics

#define kUserActivationScreenName @"User Activation"
#define kLoginScreenName @"Login"
#define kFieldSalesDashboardScreenName @"Field Sales Dashboard"
#define kFieldSalesRouteScreenName @"Route"
#define kFieldSalesCustomersScreenName @"Customers"
#define kFieldSalesProductsScreenName @"Products"
#define kFieldSalesSurveyScreenName @"Survey"
#define kFieldSalesCommunicationsScreenName @"Communications"
#define kFieldSalesReportsScreenName @"Reports"
#define kFieldSalesOnsiteVisitTypeScreenName @"Onsite Visit"
#define kFieldSalesTelephonicVisitTypeScreenName @"Telephonic Visit"
#define kFieldSalesCustomerDuesScreenName @"Customer Dues"
#define kFieldSalesOrderInitilisationScreenName @"Order Initialisation"
#define kFieldSalesSalesOrderScreenName @"Sales Order"
#define kFieldSalesSalesOrderConfirmationScreenName @"Sales Order Confirmation"
#define kFieldSalesDistributionCheckScreenName @"Distribution Check"
#define kFieldSalesReturnsScreenName @"Returns"
#define kFieldSalesReturnsInitialisationScreenName @"Returns Initialisation"
#define kFieldSalesReturnsConfirmationScreenName @"Returns Confirmation"
#define kFieldSalesOrderHistoryScreenName @"Order History"
#define kFieldTodoScreenName @"To Do"
#define kFieldTodoFilterScreenName @"To Do Filter"
#define kFieldSalesBonusDetails @"SalesOrder Bonus Details"
#define kFieldSalesStockDetails @"SalesOrder BonStockus Details"
#define kFieldSalesSalesHistory @"SalesOrder Sales History"
#define kFieldNearByCustomersScreenName @"Nearby Customers"
#define kFieldSalesCoachReportScreenName @"Coach Report"
#define kFieldSalesCoachReportConfirmationScreenName @"Coach Report Confirmation"
#define kMerchandisingScreenName @"Merchandising"
#define kFieldSalesProductMedia @"Product Media"


#define kFieldMarketingDashboard @"Field Marketing Dashboard"
#define kFieldMarketingDoctors @"Doctors"
#define kFieldMarketingPharmacies @"Pharmacies"
#define kFieldMarketingProducts @"Products"
#define kFieldMarketingVisits @"Field Marketing Visits"
#define kFieldMarketingBAVisits @"Brand Ambassador Visits"
#define kFieldMarketingCreateVisit @"Create Field Marketing Visit"
#define kFieldMarketingCreateLocation @"Create Location"
#define kFieldMarketingCreateDoctor @"Create Doctor"
#define kFieldMarketingCreateContact @"Create Contact"
#define kFieldMarketingVisitOptions @"Visit Options"

#define kFieldMarketingEdetailingMultipleVisits @"EDetailing Start Visit"
#define kFieldMarketingEdetailingStartCall @"EDetailing Start Call"
#define kFieldMarketingEdetailingEndCall @"EDetailing End Call"
#define kFieldMarketingEdetailingEndVisit @"EDetailing End Visit"

#define kFieldMarketingEdetailingTasks @"EDetailing Tasks"
#define kFieldMarketingEdetailingTaskDetails @"EDetailing Task Details"
#define kFieldMarketingEdetailingNotes @"EDetailing Tasks"
#define kFieldMarketingEdetailingNotesDetails @"EDetailing Task Details"


#define kFieldMarketingEdetailingProductImages @"EDetailing Product Images"
#define kFieldMarketingEdetailingProductVideos @"EDetailing Product Videos"
#define kFieldMarketingEdetailingProductPDF @"EDetailing Product PDF"
#define kFieldMarketingEdetailingProductPresentations @"EDetailing Product Presentations"

#define kFieldMarketingEdetailingProductEmail @"EDetailing Product Email"

#define kBrandAmbassadorVisits @"Brand Ambassador Visits"
#define kBrandAmbassadorCreateVisits @"Brand Ambassador Create Visits"
#define kBrandAmbassadorStartVisit @"Brand Ambassador Start Visit"
#define kBrandAmbassadorStartCall @"Brand Ambassador Start Call"
#define kBrandAmbassadorEndCall @"Brand Ambassador End Call"
#define kBrandAmbassadorEndVisit @"Brand Ambassador End Visit"


#define kFieldMarketingDoctorFilter @"Doctor Filter"
#define kFieldMarketingPharmcyFilter @"Pharmacy Filter"
#define kFieldMarketingProductFilter @"Product Filter"
#define kFieldMarketingVisitsFilter @"Visits Filter"
#define kFieldMarketingBrandAmbassadorVisitsFilter @"Brand Ambassador Visits Filter"


#define kFieldSalesCustomerFilter @"Customer Filter"
#define kFieldSalesProductsFilter @"Filed Sales Products Filter"
#define kFieldSalesCollectionFilter @"Collection Filter"
#define kFieldSalesCustomerFilter @"Customer Filter"

#define kNewStatisticsTitle @"New"
#define kClosedStatisticsTitle @"Closed"
#define kCancelledStatisticsTitle @"Cancelled"
#define kDeferedStatisticsTitle @"Deferred"
#define kCompletedStatisticsTitle @"Completed"


#define KCommunicationVersion2Value @"2"
#define KSalesWorxDeviceValidationErrorMessage @"Device validation error"

/** Sales Summary Screen*/
#define kCustomerNamePopOverTitle @"Customer Name"
#define kCustomerTypePopOverTitle @"Customer Type"
#define kDocumentTypePopOverTitle @"Document Type"
#define kFromDatePopOverTitle @"From Date"
#define kToDatePopOverTitle @"To Date"
#define kPaymentTypePopOverTitle @"Payment Type"
#define kTargetTypePopOverTitle @"Target Type"
#define kTargetCategoryPopOverTitle @"Target Category"


#define kAchivementVsTargetFrequencyPopOverTitle @"Frequency Type"
#define kYeralyPopOverTitle @"Year"
#define kMonthQuarterPopOverTitle @"Month/Quarter"



#define kOrderHistoryScreenERPInvoiceNumberPopOverTitle @"Invoice Numbers"


#define kSalesOrderTakenEventName @"Sales Order Taken"
#define kReturnsTakenEventName @"Returns Taken"
#define kDataSyncDoneScreenName @"Full Sync Done"
#define kFieldSalesDistributionCheckDoneScreenName @"Distribution Check Done"
#define kSurveyCapturedEventName @"Survey Captured"
#define kPaymentCapturedEventName @"Collection Taken"
#define kSalesOrderTakenFromUnConfirmedOrdersPopOverEventName @"Sales Order Taken From UnConfirmed Orders Pop Over"

//Device time validation
#define kServerTimeURL @"http://www.ucssolutions.com/utils/gettime.php"
#define kInvalidDeviceTimeAlert @"Please reset device time and try again"

#define KBasePopUpViewAnimationTime 0.5
#define keDetailingDefaultDemoTime @"5"


#define headerTitleFont kSWX_FONT_SEMI_BOLD(19)

#define kKontactBeaconAPIKey @"NtSuCqaaBxYmkYMcswqGVQQRYGIXimFn"
#define KCameraAccessRefuseAlertMessage @"You previously chose not to use your phone's camera with the SalesWorx app. To use this feature, tap Settings and turn on the camera in the privacy section."
#define KCameraAccessRestrictAlertMessage @"You've been restricted from using the camera on this device. Without camera access this feature won't work."


#define kChequeImageType @"Cheque_Image"
#define kReceiptImageType @"Receipt_Image"
#define KppsxFileExtentionStr @"ppsx"
#define kSelectedRadioButtonImage @"Sync_ GreenTickMark"
#define kUnSelectRadioButtonImage @"Sync_GreenUnTickMark"

// coach survey
#define kTextViewResponseDisplayType @"TEXT"
#define kSignatureResponseDisplayType @"SIGNATURE"

#define kMerchandisingVisitType @"Merchandising"

#define kNextVisitObjective @"Next Visit Objective"
#define kNextVisitObjectiveTitleAppCode @"FM_NEXT_VISIT_OBJECTIVE_TITLE"
#define kMediaTypeImage @"IMAGE"
#define kMediaTypeVideo @"VIDEO"
#define kMediaTypeBrochure @"BROCHURE"
#define kMediaTypePPT @"POWERPOINT"


typedef enum
{
    KEmptySignature,
    KSignatureDisabled,
    KSignatureSaved,
    KSignatureNotSaved
} SignatureViewStatusType;

typedef enum {
    ProductSerachNormal, /* ==0*/
    ProductSearchByBarCode,
    ProductSearchByPopOver
} ProductSearchType;
typedef enum {
    NormalProductTypeCell, /* ==0*/
    ManualFOCTypeCell,
    SimpleBonusTypeCell,
    AssortMentBonusTypeCell
} OrderItemsTableViewCellType;


typedef enum
{
    KVisitCreate,
    KVisitUpdate,
    KVisitReschedule
} MedRepVisitEditType;

@interface SWDefaults : NSObject
{
}


+(NSString*)applicationDocumentsDirectory;

+ (NSString*) dbGetDatabasePath;
+ (NSString*) dbGetDatabaseZipFilePath;


+(UIViewController *)currentTopViewController;

+ (void)initializeDefaults;

+ (NSString *)username;
+ (void)setUsername:(NSString *)username;

+ (NSString *)password;
+ (void)setPassword:(NSString *)password;

+ (NSString *)lastPerformaOrderReference;
+ (void)setLastPerformaOrderReference:(NSString *)ref;
+(void)ClearLoginCredentials;

+ (NSString *)lastOrderReference;
+ (void)setLastOrderReference:(NSString *)ref;

+ (NSString *)lastCollectionReference;
+ (void)setLastCollectionReference:(NSString *)ref;

+ (NSString *)lastReturnOrderReference ;
+ (void)setLastReturnOrderReference:(NSString *)ref;

+ (NSDictionary *)userProfile;
+ (void)setUserProfile:(NSDictionary *)array;

+ (NSString *)activationStatus ;
+ (void)setActivationStatus:(NSString *)ref;

+ (NSString *)lastSyncDate ;
+ (void)setLastSyncDate:(NSString *)ref;

+ (NSString *)lastSyncStatus ;
+ (void)setLastSyncStatus:(NSString *)ref;

+ (NSString *)lastSyncType ;
+ (void)setLastSyncType:(NSString *)ref;

+ (NSString *)lastSyncOrderSent ;
+ (void)setLastSyncOrderSent:(NSString *)ref;

+ (NSDictionary *)licenseKey ;
+ (void)setLicenseKey:(NSDictionary *)ref;

+ (NSString *)usernameForActivate ;
+ (void)setUsernameForActivate:(NSString *)ref;

+ (NSString *)passwordForActivate;
+ (void)setPasswordForActivate:(NSString *)ref;

+ (NSString *)isActivationDone;
+ (void)setIsActivationDone:(NSString *)ref;

+ (NSString *)serverArray ;
+ (void)setServerArray:(NSString *)ref;

+ (NSString *)selectedServerDictionary ;
+ (void)setSelectedServerDictionary:(NSString *)ref;

+ (NSString *)licenseCustomerID;
+ (void)setLicenseCustomerID:(NSString *)ref;

+ (NSArray *)appControl;
+ (void)setAppControl:(NSArray *)array;

+ (NSString *)versionNumber;
+ (void)setVersionNumber:(NSString *)ref;

+ (NSString *)licenseIDForInfo;
+ (void)setLicenseIDForInfo:(NSString *)ref;

+ (NSString *)odoMeterPreviousReading;
+ (void)setOdoMeterPreviousReading:(NSString *)reading;

//////////////////////////////////

+ (NSMutableDictionary *)customer;
+ (void)setCustomer:(NSMutableDictionary *)customerDict;
+ (void)clearCustomer;

+ (NSString *)locationFilterForCustomerList;
+ (void)setLocationFilterForCustomerList:(NSString *)filter;

+ (NSString *)filterForCustomerList;
+ (void)setFilterForCustomerList:(NSString *)cus_filter;

+ (NSString *)statusFilterForCustomerList;
+ (void)setStatusFilterForCustomerList:(NSString *)status_filter;

+ (NSString *)paymentFilterForCustomerList;
+ (void)setPaymentFilterForCustomerList:(NSString *)payment_filter;

+ (NSString *)barCodeFilterForCustomerList;
+ (void)setbarCodeFilterForCustomerList:(NSString *)barcode_filter;

+ (NSString *)codeFilterForCustomerList;
+ (void)setCodeFilterForCustomerList:(NSString *)code_filter;

+ (NSString *)nameFilterForCustomerList;
+ (void)setNameFilterForCustomerList:(NSString *)name_filter;

+ (NSString *)filterForProductList;
+ (void)setFilterForProductList:(NSString *)pro_filter;

+ (NSString *)productFilterBrand;
+ (void)setProductFilterBrand:(NSString *)value;

+ (NSString *)productFilterName;
+ (void)setProductFilterName:(NSString *)value;

+ (NSString *)productFilterProductID;
+ (void)setProductFilterProductID:(NSString *)value;

+ (NSString *)paymentType;
+ (void)setPaymentType:(NSString *)value;

+ (NSString *)currentVisitID;
+ (void)setCurrentVisitID:(NSString *)visit_ID;

+ (void)clearCurrentVisitID;

+ (NSString *)orderAmount;

+ (NSString *)pdc_due;

+ (NSString *)Credit_Period;

+ (NSString *)Over_Due;

+ (void)setOrderAmount:(NSString *)orderAmount CreditLimit:(NSString *)credit_Limit PDCDUE:(NSString *)pdc_due Credit_Limit:(NSString *)credit_period OverDues:(NSString *)over_due;

+ (NSString *)availableBalance;
+ (void)setAvailableBalance:(NSString *)avl_bal;

+ (NSDictionary *)productCategory;
+ (void)setProductCategory:(NSDictionary *)category;
+ (void)clearProductCategory;
+(BOOL)isDrApprovalAvailable;

+ (int)collectionCounter;
+ (void)setCollectionCounter:(int)ref;

+ (NSString *)startDayStatus;
+ (void)setStartDayStatus:(NSString *)ref;

+(void)showOrderHistoryDivider:(BOOL)status;

+ (NSMutableDictionary *)validateAvailableBalance:(NSArray *)orderAmmount;

+ (void)clearEverything;
+(NSString*)checkTgtField;
+(NSString*)formatWithThousandSeparator:(NSInteger)number;

+(NSString*)checkTargetSalesDashBoard;
+(NSString*)checkMultiUOM;

+(id)fetchSignature;
+(void)setSignature:(id)signatureData;


+(void)deleteOldDeviceLogFiles;
+(NSString*)fetchDeviceLogsFolderPath;


+(NSString*)fetchDashboardType;

+(NSString*)formatStringWithThousandSeparator:(NSInteger)number;

+(BOOL)isMedRep;
+(void)clearSignature;


+ (NSMutableDictionary *)nullFreeDictionary:(NSMutableDictionary *)dictionary;
+(NSDictionary*)fetchBarAttributes;
+ (BOOL)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName;
+(UIView*)createNavigationBarTitleView:(NSString*)title;

+(void)showAlertView :(NSMutableArray*)alertContentArray;

+(BOOL)validateSalesWorxLicense :(NSString*)licenseLimit;

+(void)setOnsiteVisitStatus:(BOOL)option;
+(BOOL)fetchOnsiteVisitStatus;



+(NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end andMainString:(NSString *)mainString;

+(void)invalidateCurrentFieldSalesVisitTimer;

+(BOOL)fetchSalesWorxLicenseActivationStatus;
+(void)setSalesWorxLicenseActivationStatus:(BOOL)status;
+(NSString *)getValidStringValue:(id)inputstr;
+(NSMutableArray *)parseCustomersArray:(NSMutableArray*)customersArray;
+(NSMutableArray*)fetchSyncLocations;

+(BOOL)fetchDCOptionalStatusforOnsiteVisit;
+(void)isDCOptionalforOnsiteVisit:(BOOL)status;



+(CGSize)fetchSizeofLabelWithText:(NSString*)text andFont:(UIFont*)fontString;

+(CGSize)fetchSizewithFontConstrainttoSize :(NSString*)text fontName:(UIFont*)fontName labelSize:(CGSize)labelSize;

+ (void)writeLogs:(NSString *)str;
+ (void)redirectLogToDocuments;

+(void)checkandDeletePreviousLogs;
+(NSMutableDictionary*)fetchCurrentCustDict:(Customer*)selectedCustomer;
+(void)updateDeviceValidationFailureDetailstoGoogleAnalytics:(NSMutableDictionary*)deviceDetailsDict;


+(void)displayAlertView;
+(Customer*)fetchCurrentCustObject:(NSMutableDictionary*)currentCustomer;
+(UITableView*)fetchUITableViewAttributes:(UITableView*)currentTableView;
+(NSPredicate*)fetchMultipartSearchPredicate:(NSString*)searchString withKey:(NSString*)keyString;
-(NSString *)getAppVersionWithBuild;
+(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController;
+(void) ShowConfirmationAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController;
+(UIAlertAction*)GetDefaultCancelAlertActionWithTitle:(NSString*)title;

+(void)createDirectory:(NSString *) dirName;
+(void)showCameraWithMode:(UIImagePickerControllerCameraCaptureMode)mode withDelegate:(UIViewController *)parentViewController;
-(void)saveLPOImage:(UIImage *)image InFolderWithName:(NSString *)FolderName withImageName:(NSString *)ImageName;
+(void)saveLPOImage:(UIImage *)image  withName:(NSString *)ImageName;
+(NSString *)getLPOImagesFolderPath;
+(void)deleteLPOTemporaryImages;
+(void)copyFilesFromLPOImagesTempFolder:(NSString*)path withPrefixName:(NSString *)orderId;
+(NSString *)getDCImagesFolderPath;
+(NSString *)getSignatureImagesFolderPath;
+(NSMutableArray *)getSyncImagesObjects;
+(NSMutableArray *)deleteSyncImagesInaFolder:(NSString*)folderPath;
+(BOOL)isDailyFullSyncRequired;
+(BOOL)isVersionUpdateFullSyncRequired;
+ (NSString *)lastFullSyncDate;
+ (void)setLastFullSyncDate:(NSString *)ref;
+ (NSString *)lastVTRXBackGroundSyncDate;
+ (void)setLastVTRXBackGroundSyncDate:(NSString *)ref;
+ (NSString *)lastCOMMBackGroundSyncDate;
+ (void)setLastCOMMBackGroundSyncDate:(NSString *)ref;
+ (NSString *)lastRouteRescheduleCancelBackGroundSyncDate;
+ (void)setLastRouteRescheduleCancelBackGroundSyncDate:(NSString *)ref;
+ (NSString *)lastRouteRescheduleBackGroundSyncDate;
+ (void)setLastRouteRescheduleBackGroundSyncDate:(NSString *)ref;
+(BOOL)isVTRXBackgroundSyncRequired;
+ (NSString *)VisitsAndTransactionsSyncSwitchStatus;
+ (void)setVisitsAndTransactionsSyncSwitchStatus:(NSString *)ref;
+(BOOL)isCommunicationBackgroundSyncRequired;
+ (NSString *)CommunicationSyncSwitchStatus;
+ (void)setCommunicationSyncSwitchStatus:(NSString *)ref;
+(BOOL)isRTL;

+(NSMutableArray*)fetchDatesofCurrentMonth;

+ (void)startVisitTimerWithController:(UIViewController *)viewController;
+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval;
+(void)UpdateGoogleAnalyticsforScreenName:(NSString*)screenName;
+(void)updateGoogleAnalyticsCustomEvents:(SalesWorxGoogleAnalytics*)swxGoogleAnalytics;

+(NSString *)getCollectionImagesFolderPath;
+(void)saveCollectionImage:(UIImage *)image  withName:(NSString *)ImageName;
+(void)deleteCollectionTemporaryImages;
+(void)copyFilesFromCollectionImagesTempFolder:(NSString*)path withPrefixName:(NSString *)orderId;
+(NSDate*)getCommunicationV2DefaultExpiryDate;
+(NSString *)convertDateToDeteWithZeroTimeFormat:(NSDate *)date;
+ (NSString *)lastProductStockBackGroundSyncDate;
+ (void)setLastProductStockBackGroundSyncDate:(NSString *)ref;
+(BOOL)isproductStockSyncRequired;

+(void)setActivatedServerDetailsDictionary:(NSString*)activatedServerDetailsString;
+ (NSString *)activatedServerDictionary;
+(BOOL)isCustomerLevelVisitOptionsFlagEnabled;
+(NSMutableArray*)fetchPreviousMonthsforAgeingReport;
+(BOOL)isAgeingReportDisplayFormatMonthly;

+(double )getConversionRateWithRespectToPrimaryUOMCodeForProduct:(Products *)pro;
+(double )getConversionRateWithRespectToSelectedUOMCodeForProduct:(Products *)pro;
+(NSDecimalNumber *)getQuantityWithRespectToSelectedUOMCodeForProducrt:(Products *)pro PrimaryUOMQuanity:(NSDecimalNumber *)qty;



+(void)updateGoogleAnalyticsEvent:(NSString*)eventName;
-(UIBarButtonItem *)CreateBarButtonItemWithTitle:(NSString *)title;
-(void)AddBackButtonToViewcontroller:(UIViewController *)viewController;

//device time validation methods
+(BOOL)isDeviceTimeValid;
+(BOOL)isDrApprovalAvailable;

+(void)showDeviceTimeInvalidAlertinViewController:(UIViewController*)currentVC;


+(NSDictionary*)fetchTitleTextAttributesforSegmentControl;
+(NSDictionary*)fetchSelectedTitleTextAttributesforSegmentControl;
+(NSDictionary*)fetchHmSegmentControlSegmentTitleTextAttributes;
+(NSDictionary*)fetchHmSegmentControlSelectedTitleTextAttributes;
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;
+(BOOL)compressAndMoveImagestoThumbnailDirectory:(NSMutableArray*)imageNamesArray;
+(NSString *)getValidStringAndReplaceEmptyStringWithDefaultValue:(NSString *)str;
-(NSDictionary *)getDummyMedrepVisitLocationFromPhxTblLocationTable;
+(NSString *)getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:(Customer *)customer;
+(NSDate *)beginningOfDay:(NSDate *)date;


+(void)saveCollectionReceiptImage:(UIImage *)image  withName:(NSString *)ImageName;
+(void)deleteCollectionReceiptTemporaryImages;
+(void)copyFilesFromCollectionReceiptImagesTempFolder:(NSString*)path withPrefixName:(NSString *)orderId;
+(NSString *)getCollectionReceiptImagesFolderPath;
-(UINavigationController *)getPresenationControllerWithNavigationController:(UIViewController *)delegeteController WithRootViewController:(UIViewController *)RootViewController WithSourceView:(UIView *)sourceView WithSourceRect:(CGRect)sourcerect WithBarButtonItem:(UIBarButtonItem *)barButtonItem WithPopOverContentSize:(CGSize )PopOverContentSize;
+(void)ShowToastMessage:(NSString *)message;

//merchandising survey pictures
+(NSString *)getMerchandisingSurveyMarkingPicturesFolderPath;
+(void)saveMerchandisingSurveyMarkingPictures:(UIImage *)image  withName:(NSString *)ImageName;

+(void)saveMerchandisingCompetitorImages:(UIImage*)capturedImage withImageName:(NSString*)imageImage;
+(void)savePlanogramImages:(UIImage*)capturedImage withImageName:(NSString*)imageName;
+(UINavigationController *)CreatePoPoverPresentationNavigationControllerWithViewController:(UIViewController *)Controller AndSourceView:(UIView *)sourceView AndSourceRect:(CGRect )sourcerect AndPerimittedArrowDirections:(UIPopoverArrowDirection)Arrowdirection AndDelegate:(UIViewController<UIPopoverPresentationControllerDelegate>*)delegateController;
+(UIColor *)averageColorOfImage:(UIImage*)image;
+(UIColor *)getContrastColorForImage:(UIImage*)image;


//coach survey signature image
+(void)saveCoachSurveySignatureImages:(UIImage *)image  withName:(NSString *)ImageName;
+(void)setCoachProfile:(NSMutableDictionary*)coachDictionary;
+(NSDictionary*)coachProfile;
+(void)clearCoachProfile;


@end

@interface CustomerPaymentClass : NSObject

@property (strong,nonatomic)   NSString * customerName;
@property (strong,nonatomic)   NSString * customerId;
@property (strong,nonatomic)   NSMutableArray *invoicesArray;

@property (strong,nonatomic)   NSString * paymentMethod;
@property (strong,nonatomic)   NSString * currencyType;
@property (strong,nonatomic)   NSString * currencyRate;
@property (strong,nonatomic)   NSString * bankName;
@property (strong,nonatomic)   NSString * chequeNumber;
@property (strong,nonatomic)   NSString * chequeDate;
@property (strong,nonatomic)   NSString * bankBranchName;
@property (strong,nonatomic)   NSString * remarks;

@property (strong,nonatomic)   NSDecimalNumber * customerPaidAmount;
@property (strong,nonatomic)   NSDecimalNumber * customerPaidAmountAfterConversion;
@property (strong,nonatomic)   NSDecimalNumber * customerDueAmount;
@property (strong,nonatomic)   NSDecimalNumber * settledAmount;
@property (strong,nonatomic)   NSDecimalNumber * totalAmount;
@property (strong,nonatomic)   NSDecimalNumber * amountGoingToPaid;
@end


@interface CustomerPaymentInvoiceClass : NSObject

@property (strong,nonatomic)   NSString * invoiceNumber;
@property (strong,nonatomic)    NSString * dueDate;
@property (strong,nonatomic)    NSString * InvoiceDate;

@property (strong,nonatomic)    NSDecimalNumber * dueNetAmount;
@property (strong,nonatomic)    NSDecimalNumber * previousSettledAmount;
@property (strong,nonatomic)    NSDecimalNumber * DueAmount;
@property (strong,nonatomic)    NSDecimalNumber * settledAmountForInvoice;

@end


@interface CustomerClass : NSObject
@property (strong,nonatomic)   NSString * customerName;
@property (strong,nonatomic)   NSString * customerNumber;
@property (nonatomic)   BOOL isCashCustomer;
@property (strong,nonatomic)   NSString * customerTradeChannel;
@property (strong,nonatomic)   NSString * customerId;

@end



