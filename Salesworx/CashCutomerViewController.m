//
//  CashCutomerViewController.m
//  SWPlatform
//
//  Created by msaad on 1/8/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "CashCutomerViewController.h"

@interface CashCutomerViewController ()

@end

@implementation CashCutomerViewController


- (id)initWithCustomer:(NSDictionary *)c
{
    self = [super init];
    if (self)
    {
        customer= [NSMutableDictionary dictionaryWithDictionary:c];
        [self setTitle:@"Cash Customer Detail"];
        self.view.backgroundColor = [UIColor whiteColor];
        customerHeaderView=nil;
        customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:customer];
        [customerHeaderView setShouldHaveMargins:YES];
        [self.view addSubview:customerHeaderView];
        
        tableViewController=nil;
        tableViewController=[[UITableView alloc] initWithFrame:CGRectMake(30, 90, self.view.frame.size.width - 60, self.view.frame.size.height-180) style:UITableViewStyleGrouped];
        [tableViewController setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [tableViewController setDelegate:self];
        [tableViewController setDataSource:self];
        tableViewController.backgroundView = nil;
        tableViewController.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:customerHeaderView];
        [self.view addSubview:tableViewController];
        
        //Memory Managment
        // tableViewController = nil;
        // customerHeaderView = nil;
        
        dataDict=nil;
        dataDict = [NSMutableDictionary dictionary];
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(Add:)]  animated:YES];
        
        contact = @"";
        fax = @"";
        address=@"";
        phone=@"";
        name=@"";
        
        // Do any additional setup after loading the view.

    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
       }
-(void)viewDidAppear:(BOOL)animated
{
    [Flurry logEvent:@"Cash Customer View"];
    //[Crittercism leaveBreadcrumb:@"<Cash Customer View>"];

   Singleton *single = [Singleton retrieveSingleton];
    if ([single.popToCash isEqualToString:@"Y"])
    {
        single.popToCash = @"N";
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }

}
-(void)Add:(id)sender
{
    [self.view endEditing:YES];
    if(([dataDict count]!=0 && isDropDownSelected ) || (name.length > 0 && phone.length > 0))
    {
       Singleton *single = [Singleton retrieveSingleton];
        [single setCashCustomerDictionary:dataDict];
        
        SWVisitManager *visitManager = [[SWVisitManager alloc] init];
        [visitManager startVisitWithCustomer:customer parent:self andChecker:@"D"];
       
    }
    else
    {
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Please provide required destails"
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry", nil) message:@"Please provide required destails." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
}
- (void)productSelected:(NSDictionary *)productDict
{
    isDropDownSelected = YES;
    [popoverController dismissPopoverAnimated:YES];
    [self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.10f];
}

- (void)presentProductOrder:(NSMutableDictionary *)productDict
{
    [dataDict removeAllObjects];
    if([productDict count]==0)
    {
        isDropDownSelected = NO;
    }
    else
    {
        [dataDict setValue:[productDict stringForKey:@"Customer_ID"]  forKey:@"Customer_ID"];
        [dataDict setValue:[productDict stringForKey:@"Site_Use_ID"] forKey:@"Site_Use_ID"];
        [dataDict setValue:[productDict stringForKey:@"Customer_Name"] forKey:@"Customer_Name"];
        isDropDownSelected = YES;
    }
    //NSLog(@"data %@",dataDict);
    [tableViewController reloadData];
    productDict=nil;
}
#define kNameField 1
#define kPhoneField 2
#define KContactField 3
#define KFaxField 4
#define KAdressFiels 5
#define kListField 2

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
if(section==0)
{
    return 5;
}
    else
    {
        return 1;
    }
    }

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    NSString *IDENT = @"CashCutomerDetail";
    SWTextFieldCell *cell = [tv dequeueReusableCellWithIdentifier:IDENT];
    
    if (cell == nil) {
        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT];
    }
 
    
//    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:IDENT];
//    if(cell!=nil)
//        cell=nil;
   
//        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        
        if (indexPath.section==0 && indexPath.row == 0) {
           cell.textField.placeholder = NSLocalizedString(@"Name", nil);
           cell.textField.tag = kNameField;
           cell.textField.text = name;
            [cell.textField setKeyboardType:UIKeyboardTypeAlphabet];
           cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
           cell.textField.delegate = self;
            [cell.label setText:[NSString stringWithFormat:@"*%@",NSLocalizedString(@"Name", nil)]];
            if (isDropDownSelected) {
               cell.textField.userInteractionEnabled = NO;
                
            }

        }
        else if(indexPath.section==0 && indexPath.row == 1)
        {
           cell.textField.placeholder = NSLocalizedString(@"Phone", nil);
           cell.textField.tag = kPhoneField;
           cell.textField.text = phone;
            [cell.textField setKeyboardType:UIKeyboardTypeNumberPad];
           cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
           cell.textField.delegate = self;
            [cell.label setText:[NSString stringWithFormat:@"*%@",NSLocalizedString(@"Phone", nil)]];

            if (isDropDownSelected) {
               cell.textField.userInteractionEnabled = NO;

            }
        }
        else if(indexPath.section==0 && indexPath.row == 2)
        {
           cell.textField.placeholder = NSLocalizedString(@"Address", nil);
           cell.textField.tag = KAdressFiels;
           cell.textField.text = address;
            [cell.textField setKeyboardType:UIKeyboardTypeAlphabet];
           cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
           cell.textField.delegate = self;
            [cell.label setText:NSLocalizedString(@"Address", nil)];
            
            if (isDropDownSelected) {
               cell.textField.userInteractionEnabled = NO;
                
            }
        }
        else if(indexPath.section==0 && indexPath.row == 3)
        {
           cell.textField.placeholder = NSLocalizedString(@"Contact", nil);
           cell.textField.tag = KContactField;
           cell.textField.text = contact;
            [cell.textField setKeyboardType:UIKeyboardTypeAlphabet];
           cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
           cell.textField.delegate = self;
            [cell.label setText:NSLocalizedString(@"Contact", nil)];
            
            if (isDropDownSelected) {
               cell.textField.userInteractionEnabled = NO;
                
            }
        }
        else if(indexPath.section==0 && indexPath.row == 4)
        {
           cell.textField.placeholder = @"Fax";
           cell.textField.tag = KFaxField;
           cell.textField.text = fax;
            [cell.textField setKeyboardType:UIKeyboardTypeAlphabet];
           cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
           cell.textField.delegate = self;
            [cell.label setText:@"Fax"];
            
            if (isDropDownSelected) {
               cell.textField.userInteractionEnabled = NO;
                
            }
        }
        else
        {
           cell.textField.tag = kListField;
           cell.textField.text = [dataDict stringForKey:@"Customer_Name"];
           cell.textField.userInteractionEnabled = NO;
        }
    
    
    
    return cell;

}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if(section==0)
    {
        return @"OR";
    }
    else
    {
        return nil;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
   if (section==0)
   {
    return @"Cash Customer Details";
   }
    else 
    {
        return NSLocalizedString(@"Select Customer", nil);
    }
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
  
    if (s==0)
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Cash Customer Details"];
        return sectionHeader;

    }
    else 
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:NSLocalizedString(@"Select Customer", nil)];
        return sectionHeader;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20.0f;
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)s {
  
    if(s==0)
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"OR"];
        
        return sectionHeader;
    }
    else
    {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20.0f;
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        if(!isTextFieldSelected)
        {
            isDropDownSelected = YES;
            listView=nil;
            //listView = [[GenCustListViewController alloc] init];
            listView = [[SalesWorxSurveyViewController alloc] init];
            [listView setTarget:self];
            [listView setAction:@selector(productSelected:)];
            [listView setPreferredContentSize:CGSizeMake(600, self.view.bounds.size.height - 100)];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listView];
            popoverController=nil;
            popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
            popoverController.delegate=self;
            //UITableViewCell *cell = [tableView1 cellForRowAtIndexPath:0];

            [popoverController setDelegate:self];
            [popoverController presentPopoverFromRect:CGRectMake(0, 400, tableViewController.frame.size.width, 44) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    else{
        isDropDownSelected = NO;
    }
}

#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    isTextFieldSelected = YES;
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField.tag == kNameField) {
        name=textField.text;
    } else if (textField.tag == kPhoneField){
        phone=textField.text;
    }
    else if (textField.tag == KContactField) {
        contact=textField.text;
    } else if (textField.tag == KFaxField){
        fax=textField.text;
    }
    else if (textField.tag == KAdressFiels){
        address=textField.text;
    }
    else
    {
        isTextFieldSelected = NO;

    }
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (name.length > 0 && phone.length > 0)
    {
        [dataDict removeAllObjects];
        [dataDict setValue:name forKey:@"cash_Name"];
        [dataDict setValue:phone forKey:@"cash_Phone"];
        [dataDict setValue:contact forKey:@"cash_Contact"];
        [dataDict setValue:fax forKey:@"cash_Fax"];
        [dataDict setValue:address forKey:@"cash_Address"];
        return YES;
    }
    
    // if the e-mail address has a value but the password doesn't, select the password
    if ([name length] > 0) {
        [[(SWTextFieldCell *)[tableViewController cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] textField] becomeFirstResponder];
    }
    // if the password has a value but the email doesn't...
    else if ([phone length] > 0) {
        [[(SWTextFieldCell *)[tableViewController cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]] textField] becomeFirstResponder];
    }
    else
    {
        isTextFieldSelected = NO;
    }
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverControllerq
{
    popoverControllerq=nil;
    listView=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
    
}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
