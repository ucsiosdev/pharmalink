//
//  MedRepRescheduleViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 5/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "SalesWorxCustomClass.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxBrandAmbassadorContentFilterViewController.h"
#import "SalesWorxBrandAmbassadorQueries.h"
#import "SalesWorxBrandAmbassadorContentFilterViewController.h"

#define kLocationTextField @"Location"
#define kDemoPlanTextField @"Demo Plan"

@protocol dismissDelegate <NSObject>

-(void)closeReschedulePopover;

@end


@interface MedRepRescheduleViewController : UIViewController<StringPopOverViewController_ReturnRow>
{
    StringPopOverViewController_ReturnRow* locationPopOver;
    
    SalesWorxBrandAmbassadorLocation * rescheduledLocation;
    
    DemoPlan * rescheduledDemoPlan;
    
    NSString* selectedTextField;
    
    NSMutableArray * locationsArray,*demoPlanArray;
    
    id dismissDelegate;
    NSString* popUpString;
    NSMutableArray *doctorsArray;
    NSMutableArray *pharmacyContactDetailsArray;
    
    UIPopoverController *doctorpopOverController,*datePickerpopOverController,*demonstrationPlanpopOverController;
    
    __weak IBOutlet MedRepTextField *doctorTextField;
    __weak IBOutlet MedRepTextField *dateTextField;
    __weak IBOutlet MedRepTextField *demoPlanTextField;
}

@property(nonatomic) id dismissDelegate;

@property(nonatomic,strong) NSString* locationType;
@property(nonatomic,strong) NSString* locationID;
@property(nonatomic,strong) NSMutableArray *arrDemoPlan;
@property(nonatomic,strong) NSMutableArray *contentArray;
@property(nonatomic,strong) NSMutableDictionary *visitDetailsDict;
@property(nonatomic) BOOL isBrandAmbassadorVisit;
@property (strong, nonatomic) IBOutlet UIButton *datePickerBtn;
@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * brandAmbassadorVisit;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *doctorContactLabel;
@property(strong,nonatomic) UIPopoverController* popOverController;
@property(strong,nonatomic) UINavigationController * createVisitNavigationController;



@end
