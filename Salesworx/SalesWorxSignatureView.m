//
//  SalesWorxSignatureView.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 2/2/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxSignatureView.h"
#import "SWDefaults.h"
@implementation SalesWorxSignatureView
@synthesize status,signatureArea;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void) awakeFromNib {
    [super awakeFromNib];
    status=KEmptySignature;
    [self setStatus:KEmptySignature];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // load view frame XIB
        [self commonSetup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // load view frame XIB
        [self commonSetup];
    }
    return self;
}

#pragma mark - setup view

- (UIView *)loadViewFromNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    
    //  An exception will be thrown if the xib file with this class name not found,
    UIView *view = [[bundle loadNibNamed:NSStringFromClass([self class])  owner:self options:nil] firstObject];
    return view;
}

- (void)commonSetup {
    UIView *nibView = [self loadViewFromNib];
    nibView.frame = self.bounds;
    // the autoresizingMask will be converted to constraints, the frame will match the parent view frame
    nibView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    // Adding nibView on the top of our view
    [self addSubview:nibView];
}
-(void)hideClearButton
{
    [signatureClearButton setHidden:YES];
}
-(void)showClearButton
{
    [signatureClearButton setHidden:NO];
}


-(void)hideSaveButton
{
    [signatureSaveButton setHidden:YES];
}
-(void)showSaveButton
{
    [signatureSaveButton setHidden:NO];
}

-(void)EnableSaveButton
{
    [signatureSaveButton setUserInteractionEnabled:YES];
}
-(void)disableSaveButton
{
    [signatureSaveButton setUserInteractionEnabled:NO];
}
-(void)saveButtonWithCompleteColor
{
    //[signatureSaveButton setBackgroundColor:[UIColor greenColor]];
    [signatureSaveButton UpdateImage:[UIImage imageNamed:@"Signature_Saved"] WithAnimation:YES];
}
-(void)saveButtonWithUnCompleteColor
{
    //  [signatureSaveButton setBackgroundColor:[UIColor redColor]];
    [signatureSaveButton UpdateImage:[UIImage imageNamed:@"Signature_UnSaved"] WithAnimation:YES];
    
}
-(void)setEnabled:(BOOL)flag
{
    if(flag)
    {
        [signatureArea setUserInteractionEnabled:YES];
    }
    else
    {
        [signatureArea setUserInteractionEnabled:NO];
    }
    
    //SignatureSaved
}
-(void)DisableSignatureView
{
    [signatureArea setUserInteractionEnabled:NO];
}


-(void)UpdateSignatureViewsForStatus:(SignatureViewStatusType)CStatus
{
    if(CStatus==KEmptySignature)
    {
        [self hideClearButton];
        [self hideSaveButton];
        [signatureArea clearSignature];
        [self saveButtonWithUnCompleteColor];
        [self setEnabled:YES];
    }
    else if(CStatus==KSignatureNotSaved)
    {
        [self showSaveButton];
        [self saveButtonWithUnCompleteColor];
        [self showClearButton];
    }
    else if(CStatus==KSignatureSaved)
    {
        [self showSaveButton];
        [self disableSaveButton];
        [self saveButtonWithCompleteColor];
        [self hideClearButton];
        [self setEnabled:NO];
        
    }
    else if(CStatus==KSignatureDisabled)
    {
        [signatureArea setHidden:YES];
        [signatureSaveButton setHidden:YES];
        [signatureClearButton setHidden:YES];
        
        
        CALayer *transparentLayer = [CALayer layer];
        
        
        
        transparentLayer.frame = CGRectMake(0.0f, 0, self.bounds.size.width, self.bounds.size.height);
        transparentLayer.backgroundColor = SalesWorxReadOnlyViewColor.CGColor;
        [self.layer addSublayer:transparentLayer];
    }
}

-(void)TESignatureViewTouchesBegans
{
    status=KSignatureNotSaved;
    [self setStatus:KSignatureNotSaved];
}

-(IBAction)SignatureClearButtonTapped:(id)sender
{
    if(_delegate!=nil)
    {
        [_delegate SalesWorxSignatureViewClearButtonTapped:signatureArea];
    }
    else
    {
        [signatureArea clearSignature];
        status=KEmptySignature;
        [self setStatus:KEmptySignature];
    }
}

-(IBAction)SignatureSaveButtonTapped:(id)sender
{
   if(_delegate!=nil)
   {
       [_delegate SalesWorxSignatureViewSaveButtonTapped:signatureArea];
   }
    else if(status!=KSignatureNotSaved)
    {
        [self performSignatureSaveAnimation];
    }
}
-(void)performSignatureSaveAnimation
{
    status=KSignatureSaved;
    [self setStatus:KSignatureSaved];
    [signatureSaveButton setImage:[UIImage imageNamed:@"Signature_UnSaved"]  forState:UIControlStateNormal];
    [signatureSaveButton UpdateImage:[UIImage imageNamed:@"Signature_Saved"] WithAnimation:YES];
}

-(void)setStatus:(SignatureViewStatusType)Cstatus
{
    [self UpdateSignatureViewsForStatus:Cstatus];

}
@end
