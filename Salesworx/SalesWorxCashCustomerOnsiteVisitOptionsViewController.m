//
//  SalesWorxCashCustomerOnsiteVisitOptionsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/5/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxCashCustomerOnsiteVisitOptionsViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "Singleton.h"
@interface SalesWorxCashCustomerOnsiteVisitOptionsViewController ()

@end

@implementation SalesWorxCashCustomerOnsiteVisitOptionsViewController
@synthesize segmentControl,nameTxtFld,phoneTxtFld,addressTxtFld,contactTxtFld,faxTxtFld,currentVisit;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    nameTxtFld.delegate=self;
    phoneTxtFld.delegate=self;
    addressTxtFld.delegate=self;
    contactTxtFld.delegate=self;
    faxTxtFld.delegate=self;
    addressTxtView.delegate=self;
    
   
    
    
    
    
    
    cashCustomerDetailsView.layer.cornerRadius=4.0f;
    cashCustomerDetailsView.layer.shadowColor = [UIColor blackColor].CGColor;
    cashCustomerDetailsView.layer.shadowOffset = CGSizeMake(2, 2);
    cashCustomerDetailsView.layer.shadowOpacity = 0.1;
    cashCustomerDetailsView.layer.shadowRadius = 1.0;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:titleView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(4.0, 4.0)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = titleView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    titleView.layer.mask = maskLayer;
    
    UIFont *font = MedRepSegmentControlFont;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    
    
    [segmentControl setTitleTextAttributes:attributes
                               forState:UIControlStateNormal];
    
    
    NSLog(@"cash cust details %@",currentVisit.visitOptions.customer.Cash_Cust);
    
    NSLog(@"check planned visit in visit type %hhd",currentVisit.isPlannedVisit);
   
    
//    
//    
    cashCustomerDetailsView.backgroundColor=UIViewControllerBackGroundColor;
    cashCustomerDetailsView.hidden=YES;
    [segmentControl setTitle:kOnsiteVisitTitle forSegmentAtIndex:0];
    [segmentControl setTitle:kTelephonicVisitTitle forSegmentAtIndex:1];
    
    [segmentControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    oldFrame=self.view.frame;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [cancelButton.titleLabel setFont:NavigationBarButtonItemFont];
    saveButton.titleLabel.font=NavigationBarButtonItemFont;
    
    cashCustomerDetailsDictionary=[[NSMutableDictionary alloc]init];
  
    [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if ([currentVisit.visitOptions.customer.Cash_Cust  isEqualToString:@"N"]) {
        
        //this is not a cash customer, hide cash customer container view
        customerDetailsContainerView.hidden=YES;
        detailsViewHeightConstraint.constant=0.0;
        cashCustDetailsViewTopConstraint.constant=0.0;
        
    }
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = cashCustomerDetailsView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backgroundTapped:(id)sender {
    
    [self closeTapped];
}

- (IBAction)segmentSwitch:(UISegmentedControl *)sender {
    
    selectedSegmentTitle=  [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
    NSLog(@"selected title is %@", selectedSegmentTitle);
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        //onsite  clicked
        NSLog(@"SETTING VISIT STATUS TO ONSITE");
        [SWDefaults setOnsiteVisitStatus:YES];
        
        
        
    }
    else{
       
        //telephonic  clicked
        NSString* title=  [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
        
        [SWDefaults setOnsiteVisitStatus:NO];
    }
    
    currentVisit.visitOptions.customer.Visit_Type=selectedSegmentTitle;

}

- (IBAction)saveButtonTapped:(id)sender {
    
    NSLog(@"cash customer dict in save %@",cashCustomerDetailsDictionary);
    
    if ([currentVisit.visitOptions.customer.Cash_Cust  isEqualToString:@"N"]) {
        if (selectedSegmentTitle.length==0) {
            [self displayAlertWithTitle:@"Missing Data" andMessage:[NSString stringWithFormat:@"Please choose visit type"] withButtonTitles:[NSArray arrayWithObjects:@"OK",nil] andTag:200];
        }
        else{
            [self closeTapped];
            
            if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
            {
                
                currentVisit.visitOptions.customer.cashCustomerDictionary=[[NSMutableDictionary alloc]initWithDictionary:cashCustomerDetailsDictionary];
                Singleton* singleTon=[Singleton retrieveSingleton];
                singleTon.cashCustomerDictionary=cashCustomerDetailsDictionary;
                if ([currentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"Y"]) {
                    singleTon.isCashCustomer=@"Y";
                }
                
                [self.delegate cashCustomerSelectedVisitOption:selectedSegmentTitle customer:currentVisit];

                
            }
        }
    }
    
    else
    {
    NSString* missingTextField;
    
    if (nameTxtFld.text.length==0) {
        missingTextField=@"name";

    }
    if (phoneTxtFld.text.length==0) {
        missingTextField=@"phone";
    }
//    if (addressTxtView.text.length==0) {
//        missingTextField=@"address";
//    }
//    if (faxTxtFld.text.length==0) {
//        missingTextField=@"fax";
//    }
    if (missingTextField.length>0) {
        [self displayAlertWithTitle:@"Missing Data" andMessage:[NSString stringWithFormat:@"Please enter data in %@ field",missingTextField] withButtonTitles:[NSArray arrayWithObjects:@"OK",nil] andTag:100];
    }
    else if (selectedSegmentTitle.length==0) {
        [self displayAlertWithTitle:@"Missing Data" andMessage:[NSString stringWithFormat:@"Please choose visit type"] withButtonTitles:[NSArray arrayWithObjects:@"OK",nil] andTag:200];
    }
    else{

        
    Singleton* singleTon=[Singleton retrieveSingleton];
    singleTon.cashCustomerDictionary=cashCustomerDetailsDictionary;
    singleTon.isCashCustomer=@"Y";
        NSLog(@"inof dict %@", cashCustomerDetailsDictionary);
        
        currentVisit.visitOptions.customer.cashCustomerDictionary=[[NSMutableDictionary alloc]initWithDictionary:cashCustomerDetailsDictionary];
        
        saveTapped=YES;
        
        
        [self closeTapped];
        
        if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
        {
            [self.delegate cashCustomerSelectedVisitOption:selectedSegmentTitle customer:currentVisit];
            
        }
        
    }
    }
}



- (void)CloseCashCustomerInfo
{
    //do what you need to do when animation ends...
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)closeTapped
{
    
    
        [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseCashCustomerInfo) userInfo:nil repeats:NO];
  
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    [self closeTapped];
    
}

#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"should change called with text %@", textField.text);
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([newString length] == 0)
    {

        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField==nameTxtFld)
    {
        
        NSString *expression = @"^[a-zA-Z0-9 ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=150);
    }
    else if(textField==phoneTxtFld)
    {
        NSString *expression = @"^[0-9]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=30);
    }
//    else if(textField==addressTxtFld)
//    {
//        NSString *expression = @"^[a-zA-Z0-9 ]+$";
//        NSError *error = nil;
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
//        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
//        return (numberOfMatches != 0 && newString.length<=100);
//        
//    }
    else if(textField==faxTxtFld)
    {
        NSString *expression = @"^[0-9]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<=100);
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"did begin called");
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"did end editing called");
    
    if (textField==nameTxtFld) {
        [cashCustomerDetailsDictionary setValue:nameTxtFld.text forKey:@"cash_Name"];
        currentVisit.visitOptions.customer.cash_Name=nameTxtFld.text;
       
    }
    else if (textField==phoneTxtFld)
    {
        [cashCustomerDetailsDictionary setValue:phoneTxtFld.text forKey:@"cash_Phone"];
        currentVisit.visitOptions.customer.cash_Phone=phoneTxtFld.text;

        
    }
    else if (textField==contactTxtFld)
    {
        [cashCustomerDetailsDictionary setValue:contactTxtFld.text forKey:@"cash_Contact"];
        currentVisit.visitOptions.customer.cash_Contact=contactTxtFld.text;

    }
       else if (textField==faxTxtFld)
    {
        [cashCustomerDetailsDictionary setValue:faxTxtFld.text forKey:@"cash_Fax"];
        currentVisit.visitOptions.customer.cash_Fax=faxTxtFld.text;

        
    }
        
   
    
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{

    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        
        if ([phoneTxtFld isFirstResponder]) {
            [addressTxtView becomeFirstResponder];
        }
        else{
        [textField resignFirstResponder];
        }
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark UIAlertView Delegate Methods
-(void)displayAlertWithTitle:(NSString*)Title andMessage:(NSString*)message withButtonTitles:(NSArray*)buttonTitlesArray andTag:(NSInteger)alertTag
{
    if ([UIAlertController class]) {
        // Use new API to create alert controller, add action button and display it
        alertController = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle: [buttonTitlesArray objectAtIndex:0] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            //yes button action
            if ([UIAlertController class]) {
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }        }];
        [alertController addAction: ok];
        
        if (buttonTitlesArray.count>1) {
            UIAlertAction* cancel = [UIAlertAction actionWithTitle: [buttonTitlesArray objectAtIndex:1] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                //no button action
                if ([UIAlertController class]) {
                    [alertController dismissViewControllerAnimated:YES completion:nil];
                }            }];
            [alertController addAction: cancel];
        }
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        // We are running on old SDK as the new class is not available
        // Hide the compiler errors about deprecation and use the class available on older SDK
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIAlertView *alert;
        if (buttonTitlesArray.count>1) {
            alert = [[UIAlertView alloc] initWithTitle:Title
                                               message:message
                                              delegate:self
                                     cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                     otherButtonTitles:[buttonTitlesArray objectAtIndex:1],nil];
        }
        else{
            alert = [[UIAlertView alloc] initWithTitle:Title
                                               message:message
                                              delegate:self
                                     cancelButtonTitle:[buttonTitlesArray objectAtIndex:0]
                                     otherButtonTitles:nil];
        }
        
        [alert show];
#pragma clang diagnostic pop
    }
    
}

#pragma mark KeyBoard Animation Methods
- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView animateWithDuration:5.0 animations:^{
        
        self.view.frame=CGRectMake(oldFrame.origin.x, oldFrame.origin.y-270, oldFrame.size.width, oldFrame.size.height);
    }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:5.0 animations:^{
        
        self.view.frame=oldFrame;
    }];

}


#pragma mark TextView methods

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==addressTxtView) {
        [cashCustomerDetailsDictionary setValue:textView.text forKey:@"cash_Address"];
        currentVisit.visitOptions.customer.cash_Address=textView.text;

    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    NSString *expression = @"^[a-zA-Z0-9 \n./-]+$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
            return (numberOfMatches != 0 && newString.length<=100);
    
}


@end
