//
//  SalesworxProductSamplesGivenViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/16/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesworxProductSamplesGivenViewController : UIViewController
{
    NSMutableArray *arrSamplesGiven;
}
@property (weak, nonatomic) IBOutlet UITableView *tblSamplesGiven;
-(void)updateSamplesGivenData:(NSMutableArray *)arraySamplesGiven;

@end
