//
//  SalesWorxSurveyCollectionViewCell.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 5/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSurveyCollectionViewCell.h"
#import "SWDefaults.h"


@implementation SalesWorxSurveyCollectionViewCell

//- (void)awakeFromNib
//{
//    // Initialization code
//    
////    CALayer *layer = self.captionBackGroundImageView.layer;
////    layer.shadowOffset = CGSizeMake(0, -1);
////    layer.shadowColor = [[UIColor blackColor] CGColor];
////    layer.shadowRadius = 2.0f;
////    layer.shadowOpacity = 0.1f;
////    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
////    
////    CALayer *layer1 = _surveyNumber.layer;
////    layer1.shadowOffset = CGSizeMake(0, -1);
////    layer1.shadowColor = [[UIColor blackColor] CGColor];
////    layer1.shadowRadius = 2.0f;
////    layer1.shadowOpacity = 0.1f;
////    layer1.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
//    
//    
//}


- (void)awakeFromNib {
    // Shadow and Radius
    self.layer.borderWidth=0.5;
    self.layer.cornerRadius=4.0f;
    self.layer.borderColor=[kVisitOptionsBorderColor CGColor];
    self.layer.shadowColor = [kVisitOptionsBorderColor CGColor];
    //    self.layer.shadowOffset = CGSizeMake(0, 2.0f);
    //    self.layer.shadowOpacity = 1.0f;
    //    self.layer.shadowRadius = 0.0f;
    //    self.layer.masksToBounds = NO;
    //    self.layer.cornerRadius = 4.0f;
    
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxSurveyCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}


@end
