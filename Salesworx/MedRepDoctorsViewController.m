//
//  MedRepDoctorsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepDoctorsViewController.h"
#import "ZUUIRevealController.h"
#import "MedRepTableViewCell.h"
#import "UIViewController+MJPopupViewController.h"
#import "MedRepCreateDoctorViewController.h"
#import "MedRepDefaults.h"
#import "MedRepCreateVisitViewController.h"
#import "MedRepVisitsViewController.h"
#import "MedRepMenuTableViewCell.h"
#import "medRepDoctorLocationsTableViewCell.h"
#import "MedRepSearchPopOverViewController.h"
#import "MedRepDoctorFilterViewController.h"
#import "CustomPopOverBackgroundView.h"
#import "MedRepUpdatedDesignCell.h"
#import "MedRepDoctorLocationsUpdatedTableViewCell.h"
#import "ILTranslucentView.h"
#import "UIImage+ImageEffects.h"
#import <GPUImage/GPUImage.h>
#import "MedRepMenuViewController.h"
#import "NSPredicate+Distinct.h"
#import "MedRepEdetailTaskViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepDefaults.h"
#import "AppControl.h"
#import "MedrepdoctorClassificationPopOverViewController.h"
#import "MedRep-Swift.h"


@interface MedRepDoctorsViewController ()
{
    AppControl *appControl;
}
@end

@implementation MedRepDoctorsViewController


@synthesize doctorsTblView,doctorsSearchBar,doctorNameLbl,doctorSpecializationLbl,contactNumberLbl,emailAddressLbl,tradeChannelLbl,classLbl,otcRxLbl,idLbl,statusLbl,doctorLocationTblView,locationMapView,requiredVisitsLbl,visitsCompletedLbl,lastVisitedAtLbl,lastVisitedOnLbl,doctorsArray,statusView,doctorLocationsArray,blurMask,blurredBgImage;


#pragma mark View Life Cycle Methods

    

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"WillRevealNotification" object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
  
    appControl = [AppControl retrieveSingleton];
    comingFromWillAppear=YES;

    indexPathArray=[[NSMutableArray alloc]init];
    indexPathLocationsArray=[[NSMutableArray alloc]init];
    unfilteredDoctorObjects=[[NSMutableArray alloc]init];

     [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    customSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0, 298, 44)];
    customSearchBar.hidden=YES;
    customSearchBar.delegate=self;

    doctorLocationsArray=[[NSMutableArray alloc]init];
    
    filteredDoctors=[[NSMutableArray alloc]init];

    appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSLog(@"current location from app delegate is %0.2f", appDel.currentLocation.coordinate.latitude);
    
    
    CLLocationCoordinate2D loc= appDel.currentLocation.coordinate;
    
    [self setLocationMapViewWithCoordinates:loc];
    
    // Do any additional setup after loading the view from its nib.
}


-(void)setLocationMapViewWithCoordinates:(CLLocationCoordinate2D)coordinates
{
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (coordinates , 800, 800);
    [locationMapView setRegion:regionCustom animated:NO];

    [locationMapView removeAnnotations:[locationMapView annotations]];
    
    // Create Annotation point

    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = coordinates;
    [locationMapView addAnnotation:Pin];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)emailLabelTapped
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"MedRep"];
       // [mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];
        
        NSLog(@"selected doctor data %@", doctor.Email);
        [mail.navigationBar setTintColor:[UIColor blackColor]];
        if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
           [mail setToRecipients:@[[NSString stringWithFormat:@"%@", doctor.Email]]];
        }
        
        [self removeKeyBoardObserver];
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
            
            UIAlertController * emailAlert=[UIAlertController alertControllerWithTitle:@"Email Unavailbale" message:@"This device cannot send email" preferredStyle:UIAlertControllerStyleAlert];
            
            [emailAlert addAction:[UIAlertAction actionWithTitle:(NSLocalizedString(KAlertOkButtonTitle, nil)) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self dismissViewControllerAnimated:YES completion:nil];

            }]];
            
            [self presentViewController:emailAlert animated:YES completion:nil];

        }
        
        else
        {
        
        UIAlertView * emailAlert=[[UIAlertView alloc]initWithTitle:@"Email Unavailbale "message:@"This device cannot send email" delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
            emailAlert.tag=87760;
        [emailAlert show];
        }
        
        NSLog(@"This device cannot send email");
    }
}

-(void)willRevealCalled
{
    NSLog(@"will reveal notification method called");
    
    [doctorsSearchBar setShowsCancelButton:NO animated:YES];
    
    if (isSearching==YES) {
        
    }  else {
        [self searchBarCancelButtonClicked:doctorsSearchBar];
    }
}

-(void)onKeyboardHide:(NSNotification *)notification
{
//    [doctorsSearchBar setShowsCancelButton:NO animated:YES];
//    
//    if (isSearching==YES) {
//        
//    }
//    
//    else
//    {
//        [self searchBarCancelButtonClicked:doctorsSearchBar];
//        [doctorsTblView reloadData];
//        [self deselectPreviousCellandSelectFirstCell];
//        
//    }
}

-(void)onKeyboardShow:(NSNotification *)notification
{
    [self closeSplitView];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {

    requiredVisitsLbl.font=kSWX_FONT_SEMI_BOLD(44);
    visitsCompletedLbl.font=kSWX_FONT_SEMI_BOLD(44);
    
    
    
    doctorsTblView.delegate=self;
    doctorsTblView.rowHeight = UITableViewAutomaticDimension;
    doctorsTblView.estimatedRowHeight = 70.0;
    
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingDoctors];
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRevealCalled) name:@"WillRevealNotification" object:nil];
    
 
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];

    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    

    if ([doctorsSearchBar showsCancelButton]) {
        
        [doctorsSearchBar becomeFirstResponder];
    }
    

    
    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Nav_MenuIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
        }
    }
    
    
    
     addDoctorButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"PlusIcon_White"] style:UIBarButtonItemStylePlain target:self action:@selector(createDoctorsTapped)];
    
      //[self.navigationItem setRightBarButtonItem:addDoctorButton];
    //fetch doctors data
    
    doctorsArray=[[NSMutableArray alloc]init];
    
    taskPopOverButton= [[SalesWorxPopOverNavigationBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"TaskIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnTask)];
    
    notePopOverButton=[[SalesWorxPopOverNavigationBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NoteIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnNote)];

    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:notePopOverButton,taskPopOverButton, nil];
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        if ([self.view.subviews containsObject:blurEffectView]) {
            
            [blurEffectView removeFromSuperview];
        }
        
    }
    

    if (self.isMovingToParentViewController) {
        
        doctorsArray=[[NSMutableArray alloc]init];
        
        doctorsArray=[MedRepQueries fetchDoctorObjects];
        
        
        NSLog(@"doctors count %lu", (unsigned long)[doctorsArray count]);
        
        
        //same doctor with multiple location issue handled with predicate
        
        NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:@"Doctor_ID"];
        
        
        NSArray * refinedArray=[doctorsArray filteredArrayUsingPredicate:refinedPred];
        
        
        
        doctorsArray=[refinedArray mutableCopy];
        
        unfilteredDoctorObjects=doctorsArray;
    }
    
  //  NSLog(@"doctors array count is %d", doctorsArray.count);
    
    isSearching=NO;

    if (doctorsArray.count>0) {
        [doctorsTblView reloadData];

        [self deselectPreviousCellandSelectFirstCell];
    }

    for (UIView *borderView in self.view.subviews) {
        
        if ([borderView isKindOfClass:[UIView class]]) {
            
            if (borderView.tag==1000 || borderView.tag==1001|| borderView.tag==1002|| borderView.tag==1003|| borderView.tag==1004 || borderView.tag==1005 || borderView.tag==1008 ) {
                
                
                
                borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                borderView.layer.shadowOffset = CGSizeMake(2, 2);
                borderView.layer.shadowOpacity = 0.1;
                borderView.layer.shadowRadius = 1.0;
            }
        }
    }
    
    locationMapView.layer.borderColor = UITextFieldDarkBorderColor.CGColor;
    locationMapView.layer.borderWidth = 1.0;
    locationMapView.layer.cornerRadius = 3.0;
    
    
    doctorLocationTblView.layer.borderColor = UITextFieldDarkBorderColor.CGColor;
    doctorLocationTblView.layer.borderWidth = 1.0;
    doctorLocationTblView.layer.cornerRadius = 3.0;
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Doctors"];

    NSLog(@"gestures in view %@", [self.view.gestureRecognizers description]);

    [self hideNoSelectionView];
    
    if([appControl.SHOW_TWO_LEVEL_DOCTOR_CLASSIFICATION isEqualToString:KAppControlsNOCode]){
    
        [ClassificationDetailsButton setHidden:YES];
        [ClassificationStringLabel setHidden:YES];
        [DoctorClassificationLabel setHidden:YES];
        xDoctorTypeStringLabelLeadingMarginToSuperViewContsraint.constant = 310;
        xDoctorClassLabelLeadingMarginToSuperViewConstraint.constant = 180;
    }
    
    if(![appControl.FM_SHOW_NEXT_VISIT_OBJECTIVE isEqualToString:KAppControlsYESCode]){
        [lastVisitDetailsButton setHidden:YES];
    }
    }
}

-(void)deselectPreviousCellandSelectFirstCell
{
    if ([doctorsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[doctorsTblView cellForRowAtIndexPath:selectedDoctorIndexPath];
        
        if (cell) {
            
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
        }

        [doctorsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                         animated:YES
                                   scrollPosition:UITableViewScrollPositionNone];
        
        [doctorsTblView.delegate tableView:doctorsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
}

- (void)revealController:(ZUUIRevealController *)revealController willRevealRearViewController:(UIViewController *)rearViewController
{
    NSLog(@"will show menu");
}

- (void)revealController:(ZUUIRevealController *)revealController didRevealRearViewController:(UIViewController *)rearViewController
{
    NSLog(@"did reveal called");
}

-(void)createDoctorsTapped
{
    //first create visit and then add doctor as part of visit
  
    MedRepVisitsViewController * createVisit=[[MedRepVisitsViewController alloc]init];

    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:createVisit] ;
    
    popOverController=nil;
    popOverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(600, 437) animated:YES];
    [popOverController presentPopoverFromBarButtonItem:addDoctorButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}


-(void)populateDoctorDescriptionData:(NSInteger)index
{
    visitsCompletedLbl.textColor=[UIColor redColor];

    visitsCompletedLbl.text=@"0";
    requiredVisitsLbl.text=@"N/A";
    lastVisitedAtLbl.text=@"N/A";
    lastVisitedOnLbl.text=@"N/A";
    
    
    Doctors *doc = [Doctors new];
    
    doc=[doctorsArray objectAtIndex:index];
    
    if (isSearching==YES) {
        
        //selectedDoctorData=[filteredDoctors objectAtIndex:index];

        doc = [filteredDoctors objectAtIndex:index];
    }
    else {
        //selectedDoctorData=[doctorsArray objectAtIndex:index];
        doc = [doctorsArray objectAtIndex:index];

    }
    
    if (doc!=nil) {
        
        doctorNameLbl.text=doc.Doctor_Name;
        
        idLbl.text=doc.Doctor_Code;
        
        doctorSpecializationLbl.text=doc.Classification_1;
        
        contactNumberLbl.text=doc.Phone;
        emailAddressLbl.text=doc.Email;

        doctorAdoptionstyleLabel.text=doc.adoptionStyle;
        doctorPersonalitystyleLabel.text=doc.personalityStyle;
        
        NSString* doctorStatus=doc.isActive;
        
        if (doctorStatus.length>0&& [doctorStatus isEqualToString:@"Active"]) {
            
            [doctorStatusImageView setBackgroundColor:[UIColor greenColor]];
            
        }
        else
        {
            [doctorStatusImageView setBackgroundColor:[UIColor redColor]];
            
        }

        
//        doctorNameLbl.text=[NSString stringWithFormat:@"%@", [selectedDoctorData valueForKey:@"Doctor_Name"]];
//       
//        if([NSString stringWithFormat:@"%@", [selectedDoctorData valueForKey:@"Doctor_Code"]].length>0)
//        {
//        idLbl.text=[NSString stringWithFormat:@"%@", [selectedDoctorData valueForKey:@"Doctor_Code"]];
//        }
//        else
//        {
//            idLbl.text=@"N/A";
//        }
//        
//        doctorSpecializationLbl.text=[NSString stringWithFormat:@"%@", [selectedDoctorData valueForKey:@"Specialization"]];

        
         statusLbl.text=doc.isActive;
        
        contactNumberLbl.text=doc.Phone;
        
        NSString* emailStr=doc.Email;
        
        
        
        if (![emailStr isEqualToString:@"N/A"]) {
            
            
      
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", emailStr]];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        
        
        emailAddressLbl.attributedText=attributeString;
        
        
            emailAddressLbl.userInteractionEnabled=YES;
        
        UITapGestureRecognizer * emailTapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(emailLabelTapped)];
        
        
        [emailAddressLbl addGestureRecognizer:emailTapGesture];
        
        
        }
        
        
        else
        {
            emailAddressLbl.text=emailStr;
        }
        
        classLbl.text=doc.Classification_2;
        
        NSNumber* requiredVisitsCount=[MedRepQueries fetchNumberOfVisitsRequiredForADoctor:doc];
                
        

        requiredVisitsLbl.text=[NSString stringWithFormat:@"%@",requiredVisitsCount ];

        NSNumber* completedVisitsCount=[MedRepQueries fetchNumberOFVisitsCompletedForaDoctorInCurrentMonth:doc];
        
        visitsCompletedLbl.text=[NSString stringWithFormat:@"%@",completedVisitsCount];
        
        if (completedVisitsCount.integerValue>=requiredVisitsCount.integerValue) {
            visitsCompletedLbl.textColor=[UIColor greenColor];
        }
        else if (completedVisitsCount<requiredVisitsCount){
            visitsCompletedLbl.textColor=[UIColor redColor];
        }
            
        
        
        //last visited on and last visited at is irrespective of month, so writing a saperate query
        NSMutableArray* visitCompletionArray=[MedRepQueries fetchVisitCompletionData:doc.Doctor_ID];
        if (visitCompletionArray.count>0) {
            
            // NSLog(@"location id is %@", doctor.locations.Location_ID);
            
            NSString* lastVisitedAtStr=[MedRepQueries fetchLocationNamewithID:[NSString stringWithFormat:@"%@",doc.locations.Location_ID] locationType:@"D"];
            
            if (lastVisitedAtStr.length>0) {
                //lastVisitedAtLbl.text=lastVisitedAtStr;
                
            }
 
            NSString* refinedLastVisitedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[NSString stringWithFormat:@"%@", [visitCompletionArray valueForKey:@"Visit_End_Date"]]];
            if (refinedLastVisitedDate.length>0) {
               // lastVisitedOnLbl.text=refinedLastVisitedDate;
                
            }
        }
        
        otcRxLbl.text=doc.Classification_3;
        
        tradeChannelLbl.text=doc.Custom_Attribute_1;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)resetDoctorData
{
    doctorNameLbl.text=@"";
    doctorSpecializationLbl.text=@"";
    emailAddressLbl.text=@"";
    tradeChannelLbl.text=@"";
    classLbl.text=@"";
    contactNumberLbl.text=@"";
    idLbl.text=@"";
    otcRxLbl.text=@"";
    visitsCompletedLbl.text=@"";
    requiredVisitsLbl.text=@"";
    lastVisitedAtLbl.text=@"";
    lastVisitedOnLbl.text=@"";
    
    
    CLLocationDegrees Latdegrees = 0.0;
    CLLocationDegrees Longdegrees = 0.0;
    CLLocationCoordinate2D c2D = CLLocationCoordinate2DMake(Latdegrees,Longdegrees);
    [self setLocationMapViewWithCoordinates:c2D];
    
    [doctorStatusImageView setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark UITableView DataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES && tableView==doctorsTblView) {
        
        if (filteredDoctors.count>0) {
            return filteredDoctors.count;
        }
        else {
            return 0;
        }
    }
    else
    {
        if (tableView==doctorsTblView ) {
            return doctorsArray.count;
        }
        else if (tableView==doctorLocationTblView) {
            return doctorLocationsArray.count;
        }
        else {
            return 0;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==doctorsTblView) {
        
        return UITableViewAutomaticDimension;
    }
    else {
        return 145.0f;
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==doctorsTblView) {

        static NSString* identifier=@"updatedDesignCell";
        
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    }
        
        if (isSearching && !NoSelectionView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([indexPathArray containsObject:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
        
        Doctors *doc = [Doctors new];

        if (isSearching && filteredDoctors.count>0) {
            
            doc=[filteredDoctors objectAtIndex:indexPath.row];
            
            cell.titleLbl.text=   [[NSString stringWithFormat:@"%@", doc.Doctor_Name] capitalizedString];
            
            cell.descLbl.text=[NSString stringWithFormat:@"%@", doc.Classification_1];
        }
        
        else if(doctorsArray.count>0)
        {
            doc=[doctorsArray objectAtIndex:indexPath.row];
            
            cell.titleLbl.text=[[NSString stringWithFormat:@"%@", doc.Doctor_Name] capitalizedString];
            cell.descLbl.text=[NSString stringWithFormat:@"%@", doc.Classification_1 ];
        }
        return cell;
        
    }
    
    else  {
        

            static NSString* identifier=@"updatedDoctorLocationsDesignCell";
            
            MedRepDoctorLocationsUpdatedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepDoctorLocationsUpdatedTableViewCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                
            }
            
            
            if ([indexPathLocationsArray containsObject:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else {
                
                [cell setDeSelectedCellStatus];
            }

            cell.separatorInset = UIEdgeInsetsZero;
            
            cell.titleLbl.text=[[NSString stringWithFormat:@"%@", [[doctorLocationsArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row]] capitalizedString];
            
            cell.accountNameLbl.text=[[NSString stringWithFormat:@"%@", [[doctorLocationsArray valueForKey:@"Account_Name"] objectAtIndex:indexPath.row]] capitalizedString];
            
            
            cell.descLbl.text=[[[[NSString stringWithFormat:@"%@", [[doctorLocationsArray valueForKey:@"Location"] objectAtIndex:indexPath.row]] capitalizedString]stringByAppendingString:@", "]stringByAppendingString:[[NSString stringWithFormat:@"%@", [[doctorLocationsArray valueForKey:@"Territory"] objectAtIndex:indexPath.row]] capitalizedString]];
            
            
            cell.accountNameTitleLbl.text=NSLocalizedString(@"Account Name", nil);

            return cell;
    }
}


#pragma mark UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==doctorLocationTblView) {
        
        MedRepDoctorLocationsUpdatedTableViewCell *cell = (MedRepDoctorLocationsUpdatedTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.saperatorImageView.hidden=NO;
    }
}


- (nullable NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==doctorsTblView) {

        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        cell.cellDividerImg.hidden=NO;
        
        return indexPath;
    }
    
    else
    {
        MedRepDoctorLocationsUpdatedTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        cell.saperatorImageView.hidden=NO;
        
        return indexPath;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0  ) {
        
    } else {
        [self closeSplitView];
    }
    
    
    if (tableView==doctorsTblView) {
        
        [self hideNoSelectionView];
        
        selectedDoctorIndexPath=indexPath;
        
        if (indexPathArray.count==0) {
            [indexPathArray addObject:indexPath];
        }
        else
        {
            indexPathArray=[[NSMutableArray alloc]init];
            [indexPathArray addObject:indexPath];
        }
        
        
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        cell.titleLbl.textColor=[UIColor whiteColor];
        [cell.descLbl setTextColor:[UIColor whiteColor]];
        
        cell.cellDividerImg.hidden=YES;
        
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
        
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        [doctorsTblView reloadData];
 
    }
    else {
        
        if (indexPathLocationsArray.count==0) {
            [indexPathLocationsArray addObject:indexPath];
            
        }
        else
        {
            [indexPathLocationsArray replaceObjectAtIndex:0 withObject:indexPath];
        }
        
        MedRepDoctorLocationsUpdatedTableViewCell *cell = (MedRepDoctorLocationsUpdatedTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.saperatorImageView.hidden=YES;

        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    }
    
    
    
    if (tableView==doctorsTblView) {
        
        doctor=[Doctors new];

        if (isSearching==YES)
        {
            if (filteredDoctors.count>0)
            {
                selectedVisitDetailsDict = [filteredDoctors objectAtIndex:indexPath.row];
                
                doctor= [filteredDoctors objectAtIndex:indexPath.row];
                
                doctorLocationsArray=[MedRepQueries fetchDoctorLocations:doctor.Doctor_ID];
                
                
                if (doctorLocationsArray.count>0) {
                    
                    [self populateDoctorDescriptionData:indexPath.row];
                    
                    [doctorLocationTblView reloadData];
                }
            }
        }
        else
        {
            selectedVisitDetailsDict = [doctorsArray objectAtIndex:indexPath.row];
            
            doctor=    [doctorsArray objectAtIndex:indexPath.row];
            
            doctorLocationsArray=[MedRepQueries fetchDoctorLocations:doctor.Doctor_ID];
            
            [self populateDoctorDescriptionData:indexPath.row];
            
            [doctorLocationTblView reloadData];
        }
        
        [self showLastVisitDetails:doctor.Doctor_ID];

        
        if ([doctorLocationTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
            
            [doctorLocationTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                               animated:YES
                                         scrollPosition:UITableViewScrollPositionNone];
        
        NSIndexPath * locationsFirstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        
        

        [doctorLocationTblView.delegate tableView:doctorLocationTblView didSelectRowAtIndexPath:locationsFirstIndexPath];
    }
    
    else
    {
        CLLocationCoordinate2D location;//=appDel.currentLocation.coordinate;
        
        location.latitude=25.1971;
        location.longitude=55.2741;
        
        if (doctorLocationsArray.count>0) {

            NSString* latStr=[NSString stringWithFormat:@"%@",[[doctorLocationsArray objectAtIndex:indexPath.row]valueForKey:@"Latitude"]];
            
            if (latStr.length>0) {
                
                selectedDoctorDict=[[NSMutableDictionary alloc]init];
                selectedDoctorDict=[doctorLocationsArray objectAtIndexedSubscript:indexPath.row];
                
                NSString* longStr=[NSString stringWithFormat:@"%@",[[doctorLocationsArray objectAtIndex:indexPath.row]valueForKey:@"Longitude"]];

                location.latitude=[latStr doubleValue];
                location.longitude=[longStr doubleValue];
                
                
                doctorLocationCoord=location;
            }
        }

        [self setLocationMapViewWithCoordinates:location];
    }
}




#pragma mark Map View Delegate Methods

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
//    [locationMapView setRegion:[locationMapView regionThatFits:region] animated:YES];
//    
//    // Add an annotation
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    point.coordinate = userLocation.coordinate;
//    point.title = @"Where am I?";
//    point.subtitle = @"I'm here!!!";
//    
//    [locationMapView addAnnotation:point];
}

#pragma MapView Delegate
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    NSString* doctorLatValue;
    NSString* doctorLongValue;
    
    if (selectedDoctorDict.count>0) {
        
        doctorLatValue =[NSString stringWithFormat:@"%@",[selectedDoctorDict valueForKey:@"Latitude"]];
        doctorLongValue=[NSString stringWithFormat:@"%@",[selectedDoctorDict valueForKey:@"Longitude"]];
        
        
    }
    
    
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        
        
        NSString* googleMapsUrlString;
        
        if (doctorLongValue.length>0 && doctorLatValue.length>0) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[doctorLatValue doubleValue],[doctorLongValue doubleValue]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication]openURL:googleMapsUrl];
            
        }
    }
    
    else
    {
        
        if (doctorLongValue.length>0 && doctorLatValue.length>0) {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[doctorLatValue doubleValue],[doctorLongValue doubleValue]]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
        
        else
        {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
    }
}


#pragma mark Search Bar Methods

- (IBAction)refreshButtonTapped:(id)sender {
    
    [self viewWillAppear:YES];
    [doctorsTblView reloadData];
    
    [doctorsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                animated:YES
                          scrollPosition:UITableViewScrollPositionNone];
    
    NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    
    
    
    [doctorsTblView.delegate tableView:doctorsTblView didSelectRowAtIndexPath:firstIndexPath];
}


- (IBAction)searchButtonTapped:(id)sender {

    UIButton* searchBtn=(id)sender;

    MedRepSearchPopOverViewController * popOverVC=[[MedRepSearchPopOverViewController alloc]init];
    
    popOverVC.navController=self.navigationController;
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    
    [popOverNavigationCroller setNavigationBarHidden:YES animated:NO];
    
    
    searchBarPopOverController=nil;
    searchBarPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    searchBarPopOverController.delegate=self;
    popOverVC.searchPopoverDelegate=self;
    popOverVC.popOverController=searchBarPopOverController;
    [searchBarPopOverController setPopoverContentSize:CGSizeMake(300, 44) animated:YES];

    
    CGRect btnFrame=searchBtn.frame;
    
    btnFrame.origin.x=btnFrame.origin.x+8;
    CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];
    
    
    
    [searchBarPopOverController presentPopoverFromRect:CGRectMake(relativeFrame.origin.x, searchBtn.frame.origin.y, searchBtn.frame.size.width, searchBtn.frame.size.height) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


-(void)medRepPopOverControllerSearchString:(NSString*)searchText
{
    [filteredDoctors removeAllObjects];
    
    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        isSearching = YES;
        [self searchContent:searchText];
    }
    else {
        [self hideNoSelectionView];
        isSearching = NO;
        [doctorsTblView reloadData];
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
  
    
    isSearching=NO;
    [self hideNoSelectionView];
    
      [self.view endEditing:YES];
    
    doctorsSearchBar.text=@"";
    
    [searchBar endEditing:YES];

    [self closeSplitView];
    
    if ([searchBar isFirstResponder]) {
        
        [searchBar resignFirstResponder];
    }
    
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
    if (isFiltered==YES) {
        
    }
    
    else
    {
        [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    }

    if (doctorsArray.count>0) {
        [doctorsTblView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
}


-(void)closeSplitView
{
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        if (revealController.currentFrontViewPosition !=0)
        {
            [revealController revealToggle:self];
        }
        revealController=nil;
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self closeSplitView];
    
    [doctorsSearchBar setShowsCancelButton:YES animated:YES];

    if ([searchBar isFirstResponder]) {
    }
    
    else {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {

    //Remove all objects first.
    
    [filteredDoctors removeAllObjects];

    
    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        
        isSearching = YES;
        [self searchContent:self.doctorsSearchBar.text];
    }
    else {
        isSearching = NO;
        
        [self hideNoSelectionView];
        
        [doctorsTblView reloadData];
        
        [doctorsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        [doctorsTblView.delegate tableView:doctorsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);

    isSearching=YES;
    
    [self.view endEditing:YES];
    
    [searchBar setShowsCancelButton:NO animated:YES];
}

-(void)searchContent:(NSString*)searchText
{
    NSLog(@"searching with text in medrep doctors %@",searchText);

    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"SELF.Doctor_Name contains[cd] %@",searchText];
    
    NSArray *testArray=[[NSMutableArray alloc]init];
    
    testArray=[doctorsArray filteredArrayUsingPredicate:searchPredicate];
    

    if (filteredDoctors.count>0) {
        //filteredDoctors=[[doctorsArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        
    }
    else
    {
        filteredDoctors=[[doctorsArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        
    }


    if (filteredDoctors.count>0) {
        [doctorsTblView reloadData];
        
//        [doctorsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
//                                    animated:YES
//                              scrollPosition:UITableViewScrollPositionNone];
//        
//        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//        
//        
//        
//        [doctorsTblView.delegate tableView:doctorsTblView didSelectRowAtIndexPath:firstIndexPath];


    }
    
    else if (filteredDoctors.count==0)
    {
        if (isFiltered==YES) {
            
        }
        
        isSearching=YES;
        [doctorsTblView reloadData];
//        [doctorLocationsArray removeAllObjects];
//        [doctorLocationTblView reloadData];
//        [self resetDoctorData];

    }
}

#pragma mark Create Doctor Delegate Method

-(void)addDoctorData:(NSMutableArray*)locationDetails;
{
  //  NSLog(@"doctor created %@", [locationDetails description]);
    
}


#pragma mark Filter Methods


- (UIImage*)getBlurredImage {
    // You will want to calculate this in code based on the view you will be presenting.
    CGSize size = CGSizeMake(200,200);
    
    UIGraphicsBeginImageContext(size);
    [self.view drawViewHierarchyInRect:(CGRect){CGPointZero, 1024, 768} afterScreenUpdates:YES]; // view is the view you are grabbing the screen shot of. The view that is to be blurred.
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Gaussian Blur
    //image = [image applyLightEffect];
    
    // Box Blur
     //image = [image boxblurImageWithBlur:0.2f];
    
    return image;
}


-(void)removeKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    
}


-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
}



- (IBAction)filterButtonTapped:(id)sender {

    
    [self.view endEditing:YES];
    
    [doctorsSearchBar setShowsCancelButton:NO animated:YES];
    
    if ([doctorsSearchBar isFirstResponder]) {
        
        doctorsSearchBar.text=@"";
        
        [doctorsSearchBar resignFirstResponder];
        
        [self.view endEditing:YES];
        
        [self removeKeyBoardObserver];
        
        
        isSearching=NO;
        [self hideNoSelectionView];
        
        [doctorsTblView reloadData];
        
    }

    if (indexPathArray.count>0) {
        [indexPathArray replaceObjectAtIndex:0 withObject:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
    [self deselectPreviousCellandSelectFirstCell];
    
    
    filterByArray=[[NSMutableArray alloc]initWithObjects:@"Class",@"OTC",@"Rx",@"Trade Channel",@"Specialization", nil];


    
    /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
    
    // Blurred with UIImage+ImageEffects
    
    
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];


    [self.view addSubview:blurredBgImage];

    UIButton* searchBtn=(id)sender;

    MedRepDoctorFilterViewController * popOverVC=[[MedRepDoctorFilterViewController alloc]init];
    
    popOverVC.filteredDoctorDelegate=self;
    
    if (previousFilteredParameters.count>0) {
        popOverVC.previouslyFilteredContent=previousFilteredParameters;
   
    }
    
   
    popOverVC.contentArray=unfilteredDoctorObjects;

    popOverVC.filterNavController=self.navigationController;
    popOverVC.filterTitle=@"Filter";
    popOverVC.filterArray=filterByArray;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    
    
    
    filterPopOverController=nil;
    filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    filterPopOverController.delegate=self;
    popOverVC.filterPopOverController=filterPopOverController;
    
    [filterPopOverController setPopoverContentSize:CGSizeMake(366, 473) animated:YES];
    
    
    CGRect btnFrame=searchBtn.frame;
    
    btnFrame.origin.x=btnFrame.origin.x+3;
    
   // self.view.alpha=0.70;
    
    NSLog(@"check frame %@", NSStringFromCGRect(self.medRepFilterButton.frame));
    

    CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];

    [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)selectedStringValue:(NSIndexPath *)indexPath
{
    //adding a new doctor cell so increment indexpath here

    if ([popUpString isEqualToString:@"Filter"]) {
        
        NSLog(@"selected index %@", [filterByArray objectAtIndex:indexPath.row]);
    }
    
    if (filterPopOverController)
    {
        [filterPopOverController dismissPopoverAnimated:YES];
        filterPopOverController = nil;
        filterPopOverController.delegate=nil;
    }
    stringPopOverCustomController.delegate=nil;
    stringPopOverCustomController=nil;
}

#pragma mark PopOver Controller delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

#pragma mark Filtered Data from Filter method

-(void)filteredParameters:(NSMutableDictionary*)filterParameters
{
    self.view.alpha=1.0;
    
    [self addKeyBoardObserver];
    if ([self.view.subviews containsObject:blurredBgImage]) {
        [blurredBgImage removeFromSuperview];

    }

    previousFilteredParameters=filterParameters;
}

-(void)filteredDoctorDelegate:(NSMutableArray*)filteredArray
{
    isFiltered=YES;
    
    
    NSLog(@"filtered content is %@", filteredArray);
    
    NSLog(@"doctor array before filter reset %@", filteredDoctors);
    
    [blurredBgImage removeFromSuperview];

    
    [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    
    self.view.alpha=1.0;

    
    if (isSearching==YES) {
        
        filteredDoctors=filteredArray;
    }
    else {
        
        doctorsArray=filteredArray;
    }
    
    
    [doctorsTblView reloadData];
    if ([doctorsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        
        [doctorsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];
    
    [doctorsTblView.delegate tableView:doctorsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}


-(void)CustomPopOverCancelDelegate
{
    self.view.alpha=1.0;
    [self addKeyBoardObserver];
    
    [blurredBgImage removeFromSuperview];
    
    
     [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    doctorsArray=unfilteredDoctorObjects;
    
    [doctorsTblView reloadData];
    
    if ([doctorsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        
        [doctorsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];

    [doctorsTblView.delegate tableView:doctorsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

#pragma mark Mail Composer Delegate


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    [self addKeyBoardObserver];
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)viewDidAppear:(BOOL)animated
{
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;

    lastVisitedOnLbl.textColor=LastVisitedOnTextColor;
    lastVisitedAtLbl.textColor=LastVisitedAtTextColor;
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(66.0/255.0) green:(126.0/255.0) blue:(196.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
}



#pragma mark Blur Effect methods

- (UIImage *)takeSnapshotOfView:(UIView *)view
{
    CGFloat reductionFactor = 1;
    UIGraphicsBeginImageContext(CGSizeMake(view.frame.size.width/reductionFactor, view.frame.size.height/reductionFactor));
    [view drawViewHierarchyInRect:CGRectMake(0, 0, view.frame.size.width/reductionFactor, view.frame.size.height/reductionFactor) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)blurWithImageEffects:(UIImage *)image
{
    return [image applyBlurWithRadius:2 tintColor:[UIColor colorWithWhite:0 alpha:0] saturationDeltaFactor:1 maskImage:nil];
}

- (UIImage *)blurWithCoreImage:(UIImage *)sourceImage
{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@30 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.view.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, self.view.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
    CGContextFillRect(outputContext, self.view.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}

- (UIImage *)blurWithGPUImage:(UIImage *)sourceImage
{
    GPUImageGaussianBlurFilter *blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = 30.0;
    
    return [blurFilter imageByFilteringImage: sourceImage];
}

- (UIView *)createHeaderView
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    headerView.backgroundColor = [UIColor colorWithRed:229/255.0 green:39/255.0 blue:34/255.0 alpha:0.6];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 40)];
    title.text = @"Dynamic Blur Demo";
    title.textColor = [UIColor colorWithWhite:1 alpha:1];
    [title setFont:[UIFont fontWithName:@"HelveticaNeue" size:20]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [headerView addSubview:title];
    
    return headerView;
}

- (UIView *)createContentView
{
    UIView *contentView = [[UIView alloc] initWithFrame:self.view.frame];
    
    UIImageView *contentImage = [[UIImageView alloc] initWithFrame:contentView.frame];
    contentImage.image = [UIImage imageNamed:@"demo-bg"];
    [contentView addSubview:contentImage];
    
    UIView *metaViewContainer = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 65, 335, 130, 130)];
    metaViewContainer.backgroundColor = [UIColor whiteColor];
    metaViewContainer.layer.cornerRadius = 65;
    [contentView addSubview:metaViewContainer];
    
    UILabel *photoTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 54, 130, 18)];
    photoTitle.text = @"Peach Garden";
    [photoTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
    [photoTitle setTextAlignment:NSTextAlignmentCenter];
    photoTitle.textColor = [UIColor colorWithWhite:0.4 alpha:1];
    [metaViewContainer addSubview:photoTitle];
    
    UILabel *photographer = [[UILabel alloc] initWithFrame:CGRectMake(0, 72, 130, 9)];
    photographer.text = @"by Cas Cornelissen";
    [photographer setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:9]];
    [photographer setTextAlignment:NSTextAlignmentCenter];
    photographer.textColor = [UIColor colorWithWhite:0.4 alpha:1];
    [metaViewContainer addSubview:photographer];
    
    return contentView;
}

#pragma mark Button Action
-(void)btnTask
{
    [self.view endEditing:YES];

    NSString* locationType = [[selectedVisitDetailsDict valueForKey:@"locations"]valueForKey:@"Location_Type"];
    @try {
        if ([locationType isEqualToString:@"D"])
        {
            currentVisitTasksArray=[MedRepQueries fetchTaskObjectsforLocationID:@"" andDoctorID:[selectedDoctorDict valueForKey:@"Doctor_ID"]];
        }
    }
    @catch (NSException *exception) {
        
    }
    
    if (doctorsArray.count>0)
    {
        popOverController=nil;
        
        MedRepEdetailTaskViewController * edetailTaskVC = [[MedRepEdetailTaskViewController alloc]init];
        edetailTaskVC.visitDetailsDict = selectedDoctorDict;
        edetailTaskVC.delegate=self;
        edetailTaskVC.dashBoardViewing = YES;
        edetailTaskVC.tasksArray = currentVisitTasksArray;
        
        UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
        
        popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
        [popOverController setDelegate:self];
        
        [popOverController setPopoverContentSize:CGSizeMake(376, 600) animated:YES];
        edetailTaskVC.tasksPopOverController=popOverController;
        
        
        [popOverController presentPopoverFromRect:[taskPopOverButton.sourceButton bounds]
                                                inView:taskPopOverButton.sourceButton
                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                              animated:YES];
    }
    
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:kDataUnavailable andMessage:@"Task data unavailable" withController:self];
    }
}
-(void)btnNote
{
    [self.view endEditing:YES];

    if (doctorsArray.count>0)
    {
        popOverController=nil;
        
        [[selectedVisitDetailsDict valueForKey:@"locations"] setValue:@"" forKey:@"Location_ID"];
        
        MedRepEDetailNotesViewController * edetailTaskVC=[[MedRepEDetailNotesViewController alloc]init];
        edetailTaskVC.visitDetailsDict=selectedVisitDetailsDict;
        edetailTaskVC.dashBoardViewing=YES;
        
        UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
        popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
        [popOverController setDelegate:self];
        [popOverController setPopoverContentSize:CGSizeMake(376, 550) animated:YES];
        
        edetailTaskVC.notesPopOverController=popOverController;
        
        
        [popOverController presentPopoverFromRect:[notePopOverButton.sourceButton bounds] inView:notePopOverButton.sourceButton
                         permittedArrowDirections:UIPopoverArrowDirectionAny
                                         animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:kDataUnavailable andMessage:@"Notes data unavailable" withController:self];
    }
}
#pragma Mark Task popover controller delegate

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray
{
    currentVisitTasksArray = tasksArray;
    
    if (tasksArray.count>0)
    {
        NSPredicate * updatedTasksPredicate = [NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
        NSMutableArray *updatedTasksArray = [[tasksArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
        NSLog(@"updated tasks are %@", updatedTasksArray);
        
        if (updatedTasksArray.count>0)
        {
            NSString *documentDir;
            if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                documentDir = [paths objectAtIndex:0];
            }
            else
            {
                NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
                documentDir = [filePath path] ;
            }
            
            FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
            [database open];
            BOOL updatedTasksSuccessfully;
            
            NSString* assignedTo=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
            NSString* userID=[[SWDefaults userProfile]valueForKey:@"User_ID"];
            NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++)
            {
                VisitTask * currentTask=[updatedTasksArray objectAtIndex:i];
                @try {
 
                    updatedTasksSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Tasks (Task_ID,Title, Description,Category,Priority,Start_Time, Status,Assigned_To,Emp_ID, Location_ID,Show_Reminder,IS_Active,Is_deleted,Created_At,Created_By,Doctor_ID,Contact_ID,End_Time,Last_Updated_At,Last_Updated_By,Custom_Attribute_1)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", currentTask.Task_ID, currentTask.Title==nil?@"":currentTask.Title, currentTask.Description==nil?@"":currentTask.Description, currentTask.Category==nil?@"":currentTask.Category, currentTask.Priority==nil?@"":currentTask.Priority, currentTask.Start_Time, currentTask.Status==nil?@"":currentTask.Status,assignedTo, assignedTo, [NSString isEmpty:currentTask.Location_ID]?[NSNull null]:currentTask.Location_ID, @"N",@"Y",@"N", currentTask.Start_Time, createdBy, [NSString isEmpty:[selectedVisitDetailsDict valueForKey:@"Doctor_ID"]]?[NSNull null]: [selectedVisitDetailsDict valueForKey:@"Doctor_ID"], [NSNull null] , currentTask.End_Time, currentTask.Start_Time, userID, @"IPAD",nil];
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
            }
            
            //after updating in visit addl info update the status in task table as well.
            
            BOOL updatetaskStatus=NO;
            
            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [formatterTime setLocale:usLocaleq];
            NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++)
            {
                VisitTask * task=[updatedTasksArray objectAtIndex:i];
                @try
                {
                    updatetaskStatus =  [database executeUpdate:@"Update  PHX_TBL_Tasks set Status = ?, Last_Updated_At=?, Last_Updated_By=?  where Task_ID='%@'",task.Status,createdAt,createdBy,task.Task_ID, nil];
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    [database close];
                }
            }
        }
    }
    [self updateStatusforTasksandNotes];
}

#pragma mark Task and Notes Status Update
-(void)updateStatusforTasksandNotes
{
    @try {
        NSString* locationType = [[selectedVisitDetailsDict valueForKey:@"locations"] valueForKey:@"Location_Type"];
        NSString* locationID = [[selectedVisitDetailsDict valueForKey:@"locations"] valueForKey:@"Location_ID"];
        NSInteger notesCount = 0;
        
        if ([locationType isEqualToString:@"D"])
        {
            NSString* doctorID = [selectedVisitDetailsDict valueForKey:@"Doctor_ID"];
            NSMutableArray* taskCount = [MedRepQueries fetchTasksCountforLocationID:@"" andDoctorID:doctorID];
            
            NSLog(@"task array is %@", taskCount);
            
            if (taskCount.count>0)
            {
                for (NSInteger i=0; i<taskCount.count; i++)
                {
                    NSMutableDictionary * currentDict=[taskCount objectAtIndex:i];
                    NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSDate *dateFromString = [[NSDate alloc] init];
                    dateFromString = [dateFormatter dateFromString:taskDueDate];
                    
                    BOOL testDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                    if (testDate==YES)
                    {
                        NSLog(@"RED TASKS OVER DUE");
                        [selectedVisitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                    }
                    else
                    {
                        [selectedVisitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                    }
                }
            }
            
            notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andDoctorID:doctorID];
            NSLog(@"task count : %@ notes count : %ld",taskCount,(long)notesCount);
            
            if (taskCount>0)
            {
                [selectedVisitDetailsDict setValue:@"YES" forKey:@"Tasks_Available"];
            }
            else
            {
                [selectedVisitDetailsDict setValue:@"NO" forKey:@"Tasks_Available"];
            }
            if (notesCount>0)
            {
                [selectedVisitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
            }
            else
            {
                [selectedVisitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    customerViewTopConstraint.constant = 8;
}
-(void)showNoSelectionView
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    customerViewTopConstraint.constant = 1000;
}

- (IBAction)DoctorClassificationButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    MedrepdoctorClassificationPopOverViewController * popOverVC=[[MedrepdoctorClassificationPopOverViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(310, 250);
    popover.delegate = self;
    popover.sourceView = ClassificationDetailsButton.superview;
    popover.sourceRect = ClassificationDetailsButton.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}
-(void)showLastVisitDetails:(NSString*)selectedDoctorID
{
     lastVisitDetailsDict=[MedRepQueries fetchLastVisitDetails:selectedDoctorID];
    NSLog(@"last visit details data is %@",lastVisitDetailsDict);
    
    lastVisitedAtLbl.text=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Last_Visited_At"]];
    
    NSString* lastVisitOnString=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Last_Visited_On"]];
    if (![NSString isEmpty:lastVisitOnString]) {
        NSString* refinedLastVisitedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:lastVisitOnString];
        lastVisitedOnLbl.text=[NSString getValidStringValue:refinedLastVisitedDate];
    }
    
}

- (IBAction)lastVisitDetailsButtonTapped:(id)sender {
    
    [self.view endEditing:YES];

    if([[AppControl retrieveSingleton].FM_ENABLE_VISIT_HISTORY_REPORT isEqualToString:KAppControlsYESCode]){
        
    FMVisitHistoryViewController * visitHistoryVC =  [[FMVisitHistoryViewController alloc]init];
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=@"sync_MC_ExecGetVisitHistory";
    NSString* username = [SWDefaults username];
    NSString* password = [SWDefaults password];
    NSString* userID = [[SWDefaults userProfile]valueForKey:@"SalesRep_ID"];
    NSMutableDictionary* paramatersDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:userID,@"EmpID",@"D",@"Type",doctor.Doctor_ID,@"DoctorContactID", nil];
    NSArray* valuesArray = paramatersDict.allValues;
    NSArray* keysArray = paramatersDict.allKeys;

                                           
    for (NSInteger i=0; i<paramatersDict.count; i++) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[keysArray objectAtIndex:i]];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[valuesArray objectAtIndex:i]];
        
    }
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"ORDER_HISTORY_PARAMETERS_DICT"];
    NSString *requestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_Doctor_VisitHistory",[testServerDict stringForKey:@"name"],strName,strParams];
        
    NSDictionary* orderHistoryDict = [[NSDictionary alloc]initWithObjectsAndKeys:url,@"SalesWorx_Order_History_URL",requestString,@"SalesWorx_Order_History_Paramater_String", nil];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:orderHistoryDict] forKey:@"ORDER_HISTORY_PARAMETERS_DICT"];

    [self.navigationController pushViewController:visitHistoryVC animated:YES];
    }
    else{
    
    MedRepDoctorsLastVisitDetailsPopOverViewController * popOverVC=[[MedRepDoctorsLastVisitDetailsPopOverViewController alloc]init];
    popOverVC.lastVisitDetailsDict=lastVisitDetailsDict;
    popOverVC.isVisitScreen = false;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(410, 380);
    popover.delegate = self;
    popover.sourceView = lastVisitDetailsButton.superview;
    popover.sourceRect = lastVisitDetailsButton.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
    [self presentViewController:nav animated:YES completion:^{
        
    }];
    }
}

@end
