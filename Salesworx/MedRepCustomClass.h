//
//  MedRepCustomClass.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 1/31/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MedRepCustomClass : NSObject

@end


@interface Locations : NSObject

@property(strong,nonatomic) NSString * Location_ID;
@property(strong,nonatomic) NSString * Location_Name;
@property(strong,nonatomic) NSString * Location_Type;
@property(strong,nonatomic) NSString * Location_Code;
@property(strong,nonatomic) NSString * Address_1;
@property(strong,nonatomic) NSString * Address_2;
@property(strong,nonatomic) NSString * Address_3;
@property(strong,nonatomic) NSString * City;
@property(strong,nonatomic) NSString * State;
@property(strong,nonatomic) NSString * Email;
@property(strong,nonatomic) NSString * Phone;
@property(strong,nonatomic) NSString * Postal_Code;
@property(strong,nonatomic) NSString * Latitude;
@property(strong,nonatomic) NSString * Longitude;
@property(strong,nonatomic) NSString * Classification_2;
@property(strong,nonatomic) NSString * Classification_3;
@property(strong,nonatomic) NSString * Custom_Attribute_1;
@property(strong,nonatomic) NSString * Custom_Attribute_2;
@property(strong,nonatomic) NSString * Custom_Attribute_3;
@property(strong,nonatomic) NSString * Custom_Attribute_4;
@property(nonatomic) BOOL  isActive;
@property(nonatomic) BOOL isDeleted;
@property(strong,nonatomic) NSString * Created_At;


@end

@interface Doctors : NSObject

@property(strong,nonatomic)NSString* Doctor_ID;
@property(strong,nonatomic)NSString* Doctor_Name;
@property(strong,nonatomic)NSString* Display_Name;
@property(strong,nonatomic)NSString* Doctor_Code;
@property(strong,nonatomic) NSString * Classification_1;
@property(strong,nonatomic) NSString * Classification_2;
@property(strong,nonatomic) NSString * Classification_3;
@property(strong,nonatomic) NSString * Custom_Attribute_1;
@property(strong,nonatomic) NSString * Custom_Attribute_2;
@property(strong,nonatomic) NSString * Phone;
@property(strong,nonatomic) NSString * Email;
@property(strong,nonatomic) Locations* locations;
@property(strong, nonatomic) NSString *isActive;
@property(strong, nonatomic) NSString *isDeleted;
@property(strong,nonatomic) NSString * Created_At;
@property(strong,nonatomic) NSString * Specialization;

@property(strong,nonatomic) NSString *adoptionStyle;
@property(strong,nonatomic) NSString *personalityStyle;

@property(strong,nonatomic) NSMutableArray* doctorLocationsArray;



@end

@interface Visit : NSObject

@property(strong,nonatomic) NSMutableArray* updatedTaskIDArray;

@property(strong,nonatomic) NSMutableArray* updatedNotesIDArray;

@end


@interface SampleProductLotClass : NSObject
    @property (strong,nonatomic) NSString* productName;
    @property (strong,nonatomic) NSString* productLotNumber;
    @property (strong,nonatomic) NSString* productAvailbleQty;
    @property (strong,nonatomic) NSString* ExpiryDate;
    @property (strong,nonatomic) NSString* QuantityEntered;
    @property (strong,nonatomic) NSString* Product_Code;
    @property (strong,nonatomic) NSString* Used_Qty;

@end

@interface SampleProductClass : NSObject
@property (strong,nonatomic) NSString* productName;

@property (strong,nonatomic) NSString* productLotNumber;
@property (strong,nonatomic) NSString* productAvailbleQty;
@property (strong,nonatomic) NSString* ExpiryDate;
@property (strong,nonatomic) NSString* QuantityEntered;
@property (strong,nonatomic) NSString* Product_Code;
@property (strong,nonatomic) NSString* Used_Qty;
@property (strong,nonatomic) NSString* DisplayExpiryDate;
@property (strong,nonatomic) NSString* ProductId;
@property(nonatomic) NSInteger samplesGiven;
@property(nonatomic) NSInteger sellOutGiven;
@property (strong,nonatomic) NSString* ItemType;


@end


@interface PharmacyContact : NSObject
@property (strong,nonatomic) NSString* locationName;
@property (strong,nonatomic) NSString* locationID;
@property (strong,nonatomic) NSString* contactID;
@property (strong,nonatomic) NSString* contactTitle;
@property (strong,nonatomic) NSString* contactName;
@property (strong,nonatomic) NSString* contactEmail;
@property (strong,nonatomic) NSString* contactTelephone;


@end


@interface Contact : NSObject

@property (strong,nonatomic) NSString* Contact_Name;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Email;
@property (strong,nonatomic) NSString* City;
@property (strong,nonatomic) NSString* Phone;
@end


@interface Calls : NSObject

@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Call_Started_At;
@property (strong,nonatomic) NSString* Contact_Name;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Demo_Plan_ID;

@end

@interface DemoPlan : NSObject

@property (strong,nonatomic) NSString* Demo_Plan_ID;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Valid_From;
@property (strong,nonatomic) NSString* Valid_To;
@property (strong,nonatomic) NSString* Is_Active;
@property (strong,nonatomic) NSString* Is_Deleted;
@property (strong,nonatomic) NSString* Specialization;


@end


@interface FieldMarketingPresentation : NSObject
@property (strong,nonatomic) NSString* presentationID;
@property (strong,nonatomic) NSString* isSelected;
@property (strong,nonatomic) NSString* demoPlanID;

@property (strong,nonatomic) NSString* presentationName;
@property(nonatomic) BOOL isDefaultPresentation;
@property (strong,nonatomic) NSMutableArray* presentationContentArray;
@end

@interface VisitDetails : NSObject

@property (strong,nonatomic) NSString* Accompanied_By;
@property (strong,nonatomic) NSString* ActualPlannedVisitID;
@property (strong,nonatomic) NSString* Actual_Visit_ID;
@property (strong,nonatomic) NSString* Address;
@property (strong,nonatomic) NSString* Class;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Created_At;
@property(strong,nonatomic) NSString * Contact_Name;

@property (strong,nonatomic) NSString* Created_By;
@property (strong,nonatomic) NSString* Demo_Plan_ID;
@property (strong,nonatomic) NSString* Doctor_Class;
@property (strong,nonatomic) NSString* Doctor_ID;
@property (strong,nonatomic) NSString* Doctor_Name;
@property (strong,nonatomic) NSString* EMP_ID;
@property (strong,nonatomic) NSString* Last_Visited_At;

@property (strong,nonatomic) NSString* Latitude;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Location_Name;
@property (strong,nonatomic) NSString* Location_Type;
@property (strong,nonatomic) NSString* Longitude;
@property (strong,nonatomic) NSString* Notes_Available;
@property (strong,nonatomic) NSString* OTC;

@property (strong,nonatomic) NSString* Objective;
@property (strong,nonatomic) NSString* Pharmacy_Contact_Name;
@property (strong,nonatomic) NSString* Planned_Visit_ID;
@property (strong,nonatomic) NSString* Selected_Index;
@property (strong,nonatomic) NSString* Specialization;
@property (strong,nonatomic) NSString* TASK_STATUS;
@property (strong,nonatomic) NSString* Tasks_Available;


@property (strong,nonatomic) NSString* Trade_Channel;
@property (strong,nonatomic) NSString* Visit_Date;
@property (strong,nonatomic) NSString* Visit_Rating;
@property (strong,nonatomic) NSString* Visit_Status;
@property (strong,nonatomic) NSString* Visit_Type;

@property (strong,nonatomic) NSString* VisitEndTime;


@property(strong,nonatomic) NSMutableArray* taskArray;
@property(strong,nonatomic) NSMutableArray* samplesArray;

@property(strong,nonatomic) NSMutableArray* notesArray;
@property(strong,nonatomic) NSMutableArray * locationContactsArray;

@property(strong,nonatomic) Contact * selectedContact;
@property(strong,nonatomic) Doctors * selectedDoctor;
@property(strong,nonatomic) NSMutableArray* doctorsforLocationArray;
@property(strong,nonatomic) DemoPlan *selectedDemoPlan;
@property(strong,nonatomic) NSString* currentCallStartTime;


@property(strong,nonatomic) NSString* onSiteExceptionReason;

@property(strong,nonatomic) NSMutableArray* presentationArray;
@property(strong,nonatomic) FieldMarketingPresentation * fieldMarketingPresentation;
@property(strong,nonatomic) NSMutableArray* fieldMarketingPresentationsArray;

@property(strong,nonatomic) NSMutableArray* coachSurveyArray;
@property(strong,nonatomic) NSMutableArray* coachSurveyConfirmationArray;


@end


@interface VisitTask : NSObject




@property (strong,nonatomic) NSString* Task_ID;
@property (strong,nonatomic) NSString* Title;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Category;
@property (strong,nonatomic) NSString* Priority;
@property (strong,nonatomic) NSString* Start_Time;
@property (strong,nonatomic) NSString* Status;
@property (strong,nonatomic) NSString* Assigned_To;
@property (strong,nonatomic) NSString* Emp_ID;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Show_Reminder;
@property (strong,nonatomic) NSString* IS_Active;
@property (strong,nonatomic) NSString* Is_deleted;
@property (strong,nonatomic) NSString* Doctor_ID;
@property (strong,nonatomic) NSString* Doctor_Name;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Contact_Name;
@property (strong,nonatomic) NSString* Task_Location_Type;
@property (strong,nonatomic) NSString* End_Time;
@property (strong,nonatomic) NSString* Last_Updated_At;
@property (strong,nonatomic) NSString* Last_Updated_By;
@property (strong,nonatomic) NSString* Created_At;
@property (strong,nonatomic) NSString* isUpdated;





@end



@interface VisitNotes : NSObject

@property (strong,nonatomic) NSString* Note_ID;
@property (strong,nonatomic) NSString* Title;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Category;
@property (strong,nonatomic) NSString* Emp_ID;
@property (strong,nonatomic) NSString* Visit_ID;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Doctor_ID;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Created_At;
@property (strong,nonatomic) NSString* Created_By;
@property (strong,nonatomic) NSString* Last_Updated_By;
@property (strong,nonatomic) NSString* Last_Updated_At;
@property (strong,nonatomic) NSString* isUpdated;



@end


@interface SyncLoctions : NSObject

@property(strong,nonatomic)NSString * name;
@property(strong,nonatomic)NSString * url;
@property(strong,nonatomic)NSString * seq;
@end

@interface TBLAppCode : NSObject

@property(strong,nonatomic)NSString * Code_Type;
@property(strong,nonatomic)NSString * Code_Value;
@property(strong,nonatomic)NSString * Code_Description;
@end

@interface Holiday : NSObject
@property(strong,nonatomic)NSString * holidayName;
@property(strong,nonatomic)NSString * holidayDate;
@property(strong,nonatomic)NSString * holidayCode;

@end

@interface CoachSurveyDropdownTypeQuestion : NSObject
@property(strong,nonatomic)NSString * isUpdated;
@property(strong,nonatomic)NSString * rating;
@property(strong,nonatomic)NSString * remarks;
@end

