//
//  ProductStockViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

//#import "ProductStockViewController.h"
//#import "StockInfoTableViewController.h"
//
//@interface ProductStockViewController ()
//
//@end
//
//@implementation ProductStockViewController
//
//
//@synthesize target,orderQuantity;
//@synthesize action,stockAllocation,row,savedArray,productStockVC,StockInfoTableArray;
//
//@synthesize delegate;
//
//- (id)initWithProduct:(NSDictionary *)p {
//    self = [super init];
//    
//    if (self) {
//        product=[NSDictionary dictionaryWithDictionary:p];
//        [self setTitle:NSLocalizedString(@"Stock Info", nil)];
//    }
//    
//    return self;
//}
//
//
//
//
////- (void)dealloc
////{
////    NSLog(@"Dealloc %@",self.title);
////    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////}
//
//
//-(void)viewWillAppear:(BOOL)animated
//{
//    
//    NSLog(@"saved array contents %@", [savedArray description]);
//    
//    
//    
////    BOOL  sample=[[NSUserDefaults standardUserDefaults]boolForKey:@"ValueChanged"];
////    //
////    if (sample==YES) {
////        [stockAllocation removeObject:row];
////        
////        
////        
////    }
//    
//    BOOL sampleBool=YES;
//    
//}
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//     stockAllocation = [NSMutableArray array];
//    
//    //trying through user defaults
//    
//    NSMutableArray * getStoclAllocationArray=[[[NSUserDefaults standardUserDefaults]objectForKey:@"SavedArray"]mutableCopy];
//    
//    NSLog(@"getting array from user defaults %@",[getStoclAllocationArray description] );
//    
//    
//    NSMutableDictionary * getSavedDict=[[[NSUserDefaults standardUserDefaults]objectForKey:@"SavedDictionary"]mutableCopy];
//    
//    NSLog(@"Getting saved dictionary from user defaults %@",getSavedDict);
//    
////
////    
////    
////    
//    BOOL  sample=[[NSUserDefaults standardUserDefaults]boolForKey:@"ValueChanged"];
//    //
//              if (sample==YES) {
//                  
//                  NSLog(@"Stock allocation before removing %@", [getStoclAllocationArray description]);
//
//             
//                       [getStoclAllocationArray removeAllObjects];
//                  
//                  
//                  stockAllocation=[NSMutableArray arrayWithArray:getStoclAllocationArray];
//                  
//                  //row =[NSMutableDictionary dictionaryWithDictionary:getSavedDict];
////
////                 // [stockAllocation removeObject:row];
////                  
////                 // [row removeAllObjects];
////                  
////                      
////                  NSLog(@"stock allocation after removing %@", [stockAllocation description]);
//    
////
////             
////                  
////                  
////              
////                  
////                  
////                  //[getStoclAllocationArray removeObject:getSavedDict];
////                  
////                  
////                   NSLog(@"Stock allocation after removing %@", [getStoclAllocationArray description]);
////                  
////                 // [getStoclAllocationArray removeObject:getSavedDict];
////                  
////            
////                  
////                  row=[NSMutableDictionary dictionaryWithDictionary:getSavedDict];
////                  
////                  NSLog(@"row from did load %@", [row description]);
////    
////    
//        }
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
// 
//    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
//        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
//        self.navigationController.navigationBar.translucent = NO;
//        
//        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
//        self.navigationController.toolbar.translucent = NO;
//        
//        self.extendedLayoutIncludesOpaqueBars = TRUE;
//        self.extendedLayoutIncludesOpaqueBars = NO;
//        self.edgesForExtendedLayout = UIRectEdgeBottom;
//        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//        
//        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                        [UIColor whiteColor],NSForegroundColorAttributeName,
//                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
//        
//        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
//        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
//        
//        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
//        [self setNeedsStatusBarAppearanceUpdate];
//        
//    }
//    else
//    {
//        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
//    }
//   [Flurry logEvent:@"Product Stock View"];
//    //[Crittercism leaveBreadcrumb:@"<Product Stock View>"];
//
//    stockView=[[ProductStockView alloc] initWithProduct:product ];
//    [stockView setFrame:self.view.bounds];
//    [stockView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
//    [stockView setEditMode:YES];
//    [stockView setDelegate:self];
//    [stockView loadStock];
//    
//    [self.view addSubview:stockView];
//    AppControl *appControl = [AppControl retrieveSingleton];
//
//    isLotSelection = appControl.ALLOW_LOT_SELECTION;
//   // if([[SWDefaults appControl] count]!=0)//ALLO_LOT_SELECTION
//    if(![isLotSelection isEqualToString:@"Y"])
//    {
//        stockView.gridView.userInteractionEnabled = NO;
//    }
//    //Labels
//    countLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
//    [countLabel setText:@""];
//    [countLabel setTextAlignment:NSTextAlignmentCenter];
//    [countLabel setBackgroundColor:[UIColor clearColor]];
//    [countLabel setTextColor:[UIColor whiteColor]];
//    [countLabel setFont:LightFontOfSize(14.0f)];
//    [countLabel setShadowColor:[UIColor blackColor]];
//    [countLabel setShadowOffset:CGSizeMake(0, -1)];
//    
//    orderedLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
//    [orderedLabel setText:@""];
//    [orderedLabel setTextAlignment:NSTextAlignmentCenter];
//    [orderedLabel setBackgroundColor:[UIColor clearColor]];
//    [orderedLabel setTextColor:[UIColor whiteColor]];
//    [orderedLabel setFont:LightFontOfSize(14.0f)];
//    [orderedLabel setShadowColor:[UIColor blackColor]];
//    [orderedLabel setShadowOffset:CGSizeMake(0, -1)];
//
//    allocatedLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
//    [allocatedLabel setText:@""];
//    [allocatedLabel setTextAlignment:NSTextAlignmentCenter];
//    [allocatedLabel setBackgroundColor:[UIColor clearColor]];
//    [allocatedLabel setTextColor:[UIColor whiteColor]];
//    [allocatedLabel setFont:LightFontOfSize(14.0f)];
//    [allocatedLabel setShadowColor:[UIColor blackColor]];
//    [allocatedLabel setShadowOffset:CGSizeMake(0, -1)];
//    
//
//    
//    //priceString = [NSString stringWithFormat:priceString, [product currencyStringForKey:@"Net_Price"], [product currencyStringForKey:@"List_Price"]];
//    
//    footerView = [[GroupSectionFooterView alloc] initWithFrame:CGRectMake(0, 0, stockView.bounds.size.width, 72)] ;
//    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
//    [footerView.titleLabel setText:priceString];
//    [stockView.gridView.tableView setTableFooterView:footerView];
//
//    
//    filterButton = [[UIButton alloc] init] ;
//    //filterButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
//    if([language isEqualToString:@"ar"])
//    {
//        [filterButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//    }
//    else
//    {
//        [filterButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//    }
//    [filterButton setFrame:CGRectMake(self.view.frame.size.width-120,12, 100, 32)];
//    [filterButton addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
//    [filterButton bringSubviewToFront:self.view];
//    [filterButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
//    filterButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
//    filterButton.enabled = NO;
//    [self.view addSubview:filterButton];
//    
//    // [self.gridView.tableView setTableHeaderView:self.customerHeaderView];
//    //[self addSubview:self.customerHeaderView];
//
//
//}
//
//
//
//- (void)setupToolbar
//{
//
////    [countLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods]];
////    [orderedLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Order Quantity", nil), orderQuantity]];
////    [allocatedLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity]];
////
////    UIBarButtonItem *totalLabelButton ;
////    UIBarButtonItem *orderLabelButton;
////    UIBarButtonItem *allocatedLabelButton ;
////    
////    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
////    totalLabelButton = [[UIBarButtonItem alloc] init];
////    orderLabelButton = [[UIBarButtonItem alloc] init];
////    allocatedLabelButton = [[UIBarButtonItem alloc] init];
////    
////    totalLabelButton = [UIBarButtonItem labelButtonWithLabel:countLabel];
////    orderLabelButton = [UIBarButtonItem labelButtonWithLabel:orderedLabel];
////    allocatedLabelButton = [UIBarButtonItem labelButtonWithLabel:allocatedLabel];
////    totalLabelButton.title = countLabel.text;
////    orderLabelButton.title = orderedLabel.text;
////    allocatedLabelButton.title = allocatedLabel.text;
////    [self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, orderLabelButton, flexibleSpace, allocatedLabelButton, flexibleSpace, totalLabelButton, flexibleSpace, nil]];
////    flexibleSpace=nil;
////    totalLabelButton=nil;
////    orderLabelButton=nil;
////    allocatedLabelButton=nil;
//}
//
//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    
//    [self setupToolbar];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self 
//                                             selector:@selector(keyboardWillShow:) 
//                                                 name:UIKeyboardWillShowNotification 
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self 
//                                             selector:@selector(keyboardWillHide:) 
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    
//    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Back" 
//                                                                                style:UIBarButtonItemStylePlain
//                                                                               target:self
//                                                                               action:@selector(closeOrder)] ];
//
//    
//}
//- (void)closeOrder {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    
//    [self.navigationController setToolbarHidden:YES animated:YES];
//    
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillShowNotification
//                                                  object:nil];
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//    
//
//}
//
//
//
//
//
//
//- (void)save:(id)sender {
//    [self.view endEditing:YES];
//  
//    for (row in stockView.items) {
//        
//        if ([row stringForKey:@"Allocated"].length > 0) {
//           [stockAllocation addObject:row];
//           
//            
//            NSLog(@"row from saved %@", [row description]);
//            
//            savedArray=[[NSMutableArray alloc]initWithArray:stockAllocation];
//            
//            NSLog(@"Saved array %@", savedArray);
//            
//            
////            NSUserDefaults* arrayDefaullts=[NSUserDefaults standardUserDefaults];
////            
////            [arrayDefaullts setObject:savedArray forKey:@"SavedArray"];
//            
//            
//            
//            NSUserDefaults* arrayDefaullts=[NSUserDefaults standardUserDefaults];
//            
//            [arrayDefaullts setObject:stockAllocation forKey:@"SavedArray"];
//            
//            
//            NSUserDefaults* dictionaryDefaullts=[NSUserDefaults standardUserDefaults];
//            
//            [dictionaryDefaullts setObject:row forKey:@"SavedDictionary"];
//            
//            
//            
////this code removing everything
//            
////            BOOL  sample=[[NSUserDefaults standardUserDefaults]boolForKey:@"ValueChanged"];
//////            
////            if (sample==YES) {
////                [stockAllocation removeObject:row];
////          
////            
////         
////        }
//            
//              NSLog(@"stock  from saved n deleted %@", stockAllocation);
//            
//            
//           
//            
//            //[stockAllocation removeLastObject];
//
//            
//            
//            
//            //only one value getting removed
//            
////           BOOL  sample1=[[NSUserDefaults standardUserDefaults]boolForKey:@"ValueChanged"];
////            
////            if (sample1==YES) {
////                
////                //for loop removes everything
////                for (int i=0; i< [stockAllocation count]; i++) {
////                    [stockAllocation removeObject:row ];
////                }
////                
////                //[stockAllocation removeObject:row  ];
////                
////               // NSLog(@"row deletion %@",[row stringForKey:@"Allocated"] );
////                //[[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"ValueChanged"];
////                
////                NSLog(@"test stock allocation data %@", [stockAllocation description]);
////                
////                
////                
////            }
//            
//            
//            
//        }
//    }
//    [stockAllocation addObject:row];
////
////    
////    
//    
//    
//    
//    
//    NSMutableDictionary *p = [NSMutableDictionary dictionaryWithDictionary:product];
//    [p setValue:stockAllocation forKey:@"StockAllocation"];
//    
//    NSLog(@"p dic has %@", [p description]);
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
//    [self.target performSelector:self.action withObject:p];
//#pragma clang diagnostic pop
//
//    if (delegate) {
//        [delegate didTapSaveButton];
//    }
//    
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//}
//
//
//-(void)SaveDataDB
//{
//    StockInfoTableViewController * stockInfoTableVC=[[StockInfoTableViewController alloc]init];
//    
//       
//    NSLog(@"array here in product stock is %@", [stockInfoTableVC.stockArray description]);
//
//    
//    NSMutableDictionary *p = [NSMutableDictionary dictionaryWithDictionary:product];
//    [p setValue:stockAllocation forKey:@"StockAllocation"];
//    
//    NSLog(@"p dic has %@", [p description]);
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
//    [self.target performSelector:self.action withObject:p];
//}
//
//
//
//- (void)updateAllocation {
//     orderedQuantity = 0;
//    //BOOL shouldDisplaySaveButton = NO;
//    for (row in stockView.items) {
//        
//        if ([row objectForKey:@"Allocated"]) {
//            orderedQuantity = orderedQuantity + [[row objectForKey:@"Allocated"] intValue];
//        } else
//        {
//            //shouldDisplaySaveButton = NO;
//        }
//    }
//    if (orderQuantity == orderedQuantity && !orderQuantity==0) {
//        filterButton.enabled = YES;
//
//        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
//    } else {
//        filterButton.enabled = NO;
//
//        [self.navigationItem setRightBarButtonItem:nil animated:YES];
//    }
//    [allocatedLabel setText:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"Allocated Quantity", nil) ,orderedQuantity]];
//    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
//    [footerView.titleLabel setText:priceString];
//    
//    
//    
//}
//#pragma mark Product Stock View Delegate
//- (void)productStockViewLoaded {
//    NSArray *selectedItems = [product objectForKey:@"StockAllocation"];
//    
//    if (selectedItems.count > 0) {
//        for (int i = 0; i < stockView.items.count; i++) {
//            @autoreleasepool{
//            row = [stockView.items objectAtIndex:i];
//            
//            for (NSDictionary *selectedRow in selectedItems) {
//                if ([[row stringForKey:@"Stock_ID"] isEqualToString:[selectedRow stringForKey:@"Stock_ID"]]) {
//                    [row setValue:[selectedRow stringForKey:@"Allocated"] forKey:@"Allocated"];
//                    [stockView.items replaceObjectAtIndex:i withObject:row];
//                    break;
//                }
//            }
//        }
//        }
//
//    }
//   Singleton *single = [Singleton retrieveSingleton];
//
//    for (row in stockView.items) {
//        int qty = [[row objectForKey:@"Lot_Qty"] intValue];
//        
//        totalGoods = totalGoods + qty;
//
//    }
//    single.stockAvailble = [NSString stringWithFormat:@"%d",totalGoods];
//
//    [countLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Available Stock", nil),totalGoods]];
//    
//    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
//    [footerView.titleLabel setText:priceString];
//    
//    [self updateAllocation];
//}
//
//- (void)productStockViewDidFinishedEditingForRowIndex:(int)rowIndex {
//    [self updateAllocation];
//}


//#pragma mark UIKeyboard Notifications
//- (void)keyboardWillHide:(NSNotification *)notification {
//    NSDictionary *keyboardInfo = [notification userInfo];
//    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
//    CGRect toolbarFrame = self.navigationController.toolbar.frame;
//    toolbarFrame.origin.y = self.view.bounds.size.height + 44;
//    [UIView animateWithDuration:animationDuration animations:^ {
//      //  [self.gridView setFrame:self.view.bounds];
//        [self.navigationController.toolbar setFrame:toolbarFrame];
//        
//    }];
//}
//
//- (void)keyboardWillShow:(NSNotification *)notification {
//    NSDictionary *keyboardInfo = [notification userInfo];
//    
//    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
//    
//    float keyboardHeight = MIN(keyboardFrameBeginRect.size.width, keyboardFrameBeginRect.size.height);
//    CGRect toolbarFrame = self.navigationController.toolbar.frame;
//    toolbarFrame.origin.y = self.view.bounds.size.height + 44 - keyboardHeight;
//    
//    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
//    [UIView animateWithDuration:animationDuration animations:^ {
//        CGRect f = self.view.bounds;
//        f.size.height = f.size.height - keyboardHeight;
//       // [self.ta setFrame:f];
//        [self.navigationController.toolbar setFrame:toolbarFrame];
//    }];
//}

//-(void) viewDidUnload
//{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
//}
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        self.view = nil;
//    
//    // Dispose of any resources that can be recreated.
//}

//@end


















//product stock view previous








//
//  ProductStockViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductStockViewController.h"

@interface ProductStockViewController ()

@end

@implementation ProductStockViewController


@synthesize target,orderQuantity;
@synthesize action;

@synthesize delegate;

- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    
    if (self) {
        product=[NSDictionary dictionaryWithDictionary:p];
        [self setTitle:NSLocalizedString(@"Stock Info", nil)];
    }
    
    return self;
}




//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    [Flurry logEvent:@"Product Stock View"];
    //[Crittercism leaveBreadcrumb:@"<Product Stock View>"];
    
    stockView=[[ProductStockView alloc] initWithProduct:product ];
    [stockView setFrame:self.view.bounds];
    [stockView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [stockView setEditMode:YES];
    [stockView setDelegate:self];
    [stockView loadStock];
    
    [self.view addSubview:stockView];
    AppControl *appControl = [AppControl retrieveSingleton];
    
    isLotSelection = appControl.ALLOW_LOT_SELECTION;
    // if([[SWDefaults appControl] count]!=0)//ALLO_LOT_SELECTION
    if(![isLotSelection isEqualToString:@"Y"])
    {
        stockView.gridView.userInteractionEnabled = NO;
    }
    //Labels
    countLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
    [countLabel setText:@""];
    [countLabel setTextAlignment:NSTextAlignmentCenter];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor:[UIColor whiteColor]];
    [countLabel setFont:LightFontOfSize(14.0f)];
    [countLabel setShadowColor:[UIColor blackColor]];
    [countLabel setShadowOffset:CGSizeMake(0, -1)];
    
    orderedLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
    [orderedLabel setText:@""];
    [orderedLabel setTextAlignment:NSTextAlignmentCenter];
    [orderedLabel setBackgroundColor:[UIColor clearColor]];
    [orderedLabel setTextColor:[UIColor whiteColor]];
    [orderedLabel setFont:LightFontOfSize(14.0f)];
    [orderedLabel setShadowColor:[UIColor blackColor]];
    [orderedLabel setShadowOffset:CGSizeMake(0, -1)];
    
    allocatedLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
    [allocatedLabel setText:@""];
    [allocatedLabel setTextAlignment:NSTextAlignmentCenter];
    [allocatedLabel setBackgroundColor:[UIColor clearColor]];
    [allocatedLabel setTextColor:[UIColor whiteColor]];
    [allocatedLabel setFont:LightFontOfSize(14.0f)];
    [allocatedLabel setShadowColor:[UIColor blackColor]];
    [allocatedLabel setShadowOffset:CGSizeMake(0, -1)];
    
    
    
    //priceString = [NSString stringWithFormat:priceString, [product currencyStringForKey:@"Net_Price"], [product currencyStringForKey:@"List_Price"]];
    
    footerView = [[GroupSectionFooterView alloc] initWithFrame:CGRectMake(0, 0, stockView.bounds.size.width, 72)] ;
    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
    [footerView.titleLabel setText:priceString];
    [stockView.gridView.tableView setTableFooterView:footerView];
    
    
    filterButton = [[UIButton alloc] init] ;
    //filterButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {
        [filterButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
    }
    else
    {
        [filterButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
    }
    [filterButton setFrame:CGRectMake(self.view.frame.size.width-120,12, 100, 32)];
    [filterButton addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [filterButton bringSubviewToFront:self.view];
    [filterButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    filterButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
    filterButton.enabled = NO;
    [self.view addSubview:filterButton];
    
    // [self.gridView.tableView setTableHeaderView:self.customerHeaderView];
    //[self addSubview:self.customerHeaderView];
    
    
}



- (void)setupToolbar
{
    
    //    [countLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods]];
    //    [orderedLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Order Quantity", nil), orderQuantity]];
    //    [allocatedLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity]];
    //
    //    UIBarButtonItem *totalLabelButton ;
    //    UIBarButtonItem *orderLabelButton;
    //    UIBarButtonItem *allocatedLabelButton ;
    //
    //    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    //    totalLabelButton = [[UIBarButtonItem alloc] init];
    //    orderLabelButton = [[UIBarButtonItem alloc] init];
    //    allocatedLabelButton = [[UIBarButtonItem alloc] init];
    //
    //    totalLabelButton = [UIBarButtonItem labelButtonWithLabel:countLabel];
    //    orderLabelButton = [UIBarButtonItem labelButtonWithLabel:orderedLabel];
    //    allocatedLabelButton = [UIBarButtonItem labelButtonWithLabel:allocatedLabel];
    //    totalLabelButton.title = countLabel.text;
    //    orderLabelButton.title = orderedLabel.text;
    //    allocatedLabelButton.title = allocatedLabel.text;
    //    [self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, orderLabelButton, flexibleSpace, allocatedLabelButton, flexibleSpace, totalLabelButton, flexibleSpace, nil]];
    //    flexibleSpace=nil;
    //    totalLabelButton=nil;
    //    orderLabelButton=nil;
    //    allocatedLabelButton=nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setupToolbar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:self
                                                                              action:@selector(closeOrder)] ];
    
    
}
- (void)closeOrder {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
}

- (void)save:(id)sender {
    [self.view endEditing:YES];
    NSMutableArray *stockAllocation = [NSMutableArray array];
    for (NSDictionary *row in stockView.items) {
        
        if ([row stringForKey:@"Allocated"].length > 0) {
            [stockAllocation addObject:row];
        }
    }
    
    NSMutableDictionary *p = [NSMutableDictionary dictionaryWithDictionary:product];
    [p setValue:stockAllocation forKey:@"StockAllocation"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:p];
#pragma clang diagnostic pop
    
    if (delegate) {
        [delegate didTapSaveButton];
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)updateAllocation {
    orderedQuantity = 0;
    //BOOL shouldDisplaySaveButton = NO;
    for (NSDictionary *row in stockView.items) {
        
        if ([row objectForKey:@"Allocated"]) {
            orderedQuantity = orderedQuantity + [[row objectForKey:@"Allocated"] intValue];
        } else
        {
            //shouldDisplaySaveButton = NO;
        }
    }
    if (orderQuantity == orderedQuantity && !orderQuantity==0) {
        filterButton.enabled = YES;
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    } else {
        filterButton.enabled = NO;
        
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
    [allocatedLabel setText:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"Allocated Quantity", nil) ,orderedQuantity]];
    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
    [footerView.titleLabel setText:priceString];
    
    
    
}
#pragma mark Product Stock View Delegate
- (void)productStockViewLoaded {
    NSArray *selectedItems = [product objectForKey:@"StockAllocation"];
    
    if (selectedItems.count > 0) {
        for (int i = 0; i < stockView.items.count; i++) {
            @autoreleasepool{
                NSDictionary *row = [stockView.items objectAtIndex:i];
                
                for (NSDictionary *selectedRow in selectedItems) {
                    if ([[row stringForKey:@"Stock_ID"] isEqualToString:[selectedRow stringForKey:@"Stock_ID"]]) {
                        [row setValue:[selectedRow stringForKey:@"Allocated"] forKey:@"Allocated"];
                        [stockView.items replaceObjectAtIndex:i withObject:row];
                        break;
                    }
                }
            }
        }
        
    }
    Singleton *single = [Singleton retrieveSingleton];
    
    for (NSDictionary *row in stockView.items) {
        int qty = [[row objectForKey:@"Lot_Qty"] intValue];
        
        totalGoods = totalGoods + qty;
        
    }
    single.stockAvailble = [NSString stringWithFormat:@"%d",totalGoods];
    
    [countLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Available Stock", nil),totalGoods]];
    
    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
    [footerView.titleLabel setText:priceString];
    
    [self updateAllocation];
}

- (void)productStockViewDidFinishedEditingForRowIndex:(int)rowIndex {
    [self updateAllocation];
}


#pragma mark UIKeyboard Notifications
- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect toolbarFrame = self.navigationController.toolbar.frame;
    toolbarFrame.origin.y = self.view.bounds.size.height + 44;
    [UIView animateWithDuration:animationDuration animations:^ {
        //  [self.gridView setFrame:self.view.bounds];
        [self.navigationController.toolbar setFrame:toolbarFrame];
        
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    float keyboardHeight = MIN(keyboardFrameBeginRect.size.width, keyboardFrameBeginRect.size.height);
    CGRect toolbarFrame = self.navigationController.toolbar.frame;
    toolbarFrame.origin.y = self.view.bounds.size.height + 44 - keyboardHeight;
    
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        CGRect f = self.view.bounds;
        f.size.height = f.size.height - keyboardHeight;
        // [self.ta setFrame:f];
        [self.navigationController.toolbar setFrame:toolbarFrame];
    }];
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end















