//
//  SalesWorxImageAnnimateButton.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxImageAnnimateButton.h"

@implementation SalesWorxImageAnnimateButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)UpdateImage:(UIImage *)newImage WithAnimation:(BOOL)Animated
{
    CABasicAnimation *crossFade = [CABasicAnimation animationWithKeyPath:@"contents"];
    crossFade.duration = 0.7;
    crossFade.fromValue = (id)self.imageView.image.CGImage;
    crossFade.toValue = (id)newImage.CGImage;
    crossFade.removedOnCompletion = NO;
    crossFade.fillMode = kCAFillModeRemoved;
    [self.imageView.layer addAnimation:crossFade forKey:@"animateContents"];
    
    //Make sure to add Image normally after so when the animation
    //is done it is set to the new Image
    [self setImage:newImage forState:UIControlStateNormal];
}

@end
