//
//  CustomerOrderHistoryView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "CustomerOrderHistoryCell.h"
#import "CustomerHeaderView.h"
@interface CustomerOrderHistoryView : UIView <UITableViewDelegate, UITableViewDataSource, CustomerOrderHistoryCellDelegate,GridViewDataSource,GridViewDelegate>
{
    UITableView *tableView;
    NSDictionary *customer;
    NSArray *orders;
    UIPopoverController *popOverController;
    CustomerHeaderView *customerHeaderView;
    CustomerHeaderView *subCustomerHeaderView;
    UIButton *backButton;
    OrderItemsViewController *orderItemsViewController;
    OrderInvoiceViewController *orderInvoiceViewController;
    UIButton *upButton;
    UIButton *downButton;
    UISegmentedControl *segmentedControl;
    UIView *orderBottomView;
    UITableView *orderDetailTableView;
    UIImageView *bacgroundBar;
    NSArray *items;
    UILabel *orderTitle;
    NSString *invoinceTotal;
    UIView *borderView;
    
    UIView *orderItemView;
    BOOL isToggled;
    BOOL isInvoice;
    BOOL isPort;
    //
    int selectedSegment;
    int orderIndexPath;


}

@property (nonatomic, strong) UIView *orderItemView;

@property (nonatomic, readwrite)BOOL isToggled;

@property (nonatomic, strong)  GridView *gridView;

@property(nonatomic) int testIndexPath;


-(void)loadTestView;


- (id)initWithCustomer:(NSDictionary *)row;
- (void)loadOrders;

- (void)refreshOrderHistory;
@end
