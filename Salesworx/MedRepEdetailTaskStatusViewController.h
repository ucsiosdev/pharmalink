//
//  MedRepEdetailTaskStatusViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/26/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TaskStatusDelete <NSObject>

-(void)selectedStatus:(NSMutableDictionary*)statusDict;


@end

@interface MedRepEdetailTaskStatusViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    id delegate;
}
@property (strong, nonatomic) IBOutlet UITableView *taskStatusTblView;
@property(retain)  NSIndexPath* lastIndexPath;
@property(nonatomic) id delegate;
@property(strong,nonatomic) NSMutableArray* contentArray;
@property(nonatomic)NSInteger selectedIndex;

@property(strong,nonatomic) NSString* savedTaskString;
@end
