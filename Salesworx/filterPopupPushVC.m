//
//  filterPopupPushVC.m
//  AWR-CMS
//
//  Created by Unique Computer Systems on 5/19/14.
//  Copyright (c) 2014 Saad Ansari. All rights reserved.
//

#import "filterPopupPushVC.h"


@interface filterPopupPushVC ()

@end

@implementation filterPopupPushVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableArray = [[NSMutableArray alloc]init];
    self.tableArrayFiletered = [[NSMutableArray alloc]init];
    
    [self.tableArray addObjectsFromArray:self.tableArrayFrom];
}

- (void)viewWillAppear:(BOOL)animated {
    
    CGSize size = CGSizeMake(350, 400); 
    self.preferredContentSize = size;
    
    [super viewWillAppear:animated];
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0) {
        searching = NO;
        [self.tableView reloadData];
    }else{
        searching = YES;
        
        [self.tableArrayFiletered removeAllObjects];
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF contains[c] %@",
                                  searchText];
        self.tableArrayFiletered = [[self.tableArray filteredArrayUsingPredicate:predicate]mutableCopy];
        [self.tableView reloadData];
    }
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar1{
    
    NSString* newText = searchBar1.text;
    if (newText.length >0) {
        
    }else{
        searching = NO;
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count;
    if (searching) {
        count = self.tableArrayFiletered.count;
    }else{
        count = self.tableArray.count;
    }

    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        cell.textLabel.font = Helvetica_Neue_Light_16;
//        cell.textLabel.textColor = UIColorFromRGB(0X5A6670);
        cell.backgroundColor = [UIColor clearColor];
    }
    
    if (searching) {
        cell.textLabel.text = [self.tableArrayFiletered objectAtIndex:indexPath.row];
    }else{
        cell.textLabel.text = [self.tableArray objectAtIndex:indexPath.row];
    }
    return cell;

}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (searching) {
        
        NSString * selectedRow = [self.tableArrayFiletered objectAtIndex:indexPath.row];
        NSUInteger fooIndex = [self.tableArray indexOfObject: selectedRow];
        if (_delegate != nil) {
            [_delegate selectedChild:fooIndex];
        }
    }else {
        if (_delegate != nil) {
            [_delegate selectedChild:indexPath.row];
        }
    }
   
    [self.selectedPopover dismissPopoverAnimated:YES];
}



- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
