//
//  SWCustomerOrderHistoryDescriptionViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/31/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepElementTitleDescriptionLabel.h"
#import "SalesWorxTableView.h"
#import "SalesWorxNavigationHeaderLabel.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxNavigationButton.h"
#import "SWCustomerOrderHistoryDescriptionTableViewCell.h"
#import "NSString+Additions.h"
#import "MedRepTextField.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@interface SWCustomerOrderHistoryDescriptionViewController : SalesWorxPresentControllerBaseViewController<UITableViewDelegate,UITableViewDataSource,SalesWorxPopoverControllerDelegate,UIPopoverPresentationControllerDelegate>

{
    
    IBOutlet SalesWorxNavigationHeaderLabel *titleLabel;
    IBOutlet SalesWorxNavigationButton *closeButton;
    IBOutlet UISegmentedControl *descriptionSegment;
    IBOutlet MedRepElementTitleLabel *orderNumberTitleLbl;
    IBOutlet MedRepElementDescriptionLabel *orderNumberDescriptionLbl;
    IBOutlet SalesWorxTableView *orderDescTblView;
    IBOutlet MedRepElementTitleLabel *statusTitleLbl;
    IBOutlet MedRepElementDescriptionLabel *statusDescriptionLbl;
    IBOutlet UIView *contentBGView;
    IBOutlet UIView *headerView;
    NSString *popOverTitle;
    NSMutableArray* filteredInvoiceLineItems;
    
    IBOutlet MedRepElementDescriptionLabel *erpOrderNumberDescriptionLbl;
    IBOutlet MedRepTextField *invoiceSelectionTextfield;
    IBOutlet MedRepElementTitleLabel *erpReferenceNumerTextLabel;

}
- (IBAction)descriptionSegmentTapped:(UISegmentedControl *)sender;
- (IBAction)closeButtonTapped:(id)sender;
@property(strong,nonatomic) Customer * selectedCustomer;
@property(strong,nonatomic) NSString* orderNumber;
@property(strong,nonatomic) NSString* erpOrderNumber;
@property(strong,nonatomic) NSString* status;
@property(strong,nonatomic) NSMutableArray* orderLineItems;
@property(strong,nonatomic) NSMutableArray* invoiceLineItems;




@end
