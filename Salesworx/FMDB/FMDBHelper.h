//
//  FMDBHelper.h
//  SmartTracker
//
//  Created by Priyanka Bhatia on 7/25/13.
//  Copyright (c) 2013 UCS. All rights reserved.
//*
/*
#import <Foundation/Foundation.h>

@class NewsItem;
@class EventItem;
@class HealthStats;

@interface FMDBHelper : NSObject

//----------------------------------
//NEWS ITEM
- (BOOL)insertNewsItem:(NewsItem *)newsItem withLanguageCode:(NSString *)langCode;//C
- (NSMutableArray *)getAllNewsItemsWithLanguageCode:(NSString *)langCode;//R
- (NewsItem *)getNewsItem: (NewsItem *)newsItem;//R
- (BOOL)updateNewsItem:(NewsItem *)newsItem withLanguageCode:(NSString *)langCode;//U
- (BOOL)deleteAllNewsItemsWithLanguageCode:(NSString *)langCode;//D
//----------------------------------

//----------------------------------
//EVENT ITEM
- (BOOL)insertEventItem:(EventItem *)eventItem withLanguageCode:(NSString *)langCode;//C
- (NSMutableArray *)getAllEventItemsWithLanguageCode:(NSString *)langCode;//R
- (EventItem *)getEventItem: (EventItem *)eventItem;//R
- (BOOL)updateEventItem:(EventItem *)eventItem withLanguageCode:(NSString *)langCode;//U
- (BOOL)deleteAllEventItemsWithLanguageCode:(NSString *)langCode;//D
//----------------------------------

//----------------------------------
//HEALTH STATS
- (BOOL)insertHealthStats:(HealthStats *)stats;//C
- (NSMutableArray *)getAllHealthStats;//R
- (HealthStats *)getHealthStats: (HealthStats *)stats;//R
- (BOOL)updateHealthStats:(HealthStats *)stats;//U
- (BOOL)deleteHealthStats:(HealthStats *)stats;//D
- (BOOL)deleteAllHealthStats;//D
//----------------------------------
*/

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMResultSet.h"
@interface FMDBHelper : NSObject

+ (FMDatabase *)getDatabase;
+ (BOOL)executeNonQuery:(NSString *)sqlQuery;
+ (NSMutableArray *)executeQuery:(NSString *)sqlQuery;

@end
