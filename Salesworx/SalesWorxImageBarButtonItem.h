//
//  SalesWorxImageBarButtonItem.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 1/19/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxImageBarButtonItem : UIBarButtonItem
@property (nonatomic) IBInspectable BOOL RTLSupport;
@end
