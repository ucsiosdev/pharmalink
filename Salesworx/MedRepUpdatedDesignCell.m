//
//  MedRepUpdatedDesignCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/8/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepUpdatedDesignCell.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@implementation MedRepUpdatedDesignCell
@synthesize isCellSelected,cellStatusViewType;
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.titleLbl.font= MedRepHeaderTitleFont;
    self.descLbl.font = kSWX_FONT_SEMI_BOLD(14);
    cellStatusViewType=KSelectionColorStatusView;

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setSelectedCellStatus
{

    self.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    [self.descLbl setTextColor:MedRepMenuTitleSelectedCellTextColor];
    self.cellDividerImg.hidden=YES;
    self.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    [self.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    
    if(cellStatusViewType==KSelectionColorStatusView)
    {
        self.statusView.backgroundColor =MedRepUITableviewSelectedCellStatusViewBackgroundColor;
  
    }
    else if(cellStatusViewType==KCustomColorStatusView)
    {        
    }
    else if(cellStatusViewType==KNoColorStatusView)
    {
        [self.statusView setHidden:YES];
    }
}
-(void)setDeSelectedCellStatus
{

    self.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    self.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    self.descLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    [self.contentView setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
    self.cellDividerImg.hidden=NO;
    
    if(cellStatusViewType==KSelectionColorStatusView)
    {
        self.statusView.backgroundColor =[UIColor clearColor];
    }
    else if(cellStatusViewType==KCustomColorStatusView)
    {
    }
    else if(cellStatusViewType==KNoColorStatusView)
    {
        [self.statusView setHidden:YES];
    }
}
@end
