//
//  TaskPopoverViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/7/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "TaskPopoverViewController.h"
#import "SWDatabaseManager.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepTaskListTableViewCell.h"
#import "TaskPopoverFilterViewController.h"
#import "MedRepQueries.h"
#import "TaskDetailPopUpViewController.h"


@interface TaskPopoverViewController ()

@end

@implementation TaskPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    filteredTaskArray = [[NSMutableArray alloc]init];
    previousFilterParameters = [[NSMutableDictionary alloc]init];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    taskArray = [MedRepQueries fetchAllTaskObjects];
    filteredTaskArray = taskArray;
    
    if(self.isMovingToParentViewController)
    {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        [TopContentView setHidden:NO];
        [self searchContent];
    }
}

#pragma mark UITableView Methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    tableView.separatorColor=kUITableViewSaperatorColor;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filteredTaskArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0f;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [taskTableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    static NSString *CellIdentifier = @"TaskListCell";
    
    MedRepTaskListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepTaskListTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    VisitTask *task = [filteredTaskArray objectAtIndex:indexPath.row];
    
    
    cell.titleLbl.text=[NSString stringWithFormat:@"%@", task.Title];
    
    cell.descriptionLbl.hidden=NO;
    cell.statusImageViewTopConstraint.constant=27.0f;
    cell.descriptionLblheightConstraint.constant=25.0;
    
    if (task.Doctor_ID.length > 0) {
        cell.descriptionLbl.text = [MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchDoctorwithID:task.Doctor_ID]];
    }
    else if(task.Contact_ID.length > 0){
        cell.descriptionLbl.text = [MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchContactwithContactID:task.Contact_ID]];
    }else{
        cell.descriptionLbl.text=KNotApplicable;
    }

    UIImage* statusImage;
    NSString* taskStatus = task.Status;
    
    NSLog(@"Status is %@", taskStatus);
    
    if ([taskStatus isEqualToString:kNewStatisticsTitle]) {
        statusImage=[UIImage imageNamed:@"Task_NewIcon"];
    } else if ([taskStatus isEqualToString:kClosedStatisticsTitle]) {
        
        statusImage=[UIImage imageNamed:@"Task_CloseIcon"];
    } else if ([taskStatus isEqualToString:kDeferedStatisticsTitle]){
        
        statusImage=[UIImage imageNamed:@"Task_DeferredIcon"];
    } else {
        
        statusImage=[UIImage imageNamed:@"Task_CancelIcon"];
    }
    
    
    cell.statusImageView.image=statusImage;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font=MedRepTitleFont;
    cell.textLabel.textColor=MedRepMenuTitleFontColor;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    [TopContentView setHidden:YES];

    TaskDetailPopUpViewController *obj = [[TaskDetailPopUpViewController alloc]initWithNibName:@"TaskDetailPopUpViewController" bundle:[NSBundle mainBundle]];
    obj.selectedTask = [filteredTaskArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:obj animated:NO];
}

- (IBAction)doneButtontapped:(id)sender {
    
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];
}
- (void)CloseSyncPopOver
{
    //do what you need to do when animation ends...
    [self dismissViewControllerAnimated:NO completion:^{
        
        [_TaskPopoverDelegate updateTaskData];
    }];
    
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = TopContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
- (IBAction)filterButtonTapped:(id)sender {

    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    blurredBgImage.image = [UIImage imageNamed:@"BlurView"];

    TaskPopoverFilterViewController *popOverVC = [[TaskPopoverFilterViewController alloc]init];
    popOverVC.delegate = self;
    popOverVC.previousFilterParametersDict = previousFilterParameters;
    
    popOverVC.taskArray = taskArray;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(376, 250);
    self.modalInPopover=YES;
    
    popover.sourceView = filterButton;
    popover.sourceRect = filterButton.bounds;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    //[self presentViewController:nav animated:YES completion:nil];
    taskSearchBar.text=@"";
    [self searchContent];
    
    
    [self presentViewController:nav animated:YES completion:^{
        popover.passthroughViews=nil;
        self.modalInPopover=YES;
        
    }];
}

-(void)taskFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    filteredTaskArray = [taskArray mutableCopy];
    [taskTableView reloadData];
}
-(void)taskFilterDidClose
{
    [blurredBgImage removeFromSuperview];
}

-(void)filteredTaskList:(NSMutableArray*)filteredContent
{
    NSLog(@"filtered todo list is %@", filteredContent);
    [blurredBgImage removeFromSuperview];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    [taskTableView reloadData];
}
-(void)filteredTaskParameters:(NSMutableDictionary*)parametersDict
{
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    previousFilterParameters=parametersDict;
    NSLog(@"filtered parameters dict %@", previousFilterParameters);
    [self searchContent];
    
}
- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

#pragma mark Search Methods


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [taskSearchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchContent];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [taskSearchBar setText:@""];
    [taskSearchBar setShowsCancelButton:NO animated:YES];
    filteredTaskArray=[[NSMutableArray alloc]init];
    if ([taskSearchBar isFirstResponder]) {
        [taskSearchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    [self searchContent];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [taskSearchBar setShowsCancelButton:NO animated:YES];
}

-(void)searchContent
{
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filter parameters dict is %@",previousFilterParameters);
    NSLog(@"searching with text in task popover %@",taskSearchBar.text);

    
    NSString* dateString=[previousFilterParameters valueForKey:@"Created_At"];
    NSString* statusString=[previousFilterParameters valueForKey:@"Status"];
    if([NSString isEmpty:dateString]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Created_At ==[cd] %@",dateString]];
    }
    
    if([NSString isEmpty:statusString]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Status ==[cd] %@",statusString]];
    }
    
    NSString *searchString = taskSearchBar.text;
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Title", searchString];
    if(searchString.length>0)
        [predicateArray addObject:predicate1];
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    filteredTaskArray = [[NSMutableArray alloc]init];
    filteredTaskArray = [[taskArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    [taskTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
