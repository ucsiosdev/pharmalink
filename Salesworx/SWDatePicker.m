//
//  FBDatePicker.m
//  FormBuilder
//
//  Created by Irfan Bashir on 5/1/12.
//  Copyright (c) 2012 App ER. All rights reserved.
//

#import "SWDatePicker.h"

@implementation SWDatePicker

@synthesize datePicker;
@synthesize toolbar;
@synthesize delegate= _delegate;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        if (self.datePicker) {
            self.datePicker=nil;
        }
        [self setDatePicker:[[UIDatePicker alloc] init] ];
        [self.datePicker setDate:[NSDate date]];
        [self.datePicker setDatePickerMode:UIDatePickerModeDate];
        
        if([[[NSUserDefaults standardUserDefaults] stringForKey:@"PaymentType"] isEqualToString:@"CHEQUE"])
        {
            [self.datePicker setMaximumDate:[NSDate date]];

        }
        else if([[[NSUserDefaults standardUserDefaults] stringForKey:@"PaymentType"] isEqualToString:@"PDC"])
        {
            NSDate *previousDate = [[NSDate date] dateByAddingTimeInterval:(1.0*24*60*60)];
            [self.datePicker setMinimumDate:previousDate];
        }
        else if([[[NSUserDefaults standardUserDefaults] stringForKey:@"PaymentType"] isEqualToString:@"CASH"])
        {
            [self.datePicker setMinimumDate:[NSDate date]];

        }
        
        [self addSubview:self.datePicker];
        
        if (self.toolbar) {
            self.toolbar=nil;
        }
        [self setToolbar:[[UIToolbar alloc] init] ];
        [self.toolbar setTranslucent:YES];
        [self.toolbar setBarStyle:UIBarStyleBlackTranslucent];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dateSelectionDone)] ;
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelSelection)] ;
        
        [self.toolbar setItems:[NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil]];
        
        [self addSubview:self.toolbar];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.toolbar setFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
    [self.datePicker setFrame:CGRectMake(0, 44, self.bounds.size.width, 180)];
}



#pragma mark - Toolbar Selectors

- (void)dateSelectionDone {
    if ([self.delegate respondsToSelector:@selector(datePickerSelectionDone:withDate:)]) {
        [self.delegate datePickerSelectionDone:self withDate:self.datePicker.date];
    }
}

- (void)cancelSelection {
    if ([self.delegate respondsToSelector:@selector(datePickerSelectionCancelled:)]) {
        [self.delegate datePickerSelectionCancelled:self];
    }
}
@end
