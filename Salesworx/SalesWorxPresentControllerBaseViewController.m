//
//  SalesWorxPresentControllerBaseViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPresentControllerBaseViewController.h"
#import "SWDefaults.h"
#define KDeviceScreenHeight [UIScreen mainScreen].bounds.size.height
#define KiServicesPopUpsBackgroundColor [UIColor colorWithRed:(0.0/255.0) green:(0.0/255.0)  blue:(0.0/255.0)  alpha:0.5]
#define KiServicesPopUpsDismissBackgroundColor [UIColor colorWithRed:(0.0/255.0) green:(0.0/255.0)  blue:(0.0/255.0)  alpha:0]
@interface SalesWorxPresentControllerBaseViewController ()
{
    BOOL isViewLoaded;

}
@end

@implementation SalesWorxPresentControllerBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [topContentView setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [topContentView setHidden:YES];
    topContentView.backgroundColor=[UIColor colorWithRed:(245.0/255.0) green:(245.0/255.0) blue:(245.0/255.0) alpha:1.0];
}
-(void)viewDidAppear:(BOOL)animated{
    //[NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(performAnimation) userInfo:nil repeats:NO];
    [self setHiddenAnimated:NO duration:1.0 transitionType:kCATransitionReveal];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)dimissViewControllerWithSlideDownAnimation:(BOOL)Animated
{
    
    self.view.userInteractionEnabled=NO;
    [self setHiddenAnimated:YES duration:KBasePopUpViewAnimationTime transitionType:kCATransitionReveal];
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CGFloat yPosition=topContentView.frame.origin.y;
    
    if(!hide)
    {
        isViewLoaded=YES;
        topContentView.frame=CGRectMake(topContentView.frame.origin.x, -topContentView.frame.size.height, topContentView.frame.size.width, topContentView.frame.size.height);
        
        topContentView.hidden=hide;
    }
    
    [UIView transitionWithView:topContentView
                      duration:KBasePopUpViewAnimationTime
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        
                        if(hide)
                        {
                            isViewLoaded=NO;
                            
                            topContentView.frame=CGRectMake(topContentView.frame.origin.x, self.view.frame.size.height, topContentView.frame.size.width, topContentView.frame.size.height);
                            self.view.backgroundColor=KiServicesPopUpsDismissBackgroundColor;
                            
                            
                        }
                        else
                        {
                            
                            topContentView.frame=CGRectMake(topContentView.frame.origin.x, yPosition, topContentView.frame.size.width, topContentView.frame.size.height);
                            self.view.backgroundColor=KiServicesPopUpsBackgroundColor;
                            
                        }
                        
                    }
                    completion:^(BOOL finished) {
                        topContentView.hidden=hide;
                        
                        if(hide)
                        {
                            [self AnimationViewDidDisAppear];
                            [self dismissViewControllerAnimated:NO completion:nil];
                        }else{
                            [self AnimationViewDidAppear];
                            self.view.userInteractionEnabled=YES;
                        }
                        
                    }];
    
}
-(void)AnimationViewDidAppear{
    /** do not write anything in this method.These methods are overwritten in the subclasses*/
}
-(void)AnimationViewDidDisAppear{
    /** do not write anything in this method.These methods are overwritten in the subclasses*/
    
}

@end
