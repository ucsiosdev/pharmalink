//
//  MedRepTaskListTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MedRepElementTitleLabel.h"
@interface MedRepTaskListTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *statusImageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *statusImageTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionLblheightConstraint;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *descriptionLbl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *statusImageViewWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *statusImageViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *statusImageViewTopConstraint;


@end
