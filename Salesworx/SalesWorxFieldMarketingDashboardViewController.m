//
//  SalesWorxFieldMarketingDashboardViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 12/11/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldMarketingDashboardViewController.h"
#import "MedRepVisitsListViewController.h"

@interface SalesWorxFieldMarketingDashboardViewController ()

@end

@implementation SalesWorxFieldMarketingDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    appDel = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.view.backgroundColor = UIViewBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Field Marketing Dashboard", nil)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view layoutIfNeeded];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        } else {
        }
    }
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingDashboard];
    }
    
    btnTask = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"TaskIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnTask)];
    [btnTask setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = btnTask;
    
    // create border on face time and wait time view
    faceTimeView.layer.borderWidth = 1;
    faceTimeView.layer.borderColor = [UIColor colorWithRed:43.0/255.0 green:155.0/255.0 blue:216.0/255.0 alpha:1].CGColor;
    faceTimeView.layer.masksToBounds = YES;
    
    waitTimeView.layer.borderWidth = 1;
    waitTimeView.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:91.0/255.0 blue:91.0/255.0 alpha:1].CGColor;
    waitTimeView.layer.masksToBounds = YES;
    
    
    // get Current month
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    df.dateFormat=@"yyyy-MM";
    monthString = [[df stringFromDate:[NSDate date]] capitalizedString];

    
    NSString *currentDateinDisplayFormat = [MedRepQueries fetchDatabaseDateFormat];
    NSMutableArray *VisitsArray = [MedRepQueries fetchPlannedVisitsforSelectedDate:currentDateinDisplayFormat];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    if ([VisitsArray count] > 0) {
        NSMutableArray *plannedVisit = [[NSMutableArray alloc]init];
        for (int i = 0; i<[VisitsArray count]; i++)
        {
            NSString *dateStr = [[VisitsArray objectAtIndex:i] objectForKey:@"Visit_Date"];
            NSDate *date = [dateFormatter dateFromString:dateStr];
            
            if (![[[VisitsArray objectAtIndex:i]valueForKey:@"Visit_Status"] isEqualToString:@"Y"] && ([date compare:[NSDate date]] == NSOrderedDescending)) {
                [plannedVisit addObject:[VisitsArray objectAtIndex:i]];
            }
        }
        if ([plannedVisit count] == 0)
        {
            viewUpcomingAppointment.hidden = YES;
            btnRescheduleVisit.hidden = YES;
            lblAppointmentTime.hidden = YES;
        } else
        {
            
            NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"Visit_Date" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
            sortedPlannedVisitArray = [plannedVisit sortedArrayUsingDescriptors:sortDescriptors];
            
            if ([[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Doctor_Name"] isEqualToString:@"N/A"])
            {
                lblDoctorName.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Pharmacy_Contact_Name"];
            }
            else
            {
                lblDoctorName.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Doctor_Name"];
            }
            lblLocation.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Location_Name"];
            lblTelephoneNo.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Phone"];
            if (lblTelephoneNo.text.length == 0) {
                lblTelephoneNo.text = @"--";
            }

            lblAppointmentTime.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"dd MMMM, hh:mm a" scrString:[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Visit_Date"]];
        }
    }
    
    // calculate face time and wait time
    
    NSArray *arrFaceTime = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"Select round (sum ((JulianDay(Call_Ended_At) - JulianDay(Call_Started_At)) * 24 * 60)) As faceTime FROM PHX_TBL_Actual_Visits where Visit_Start_Date like '%%%@%%'",monthString]];
    
    NSArray *arrWaitTime = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"Select round (sum ((JulianDay(Call_Started_At) - JulianDay(Visit_Start_Date)) * 24 * 60)) As waitTime FROM PHX_TBL_Actual_Visits where Visit_Start_Date like '%%%@%%'",monthString]];
    
    int faceTime = 0;
    int waitTime = 0;
    if (arrFaceTime >0)
    {
        faceTime = [[[arrFaceTime objectAtIndex:0]stringForKey:@"faceTime"]intValue];
        if (faceTime < 0) {
            faceTime = 0;
        }
    }
    if (arrWaitTime >0)
    {
        waitTime = [[[arrWaitTime objectAtIndex:0]stringForKey:@"waitTime"]intValue];
        if (waitTime < 0) {
            waitTime = 0;
        }
    }

    int hoursFaceTime =  faceTime / 60;
    int minutesFaceTime = (faceTime - hoursFaceTime * 60 );
    lblFaceTime.text = [NSString stringWithFormat:@"%.2d:%.2d", hoursFaceTime, minutesFaceTime];
    
    
    int hoursWaitTime =  waitTime / 60;
    int minutesWaitTime = (waitTime - hoursWaitTime * 60 );
    lblWaitTime.text = [NSString stringWithFormat:@"%.2d:%.2d", hoursWaitTime, minutesWaitTime];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadAllChart];
    [self loadTaskData];
    [self loadVisitFrequency];
    [self loadViewForDemonstratedProducts];
}

-(void)loadViewForDemonstratedProducts {
    
    NSMutableArray *arrproductColors = [[NSMutableArray alloc]initWithObjects:PNPieChartGreen, PNPieChartBlue, PNPieChartYellow, PNPieChartRed, PNPieChartOrange, nil];
    NSMutableArray *itemsArray = [[NSMutableArray alloc]init];
    
    NSMutableArray *CountMedialFiles = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT  IFNULL(p.Product_Name,'') AS Product_Name, e.Product_ID, IFNULL(COUNT(*),0) AS ProductCount   FROM PHX_TBL_EDetailing AS e left join phx_tbl_products as p on e.Product_ID=p.Product_ID   where e.Discussed_At like '%%%@%%' and  p.Product_Name not null GROUP BY e.Product_ID ORDER BY e.Product_ID ASC limit 5",monthString]];
    
    if ([CountMedialFiles count] == 0) {
        self.lblNoProductDemonstratedData.hidden = NO;
        self.pieChartParentView.hidden = YES;
    }
    else
    {
        self.lblNoProductDemonstratedData.hidden = YES;
        self.pieChartParentView.hidden = NO;
        
        for (int i = 0; i< [CountMedialFiles count]; i++) {
            
            NSMutableDictionary *dic = [CountMedialFiles objectAtIndex:i];
            [itemsArray addObject:[PNPieChartDataItem dataItemWithValue:[[dic valueForKey:@"ProductCount"]floatValue] color:[arrproductColors objectAtIndex:i] description:[dic valueForKey:@"Product_Name"]]];
        }
    }
    
    NSArray *items = [(NSArray *) itemsArray mutableCopy];
    //For Pie Chart
    PNPieChart *productDemonstratedPieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(0.0, 0.0, _pieChartParentView.frame.size.width, _pieChartParentView.frame.size.height) items:items];
    productDemonstratedPieChart.descriptionTextColor = [UIColor whiteColor];
    productDemonstratedPieChart.descriptionTextFont  = kSWX_FONT_SEMI_BOLD(20);
    
    productDemonstratedPieChart.descriptionTextShadowColor = [UIColor clearColor];
    productDemonstratedPieChart.showAbsoluteValues = NO;
    productDemonstratedPieChart.showOnlyValues = YES;
    [productDemonstratedPieChart strokeChart];
    
    
    //Build the legend
    productDemonstratedPieChart.legendStyle = PNLegendItemStyleStacked;
    productDemonstratedPieChart.legendFont = MedRepElementDescriptionLabelFont;
    productDemonstratedPieChart.legendFontColor = MedRepElementDescriptionLabelColor;
    UIView *legend = [productDemonstratedPieChart getLegendWithMaxWidth:200];
    
    //Move legend to the desired position and add to view
    [legend setFrame:CGRectMake(10, 10, legend.frame.size.width, self.viewDemonstratedProduct.frame.size.height-60)];
    [self.viewDemonstratedProduct addSubview:legend];
    [_pieChartParentView addSubview:productDemonstratedPieChart];
}


- (void)loadTaskData {

    // Load Tasks data of Doctors and Pharmacies
    NSMutableArray *arrTasks = [MedRepQueries fetchTotalTasksForDoctorAndLocation];
    int tasksOpen = 0;
    int tasksCompleted = 0;
    
    for (VisitTask *selectedTask in arrTasks) {
        if ([selectedTask.Status isEqualToString:@"Closed"])
        {
            tasksCompleted++;
        }
        else if ([selectedTask.Status isEqualToString:@"New"] || [selectedTask.Status isEqualToString:@"Deffered"])
        {
            tasksOpen++;
        }
    }

    lblTasksOpen.text = [NSString stringWithFormat:@"%d",tasksOpen];
    lblTasksCompleted.text = [NSString stringWithFormat:@"%d",tasksCompleted];
}

#pragma mark Load PercentageChart
-(void)loadAllChart
{
    // Load view of Doctor's Achievement
    NSNumber *doctorsCompletedVisitsCount=[MedRepQueries fetchCompletedDoctorsCallsInCurrentMonth];
    NSNumber *doctorsRequiredVisitsCount=[MedRepQueries fetchNumberOfVisitsRequiredForDoctorsInaMonth];
    lblDoctorsVisitsRequired.text = [NSString stringWithFormat:@"%@",doctorsRequiredVisitsCount];
    lblDoctorsVisitsCompleted.text = [NSString stringWithFormat:@"%@",doctorsCompletedVisitsCount];

    
    // Load view of Pharmacy's Achievement
    NSNumber *PharmaciesCompletedVisitsCount=[MedRepQueries fetchCompletedPharmaciesCallsInCurrentMonth];
    NSNumber *PharmaciesRequiredVisitsCount=[MedRepQueries fetchNumberOfVisitsRequiredForPharmaciesInaMonth];
    lblPharmaciesVisitsRequired.text = [NSString stringWithFormat:@"%@",PharmaciesRequiredVisitsCount];
    lblPharmaciesVisitsCompleted.text = [NSString stringWithFormat:@"%@",PharmaciesCompletedVisitsCount];
    
    
    // Load View for Productive Visits
    NSArray *arrTotalVisits = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select count(*) from PHX_TBL_Actual_Visits where Visit_Start_Date like '%%%@%%'",monthString]];
    NSArray *arrProductiveVisits = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select count(distinct Planned_Visit_ID) As count from PHX_TBL_Actual_Visits  where Visit_Start_Date like '%%%@%%' and Call_Started_At is not null",monthString]];
    
    
    int totalVisits = 0;
    int productiveVisits = 0;
    if (arrTotalVisits >0)
    {
        totalVisits = [[[arrTotalVisits objectAtIndex:0]stringForKey:@"count(*)"]intValue];
    }
    
    if (arrProductiveVisits >0)
    {
        productiveVisits = [[[arrProductiveVisits objectAtIndex:0]stringForKey:@"count"]intValue];
    }
    
    NSArray *arrProductDemonstrated = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select count(*) from PHX_TBL_EDetailing where Discussed_At like '%%%@%%'",monthString]];
    NSArray *arrSamplesGiven = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select sum(A.Issue_Qty) As sum from PHX_TBL_Issue_Note_Line_Items AS A left join PHX_TBL_Issue_Note As B  where A.Doc_No = B.Doc_No and B.Start_Time like '%%%@%%'",monthString]];
    
    
    int productDemonstrated = 0;
    int samplesGiven = 0;
    if (arrProductDemonstrated >0)
    {
        productDemonstrated = [[[arrProductDemonstrated objectAtIndex:0]stringForKey:@"count(*)"]intValue];
    }
    
    if (arrSamplesGiven >0)
    {
        samplesGiven = [[[arrSamplesGiven objectAtIndex:0]stringForKey:@"sum"]intValue];
    }
    
    lblProductsDemonstrated.text = [NSString stringWithFormat:@"%d",productDemonstrated];
    lblSamplesGiven.text = [NSString stringWithFormat:@"%d",samplesGiven];
}

#pragma mark Load Vist Frequency Graph

-(void)loadVisitFrequency
{
    [self.gkLineGraph reset];
    
    arrVisitFrequency = [[NSMutableArray alloc]init];
    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        
        NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
        NSString *output = @"0.00";
        NSString *newOutput = [NSString stringWithFormat:@"%@", output];
        [dummyDict setObject:newOutput forKey:@"Ammount"];
        [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
        
        NSArray *temp = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) AS Total FROM PHX_TBL_Planned_Visits where Visit_Date Like '%%%@%%' AND Visit_Status = 'Y'", currentString]];
        [dummyDict setObject:[[temp objectAtIndex:0]valueForKey:@"Total"] forKey:@"Total"];
        
        if ([[[temp objectAtIndex:0]valueForKey:@"Total"]intValue] >0) {
            [arrVisitFrequency addObject:dummyDict];
        }
    }
    
    if (arrVisitFrequency.count==0) {
        isEmptyGraph=YES;
        
        [self displayEmptyDataGraph];
    }
    else{
        
        //adding missing date data
        
        NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
        
        for (NSInteger i=0; i<tempDates.count; i++) {
            NSString* currentString=[tempDates objectAtIndex:i];
            if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
                
            }
            else{
                NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
                NSString *output = @"0.00";
                NSString *newOutput = [NSString stringWithFormat:@"%@", output];
                [dummyDict setObject:newOutput forKey:@"Ammount"];
                [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
                [dummyDict setObject:@0 forKey:@"Total"];
                [arrVisitFrequency insertObject:dummyDict atIndex:i];
            }
        }
        
        NSLog(@"sales data %@" ,arrVisitFrequency);
        
        isEmptyGraph=NO;
        
        [self testDataGraph];
    }
}

-(void)testDataGraph
{
    if (arrVisitFrequency.count>0)
    {
        NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
        currentMonthFormatter.dateFormat=@"MMMM";
        NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
        NSLog(@"current month is %@", currentMonth);
        monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];
        
        NSMutableArray * refinedSalesArray=[[NSMutableArray alloc]init];
        
        if (arrVisitFrequency.count>0) {
            //this is for first of every month
            
            if (arrVisitFrequency.count==1) {
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:1]];
            }
            for (NSString * amountStr in [arrVisitFrequency valueForKey:@"Total"]) {
                
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
            }
        }
        
        NSMutableArray * testArray=[[NSMutableArray alloc]init];
        [testArray addObject:refinedSalesArray];
        
        NSLog(@" test array %@",testArray);
        self.data =testArray;
        
        
        NSLog(@"Vist frequency data array is %@",arrVisitFrequency);
        
        
        NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
        
        
        self.labels = labalesArray;
        
        NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));
        
        self.gkLineGraph.dataSource = self;
        self.gkLineGraph.lineWidth = 3.0;
        self.gkLineGraph.startFromZero=YES;
        self.gkLineGraph.valueLabelCount = 5;
        
        testArray = [[testArray objectAtIndex:0] valueForKeyPath:@"@distinctUnionOfObjects.self"];
        self.gkLineGraph.valueLabelCount = [testArray count];
        
        self.gkLineGraph.isMedRepDashboard = YES;
        
        [_gkLineGraph draw];
    }
}

-(void)displayEmptyDataGraph
{
    NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
    currentMonthFormatter.dateFormat=@"MMMM";
    NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
    NSLog(@"current month is %@", currentMonth);
    monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];
    
    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    
    NSMutableArray * testSalesDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * testReturnsDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * emptyGraphDataArray=[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testSalesDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testReturnsDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    NSMutableArray * testArray=[[NSMutableArray alloc]init];
    NSMutableArray * tempSalesDataArray=[[NSMutableArray alloc]init];
    NSMutableArray * tempReturnsDataArray=[[NSMutableArray alloc]init];
    
    
    for (NSString * amountStr in [testSalesDataArray valueForKey:@"Ammount"]) {
        [tempSalesDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        [emptyGraphDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        
    }
    for (NSString * amountStr in [testReturnsDataArray valueForKey:@"Ammount"]) {
        
        [tempReturnsDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        
    }
    
    [testArray addObject:tempSalesDataArray];
    [testArray addObject:tempReturnsDataArray];
    
    NSLog(@"test array for dummy graph %@ \n%@ \n%@",tempSalesDataArray,tempSalesDataArray,emptyGraphDataArray);
    
    
    [emptyGraphDataArray replaceObjectAtIndex:0 withObject:[NSDecimalNumber numberWithInteger:[@"100" integerValue]]];
    [testArray addObject:emptyGraphDataArray];
    
    
    self.data =testArray;
    
    
    NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
    self.labels = labalesArray;
    
    
    NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));
    
    self.gkLineGraph.dataSource = self;
    self.gkLineGraph.lineWidth = 3.0;
    
    self.gkLineGraph.valueLabelCount = 5;
    NSLog(@"trying to draw graph %@", self.gkLineGraph.layer.sublayers);
    [self.gkLineGraph draw];
}

-(NSMutableArray*)fetchDatesofCurrentMonthinDBFormat
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    [fmt setDateFormat:@"yyyy-MM-dd"]; // you can set this to whatever you like
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
        [dates sortUsingSelector:@selector(compare:)];
        
    }
    return dates;
}

-(NSMutableArray*)fetchDatesofCurrentMonth
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"dd"]; // you can set this to whatever you like
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        [dates sortUsingSelector:@selector(compare:)];
    }
    return dates;
}

#pragma mark - GKLineGraphDataSource

- (NSInteger)numberOfLines {
    return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index {
    
    if (isEmptyGraph==YES) {
        
        id colors = @[[UIColor clearColor],
                      [UIColor colorWithRed:(75.0/255.0) green:(143.0/255.0) blue:(249.0/255.0) alpha:1],
                      [UIColor clearColor],
                      ];
        return [colors objectAtIndex:index];
        
    }
    else{
        id colors = @[[UIColor colorWithRed:(13.0/255.0) green:(133.0/255.0) blue:(248.0/255.0) alpha:1],
                      [UIColor clearColor],
                      ];
        return [colors objectAtIndex:index];
        
    }
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index {
    
    if (isEmptyGraph==YES) {
        return [[@[@1, @1.6,@1.6] objectAtIndex:index] doubleValue];
        
    }
    else{
        return [[@[@1, @1.6] objectAtIndex:index] doubleValue];
        
    }
    
}

- (NSString *)titleForLineAtIndex:(NSInteger)index {
    
    @try {
        if ([self.labels count] > 15) {
            if ([[self.labels lastObject]integerValue] % 2 == 0) {
                // current date is even date
                
                NSInteger object = [[self.labels objectAtIndex:index]integerValue];
                if (object % 2 == 0)
                {
                    // even dates
                    return [self.labels objectAtIndex:index];
                }
                else
                {
                    return @"";
                }
            }
            else
            {
                // current date is odd date
                
                NSInteger object = [[self.labels objectAtIndex:index]integerValue];
                
                if (object % 2 != 0)
                {
                    // odd dates
                    return [self.labels objectAtIndex:index];
                }
                else
                {
                    return @"";
                }
            }
        }
        else
        {
            return [self.labels objectAtIndex:index];
        }
    } @catch (NSException *exception) {
        return @"";
        
    } @finally {
        
    }
}

#pragma mark button action
- (IBAction)btnRescheduleVisit:(id)sender {
    MedRepVisitsListViewController *visitVC = [[MedRepVisitsListViewController alloc] init];
    visitVC.selectedVisitDetailsDict = [sortedPlannedVisitArray objectAtIndex:0];
    visitVC.isFromDashboard = YES;
    [self.navigationController pushViewController:visitVC animated:YES];
}

- (void)btnTask{
    TaskPopoverViewController *presentingView = [[TaskPopoverViewController alloc]init];
    presentingView.TaskPopoverDelegate = self;
    UINavigationController *toDoPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    toDoPopUpViewController.navigationBarHidden=YES;
    toDoPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    toDoPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:toDoPopUpViewController animated:NO completion:nil];
}

#pragma mark Task Popover Delegate
- (void)updateTaskData {
    [self loadTaskData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
