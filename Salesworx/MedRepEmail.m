//
//  MedRepEmail.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/10/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepEmail.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"

@implementation MedRepEmail

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib

{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, self.frame.size.height)];
    self.leftView = paddingView;
    paddingView.userInteractionEnabled=NO;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    //  [self addObserver:self forKeyPath:@"enabled" options:0 context:nil];
    
    
    self. font=MedRepElementDescriptionLabelFont;
    self.textColor=MedRepElementDescriptionLabelColor;
    
    self.layer.borderWidth=1.0f;
    self.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    self.layer.masksToBounds = NO;
    //   self.layer.shadowColor = [UIColor blackColor].CGColor;
    //   self.layer.shadowOpacity = 0.1f;
    //   self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    //   self.layer.shadowRadius = 1.0f;
    // self.layer.shouldRasterize = YES;
    self.layer.cornerRadius=5.0;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:EMAIL_ACCEPTABLE_CHARECTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return ![string isEqualToString:filtered];
}

@end
