//
//  CustomerPriceList.h
//  SWCustomer
//
//  Created by Irfan on 10/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "CustomerPriceListCell.h"

@interface CustomerPriceList : UIView <GridViewDataSource,UIPopoverControllerDelegate>
{
   // UITableView *tableViewP;
    NSDictionary *customerP;
    NSArray *price;
    UIPopoverController *popOverController;
    CustomerHeaderView *customerHeaderView;
    UILabel *refLabel;
    UILabel *dateLabel;
    UILabel *amountLabel;
    UILabel *statusLabel;
    UILabel *priceLabel;
    GridView *gridView;
    UIPopoverController *filterPopOver;
    UIButton *filterButton;
    

}
//@property (nonatomic, strong) GridView *gridView;
//
////@property (nonatomic, strong) UITableView *tableViewP;
//@property (nonatomic, strong) NSDictionary *customerP;
//@property (nonatomic, strong) NSArray *price;
//@property (nonatomic, strong) UIPopoverController *popOverController;
//@property (nonatomic, strong) CustomerHeaderView *customerHeaderView;
//
//@property (nonatomic, strong) UILabel *refLabel;
//@property (nonatomic, strong) UILabel *dateLabel;
//@property (nonatomic, strong) UILabel *amountLabel;
//@property (nonatomic, strong) UILabel *statusLabel;
//@property (nonatomic, strong) UILabel *priceLabel;
//
//@property (nonatomic, strong) UIPopoverController *filterPopOver;
//
//@property (nonatomic, strong)  UIButton *filterButton;


- (id)initWithCustomer:(NSDictionary *)row;
- (void)loadprice;

@end
