//
//  AlSeerSignatureViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/18/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJRSignatureView.h"

@interface AlSeerSignatureViewController : UIViewController
{
    PJRSignatureView * pjrSignView;
    
    UIImageView* signImageView;
    
}


@property(strong,nonatomic)UIImage* capturedImage;

- (IBAction)closeButtonTapped:(id)sender;
- (IBAction)saveButtonTapped:(id)sender;
- (IBAction)eraseButtonTapped:(id)sender;
@end
