//
//  SurveyViewController.m
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "SurveyViewControllerOld.h"
#import "SurveyQuestionViewController.h"

@interface SurveyViewControllerOld ()

@end

@implementation SurveyViewControllerOld


#define kCustomerField 1


- (id)initWithCustomer:(NSDictionary *)c
{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        [self setTitle:NSLocalizedString(@"Survey", nil)];
        isCustomer =YES;
    
    }
    return self;
}
- (id)initWithOutCustomer
{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionary];
        [self setTitle:NSLocalizedString(@"Survey", nil)];
        isCustomer =NO;

    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
        self.view.backgroundColor = [UIColor whiteColor];
   
    
    tableViewController=[[UITableView alloc] initWithFrame:CGRectMake(30, 90, self.view.frame.size.width - 60, self.view.frame.size.height-180) style:UITableViewStyleGrouped];
    [tableViewController setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableViewController setDelegate:self];
    [tableViewController setDataSource:self];
    tableViewController.backgroundView = nil;
    tableViewController.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tableViewController];
    
    dataDict = [NSMutableDictionary dictionary];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Start Survey" style:UIBarButtonItemStyleBordered target:self action:@selector(Add:)]  animated:YES];
    
    
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [Flurry logEvent:@"Survey View"];
    //[Crittercism leaveBreadcrumb:@"<Survey View>"];

}
-(void)Add:(id)sender
{
    // Push view controller
    [self.view endEditing:YES];
    SurveyQuestionViewController *questionView = [[SurveyQuestionViewController alloc] init];
    if([customer count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select customer." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
       [alert show];
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Please select customer."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];

    }
    else
    {
        [self.navigationController pushViewController:questionView animated:YES];
    }
}
- (void)productSelected:(NSDictionary *)productDict
{
    [popoverController dismissPopoverAnimated:YES];
    [self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.10f];
}

- (void)presentProductOrder:(NSMutableDictionary *)productDict
{
    [customer removeAllObjects];
    customer=productDict;
    [tableViewController reloadData];
}


#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *IDENT = @"CashCutomerDetail";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:IDENT];
    if(cell)
    {
        cell=nil;
    }
    cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
    
    if (indexPath.row == 0) {
        ((SWTextFieldCell *)cell).label.text = NSLocalizedString(@"Survey Name", nil);
        ((SWTextFieldCell *)cell).textField.text = @"Test Survey Name";
        ((SWTextFieldCell *)cell).textField.userInteractionEnabled = NO;
    }
    else if(indexPath.row == 1)
    {
        ((SWTextFieldCell *)cell).label.text = @"Survey Description";
        ((SWTextFieldCell *)cell).textField.text = @"Test Survey Description";
        ((SWTextFieldCell *)cell).textField.userInteractionEnabled = NO;
    }
    else {
         
            ((SWTextFieldCell *)cell).textField.tag = kCustomerField;
            ((SWTextFieldCell *)cell).label.text = @"Customer Name";
            ((SWTextFieldCell *)cell).textField.placeholder = @"Tap to select Customer";
            ((SWTextFieldCell *)cell).textField.text = [customer stringForKey:@"Customer_Name"];
            ((SWTextFieldCell *)cell).textField.userInteractionEnabled = NO;
        
     
    }
    
    
    
    return cell;
}
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
        return nil;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
    
  GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Survey Details"];
    return sectionHeader;
        
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)s {
    
    
        return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 28.0f;
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 2)
    {
        if(!isCustomer)
        {
//            listView = [[GenCustListViewController alloc] init];
            listView = [[SalesWorxSurveyViewController alloc] init];
            [listView setTarget:self];
            [listView setAction:@selector(productSelected:)];
            [listView setPreferredContentSize:CGSizeMake(600, self.view.bounds.size.height - 100)];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listView];
            popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
            popoverController.delegate=self;
            UITableViewCell *cell = [tableView1 cellForRowAtIndexPath:0];
            [popoverController setDelegate:self];
            [popoverController presentPopoverFromRect:cell.frame inView:tableViewController permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    else
    {
        
    }
}
#pragma mark UITextFieldDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pV
{
    listView=nil;
    popoverController=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
    
}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
