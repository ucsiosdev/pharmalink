//
//  SurveyViewController.m
//  DataSyncApp
//
//  Created by sapna on 1/19/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "SurveyViewController.h"
#import "SurveyFormViewController.h"
#import "DataSyncManager.h"
#import "SurveyPageControlViewControl.h"
#import "CustomerResponseDetails.h"
#import "SurveyQuestionDetails.h"
#import <QuartzCore/QuartzCore.h>
#import "SWPlatform.h"
//
@interface SurveyViewController ()
{
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
}
@end

@implementation SurveyViewController
@synthesize scrollView, pageControl, viewControllers;

@synthesize surveyQuestionArray =_surveyQuestionArray;


//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set back button
    self.navigationItem.hidesBackButton = NO;
        self.view.backgroundColor = [UIColor whiteColor];

    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Survey", nil)
                                     style:UIBarButtonItemStyleBordered
                                    target:self
                                    action:@selector(btnBackPressed)];
	//self.navigationItem.leftBarButtonItem = customBarItem;

    
    //Add observer 


    
    //
    
    pageControl.pageIndicatorTintColor = [UIColor grayColor];
    pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0x4790D2);
    
    appDelegate=[DataSyncManager sharedManager];
    self.title =appDelegate.surveyDetails.Survey_Title;

    
    if(appDelegate.customerResponseArray)
        [appDelegate.customerResponseArray removeAllObjects];

    // Get survey question
    NSLog(@"survey id old %d", appDelegate.surveyDetails.Survey_ID);
    
    self.surveyQuestionArray = [[SWDatabaseManager retrieveManager] selectQuestionBySurveyId:appDelegate.surveyDetails.Survey_ID];
    appDelegate.SurveyType = appDelegate.surveyDetails.Survey_Type_Code;
    
    if ([appDelegate.SurveyType isEqualToString:@"N"])
        appDelegate.key=[[SWDatabaseManager retrieveManager] dbGetCustomerResponseCount];
    else if([appDelegate.SurveyType isEqualToString:@"A"])
        appDelegate.key=[[SWDatabaseManager retrieveManager] dbGetAuditResponseCount];
    
    //Set no. of pages 
    kNumberOfPages = [self.surveyQuestionArray count];
    appDelegate.MaxPages=kNumberOfPages-1;
   
    self.navigationController.toolbarHidden=YES;

    //
    [self setViewForpageControl];
    
}

-(void)viewDidAppear:(BOOL)animated
{
     [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationTOStartScroll:)
                                                 name:@"StartScrollNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationTOStopScroll:)
                                                 name:@"StopScrollNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForSuccessMessage:)
                                                 name:@"InsertSurveyNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForTableDataInsertion:)
                                                 name:@"TableDataInsertionNotification"
                                               object:nil];
}
-(void)btnBackPressed
{
     alertType=@"Back";
    if(appDelegate.MaxPages+1  != [appDelegate.customerResponseArray count])
    {
        if(!appDelegate.getResponseForAllQuestions)
        {
            [self UserAlert:@" Your data will not be saved without completing the survey. Would you like to stop the survey?"];
        }
        else
        {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
        }
    }
    else
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
    }
}
#pragma mark
#pragma mark SetView
-(void)setViewForpageControl
{
    @autoreleasepool {
     
    NSMutableArray *controllers = [NSMutableArray array];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
   
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
        
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = YES;
    scrollView.delegate = self;
    scrollView.scrollEnabled = TRUE;

    
        //swipe changed
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    recognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [scrollView addGestureRecognizer:recognizer];
       
    pageControl.numberOfPages = kNumberOfPages;
    pageControl.currentPage = 0;
	
    // pages are created on demand
    // load the visible page
    
    [self loadScrollViewWithPage:0];
   
    }
}
#pragma mark
#pragma mark SetPage as require
- (void)loadScrollViewWithPage:(int)page {
    
    @autoreleasepool {
        
    for(UIView *subview in [scrollView                                                                                                                                                                          subviews]) {
        [subview removeFromSuperview];
    }
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
	
    // replace the placeholder if necessary
    SurveyPageControlViewControl *controller = [viewControllers objectAtIndex:page];
    SurveyQuestionDetails *_questiondetails = [_surveyQuestionArray objectAtIndex:page];
        NSLog(@"Survey questions %@", _questiondetails.Question_Text);
        
    if ((NSNull *)controller == [NSNull null]) {
        controller = [[SurveyPageControlViewControl alloc]initWithSurveyQuestion:_questiondetails];
       
      
       
        
        [viewControllers replaceObjectAtIndex:page withObject:controller];
       
    }
	
    // add the controller's view to the scroll view with animation
    if (nil == controller.view.superview) {
       
        CGRect frame = scrollView.frame;
        controller.view.frame = frame;
        //Swipe animation
        CATransition *transition = [CATransition animation];
		// Animate over 3/4 of a second
		transition.duration = 0.75;
		// using the ease in/out timing function
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
		transition.type=kCATransitionPush;
		transition.subtype=kCATransitionFromTop;
		transition.removedOnCompletion=YES;
		[scrollView.layer addAnimation:transition forKey:nil];
        [scrollView addSubview:controller.view];
    }

    }
}

#pragma mark
#pragma mark Swipe recognizer method
-(void)handleSwipeFrom:(id)sender
{
    
   // NSLog(@"MAX PAGE %d ...... kNumberOfPages %d ..........Current page %d",appDelegate.MaxPages,kNumberOfPages,appDelegate.currentPage);

    if (pageControlUsed) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    
    if(appDelegate.isMultiResponseTable==1)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"CheckandInsertDataNotification"
         object:self];
        return;
    }
    //Set current page
    int page =appDelegate.currentPage+1;
    pageControl.currentPage = page;
    appDelegate.currentPage=page;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page];
}

#pragma mark
#pragma mark notification method
- (void) receiveNotificationTOStartScroll:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"StartScrollNotification"])
    {
        recognizer.enabled = YES;

    }
}
- (void) receiveNotificationTOStopScroll:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:@"StopScrollNotification"])
    {
       recognizer.enabled=NO;
    }
}
-(void)receiveNotificationForSuccessMessage:(NSNotification *) notification
{
 
    tempNoti = notification;
   // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnDonePressed)];
    
    pageControlUsed = YES;
    
    [saveSurvey bringSubviewToFront:self.view];
    saveSurvey.hidden=NO;
}
-(IBAction)SaveSurveyAction:(id)sender
{
    [self btnDonePressed];
}

-(void)btnDonePressed
{
    [self currentLocation];
    if ([[tempNoti name] isEqualToString:@"InsertSurveyNotification"])
    {
        appDelegate.getResponseForAllQuestions=YES;

        
        NSLog(@"customer response array is %@", [appDelegate.customerResponseArray description]);
        
        alertType=@"Notofication";
        for(int i=0;i<[appDelegate.customerResponseArray count];i++)
        {
            NSString *strquery;
            if([appDelegate.SurveyType isEqualToString:@"N"])
            {
                NSMutableDictionary *indertDict = [NSMutableDictionary dictionaryWithDictionary:[appDelegate.customerResponseArray objectAtIndex:i]];
                strquery = [NSString stringWithFormat:@"insert into TBL_Survey_Cust_Responses(Customer_Survey_ID,Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('%@','%d','%d','%@','%d','%d','%d','%@','%@')",[NSString createGuid],[[indertDict stringForKey:@"Survey_ID"] intValue],[[indertDict stringForKey:@"Question_ID"] intValue],[indertDict stringForKey:@"Response"],[[indertDict stringForKey:@"Customer_ID"] intValue],[[indertDict stringForKey:@"Site_Use_ID"] intValue],[[indertDict stringForKey:@"SalesRep_ID"] intValue],[indertDict stringForKey:@"Emp_Code"],[indertDict stringForKey:@"Survey_Timestamp"]];
            }
            else if (_isBrandAmbassadorVisit)
            {
                
//                NSMutableDictionary *indertDict = [NSMutableDictionary dictionaryWithDictionary:[appDelegate.customerResponseArray objectAtIndex:i]];
//                strquery = [NSString stringWithFormat:@"insert into TBL_Survey_Walkin_Responses(Walkin_Response_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Actual_Visit_ID) Values('%@','%@','%@','%@','%@','%@','%@','%@')",[indertDict stringForKey:@"Audit_Survey_ID"],[indertDict stringForKey:@"Survey_ID"],[indertDict stringForKey:@"Question_ID"],[indertDict stringForKey:@"Response"],[indertDict stringForKey:@"SalesRep_ID"],[indertDict stringForKey:@"Survey_Timestamp"],[indertDict stringForKey:@"Surveyed_By"],self.currentVisit.vi];
                
            }
            else
            {
                NSMutableDictionary *indertDict = [NSMutableDictionary dictionaryWithDictionary:[appDelegate.customerResponseArray objectAtIndex:i]];
                strquery = [NSString stringWithFormat:@"insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By) Values('%@','%@','%@','%@','%@','%@','%@','%@')",[indertDict stringForKey:@"Audit_Survey_ID"],[indertDict stringForKey:@"Survey_ID"],[indertDict stringForKey:@"Question_ID"],[indertDict stringForKey:@"Response"],[indertDict stringForKey:@"SalesRep_ID"],[indertDict stringForKey:@"Emp_Code"],[indertDict stringForKey:@"Survey_Timestamp"],[indertDict stringForKey:@"Surveyed_By"]];
                
                }
            [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:strquery];
        }
        
        NSString *strquery;
        if([appDelegate.SurveyType isEqualToString:@"N"])
        {
            NSMutableDictionary *indertDict = [NSMutableDictionary dictionaryWithDictionary:[appDelegate.customerResponseArray objectAtIndex:0]];
            
            //why is survey ID hard coded as 0, y same query called twice
            
            strquery = [NSString stringWithFormat:@"insert into TBL_Survey_Cust_Responses(Customer_Survey_ID,Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('%@','%d','%@','%@','%d','%d','%d','%@','%@')",[NSString createGuid],[[indertDict stringForKey:@"Survey_ID"] intValue],@"0" ,[NSString stringWithFormat:@"%@,%@",latitude,longitude],[[indertDict stringForKey:@"Customer_ID"] intValue],[[indertDict stringForKey:@"Site_Use_ID"] intValue],[[indertDict stringForKey:@"SalesRep_ID"] intValue],[indertDict stringForKey:@"Emp_Code"],[indertDict stringForKey:@"Survey_Timestamp"]];
        }
        else
        {
            NSMutableDictionary *indertDict = [NSMutableDictionary dictionaryWithDictionary:[appDelegate.customerResponseArray objectAtIndex:0]];
            strquery = [NSString stringWithFormat:@"insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By) Values('%@','%@','%@','%@','%@','%@','%@','%@')",[indertDict stringForKey:@"Audit_Survey_ID"],[indertDict stringForKey:@"Survey_ID"],@"0",[NSString stringWithFormat:@"%@,%@",latitude,longitude],[indertDict stringForKey:@"SalesRep_ID"],[indertDict stringForKey:@"Emp_Code"],[indertDict stringForKey:@"Survey_Timestamp"],[indertDict stringForKey:@"Surveyed_By"]];
            
        }
        [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:strquery];

        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InsertSurveyNotification" object:nil];
        [self UserAlert:@"Survey saved successfully."];
    }
    
}

-(void)receiveNotificationForTableDataInsertion:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"TableDataInsertionNotification"])
    {
        //Set current page
        int page =appDelegate.currentPage+1;
        pageControl.currentPage = page;
        appDelegate.currentPage=page;
        
        // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
        [self loadScrollViewWithPage:page];
        
    }
    
}

#pragma mark
#pragma mark alertView methods
-(void)UserAlert:(NSString *)Message
{
//    [ILAlertView showWithTitle:NSLocalizedString(@"Message", nil)
//                       message:Message
//              closeButtonTitle:NSLocalizedString(@"OK", nil)
//             secondButtonTitle:nil
//           tappedButtonAtIndex:nil];
   UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil,nil];
    [ErrorAlert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==1) {
       if( appDelegate.customerResponseArray)
       {
           [appDelegate.customerResponseArray removeAllObjects];
       }
        //
        appDelegate.alertMessageShown=0;
        appDelegate.currentPage=0;
        appDelegate.MaxPages=0;
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
    else{
        if([alertType isEqualToString:@"Notofication"])
        {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:([self.navigationController.viewControllers count]-3)] animated:YES];
        }
    }
}
#pragma mark
- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:@"StartScrollNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:@"StopScrollNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:@"InsertSurveyNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:@"TableDataInsertionNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
-(void)viewDidUnload
{
    
}

- (void)currentLocation
{
    for (int i= 0; i <=3; i++) {
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        locationManager.distanceFilter = kCLDistanceFilterNone;

    
        [locationManager startUpdatingLocation];
        CLLocation *location = [locationManager location];
        CLLocationCoordinate2D coordinate = [location coordinate];
        latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        [locationManager stopUpdatingLocation];
        if ([latitude isEqualToString:@"0"] || [longitude isEqualToString:@"0"]) {
        }
        else
        {
            break;
        }
    }
    //  Latitude = "25.304524";
    //  Longitude = "55.373513";
    //  NSLog(@"latitude %@",latitude);
    //  NSLog(@"longitude %@",longitude);
}
@end
