

#import "SurveyPageControlViewControl.h"
#import <QuartzCore/QuartzCore.h>
#import "SurveyResponseDetails.h"
#import "AppConstantsList.h"
#import "CustomerResponseDetails.h"
#import "AuditResponseDetail.h"
#import "DataSyncManager.h"

@implementation SurveyPageControlViewControl
@synthesize surveyResponseArray =_surveyResponseArray;
@synthesize pageNumberLabel;



// Load the view nib and initialize the pageNumber ivar.
- (id)initWithSurveyQuestion:(SurveyQuestionDetails *)Survey_Question {
    if (self = [super initWithNibName:@"SurveyPageControlViewControl" bundle:nil]) {
        _surveyQuestion=Survey_Question;
        
      
        
        
        
       // self.surveyResponseArray=[[NSMutableArray alloc]init];
        
        NSLog(@"question id in old is %d", Survey_Question.Question_ID);

         self.surveyResponseArray  = [[SWDatabaseManager retrieveManager] selectResponseType:Survey_Question.Question_ID];
        
        
        
        

        NSArray* push=self.surveyResponseArray;
        
        NSLog(@"push %@", [push description]);
        
       
        
        
    }
    return self;
}

// Set the label and background color when the view has finished loading.
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    
    
    
    
    //Post notification to stop the swipe 
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"StopScrollNotification"
     object:self];
    
        self.view.backgroundColor = [UIColor whiteColor];
    //
    appDelegate=[DataSyncManager sharedManager];
    appDelegate.getResponseForAllQuestions=NO;
    //
    surveyVC =[[SurveyViewController alloc] init];
    strResponse=[[NSString alloc] init];
    selectedIndexes=[NSMutableArray array];
    //question text
    lblQuestion.text = [NSString stringWithFormat:@"  %@",_surveyQuestion.Question_Text] ;
    [self setViewByResponseType];
    
    lblQuestion.layer.borderColor = [UIColor lightGrayColor].CGColor;
    lblQuestion.layer.borderWidth = 1.0;
    
    lblSliderValue.layer.borderColor = [UIColor lightGrayColor].CGColor;
    lblSliderValue.layer.borderWidth = 1.0;
    
    [lblAnswer setBackgroundColor:[UIColor clearColor]];
    [lblAnswer setTextColor:UIColorFromRGB(0x4E566C)];
    [lblAnswer setShadowOffset:CGSizeMake(0, 1)];
    [lblAnswer setShadowColor:[UIColor whiteColor]];
    [lblAnswer setFont:BoldSemiFontOfSize(18.0f)];
    [lblAnswer setNumberOfLines:0];
    [lblAnswer setLineBreakMode:NSLineBreakByWordWrapping];
    
    [staticQuestion setBackgroundColor:[UIColor clearColor]];
    [staticQuestion setTextColor:UIColorFromRGB(0x4E566C)];
    [staticQuestion setShadowOffset:CGSizeMake(0, 1)];
    [staticQuestion setShadowColor:[UIColor whiteColor]];
    [staticQuestion setFont:BoldSemiFontOfSize(18.0f)];
    [staticQuestion setNumberOfLines:0];
    [staticQuestion setLineBreakMode:NSLineBreakByWordWrapping];
}
#pragma mark
#pragma mark SetView method
-(void)setViewByResponseType
{
    @autoreleasepool {
       
    UIView *previousView =[self.view viewWithTag:111];
    [previousView removeFromSuperview];
    CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(50, 00, self.view.bounds.size.width, 60) andCustomer:[SWDefaults customer]] ;
        
    [self.view addSubview:headerView];
    SurveyResponseDetails *info =[self.surveyResponseArray objectAtIndex:0];
    ResponseId=info.Response_Type_ID;
       
        NSLog(@"response id old %d", ResponseId);
        
        NSLog(@"response type is %d", info.Response_Type_ID);
        
        NSLog(@"survey answer old %@", info.Response_Text);
        
    switch (ResponseId) {
        case 1:{
            // Text response
             appDelegate.isMultiResponseTable=0;
            txtViewResponse =[[UITextView alloc] initWithFrame:CGRectMake(50,250 ,925,100 )];
           
            
            txtViewResponse.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
            
            [txtViewResponse setFont:RegularFontOfSize(16)];
            txtViewResponse.tag=111;
            txtViewResponse.returnKeyType =UIReturnKeyNext;
            txtViewResponse.editable=YES;
            txtViewResponse.delegate=self;
            txtViewResponse.layer.borderColor = [UIColor lightGrayColor].CGColor;
            txtViewResponse.layer.borderWidth = 1.0;
            [self.view addSubview:txtViewResponse];
            
        }
            break;
        case 2:
        {
            // Single response
             appDelegate.isMultiResponseTable=1;
            lblAnswer.hidden=YES;
            lblQuestion.hidden=YES;
            lblSliderValue.hidden = YES;

            staticQuestion.hidden=YES;
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(CheckandInsertData:)
                                                         name:@"CheckandInsertDataNotification"
                                                       object:nil];

            tblResponse=[[UITableView alloc] initWithFrame:CGRectMake(0,100 ,1000,500 ) style:UITableViewStyleGrouped];
            tblResponse.bounces=NO;
            [tblResponse setBackgroundView:Nil];
            tblResponse.backgroundView = nil;
            [tblResponse setBackgroundColor:[UIColor whiteColor]];
            tblResponse.tag=111;
            tblResponse.delegate=self;
            tblResponse.dataSource=self;
            [self.view addSubview:tblResponse];
        }
            break;
        case 3:{
            //Multiple Response
            lblAnswer.hidden=YES;
            lblQuestion.hidden=YES;
            staticQuestion.hidden=YES;
            lblSliderValue.hidden = YES;

            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(CheckandInsertData:)
                                                         name:@"CheckandInsertDataNotification"
                                                       object:nil];

            appDelegate.isMultiResponseTable=1;
            tblResponse=[[UITableView alloc] initWithFrame:CGRectMake(0,100 ,1000,500 ) style:UITableViewStyleGrouped];
            tblResponse.bounces=NO;
             [tblResponse setBackgroundView:Nil];
            tblResponse.backgroundColor=[UIColor whiteColor];
            tblResponse.tag=111;
            tblResponse.delegate=self;
            tblResponse.dataSource=self;
            [self.view addSubview:tblResponse];
        }
            
            break;
        case 4:
        {
            //Slider Response
            
             appDelegate.isMultiResponseTable=0;
            int responseCount= [self.surveyResponseArray count]-1;
            sliderResponse =[[UISlider alloc] initWithFrame:CGRectMake(50,294 ,925 ,44 )];
            sliderResponse.tag=111;
            [sliderResponse addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
            sliderResponse.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
            sliderResponse.layer.borderColor = [UIColor lightGrayColor].CGColor;
            sliderResponse.layer.borderWidth = 1.0;
            
            [self.view addSubview:sliderResponse];
            [sliderResponse setMinimumValue:0.0];
            [sliderResponse setMaximumValue:responseCount];
             sliderResponse.userInteractionEnabled=TRUE;
            sliderResponse.continuous = NO;
            sliderResponse.value = 0.0;
        }
            
            break;
        case 5:
        {
            //Rating Response
             appDelegate.isMultiResponseTable=0;
            RateView *rateView =[[RateView alloc] initWithFrame:CGRectMake(20,200 ,489 , 95)];
            [self.view addSubview:rateView];
            
            rateView.notSelectedImage = [UIImage imageNamed:@"kermit_empty.png"cache:NO];
            rateView.halfSelectedImage = [UIImage imageNamed:@"kermit_half.png"cache:NO];
            rateView.fullSelectedImage = [UIImage imageNamed:@"kermit_full.png"cache:NO];
            
            rateView.editable = YES;
            rateView.maxRating = 5;
            rateView.delegate = self;
            strRate=[NSString stringWithFormat:@"%f",rateView.rating];
            for(int i=0;i<[self.surveyResponseArray count];i++)
            {
                SurveyResponseDetails *info =[self.surveyResponseArray objectAtIndex:i];
                
                if(_surveyQuestion.Default_Response_ID ==info.Response_ID){
                    appDelegate.isDefaultValueUsed=1;
                    rateView.rating= [info.Response_Text floatValue];
                    rateView.rating = rateView.rating;
                    strRate=[NSString stringWithFormat:@"%f",rateView.rating];
                    NSLog(@"setCustomerResponse 1");
                    [self setCustomerResponse];
                    return;
                }
                else{
                    appDelegate.isDefaultValueUsed=1;
                    rateView.rating = 0;
                }
            }
            
        }
            
            break;
        default:
            break;
        }
    }

}

#pragma mark -
#pragma mark Table view data source
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
    
    if(s==0)
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Question"];

        return sectionHeader;
    }
   else
   {
     GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Answer"];
       
       UIImageView *multiImage = [[UIImageView alloc] initWithFrame:CGRectMake(sectionHeader.frame.size.width-40, 0, 30, 30)];
       if (ResponseId==2)
       {
           multiImage.image = [UIImage imageNamed:@"Old_SurveySingleSelect"];
       }
       else
       {
           multiImage.image = [UIImage imageNamed:@"Old_SurveyMultiSelect"];
       }
       [sectionHeader addSubview:multiImage];

       return sectionHeader;
   }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if(section==0)
    {
        return 1;
    }
    else
	return [self.surveyResponseArray count];
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @autoreleasepool {
    NSString *SimpleTableIdentifier = @"SimpleTableIdentifierevent";
        
		UITableViewCell *cell1= [tableView dequeueReusableCellWithIdentifier:SimpleTableIdentifier];
		
		
		if (cell1 == nil ) {
            cell1 = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:SimpleTableIdentifier];
            cell1.textLabel.font = RegularFontOfSize(16);

        }
        
    if(indexPath.section==0)
    {
        cell1.textLabel.text =_surveyQuestion.Question_Text;
        return  cell1;
    }
    else
    {
        cell1.selectionStyle=UITableViewCellEditingStyleNone;
        SurveyResponseDetails *info =[self.surveyResponseArray objectAtIndex:indexPath.row];
        cell1.textLabel.text =info.Response_Text;
        //Check if default respose id from survey question matches with the answer response
        if(_surveyQuestion.Default_Response_ID ==info.Response_ID)
        {
            lastIndexPath=indexPath;
            [[NSNotificationCenter defaultCenter]
            postNotificationName:@"StartScrollNotification" object:self];
            [cell1  setAccessoryType:UITableViewCellAccessoryCheckmark];
            [selectedIndexes addObject:[self.surveyResponseArray objectAtIndex:indexPath.row]];
        }
        return  cell1;
    }
    }
}
#pragma mark
#pragma mark TableView Datasource method
// survey response handler
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!indexPath.section==0)
    {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        if(ResponseId==2){
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"StartScrollNotification"
             object:self];
                if([tableView cellForRowAtIndexPath:indexPath].accessoryType==UITableViewCellAccessoryCheckmark){
                     
                if([selectedIndexes containsObject: [self.surveyResponseArray objectAtIndex:indexPath.row]])
                    {
                        [selectedIndexes removeObject:[self.surveyResponseArray objectAtIndex:indexPath.row]];
                    }
            
                    [tableView cellForRowAtIndexPath:indexPath].accessoryType=UITableViewCellAccessoryNone;

                }
                 else{
                     
                     if(lastIndexPath)
                         [tableView cellForRowAtIndexPath:lastIndexPath].accessoryType=UITableViewCellAccessoryNone;
                     
                     if(selectedIndexes)
                         [selectedIndexes removeAllObjects];
                     [selectedIndexes insertObject:[self.surveyResponseArray objectAtIndex:indexPath.row] atIndex:0];
                  
                     NSLog(@"survey response array desc in survey page control %@", [self.surveyResponseArray description]);
                     
                  
                     
                     
                     
                     
                     [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
                   
                     lastIndexPath=indexPath;
                    
                }

             }
        else{
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"StartScrollNotification"
             object:self];
            
            if([tableView cellForRowAtIndexPath:indexPath].accessoryType==UITableViewCellAccessoryNone){
            
                [selectedIndexes addObject:[self.surveyResponseArray objectAtIndex:indexPath.row]];
                [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
                SurveyResponseDetails *info =[self.surveyResponseArray objectAtIndex:indexPath.row];
                strTableResponse = [NSString stringWithFormat:@"%d",info.Response_ID];
            }
            else{
          
                if([selectedIndexes containsObject: [self.surveyResponseArray objectAtIndex:indexPath.row]])
                {
                    [selectedIndexes removeObject:[self.surveyResponseArray objectAtIndex:indexPath.row]];
                }
               [tableView cellForRowAtIndexPath:indexPath].accessoryType=UITableViewCellAccessoryNone;
              

            }

        }
    }
}

#pragma mark
#pragma mark Slider action method
-(IBAction)sliderValueChanged:(UISlider*)sender
{
    int responseValue= [sender value];
    
    if (sliderValueUsed==0) {
        SurveyResponseDetails *info =[self.surveyResponseArray objectAtIndex:responseValue];
        
        lblSliderValue.text=[NSString stringWithFormat:@"  %@",info.Response_Text] ;
        NSLog(@"setCustomerResponse 2");
        [self setCustomerResponse];
        sliderValueUsed=1;
    }
    else{
        SurveyResponseDetails *info =[self.surveyResponseArray objectAtIndex:responseValue];
        
        lblSliderValue.text=[NSString stringWithFormat:@"  %@",info.Response_Text] ;
        appDelegate.key--;
        [appDelegate.customerResponseArray removeLastObject];
        NSLog(@"setCustomerResponse 3");
        [self setCustomerResponse];
        
    }
    
}

#pragma mark
#pragma mark Rating Delegate method
- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
   
    strRate=[NSString stringWithFormat:@"%f", rating];
    if(appDelegate.isDefaultValueUsed==1)
    {
        appDelegate.key--;
        [appDelegate.customerResponseArray removeLastObject];
        NSLog(@"setCustomerResponse 4");
        [self setCustomerResponse];
    }
    else{
        appDelegate.isDefaultValueUsed=0;
        NSLog(@"setCustomerResponse 5");
         [self setCustomerResponse];
    }
   
}

#pragma mark
#pragma mark TextView delegate method
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]) {
        txtViewResponse.editable=NO;
        NSLog(@"setCustomerResponse 6");
        [self setCustomerResponse];
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    textView.text=@"";
}

#pragma mark
#pragma mark Save response method
//If  response type is table then this method will call to insert data
-(void)CheckandInsertData:(id)sender{

    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];
    if([selectedIndexes count]<=0)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"StopScrollNotification" object:self];
        [self UserAlert:@"Please select any option"];
        return;
    }
        
    appDelegate.isMultiResponseTable=0;
    if([appDelegate.SurveyType isEqualToString:@"N"]) {
        for (int i=0; i<[selectedIndexes count]; i++) {
            
            NSString *strCustomer_survey_ID=[NSString stringWithFormat:@"%d",appDelegate.key];

            SurveyResponseDetails *Surveyinfo =[selectedIndexes objectAtIndex:i];
            
            //strResponse=Surveyinfo.Response_Text;
            strResponse = [NSString stringWithFormat:@"%d",Surveyinfo.Response_ID];

            CustomerResponseDetails *info =[[CustomerResponseDetails alloc] initWithCustomer_Survey_ID:strCustomer_survey_ID Survey_ID:appDelegate.surveyDetails.Survey_ID Question_ID:_surveyQuestion.Question_ID Response:strResponse Customer_ID:appDelegate.surveyDetails.Customer_ID Site_Use_ID:appDelegate.surveyDetails.Site_Use_ID SalesRep_ID:appDelegate.surveyDetails.SalesRep_ID Emp_Code:[[SWDefaults userProfile]stringForKey:@"Emp_Code"] Survey_Timestamp:dateString];
            
            NSMutableDictionary *objectDict = [NSMutableDictionary dictionary];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Survey_ID] forKey:@"Survey_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Customer_ID] forKey:@"Customer_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Site_Use_ID] forKey:@"Site_Use_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.SalesRep_ID] forKey:@"SalesRep_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Question_ID] forKey:@"Question_ID"];
            [objectDict setValue:info.Response forKey:@"Response"];
            [objectDict setValue:info.Emp_Code forKey:@"Emp_Code"];
            [objectDict setValue:info.Survey_Timestamp forKey:@"Survey_Timestamp"];
            [objectDict setValue:info.Customer_Survey_ID forKey:@"Customer_Survey_ID"];
            
     
            
            
            [appDelegate.customerResponseArray addObject:objectDict];
            appDelegate.key++;
        }
    
    }
    else{
        for (int i=0; i<[selectedIndexes count]; i++) {
            NSString *strCustomer_survey_ID=[NSString stringWithFormat:@"%d",appDelegate.key];
            SurveyResponseDetails *Surveyinfo =[selectedIndexes objectAtIndex:i];
            //strResponse=Surveyinfo.Response_Text;
            strResponse = [NSString stringWithFormat:@"%d",Surveyinfo.Response_ID];

            

        AuditResponseDetail *info =[[AuditResponseDetail alloc] initWithAudit_Survey_ID:strCustomer_survey_ID Survey_ID:appDelegate.surveyDetails.Survey_ID Question_ID:_surveyQuestion.Question_ID Response:strResponse Customer_ID:appDelegate.surveyDetails.Customer_ID _Surveyed_By:SurveyedBy SalesRep_ID:appDelegate.surveyDetails.SalesRep_ID Emp_Code:[[SWDefaults userProfile]stringForKey:@"Emp_Code"] Survey_Timestamp:dateString];
            
            NSMutableDictionary *objectDict = [NSMutableDictionary dictionary];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Survey_ID] forKey:@"Survey_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Customer_ID] forKey:@"Customer_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Surveyed_By] forKey:@"Surveyed_By"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.SalesRep_ID] forKey:@"SalesRep_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Question_ID] forKey:@"Question_ID"];
            [objectDict setValue:info.Response forKey:@"Response"];
            [objectDict setValue:info.Emp_Code forKey:@"Emp_Code"];
            [objectDict setValue:info.Survey_Timestamp forKey:@"Survey_Timestamp"];
            [objectDict setValue:info.Audit_Survey_ID forKey:@"Audit_Survey_ID"];
       
          
            
            [appDelegate.customerResponseArray addObject:objectDict];
            appDelegate.key++;
        }
    }
   
    if(appDelegate.currentPage ==appDelegate.MaxPages)
    {
        // NSLog(@"setCustomerResponse 7");
        // [self setCustomerResponse];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"InsertSurveyNotification" object:self];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"TableDataInsertionNotification" object:self];
    }
    formater=nil;
    usLocale=nil;
}
// General response handler method
- (void)setCustomerResponse{
    NSLog(@"setCustomerResponse Method");
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"StartScrollNotification"
     object:self];
    
    switch (ResponseId) {
        case 1:
            strResponse =txtViewResponse.text;

            break;
        case 2:
            strResponse =strTableResponse;

            break;
        case 3:
            strResponse =strTableResponse;

            break;
        case 4:
            strResponse =lblSliderValue.text;

            break;
        case 5:
            strResponse =strRate;

            
            break;
        default:break;
    }
    
    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];
    NSLog(@"strResponse %@",strResponse);    //Add Response to array
    if(appDelegate.isMultiResponseTable!=1)
    {
        NSString *strCustomer_survey_ID=[NSString stringWithFormat:@"%d",appDelegate.key];
        

        if([appDelegate.SurveyType isEqualToString:@"N"]) {
        CustomerResponseDetails *info =[[CustomerResponseDetails alloc] initWithCustomer_Survey_ID:strCustomer_survey_ID Survey_ID:appDelegate.surveyDetails.Survey_ID Question_ID:_surveyQuestion.Question_ID Response:strResponse Customer_ID:appDelegate.surveyDetails.Customer_ID Site_Use_ID:appDelegate.surveyDetails.Site_Use_ID SalesRep_ID:appDelegate.surveyDetails.SalesRep_ID Emp_Code:[[SWDefaults userProfile]stringForKey:@"Emp_Code"] Survey_Timestamp:dateString];
        
            NSMutableDictionary *objectDict = [NSMutableDictionary dictionary];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Survey_ID] forKey:@"Survey_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Customer_ID] forKey:@"Customer_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Site_Use_ID] forKey:@"Site_Use_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.SalesRep_ID] forKey:@"SalesRep_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Question_ID] forKey:@"Question_ID"];
            [objectDict setValue:info.Response forKey:@"Response"];
            [objectDict setValue:info.Emp_Code forKey:@"Emp_Code"];
            [objectDict setValue:info.Survey_Timestamp forKey:@"Survey_Timestamp"];
            [objectDict setValue:info.Customer_Survey_ID forKey:@"Customer_Survey_ID"];
            
            [appDelegate.customerResponseArray addObject:objectDict];
            
        
        }
        else{
            AuditResponseDetail *info =[[AuditResponseDetail alloc] initWithAudit_Survey_ID:strCustomer_survey_ID Survey_ID:appDelegate.surveyDetails.Survey_ID Question_ID:_surveyQuestion.Question_ID Response:strResponse Customer_ID:appDelegate.surveyDetails.Customer_ID _Surveyed_By:SurveyedBy SalesRep_ID:appDelegate.surveyDetails.SalesRep_ID Emp_Code:[[SWDefaults userProfile]stringForKey:@"Emp_Code"] Survey_Timestamp:dateString];
            
            NSMutableDictionary *objectDict = [NSMutableDictionary dictionary];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Survey_ID] forKey:@"Survey_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Customer_ID] forKey:@"Customer_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Surveyed_By] forKey:@"Surveyed_By"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.SalesRep_ID] forKey:@"SalesRep_ID"];
            [objectDict setValue:[NSString stringWithFormat:@"%d",info.Question_ID] forKey:@"Question_ID"];
            [objectDict setValue:info.Response forKey:@"Response"];
            [objectDict setValue:info.Emp_Code forKey:@"Emp_Code"];
            [objectDict setValue:info.Survey_Timestamp forKey:@"Survey_Timestamp"];
            [objectDict setValue:info.Audit_Survey_ID forKey:@"Audit_Survey_ID"];
          
            [appDelegate.customerResponseArray addObject:objectDict];
        }
        
         appDelegate.key++;
    }
    if(appDelegate.currentPage==appDelegate.MaxPages)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"InsertSurveyNotification"
         object:self];
    }
    formater=nil;
    usLocale=nil;
}
- (void)UserAlert:(NSString *)Message{
    UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message",nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
    [ErrorAlert show];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
-(void)viewDidDisappear:(BOOL)animated
{
    if(txtViewResponse)
        txtViewResponse=nil;
    if(sliderResponse)
        sliderResponse=nil;
    if(tblResponse)
        tblResponse=nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidUnload{
    if(txtViewResponse)
        txtViewResponse=nil;
    if(sliderResponse)
        sliderResponse=nil;
    if(tblResponse)
        tblResponse=nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}


@end
