//
//  SurveyFormViewController.h
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSyncManager.h"
#import "FTPBaseViewController.h"
@interface SurveyFormViewController : FTPBaseViewController
{
    
    DataSyncManager *appDelegate;
    IBOutlet UITableView *tblView;
    NSArray *SurveyArray;
    NSMutableDictionary *customer;
    UIView *myBackgroundView;
    
    //sample button
    
    UIButton * surveyBtn;
     SurveyDetails *infoForNewSurvey;
}
@property(nonatomic,strong) NSArray *SurveyArray;

@property(nonatomic,strong) NSMutableDictionary *customer;
@property(strong,nonatomic) NSString* surveyParentLocation;
-(void)ShowSurvey;
@end
