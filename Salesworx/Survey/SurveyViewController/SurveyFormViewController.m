//
//  SurveyFormViewController.m
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "SurveyFormViewController.h"
#import "SurveyViewController.h"
#import "SurveyDetails.h"
#import "DataSyncManager.h"
#import "SWPlatform.h"
#import "NewSurveyViewController.h"

#import "SalesWorxFeedbackViewController.h"

@interface SurveyFormViewController ()

@end

@implementation SurveyFormViewController
@synthesize SurveyArray,customer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title =NSLocalizedString(@"Survey", nil);
        self.view.backgroundColor = [UIColor whiteColor];
    //Set back button
    self.navigationItem.hidesBackButton = NO;
    tblView.backgroundView = nil;
    tblView.backgroundColor = [UIColor whiteColor];
    //Set Page count
    appDelegate=[DataSyncManager sharedManager];
    appDelegate.alertMessageShown=0;
    appDelegate.currentPage=0;
    appDelegate.MaxPages=0;
    
    
    
    //creating a sample button on tool bar
    
    surveyBtn=[[UIButton alloc]initWithFrame:CGRectMake(700, 500, 60, 30)];
    
    surveyBtn.backgroundColor=[UIColor redColor];
    
    [surveyBtn addTarget:self action:@selector(ShowSurvey) forControlEvents:UIControlEventTouchUpInside];
    
    
    //removing the sample button
    //[self.view addSubview:surveyBtn];
    
    
    
    self.SurveyArray =[[SWDatabaseManager retrieveManager] selectSurveyWithCustomerID:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Site_Use_ID"]];
    
    
   
    
    CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(50, 0, self.view.bounds.size.width, 60) andCustomer:[SWDefaults customer]] ;
    
    [self.view addSubview:headerView];
    
    
}


-(void)ShowSurvey
{
    
    NSLog(@"button taped");
    
    if(![self.SurveyArray count]==0)
    {
       
        
        for (int i=0; i<[SurveyArray count]; i++) {
             infoForNewSurvey=[self.SurveyArray objectAtIndex:i];
        }
        
        
        
        NSArray *tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Site_Use_ID"] ansSurveyID:[NSString stringWithFormat:@"%d",infoForNewSurvey.Survey_ID]];
        
        NSLog(@"survey id before pushing %d", infoForNewSurvey.Survey_ID);
        
        
        
        NSLog(@"temp array before pushing %@", [self.SurveyArray description]);
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        
        if([tempArray count]==0)
        {
            SurveyViewController *objSurveyVC =[[SurveyViewController alloc] initWithNibName:@"SurveyViewController" bundle:nil];
//            appDelegate.surveyDetails=[self.SurveyArray objectAtIndex:indexPath.row];
//            [self.navigationController pushViewController:objSurveyVC animated:YES];
        }
        else
        {
            NSMutableDictionary *tempDictionary=[tempArray objectAtIndex:0];
            NSDateFormatter* df = [NSDateFormatter new];
            [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [df setLocale:usLocale];
            NSDate *selectedDate = [df dateFromString:[tempDictionary stringForKey:@"Start_Time"]];
            int noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
            df=nil;
            usLocale=nil;
            if(noOfDays <= 0)
            {
                
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Survey has been already submitted for this customer", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
            }
            else
            {
//                SurveyViewController *objSurveyVC =[[SurveyViewController alloc] initWithNibName:@"SurveyViewController" bundle:nil];
//                appDelegate.surveyDetails=[self.SurveyArray objectAtIndex:indexPath.row];
//                [self.navigationController pushViewController:objSurveyVC animated:YES];
            }
        }
        
    }
    
    
    //NewSurveyViewController* newSurveyVC=[[NewSurveyViewController alloc]init];
    SalesWorxFeedbackViewController *newSurveyVC = [[SalesWorxFeedbackViewController alloc]init];
    
    newSurveyVC.survetIDNew=infoForNewSurvey.Survey_ID;
   // [self presentViewController:newSurveyVC animated:YES completion:nil];
    
    [self.navigationController pushViewController:newSurveyVC animated:YES];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.toolbarHidden=YES;

}
-(void)btnBackPressed
{

    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];

}
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([self.SurveyArray count]==0)
    {
        return 1;
    }
    else
    {
        return [self.SurveyArray count];

    }

    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    	return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
    
    NSString *title =  NSLocalizedString(@"Survey Name", nil);;
    
    GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:title] ;
    return sectionHeader;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath


// Customize the appearance of table view cells.
{
    @autoreleasepool
    {
        NSString *CellIdentifier = @"CustomerDetailViewCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
            [cell.textLabel setFont:LightFontOfSize(14.0f)];

        }
        
        
        

    
    if([self.SurveyArray count] ==0)

        {
            [cell.textLabel setText:NSLocalizedString(@"There is no survey for this customer", nil)];
        }
        else
        {
            SurveyDetails *info =[self.SurveyArray objectAtIndex:indexPath.row];
            [cell.textLabel setText:info.Survey_Title];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    return cell;
            
    }
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if(![self.SurveyArray count]==0)
    {

    
        NSLog(@"survey array in first %@", [SurveyArray description]);
        
            SurveyDetails *info =[self.SurveyArray objectAtIndex:indexPath.row];
        
       
        
        NSLog(@"sales rep id is %d", appDelegate.surveyDetails.SalesRep_ID);
        
        
        
        NSLog(@"info %d, %@, %@", info.Survey_ID,info.Survey_Title,info.Survey_Type_Code);
        
            NSArray *tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:[customer stringForKey:@"Customer_ID"] andSiteUseID:[customer stringForKey:@"Site_Use_ID"] ansSurveyID:[NSString stringWithFormat:@"%d",info.Survey_ID]];
        
        NSLog(@"temp array in did select %@", [tempArray description]);
        
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
                    
            if([tempArray count]==0)
            {
                SurveyViewController *objSurveyVC =[[SurveyViewController alloc] initWithNibName:@"SurveyViewController" bundle:nil];
                appDelegate.surveyDetails=[self.SurveyArray objectAtIndex:indexPath.row];
                
                
                 NSLog(@"survey old  id %d", appDelegate.surveyDetails.Survey_ID);
                
                NSLog(@"sales rep ID %d", appDelegate.surveyDetails.SalesRep_ID);
                
                NSLog(@"Emp code %@", [[SWDefaults userProfile]stringForKey:@"Emp_Code"]);
                
                
                NSDate *date =[NSDate date];
                NSDateFormatter *formater =[NSDateFormatter new];
                [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [formater setLocale:usLocale];
                NSString *dateString = [formater stringFromDate:date];
                
                NSLog(@"Survey Time Stamp is %@", dateString);
                
                
                
                 if([info.Survey_Type_Code isEqualToString:@"N"]) {
                     NSLog(@"app delegate key %d", appDelegate.key);
                 }
                
                //NewSurveyViewController* newSurveyVC=[[NewSurveyViewController alloc]init];
                SalesWorxFeedbackViewController *newSurveyVC = [[SalesWorxFeedbackViewController alloc]init];
                if (self.surveyParentLocation) {
                    
                    newSurveyVC.surveyParentLocation=self.surveyParentLocation;
                }
                
                [self.navigationController pushViewController:newSurveyVC animated:YES];
                
                
               // [self.navigationController pushViewController:objSurveyVC animated:YES];
            }
            else
            {
                NSMutableDictionary *tempDictionary=[tempArray objectAtIndex:0];
                NSDateFormatter* df = [NSDateFormatter new];
                [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [df setLocale:usLocale];
                NSDate *selectedDate = [df dateFromString:[tempDictionary stringForKey:@"Start_Time"]];
                int noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
                df=nil;
                usLocale=nil;
                if(noOfDays <= 0)
                {
                    
                    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Survey has been already submitted for this customer", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
                }
                else
                {
                    SurveyViewController *objSurveyVC =[[SurveyViewController alloc] initWithNibName:@"SurveyViewController" bundle:nil];
                    appDelegate.surveyDetails=[self.SurveyArray objectAtIndex:indexPath.row];
                    NSLog(@"survey old %@", [self.SurveyArray objectAtIndex:indexPath.row]);
                    
                    
                    //NewSurveyViewController* newSurveyVC=[[NewSurveyViewController alloc]init];
                    SalesWorxFeedbackViewController *newSurveyVC = [[SalesWorxFeedbackViewController alloc]init];
                    [self.navigationController pushViewController:newSurveyVC animated:YES];
                    
                    
                    //[self.navigationController pushViewController:objSurveyVC animated:YES];
                }
            }
        
    }
}
- (NSInteger)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2
{
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return [components day];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
