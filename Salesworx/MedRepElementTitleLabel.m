//
//  MedRepElementTitleLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepElementTitleLabel.h"
#import "MedRepDefaults.h"

@implementation MedRepElementTitleLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    if (self.isSemiBold==YES) {
        
    }
    self.font=MedRepElementTitleLabelFont;
    
    if (self.isWhiteFontColor==YES) {
        self.textColor=[UIColor whiteColor];
    }
    else{
    
    self.textColor=MedRepElementTitleLabelFontColor;
    }
    //self.textColor=[UIColor redColor];
    
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && super.text.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[super.text characterAtIndex:super.text.length-1]];
            NSString *nString=[super.text stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(super.text, nil);
            
        }
    }
    
    if (self.isSemiBold15) {
        self.font = kSWX_FONT_SEMI_BOLD(15);
    }
    
}

-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && newText.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[newText characterAtIndex:newText.length-1]];
            NSString *nString=[newText stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(newText, nil);
            
        }
    }
    else
        super.text = newText;
    
    
}
@end
