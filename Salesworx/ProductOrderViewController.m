//
//  ProductOrderViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/12/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
#import "ProductOrderViewController.h"
#import "Singleton.h"
#define NUMERIC                 @"1234567890"
@interface ProductOrderViewController ()
- (void)calculateBonus;
@end

@implementation ProductOrderViewController
@synthesize target;
@synthesize action;
@synthesize product;
- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    if (self)
    {
        [self setTitle:NSLocalizedString(@"Add Product Order", nil)];
        self.product=[NSMutableDictionary dictionaryWithDictionary:p  ] ;
        
        
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)] ];
        /////////////////
        productHeaderView = [[ProductHeaderView alloc] init];
        tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad{
    
   // [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];

    stockView = [[ProductStockView alloc]initWithProduct:self.product];
    [stockView setDelegate:self];
    bonusString=@"0";
    isPopover = NO;
    isSaveOrder = NO;
    AppControl *appControl = [AppControl retrieveSingleton];
    isLineItemNotes=appControl.ENABLE_LINE_ITEM_NOTES;
    showHistory  = appControl.SHOW_SALES_HISTORY;
    if([showHistory isEqualToString:@"Y"])
    {
        isShowHistory = YES;
    }
    else
    {
        isShowHistory = NO;
    }
    // if([[SWDefaults appControl] count]==0)//ENABLE_LINE_ITEM_NOTES
    if([isLineItemNotes isEqualToString:@"Y"])
    {
        isComment=YES;
        [self.product setValue:@"" forKey:@"lineItemNotes"];
    }
    else
    {
        isComment = NO;
        [self.product setValue:@"" forKey:@"lineItemNotes"];
    }
    
    [productHeaderView.headingLabel setText:[self.product stringForKey:@"Description"]];
    [productHeaderView.detailLabel setText:[self.product stringForKey:@"Item_Code"]];
    priceFooterView=[[GroupSectionFooterView alloc] initWithWidth:self.view.bounds.size.width text:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Price", nil), [self.product currencyStringForKey:@"Price"]]] ;
    tableView.frame =self.view.bounds;
    productHeaderView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 60);
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    [tableView setTableHeaderView:productHeaderView];
    [self.view addSubview:tableView];
    
    [stockView loadStock];
    //[self editableCell:nil didUpdateValue:[self.product stringForKey:@"Qty"] forKey:@"Qty"];
    //- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key

}
-(void)viewDidAppear:(BOOL)animated{
    
}
- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setToolbarHidden:YES animated:animated];
   // [self calculateBonus];
}
- (void)cancel:(id)sender{
    if(isPopover)
    {
        duesView.alpha=0.0;
        isPopover=NO;
        //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    }
    else
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
    }
}
- (void)calculateBonus {
    
    int quantity = 0;
    bonus = 0;
    NSDictionary *tempBonus;

        if ([self.product objectForKey:@"Qty"])
        {
            quantity = [[self.product objectForKey:@"Qty"] intValue];
            for(int i=0 ; i<[bonusInfo count] ; i++ )
            {
                NSDictionary *row = [bonusInfo objectAtIndex:i];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    if(checkBonus)
                    {
                        isBonusAllow = YES;
                    }
                    if(i == [bonusInfo count]-1)
                    {
                        if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                        {
                            int dividedValue = quantity / rangeStart ;
                            bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                            tempBonus=row;
                        }
                        
                        else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                        {
                            int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                            bonus = floor(dividedValue) ;
                            tempBonus=row;

                        }
                        else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"POINT"])
                        {
                            bonus = [[row objectForKey:@"Get_Qty"] intValue];
                            tempBonus=row;

                        }
                    }
                    else 
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue];
                        tempBonus=row;

                    }
                    break;
                }
                else
                {
                    if(checkBonus)
                    {
                        isBonusAllow = NO;
                    }
                }
            }
        }
    

        if (bonus > 0 && bonusInfo)
        {
            if(![[tempBonus stringForKey:@"Get_Item"] isEqualToString:[bonusProduct stringForKey:@"Item_Code"]])
            {
                NSArray *temBonusArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:[NSString stringWithFormat:@"select * from TBL_Product where Item_Code ='%@'",[tempBonus stringForKey:@"Get_Item"]]];
                bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[temBonusArray objectAtIndex:0]];
                [bonusProduct removeObjectForKey:@"Qty"];
                [bonusProduct removeObjectForKey:@"FOC_Product"];
                [bonusProduct removeObjectForKey:@"StockAllocation"];
            }
            
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Def_Qty"];
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Qty"];
            
            if(!isDualBonusAllow)
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
            }
            if(isDiscountAllow)
            {
                [self.product setValue:@"0" forKey:@"Discount"];
            }
        }
        else
        {
            [bonusProduct removeObjectForKey:@"Qty"];
        }
}
- (void)save:(id)sender {
    
    isSaveOrder = YES;
    int totalBonus = 0 ;
    [self.product removeObjectForKey:@"FOC_Product"];
    [self.product removeObjectForKey:@"Bonus_Product"];
    
    [self.product setValue:bonusProduct forKey:@"Bonus_Product"];
    [self.product setValue:fBonusProduct forKey:@"FOC_Product"];
    
    BOOL isBothBonus = NO;
    if([bonusProduct objectForKey:@"Qty"])
    {
        totalBonus = [[bonusProduct stringForKey:@"Qty"] intValue];
        if(!isDualBonusAllow)
        {
            [self.product removeObjectForKey:@"FOC_Product"];
        }
        else if([fBonusProduct objectForKey:@"Qty"])
        {
            isBothBonus = YES;
            totalBonus = totalBonus + [[fBonusProduct stringForKey:@"Qty"] intValue];
        }
        else
        {
            [self.product removeObjectForKey:@"FOC_Product"];
        }
    }
    else
    {
        [self.product removeObjectForKey:@"Bonus_Product"];
    }
    
    if([fBonusProduct objectForKey:@"Qty"] && !isBothBonus)
    {
        totalBonus = [[fBonusProduct stringForKey:@"Qty"] intValue];
    }
    else if(!isBothBonus)
    {
        [self.product removeObjectForKey:@"FOC_Product"];
    }
    
    
    [self.product setValue:[NSString stringWithFormat:@"%d",totalBonus] forKey:@"Def_Bonus"];
    [self.product setValue:[self.product stringForKey:@"Discount"] forKey:@"DiscountPercent"];

    if([self.product objectForKey:@"StockAllocation"])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:self.product];
#pragma clang diagnostic pop
    }
    else
    {
        
        [self.product setValue:stockItems forKey:@"StockAllocation"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:self.product];
#pragma clang diagnostic pop
    }
    //NSLog(@"Product %@",self.product);
  [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
}
- (void)updatePrice{


    if ([self.product stringForKey:@"Qty"].length > 0)
    {
        int qty = [[self.product stringForKey:@"Qty"] intValue];
        if (qty > 0)
        {
            double totalPrice = (double)qty * ([[self.product objectForKey:@"Net_Price"] doubleValue]);
            if(bonus<=0 || !checkDiscount)
            {
                float discountAmount = [[self.product stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                [self.product setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [self.product setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                [self.product setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];

                
                [priceFooterView.titleLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
            }
            
            else
            {
                [self.product setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [self.product setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Discounted_Price"];
                [self.product setValue:@"0.00" forKey:@"Discounted_Amount"];
                [priceFooterView.titleLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Price", nil), [self.product currencyStringForKey:@"Price"]]];
            }
        }
        else{
            [priceFooterView.titleLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Price", nil), [self.product currencyStringForKey:@"Price"]]];

        }
        //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    }
    else
    {
        [priceFooterView.titleLabel setText:@""];
        //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
}
#pragma mark UITableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(section == 0)
    {
        if (isComment)
        {
            return 5;
        }
        else
        {
            return 4;
        }
    }
    else if (section == 1)
    {
        return 5;
    }
    else if(section==3)
    {
        return 1;
    }
    else
    {
        if (isShowHistory) {
            return 3;
        }
        else
        {
            return 2;
        }
    }
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:NSLocalizedString(@"Order Info", nil)] ;
        return sectionHeader;
    }
    else if (section == 1) {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:NSLocalizedString(@"Product Info", nil)] ;
        return sectionHeader;
    }
    else if(section == 2){
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:NSLocalizedString(@"More Info", nil)] ;
        return sectionHeader;
    }
    else {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:NSLocalizedString(@"Comments", nil)] ;
        return sectionHeader;
    }
    
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  
    
    return 20.0f;
}
- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)section {
    
    if (section != 0) {
        return nil;
    }

    
    priceFooterView.titleLabel.textAlignment = NSTextAlignmentRight;
    return priceFooterView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section != 0)
    {
        return 0.0f;
    }
    
    return 20.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"Identifier";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        if (indexPath.section == 1 || indexPath.section == 2)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        else
        {
            if (indexPath.row == 0)
            {
                cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
            }
            else
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
            }
        }
    }
    if (indexPath.section == 0)
    {
        //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (indexPath.row == 0)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Quantity", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Qty"];
            [((SWTextFieldCell *)cell).textField setText:[self.product stringForKey:@"Qty"]];
            [((SWTextFieldCell *)cell).label setText:[NSString stringWithFormat:@"*%@",NSLocalizedString(@"Quantity", nil)]];
        }
        else if (indexPath.row == 1)
        {
           
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Bonus"];
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Bonus", nil)];

            if(![fBonusProduct objectForKey:@"Qty"]  || isDualBonusAllow)
            {
                [((SWTextFieldCell *)cell).textField setText:[bonusProduct stringForKey:@"Qty"]];
            }
            [((SWTextFieldCell *)cell).secondLabel setText:[bonusProduct stringForKey :@"Description"]];

            
            if (bonus<=0){
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:NO];
                [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"No Bonus", nil)];
            }
            else{
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:YES];
                [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Bonus", nil)];
            }
            if(isBonusAllow)//APP CONTROL
            {
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:YES];
            }
            else{
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:NO];
                ((SWTextFieldCell *)cell).label.textColor = [UIColor grayColor];
                ((SWTextFieldCell *)cell).secondLabel.textColor = [UIColor grayColor];
            }
        }
        
        else if (indexPath.row == 2)
        {
            
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Manual FOC", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"FBonus"];
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Manual FOC", nil)];

            if(![bonusProduct objectForKey:@"Qty"] || isDualBonusAllow)
            {
                [((SWTextFieldCell *)cell).textField setText:[fBonusProduct stringForKey:@"Qty"]];
            }
            [((SWTextFieldCell *)cell).secondLabel setText:[fBonusProduct stringForKey:@"Description"]];

            if(isFOCItemAllow)
            {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }

            if(isFOCAllow)//APP CONTROL
            {
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:YES];
            }
            else
            {
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:NO];
                ((SWTextFieldCell *)cell).secondLabel.textColor = [UIColor grayColor];
                ((SWTextFieldCell *)cell).label.textColor = [UIColor grayColor];
            }
            
        }
        else if (indexPath.row == 3)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Discount"];
            NSString *discountString = [self.product stringForKey:@"Discount"];
            if([discountString rangeOfString:@"%"].location == NSNotFound)
            {
                discountString = [discountString stringByAppendingString:@"%"];
            }

            [((SWTextFieldCell *)cell).textField setText: discountString];
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Discount", nil)];
            
            if(isDiscountAllow)//APP CONTROL
            {
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:YES];
            }
            else
            {
                [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:NO];
                ((SWTextFieldCell *)cell).label.textColor = [UIColor grayColor];
            }
        }
        else if (indexPath.row == 4)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"lineItemNotes"];
            [((SWTextFieldCell *)cell).textField setText:[self.product stringForKey:@"lineItemNotes"]];
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Comments", nil)];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Comments", nil)];
        }
    }
    else if (indexPath.section == 1)
    {
        //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        if (indexPath.row == 4)
        {
            [cell.textLabel setText:NSLocalizedString(@"Stock", nil)];
           Singleton *single = [Singleton retrieveSingleton];
            if (totaleStock ==0)
            {
                single.stockAvailble = @"";
                [cell.detailTextLabel setText:[NSString stringWithFormat:@"No stock available"]];
            }
            else
            {
                [cell.detailTextLabel setText:[NSString stringWithFormat:@"%d %@", totaleStock,NSLocalizedString(@"Available", nil)]];
                single.stockAvailble = @"";
            }
            
        }
        else if (indexPath.row == 3)
        {
            [cell.textLabel setText:NSLocalizedString(@"UOM code", nil)];
            [cell.detailTextLabel setText:[self.product stringForKey:@"Primary_UOM_Code"]];
        }
        else if (indexPath.row == 2)
        {
            [cell.textLabel setText:NSLocalizedString(@"Expiry", nil)];
            [cell.detailTextLabel setText:[self.product dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
        }
        else if (indexPath.row == 1)
        {
            [cell.textLabel setText:NSLocalizedString(@"Retail Price", nil)];
            [cell.detailTextLabel setText:[self.product currencyStringForKey:@"List_Price"]];
        }
        else if (indexPath.row == 0)
        {
            if ([[self.product stringForKey:@"Control_1"] isEqualToString:@"Y"])
            {
                cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
                [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
                [((SWTextFieldCell *)cell) setDelegate:self];
                [((SWTextFieldCell *)cell) setKey:@"Net_Price"];
                NSString *discountString = [self.product stringForKey:@"Net_Price"];
                [((SWTextFieldCell *)cell).textField setText: [discountString currencyString]];
                [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Wholesale Price", nil)];
            }
            else
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
                [cell.textLabel setText:NSLocalizedString(@"Wholesale Price", nil)];
                [cell.detailTextLabel setText:[self.product currencyStringForKey:@"Net_Price"]];
            }
        }
    }
    else if (indexPath.section == 2)
    {
        //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        if (indexPath.row == 0)
        {
            [cell.textLabel setText:NSLocalizedString(@"Stock and Lot", nil)];
            [cell.detailTextLabel setText:NSLocalizedString(@"Review Stock and Select/Assign lots", nil)];
        }
        else if (indexPath.row == 1)
        {
            [cell.textLabel setText:NSLocalizedString(@"Prices and Bonus", nil)];
            [cell.detailTextLabel setText:NSLocalizedString(@"Review Price and Bonus Information", nil)];
            
        }
        else
        {
            [cell.textLabel setText:NSLocalizedString(@"Sales History", nil)];
            [cell.detailTextLabel setText:@"Review sales history"];
        }
    }

    else if (indexPath.section == 3)
    {
        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier] ;
        [((SWTextFieldCell *)cell) setDelegate:self];
        [((SWTextFieldCell *)cell) setKey:@"lineItemNotes"];
        [((SWTextFieldCell *)cell).textField setText:[self.product stringForKey:@"lineItemNotes"]];

    }
    
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];

        return cell;
}
}
#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && indexPath.row == 2 && isFOCAllow & isFOCItemAllow)
    {
        isPopover = YES ;
       Singleton *single = [Singleton retrieveSingleton];
        single.productBarPopOver = @"PopOver";
      
        productListViewController = [[ProductListViewController alloc] initWithCategory:[SWDefaults productCategory]] ;
        [productListViewController setTarget:self];
        [productListViewController setAction:@selector(productSelected:)];
        [productListViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
        navigationControllerR = [[UINavigationController alloc] initWithRootViewController:productListViewController] ;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationControllerR] ;
        [popoverController setDelegate:self];
        [popoverController presentPopoverFromRect:CGRectMake(0, 0, 300, 400) inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if (indexPath.section ==2 && indexPath.row == 1)
    {
        if(isBonus)
        {
            productBonusViewController = [[ProductBonusViewController alloc] initWithProduct:self.product] ;
            
            navigationControllerQ = [[UINavigationController alloc] initWithRootViewController:productBonusViewController] ;
            [self.navigationController presentViewController:navigationControllerQ animated:YES completion:nil];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"No Bonus associated with this product" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
//            [ILAlertView showWithTitle:@"Sorry"
//                               message:@"No Bonus associated with this product"
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:nil];
        }
    }
    else if (indexPath.section == 2 && indexPath.row == 0)
    {

        
        productStockViewController = [[ProductStockViewController alloc] initWithProduct:self.product] ;
        [productStockViewController setOrderQuantity:[[self.product stringForKey:@"Qty"] intValue]];
        [productStockViewController setTarget:self];
        [productStockViewController setAction:@selector(stockAllocated:)];
        navigationControllerS = [[UINavigationController alloc] initWithRootViewController:productStockViewController] ;
        [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
    }
    else if (indexPath.section == 2 && indexPath.row == 2)
    {
        customerSalesVC = [[CustomerSalesViewController alloc] initWithCustomer:[SWDefaults customer] andProduct:product] ;
        navigationControllerS = [[UINavigationController alloc] initWithRootViewController:customerSalesVC] ;
        [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
    }
}
#pragma mark EditableCell Delegate
- (void)editableCellDidStartUpdate:(EditableCell *)cell{
    //[self.navigationItem setRightBarButtonItem:nil animated:YES];
}
- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key
{
    NSCharacterSet *unacceptedInput = nil;
    NSString *zeroString=@"0";
    if(![key isEqualToString:@"lineItemNotes"])
    {
        value = [value stringByReplacingOccurrencesOfString:@" " withString:@""];
        unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    }
    if([key isEqualToString:@"FBonus"])
    {
        
        if ([value length]!=0)
        {
            [fBonusProduct setValue:value forKey:@"Qty"];
            [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
        }
        else
        {
            [fBonusProduct removeObjectForKey:@"Qty"];
            [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];

        }
    }
    if ([key isEqualToString:@"Bonus"])
    {
        [bonusProduct setValue:value forKey:@"Qty"];
        if([isBonusAppControl isEqualToString:@"Y"])
        {
            bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
            bonusEdit = @"Y";
        }
        else if([isBonusAppControl isEqualToString:@"I"])
        {
            if([[bonusProduct stringForKey:@"Qty"] intValue] >= bonus)
            {
                bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                bonusEdit = @"Y";
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Bonus can increase only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                [alert show];
//                [ILAlertView showWithTitle:@"Sorry"
//                                   message:@"Bonus can increase only."
//                          closeButtonTitle:NSLocalizedString(@"OK", nil)
//                         secondButtonTitle:nil
//                       tappedButtonAtIndex:nil];
                [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
            }
        }
        else if([isBonusAppControl isEqualToString:@"D"])
        {
            if([[bonusProduct stringForKey:@"Qty"] intValue] <= bonus)
            {
                bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                bonusEdit = @"Y";
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Bonus can decrease only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                [alert show];
//                [ILAlertView showWithTitle:@"Sorry"
//                                   message:@"Bonus can decrease only."
//                          closeButtonTitle:NSLocalizedString(@"OK", nil)
//                         secondButtonTitle:nil
//                       tappedButtonAtIndex:nil];
                [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
            }
        }
        
        
        if (bonus==0) {
            [bonusProduct removeObjectForKey:@"Qty"];
        }
    }
    if ([key isEqualToString:@"Discount"])
    {
        [self.product setValue:value forKey:key];
    }
    if ([key isEqualToString:@"Qty"])
    {
        if ([[value componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1)
        {
            [self.product setValue:value forKey:key];
            if (bonusInfo.count > 0 || ![[self.product stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
            {
                if([bonusEdit isEqualToString:@"N"])
                {
                    [self calculateBonus];
                }
                else
                {
                    bonusEdit = @"N";
                }
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
//            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                               message:@"Quantity should be a numeric value only."
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:nil];
            
            [self.product setValue:@"" forKey:@"Qty"];
        }
    }
    if ([key isEqualToString:@"Net_Price"])
    {
        [self.product setValue:value forKey:key];
    }
    if ([key isEqualToString:@"lineItemNotes"])
    {
        [self.product setValue:value forKey:key];
    }
 
    
   
    
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    if([self.product objectForKey:@"Qty"])
    {
        //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    }
    [tableView reloadData];
}
#pragma mark ProductListView Controller action
- (void)productSelected:(NSDictionary *)productDict{
    [popoverController dismissPopoverAnimated:YES];
    [self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.10f];
}
- (void)presentProductOrder:(NSMutableDictionary *)productDict{
    if (![productDict objectForKey:@"Guid"])
    {
        [productDict setValue:[NSString createGuid] forKey:@"Guid"];
    }
    if([fBonusProduct objectForKey:@"Qty"])
    {
        [productDict setValue:[fBonusProduct objectForKey:@"Qty"] forKey:@"Qty"];
    }
    fBonusProduct=productDict;
    [tableView reloadData];
}
- (void)stockAllocated:(NSMutableDictionary *)p {
    product=p;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark ProductService delegate
- (void)getProductServiceDiddbGetBonusInfo:(NSArray *)info{
    isBonusRowDeleted=NO;
    bonusInfo=info;
    AppControl *appControl = [AppControl retrieveSingleton];
    isBonusAppControl = appControl.ALLOW_BONUS_CHANGE;
    isFOCAppControl = appControl.ENABLE_MANUAL_FOC;
    isDiscountAppControl = appControl.ALLOW_DISCOUNT;
    isDualBonusAppControl = appControl.ALLOW_DUAL_FOC;
    isFOCItemAppControl=appControl.ALLOW_MANUAL_FOC_ITEM_CHANGE;
        
    if([isFOCAppControl isEqualToString:@"Y"])
    {
        isFOCAllow=YES;
        checkFOC=YES;
    }
    else 
    {
        if ([isDualBonusAppControl isEqualToString:@"N"])
        {
            isFOCAllow=NO;
            checkFOC=NO;
        }
    }
    
    if([isDiscountAppControl isEqualToString:@"Y"])
    {
        isDiscountAllow=YES;
        checkDiscount = YES;
    }
    else
    {
        isDiscountAllow=YES;
        checkDiscount = NO;
    }
    
    if([isFOCItemAppControl isEqualToString:@"Y"])
    {
        isFOCItemAllow=YES;
    }
    else
    {
        isFOCItemAllow=NO;
    }
    
    if([isDualBonusAppControl isEqualToString:@"Y"])
    {
        isDualBonusAllow=YES;
    }
    else
    {
        isDualBonusAllow=NO;
    }

    if (bonusInfo.count > 0)
    {
        isBonus = YES;
        if([isBonusAppControl isEqualToString:@"Y"])
        {
            isBonusAllow = YES;
            checkBonus = YES;
        }
        else
        {
            isBonusAllow = NO;
            checkBonus = NO;
        }
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        isBonus = NO;
        bonus=0;
        isBonusAllow = NO;
        checkBonus = NO;
    }
    bonusEdit = @"N";
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    if([self.product objectForKey:@"FOC_Product"])
    {
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:[self.product objectForKey:@"FOC_Product"]];
    }
    else
    {
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:self.product];
        [fBonusProduct removeObjectForKey:@"Qty"];
        [fBonusProduct removeObjectForKey:@"Bonus_Product"];
        [fBonusProduct removeObjectForKey:@"StockAllocation"];
    }
    
    if([self.product objectForKey:@"Bonus_Product"])
    {
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[self.product objectForKey:@"Bonus_Product"]];
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[self.product stringForKey:@"Item_Code"]]];
        [bonusProduct removeObjectForKey:@"Qty"];
        [bonusProduct removeObjectForKey:@"FOC_Product"];
        [bonusProduct removeObjectForKey:@"StockAllocation"];
    }

    
    [tableView reloadData];
    info=nil;
}

- (void)productStockViewLoaded{
    totalGoods=0;
    NSArray *selectedItems = [self.product objectForKey:@"StockAllocation"];
    if (selectedItems.count > 0)
    {
        for (int i = 0; i < stockView.items.count; i++)
        {
            @autoreleasepool{
            NSDictionary *row = [stockView.items objectAtIndex:i];
            
            for (NSDictionary *selectedRow in selectedItems)
            {
                @autoreleasepool{
                if ([[row stringForKey:@"Stock_ID"] isEqualToString:[selectedRow stringForKey:@"Stock_ID"]])
                {
                    [row setValue:[selectedRow stringForKey:@"Allocated"] forKey:@"Allocated"];
                    [stockView.items replaceObjectAtIndex:i withObject:row];
                    break;
                }
                }
            }
            }
        }
    }
   Singleton *single = [Singleton retrieveSingleton];
    for (NSDictionary *row in stockView.items)
    {
        int qty = [[row objectForKey:@"Lot_Qty"] intValue];
        totalGoods = totalGoods + qty;
    }
    single.stockAvailble = [NSString stringWithFormat:@"%d",totalGoods];
    totaleStock = [single.stockAvailble intValue];

    if(![stockView.items count] == 0)
    {
        if(![self.product objectForKey:@"Expiry_Date"])
        {
            [self.product setValue:[[stockView.items objectAtIndex:0] stringForKey:@"Expiry_Date"] forKey:@"Expiry_Date"];
        }
    }
    stockItems=[NSMutableArray arrayWithArray:stockView.items];
    
    
    //serProduct.delegate=self;
    
    [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[self.product stringForKey:@"Item_Code"]]];

}
- (void)deleteRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (void)insertRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (void)reloadRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC{
    productListViewController=nil;
    popoverController=nil;
}
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    
    return YES;
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setToolbarHidden:NO animated:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
  }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end