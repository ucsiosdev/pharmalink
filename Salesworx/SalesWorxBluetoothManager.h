//
//  SalesWorxBluetoothManager.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface SalesWorxBluetoothManager : NSObject<CBCentralManagerDelegate>

{
    UIViewController * currentTopVC;
}
@property (nonatomic, strong) CBCentralManager *bluetoothManager;

+ (SalesWorxBluetoothManager*) retrieveSingleton;
-(void)initializeBluetoothManager;


@end
