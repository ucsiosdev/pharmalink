//
//  SalesWorxCustomerAvailableBalanceLabel.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 1/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxCustomerAvailableBalanceLabel.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@implementation SalesWorxCustomerAvailableBalanceLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    self.font=kSWX_FONT_SEMI_BOLD(20);
    self.textColor=[UIColor colorWithRed:(70.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1.0];

    [super awakeFromNib];    
}

@end
