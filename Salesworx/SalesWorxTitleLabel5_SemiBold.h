//
//  SalesWorxTitleLabel5_SemiBold.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"

@interface SalesWorxTitleLabel5_SemiBold : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
-(NSString*)text;
-(void)setText:(NSString*)newText;

@end
