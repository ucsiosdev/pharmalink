//
//  SWSplitViewController.h
//  SWDashboard
//
//  Created by Irfan Bashir on 5/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "ZUUIRevealController.h"

@class FrontViewController;
@class RearViewController;

@interface  SWSplitViewController : ZUUIRevealController <ZUUIRevealControllerDelegate>

- (id)initWithFrontViewController:(UIViewController *)aFrontViewController rearViewController:(UIViewController *)aBackViewController;
@end