//
//  SWCustomersListViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "SWCollectionCustListViewController.h"
#import "SWSection.h"
#import "LocationFilterViewController.h"
#import "SWCustomerListCell.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "SWDefaults.h"
#import "CustomersListViewController.h"
#import "SalesWorxPaymentCollectionViewController.h"

#define kNavigationFilterLocationButton 1
#define kNavigationFilterCategoryButton 2

@interface SWCollectionCustListViewController ()
- (void)locationFilter:(id)sender;
- (void)locationFilterSelected;
- (void)setupToolbar;
- (void)executeSearch:(NSString *)term;
@end

@implementation SWCollectionCustListViewController


@synthesize target;
@synthesize action;


- (id)init {
    self = [super init];
    
    if (self) {
        [self setTitle:NSLocalizedString(@"Customers", nil) ];
        
        searchData=[NSMutableArray array];
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
   
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // loading view
    loadingView=[[SWLoadingView alloc] initWithFrame:self.view.bounds];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    
    refinedCustomerListArray=[[NSMutableArray alloc]init];

    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    
    // table view
    tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,44.0, self.view.bounds.size.width, self.view.bounds.size.height-44) style:UITableViewStylePlain];
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    // search display controller
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 1024, 44.0f)] ;
    searchController=[[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    // [self.searchBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    
    [searchController setDelegate:self];
	[searchController setSearchResultsDataSource:self];
    [searchController setSearchResultsDelegate:self];
    [searchController.searchResultsTableView setDelegate:self];
    
    [self.view addSubview:searchBar];
    [self.view addSubview:tableView];
    [self.view addSubview:loadingView];
    // popovers
    LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
    [locationFilterController setTarget:self];
    [locationFilterController setAction:@selector(locationFilterSelected)];
    [locationFilterController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:locationFilterController] ;
    
    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    locationFilterPopOver.delegate=self;
    // Count label
    infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 25)];
    [infoLabel setText:@""];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:BoldSemiFontOfSize(14.0f)];

    [self performSelector:@selector(navigatetodeals) withObject:nil afterDelay:0.0];


}
-(void)viewWillAppear:(BOOL)animated
{
//    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//    [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
        searchController.searchResultsTableView.cellLayoutMarginsFollowReadableWidth=NO;
        
    }
    
   }
-(void)navigatetodeals
{
    
    //customerSer.delegate = self;
   // [SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

}
- (void)setupToolbar {
    
    NSString *text = [NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"Total Customers", nil), totalRecords];
    if ([[SWDefaults locationFilterForCustomerList] length] > 0) {
        text = [NSString stringWithFormat:@"%@ (filtered by: %@)", text, [SWDefaults locationFilterForCustomerList]];
    }
    
    [infoLabel setText:text];
    
    if (!self.navigationController.toolbarHidden) {
        return;
    }
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    
    UIBarButtonItem *totalLabelButton = [UIBarButtonItem labelButtonWithLabel:infoLabel];
    
    [self setToolbarItems:nil];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton, flexibleSpace, nil]];
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    
    UIBarButtonItem *displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_filterImage"cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(locationFilter:)] ;
    
    UIBarButtonItem *displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_barcode"cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
    
    
    
    
    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    
    flexibleSpace=nil;
    totalLabelButton=nil;
    displayActionBarButton=nil;
    displayMapBarButton=nil;
    //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:buttonsView]  animated:YES];
}

- (void)displayMap:(id)sender {
    
    Singleton *single = [Singleton retrieveSingleton];
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    //reader.showsZBarControls = Y;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)] ;
    customOverlay.backgroundColor = [UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    reader.wantsFullScreenLayout = NO;
    reader.readerView.zoom = 1.0;
    reader.showsZBarControls=NO;
    [self presentViewController: reader animated: YES  completion:nil];
   // [reader release];
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil];
    
}

- (void) imagePickerController: (UIImagePickerController*) readers
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    
    for(symbol in results){
        single.valueBarCode=symbol.data;
        // NSString *upcString = symbol.data;
        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
        [locationFilterController setTarget:self];
        [locationFilterController setAction:@selector(filterChangedBarCode)];
        [locationFilterController selectionDone:self];
        
        [reader dismissViewControllerAnimated: YES completion:nil];
        
        single.isBarCode = NO;
    }
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    if (self.target && [self.target respondsToSelector:self.action]) {
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)] ];
        [self setTitle:NSLocalizedString(@"Select Customer", nil)];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}


- (void)locationFilter:(id)sender {
    
    
    [locationFilterPopOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}


- (void)filterChangedBarCode {
    
    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:YES animated:YES];
    [self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;

   // [SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

}
- (void)locationFilterSelected {
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    [[SWDatabaseManager retrieveManager] cancel];
    
    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:YES animated:YES];
    [self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;

    //[SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

}

- (void)executeSearch:(NSString *)term {
    [searchData removeAllObjects];
    NSDictionary *element=[NSDictionary dictionary];
    for(element in customerList)
    {
        NSString *customerName = [element objectForKey:@"Customer_Name"];
        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        if (r.length > 0)
        {
            [searchData addObject:element];
        }
	}
    sectionSearch = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    // Loop through the books and create our keys
    for (NSDictionary *book in searchData)
    {
        NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [sectionSearch allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the books into their respective keys
    for (NSDictionary *book in searchData)
    {
        [[sectionSearch objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
    }
    
    // Sort each section array
    for (NSString *key in [sectionSearch allKeys])
    {
        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
    }
    
}

- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Customer List service delegate
//- (void)customerServiceDidGetList:(NSArray *)cl {
- (void)getListFunction:(NSArray *)cl
{
    [loadingView setHidden:YES];
    customerList = [NSMutableArray arrayWithArray:cl ] ;
    
       if(![customerList count]==0)
    {
        if ([searchController isActive])
        {
            [self executeSearch:self.searchDisplayController.searchBar.text];
            [searchController.searchResultsTableView reloadData];
        }
        else
        {
            sectionList = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in customerList)
            {
                NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionList allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionList setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in customerList)
            {
                [[sectionList objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each section array
            for (NSString *key in [sectionList allKeys])
            {
                [[sectionList objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
            }
            
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No customer found." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];

//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"No customer found."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
    }
    totalRecords=customerList.count;
    [tableView reloadData];
    [self setupToolbar];
    cl=nil;
    
}

#pragma mark UISearchDisplay Controller delegate
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    
    [self executeSearch:searchString];
    
    return YES;
}

#pragma mark UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv
{
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        return [[sectionSearch allKeys] count];
    }
    return [[sectionList allKeys] count];
}

//- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)s
//{
//    if(tv == self.searchDisplayController.searchResultsTableView)
//    {
//        return [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
//    }
//    // return [customerList count];
//    return [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
//}


- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)s
{
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        return [(NSArray*)[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
    }
    // return [customerList count];
    return [(NSArray*)[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Identifier = @"CUSTOMERLISTCELL";
    SWCustomerListCell *cell = [tv dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil)
    {
        cell = [[SWCustomerListCell alloc] initWithReuseIdentifier:Identifier] ;
        //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        [cell applyAttributes:[[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    else
    {
        [cell applyAttributes:[[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
        
        [refinedCustomerListArray addObject:[[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
        
        
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableViews titleForHeaderInSection:(NSInteger)section
{
    if(tableViews == self.searchDisplayController.searchResultsTableView)
    {
        return [[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        
    }
    return [[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    return 60.0f;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tv
{
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        return [[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
    }
    return [[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
}


- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
    
    //testing new screen
   
    
    SalesWorxPaymentCollectionViewController * paymentCollectionVC=[[SalesWorxPaymentCollectionViewController alloc]init];
    paymentCollectionVC.selectedCustomerIndexPath=indexPath;
    
    
    //paymentCollectionVC.customerListArray=[];
    //paymentCollectionVC.customerSectionsListArray=refinedSectionsArray;
    
    
    [self.navigationController pushViewController:paymentCollectionVC animated:YES];
    
    
    
    
    /*
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0) {
        [revealController revealToggle:self];
    }
    
    if(tv == self.searchDisplayController.searchResultsTableView){
        
        NSDictionary *row = [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        SWCollectionViewController *customerDetailViewController = [[SWCollectionViewController alloc] initWithCustomer:row] ;
        [self.navigationController pushViewController:customerDetailViewController animated:YES];
    } else
    {
        
        NSDictionary *row = [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        SWCollectionViewController *customerDetailViewController = [[SWCollectionViewController alloc] initWithCustomer:row] ;
        [self.navigationController pushViewController:customerDetailViewController animated:YES];
    }*/

    
    }




#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    [cell.textLabel setFont:LightFontOfSize(14.0f)];
//    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
