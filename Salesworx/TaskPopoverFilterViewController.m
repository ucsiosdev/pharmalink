//
//  TaskPopoverFilterViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "TaskPopoverFilterViewController.h"

@interface TaskPopoverFilterViewController ()

@end

@implementation TaskPopoverFilterViewController

@synthesize dateTextField,statusTextField,dateTitleLbl,statusTitleLbl,popOverPresentationController,previousFilterParametersDict,filterNavController,taskArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    filterParametersDict=[[NSMutableDictionary alloc]init];
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldTodoFilterScreenName];
    }
    
    if (self.isMovingToParentViewController) {
        
        dateTitleLbl.text=NSLocalizedString(@"Date", nil);
        statusTitleLbl.text=NSLocalizedString(@"Status", nil);
        
        NSString* statusString=[previousFilterParametersDict valueForKey:@"Status"];
        NSString* dateSting=[previousFilterParametersDict valueForKey:@"Created_At"];
        filterParametersDict=previousFilterParametersDict;
        if (![NSString isEmpty:statusString]) {
            
            statusTextField.text=[SWDefaults getValidStringValue:statusString];
        }
        
        if (![NSString isEmpty:dateSting]) {
            
            dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:dateSting]];
        }
        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
        [self.navigationController.navigationBar setTranslucent:NO];
        
        UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
        clearFilterBtn.tintColor=[UIColor whiteColor];
        self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
        
        UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
        closeFilterBtn.tintColor=[UIColor whiteColor];
        self.navigationItem.leftBarButtonItem=closeFilterBtn;
        
        [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                      forState:UIControlStateNormal];
        [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                      forState:UIControlStateNormal];
    }
}


-(void)closeFilter
{
    if ([self.delegate respondsToSelector:@selector(taskFilterDidClose)]) {
        [self.delegate taskFilterDidClose];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)clearFilter
{
    dateTextField.text=@"";
    statusTextField.text=@"";
    filterParametersDict=[[NSMutableDictionary alloc]init];
}


#pragma mark UITextField Methods

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    MedRepDoctorFilterDescriptionViewController *filterDescVC = [[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate = self;
    filterDescVC.descTitle = selectedTextField;
    filterDescVC.filterNavController = self.filterNavController;
    filterDescVC.filterPopOverController = self.filterPopOverController;
    
    NSMutableArray *filterDescArray = [[NSMutableArray alloc]init];
    NSMutableArray *unfilteredArray = [[NSMutableArray alloc]init];
    
    //get distinct keys
    NSPredicate *refinedPred = [NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray = [[taskArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray = [sortedArray valueForKey:predicateString];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray = filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
    }
}

-(void)selectedFilterValue:(NSString*)selectedString
{
    if ([selectedTextField isEqualToString:kStatusTextField]) {
        statusTextField.text=[SWDefaults getValidStringValue:selectedString];
        [filterParametersDict setValue:selectedString forKey:@"Status"];
    }
}


-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    NSLog(@"selected visit date is %@", selectedDate);
    dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:selectedDate]];
    [filterParametersDict setValue:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:selectedDate] forKey:@"Created_At"];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==dateTextField)
    {
        selectedTextField=kDateTextField;
        selectedPredicateString=@"Created_At";
        
        MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        dateFilterVC.datePickerMode=kTodoTitle;
        dateFilterVC.titleString=@"Date";
        dateFilterVC.visitDateDelegate=self;
        [self.navigationController pushViewController:dateFilterVC animated:YES];
        return NO;
    }
    
    else if (textField==statusTextField)
    {
        selectedTextField=kStatusTextField;
        selectedPredicateString=@"Status";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    else
    {
        return YES;
    }
}
- (IBAction)searchButtonTapped:(id)sender {
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filter parameters dict is %@",filterParametersDict);
    
    NSMutableArray * filteredToDoArray=[[NSMutableArray alloc]init];
    
    NSString* dateString=[filterParametersDict valueForKey:@"Created_At"];
    NSString* statusString=[filterParametersDict valueForKey:@"Status"];
    if([NSString isEmpty:dateString]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Created_At ==[cd] %@",dateString]];
    }
    
    if([NSString isEmpty:statusString]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Status ==[cd] %@",statusString]];
    }
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredToDoArray = [[taskArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    if (predicateArray.count==0) {
        
        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please select your filter criteria and try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [noFilterAlert show];
    }
    else  if (filteredToDoArray.count>0)
    {
        if ([self.delegate respondsToSelector:@selector(filteredTaskList:)]) {
            [self.delegate filteredTaskList:filteredToDoArray];
            [self.delegate filteredTaskParameters:filterParametersDict];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    else
    {
        [MedRepDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
    }
}

- (IBAction)resetButtonTapped:(id)sender {
    
    previousFilterParametersDict=[[NSMutableDictionary alloc]init];
    
    if ([self.delegate respondsToSelector:@selector(taskFilterdidReset)]) {
        [self.delegate taskFilterdidReset];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
