//
//  MedrepdoctorClassificationPopOverTableViewCell.h
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 12/5/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"

@interface MedrepdoctorClassificationPopOverTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *valueLbl;
@property (strong, nonatomic) IBOutlet UIImageView *IconImageView;
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *TitleLbl;

@end
