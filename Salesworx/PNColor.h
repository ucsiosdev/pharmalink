//
//  PNColor.h
//  PNChart
//
//  Created by kevin on 13-6-8.
//  Copyright (c) 2013年 kevinzhow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/*
 *  System Versioning Preprocessor Macros
 */

#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)


#define PNGreen         [UIColor colorWithRed:77.0 / 255.0 green:186.0 / 255.0 blue:122.0 / 255.0 alpha:1.0f]
#define PNFreshGreen    [UIColor colorWithRed:12.0 / 255.0 green:120.0 / 255.0 blue:27.0 / 255.0 alpha:1.0f]
#define PNTwitterColor  [UIColor colorWithRed:0.0 / 255.0 green:100.0 / 255.0 blue:230.0 / 255.0 alpha:1.0]


#define PNPieChartGreen         [UIColor colorWithRed:33.0 / 255.0 green:186.0 / 255.0 blue:164.0 / 255.0 alpha:1.0f]
#define PNPieChartBlue          [UIColor colorWithRed:77.0 / 255.0 green:144.0 / 255.0 blue:210.0 / 255.0 alpha:1.0f]
#define PNPieChartYellow        [UIColor colorWithRed:255.0 / 255.0 green:166.0 / 255.0 blue:77.0 / 255.0 alpha:1.0f]
#define PNPieChartRed           [UIColor colorWithRed:255.0 / 255.0 green:77.0 / 255.0 blue:77.0 / 255.0 alpha:1.0f]
#define PNPieChartOrange        [UIColor colorWithRed:255.0 / 255.0 green:122.0 / 255.0 blue:77.0 / 255.0 alpha:1.0f]



@interface PNColor : NSObject

- (UIImage *)imageFromColor:(UIColor *)color;

@end
