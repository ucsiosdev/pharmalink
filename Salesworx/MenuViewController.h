//
//  MenuViewController.h
//  SWDashboard
//
//  Created by Irfan Bashir on 5/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "AboutUsViewController.h"
#import "SWDashboardViewController.h"
#import "SalesOrderNewViewController.h"
#import "DashboardAlSeerViewController.h"
#import "AboutUsViewController.h"
#import "SalesWorxSyncPopoverViewController.h"
#import "SalesWorxFieldSalesDashboardViewController.h"

@interface MenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource , SynViewControllerDelegate> {
    UITableView *tableView;
    NSArray *modules;
    SynViewController *synch;
    
    SWLoadingView *loadingView;
    NSString *dateString;
    NSString *isCollectionEnable;
    //UINavigationController *navigationControllerFront;
   // SWSplitViewController *revealController;
    
    
    SettingViewController *settingVController;
    //
    AboutUsViewController *aboutVController;
    SWDashboardViewController *dashBViewController;
    SalesOrderNewViewController *newSalesOrder;
    DashboardAlSeerViewController* alseerDashBoard;
    
    
    BOOL customerIsCollapsed;
    BOOL siteIsCollapsed ;
    
    
    BOOL customerAlreadyCollapsed;
    
    
    UIViewController *CommunicationViewControllerNew1,*SurveyFormViewController1,*SWDashboardViewController1,*SalesWorxRouteViewController1,*SWProductListViewController1,*SWCustomerListViewController1,*ReportViewController1,*SWCollectionCustListViewController1,*newSalesOrder1,*dashboardAlSeerViewController1;
}

@end
