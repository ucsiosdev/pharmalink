//
//  SalesWorxAssortmentBonusManager.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/26/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxAssortmentBonusManager.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "AppControl.h"
@implementation SalesWorxAssortmentBonusManager

-(NSMutableArray *)FetchAssortmentBonusPlansAvailableForProduct:(NSString *)Item_Code ProductsArray:(NSMutableArray *)productsArray/**array of Products Dictionaries*/
{
    NSString *AssortmentItemsquery;
    
    AssortmentItemsquery=[NSString stringWithFormat:@"Select * from TBL_BNS_Assortment_Items where Assortment_Plan_ID in ( select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B  on A.Item_Code=B.Item_Code where Is_Get_Item='N' AND A.Item_code='%@') order by Assortment_Plan_ID" ,Item_Code];
    
    
    NSMutableArray *plansArray=[[NSMutableArray alloc] init];
    
    NSString *AssortmentSlabsquery= [NSString stringWithFormat:@"Select * from TBL_BNS_Assortment_Slabs Order By Prom_Qty_From"];
    
    NSMutableArray *AssortmentSlabsDataArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:AssortmentSlabsquery];
    
    
    NSLog(@"AssortmentPlansquery %@",AssortmentItemsquery);
    
    NSMutableArray *AssortmentItems= [[SWDatabaseManager retrieveManager]fetchDataForQuery:AssortmentItemsquery];
    
    NSArray *assortmentPlansArray = [[NSMutableArray alloc]initWithArray:[AssortmentItems valueForKeyPath:@"@distinctUnionOfObjects.Assortment_Plan_ID"]];
    
    for (NSInteger i=0;i<assortmentPlansArray.count;i++)
    {
        
        NSPredicate *Assortment_Plan_IDPredicate=[NSPredicate predicateWithFormat:@"SELF.Assortment_Plan_ID ==[cd] %@",[assortmentPlansArray objectAtIndex:i]];
        NSMutableArray *tempAssortmentPlanDetailsArray=[[AssortmentItems filteredArrayUsingPredicate:Assortment_Plan_IDPredicate] mutableCopy] ;
        
        NSMutableArray *bonusProductsArray=[[NSMutableArray alloc]init];
        NSMutableArray *offerProductsArray=[[NSMutableArray alloc]init];
        
        
        NSMutableArray *tempAssortmentSlabsDetailsArray=[[AssortmentSlabsDataArray filteredArrayUsingPredicate:Assortment_Plan_IDPredicate] mutableCopy] ;
        
        
        if(tempAssortmentSlabsDetailsArray.count>0)
        {
            NSMutableArray *mandatoryProductCodesArray=[[NSMutableArray alloc]init];
            SalesworxAssortmentBonusPlan *plan=[[SalesworxAssortmentBonusPlan alloc]init];
            for (NSInteger j=0;j<tempAssortmentPlanDetailsArray.count;j++)
            {
                NSPredicate *itemCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Item_Code"]];
                NSMutableArray *tempProductsArray=[[productsArray filteredArrayUsingPredicate:itemCodeProductPredicate] mutableCopy] ;
                if(tempProductsArray.count==1)
                {
                    if([[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Is_Get_Item"] isEqualToString:@"Y"])
                        [bonusProductsArray addObject:[tempProductsArray objectAtIndex:0]];
                    else{
                        [offerProductsArray addObject:[tempProductsArray objectAtIndex:0]];
                        if([[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Is_Mandatory"] isEqualToString:@"Y"] )
                            [mandatoryProductCodesArray addObject:[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Item_Code"]];
                    }
                }
                else
                {
                    bonusProductsArray=[[NSMutableArray alloc]init];
                    offerProductsArray=[[NSMutableArray alloc]init];
                    break;
                }
                
            }
            if(bonusProductsArray.count>0 && offerProductsArray.count>0)
            {
                NSMutableArray *PlanSlabsArray=[[NSMutableArray alloc]init];
                for(NSInteger k=0;k<tempAssortmentSlabsDetailsArray.count;k++)
                {
                    NSDictionary *tempSlabData=[tempAssortmentSlabsDetailsArray objectAtIndex:k];
                    SalesworxAssortmentBonusSlab *slab=[[SalesworxAssortmentBonusSlab alloc]init];
                    slab.Prom_Qty_From=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Prom_Qty_From"] ];
                    slab.Prom_Qty_To=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Prom_Qty_To"]];
                    slab.Price_Break_Type_Code=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Price_Break_Type_Code"]];
                    slab.Get_Qty=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Get_Qty"]];
                    [PlanSlabsArray addObject:slab];
                }
                
                plan.PlanId=[[assortmentPlansArray objectAtIndex:i] integerValue];
                plan.bonusProductsArray=bonusProductsArray;
                plan.OfferProductsArray=offerProductsArray;
                plan.MandatoryOfferProductItemCodesArray=mandatoryProductCodesArray;
                plan.SlabsArray=PlanSlabsArray;
                [plansArray addObject:plan];
            }
        }
        
    }
    return plansArray;
}




-(NSMutableArray *)FetchAssortmentBonusPlansAvailableForProducts:(NSMutableArray *)productsArray/**array of "<Products>" objects*/
{
    NSString *AssortmentItemsquery;
    
    AssortmentItemsquery=[NSString stringWithFormat:@"Select * from TBL_BNS_Assortment_Items where Assortment_Plan_ID in ( select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B  on A.Item_Code=B.Item_Code where Is_Get_Item='Y') order by Assortment_Plan_ID"];

    
    NSMutableArray *plansArray=[[NSMutableArray alloc] init];
    
    NSString *AssortmentSlabsquery= [NSString stringWithFormat:@"Select * from TBL_BNS_Assortment_Slabs Order By Prom_Qty_From"];
    
    NSMutableArray *AssortmentSlabsDataArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:AssortmentSlabsquery];
    
    
    NSLog(@"AssortmentPlansquery %@",AssortmentItemsquery);
    
    NSMutableArray *AssortmentItems= [[SWDatabaseManager retrieveManager]fetchDataForQuery:AssortmentItemsquery];
    
    NSArray *assortmentPlansArray = [[NSMutableArray alloc]initWithArray:[AssortmentItems valueForKeyPath:@"@distinctUnionOfObjects.Assortment_Plan_ID"]];
    
    for (NSInteger i=0;i<assortmentPlansArray.count;i++)
    {
        
        NSPredicate *Assortment_Plan_IDPredicate=[NSPredicate predicateWithFormat:@"SELF.Assortment_Plan_ID ==[cd] %@",[assortmentPlansArray objectAtIndex:i]];
        NSMutableArray *tempAssortmentPlanDetailsArray=[[AssortmentItems filteredArrayUsingPredicate:Assortment_Plan_IDPredicate] mutableCopy] ;
        
        NSMutableArray *bonusProductsArray=[[NSMutableArray alloc]init];
        NSMutableArray *offerProductsArray=[[NSMutableArray alloc]init];
        
        
        NSMutableArray *tempAssortmentSlabsDetailsArray=[[AssortmentSlabsDataArray filteredArrayUsingPredicate:Assortment_Plan_IDPredicate] mutableCopy] ;
        
        
        if(tempAssortmentSlabsDetailsArray.count>0)
        {
            NSMutableArray *mandatoryProductCodesArray=[[NSMutableArray alloc]init];
            SalesworxAssortmentBonusPlan *plan=[[SalesworxAssortmentBonusPlan alloc]init];
            for (NSInteger j=0;j<tempAssortmentPlanDetailsArray.count;j++)
            {
                NSPredicate *itemCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Item_Code"]];
                NSMutableArray *tempProductsArray=[[productsArray filteredArrayUsingPredicate:itemCodeProductPredicate] mutableCopy] ;
                if(tempProductsArray.count==1)
                {
                    if([[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Is_Get_Item"] isEqualToString:@"Y"])
                        [bonusProductsArray addObject:[tempProductsArray objectAtIndex:0]];
                    else{
                        [offerProductsArray addObject:[tempProductsArray objectAtIndex:0]];
                        if([[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Is_Mandatory"] isEqualToString:@"Y"] )
                            [mandatoryProductCodesArray addObject:[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Item_Code"]];
                    }
                }
                else
                {
                    bonusProductsArray=[[NSMutableArray alloc]init];
                    offerProductsArray=[[NSMutableArray alloc]init];
                    break;
                }
                
            }
            if(bonusProductsArray.count>0 && offerProductsArray.count>0)
            {
                NSMutableArray *PlanSlabsArray=[[NSMutableArray alloc]init];
                for(NSInteger k=0;k<tempAssortmentSlabsDetailsArray.count;k++)
                {
                    NSDictionary *tempSlabData=[tempAssortmentSlabsDetailsArray objectAtIndex:k];
                    SalesworxAssortmentBonusSlab *slab=[[SalesworxAssortmentBonusSlab alloc]init];
                    slab.Prom_Qty_From=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Prom_Qty_From"] ];
                    slab.Prom_Qty_To=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Prom_Qty_To"]];
                    slab.Price_Break_Type_Code=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Price_Break_Type_Code"]];
                    slab.Get_Qty=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Get_Qty"]];
                    [PlanSlabsArray addObject:slab];
                }
                
                plan.PlanId=[[assortmentPlansArray objectAtIndex:i] integerValue];
                plan.bonusProductsArray=bonusProductsArray;
                plan.OfferProductsArray=offerProductsArray;
                plan.MandatoryOfferProductItemCodesArray=mandatoryProductCodesArray;
                plan.SlabsArray=PlanSlabsArray;
                [plansArray addObject:plan];
            }
        }
        
    }
    return plansArray;
}





-(NSMutableArray *)FetchAssortmentBonusPlansForCategoryProducts:(NSMutableArray *)productsArray ForCustomer:(Customer *)customer/**array of "<Products>" objects*/
{
    NSString *AssortmentItemsquery;
    AppControl *appControl=[AppControl retrieveSingleton];
    if([appControl.ENABLE_BONUS_GROUPS isEqualToString:KAppControlsYESCode]){
        
        AssortmentItemsquery=[NSString stringWithFormat:@"Select * from TBL_BNS_Assortment_Items where Assortment_Plan_ID in ( select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B  on A.Item_Code=B.Item_Code where B.Category in('%@') and Is_Get_Item='Y') AND Assortment_Plan_ID IN (Select Bonus_Plan_ID from TBL_Customer_Bonus_Map where Customer_ID='%@' AND Site_Use_ID='%@' AND Plan_Type='Assortment') order by Assortment_Plan_ID",[[productsArray objectAtIndex:0] valueForKey:@"Category"],customer.Customer_ID,customer.Site_Use_ID];

    }
    else{
        AssortmentItemsquery=[NSString stringWithFormat:@"Select * from TBL_BNS_Assortment_Items where Assortment_Plan_ID in ( select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B  on A.Item_Code=B.Item_Code where B.Category in('%@') and Is_Get_Item='Y') order by Assortment_Plan_ID",[[productsArray objectAtIndex:0] valueForKey:@"Category"]];
    }
    
    NSMutableArray *plansArray=[[NSMutableArray alloc] init];
    
    NSString *AssortmentSlabsquery= [NSString stringWithFormat:@"select * from TBL_BNS_Assortment_Slabs Order By Prom_Qty_From"];

    NSMutableArray *AssortmentSlabsDataArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:AssortmentSlabsquery];

    
    NSLog(@"AssortmentPlansquery %@",AssortmentItemsquery);
    
    NSMutableArray *AssortmentItems= [[SWDatabaseManager retrieveManager]fetchDataForQuery:AssortmentItemsquery];
    
    NSArray *assortmentPlansArray = [[NSMutableArray alloc]initWithArray:[AssortmentItems valueForKeyPath:@"@distinctUnionOfObjects.Assortment_Plan_ID"]];
    
    for (NSInteger i=0;i<assortmentPlansArray.count;i++)
    {
        
        NSPredicate *Assortment_Plan_IDPredicate=[NSPredicate predicateWithFormat:@"SELF.Assortment_Plan_ID ==[cd] %@",[assortmentPlansArray objectAtIndex:i]];
        NSMutableArray *tempAssortmentPlanDetailsArray=[[AssortmentItems filteredArrayUsingPredicate:Assortment_Plan_IDPredicate] mutableCopy] ;
        
        NSMutableArray *bonusProductsArray=[[NSMutableArray alloc]init];
        NSMutableArray *offerProductsArray=[[NSMutableArray alloc]init];

        
        NSMutableArray *tempAssortmentSlabsDetailsArray=[[AssortmentSlabsDataArray filteredArrayUsingPredicate:Assortment_Plan_IDPredicate] mutableCopy] ;
        
        
        if(tempAssortmentSlabsDetailsArray.count>0)
        {
            NSMutableArray *mandatoryProductCodesArray=[[NSMutableArray alloc]init];
            SalesworxAssortmentBonusPlan *plan=[[SalesworxAssortmentBonusPlan alloc]init];
            for (NSInteger j=0;j<tempAssortmentPlanDetailsArray.count;j++)
            {
                NSPredicate *itemCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Item_Code"]];
                NSMutableArray *tempProductsArray=[[productsArray filteredArrayUsingPredicate:itemCodeProductPredicate] mutableCopy] ;
                if(tempProductsArray.count==1)
                {
                    if([[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Is_Get_Item"] isEqualToString:@"Y"])
                            [bonusProductsArray addObject:[tempProductsArray objectAtIndex:0]];
                    else{
                            [offerProductsArray addObject:[tempProductsArray objectAtIndex:0]];
                            if([[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Is_Mandatory"] isEqualToString:@"Y"] )
                                [mandatoryProductCodesArray addObject:[[tempAssortmentPlanDetailsArray objectAtIndex:j] valueForKey:@"Item_Code"]];
                        }
                }
                else
                {
                   bonusProductsArray=[[NSMutableArray alloc]init];
                    offerProductsArray=[[NSMutableArray alloc]init];
                    break;
                }

            }
            if(bonusProductsArray.count>0 && offerProductsArray.count>0)
            {
                NSMutableArray *PlanSlabsArray=[[NSMutableArray alloc]init];
                for(NSInteger k=0;k<tempAssortmentSlabsDetailsArray.count;k++)
                {
                    NSDictionary *tempSlabData=[tempAssortmentSlabsDetailsArray objectAtIndex:k];
                    SalesworxAssortmentBonusSlab *slab=[[SalesworxAssortmentBonusSlab alloc]init];
                    slab.Prom_Qty_From=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Prom_Qty_From"] ];
                    slab.Prom_Qty_To=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Prom_Qty_To"]];
                    slab.Price_Break_Type_Code=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Price_Break_Type_Code"]];
                    slab.Get_Qty=[SWDefaults getValidStringValue:[tempSlabData valueForKey:@"Get_Qty"]];
                    [PlanSlabsArray addObject:slab];
                }
                
                plan.PlanId=[[assortmentPlansArray objectAtIndex:i] integerValue];
                plan.bonusProductsArray=bonusProductsArray;
                plan.OfferProductsArray=offerProductsArray;
                plan.MandatoryOfferProductItemCodesArray=mandatoryProductCodesArray;
                plan.SlabsArray=PlanSlabsArray;
                [plansArray addObject:plan];
            }
        }
        
    }
    return plansArray;
}


-(NSInteger )FetchAvailableBonusPlanForProduct:(Products *)product AssortmentPlans:(NSMutableArray *)plans
{
    NSMutableArray  *ProductAssortmentPlans=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B on A.Item_Code=B.Item_Code Where A.Item_Code='%@' and Is_Get_Item='N'",product.Item_Code]];
    if(ProductAssortmentPlans.count>0)
    {
        ProductAssortmentPlans= [ProductAssortmentPlans valueForKey:@"Assortment_Plan_ID"];
        
        return [[ProductAssortmentPlans  objectAtIndex:0] integerValue];
    }
    
    return NSNotFound;
}

-(BOOL)IsAnyAssortmentBonusAvailableForTheProduct:(Products *)product AssortmentPlans:(NSMutableArray *)plans
{
    NSMutableArray  *ProductAssortmentPlans=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B on A.Item_Code=B.Item_Code Where A.Item_Code='%@' and Is_Get_Item='N'",product.Item_Code]];
    if(ProductAssortmentPlans.count>0)
    {
       ProductAssortmentPlans= [ProductAssortmentPlans valueForKey:@"Assortment_Plan_ID"];
    
        if([[plans filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId IN %@", ProductAssortmentPlans]] count]>0){
            return YES;
        }
    }
    
    return NO;
}

-(NSMutableDictionary *)FetchNewOrUpdatedAssortmentBonusPlanAvailableForSalesOrder:(SalesOrder *)salesOrder ForProduct:(SalesOrderLineItem *)OrderItem{
    
    NSMutableDictionary *planDetailsDic=[[NSMutableDictionary alloc] init];
    
    NSMutableArray  *ProductAssortmentPlans=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B on A.Item_Code=B.Item_Code Where A.Item_Code='%@' and Is_Get_Item='N'",OrderItem.OrderProduct.Item_Code]];
    if(ProductAssortmentPlans.count>0)
    {
        ProductAssortmentPlans= [ProductAssortmentPlans valueForKey:@"Assortment_Plan_ID"];
        
        if([[salesOrder.AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId IN %@", ProductAssortmentPlans]] count]>0)
        {
         
            SalesworxAssortmentBonusPlan *plan=[[[salesOrder.AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId IN %@", ProductAssortmentPlans]] mutableCopy] objectAtIndex:0];
            double PlanOrderQty=[self getProductsQuantityOfPlanID:plan ForSalesOrder:salesOrder];
            double bonusQty=[plan CalculateBonusForQty:PlanOrderQty];
            
            if(bonusQty>0)
            {
                [planDetailsDic setObject:plan forKey:@"Plan"];
                [planDetailsDic setValue:[NSString stringWithFormat:@"%f",bonusQty] forKey:@"Qty"];

                return planDetailsDic;
            }
        }
    }
    return nil;

}


-(double)FetchAvailableBonusQuantityForSalesOrder:(SalesOrder *)salesOrder ForPlanId:(NSInteger)PlanId
{

    SalesworxAssortmentBonusPlan *plan=[[[salesOrder.AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId==%d", PlanId]] mutableCopy] objectAtIndex:0];
    double PlanOrderQty=[self getProductsQuantityOfPlanID:plan ForSalesOrder:salesOrder];
    double bonusQty=[plan CalculateBonusForQty:PlanOrderQty];    
    return bonusQty;

}

-(double)getProductsQuantityOfPlanID:(SalesworxAssortmentBonusPlan *)Plan ForSalesOrder:(SalesOrder *)Order
{
    double PlanIdOfferProductsCount=0;
    
    NSMutableArray *AssignedMandatoryProductCodesArray=[[NSMutableArray alloc]init];
    NSMutableArray *PlanMandatoryOfferProductsItemsCodes=Plan.MandatoryOfferProductItemCodesArray;
    
    NSMutableArray *PlanOfferProdctsItemcodesArray=[Plan.OfferProductsArray valueForKey:@"Item_Code"];
    
    
    for (NSInteger i=0; i<Order.SalesOrderLineItemsArray.count; i++) {
        SalesOrderLineItem *tempOrcerLineItem=[Order.SalesOrderLineItemsArray objectAtIndex:i];

        if(tempOrcerLineItem.manualFOCQty==0 && [PlanOfferProdctsItemcodesArray containsObject:[[(SalesOrderLineItem *)tempOrcerLineItem OrderProduct] Item_Code]] ) /** Asst.bonus not applicable for manual foc given products*/
            
        {
            double orderrimaryUOMProductQty=0;
            if([tempOrcerLineItem.OrderProduct.primaryUOM isEqualToString:tempOrcerLineItem.OrderProduct.selectedUOM]){
                 orderrimaryUOMProductQty= [[(SalesOrderLineItem *)tempOrcerLineItem OrderProductQty] doubleValue];
            }
            else{
                orderrimaryUOMProductQty=tempOrcerLineItem.OrderProductQty.doubleValue*[SWDefaults getConversionRateWithRespectToPrimaryUOMCodeForProduct:[tempOrcerLineItem.OrderProduct copy]];
            }
            
            PlanIdOfferProductsCount=PlanIdOfferProductsCount+orderrimaryUOMProductQty;
            
            
            /** adding assigned mandatory product codes into abn arry to cross check with plan mandatory item codes*/
            if([PlanMandatoryOfferProductsItemsCodes containsObject:[[(SalesOrderLineItem *)tempOrcerLineItem OrderProduct] Item_Code]]&&
               ![AssignedMandatoryProductCodesArray containsObject:[[(SalesOrderLineItem *)tempOrcerLineItem OrderProduct] Item_Code]])
            {
                [AssignedMandatoryProductCodesArray addObject:[[(SalesOrderLineItem *)tempOrcerLineItem OrderProduct] Item_Code]];
            }
        }
    }
    if(AssignedMandatoryProductCodesArray.count==PlanMandatoryOfferProductsItemsCodes.count)
        return PlanIdOfferProductsCount;
    return 0;
}

-(SalesOrder *)AssignDefaultBonusPlansForSalesOrder:(SalesOrder *)Order
{
    Order.AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<Order.SalesOrderLineItemsArray.count; i++) {
        SalesOrderLineItem *OrderLineItem=[Order.SalesOrderLineItemsArray objectAtIndex:i];

        if(OrderLineItem.AppliedAssortMentPlanID==NSNotFound)
            Order=[self AssignDefaultAsstBonusForLineItem:OrderLineItem ForSalesOrder:Order];
    }
    return Order;
}
-(SalesOrder *)AssignDefaultAsstBonusForLineItem:(SalesOrderLineItem *)OrderLineItem ForSalesOrder:(SalesOrder *)Order
{
    NSMutableArray *tempAssortmentBonusProductsArray=[[NSMutableArray alloc]init];

    if([self IsAnyAssortmentBonusAvailableForTheProduct:OrderLineItem.OrderProduct AssortmentPlans:Order.AssortmentPlansArray])
    {
        NSMutableDictionary *planDetailsDic= [self FetchNewOrUpdatedAssortmentBonusPlanAvailableForSalesOrder:Order ForProduct:OrderLineItem];
        if(planDetailsDic.count>0)
        {
            SalesworxAssortmentBonusPlan *availablePlan=(SalesworxAssortmentBonusPlan*)[planDetailsDic valueForKey:@"Plan"];
            NSInteger BonusQty=[[planDetailsDic valueForKey:@"Qty"] integerValue];
            NSMutableArray *BonusProductsArray=  availablePlan.bonusProductsArray;
            
            SalesworxAssortmentBonusProduct *assortmentBonusProduct= [self PrepareAssortmentBonusProduct:[(Products *)[BonusProductsArray objectAtIndex:0] copy] ForPlan:availablePlan WithQty:BonusQty AndDefaultPlanBonusQty:[NSDecimalNumber decimalNumberWithString:[planDetailsDic valueForKey:@"Qty"]]];
            [tempAssortmentBonusProductsArray addObject:assortmentBonusProduct];
            
            if(tempAssortmentBonusProductsArray.count>0){
                Order.SalesOrderLineItemsArray=[self UpdateAsstBonusIDsForSalesOrderItems:Order WithApliedBonusDetils:availablePlan];
                [Order.AssortmentBonusProductsArray addObjectsFromArray:tempAssortmentBonusProductsArray];
            }
            
        }
        
        
    }
    return  Order;
}
-(NSMutableArray *)UpdateAsstBonusIDsForSalesOrderItems:(SalesOrder *)Order WithApliedBonusDetils:(SalesworxAssortmentBonusPlan *)bnsPlan
{

    NSMutableArray *offerProductCodesArray= [bnsPlan.OfferProductsArray valueForKey:@"Item_Code"];
    NSMutableArray *orderLineItemsArray=Order.SalesOrderLineItemsArray;
    NSMutableArray *tempBonusAplliedOrderLinesArray=[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<orderLineItemsArray.count; i++) {
        SalesOrderLineItem *tempLineItem=  [orderLineItemsArray objectAtIndex:i];
        if([offerProductCodesArray containsObject:tempLineItem.OrderProduct.Item_Code] && tempLineItem.manualFOCQty==0)/** Asst.bonus not applicable for manual foc given products*/
        {
            tempLineItem.showApliedAssortMentPlanBonusProducts=NO;
            tempLineItem.AppliedAssortMentPlanID=bnsPlan.PlanId;
            if(tempBonusAplliedOrderLinesArray.count==0)
                tempLineItem.showApliedAssortMentPlanBonusProducts=YES;/** Assigning showApliedAssortMentPlanBonusProducts yes to first product in order line items*/
            
            [orderLineItemsArray replaceObjectAtIndex:i withObject:tempLineItem];
            [tempBonusAplliedOrderLinesArray addObject:tempLineItem];
        }
    }
    
    
    
    return orderLineItemsArray;
}
-(SalesworxAssortmentBonusProduct *)PrepareAssortmentBonusProduct:(Products *)pro ForPlan:(SalesworxAssortmentBonusPlan *)Plan WithQty:(NSInteger)bonusQty AndDefaultPlanBonusQty:(NSDecimalNumber *)planBnsQty
{
    SalesworxAssortmentBonusProduct *assortmentBonusProduct=[[SalesworxAssortmentBonusProduct alloc]init];
    assortmentBonusProduct.product=pro;
    assortmentBonusProduct.assignedQty=[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%ld",(long)bonusQty]];
    assortmentBonusProduct.PlanId=Plan.PlanId;
    assortmentBonusProduct.PlanDefaultBnsQty=planBnsQty;
    return assortmentBonusProduct;
}
/** confirm Order*/
-(SalesOrder *)UpdateConfirmesSalesOrderLineItems:(SalesOrder *)Order WithDBAssortmentBonusProductsArray:(NSMutableArray *)DBAsstBnsProductsArray AndProductsArray:(NSMutableArray *)ProductsArray
{
    NSMutableArray *AssortmentBonusProductsArray=[[NSMutableArray alloc]init];
    Order.AssortmentBonusProductsArray=AssortmentBonusProductsArray;
    for (NSInteger i=0; i<DBAsstBnsProductsArray.count; i++) {
       Order=[self AddDBAsstBnsProduct:[DBAsstBnsProductsArray objectAtIndex:i] ToSalesOrder:Order AndProductsArray:ProductsArray];
    }
    return Order;
}
-(SalesOrder *)AddDBAsstBnsProduct:(NSDictionary *)dic ToSalesOrder:(SalesOrder *)Order AndProductsArray:(NSMutableArray *)ProductsArray
{
    
    NSMutableArray *AssortmentBonusProductsArray=Order.AssortmentBonusProductsArray;

    NSDictionary *tempDBBnsItem=  dic;
    NSLog(@"tempDBBnsItem %@",tempDBBnsItem);
    NSString * tempDBBnsItemCode= [SWDefaults getValidStringValue:[tempDBBnsItem valueForKey:@"Item_Code"]];
    NSPredicate *itemCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",tempDBBnsItemCode];
    NSMutableArray *tempProductsArray= [[ProductsArray filteredArrayUsingPredicate:itemCodeProductPredicate] mutableCopy];
    if(tempProductsArray.count>0)
    {
        NSMutableArray *tempBonusPlanArray=[[Order.AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId==%d", [[SWDefaults getValidStringValue:[tempDBBnsItem valueForKey:@"Assortment_Plan_ID"]] integerValue]]] mutableCopy];
        
        if(tempBonusPlanArray.count>0)
        {
            SalesworxAssortmentBonusProduct *assortmentBonusProduct=[self PrepareAssortmentBonusProduct:[(Products *)[tempProductsArray objectAtIndex:0] copy] ForPlan:[tempBonusPlanArray objectAtIndex:0] WithQty:[[tempDBBnsItem valueForKey:@"Ordered_Quantity"] integerValue] AndDefaultPlanBonusQty:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",[[tempDBBnsItem valueForKey:@"Def_Bonus"] integerValue]]]];
            [AssortmentBonusProductsArray addObject:assortmentBonusProduct];
            Order.SalesOrderLineItemsArray=[self UpdateAsstBonusIDsForSalesOrderItems:Order WithApliedBonusDetils:[tempBonusPlanArray objectAtIndex:0]];
        }
    }
    return Order;
}
/*Proforma Order Bonus Checking*/
-(NSDictionary *)UpdateProformaSalesOrderLineItems:(SalesOrder *)Order WithDBAssortmentBonusProductsArray:(NSMutableArray *)DBAsstBnsProductsArray AndProductsArray:(NSMutableArray *)ProductsArray
{
    BOOL ShowOneOrMoreProductsAsstBonusProductChanged=NO;
    
    
    NSArray *AppliedAssortmentPlansArray = [[NSMutableArray alloc]initWithArray:[DBAsstBnsProductsArray valueForKeyPath:@"@distinctUnionOfObjects.Assortment_Plan_ID"]];
    Order.AssortmentBonusProductsArray=[[NSMutableArray alloc] init];

    for (NSInteger i=0; i<AppliedAssortmentPlansArray.count; i++) {
       NSInteger tempAppliedPlanId= [[SWDefaults getValidStringValue:[AppliedAssortmentPlansArray objectAtIndex:i]] integerValue];
        
        NSMutableArray *tempPlansObjectsArray=[[Order.AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId==%d", tempAppliedPlanId]] mutableCopy];
        
        if(tempPlansObjectsArray.count>0){ /** applied bonus plan exist*/
            SalesworxAssortmentBonusPlan *AplliedBonusPlanObject=[tempPlansObjectsArray objectAtIndex:0];
            
            
            NSMutableArray *appliedBonusPlanAllocatedProducts=[[DBAsstBnsProductsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Assortment_Plan_ID==%d", AplliedBonusPlanObject.PlanId]] mutableCopy];

            NSNumber* sum = [[appliedBonusPlanAllocatedProducts valueForKey:@"Ordered_Quantity"] valueForKeyPath: @"@sum.self"];
            double calculatedBonusQtyAsPerExistingBonusRules= [self FetchAvailableBonusQuantityForSalesOrder:Order ForPlanId:AplliedBonusPlanObject.PlanId];

            if(sum.integerValue==[[NSString stringWithFormat:@"%f",calculatedBonusQtyAsPerExistingBonusRules] integerValue])
            {
                    BOOL isMandatoryItemsAddedToSalesOrder=YES;
                    for (NSInteger M=0; M<AplliedBonusPlanObject.MandatoryOfferProductItemCodesArray.count; M++) {
                        NSMutableArray *tempOrderLineItems=[[Order.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.OrderProduct.Item_Code IN %@",[AplliedBonusPlanObject.MandatoryOfferProductItemCodesArray objectAtIndex:M]]] mutableCopy];
                        if(tempOrderLineItems.count==0)
                        {
                            isMandatoryItemsAddedToSalesOrder=NO;
                            ShowOneOrMoreProductsAsstBonusProductChanged=YES;
                            break;
                        }
                    }
                    
                    if(isMandatoryItemsAddedToSalesOrder)
                    {
                        for (NSInteger b=0; b<appliedBonusPlanAllocatedProducts.count; b++) {
                            Order=[self AddDBAsstBnsProduct:[appliedBonusPlanAllocatedProducts objectAtIndex:b] ToSalesOrder:Order AndProductsArray:ProductsArray];
                        }
                    }
            }
            
            
        }
    }
    
    NSMutableArray *OrderLineItemsWithOutAsstBnsPlan=[[NSMutableArray alloc]init];
    
    if(Order.AssortmentPlansArray.count>0)
        OrderLineItemsWithOutAsstBnsPlan=[[Order.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(NOT (SELF.AppliedAssortMentPlanID IN %@)) AND manualFOCQty==0",[Order.AssortmentPlansArray valueForKey:@"PlanId"]]] mutableCopy];
    
    for (NSInteger j=0; j<OrderLineItemsWithOutAsstBnsPlan.count; j++) {
        Order=[self AssignDefaultAsstBonusForLineItem:[OrderLineItemsWithOutAsstBnsPlan objectAtIndex:j] ForSalesOrder:Order];
        
        /** checking for change in the applied plan Id*/
        SalesOrderLineItem *updatedLineItem =[[[Order.SalesOrderLineItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Guid==%@",[(SalesOrderLineItem *)[OrderLineItemsWithOutAsstBnsPlan objectAtIndex:j] Guid]]] mutableCopy] objectAtIndex:0];
        if(updatedLineItem.AppliedAssortMentPlanID!=NSNotFound){
            ShowOneOrMoreProductsAsstBonusProductChanged=YES;
        }
    }
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:Order,@"SalesOrder",ShowOneOrMoreProductsAsstBonusProductChanged==YES?@"Y":@"N",@"ShowOneOrMoreProductsAsstBonusProductChanged", nil];
}

@end
