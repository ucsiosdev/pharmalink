//
//  SalesWorxSalesSummaryTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxSalesSummaryTableViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocumentNo;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocumentType;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDate;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblName;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel *lblAmount;

@property(nonatomic) BOOL isHeader;


@end
