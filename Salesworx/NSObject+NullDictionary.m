//
//  NSObject+NullDictionary.m
//  AWR-CMS
//
//  Created by Unique Computer Systems on 7/14/14.
//  Copyright (c) 2014 Saad Ansari. All rights reserved.
//

#import "NSObject+NullDictionary.h"

@implementation NSObject (NullDictionary)

+ (NSMutableDictionary *)nullFreeDictionaryWithDictionary:(NSDictionary *)dictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    // Iterate through each key-object pair.
    [dictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        // If object is a dictionary, recursively remove NSNull from dictionary.
        if ([object isKindOfClass:[NSDictionary class]]) {
            NSDictionary *innerDict = object;
            replaced[key] = [NSMutableDictionary nullFreeDictionaryWithDictionary:innerDict];
        }
        // If object is an array, enumerate through array.
        else if ([object isKindOfClass:[NSArray class]]) {
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                // If object is a dictionary, recursively remove NSNull from dictionary.
                if ([record isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *nullFreeRecord = [NSMutableDictionary nullFreeDictionaryWithDictionary:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
                else {
                    if (object == [NSNull null] ) {
                        [nullFreeRecords addObject:@""];
                        
                       // NSLog(@"null caught for %@", object);
                        
                    }
                    else {
                        [nullFreeRecords addObject:record];
                    }
                }
            }
            replaced[key] = nullFreeRecords;
        }
        else {
            // Replace [NSNull null] with nil string "" to avoid having to perform null comparisons while parsing.
            if (object == [NSNull null]) {
                replaced[key] = @"N/A";
               // NSLog(@"null caught for %@", object);

            }
        }
    }];
    
    return [NSMutableDictionary dictionaryWithDictionary:replaced];
}



@end
