//
//  LoginViewController.h
//  Salesworx
//
//  Created by Saad Ansari on 12/8/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "FTPBaseViewController.h"
#import "MedRepDefaults.h"
#import <MedRepButton.h>
#import <MessageUI/MessageUI.h>
#import "AppControl.h"
#import "SWAppDelegate.h"
#import "MedRepPanelTitle.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxFieldSalesDashboardViewController.h"
#import "MedrepTextfield.h"
#import "SalesWorxBluetoothManager.h"

@interface LoginViewController : FTPBaseViewController <UIAlertViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    //IBOutlet UITextField *txtUserName;
    //IBOutlet UITextField *txtPassword;
    IBOutlet UILabel *lblLogin;
    IBOutlet UILabel *lblVersion;
    IBOutlet UILabel *lblPowered;
    IBOutlet MedRepElementTitleLabel *userNameLbl;
    IBOutlet MedRepElementTitleLabel *passwordLbl;
    IBOutlet MedRepPanelTitle *lblActivatedUserName;
    
    IBOutlet MedRepButton *loginButton;
    IBOutlet MedRepPanelTitle *loginLabel;
    IBOutlet UIImageView *logoImageView;
    IBOutlet NSLayoutConstraint *loginViewTopConstraint;
    NSString *login;
    NSString *password;
    SWLoadingView *loadingView;
    NSString *ClientVersion;
    
    CGRect oldFrame;
    CGPoint originalCenter;
    
    IBOutlet UIView *loginCustomView;
    CGRect versionViewFrame;
    
    IBOutlet UILabel *versionNumber;
    AppControl *appControl;
    NSString *PDAComponents;
    BOOL isApplicationHaveTheValidMenuOptions;
    SWAppDelegate* appdel;
    
    BOOL isComingFromBeacon;
}
-(IBAction)buttonAction:(id)sender;
@property(strong,nonatomic) IBOutlet MedRepTextField* txtUserName;
@property(strong,nonatomic) IBOutlet MedRepTextField *txtPassword;
@property(strong,nonatomic)    IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIView *versionView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *customLoginViewTopConstraint;
@property(strong,nonatomic) NSString * matchedCustomerIDFromBeacon;

@end
