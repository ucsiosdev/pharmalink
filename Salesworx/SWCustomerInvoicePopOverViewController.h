//
//  SWCustomerInvoicePopOverViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWCustomerInvoicePopOverViewController : UIViewController


-(IBAction)closeTapped:(id)sender;

@property(strong,nonatomic) IBOutlet UILabel * orderNumberLbl;

@property(strong,nonatomic) IBOutlet UILabel * orderDateLbl;

@property(strong,nonatomic) UIPopoverController * invoicePopOver;

@property(strong,nonatomic) NSString* orderNumber;

@property(strong,nonatomic) NSString* orderDate;

@end
