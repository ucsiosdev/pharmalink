//
//  SalesWorxPaymentCollectionInvoicesHeaderTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxPaymentCollectionInvoicesHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocumentNo;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblInvoiceDate;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblBalanceAmt;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblSettledAmt;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPaidAmt;

@end
