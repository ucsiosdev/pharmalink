//
//  MedRepProductImageDisplayTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepProductImageDisplayTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *productImageView;

@end
