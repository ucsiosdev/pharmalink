//
//  NSObject+NullDictionary.h
//  AWR-CMS
//
//  Created by Unique Computer Systems on 7/14/14.
//  Copyright (c) 2014 Saad Ansari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NullDictionary)

+ (NSMutableDictionary *)nullFreeDictionaryWithDictionary:(NSDictionary *)dictionary;


@end
