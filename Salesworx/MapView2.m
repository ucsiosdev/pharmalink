//
//  MapViewController.m
//  Miller
//
//  Created by kadir pekel on 2/7/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MapView2.h"

@interface MapView2()

//-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded;
//-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) from to: (CLLocationCoordinate2D) to;

@end

@implementation MapView2

@synthesize lineColor;
@synthesize mapView,span,region;

- (id) initWithFrame:(CGRect) frame
{
	self = [super initWithFrame:frame];
	if (self != nil) {
       
        if (mapView == nil)
        {
            self.mapView = [[MKMapView alloc] init];
        }
        [self.mapView setDelegate:self];

        self.mapView.frame=CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self.mapView setMapType:MKMapTypeStandard];
		[self.mapView setZoomEnabled:YES];
		[self.mapView setScrollEnabled:YES];
		[self.mapView setShowsUserLocation:YES];
		[self.mapView setUserInteractionEnabled:YES];
        
        span.latitudeDelta=5;
        span.longitudeDelta=5;
        region.span=span;
        
        if ( (region.center.latitude >= -90) && (region.center.latitude <= 90) && (region.center.longitude >= -180) && (region.center.longitude <= 180))
        {
            [mapView setRegion:[mapView regionThatFits:region]];
        }
        [self addSubview:self.mapView];
	}
	return self;
}
#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{

}

#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    if(annotation == map.userLocation)
    {
        return nil;
    }
    static NSString *defaultID=@"MYLoction";
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultID] ;
    [pinView setSelected:YES animated:YES];
    [pinView setEnabled:YES];
    if (!pinView)
    {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultID] ;
        pinView.canShowCallout=YES;
        pinView.animatesDrop=YES;
    }
    return pinView;
}

-(void)RemoveIT
{
    switch (self.mapView.mapType)
    {
        case MKMapTypeHybrid:
        {
            self.mapView.mapType = MKMapTypeStandard;
        }
        break;
        
        case MKMapTypeStandard:
        {
            self.mapView.mapType = MKMapTypeHybrid;
        }
        break;
        
        default:
        break;
    }
    
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = NO;
    //[self.mapView removeAnnotations:self.mapView.annotations];
    self.mapView.delegate=nil;
    self.mapView = nil;
    //[self.mapView removeFromSuperview];
}



@end
