//
//  NSPredicate+Distinct.h
//
//  Created by Maurizio Cremaschi on 11/11/2013.
//  Copyright (c) 2013 Myfleek Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSPredicate (Distinct)

+ (NSPredicate *)predicateForDistinctWithProperty:(NSString *)property;

@end
