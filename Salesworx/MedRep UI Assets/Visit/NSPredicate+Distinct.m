//
//  NSPredicate+Distinct.m
//
//  Created by Maurizio Cremaschi on 11/11/2013.
//  Copyright (c) 2013 Myfleek Ltd. All rights reserved.
//

#import "NSPredicate+Distinct.h"

@implementation NSPredicate (Distinct)

+ (NSPredicate *)predicateForDistinctWithProperty:(NSString *)property
{
    __block NSMutableSet *set = [NSMutableSet new];
    return [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        id key = [evaluatedObject valueForKeyPath:property];
        BOOL contained = [set containsObject:key];
        if (!contained) {
            [set addObject:key];
        }
        return !contained;
    }];
}

@end
