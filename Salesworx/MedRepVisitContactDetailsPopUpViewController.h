//
//  MedRepVisitContactDetailsPopUpViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@interface MedRepVisitContactDetailsPopUpViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet UIView *headerView;
    IBOutlet MedRepElementDescriptionLabel*addressLabel;
    IBOutlet MedRepElementDescriptionLabel*cityLabel;
    IBOutlet MedRepElementDescriptionLabel*conatcatLabel;

}

@property (strong, nonatomic) IBOutlet  NSMutableDictionary* VisitDetailsDictionary;
@property(nonatomic) BOOL isObjectData;
@property(strong,nonatomic) SalesWorxBrandAmbassadorLocation * selectedLocation;
- (IBAction)doneButtontapped:(id)sender;
@end
