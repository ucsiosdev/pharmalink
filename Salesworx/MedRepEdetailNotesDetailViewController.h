//
//  MedRepEdetailNotesDetailViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "MedRepTextView.h"
#import "MedRepCustomClass.h"
#import "MedRepTextField.h"
#import "MedRepDoctorFilterDescriptionViewController.h"

@protocol SavedNoteDelegate <NSObject>


-(void)savedNoteDetails:(NSMutableArray*)updatedNotesArray;

@end

@interface MedRepEdetailNotesDetailViewController : SWViewController<UIPopoverControllerDelegate,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,SelectedFilterDelegate>


{
    VisitNotes * notes;
    
    IBOutlet MedRepTextField *doctorTextField;
    id delegate;
    
    NSString* locationType;
}

@property(nonatomic,strong) id delegate;

@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;

@property (strong, nonatomic) IBOutlet MedRepTextField *titleTxtFld;
@property(strong,nonatomic)UIPopoverController* notesPopOverController;

@property(strong,nonatomic)NSMutableDictionary* notesDetailsDict;

@property(strong,nonatomic)NSMutableArray* notesArray;

@property(nonatomic) BOOL isVisitCompleted;

@property(nonatomic) BOOL isUpdatingNotes;

@property(nonatomic) BOOL isAddingNotes;

@property(nonatomic) BOOL isInMultiCalls;

@property(nonatomic) BOOL dashBoardViewing;

@property(strong,nonatomic) NSIndexPath * selectedNoteIndex;

@property (strong, nonatomic) IBOutlet MedRepTextView *notesDescTxtView;



@property(strong,nonatomic) VisitDetails * visitDetails;
@end
