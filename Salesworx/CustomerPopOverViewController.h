//
//  CustomerPopOverViewController.h
//  Salesworx
//
//  Created by msaad on 6/19/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SWPlatform.h"

@interface CustomerPopOverViewController : SWTableViewController
{
    NSArray *types;
    SEL action;
}
@property (nonatomic, strong) NSArray *types;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
@end