//
//  MedRepPharmaciesFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/9/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepTextField.h"
@protocol PharmacyFilterDelegate <NSObject>

-(void)filteredPharmacyDetails:(NSMutableArray*)filteredPharmacyArray;

-(void)filteredPharmacyParameters:(NSMutableDictionary*)parameters;

-(void)pharmacyFilterDidCancel;

-(void)pharmacyFilterDidReset;

@end

@interface MedRepPharmaciesFilterViewController : UIViewController<SelectedFilterDelegate,UITextFieldDelegate>

{
    id pharmacyFilterDelegate;
    
    NSMutableDictionary * filterDict;
    
    NSString *selectedDataString;
    
    NSMutableArray* filteredContentArray,* descArray;
    
    
    IBOutlet MedRepTextField *tradeChannelTextField;
    IBOutlet MedRepTextField *locationTextField;

    IBOutlet UISwitch *statusSwitch;
    

}
@property(strong,nonatomic) id pharmacyFilterDelegate;
@property (strong, nonatomic) IBOutlet UISegmentedControl *typeSegment;
@property(strong,nonatomic)NSMutableArray* contentArray;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) UINavigationController *filterNavController;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property (strong, nonatomic) IBOutlet UIButton *tradeChannelBtn;
@property (strong, nonatomic) IBOutlet UIButton *locationsBtn;
@property(strong,nonatomic) NSMutableDictionary* previousFilteredParameters;

- (IBAction)resetButtonTapped:(id)sender;

@end
