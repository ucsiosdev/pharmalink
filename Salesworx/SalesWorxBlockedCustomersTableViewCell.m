//
//  SalesWorxBlockedCustomersTableViewCell.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBlockedCustomersTableViewCell.h"

@implementation SalesWorxBlockedCustomersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIsHeader:(BOOL)isHeader
{
    if (isHeader==YES) {
        
        CGRect sepFrame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1);
        UIView*  seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = kUITableViewSaperatorColor;
        [self addSubview:seperatorView];
    }
}

@end
