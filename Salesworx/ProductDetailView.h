//
//  ProductDetailView.h
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@interface ProductDetailView : DetailBaseView  {
    NSMutableDictionary *priceDict;
}

-(void)loadDetail;
- (id)initWithProduct:(NSDictionary *)product;
@end