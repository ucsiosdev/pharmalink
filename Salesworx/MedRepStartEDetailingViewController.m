//
//  MedRepStartEDetailingViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepStartEDetailingViewController.h"
#import "MedRepEdetailTaskViewController.h"
#import "MedRepEDetailingStartCallViewController.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepEDetailingEndVisitViewController.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepDefaults.h"
#import "MedRepProductsCollectionViewCell.h"
#import "MedRepCollectionViewCheckMarkViewController.h"

@interface MedRepStartEDetailingViewController ()

@end

@implementation MedRepStartEDetailingViewController

@synthesize waitTimeLbl,visitDetailsDict,locationLbl,doctorLbl,objectiveTxtView,accompaniedByLbl;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    
    //start a timer to capture face time, ie the time before meeting a doctor

    
    
    
    self.navigationItem.titleView =     self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Visit Information", nil)];
;

    
    testDate=[NSDate date];
    

    waitTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
    

    
    //check if task and notes are present for location Id and contact id
    
    //NSLog(@"visit details in start call %@", visitDetailsDict);
    
    
    
    
       // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    //stop wait timer and update value
    
    if (goToNextScreen)
    {
        [[NSUserDefaults standardUserDefaults]setObject:waitTimeLbl.text forKey:@"waitTimer"];
        
        NSLog(@"WAIT TIME WHILE DISAPPEARING %@", [[NSUserDefaults standardUserDefaults]objectForKey:@"waitTimer"]);
        
        
        [waitTimer invalidate];
        waitTimer=nil;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingMultipleVisits];
    }
    NSLog(@"visit details in will appear %@", [visitDetailsDict description]);
    
    
    
    waitTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    waitTimeLbl.textColor = KWaitTimeColor;
    

    
    NSLog(@"task array count in start call %d", _visitDetails.taskArray.count);
    
    VisitDetails * currentVisitDetails=[[VisitDetails alloc]init];
  
    VisitTask * task=[[VisitTask alloc]init];
    
    
    for (NSInteger i=0; i<self.visitDetails.taskArray.count; i++) {
        
        task= [self.visitDetails.taskArray objectAtIndex:i];
        
        NSLog(@"status in will appear %@", task.Status);
        
    }
    
    
    NSString* notesAvailable=[visitDetailsDict valueForKey:@"Notes_Available"];
    
    
    NSString* taskStatus=[visitDetailsDict valueForKey:@"TASK_STATUS"];
    
    if ([taskStatus isEqualToString:@"OVER_DUE"]) {
        
        [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
        
        
    }
    
    
    else if ([taskStatus isEqualToString:@"PENDING"])
        
    {
        [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
    }
    
    
    
    if ([notesAvailable isEqualToString:@"YES"]) {
        
        [notesStatusButton setBackgroundColor:KNOTESAVAILABLECOLOR];
        
    }
    
    else
    {
        [notesStatusButton setBackgroundColor:KNOTESUNAVAILABLECOLOR];
        
    }
    //objectiveTxtView.contentInset = UIEdgeInsetsMake(0, 6, 0, 0);

   
    
    
    //adding bar button items
    
    
    UIBarButtonItem* taskBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"medRepTasks.png"] style:UIBarButtonItemStylePlain target:self action:@selector(taskBarButtonTapped:)];
    
    
//    UIBarButtonItem* taskBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"T" style:UIBarButtonItemStylePlain target:self action:@selector(taskBarButtonTapped:)];
    
    
    
    UIBarButtonItem* notesBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"medRepNotes.png"] style:UIBarButtonItemStylePlain target:self action:@selector(notesBarButtonTapped:)];
    
    
//    UIBarButtonItem* notesBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"N" style:UIBarButtonItemStylePlain target:self action:@selector(notesBarButtonTapped:)];
    
    
    
     UIBarButtonItem* startVisitBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartCall"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitBarButtonTapped:)];
    

//    UIBarButtonItem* startVisitBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"S" style:UIBarButtonItemStylePlain target:self action:@selector(startVisitBarButtonTapped:)];
    
   // NSArray*barButtonArray=[[NSArray alloc]initWithObjects:taskBarButtonItem,notesBarButtonItem,startVisitBarButtonItem, nil];
    
        
//    self.navigationItem.rightBarButtonItems=barButtonArray;

    
    self.navigationItem.rightBarButtonItem=startVisitBarButtonItem;
    
//    objectiveTxtView.layer.borderColor = UITextViewBorderColor.CGColor;
//    objectiveTxtView.layer.borderWidth = 1.0;
//    objectiveTxtView.layer.cornerRadius = 3;

    
//   accompaniedByLbl .layer.borderColor = UIMapViewViewBorderColor.CGColor;
//    accompaniedByLbl.layer.borderWidth = 1.0;
//    accompaniedByLbl.layer.cornerRadius = 3.0;
    

    
    if (visitDetailsDict.count>0) {
        
        NSLog(@"visit details in start edetailing %@", [visitDetailsDict description]);
        locationLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Location_Name"]];
        
        
        
        
        
        if ([[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
            
           // doctorLbl.text=@"N/A";
            
            //this might be a pharmacy
            
            self.doctorPharmacyHeaderLbl.text=@"Contact Name";
            
            doctorLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Pharmacy_Contact_Name"]];

        }
        else
        {
        
        doctorLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Doctor_Name"]];
        
        }
        objectiveTxtView.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Objective"]];
        
    }
    
    
    
    
    UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close Visit", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(closeVisitTapped)];
    
    
       [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];

    
    self.navigationItem.leftBarButtonItem=closeVisitButton;
    
    
    
    tasksButton.layer.borderWidth=1.0f;
    notesButton.layer.borderWidth=1.0f;
    tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    
    
    
    notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    notesButton.layer.shadowOffset = CGSizeMake(2, 2);
    notesButton.layer.shadowOpacity = 0.1;
    notesButton.layer.shadowRadius = 1.0;
    
    
    
    tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
    tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
    tasksButton.layer.shadowOpacity = 0.1;
    tasksButton.layer.shadowRadius = 1.0;
    
    
    
    
    for (UIView *borderView in self.view.subviews) {
        
        if ([borderView isKindOfClass:[UIView class]]) {
            
            
            
            if (borderView.tag==1 || borderView.tag==2|| borderView.tag==3 ) {
                
                borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                borderView.layer.shadowOffset = CGSizeMake(2, 2);
                borderView.layer.shadowOpacity = 0.1;
                borderView.layer.shadowRadius = 1.0;
                
            }
        }
    }
    
    [_demoPlanProductsCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    
    NSString* demoPlanID=[visitDetailsDict valueForKey:@"Demo_Plan_ID"];
    if (demoPlanID!=nil)
    {
        NSMutableArray* tempArray=[MedRepQueries fetchMediaFileswithDemoPlanID:demoPlanID];

        productImagesArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
        productVideoFilesArray= [[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
        productPdfFilesArray=[[tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
    }
    else
    {
        productMediaFilesArray=[MedRepQueries fetchMediaFilesforDemoPlanID:@"3"];
    }
    
    //seggregating images,pdf and video
    NSLog(@"product media files array count is %d", productMediaFilesArray.count);
    
    if (productMediaFilesArray.count>0)
    {
        NSLog(@"product medis files array before selecting %@", [productMediaFilesArray description]);
        
        for (NSInteger i=0; i<productMediaFilesArray.count;i++)
        {
            NSString* mediaType = [[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] ;
            if ([mediaType isEqualToString:@"Image"])
            {
                NSMutableDictionary* imagesDataDict=[[NSMutableDictionary alloc]init];
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"File_Name"] objectAtIndex:i] forKey:@"File_Name"];
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] forKey:@"Media_Type"];
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_File_ID"] objectAtIndex:i] forKey:@"Media_File_ID"];
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Caption"] objectAtIndex:i] forKey:@"Caption"];
                
                //fetch product detaisl based on media file id
                
                NSMutableArray* tempProductDetailsArray=[MedRepQueries fetchProductDetaislwithMediaFileID:[[productMediaFilesArray valueForKey:@"Media_File_ID"] objectAtIndex:i]];
                [imagesDataDict setValue:[tempProductDetailsArray valueForKey:@"Product_ID"] forKey:@"Product_ID"];
                [imagesDataDict setValue:[tempProductDetailsArray valueForKey:@"Product_Name"] forKey:@"Product_Name"];
                [imagesDataDict setValue:[tempProductDetailsArray valueForKey:@"Product_Code"] forKey:@"Product_Code"];
                [imagesDataDict setValue:[tempProductDetailsArray valueForKey:@"Detailed_Info"] forKey:@"Detailed_Info"];
                [productImagesArray addObject:imagesDataDict];
            }
            else if ([mediaType isEqualToString:@"Video"])
            {
                NSMutableDictionary* videosDataDict=[[NSMutableDictionary alloc]init];
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"File_Name"] objectAtIndex:i] forKey:@"File_Name"];
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] forKey:@"Media_Type"];
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_File_ID"] objectAtIndex:i] forKey:@"Media_File_ID"];
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Caption"] objectAtIndex:i] forKey:@"Caption"];
                
                NSString* mediaFileID=[[productMediaFilesArray  objectAtIndex:i]valueForKey:@"Media_File_ID"];
                NSString* downloadUrlforImage=[[[NSUserDefaults standardUserDefaults]valueForKey:@"ServerAPILink"] stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mediaFileID,@"M"]];
                
                [videosDataDict setValue:downloadUrlforImage forKey:@"Download_Url"];
                
                //fetch product detaisl based on media file id
                
                NSMutableArray* tempProductDetailsArray=[MedRepQueries fetchProductDetaislwithMediaFileID:[[productMediaFilesArray valueForKey:@"Media_File_ID"] objectAtIndex:i]];
                [videosDataDict setValue:[tempProductDetailsArray valueForKey:@"Product_ID"] forKey:@"Product_ID"];
                [videosDataDict setValue:[tempProductDetailsArray valueForKey:@"Product_Name"] forKey:@"Product_Name"];
                [videosDataDict setValue:[tempProductDetailsArray valueForKey:@"Detailed_Info"] forKey:@"Detailed_Info"];
                [productVideoFilesArray addObject:videosDataDict];
            }
            else if ([mediaType isEqualToString:@"Brochure"])
            {
                NSMutableDictionary* pdfDataDict=[[NSMutableDictionary alloc]init];
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"File_Name"] objectAtIndex:i] forKey:@"File_Name"];
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] forKey:@"Media_Type"];
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_File_ID"] objectAtIndex:i] forKey:@"Media_File_ID"];
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Caption"] objectAtIndex:i] forKey:@"Caption"];
                
                //fetch product detaisl based on media file id
                
                NSMutableArray* tempProductDetailsArray=[MedRepQueries fetchProductDetaislwithMediaFileID:[[productMediaFilesArray valueForKey:@"Media_File_ID"] objectAtIndex:i]];
                [pdfDataDict setValue:[tempProductDetailsArray valueForKey:@"Product_ID"] forKey:@"Product_ID"];
                [pdfDataDict setValue:[tempProductDetailsArray valueForKey:@"Product_Name"] forKey:@"Product_Name"];
                [pdfDataDict setValue:[tempProductDetailsArray valueForKey:@"Detailed_Info"] forKey:@"Detailed_Info"];
                [productPdfFilesArray addObject:pdfDataDict];
            }
        }
        NSLog(@"There are %d images, %d videos, %d, pdf's for this product", productImagesArray.count,productVideoFilesArray.count,productPdfFilesArray.count);
        
        collectionViewDataArray=[[NSMutableArray alloc]init];
        collectionViewDataArray=productImagesArray;
        NSLog(@"product images array %@", [productImagesArray description]);
        [_demoPlanProductsCollectionView reloadData];
    }
    
    else
    {
        collectionViewDataArray=[[NSMutableArray alloc]init];
        collectionViewDataArray=productMediaFilesArray;
        [_demoPlanProductsCollectionView reloadData];
    }
    
    if ([_btnImages isSelected]==NO && [_btnVideo isSelected]==NO && [_btnPPT isSelected]==NO)
    {
        [_btnImages setSelected:YES];
    }
    
    if ([_btnImages isSelected]==YES)
    {
        [_btnImages setSelected:YES];
        [_btnVideo setSelected:NO];
        [_btnPPT setSelected:NO];
        
        collectionViewDataArray=[[NSMutableArray alloc]init];
        collectionViewDataArray=productImagesArray;
        [_demoPlanProductsCollectionView reloadData];
    }
    else if ([_btnVideo isSelected]==YES)
    {
        [_btnImages setSelected:NO];
        [_btnVideo setSelected:YES];
        [_btnPPT setSelected:NO];
        
        collectionViewDataArray=[[NSMutableArray alloc]init];
        collectionViewDataArray=productVideoFilesArray;
        [_demoPlanProductsCollectionView reloadData];
    }
    else if ([_btnPPT isSelected]==YES)
    {
        [_btnImages setSelected:NO];
        [_btnVideo setSelected:NO];
        [_btnPPT setSelected:YES];
        
        collectionViewDataArray=[[NSMutableArray alloc]init];
        collectionViewDataArray=productPdfFilesArray;
        [_demoPlanProductsCollectionView reloadData];
    }
    
    NSLog(@"Medis files count in start call  Images : %d Videos: %d PDF: %d", productImagesArray.count,productVideoFilesArray.count,productPdfFilesArray.count);
    
    if (productImagesArray.count==0)
    {
        [_btnImages setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
    }
    else
    {
        [_btnImages setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
    }
    
    if (productVideoFilesArray.count==0)
    {
        [_btnVideo setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
    }
    else
    {
        [_btnVideo setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
    }
    
    if (productPdfFilesArray.count==0)
    {
        [_btnPPT setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
    }
    else
    {
        [_btnPPT setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
    }
    
    //[self updateStatusforTasksandNotes];
}


-(void)viewDidAppear:(BOOL)animated
{

}
-(void)closeVisitTapped
{
    
    UIAlertView * endVisitAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"End Visit", nil) message:NSLocalizedString(@"Would you like to end the visit?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:NSLocalizedString(@"CANCEL", nil), nil];
    endVisitAlert.tag=100000;
    [endVisitAlert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag==100000) {
        
        if (buttonIndex==1) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:visitDetailsDict];
        }
        else
        {
        
//            MedRepEDetailingEndVisitViewController* endCallVC=[[MedRepEDetailingEndVisitViewController alloc]init];
//            endCallVC.visitDetailsDict=visitDetailsDict;
//            endCallVC.endVisitWithoutCall=YES;
//            endCallVC.visitDetails=self.visitDetails;
//            [self.navigationController pushViewController:endCallVC animated:YES];
        }
        
    }
}

-(IBAction)taskButtonTapped:(id)sender
{
    NSLog(@"shall i show tasks");
    
    
    NSLog(@"task button frame is %@", NSStringFromCGRect(tasksButton.frame));
    
    
    NSLog(@"task array in task button tapped %@", self.visitDetails.taskArray);
    
    popOverController=nil;
    
    MedRepEdetailTaskViewController * edetailTaskVC=[[MedRepEdetailTaskViewController alloc]init];
    edetailTaskVC.visitDetailsDict=visitDetailsDict;
    edetailTaskVC.tasksArray=self.visitDetails.taskArray;
    edetailTaskVC.delegate=self;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    
    
    
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(376, 600) animated:YES];
    edetailTaskVC.tasksPopOverController=popOverController;

    
//    [popOverController presentPopoverFromRect:CGRectMake(794, 305, 10, 10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

    
    [popOverController presentPopoverFromRect:tasksButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];

    
    //[popOverController presentPopoverFromBarButtonItem:barBtn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

    
}


-(IBAction)notesButtonTapped:(id)sender
{
    
    NSLog(@"shall i show notes?");
    
    NSLog(@"task button frame is %@", NSStringFromCGRect(notesButton.frame));

    
    
    
    popOverController=nil;
    
    MedRepEDetailNotesViewController * edetailTaskVC=[[MedRepEDetailNotesViewController alloc]init];
    edetailTaskVC.visitDetailsDict=visitDetailsDict;
    edetailTaskVC.delegate=self;
    
    
    edetailTaskVC.notesArray=self.visitDetails.notesArray;
    
    edetailTaskVC.visitDetails=self.visitDetails;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    
    
    
    [popOverController setDelegate:self];
    
    [popOverController setPopoverContentSize:CGSizeMake(376, 350) animated:YES];

    //[popOverController setPopoverContentSize:CGSizeMake(500, 437) animated:YES];
    
    edetailTaskVC.notesPopOverController=popOverController;

    
//    [popOverController presentPopoverFromRect:CGRectMake(924, 305, 10, 10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

    
      [popOverController presentPopoverFromRect:notesButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
//    [popOverController presentPopoverFromBarButtonItem:barBtn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    

    
    
}

-(void)startVisitBarButtonTapped:(UIBarButtonItem*)barBtn
{
    NSLog(@"shall i start a visit?");
    if (accompaniedBy.length>0)
    {
        goToNextScreen = true;
        [visitDetailsDict setObject:accompaniedBy forKey:@"Accompanied_By"];
        MedRepEDetailingStartCallViewController* startCallVC=[[MedRepEDetailingStartCallViewController alloc]init];
        startCallVC.visitDetailsDict=visitDetailsDict;
        startCallVC.visitDetails=self.visitDetails;
        
        
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
           
            [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
        
        [self.navigationController pushViewController:startCallVC animated:YES];
    }
    
    else
    {
        UIAlertView* missingDataAlert=[[UIAlertView alloc]initWithTitle:@"Missing Data" message:@"Please Select Accompanied By" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [missingDataAlert show];
        
    }
    
    
    
    
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

-(void)startTimer{
    NSDate * currentTimer=testDate;
    NSDate *now = [NSDate date];
    NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
    
   // NSLog(@"wait time counter ticking %@",[self stringFromTimeInterval:elapsedTimeDisplayingPoints]);
   
    waitTimeLbl.text=[self stringFromTimeInterval:elapsedTimeDisplayingPoints];
    
    
   // NSLog(@"WAIT TIME START TIMER %@", waitTimeLbl.text);

}



//- (IBAction)accompaniedByTapped:(id)sender {
-(void)accompaniedByTextFieldTapped{
     accompaniedByArray=[MedRepQueries fetchAccompaniedByData];
    
    
    
    
    NSLog(@"accompanied by array is %@", [accompaniedByArray description]);
    
    if (accompaniedByArray.count>0) {
        
        
        
        accompaniedByPopOver=nil;
        
        locationPopOver = nil;
        
        
        locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
        
                   locationPopOver.colorNames = [accompaniedByArray valueForKey:@"Username"];
        
        locationPopOver.delegate = self;
        
        if (accompaniedByPopOver == nil) {
            //The color picker popover is not showing. Show it.
            accompaniedByPopOver = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
            [accompaniedByPopOver presentPopoverFromRect:accompaniedTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            accompaniedByPopOver.delegate=self;
            
        }
        else {
            //The color picker popover is showing. Hide it.
            [accompaniedByPopOver dismissPopoverAnimated:YES];
            accompaniedByPopOver = nil;
            accompaniedByPopOver.delegate=nil;
        }
        

    }
    
}

-(void)selectedStringValue:(NSIndexPath *)indexPath

{
//    self.accompaniedByLbl.text=[NSString stringWithFormat:@" %@",[[accompaniedByArray valueForKey:@"Username"] objectAtIndex:indexPath.row] ];
    
   // [accompaniedButton setTitle:[NSString stringWithFormat:@" %@",[[accompaniedByArray valueForKey:@"Username"] objectAtIndex:indexPath.row] ] forState:UIControlStateNormal];
    
    accompaniedTextField.text=[NSString stringWithFormat:@" %@",[[accompaniedByArray valueForKey:@"Username"] objectAtIndex:indexPath.row] ];
    accompaniedBy=[NSString stringWithFormat:@"%@",[[accompaniedByArray valueForKey:@"User_ID"] objectAtIndex:indexPath.row] ];
    
    
    
    if (accompaniedByPopOver)
    {
        [accompaniedByPopOver dismissPopoverAnimated:YES];
        accompaniedByPopOver = nil;
        accompaniedByPopOver.delegate=nil;
    }
    
    
    
    if (popOverController)
    {
        [popOverController dismissPopoverAnimated:YES];
        popOverController = nil;
        popOverController.delegate=nil;
    }
    locationPopOver.delegate=nil;
    locationPopOver=nil;
    
}

#pragma mark UIPopOver Controller delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController==accompaniedByPopOver) {
        return YES;
    }
    
    else
    {
        return NO;
    }
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==accompaniedTextField)
    {
        [self accompaniedByTextFieldTapped];
    return NO;
    }
    return YES;
}

#pragma mark Task and Notes Status Update

-(void)updateStatusforTasksandNotes

{
    
    NSString* locationType=[visitDetailsDict valueForKey:@"Location_Type"];
    
    NSString* locationID=[visitDetailsDict valueForKey:@"Location_ID"];
    
    NSInteger  taskCount=0;
    
    NSInteger notesCount=0;
    
    if ([locationType isEqualToString:@"D"]) {
        
        NSString* doctorID=[visitDetailsDict valueForKey:@"Doctor_ID"];
        
        
        NSMutableArray* taskCount=[MedRepQueries fetchTasksCountforLocationID:locationID andDoctorID:doctorID];
        
        
        NSLog(@"task array is %@", taskCount);

        
        if (taskCount.count>0) {
            
            for (NSInteger i=0; i<taskCount.count; i++) {
                
                NSMutableDictionary * currentDict=[taskCount objectAtIndex:i];
                
                NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:taskDueDate];
                
                NSLog(@"date from string is %@", dateFromString);
                
                BOOL testDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                
                BOOL colorSet=NO;
                
                NSLog(@"check test date %hhd", testDate);
                
                if (testDate==YES) {
                    
                    NSLog(@"RED RED TASKS OVER DUE");
                    [visitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                    
                   // [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
                    
                    colorSet=YES;
                }
                
                else
                {
                    NSLog(@"YELLOW");
                    
                    if (colorSet==YES) {
                        
                    }
                    else
                    {
                       // [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
                    }
                    
                    [visitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                    
                    
                }
                
            }
            
        }
        
        
        
        notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andDoctorID:doctorID];
        
        NSLog(@"task count : %@ notes count : %d",taskCount,notesCount);
        
        
        if (taskCount>0) {
            [visitDetailsDict setValue:@"YES" forKey:@"Tasks_Available"];
        }
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Tasks_Available"];
            
        }
        if (notesCount>0)
            
        {
            [visitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
            
        }
        
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
            
        }
        
        
    }
    
    else if ([locationType isEqualToString:@"P"])
        
    {
        NSString* contactID=[visitDetailsDict valueForKey:@"Contact_ID"];
        
        NSMutableArray* tasksArray=[MedRepQueries fetchTaskCountforLocationID:locationID andContactID:contactID];
        
        
        
        if (tasksArray.count>0) {
            
            for (NSInteger i=0; i<tasksArray.count; i++) {
                
                NSMutableDictionary * currentDict=[tasksArray objectAtIndex:i];
                
                NSString* taskDueDate=[currentDict valueForKey:@"Start_Time"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:taskDueDate];
                
                
                BOOL testTaskDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
                
                NSLog(@"task test date %hhd", testTaskDate);
                
                BOOL colorUpdated=NO;
                
                if (testTaskDate==YES) {
                    
                    NSLog(@"RED RED TASKS OVER DUE");
                    [visitDetailsDict setValue:@"OVER_DUE" forKey:@"TASK_STATUS"];
                    
                    //[taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];

                }
                
                
                
                else
                {
                    NSLog(@"YELLOW");
                    
                    //[taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
                    
                    [visitDetailsDict setValue:@"PENDING" forKey:@"TASK_STATUS"];
                    
                    
                }
                
            }
            
        }
        
        
        else if (tasksArray.count==0)
            
        {
            [taskStatusButton setBackgroundColor:[UIColor whiteColor]];
            
        }
        
        notesCount=[MedRepQueries fetchNotesCountforLocationID:locationID andContactID:contactID];
        
        
        
        
        
        NSLog(@"task count : %d notes count : %d",taskCount,notesCount);
        
        
        
        
        if (notesCount>0)
            
        {
            [visitDetailsDict setValue:@"YES" forKey:@"Notes_Available"];
            
        }
        
        else
        {
            [visitDetailsDict setValue:@"NO" forKey:@"Notes_Available"];
            
        }
        
        
        
    }
    
    
    if (notesCount>0) {
        
        notesStatusButton.backgroundColor=KNOTESAVAILABLECOLOR;
    }
    else
    {
        notesStatusButton.backgroundColor=KNOTESUNAVAILABLECOLOR;
        
    }
    
    
    
    //update task objects
    
    
    
    
    
    
    
    
    
    
    NSLog(@"task array while updating color  %@", self.visitDetails.taskArray);
    
    BOOL isColorUpdated=NO;

    
    for (VisitTask * task in self.visitDetails.taskArray) {
        
       
        NSLog(@"task status is %@", task.Status);
        
        NSString* taskDueDate=task.Start_Time;
        
        
        NSLog(@"task due date is %@", task.Start_Time);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];

        NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setTimeZone:timeZone];

        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        NSDate *dateFromString = [[NSDate alloc] init];
        // voila!
        dateFromString = [dateFormatter dateFromString:taskDueDate];
        
        NSLog(@"date being passed %@", dateFromString);
        
        BOOL testTaskDate=[MedRepDefaults isEndDateIsSmallerThanCurrent:dateFromString];
        
        NSLog(@"test task date %hhd", testTaskDate);
        
        NSLog(@"task status is %@", task.Status);
        
        if ([task.Status isEqualToString:@"Closed"] && isColorUpdated==NO) {
            
            [taskStatusButton setBackgroundColor:[UIColor whiteColor]];

        }
        
        
      else if (testTaskDate==YES && isColorUpdated==NO) {
           
           NSLog(@"task status color updated to red");
           
            [taskStatusButton setBackgroundColor:KTASKOVERDUECOLOR];
          isColorUpdated=YES;


        }
        
        else if (isColorUpdated==YES)
            
        {
            
        }
        
        else
        {
            [taskStatusButton setBackgroundColor:KTASKPENDINGCOLOR];
            NSLog(@"task status color updated to green");

        }

        
    }
    
    
    
    //update notes based on custom objects
    
    NSLog(@"notes array in edet %@", self.visitDetails.notesArray);
    notesCount=self.visitDetails.notesArray.count;
    
    if (notesCount>0) {
        
        notesStatusButton.backgroundColor=KNOTESAVAILABLECOLOR;
    }
    else
    {
        notesStatusButton.backgroundColor=KNOTESUNAVAILABLECOLOR;
        
    }
    
    
    
}

#pragma Mark Task popover controller delegate

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray;
{
    
    self.visitDetails.taskArray=tasksArray;
    
    NSLog(@"visit details task array is %@", self.visitDetails.taskArray);
        
        [self updateStatusforTasksandNotes];
    
    
    
    
}

-(void)notesDidClose:(NSMutableArray*)updatedNotes;

{
    self.visitDetails.notesArray=updatedNotes;
    NSLog(@"notes array in did close %@", self.visitDetails.notesArray);
    
    
    if (self.visitDetails.notesArray.count>0) {
        [self updateStatusforTasksandNotes];
        
    }
    
    
}

#pragma mark button action methods

- (IBAction)btnImages:(id)sender
{
    [_btnImages setSelected:YES];
    [_btnVideo setSelected:NO];
    [_btnPPT setSelected:NO];

    if ([_btnImages isSelected])
    {
        _btnImages .layer.borderWidth=2.0;
        _btnImages.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    _btnVideo.layer.borderWidth=0.0;
    _btnPPT.layer.borderWidth=0.0;
    
    collectionViewDataArray = [[NSMutableArray alloc]init];
    collectionViewDataArray = productImagesArray;
    [_demoPlanProductsCollectionView reloadData];
}

- (IBAction)btnVideo:(id)sender
{
    [_btnImages setSelected:NO];
    [_btnVideo setSelected:YES];
    [_btnPPT setSelected:NO];
    
    
    if ([_btnVideo isSelected]) {
        
        _btnVideo .layer.borderWidth=2.0;
        _btnVideo.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    _btnImages.layer.borderWidth=0.0;
    _btnPPT.layer.borderWidth=0.0;
    
    collectionViewDataArray = [[NSMutableArray alloc]init];
    collectionViewDataArray = productVideoFilesArray;
    [_demoPlanProductsCollectionView reloadData];    
}

- (IBAction)btnPPT:(id)sender
{
    [_btnImages setSelected:NO];
    [_btnVideo setSelected:NO];
    [_btnPPT setSelected:YES];
    
    if ([_btnPPT isSelected])
    {
        _btnPPT .layer.borderWidth=2.0;
        _btnPPT.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    _btnImages.layer.borderWidth=0.0;
    _btnVideo.layer.borderWidth=0.0;
    
    collectionViewDataArray = [[NSMutableArray alloc]init];
    collectionViewDataArray = productPdfFilesArray;
    [_demoPlanProductsCollectionView reloadData];
}

#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionViewDataArray.count>0) {
        return collectionViewDataArray.count;
    }
    else
    {
    return  0;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(200, 190);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productCell";
    NSString* mediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.selectedImgView.hidden=YES;
    
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row] ] ];
    
    if ([mediaType isEqualToString:@"Image"])
    {
        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
        cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
        
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        cell.captionLbl.text=[[productVideoFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        cell.captionLbl.text=[[productPdfFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    
    //adding check mark to selected cell
    
    MedRepCollectionViewCheckMarkViewController* checkMarkVC=[[MedRepCollectionViewCheckMarkViewController alloc]init];
    if (demoedMediaFiles.count>0)
    {
        for (NSInteger i=0; i<demoedMediaFiles.count; i++)
        {
            NSString* selectedFileName=[demoedMediaFiles objectAtIndex:i];
            NSString * currentFileName=[[collectionViewDataArray valueForKey:@"Media_File_ID"] objectAtIndex:indexPath.row] ;
            
            //NSLog(@"selected file name : %@  current file name : %@", selectedFileName,currentFileName);
            
            if ([selectedFileName isEqualToString:currentFileName])
            {
                cell.selectedImgView.hidden=NO;
            }
            else
            {
            }
        }
    }
    
    cell.productImageView.backgroundColor=[UIColor clearColor];
    if (indexPath.row== selectedCellIndex)
    {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else
    {
        cell.layer.borderWidth=0;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCellIndex=indexPath.row;
    NSMutableDictionary * tempDict=[collectionViewDataArray objectAtIndex:indexPath.row];
    
    [tempDict setObject:@"Yes" forKey:@"Demoed"];
    [collectionViewDataArray replaceObjectAtIndex:indexPath.row withObject:tempDict];
    
    NSLog(@"data after replacing %@", [collectionViewDataArray objectAtIndex:indexPath.row]);
    
    NSString* selectedFileID=[[collectionViewDataArray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID"];
    if ([demoedMediaFiles containsObject:selectedFileID])
    {
        
    }
    else
    {
        [demoedMediaFiles addObject:selectedFileID];
    }
    
    NSLog(@"demoed file id %@", selectedFileID);
    
    NSString* mediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row];
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
    if ([mediaType isEqualToString:@"Image"])
    {
        [imagesPathArray removeAllObjects];
        for (NSInteger i=0; i<productImagesArray.count; i++)
        {
            [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i] ]];
        }
        
        NSLog(@"product images array before selecting %@", [productImagesArray description]);
        
        MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
        imagesVC.productImagesArray=imagesPathArray;
        imagesVC.imagesViewedDelegate=self;
        
        imagesVC.productDataArray=productImagesArray;
        imagesVC.currentImageIndex=indexPath.row;
        imagesVC.selectedIndexPath=indexPath;
        [self presentViewController:imagesVC animated:YES completion:nil];
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
        videosVC.videosViewedDelegate=self;
        videosVC.currentImageIndex=indexPath.row;
        videosVC.selectedIndexPath=indexPath;
        videosVC.productDataArray=productVideoFilesArray;
        [self.navigationController presentViewController:videosVC animated:YES completion:nil];
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        if (productPdfFilesArray.count>0)
        {
            MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
            pdfVC.pdfViewedDelegate=self;
            pdfVC.productDataArray=productPdfFilesArray;
            pdfVC.selectedIndexPath=indexPath;
            [self presentViewController:pdfVC animated:YES completion:nil];
        }
        else
        {
            UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No PDF files are available for the selected demo plan" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [noDataAvblAlert show];
        }
    }
}


@end
