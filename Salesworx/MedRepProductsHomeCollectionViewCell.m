//
//  MedRepProductsHomeCollectionViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/27/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepProductsHomeCollectionViewCell.h"

@implementation MedRepProductsHomeCollectionViewCell

@synthesize productImageView;

- (void)awakeFromNib {
    // Initialization code
    
    
    CALayer *layer = self.captionBackGroundImageView.layer;
    layer.shadowOffset = CGSizeMake(0, -1);
    
    layer.shadowColor = [[UIColor blackColor] CGColor];
    
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 0.1f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
    

    
    
//    productImageView.layer.shadowColor = [UIColor purpleColor].CGColor;
//    productImageView.layer.shadowOffset = CGSizeMake(0, 1);
//    productImageView.layer.shadowOpacity = 1;
//    productImageView.layer.shadowRadius = 1.0;
//    productImageView.clipsToBounds = NO;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        
        
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MedRepProductsHomeCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
        //        self.restorationIdentifier = @"cvCell";
        //        self.backgroundColor = [UIColor clearColor];
        //        self.autoresizingMask = UIViewAutoresizingNone;
        //
        //        CGFloat borderWidth = 3.0f;
        //        UIView *bgView = [[UIView alloc] initWithFrame:frame];
        //        bgView.layer.borderColor = [UIColor redColor].CGColor;
        //        bgView.layer.borderWidth = borderWidth;
        //        self.selectedBackgroundView = bgView;
        //
        //        CGRect myContentRect = CGRectInset(self.contentView.bounds, borderWidth, borderWidth);
        //
        //        UIView *myContentView = [[UIView alloc] initWithFrame:myContentRect];
        //        myContentView.backgroundColor = [UIColor whiteColor];
        //        myContentView.layer.borderColor = [UIColor colorWithWhite:0.5f alpha:1.0f].CGColor;
        //        myContentView.layer.borderWidth = borderWidth;
        //        [self.contentView addSubview:myContentView];
        
    }
    
    return self;
    
}


@end
