//
//  MedRepProductImagesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MedRepTextView.h"
#import "MedRepElementDescriptionLabel.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"

@protocol ViewedImagesDelegate <NSObject>

-(void)imagesViewedinFullScreenDemo:(NSMutableArray*)imagesArray;

-(void)fullScreenImagesDidCancel;

@optional
-(void)setBorderOnLastViewedImage:(NSInteger)index;

@end

@interface MedRepProductImagesViewController : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate>

{
    NSTimer* demoTimer;
    NSInteger demoTimeInterval;
    
    NSDate* viewingStartTime;
    ProductMediaFile* selectedMediaFile;

    IBOutlet UICollectionView *imagesCollectionView;
    IBOutlet UIButton *emailButton;
    
    UIView * zoomableView
    ;
    NSDate * timerDate;
    NSString *buttonTitle;
 
    BOOL isEmailTapped;
    
    BOOL sendEmail;
    
    UIScrollView * collectionScrollView;
    
    UIImageView *fullScreenImageView;
    id imagesViewedDelegate;
    
    NSIndexPath * selectedCellIndexPath;
    
    NSMutableArray* viewedImages;
    
    NSIndexPath * selectedProductIndexPath;
    
    NSInteger selectedCellIndex;
    
    NSMutableArray* selectedIndexPathArray;
    
    IBOutlet UITextView *productDescTxtView;
    
    NSMutableArray* selectedCellIndexArray;
    
    NSIndexPath *prevIndexPath;
    
    NSString*documentsDirectory;

    
    NSInteger numberOfPages ;
    
    UIScrollView *pageScrollView;
    UIScrollView *currentPageScrollView;
    
    UIImageView *imageForZooming;
    IBOutlet UILabel *productHeaderTitle;
    IBOutlet UILabel *titleTextView;
    IBOutlet MedRepElementDescriptionLabel *productNameLbl;
    
    IBOutlet MedRepTextView *productDescriptionTextView;
    
    
    IBOutlet UIButton *cancelButton;
    NSInteger imageIndex;
    NSString *emailIDForAutoInsert;
}

@property(strong,nonatomic) SalesWorxVisit* currentVisit;
@property(strong,nonatomic)NSMutableArray* productDataArray;
@property(nonatomic)    NSInteger currentImageIndex;
@property(nonatomic) BOOL isViewing;

@property(nonatomic) BOOL isFromProductsScreen;

@property(strong,nonatomic) id imagesViewedDelegate;
@property (strong, nonatomic) IBOutlet UICollectionView *productImagesCollectionView;
@property(strong,nonatomic) NSMutableArray * contentDataArray;

- (IBAction)cancelButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIPageControl *customPageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *imagesScrollView;
@property(strong,nonatomic)NSMutableArray* productImagesArray;
@property(strong,nonatomic)NSIndexPath* selectedIndexPath;
- (IBAction)closeButtontapped:(id)sender;
@property(strong,nonatomic)NSMutableDictionary* selectedProductDetails;
@property(nonatomic) BOOL isCurrentUserDoctor;
@property(nonatomic) BOOL isCurrentUserPharmacy;
@property(strong,nonatomic) NSString *doctor_ID ;
@property(strong,nonatomic) NSString *contact_ID ;
@end
