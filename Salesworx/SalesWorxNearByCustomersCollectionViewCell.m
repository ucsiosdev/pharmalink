//
//  SalesWorxNearByCustomersCollectionViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/19/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxNearByCustomersCollectionViewCell.h"

@implementation SalesWorxNearByCustomersCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxNearByCustomersCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
}


@end
