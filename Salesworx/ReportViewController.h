//
//  ReportViewController.h
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"  
#import "TTSlidingPagesDataSource.h"

@interface ReportViewController : SWViewController <TTSlidingPagesDataSource>

@end
