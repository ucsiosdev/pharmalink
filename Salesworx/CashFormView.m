//
//  FOCView.m
//  SWCustomer
//
//  Created by msaad on 12/19/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CashFormView.h"
#import "CustomersListViewController.h"

@implementation CashFormView

@synthesize customerListView,formView,customer;

- (id)initWithCustomer:(NSDictionary *)customerDict
{
    self = [super init];
    if (self) {
        // Initialization code

    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [Flurry logEvent:@"Cash Customer Form View"];
    //[Crittercism leaveBreadcrumb:@"<Cash Customer Form View>"];

    self.formView=nil;
    self.formView = [[UIView alloc] initWithFrame:CGRectMake(350, 10,300  ,600)] ;
    self.formView.backgroundColor = [UIColor clearColor];
    [self.formView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.formView.layer setBorderWidth:1.0f];
    [self.view addSubview:self.formView];
    
    CustomersListViewController *listView = [[CustomersListViewController alloc] init] ;
    [listView setTarget:self];
    [listView.view setBackgroundColor:[UIColor yellowColor]];
    [listView setAction:@selector(presentProductOrder:)];
    listView.view.frame = CGRectMake(10, 10, 300, 600);
    [self.view addSubview:listView.view];
}
- (void)presentProductOrder:(NSDictionary *)product {
    
    if (![product objectForKey:@"Guid"]) {
        [product setValue:[NSString createGuid] forKey:@"Guid"];
    }

}


@end
