//
//  MedRepView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepView.h"
#import "MedRepDefaults.h"

@implementation MedRepView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
//    self.layer.borderWidth=1.0f;
//    self.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
//    self.layer.masksToBounds = NO;
//    self.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.layer.shadowOpacity = 0.1f;
//    self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//    self.layer.shadowRadius = 1.0f;
//    self.layer.shouldRasterize = YES;
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(2, 2);
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowRadius = 1.0;

    
}

@end
