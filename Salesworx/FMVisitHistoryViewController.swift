//
//  FMVisitHistoryViewController.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/3/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import UIKit

/*{
 "Contact_ID" = "<null>";
 DoctorName = "Akshay Sharma";
 "Doctor_ID" = "6f19cf8a-3f52-4bdc-b680-05980fefd3f2";
 EDetail = Y;
 EmpNotes = 0;
 Location = "Abu Dhabi Knee & Sports Medicine Center";
 LocationType = Doctor;
 "Location_Id" = "a6bf686f-344c-49ba-8928-dd620c037fe5";
 NextObjective = Rese;
 Samples = Y;
 Tasks = 0;
 TimeSpent = 1;
 VisitDate = "2018-09-21 14:40:46";
 VisitNote = Test;
 "Visit_ID" = "7dc824ec-d5af-4052-ac20-d15415d64bdc";
 }*/




@objc class FMVisitHistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    

    @IBOutlet var visitHistoryTableView: UITableView!
    var parametersDictionary = NSDictionary()
    var visitHistoryArray : [Visit_History] =  [Visit_History]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let def = UserDefaults.standard
        let data = def.object(forKey: "ORDER_HISTORY_PARAMETERS_DICT") as? Data
        var retrievedDictionary: [AnyHashable : Any]? = nil
        if let aData = data {
            retrievedDictionary = NSKeyedUnarchiver.unarchiveObject(with: aData) as? [AnyHashable : Any]
        }
        parametersDictionary = retrievedDictionary! as NSDictionary

        
        print("parameters dictionary is \(parametersDictionary)")
        visitHistoryTableView.register(UINib(nibName: "FMVisitHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "visitHistoryCell")

        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 44))
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.font = UIFont(name: "WeblySleekUISemibold", size: 19)
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.white
        titleLabel.text = "Visit History"
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
    }
   
    override func viewWillAppear(_ animated: Bool) {
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            fetchOrderHistory()
            MBProgressHUD.showAdded(to: self.view, animated: true)

        }else{
            print("Internet Connection not Available!")
            let alert = UIAlertController(title: "No Internet Connection", message: "Please check your internet connection and try again", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    self.navigationController?.popViewController(animated: true)
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }}))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    func fetchOrderHistory(){

        let requestURL : URL = parametersDictionary.value(forKey: "SalesWorx_Order_History_URL") as! URL
        let parameterString : String = parametersDictionary.value(forKey: "SalesWorx_Order_History_Paramater_String") as! String
        var request = URLRequest(url: requestURL)
        request.httpMethod = "POST"
        let data = parameterString.data(using: .utf8)
        request.url = requestURL
        request.httpBody = data
        let session = URLSession.shared
    
        session.dataTask(with: request) { (data, response, error) in
           
           
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print("visit history json is \(json)")
                    if  let object = json as? [Any] {
                     
                         for currentHistory in object as! [Dictionary<String, Any>] {
                            let Contact_ID = currentHistory["Contact_ID"]  as? String
                            let DoctorName = currentHistory["DoctorName"] as? String
                            let Doctor_ID = currentHistory["Doctor_ID"] as? String
                            let EDetail = currentHistory["EDetail"] as? Int
                            let Samples = currentHistory["Samples"] as? Int
                            let EmpNotes = currentHistory["EmpNotes"] as? String
                            let Location = currentHistory["Location"] as? String
                            let LocationType = currentHistory["LocationType"] as? String
                            let Location_Id = currentHistory["Location_Id"] as? String
                            let NextObjective = currentHistory["NextObjective"] as? String
                            let Tasks = currentHistory["Tasks"] as? String
                            let TimeSpent = currentHistory["TimeSpent"] as? Int
                            var refinedDate = ""
                            if let currentVisitDate = currentHistory["VisitDate"] as? String{
                                 refinedDate = SalesWorxDefaults.retrieveDefaults.refineDateFormat(sourceFormat: "yyyy-MM-dd HH:mm:ss", sourceString: currentVisitDate, destinationFormat: "MMM d, h:mm a")
                            }
                            let VisitDate = refinedDate
                            let VisitNote = currentHistory["VisitNote"] as? String
                            let Visit_ID = currentHistory["Visit_ID"] as? String
                            
                            let currentHistoryObj = Visit_History(contactID: Contact_ID ?? "", doctorName: DoctorName ?? "", doctorID: Doctor_ID ?? "", eDetail: EDetail ?? 0, empNotes: EmpNotes ?? "", location: Location ?? "", locationType: LocationType ?? "", locationID: Location_Id ?? "", nextVisitObjective: NextObjective ?? "", samples: Samples ?? 0, tasks: Tasks ?? "", timeSpent: TimeSpent ?? 0, visitDate: VisitDate , visitNote: VisitNote ?? "", visitID: Visit_ID ?? "")
                            self.visitHistoryArray.append(currentHistoryObj)
                        }
                    }
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.visitHistoryTableView.reloadData()
                    }
                }catch {
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            }
        }
        .resume()
    }
    
    //MARK :- UITableView Methods
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return visitHistoryArray.count>0 ? visitHistoryArray.count:0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 131.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "visitHistoryCell", for: indexPath)
            as! FMVisitHistoryTableViewCell
        if indexPath.row % 2 == 0{
            cell.contentView.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
        }
        else{
            cell.contentView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        let currentVisitHistory = visitHistoryArray[indexPath.row]
        
        cell.locationNameLabel.text = currentVisitHistory.Location
        cell.doctorNameLabel.text = currentVisitHistory.DoctorName
        cell.dateLabel.text = currentVisitHistory.VisitDate
        cell.timeSpentLabel.text = "\(currentVisitHistory.TimeSpent)"
        cell.productsDemonstratedLabel.text = "\(currentVisitHistory.EDetail)"
        cell.samplesGivenLabel.text = "\(currentVisitHistory.Samples)"
        cell.tasksLabel.text = currentVisitHistory.Tasks == "Y" ? "YES" : "NO"
        cell.notesLabel.text = currentVisitHistory.EmpNotes == "Y" ? "YES" : "NO"
        cell.previousVisitNotesLabel.text = currentVisitHistory.VisitNote
        cell.nextVisitObjectiveLabel.text = currentVisitHistory.NextObjective

        return cell

    }
    
    /*- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
     {
     CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
     NSError *error;
     NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
     NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
     NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
     
     NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
     NSURL *url = [NSURL URLWithString:strurl];
     AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
     
     
     
     NSString *strParams =[[NSString alloc] init];
     NSString *strName=procName;
     
     NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"OrderList",@"RMAList",@"OrderHistoryData",@"CollectionInfo",@"DocAdditionalData",@"VisitsData", nil];
     
     for (NSInteger i=0; i<procedureParametersArray.count; i++) {
     strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
     strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];
     
     }
     
     strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRep_ID"];
     strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
     
     
     
     // NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
     NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
     NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
     
     NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,strName,strParams];
     
     NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
     NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
     [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
     [urlRequest setHTTPMethod:@"POST"];
     [urlRequest setHTTPBody:myRequestData];
     
     SWAppDelegate *appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
     
     AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
     
     [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
     
     [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
     NSString *responseText = [operationQ responseString];
     NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
     NSLog(@"background sync Response %@",resultArray);
     appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None
     ;
     if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
     {
     [[SWDatabaseManager retrieveManager]deleteOrdersAndReturnsAfterBackGroundSync:currentSyncStarttimeStr AndLastSyncTime:lastSynctimeStr];
     [SWDefaults setLastVTRXBackGroundSyncDate:currentSyncStarttimeStr];
     }
     }
     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
     NSLog(@" background sync error %@",error);
     appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None
     ;
     }];
     
     //call start on your request operation
     [operation start];
     }
*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
