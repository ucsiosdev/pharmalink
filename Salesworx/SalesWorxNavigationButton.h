//
//  SalesWorxNavigationButton.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/24/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@interface SalesWorxNavigationButton : UIButton


@property (nonatomic) IBInspectable BOOL EnableTitleRTLSupport;
@property (nonatomic) IBInspectable BOOL EnableImageRTLSupport;

@end
