//
//  SWCustomerPriceListFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "SalesWorxCustomClass.h"


@protocol CustomerPriceListDelegate <NSObject>

-(void)customerPriceListFilterDidCancel;
-(void)customerPriceListFilterDidClose;
-(void)customerPriceListFilterDidReset;
-(void)filteredPriceList:(id)priceListdata;
-(void)filteredPriceListParameters:(id)priceListdata;

@end


@interface SWCustomerPriceListFilterViewController : UIViewController

{
    id delegate;
    
    IBOutlet MedRepTextField *itemNameTxtFld;
    IBOutlet MedRepTextField *itemCodeTextFld;
    NSString* selectedTextField;
    
    IBOutlet MedRepTextField *uomTextField;
    IBOutlet MedRepTextField *brandTxtFld;
    
    NSMutableDictionary * filterParametersDict;
    NSString* selectedPredicateString;
    
    NSMutableArray *filteredPriceListArray;

}
@property(nonatomic) id delegate;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;
@property(strong,nonatomic)UINavigationController * filterNavController;
@property(strong,nonatomic) NSString* filterTitle;
- (IBAction)searchButtontapped:(id)sender;
- (IBAction)resetButtontapped:(id)sender;
@property(strong,nonatomic)NSMutableArray* priceListArray;
@property(strong,nonatomic) Customer * selectedCustomer;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@end
