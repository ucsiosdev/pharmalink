//
//  SurveyQuestionViewController.h
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"


@interface SurveyQuestionViewController :  SWViewController <UIScrollViewDelegate , UIPageViewControllerDelegate>
{
    NSDictionary *customer;
    UIScrollView *detailScroller;
    UIPageControl *pageControl;
    BOOL isQuestionDone;
}
@property (nonatomic, strong) NSDictionary *customer;
@property (nonatomic, strong) UIScrollView *detailScroller;
@property (nonatomic, strong) UIPageControl *pageControl;
@end
