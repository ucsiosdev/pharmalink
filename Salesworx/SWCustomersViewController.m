//
//  SWCustomersViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/6/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SWCustomersViewController.h"
#define SegmentControlWidth 750
#define ScrollViewHeight 634
#import "MedRepUpdatedDesignCell.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SWCustomerOrderHistoryTableViewCell.h"
#import "SWCustomerPriceListTableViewCell.h"
#import "SWCustomerOrderHistoryDescriptionTableViewCell.h"
#import "SWCustomerInvoicePopOverViewController.h"
#import "SalesWorxVisitOptionsViewController.h"
#import "SalesWorxCashCustomerOnsiteVisitOptionsViewController.h"
#import "SalesWorxODOMeterViewController.h"
#import "LGAlertView.h"


@interface SWCustomersViewController ()

@end


@implementation SWCustomersViewController

@synthesize segmentControl,customersScrollView,detailsView,OrderHistoryView,priceListView,customersFilterButton,customersSearchBar,customersTableView,customersArray,indexPathArray,detailsCustomerCodeLbl,detailsCustomerNameLbl,customerTargetPieChart,priceListFilterButton,isFromDashBoard,selectedCustomerFromDashBoard,detailsCustomerClassLbl ;

@synthesize  pieChartForCustomerPotential = _pieChartForCustomerPotential;
@synthesize slicesForCustomerPotential = _slicesForCustomerPotential;
@synthesize sliceColorsForCustomerPotential = _sliceColorsForCustomerPotential;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    appControl = [AppControl retrieveSingleton];
    
    startTime = [NSDate date];
    
    selectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    filteredCustomers=[[NSMutableArray alloc]init];
    indexPathArray=[[NSMutableArray alloc]init];
    pieChartDataArray=[[NSMutableArray alloc]init];
    categoriesArray=[[NSMutableArray alloc]init];
    previousPriceListFilterParameters=[[NSMutableDictionary alloc]init];
    filteredCustomerPriceListArray=[[NSMutableArray alloc]init];
    customerPriceListArray=[[NSMutableArray alloc]init];
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = NSLocalizedString(@"Customers", nil);
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Customers", nil)];
    

    hideDivider=YES;
    
    methodStart = [NSDate date];
    
    /* ... Do whatever you need to do ... */
    
    startVisitButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartVisit"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitTapped)];
    
    self.navigationItem.rightBarButtonItem=startVisitButton;
    

    NSLog(@"view did load called");
    
    orderHistoryDetailsView.hidden=YES;
    
    
    
}

-(void)closeSplitView
{
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        if (revealController.currentFrontViewPosition !=0)
        {
            [revealController revealToggle:self];
        }
        revealController=nil;
    }
}


-(void)deselectPreviousCellandSelectFirstCell
{
    
    if ([customersTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        selectedCustomer=[customersArray objectAtIndex:0];
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[customersTableView cellForRowAtIndexPath:selectedIndexPath];
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [customersTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                     animated:YES
                               scrollPosition:UITableViewScrollPositionNone];
        [customersTableView.delegate tableView:customersTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:YES];
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesCustomersScreenName];

    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if([appControl.ENABLE_FSR_VISIT_NOTES isEqualToString:KAppControlsYESCode] ||
           [appControl.SHOW_FS_VISIT_CUSTOMER_CONTACT isEqualToString:KAppControlsYESCode]){
            lastvisitDetailsButton.hidden=NO;
        }else{
            lastvisitDetailsButton.hidden=YES;
            
        }
    });
   

    orderHistoryDetailsView.hidden=YES;
    customersTableView.delegate=self;
    customersTableView.rowHeight = UITableViewAutomaticDimension;
    customersTableView.estimatedRowHeight = 70.0;
}

#pragma mark Selected Visit Option Delegate

-(void)selectedVisitOption:(NSString*)visitOption
{
    NSLog(@"selected visit option in delegate %@", visitOption);
    
    if ([visitOption isEqualToString:kTelephonicVisitTitle]) {
        
        [SWDefaults setOnsiteVisitStatus:NO];
    }
    else if ([visitOption isEqualToString:kOnsiteVisitTitle])
    {
        [SWDefaults setOnsiteVisitStatus:YES];
    }
    else{
        
    }
    
    
    
    NSMutableDictionary * currentCustDict=[[NSMutableDictionary alloc]init];
    
    [currentCustDict setValue:selectedCustomer.Address forKey:@"Address"];
    [currentCustDict setValue:selectedCustomer.Allow_FOC forKey:@"Allow_FOC"];
    [currentCustDict setValue:selectedCustomer.Avail_Bal forKey:@"Avail_Bal"];
    [currentCustDict setValue:selectedCustomer.Customer_Name forKey:@"Customer_Name"];
    [currentCustDict setValue:selectedCustomer.Bill_Credit_Period forKey:@"Bill_Credit_Period"];
    [currentCustDict setValue:selectedCustomer.Cash_Cust forKey:@"Cash_Cust"];
    [currentCustDict setValue:selectedCustomer.Chain_Customer_Code forKey:@"Chain_Customer_Code"];
    [currentCustDict setValue:selectedCustomer.City forKey:@"City"];
    [currentCustDict setValue:selectedCustomer.Creation_Date forKey:@"Creation_Date"];
    [currentCustDict setValue:selectedCustomer.Credit_Hold forKey:@"Credit_Hold"];
    [currentCustDict setValue:selectedCustomer.Credit_Limit forKey:@"Credit_Limit"];
    [currentCustDict setValue:selectedCustomer.Cust_Lat forKey:@"Cust_Lat"];
    [currentCustDict setValue:selectedCustomer.Cust_Long forKey:@"Cust_Long"];
    [currentCustDict setValue:selectedCustomer.Cust_Status forKey:@"Cust_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Barcode forKey:@"Customer_Barcode"];
    [currentCustDict setValue:selectedCustomer.Customer_Class forKey:@"Customer_Class"];
    [currentCustDict setValue:selectedCustomer.Customer_ID forKey:@"Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_No forKey:@"Customer_No"];
    [currentCustDict setValue:selectedCustomer.Customer_OD_Status forKey:@"Customer_OD_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Segment_ID forKey:@"Customer_Segment_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_Type forKey:@"Customer_Type"];
    [currentCustDict setValue:selectedCustomer.Dept forKey:@"Dept"];
    [currentCustDict setValue:selectedCustomer.Location forKey:@"Location"];
    [currentCustDict setValue:selectedCustomer.Phone forKey:@"Phone"];
    [currentCustDict setValue:selectedCustomer.Postal_Code forKey:@"Postal_Code"];
    [currentCustDict setValue:selectedCustomer.Price_List_ID forKey:@"Price_List_ID"];
    [currentCustDict setValue:selectedCustomer.SalesRep_ID forKey:@"SalesRep_ID"];
    [currentCustDict setValue:selectedCustomer.Sales_District_ID forKey:@"Sales_District_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Customer_ID forKey:@"Ship_Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Site_Use_ID forKey:@"Ship_Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Site_Use_ID forKey:@"Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Trade_Classification forKey:@"Trade_Classification"];
    [currentCustDict setValue:visitOption forKey:@"Visit_Type"];
    [currentCustDict setValue:[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:selectedCustomer.Area] forKey:@"Area"];
    
    /*
     
     Singleton *single = [Singleton retrieveSingleton];
     single.planDetailId = @"0";
     if([[currentCustDict stringForKey:@"Cash_Cust"]isEqualToString:@"Y"]){
     single.isCashCustomer = @"Y";
     CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:currentCustDict] ;
     [self.navigationController pushViewController:cashCustomer animated:YES];
     cashCustomer = nil;
     }
     else{
     single.isCashCustomer = @"N";
     [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"D"];
     }
     */
    
    // selectedCustomer.Cash_Cust=@"Y";
    if([selectedCustomer.Cash_Cust isEqualToString:@"Y"]){
        CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:currentCustDict] ;
        cashCustomer.selectedCustomer=selectedCustomer;
        [self.navigationController pushViewController:cashCustomer animated:YES];
        cashCustomer = nil;
    }
    else{
        Singleton *single = [Singleton retrieveSingleton];
        single.isCashCustomer = @"N";
        
        
        [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"D"];
        
        //    SalesWorxVisitOptionsViewController * visitOptions=[[SalesWorxVisitOptionsViewController alloc]init];
        //    SalesWorxVisit* salesWorxVisit=[[SalesWorxVisit alloc]init];
        //    VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
        //    currentVisitOptions.customer=selectedCustomer;
        //    salesWorxVisit.visitOptions=currentVisitOptions;
        //    visitOptions.salesWorxVisit=salesWorxVisit;
        //    [self.navigationController pushViewController:visitOptions animated:YES];
    }
}


-(void)cashCustomerSelectedVisitOption:(NSString*)visitOption customer:(SalesWorxVisit*)currentVisit
{
    NSLog(@"selected customer is %@", selectedCustomer);
    
    NSLog(@"selected visit option is %@", visitOption);
    
    
    if ([visitOption isEqualToString:kTelephonicVisitTitle]) {
        
        [SWDefaults setOnsiteVisitStatus:NO];
    }
    else if ([visitOption isEqualToString:kOnsiteVisitTitle])
    {
        [SWDefaults setOnsiteVisitStatus:YES];
    }
    else{
        
    }
    
    Singleton *single = [Singleton retrieveSingleton];
    
    if ([currentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"Y"]) {
        single.isCashCustomer = @"Y";
    }
    else{
        single.isCashCustomer=@"N";
    }
    
//    
//    [[[SWVisitManager alloc] init] startVisitWithCustomer:[SWDefaults fetchCurrentCustDict:currentVisit.visitOptions.customer] parent:self andChecker:@"D"];

    [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:currentVisit withParent:self];
    NSLog(@"check customer dict for cash customer %@",[SWDefaults fetchCurrentCustDict:currentVisit.visitOptions.customer]);
    
    
    

}

-(void)cashCustomerSelectedVisitOption:(NSString*)visitOption
{
    
    
    
    NSMutableDictionary * currentCustDict=[[NSMutableDictionary alloc]init];
    
    [currentCustDict setValue:selectedCustomer.Address forKey:@"Address"];
    [currentCustDict setValue:selectedCustomer.Allow_FOC forKey:@"Allow_FOC"];
    [currentCustDict setValue:selectedCustomer.Avail_Bal forKey:@"Avail_Bal"];
    [currentCustDict setValue:selectedCustomer.Customer_Name forKey:@"Customer_Name"];
    [currentCustDict setValue:selectedCustomer.Bill_Credit_Period forKey:@"Bill_Credit_Period"];
    [currentCustDict setValue:selectedCustomer.Cash_Cust forKey:@"Cash_Cust"];
    [currentCustDict setValue:selectedCustomer.Chain_Customer_Code forKey:@"Chain_Customer_Code"];
    [currentCustDict setValue:selectedCustomer.City forKey:@"City"];
    [currentCustDict setValue:selectedCustomer.Creation_Date forKey:@"Creation_Date"];
    [currentCustDict setValue:selectedCustomer.Credit_Hold forKey:@"Credit_Hold"];
    [currentCustDict setValue:selectedCustomer.Credit_Limit forKey:@"Credit_Limit"];
    [currentCustDict setValue:selectedCustomer.Cust_Lat forKey:@"Cust_Lat"];
    [currentCustDict setValue:selectedCustomer.Cust_Long forKey:@"Cust_Long"];
    [currentCustDict setValue:selectedCustomer.Cust_Status forKey:@"Cust_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Barcode forKey:@"Customer_Barcode"];
    [currentCustDict setValue:selectedCustomer.Customer_Class forKey:@"Customer_Class"];
    [currentCustDict setValue:selectedCustomer.Customer_ID forKey:@"Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_No forKey:@"Customer_No"];
    [currentCustDict setValue:selectedCustomer.Customer_OD_Status forKey:@"Customer_OD_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Segment_ID forKey:@"Customer_Segment_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_Type forKey:@"Customer_Type"];
    [currentCustDict setValue:selectedCustomer.Dept forKey:@"Dept"];
    [currentCustDict setValue:selectedCustomer.Location forKey:@"Location"];
    [currentCustDict setValue:selectedCustomer.Phone forKey:@"Phone"];
    [currentCustDict setValue:selectedCustomer.Postal_Code forKey:@"Postal_Code"];
    [currentCustDict setValue:selectedCustomer.Price_List_ID forKey:@"Price_List_ID"];
    [currentCustDict setValue:selectedCustomer.SalesRep_ID forKey:@"SalesRep_ID"];
    [currentCustDict setValue:selectedCustomer.Sales_District_ID forKey:@"Sales_District_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Customer_ID forKey:@"Ship_Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Site_Use_ID forKey:@"Ship_Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Site_Use_ID forKey:@"Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Trade_Classification forKey:@"Trade_Classification"];
    [currentCustDict setValue:visitOption forKey:@"Visit_Type"];
    [currentCustDict setValue:[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:selectedCustomer.Area] forKey:@"Area"];

    NSLog(@"current cust dict data %@", [currentCustDict valueForKey:@"Visit_Type"]);
    
    /*
     
     Singleton *single = [Singleton retrieveSingleton];
     single.planDetailId = @"0";
     if([[currentCustDict stringForKey:@"Cash_Cust"]isEqualToString:@"Y"]){
     single.isCashCustomer = @"Y";
     CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:currentCustDict] ;
     [self.navigationController pushViewController:cashCustomer animated:YES];
     cashCustomer = nil;
     }
     else{
     single.isCashCustomer = @"N";
     [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"D"];
     }
     */
    
    // selectedCustomer.Cash_Cust=@"Y";

        Singleton *single = [Singleton retrieveSingleton];
        single.isCashCustomer = @"N";
    
    VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
    salesWorxVisit=[[SalesWorxVisit alloc]init];
    currentVisitOptions.customer=selectedCustomer;
    salesWorxVisit.visitOptions=currentVisitOptions;
    
    [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
    
//        [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"D"];
//    
    
        //    SalesWorxVisitOptionsViewController * visitOptions=[[SalesWorxVisitOptionsViewController alloc]init];
        //    SalesWorxVisit* salesWorxVisit=[[SalesWorxVisit alloc]init];
        //    VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
        //    currentVisitOptions.customer=selectedCustomer;
        //    salesWorxVisit.visitOptions=currentVisitOptions;
        //    visitOptions.salesWorxVisit=salesWorxVisit;
        //    [self.navigationController pushViewController:visitOptions animated:YES];
   }

#pragma mark Start Visit Methods


-(void)startCustomerVisit
{
    NSString *DateString = [MedRepQueries fetchDatabaseDateFormat];

    NSMutableArray *SODReading = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_FSR_Process_Transactions where Trx_Date like '%@%%'",DateString]];
    
    if ([SODReading count] == 0 && [appControl.ODOMETER_READING isEqualToString:KAppControlsYESCode]) {
        SalesWorxODOMeterViewController *odoMeterVC = [[SalesWorxODOMeterViewController alloc]init];
        odoMeterVC.ODOMeterDelegate = self;
        UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:odoMeterVC];
        mapPopUpViewController.navigationBarHidden = YES;
        mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
        mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
        odoMeterVC.view.backgroundColor = KPopUpsBackGroundColor;
        odoMeterVC.modalPresentationStyle = UIModalPresentationCustom;
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:mapPopUpViewController animated:NO completion:nil];
        
    } else {
        BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
        if (!deviceTimeisValid)
        {
            [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
        }
        else
        {
            if ([appControl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
                
                VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
                SalesWorxCashCustomerOnsiteVisitOptionsViewController * cashCustOnsiteVC=[[SalesWorxCashCustomerOnsiteVisitOptionsViewController alloc]init];
                cashCustOnsiteVC.delegate=self;
                salesWorxVisit=[[SalesWorxVisit alloc]init];
                currentVisitOptions.customer=selectedCustomer;
                salesWorxVisit.visitOptions=currentVisitOptions;
                salesWorxVisit.isPlannedVisit=NO;
                cashCustOnsiteVC.currentVisit=salesWorxVisit;
                UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:cashCustOnsiteVC];
                mapPopUpViewController.navigationBarHidden=YES;
                //mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
                mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
                //cashCustOnsiteVC.view.backgroundColor = KPopUpsBackGroundColor;
                cashCustOnsiteVC.modalPresentationStyle = UIModalPresentationCustom;
                self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
                [self presentViewController:mapPopUpViewController animated:NO completion:nil];
                
                
                
                
                
                
                
                // [self.navigationController presentMJPopoverViewController:cashCustOnsiteVC animationType:MJPopupViewAnimationSlideTopBottom backGroundTap:YES dismissed:nil];
                
                //        if ([selectedCustomer.Cash_Cust isEqualToString:@"Y"]) {
                //            SalesWorxCashCustomerOnsiteVisitOptionsViewController * cashCustOnsiteVC=[[SalesWorxCashCustomerOnsiteVisitOptionsViewController alloc]init];
                //            cashCustOnsiteVC.delegate=self;
                //         cashCustOnsiteVC.view.backgroundColor = [UIColor colorWithRed:(86.0/255.0) green:(86.0/255.0)  blue:(86.0/255.0)  alpha:0.5];
                //            cashCustOnsiteVC.modalPresentationStyle = UIModalPresentationCustom;
                //
                //            [self.navigationController presentViewController:cashCustOnsiteVC animated:NO completion:nil];
                //        }
                //
                //        else{
                //
                //
                //        OnsiteOptionsViewController* onsiteOptions=[[OnsiteOptionsViewController alloc]init];
                //        onsiteOptions.delegate=self;
                ////        [self presentPopupViewController:onsiteOptions animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
                //
                //        [self.navigationController presentMJPopoverViewController:onsiteOptions animationType:MJPopupViewAnimationSlideTopBottom backGroundTap:YES dismissed:nil];
                //        }
                
            }
            
            else
            {
                
                NSMutableDictionary * currentCustDict=[[NSMutableDictionary alloc]init];
                
                [currentCustDict setValue:selectedCustomer.Address forKey:@"Address"];
                [currentCustDict setValue:selectedCustomer.Allow_FOC forKey:@"Allow_FOC"];
                [currentCustDict setValue:selectedCustomer.Avail_Bal forKey:@"Avail_Bal"];
                [currentCustDict setValue:selectedCustomer.Customer_Name forKey:@"Customer_Name"];
                [currentCustDict setValue:selectedCustomer.Bill_Credit_Period forKey:@"Bill_Credit_Period"];
                [currentCustDict setValue:selectedCustomer.Cash_Cust forKey:@"Cash_Cust"];
                [currentCustDict setValue:selectedCustomer.Chain_Customer_Code forKey:@"Chain_Customer_Code"];
                [currentCustDict setValue:selectedCustomer.City forKey:@"City"];
                [currentCustDict setValue:selectedCustomer.Creation_Date forKey:@"Creation_Date"];
                [currentCustDict setValue:selectedCustomer.Credit_Hold forKey:@"Credit_Hold"];
                [currentCustDict setValue:selectedCustomer.Credit_Limit forKey:@"Credit_Limit"];
                [currentCustDict setValue:selectedCustomer.Cust_Lat forKey:@"Cust_Lat"];
                [currentCustDict setValue:selectedCustomer.Cust_Long forKey:@"Cust_Long"];
                [currentCustDict setValue:selectedCustomer.Cust_Status forKey:@"Cust_Status"];
                [currentCustDict setValue:selectedCustomer.Customer_Barcode forKey:@"Customer_Barcode"];
                [currentCustDict setValue:selectedCustomer.Customer_Class forKey:@"Customer_Class"];
                [currentCustDict setValue:selectedCustomer.Customer_ID forKey:@"Customer_ID"];
                [currentCustDict setValue:selectedCustomer.Customer_No forKey:@"Customer_No"];
                [currentCustDict setValue:selectedCustomer.Customer_OD_Status forKey:@"Customer_OD_Status"];
                [currentCustDict setValue:selectedCustomer.Customer_Segment_ID forKey:@"Customer_Segment_ID"];
                [currentCustDict setValue:selectedCustomer.Customer_Type forKey:@"Customer_Type"];
                [currentCustDict setValue:selectedCustomer.Dept forKey:@"Dept"];
                [currentCustDict setValue:selectedCustomer.Location forKey:@"Location"];
                [currentCustDict setValue:selectedCustomer.Phone forKey:@"Phone"];
                [currentCustDict setValue:selectedCustomer.Postal_Code forKey:@"Postal_Code"];
                [currentCustDict setValue:selectedCustomer.Price_List_ID forKey:@"Price_List_ID"];
                [currentCustDict setValue:selectedCustomer.SalesRep_ID forKey:@"SalesRep_ID"];
                [currentCustDict setValue:selectedCustomer.Sales_District_ID forKey:@"Sales_District_ID"];
                [currentCustDict setValue:selectedCustomer.Ship_Customer_ID forKey:@"Ship_Customer_ID"];
                [currentCustDict setValue:selectedCustomer.Ship_Site_Use_ID forKey:@"Ship_Site_Use_ID"];
                [currentCustDict setValue:selectedCustomer.Site_Use_ID forKey:@"Site_Use_ID"];
                [currentCustDict setValue:selectedCustomer.Trade_Classification forKey:@"Trade_Classification"];
                
                
                
                
                Singleton *single = [Singleton retrieveSingleton];
                single.isCashCustomer = @"N";
                // [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"D"];
                
                VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
                salesWorxVisit=[[SalesWorxVisit alloc]init];
                
                currentVisitOptions.customer=selectedCustomer;
                salesWorxVisit.visitOptions=currentVisitOptions;
                
                [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
                
                
                
                
                /*
                 
                 Singleton *single = [Singleton retrieveSingleton];
                 single.planDetailId = @"0";
                 if([[currentCustDict stringForKey:@"Cash_Cust"]isEqualToString:@"Y"]){
                 single.isCashCustomer = @"Y";
                 CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:currentCustDict] ;
                 [self.navigationController pushViewController:cashCustomer animated:YES];
                 cashCustomer = nil;
                 }
                 else{
                 single.isCashCustomer = @"N";
                 [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"D"];
                 }
                 
                 
                 // selectedCustomer.Cash_Cust=@"Y";
                 if([selectedCustomer.Cash_Cust isEqualToString:@"Y"]){
                 CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:currentCustDict] ;
                 cashCustomer.selectedCustomer=selectedCustomer;
                 [self.navigationController pushViewController:cashCustomer animated:YES];
                 cashCustomer = nil;
                 }
                 else{
                 
                 
                 //[self.navigationController pushViewController:visitOptions animated:YES];
                 }
                 
                 */
                
            }
        }
    }
}

-(void)StartVisitAfterPopUp {
    //[self updateVisitOptions];
    [self startCustomerVisit];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    openVisitAlert=nil;
    
    if (alertView.tag==300 && buttonIndex==0) {
        //yes tapped
        //we are updating visit end date to tbl_fsr__actual visits similary if its a planned visit we should update status flag of TBL_FSR_Planned_Visits, get plannev visit id of actula visit if based on fsr_plan_detail id, fetch planned visit id of open visit
        
        
        NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Planned_Visit_ID from TBL_FSR_Planned_Visits where FSR_Plan_Detail_ID ='%@'",openPlannedDetailID]];
        
        if (plannedVisitIDArray.count>0) {
            
            openPlannedVisitID=[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Planned_Visit_ID"];
        }
        
        BOOL plannedVisitStatus;
        
        if ([NSString isEmpty:openPlannedVisitID]==NO) {
            plannedVisitStatus=[[SWDatabaseManager retrieveManager]closePlannedVisitwithVisitID:openPlannedVisitID];
        }
        
        BOOL visitEndDateStatus =NO;
        
        if ([NSString isEmpty:openVisitID]==NO) {
             visitEndDateStatus=  [[SWDatabaseManager retrieveManager]updateVisitEndDatewithAlert:openVisitID];
        }
       
        if (visitEndDateStatus==YES || plannedVisitStatus==YES) {
            
            [self startCustomerVisit];
        }
       
        
        NSLog(@"visit end time updated");
    }
    //    else if (alertView.tag==300 && buttonIndex==1)
    //    {
    //        //[self.navigationController popViewControllerAnimated:YES];
    //    }
}

-(void)startVisitTapped
{
    
    

    
    if ([customersSearchBar isFirstResponder]) {
        
        [customersSearchBar resignFirstResponder];
    }
    
    
    //show an alert if there is any open visit to close that visit.
     openVisitsArray=[[SWDatabaseManager retrieveManager]fetchOpenVisitsforOtherCustomers:@"" ]
    ;
    if (openVisitsArray.count>0) {
        
        NSString* customerName=[openVisitsArray valueForKey:@"Customer_Name"];
        
        openVisitID=[openVisitsArray valueForKey:@"Actual_Visit_ID"];
        
        openPlannedDetailID=[openVisitsArray valueForKey:@"FSR_Plan_Detail_ID"];

        NSString* visitType = [SWDefaults getValidStringValue:[openVisitsArray valueForKey:@"Visit_Type"]];

        //move directly to the visit if current selected customer is same as open visit
        NSString* selectedCustomerID = selectedCustomer.Ship_Customer_ID;
        NSString* selectedSiteUse_ID = selectedCustomer.Ship_Site_Use_ID;
        NSString* openVisitCustomerID = [NSString getValidStringValue:[openVisitsArray valueForKey:@"Customer_ID"]];
        NSString* openVisitSiteUseID = [NSString getValidStringValue:[openVisitsArray valueForKey:@"Site_Use_ID"]];

        NSLog(@"selected customer id is %@\n site use id : %@\n open visit customer id is %@ \n %@ open visit customer 5@",selectedCustomerID,openVisitCustomerID,selectedSiteUse_ID,openVisitSiteUseID);
         openVisitCustomer = [[Customer alloc]init];
        //get the customer object for open visit customer
        NSPredicate* openVisitCustomerPredicate = [NSPredicate predicateWithFormat:@"SELF.Ship_Customer_ID ==%@ AND SELF.Ship_Site_Use_ID == %@",openVisitCustomerID,openVisitSiteUseID];
        NSArray* filteredArray = [customersArray filteredArrayUsingPredicate:openVisitCustomerPredicate];
        if (filteredArray.count>0){
            openVisitCustomer=filteredArray[0];
        }
        
        if([selectedCustomerID isEqualToString:openVisitCustomerID] && [openVisitSiteUseID isEqualToString:selectedSiteUse_ID])
        {
            //its the same customer, continue open visit id
            salesWorxVisit=[[SalesWorxVisit alloc]init];
            VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
            currentVisitOptions.customer=selectedCustomer;

            salesWorxVisit.Visit_Type=visitType;
            salesWorxVisit.visitOptions=currentVisitOptions;
            salesWorxVisit.visitOptions.customer.Visit_Type = visitType;

            //kOnsiteVisitTitle
            [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];

//            [self startCustomerVisit];

        }
        
       else  {

           
           OpenVisitAlertViewController *alertVC = [[OpenVisitAlertViewController alloc]init];
           alertVC.openVisitCustomer = openVisitCustomer;
           alertVC.selectedCustomer=selectedCustomer;
           alertVC.selectedReasonDelegate=self;
           UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:alertVC];
           mapPopUpViewController.navigationBarHidden = YES;
           mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
           mapPopUpViewController.preferredContentSize = CGSizeMake(400, 300);
           alertVC.modalPresentationStyle = UIModalPresentationFormSheet;
           mapPopUpViewController.modalPresentationStyle = UIModalPresentationFormSheet;
           [self presentViewController:mapPopUpViewController animated:YES completion:nil];
           
           /*
           [[UILabel appearanceWhenContainedIn:[UIAlertController class], nil] setNumberOfLines:2];
           [[UILabel appearanceWhenContainedIn:[UIAlertController class], nil] setFont:kSWX_FONT_REGULAR(10)];
           [[UILabel appearanceWhenContainedIn:[UIAlertController class], nil] setTextAlignment:NSTextAlignmentLeft];

           
//           __________________________________________________________________
//           ------------------------------------------------------------------
           NSString* messageString = [NSString stringWithFormat:@"You already have an existing open visit for %@\n\nAnd you have attempted to start a new visit for %@\n\nPlease confirm how would you like to proceed?",openVisitCustomer.Customer_Name,selectedCustomer.Customer_Name];
           [LGAlertView appearance].messageTextAlignment=NSTextAlignmentCenter;
           [LGAlertView appearance].buttonsTextAlignment=NSTextAlignmentCenter;
           [LGAlertView appearance].cancelButtonTextAlignment=NSTextAlignmentCenter;
           [LGAlertView appearance].messageFont=kSWX_FONT_REGULAR(14);
           [LGAlertView appearance].titleFont=kSWX_FONT_SEMI_BOLD(15);
           [LGAlertView appearance].width = 350;
//           [LGAlertView appearance].tintColor = UIColor.blueColor;
          // [LGAlertView appearance].backgroundColor = [UIColor colorWithRed:(241.0/255.0) green:(241.0/255.0) blue:(241.0/255.0) alpha:1];
           
           [LGAlertView appearance].buttonsFont=kSWX_FONT_SEMI_BOLD(15);
           [LGAlertView appearance].cancelButtonFont=kSWX_FONT_SEMI_BOLD(15);
           
           LGAlertView* lgAlert = [[LGAlertView alloc]initWithTitle:@"Open Visit" message:messageString style:LGAlertViewStyleAlert buttonTitles:@[@"Close the existing visit and start new visit",@"Continue with the existing open visit"] cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil delegate:self];
        
          // [lgAlert show];
           

           openVisitAlertController = [UIAlertController alertControllerWithTitle:@"Open Visit" message:messageString preferredStyle:UIAlertControllerStyleAlert];
           [openVisitAlertController setValue:[self fetchAttributedMessageForAlert:messageString] forKey:@"attributedMessage"];
           
           
           UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Close the existing visit and start new visit." style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Planned_Visit_ID from TBL_FSR_Planned_Visits where FSR_Plan_Detail_ID ='%@'",openPlannedDetailID]];
                                                                if (plannedVisitIDArray.count>0) {
                                                                    openPlannedVisitID=[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Planned_Visit_ID"];
                                                                }
                                                                BOOL plannedVisitStatus;
                                                                if ([NSString isEmpty:openPlannedVisitID]==NO) {
                                                                    plannedVisitStatus=[[SWDatabaseManager retrieveManager]closePlannedVisitwithVisitID:openPlannedVisitID];
                                                                }
                                                                BOOL visitEndDateStatus =NO;
                                                                if ([NSString isEmpty:openVisitID]==NO) {
                                                                    visitEndDateStatus=  [[SWDatabaseManager retrieveManager]updateVisitEndDatewithAlert:openVisitID];
                                                                }
                                                                if (visitEndDateStatus==YES || plannedVisitStatus==YES) {
                                                                    [self startCustomerVisit];
                                                                }
                                                            }];
          // [okAction setValue:[self fetchAttributedMessageForAlert:@"Close the existing visit and start new visit."] forKey:@"attributedTitle"];

           UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Continue with the existing open visit." style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action) {
                                                                    NSString* visitType = [SWDefaults getValidStringValue:[openVisitsArray valueForKey:@"Visit_Type"]];
                                                                    
                                                                    salesWorxVisit=[[SalesWorxVisit alloc]init];
                                                                    VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
                                                                    currentVisitOptions.customer=openVisitCustomer;
                                                                    
                                                                    salesWorxVisit.visitOptions=currentVisitOptions;
                                                                    salesWorxVisit.visitOptions.customer.Visit_Type = visitType;
                                                                    
                                                                    //kOnsiteVisitTitle
                                                                    [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
                                                                }];
          // [cancelAction setValue:[self fetchAttributedMessageForAlert:@"Continue with the existing open visit."] forKey:@"attributedTitle"];

           [openVisitAlertController addAction:okAction];
           [openVisitAlertController addAction:cancelAction];
           //[self presentViewController:openVisitAlertController animated:YES completion:nil];

     
          
          

           //[self presentViewController:alert animated:YES completion:nil];
           
         
           
           */
           
        }
        
    }
    else
    {
        [self startCustomerVisit];
    }
    }
-(NSAttributedString*)fetchAttributedMessageForAlert:(NSString*)message{
    NSMutableParagraphStyle *messageParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    [messageParagraphStyle setAlignment:NSTextAlignmentLeft];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:message];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:messageParagraphStyle range:NSMakeRange(0, [message length])];
    [attributedString addAttribute:NSFontAttributeName value:kSWX_FONT_REGULAR(12) range:NSMakeRange(0, [message length])];
    return attributedString;
}
-(void)updateVisitOptions{
    //if 0 then close the open visit and start new visit, if 1 continue open visit
    if(selectedReason ==0){
        NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Planned_Visit_ID from TBL_FSR_Planned_Visits where FSR_Plan_Detail_ID ='%@'",openPlannedDetailID]];
        if (plannedVisitIDArray.count>0) {
            openPlannedVisitID=[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Planned_Visit_ID"];
        }
        BOOL plannedVisitStatus;
        if ([NSString isEmpty:openPlannedVisitID]==NO) {
            plannedVisitStatus=[[SWDatabaseManager retrieveManager]closePlannedVisitwithVisitID:openPlannedVisitID];
        }
        BOOL visitEndDateStatus =NO;
        if ([NSString isEmpty:openVisitID]==NO) {
            visitEndDateStatus=  [[SWDatabaseManager retrieveManager]updateVisitEndDatewithAlert:openVisitID];
            
            //<NG 2019-07-16> GEO validation flag added as suggested by Harpal
            if ([appControl.ENABLE_VISIT_LOC_UPDATE_ON_CLOSE isEqualToString:@"Y"]) {
                [self validateLocationCoOrdinateforOnSiteVisit];
            }
        }
        if (visitEndDateStatus==YES || plannedVisitStatus==YES) {
            [self startCustomerVisit];
        }
    }else if (selectedReason == 1){
        NSString* visitType = [SWDefaults getValidStringValue:[openVisitsArray valueForKey:@"Visit_Type"]];
        salesWorxVisit=[[SalesWorxVisit alloc]init];
        VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
        currentVisitOptions.customer=openVisitCustomer;
        salesWorxVisit.Visit_Type=visitType;

        salesWorxVisit.visitOptions=currentVisitOptions;
        salesWorxVisit.visitOptions.customer.Visit_Type = visitType;
        //kOnsiteVisitTitle
        [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
    }
}

-(void)validateLocationCoOrdinateforOnSiteVisit
{
    NSString *customerLatitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Lat];
    NSString *customerLongitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Long];
    
    //check if customer location is available
    
    SWAppDelegate *appDel = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString *fsrLatitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude];
    NSString *fsrLongitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
    
    NSLog(@"CLOSE_VISIT_LOC_UPDATE_LIMIT is %@",appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT);
    
    //fetch range from tbl_addl_info
    double distanceLimit = [appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT doubleValue];
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[customerLatitude doubleValue] longitude:[customerLongitude doubleValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[fsrLatitude doubleValue] longitude:[fsrLongitude doubleValue]];
    CLLocationDistance distance = [location1 distanceFromLocation:location2];
    
    NSLog(@"Distance between Locations in meters: %f", distance);
    
    if (distance > distanceLimit)
    {
        [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@',Custom_Attribute_6='Y' Where Actual_Visit_ID='%@'",fsrLatitude, fsrLongitude, openVisitID]];
    }
}

-(void)selectedOpenVisitReason:(NSInteger)selectedReasonCode{
    selectedReason = selectedReasonCode;
    [self performSelector:@selector(updateVisitOptions) withObject:self afterDelay:2];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
   
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")) {
        
        
    }
    
    
    customerCreditLimitLbl.font= kSWX_FONT_SEMI_BOLD(16);
    customerAvailableBalanceLbl.font=kSWX_FONT_SEMI_BOLD(16);
    customerTotalOutStandingLbl.font = kSWX_FONT_SEMI_BOLD(16);
    customerOverDueLbl.font = kSWX_FONT_SEMI_BOLD(16);
    
    if (self.isMovingToParentViewController) {
      //  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            NSMutableArray * customerListArray=[[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
           // NSLog(@"check status %@", [[customerListArray objectAtIndex:0]valueForKey:@"Cash_Cust"]);
            NSMutableArray * beaconDataArray=[[SWDatabaseManager retrieveManager]fetchBeaconDetailsforCustomers];
            NSLog(@"beacon data array is %@",beaconDataArray);
            
            NSLog(@"customers count is %lu", (unsigned long)customerListArray.count);
            
            
            customersArray=[[NSMutableArray alloc]init];
            customerOrderHistoryArray=[[NSMutableArray alloc]init];
            if (customerListArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary   * currentDict in customerListArray) {
                    
                    NSMutableDictionary   * currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Customer * currentCustomer=[[Customer alloc] init];
                    currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
                    currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
                    currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
                    currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
                    currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
                    currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
                    currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
                    currentCustomer.City=[currentCustDict valueForKey:@"City"];
                    currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
                    currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
                    currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
                    currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
                    currentCustomer.Cust_Lat=[currentCustDict valueForKey:@"Cust_Lat"];
                    currentCustomer.Cust_Long=[currentCustDict valueForKey:@"Cust_Long"];
                    currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
                    currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
                    currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
                    currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
                    currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
                    currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
                    currentCustomer.Customer_Segment_ID=[currentCustDict valueForKey:@"Customer_Segment_ID"];
                    currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
                    currentCustomer.Dept=[currentCustDict valueForKey:@"Dept"];
                    currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
                    currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
                    currentCustomer.Postal_Code=[currentCustDict valueForKey:@"Postal_Code"];
                    currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
                    currentCustomer.SalesRep_ID=[currentCustDict valueForKey:@"SalesRep_ID"];
                    currentCustomer.Sales_District_ID=[currentCustDict valueForKey:@"Sales_District_ID"];
                    currentCustomer.Ship_Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]];
                    currentCustomer.Ship_Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]];
                    currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
                    currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
                    currentCustomer.Trade_License_Expiry=[currentCustDict valueForKey:@"Trade_License_Expiry"];
                    currentCustomer.isDelegatedCustomer=[currentCustDict valueForKey:@"isDelegatedCustomer"];
                    currentCustomer.Area=[currentCustDict valueForKey:@"Area"];
                    
                    currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:@"CUST_LOC_RNG"];
                    
                    
                    //adding beacon data
                    currentCustomer.currentCustomerBeacon=nil;

                    if (beaconDataArray.count>0) {
                        NSPredicate * matchBeaconPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_ID ==  %@ and SELF.Site_Use_ID ==  %@",currentCustomer.Customer_ID,currentCustomer.Site_Use_ID]];
                        NSMutableArray * matchedBeaconDataArray=[[beaconDataArray filteredArrayUsingPredicate:matchBeaconPredicate]mutableCopy];
                        
                        if (matchedBeaconDataArray.count>0) {
                        
                            NSLog(@"matched beacon in customers %@", matchedBeaconDataArray);
                            
                        NSMutableDictionary * beaconDict=[matchedBeaconDataArray objectAtIndex:0];
                        SalesWorxBeacon * currentCustomerBeacon=[[SalesWorxBeacon alloc]init];
                        currentCustomerBeacon.Beacon_UUID=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_UUID"]];
                        currentCustomerBeacon.Beacon_Major=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Major"]];
                        currentCustomerBeacon.Beacon_Minor=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Minor"]];
                        currentCustomer.currentCustomerBeacon=currentCustomerBeacon;
                        }
                    }

                    
                    
                    
                    
                    
                    [customersArray addObject:currentCustomer];
                    
                }
                
            }
            unfilteredCustomersArray=[[NSMutableArray alloc]init];
            unfilteredCustomersArray=customersArray;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                if (customersArray.count>0) {
                   
                    customersTableView.delegate=self;
                    customersTableView.rowHeight = UITableViewAutomaticDimension;
                    customersTableView.estimatedRowHeight = 70.0;
                    NSLog(@"reloading in did appear bg thread");
                    [customersTableView reloadData];
                    
                    
                    NSTimeInterval timeSpentInBackground = [[NSDate date] timeIntervalSinceDate:startTime];

                    
                    NSLog(@"time taken to looad screen till dispatch aync %f", timeSpentInBackground);
                    
                    if (isFromDashBoard) {
                        
                    }
                    
                    //this is for beacon visit
                    
                   else if (self.isFromBeacon) {
                        
                        [self setSelectedCustomerObjectwithCustomerID:self.matchedCustomerIdFromBeacon];
                    }
                    else
                    {
                    
                    [self deselectPreviousCellandSelectFirstCell];
                    
                    }
                    self.navigationItem.rightBarButtonItem.enabled=YES;
                }
                else
                {
                    self.navigationItem.rightBarButtonItem.enabled=NO;
                }
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                
            });
        });
        
        
        
        
        
  
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"SWCustomersViewController"
                                                          owner:self
                                                        options:nil];
        
        //detailsView=[nibViews objectAtIndex:0];
        OrderHistoryView=[nibViews objectAtIndex:1];
        //OrderHistoryView.frame=detailsView.frame;
        priceListView=[nibViews objectAtIndex:2];
        priceListView.frame=detailsView.frame;
        
        
        
        
        OrderHistoryView.layer.shadowColor = [UIColor blackColor].CGColor;
        OrderHistoryView.layer.shadowOffset = CGSizeMake(2, 2);
        OrderHistoryView.layer.shadowOpacity = 0.1;
        OrderHistoryView.layer.shadowRadius = 1.0;
        
        priceListView.layer.shadowColor = [UIColor blackColor].CGColor;
        priceListView.layer.shadowOffset = CGSizeMake(2, 2);
        priceListView.layer.shadowOpacity = 0.1;
        priceListView.layer.shadowRadius = 1.0;
        
        
        
        
        //detailsView.frame=CGRectMake(266, 8,SegmentControlWidth , ScrollViewHeight);
        //NSLog(@"details view frame is %@",detailsView);
        //adding segment control
      //  segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(266, 8, SegmentControlWidth, 44)];
        

        NSLog(@"This is target Information \n %@%@",NSLocalizedString(@"Target", nil),NSLocalizedString(@"Information", nil));
        
        segmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
        segmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];


        segmentControl.sectionTitles = @[NSLocalizedString(@"Details", nil), NSLocalizedString(@"Order History", nil), NSLocalizedString(@"Price List", nil)];
        
        
        
        
        
        
        //    segmentControl.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
       // segmentControl.backgroundColor = [UIColor clearColor];
        
        segmentControl.selectionIndicatorBoxOpacity=0.0f;
        segmentControl.verticalDividerEnabled=YES;
        segmentControl.verticalDividerWidth=1.0f;
        segmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
        
        segmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
        segmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
        segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        segmentControl.tag = 3;
        segmentControl.selectedSegmentIndex = 0;
        
        
        segmentControl.layer.shadowColor = [UIColor blackColor].CGColor;
        segmentControl.layer.shadowOffset = CGSizeMake(2, 2);
        segmentControl.layer.shadowOpacity = 0.1;
        segmentControl.layer.shadowRadius = 1.0;
        
//        segmentControl.layer.shadowColor = [UIColor blackColor].CGColor;
//        segmentControl.layer.shadowOffset = CGSizeMake(0, -2);
//        segmentControl.layer.shadowOpacity = 0.1;
//        segmentControl.layer.shadowRadius = 2.0;
//        

        
        [segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
        
        __weak typeof(self) weakSelf = self;
        [segmentControl setIndexChangeBlock:^(NSInteger index) {
            // [weakSelf.customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth * index, 0, SegmentControlWidth, 200) animated:YES];
            [weakSelf.customersScrollView setContentOffset:CGPointMake(SegmentControlWidth * index, 0)];
            
            
        }];
        
        //[self.view addSubview:segmentControl];
        
       // customersScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(266, 62, SegmentControlWidth, 634)];
        customersScrollView.backgroundColor = [UIColor clearColor];
        customersScrollView.pagingEnabled = YES;
        customersScrollView.showsHorizontalScrollIndicator = NO;
        customersScrollView.contentSize = CGSizeMake(SegmentControlWidth * 3, 200);
        customersScrollView.delegate = self;
        //[self.view addSubview:customersScrollView];
        
        detailsView.frame=CGRectMake(0, 113, SegmentControlWidth, ScrollViewHeight);
        [customersScrollView addSubview:detailsView];
        
        OrderHistoryView.frame=CGRectMake(SegmentControlWidth, 113, SegmentControlWidth, ScrollViewHeight);
        [customersScrollView addSubview:OrderHistoryView];
        
        priceListView.frame=CGRectMake(SegmentControlWidth * 2, 113, SegmentControlWidth, ScrollViewHeight);
        [customersScrollView addSubview:priceListView];
        
        
        if (segmentControl.selectedSegmentIndex==0) {
            
            [customersScrollView scrollRectToVisible:CGRectMake(0, 0, SegmentControlWidth, 200) animated:NO];
            
        }
        else if (segmentControl.selectedSegmentIndex==1)
        {
            //[customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth, 0, SegmentControlWidth, 200) animated:NO];
            [customersScrollView setContentOffset:CGPointMake(SegmentControlWidth, 0)];
            
            
        }
        else
        {
            // [customersScrollView scrollRectToVisible:CGRectMake(SegmentControlWidth *2, 0, SegmentControlWidth, 200) animated:NO];
            [customersScrollView setContentOffset:CGPointMake(SegmentControlWidth*2, 0)];
            
        }
        
        NSLog(@"content offset for scroll view in customers %@", NSStringFromCGPoint(customersScrollView.contentOffset));
        
        
        
             
        // to be done
        //    if (filteredCustomers.count>0) {
        //        [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
        //        }
        //    else
        //    {
        //        [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        //
        //
        //    }
        [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        
        [priceListFilterButton setImage:[UIImage imageNamed:@"Customer_FilterEmpty"] forState:UIControlStateNormal];
        
        
        
        
        
        //    customerOrderHistoryTableView.layer.borderColor = UITextViewBorderColor.CGColor;
        //    customerOrderHistoryTableView.layer.borderWidth = 2.0;
        //    customerOrderHistoryTableView.layer.cornerRadius = 1.0;
        
        
        
        
        //    orderHistoryDescriptionTableView.layer.borderColor = UITextViewBorderColor.CGColor;
        //    orderHistoryDescriptionTableView.layer.borderWidth = 2.0;
        //    orderHistoryDescriptionTableView.layer.cornerRadius = 1.0;
        
        
        //    priceListTableView.layer.borderColor = UITextViewBorderColor.CGColor;
        //    priceListTableView.layer.borderWidth = 2.0;
        //    priceListTableView.layer.cornerRadius = 1.0;
        
        customerHeaderView.layer.shadowColor = [UIColor blackColor].CGColor;
        customerHeaderView.layer.shadowOffset = CGSizeMake(2, 2);
        customerHeaderView.layer.shadowOpacity = 0.1;
        customerHeaderView.layer.shadowRadius = 1.0;
        
        
        
        for (UIView *borderView in self.view.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                
                if (borderView.tag==1000 || borderView.tag==1001|| borderView.tag==1002|| borderView.tag==1003|| borderView.tag==1004 || borderView.tag==104 || borderView.tag==1005 || borderView.tag==1008||borderView.tag==1010 ) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                    
                }
            }
        }
        
        
        for (UIView * currentView in self.detailsView.subviews) {
            if ([currentView isKindOfClass:[UIView class]]) {
                if (currentView.tag==101 || currentView.tag==102|| currentView.tag==1002|| currentView.tag==103|| currentView.tag==104 || currentView.tag==1005 || currentView.tag==1008 ) {
                    
                    currentView.layer.shadowColor = [UIColor blackColor].CGColor;
                    currentView.layer.shadowOffset = CGSizeMake(2, 2);
                    currentView.layer.shadowOpacity = 0.1;
                    currentView.layer.shadowRadius = 1.0;
                    
                }
            }
            
        }
        
        
        
        
        
        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
        [self.navigationController.navigationBar setTranslucent:NO];
        
        if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
            
            if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
                UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
                
                [self.navigationItem setLeftBarButtonItem:leftBtn];
                
            } else {
            }
        }
        self.view.backgroundColor=UIViewControllerBackGroundColor;
        
        
        
        //[targetInfoView addSubview:self.pieChartForCustomerPotential];
        
        // NSLog(@"details view subviews are %@", targetInfoView.subviews);
        
        CGRect relativeFrame = [self.tempPieView convertRect:self.tempPieView.bounds toView:self.view];
        
        // NSLog(@"reliative frame is %@", NSStringFromCGRect(relativeFrame));
//        self.pieChartForCustomerPotential=[[XYPieChart alloc]initWithFrame:CGRectMake(316, 350, 140, 140)];
        //self.pieChartForCustomerPotential=[[XYPieChart alloc]initWithFrame:self.tempPieView.frame];
        
        self.pieChartForCustomerPotential.backgroundColor=[UIColor clearColor];
        
        self.pieChartForCustomerPotential.labelFont =[UIFont boldSystemFontOfSize:14];
        self.slicesForCustomerPotential = [[NSMutableArray alloc]init];
        //    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:60]];
        //    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:40]];
        
        self.pieChartForCustomerPotential.labelRadius = 40;
        [self.pieChartForCustomerPotential setDelegate:self];
        [self.pieChartForCustomerPotential setDataSource:self];
        //[self.pieChartForCustomerPotential setPieCenter:CGPointMake(50, 60)];
        [self.pieChartForCustomerPotential setPieCenter:CGPointMake(self.pieChartForCustomerPotential.bounds.size.width/2, self.pieChartForCustomerPotential.bounds.size.height/2)];

        [self.pieChartForCustomerPotential setShowPercentage:YES];
        [self.pieChartForCustomerPotential setLabelColor:[UIColor whiteColor]];
        [self.pieChartForCustomerPotential setUserInteractionEnabled:NO];

        self.sliceColorsForCustomerPotential =[NSArray arrayWithObjects:
                                               [UIColor colorWithRed:53.0/255.0 green:194.0/255.0 blue:195.0/255.0 alpha:1],
                                               [UIColor colorWithRed:16.0/255.0 green:49.0/255.0 blue:57.0/255.0 alpha:1],
                                               nil];
        
       // [detailsView addSubview:self.pieChartForCustomerPotential];
        
        
        
        
        [self.pieChartForCustomerPotential reloadData];
        
        customerTargetValueLbl.text=@"N/A";
        customerSalesValueLbl.text=@"N/A";
        customerBalanceToGoLbl.text=@"N/A";
        
        
        
        customerLocationMapView.showsUserLocation=YES;
        
        

        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
        NSLog(@"executionTime = %f", executionTime);
        
        
    
        // check SHOW_COLLECTION_TARGET flag is Y or N
        if ([appControl.SHOW_COLLECTION_TARGET isEqualToString:KAppControlsYESCode]) {
            targetInformationSegment.hidden = NO;
            lblTargetInformation.hidden = YES;
            dividerImageofTargetInformation.hidden = YES;
            
            
            targetInformationSegment.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
            targetInformationSegment.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
            targetInformationSegment.sectionTitles = @[NSLocalizedString(@"Sales Target", nil), NSLocalizedString(@"Collection Target", nil)];
 
            targetInformationSegment.selectionIndicatorBoxOpacity=0.0f;
            targetInformationSegment.verticalDividerEnabled=YES;
            targetInformationSegment.verticalDividerWidth=1.0f;
            targetInformationSegment.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
            
            targetInformationSegment.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
            targetInformationSegment.selectionStyle = HMSegmentedControlSelectionStyleBox;
            targetInformationSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
            targetInformationSegment.tag = 2;
            targetInformationSegment.selectedSegmentIndex = 0;
            
            targetInformationSegment.layer.shadowColor = [UIColor blackColor].CGColor;
            targetInformationSegment.layer.shadowOffset = CGSizeMake(2, 2);
            targetInformationSegment.layer.shadowOpacity = 0.1;
            targetInformationSegment.layer.shadowRadius = 1.0;
            
            [targetInformationSegment addTarget:self action:@selector(targetInformationSegmentValueChanged:) forControlEvents:UIControlEventValueChanged];
        } else {
            lblTargetInformation.hidden = NO;
            dividerImageofTargetInformation.hidden = NO;
            targetInformationSegment.hidden = YES;
        }
        lblCollectionTargetMonth.hidden = YES;
        
        if([appControl.FSR_VALUE_TYPE isEqualToString:@"Q"]){
            salesValueLabel.text = @"Target Achieved";
            TargetValueTitleLable.text = @"Target Quantity";
        }else{
            salesValueLabel.text = @"Target Achieved";
            TargetValueTitleLable.text = @"Target Value";
        }
    }
    
    
    
   
    
    
    if (isFromDashBoard) {
        
        selectedCustomer=selectedCustomerFromDashBoard;
        //match the selected customer index in customers tableview
        
   
        
        NSUInteger barIndex = [customersArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            if ([[(Customer *)obj Customer_ID] isEqualToString:selectedCustomer.Customer_ID]) {
                *stop = YES;
                return YES;
            }
            return NO;
        }];
        
        
        NSLog(@"bar index is %lu",(unsigned long)barIndex);
        

        
        if ([customersTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        {
            selectedCustomer=selectedCustomerFromDashBoard;
            MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[customersTableView cellForRowAtIndexPath:selectedIndexPath];
            if (cell) {
                cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
                cell.titleLbl.textColor=MedRepMenuTitleFontColor;
                cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
                [cell.contentView setBackgroundColor:[UIColor whiteColor]];
                cell.cellDividerImg.hidden=NO;
                
                NSLog(@"PREVIOUS CELL BEING DESELECTED");
            }
            
            [customersTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:barIndex inSection:0]
                                            animated:YES
                                      scrollPosition:UITableViewScrollPositionTop];
            [customersTableView.delegate tableView:customersTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:barIndex inSection:0]];
        }
    }
    
   // orderHistoryDetailsView.frame = CGRectMake(1024, 8, 734, 502);

    NSLog(@"order history detail view frame is %@",NSStringFromCGRect(orderHistoryDetailsView.frame));
    
    
    customerNameTitle.text =@"Name";
    
   
    areaNameLabel.text=@"";

    [self setBrandTitleName];

    NSLog(@"order history details frame check %@", NSStringFromCGRect(orderHistoryDetailsView.frame));
    
    [self hideNoSelectionView];
    [self updateCustomerLastVisitDetails];
}

-(void)setBrandTitleName{
    if ([appControl.FSR_TARGET_TYPE isEqualToString:@"B"] ) {
        
        brandCategoryTitleLbl.text = NSLocalizedString(@"Brand", nil);
    } else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"C"]) {
        
        brandCategoryTitleLbl.text = NSLocalizedString(@"Category", nil);
    }
    else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"P"] && [appControl.FSR_TARGET_PRIMARY isEqualToString:@"Area"]) {
        
        brandCategoryTitleLbl.text = NSLocalizedString(@"Area", nil);
        [brandCategoryTextField setHidden:YES];
    }
    else {
        brandCategoryTitleLbl.text = [[NSString stringWithFormat:@"%@/",NSLocalizedString(@"Brand", nil)] stringByAppendingString:NSLocalizedString(@"Category", nil)];
    }
}
#pragma mark iBeacon Visit Methods

-(void)setSelectedCustomerObjectwithCustomerID:(NSString*)selectedCustID
{

    NSInteger barIndex;
    NSPredicate *matchedObjectPredicate=[NSPredicate predicateWithFormat:@"SELF.Customer_ID == %@",selectedCustID];
    
    barIndex=[customersArray indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [matchedObjectPredicate evaluateWithObject:obj];
        
    }];
    
    NSLog(@"matched index is customers %ld", (long)barIndex);
    
    if ([customersTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        selectedCustomer=[customersArray objectAtIndex:barIndex];
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[customersTableView cellForRowAtIndexPath:selectedIndexPath];
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }
        
        [customersTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:barIndex inSection:0]
                                        animated:YES
                                  scrollPosition:UITableViewScrollPositionTop];
        [customersTableView.delegate tableView:customersTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:barIndex inSection:0]];
        
        //[self startVisitTapped];

    }
}


- (void)barButtonBackPressed:(id)sender{
    [reader dismissViewControllerAnimated: YES completion:nil];
    
}
- (void)imagePickerController: (UIImagePickerController*) readers didFinishPickingMediaWithInfo: (NSDictionary*) info{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    for(symbol in results){
        single.valueBarCode=symbol.data;
        NSLog(@"scanned barcode is %@", single.valueBarCode);
        
        //check for customer
        // NSString *upcString = symbol.data;
//        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
//        [locationFilterController setTarget:self];
//        [locationFilterController setAction:@selector(filterChangedBarCode)];
//        [locationFilterController selectionDone:self];
//        [reader dismissViewControllerAnimated: YES completion:nil];
        single.isBarCode = NO;
    }
}


-(void)launchCamera
{
    Singleton *single = [Singleton retrieveSingleton];
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    customOverlay.backgroundColor = [UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(barButtonBackPressed:)];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    
    // reader.wantsFullScreenLayout = NO;
    reader.readerView.zoom = 1.0;
    reader.showsZBarControls=NO;
    
    [self presentViewController:reader animated:YES completion:nil];
}

-(void)displayAccessDeniedAlert
{
    // denied
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
        
        alertButton = @"Go";
    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:alertText delegate:self cancelButtonTitle:alertButton otherButtonTitles:nil];
    [alert show];
}
-(void)scanBarCodeTapped
{
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        
        [self launchCamera];
        
    } else if(authStatus == AVAuthorizationStatusDenied)
    {
        [self displayAccessDeniedAlert];
    }
    
    else if(authStatus == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                
                [self launchCamera];
                
            } else { // Access denied ..do something
                [self displayAccessDeniedAlert];
            }
        }];
    }
    
    
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
   // NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    if (segmentedControl.selectedSegmentIndex==1) {
        [self loadOrderHistoryDetails];
    }
    else if (segmentedControl.selectedSegmentIndex==2)
    {
        [self loadPriceListDetails];
    }
    
   
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld", (long)segmentedControl.selectedSegmentIndex);
    
    
}


- (void)setApperanceForLabel:(UILabel *)label {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    label.backgroundColor = color;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:21.0f];
    label.textAlignment = NSTextAlignmentCenter;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma scrollview delegate methods


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==customersTableView) {
//        [customersSearchBar setShowsCancelButton:NO animated:YES];
//        [customersSearchBar resignFirstResponder];

    }
    
    //NSLog(@"content offset in did scroll %f", scrollView.contentOffset.y);
    
    
    
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (scrollView==customerOrderHistoryTableView || scrollView==customersTableView||scrollView==priceListTableView||scrollView==orderHistoryDescriptionTableView) {
        
    }
    else{
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    NSLog(@"page is %d", page);
    
    if (page==1) {
        [self loadOrderHistoryDetails];
        }
        else if (page==2)
        {
            [self loadPriceListDetails];
        }
        else if(page==0)
        {
            
        }
     [segmentControl setSelectedSegmentIndex:page animated:YES];
    }
   }

-(void)loadOrderHistoryDetails
{
    
    if (selectedCustomer.customerOrderHistoryArray.count>0) {
        
        
        [customerOrderHistoryTableView reloadData];
        totalOrdersLbl.text=[NSString stringWithFormat:@"%lu",(unsigned long)selectedCustomer.customerOrderHistoryArray.count];
        totalOrdervalueLbl.text=[[NSString stringWithFormat:@"%0.2f",selectedCustomer.totalOrderAmount] currencyString];
        
    }
    else{
        
    }
    
    NSTimeInterval timeSpentInBackground = [[NSDate date] timeIntervalSinceDate:startTime];
    
    
    NSLog(@"time taken to looad screen till order history details %f", timeSpentInBackground);

}


-(void)loadPriceListDetails
{
    [priceListTableView reloadData];
    if(selectedCustomer.customerPriceListArray.count==0){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    }
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        if(selectedCustomer.customerPriceListArray.count==0){
            NSMutableArray * curentCustPriceListArray=[[[SWDatabaseManager retrieveManager] dbGetPriceListGeneric:selectedCustomer] mutableCopy];
            selectedCustomer.customerPriceListArray=curentCustPriceListArray;
            filteredCustomerPriceListArray=curentCustPriceListArray;
        }

        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [priceListTableView reloadData];

        });
    });

    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/






#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==customersTableView) {
        
        return 70.0f;
    }
    else{
        if (hideDivider==YES) {
            return 45.0f;
        }
        else{
            return 49.0f;
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==customersTableView) {
        
        return UITableViewAutomaticDimension;

    }
    
    else
    {
        if (hideDivider==YES) {
            return 45.0f;
        }
        else{
        return 49.0f;
        }

    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (tableView==customerOrderHistoryTableView && selectedCustomer.customerOrderHistoryArray.count>0 )
    {
        if (selectedCustomer.customerOrderHistoryArray.count>0) {
            return selectedCustomer.customerOrderHistoryArray.count;
        }
        else{
            return 0;
        }
        
        //return selectedCustomer.customerOrderHistoryArray.count;
    }
    else if(tableView==customersTableView)
    {
        if (isSearching==YES && filteredCustomers.count>0) {
            [self EnableCustomerOptions];
            return filteredCustomers.count;
        }
        else if(isSearching ==NO && customersArray.count>0)
        {
            [self EnableCustomerOptions];
            return customersArray.count;
        }
        else
        {
            [self DisableCustomerOptions];
            return 0;
        }
    }
    
    else if (tableView==priceListTableView && filteredCustomerPriceListArray.count>0)
    {
        return filteredCustomerPriceListArray.count;
    }
    
    else if (tableView==orderHistoryDescriptionTableView && selectedCustomer.customerOrderHistoryLineItemsArray.count>0)
    {
        return selectedCustomer.customerOrderHistoryLineItemsArray.count;
        
    }
    
    else
    {
        return 0;
    }
}
-(void)DisableCustomerOptions
{
    [segmentControl setUserInteractionEnabled:NO];
    [startVisitButton setEnabled:NO];

}
-(void)EnableCustomerOptions
{
    [segmentControl setUserInteractionEnabled:YES];
    [startVisitButton setEnabled:YES];

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
   // NSLog(@"index path is %d", indexPath.row);
   // NSLog(@"array count %d",customersArray.count);
    if (tableView==customersTableView) {
        
        Customer * currentCust;
        if (isSearching==YES) {
            currentCust=[filteredCustomers objectAtIndex:indexPath.row];
        }
        else
        {
            currentCust=[customersArray objectAtIndex:indexPath.row];
        }
        
        static NSString* identifier=@"updatedDesignCell";
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if (isSearching && !NoSelectionView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([indexPathArray containsObject:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
        
        cell.titleLbl.text=[[NSString stringWithFormat:@"%@",currentCust.Customer_Name] capitalizedString];
        cell.descLbl.text=[NSString stringWithFormat:@"%@",currentCust.Customer_No];
        
        
        if([currentCust.isDelegatedCustomer isEqualToString:KAppControlsYESCode]){
            [cell.cellSpecialIndicationImageView setHidden:NO];
            cell.cellSpecialIndicationImageView.image=[UIImage imageNamed:@"DelegatedCustomerFlag"];
        }else{
            [cell.cellSpecialIndicationImageView setHidden:YES];
        }
        
        return cell;
    }
    
    else if (tableView==customerOrderHistoryTableView)
    {
        static NSString* identifier=@"customerOrderHistoryCell";
        SWCustomerOrderHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        if (hideDivider==YES) {
            cell.dividerImageView.hidden=YES;
        }
        else{
            cell.dividerImageView.hidden=NO;
        }
        CustomerOrderHistory * currentCustOrderHistoryDict=[selectedCustomer.customerOrderHistoryArray objectAtIndex:indexPath.row];
        
            CustomerOrderHistory* currentCustomerOrderHistory=[[CustomerOrderHistory alloc]init];
             currentCustomerOrderHistory.Creation_Date=[currentCustOrderHistoryDict valueForKey:@"Creation_Date"];
             currentCustomerOrderHistory.Customer_Name=[currentCustOrderHistoryDict valueForKey:@"Customer_Name"];
                 currentCustomerOrderHistory.Shipping_Instructions=[currentCustOrderHistoryDict valueForKey:@"Shipping_Instructions"];
             currentCustomerOrderHistory.ERP_Status=[currentCustOrderHistoryDict valueForKey:@"ERP_Status"];
             currentCustomerOrderHistory.FSR=[currentCustOrderHistoryDict valueForKey:@"FSR"];
                 currentCustomerOrderHistory.ERP_Ref_Number=[currentCustOrderHistoryDict valueForKey:@"ERP_Ref_No"];
             currentCustomerOrderHistory.Orig_Sys_Document_Ref=[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"];
             currentCustomerOrderHistory.Transaction_Amt=[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"];
                 currentCustomerOrderHistory.End_Time=[currentCustOrderHistoryDict valueForKey:@"End_Time"];

        
        
        cell.docRefNumberLbl.text=[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Orig_Sys_Document_Ref];
        if ([[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Shipping_Instructions] isEqualToString:@"N/A"]) {
            
            cell.orderStatusLbl.text=kNewStatus;
        }
        else
        {
        cell.orderStatusLbl.text=[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Shipping_Instructions];
        }
        NSString*refinedCreatedtionDate=[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.End_Time];
        cell.orderDateLbl.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:refinedCreatedtionDate];
        cell.orderValueLbl.text=[[NSString stringWithFormat:@"%@",currentCustomerOrderHistory.Transaction_Amt] currencyString];
        return cell;
    }
    
    
    else if (tableView==priceListTableView)
    {
       
        static NSString* identifier=@"customerPriceList";
        SWCustomerPriceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerPriceListTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        if (hideDivider==YES) {
            cell.dividerImageView.hidden=YES;
        }
        else{
            cell.dividerImageView.hidden=NO;
        }
        SWCustomerPriceList * currentPriceListDict=[filteredCustomerPriceListArray objectAtIndex:indexPath.row];
        
        SWCustomerPriceList * currentCustomerPriceList=[[SWCustomerPriceList alloc]init];
        currentCustomerPriceList.Brand_Code=[currentPriceListDict valueForKey:@"Brand_Code"];
        currentCustomerPriceList.Description=[currentPriceListDict valueForKey:@"Description"];
        currentCustomerPriceList.Is_Generic=[currentPriceListDict valueForKey:@"Is_Generic"];
        currentCustomerPriceList.Item_Code=[currentPriceListDict valueForKey:@"Item_Code"];
        currentCustomerPriceList.Item_UOM=[currentPriceListDict valueForKey:@"Item_UOM"];
        currentCustomerPriceList.Unit_List_Price=[currentPriceListDict valueForKey:@"Unit_List_Price"];
        currentCustomerPriceList.Unit_Selling_Price=[currentPriceListDict valueForKey:@"Unit_Selling_Price"];

        
        
        cell.descriptionLbl.text=[NSString stringWithFormat:@"%@",currentCustomerPriceList.Description];
        cell.isGenricLbl.text=[NSString stringWithFormat:@"%@",currentCustomerPriceList.Is_Generic];
        cell.uomLbl.text=[NSString stringWithFormat:@"%@",currentCustomerPriceList.Item_UOM];
        cell.itemCodeLbl.text=[NSString stringWithFormat:@"%@",currentCustomerPriceList.Item_Code];
        cell.unitListPriceLbl.text=[[NSString stringWithFormat:@"%@",currentCustomerPriceList.Unit_List_Price] currencyString];
        cell.unitSellingPriceLbl.text=[[NSString stringWithFormat:@"%@",currentCustomerPriceList.Unit_Selling_Price] currencyString];
        return cell;
    }
    
    else  if (tableView==orderHistoryDescriptionTableView)
    {
        static NSString* identifier=@"orderHistoryDescription";
        SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        if (hideDivider==YES) {
            cell.dividerImageView.hidden=YES;
        }
        else{
            cell.dividerImageView.hidden=NO;
        }
        CustomerOrderHistoryLineItems * currentCustomerHistoryLineItems=[selectedCustomer.customerOrderHistoryLineItemsArray objectAtIndex:indexPath.row];
        
        if ([NSString isEmpty:[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Item_Code]]==YES) {
            
            cell.itemCodeLbl.text=KNotApplicable;
        }
        else
        {
            cell.itemCodeLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Item_Code];
  
        }
        
        if ([NSString isEmpty:[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Description]]==YES) {
             cell.descriptionLbl.text=kProductInfoUnavailable;
        }
        
        else
        {
        
        cell.descriptionLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Description];
        }
        cell.qtyLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Ordered_Quantity];
        cell.unitPriceLbl.text=[[SWDefaults getValidStringValue:currentCustomerHistoryLineItems.Unit_Selling_Price]  currencyString];
        cell.totalLbl.text=[[SWDefaults getValidStringValue:currentCustomerHistoryLineItems.Item_Value ]  currencyString];
        
        
      
        return cell;
    }
    
    else
    {
        return nil;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==customerOrderHistoryTableView|| tableView==priceListTableView||tableView==orderHistoryDescriptionTableView) {
        return 49.0f;
    }
    else
    {
        return 0.0f;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==customerOrderHistoryTableView) {
    static NSString* identifier=@"customerOrderHistoryCell";
    SWCustomerOrderHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell=nil;
        if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        cell.dividerImageView.hidden=YES;
        cell.isHeader=YES;
        cell.docRefNumberLbl.isHeader=YES;
        cell.orderStatusLbl.isHeader=YES;
        cell.orderDateLbl.isHeader=YES;
        cell.orderValueLbl.isHeader=YES;

        
        
    cell.docRefNumberLbl.text=@"Order No";
    cell.orderStatusLbl.text=@"Order Status";
    cell.orderDateLbl.text=@"Order Date";
    cell.orderValueLbl.text=@"Order Value";
    return cell;
    }
    
    else if (tableView==priceListTableView)
    {
        static NSString* identifier=@"customerPriceList";
        SWCustomerPriceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell=nil;

        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerPriceListTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        cell.dividerImageView.hidden=YES;
        
        cell.isHeader=YES;
        
        cell.descriptionLbl.isHeader=YES;
        cell.uomLbl.isHeader=YES;
        cell.itemCodeLbl.isHeader=YES;
        cell.unitListPriceLbl.isHeader=YES;
        cell.unitSellingPriceLbl.isHeader=YES;
       
        
        cell.descriptionLbl.text=@"Description";
        cell.uomLbl.text=@"UOM";
        cell.itemCodeLbl.text=@"Item Code";
        cell.unitListPriceLbl.text=@"List Price";
        cell.unitSellingPriceLbl.text=@"Selling Price";
        return cell;
        
    }
    
    else if (tableView==orderHistoryDescriptionTableView)
    {
        static NSString* identifier=@"orderHistoryDescription";
        SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell=nil;

        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        cell.dividerImageView.hidden=YES;
        cell.isHeader=YES;
        
        cell.itemCodeLbl.isHeader=YES;
        cell.descriptionLbl.isHeader=YES;
        cell.itemCodeLbl.isHeader=YES;
        cell.qtyLbl.isHeader=YES;
        cell.totalLbl.isHeader=YES;
        cell.unitPriceLbl.isHeader=YES;
        
       

        cell.itemCodeLbl.text=@"Item Code";
        cell.descriptionLbl.text=@"Description";
        cell.qtyLbl.text=@"Qty";
        cell.unitPriceLbl.text=@"Unit Price";
        cell.totalLbl.text=@"Total";
        
        return cell;

    }
    else
    {
        return nil;
    }
}

-(void)fetchOrderLineItems:(CustomerOrderHistory*)orderHistory
{
    
   // NSDate *testDate=[NSDate date];
    
   
    
    
    lineItemsSegment.selectedSegmentIndex=0;
    
    
    NSMutableArray * currentOrderHistoryLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderItems:orderHistory.Orig_Sys_Document_Ref] mutableCopy];
    
    unfilteredOrderedLineItems=[[NSMutableArray alloc]init];
    
    selectedCustomer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc]init];
    selectedCustomer.customerOrderHistoryInvoiceLineItemsArray=[[NSMutableArray alloc]init];
    
    if (currentOrderHistoryLineItemsArray.count>0) {
    
        NSLog(@"order line item dict %@", currentOrderHistoryLineItemsArray);

        
    selectedCustomer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc] init];

    for (NSMutableDictionary * currentLineItemDict in currentOrderHistoryLineItemsArray) {
        
        CustomerOrderHistoryLineItems * custOrderHistoryLineItems=[[CustomerOrderHistoryLineItems alloc]init];
        
        
        custOrderHistoryLineItems.Calc_Price_Flag=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Calc_Price_Flag"]];
        custOrderHistoryLineItems.Description=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Description"]];
        custOrderHistoryLineItems.Inventory_Item_ID=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Inventory_Item_ID"]];
        custOrderHistoryLineItems.Item_Code=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Item_Code"]];
        custOrderHistoryLineItems.Order_Quantity_UOM=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Order_Quantity_UOM"]];
        custOrderHistoryLineItems.Ordered_Quantity=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Ordered_Quantity"]];
        custOrderHistoryLineItems.Unit_Selling_Price=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Unit_Selling_Price"]];
        custOrderHistoryLineItems.Item_Value=[SWDefaults getValidStringValue:[currentLineItemDict valueForKey:@"Item_Value"]];
        
        [selectedCustomer.customerOrderHistoryLineItemsArray addObject:custOrderHistoryLineItems];
        
        [unfilteredOrderedLineItems addObject:custOrderHistoryLineItems];
        
    }
    
    }
    
       NSLog(@"history line items are %@", selectedCustomer.customerOrderHistoryLineItemsArray);
    
    NSLog(@"fetching line items for customer %@", selectedCustomer.Customer_Name);
    //fetch invoiced items
    
    
    NSMutableArray* invoicedLineItemsArray=[[[SWDatabaseManager retrieveManager] dbGetSalesOrderInvoice:orderHistory.Orig_Sys_Document_Ref] mutableCopy];
    NSLog(@"invoices line items are %@", invoicedLineItemsArray);
    
    
    unfilteredInvoiceLineItems=[[NSMutableArray alloc]init];
    
    
    selectedCustomer.customerOrderHistoryInvoiceLineItemsArray=[[NSMutableArray alloc]init];
    
    for (NSMutableDictionary * tempDict in invoicedLineItemsArray) {
        
      NSMutableDictionary*  invoiceItemsDict=[NSMutableArray nullFreeDictionaryWithDictionary:tempDict];
        
        CustomerOrderHistoryInvoiceLineItems * invoiceLineItems=[[CustomerOrderHistoryInvoiceLineItems alloc]init];
        invoiceLineItems.Calc_Price_Flag=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Calc_Price_Flag"]];
        invoiceLineItems.Description=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Description"]];
        invoiceLineItems.Expiry_Date=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Expiry_Date"]];
        invoiceLineItems.Inventory_Item_ID=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Inventory_Item_ID"]];
        invoiceLineItems.Item_Code=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Item_Code"]];
        invoiceLineItems.Item_Value=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Item_Value"]];
        invoiceLineItems.Order_Quantity_UOM=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Order_Quantity_UOM"]];
        invoiceLineItems.Ordered_Quantity=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Ordered_Quantity"]];
        invoiceLineItems.Unit_Selling_Price=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"Unit_Selling_Price"]];
        invoiceLineItems.ERP_Ref_No=[SWDefaults getValidStringValue:[invoiceItemsDict valueForKey:@"ERP_Ref_No"]];
        
        
        
        //fetch creation date
        
        NSString* creationDate=[[SWDatabaseManager retrieveManager]fetchCreationDateforInvoice:invoiceLineItems.ERP_Ref_No];
        
        NSLog(@"creation Date is %@", creationDate);
        
        invoiceLineItems.Order_Date=creationDate;
        
        
        [selectedCustomer.customerOrderHistoryInvoiceLineItemsArray addObject:invoiceLineItems];
        
        [unfilteredInvoiceLineItems addObject:invoiceLineItems];
        
        
    }
    

    orderHistoryLineItemsOrderNumberLbl.text=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"Order No: %@",orderHistory.Orig_Sys_Document_Ref]];
    

   // [orderHistoryDescriptionTableView reloadData];

    NSString * refinedOrderStatus=[[NSString alloc]init];
    
    if ([orderStatus isEqualToString:KNotApplicable] ||[NSString isEmpty:orderStatus]==YES) {
        
                    refinedOrderStatus=kNewStatus;
                }
                else
                {
                refinedOrderStatus=[NSString stringWithFormat:@"%@",orderStatus];
                }

    
  
//    NSTimeInterval timeSpentInBackground = [[NSDate date] timeIntervalSinceDate:testDate];
//    
//    
//    NSLog(@"time taken to load order line items %f", timeSpentInBackground);
    
    
    
    
    
    
    
    SWCustomerOrderHistoryDescriptionViewController * orderHistoryDescVC=[[SWCustomerOrderHistoryDescriptionViewController alloc]init];
    orderHistoryDescVC.orderLineItems=unfilteredOrderedLineItems;
    orderHistoryDescVC.invoiceLineItems=unfilteredInvoiceLineItems;
    orderHistoryDescVC.status=refinedOrderStatus;
    orderHistoryDescVC.orderNumber=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",orderHistory.Orig_Sys_Document_Ref]];
    orderHistoryDescVC.selectedCustomer=selectedCustomer;
    orderHistoryDescVC.erpOrderNumber=orderHistory.ERP_Ref_Number;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:orderHistoryDescVC];
    mapPopUpViewController.navigationBarHidden=YES;
    //mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    //cashCustOnsiteVC.view.backgroundColor = KPopUpsBackGroundColor;
    orderHistoryDescVC.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];

    
    
    
    
    
    
//    [UIView animateWithDuration:0.3 animations:^{
////        orderHistoryDetailsView.hidden=NO;
//        orderHistoryDetailsView.frame = CGRectMake(8, 8, 734, 505);
//
//        if ([orderStatus isEqualToString:KNotApplicable] ||[NSString isEmpty:orderStatus]==YES) {
//            
//            statusLbl.text=kNewStatus;
//        }
//        else
//        {
//        statusLbl.text=[NSString stringWithFormat:@"%@",orderStatus];
//        }
//    } completion:^(BOOL finished) {
//        NSLog(@"customer order history line items before reloading data %@", selectedCustomer.customerOrderHistoryLineItemsArray);
//
//        
//    }];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    customerTargetValueLbl.text=@"N/A";
    customerSalesValueLbl.text=@"N/A";
    customerBalanceToGoLbl.text=@"N/A";

       if (tableView==customerOrderHistoryTableView) {
      

           
           
      //  CustomerOrderHistory * selectedCustOrderHistory=[selectedCustomer.customerOrderHistoryArray objectAtIndex:indexPath.row];
        
           
           CustomerOrderHistory * currentCustOrderHistoryDict=[selectedCustomer.customerOrderHistoryArray objectAtIndex:indexPath.row];
           
           CustomerOrderHistory* currentCustomerOrderHistory=[[CustomerOrderHistory alloc]init];
           currentCustomerOrderHistory.Creation_Date=[currentCustOrderHistoryDict valueForKey:@"Creation_Date"];
           currentCustomerOrderHistory.Customer_Name=[currentCustOrderHistoryDict valueForKey:@"Customer_Name"];
           currentCustomerOrderHistory.Shipping_Instructions=[currentCustOrderHistoryDict valueForKey:@"Shipping_Instructions"];
           currentCustomerOrderHistory.ERP_Status=[currentCustOrderHistoryDict valueForKey:@"ERP_Status"];
           currentCustomerOrderHistory.FSR=[currentCustOrderHistoryDict valueForKey:@"FSR"];
           currentCustomerOrderHistory.ERP_Ref_Number=[currentCustOrderHistoryDict valueForKey:@"ERP_Ref_No"];
           currentCustomerOrderHistory.Orig_Sys_Document_Ref=[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"];
           currentCustomerOrderHistory.Transaction_Amt=[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"];
           currentCustomerOrderHistory.End_Time=[currentCustOrderHistoryDict valueForKey:@"End_Time"];

           
   
           orderHistoryDetailsView.hidden=NO;
           orderStatus= currentCustomerOrderHistory.Shipping_Instructions;
        [self fetchOrderLineItems:currentCustomerOrderHistory];
           
       }else if (tableView==priceListTableView){
           
       }
    
    else if (tableView==orderHistoryDescriptionTableView && self.orderHistoryLineItemsSegment.selectedSegmentIndex==1)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        SWCustomerInvoicePopOverViewController * invoicePopOverVC=[[SWCustomerInvoicePopOverViewController alloc]init];
        invoicePopOverController=nil;
        
        CustomerOrderHistoryInvoiceLineItems * invLineItems=[unfilteredInvoiceLineItems objectAtIndex:indexPath.row];
        
        invoicePopOverVC.orderDate=invLineItems.Order_Date;
        
        invoicePopOverVC.orderNumber=invLineItems.ERP_Ref_No;
        
        
        invoicePopOverController = [[UIPopoverController alloc]
                        initWithContentViewController:invoicePopOverVC];
        invoicePopOverController.popoverContentSize=CGSizeMake(300, 195);
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        CGRect rect=CGRectMake(cell.bounds.origin.x+274, cell.bounds.origin.y+10, 50, 30);
        
        invoicePopOverVC.invoicePopOver=invoicePopOverController;
        
        
        [invoicePopOverController presentPopoverFromRect:rect inView:cell permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
    else
    {
        filteredCustomerPriceListArray=[[NSMutableArray alloc]init];

        [targetInformationSegment setSelectedSegmentIndex:0 animated:YES];
        lblCollectionTargetMonth.hidden = YES;
        brandCategoryTextField.hidden = NO;
        [self setBrandTitleName];
        
        _slicesForCustomerPotential=[[NSMutableArray alloc]init];
        [_pieChartForCustomerPotential reloadData];
        
    selectedIndexPath=indexPath;
    pieChartDataArray=[[NSMutableArray alloc]init];
   // [customerTargetPieChart reloadData];
  
        if (tableView==customersTableView) {
            
            [self customerPriceListFilterDidReset];
            [self hideNoSelectionView];
            
            if (isSearching==YES && filteredCustomers.count>0) {
                
                if (filteredCustomers.count < indexPath.row) {
                    return;
                }
                selectedCustomer=[filteredCustomers objectAtIndex:indexPath.row];
            }
            else
            {
                if (customersArray.count < indexPath.row) {
                    return;
                }
                selectedCustomer=[customersArray objectAtIndex:indexPath.row];
                
            }
//            selectedCustomer.customerPriceListArray=[[NSMutableArray alloc]init];
            
            
            [SalesWorxiBeaconManager destroyBeaconSingleton];
            if (selectedCustomer.currentCustomerBeacon!=nil) {
                [self scanForiBeacon:selectedCustomer.currentCustomerBeacon];
            }
            
            [self fetchLatestOrderHistory];
            
            
            
            orderHistoryDetailsView.hidden=NO;
            orderHistoryDetailsView.frame = CGRectMake(2024, 8, 734, 502);
            
            [segmentControl setSelectedSegmentIndex:0 animated:YES];
            [customersScrollView setContentOffset:CGPointZero animated:YES];
            
            
            
            NSLog(@"check customer status in did select %@", selectedCustomer.Cash_Cust);
            
            
            // Create Annotation point
            MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
            CLLocationCoordinate2D custCoordinates;
            custCoordinates.latitude=[selectedCustomer.Cust_Lat doubleValue];
            custCoordinates.longitude=[selectedCustomer.Cust_Long doubleValue];
            NSLog(@"customer coordinates are %0.2f,%0.2f",custCoordinates.latitude,custCoordinates.longitude);
            Pin.coordinate = custCoordinates;
            // add annotation to mapview
            [customerLocationMapView removeAnnotations:customerLocationMapView.annotations];
            [customerLocationMapView addAnnotation:Pin];
            
            [customerLocationMapView setCenterCoordinate:custCoordinates animated:YES];
            
            [customerLocationMapView setUserTrackingMode:MKUserTrackingModeNone];
            
            MKCoordinateRegion adjustedRegion = [customerLocationMapView regionThatFits:MKCoordinateRegionMakeWithDistance(custCoordinates, 5000, 5000)];
            [customerLocationMapView setRegion:adjustedRegion animated:NO];
            
            
            selectedDoctorIndexPath=indexPath;
            if (indexPathArray.count==0) {
                [indexPathArray addObject:indexPath];
            }
            else
            {
                indexPathArray=[[NSMutableArray alloc]init];
                [indexPathArray addObject:indexPath];
                //[indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
            }
            MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
            
            cell.titleLbl.textColor=[UIColor whiteColor];
            [cell.descLbl setTextColor:[UIColor whiteColor]];
            cell.cellDividerImg.hidden=YES;
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
            [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
            [self populateSelectedCustomerDetails:selectedCustomer];
            NSLog(@"customers array count in reload did select %lu", (unsigned long)customersArray.count);
            
            customersTableView.delegate=self;
            customersTableView.rowHeight = UITableViewAutomaticDimension;
            customersTableView.estimatedRowHeight = 70.0;
            
            NSLog(@"reloading in did select");
            if ([appControl.FSR_TARGET_TYPE isEqualToString:@"P"] && [appControl.FSR_TARGET_PRIMARY isEqualToString:@"Area"]) {
                
                brandCategoryTitleLbl.text = NSLocalizedString(@"Area", nil);
                [brandCategoryTextField setHidden:YES];
                areaNameLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:selectedCustomer.Area];/**City replace with Area in future*/
            }
            [customersTableView reloadData];
            [customerOrderHistoryTableView reloadData];
            
            
            [self selectFirstCategory];
            NSMutableArray * curentCustPriceListArray=[[[SWDatabaseManager retrieveManager] dbGetPriceListGeneric:selectedCustomer] mutableCopy];
            selectedCustomer.customerPriceListArray=curentCustPriceListArray;
        }
    }
}


-(void)populateSelectedCustomerDetails:(Customer*)selectedCust
{
    
    detailsCustomerNameLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Customer_Name];
    detailsCustomerCodeLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Customer_No];
    detailsCustomerClassLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Customer_Class];
    //customerPoBoxLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Postal_Code];
    brandCategoryTextField.text=@"";
    
    customeraddressLbl.numberOfLines=1;
    
    if (![selectedCust isEqual:[NSNull null]]) {
        
        if (![NSString isEmpty:selectedCust.Address]) {
            NSString* addressString=[NSString stringWithFormat:@"%@",selectedCust.Address];
           // addressString=[[addressString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@"   "]     ;
            customeraddressLbl.text=[addressString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }else{
            customeraddressLbl.text=KNotApplicable;
        }
        if (![NSString isEmpty:selectedCust.Phone]) {
            telephoneNumberLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Phone];
        }else{
            telephoneNumberLbl.text=KNotApplicable;

        }
        if (![NSString isEmpty:selectedCust.Area]) {/**City replace with Area in future*/
            customerCityLbl.text=[NSString stringWithFormat:@"%@",selectedCust.Area];
        }else{
            customerCityLbl.text=KNotApplicable;

        }
    }else{
        customeraddressLbl.text=KNotApplicable;
        telephoneNumberLbl.text=KNotApplicable;
        customerCityLbl.text=KNotApplicable;
    }
    
    
    if (selectedCust.customerOrderHistoryArray.count==0) {
        totalOrdersLbl.text=@"N/A";
        totalOrdervalueLbl.text=@"N/A";
    }
    
    
    customerCreditLimitLbl.text=[[NSString stringWithFormat:@"%@",selectedCust.Credit_Limit] currencyString];
    
    customerAvailableBalanceLbl.text=[[NSString stringWithFormat:@"%@",selectedCust.Avail_Bal] currencyString];
    
    
    
    if ([selectedCust.Cust_Status isEqualToString:@"Y"]) {
        
        [customerStatusImageView setBackgroundColor:CustomerActiveColor];
    }
    else
    {
        [customerStatusImageView setBackgroundColor:[UIColor redColor]];
    }

    NSMutableArray * outStandingArray=[[SWDatabaseManager retrieveManager]fetchTotalOutStandingforCustomer:selectedCust];
   
    NSLog(@"outstanding array is %@", outStandingArray);
    
    if (outStandingArray.count>0) {
        
        customerTotalOutStandingLbl.text=[[NSString stringWithFormat:@"%@",[[outStandingArray objectAtIndex:0]valueForKey:@"Total"]] currencyString];
        customerOverDueLbl.text=[[NSString stringWithFormat:@"%@",[[outStandingArray objectAtIndex:0] valueForKey:@"Over_Due"]] currencyString];
        
        
        selectedCustomer.totalOutStanding=[NSString stringWithFormat:@"%@",[[outStandingArray objectAtIndex:0]valueForKey:@"Total"]];
        selectedCustomer.Over_Due=[NSString stringWithFormat:@"%@",[[outStandingArray objectAtIndex:0]valueForKey:@"Over_Due"]];
        
    }
    else
    {
        customerTotalOutStandingLbl.text=@"N/A";
        customerOverDueLbl.text=@"N/A";
    }
    
    CLLocationCoordinate2D  currentCustCoord;
    currentCustCoord.latitude=[selectedCust.Cust_Lat doubleValue];
    currentCustCoord.latitude=[selectedCust.Cust_Long doubleValue];
    NSLog(@"cust coordinates are %@ %@", selectedCust.Cust_Lat,selectedCust.Cust_Long);
    
    
    [self setLocationMapViewWithCoordinates:currentCustCoord];
    
    NSLog(@"test");
    //fetch order history and price list here
    
    
     
     //now fetch order history for this customer
     customerOrderHistoryArray=[[NSMutableArray alloc]init];
     selectedCust.customerOrderHistoryArray=[[NSMutableArray alloc]init];
    
    selectedCustomer.customerOrderHistoryArray=[[NSMutableArray alloc]init];
    
     customerOrderHistoryArray=[[[SWDatabaseManager retrieveManager] dbGetRecentOrdersForCustomer:selectedCust] mutableCopy];
    
    
  //  NSLog(@"customer order history is %@", customerOrderHistoryArray);
    
    
     
     selectedCust.totalOrderAmount=0;
    
    selectedCustomer.totalOrderAmount=0.0;
    
//     if (customerOrderHistoryArray.count>0) {
//     
//     for (NSMutableDictionary   * currentCustOrderHistoryDict in customerOrderHistoryArray) {
//     CustomerOrderHistory * currentCustomerOrderHistory=[[CustomerOrderHistory alloc] init];
//     currentCustomerOrderHistory.Creation_Date=[currentCustOrderHistoryDict valueForKey:@"Creation_Date"];
//     currentCustomerOrderHistory.Customer_Name=[currentCustOrderHistoryDict valueForKey:@"Customer_Name"];
//         currentCustomerOrderHistory.Shipping_Instructions=[currentCustOrderHistoryDict valueForKey:@"Shipping_Instructions"];
//     currentCustomerOrderHistory.ERP_Status=[currentCustOrderHistoryDict valueForKey:@"ERP_Status"];
//     currentCustomerOrderHistory.FSR=[currentCustOrderHistoryDict valueForKey:@"FSR"];
//         currentCustomerOrderHistory.ERP_Ref_Number=[currentCustOrderHistoryDict valueForKey:@"ERP_Ref_No"];
//     currentCustomerOrderHistory.Orig_Sys_Document_Ref=[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"];
//     currentCustomerOrderHistory.Transaction_Amt=[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"];
//         currentCustomerOrderHistory.End_Time=[currentCustOrderHistoryDict valueForKey:@"End_Time"];
//
//         
//         double currentOrderTotal=[[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"] doubleValue];
//        // NSLog(@"current tot temp %0.2f",currentOrderTotal);
//
//         
//     selectedCustomer.totalOrderAmount=selectedCust.totalOrderAmount+currentOrderTotal;
//         
//     
//     [selectedCustomer.customerOrderHistoryArray addObject:currentCustomerOrderHistory];
//     }
//     
//     }
    
    @try {
        selectedCustomer.totalOrderAmount=[[NSString stringWithFormat:@"%@",[[customerOrderHistoryArray valueForKey:@"Transaction_Amt"] valueForKeyPath:@"@sum.self"]] doubleValue];
    }@catch (id e) {
            // Ignored
        NSLog(@"Exception transaction amout sum is %@",e);
        selectedCustomer.totalOrderAmount=0;
    }
    
    selectedCustomer.customerOrderHistoryArray=customerOrderHistoryArray;
    NSLog(@"check order history array count %lu", (unsigned long)selectedCustomer.customerOrderHistoryArray.count);
     
    

    [self updateCustomerLastVisitDetails];

   
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==brandCategoryTextField)
    {
        [self categoryTextFieldTapped];
        return NO;
    }
    return YES;
}

#pragma  mark Category textfield tapped method

-(void)categoryTextFieldTapped
{
    
    NSMutableArray * brandsArray=[[[SWDatabaseManager retrieveManager]fetchBrandsforCustomer:selectedCustomer] mutableCopy];
    NSLog(@"brands are %@", brandsArray);
    if (brandsArray.count>0) {
        selectedCustomer.brandsArray=[[NSMutableArray alloc]init];
        for (NSMutableDictionary * currentCategory in brandsArray) {
            
            NSString* currentBrand=[currentCategory valueForKey:@"Classification_1"];
            [selectedCustomer.brandsArray addObject:currentBrand];
            
        }
        NSLog(@"categories for selected customer %@", selectedCustomer.brandsArray);
        popOverController=nil;
        brandCategoryTextField.enabled=YES;
        popString=@"BrandCategory";
        locationPopOver = nil;
        locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:200];
        
        locationPopOver.colorNames = selectedCustomer.brandsArray;
        
        CGRect relativeFrame = [brandCategoryTextField convertRect:brandCategoryTextField.bounds toView:self.view];
        
        locationPopOver.delegate = self;
        if (popOverController == nil) {
            popOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
            [popOverController presentPopoverFromRect:brandCategoryTextField.dropdownImageView.frame inView:brandCategoryTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            popOverController.delegate=self;
        }
        else {
            //The color picker popover is showing. Hide it.
            [popOverController dismissPopoverAnimated:YES];
            popOverController = nil;
            popOverController.delegate=nil;
        }
    }
    else
    {
        NSString *msg;
        if ([appControl.FSR_TARGET_TYPE isEqualToString:@"B"] ) {
            msg = @"No Brand available";
        } else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"C"]) {
            msg = @"No Category available";
        }
        
//        else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"N"]) {
//            msg = @"No Agency available";
//        }
        else {
            msg = @"No Brand/Category available";
        }
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:NSLocalizedString(msg, nil) withController:self];
    }
}

#pragma mark Popover selection delegate

-(void)fetchLatestOrderHistory
{
    selectedCustomer.customerOrderHistoryArray=[[NSMutableArray alloc]init];
    customerOrderHistoryArray=[[[SWDatabaseManager retrieveManager] dbGetRecentOrdersForCustomer:selectedCustomer] mutableCopy];
    selectedCustomer.totalOrderAmount=0.0;
    
//    if (customerOrderHistoryArray.count>0) {
//        
//        for (NSMutableDictionary   * currentCustOrderHistoryDict in customerOrderHistoryArray) {
//            CustomerOrderHistory * currentCustomerOrderHistory=[[CustomerOrderHistory alloc] init];
//            currentCustomerOrderHistory.Creation_Date=[currentCustOrderHistoryDict valueForKey:@"Creation_Date"];
//            currentCustomerOrderHistory.Customer_Name=[currentCustOrderHistoryDict valueForKey:@"Customer_Name"];
//            currentCustomerOrderHistory.Shipping_Instructions=[currentCustOrderHistoryDict valueForKey:@"Shipping_Instructions"];
//            currentCustomerOrderHistory.ERP_Status=[currentCustOrderHistoryDict valueForKey:@"ERP_Status"];
//            currentCustomerOrderHistory.FSR=[currentCustOrderHistoryDict valueForKey:@"FSR"];
//            currentCustomerOrderHistory.Orig_Sys_Document_Ref=[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"];
//            currentCustomerOrderHistory.Transaction_Amt=[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"];
//            currentCustomerOrderHistory.End_Time=[currentCustOrderHistoryDict valueForKey:@"End_Time"];
//            currentCustomerOrderHistory.ERP_Ref_Number=[currentCustOrderHistoryDict valueForKey:@"ERP_Ref_No"];
//
//            
//            double currentOrderTotal=[[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"] doubleValue];
//            // NSLog(@"current tot temp %0.2f",currentOrderTotal);
//            
//            
//            selectedCustomer.totalOrderAmount=selectedCustomer.totalOrderAmount+currentOrderTotal;
//            
//            
//            [selectedCustomer.customerOrderHistoryArray addObject:currentCustomerOrderHistory];
//        }
//        
//    }
    
    @try {
        selectedCustomer.totalOrderAmount=[[NSString stringWithFormat:@"%@",[[customerOrderHistoryArray valueForKey:@"Transaction_Amt"] valueForKeyPath:@"@sum.self"]] doubleValue];
    }@catch (id e) {
        // Ignored
        NSLog(@"Exception transaction amout sum is %@",e);
        selectedCustomer.totalOrderAmount=0;
    }
    
    selectedCustomer.customerOrderHistoryArray=customerOrderHistoryArray;

    NSLog(@"check order history array count %lu", (unsigned long)selectedCustomer.customerOrderHistoryArray.count);
    
    //add price list now
    


    [self loadOrderHistoryDetails];
    
    

}

-(void)selectFirstCategory
{
    [_slicesForCustomerPotential removeAllObjects];
    [_pieChartForCustomerPotential reloadData];
    NSMutableArray* targetArray=[[NSMutableArray alloc]init];
    if([appControl.FSR_TARGET_PRIMARY isEqualToString:@"P"])/*Product*/{
        
        /** if fsr target type is "P" then showing target info with customer selected area.*/
        
    }else {
        NSMutableArray * brandsArray=[[[SWDatabaseManager retrieveManager]fetchBrandsforCustomer:selectedCustomer] mutableCopy];
        NSLog(@"brands are %@", brandsArray);
        if (brandsArray.count>0) {
            selectedCustomer.brandsArray=[[NSMutableArray alloc]init];
            for (NSMutableDictionary * currentCategory in brandsArray) {
                NSString* currentBrand=[currentCategory valueForKey:@"Classification_1"];
                [selectedCustomer.brandsArray addObject:currentBrand];
            }
        }
        
        if (selectedCustomer.brandsArray.count>0) {
            NSString*selectedBrand=[selectedCustomer.brandsArray objectAtIndex:0];
            NSLog(@"selected brand is %@", selectedBrand);
            brandCategoryTextField.text=[NSString stringWithFormat:@"%@",[selectedCustomer.brandsArray objectAtIndex:0]];
            selectedCustomer.selectedBrand=[[SelectedBrand alloc]init];
            selectedCustomer.selectedBrand.brandCode=[selectedCustomer.brandsArray objectAtIndex:0]
            ;
            NSLog(@"check data %@", selectedCustomer.selectedBrand.brandCode);
            
        }
    }
    targetArray=[[SWDatabaseManager retrieveManager]fetchTargetInformationForCustomer:selectedCustomer];

    if(targetArray.count>0 ){
        
        [NoTargetDataImageView setHidden:YES];
        
        double targetVal=[[[targetArray objectAtIndex:0] valueForKey:@"Target_Value"] doubleValue];
        double salesVal=[[[targetArray objectAtIndex:0] valueForKey:@"Sales_Value"] doubleValue];
        double btgVal=[[[targetArray objectAtIndex:0] valueForKey:@"Balance_To_Go"] doubleValue];

//        if (targetVal < btgVal) {
//            btgVal = targetVal;
//        }
      //  double salesVal = targetVal-btgVal;
        
        [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:salesVal]];
        [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:btgVal]];
        [_pieChartForCustomerPotential reloadData];

        
        if(salesVal==0 && btgVal==0){
            [NoTargetDataImageView setHidden:NO];
        }
        
        if([appControl.FSR_VALUE_TYPE isEqualToString:@"Q"]){
            customerTargetValueLbl.text=[NSString stringWithFormat:@"%0.0f", targetVal];
            customerSalesValueLbl.text=[NSString stringWithFormat:@"%0.0f", salesVal];
            customerBalanceToGoLbl.text=[NSString stringWithFormat:@"%0.0f", btgVal];
        }else{
            customerTargetValueLbl.text=[[NSString stringWithFormat:@"%0.2f", targetVal] currencyString];
            customerSalesValueLbl.text=[[NSString stringWithFormat:@"%0.2f", salesVal] currencyString];
            customerBalanceToGoLbl.text=[[NSString stringWithFormat:@"%0.2f", btgVal] currencyString];
        }
        
    }else{
        customerTargetValueLbl.text=@"N/A";
        customerSalesValueLbl.text=@"N/A";
        customerBalanceToGoLbl.text=@"N/A";
        [NoTargetDataImageView setHidden:NO];
    }

}
-(void)selectedStringValue:(NSIndexPath *)indexPath
{
    if ([popString isEqualToString:@"BrandCategory"]) {
        
        [_slicesForCustomerPotential removeAllObjects];
        [_pieChartForCustomerPotential reloadData];
        
        NSString*selectedBrand=[selectedCustomer.brandsArray objectAtIndex:indexPath.row];
        
        NSLog(@"selected brand is %@", selectedBrand);
        
        brandCategoryTextField.text=[NSString stringWithFormat:@"%@",[selectedCustomer.brandsArray objectAtIndex:indexPath.row]];
       
        selectedCustomer.selectedBrand=[[SelectedBrand alloc]init];
        
        
        selectedCustomer.selectedBrand.brandCode=[selectedCustomer.brandsArray objectAtIndex:indexPath.row]
        ;
        
        NSLog(@"check data %@", selectedCustomer.selectedBrand.brandCode);
        
        NSMutableArray* targetArray=[[SWDatabaseManager retrieveManager]fetchTargetInformationForCustomer:selectedCustomer];
   
        selectedCustomer.selectedBrand.Sales_Value=[[targetArray objectAtIndex:0] valueForKey:@"Sales_Value"];
            
        selectedCustomer.selectedBrand.Target_Value=[[targetArray objectAtIndex:0] valueForKey:@"Target_Value"];
        
        selectedCustomer.selectedBrand.Balance_To_Go=[[targetArray objectAtIndex:0] valueForKey:@"Balance_To_Go"];
        
        double targetVal=[selectedCustomer.selectedBrand.Target_Value doubleValue];
        
       double salesVal=[selectedCustomer.selectedBrand.Sales_Value doubleValue];
        
        double btgVal=[selectedCustomer.selectedBrand.Balance_To_Go doubleValue];
        
//        if (targetVal < btgVal) {
//            btgVal = targetVal;
//        }
      //  double salesVal = targetVal-btgVal;
        
        pieChartDataArray=[[NSMutableArray alloc]init];
        [_slicesForCustomerPotential removeAllObjects];
        
        [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:salesVal]];
        [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:btgVal]];
        [_pieChartForCustomerPotential reloadData];

        
        double tempTarget= (salesVal/targetVal)*100;
        
        NSLog(@"temp target  %0.2f", tempTarget);
        
        /*
        Let's say you have two numbers, 40 and 30.
        
        30/40*100 = 75.
        So 30 is 75% of 40.
        
        40/30*100 = 133.
        So 40 is 133% of 30.
        
        The percentage increase from 30 to 40 is:
        (40-30)/30 * 100 = 33%
        
        The percentage decrease from 40 to 30 is:
        (40-30)/40 * 100 = 25%.
        */
        
        
        CGRect relativeFrame = [customerTargetPieChart convertRect:customerTargetPieChart.bounds toView:self.view];
        
        [customerTargetPieChart removeFromSuperview];
        
        
        customerTargetPieChart = [[PCPieChart alloc] initWithFrame:relativeFrame] ;
        customerTargetPieChart.outline=NO;
        [customerTargetPieChart setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin];
        customerTargetPieChart.showArrow=NO;
        //customerTargetPieChart.percentageFont=[UIFont fontWithName:@"OpenSans" size:14.0];
        
        [customerTargetPieChart setDiameter:120];
        [customerTargetPieChart setSameColorLabel:YES];
      NSMutableArray *  components = [NSMutableArray array] ;
        PCPieComponent *component = [PCPieComponent pieComponentWithTitle:@"" value:tempTarget];
        PCPieComponent *component2 = [PCPieComponent pieComponentWithTitle:@"" value:100-tempTarget];
        
        [component2 setColour:PCColorRed];
        [component setColour:PCColorGreen];
        
        [components addObject:component2];
        [components addObject:component];
        [customerTargetPieChart setComponents:components];
        
        
        
        NSLog(@"pie chart being added ");
        
        
       // [self.view addSubview: customerTargetPieChart];
        
        
//        
//        [pieChartDataArray addObject:@"100"];
//
//        [pieChartDataArray addObject:@"84"];
//
//        //[pieChartDataArray addObject:[NSString stringWithFormat:@"%0.2f",tempTarget] ];
//
//        
//        NSLog(@"contents of pie chart are %@", pieChartDataArray);
//        
//        //oin some cases sales value in negative
//        if (salesVal>0 ) {
//            [customerTargetPieChart reloadData];
//
//        }
//        else
//        {
//            
//        }
        [NoTargetDataImageView setHidden:YES];

        if(salesVal==0 && btgVal==0){
            [NoTargetDataImageView setHidden:NO];
        }

        if([appControl.FSR_VALUE_TYPE isEqualToString:@"Q"]){
            customerTargetValueLbl.text=[NSString stringWithFormat:@"%0.2f", targetVal];
            customerSalesValueLbl.text=[NSString stringWithFormat:@"%0.2f", salesVal];
            customerBalanceToGoLbl.text=[NSString stringWithFormat:@"%0.2f", btgVal];
        }else{
            customerTargetValueLbl.text=[[NSString stringWithFormat:@"%0.2f", targetVal] currencyString];
            customerSalesValueLbl.text=[[NSString stringWithFormat:@"%0.2f", salesVal] currencyString];
            customerBalanceToGoLbl.text=[[NSString stringWithFormat:@"%0.2f", btgVal] currencyString];
        }
     
    }
    
    else
    {
        customerTargetValueLbl.text=@"N/A";
        customerSalesValueLbl.text=@"N/A";
        customerBalanceToGoLbl.text=@"N/A";
        [NoTargetDataImageView setHidden:NO];
    }
    
    if (popOverController)
    {
        [popOverController dismissPopoverAnimated:YES];
        popOverController = nil;
        popOverController.delegate=nil;
    }
    locationPopOver.delegate=nil;
    locationPopOver=nil;
   
    
    
}

#pragma mark Pie chart delegate methods

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;

    NSLog(@"number of slices called %@",self.slicesForCustomerPotential);
    
    if (pieChart == self.pieChartForCustomerPotential && self.slicesForCustomerPotential.count>0) {
        count = self.slicesForCustomerPotential.count;
    }
    else
    {
    }
    
    return count;

}


- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;

    if (pieChart == self.pieChartForCustomerPotential) {
        count = [[NSString stringWithFormat:@"%@",[self.slicesForCustomerPotential objectAtIndex:index]]floatValue];
    }
    else
    {
    }
    return count;

    
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == self.pieChartForCustomerPotential) {
        colour = [self.sliceColorsForCustomerPotential objectAtIndex:(index % self.sliceColorsForCustomerPotential.count)];
    }
    return colour;
}

#pragma mark - XYPieChart Delegate
//- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"will select slice at index %d",index);
//}
//- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"will deselect slice at index %d",index);
//}
//- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"did deselect slice at index %d",index);
//}
//- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
//{
//    NSLog(@"did select slice at index %d",index);
//   // self.selectedSliceLabel.text = [NSString stringWithFormat:@"$%@",[self.slices objectAtIndex:index]];
//}

#pragma mark Search Bar Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self closeSplitView];
    [searchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
    }
    else
    {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        
        isSearching = YES;
        [self searchContent];
    }
    else {
        
        [self hideNoSelectionView];
        
        isSearching = NO;
        if (customersArray.count>0) {
            NSLog(@"reloading in search did change");

            [customersTableView reloadData];
            [customersTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                         animated:YES
                                   scrollPosition:UITableViewScrollPositionNone];
            NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            [customersTableView.delegate tableView:customersTableView didSelectRowAtIndexPath:firstIndexPath];
        }
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [customersSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    [self hideNoSelectionView];
    
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (customersArray.count>0) {
        NSLog(@"reloading in search cancel");

        [customersTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
    
  }


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with search term in customers %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(void)searchContent
{
    NSString *searchString = self.customersSearchBar.text;
    NSLog(@"search text in searchContent function %@",searchString);
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Customer_Name", searchString];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:filter, @"Customer_No", searchString];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2]];
    filteredCustomers=[[customersArray filteredArrayUsingPredicate:predicate] mutableCopy];
    NSLog(@"filtered customers count %d", filteredCustomers.count);
      if (filteredCustomers.count>0) {
          NSLog(@"reloading in search content with search results %@ \n %@",[SWDefaults getValidStringValue:[filteredCustomers valueForKey:@"Customer_Name"]],[SWDefaults getValidStringValue:[filteredCustomers valueForKey:@"Customer_No"]]);

        [customersTableView reloadData];
//        [customersTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
//                                     animated:YES
//                               scrollPosition:UITableViewScrollPositionNone];
//        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//        [customersTableView.delegate tableView:customersTableView didSelectRowAtIndexPath:firstIndexPath];
        
    }
    else if (filteredCustomers.count==0)
    {
        isSearching=YES;
        NSLog(@"reloading in search content2");

        [customersTableView reloadData];
//        [self resetCustomerDetails];
    
    }
    
}
-(void)resetCustomerDetails
{
    detailsCustomerCodeLbl.text=@"N/A";
    detailsCustomerClassLbl.text=@"N/A";
    detailsCustomerNameLbl.text=@"N/A";
    customeraddressLbl.text=@"N/A";
    customerCityLbl.text=@"N/A";
    //customerPoBoxLbl.text=@"N/A";
    customerCreditLimitLbl.text=@"N/A";
    customerAvailableBalanceLbl.text=@"N/A";
    customerTotalOutStandingLbl.text=@"N/A";
    customerOverDueLbl.text=@"N/A";
    customerTargetValueLbl.text=@"N/A";
    customerSalesValueLbl.text=@"N/A";
    customerBalanceToGoLbl.text=@"N/A";
    
    selectedCustomer=nil;
    [self DisableCustomerOptions];
}

#pragma map did select method


-(void)setLocationMapViewWithCoordinates:(CLLocationCoordinate2D)coordinates
{
    
//    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (coordinates
//                                                                          , 800, 800);
//    [customerLocationMapView setRegion:regionCustom animated:NO];
//   
//    [customerLocationMapView removeAnnotations:[customerLocationMapView annotations]];
// 
//    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
//    Pin.coordinate = coordinates;
//
//    [customerLocationMapView addAnnotation:Pin];
    
 }

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view

{
    
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
     NSString* custLatValue;
    NSString* custLongValue;
    if (selectedCustomer) {
        custLatValue =[NSString stringWithFormat:@"%@",selectedCustomer.Cust_Lat];
        custLongValue=[NSString stringWithFormat:@"%@",selectedCustomer.Cust_Long];
    }
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        NSString* googleMapsUrlString;
        
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication]openURL:googleMapsUrl];
            
        }
    }
    
    else
    {
        
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
        
        else
        {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
        
    }
    
}

#pragma mark Filter Button methods

- (IBAction)priceListFilterButtonTapped:(id)sender
{
    if (customersArray.count>0) {
    
    SWCustomerPriceListFilterViewController * popOverVC=[[SWCustomerPriceListFilterViewController alloc]init];
    popOverVC.delegate=self;
        
    UIButton* searchBtn=(id)sender;
    if (previousFilteredParameters.count>0) {
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
    }
    popOverVC.selectedCustomer=selectedCustomer;
    popOverVC.filterNavController=self.navigationController;
    popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    
    filterPopOverController=nil;
    filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    filterPopOverController.delegate=self;
    popOverVC.filterPopOverController=filterPopOverController;
    [filterPopOverController setPopoverContentSize:CGSizeMake(366, 430) animated:YES];
    CGRect btnFrame=searchBtn.frame;
    popOverVC.previousFilterParametersDict=previousPriceListFilterParameters;
    btnFrame.origin.x=btnFrame.origin.x+3;
    CGRect currentFrame=CGRectMake(695, 8, 32, 32);
    
    
    
    CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];
    
    [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Data", nil) message:NSLocalizedString(@"Please try later", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [unavailableAlert show];
    }
}


- (IBAction)orderHistoryBackButtontapped:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        orderHistoryDetailsView.frame = CGRectMake(2024, 8, 734, 502);
    } completion:^(BOOL finished) {
        orderHistoryDetailsView.hidden=YES;

    }];
}

- (IBAction)customersFilterButtonTapped:(id)sender {
    [self closeSplitView];

    NSLog(@"unfiltered data in %@", unfilteredCustomersArray);
    isSearching=NO;
    [self hideNoSelectionView];
    
    if (customersArray.count>0) {
    [customersSearchBar setShowsCancelButton:NO animated:YES];

    
        customersSearchBar.text=@"";
        [customersSearchBar resignFirstResponder];
        [self.view endEditing:YES];
        
        NSLog(@"reloading in customersFilterButtonTapped");

        [customersTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
//    NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//    if (indexPathArray.count>0) {
//        [indexPathArray replaceObjectAtIndex:0 withObject:firstIndexPath];
//        
//    }
        
        

    /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
    
    // Blurred with UIImage+ImageEffects
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    //blurredBgImage.image = [self blurWithImageEffects:[self takeSnapshotOfView:[self view]]];
    
    [self.view addSubview:blurredBgImage];
    
    UIButton* searchBtn=(id)sender;
    
    SWCustomersFilterViewController * popOverVC=[[SWCustomersFilterViewController alloc]init];
    popOverVC.delegate=self;
    
    if (previousFilteredParameters.count>0) {
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
    }
    popOverVC.customersArray=unfilteredCustomersArray;
    popOverVC.filterNavController=self.navigationController;
    popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    
    filterPopOverController=nil;
    filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    filterPopOverController.delegate=self;
    popOverVC.filterPopOverController=filterPopOverController;
    [filterPopOverController setPopoverContentSize:CGSizeMake(366, 510) animated:YES];
    CGRect btnFrame=searchBtn.frame;
    popOverVC.previousFilterParametersDict=previousFilterParameters;
        
        CGRect relativeFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];

    btnFrame.origin.x=btnFrame.origin.x+3;
    CGRect currentFrame=CGRectMake(222, 20, 30, 20);

    [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    

    
    else{
        
        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Data", nil) message:NSLocalizedString(@"Please try later", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [unavailableAlert show];
    }
}

#pragma  mark Filter Delegate Methods

-(void)customerFilterDidClose
{
    [blurredBgImage removeFromSuperview];
}

-(void)filteredCustomers:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    customersArray=filteredArray;
    NSLog(@"reloading in filteredCustomers");
    [customersTableView reloadData];
    NSLog(@"customers array count in filter %d",customersArray.count);
   [self deselectPreviousCellandSelectFirstCell];
}
-(void)filteredCustomerParameters:(NSMutableDictionary*)parametersDict
{
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    previousFilterParameters=parametersDict;
}


-(void)customerFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    [customersFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    customersArray=unfilteredCustomersArray;
    previousFilterParameters=[[NSMutableDictionary alloc]init];
    NSLog(@"reloading in customerFilterdidReset");

    [customersTableView reloadData];
    [self deselectPreviousCellandSelectFirstCell];
}


#pragma mark Price List FIlter Delegate Methods

-(void)filteredPriceList:(id)priceListdata
{
    filteredCustomerPriceListArray=priceListdata;
    if (filteredCustomerPriceListArray.count>0) {
        [priceListFilterButton setImage:[UIImage imageNamed:@"Customer_FilterFilled"] forState:UIControlStateNormal];
        
        [priceListTableView reloadData];

    }
}
-(void)filteredPriceListParameters:(id)priceListdata
{
    previousPriceListFilterParameters=[[NSMutableDictionary alloc]init];
    previousPriceListFilterParameters=priceListdata;
    
}

-(void)customerPriceListFilterDidReset
{
    previousPriceListFilterParameters=[[NSMutableDictionary alloc]init];
    filteredCustomerPriceListArray=selectedCustomer.customerPriceListArray;
    [priceListFilterButton setImage:[UIImage imageNamed:@"Customer_FilterEmpty"] forState:UIControlStateNormal];
    [priceListTableView reloadData];
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController ==popOverController) {
        
        return YES;
    }
    else
    {
    return NO;
    }
}

- (IBAction)orderHistoryLineitemsSegmenttapped:(id)sender {
    
    UISegmentedControl *lineItemsSegmentController=(id)sender;
    
    if (lineItemsSegmentController.selectedSegmentIndex==1) {
        
        NSLog(@"invoiced items tapped ");
        
        if (unfilteredInvoiceLineItems.count>0) {
            
            selectedCustomer.customerOrderHistoryLineItemsArray=unfilteredInvoiceLineItems;
            
           // [orderHistoryDescriptionTableView reloadData];
        }
        else
        {
            selectedCustomer.customerOrderHistoryLineItemsArray=[[NSMutableArray alloc]init];
            [orderHistoryDescriptionTableView reloadData];
        }
    }
    
    else if (lineItemsSegmentController.selectedSegmentIndex==0)
        
    {
        if (unfilteredOrderedLineItems.count>0) {
            
            selectedCustomer.customerOrderHistoryLineItemsArray=unfilteredOrderedLineItems;
           // [orderHistoryDescriptionTableView reloadData];

            
        }
    }
    
    
}

#pragma mark Beacon Methods
-(void)scanForiBeacon:(SalesWorxBeacon*)currentCustomerBeacon
{
    NSLog(@"scanning for iBeacon in customers");
    
    [[SalesWorxiBeaconManager retrieveSingleton]initilizeiBeaconManager:currentCustomerBeacon];
    
    NSLog(@"check detected beacon manager %@",[SalesWorxiBeaconManager retrieveSingleton].detectedBeacon );
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    segmentViewTopConstraint.constant = 8;
}
-(void)showNoSelectionView
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    segmentViewTopConstraint.constant = 1000;
}
#pragma mark customer last visit
-(void)updateCustomerLastVisitDetails{
   __block NSString *lastVisitedDateStr=[[NSString alloc] init];
    lastVisitedDateLabel.text=KNotApplicable;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        if(selectedCustomer!=nil && selectedCustomer.Customer_ID!=nil){
            lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:selectedCustomer];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            lastVisitedDateLabel.text=lastVisitedDateStr.length==0?KNotApplicable:lastVisitedDateStr;
        });
    });
    
    
}

#pragma mark target Information Segment
- (IBAction)targetInformationSegmentValueChanged:(id)sender {
    
    if (targetInformationSegment.selectedSegmentIndex == 0) {
        
        if([appControl.FSR_VALUE_TYPE isEqualToString:@"Q"]){
            salesValueLabel.text = @"Target Achieved";
            TargetValueTitleLable.text = @"Target Quantity";
        }else{
            salesValueLabel.text = @"Target Achieved";
            TargetValueTitleLable.text = @"Target Value";
        }
        
        lblCollectionTargetMonth.hidden = YES;
        brandCategoryTextField.hidden = NO;
        areaNameLabel.hidden = NO;
        [self setBrandTitleName];
        [self selectFirstCategory];
    }
    else if (targetInformationSegment.selectedSegmentIndex == 1)
    {
        areaNameLabel.hidden = YES;
        salesValueLabel.text = @"Collection Value";
        TargetValueTitleLable.text = @"Target Value";
        [_slicesForCustomerPotential removeAllObjects];
        [_pieChartForCustomerPotential reloadData];
        
        brandCategoryTitleLbl.text = NSLocalizedString(@"Target For", nil);
        lblCollectionTargetMonth.hidden = NO;
        brandCategoryTextField.hidden = YES;
        collectionTargetArrayForSelectedCustomer = [[SWDatabaseManager retrieveManager] fetchCollectionTarget:selectedCustomer.Customer_No];

        if (collectionTargetArrayForSelectedCustomer.count > 0) {
            [NoTargetDataImageView setHidden:YES];
            
            CustomerCollectionTarget *objCollectionTarget = [collectionTargetArrayForSelectedCustomer objectAtIndex:0];
            lblCollectionTargetMonth.text = [NSString stringWithFormat:@"%@-%@",objCollectionTarget.Target_Month, objCollectionTarget.Target_Year];
            
            double targetVal = [objCollectionTarget.Target_Value doubleValue];
            double salesVal = [objCollectionTarget.Achieved_Value doubleValue];
            double btgVal = [objCollectionTarget.Balance_ToGo doubleValue];
            
            [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:salesVal]];
            [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:btgVal]];
            [_pieChartForCustomerPotential reloadData];
            
            if(salesVal==0 && btgVal==0){
                [NoTargetDataImageView setHidden:NO];
            }
            
            customerTargetValueLbl.text=[[NSString stringWithFormat:@"%0.2f", targetVal] currencyString];
            customerSalesValueLbl.text=[[NSString stringWithFormat:@"%0.2f", salesVal] currencyString];
            customerBalanceToGoLbl.text=[[NSString stringWithFormat:@"%0.2f", btgVal] currencyString];
            
        } else {
            customerTargetValueLbl.text=@"N/A";
            customerSalesValueLbl.text=@"N/A";
            customerBalanceToGoLbl.text=@"N/A";
            [NoTargetDataImageView setHidden:NO];
        }
    }
}

- (IBAction)lastVisitDetailsButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    UIButton * tappedButton=sender;
    
    LastVisitDetailsViewController * popOverVC=[[LastVisitDetailsViewController alloc]init];
    popOverVC.selectedCustomer=selectedCustomer;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(380, 380);
    popover.delegate = self;
    popover.sourceView = tappedButton.superview;
    popover.sourceRect = tappedButton.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
    [self presentViewController:nav animated:YES completion:^{
        
    }];
    
}
@end
