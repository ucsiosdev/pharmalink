//
//  CustomPopOverViewController.m
//  MathMonsters
//
//  Created by Transferred on 1/12/13.
//  Copyright (c) 2013 Designated Nerd Software. All rights reserved.
//

#import "StringPopOverViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"

@implementation StringPopOverViewController
#pragma mark - Init
-(id)initWithStyle:(UITableViewStyle)style
{
    if ([super initWithStyle:style] != nil) {
        
        _colorNames = [NSMutableArray array];
        self.clearsSelectionOnViewWillAppear = NO;
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
            // Load resources for iOS 6.1 or earlier
        } else {
            self.preferredContentSize = CGSizeMake(300  , 700);
        }

    }
    
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_colorNames sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_colorNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.textLabel.font = Helvetica_Neue_Light_16;
        cell.textLabel.font = kSWX_FONT_SEMI_BOLD(36);

        cell.textLabel.textColor = [UIColor blackColor];//]UIColorFromRGB(0X5A6670);
        cell.selectionStyle=UITableViewCellAccessoryCheckmark;
    }
    
    // Configure the cell...
    cell.textLabel.text = [_colorNames objectAtIndex:indexPath.row];

    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedColorName = [_colorNames objectAtIndex:indexPath.row];
    
    
    
    //Notify the delegate if it exists.
    if (_delegate != nil) {
        [_delegate selectedStringValue:selectedColorName];
    }
}

@end
