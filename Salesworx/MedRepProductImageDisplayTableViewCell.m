//
//  MedRepProductImageDisplayTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepProductImageDisplayTableViewCell.h"

@implementation MedRepProductImageDisplayTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self.transform = CGAffineTransformMakeRotation(M_PI * 0.5);

}


@end
