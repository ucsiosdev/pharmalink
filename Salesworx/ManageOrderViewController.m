//
//  ManageOrderViewController.m
//  SWCustomer
//
//  Created by msaad on 12/26/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ManageOrderViewController.h"
#import "ProductOrderViewController.h"
#import "SalesOrderViewController.h"
#import "SalesOrderNewViewController.h"
//#import "Flurry.h"

#import "AlSeerSalesOrderViewController.h"

@interface ManageOrderViewController ()

@end

@implementation ManageOrderViewController


- (id)initWithCustomer:(NSDictionary *)c
{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        [self setTitle:NSLocalizedString(@"Manage Orders", nil)];
        self.view.backgroundColor = [UIColor whiteColor];
        CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60) andCustomer:customer] ;
        [headerView setShouldHaveMargins:YES];
        
        gridView=[[GridView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width , self.view.frame.size.height-60) ];
        [gridView setDataSource:self];
        [gridView setDelegate:self];
        [gridView setShouldAllowDeleting:YES];
        [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:gridView];
        [self.view addSubview:headerView];
        
        totalOrderArray = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getSalesOrderServiceDiddbGetPerformaOrder:[[SWDatabaseManager retrieveManager] dbGetPerformaOrder:[customer stringForKey:@"Ship_Customer_ID"] withSiteId:[customer stringForKey:@"Ship_Site_Use_ID"]]];
}
#pragma mark GridView DataSource
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return totalOrderArray.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return 30.0f;
            break;
        case 1:
            return 25.0f;
            break;
        case 2:
            return 25.0f;
            break;
        case 3:
            return 25.0f;
            break;
        default:
            break;
    }
    return 20.0f;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    switch (column) {
        case 0:
            title = NSLocalizedString(@"Reference No", nil);
            break;
        case 1:
            title = NSLocalizedString(@"Amount", nil);
            break;
        case 2:
            title = NSLocalizedString(@"Status", nil);
            break;
        case 3:
            title = NSLocalizedString(@"Date", nil);
            break;
        default:
            break;
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [totalOrderArray objectAtIndex:row];
    
    switch (column) {
        case 0:
            text = [data stringForKey:@"Orig_Sys_Document_Ref"];
            break;
        case 1:
            text = [data currencyStringForKey:@"Order_Amt"];
            break;
        case 2:
            if([data objectForKey:@"Schedule_Ship_Date"])
            {
                text = @"Confirmed";
            }
            else
            {
                text = @"Unconfirmed";
            }
            break;
        case 3:
            text = [data stringForKey:@"Creation_Date"];
            break;
        default:
            break;
    }
    
    return text;
}

#pragma mark GridView Delegate
- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex
{
    Singleton *single = [Singleton retrieveSingleton];
    single.orderStartDate = [NSDate date];
    NSString *orderString = @"";
    if([[totalOrderArray objectAtIndex:rowIndex] objectForKey:@"Schedule_Ship_Date"])
    {
        orderString = @"confirmed";
    }
    else
    {
        orderString = @"notconfirmed";
    }
    
    NSLog(@"order array in manage orders %@", totalOrderArray);
    
    NSString *savedCategoryID = [[totalOrderArray objectAtIndex:rowIndex] stringForKey:@"Custom_Attribute_1"];
    [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer] andCategoryName:savedCategoryID andOrderDictionary:[totalOrderArray objectAtIndex:rowIndex] andOrderString:orderString];
}
- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict andOrderString:(NSString *)orderString
{
    //categoryArray=categories;
    
    NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:categories];
   // NSMutableDictionary *category= [NSMutableDictionary new];
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID]) {
             //NSMutableDictionary *category=[row mutableCopy];
            NSLog(@"setProductCategory 2");
            [SWDefaults setProductCategory:[row mutableCopy]];
            break;
        }
    }
    
    
    
//    if ([[SWDefaults checkTgtField]isEqualToString:@"Y"]) {
    if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {

        
        AlSeerSalesOrderViewController *newSalesOrder =[[AlSeerSalesOrderViewController alloc] initWithNibName:@"AlSeerSalesOrderViewController" bundle:nil];
        newSalesOrder.parentViewString = @"MO";
        newSalesOrder.preOrderedItems=[NSMutableArray new];
        
        
        //for unconfirmed orders we wont have ORig_Sys_Document ref so fetch the selected UOM for unconfirmed orders from TBL_Proforma orderd
        
        if ([orderString isEqualToString:@"notconfirmed"]) {
            
             NSString* fetchUomQry=[NSString stringWithFormat:@"select Order_Quantity_UOM from tbl_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",[dict stringForKey:@"Orig_Sys_Document_Ref"]];
            
            //we have value in this array ony if uom is selected handle the condition if uom is not selected and order is saved not confirmed
            NSMutableArray* orderUom=[[SWDatabaseManager retrieveManager] fetchDataForQuery:fetchUomQry];

            
    
            NSString* orderwithUOMSelected= [[orderUom valueForKey:@"Order_Quantity_UOM"] objectAtIndex:0];
            
            if (orderwithUOMSelected==nil || [orderwithUOMSelected isEqualToString:@""]||[orderwithUOMSelected isEqual:[NSNull null]])
            {
                
                
                //order qty uom is null select the display uom as default
                
                NSString* fetchUomQry=[NSString stringWithFormat:@"select Display_UOM from tbl_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",[dict stringForKey:@"Orig_Sys_Document_Ref"]];
                
                //we have value in this array ony if uom is selected handle the condition if uom is not selected and order is saved not confirmed
                NSMutableArray* orderwithoutUOM=[[SWDatabaseManager retrieveManager] fetchDataForQuery:fetchUomQry];
                
                
                
                NSString* orderwithUOMSelected= [[orderwithoutUOM valueForKey:@"Display_UOM"] objectAtIndex:0];

                [customer setValue:[[orderwithoutUOM valueForKey:@"Display_UOM"] objectAtIndex:0] forKey:@"Order_Quantity_UOM"];

                
                
            }
            
    
            
           else if (orderUom.count>0) {
                [customer setValue:[[orderUom valueForKey:@"Order_Quantity_UOM"] objectAtIndex:0] forKey:@"Order_Quantity_UOM"];


            }
        }
        
        else
        {
        
        
        
        
        
        
        //adding the selected uom to the dict for Manage orders
        
        NSString* fetchUomQry=[NSString stringWithFormat:@"select Order_Quantity_UOM from TBL_Order_History_Line_Items where Orig_Sys_Document_Ref='%@'",[dict stringForKey:@"Orig_Sys_Document_Ref"]];
        
        
        NSMutableArray* orderUom=[[SWDatabaseManager retrieveManager] fetchDataForQuery:fetchUomQry];
        
        if (orderUom.count>0) {
            
            
            
            [customer setValue:[orderUom valueForKey:@"Order_Quantity_UOM"]  forKey:@"Order_Quantity_UOM"];
            
            
        }
        }
        NSLog(@"check customer with uom data %@", [customer description]);
        
        
        
        
        newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];

        
        
        
        
        
        
        newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
        newSalesOrder.isOrderConfirmed=orderString;
        [self.navigationController pushViewController:newSalesOrder animated:YES];
        
        
        
        
        
        
        
        
        
        
        
        
//        SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
//        newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//        newSalesOrder.parentViewString = @"MO";
//        newSalesOrder.preOrderedItems=[NSMutableArray new];
//        newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
//        newSalesOrder.isOrderConfirmed=orderString;
//        [self.navigationController pushViewController:newSalesOrder animated:YES];
//

    }
    
    
    else
    {
    
        
        
        
        NSString* fetchUomQry=[NSString stringWithFormat:@"select * from from tbl_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",[dict stringForKey:@"Orig_Sys_Document_Ref"]];
        
        
        NSMutableArray* proformadata=[[SWDatabaseManager retrieveManager] fetchDataForQuery:fetchUomQry];
        
        NSLog(@"proforma data is %@", proformadata);
    
    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    newSalesOrder.parentViewString = @"MO";
    newSalesOrder.preOrderedItems=[NSMutableArray new];
    newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
        NSLog(@"PRE ORDERED ITEMS %@", newSalesOrder.preOrdered);
    newSalesOrder.isOrderConfirmed=orderString;
    [self.navigationController pushViewController:newSalesOrder animated:YES];
    }
}

- (void)gridView:(GridView *)gv commitDeleteRowAtIndex:(int)rowIndex
{
    NSDictionary *data = [totalOrderArray objectAtIndex:rowIndex];
    if(![data objectForKey:@"Schedule_Ship_Date"])
    {
        [[SWDatabaseManager retrieveManager] deleteManageOrderWithRef:[data stringForKey:@"Orig_Sys_Document_Ref"]];
        [totalOrderArray removeObjectAtIndex:rowIndex];
        [gv deleteRowAtIndex:rowIndex];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil) message:NSLocalizedString(@"Confirmed orders cannot be deleted", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
}

- (void)presentProductOrder:(NSDictionary *)product
{
    
   
}
- (void)getSalesOrderServiceDiddbGetPerformaOrder:(NSArray *)performaOrder
{
    performaOrderArray = [NSMutableArray arrayWithArray:performaOrder];
    //salesSer.delegate=self;
    
    [self getSalesOrderServiceDiddbGetConfirmOrder:[[SWDatabaseManager retrieveManager] dbGetConfirmOrder:[customer stringForKey:@"Ship_Customer_ID"] withSiteId:[customer stringForKey:@"Ship_Site_Use_ID"]]];
    performaOrder=nil;
}


- (void)getSalesOrderServiceDiddbGetConfirmOrder:(NSArray *)performaOrder
{
    confirmOrderArray = [NSMutableArray arrayWithArray:performaOrder];
    totalOrderArray = [[performaOrderArray arrayByAddingObjectsFromArray:confirmOrderArray] mutableCopy];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Creation_Date" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    totalOrderArray = [[totalOrderArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    [gridView reloadData];
    performaOrder=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
