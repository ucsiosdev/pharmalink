//
//  ManageOrderViewController.h
//  SWCustomer
//
//  Created by msaad on 12/26/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
#import "SWPlatform.h"
#import "SalesOrderViewController.h"
@interface ManageOrderViewController : SWViewController <UIAlertViewDelegate   , GridViewDataSource , GridViewDelegate>
{
    NSMutableDictionary *customer;
    //
    GridView *gridView;
    NSMutableArray *performaOrderArray;
    NSMutableArray *confirmOrderArray;
    NSMutableArray *totalOrderArray;
    SalesOrderViewController  *salesOrderViewController;
    
    
    
}


- (id)initWithCustomer:(NSDictionary *)customer;
@end