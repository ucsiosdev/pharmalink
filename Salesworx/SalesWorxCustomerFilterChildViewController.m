//
//  SalesWorxCustomerFilterChildViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxCustomerFilterChildViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
@interface SalesWorxCustomerFilterChildViewController ()

@end

@implementation SalesWorxCustomerFilterChildViewController
@synthesize customerListArray,searchElementName,titleString;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    filteredCustomerListArray=[customerListArray mutableCopy];
    tableRowValues=[[NSMutableArray alloc]init];
    filteredTableRowValues=[[NSMutableArray alloc]init];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:  kSWX_FONT_REGULAR(16), NSFontAttributeName, nil];
    CGSize requestedTitleSize = [titleString sizeWithAttributes:textTitleOptions];
    CGFloat titleWidth = MIN(self.view.frame.size.width, requestedTitleSize.width);
    CGRect frame = CGRectMake(0, 0, titleWidth, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = titleString;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(titleString, nil)];
    

    
    
    
 //   filteredTableRowValues=[[filteredCustomerListArray objectAtIndex:indexPath.row]valueForKey:@""];
    if([searchElementName isEqualToString:@"customerNumber"])
    {
        tableRowValues = [customerListArray valueForKeyPath:@"@distinctUnionOfObjects.customerNumber"];
        filteredTableRowValues=[tableRowValues mutableCopy];
    }
    else
    {
        tableRowValues = [customerListArray valueForKeyPath:@"@distinctUnionOfObjects.customer"];
        filteredTableRowValues=[tableRowValues mutableCopy];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return filteredTableRowValues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.font=MedRepTitleFont;
    cell.textLabel.textColor=MedRepTableViewCellTitleColor;
    
    cell.textLabel.text=[filteredTableRowValues objectAtIndex:indexPath.row];
    return cell;
}




#pragma mark Search DisplayController methods



- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    NSMutableArray *tempRowValues=[tableRowValues mutableCopy];
    filteredTableRowValues = [[tempRowValues filteredArrayUsingPredicate:resultPredicate] mutableCopy];
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length>0)
    {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF contains[cd] %@",
                                        searchBar.text];
        NSMutableArray *tempRowValues=[tableRowValues mutableCopy];
        filteredTableRowValues = [[tempRowValues filteredArrayUsingPredicate:resultPredicate] mutableCopy];

    }
    else
    {
        filteredTableRowValues=[tableRowValues mutableCopy];
    }
    [dataTableView reloadData];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;   // called when text changes (including clear)
{
    if(searchBar.text.length>0)
    {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF contains[cd] %@",
                                        searchBar.text];
        NSMutableArray *tempRowValues=[tableRowValues mutableCopy];
        filteredTableRowValues = [[tempRowValues filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        
    }
    else
    {
        filteredTableRowValues=[tableRowValues mutableCopy];
    }
    [dataTableView reloadData];

}
@end
