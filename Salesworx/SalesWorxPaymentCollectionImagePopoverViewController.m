//
//  SalesWorxPaymentCollectionImagePopoverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/27/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPaymentCollectionImagePopoverViewController.h"
#import "NSString+Additions.h"
@interface SalesWorxPaymentCollectionImagePopoverViewController ()

@end

@implementation SalesWorxPaymentCollectionImagePopoverViewController
@synthesize contentArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Images"];
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=closeButton;

    
    
    UIBarButtonItem* saveButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    [saveButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=saveButton;
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)saveTapped
{
    if ([self.delegate respondsToSelector:@selector(capturedImages:)]) {
        
        [self.delegate capturedImages:contentArray];
    }
    
   

    
    [self dismissViewControllerAnimated:YES completion:nil];

}
-(void)closeTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIBarButtonItem *btn = [[_popOverController valueForKey:@"_delegate"]valueForKey:@"leftBtn"];
    btn.enabled = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return contentArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SalesWorxPaymentCollectionImagePopOverTableViewCell* cell =[tableView dequeueReusableCellWithIdentifier:@"popOverCell"];
    
    if (cell==nil) {
       NSArray* nibArray=[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentCollectionImagePopOverTableViewCell" owner:self options:nil];
        cell=[nibArray objectAtIndex:0];
        
    }
    
    PaymentImage* currentImage=[contentArray objectAtIndex:indexPath.row];
    cell.titleLbl.text=currentImage.title;
    cell.transactionImageView.image=currentImage.image;

    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        PaymentImage *imgObj = [contentArray objectAtIndex:indexPath.row];
        imgObj.image = [UIImage imageNamed:@"SalesOrder_AddImage_CameraIcon"];
        imgObj.imageID = nil;
        [contentArray replaceObjectAtIndex:indexPath.row withObject:imgObj];
        [imagesTableView reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedImageObject=[contentArray objectAtIndex:indexPath.row];
    selectedIndex=indexPath.row;
    if ([NSString isEmpty:selectedImageObject.imageID]) {
        
    UIImagePickerController * imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = self;
    imagePickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
    imagePickerController.allowsEditing = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
    }
    else{
        [self showImageDetails:indexPath];
    }
}

-(void)showImageDetails:(NSIndexPath*)selectedIndexPath
{
    LPOImagesGalleryCollectionDetailViewController *imageDetailsVC = [[LPOImagesGalleryCollectionDetailViewController alloc]init];
    imageDetailsVC.delegate = self;
    NSMutableArray *imagesPathsArray=[[NSMutableArray alloc]init];
    
    if ([selectedImageObject.imageType isEqualToString:kReceiptImageType]) {
        [imagesPathsArray addObject:[[SWDefaults getCollectionReceiptImagesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",selectedImageObject.imageID]]];
    }
    else if ([selectedImageObject.imageType isEqualToString:kChequeImageType])
    {
        [imagesPathsArray addObject:[[SWDefaults getCollectionImagesFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@.jpg",selectedImageObject.imageID]]];
    }
    
    imageDetailsVC.swipImagesUrls=imagesPathsArray;
    imageDetailsVC.strTitle = @"Collection Images";
    imageDetailsVC.selectedImageIndex=[NSString stringWithFormat:@"%ld",(long)selectedIndexPath.row];
    imageDetailsVC.paymentImageObjectsArray=contentArray;
    imageDetailsVC.isPaymentImage=YES;
    
    UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:imageDetailsVC];
    mapPopUpViewController.navigationBarHidden = YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    imageDetailsVC.view.backgroundColor = KPopUpsBackGroundColor;
    imageDetailsVC.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}

-(void)updateImagesArray:(NSMutableArray*)imagesArray
{
  
    contentArray=imagesArray;
    [imagesTableView reloadData];
    
    
//    contentArray replaceObjectAtIndex:selectedIndex withObject:
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *capturedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        if (capturedImage != nil) {
            for (PaymentImage *imgObj in contentArray) {
                if ([imgObj.imageType isEqualToString:selectedImageObject.imageType]) {
                    imgObj.image=capturedImage;
                    imgObj.imageID=[NSString createGuid];
                    
                    if ([imgObj.imageType isEqualToString:kReceiptImageType]) {
                        [SWDefaults saveCollectionReceiptImage:imgObj.image withName:imgObj.imageID];
                    }
                    else if ([imgObj.imageType isEqualToString:kChequeImageType])
                    {
                        [SWDefaults saveCollectionImage:imgObj.image withName:imgObj.imageID];
                    }
                    
                }
            }
            [imagesTableView reloadData];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
