//
//  SalesWorxDatePickerPopOverPresentationViewController.h
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 2/6/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>




@protocol SalesWorxDatePickerPopOverPresentationViewControllerDelegate <NSObject>
-(void)datePicket:(UIDatePicker *)datePicker didSelectDate:(NSDate *)selectedDate;
@end


@interface SalesWorxDatePickerPopOverPresentationViewController : UIViewController<UIPickerViewDelegate>{
    IBOutlet UIDatePicker *salesWorxDatePicker;
}
@property(strong,nonatomic) id<SalesWorxDatePickerPopOverPresentationViewControllerDelegate> datePickerPopOverPresentationViewControllerDelegate;
@property(strong,nonatomic) NSString * titleString;
@property(nonatomic)        UIDatePickerMode  datePickerMode;
@property(strong,nonatomic) NSDate   * previousSelectedDate;
@property(strong,nonatomic) NSDate   * datePickerMaximumDate;
@property(strong,nonatomic) NSDate   * datePickerMinimumDate;

@end
