//
//  SWSessionManager.m
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWSessionManager.h"
#import "SWSessionViewController.h"
#import "SWSessionConstants.h"
#import "LicensingViewController.h"
#import "LoginViewController.h"
#import "SalesworxLicenseActivationViewController.h"
@interface SWSessionManager ()
- (void)login;
@end

@implementation SWSessionManager

@synthesize window;
@synthesize applicationViewController;

- (id)initWithWindow:(UIWindow *)w andApplicationViewController:(UIViewController *)avc {
    self = [super init];
    
    if (self)
    {
        //[self setWindow:w];
       // self.window = [[UIWindow alloc] init] ;
        self.window= w;

        //self.applicationViewController = [[UIViewController alloc] init] ;
        self.applicationViewController = avc;
        //[self setApplicationViewController:avc];
        
        [self.window setBackgroundColor:[UIColor whiteColor]];
        [self.window makeKeyAndVisible];
        
        [self login];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDone:) name:kSessionLoginDoneNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutDone:) name:kSessionLogoutNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(licenseError:) name:kSessionActivationErrorNotification object:nil];
    }
    
    return self;
}

- (void)login
{
  //  SWSessionViewController *sessionViewController = [[SWSessionViewController alloc] init] ;
    LoginViewController *loginVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
    [self.window setRootViewController:navigationController];
   // sessionViewController=nil;
}

- (void)LicenseView
{
    
//    SalesworxLicenseActivationViewController * sessionViewController=[[SalesworxLicenseActivationViewController alloc]init];
    
    
    LicensingViewController *sessionViewController = [[LicensingViewController alloc] init] ;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:sessionViewController] ;
    [self.window setRootViewController:navigationController];
    sessionViewController=nil;
}



#pragma mark Notifications
- (void)loginDone:(NSNotification *)notification
{
    NSLog(@"login done notification called");
    
    
    [[[UIApplication sharedApplication]keyWindow]setRootViewController:self.applicationViewController];

    //[self.window setRootViewController:self.applicationViewController];
}

- (void)logoutDone:(NSNotification *)notification   
{
    [SWDefaults setUsername:nil];
    [SWDefaults setPassword:nil];
    
    NSLog(@"log out done notification ");
    
    
    [self login];
}

- (void)licenseError:(NSNotification *)notification
{
    [SWDefaults setLicenseKey:nil];
    [SWDefaults setLicenseCustomerID:nil];
    [self LicenseView];
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
