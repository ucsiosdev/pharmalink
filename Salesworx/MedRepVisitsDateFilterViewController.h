//
//  MedRepVisitsDateFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/16/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VisitDatePickerDelegate <NSObject>


-(void)selectedVisitDateDelegate:(NSString*)selectedDate;

@end

@interface MedRepVisitsDateFilterViewController : UIViewController

{
    id visitDateDelegate;
}
@property (strong, nonatomic) IBOutlet UIDatePicker *visitsDatePicker;

@property(strong,nonatomic) id visitDateDelegate;

@property(strong,nonatomic) NSString* titleString;

@property(strong,nonatomic) NSString* datePickerMode;

@property(strong,nonatomic) NSString * previouslySelectedDate;

@property(nonatomic) BOOL isVisitFilter;

@property(nonatomic) BOOL isBrandAmbassadorVisit;



@end
