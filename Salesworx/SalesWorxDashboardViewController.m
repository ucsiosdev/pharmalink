//
//  SalesWorxDashboardViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/25/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDashboardViewController.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepMenuViewController.h"
#import "TaskPopoverViewController.h"
#import "MedRepVisitsListViewController.h"

#define kMovetKey @"moveViewAnimation"

@interface SalesWorxDashboardViewController ()
{
    UIViewController *sortViewController;
    UILabel *alle;
    UIButton *notificationButton;
}
@end

@implementation SalesWorxDashboardViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"FM dashboard called");
    
    // Do any additional setup after loading the view from its nib.
    appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.view.backgroundColor=UIViewBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Field Marketing Dashboard", nil)];
    
    appControl = [AppControl retrieveSingleton];
}
    
    
-(void)viewWillAppear:(BOOL)animated
{
    [self.view layoutIfNeeded];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        } else {
        }
    }
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingDashboard];
    }
 
    // get Current month
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.locale=[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];;
    df.dateFormat=@"yyyy-MM";
    monthString = [[df stringFromDate:[NSDate date]] capitalizedString];
    
    [self performSelectorOnMainThread:@selector(loadAllChart) withObject:nil waitUntilDone:YES];//:@selector(loadAllChart) withObject:nil];
    
    
    NSString* currentDateinDisplayFormat = [MedRepQueries fetchDatabaseDateFormat];
    NSMutableArray *VisitsArray = [MedRepQueries fetchPlannedVisitsforSelectedDate:currentDateinDisplayFormat];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    NSDate *currentDate = [dateFormatter dateFromString:dateString];
    
    @try {
        NSMutableArray *plannedVisit = [[NSMutableArray alloc]init];
        for (int i = 0; i<[VisitsArray count]; i++)
        {
            NSString *dateStr = [[VisitsArray objectAtIndex:i] objectForKey:@"Visit_Date"];
            NSDate *date = [dateFormatter dateFromString:dateStr];
            
            if (![[[VisitsArray objectAtIndex:i]valueForKey:@"Visit_Status"] isEqualToString:@"Y"] && ([date compare:currentDate] == NSOrderedDescending)) {
                [plannedVisit addObject:[VisitsArray objectAtIndex:i]];
            }
        }
        if ([plannedVisit count] == 0)
        {
            self.viewUpcomingAppointment.hidden = YES;
        } else
        {
            
            NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"Visit_Date" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
            sortedPlannedVisitArray = [plannedVisit sortedArrayUsingDescriptors:sortDescriptors];
            
            if ([[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Doctor_Name"] isEqualToString:@"N/A"])
            {
                _lblDoctorName.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Pharmacy_Contact_Name"];
            }
            else
            {
                _lblDoctorName.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Doctor_Name"];
            }
            _lblLocation.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Location_Name"];
            _lblTelephoneNo.text = [[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Phone"];
            if (_lblTelephoneNo.text.length == 0) {
                _lblTelephoneNo.text = @"--";
            }
            
            _lblTime.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"hh:mm" scrString:[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Visit_Date"]];
            
            _lblATime.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"a" scrString:[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Visit_Date"]];
            
            _lblDate.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"dd MMMM" scrString:[[sortedPlannedVisitArray objectAtIndex:0]valueForKey:@"Visit_Date"]];
        }
    }
    @catch (NSException *exception) {
        
    }

    [self loadTaskData];
    
    btnTask = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"TaskIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnTask)];
    [btnTask setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = btnTask;
}

- (void)loadTaskData {
    
    if (_viewTasks.layer.sublayers.count>0) {
        
        [_viewTasks.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    }
    
    // Load Tasks data of Doctors and Pharmacies
    NSMutableArray *arrTasks = [MedRepQueries fetchTotalTasksForDoctorAndLocation];
    int tasksOpen = 0;
    int tasksCompleted = 0;
    
    for (int i = 0; i<[arrTasks count]; i++)
    {
        NSString * task = [[arrTasks objectAtIndex:i]valueForKey:@"Status"];
        
        if ([task isEqualToString:@"Closed"])
        {
            tasksCompleted++;
        }
        else if ([task isEqualToString:@"New"] || [task isEqualToString:@"Deffered"])
        {
            tasksOpen++;
        }
        
    }
    _lblTasksOpen.text = [NSString stringWithFormat:@"%d",tasksOpen];
    _lblTasksCompleted.text = [NSString stringWithFormat:@"%d",tasksCompleted];
    
    if (tasksOpen+tasksCompleted == 0 && tasksCompleted > 0)
    {
        [self loadPercentageChart:circleChartForTasks :_viewTasks :@100 :@100 :[UIColor colorWithRed:(38.0/255.0) green:(196.0/255.0) blue:(165.0/255.0) alpha:1]];
    }else
    {
        [self loadPercentageChart:circleChartForTasks :_viewTasks :[NSNumber numberWithInt:(tasksOpen+tasksCompleted)]  :[NSNumber numberWithInt:tasksCompleted] :[UIColor colorWithRed:(38.0/255.0) green:(196.0/255.0) blue:(165.0/255.0) alpha:1]];
    }
}
- (void)btnTask{
    TaskPopoverViewController *presentingView = [[TaskPopoverViewController alloc]init];
    presentingView.TaskPopoverDelegate = self;
    UINavigationController *toDoPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    toDoPopUpViewController.navigationBarHidden=YES;
    toDoPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    toDoPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:toDoPopUpViewController animated:NO completion:nil];
}

-(void)updateTaskData
{
    [self loadTaskData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadVisitFrequency];
    [self loadViewForDemonstratedProducts];
    [self loadViewForFaceTimeVsWaitTime];
    [self loadViewForProductiveVisits];
}

#pragma mark Load PercentageChart
-(void)loadAllChart
{
    // Load Percentage data of Doctor's Achievement
    NSNumber *doctorsCompletedVisitsCount=[MedRepQueries fetchCompletedDoctorsCallsInCurrentMonth];
    NSNumber *doctorsRequiredVisitsCount=[MedRepQueries fetchNumberOfVisitsRequiredForDoctorsInaMonth];
    _lblDoctorsVisitsRequired.text = [NSString stringWithFormat:@"%@",doctorsRequiredVisitsCount];
    _lblDoctorsVisitsCompleted.text = [NSString stringWithFormat:@"%@",doctorsCompletedVisitsCount];
    
    
    
    for(UIView *subview in [_viewDoctorsVisit subviews]) {
        if([subview isKindOfClass:[PNCircleChart class]]) {
            [subview removeFromSuperview];
        }
    }
    if (doctorsRequiredVisitsCount == 0 && doctorsCompletedVisitsCount > 0)
    {
        [self loadPercentageChart:circleChartForDoctors :_viewDoctorsVisit :@100 :@100 :[UIColor colorWithRed:(50.0/255.0) green:(156.0/255.0) blue:(212.0/255.0) alpha:1]];
    }else
    {
        [self loadPercentageChart:circleChartForDoctors :_viewDoctorsVisit :doctorsRequiredVisitsCount :doctorsCompletedVisitsCount :[UIColor colorWithRed:(50.0/255.0) green:(156.0/255.0) blue:(212.0/255.0) alpha:1]];
    }
    
    

    
    NSNumber *PharmaciesCompletedVisitsCount=[MedRepQueries fetchCompletedPharmaciesCallsInCurrentMonth];
    NSNumber *PharmaciesRequiredVisitsCount=[MedRepQueries fetchNumberOfVisitsRequiredForPharmaciesInaMonth];

    
    _lblPharmaciesVisitsRequired.text = [NSString stringWithFormat:@"%@",PharmaciesRequiredVisitsCount];
    _lblPharmaciesVisitsCompleted.text = [NSString stringWithFormat:@"%@",PharmaciesCompletedVisitsCount];
    
    for(UIView *subview in [_viewPharmaciesVisit subviews]) {
        if([subview isKindOfClass:[PNCircleChart class]]) {
            [subview removeFromSuperview];
        }
    }
    if (PharmaciesRequiredVisitsCount == 0 && PharmaciesCompletedVisitsCount > 0)
    {
        [self loadPercentageChart:circleChartForPhamacies :_viewPharmaciesVisit :@100 :@100 :[UIColor colorWithRed:(246.0/255.0) green:(182.0/255.0) blue:(72.0/255.0) alpha:1]];
    }else
    {
        /** visits Required for pharmacies always zero. So passing completed viists count to chart instead of required visits to show as 100%*/
        [self loadPercentageChart:circleChartForPhamacies :_viewPharmaciesVisit :PharmaciesCompletedVisitsCount :PharmaciesCompletedVisitsCount :[UIColor colorWithRed:(246.0/255.0) green:(182.0/255.0) blue:(72.0/255.0) alpha:1]];
    }
}
-(void)loadPercentageChart:(PNCircleChart *)circleChart :(UIView *)view :(NSNumber *)totalPercentage :(NSNumber *)percentage :(UIColor *)gradientColor
{
    circleChart = [[PNCircleChart alloc] initWithFrame:CGRectMake(52, 0, 160, 160) total:totalPercentage current:percentage clockwise:YES shadow:NO shadowColor:[UIColor colorWithRed:(232.0f/255.0f) green:(243.0f/255.0f) blue:(246.0f/255.0f) alpha:1] check:NO lineWidth:@8.0f];
    
    circleChart.backgroundColor = [UIColor clearColor];
    [circleChart setStrokeColor:[UIColor clearColor]];
    [circleChart setStrokeColorGradientStart:gradientColor];
    [circleChart strokeChart];
    
    [view addSubview:circleChart];
}


#pragma mark

-(NSMutableArray*)fetchContactIDWithLocationID:(NSString *)contactName
{
    NSString* locationsQuery=[NSString stringWithFormat:@"select Contact_ID from PHX_TBL_Contacts where Contact_Name = '%@'",contactName];
    
    NSMutableArray* locationsData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:locationsQuery];
    if (locationsData.count>0)
    {
        return locationsData;
    }
    else
    {
        return nil;
    }
}

#pragma mark Load Chart for Demonstrated Products
-(void)loadViewForDemonstratedProducts
{
    [_chart reset];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSMutableArray *arrValue = [[NSMutableArray alloc]init];
        arrproductName = [[NSMutableArray alloc]init];
        NSString* countQry = @"";
        
        if ([appControl.INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
            countQry = [NSString stringWithFormat:@"select E.Visit_ID,E.Product_ID, IFNULL(p.Product_Name,'') AS Product_Name from PHX_TBL_Edetailing AS E left join phx_tbl_products as p on e.Product_ID=p.Product_ID where e.Discussed_At like '%%%@%%' and p.Product_Name not null group by E.Visit_ID,E.Product_ID", monthString];
        } else {
            countQry = [NSString stringWithFormat:@"SELECT IFNULL(p.Product_Name,'') AS Product_Name, e.Product_ID FROM PHX_TBL_EDetailing AS e left join phx_tbl_products as p on e.Product_ID=p.Product_ID left join PHX_TBL_Doctors AS D on e.Doctor_ID = D.Doctor_ID left join PHX_TBL_LOcations AS L on e.Location_ID = L.Location_ID where e.Discussed_At like '%%%@%%' and p.Product_Name not null and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y' GROUP BY E.Visit_ID, E.Product_ID ",monthString];
        }
        
        NSMutableArray *CountMedialFiles = [[SWDatabaseManager retrieveManager]fetchDataForQuery:countQry];
        
        if ([CountMedialFiles count] == 0) {
            viewDemonstratedGraph.hidden = YES;
        }
        else
        {
           // arrproductName=[CountMedialFiles valueForKey:@"Product_Name"];
            //arrValue=[CountMedialFiles valueForKey:@"ProductCount"];
            //arrValue=[CountMedialFiles valueForKey:@"Product_Name"];
            NSMutableArray* valuesArray = [[NSMutableArray alloc]init];
            for(NSInteger i = 0;i<CountMedialFiles.count;i++){
                NSMutableDictionary* currentDict = [[NSMutableDictionary alloc]init];
                currentDict = [CountMedialFiles objectAtIndex:i];
                NSMutableDictionary* valuesDict = [[NSMutableDictionary alloc]init];

                //check if products id exists if yes then increment the count
                NSArray * idArray = [valuesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Product_ID == %@",[NSString getValidStringValue:[currentDict valueForKey:@"Product_ID"]]]];
                if (idArray.count>0){
                    NSLog(@"index of object is %lu",(unsigned long)[valuesArray indexOfObject:[idArray objectAtIndex:0]]);
                    
                    [valuesArray removeObject:[idArray objectAtIndex:0]];
                    [valuesDict setObject:[NSString getValidStringValue:[currentDict valueForKey:@"Product_Name"]] forKey:@"Product_Name"];
                    NSInteger existingCount = [[NSString getValidStringValue:[[idArray objectAtIndex:0]valueForKey:@"Product_Count"]] integerValue];
                    existingCount++;
                    [valuesDict setObject:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",existingCount]] forKey:@"Product_Count"];
                    [valuesDict setObject:[NSString getValidStringValue:[currentDict valueForKey:@"Product_ID"]] forKey:@"Product_ID"];

                }else{
                    [valuesDict setObject:[NSString getValidStringValue:[currentDict valueForKey:@"Product_Name"]] forKey:@"Product_Name"];
                    [valuesDict setObject:[NSString getValidStringValue:@"1"] forKey:@"Product_Count"];
                    [valuesDict setObject:[NSString getValidStringValue:[currentDict valueForKey:@"Product_ID"]] forKey:@"Product_ID"];

                }
                [valuesArray addObject:valuesDict];
                
            }
           // [valuesArray sortUsingDescriptors:[NSSortDescriptor sortDescriptorWithKey:@"Product_Count" ascending:NO]];
            
            NSArray *sortedArray = [valuesArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                long data1 = [[obj1 valueForKey:@"Product_Count"] integerValue];
                long data2 = [[obj2 valueForKey:@"Product_Count"] integerValue];
                if (data1 > data2) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                if (data1 < data2) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                return (NSComparisonResult)NSOrderedSame;
            }];

            
            NSArray *topFiveProductsArray = sortedArray;
            if (topFiveProductsArray.count>5){
                topFiveProductsArray = [topFiveProductsArray subarrayWithRange:NSMakeRange(0, 5)];
            }

            NSLog(@"values array is %@", topFiveProductsArray);
            arrproductName=[topFiveProductsArray valueForKey:@"Product_Name"];
            arrValue=[topFiveProductsArray valueForKey:@"Product_Count"];

            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                _values = (NSArray *)arrValue;
                _barColors						= @[[UIColor colorWithRed:(33.0/255.0) green:(186.0/255.0) blue:(164.0/255.0) alpha:1]];
                _currentBarColor				= 0;
                
                _chart							= [[SimpleBarChart alloc] initWithFrame:CGRectMake(0,40, _viewDemonstratedProducts.frame.size.width, _viewDemonstratedProducts.frame.size.height-80)];
                _chart.center					= CGPointMake((_viewDemonstratedProducts.frame.size.width / 2.0), (_viewDemonstratedProducts.frame.size.height / 2.0)+15);
                _chart.delegate					= self;
                _chart.dataSource				= self;
                _chart.barShadowOffset			= CGSizeMake(2.0, 1.0);
                _chart.animationDuration		= 1.0;
                _chart.barShadowColor			= [UIColor grayColor];
                _chart.barShadowAlpha			= 0.5;
                _chart.barShadowRadius			= 1.0;
                _chart.barWidth					= 35.0;
                _chart.xLabelType				= SimpleBarChartXLabelTypeHorizontal;
                _chart.gridColor				= [UIColor whiteColor];
                [_viewDemonstratedProducts addSubview:_chart];
                [_chart reloadData];
                
            });
        }
    });
}

#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return _values.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] floatValue];
}
- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    return [arrproductName objectAtIndex:index];
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
    return [_barColors objectAtIndex:_currentBarColor];
}

#pragma mark Face Time Vs. Wait Time

-(void)loadViewForFaceTimeVsWaitTime
{
    if (_viewFaceTimeVsWaitTime.layer.sublayers.count>0) {
        
        [_viewFaceTimeVsWaitTime.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    }
    NSString* faceTimeQuery = @"";
    NSString* waitTimeQuery = @"";
    
    if ([appControl.INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
        faceTimeQuery=[NSString stringWithFormat:@"Select round (sum ((JulianDay(Call_Ended_At) - JulianDay(Call_Started_At)) * 24 * 60)) As faceTime FROM PHX_TBL_Actual_Visits where Visit_Start_Date like '%%%@%%'",monthString];
        waitTimeQuery = [NSString stringWithFormat:@"Select round (sum ((JulianDay(Call_Started_At) - JulianDay(Visit_Start_Date)) * 24 * 60)) As waitTime FROM PHX_TBL_Actual_Visits where Visit_Start_Date like '%%%@%%'",monthString];
    } else {
        faceTimeQuery = [NSString stringWithFormat:@"Select round (sum ((JulianDay(A.Call_Ended_At) - JulianDay(A.Call_Started_At)) * 24 * 60)) As faceTime FROM PHX_TBL_Actual_Visits As A left join PHX_TBL_Doctors AS D ON A.Doctor_ID = D.Doctor_ID left join PHX_TBL_Locations as L on A.Location_ID = L.Location_ID where Visit_Start_Date like '%%%@%%' and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y'",monthString];
        
        waitTimeQuery = [NSString stringWithFormat:@"Select round (sum ((JulianDay(A.Call_Started_At) - JulianDay(A.Visit_Start_Date)) * 24 * 60)) As waitTime FROM PHX_TBL_Actual_Visits As A left join PHX_TBL_Doctors AS D ON A.Doctor_ID = D.Doctor_ID left join PHX_TBL_Locations as L on A.Location_ID = L.Location_ID where Visit_Start_Date like '%%%@%%' and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y'",monthString];
    }
    
    NSArray *arrFaceTime = [[SWDatabaseManager retrieveManager] fetchDataForQuery:faceTimeQuery];
    NSArray *arrWaitTime = [[SWDatabaseManager retrieveManager] fetchDataForQuery:waitTimeQuery];
    
    int faceTime = 0;
    int waitTime = 0;
    if (arrFaceTime.count >0)
    {
        faceTime = [[[arrFaceTime objectAtIndex:0]stringForKey:@"faceTime"]intValue];
    }
    if (arrWaitTime.count >0)
    {
        waitTime = [[[arrWaitTime objectAtIndex:0]stringForKey:@"waitTime"]intValue];
    }
    
    NSLog(@"FAT AND WT ARE %d %d",faceTime,waitTime);
    
    
    self.lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(10, 0, 460, 95)];
    self.lineChart.yLabelFormat = @"%1.1f";
    self.lineChart.backgroundColor = [UIColor clearColor];
    //[self.lineChart setXLabels:@[@"SEP 1",@"SEP 2"]];
    self.lineChart.showCoordinateAxis = YES;
    
    //Use yFixedValueMax and yFixedValueMin to Fix the Max and Min Y Value
    //Only if you needed
    self.lineChart.yFixedValueMax = 60.0;
    self.lineChart.yFixedValueMin = 0.0;
    
    [self.lineChart setYLabels:@[@"W.T.",@"F.T.",]];
    
    // Line Chart #1
    NSArray * data01Array = @[@50.0, @50.0];
    PNLineChartData *data01 = [PNLineChartData new];
    data01.dataTitle = @"Alpha";
    data01.color = KSalesWorxFaceTimeColor;
    data01.alpha = 1.0f;
    data01.itemCount = data01Array.count;
    data01.inflexionPointStyle = PNLineChartPointStyleTriangle;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    // Line Chart #2
    NSArray * data02Array = @[@0.0, @0.0];
    PNLineChartData *data02 = [PNLineChartData new];
    data02.dataTitle = @"Beta";
    data02.color = KSalesWorxWaitTimeColor;
    data02.alpha = 1.0f;
    data02.itemCount = data02Array.count;
    data02.inflexionPointStyle = PNLineChartPointStyleCircle;
    data02.getData = ^(NSUInteger index) {
        CGFloat yValue = [data02Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    self.lineChart.chartData = @[data01, data02];
    
    if (waitTime < 0) {
        waitTime = 0;
    }
    else if (faceTime < 0) {
        faceTime = 0;
    }
    
    if(faceTime>350 || waitTime>350)
        [self.lineChart strokeChart:faceTime*350/(faceTime+waitTime) :waitTime*350/(faceTime+waitTime)];
    else
        [self.lineChart strokeChart:faceTime :waitTime];
    
    self.lineChart.delegate = self;
    [_viewFaceTimeVsWaitTime addSubview:self.lineChart];
    
    self.lineChart.legendStyle = PNLegendItemStyleStacked;
    self.lineChart.legendFont = kSWX_FONT_SEMI_BOLD(12);
    
    NSString *FT = [NSString stringWithFormat:@"%d",faceTime];
    NSString *WT = [NSString stringWithFormat:@"%d",waitTime];
    
    UIView *legend = [self.lineChart getLegendWithMaxWidth:100 :FT :WT];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language containsString:@"ar"])
    {
        [legend setFrame:CGRectMake(330, 5, 70, legend.frame.size.height)];
    }
    else
    {
        [legend setFrame:CGRectMake(380, 5, 70, legend.frame.size.height)];
    }
    [_viewFaceTimeVsWaitTime addSubview:legend];

}

- (void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex pointIndex:(NSInteger)pointIndex{
    NSLog(@"Click Key on line %f, %f line index is %d and point index is %d",point.x, point.y,(int)lineIndex, (int)pointIndex);
}

- (void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
    NSLog(@"Click on line %f, %f, line index is %d",point.x, point.y, (int)lineIndex);
}

#pragma mark Productive Visits

-(void)loadViewForProductiveVisits
{
    if (_viewProductiveVisits.layer.sublayers.count>0) {
        
        [_viewProductiveVisits.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    }
    
    NSArray *arrTotalVisits = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select count(*) from PHX_TBL_Actual_Visits where Visit_Start_Date like '%%%@%%'",monthString]];
    
    NSString* visitsQuery = @"";
    int totalVisits = 0;
    int productiveVisits = 0;

    if ([appControl.INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
        visitsQuery = [NSString stringWithFormat:@"select count(distinct Planned_Visit_ID) As count from PHX_TBL_Actual_Visits  where Visit_Start_Date like '%%%@%%' and Call_Started_At is not null", monthString];
    } else {
        visitsQuery = [NSString stringWithFormat:@"select count(distinct A.Planned_Visit_ID) As count,D.Doctor_ID from PHX_TBL_Actual_Visits AS A left join PHX_TBL_Doctors AS D on A.Doctor_ID = D.Doctor_ID left join PHX_TBL_LOcations AS L on A.Location_ID = L.Location_ID where A.Visit_Start_Date like '%%%@%%' and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y' and A.Call_Started_At is not null",monthString];
    }

    NSArray *arrProductiveVisits = [[SWDatabaseManager retrieveManager] fetchDataForQuery:visitsQuery];
    if (arrTotalVisits >0)
    {
        totalVisits = [[[arrTotalVisits objectAtIndex:0]stringForKey:@"count(*)"]intValue];
    }
    
    if (arrProductiveVisits >0)
    {
        productiveVisits = [[[arrProductiveVisits objectAtIndex:0]stringForKey:@"count"]intValue];
    }

    
    NSString *productsDemonstratedQuery = @"";
    NSString* samplesGivenQuery = @"";
    int productDemonstrated = 0;
    int samplesGiven = 0;

    NSArray *arrProductDemonstrated = [[NSArray alloc]init];
    NSArray *arrSamplesGiven = [[NSArray alloc]init];
    
    
    if ([appControl.INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
        productsDemonstratedQuery = [NSString stringWithFormat:@"select count(*) from PHX_TBL_EDetailing where Discussed_At like '%%%@%%'",monthString];
        samplesGivenQuery = [NSString stringWithFormat:@"select sum(A.Issue_Qty) As sum from PHX_TBL_Issue_Note_Line_Items AS A left join PHX_TBL_Issue_Note As B  where A.Doc_No = B.Doc_No and B.Start_Time like '%%%@%%'",monthString];
    } else {
        productsDemonstratedQuery = [NSString stringWithFormat:@"select count(*) from PHX_TBL_EDetailing as E left join PHX_TBL_Locations as L on E.Location_ID = L.Location_ID left join PHX_TBL_Doctors AS D on E.Doctor_ID = D.Doctor_ID where Discussed_At like '%%%@%%' and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y'", monthString];

        samplesGivenQuery = [NSString stringWithFormat:@"select SUM(IL.Issue_Qty) AS sum from PHX_TBL_Issue_Note AS I inner join PHX_TBL_Issue_Note_Line_Items AS IL on I.Doc_No = IL.Doc_No left join PHX_TBL_Locations AS L on I.Location_ID = L.Location_ID left join PHX_TBL_Doctors AS D on I.Doctor_ID = D.Doctor_ID and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y' and I.Start_Time like '%%%@%%'",monthString];
    }
    
    arrProductDemonstrated = [[SWDatabaseManager retrieveManager] fetchDataForQuery:productsDemonstratedQuery];
    arrSamplesGiven = [[SWDatabaseManager retrieveManager] fetchDataForQuery:samplesGivenQuery];
    
    if (arrProductDemonstrated.count >0)
    {
        productDemonstrated = [[[arrProductDemonstrated objectAtIndex:0]stringForKey:@"count(*)"]intValue];
    }
    if (arrSamplesGiven.count >0)
    {
        samplesGiven = [[[arrSamplesGiven objectAtIndex:0]stringForKey:@"sum"]intValue];
    }
    
    _lblProductsDemonstrated.text = [NSString stringWithFormat:@"%d",productDemonstrated];
    _lblSamplesGiven.text = [NSString stringWithFormat:@"%d",samplesGiven];
    
    [self loadPercentageChart:circleChartForProductiveVisits :_viewProductiveVisits :[NSNumber numberWithInt:totalVisits] :[NSNumber numberWithInt:productiveVisits] :[UIColor colorWithRed:(88.0/255.0) green:(116.0/255.0) blue:(152.0/255.0) alpha:1]];
}

#pragma mark Load Vist Frequency Graph

-(void)loadVisitFrequency
{
    [self.gkLineGraph reset];

    arrVisitFrequency = [[NSMutableArray alloc]init];
    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        
        NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
        NSString *output = @"0.00";
        NSString *newOutput = [NSString stringWithFormat:@"%@", output];
        [dummyDict setObject:newOutput forKey:@"Ammount"];
        [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
        
        
        //2018-09-24 : As requested by INMAA we shall display the number of calls in visit frquency graph instead of visits, so displaying data from PHX_TBL_Actual_Visits based on flag FM_DISPLAY_CALLS_VISIT_FREQUENCY
        NSString* visitFrequencyQuery = [[NSString alloc]init];
        NSString* displayCallsinVisitFrequency = [AppControl retrieveSingleton].FM_DISPLAY_CALLS_VISIT_FREQUENCY;
       // displayCallsinVisitFrequency = KAppControlsYESCode;
        if ([[SWDefaults getValidStringValue:displayCallsinVisitFrequency] isEqualToString:KAppControlsYESCode])
        {
            if ([appControl.INCLUDE_UNAPPR_DR_LOC isEqualToString:KAppControlsYESCode]) {
                visitFrequencyQuery = [NSString stringWithFormat:@"SELECT COUNT(*) AS Total FROM PHX_TBL_Actual_Visits where Visit_End_Date Like '%%%@%%'", currentString];
            } else {
                visitFrequencyQuery = [NSString stringWithFormat:@"select count(*) As Total from PHX_TBL_Actual_Visits AS A left join PHX_TBL_Doctors AS D on A.Doctor_ID = D.Doctor_ID left join PHX_TBL_Locations AS L on A.Location_ID = L.Location_ID where A.Visit_End_Date like '%%%@%%' and IFNULL(D.Is_Approved,'Y') = 'Y' AND IFNULL(L.Is_Approved,'Y') = 'Y'",currentString];
            }
        }
        else
        {
            visitFrequencyQuery = [NSString stringWithFormat:@"SELECT COUNT(*) AS Total FROM PHX_TBL_Planned_Visits where Visit_Date Like '%%%@%%' AND Visit_Status = 'Y'", currentString];
        }
        
        NSArray *temp = [[SWDatabaseManager retrieveManager] fetchDataForQuery:visitFrequencyQuery];
        if(temp.count>0)
        {
        [dummyDict setObject:[[temp objectAtIndex:0]valueForKey:@"Total"] forKey:@"Total"];
        if ([[[temp objectAtIndex:0]valueForKey:@"Total"]intValue] >0) {
            [arrVisitFrequency addObject:dummyDict];
//            [arrVisitFrequency insertObject:dummyDict atIndex:i];
        }
        }
    }
    NSLog(@"visit frequency is %@",arrVisitFrequency);
    
    
    if (arrVisitFrequency.count==0) {
        isEmptyGraph=YES;
        
        [self displayEmptyDataGraph];
    }
    else{
        
        //adding missing date data
        
        NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
        
        for (NSInteger i=0; i<tempDates.count; i++) {
            NSString* currentString=[tempDates objectAtIndex:i];
            if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
                
            }
            else{
                NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
                NSString *output = @"0.00";
                NSString *newOutput = [NSString stringWithFormat:@"%@", output];
                [dummyDict setObject:newOutput forKey:@"Ammount"];
                [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
                [dummyDict setObject:@0 forKey:@"Total"];
                [arrVisitFrequency insertObject:dummyDict atIndex:i];
            }
        }
        
        NSLog(@"sales data in visit frequency is  %@" ,arrVisitFrequency);
        
        isEmptyGraph=NO;
        
        [self testDataGraph];
    }
}

-(void)testDataGraph
{
    if (arrVisitFrequency.count>0)
    {
        NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
        currentMonthFormatter.dateFormat=@"MMMM";
        NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
        NSLog(@"current month is %@", currentMonth);
        monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];
        
        NSMutableArray * refinedSalesArray=[[NSMutableArray alloc]init];
        
        if (arrVisitFrequency.count>0) {
            //this is for first of every month
            
            if (arrVisitFrequency.count==1) {
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:1]];
            }
            for (NSString * amountStr in [arrVisitFrequency valueForKey:@"Total"]) {
                
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
            }
        }
   
        NSMutableArray * testArray=[[NSMutableArray alloc]init];
        [testArray addObject:refinedSalesArray];
        
        //NSLog(@" test array %@",testArray);
        NSLog(@" graph array %@",testArray);
        self.data =testArray;
        
        
        NSLog(@"Vist frequency data array is %@",arrVisitFrequency);
        
        
        NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
        

        self.labels = labalesArray;
 
        NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));

        self.gkLineGraph.dataSource = self;
        self.gkLineGraph.lineWidth = 3.0;
        self.gkLineGraph.startFromZero=YES;
        self.gkLineGraph.valueLabelCount = 5;
        
        testArray = [[testArray objectAtIndex:0] valueForKeyPath:@"@distinctUnionOfObjects.self"];
        self.gkLineGraph.valueLabelCount = [testArray count];
        
        self.gkLineGraph.isMedRepDashboard = YES;
        
        [_gkLineGraph draw];
    }
}

-(void)displayEmptyDataGraph
{
    NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
    currentMonthFormatter.dateFormat=@"MMMM";
    NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
    NSLog(@"current month is %@", currentMonth);
    monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];
    
    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    
    NSMutableArray * testSalesDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * testReturnsDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * emptyGraphDataArray=[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testSalesDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[arrVisitFrequency valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testReturnsDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    NSMutableArray * testArray=[[NSMutableArray alloc]init];
    NSMutableArray * tempSalesDataArray=[[NSMutableArray alloc]init];
    NSMutableArray * tempReturnsDataArray=[[NSMutableArray alloc]init];
    
    
    for (NSString * amountStr in [testSalesDataArray valueForKey:@"Ammount"]) {
        [tempSalesDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        [emptyGraphDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        
    }
    for (NSString * amountStr in [testReturnsDataArray valueForKey:@"Ammount"]) {
        
        [tempReturnsDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        
    }
    
    [testArray addObject:tempSalesDataArray];
    [testArray addObject:tempReturnsDataArray];
    
    NSLog(@"test array for dummy graph %@ \n%@ \n%@",tempSalesDataArray,tempSalesDataArray,emptyGraphDataArray);
    
    
    [emptyGraphDataArray replaceObjectAtIndex:0 withObject:[NSDecimalNumber numberWithInteger:[@"100" integerValue]]];
    [testArray addObject:emptyGraphDataArray];
    
    
    self.data =testArray;
    
    
    NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
    self.labels = labalesArray;

    
    NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));
    
    self.gkLineGraph.dataSource = self;
    self.gkLineGraph.lineWidth = 3.0;
    
    self.gkLineGraph.valueLabelCount = 5;
    NSLog(@"trying to draw graph %@", self.gkLineGraph.layer.sublayers);
    [self.gkLineGraph draw];
}

#pragma mark - GKLineGraphDataSource

- (NSInteger)numberOfLines {
    return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index {
    
    if (isEmptyGraph==YES) {
        
        id colors = @[[UIColor clearColor],
                      [UIColor colorWithRed:(75.0/255.0) green:(143.0/255.0) blue:(249.0/255.0) alpha:1],
                      [UIColor clearColor],
                      ];
        return [colors objectAtIndex:index];
        
    }
    else{
        id colors = @[[UIColor colorWithRed:(13.0/255.0) green:(133.0/255.0) blue:(248.0/255.0) alpha:1],
                      [UIColor clearColor],
                      ];
        return [colors objectAtIndex:index];
        
    }
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index {
    
    if (isEmptyGraph==YES) {
        return [[@[@1, @1.6,@1.6] objectAtIndex:index] doubleValue];
        
    }
    else{
        return [[@[@1, @1.6] objectAtIndex:index] doubleValue];
        
    }
    
}

- (NSString *)titleForLineAtIndex:(NSInteger)index {
    
    @try {
        if ([self.labels count] > 15) {
            if ([[self.labels lastObject]integerValue] % 2 == 0) {
                // current date is even date
                
                NSInteger object = [[self.labels objectAtIndex:index]integerValue];
                if (object % 2 == 0)
                {
                    // even dates
                    return [self.labels objectAtIndex:index];
                }
                else
                {
                    return @"";
                }
            }
            else
            {
                // current date is odd date
                
                NSInteger object = [[self.labels objectAtIndex:index]integerValue];
                
                if (object % 2 != 0)
                {
                    // odd dates
                    return [self.labels objectAtIndex:index];
                }
                else
                {
                    return @"";
                }
            }
        }
        else
        {
            return [self.labels objectAtIndex:index];
        }
    } @catch (NSException *exception) {
        return @"";
        
    } @finally {
        
    }
}

-(NSMutableArray*)fetchDatesofCurrentMonthinDBFormat
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    [fmt setDateFormat:@"yyyy-MM-dd"]; // you can set this to whatever you like
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
        [dates sortUsingSelector:@selector(compare:)];
        
    }
    return dates;
}

-(NSMutableArray*)fetchDatesofCurrentMonth
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"dd"]; // you can set this to whatever you like
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
        // [dates insertObject:@"0" atIndex:0];
        
        //NSLog(@"dates are %@",dates);
        
        [dates sortUsingSelector:@selector(compare:)];
    }
    return dates;
}

#pragma mark IBAction Visit
- (IBAction)btnVisit:(id)sender {
    
    MedRepVisitsListViewController *visitVC = [[MedRepVisitsListViewController alloc] init];
    visitVC.selectedVisitDetailsDict = [sortedPlannedVisitArray objectAtIndex:0];
    visitVC.isFromDashboard = YES;
    [self.navigationController pushViewController:visitVC animated:YES];
}

#pragma mark
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
