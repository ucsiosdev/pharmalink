
//
//  SalesWorxFeedbackViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxFeedbackViewController.h"

#import "SWDatabaseManager.h"
#import "SurveyQuestionViewController.h"
#import "SurveyQuestionDetails.h"
#import "SurveyResponseDetails.h"
#import "DataType.h"
#import "FMDB/FMDBHelper.h"
#import "MedRepTextView.h"
#import "NMRangeSlider.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxImageBarButtonItem.h"
#import "SalesWorxItemDividerLabel.h"

#define saveSurveyResponse @"insert into TBL_Survey_Cust_Responses(Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('%d','%d','%@','%d','%d','%d','%@','%@')"


@interface SalesWorxFeedbackViewController ()

@end

@implementation SalesWorxFeedbackViewController

@synthesize surveyQuestionArray=_surveyQuestionArray,survetIDNew,surveyResponseArray=_surveyResponseArray,qID,containerView,scrollView,modifiedSurveyQuestionsArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIViewBackGroundColor;
    modifiedSurveyQuestionsArray=[[NSMutableArray alloc]init];
    
    viewX = 0;
    viewY =0;
    viewWidth = 1008;
    totalQuestions = 1000;
    
    textAreaQuestionAnswerArray = [[NSMutableArray alloc]init];
    checkBoxQuestionAnswerArray = [[NSMutableArray alloc]init];
    radioQuestionAnswerArray = [[NSMutableArray alloc]init];
    sliderQuestionAnswerArray = [[NSMutableArray alloc]init];
    
    textAreaQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    checkBoxQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    radioQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    textAreaAnswerDescriptionArray=[[NSMutableArray alloc]init];
    sliderQuestionAnswerValidationArray = [[NSMutableArray alloc]init];
    
    appDelegate.SurveyType = appDelegate.surveyDetails.Survey_Type_Code;
    
    NSLog(@"survey type new is %@", appDelegate.SurveyType);
    
    UIBarButtonItem *btnClose = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnBack:)];
    [btnClose setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = btnClose;
    
    SalesWorxImageBarButtonItem *btnSave = [[SalesWorxImageBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(Save)];
    [btnSave setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = btnSave;
    
    scrollView.scrollEnabled=YES;
    scrollView.delegate=self;
    
    appDelegate=[DataSyncManager sharedManager];
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:appDelegate.surveyDetails.Survey_Title];
    
    if(appDelegate.customerResponseArray)
        [appDelegate.customerResponseArray removeAllObjects];
    appDelegate.SurveyType = appDelegate.surveyDetails.Survey_Type_Code;
    
    NSLog(@"Survey type is %@",appDelegate.SurveyType);
    NSLog(@"survey type code is %@", appDelegate.surveyDetails.Survey_Type_Code);
    // Get survey question
    NSLog(@"survey id new %d", survetIDNew);
    NSLog(@"survey id in new %d", appDelegate.surveyDetails.Survey_ID);
    
    saveSurveyID=appDelegate.surveyDetails.Survey_ID;
    NSLog(@"saved survey id is %d", saveSurveyID);

    
    
    _surveyQuestionArray = [[SWDatabaseManager retrieveManager] selectQuestionBySurveyId:appDelegate.surveyDetails.Survey_ID];//survetIDNew];
    [self ModifyQuestionResponse];
    
    NSLog(@"new survey questions array count is %lu", (unsigned long)[modifiedSurveyQuestionsArray count]);
    
    if ([modifiedSurveyQuestionsArray count]>0)
    {
        [self CreateQuestionsView];
    }
    
    //got survey questions here
    for (int i=0; i<[self.surveyQuestionArray count]; i++)
    {
        surveyQuestionDetails = [_surveyQuestionArray objectAtIndex:i];
        NSLog(@"Survey questions new %@", surveyQuestionDetails.Question_Text);
        NSLog(@"Question id is %d", surveyQuestionDetails.Question_ID);
        
        
        //answer here
        self.surveyResponseArray=[[SWDatabaseManager retrieveManager] selectResponseType:surveyQuestionDetails.Question_ID];
        
        for (int j=0; j<[self.surveyResponseArray count]; j++)
        {
            info=[_surveyResponseArray objectAtIndex:j];

            NSLog(@"info tect is %@", info.Response_Text);
            NSLog(@"response type is %d", info.Response_Type_ID);
        }
        NSLog(@"response type id before view gets created is %@", [_surveyResponseArray valueForKey:@"Response_Type_ID"]);
    }
    
    NSLog(@"info count is %lu", (unsigned long)[self.surveyResponseArray count]);
}
- (void) btnBack:(id)sender
{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
}
-(void)CreateQuestionsView
{
    for (int i=0;i<modifiedSurveyQuestionsArray.count;i++)
    {
        SurveyQuestion* question=[modifiedSurveyQuestionsArray objectAtIndex:i];
        
        if ([question.Response_Type_ID isEqualToString:@"1"]) {
            [self createViewForTextAreaQuestion:question withQuestionNumber:i+1];
        }
        else if ([question.Response_Type_ID isEqualToString:@"2"])// && [question.Question_ID intValue] == 1)
        {
            [self createViewForRadioQuestion:question withQuestionNumber:i+1];
        }
        else if ([question.Response_Type_ID isEqualToString:@"3"])
        {
            [self createViewForCheckBoxQuestion:question withQuestionNumber:i+1];
        }
        else if ([question.Response_Type_ID isEqualToString:@"4"])
        {
            [self createViewForSliderQuestion:question withQuestionNumber:i+1];
        }
    }
    //btnStatusForQ.hidden = YES;
}

-(void)ModifyQuestionResponse
{
    [modifiedSurveyQuestionsArray removeAllObjects];
    
    NSLog(@"int is %d", saveSurveyID);
    NSString *query = [NSString stringWithFormat:@"SELECT DISTINCT A.*, B.Response_Type_ID FROM TBL_Survey_Questions As A INNER JOIN TBL_Survey_Responses As B ON A.Question_ID=B.Question_ID WHERE A.Survey_ID= %d", saveSurveyID];
    
    NSLog(@"query to fetch questions is %@", query);
    
    NSArray *temp =[FMDBHelper executeQuery:query];
    NSLog(@"temp is %@", [temp description]);
    
    for (NSMutableDictionary *customerDic in temp)
    {
        SurveyQuestion *customer = [SurveyQuestion new];
        
        if ([[customerDic valueForKey:@"Question_ID"] isEqual: [NSNull null]]) {
            customer.Question_ID = @"";
        }else{
            customer.Question_ID = [customerDic valueForKey:@"Question_ID"];
        }
        
        if ([[customerDic valueForKey:@"Question_Text"] isEqual: [NSNull null]]) {
            customer.Question_Text = @"";
        }else{
            customer.Question_Text = [customerDic valueForKey:@"Question_Text"];
        }
        
        if ([[customerDic valueForKey:@"Survey_ID"] isEqual: [NSNull null]]) {
            customer.Survey_ID = @"";
        }else{
            customer.Survey_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Survey_ID"]];
        }
        
        if ([[customerDic valueForKey:@"Default_Response_ID"] isEqual: [NSNull null]]) {
            customer.Default_Response_ID= @"";
        }else{
            customer.Default_Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Default_Response_ID"]];
        }
        
        if ([[customerDic valueForKey:@"Response_Type_ID"] isEqual: [NSNull null]]) {
            customer.Response_Type_ID = @"";
        }else{
            customer.Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        }
        [modifiedSurveyQuestionsArray addObject:customer];
    }
}

#pragma mark create Radio Buttons View

-(void)createViewForRadioQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber
{
    int viewHeightCalculator = 0;
    
    SalesWorxDropShadowView *textQuestionView = [[SalesWorxDropShadowView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, 100)];
    textQuestionView.backgroundColor = [UIColor whiteColor];
    textQuestionView.layer.shadowColor = [UIColor blackColor].CGColor;
    textQuestionView.layer.shadowOffset = CGSizeMake(2, 2);
    textQuestionView.layer.shadowOpacity = 0.1;
    textQuestionView.layer.shadowRadius = 1.0;
    textQuestionView.tag = questionNumber;

    btnStatusForQ = [[UIButton alloc]initWithFrame:CGRectMake (0, viewHeightCalculator, 50, 50)];
    [btnStatusForQ setImage:[UIImage imageNamed:@"Survey_CheckStatus"] forState:UIControlStateNormal];
    [btnStatusForQ setTag:totalQuestions+questionNumber];
    btnStatusForQ.hidden = YES;
    [textQuestionView addSubview:btnStatusForQ];
    
    viewHeightCalculator = viewHeightCalculator+15;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (50, viewHeightCalculator, viewWidth-15, 30)];
    NSString * QuestionText = [NSString stringWithFormat:@"%@",question.Question_Text];
    questionLabel.font = kSWX_FONT_REGULAR(18);
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
    
    
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    
    SalesWorxItemDividerLabel *separatorView = [[SalesWorxItemDividerLabel alloc]init];// [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Divider_horizontal"]];
    separatorView.backgroundColor=kUIElementDividerBackgroundColor;
    separatorView.frame =CGRectMake (8, viewHeightCalculator+20, 992, 2);
    [textQuestionView addSubview:separatorView];

    viewHeightCalculator = viewHeightCalculator+40;
    
    NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
    NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
    NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
   
    for (NSMutableDictionary *customerDic in array)
    {
        SurveyResponse *customer = [SurveyResponse new];
        customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
        customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
        customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
        customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        [SurveyResponseArray addObject: customer];
    }
    //button 1
    int xAxis = 50;
    for (int i= 0; i<SurveyResponseArray.count; i++)
    {
        SurveyResponse * response = [SurveyResponseArray objectAtIndex:i];
        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.frame = CGRectMake(xAxis, viewHeightCalculator, 32, 32);
        UIImage *btnImage = [UIImage imageNamed:@"Survey_RadioInActive"];
        UIImage *btnImage1 = [UIImage imageNamed:@"Survey_RadioActive"];
        [imageButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [imageButton setBackgroundImage:btnImage1 forState:UIControlStateSelected];
        [imageButton addTarget:self action:@selector(buttonClickedForRadio:) forControlEvents:UIControlEventTouchDown];
        
        imageButton.tag =questionNumber  * 100 + [response.Response_ID intValue];
        NSLog(@" image button tag is %ld",(long)imageButton.tag);
        NSLog(@"response id is %@", response.Response_ID);
        
        if ([question.Default_Response_ID isEqualToString:response.Response_ID]) {
            //imageButton.selected =YES;
        }
        [textQuestionView addSubview:imageButton];
        
        UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (xAxis+40, viewHeightCalculator+5, viewWidth-15, 20)];
        questionLabel.font=MedRepElementDescriptionLabelFont;
        questionLabel.textColor=MedRepElementDescriptionLabelColor;
        NSString * QuestionText = response.Response_Text;
        questionLabel.text = QuestionText;
        questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        questionLabel.numberOfLines = 0;
        CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);

        CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
        CGRect questionLabelOldFrame = questionLabel.frame;
        questionLabelOldFrame.size.height = expectedLabelSize.height;
        questionLabelOldFrame.size.width = expectedLabelSize.width;
        questionLabel.frame = questionLabelOldFrame;
        [textQuestionView addSubview:questionLabel];
        
        xAxis = xAxis+200;
    }
    
    viewHeightCalculator = viewHeightCalculator+51;
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 8;
    
    [self.scrollView setContentSize:(CGSizeMake(scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
}
- (void)buttonClickedForRadio:(UIButton*)button
{
    UIView *textQuestionView = button.superview;
    NSLog(@"Hi %ld",(long)textQuestionView.tag);
    [[self.view viewWithTag:textQuestionView.tag+1000]setHidden:NO];
    
    SurveyQuestion * question = [modifiedSurveyQuestionsArray objectAtIndex:textQuestionView.tag-1];
    if ([question.Response_Type_ID isEqualToString:@"2"]){
        
        NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
        NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
        NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
        for (NSMutableDictionary *customerDic in array) {
            SurveyResponse *customer = [SurveyResponse new];
            customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
            customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
            customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
            customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
            [SurveyResponseArray addObject: customer];
            
            
            UIButton * allButton  = (UIButton *)[textQuestionView viewWithTag:textQuestionView.tag * 100 + [customer.Response_ID intValue]];
            
            if (allButton.tag == button.tag) {
                allButton.selected = YES;
            }else{
                allButton.selected = NO;
            }
        }
    }
}

#pragma mark create Text Field View
-(void)createViewForTextAreaQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber{
    
    int viewHeightCalculator = 0;
    
    SalesWorxDropShadowView *textQuestionView = [[SalesWorxDropShadowView alloc] initWithFrame:CGRectMake( viewX, viewY, viewWidth, 100)];
    textQuestionView.backgroundColor = [UIColor whiteColor];
    textQuestionView.layer.shadowColor = [UIColor blackColor].CGColor;
    textQuestionView.layer.shadowOffset = CGSizeMake(2, 2);
    textQuestionView.layer.shadowOpacity = 0.1;
    textQuestionView.layer.shadowRadius = 1.0;
    textQuestionView.tag = questionNumber;
    
    btnStatusForQ = [[UIButton alloc]initWithFrame:CGRectMake(0, viewHeightCalculator, 50, 50)];
    [btnStatusForQ setImage:[UIImage imageNamed:@"Survey_CheckStatus"] forState:UIControlStateNormal];
    [btnStatusForQ setTag:totalQuestions+questionNumber];
    btnStatusForQ.hidden = YES;
    [textQuestionView addSubview:btnStatusForQ];
    
    viewHeightCalculator = viewHeightCalculator+15;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, viewHeightCalculator, viewWidth-15, 30)];
    questionLabel.font = kSWX_FONT_REGULAR(18);
    NSString * QuestionText = [NSString stringWithFormat:@"%@",question.Question_Text];
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    
    
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    
    SalesWorxItemDividerLabel *separatorView = [[SalesWorxItemDividerLabel alloc]init];// [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Divider_horizontal"]];
    separatorView.backgroundColor=kUIElementDividerBackgroundColor;
    separatorView.frame =CGRectMake (8, viewHeightCalculator+20, 992, 2);
    [textQuestionView addSubview:separatorView];
    
    viewHeightCalculator = viewHeightCalculator+38;
    
    UITextView *txtview =[[MedRepTextView alloc]init];
    [txtview setFrame:CGRectMake(50,viewHeightCalculator,895,50)];
    [txtview setReturnKeyType:UIReturnKeyDone];
    txtview.text = @"";
    txtview.font=MedRepElementDescriptionLabelFont;
    txtview.textColor=MedRepElementDescriptionLabelColor;
    txtview.layer.borderWidth=1.0f;
    txtview.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    txtview.layer.cornerRadius=5.0;
    txtview.tag = questionNumber *100;
    txtview.delegate = self;
    [textQuestionView addSubview:txtview];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame: CGRectMake(0,0,900,100)];
    imgView.image = [UIImage imageNamed: @"survey_comments.png"];
    [txtview addSubview: imgView];
    [txtview sendSubviewToBack: imgView];
    
    viewHeightCalculator = viewHeightCalculator+txtview.frame.size.height;
    viewHeightCalculator = viewHeightCalculator+20;

    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 8;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
}

#pragma mark create Slider View
-(void)createViewForSliderQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber
{
    int viewHeightCalculator = 0;
    
    SalesWorxDropShadowView *textQuestionView = [[SalesWorxDropShadowView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, 100)];
    textQuestionView.backgroundColor = [UIColor whiteColor];
    textQuestionView.layer.shadowColor = [UIColor blackColor].CGColor;
    textQuestionView.layer.shadowOffset = CGSizeMake(2, 2);
    textQuestionView.layer.shadowOpacity = 0.1;
    textQuestionView.layer.shadowRadius = 1.0;
    textQuestionView.tag = questionNumber;
    
    btnStatusForQ = [[UIButton alloc]initWithFrame:CGRectMake(0, viewHeightCalculator, 50, 50)];
    [btnStatusForQ setImage:[UIImage imageNamed:@"Survey_CheckStatus"] forState:UIControlStateNormal];
    [btnStatusForQ setTag:totalQuestions+questionNumber];
    btnStatusForQ.hidden = YES;
    [textQuestionView addSubview:btnStatusForQ];
    
    viewHeightCalculator = viewHeightCalculator+15;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (50, viewHeightCalculator, viewWidth-15, 30)];
    NSString * QuestionText = [NSString stringWithFormat:@"%@",question.Question_Text];
    questionLabel.font = kSWX_FONT_REGULAR(18);
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];
    
    
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;

    SalesWorxItemDividerLabel *separatorView = [[SalesWorxItemDividerLabel alloc]init];// [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Divider_horizontal"]];
    separatorView.backgroundColor=kUIElementDividerBackgroundColor;
    separatorView.frame =CGRectMake (8, viewHeightCalculator+20, 992, 2);
    [textQuestionView addSubview:separatorView];
    
    viewHeightCalculator = viewHeightCalculator+40;
    
    NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
    NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
    NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
    
    for (NSMutableDictionary *customerDic in array)
    {
        SurveyResponse *customer = [SurveyResponse new];
        customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
        customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
        customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
        customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        [SurveyResponseArray addObject: customer];
    }
    
    //button 1
    int xAxis = 50;
    
    
    camelLabelsView = [[TGPCamelLabels7 alloc] initWithFrame:CGRectMake(xAxis, viewHeightCalculator, 895, 44.0)];
    camelLabelsView.backgroundColor = [UIColor clearColor];
    camelLabelsView.tickCount=10;
    camelLabelsView.ticksDistance=75;
    camelLabelsView.value=0;
    camelLabelsView.upFontName=@"WeblySleekUISemibold";
    camelLabelsView.upFontSize=20;
    camelLabelsView.upFontColor=kNavigationBarBackgroundColor;
    camelLabelsView.downFontName=@"WeblySleekUISemibold";
    camelLabelsView.downFontSize=20;
    camelLabelsView.downFontColor=[UIColor lightGrayColor];
    camelLabelsView.backgroundColor=[UIColor clearColor];
    [textQuestionView addSubview:camelLabelsView];
    
    
    descreteSliderView = [[TGPDiscreteSlider7 alloc] initWithFrame:CGRectMake(xAxis, viewHeightCalculator+30, 895, 44.0)];
    descreteSliderView.backgroundColor = [UIColor clearColor];
    descreteSliderView.tickStyle=1;
    descreteSliderView.tickSize = CGSizeMake(1, 8);
    descreteSliderView.tickCount=10;
    descreteSliderView.trackThickness=2;
    descreteSliderView.incrementValue=0;
    descreteSliderView.minimumValue=0;
    descreteSliderView.value=0;
    [descreteSliderView addTarget:self action:@selector(customSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    descreteSliderView.tintColor=kNavigationBarBackgroundColor;
    [textQuestionView addSubview:descreteSliderView];
    descreteSliderView.ticksListener=camelLabelsView;
    
    
    
//    rangeSlider = [[NMRangeSlider alloc] initWithFrame:frame];
//    [rangeSlider addTarget:self action:@selector(SliderChanged:) forControlEvents:UIControlEventValueChanged];
//
//    rangeSlider.minimumValue = 0;
//    rangeSlider.maximumValue = 50000;
//    rangeSlider.lowerValue = 0;
//    rangeSlider.upperValue = 50000;
//    rangeSlider.minimumRange = 0;
//    [textQuestionView addSubview:rangeSlider];
    
//    UILabel *lblSart = [[UILabel alloc]initWithFrame:CGRectMake (50, viewHeightCalculator+40, 50, 20)];
//    lblSart.font = MedRepElementDescriptionLabelFont;
//    lblSart.textColor=MedRepElementDescriptionLabelColor;
//    lblSart.text = @"0";
//    [textQuestionView addSubview:lblSart];
//
//    UILabel *lblEnd = [[UILabel alloc]initWithFrame:CGRectMake (895, viewHeightCalculator+40, 80, 20)];
//    lblEnd.font = MedRepElementDescriptionLabelFont;
//    lblEnd.textColor=MedRepElementDescriptionLabelColor;
//    lblEnd.text = @"50000";
//    [textQuestionView addSubview:lblEnd];
    
//    viewHeightCalculator = viewHeightCalculator+70;
    
//    UILabel *lblFrom = [[UILabel alloc]initWithFrame:CGRectMake (50, viewHeightCalculator, 50, 20)];
//    lblFrom.font = MedRepElementTitleLabelFont;
//    lblFrom.textColor=MedRepElementTitleLabelFontColor;
//    lblFrom.text = @"From";
//    [textQuestionView addSubview:lblFrom];
//
//    UILabel *lblTo = [[UILabel alloc]initWithFrame:CGRectMake (300, viewHeightCalculator, 50, 20)];
//    lblTo.font = MedRepElementTitleLabelFont;
//    lblTo.textColor=MedRepElementTitleLabelFontColor;
//    lblTo.text = @"To";
//    [textQuestionView addSubview:lblTo];
//
//    txtForm =[[UITextField alloc]init];
//    [txtForm setFrame:CGRectMake(50,viewHeightCalculator+30,150,40)];
//    txtForm.enabled = NO;
//    txtForm.font=MedRepElementDescriptionLabelFont;
//    txtForm.textColor=MedRepElementDescriptionLabelColor;
//    txtForm.layer.borderWidth=1.0f;
//    txtForm.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
//    txtForm.layer.cornerRadius=5.0;
//    txtForm.delegate = self;
//    [textQuestionView addSubview:txtForm];
//
//    txtTo =[[UITextField alloc]init];
//    [txtTo setFrame:CGRectMake(300,viewHeightCalculator+30,150,40)];
//    txtTo.enabled = NO;
//    txtTo.font=MedRepElementDescriptionLabelFont;
//    txtTo.textColor=MedRepElementDescriptionLabelColor;
//    txtTo.layer.borderWidth=1.0f;
//    txtTo.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
//    txtTo.layer.cornerRadius=5.0;
//    txtTo.delegate = self;
//    [textQuestionView addSubview:txtTo];
    
    viewHeightCalculator = viewHeightCalculator+100;
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 8;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
}

//// Handle control value changed events just like a normal slider
- (void)customSliderValueChanged:(TGPDiscreteSlider7*)sender
{
    UIView *textQuestionView = sender.superview;
    NSLog(@"Hi %ld",(long)textQuestionView.tag);
    [[self.view viewWithTag:textQuestionView.tag+1000]setHidden:NO];
}

//- (IBAction)SliderChanged:(NMRangeSlider*)sender
//{
//    UIView *textQuestionView = sender.superview;
//    NSLog(@"Hi %ld",(long)textQuestionView.tag);
//    [[self.view viewWithTag:textQuestionView.tag+1000]setHidden:NO];
//
//    if (rangeSlider.lowerValue < 10000)
//    {
//        txtForm.text = [NSString stringWithFormat:@"  0"];
//    }
//    else if ((rangeSlider.lowerValue >= 10000) && (rangeSlider.lowerValue <25000))
//    {
//        txtForm.text = [NSString stringWithFormat:@"  10000"];
//    }
//    else if ((rangeSlider.lowerValue >= 25000) && (rangeSlider.lowerValue <50000))
//    {
//        txtForm.text = [NSString stringWithFormat:@"  25000"];
//    }
//    else
//    {
//        txtForm.text = [NSString stringWithFormat:@"  50000"];
//    }
//    if (rangeSlider.upperValue < 10000)
//    {
//        txtTo.text = [NSString stringWithFormat:@"  0"];
//    }
//    else if ((rangeSlider.upperValue >= 10000) && (rangeSlider.upperValue <25000))
//    {
//        txtTo.text = [NSString stringWithFormat:@"  10000"];
//    }
//    else if ((rangeSlider.upperValue >= 25000) && (rangeSlider.upperValue <50000))
//    {
//        txtTo.text = [NSString stringWithFormat:@"  25000"];
//    }
//    else
//    {
//        txtTo.text = [NSString stringWithFormat:@"  50000"];
//    }
//}

#pragma mark create Check Box View
-(void)createViewForCheckBoxQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber
{
    int viewHeightCalculator = 0;
    
    SalesWorxDropShadowView *textQuestionView = [[SalesWorxDropShadowView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, 150)];
    textQuestionView.backgroundColor = [UIColor whiteColor];
    textQuestionView.layer.shadowColor = [UIColor blackColor].CGColor;
    textQuestionView.layer.shadowOffset = CGSizeMake(2, 2);
    textQuestionView.layer.shadowOpacity = 0.1;
    textQuestionView.layer.shadowRadius = 1.0;
    textQuestionView.tag = questionNumber;
    
    btnStatusForQ = [[UIButton alloc]initWithFrame:CGRectMake (0, viewHeightCalculator, 50, 50)];
    [btnStatusForQ setImage:[UIImage imageNamed:@"Survey_CheckStatus"] forState:UIControlStateNormal];
    [btnStatusForQ setTag:totalQuestions+questionNumber];
    btnStatusForQ.hidden = YES;
    [textQuestionView addSubview:btnStatusForQ];
    
    viewHeightCalculator = viewHeightCalculator+15;

    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (50, viewHeightCalculator, viewWidth-15, 30)];
    questionLabel.font = kSWX_FONT_REGULAR(18);
    
    NSString * QuestionText = [NSString stringWithFormat:@"%@",question.Question_Text];
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];

    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    
    SalesWorxItemDividerLabel *separatorView = [[SalesWorxItemDividerLabel alloc]init];// [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Divider_horizontal"]];
    separatorView.backgroundColor=kUIElementDividerBackgroundColor;
    separatorView.frame = CGRectMake (8, viewHeightCalculator+20, 992, 2);
    [textQuestionView addSubview:separatorView];
    
    viewHeightCalculator = viewHeightCalculator+40;
    
    
    NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
    NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
    NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
    for (NSMutableDictionary *customerDic in array) {
        SurveyResponse *customer = [SurveyResponse new];
        customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
        customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
        customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
        customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        [SurveyResponseArray addObject: customer];
    }
    //button 1
    for (int i= 0; i<SurveyResponseArray.count; i++) {
        SurveyResponse * response = [SurveyResponseArray objectAtIndex:i];
        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.frame = CGRectMake(40, viewHeightCalculator, 32, 34);
        UIImage *btnImage = [UIImage imageNamed:@"Survey_CheckboxInActive"];
        UIImage *btnImage1 = [UIImage imageNamed:@"Survey_CheckboxActive"];
        [imageButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [imageButton setBackgroundImage:btnImage1 forState:UIControlStateSelected];
        [imageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
        imageButton.tag =questionNumber  * 100 + [response.Response_ID intValue];
        NSLog(@"%ld",(long)imageButton.tag);
        if ([question.Default_Response_ID isEqualToString:response.Response_ID]) {
//            imageButton.selected =YES;
        }
        
        [textQuestionView addSubview:imageButton];
        
        UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (80, viewHeightCalculator, viewWidth-15, 20)];
        questionLabel.font=MedRepElementDescriptionLabelFont;
        questionLabel.textColor=MedRepElementDescriptionLabelColor;
        NSString * QuestionText = response.Response_Text;
        questionLabel.text = QuestionText;
        questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        questionLabel.numberOfLines = 0;
        CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
        
        CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:QuestionText fontName:questionLabel.font labelSize:maximumLabelSize];

        CGRect questionLabelOldFrame = questionLabel.frame;
        questionLabelOldFrame.size.height = expectedLabelSize.height;
        questionLabelOldFrame.size.width = expectedLabelSize.width;
        questionLabel.frame = questionLabelOldFrame;
        [textQuestionView addSubview:questionLabel];
        
        viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height+25;
    }
    
    viewHeightCalculator = viewHeightCalculator+10;
    
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 8;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame]; 
}

- (void)buttonClicked:(UIButton*)button
{
    int countSelectedCheckBoxes = 0;
    UIView *textQuestionView = button.superview;
    if (button.selected)
    {
        button.selected = NO;
    }else
    {
        button.selected = YES;
    }
  
    SurveyQuestion * question = [modifiedSurveyQuestionsArray objectAtIndex:textQuestionView.tag-1];
    if ([question.Response_Type_ID isEqualToString:@"3"]){
        
        NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
        NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
        NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
        for (NSMutableDictionary *customerDic in array) {
            SurveyResponse *customer = [SurveyResponse new];
            customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
            customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
            customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
            customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
            [SurveyResponseArray addObject: customer];
            
            
            UIButton * allButton  = (UIButton *)[textQuestionView viewWithTag:textQuestionView.tag * 100 + [customer.Response_ID intValue]];
            
            if (allButton.selected) {
                countSelectedCheckBoxes ++;
            }
        }
    }

    if (countSelectedCheckBoxes == 0)
    {
        [[self.view viewWithTag:textQuestionView.tag+1000]setHidden:YES];
    } else
    {
        [[self.view viewWithTag:textQuestionView.tag+1000]setHidden:NO];
    }
}

#pragma mark
- (IBAction)dismissButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    NSLog(@"%f",sender.contentOffset.x);
    if (sender.contentOffset.x != 0)
    {
        CGPoint offset = sender.contentOffset;
        offset.x = 0;
        sender.contentOffset = offset;
    }
}

#pragma mark button Save Action
-(void)Save
{
    [textAreaQuestionAnswerArray removeAllObjects];
    [checkBoxQuestionAnswerArray removeAllObjects];
    [radioQuestionAnswerArray removeAllObjects];
    [sliderQuestionAnswerArray removeAllObjects];
    
    [textAreaQuestionAnswerValidationArray removeAllObjects];
    [checkBoxQuestionAnswerValidationArray removeAllObjects];
    [radioQuestionAnswerValidationArray removeAllObjects];
    [sliderQuestionAnswerValidationArray removeAllObjects];
    
    for (SalesWorxDropShadowView * subVw in containerView.subviews) {
        if (![subVw isKindOfClass:[UILabel class]] && ![subVw isKindOfClass:[UIImageView class]]) {
            SurveyQuestion * question = [modifiedSurveyQuestionsArray objectAtIndex:subVw.tag-1];

            if ([question.Response_Type_ID isEqualToString:@"1"]) {
                
                UITextView * text  = (UITextView *)[subVw viewWithTag:subVw.tag * 100];
                NSLog(@"text %@",text.text);
                
                TextQuestionAnswer * answer = [TextQuestionAnswer new];
                answer.Question_ID = question.Question_ID;
                answer.Text = text.text;
                [textAreaQuestionAnswerArray addObject:answer];
            }
            else if ([question.Response_Type_ID isEqualToString:@"2"])// && [question.Question_ID intValue] == 1)
            {
                NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
                NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
                NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
                
                NSString * responseID;
                for (NSMutableDictionary *customerDic in array) {
                    SurveyResponse *customer = [SurveyResponse new];
                    customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
                    customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
                    customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
                    customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
                    [SurveyResponseArray addObject: customer];
                    
                    UIButton * button  = (UIButton *)[subVw viewWithTag:subVw.tag * 100 + [customer.Response_ID intValue]];
                    
                    if (button.selected) {
                        NSLog(@"Seletced");
                        responseID = customer.Response_ID;
                    }else{
                        NSLog(@"NotSeletced");
                    }
                }
                
                radioQuestionAnswer * radioAnswer = [radioQuestionAnswer new];
                radioAnswer.Question_ID = question.Question_ID;
                radioAnswer.Response_ID =responseID;
                [radioQuestionAnswerArray addObject:radioAnswer];
                
            }
            else if ([question.Response_Type_ID isEqualToString:@"3"])
            {
                NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
                NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
                NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
                
                NSMutableArray * idArray = [[NSMutableArray alloc]init];
                for (NSMutableDictionary *customerDic in array) {
                    SurveyResponse *customer = [SurveyResponse new];
                    customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
                    customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
                    customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
                    customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
                    [SurveyResponseArray addObject: customer];
                    
                    UIButton * button  = (UIButton *)[subVw viewWithTag:subVw.tag * 100 + [customer.Response_ID intValue]];
                    if (button.selected) {
                        NSLog(@"Seletced");
                        [idArray addObject:customer.Response_ID];
                    }else{
                        NSLog(@"NotSeletced");
                    }
                }
                CheckBoxQuestionAnswer * checkBoxAns = [CheckBoxQuestionAnswer new];
                checkBoxAns.Question_ID = question.Question_ID;
                checkBoxAns.ResponseIdArray = idArray;
                [checkBoxQuestionAnswerArray addObject:checkBoxAns];
            }
            else if ([question.Response_Type_ID isEqualToString:@"4"])
            {
                NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
                NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
                NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
                
                NSString * responseID;
                for (NSMutableDictionary *customerDic in array) {
                    SurveyResponse *customer = [SurveyResponse new];
                    customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
                    customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
                    customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
                    customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
                    [SurveyResponseArray addObject: customer];
                    
//                    if (txtTo.text.length != 0)
//                    {
//                        if ([txtTo.text intValue] < 10000 && [customer.Response_Text isEqualToString:@"< 10,000"])
//                        {
//                            responseID = customer.Response_ID;
//                        }
//                        else if (([txtTo.text intValue] >= 10000) && ([txtTo.text intValue] <25000) && [customer.Response_Text isEqualToString:@"10,000 - 25,000"])
//                        {
//                            responseID = customer.Response_ID;
//                        }
//                        else if (([txtTo.text intValue] >= 25000) && ([txtTo.text intValue] <50000) && [customer.Response_Text isEqualToString:@"25,000 - 50,000"])
//                        {
//                            responseID = customer.Response_ID;
//                        }
//                        else if([txtTo.text intValue] == 50000 && [customer.Response_Text isEqualToString:@"> 50,000"])
//                        {
//                            responseID = customer.Response_ID;
//                        }
//                    }
                }
                
                sliderQuestionAnswer * radioAnswer = [sliderQuestionAnswer new];
                radioAnswer.Question_ID = question.Question_ID;
                radioAnswer.Response_ID =responseID;
                [sliderQuestionAnswerArray addObject:radioAnswer];
            }
        }
    }
    
    NSInteger numberofQ = textAreaQuestionAnswerArray.count + checkBoxQuestionAnswerArray.count + radioQuestionAnswerArray.count + sliderQuestionAnswerArray.count;
    NSInteger numberofA = 0;
    
    if (textAreaQuestionAnswerArray.count > 0) {
        
        for (TextQuestionAnswer * answer in textAreaQuestionAnswerArray)
        {
            if ([answer.Text isEqualToString:@""]|| answer.Text==nil ||[answer.Text isEqual:[NSNull null]]){}
            else
            {
                numberofA++;
            }
        }
    }
    if (checkBoxQuestionAnswerArray.count > 0) {
        
        for (CheckBoxQuestionAnswer *answer in checkBoxQuestionAnswerArray)
        {
            for (NSInteger i = 0; i< answer.ResponseIdArray.count; i++)
            {
                if ([answer.ResponseIdArray objectAtIndex:i]==nil || [[answer.ResponseIdArray objectAtIndex:i] isEqual:[NSNull null]] ||[[answer.ResponseIdArray objectAtIndex:i] isEqualToString:@""]){}
                else
                {
                    numberofA++;
                    break;
                }
            }
        }
    }
    if (radioQuestionAnswerArray.count > 0) {
        
        for (radioQuestionAnswer *answer in radioQuestionAnswerArray)
        {
            if (answer.Response_ID==nil|| [answer.Response_ID isEqualToString:@""]){}
            else
            {
                numberofA++;
            }
        }
    }
    if (sliderQuestionAnswerArray.count > 0) {
        
        for (sliderQuestionAnswer *answer in sliderQuestionAnswerArray)
        {
            if (answer.Response_ID==nil|| [answer.Response_ID isEqualToString:@""]){}
            else
            {
                numberofA++;
            }
        }
    }
    
    
    if (numberofA == 0 || numberofA < numberofQ)
    {
        [self saveError];
    }
    else
    {
        if (textAreaQuestionAnswerArray.count > 0) {
            
            for (TextQuestionAnswer * answer in textAreaQuestionAnswerArray)
            {
                if ([answer.Text isEqualToString:@""]|| answer.Text==nil ||[answer.Text isEqual:[NSNull null]])
                {
                    
                }
                else
                {
                    [textAreaQuestionAnswerValidationArray addObject:answer.Text];
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Text];
                }
            }
        }
        if (checkBoxQuestionAnswerArray.count > 0) {
            
            for (CheckBoxQuestionAnswer *answer in checkBoxQuestionAnswerArray)
            {
                
                if(answer.ResponseIdArray!=nil && answer.ResponseIdArray.count>0){
                    
                    NSString * response=[answer.ResponseIdArray componentsJoinedByString:@","];
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:response];
                    [checkBoxQuestionAnswerValidationArray addObject:response];
                }
                
                
//                for (NSInteger i = 0; i< answer.ResponseIdArray.count; i++)
//                {
//                    if ([answer.ResponseIdArray objectAtIndex:i]==nil || [[answer.ResponseIdArray objectAtIndex:i] isEqual:[NSNull null]] ||[[answer.ResponseIdArray objectAtIndex:i] isEqualToString:@""])
//                    {
//                        
//                    }
//                    else
//                    {
//                        [checkBoxQuestionAnswerValidationArray addObject:[answer.ResponseIdArray objectAtIndex:i]];
//                        [self saveSurveyCustResponse:answer.Question_ID AndResponse:[answer.ResponseIdArray objectAtIndex:i]];
//                    }
//                }
            }
        }
        
        if (radioQuestionAnswerArray.count > 0) {
            
            for (radioQuestionAnswer *answer in radioQuestionAnswerArray)
            {
                if (answer.Response_ID==nil|| [answer.Response_ID isEqualToString:@""])
                {
                    
                }
                else
                {
                    NSLog(@"radio answer before inserting %@", [radioQuestionAnswerValidationArray description]);
                    [radioQuestionAnswerValidationArray addObject:answer.Response_ID];
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Response_ID];
                }
            }
        }
        if (sliderQuestionAnswerArray.count > 0) {
            
            for (sliderQuestionAnswer *answer in sliderQuestionAnswerArray)
            {
                if (answer.Response_ID==nil|| [answer.Response_ID isEqualToString:@""])
                {
                    
                }
                else
                {
                    NSLog(@"slider answer before inserting %@", [sliderQuestionAnswerValidationArray description]);
                    [sliderQuestionAnswerValidationArray addObject:answer.Response_ID];
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Response_ID];
                }
            }
        }
        
        NSLog(@"arrays to be saved are %@,%@,%@,%@",[textAreaQuestionAnswerArray description],[checkBoxQuestionAnswerArray description],[radioQuestionAnswerArray description],[sliderQuestionAnswerArray description]);
        
        [self saveAlert];
    }
}
-(void) saveSurveyCustResponse :(NSString *)questionID AndResponse :(NSString *)response
{
    NSDate * startDate = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *startDateString =[formatter stringFromDate:startDate];
    
    NSString * Customer_Survey_ID =[NSString createGuid];
    NSString* survey_ID= [NSString stringWithFormat:@"%d" ,appDelegate.surveyDetails.Survey_ID];
    NSString* salesRep_ID= [NSString stringWithFormat:@"%d",appDelegate.surveyDetails.SalesRep_ID];
    NSString* empCode=[[SWDefaults userProfile]stringForKey:@"Emp_Code"];
    NSString* site_Use_ID=[NSString stringWithFormat:@"%d",appDelegate.surveyDetails.Site_Use_ID ];
    NSString* Question_ID=[NSString stringWithFormat:@"%@", questionID];
    NSString* survey_By=[NSString stringWithFormat:@"%d", SurveyedBy];
    
    NSLog(@"survey by %@", survey_By);
    NSLog(@"new logs");
    NSLog(@"survey old  id %d", appDelegate.surveyDetails.Survey_ID);
    NSLog(@"question id is %@", questionID);
    NSLog(@"sales rep ID %d", appDelegate.surveyDetails.SalesRep_ID);
    NSLog(@"Emp code %@", [[SWDefaults userProfile]stringForKey:@"Emp_Code"]);
    NSLog(@"Customer id is %@", _customerID);
    NSLog(@"new survey type is %@", appDelegate.SurveyType);
    
    if ([appDelegate.SurveyType isEqualToString:@"N"])
    {
        NSString *temp = @"insert into TBL_Survey_Cust_Responses(Customer_Survey_ID,Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')";
        
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:Customer_Survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString: survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:Question_ID];
        
        
        
        if (!response || response==nil ||[response isEqual:[NSNull null]] || [response isEqualToString:@""]) {
            response=@" ";
            surveyIncomplete=YES;
            
            if (!saveError || ![[NSUserDefaults standardUserDefaults] boolForKey:@"alertShown"]) {
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"alertShown"];
                [self saveError];
            }
        }
        else{
            questionsCount=radioQuestionAnswerArray.count+checkBoxQuestionAnswerArray.count+textAreaQuestionAnswerArray.count+sliderQuestionAnswerArray.count;
            answersCount=radioQuestionAnswerValidationArray.count+checkBoxQuestionAnswerValidationArray.count+textAreaQuestionAnswerValidationArray.count+sliderQuestionAnswerValidationArray.count;
            
            NSLog(@"questions count is %lu", (unsigned long)questionsCount);
            NSLog(@"answers count is %lu", (unsigned long)answersCount);

            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:response];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:_customerID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString: site_Use_ID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:salesRep_ID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString: empCode];
            temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:startDateString];
            
            NSLog(@"temp query is %@", temp);
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InsertSurveyNotification" object:nil];
            
            if (!saveSuccess )
            {
//                if (answersCount>questionsCount || answersCount==questionsCount )
//                {
                    [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:temp];
//                    [self saveAlert];
//                }
            }
        }
    }
    else if (self.isBrandAmbassadorSurvey)
    {
       //insert into TBL_Survey_Walkin_Responses
        
       // NSString * insertWalkInResponseSurvey=@"insert into TBL_Survey_Walkin_Responses(Walkin_Response_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Survey_Timestamp,Walkin_Session_ID,Actual_Visit_ID); Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')";
        
      
        
        SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses *responses=[[SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses alloc]init];
        responses.Walkin_Response_ID=[NSString createGuid];
        responses.Survey_ID=survey_ID;
        responses.Question_ID=Question_ID;
        responses.Response=response;
        responses.SalesRep_ID=salesRep_ID;
        responses.Survey_Timestamp=startDateString;
        responses.Walkin_Session_ID=self.brandAmbassadorVisit.walkinCustomer.Walkin_Session_ID;
        responses.Actual_Visit_ID=self.brandAmbassadorVisit.walkinCustomer.Actual_Visit_ID;
        
        if (walkinSurveyResponses.count>0) {
            [walkinSurveyResponses addObject:responses];
        }
        else{
            walkinSurveyResponses=[[NSMutableArray alloc]init];
            [walkinSurveyResponses addObject:responses];
        }
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SurveyDidFinishNotification" object:walkinSurveyResponses];
        
        
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{1}" withString: survey_ID];
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{2}" withString:Question_ID];
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{3}" withString:response];
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{4}" withString:salesRep_ID];
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{5}" withString: startDateString];
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{6}" withString:self.brandAmbassadorVisit.walkinCustomer.Walkin_Session_ID];
//        insertWalkInResponseSurvey = [insertWalkInResponseSurvey stringByReplacingOccurrencesOfString:@"{7}" withString:self.brandAmbassadorVisit.Actual_Visit_ID];
//
//        NSLog(@"query for inserting survey walkin responses %@", insertWalkInResponseSurvey);
//        
//        BOOL status=[FMDBHelper executeNonQuery:insertWalkInResponseSurvey];
//        
//        if (status==YES) {
//            
//            NSLog(@"Survey walk in responses inserted successfully");
//        }
        
    }
    
    
    else{
        
        NSString * temp=@"insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By) Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')";
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:Customer_Survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString: survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:Question_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:response];
        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:salesRep_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString: empCode];
        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:startDateString];
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:survey_By];
        
        [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:temp];
        
        if ([response isEqual:@""]) {
            NSLog(@"captured");
        }
    }
}

#pragma mark Alert View Delegate Method

-(void)saveError
{
    saveError=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Survey InComplete", nil) message:NSLocalizedString(@"Please fill all details", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    saveError.tag=87952;
    
    [saveError show];
}

-(void)saveAlert
{
    
    
    [SWDefaults updateGoogleAnalyticsEvent:kSurveyCapturedEventName];
    saveSuccess=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Survey Completed",nil) message:NSLocalizedString(@"Survey saved successfully",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil)  otherButtonTitles:nil];
    saveSuccess.tag=87760;
    NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kSurveyTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                        object:self
                                                      userInfo:visitOptionDict];
    [saveSuccess show] ;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==87760 ) {
        
        if (buttonIndex==1)
        {
            if ([self.surveyParentLocation isEqualToString:@"Visit"])
            {
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
            }
            
            else
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        
        
    }
    else if (alertView.tag==87952)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"alertShown"];
    }
}
#pragma mark textView delegate methods

#define MAX_LENGTH 100
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView.text.length == 0 && text.length == 0)
    {
        [[self.view viewWithTag:textView.superview.tag+1000]setHidden:YES];
    }
    else if (textView.text.length == 1 && text.length == 0)
    {
        [[self.view viewWithTag:textView.superview.tag+1000]setHidden:YES];
    }
    else
    {
        [[self.view viewWithTag:textView.superview.tag+1000]setHidden:NO];
    }
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    NSString *newText = [ textView.text stringByReplacingCharactersInRange: range withString: text];
    if( [newText length]<= MAX_LENGTH ){
        return YES;
    }
    // case where text length > MAX_LENGTH
    textView.text = [ newText substringToIndex: MAX_LENGTH ];
    return NO;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    UIView *txtParentView = [self.view viewWithTag:textView.superview.tag];
    
    [scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DismissKeyboard:)]];
    
    CGSize kbSize = txtParentView.frame.size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    CGPoint scrollPoint = CGPointMake(0, txtParentView.frame.origin.y);
    [scrollView setContentOffset:scrollPoint animated:YES];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""])
    {
        textView.text = nil;
        [[self.view viewWithTag:textView.superview.tag+1000]setHidden:YES];
    }
    
//    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    scrollView.contentInset = contentInsets;
//    scrollView.scrollIndicatorInsets = contentInsets;
    
    [scrollView  setContentOffset:CGPointMake(0,0) animated:YES];
}
     
- (IBAction)DismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
