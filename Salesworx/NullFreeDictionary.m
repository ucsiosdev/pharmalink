//
//  NullFreeDictionary.m
//  Salesworx
//
//  Created by Unique Computer Systems on 4/8/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "NullFreeDictionary.h"

@implementation NullFreeDictionary

- (id)safeObjectForKey:(id)aKey {
    NSObject *object = self[aKey];
    
    if (object == [NSNull null]) {
        return nil;
    }
    
    return object;
}
@end
