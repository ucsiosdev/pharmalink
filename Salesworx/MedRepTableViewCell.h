//
//  MedRepTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UIImageView *cellImageView;
@property (strong, nonatomic) IBOutlet UIButton *randomButton;
@end
