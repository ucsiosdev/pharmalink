//
//  FBDatePicker.h
//  FormBuilder
//
//  Created by Irfan Bashir on 5/1/12.
//  Copyright (c) 2012 App ER. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWDatePicker;

@protocol SWDatePickerDelegate <NSObject>
@optional
- (void)datePickerSelectionDone:(SWDatePicker *)picker withDate:(NSDate *)date;
- (void)datePickerSelectionCancelled:(SWDatePicker *)picker;

@end

@interface SWDatePicker : UIView {
    UIDatePicker *datePicker;
    UIToolbar *toolbar;
    
    id <SWDatePickerDelegate> delegate;
}

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIToolbar *toolbar;
@property (unsafe_unretained) id <SWDatePickerDelegate> delegate;

@end