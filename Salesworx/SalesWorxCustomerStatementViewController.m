//
//  SalesWorxCustomerStatementViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//


/*
 
 ##################
 These tasks done by Ravinder kumar
 
 1. Listing showing wrong results
 2. Filter not working properly
 3. Add email functionality
 
 These method  added by Ravinder kumar
 Method Names:
 initObj,setNavigBar,isCustomerSelected,isSegmentControlAtFirstIndex,emailButtonTapped,disableEmailBarButton,disableEmailBarButton,generateStatementPDFDocument, handleEmailButtonState etc,
 
 NOTE  SOME EXISTING METHODS HAS BEEN ALSO CHANGED AS PER NEED
 
 
 ##################
*/


#import "SalesWorxCustomerStatementViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxCustomerStatementTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxDateHelper.h"
#import <objc/runtime.h>

@interface SalesWorxCustomerStatementViewController (){
   
     SalesWorxDateHelper * dateHelper;
     UIBarButtonItem* emailButton;
     CGRect invoicesViewFrame;
    
}
@end





@implementation SalesWorxCustomerStatementViewController
@synthesize arrCustomersStatement, selectedCustomer;

#pragma mark -VC LIFE CYCYLE

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initObj];
    
    [self setNavigBar];
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    viewFooter.backgroundColor = MedRepMenuTitleUnSelectedCellTextColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kCustomerStatementTitle localizedCapitalizedString], nil)];
    
    dateHelper=[[SalesWorxDateHelper alloc] init];
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    [DateRangeTextField SetCustomDateRangeText];
    
    lblTotalPaidAmount.text = [@"0" currencyString];
    lblTotalNetAmount.text = [@"0" currencyString];
    customerDuesArray=[[NSMutableArray alloc]init];
    [self configureSegmentControl];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
    pieChartPlaceholderImage.hidden = NO;
    parentViewOfPieChart.hidden = YES;
    
    arrCustomerName = [[NSMutableArray alloc]init];
    arrCustomersDetails = [[NSMutableArray alloc]init];
    
    NSMutableArray *customerListArray = [[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
    for (NSMutableDictionary *currentDict in customerListArray)
    {
        NSMutableDictionary *currentCustDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
        Customer * currentCustomer = [[Customer alloc] init];
        currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
        currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
        currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
        currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
        currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
        currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
        currentCustomer.City=[currentCustDict valueForKey:@"City"];
        currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
        currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
        currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
        currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
        currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
        currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
        currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
        currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
        currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
        currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
        currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
        currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
        currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
        currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
        currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
        currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
        currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
        
        [arrCustomersDetails addObject:currentCustomer];
        if ([currentCustomer.Customer_Name containsString:currentCustomer.Customer_No]) {
            [arrCustomerName addObject:[currentCustDict valueForKey:@"Customer_Name"]];
        } else {
            [arrCustomerName addObject:[[currentCustDict valueForKey:@"Customer_Name"] stringByAppendingString:[NSString stringWithFormat:@" [%@]",[currentCustDict valueForKey:@"Customer_No"]]]];
        }
    }
    if (self.isMovingToParentViewController){
        
        txtCustomer.text = [SWDefaults getValidStringValue:selectedCustomer.Customer_Name];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController)
    {
        customerPieChart.backgroundColor = [UIColor clearColor];
        customerPieChart.labelFont = [UIFont boldSystemFontOfSize:14];
        self.slicesForCustomerPotential = [[NSMutableArray alloc]init];
        
        customerPieChart.labelRadius = 25;
        [customerPieChart setDelegate:self];
        [customerPieChart setDataSource:self];
        [customerPieChart setPieCenter:CGPointMake(70, 65)];
        [customerPieChart setShowPercentage:YES];
        [customerPieChart setLabelColor:[UIColor whiteColor]];
        [customerPieChart setUserInteractionEnabled:NO];
        
        self.sliceColorsForCustomerPotential = [NSArray arrayWithObjects:
                                                [UIColor colorWithRed:74.0/255.0 green:145.0/255.0 blue:207.0/255.0 alpha:1],
                                                [UIColor colorWithRed:62.0/255.0 green:193.0/255.0 blue:194.0/255.0 alpha:1],
                                                nil];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            NSMutableArray * customerListArray=[[[SWDatabaseManager retrieveManager]fetchCustomerStatementData] mutableCopy];
            unfilteredCustomersStatementArray=[[NSMutableArray alloc]init];
            if (customerListArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary * currentDict in customerListArray)
                {
                    NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Customer_Statement *currentCustomer = [[Customer_Statement alloc] init];
                    
                    currentCustomer.Invoice_Ref_No = [currentCustDict valueForKey:@"Invoice_Ref_No"];
                    currentCustomer.NetAmount = [currentCustDict valueForKey:@"NetAmount"];
                    currentCustomer.PaidAmt = [currentCustDict valueForKey:@"PaidAmt"];
                    currentCustomer.InvDate = [currentCustDict valueForKey:@"InvDate"];
                    currentCustomer.InvDateWithTime = [currentCustDict valueForKey:@"InvDate"];
                    currentCustomer.Customer_ID = [currentCustDict valueForKey:@"Customer_ID"];
                    
                    //Ravinder code, 16 May 2019, added as per needs in next view controller (SalesWorxCustomerStatementPDFTemplateView)
                    currentCustomer.DueDate = [currentCustDict valueForKey:@"Due_Date"];
                    currentCustomer.DueAmount = [currentCustDict valueForKey:@"NetAmount"];
                    
                    [unfilteredCustomersStatementArray addObject:currentCustomer];
                }
            }
            //  unfilteredCustomersStatementArray = arrCustomersStatement;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
               
                [self handleEmailButtonState];
                
                
            });
        });
        
        
    }
    
    
}


#pragma mark -HELPER METHOD
-(void)initObj{
    
    customerDuesArray=[[NSMutableArray alloc]init];
    
    //invoices=[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetInvoicesOfCustomer:selectedCustomer.Customer_ID withSiteId:selectedCustomer.Site_Use_ID andShipCustID:selectedCustomer.Ship_Customer_ID withShipSiteId:selectedCustomer.Ship_Site_Use_ID andIsStatement:YES]];
}


-(void)setNavigBar{
    
    emailButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Email", nil) style:UIBarButtonItemStylePlain target:self action:@selector(emailButtonTapped)];
    [emailButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=emailButton;
}


-(BOOL)isCustomerSelected {
    
    if ((![txtCustomer.text isEqualToString:@"N/A"]) && (txtCustomer.text.length>0)) {
        return YES;
    }
    else {
        return NO;
    }
}


-(BOOL)isSegmentControlAtFirstIndex {
    
    if (customerStatementSegmentControl.selectedSegmentIndex==0){
         return YES;
    }
    else {
        return NO;
    }
}


-(void)emailButtonTapped{
    emailButton.title = NSLocalizedString(@"Preparing PDF Please Wait..", nil);
    [self disableEmailBarButton];
    [self performSelector:@selector(generateStatementPDFDocument) withObject:nil afterDelay:0.5];
}


-(void)disableEmailBarButton{
    [emailButton setEnabled:NO];
}

-(void)enableEmailBarButton{
    [emailButton setEnabled:YES];
}

-(void)generateStatementPDFDocument{
    
    
    AppControl *appControl = [AppControl retrieveSingleton];
    
    if ([self isSegmentControlAtFirstIndex]) {
        
        
        if (arrCustomersStatement.count > 300) {
            [self enableEmailBarButton];
            emailButton.title = NSLocalizedString(@"Email", nil);
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Selected data is too big size for email, Please change your filter criteria and try again" withController:self];
            
            return;
        }
       
        NSString *documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        // get the image path
        
        NSString *customerNo = @"";
        if (selectedCustomer.Customer_No != nil &&  selectedCustomer.Customer_No.length>0 ){
            customerNo = selectedCustomer.Customer_No;
        }
        
        
        NSString *documentPathComponent=[NSString stringWithFormat:@"CustomerSatement-%@.pdf",customerNo];
        NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        
        
        // make sure the file is removed if it exists
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:filename]) {
        }
        
        NSInteger numberOfPages = (arrCustomersStatement.count%15) == 0 ? (arrCustomersStatement.count/15) : (arrCustomersStatement.count/15)+1;
        NSMutableData *pdfData = [NSMutableData data];
        UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
        
        //creating dic for pass this SalesWorxCustomerStatementPDFTemplateView
        NSMutableDictionary *dicForPDFTemplateView =  [[NSMutableDictionary alloc]init];
        
        if (selectedCustomer != nil) {
            [dicForPDFTemplateView setValue:selectedCustomer.Customer_Name forKey:@"Customer_Name"];
            [dicForPDFTemplateView setValue:selectedCustomer.Avail_Bal forKey:@"Avail_Bal"];
        }
        else {
            [dicForPDFTemplateView setValue:@"All" forKey:@"Customer_Name"];
            [dicForPDFTemplateView setValue:@"N/A" forKey:@"Avail_Bal"];
        }
        
        
        for (NSInteger i=0; i< numberOfPages ; i ++) {
        
            SalesWorxCustomerStatementPDFTemplateView * PageTemplate =[[SalesWorxCustomerStatementPDFTemplateView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
            
            /*
            ############################
            
              Set here more key in NSDictionary(dicForPDFTemplateView) and just pass to  SalesWorxCustomerStatementPDFTemplateView as per your need
            
             Hint: will use in this SalesWorxCustomerAgeingReportPDFTemplateView class
             [selectedCustomer valueForKey:@"Customer_Name”];
             [selectedCustomer valueForKey:@"Avail_Bal"]
             
            ############################
             
            */
            
            
            
            PageTemplate.isDataIsModelFromPerviousVC = YES;
            PageTemplate.selectedCustomer = dicForPDFTemplateView;
            PageTemplate.arrCustomersStatement = arrCustomersStatement;
            PageTemplate.pageNumber =i+1;
            
            [PageTemplate UpdatePdfPgae];
            
            UIGraphicsBeginPDFPage();
            CGContextRef pdfContext = UIGraphicsGetCurrentContext();
            [PageTemplate.layer renderInContext:pdfContext];
        }
        
        UIGraphicsEndPDFContext();
        emailButton.title = NSLocalizedString(@"Email", nil);
        if ([MFMailComposeViewController canSendMail]){
            MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
            mailController.mailComposeDelegate = self;
            [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
            
            NSString *customerName = @"";
            if (selectedCustomer.Customer_Name != nil ) {
                customerName = [NSString stringWithFormat:@"- %@",selectedCustomer.Customer_Name] ;
                
            }
            
            
           
            if ([appControl.POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]) {
                
                NSString *appUserEmailAddress = [[SWDatabaseManager retrieveManager] fetchEmailAddressViaCustomerID:selectedCustomer.Customer_ID];
                [mailController setToRecipients:@[appUserEmailAddress]];
            }
            
            
            [mailController setSubject:[NSString stringWithFormat:@"Customer Statement %@",customerName]];
            [mailController setMessageBody:@"Hi,\n Please find the attached statement." isHTML:NO];
            
            
            NSString *customerNo = @"";
            if (selectedCustomer.Customer_No != nil) {
                customerNo = [NSString stringWithFormat:@"-%@",selectedCustomer.Customer_No];
            }
            [mailController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"CustomerSatement%@.pdf",customerNo]];
            
            [self presentViewController:mailController animated:YES completion:nil];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
            [self enableEmailBarButton];
        }
    }
    else{
        
        if (customerDuesArray.count > 300) {
            [self enableEmailBarButton];
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Selected data is too big size for email, Please change your filter criteria and try again" withController:self];
            return;
        }
        
        NSString *documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        // get the image path
        NSString *documentPathComponent=[NSString stringWithFormat:@"CustomerAgeingReport-%@.pdf",selectedCustomer.Customer_No];
        NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        
        
        // make sure the file is removed if it exists
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:filename]) {
        }
        
        NSInteger numberOfPages = (customerDuesArray.count%15) == 0 ? (customerDuesArray.count/15) : (customerDuesArray.count/15)+1;
        NSMutableData *pdfData = [NSMutableData data];
        UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
        
        
       
        NSMutableDictionary *dicForPDFTemplateView =  [[NSMutableDictionary alloc]init];
       
        if (selectedCustomer != nil ){
            [dicForPDFTemplateView setValue:selectedCustomer.Customer_Name forKey:@"Customer_Name"];
            [dicForPDFTemplateView setValue:selectedCustomer.Avail_Bal forKey:@"Avail_Bal"];
        }
        else {
            [dicForPDFTemplateView setValue:@"All" forKey:@"Customer_Name"];
            [dicForPDFTemplateView setValue:@"N/A" forKey:@"Avail_Bal"];
        }
      
        
        for (NSInteger i=0; i< numberOfPages ; i ++) {
            SalesWorxCustomerAgeingReportPDFTemplateView * PageTemplate =[[SalesWorxCustomerAgeingReportPDFTemplateView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
            
            
            /*
             ############################
             Set here more key in NSDictionary(dicForPDFTemplateView) and just pass to  SalesWorxCustomerStatementPDFTemplateView as per your need
             
             Hint: will use in this SalesWorxCustomerAgeingReportPDFTemplateView class
             [selectedCustomer valueForKey:@"Customer_Name”];
             [selectedCustomer valueForKey:@"Avail_Bal"]
             
             ############################
             */
             
            PageTemplate.selectedCustomer = dicForPDFTemplateView;
            PageTemplate.arrCustomersAgeingReport = customerDuesArray;
            PageTemplate.pageNumber =i+1;
            
            [PageTemplate UpdatePdfPgae];
            UIGraphicsBeginPDFPage();
            CGContextRef pdfContext = UIGraphicsGetCurrentContext();
            [PageTemplate.layer renderInContext:pdfContext];
        }
        
        UIGraphicsEndPDFContext();
       
        emailButton.title = NSLocalizedString(@"Email", nil);
      
        if ([MFMailComposeViewController canSendMail]){
            MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
            mailController.mailComposeDelegate = self;
            [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
            
           
            if ([appControl.POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]) {
                
                NSString *appUserEmailAddress = [[SWDatabaseManager retrieveManager] fetchEmailAddressViaCustomerID:selectedCustomer.Customer_ID];
                [mailController setToRecipients:@[appUserEmailAddress]];
            }
            
            [mailController setSubject:[NSString stringWithFormat:@"Customer ageing report - %@",selectedCustomer.Customer_Name]];
            [mailController setMessageBody:@"Hi,\n Please find the attached report." isHTML:NO];
            [mailController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"CustomerAgeingReport-%@.pdf",selectedCustomer.Customer_No]];
            [self presentViewController:mailController animated:YES completion:nil];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
            [self enableEmailBarButton];
        }
    }
}


#pragma mark -MFMailComposeViewController Delegate Method
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        [self enableEmailBarButton];
    }];
}



#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    
      if (([self isSegmentControlAtFirstIndex]) && (tableView == tblCustomerStatement)) {
       
          if ([self isCustomerSelected]) {
             return arrCustomersStatement.count;
          }
          else {
              return 0;
          }
     }
      else{
          
          if ((customerDuesArray.count>0) && ([self isCustomerSelected]) && (tableView == ageingReportTableView)) {
            return  7;
          }
       else {
           return 0;
       }
     }

    
    /*
    if ((customerStatementSegmentControl.selectedSegmentIndex == 0) && (tableView == tblCustomerStatement)){
        return arrCustomersStatement.count;
     }
        
    else {
        
        if (customerDuesArray.count>0)  {
            return 7;
        }
        
        else{
            return 0;
        }
        
    }
     
     */
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tblCustomerStatement)
    {
        return 49.0f;
    }
    else
    {
        return 44.0f;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == tblCustomerStatement)
    {
        return 49.0f;
    }
    else
    {
        return 44.0f;
    }}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(customerStatementSegmentControl.selectedSegmentIndex==0 && tableView == tblCustomerStatement)
    {
    static NSString* identifier=@"customerStatement";
    SalesWorxCustomerStatementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerStatementTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;

    cell.lblDocumentNo.isHeader=YES;
    cell.lblInvoiceDate.isHeader=YES;
    cell.lblNetAmount.isHeader=YES;
    cell.lblPaidAmount.isHeader=YES;
    
    cell.lblDocumentNo.text = NSLocalizedString(@"Document No", nil);
    cell.lblInvoiceDate.text= NSLocalizedString(@"Invoice Date",nil);
    cell.lblNetAmount.text=NSLocalizedString(@"Net Amount", nil);
    cell.lblPaidAmount.text=NSLocalizedString(@"Paid Amount", nil);
    
    return cell;
    }
    else
    {
        static NSString* identifier=@"duesCell";
        SalesWorxCustomerDuesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerDuesTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        NSLog(@"\n\n ** cell frame is %@  **\n\n",NSStringFromCGRect(tableView.frame));
        
        CGRect sepFrame = CGRectMake(0,43, tableView.frame.size.width, 1);
        UIView*  seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = kUITableViewSaperatorColor;
        [cell addSubview:seperatorView];
        
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
        cell.titleLbl.isHeader=YES;
        cell.descLbl.isHeader=YES;
        cell.titleLbl.text=NSLocalizedString(@"Period", nil);
        cell.descLbl.text=NSLocalizedString(@"Outstanding", nil);
        
        return cell;
        
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (customerStatementSegmentControl.selectedSegmentIndex == 0 && tableView == tblCustomerStatement)
    {
    static NSString* identifier=@"customerStatement";
    SalesWorxCustomerStatementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerStatementTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Customer_Statement *data = [arrCustomersStatement objectAtIndex:indexPath.row];
    cell.lblDocumentNo.text = data.Invoice_Ref_No;
    cell.lblInvoiceDate.text = [NSString stringWithFormat:@"%@", [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:data.InvDateWithTime]];
    cell.lblNetAmount.text = [[NSString stringWithFormat:@"%@",data.NetAmount] currencyString];
    cell.lblPaidAmount.text = [[NSString stringWithFormat:@"%@",data.PaidAmt] currencyString];
    
    return cell;
    }
    else
    
    {
     
     NSMutableDictionary* duesDict=[customerDuesArray objectAtIndex:0];
     
     static NSString* identifier=@"duesCell";
     SalesWorxCustomerDuesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
     if(cell == nil) {
     cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerDuesTableViewCell" owner:nil options:nil] firstObject];
     cell.selectionStyle=UITableViewCellSelectionStyleNone;
     }
     
     NSMutableArray * prevoiusMonthsArray=[[NSMutableArray alloc]init];
     
     BOOL ageingReportDisplayFormatMonthly=[SWDefaults isAgeingReportDisplayFormatMonthly];
     if (ageingReportDisplayFormatMonthly) {
     prevoiusMonthsArray=[SWDefaults fetchPreviousMonthsforAgeingReport];
     }
     
     if(indexPath.row==0)
     {
     if (ageingReportDisplayFormatMonthly) {
     [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:0]];
     }
     else{
     [cell.titleLbl setText:NSLocalizedString(@"30 Days", nil)];
     }
     [cell.descLbl setText:[duesDict currencyStringForKey:@"M0_Due"]];
     }
     else  if(indexPath.row==1)
     {
     if (ageingReportDisplayFormatMonthly) {
     [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:1]];
     }
     else{
     [cell.titleLbl setText:NSLocalizedString(@"60 Days", nil)];
     }
     [cell.descLbl setText:[duesDict currencyStringForKey:@"M1_Due"]];
     }
     else  if(indexPath.row==2)
     {
     if (ageingReportDisplayFormatMonthly) {
     [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:2]];
     }
     else{
     [cell.titleLbl setText:NSLocalizedString(@"90 Days", nil)];
     }
     [cell.descLbl setText:[duesDict currencyStringForKey:@"M2_Due"]];
     }
     else  if(indexPath.row==3)
     {
     if (ageingReportDisplayFormatMonthly) {
     [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:3]];
     }
     else{
     [cell.titleLbl setText:NSLocalizedString(@"120 Days", nil)];
     }
     [cell.descLbl setText:[duesDict currencyStringForKey:@"M3_Due"]];
     }
     else  if(indexPath.row==4)
     {
     if (ageingReportDisplayFormatMonthly) {
     [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:4]];
     }
     else{
     [cell.titleLbl setText:NSLocalizedString(@"150 Days", nil)];
     }
     [cell.descLbl setText:[duesDict currencyStringForKey:@"M4_Due"]];
     }
     else if (indexPath.row==5)
     {
     if (ageingReportDisplayFormatMonthly) {
     [cell.titleLbl setText:[prevoiusMonthsArray objectAtIndex:5]];
     }
     else{
     [cell.titleLbl setText:NSLocalizedString(@"180 Days", nil)];
     }
     [cell.descLbl setText:[duesDict currencyStringForKey:@"M5_Due"]];
     
     }
     else  if(indexPath.row==6)
     {
     [cell.titleLbl setText:NSLocalizedString(@"Prior Due", nil)];
     [cell.descLbl setText:[duesDict currencyStringForKey:@"Prior_Due"]];
     }
     
     
     return cell;
     
     }
}


#pragma  mark textfield methods
-(BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{
    if(textField == txtCustomer)
    {
        [self textFieldTapped];
        return NO;
    }
    else if(textField == DateRangeTextField)
    {
        SalesWorxReportsDateSelectorViewController * popOverVC=[[SalesWorxReportsDateSelectorViewController alloc]init];
        popOverVC.delegate=self;
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(450, 570);
        popover.delegate = self;
        popover.sourceView = DateRangeTextField.rightView;
        popover.sourceRect = DateRangeTextField.dropdownImageView.frame;
        popover.permittedArrowDirections = UIPopoverArrowDirectionLeft;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
        return NO;
    }
    return YES;
}

#pragma  mark textfield tapped method

-(void)textFieldTapped
{
    txtCustomer.enabled = YES;
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = arrCustomerName;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=NO;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(400, 273);
    popover.delegate = self;
    popOverVC.titleKey = kCustomerNamePopOverTitle;
    popover.sourceView = txtCustomer.rightView;
    popover.sourceRect =txtCustomer.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    
    [self presentViewController:nav animated:YES completion:^{
    }];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
   
    txtCustomer.text = [NSString stringWithFormat:@"%@",[arrCustomerName objectAtIndex:selectedIndexPath.row]];
    selectedCustomer = [arrCustomersDetails objectAtIndex:selectedIndexPath.row];
    //[self fetchAgeingReportData];
    
   //arrCustomersDetails
}
#pragma mark Pie chart delegate methods

-(void)loadPieChart:(NSArray *)arr
{
    if ([arr count] == 0) {
        pieChartPlaceholderImage.hidden = NO;
        parentViewOfPieChart.hidden = YES;
    } else {
        pieChartPlaceholderImage.hidden = YES;
        parentViewOfPieChart.hidden = NO;
    }
    
    [_slicesForCustomerPotential removeAllObjects];
    [customerPieChart reloadData];
   
    
    double netAmount = 0, paidAmount = 0;
    for (Customer_Statement *currentCustomer in arr) {
        netAmount = netAmount + [currentCustomer.NetAmount doubleValue];
        paidAmount = paidAmount + [currentCustomer.PaidAmt doubleValue];
    }
    
    double unPaidMaount=netAmount-paidAmount;
    
    [_slicesForCustomerPotential removeAllObjects];
    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:paidAmount]];
    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:unPaidMaount]];
    [customerPieChart reloadData];
    
    lblTotalPaidAmount.text = [[NSString stringWithFormat:@"%f",paidAmount]currencyString];
    lblTotalNetAmount.text = [[NSString stringWithFormat:@"%f",netAmount]currencyString];
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    if (pieChart == customerPieChart && self.slicesForCustomerPotential.count>0) {
        count = self.slicesForCustomerPotential.count;
    }
    return count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    if (pieChart == customerPieChart) {
        count = [[NSString stringWithFormat:@"%@",[self.slicesForCustomerPotential objectAtIndex:index]]floatValue];
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == customerPieChart) {
        colour = [self.sliceColorsForCustomerPotential objectAtIndex:(index % self.sliceColorsForCustomerPotential.count)];
    }
    return colour;
}


#pragma mark -Action Button
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btnSearch:(id)sender {
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    if ([self isCustomerSelected]) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF.Customer_Name",txtCustomer.text];
        NSMutableArray *filteredArray = [[arrCustomersDetails filteredArrayUsingPredicate:predicate] mutableCopy];

        if ([filteredArray count] > 0) {
            Customer *selectdCustomer = [filteredArray objectAtIndex:0];
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"Customer_ID == %d",[selectdCustomer.Customer_ID intValue]]];
        }
    }
    else{
        
        if (([self isSegmentControlAtFirstIndex])){
            [arrCustomersStatement removeAllObjects];
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select customer" withController:self];
            
        }
        else {
             [customerDuesArray removeAllObjects];
             [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select customer" withController:self];
        }
        return;
    }
  
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"(InvDate >= %@) AND (InvDate <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]]];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"\n\n*** predicate is %@   ***\n\n", [predicate description]);
    
    NSMutableArray* filteredArray = [[unfilteredCustomersStatementArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    [self fetchAgeingReportData];
    
    if ((filteredArray.count == 0) && ([self isSegmentControlAtFirstIndex])){
        
        
        
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
        
        
     }
    else if ((customerDuesArray.count ==0) && (![self isSegmentControlAtFirstIndex])){
        
        
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
       
    }
    

    arrCustomersStatement = filteredArray;
   
    [self loadPieChart:arrCustomersStatement];
    
    [tblCustomerStatement reloadData];
    
    [ageingReportTableView reloadData];
    
    [self handleEmailButtonState];
    
}

-(void)handleEmailButtonState{
 
    // 1st tab customer report
    if ([self isSegmentControlAtFirstIndex]) {
     
        if ((arrCustomersStatement.count > 0) && ([self isCustomerSelected])) {
           [self enableEmailBarButton];
     }
     else {
          [self disableEmailBarButton];
     }
   }
 
    // 2nd tab Ageing report
    else {
    
      if ((customerDuesArray.count > 0) && ([self isCustomerSelected])) {
          [self enableEmailBarButton];
       }
       else {
            [self disableEmailBarButton];
       }
   }
    
        
}

- (IBAction)btnReset:(id)sender {
   
    txtCustomer.text = @"N/A";
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kDateFormatWithoutTime];
    [df setLocale:KUSLOCALE];
   
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    DateRangeTextField.SelecetdCustomDateRangeText = nil;
    [DateRangeTextField SetCustomDateRangeText];
    
    
    /*
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(InvDate >= %@) AND (InvDate <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]];
    NSMutableArray* filteredArray = [[unfilteredCustomersStatementArray filteredArrayUsingPredicate:predicate] mutableCopy];
    arrCustomersStatement = filteredArray;
    
    */
    
    [arrCustomersStatement removeAllObjects];
    
    [customerDuesArray removeAllObjects];
   
  
    
    [tblCustomerStatement reloadData];
    
    [ageingReportTableView reloadData];
    
    [self loadPieChart:arrCustomersStatement];
    
    customerStatementSegmentControl.selectedSegmentIndex = 0;
    
    [self.view sendSubviewToBack:ageingReportView];
    
    [self handleEmailButtonState];
}

#pragma mark date picker method

-(void)DidUserSelectDateRange:(NSDate *)StartDate EndDate:(NSDate *)EndDate AndCustomRangePeriodText:(NSString *)customPeriodText{
    
    DateRangeTextField.StartDate = StartDate;
    DateRangeTextField.EndDate = EndDate;
    DateRangeTextField.SelecetdCustomDateRangeText = customPeriodText;
    
    [DateRangeTextField SetCustomDateRangeText];
    
    NSLog(@"\n\n ** StartDateStr %@  **\n\n",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate]);
    NSLog(@"\n\n ** EndDateStr %@    **\n\n",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Segment control methods
-(void)fetchAgeingReportData
{
    customerDuesArray=[[SWDatabaseManager retrieveManager] fetchDuesForCustomer:selectedCustomer];
    NSMutableDictionary* duesDict = [[NSMutableDictionary alloc]init];
    duesDict=[[NSMutableDictionary alloc]init];
    if (customerDuesArray.count>0) {
        duesDict=[customerDuesArray objectAtIndex:0];
    }
    NSLog(@"\n\n ** customer dues array in statement %@ **\n\n",customerDuesArray);
   
    //[ageingReportTableView reloadData];
    
}
-(void)configureSegmentControl
{
    
    customerStatementSegmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    customerStatementSegmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    customerStatementSegmentControl.sectionTitles = @[NSLocalizedString(@"Customer Statement", nil), NSLocalizedString(@"Ageing Report", nil)];
    
    customerStatementSegmentControl.selectionIndicatorBoxOpacity=0.0f;
    customerStatementSegmentControl.verticalDividerEnabled=YES;
    customerStatementSegmentControl.verticalDividerWidth=1.0f;
    customerStatementSegmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    customerStatementSegmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    customerStatementSegmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    customerStatementSegmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    customerStatementSegmentControl.tag = 3;
    customerStatementSegmentControl.selectedSegmentIndex = 0;
    
    
    customerStatementSegmentControl.layer.shadowColor = [UIColor blackColor].CGColor;
    customerStatementSegmentControl.layer.shadowOffset = CGSizeMake(2, 2);
    customerStatementSegmentControl.layer.shadowOpacity = 0.1;
    customerStatementSegmentControl.layer.shadowRadius = 1.0;
    
    
    [customerStatementSegmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
}


#pragma mark -HMSegmentedControl Methods
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    // NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    if (customerStatementSegmentControl.selectedSegmentIndex==0) {
        [tblCustomerStatement reloadData];
        [self handleEmailButtonState];
        [self.view sendSubviewToBack:ageingReportView];
    }
    else if (segmentedControl.selectedSegmentIndex==1){
        [ageingReportTableView reloadData];
        [self handleEmailButtonState];
        [self.view bringSubviewToFront:ageingReportView];
        
    }
    
    
}

@end

/*
Testing purpose
 -(NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj{
 
 NSMutableDictionary *dict = [NSMutableDictionary dictionary];
 
 unsigned count;
 objc_property_t *properties = class_copyPropertyList([obj class], &count);
 
 for (int i = 0; i < count; i++) {
 NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
 Class classObject = NSClassFromString([key capitalizedString]);
 if (classObject) {
 id subObj = [self dictionaryWithPropertiesOfObject:[obj valueForKey:key]];
 [dict setObject:subObj forKey:key];
 }
 else
 {
 id value = [obj valueForKey:key];
 if(value) [dict setObject:value forKey:key];
 }
 }
 
 free(properties);
 
 return [NSDictionary dictionaryWithDictionary:dict];
 }
 */


/*

 Testing purpose
 - (void)getSalesOrderServiceDidGetAllInvoices
 {
 
 invoices=[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetInvoicesOfCustomer:selectedCustomer.Customer_ID withSiteId:selectedCustomer.Site_Use_ID andShipCustID:selectedCustomer.Ship_Customer_ID withShipSiteId:selectedCustomer.Ship_Site_Use_ID andIsStatement:YES]];
 
 
 NSMutableArray *arrFoundIndex = [[NSMutableArray alloc]init];
 
 
 for (int i = 0; i < invoices.count; i++) {
 
 BOOL isIndexFound = NO;
 
 NSDictionary *row = [invoices objectAtIndex:i];
 NSString *invoiceRefNo_1 = [row valueForKey:@"Invoice_Ref_No"];
 
 for (int j = 0; j < arrCustomersStatement.count; j++) {
 
 NSString *invoiceReftNo_2 = [[arrCustomersStatement objectAtIndex:j]valueForKey:@"Invoice_Ref_No"];
 
 if ([invoiceRefNo_1 isEqualToString:invoiceReftNo_2]){
 isIndexFound = YES;
 break;
 
 }
 else {
 isIndexFound = NO;
 }
 }
 
 if (isIndexFound==NO){
 [arrFoundIndex addObject:[NSNumber numberWithInt:i]];
 }
 
 }
 
 NSLog(@"\n\n  will be delete from invoices array: %@  **\n\n ",arrFoundIndex);
 
 
 for (NSInteger k=arrFoundIndex.count-1;  k>=0; k--) {
 
 NSInteger indexWillDelete = [[arrFoundIndex objectAtIndex:k]integerValue];
 
 [invoices removeObjectAtIndex:indexWillDelete];
 
 }
 
 
 
 NSLog(@"\n\n final filter invoices array: %@  **\n\n ",invoices);
 
 /*
 for (int i = 0; i < invoices.count; i++) {
 @autoreleasepool{
 NSDictionary *row = [invoices objectAtIndex:i];
 double amount = [[row objectForKey:@"NetAmount"] doubleValue];
 
 _total = _total + amount;
 }
 
 */




