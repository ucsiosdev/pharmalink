//
//  MedRepTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "MedRepDefaults.h"

@implementation MedRepTableViewCell

@synthesize randomButton;

- (void)awakeFromNib {
    // Initialization code
    
    
    randomButton.layer.cornerRadius = 25;
    randomButton.layer.borderWidth = 1.0;
    randomButton.layer.masksToBounds = YES;
    UIColor* buttonBGColor=[MedRepDefaults generateRandomColor];
    randomButton.backgroundColor=buttonBGColor;
    
    [randomButton setBackgroundImage:[self imageWithColor:buttonBGColor] forState:UIControlStateHighlighted];
    [randomButton setBackgroundImage:[self imageWithColor:buttonBGColor] forState:UIControlStateSelected];
    

    randomButton.layer.borderColor = buttonBGColor.CGColor;
    
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}



- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
