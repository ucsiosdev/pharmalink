//
//  SalesWorxAboutUsTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 12/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxAboutUsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCell;

@end
