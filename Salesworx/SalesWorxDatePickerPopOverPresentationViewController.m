//
//  SalesWorxDatePickerPopOverPresentationViewController.m
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 2/6/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxDatePickerPopOverPresentationViewController.h"
#import "SWDefaults.h"
@interface SalesWorxDatePickerPopOverPresentationViewController ()

@end

@implementation SalesWorxDatePickerPopOverPresentationViewController
@synthesize datePickerMaximumDate,datePickerMinimumDate,datePickerMode,previousSelectedDate;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:YES];
    [self CustomizeNavigationBar];
    [self InitializeDatePikcer];
    
    if(datePickerMaximumDate != nil){
        salesWorxDatePicker.maximumDate = datePickerMaximumDate;
    }
    
    if(datePickerMinimumDate != nil){
        salesWorxDatePicker.minimumDate = datePickerMinimumDate;
    }

//    if(datePickerMode != UIDatePickerModeDate){
//        salesWorxDatePicker.datePickerMode = datePickerMode;
//    }

    if(previousSelectedDate != nil){
        salesWorxDatePicker.date = previousSelectedDate;
    }else{
        salesWorxDatePicker.date = [NSDate date];
    }
}
-(void)InitializeDatePikcer{
    salesWorxDatePicker.datePickerMode = UIDatePickerModeDate;
}
-(void)CustomizeNavigationBar{
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:_titleString];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    /**Save Button*/
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDateTapped)];
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    saveBtn.tintColor=[UIColor whiteColor];
    
    
    /**Close Button*/
    UIBarButtonItem * closeBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    [closeBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                            forState:UIControlStateNormal];
    closeBtn.tintColor=[UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem=saveBtn;
    self.navigationItem.leftBarButtonItem=closeBtn;
}


-(void)closeButtonTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveDateTapped{
    
    if(self.datePickerPopOverPresentationViewControllerDelegate != nil ){
        [self dismissViewControllerAnimated:YES completion:^{
            [self.datePickerPopOverPresentationViewControllerDelegate datePicket:salesWorxDatePicker didSelectDate:salesWorxDatePicker.date];
        }];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
