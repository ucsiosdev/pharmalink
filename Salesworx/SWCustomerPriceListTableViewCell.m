//
//  SWCustomerPriceListTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWCustomerPriceListTableViewCell.h"

@implementation SWCustomerPriceListTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIsHeader:(BOOL)isHeader
{
    if (isHeader==YES) {
        
        CGRect sepFrame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1);
        UIView*  seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = kUITableViewSaperatorColor;
        [self addSubview:seperatorView];
    }
}
@end
