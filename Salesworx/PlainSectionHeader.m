//
//  PlainSectionHeader.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "PlainSectionHeader.h"
#import "SWFoundation.h"

@implementation PlainSectionHeader

@synthesize headerImage;
@synthesize sectionText;

- (id)initWithWidth:(CGFloat)width text:(NSString *)text {
    self = [super initWithFrame:CGRectMake(0, 0, width, 28)];
    
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
        [self setHeaderImage:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PlainSectionHeader"]] ];
        [self.headerImage setContentMode:UIViewContentModeScaleToFill];
        
        [self setSectionText:[[UILabel alloc] initWithFrame:CGRectMake(10, 2, width - 20, 24)] ];
        [self.sectionText setText:text];
        [self.sectionText setBackgroundColor:[UIColor clearColor]];
        [self.sectionText setTextColor:[UIColor whiteColor]];
        [self.sectionText setShadowOffset:CGSizeMake(0, 1)];
        [self.sectionText setShadowColor:[UIColor darkGrayColor]];
        [self.sectionText setFont:BoldFontOfSize(18.0f)];
        
        [self addSubview:self.headerImage];
        [self addSubview:self.sectionText];
    }
    
    return self;
}



- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.headerImage setFrame:CGRectMake(0, 0, self.bounds.size.width, 28)];
    [self.sectionText setFrame:CGRectMake(10, 2, self.bounds.size.width - 20, 24)];
}

@end
