//
//  SalesWorxRouteViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxRouteViewController.h"
#import "Reachability.h"

@interface SalesWorxRouteViewController ()<UIPopoverPresentationControllerDelegate, UIPopoverControllerDelegate,UITableViewDelegate,UITableViewDataSource>

@end

@implementation SalesWorxRouteViewController

@synthesize btnDate,btnPlusDate,btnMinusDate,customerTableView,segmentControl,customersScrollView,locationView,datePickerView,selectedCustomer;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    customerTableView.delegate=self;
    appControl = [AppControl retrieveSingleton];
    
    customersArray=[[NSMutableArray alloc]init];
    [self fetchCustomersList];
    self.view.backgroundColor=UIViewBackGroundColor;
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Route"];
    [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesRouteScreenName];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(GetRouteDataFromBackground:) name:@"GetRouteDataFromBackground" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    // Do any additional setup after loading the view from its nib.
    
    
    
    customersDataDicsArray = [[NSMutableArray alloc]init];
    [SWDefaults clearCustomer];
    
    isAppRefresh = NO;
    
    selecDate=[NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:kDateFormatWithoutTime];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [btnDate setTitle:[formatter stringFromDate:selecDate] forState:UIControlStateNormal];
    btnDate.titleLabel.font=kSWX_FONT_SEMI_BOLD(18);
    
    
    
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        //self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        //self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        
        // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            [leftBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        }
        else
        {
            [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
            
        }
    }
    btnBarCode = [[SalesWorxImageBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarcodeIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBarCode:)];
    btnStartVisit = [[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartVisit"] style:UIBarButtonItemStylePlain target:self action:@selector(btnStartVisit:)];
    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:btnStartVisit, btnBarCode, nil];
}

#pragma mark Start Visit methods
-(void)fetchCustomersList{
        //  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            NSMutableArray * customerListArray=[[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
            // NSLog(@"check status %@", [[customerListArray objectAtIndex:0]valueForKey:@"Cash_Cust"]);
            NSMutableArray * beaconDataArray=[[SWDatabaseManager retrieveManager]fetchBeaconDetailsforCustomers];
            NSLog(@"beacon data array is %@",beaconDataArray);
            
            NSLog(@"customers count is %lu", (unsigned long)customerListArray.count);
            customersArray=[[NSMutableArray alloc]init];
            customerOrderHistoryArray=[[NSMutableArray alloc]init];
            if (customerListArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary   * currentDict in customerListArray) {
                    NSMutableDictionary   * currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Customer * currentCustomer=[[Customer alloc] init];
                    currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
                    currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
                    currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
                    currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
                    currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
                    currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
                    currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
                    currentCustomer.City=[currentCustDict valueForKey:@"City"];
                    currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
                    currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
                    currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
                    currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
                    currentCustomer.Cust_Lat=[currentCustDict valueForKey:@"Cust_Lat"];
                    currentCustomer.Cust_Long=[currentCustDict valueForKey:@"Cust_Long"];
                    currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
                    currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
                    currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
                    currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
                    currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
                    currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
                    currentCustomer.Customer_Segment_ID=[currentCustDict valueForKey:@"Customer_Segment_ID"];
                    currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
                    currentCustomer.Dept=[currentCustDict valueForKey:@"Dept"];
                    currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
                    currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
                    currentCustomer.Postal_Code=[currentCustDict valueForKey:@"Postal_Code"];
                    currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
                    currentCustomer.SalesRep_ID=[currentCustDict valueForKey:@"SalesRep_ID"];
                    currentCustomer.Sales_District_ID=[currentCustDict valueForKey:@"Sales_District_ID"];
                    currentCustomer.Ship_Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]];
                    currentCustomer.Ship_Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]];
                    currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
                    currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
                    currentCustomer.isDelegatedCustomer=[currentCustDict valueForKey:@"isDelegatedCustomer"];
                    currentCustomer.Area=[currentCustDict valueForKey:@"Area"];
                    currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:@"CUST_LOC_RNG"];
                    
                    //adding beacon data
                    currentCustomer.currentCustomerBeacon=nil;
                    
                    if (beaconDataArray.count>0) {
                        NSPredicate * matchBeaconPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_ID ==  %@ and SELF.Site_Use_ID ==  %@",currentCustomer.Customer_ID,currentCustomer.Site_Use_ID]];
                        NSMutableArray * matchedBeaconDataArray=[[beaconDataArray filteredArrayUsingPredicate:matchBeaconPredicate]mutableCopy];
                        
                        if (matchedBeaconDataArray.count>0) {
                            
                            NSLog(@"matched beacon in customers %@", matchedBeaconDataArray);
                            
                            NSMutableDictionary * beaconDict=[matchedBeaconDataArray objectAtIndex:0];
                            SalesWorxBeacon * currentCustomerBeacon=[[SalesWorxBeacon alloc]init];
                            currentCustomerBeacon.Beacon_UUID=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_UUID"]];
                            currentCustomerBeacon.Beacon_Major=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Major"]];
                            currentCustomerBeacon.Beacon_Minor=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Minor"]];
                            currentCustomer.currentCustomerBeacon=currentCustomerBeacon;
                        }
                    }
                    [customersArray addObject:currentCustomer];
                }
            }
        });
}

                       
                       
                       
                    
- (IBAction)btnStartVisit:(id)sender
{
    openVisitsArray=[[SWDatabaseManager retrieveManager]fetchOpenVisitsforOtherCustomers:selectedCustomer.Ship_Customer_ID ];
    openVisitID=[openVisitsArray valueForKey:@"Actual_Visit_ID"];
    openPlannedDetailID=[openVisitsArray valueForKey:@"FSR_Plan_Detail_ID"];
    NSString* visitType = [SWDefaults getValidStringValue:[openVisitsArray valueForKey:@"Visit_Type"]];
    //move directly to the visit if current selected customer is same as open visit
    NSString* selectedCustomerID = selectedCustomer.Ship_Customer_ID;
    NSString* selectedSiteUse_ID = selectedCustomer.Ship_Site_Use_ID;
    NSString* openVisitCustomerID = [NSString getValidStringValue:[openVisitsArray valueForKey:@"Customer_ID"]];
    NSString* openVisitSiteUseID = [NSString getValidStringValue:[openVisitsArray valueForKey:@"Site_Use_ID"]];
    
    NSLog(@"selected customer id is %@\n site use id : %@\n open visit customer id is %@ \n %@ open visit customer ",selectedCustomerID,openVisitCustomerID,selectedSiteUse_ID,openVisitSiteUseID);
   openVisitCustomer = [[Customer alloc]init];
    //get the customer object for open visit customer
    NSPredicate* openVisitCustomerPredicate = [NSPredicate predicateWithFormat:@"SELF.Ship_Customer_ID ==%@ AND SELF.Ship_Site_Use_ID == %@",openVisitCustomerID,openVisitSiteUseID];

   
    NSArray* filteredArray = [customersArray filteredArrayUsingPredicate:openVisitCustomerPredicate];
    if (filteredArray.count>0){
        openVisitCustomer=filteredArray[0];
    }
    
    if([selectedCustomerID isEqualToString:openVisitCustomerID] && [openVisitSiteUseID isEqualToString:selectedSiteUse_ID])
    {
        //its the same customer, continue open visit id
       SalesWorxVisit* salesWorxVisit=[[SalesWorxVisit alloc]init];
        VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
        currentVisitOptions.customer=selectedCustomer;
        salesWorxVisit.visitOptions=currentVisitOptions;
        salesWorxVisit.visitOptions.customer.Visit_Type = visitType;
        //kOnsiteVisitTitle
        isVisitStarted = true;
        [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];

    }else if (openVisitsArray.count>0){
        OpenVisitAlertViewController *alertVC = [[OpenVisitAlertViewController alloc]init];
        alertVC.openVisitCustomer = openVisitCustomer;
        alertVC.selectedCustomer=selectedCustomer;
        alertVC.selectedReasonDelegate=self;
        UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:alertVC];
        mapPopUpViewController.navigationBarHidden = YES;
        mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
        mapPopUpViewController.preferredContentSize = CGSizeMake(400, 300);
        alertVC.modalPresentationStyle = UIModalPresentationFormSheet;
        mapPopUpViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:mapPopUpViewController animated:YES completion:nil];
    }else{
        [self startCustomerVisit];
    }
    /*
    //show an alert if there is any open visit to close that visit.
    NSMutableArray * openVisits=[[SWDatabaseManager retrieveManager]fetchOpenVisitsforOtherCustomers:selectedCustomer.Ship_Customer_ID ];
    NSLog(@"open visits count is %@", openVisits);
    if (openVisits.count>0) {
        
        NSString* customerName=[openVisits valueForKey:@"Customer_Name"];
        openPlannedDetailID=[openVisits valueForKey:@"FSR_Plan_Detail_ID"];
        openVisitID=[openVisits valueForKey:@"Actual_Visit_ID"];
        
        
        NSString * firstText=NSLocalizedString(@"There is an open visit for the customer", nil);
        NSString* secondText=NSLocalizedString(@"would you like to close this visit before starting a new visit", nil);
        NSString* finalString=[NSString stringWithFormat:@"%@\n %@ \n %@",firstText,customerName,secondText ];
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Planned_Visit_ID from TBL_FSR_Planned_Visits where FSR_Plan_Detail_ID ='%@'",openPlannedDetailID]];
                                       
                                       if (plannedVisitIDArray.count>0) {
                                           
                                           openPlannedVisitID=[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Planned_Visit_ID"];
                                       }
                                       
                                       BOOL plannedVisitStatus;
                                       
                                       if ([NSString isEmpty:openPlannedVisitID]==NO) {
                                           plannedVisitStatus=[[SWDatabaseManager retrieveManager]closePlannedVisitwithVisitID:openPlannedVisitID];
                                       }
                                       
                                       BOOL visitEndDateStatus =NO;
                                       
                                       if ([NSString isEmpty:openVisitID]==NO) {
                                           visitEndDateStatus=  [[SWDatabaseManager retrieveManager]updateVisitEndDatewithAlert:openVisitID];
                                       }
                                       
                                       if (visitEndDateStatus==YES || plannedVisitStatus==YES) {
                                           
                                           [self startCustomerVisit];
                                       }
                                       
                                       
                                       
                                   }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Open Visit", nil) andMessage:finalString andActions:actionsArray withController:[SWDefaults currentTopViewController]];
        
        
        
    }
    else
    {
        [self startCustomerVisit];
    }*/
    
}
-(void)updateVisitOptions{
    //if 0 then close the open visit and start new visit, if 1 continue open visit
    if(selectedReason ==0){
        NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Planned_Visit_ID from TBL_FSR_Planned_Visits where FSR_Plan_Detail_ID ='%@'",openPlannedDetailID]];
        if (plannedVisitIDArray.count>0) {
            openPlannedVisitID=[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Planned_Visit_ID"];
        }
        BOOL plannedVisitStatus;
        if ([NSString isEmpty:openPlannedVisitID]==NO) {
            plannedVisitStatus=[[SWDatabaseManager retrieveManager]closePlannedVisitwithVisitID:openPlannedVisitID];
        }
        BOOL visitEndDateStatus =NO;
        if ([NSString isEmpty:openVisitID]==NO) {
            visitEndDateStatus=  [[SWDatabaseManager retrieveManager]updateVisitEndDatewithAlert:openVisitID];
            
            //<NG 2019-07-16> GEO validation flag added as suggested by Harpal
            if ([appControl.ENABLE_VISIT_LOC_UPDATE_ON_CLOSE isEqualToString:@"Y"]) {
                [self validateLocationCoOrdinateforOnSiteVisit];
            }
        }
        if (visitEndDateStatus==YES || plannedVisitStatus==YES) {
            [self startCustomerVisit];
        }
    }else if (selectedReason == 1){
        NSString* visitType = [SWDefaults getValidStringValue:[openVisitsArray valueForKey:@"Visit_Type"]];
        salesWorxVisit=[[SalesWorxVisit alloc]init];
        VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
        currentVisitOptions.customer=openVisitCustomer;
        salesWorxVisit.visitOptions=currentVisitOptions;
        salesWorxVisit.isPlannedVisit=YES;
        salesWorxVisit.FSR_Plan_Detail_ID=selectedCustomer.FSR_Plan_Detail_ID;
        salesWorxVisit.visitOptions.customer.Visit_Type = visitType;
        //kOnsiteVisitTitle
        isVisitStarted = true;
        [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
        
    }
}
-(void)validateLocationCoOrdinateforOnSiteVisit
{
    NSString *customerLatitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Lat];
    NSString *customerLongitude=[NSString stringWithFormat:@"%@",salesWorxVisit.visitOptions.customer.Cust_Long];
    
    //check if customer location is available
    
    SWAppDelegate *appDel = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString *fsrLatitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude];
    NSString *fsrLongitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
    
    NSLog(@"CLOSE_VISIT_LOC_UPDATE_LIMIT is %@", appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT);
    
    //fetch range from tbl_addl_info
    double distanceLimit = [appControl.CLOSE_VISIT_LOC_UPDATE_LIMIT doubleValue];
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[customerLatitude doubleValue] longitude:[customerLongitude doubleValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[fsrLatitude doubleValue] longitude:[fsrLongitude doubleValue]];
    CLLocationDistance distance = [location1 distanceFromLocation:location2];
    
    NSLog(@"Distance between Locations in meters: %f", distance);
    
    if (distance > distanceLimit)
    {
        [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@',Custom_Attribute_6='Y' Where Actual_Visit_ID='%@'",fsrLatitude, fsrLongitude, openVisitID]];
    }
}

-(void)selectedOpenVisitReason:(NSInteger)selectedReasonCode{
    selectedReason = selectedReasonCode;
    [self performSelector:@selector(updateVisitOptions) withObject:self afterDelay:2];
}
-(void)startCustomerVisit
{
    NSString *DateString = [MedRepQueries fetchDatabaseDateFormat];
    NSMutableArray *SODReading = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_FSR_Process_Transactions where Trx_Date like '%@%%'",DateString]];
    AppControl* appControl =[AppControl retrieveSingleton];
    
    if ([SODReading count] == 0 && [appControl.ODOMETER_READING isEqualToString:KAppControlsYESCode]) {
        SalesWorxODOMeterViewController *odoMeterVC = [[SalesWorxODOMeterViewController alloc]init];
        odoMeterVC.ODOMeterDelegate = self;
        UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:odoMeterVC];
        mapPopUpViewController.navigationBarHidden = YES;
        mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
        mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
        odoMeterVC.view.backgroundColor = KPopUpsBackGroundColor;
        odoMeterVC.modalPresentationStyle = UIModalPresentationCustom;
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:mapPopUpViewController animated:NO completion:nil];
        
    } else {
        
        Singleton *single = [Singleton retrieveSingleton];
        
        if ([appControl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
            
            SalesWorxCashCustomerOnsiteVisitOptionsViewController * cashCustOnsiteVC=[[SalesWorxCashCustomerOnsiteVisitOptionsViewController alloc]init];
            cashCustOnsiteVC.delegate=self;
            salesWorxVisit=[[SalesWorxVisit alloc]init];
            VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
            currentVisitOptions.customer=selectedCustomer;
            salesWorxVisit.detectedBeacon=[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon];
            salesWorxVisit.visitOptions=currentVisitOptions;
            salesWorxVisit.isPlannedVisit=NO;
            cashCustOnsiteVC.currentVisit=salesWorxVisit;
            UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:cashCustOnsiteVC];
            mapPopUpViewController.navigationBarHidden=YES;
            //mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
            mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
            //cashCustOnsiteVC.view.backgroundColor = KPopUpsBackGroundColor;
            cashCustOnsiteVC.modalPresentationStyle = UIModalPresentationCustom;
            self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
            [self presentViewController:mapPopUpViewController animated:NO completion:nil];
        }
        else{
            
            single.isCashCustomer = @"N";
            // [[[SWVisitManager alloc] init] startVisitWithCustomer:customer parent:self andChecker:@"D"];
            salesWorxVisit.FSR_Plan_Detail_ID=selectedCustomer.FSR_Plan_Detail_ID;
            
            
            SalesWorxVisitOptionsViewController * visitOptions=[[SalesWorxVisitOptionsViewController alloc]init];
            salesWorxVisit=[[SalesWorxVisit alloc]init];
            VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
            currentVisitOptions.customer=selectedCustomer;
            salesWorxVisit.visitOptions=currentVisitOptions;
            if([NSString isEmpty:[NSString getValidStringValue:selectedCustomer.FSR_Plan_Detail_ID]]){
                salesWorxVisit.isPlannedVisit=NO;
            }else{
                salesWorxVisit.isPlannedVisit=YES;
            }
            visitOptions.salesWorxVisit=salesWorxVisit;
            
            //[self.navigationController pushViewController:visitOptions animated:YES];
            isVisitStarted = true;
            [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
            
        }
    }
}

-(void)StartVisitAfterPopUp {
   // [self updateVisitOptions];
    [self startCustomerVisit];
}

-(void)cashCustomerSelectedVisitOption:(NSString*)visitOption customer:(SalesWorxVisit*)currentVisit
{
    
    NSLog(@"selected visit option is %@", visitOption);
    
    
    if ([visitOption isEqualToString:kTelephonicVisitTitle]) {
        
        [SWDefaults setOnsiteVisitStatus:NO];
    }
    else if ([visitOption isEqualToString:kOnsiteVisitTitle])
    {
        [SWDefaults setOnsiteVisitStatus:YES];
    }
    else{
        
    }
    
    Singleton *single = [Singleton retrieveSingleton];
    
    if ([currentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"Y"]) {
        single.isCashCustomer = @"Y";
    }
    else{
        single.isCashCustomer=@"N";
    }
    isVisitStarted = true;
    [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:currentVisit withParent:self];
    
    //    [[[SWVisitManager alloc] init] startVisitWithCustomer:[SWDefaults fetchCurrentCustDict:currentVisit.visitOptions.customer] parent:self andChecker:@"D"];
    
    NSLog(@"check customer dict for cash customer %@",[SWDefaults fetchCurrentCustDict:currentVisit.visitOptions.customer]);
    
    
    NSLog(@"visit status is %hhd", [SWDefaults fetchOnsiteVisitStatus]);
    
    
}

-(void)selectedVisitOption:(NSString*)visitOption
{
    
    NSMutableDictionary * currentCustDict=[[NSMutableDictionary alloc]init];
    
    [currentCustDict setValue:selectedCustomer.Address forKey:@"Address"];
    [currentCustDict setValue:selectedCustomer.Allow_FOC forKey:@"Allow_FOC"];
    [currentCustDict setValue:selectedCustomer.Avail_Bal forKey:@"Avail_Bal"];
    [currentCustDict setValue:selectedCustomer.Customer_Name forKey:@"Customer_Name"];
    [currentCustDict setValue:selectedCustomer.Bill_Credit_Period forKey:@"Bill_Credit_Period"];
    [currentCustDict setValue:selectedCustomer.Cash_Cust forKey:@"Cash_Cust"];
    [currentCustDict setValue:selectedCustomer.Chain_Customer_Code forKey:@"Chain_Customer_Code"];
    [currentCustDict setValue:selectedCustomer.City forKey:@"City"];
    [currentCustDict setValue:selectedCustomer.Creation_Date forKey:@"Creation_Date"];
    [currentCustDict setValue:selectedCustomer.Credit_Hold forKey:@"Credit_Hold"];
    [currentCustDict setValue:selectedCustomer.Credit_Limit forKey:@"Credit_Limit"];
    [currentCustDict setValue:selectedCustomer.Cust_Lat forKey:@"Cust_Lat"];
    [currentCustDict setValue:selectedCustomer.Cust_Long forKey:@"Cust_Long"];
    [currentCustDict setValue:selectedCustomer.Cust_Status forKey:@"Cust_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Barcode forKey:@"Customer_Barcode"];
    [currentCustDict setValue:selectedCustomer.Customer_Class forKey:@"Customer_Class"];
    [currentCustDict setValue:selectedCustomer.Customer_ID forKey:@"Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_No forKey:@"Customer_No"];
    [currentCustDict setValue:selectedCustomer.Customer_OD_Status forKey:@"Customer_OD_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Segment_ID forKey:@"Customer_Segment_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_Type forKey:@"Customer_Type"];
    [currentCustDict setValue:selectedCustomer.Dept forKey:@"Dept"];
    [currentCustDict setValue:selectedCustomer.Location forKey:@"Location"];
    [currentCustDict setValue:selectedCustomer.Phone forKey:@"Phone"];
    [currentCustDict setValue:selectedCustomer.Postal_Code forKey:@"Postal_Code"];
    [currentCustDict setValue:selectedCustomer.Price_List_ID forKey:@"Price_List_ID"];
    [currentCustDict setValue:selectedCustomer.SalesRep_ID forKey:@"SalesRep_ID"];
    [currentCustDict setValue:selectedCustomer.Sales_District_ID forKey:@"Sales_District_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Customer_ID forKey:@"Ship_Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Site_Use_ID forKey:@"Ship_Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Site_Use_ID forKey:@"Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Trade_Classification forKey:@"Trade_Classification"];
    [currentCustDict setValue:visitOption forKey:@"Visit_Type"];
    [currentCustDict setValue:[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:selectedCustomer.Area] forKey:@"Area"];
    
    selectedCustomer.Visit_Type=[NSString getValidStringValue:visitOption];
    Singleton *single = [Singleton retrieveSingleton];
    // CHECK IF CASH CUSTOMER THEN SHOW FORM
    
    
    if([selectedCustomer.Cash_Cust isEqualToString:@"Y"]){
        CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:currentCustDict] ;
        cashCustomer.selectedCustomer=selectedCustomer;
        [self.navigationController pushViewController:cashCustomer animated:YES];
        cashCustomer = nil;
    }
    else{
        single.isCashCustomer = @"N";
        [[[SWVisitManager alloc] init] startVisitWithCustomer:currentCustDict parent:self andChecker:@"R"];
    }
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{

    if(self.isMovingToParentViewController){
        [segmentControl setSelectedSegmentIndex:0 animated:YES];
        [customersScrollView setContentOffset:CGPointZero animated:YES];
        
        [self prepareSegmentControl];
        [self prepareChildViewControllers];
        [self UpdateRoutecustomersList];
    }
    else{
        if (isVisitStarted == true) {
            isVisitStarted = false;
            
            [self UpdateRoutecustomersList];
            if ([appControl.FS_ENABLE_ROUTE_RESCHEDULING isEqualToString:KAppControlsYESCode]) {
                SalesWorxBackgroundSyncManager *salesWorxBackgroundSyncManager=[[SalesWorxBackgroundSyncManager alloc]init];
                [salesWorxBackgroundSyncManager syncVisitDataToserver];
            }
        }
        else {
            segmentControl.indexChangeBlock(1);
        }
    }
}
-(void)viewDidAppear:(BOOL)animated{
    if(self.isMovingToParentViewController){
        [self.view layoutIfNeeded];
        [self.view layoutSubviews];
    }
}

-(void)prepareSegmentControl
{
    segmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    segmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    segmentControl.sectionTitles = @[NSLocalizedString(@"Details", nil), NSLocalizedString(@"Nearby Customers", nil)];
    
    segmentControl.selectionIndicatorBoxOpacity=0.0f;
    segmentControl.verticalDividerEnabled=YES;
    segmentControl.verticalDividerWidth=1.0f;
    segmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    segmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    segmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentControl.tag = 3;
    segmentControl.selectedSegmentIndex = 0;
    
    segmentControl.layer.shadowColor = [UIColor blackColor].CGColor;
    segmentControl.layer.shadowOffset = CGSizeMake(2, 2);
    segmentControl.layer.shadowOpacity = 0.1;
    segmentControl.layer.shadowRadius = 1.0;
    
    
    
    
    __weak typeof(self) weakSelf = self;
    [segmentControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.customersScrollView setContentOffset:CGPointMake(customersScrollView.frame.size.width * index, 0)];
        if(selectedCustomer!=nil){
            [nearByCustomersViewController updateNearByCustomersData:selectedCustomer];
        }
    }];
    
    
}

- (void)GetRouteDataFromBackground:(NSNotification *)notification{
    
    NSLog(@"application entered foreground called route");
    //routeSer.delegate=self;
    [self refreshData];
}

-(void)refreshData
{
    isAppRefresh = YES;
    selecDate=[NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[formatter stringFromDate:selecDate]] forState:UIControlStateNormal];
    
    [segmentControl setSelectedSegmentIndex:0 animated:YES];
    [customersScrollView setContentOffset:CGPointZero animated:YES];
    
    [self UpdateRoutecustomersList];
}


-(void)becomeActive
{
    NSLog(@"did become active called in route");
//    [self refreshData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Database Methods
-(NSMutableArray *)fetchRouteCustomersForSelectedDate
{
    NSMutableArray *arrCustomers = [[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selecDate] mutableCopy];
    
    if(arrCustomers.count>0){
        NSLog(@"Route Customers are %@", [arrCustomers valueForKey:@"Customer_Name"]);
    }else{
        NSLog(@"No Route Customers for date %@", selecDate);
    }
    
    return arrCustomers;
}
-(Customer *)PrepareRouteCustomerObjectWithData:(NSDictionary *)currentCustomer{
    
        Customer * currentCust=[[Customer alloc]init];
        currentCust.Address=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Address"]];
        currentCust.Avail_Bal=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Avail_Bal"]];
        currentCust.Allow_FOC=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Allow_FOC"]];
        currentCust.Customer_Name=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Name"]];
        currentCust.Bill_Credit_Period=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Bill_Credit_Period"]];
        currentCust.Cash_Cust=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cash_Cust"]];
        currentCust.Chain_Customer_Code=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Chain_Customer_Code"]];
        currentCust.City=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"City"]];
        currentCust.Creation_Date=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Creation_Date"]];
        currentCust.Credit_Hold=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Credit_Hold"]];
        currentCust.Credit_Limit=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Credit_Limit"]];
        currentCust.Cust_Lat=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cust_Lat"]];
        currentCust.Cust_Long=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cust_Long"]];
        currentCust.Cust_Status=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cust_Status"]];
        currentCust.Customer_Barcode=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Barcode"]];
        currentCust.Customer_Class=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Class"]];
        currentCust.Customer_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_ID"]];
        currentCust.Customer_No=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_No"]];
        currentCust.Customer_OD_Status=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_OD_Status"]];
        currentCust.Customer_Segment_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Segment_ID"]];
        currentCust.Customer_Type=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Type"]];
        currentCust.Dept=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Dept"]];
        currentCust.Location=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Location"]];
        currentCust.Phone=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Phone"]];
        currentCust.Postal_Code=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Postal_Code"]];
        currentCust.Price_List_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Price_List_ID"]];
        currentCust.SalesRep_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"SalesRep_ID"]];
        currentCust.Sales_District_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Sales_District_ID"]];
        currentCust.Ship_Customer_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Ship_Customer_ID"]];
        currentCust.Ship_Site_Use_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Ship_Site_Use_ID"]];
        currentCust.Site_Use_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Site_Use_ID"]];
        currentCust.Trade_Classification=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Trade_Classification"]];
        currentCust.Over_Due=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Over_Due"]];
        
        currentCust.visitStatus=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Visit_Status"]];
        currentCust.FSR_Plan_Detail_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"FSR_Plan_Detail_ID"]];
        currentCust.Visit_Date=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Visit_Date"]];
        
        if ([currentCustomer valueForKey:@"Planned_Visit_ID"]) {
            currentCust.Planned_visit_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Planned_Visit_ID"]];
        }
        else{
            
        }
        
        if ([currentCustomer valueForKey:@"Visit_Type"]) {
            
            currentCust.Visit_Type=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Visit_Type"]];
        }
        
        if ([[currentCustomer valueForKey:@"Cash_Cust"] isEqualToString:@"Y"]) {
            
            currentCust.cash_Name=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Name"]];
            currentCust.cash_Phone=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Phone"]];
            currentCust.cash_Address=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Address"]];
            currentCust.cash_Contact=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Contact"]];
            currentCust.cash_Fax=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Fax"]];
        }
        currentCust.lastVisitedOn = @"";

    
    return currentCust;
        
}
#pragma mark UITableView DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return customersDataDicsArray.count;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"todoCell";
    SalesWorxVisitOptionsToDoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxVisitOptionsToDoTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }

    NSDictionary  * currentCust=[customersDataDicsArray objectAtIndex:indexPath.row];

    [cell.titleLbl setText:[[SWDefaults getValidStringValue:[currentCust valueForKey:@"Customer_Name"]] capitalizedString]];
    
    NSString* visitStatusString=[SWDefaults getValidStringValue:[currentCust valueForKey:@"Visit_Status"]];
    if ([visitStatusString isEqualToString:@"Y"])
    {
        [cell.statusView setBackgroundColor:KCompletedVisitColor];
        //  [cell setEditing:NO animated:NO];
    }
    else
    {
        //  [cell setEditing:YES animated:NO];
        [cell.statusView setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(128.0/255.0) blue:(255.0/255.0) alpha:1]];
    }
    
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell.titleLbl setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if (selectedIndexPath.row==indexPath.row) {
        
        cell.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        [cell.titleLbl setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        [cell setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
    }
    
    else
    {
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        cell.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        [cell.contentView setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
        [cell.titleLbl setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
        [cell setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
        
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"\n\n *** 1 canEditRowAtIndexPath  ***\n\n");
    if ([appControl.FS_ENABLE_ROUTE_RESCHEDULING isEqualToString:KAppControlsNOCode]) {
        return NO;
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSString *selectDate = [formatter stringFromDate:selecDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    if (![todayDate isEqualToString:selectDate]) {
        
        // rescheduling checks ignore for future visits
        return YES;
    }
    
    NSString *visitChangeEndTimeString = @"";
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger currentWeekday = [components weekday];
    
    NSMutableArray *routePlanDayOffArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select IFNULL(Control_Value,'N/A') AS Code_Value from TBL_App_Control where Control_Key = 'ROUTEPLAN_DAY_OFF'"];
    if (routePlanDayOffArray.count>0) {
        NSString *routePlanDayOffName = [[routePlanDayOffArray valueForKey:@"Code_Value"]objectAtIndex:0];
        
        NSArray *routePlanDayOffNameArray = [routePlanDayOffName componentsSeparatedByString:@","];
        for (int i = 0; i< [routePlanDayOffNameArray count]; i++) {
            
            NSMutableArray *routePlanDayOffArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_1,'N/A') AS Day_OFF from TBL_App_Codes where Code_Type = 'ROUTEPLAN_DAY_OFF' AND Code_Value = '%@'",routePlanDayOffNameArray[i]]];
            if (routePlanDayOffArray.count>0) {
                NSString *dayOFF = [[routePlanDayOffArray valueForKey:@"Day_OFF"]objectAtIndex:0];
                if ([dayOFF integerValue] == currentWeekday) {
                    return NO;
                }
            }
        }
    }
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *currentTimeString = [dateFormatter stringFromDate:date];
    NSDate *currentTime = [dateFormatter dateFromString:currentTimeString];
    
    NSMutableArray *visitChangeEndTimeArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select IFNULL(Control_Value,'N/A') AS Code_Value from TBL_App_Control where Control_Key = 'FS_VISIT_CHANGE_END_TIME'"];
    if (visitChangeEndTimeArray.count>0) {
        visitChangeEndTimeString = [[visitChangeEndTimeArray valueForKey:@"Code_Value"]objectAtIndex:0];
    }
    NSDate *visitChangeEndTime = [dateFormatter dateFromString:visitChangeEndTimeString];
    
    if([visitChangeEndTime compare: currentTime] == NSOrderedAscending)
    {
        return NO;
    }
    
    return YES;
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedIndexPath.row != indexPath.row) {
        //[self performSelector:@selector(callDidSelectMethodWithIndexPath:) withObject:indexPath afterDelay:1.0];
        [customerTableView.delegate tableView:customerTableView didSelectRowAtIndexPath:indexPath];
    }
    NSMutableArray *arrRescheduledVisitsForSelectedCustomer = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Visit_Change_Log where Customer_ID = '%@' and Site_Use_ID = '%@' and date(Visit_Date) = date('now')",selectedCustomer.Customer_ID, selectedCustomer.Ship_Site_Use_ID]];
    

    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSString *selectDate = [formatter stringFromDate:selecDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    if (![todayDate isEqualToString:selectDate]) {
        
        // rescheduling checks ignore for future visits
        arrRescheduledVisitsForSelectedCustomer = [[NSMutableArray alloc]init];
    }
    
    UITableViewRowAction *rescheduleAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Reschedule" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                              {
                                                  //insert your reschedule Action here
                                                  @try {
                                                      [self RouteRescheduleVisitButtonTapped:@"R"];
                                                  }
                                                  @catch (NSException *exception) {
                                                      NSLog(@"%@",exception);
                                                  }
                                              }];
    rescheduleAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *cancelAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Cancel Reschedule" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              //insert your cancel reschedule Action here
                                              @try {
                                                  NSMutableArray *arrRescheduledVisitsForSelectedCustomer = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM 'TBL_Visit_Change_Log' where Proc_Status = 'N' and Customer_ID = '%@' and date(Visit_Date) = date('now')",selectedCustomer.Customer_ID]];
                                                  
                                                  if (arrRescheduledVisitsForSelectedCustomer.count > 0) {
                                                      [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"Delete from TBL_Visit_Change_Log where Proc_Status = 'N' and Customer_ID = '%@' and date(Visit_Date) = date('now')", selectedCustomer.Customer_ID]];
                                                      
                                                      [self UpdateRoutecustomersList];
                                                      [SWDefaults showAlertAfterHidingKeyBoard:@"Reschedule Route" andMessage:@"Route reschedule cancelled successfully" withController:self];
                                                      
                                                  } else {
                                                      NSString *currentTime = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
                                                      NSDictionary *FSR = [SWDefaults userProfile];
                                                      
                                                      [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_Visit_Change_Log set Cancelled_At = '%@', Cancelled_By = '%@' where Customer_ID = '%@' and date(Visit_Date) = date('now')", currentTime, [FSR stringForKey:@"User_ID"], selectedCustomer.Customer_ID]];
                                                      
                                                      [self UpdateRoutecustomersList];
                                                      
                                                      Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
                                                      NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
                                                      if (networkStatus == NotReachable)
                                                      {
                                                          [SWDefaults showAlertAfterHidingKeyBoard:KNoNetworkAlertTitle andMessage:@"Request to cancel reschedule will be sent once connected to Internet" withController:self];
                                                      }
                                                      else {
                                                          [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_ROUTE_CANCEL_BS_NOTIFICATIONNameStr object:nil];
                                                          [SWDefaults showAlertAfterHidingKeyBoard:@"Reschedule Route" andMessage:@"Request to cancel reschedule sent successfully" withController:self];
                                                      }
                                                  }
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"%@",exception);
                                              }
                                          }];
    cancelAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              //insert your delete Action here
                                              
                                              [self RouteRescheduleVisitButtonTapped:@"D"];
                                          }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    if ([selectedCustomer.visitStatus isEqualToString:@"Y"]) {
        NSLog(@"\n\n *** button: Empty 1 ***\n\n");
        return @[];
    }
    
    if (arrRescheduledVisitsForSelectedCustomer.count > 0) {
        NSMutableDictionary *dictObj = [arrRescheduledVisitsForSelectedCustomer objectAtIndex:0];
        NSString *approvalStatus = [NSString getValidStringValue:[dictObj valueForKey:@"Approval_Status"]];
        
        if ([approvalStatus isEqualToString:@"N"]) {
            if ([[dictObj valueForKey:@"Change_Type"] isEqualToString:@"R"]) {
                NSLog(@"\n\n *** button: cancel 2 ***\n\n");
                return @[cancelAction];
            } else {
                NSLog(@"\n\n *** button: Empty 3 ***\n\n");
                return @[];
            }
        } else {
            NSLog(@"\n\n *** button: delete, re-schedule 4 ***\n\n");
            return @[deleteAction, rescheduleAction];
        }
    } else {
        NSLog(@"\n\n *** button: delete, re-schedule 5***\n\n");
        return @[deleteAction, rescheduleAction];
    }
}

-(void)updateRouteRescheduleData:(NSString *)changeType {
    [self UpdateRoutecustomersList];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if ([changeType isEqualToString:@"R"]) {
        if (networkStatus == NotReachable) {
            [SWDefaults showAlertAfterHidingKeyBoard:KNoNetworkAlertTitle andMessage:@"Route reschedule request will be sent once connected to Internet" withController:self];
        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_ROUTE_BS_NOTIFICATIONNameStr object:nil];
            [SWDefaults showAlertAfterHidingKeyBoard:@"Reschedule Route" andMessage:@"Route reschedule request sent successfully" withController:self];
        }
    } else {
        if (networkStatus == NotReachable) {
            [SWDefaults showAlertAfterHidingKeyBoard:KNoNetworkAlertTitle andMessage:@"Route delete request will be sent once connected to Internet" withController:self];
        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_ROUTE_BS_NOTIFICATIONNameStr object:nil];
            [SWDefaults showAlertAfterHidingKeyBoard:@"Delete Route" andMessage:@"Route delete request sent successfully" withController:self];
        }
    }
}

-(void)RouteRescheduleVisitButtonTapped:(NSString *)changeType {
    CGRect rectOfCellInTableView = [customerTableView rectForRowAtIndexPath:selectedIndexPath];
    CGRect rectOfCellInSuperview = [customerTableView convertRect:rectOfCellInTableView toView:self.view];
    
    SalesWorxRouteRescheduleViewController *rescheduleVC = [[SalesWorxRouteRescheduleViewController alloc]init];
    rescheduleVC.dismissDelegate = self;
    rescheduleVC.selectedCustomer = selectedCustomer;
    rescheduleVC.changeType = changeType;

    UINavigationController * popOverNavigationCroller= [[[SWDefaults alloc] init]getPresenationControllerWithNavigationController:self WithRootViewController:rescheduleVC WithSourceView:self.view WithSourceRect:rectOfCellInSuperview WithBarButtonItem:nil WithPopOverContentSize:CGSizeMake(300, 273)];
    [self presentViewController:popOverNavigationCroller animated:YES completion:^{
        NSLog(@"Popover Controller shown");
    }];
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return NO;
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"\n\n *** 2 didSelectRowAtIndexPath  ***\n\n");
    NSIndexPath * previousSelectedIndexPath = selectedIndexPath;
    selectedIndexPath=indexPath;

    if (previousSelectedIndexPath && previousSelectedIndexPath.row < customersDataDicsArray.count) {
        [customerTableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:previousSelectedIndexPath, nil] withRowAnimation:UITableViewRowAnimationNone];

    }

    
    selectedCustomer = [self PrepareRouteCustomerObjectWithData:[customersDataDicsArray objectAtIndex:indexPath.row]];
    
    [routeDetailsViewController updateSelectedCustomerData:selectedCustomer withSelectedDate:selecDate];

    if(segmentControl.selectedSegmentIndex == 1){
        [nearByCustomersViewController updateNearByCustomersData:selectedCustomer];
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSString *selectDate =[formatter stringFromDate:selecDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    if ([selectedCustomer.visitStatus isEqualToString:@"Y"] || (![todayDate isEqualToString:selectDate])) {
        
        btnBarCode.enabled = NO;
        btnStartVisit.enabled = NO;
    }
    else{
        btnBarCode.enabled = YES;
        btnStartVisit.enabled = YES;
    }
    
    //old code
    //[customerTableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:selectedIndexPath, nil] withRowAnimation:UITableViewRowAnimationNone];

    
    //working code
    SalesWorxVisitOptionsToDoTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
    cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
    [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    [cell.titleLbl setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    [cell setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
   
    
   // [self performSelector:@selector(reloadTblView) withObject:nil afterDelay:1.0];
   
   // [customerTableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:selectedIndexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    
    /*
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        isDidSelectSelected = NO;
    });
    */
}

-(void)reloadTblView{
    
    [self.customerTableView beginUpdates];
    [self.customerTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:selectedIndexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    [self.customerTableView endUpdates];
    
    
     //[customerTableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:selectedIndexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark Segment Control Methods
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
}


#pragma mark Date selection methods
- (IBAction)btnDate:(id)sender
{
    [self.view endEditing:YES];
    
    SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
    popOverVC.didSelectDateDelegate=self;
    popOverVC.titleString = NSLocalizedString(@"Select Date", nil);
    popOverVC.setMinimumDateCurrentDate=YES;
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    UIPopoverController *paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.datePickerPopOverController=paymentPopOverController;
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(300, 273) animated:YES];
    [paymentPopOverController presentPopoverFromRect:btnDate.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)didSelectDate:(NSString *)selectedDate
{
    [segmentControl setSelectedSegmentIndex:0 animated:YES];
    [customersScrollView setContentOffset:CGPointZero animated:YES];
    
    if (selectedDate == nil)
    {
        NSDateFormatter *formatter = [NSDateFormatter new]  ;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *todayDate =  [formatter stringFromDate:selecDate];
        

        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSMutableArray *arrCustomers = [self fetchRouteCustomersForSelectedDate];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];

                if ([arrCustomers count] == 0)
                {
                    selecDate=[selecDate dateByAddingTimeInterval:(+1.0*24*60*60)];
                    btnBarCode.enabled = NO;
                    btnStartVisit.enabled = NO;
                    btnMinusDate.enabled = YES;
                    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"No Data", nil) andMessage:NSLocalizedString(@"No data available", nil) withController:self];
                } else
                {
                    [self hideNoSelectionView];
                    customersDataDicsArray = arrCustomers;
                    [btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:todayDate] forState:UIControlStateNormal];
                    [customerTableView reloadData];
                    [customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
                    [self tableView:customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                }
            });
        });
    }
    else
    {
        NSDateFormatter *formatter = [NSDateFormatter new]  ;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
        selecDate = [formatter dateFromString:selectedDate];
        
        if(![todayDate isEqualToString:selectedDate])
        {
            btnMinusDate.enabled=YES;
            btnBarCode.enabled = NO;
            btnStartVisit.enabled = NO;
        }
        else
        {
            btnMinusDate.enabled=NO;
            btnBarCode.enabled = YES;
            btnStartVisit.enabled = YES;
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
             NSMutableArray *arrCustomers = [self fetchRouteCustomersForSelectedDate];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                if ([arrCustomers count] == 0)
                {
                    NSString *dateOnButton = [MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:@"yyyy-MM-dd" scrString:btnDate.titleLabel.text];
                    selecDate = [formatter dateFromString:dateOnButton];
                    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"No Data", nil) andMessage:NSLocalizedString(@"No data available", nil) withController:self];
                    
                    btnMinusDate.enabled=YES;
                    btnBarCode.enabled = NO;
                    btnStartVisit.enabled = NO;
                    
                    if ([dateOnButton isEqualToString:todayDate]) {
                        btnMinusDate.enabled=NO;
                        if (customersDataDicsArray.count > 0) {
                            btnBarCode.enabled = YES;
                            btnStartVisit.enabled = YES;
                            [customerTableView reloadData];
                            [customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
                            [self tableView:customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                        }
                    }
                } else
                {
                    [self hideNoSelectionView];
                    customersDataDicsArray = arrCustomers;
                    [btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:selectedDate] forState:UIControlStateNormal];
                    [customerTableView reloadData];
                    [customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
                    [self tableView:customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                }
            });
        });
        [datePickerPopOver dismissPopoverAnimated:YES];
    }
}
- (IBAction)btnMinusDate:(id)sender
{
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSDate *previousDate = [selecDate dateByAddingTimeInterval:(-1.0*24*60*60)];
    NSString *preDateString =[formatter stringFromDate:previousDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    if([todayDate isEqualToString:preDateString])
    {
        btnBarCode.enabled = YES;
        btnStartVisit.enabled = YES;
        btnMinusDate.enabled=NO;
    }
    else
    {
        btnBarCode.enabled = NO;
        btnStartVisit.enabled = NO;
    }
    selecDate=[selecDate dateByAddingTimeInterval:(-1.0*24*60*60)];
    [nearByCustomersViewController updateDate:selecDate];
    
    [self didSelectDate:nil];
}
- (IBAction)btnPlusDate:(id)sender
{
    [segmentControl setSelectedSegmentIndex:0 animated:YES];
    [customersScrollView setContentOffset:CGPointZero animated:YES];
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    selecDate = [selecDate dateByAddingTimeInterval:(1.0*24*60*60)];
    
    [nearByCustomersViewController updateDate:selecDate];
    
    NSString *preDateString =[formatter stringFromDate:selecDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSMutableArray *arrCustomers = [self fetchRouteCustomersForSelectedDate];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if ([arrCustomers count] == 0)
            {
                selecDate=[selecDate dateByAddingTimeInterval:(-1.0*24*60*60)];
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"No Data", nil) andMessage:NSLocalizedString(@"No data available", nil) withController:self];
//                [customerTableView reloadData];

            } else
            {
                [self hideNoSelectionView];
                customersDataDicsArray = arrCustomers;
                [btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[formatter stringFromDate:selecDate]] forState:UIControlStateNormal];
                [customerTableView reloadData];
                [customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
                [self tableView:customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                
                if(![todayDate isEqualToString:preDateString])
                {
                    btnMinusDate.enabled=YES;
                    btnBarCode.enabled = NO;
                    btnStartVisit.enabled = NO;
                }
            }
        });
    });
}
#pragma mark Child Viewcontrollers

-(void)prepareChildViewControllers{
    
    [customersScrollView setPagingEnabled:YES];
    
    routeDetailsViewController=[[SalesWorxRouteDetailsViewController alloc] initWithNibName:@"SalesWorxRouteDetailsViewController" bundle:[NSBundle mainBundle]];
    nearByCustomersViewController=[[SalesWorxNearByCustomersViewController alloc] initWithNibName:@"SalesWorxNearByCustomersViewController" bundle:[NSBundle mainBundle]];
    
    routeDetailsViewController.view.frame=CGRectMake(0, 0, customersScrollView.frame.size.width, customersScrollView.frame.size.height);
    nearByCustomersViewController.view.frame=CGRectMake(customersScrollView.frame.size.width, 0, customersScrollView.frame.size.width, customersScrollView.frame.size.height);
    
    [customersScrollView addSubview:routeDetailsViewController.view];
    [customersScrollView addSubview:nearByCustomersViewController.view];
    
    [self addChildViewController:routeDetailsViewController];
    [self addChildViewController:nearByCustomersViewController];
    

}

-(void)UpdateRoutecustomersList{
    
    errorMessageLabel.text=@"";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        customersDataDicsArray = [self fetchRouteCustomersForSelectedDate];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            [customerTableView reloadData];
            
            if (customersDataDicsArray.count>0) {
                [self hideNoSelectionView];
                if (![NSString isEmpty:[SWDefaults getValidStringValue:selectedCustomer.Customer_ID]] && !selectedIndexPath && !isAppRefresh) {
                    
                    NSArray *filtered = [customersDataDicsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_Name == \"%@\"",[SWDefaults getValidStringValue:selectedCustomer.Customer_Name]]]];
                    
                    if (filtered.count > 0) {
                        NSInteger indexObj  = [customersDataDicsArray indexOfObject:[filtered objectAtIndex:0]];
                        
                        if ([customerTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
                        {
                            [customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                            [customerTableView.delegate tableView:customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]];
                        }
                    }
                }
                else if (selectedIndexPath && selectedIndexPath.row < customersDataDicsArray.count && !isAppRefresh) {
                    [customerTableView selectRowAtIndexPath:selectedIndexPath animated:YES scrollPosition:0];
                    [self tableView:customerTableView didSelectRowAtIndexPath:selectedIndexPath];
                }
                else{
                    [customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
                    [self tableView:customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                }
            }
            else
            {
                [self showNoSelectionViewWithMessage];
                btnBarCode.enabled = NO;
                btnStartVisit.enabled = NO;
                errorMessageLabel.text=@"No planned visits";
            }
        });
    });
}

#pragma mark UIScrollView methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView==customersScrollView){
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        [segmentControl setSelectedSegmentIndex:page animated:YES];
        if(selectedCustomer!=nil && page == 1){
            [nearByCustomersViewController updateNearByCustomersData:selectedCustomer];
        }
    }
}

#pragma Barcode Functions
- (void)btnBarCode:(id)sender
{
    if (customersDataDicsArray.count>0)
    {
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(authStatus == AVAuthorizationStatusAuthorized)
        {
            [self openCamera];
        }
        else if(authStatus == AVAuthorizationStatusNotDetermined)
        {
            NSLog(@"%@", @"Camera access not determined. Ask for permission.");
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void){
                     //Run UI Updates
                     if(granted)
                     {
                         NSLog(@"Granted access to %@", AVMediaTypeVideo);
                         [self openCamera];
                     }
                     else
                     {
                         NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                         [self cameraDenied];
                     }
                 });
             }];
        }
        else if (authStatus == AVAuthorizationStatusRestricted)
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"You've been restricted from using the camera on this device." withController:self];
        }
        else if(authStatus == AVAuthorizationStatusDenied) {
            [self cameraDenied];
        }
        else
        {
            NSLog(@"%@", @"Camera access unknown error.");
        }
    }
}
-(void)openCamera {
    Singleton *single = [Singleton retrieveSingleton];
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    customOverlay.backgroundColor = [UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(barButtonBackPressed:)];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    reader.readerView.zoom = 1.0;
    reader.showsZBarControls=NO;
    
    [self presentViewController: reader animated: YES completion:nil];
}
-(void)cameraDenied
{
    // denied
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Select Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
        
        alertButton = @"Go";
    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:alertButton
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   if (canOpenSettings)
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Error" andMessage:alertText andActions:actionsArray withController:[SWDefaults currentTopViewController]];
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil] ;
}
#pragma mark - ZBar's Delegate method
- (void)imagePickerController: (UIImagePickerController*) reader2 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    [reader dismissViewControllerAnimated:YES completion:^{
        NSLog(@"scanned bar code is %@", symbol.data);
        NSLog(@"symbol is %@", symbol.typeName);
        [self refineScannedCustomers:[[SWDatabaseManager retrieveManager] fetchCutomerDetailsForBarCode:[NSString getValidStringValue: symbol.data]]];
    }];
    
}


- (void)refineScannedCustomers:(NSArray *)scannedCustomerDetailsArray
{
    if (scannedCustomerDetailsArray.count>0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Customer_ID ==  %@", [[scannedCustomerDetailsArray objectAtIndex:0] valueForKey:@"Customer_ID"]];
        
        NSArray *filteredList = [customersDataDicsArray filteredArrayUsingPredicate:predicate];
        if (filteredList.count > 0) {
            NSUInteger index = 0;
            index = [customersDataDicsArray indexOfObject:filteredList[0]];
            
            [self tableView:customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            [self btnStartVisit:nil];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:[NSString stringWithFormat:@"%@ is not in today's route list",selectedCustomer.Customer_Name] withController:self];
        }
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Barcode does not match customer. Please try again" withController:self];
    }
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionBottomMarginConstraint.constant=KZero;
}
-(void)showNoSelectionViewWithMessage
{
    [NoSelectionView setHidden:NO];
    NoSelectionLeadingMarginConstraint.constant=266;
    NoSelectionBottomMarginConstraint.constant=8;
}

@end

