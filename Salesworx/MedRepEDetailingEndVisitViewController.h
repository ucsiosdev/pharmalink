//
//  MedRepEDetailingEndVisitViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepElementTitleLabel.h"

#import "MedRepTextView.h"

#import "MedRepEdetailTaskViewController.h"

#import "MedRepEDetailNotesViewController.h"
#import "SalesWorxImageView.h"
#import "MedRepCustomClass.h"
#import "MedRepVisitsStartMultipleCallsViewController.h"
#import "SalesWorxPopOverViewController.h"
#import "SWAppDelegate.h"
#import "SalesWorxDefaultLabel.h"
#import "MedRepPanelTitle.h"
#import "MedRepMeetingAttendeesViewController.h"
#import "SalesWorxImageBarButtonItem.h"
#import "TGPCamelLabels7.h"
#import "TGPDiscreteSlider7.h"
#import "PJRSignatureView.h"
#import "SalesWorxImageAnnimateButton.h"

@protocol SelectedIndexDelegate <NSObject>

-(void)selectedIndexDelegate:(id)selectedIndex;

-(void)visitDidFailedWithIndex:(id)selectedIndex;

@end




@interface MedRepEDetailingEndVisitViewController : UIViewController<StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,UITextViewDelegate,UIAlertViewDelegate,UITextFieldDelegate,TaskPopOverDelegate,EDetailingNotesDelegate,SalesWorxPopoverControllerDelegate>

{
    UIPopoverController* popOverController;
    SalesWorxImageBarButtonItem * endVisitBarButtonItem;
    IBOutlet MedRepElementTitleLabel *doctorHeaderLbl;
    IBOutlet MedRepTextField *meetingAttendeesTextField;
    //Visit_Conclusion_Notes
    
    SWAppDelegate *appDelegate;

    NSString* popOverTitle;
    id selectedDelegate;
    
    
    IBOutlet UITableView *selectedMeetingAttendeesTblView;
    NSString *visitRating;
    
    NSTimer* faceTimeTimer;
    IBOutlet UIButton *notesButton;
    
    IBOutlet UILabel *meetingAttendeesSaperator;
    IBOutlet UILabel *waitTimeLbl;
    IBOutlet UIButton *tasksButton;
    NSString* visitType;
    IBOutlet UIView *visitStatisticsBackgroundView;
    
    IBOutlet UIView *visitRatingBackgroundView;
    IBOutlet UIView *meetingAttendeesBackgroundView;
    
    CGRect oldFrame;
    
    IBOutlet UIButton *taskStatusButton;
    IBOutlet UIButton *notesStatusButton;
    
    UIPopoverController * meetingAttendeesPopOverController;
    AppControl *appCtrl;
    NSString* wordsLimitForVisitConclusion;
    NSString* wordsLimitForNextVisitObjective;
    
    NSMutableArray * meetingAttendeesArray;
    //NSMutableArray* selectedMeetingAttendeesArray;
    
    IBOutlet SalesWorxImageView *notesStatusImageView;
    IBOutlet SalesWorxImageView *taskStatusImageView;
    
    IBOutlet NSLayoutConstraint *xMeetingAttendeesWidthConstraint;
    
    
    IBOutlet NSLayoutConstraint *xMeetingAttendeesTrainlingConstraint;
    
    IBOutlet MedRepTextView *nextVisitObjectiveTextView;
    
    IBOutlet SalesWorxDropShadowView *nextVisitObjectiveView;
    
    IBOutlet NSLayoutConstraint *xNextVisitObjectiveTrailingConstraint;
    IBOutlet NSLayoutConstraint *xNextVisitObjectiveWidthConstraint;
    
    
    IBOutlet TGPCamelLabels7 *camelLabelsView;
    IBOutlet TGPDiscreteSlider7 *descreteSliderView;
    
    IBOutlet PJRSignatureView *pjrSignView;
    BOOL isSignatureCaptured;
    IBOutlet SalesWorxImageAnnimateButton *signatureClearButton;
    IBOutlet SalesWorxImageAnnimateButton *signatureSaveButton;
    IBOutlet UIView *signatureContainerView;
    NSString * nextVisitObjectiveTitle;
    IBOutlet MedRepPanelTitle *nextVisitObjectiveTitleLabel;
    
    IBOutlet NSLayoutConstraint *nextVisitObjectiveTrailingConstraint;
    IBOutlet NSLayoutConstraint *nextVisitObjectiveWidthConstraint;
    IBOutlet NSLayoutConstraint *nextVisitObjectiveLeadingConstraint;
    IBOutlet NSLayoutConstraint *visitConclusionNotesWidthConstraint;
    
}

@property(nonatomic) id selectedDelegate;


@property(strong,nonatomic)NSMutableArray* demonstratedProductsArray;
@property(strong,nonatomic) NSMutableArray *selectedMeetingAttendeesArray;

@property(strong,nonatomic) NSMutableArray * samplesGivenArray;

@property (strong, nonatomic) IBOutlet UIView *meetingAttendeesBorderView;
@property(strong,nonatomic)         NSMutableDictionary* visitAdditionalInfoDict;

@property (strong, nonatomic) IBOutlet UIView *visitConclusionView;
@property(nonatomic) BOOL endVisitWithoutCall;

@property (strong, nonatomic) IBOutlet UILabel *roundTableMeetingLbl;
@property(strong,nonatomic) NSMutableDictionary* visitDetailsDict;

@property(strong,nonatomic) NSString* selectedDoctorName;

@property (strong, nonatomic) IBOutlet UILabel *doctorLbl;

@property (strong, nonatomic) IBOutlet UILabel *locationLbl;

@property (strong, nonatomic) IBOutlet UILabel *visitStartTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel *visitEndTimeLbl;
@property (strong, nonatomic) IBOutlet MedRepTextView *visitConclusionNotesTxtView;
@property (strong, nonatomic) IBOutlet MedRepTextView *meetingAttendeesTxtView;

-(void)dismissKeyboard;
@property(strong,nonatomic) VisitDetails * visitDetails;

@property(nonatomic) BOOL isFromDashboard;

//contact outlets

@end
