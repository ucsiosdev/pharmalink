//
//  HTMLViewerViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 9/29/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HTMLViewerDelegate

-(void)willCloseWebPage;

@end

@interface HTMLViewerViewController : UIViewController
{
    id htmlDelegate;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) NSURLRequest *req;
@property(nonatomic) id htmlDelegate;
@property(nonatomic) BOOL isFromProductsScreen;


@end
