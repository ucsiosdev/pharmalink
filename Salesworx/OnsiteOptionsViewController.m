//
//  OnsiteOptionsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "OnsiteOptionsViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SWDefaults.h"

@interface OnsiteOptionsViewController ()

@end

@implementation OnsiteOptionsViewController


@synthesize segmentControl,currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [segmentControl setTitle:kOnsiteVisitTitle forSegmentAtIndex:0];
    [segmentControl setTitle:kTelephonicVisitTitle forSegmentAtIndex:1];
    
    [segmentControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)segmentAction:(UISegmentedControl *)segment
{
    
}

+ (BOOL)coordinate:(CLLocationCoordinate2D)coord inRegion:(MKCoordinateRegion)region
{
    CLLocationCoordinate2D center = region.center;
    MKCoordinateSpan span = region.span;
    
    BOOL result = YES;
    result &= cos((center.latitude - coord.latitude)*M_PI/180.0) > cos(span.latitudeDelta/2.0*M_PI/180.0);
    result &= cos((center.longitude - coord.longitude)*M_PI/180.0) > cos(span.longitudeDelta/2.0*M_PI/180.0);
    return result;
}
float MilesToMeters(float miles) {
    // 1 mile is 1609.344 meters
    // source: http://www.google.com/search?q=1+mile+in+meters
    return 1609.344f * miles;
}


- (IBAction)segmentSwitch:(UISegmentedControl *)sender {
    
    NSString* title=  [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];

    
    NSLog(@"selected title is %@", title);
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        //onsite  clicked
        
        
//        
//        MKCoordinateSpan customerSpan=MKCoordinateSpanMake(<#CLLocationDegrees latitudeDelta#>, <#CLLocationDegrees longitudeDelta#>)
//        
//        MKCoordinateRegion customerRegion = MKCoordinateRegionMake(customerLocation, <#MKCoordinateSpan span#>)
        
        
        
        NSLog(@"SETTING VISIT STATUS TO ONSITE");
        
        
        [SWDefaults setOnsiteVisitStatus:YES];
        
        
//        NSString *isDCFlag = [[SWDefaults userProfile] objectForKey:@"Is_DC_Optional"];
//
//        
//        if ([isDCFlag isEqualToString:@"Y"]) {
//            
//           
//            [SWDefaults isDCOptionalforOnsiteVisit:YES];
//            
//            
//        }
        
        if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
        {
            [self.delegate selectedVisitOption:title];
        }
        
        
        //now enable button in sw visit options if dc optinal is no and visit is telephonic, then we should not restrict user to gocomplete dc
        
        
        
        //write a delegate may be
        
        
        
        
        
        [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];

        
        
        /*
        UIAlertView* onsiteAlert=[[UIAlertView alloc]initWithTitle:@"You have selected Onsite Visit" message:@"Would you like to continue?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"CANCEL", nil];
        onsiteAlert.tag=100;
        
        [onsiteAlert show];*/
        
        
        

        
    }
    else{
        
        /*
        
        UIAlertView* telephonicAlert=[[UIAlertView alloc]initWithTitle:@"You have selected Telephonic Visit" message:@"Would you like to continue?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"CANCEL", nil];
        telephonicAlert.tag=200;
        
        [telephonicAlert show];*/
        
        //telephonic  clicked
        
        NSString* title=  [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];

        
        NSLog(@"SETTING VISIT STATUS TO TELEPHONIC");

        
        [SWDefaults setOnsiteVisitStatus:NO];
        
        

        if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
        {
            [self.delegate selectedVisitOption:@"Telephonic"];
        }
        
        
        [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];


        
       // [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];

    }
}

/*

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100) {
        
        if (buttonIndex==0) {
            
            
            [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];

            
        }
        
        else
        {

        }
        
    }
    
    else
    {
        if (buttonIndex==0) {
            
            //telephonic selected
            

            
            
            [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];

            
        }
        
        else
        {
            
        }
    }
}
*/



@end
