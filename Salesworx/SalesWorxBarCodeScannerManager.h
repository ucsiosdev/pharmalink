//
//  SalesWorxBarCodeScannerManager.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/22/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZBarSDK.h"
#import "SWDefaults.h"

@protocol SalesWorxBarCodeScannerManagerDelegate <NSObject>
@end


@interface SalesWorxBarCodeScannerManager : NSObject<ZBarReaderDelegate,ZBarReaderViewDelegate,ZBarHelpDelegate>
{
    ZBarReaderViewController *reader;

}
@property(nonatomic) id  swxBCMdelegate;
-(void)ScanBarCode;

@end
