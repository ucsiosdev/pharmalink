//
//  SWDatePickerViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/17/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDatePickerViewController.h"

@interface SWDatePickerViewController ()

@end

@implementation SWDatePickerViewController

@synthesize picker;
@synthesize target;
@synthesize action;
@synthesize selectedDate;
@synthesize toolbar,isRoute,forReports;

- (id)initWithTarget:(id)t action:(SEL)a {
    self = [super init];
    if (self) {
        [self setTarget:t];
        [self setAction:a];
        [self setTitle:NSLocalizedString(@"Select Date", nil)];
        self.forReports= NO;
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setPicker:[[UIDatePicker alloc] init]];
    [self.picker setFrame:CGRectMake(0, 0, 300, 400)];
    [self.picker setDatePickerMode:UIDatePickerModeDate];
    [self.picker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    NSLog(@"is todate %hhd", self.isToDate);
    
    if (self.isRoute )
    {
        [self.picker setMinimumDate:[NSDate date]];
    }
    
    if (forReports) {
        
        //date fix in reports
        NSLog(@"test");
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDate *currentDate = [NSDate date];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setYear:30];
        NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
        
        [self.picker setMaximumDate:maxDate];

    }
    [self.view addSubview:self.picker];
    
    [self setPreferredContentSize:CGSizeMake(300, 350)];
    
    [self setToolbar:[[UIToolbar alloc] init]];
    [self.toolbar setTranslucent:YES];
    [self.toolbar setBarStyle:UIBarStyleBlackTranslucent];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dateSelectionDone)];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelSelection)];
    
    [self.toolbar setItems:[NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil]];
    
    [self.view addSubview:self.toolbar];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.toolbar setFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}

- (void)dateChanged:(id)sender{
    
}

- (void)cancelSelection {
     #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action withObject:nil]; 
                #pragma clang diagnostic pop
}

- (void)dateSelectionDone {
    [self setSelectedDate:self.picker.date];
    NSLog(@"slected date %@", self.picker.date);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:self];
#pragma clang diagnostic pop

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
