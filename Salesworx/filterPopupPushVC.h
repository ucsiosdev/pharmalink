//
//  filterPopupPushVC.h
//  AWR-CMS
//
//  Created by Unique Computer Systems on 5/19/14.
//  Copyright (c) 2014 Saad Ansari. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol filterPopupPushVCDelegate <NSObject>
@required
-(void)selectedChild:(NSInteger)childIndex;
@end

@interface filterPopupPushVC : UIViewController <UISearchBarDelegate,UISearchDisplayDelegate>{
    
    BOOL searching;
}
- (IBAction)backButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) NSMutableArray *tableArrayFrom;
@property (nonatomic, strong) NSMutableArray *tableArray;
@property (nonatomic, strong) NSMutableArray *tableArrayFiletered;
@property (nonatomic, weak) id<filterPopupPushVCDelegate> delegate;
@property (nonatomic, strong) UIPopoverController *selectedPopover;

@end
