//
//  SalesWorxCoachReportViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 2/19/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxCoachReportViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@interface SalesWorxCoachReportViewController ()

@end

NSString *const keyIndent = @"level";
NSString *const keyChildren = @"Objects";


@implementation SalesWorxCoachReportViewController
@synthesize surveyTableView,selectedSurveyType,isFromVisit;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
     selectedSurveyTitle = [NSString getValidStringValue:selectedSurveyType.Survey_Title];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:[NSString isEmpty:selectedSurveyTitle]?NSLocalizedString(kFieldSalesCoachReportScreenName, nil):selectedSurveyTitle];
    
    self.navigationController.navigationBarHidden = NO;
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
        [leftBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
       // [self.navigationItem setLeftBarButtonItem:leftBtn];
    }
    
    UIBarButtonItem* closeButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    [closeButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    [self.navigationItem setLeftBarButtonItem:closeButton];

    
    
    UIBarButtonItem* nextBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next",nil) style:UIBarButtonItemStylePlain target:self action:@selector(NextButtonTapped)];
    nextBarButtonItem.tintColor=[UIColor whiteColor];
    [nextBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    UIBarButtonItem * saveBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    saveBarButtonItem.tintColor=[UIColor whiteColor];
    [saveBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

  
    
    if ([selectedSurveyType.Survey_Type_Code isEqualToString:@"D"]) {
        //this is tore audit no signature types needed
        self.navigationItem.rightBarButtonItem=saveBarButtonItem;
    }
    else
    {
        self.navigationItem.rightBarButtonItem=nextBarButtonItem;
    }
    
    
    NSDictionary *plistDict = [[SWDatabaseManager retrieveManager]fetchCoachSurveyStructure:selectedSurveyType];
    mainArray = [plistDict valueForKey:@"Objects"];
    menuItemsArray = [[NSMutableArray alloc] init];
    selectArray = [[NSMutableArray alloc] init];
    [menuItemsArray addObjectsFromArray:mainArray];
    
    [menuItemsArray setValue:selectedSurveyType.Survey_Type_Code forKey:@"Survey_Type_Code"];

    NSLog(@"menu items array in view did load %@",menuItemsArray);
    
    
    
    surveyTableView.delegate = self;
    surveyTableView.dataSource = self;
    
    surveyTableView.tableFooterView = [UIView new];
    if (isFromVisit) {
    
    }
    surveyTableView.estimatedRowHeight=70.0f;
    surveyTableView.rowHeight=UITableViewAutomaticDimension;
    [surveyTableView reloadData];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    NSLog(@"selected survey type is %@",selectedSurveyType.Survey_Type_Code);
    arrSurveyConfirmationData = [[SWDatabaseManager retrieveManager]fetchCoachSurveyConfirmationData:selectedSurveyType];
    //textFieldLimit = [[SWDatabaseManager retrieveManager]fetchLimitofField:@"Response" in:@"TBL_Survey_Audit_Responses"];
   // NSLog(@"limit of field RESPONSE in TBL_Survey_Audit_Responses %ld",textFieldLimit);
    textFieldLimit = 1000;
}

-(void)fetchFinalResponseArray
{
    responsesArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0;i<mainArray.count;i++) {
        NSMutableArray* currentArray=[[mainArray objectAtIndex:i] valueForKey:@"Objects"];
        NSMutableArray * filteredArray=[[currentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.isQuestion ='Y'"]] mutableCopy];
        if (filteredArray.count>0) {
            [responsesArray addObjectsFromArray:filteredArray];
        }
        else{
            //check for childs
            for (NSInteger j=0; j<currentArray.count; j++) {
                NSMutableArray * subSectionsArray=[[currentArray objectAtIndex:j]valueForKey:@"Objects"];
                NSMutableArray * filteredArray=[[subSectionsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.isQuestion ='Y'"]] mutableCopy];
                if (filteredArray.count>0) {
                    [responsesArray addObjectsFromArray:filteredArray];
                }
            }
        }
    }
    
    /*
    if([selectedSurveyType.Survey_Type_Code isEqualToString:@"Q"])
    {
    NSPredicate *updatedPredicate=[NSPredicate predicateWithFormat:@"SELF.isUpdated = 'Y'"];
    NSMutableArray* updatedArray=[[responsesArray filteredArrayUsingPredicate:updatedPredicate] mutableCopy];
    responsesArray=[[NSMutableArray alloc]init];
    responsesArray=updatedArray;
    }*/
    NSLog(@"response array count is %ld", responsesArray.count);

}
-(void)insertDatatoDatabase
{
    //inserting data to db
    BOOL success =[[SWDatabaseManager retrieveManager]insertCoachDataSurvey:responsesArray withConfirmation:nil andSurveyType:selectedSurveyType];
    if (success) {
        NSLog(@"coach data inserted successfully");
        
        
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:KAlertOkButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            // [self resetNavigation];
            //[self logoutTapped];
            if([selectedSurveyType.Survey_Type_Code isEqualToString:@"D"]||[selectedSurveyType.Survey_Type_Code isEqualToString:@"Q"])
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
            [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Survey saved successfully" andActions:[[NSMutableArray alloc]initWithObjects:okAction, nil] withController:self];
    }
    else
    {
        
    }
}
-(void)saveButtonTapped
{

    [self fetchFinalResponseArray];
    
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:KAlertYESButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (isFromVisit) {
         
            NSMutableDictionary* coachSurveyDict = [[NSMutableDictionary alloc]init];
            [coachSurveyDict setObject:responsesArray forKey:@"Survey_Responses_Array"];
            [coachSurveyDict setObject:[[NSMutableArray alloc]init] forKey:@"Survey_Confirmation_Array"];

            [[NSNotificationCenter defaultCenter]postNotificationName:@"COACH_SURVEY" object:coachSurveyDict];
            
            UIAlertAction * okAction = [UIAlertAction actionWithTitle:KAlertOkButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];

                
            }];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Survey saved successfully" andActions:[[NSMutableArray alloc]initWithObjects:okAction, nil] withController:self];
            
        }
        
       // [self insertDatatoDatabase];
    }];
    
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:KAlertNoButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:kSaveTitle andMessage:[NSString stringWithFormat:@"Would you like to save %@?",selectedSurveyTitle.lowercaseString] andActions:[[NSMutableArray alloc]initWithObjects:okAction,noAction, nil] withController:self];
    
}

-(void)closeButtonTapped
{
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:kYesTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (self.isFromVisit) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
        [self.navigationController popToRootViewControllerAnimated:YES];
        }
        //[self resetNavigation];
    }];
    
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:kNoTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    NSMutableArray* actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,noAction, nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Close Survey" andMessage:@"Would you like to close the survey? unsaved data will be discarded" andActions:actionsArray withController:self];
    
}
-(void)resetNavigation
{
    AppControl *appControl = [AppControl retrieveSingleton];
    
    SWAppDelegate *appdel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    UINavigationController *menuNavigationController;
    SalesWorxFieldSalesDashboardViewController*     dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
    appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]] trimString];
        
        if([PDARIGHTS containsString:KFieldSalesSectionPDA_RightsCode])
        {
            SalesWorxFieldSalesDashboardViewController*     dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
            appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
            
        }
        else if([PDARIGHTS containsString:KFieldMarketingSectionPDA_RightsCode] ||
                [PDARIGHTS containsString:KFieldMarketingVisitsPDA_RightsCode])
        {
            SalesWorxDashboardViewController *dashboardViewController = [[SalesWorxDashboardViewController alloc] init];
            appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
        }
        
        
        
    }
    
    
    MedRepMenuViewController *medrepMenuViewController;
    medrepMenuViewController = [[MedRepMenuViewController alloc] init] ;
    menuNavigationController = [[UINavigationController alloc] initWithRootViewController:medrepMenuViewController] ;
    
    SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:appdel.mainNavigationController rearViewController:menuNavigationController] ;
    SWSessionManager * sessionManager=[[SWSessionManager alloc] initWithWindow:appdel.window andApplicationViewController:splitViewController];
    [[UINavigationBar appearance]setBarTintColor:kNavigationBarBackgroundColor];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance]setTintColor:kNavigationBarTitleFontColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: kNavigationBarTitleFontColor}];
    
    medrepMenuViewController=nil;
    splitViewController=nil;
    dashboardViewController=nil;
    
    
}

-(void) viewWillAppear:(BOOL)animated {
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:@"Coach Report"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView DataSource and Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuItemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* isQuestion=[NSString getValidStringValue:[[menuItemsArray objectAtIndex:indexPath.row] valueForKey:@"isQuestion"]];
    
    NSString* responseDisplayType= [NSString getValidStringValue:[[menuItemsArray objectAtIndex:indexPath.row] valueForKey:@"Response_Display_Type"]];
    
    NSString* qualityAuditCellIdentifier = @"qualityAuditCell";
    NSString * headername =[[menuItemsArray valueForKey:@"name"] objectAtIndex:indexPath.row];

    //NSLog(@"survey type code is %@",selectedSurveyType.Survey_Type_Code);
    
    if ([isQuestion isEqualToString:KAppControlsYESCode] && [responseDisplayType isEqualToString:kCoachSurveySliderResponseType] && [selectedSurveyType.Survey_Type_Code isEqualToString:@"D"]) {
        SalesWorxCoachSurveyQualityAuditTableViewCell * cell = (SalesWorxCoachSurveyQualityAuditTableViewCell*)[tableView dequeueReusableCellWithIdentifier:qualityAuditCellIdentifier];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCoachSurveyQualityAuditTableViewCell" owner:self options:nil]objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        cell.camelLabelsView.value=0;
        cell.sliderView.value=0;
        cell.commentsTextField.delegate=self;
        cell.commentsTextField.tag=indexPath.row;
        //[cell.commentsTextField addTarget:self action:@selector(remarksTextFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
        
        NSString * remarksText =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"Remarks"] objectAtIndex:indexPath.row]];
        cell.commentsTextField.text = [NSString isEmpty:remarksText]?@"":remarksText;
        
        
        //set color for line one
        NSString* currentString = [headername stringByReplacingOccurrencesOfString:@"\\n" withString:NEW_LINE];

        NSArray* lines = [currentString componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
        NSString *firstLine;
        NSString *secondLine = @"";
        if (lines.count > 0) {
            firstLine = [lines objectAtIndex:0];
            for (int i = 1; i < [lines count]; i++) {
                secondLine = [secondLine stringByAppendingString:[lines objectAtIndex:i]];
            }
        }
        //cell.titleLbl.text = [headername stringByReplacingOccurrencesOfString:@"\\n" withString:NEW_LINE];
        
        
        NSRange range1 = [currentString rangeOfString:firstLine];
        NSRange range2 = [currentString rangeOfString:secondLine];
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:currentString];
        
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:(70.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]} range:range1];

        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} range:range2];
//        [attributedText setAttributes:@{NSFontAttributeName:kSWX_FONT_REGULAR(14)} range:range2];
     //   [attributedText setAttributes:@{NSFontAttributeName:kSWX_FONT_SEMI_BOLD(14)} range:range1];

//        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:(74.0/255.0) green:(84.0/255.0) blue:(120.0/255.0) alpha:1]} range:range1];
//        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]} range:range2];
        
        cell.titleLbl.attributedText = attributedText;
        
        
        //set slider tick count
        cell.camelLabelsView.startCountWithZero=YES;
        cell.sliderView.tickCount = 3;
        cell.camelLabelsView.tickCount = 3;
        
        
        NSString * sliderValue =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"sliderValue"] objectAtIndex:indexPath.row]];
        cell.sliderView.value=[sliderValue floatValue];
        cell.sliderView.tag=indexPath.row;
        [cell.sliderView addTarget:self action:@selector(customSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        NSString* status = [NSString getValidStringValue:[[menuItemsArray valueForKey:@"Status"] objectAtIndex:indexPath.row]];
        if ([status isEqualToString:KAppControlsNOCode]){
            cell.commentsTextField.isReadOnly = YES;
            cell.sliderView.backgroundColor = SalesWorxReadOnlyTextFieldColor;
            cell.sliderView.userInteractionEnabled=NO;
        }
        else{
            cell.sliderView.backgroundColor = UIColor.clearColor;
            cell.sliderView.userInteractionEnabled=YES;
            cell.commentsTextField.isReadOnly = NO;

        }
        return cell;
    }
    else if (([isQuestion isEqualToString:KAppControlsYESCode] && [responseDisplayType isEqualToString:kCoachSurveySegmentResponseType] && [selectedSurveyType.Survey_Type_Code isEqualToString:@"Q"]))
    {
        SalesWorxCoachReportQCTableViewCell * cell = (SalesWorxCoachReportQCTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"qcCell"];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCoachReportQCTableViewCell" owner:self options:nil]objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        cell.remarkstextField.delegate=self;
        cell.remarkstextField.tag=indexPath.row;
        
        NSString * remarksText =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"Remarks"] objectAtIndex:indexPath.row]];
        cell.remarkstextField.text = [NSString isEmpty:remarksText]?@"":remarksText;
       // [cell.remarkstextField addTarget:self action:@selector(remarksTextFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
        cell.questionSegment.tag = indexPath.row;
        [cell.questionSegment addTarget:self action:@selector(segmentChangeViewValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        NSString * segmentValue =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"segmentValue"] objectAtIndex:indexPath.row]];
        if ([NSString isEmpty:segmentValue]) {
            cell.questionSegment.selectedSegmentIndex = UISegmentedControlNoSegment;
        }
        else if ([segmentValue isEqualToString:KAppControlsYESCode])
        {
            cell.questionSegment.selectedSegmentIndex= 0;
        }
        else
        {
            cell.questionSegment.selectedSegmentIndex= 1;
        }
        

        
        cell.questionLabel.text = headername;
        return cell;
        
    }
    
   else if ([isQuestion isEqualToString:KAppControlsYESCode] && [responseDisplayType isEqualToString:kCoachSurveySliderResponseType] ) {
        //show slider only for this cell

        SalesWorxCoachSurveyTableViewCell * cell = (SalesWorxCoachSurveyTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"sliderCell"];
        if (cell==nil) {
                cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCoachSurveyTableViewCell" owner:self options:nil]objectAtIndex:0];
            }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
       tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        cell.camelLabelsView.value=0;
        cell.sliderView.value=0;
        NSString * level =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"level"] objectAtIndex:indexPath.row]];
        if([level isEqualToString:@"0"])
        {
            cell.titleLableLeadingConstraint.constant=38;
        }
        else
        {
            cell.titleLableLeadingConstraint.constant=48;
        }
        
        NSString * headername =[[menuItemsArray valueForKey:@"name"] objectAtIndex:indexPath.row];
        
       
            cell.titleLbl.text = headername;
       
        
        
        
        
        NSString * sliderValue =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"sliderValue"] objectAtIndex:indexPath.row]];

        cell.sliderView.value=[sliderValue floatValue];
        cell.sliderView.tag=indexPath.row;
        [cell.sliderView addTarget:self action:@selector(customSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        cell.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
        return cell;
    }
    else if ([isQuestion isEqualToString:KAppControlsYESCode] && [responseDisplayType isEqualToString:kCoachSurveySegmentResponseType] ) {
        SalesWorxCoachReportMaketingSurveyTableViewCell * cell = (SalesWorxCoachReportMaketingSurveyTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"marketingCell"];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCoachReportMaketingSurveyTableViewCell" owner:self options:nil]firstObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        cell.remarksTextField.delegate=self;
        cell.optionsTextField.delegate=self;

        NSString * headername =[[menuItemsArray valueForKey:@"name"] objectAtIndex:indexPath.row];
        NSString * rating =[[menuItemsArray valueForKey:@"Rating"] objectAtIndex:indexPath.row];
        NSString * remarks =[[menuItemsArray valueForKey:@"Remarks"] objectAtIndex:indexPath.row];
        cell.titleLbl.text=[NSString getValidStringValue:headername];
        cell.optionsTextField.identifier=kRatingTextFieldIdentifier;
        cell.remarksTextField.identifier=kRemarksTextFieldIdentifier;
        cell.optionsTextField.text=[NSString getValidStringValue:rating];
        cell.remarksTextField.text=[NSString getValidStringValue:remarks];

        cell.optionsTextField.tag=indexPath.row;
        cell.remarksTextField.tag=indexPath.row;

        return cell;
        
       /* SalesWorxCoachSurveyTableViewCell * cell = (SalesWorxCoachSurveyTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"sliderCell"];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCoachSurveyTableViewCell" owner:self options:nil]firstObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        
        cell.camelLabelsView.value=0;
        cell.sliderView.value=0;
        
        NSString * headername =[[menuItemsArray valueForKey:@"name"] objectAtIndex:indexPath.row];
        headername = [headername stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        cell.titleLbl.text = headername;
        
        NSString * sliderValue =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"sliderValue"] objectAtIndex:indexPath.row]];
        
        cell.sliderView.value=[sliderValue floatValue];
        cell.sliderView.tag=indexPath.row;
        [cell.sliderView addTarget:self action:@selector(customSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        cell.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
        return cell;*/
        
    }
    else{
        
        
      

        SalesworxCoachSurveyHeaderTableViewCell * cell = (SalesworxCoachSurveyHeaderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesworxCoachSurveyHeaderTableViewCell" owner:self options:nil]firstObject];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(indexPath.row==menuItemsArray.count-1)
        {
            tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        }
        else
        {
            //tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        }
        if (indexPath.row==0) {
            cell.dividerView.hidden=YES;
            cell.dividerViewHeightConstraint.constant=0;
        }
        else
        {
            cell.dividerView.hidden= NO;
            cell.dividerViewHeightConstraint.constant=10;

        }
        NSDictionary *dic = menuItemsArray[indexPath.row];

        NSString * headername =[[menuItemsArray valueForKey:@"name"] objectAtIndex:indexPath.row];
        headername = [headername stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        
        NSString * level =[NSString getValidStringValue:[[menuItemsArray valueForKey:@"level"] objectAtIndex:indexPath.row]];

 

        
        cell.titleLbl.text = headername;

        if ([selectArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
           // [self acccesory:cell :@"CoachUpArrow"];
            if ([level isEqualToString:@"0"]) {
                cell.thumbImageView.image=[UIImage imageNamed:@"CoachHeaderUp"];
                cell.imageViewLeadingConstraint.constant=8;

            }
            else if ([level isEqualToString:@"1"])
            {
                cell.thumbImageView.image=[UIImage imageNamed:@"CoachContentUp"];
                cell.imageViewLeadingConstraint.constant=16;

            }
           // NSLog(@"level is %@",level);
            if ([level isEqualToString:@"0"]) {
                cell.backgroundColor=[UIColor blueColor];
            }
            else{
                cell.backgroundColor=KCoachContentColor;
            }
            
        }
        else
        {
            //NSLog(@"level in else is %@",level);

            if ([(NSArray*)dic[keyChildren] count])
            {
               // [self acccesory:cell :@"CoachDownArrow"];
                
                if ([level isEqualToString:@"0"]) {
                    cell.imageViewLeadingConstraint.constant=8;

                    cell.thumbImageView.image=[UIImage imageNamed:@"CoachHeaderDown"];
                }
                else if ([level isEqualToString:@"1"])
                {
                    cell.imageViewLeadingConstraint.constant=16;
                    cell.thumbImageView.image=[UIImage imageNamed:@"CoachContentDown"];
                }
                
                cell.backgroundColor=KCoachContentColor;
                
            }
            else
            {
                cell.accessoryView = nil;
            }
        }
        
        if ([level isEqualToString:@"0"]) {
            cell.backgroundColor=KCoachHeaderColor;
        }
        
        return cell;
        
        /*
        NSString * cellIdentifier = @"cellIdentifier";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil)
        {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            
        }
        
        for (UIView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
        
        NSDictionary *dic = menuItemsArray[indexPath.row];
        
        NSInteger indLevel=[menuItemsArray[indexPath.row][keyIndent] integerValue];
        NSLog(@"indent level %ld", (long)indLevel);
        

        if ([selectArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
            [self acccesory:cell :@"CoachUpArrow"];
       
            cell.backgroundColor=surveyCellExpandColor;
 cell.textLabel.textColor=KSalesOrderProductTableViewBrandCodeCellExpandTextColor;
            cell.textLabel.font = kSWX_FONT_SEMI_BOLD(16);

        }
        else
        {
            if ([(NSArray*)dic[keyChildren] count])
            {
                [self acccesory:cell :@"CoachDownArrow"];
        cell.backgroundColor=surveyCellCollapseColor;
        cell.textLabel.textColor=KSalesOrderProductTableViewBrandCodeCellCollapsedTextColor;
                cell.textLabel.font = kSWX_FONT_SEMI_BOLD(16);

            }
            else
            {
                cell.accessoryView = nil;
            }
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        
        
        NSString * headername =[[menuItemsArray valueForKey:@"name"] objectAtIndex:indexPath.row];
        headername = [headername stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        
        cell.textLabel.text = headername;
        
        cell.indentationWidth = 20;
        cell.indentationLevel =[menuItemsArray[indexPath.row][keyIndent] integerValue];
        
//        cell.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
       // cell.backgroundColor=kUITableViewHeaderBackgroundColor;

        
        //cell.backgroundColor
        // cell.contentView.superview.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:(CGFloat)(cell.indentationLevel)/25.0];
        
        //cell.textLabel.backgroundColor = [UIColor clearColor];
  
        
        return cell;
         */
    }
}

- (void)customSliderValueChanged:(TGPDiscreteSlider7*)slider
{
    [[menuItemsArray objectAtIndex:slider.tag]setValue:[NSString stringWithFormat:@"%.0f",slider.value] forKey:@"sliderValue"];
    
    [[menuItemsArray objectAtIndex:slider.tag]setValue:KAppControlsYESCode forKey:@"isUpdated"];
    
    [[menuItemsArray objectAtIndex:slider.tag]setValue:selectedSurveyType.Survey_Type_Code forKey:@"Survey_Type_Code"];

    [[menuItemsArray objectAtIndex:slider.tag]setValue:selectedSurveyType.Survey_ID forKey:@"Survey_ID"];

}
-(void)segmentChangeViewValueChanged:(UISegmentedControl *)segmentControl
{
    NSString* selectedSegmentText=[[NSString alloc]init];
    selectedSegmentText=segmentControl.selectedSegmentIndex ==0 ? KAppControlsYESCode:KAppControlsNOCode;
    [[menuItemsArray objectAtIndex:segmentControl.tag]setValue:[NSString getValidStringValue:selectedSegmentText] forKey:@"segmentValue"];
    [[menuItemsArray objectAtIndex:segmentControl.tag]setValue:KAppControlsYESCode forKey:@"isUpdated"];
    [[menuItemsArray objectAtIndex:segmentControl.tag]setValue:selectedSurveyType.Survey_Type_Code forKey:@"Survey_Type_Code"];
  [[menuItemsArray objectAtIndex:segmentControl.tag]setValue:selectedSurveyType.Survey_ID forKey:@"Survey_ID"];
}

#pragma mark - Table View Delegate
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* isQuestion=[NSString getValidStringValue:[[menuItemsArray objectAtIndex:indexPath.row] valueForKey:@"isQuestion"]];

    //not applicable only for other survey, only for store audit.
    if ([isQuestion isEqualToString:KAppControlsYESCode] && [selectedSurveyType.Survey_Type_Code isEqualToString:@"D"]) {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray* actionsArray = [[NSMutableArray alloc]init];
    NSMutableDictionary* currentDict = [menuItemsArray objectAtIndex:indexPath.row];
    NSString* currentStatus = [NSString getValidStringValue:[currentDict valueForKey:@"Status"]];
    NSString* currentStatusTitle = [currentStatus isEqualToString:KAppControlsNOCode]?@"Applicable":@"Not Applicable";
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:currentStatusTitle handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // maybe show an action sheet with more options
        NSLog(@"not applicable tapped");
        
        NSString* status = [currentStatus isEqualToString:KAppControlsNOCode]?KAppControlsYESCode:KAppControlsNOCode;
        [currentDict setValue:status forKey:@"Status"];
        [menuItemsArray replaceObjectAtIndex:indexPath.row withObject:currentDict];
        [surveyTableView beginUpdates];
        [surveyTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        [surveyTableView endUpdates];
        
    }];
    moreAction.backgroundColor = [UIColor redColor];
    [actionsArray addObject:moreAction];
    return actionsArray;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    NSDictionary *dic = menuItemsArray[indexPath.row];
    
    NSString* isQuestion=[dic valueForKey:@"isQuestion"];
    
    if ([isQuestion isEqualToString:KAppControlsYESCode]) {
        NSLog(@"Tapped on question %@",[[menuItemsArray objectAtIndex:indexPath.row] valueForKey:@"name"]);
    }
    
    
    NSArray *ar=[dic valueForKey:keyChildren];
    
    
    
    NSInteger indentLevel = [menuItemsArray[indexPath.row][keyIndent] integerValue]; //indentLevel of the selected cell
    
    BOOL isChildrenAlreadyInserted = [menuItemsArray containsObject:dic[keyChildren]]; //checking contains children
    NSLog(@"isChildrenAlreadyInserted step 1 %hhd",isChildrenAlreadyInserted);
    for(NSDictionary *dicChildren in dic[keyChildren])
    {
        NSInteger indexe=[menuItemsArray indexOfObjectIdenticalTo:dicChildren];
        
        isChildrenAlreadyInserted=(indexe>0 && indexe!=NSIntegerMax); //checking contains children
        NSLog(@"isChildrenAlreadyInserted step 2 %hhd",isChildrenAlreadyInserted);

        if(isChildrenAlreadyInserted) break;
        
    }
    
    NSString * T = [[menuItemsArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    if(isChildrenAlreadyInserted)
    {
        [self miniMizeThisRows:ar];
        
        if(indentLevel ==0)
        {
            [selectArray removeAllObjects];
        }
        
        [selectArray removeObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        
        [self performSelector:@selector(tblReload) withObject:indexPath afterDelay:0.20];
        
    }
    
    else if([(NSArray*)dic[keyChildren] count])
    {
        NSUInteger count=indexPath.row+1;
        
        if(removeArray.count && indentLevel ==0)
        {
            [self miniMizeThisRows:removeArray];
            
            [selectArray removeAllObjects];
            
            for (int i=0; i< menuItemsArray.count; i++)
            {
                NSString * T1 = [[menuItemsArray objectAtIndex:i] valueForKey:@"name"];
                
                if([T1 isEqualToString:T])
                {
                    count = i+1;
                }
            }
            
            [selectArray addObject:[NSString stringWithFormat:@"%ld",(long)count-1]];
        }
        else
        {
            [selectArray addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        }
        
        NSMutableArray *arCells=[NSMutableArray array];
        for(NSDictionary *dInner in ar )
        {
            [arCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
            [menuItemsArray insertObject:dInner atIndex:count++];
        }
        
        if(indentLevel == 0)
        {
            removeArray = ar;
        }
        
        if(selectedIndexPath == nil)
        {
            selectedIndexPath = indexPath;
        }
        
        [self.surveyTableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationNone];
        
        [self performSelector:@selector(tblReload) withObject:indexPath afterDelay:0.20];
        
    }
    else
    {
        
        // Navigation to other View
        
    }
    
}

-(void)tblReload
{
    [self.surveyTableView reloadData];
}

-(void)miniMizeThisRows:(NSArray*)ar
{
    
    for(NSDictionary *dInner in ar )
    {
        NSUInteger indexToRemove=[menuItemsArray indexOfObjectIdenticalTo:dInner];
        
        NSArray *arInner=[dInner valueForKey:keyChildren];
        if(arInner && [arInner count]>0){
            [self miniMizeThisRows:arInner];
        }
        
        if([menuItemsArray indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
            [menuItemsArray removeObjectIdenticalTo:dInner];
            
            
            [self.surveyTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                          [NSIndexPath indexPathForRow:indexToRemove inSection:0]]withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* isQuestion=[NSString getValidStringValue:[[menuItemsArray objectAtIndex:indexPath.row] valueForKey:@"isQuestion"]];
    
    if ([isQuestion isEqualToString:KAppControlsYESCode]) {
//        NSString *headername = [[menuItemsArray valueForKey:@"name"] objectAtIndex:indexPath.row];
//        headername = [headername stringByReplacingOccurrencesOfString:@"\\n" withString:NEW_LINE];
//
//        CGFloat cvWidth = (self.view.bounds.size.width-480);
//        if ([selectedSurveyType.Survey_Type_Code isEqualToString:@"D"]) {
//            cvWidth = (self.view.bounds.size.width-530);
//        }
//
//        CGSize size = [headername boundingRectWithSize:CGSizeMake(cvWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"WeblySleekUISemibold" size:16]} context:nil].size;
//        size.height = size.height+16;
//
//        if (size.height <= 70) {
//            return 70;
//        } else {
//            return size.height;
//        }
        if(isFromVisit)
        {
        return UITableViewAutomaticDimension;
        }
        else
        {
            return 70.0f;
        }
    }
    else{
        return indexPath.row==0?50:60;
    }
}

-(void)acccesory :(UITableViewCell *)cell :(NSString*)img
{
    UIEdgeInsets insets = UIEdgeInsetsMake(24, 0, 0, 0);
    // Create custom accessory view, in this case an image view
    UIImage *customImage = [UIImage imageNamed:img];
    UIImageView *accessoryView = [[UIImageView alloc] initWithImage:customImage];
    
    // Create wrapper view with size that takes the insets into account
    UIView *accessoryWrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, customImage.size.width+insets.left+insets.right, customImage.size.height+insets.top+insets.bottom)];
    [accessoryWrapperView setBackgroundColor:[UIColor clearColor]];
    
    // Add custom accessory view into wrapper view
    [accessoryWrapperView addSubview:accessoryView];
    
    // Use inset's left and top values to position the custom accessory view inside the wrapper view
    accessoryView.frame = CGRectMake(insets.left+12, insets.top-5, customImage.size.width-10, customImage.size.height-10);
    
    // Set accessory view of cell (in this case this code is called from within the cell)
    cell.accessoryView = accessoryWrapperView;
}

#pragma mark NextButton Action
-(void)NextButtonTapped
{

    [self.view endEditing:YES];
    
    [self fetchFinalResponseArray];
    
    
    
    
   
    SalesWorxCoachReportConfirmationViewController *coachReportConfirmationVC = [[SalesWorxCoachReportConfirmationViewController alloc]init];
    coachReportConfirmationVC.surveyResponsesArray = responsesArray;
    coachReportConfirmationVC.arrSurveyConfirmationData = arrSurveyConfirmationData;
    coachReportConfirmationVC.selectedSurveyType=selectedSurveyType;
    coachReportConfirmationVC.isFromVisit=isFromVisit;
    [self.navigationController pushViewController:coachReportConfirmationVC animated:YES];
    
}


#pragma mark TextField methods

- (BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{

    
    if([selectedTextField isFirstResponder])
    {
        [selectedTextField resignFirstResponder];
    }
    selectedTextField=textField;
    
    
    if ([[NSString getValidStringValue:textField.identifier]isEqualToString:kRatingTextFieldIdentifier]) {
        
     contentArray=[[NSMutableArray alloc]initWithObjects:@" Unsatisfactory",@"Needs Improvement",@"Average",@"Good",@"Excellent", nil];
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    dashboardPopoverController=nil;
    dashboardPopoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    dashboardPopoverController.delegate=self;
    popOverVC.popOverController=dashboardPopoverController;
    popOverVC.titleKey=@"Survey Type";
    [dashboardPopoverController setPopoverContentSize:CGSizeMake(250, 260) animated:YES];

    [dashboardPopoverController presentPopoverFromRect:textField.dropdownImageView.frame inView:textField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    return NO;
    }
    else
    {
        return YES;
    }
}

-(void)remarksTextFieldDidEndEditing:(UITextField*)textField
{
    
    [[menuItemsArray objectAtIndex:textField.tag]setValue:[NSString getValidStringValue:textField.text] forKey:@"Remarks"];
    
    
    [[menuItemsArray objectAtIndex:textField.tag]setValue:KAppControlsYESCode forKey:@"isUpdated"];
    
    [[menuItemsArray objectAtIndex:textField.tag]setValue:selectedSurveyType.Survey_Type_Code forKey:@"Survey_Type_Code"];

      [[menuItemsArray objectAtIndex:textField.tag]setValue:selectedSurveyType.Survey_ID forKey:@"Survey_ID"];
    
    NSLog(@"menu items array after comments is %@", [menuItemsArray valueForKey:@"Remarks"]);
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= textFieldLimit;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [[menuItemsArray objectAtIndex:textField.tag]setValue:[NSString getValidStringValue:textField.text] forKey:@"Remarks"];
  
    
    [[menuItemsArray objectAtIndex:textField.tag]setValue:KAppControlsYESCode forKey:@"isUpdated"];

    [[menuItemsArray objectAtIndex:textField.tag]setValue:selectedSurveyType.Survey_Type_Code forKey:@"Survey_Type_Code"];

    [[menuItemsArray objectAtIndex:textField.tag]setValue:selectedSurveyType.Survey_ID forKey:@"Survey_ID"];

    

    NSLog(@"menu items array after comments is %@", [menuItemsArray valueForKey:@"Remarks"]);
    /*
    if ([[NSString getValidStringValue:selectedTextField.identifier]isEqualToString:kRemarksTextFieldIdentifier]) {

        [[menuItemsArray objectAtIndex:selectedIdx.row]setValue:KAppControlsYESCode forKey:@"isUpdated"];

    [[menuItemsArray objectAtIndex:selectedIdx.row]setValue:[NSString getValidStringValue:textField.text] forKey:@"Remarks"];
        
        NSLog(@"setting remarks %@ for row %ld", textField.text,(long)selectedIdx.row);
        selectedTextField.text= textField.text;

       // [surveyTableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:selectedIdx, nil] withRowAnimation:UITableViewRowAnimationNone];

    }*/
}


-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSString* selectedOption=[NSString getValidStringValue:[contentArray objectAtIndex:selectedIndexPath.row]];
    [[menuItemsArray objectAtIndex:selectedTextField.tag]setValue:selectedOption forKey:@"Rating"];
    [[menuItemsArray objectAtIndex:selectedTextField.tag]setValue:KAppControlsYESCode forKey:@"isUpdated"];

    selectedTextField.text= selectedOption;
    
   // [surveyTableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:selectedIdx, nil] withRowAnimation:UITableViewRowAnimationNone];
    
}


@end

