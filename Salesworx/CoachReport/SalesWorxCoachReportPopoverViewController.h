//
//  SalesWorxCoachReportPopoverViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/19/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepTextField.h"
#import "NSString+Additions.h"
#import "SWDatabaseManager.h"
#import "SurveyListViewController.h"
#import "SalesWorxCoachReportPharmacyDetailsViewController.h"

@protocol CoachReportViewControllerDelegate

-(void)CoachNameValidate:(CoachSurveyType*)selectedSurvey ;

@end

@interface SalesWorxCoachReportPopoverViewController : UIViewController<UITextFieldDelegate,SalesWorxSurveyListViewControllerDelegate,UIPopoverControllerDelegate>
{
    IBOutlet MedRepTextField *surveyTypeTextField;
    IBOutlet MedRepTextField *txtUserName;
    IBOutlet MedRepTextField *txtPassword;
    
    IBOutlet NSLayoutConstraint *XPanelviewHeightConstraint;
    IBOutlet UIView *placeholderView;
    IBOutlet UIView *coachReportPopoverContentView;
    IBOutlet NSLayoutConstraint *xPharmacistCiewHeightConstraint;
    IBOutlet UIView *headerView;
    
    IBOutlet UIView *pharmacyDetailsView;
    id CoachReportDelegate;
    NSMutableArray* surveysArray;
    CoachSurveyType * selectedSurvey;
    UIPopoverController *dashboardPopoverController;
}

@property(nonatomic) id CoachReportDelegate;


@end


