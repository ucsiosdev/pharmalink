//
//  SalesWorxCoachReportPopoverViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 2/19/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxCoachReportPopoverViewController.h"
#import "SalesWorxCoachReportViewController.h"
#import "MedRep-Swift.h"
@interface SalesWorxCoachReportPopoverViewController ()

@end

@implementation SalesWorxCoachReportPopoverViewController
@synthesize CoachReportDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    
    coachReportPopoverContentView.layer.cornerRadius = 5.0;
    [coachReportPopoverContentView setBackgroundColor:UIViewControllerBackGroundColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if(self.isMovingToParentViewController)
    {
        
        coachReportPopoverContentView.backgroundColor=UIViewControllerBackGroundColor;
        
        for (UIView *borderView in coachReportPopoverContentView.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                if (borderView.tag==101|| borderView.tag==102|| borderView.tag==103|| borderView.tag==104 || borderView.tag==105) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                }
            }
        }
       // [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
        
         surveysArray=[[SWDatabaseManager retrieveManager] fetchSurveys];
        if (surveysArray.count>0) {
            if (surveysArray.count==1) {
                selectedSurvey=[surveysArray objectAtIndex:0];
               // surveyTypeTextField.text=[NSString getValidStringValue: selectedSurvey.Survey_Title];
               // surveyTypeTextField.isReadOnly=YES;
            }
            
        }
    }
    else
    {
       // [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        //[coachReportPopoverContentView setHidden:NO];
    }
    
    
   /*
    if (self.isMovingToParentViewController) {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        [coachReportPopoverContentView setHidden:NO];
    }*/
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(kFieldSalesCoachReportScreenName, nil)];
    self.navigationController.navigationBarHidden = NO;
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
      UIBarButtonItem*  leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
        [leftBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
         [self.navigationItem setLeftBarButtonItem:leftBtn];
    }
    
    surveyTypeTextField.text=@"";
    txtUserName.text=@"";
    txtPassword.text=@"";
    
    /*
    BOOL surveyCompletedForToday=[[SWDatabaseManager retrieveManager]surveyCompletedForToday];
    if (surveyCompletedForToday) {
        placeholderView.hidden=NO;
    }
    else
    {
        placeholderView.hidden=YES;
    }*/
    placeholderView.hidden=YES;

}



-(void)viewDidAppear:(BOOL)animated
{
    pharmacyDetailsView.hidden=YES;
    xPharmacistCiewHeightConstraint.constant =0.0;
    XPanelviewHeightConstraint.constant=325;

    
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
}

- (IBAction)closeButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
   /* [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO completion:^{
        }];
    });*/
}

- (IBAction)btnSaveCoachName:(id)sender
{
    [self.view endEditing:YES];

    NSString *errorMessageString;
    
    if (txtUserName.text.length==0) {
        errorMessageString=@"Please enter coach name and try again";
    }
    else if (txtPassword.text.length==0) {
        errorMessageString=@"Please enter password and try again";
    }
    else if (surveyTypeTextField.text.length == 0) {
        errorMessageString=@"Please select survey type and try again";
    }

    if (![NSString isEmpty:errorMessageString]) {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:errorMessageString withController:self];
    } else {

        BOOL isSurveyCompletedPreviously=[[SWDatabaseManager retrieveManager]isCoachSurveyCompletedForToday:selectedSurvey.Survey_ID];
        if (isSurveyCompletedPreviously) {
            
            [SWDefaults showAlertAfterHidingKeyBoard:@"Survey Completed" andMessage:[NSString stringWithFormat:@"%@ completed for today",[NSString getValidStringValue:selectedSurvey.Survey_Title]] withController:self];
            
        }
        else
        {
        
        
        NSString *strUserName = [NSString getValidStringValue:txtUserName.text];
        NSString *strPassword = [[SWDatabaseManager retrieveManager] digest:txtPassword.text];
        
        BOOL isCoachSurveyAllowed = NO;
            NSString* validationQry =[NSString stringWithFormat:@"SELECT * FROM 'TBL_Users_ALL' where Username = '%@' and Password = '%@' and Is_Coach = 'Y'",strUserName, [strPassword uppercaseString]];
            NSLog(@"coach validation qry %@", validationQry);
            
        NSMutableArray *arrUser = [[SWDatabaseManager retrieveManager]fetchDataForQuery:validationQry];
        
        if (arrUser.count > 0) {
            isCoachSurveyAllowed = YES;
        }

        if (isCoachSurveyAllowed) {
            
            [SWDefaults setCoachProfile:[[arrUser objectAtIndex:0] valueForKey:@"User_ID"]];
            
         

            
            /*
            SalesWorxCoachReportViewController * reportVC=[[SalesWorxCoachReportViewController alloc]init];
            reportVC.selectedSurveyType=selectedSurvey;
            [self.navigationController pushViewController:reportVC animated:YES];
            
            /*
            SurveySwiftDetailsViewController *surveyVC  =  [[SurveySwiftDetailsViewController alloc]init];
            NSString* surveyTimeStampSwift=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            NSString* salesRepIDSwift=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];
            NSString* empCodeSwift=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];
            NSString* createdBySwift=[[SWDefaults userProfile] stringForKey:@"User_ID"];
            
            NSDictionary * visitDetailsDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:@"",@"Actual_Visit_ID",salesRepIDSwift,@"SalesRep_ID",empCodeSwift,@"Emp_Code",createdBySwift,@"Created_By",surveyTimeStampSwift,@"Survey_Time_Stamp",@"N",@"is_From_Visit" ,nil];
            [[NSUserDefaults standardUserDefaults]setObject:visitDetailsDictionary forKey:@"VISIT_DETAILS_SWIFT"];

            
            [[NSUserDefaults standardUserDefaults]setValue:selectedSurvey.Survey_ID forKey:@"Selected_Survey_ID"];
            [[NSUserDefaults standardUserDefaults]setValue:selectedSurvey.Survey_Title forKey:@"Selected_Survey_Title"];

            [self.navigationController pushViewController:surveyVC animated:YES];*/
            
            
            
            
            
            
             //SURVEY 3.0
            [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Selected_Survey_ID"];
            [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Selected_Survey_Title"];
            [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"is_From_Visit"];

            
            NSString* surveyTimeStampSwift=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            NSString* salesRepIDSwift=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];
            NSString* empCodeSwift=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];
            NSString* createdBySwift=[[SWDefaults userProfile] stringForKey:@"User_ID"];
            
            NSDictionary * visitDetailsDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:salesRepIDSwift,@"SalesRep_ID",empCodeSwift,@"Emp_Code",createdBySwift,@"Created_By",surveyTimeStampSwift,@"Survey_Time_Stamp",@"N",@"is_From_Visit" ,nil];
            [[NSUserDefaults standardUserDefaults]setObject:visitDetailsDictionary forKey:@"VISIT_DETAILS_SWIFT"];

            
            if ([[AppControl retrieveSingleton].FM_SURVEY_VERSION isEqualToString:@"2"]){
                SalesWorxCoachReportViewController * reportVC=[[SalesWorxCoachReportViewController alloc]init];
                reportVC.selectedSurveyType=selectedSurvey;
               // reportVC.isFromVisit=YES;
                [self.navigationController pushViewController:reportVC animated:YES];
            }else{
                SurveySwiftDetailsViewController *surveyVC  =  [[SurveySwiftDetailsViewController alloc]init];
                [[NSUserDefaults standardUserDefaults]setValue:selectedSurvey.Survey_ID forKey:@"Selected_Survey_ID"];
                [[NSUserDefaults standardUserDefaults]setValue:selectedSurvey.Survey_Title forKey:@"Selected_Survey_Title"];
                
                [self.navigationController pushViewController:surveyVC animated:YES];
                
            }
            
            
          
        } else {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Invalid coach name or password" withController:self];
        }
    }
    }
}


-(void)closeSplitView
{
    NSLog(@"close split called");
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        if (revealController.currentFrontViewPosition !=0)
        {
            [revealController revealToggle:self];
        }
        revealController=nil;
    }
}

#pragma mark UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtUserName) {
        [textField resignFirstResponder];
        [txtPassword becomeFirstResponder];
    }
    if (textField==txtPassword) {
        [textField resignFirstResponder];
        
        return YES;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if (textField.text.length >= UsernameTextFieldLimit && range.length == 0) {
        return NO;
    } else {
        return YES;
    }
}

- (void)animateTextField:(UITextField*)textfield up:(BOOL)up
{
    NSInteger movementDistance=80;

    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = coachReportPopoverContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Textfield delegate
- (BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{

    if(textField == surveyTypeTextField)
    {
    [self.view endEditing:YES];
    /*
    [coachReportPopoverContentView setHidden:YES];
    SurveyListViewController * defualtPopOverVC =[[SurveyListViewController alloc]init];
    defualtPopOverVC.contentArray=surveysArray;
    defualtPopOverVC.surveyDelegate=self;
    [self.navigationController pushViewController:defualtPopOverVC animated:NO];
        */
        
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray=surveysArray;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        popOverVC.popoverType=kCoachSurveyPopOverTitle;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        dashboardPopoverController=nil;
        dashboardPopoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        dashboardPopoverController.delegate=self;
        popOverVC.popOverController=dashboardPopoverController;
        popOverVC.titleKey=@"Survey Type";
        [dashboardPopoverController setPopoverContentSize:CGSizeMake(220, 260) animated:YES];
        
        [dashboardPopoverController presentPopoverFromRect:textField.dropdownImageView.frame inView:textField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    return NO;
    }
    else
    {
        return YES;
    }
    
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    selectedSurvey=[surveysArray objectAtIndex:selectedIndexPath.row];
    surveyTypeTextField.text=[NSString getValidStringValue:selectedSurvey.Survey_Title];
    NSLog(@"selected survey title is %@",selectedSurvey.Survey_Title);
    /*
    if([selectedSurvey.Survey_Type_Code isEqualToString:@"M"])
    {
        pharmacyDetailsView.alpha = 0;
        pharmacyDetailsView.hidden = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            pharmacyDetailsView.alpha = 1;
            XPanelviewHeightConstraint.constant=461;
        }];

    }
    else
    {
        [UIView animateWithDuration:0.2 animations:^{
            pharmacyDetailsView.alpha = 0;

        } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
            XPanelviewHeightConstraint.constant=325;
            pharmacyDetailsView.hidden = finished;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
        }];

        
    }*/
    

}

-(void)didSelectSurvey:(CoachSurveyType*)selectedObject
{
    selectedSurvey=selectedObject;
    surveyTypeTextField.text=[NSString getValidStringValue:selectedSurvey.Survey_Title];
    NSLog(@"selected survey in didSelectSurvey %@",selectedSurvey.Survey_Type_Code);

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
