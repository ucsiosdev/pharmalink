//
//  SalesWorxCoachReportViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/19/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDatabaseManager.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
#import "SalesWorxCoachSurveyTableViewCell.h"
#import "SWDefaults.h"
#import "TGPDiscreteSlider7.h"
#import "SalesWorxCoachReportConfirmationViewController.h"
#import "SalesworxCoachSurveyHeaderTableViewCell.h"
#import "SalesWorxCoachReportMaketingSurveyTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxCoachSurveyQualityAuditTableViewCell.h"
#import "SalesWorxCoachReportQCTableViewCell.h"

#define kRemarksTextFieldIdentifier @"Remarks"
#define kRatingTextFieldIdentifier @"Rating"




@interface SalesWorxCoachReportViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPopoverControllerDelegate>
{
    UIBarButtonItem *leftBtn;
    UIBarButtonItem *saveBarButtonItem;
    
    NSArray *mainArray,* removeArray;
    NSMutableArray *menuItemsArray,* selectArray;
    NSIndexPath *selectedIndexPath;
    
    NSMutableArray *arrSurveyConfirmationData;
    UIPopoverController * dashboardPopoverController;
    
    MedRepTextField * selectedTextField;
    NSMutableArray* contentArray;
    NSInteger selectedTectFieldTag;
    NSIndexPath * selectedIdx;
    IBOutlet UIView *placeHolderView;
    NSMutableArray* responsesArray;
    NSString* selectedSurveyTitle;
    NSInteger textFieldLimit;
    
    
}
@property (strong, nonatomic) IBOutlet UITableView *surveyTableView;
@property (strong, nonatomic) CoachSurveyType* selectedSurveyType;

@property (nonatomic) BOOL isFromVisit;

@end

