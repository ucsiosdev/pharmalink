//
//  SalesWorxCoachReportQCTableViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 7/12/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxDefaultSegmentedControl.h"
#import "MedRepTextField.h"



@interface SalesWorxCoachReportQCTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxDefaultSegmentedControl *questionSegment;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *questionLabel;
@property (strong, nonatomic) IBOutlet MedRepTextField *remarkstextField;

@end
