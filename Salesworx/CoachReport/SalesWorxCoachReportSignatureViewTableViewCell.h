//
//  SalesWorxCoachReportSignatureViewTableViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/21/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSignatureView.h"
#import "MedRepPanelTitle.h"

@interface SalesWorxCoachReportSignatureViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet MedRepPanelTitle *questionLabel;
@property (weak, nonatomic) IBOutlet SalesWorxSignatureView *signatureView;

@end
