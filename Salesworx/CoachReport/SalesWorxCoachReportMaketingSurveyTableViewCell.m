//
//  SalesWorxCoachReportMaketingSurveyTableViewCell.m
//  MedRep
//
//  Created by Unique Computer Systems on 2/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxCoachReportMaketingSurveyTableViewCell.h"

@implementation SalesWorxCoachReportMaketingSurveyTableViewCell
@synthesize feedbackSegmentControl;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    feedbackSegmentControl.transitionStyle = LUNSegmentedControlTransitionStyleSlide;
    feedbackSegmentControl.shapeStyle=LUNSegmentedControlShapeStyleRoundedRect;

}


- (NSArray<UIColor *> *)segmentedControl:(LUNSegmentedControl *)segmentedControl gradientColorsForStateAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return @[[UIColor colorWithRed:160 / 255.0 green:223 / 255.0 blue:56 / 255.0 alpha:1.0], [UIColor colorWithRed:177 / 255.0 green:255 / 255.0 blue:0 / 255.0 alpha:1.0]];
            
            break;
            
        case 1:
            return @[[UIColor colorWithRed:78 / 255.0 green:252 / 255.0 blue:208 / 255.0 alpha:1.0], [UIColor colorWithRed:51 / 255.0 green:199 / 255.0 blue:244 / 255.0 alpha:1.0]];
            break;
            
        case 2:
            return @[[UIColor colorWithRed:178 / 255.0 green:0 / 255.0 blue:235 / 255.0 alpha:1.0], [UIColor colorWithRed:233 / 255.0 green:0 / 255.0 blue:147 / 255.0 alpha:1.0]];
            break;
            
        default:
            break;
    }
    return nil;
}

- (NSInteger)numberOfStatesInSegmentedControl:(LUNSegmentedControl *)segmentedControl {
    return 5;
}

- (NSAttributedString *)segmentedControl:(LUNSegmentedControl *)segmentedControl attributedTitleForStateAtIndex:(NSInteger)index {
    optionsArray=[[NSMutableArray alloc]initWithObjects:@"Unsatisfactory",@"Needs Improvement",@"Average",@"Good",@"Excellent", nil];

    NSLog(@"title delegate called %ld",(long)index);
    return [[NSAttributedString alloc] initWithString:[NSString getValidStringValue:[optionsArray objectAtIndex:index]] attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16]}];
}

- (NSAttributedString *)segmentedControl:(LUNSegmentedControl *)segmentedControl attributedTitleForSelectedStateAtIndex:(NSInteger)index {
    return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"TAB %li",(long)index] attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:16]}];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
