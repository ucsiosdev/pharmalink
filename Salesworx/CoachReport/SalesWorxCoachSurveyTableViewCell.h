//
//  SalesWorxCoachSurveyTableViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/20/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGPCamelLabels7.h"
#import "TGPDiscreteSlider7.h"
#import "MedRepElementDescriptionLabel.h"
#import "SWDefaults.h"
#import "MedRepTextField.h"

@interface SalesWorxCoachSurveyTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *titleLbl;
@property (strong, nonatomic) IBOutlet TGPDiscreteSlider7 *sliderView;
@property (strong, nonatomic) IBOutlet TGPCamelLabels7 *camelLabelsView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *titleLableLeadingConstraint;
@property (weak, nonatomic) IBOutlet MedRepTextField *remarksTextField;

@end
