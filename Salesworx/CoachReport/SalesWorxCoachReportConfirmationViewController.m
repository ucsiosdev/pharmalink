//
//  SalesWorxCoachReportConfirmationViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 2/21/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "SalesWorxCoachReportConfirmationViewController.h"
#import "SalesWorxCoachReportTextViewTableViewCell.h"
#import "SalesWorxCoachReportSignatureViewTableViewCell.h"
#import "SWDatabaseManager.h"

@interface SalesWorxCoachReportConfirmationViewController ()

@end

@implementation SalesWorxCoachReportConfirmationViewController
@synthesize surveyResponsesArray, arrSurveyConfirmationData,selectedSurveyType,isFromVisit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    
    if(isFromVisit)
    {
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Confirmation"];
    }
    else
    {
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(kFieldSalesCoachReportConfirmationScreenName, nil)];
    }
    
    saveBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(SaveButtonTapped)];
    saveBarButtonItem.tintColor=[UIColor whiteColor];
    [saveBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=saveBarButtonItem;
    
    surveyTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
}

#pragma mark TableView DataSource and Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrSurveyConfirmationData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CoachSurveyConfirmationQuestion *currentQuestion = [arrSurveyConfirmationData objectAtIndex:indexPath.section];
    
    if ([currentQuestion.Response_Display_Type isEqualToString:kTextViewResponseDisplayType]) {
        
        static NSString* identifier= @"txtViewCell";
        SalesWorxCoachReportTextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCoachReportTextViewTableViewCell" owner:nil options:nil] firstObject];
        }

        cell.questionLabel.text = currentQuestion.Question_Text;
        cell.txtViewAnswer.text = [NSString getValidStringValue:currentQuestion.Response_Text];
        cell.txtViewAnswer.tag = indexPath.section;
        cell.txtViewAnswer.delegate = self;
        
        return cell;
    } else {
        
        static NSString* identifier= @"signViewCell";
        SalesWorxCoachReportSignatureViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCoachReportSignatureViewTableViewCell" owner:nil options:nil] firstObject];
        }
        
        cell.questionLabel.text = currentQuestion.Question_Text;

        cell.signatureView.delegate = self;
        cell.signatureView.signatureArea.tag = indexPath.section;
        cell.signatureView.layer.cornerRadius = 4;
        cell.signatureView.layer.borderWidth = 1;
        cell.signatureView.layer.borderColor = UIViewControllerBackGroundColor.CGColor;
        cell.signatureView.layer.masksToBounds = YES;
        
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CoachSurveyConfirmationQuestion *currentQuestion = [arrSurveyConfirmationData objectAtIndex:indexPath.section];
    
    if ([currentQuestion.Response_Display_Type isEqualToString:kTextViewResponseDisplayType]) {
        
        CGSize size = [currentQuestion.Question_Text boundingRectWithSize:CGSizeMake((self.view.bounds.size.width-116), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kSWX_FONT_REGULAR(18)} context:nil].size;
        
        size.height += 74.0f;
        
        return size.height; 
    } else {
        
        CGSize size = [currentQuestion.Question_Text boundingRectWithSize:CGSizeMake((self.view.bounds.size.width-116), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kSWX_FONT_REGULAR(18)} context:nil].size;
        
        size.height += 157.0f;
        
        return size.height;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    return 8;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

#pragma mark TextView Delegate methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CoachSurveyConfirmationQuestion *currentQuestion = [arrSurveyConfirmationData objectAtIndex:textView.tag];
    currentQuestion.Response_Text = trimmedString;
    
    textView.text = trimmedString;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    if([newString isEqualToString:@" "] || [newString isEqualToString:@"\n"])
    {
        return NO;
    }
    
    return (newString.length <= CoachConfirmationTextFieldLimit);
    
    return YES;
}

-(void)SalesWorxSignatureViewClearButtonTapped:(UIView *)signView
{
    SalesWorxCoachReportSignatureViewTableViewCell *cell = [surveyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:signView.tag]];
    
    [cell.signatureView setStatus:KEmptySignature];
}

-(void)SalesWorxSignatureViewSaveButtonTapped:(UIView *)signView
{
    SalesWorxCoachReportSignatureViewTableViewCell *cell = [surveyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:signView.tag]];
    
    if(cell.signatureView.status==KEmptySignature)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please sign to save" withController:self];
    }
    else
    {
        [self.view endEditing:YES];
        [cell.signatureView performSignatureSaveAnimation];
        
        CoachSurveyConfirmationQuestion *currentQuestion = [arrSurveyConfirmationData objectAtIndex:signView.tag];
        currentQuestion.isSignatureSaved = YES;
        currentQuestion.signature_Section = signView.tag;
        currentQuestion.image_Name = [NSString createGuid];
    }
}

#pragma mark SaveAction
-(void)SaveButtonTapped
{
    // check signatures are provided or not
    
    BOOL AllSignatureSaved = NO;
    for (int i = 0; i<arrSurveyConfirmationData.count; i++) {
      
        CoachSurveyConfirmationQuestion *currentQuestion = [arrSurveyConfirmationData objectAtIndex:i];
        if ([currentQuestion.Response_Display_Type isEqualToString:kSignatureResponseDisplayType]) {
            
            // signature view questions
            SalesWorxCoachReportSignatureViewTableViewCell *cell = [surveyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
            
            if(cell.signatureView.status==KEmptySignature)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please provide signature for %@",currentQuestion.Question_Text] withController:self];
                AllSignatureSaved = NO;
                break;
            }
            else if(cell.signatureView.status==KSignatureNotSaved)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please save the signature for %@",currentQuestion.Question_Text] withController:self];
                AllSignatureSaved = NO;
                break;
            }
            else {
                AllSignatureSaved = YES;
            }
        } else {
            // text view questions
            
        }
    }
    
    if (AllSignatureSaved) {
        
        // save survey in db
        
        for (CoachSurveyConfirmationQuestion *currentQuestion in arrSurveyConfirmationData) {
            
            if ([currentQuestion.Response_Display_Type isEqualToString:kSignatureResponseDisplayType]) {
                
                SalesWorxCoachReportSignatureViewTableViewCell *cell = [surveyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:currentQuestion.signature_Section]];
       
                [SWDefaults saveCoachSurveySignatureImages:[cell.signatureView.signatureArea getSignatureImage] withName:currentQuestion.image_Name];
            }
        }
        
         if (isFromVisit) {
         
         NSMutableDictionary* coachSurveyDict = [[NSMutableDictionary alloc]init];
         [coachSurveyDict setObject:surveyResponsesArray forKey:@"Survey_Responses_Array"];
         [coachSurveyDict setObject:arrSurveyConfirmationData forKey:@"Survey_Confirmation_Array"];
         
         [[NSNotificationCenter defaultCenter]postNotificationName:@"COACH_SURVEY" object:coachSurveyDict];
             
             UIAlertAction * okAction = [UIAlertAction actionWithTitle:KAlertOkButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                 NSArray *array = [self.navigationController viewControllers];
                 [self.navigationController popToViewController:[array objectAtIndex:2] animated:YES];

             }];
             
             [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Survey saved successfully" andActions:[[NSMutableArray alloc]initWithObjects:okAction, nil] withController:self];
         }
        else
        {
        //inserting data to db
        BOOL success =[[SWDatabaseManager retrieveManager]insertCoachDataSurvey:surveyResponsesArray withConfirmation:arrSurveyConfirmationData andSurveyType:selectedSurveyType];
        if (success) {
            NSLog(@"coach data inserted successfully");
            
            
            UIAlertAction * okAction = [UIAlertAction actionWithTitle:KAlertOkButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               // [self resetNavigation];
                //[self logoutTapped];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Survey saved successfully" andActions:[[NSMutableArray alloc]initWithObjects:okAction, nil] withController:self];
            
            
            
        //    [SWDefaults showAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Survey saved successfully" withController:self];
            
           // [[NSNotificationCenter defaultCenter]postNotificationName:@"UPDATEDASHBOARDUINOTIFICATIONNAME" object:nil];

        }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setSliderRootViewController:(UIViewController*)viewController
{
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    
    
    UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:viewController] ;
    if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[viewController class]])
    {
        [revealController setFrontViewController:navigationControllerFront animated:YES];
    }
    else { // Seems the user attempts to 'switch' to exactly the same controller he came from!
        [revealController revealToggle:self];
        
    }
    
    
    
}

-(void)closeSplitView
{
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        if (revealController.currentFrontViewPosition !=0)
        {
            [revealController revealToggle:self];
        }
        revealController=nil;
    }
}

-(void)logoutTapped

{
    
    //[self closeSplitView];
    UINavigationController *navigationControllerFront;

    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;




   SalesWorxFieldSalesDashboardViewController*  dashBViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init] ;
    navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:dashBViewController] ;

    //navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:loginVC] ;


    if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[LoginViewController class]])
    {
        [revealController setFrontViewController:navigationControllerFront animated:YES];

    } else {
        [revealController revealToggle:self];
    }
   // [[NSNotificationCenter defaultCenter]postNotificationName:@"COACHSURVEYDIDCOMPLETENOTIFICATION" object:self];
    
 }

-(void)resetNavigation
{
    AppControl *appControl = [AppControl retrieveSingleton];
    
    SWAppDelegate *appdel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    UINavigationController *menuNavigationController;
    SalesWorxFieldSalesDashboardViewController*     dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
    appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]] trimString];
        
         if([PDARIGHTS containsString:KFieldSalesSectionPDA_RightsCode])
        {
            SalesWorxFieldSalesDashboardViewController*     dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
            appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
            
        }
        else if([PDARIGHTS containsString:KFieldMarketingSectionPDA_RightsCode] ||
                [PDARIGHTS containsString:KFieldMarketingVisitsPDA_RightsCode])
        {
            SalesWorxDashboardViewController *dashboardViewController = [[SalesWorxDashboardViewController alloc] init];
            appdel.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
        }
        
        
        
    }
    
    
    MedRepMenuViewController *medrepMenuViewController;
    medrepMenuViewController = [[MedRepMenuViewController alloc] init] ;
    menuNavigationController = [[UINavigationController alloc] initWithRootViewController:medrepMenuViewController] ;
    
    SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:appdel.mainNavigationController rearViewController:menuNavigationController] ;
    SWSessionManager * sessionManager=[[SWSessionManager alloc] initWithWindow:appdel.window andApplicationViewController:splitViewController];
    [[UINavigationBar appearance]setBarTintColor:kNavigationBarBackgroundColor];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance]setTintColor:kNavigationBarTitleFontColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: kNavigationBarTitleFontColor}];
    
    medrepMenuViewController=nil;
    splitViewController=nil;
    dashboardViewController=nil;
    
    
}
    
    
    
    
    
    



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
