//
//  SalesWorxCoachSurveyQTableViewCell.m
//  MedRep
//
//  Created by Prasann on 7/20/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCoachSurveyQTableViewCell.h"
#import "SWDefaults.h"

@implementation SalesWorxCoachSurveyQTableViewCell
@synthesize camelLabelsView,sliderView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    camelLabelsView.tickCount=3;
    camelLabelsView.ticksDistance=75;
    camelLabelsView.value=0;
    camelLabelsView.upFontName=@"WeblySleekUISemibold";
    camelLabelsView.upFontSize=20;
    camelLabelsView.upFontColor=kNavigationBarBackgroundColor;
    camelLabelsView.downFontName=@"WeblySleekUISemibold";
    camelLabelsView.downFontSize=20;
    camelLabelsView.downFontColor=[UIColor lightGrayColor];
    camelLabelsView.backgroundColor=[UIColor clearColor];
    
    
    sliderView.tickStyle=1;
    sliderView.tickSize = CGSizeMake(1, 8);
    sliderView.tickCount=3;
    sliderView.trackThickness=2;
    sliderView.incrementValue=1;
    sliderView.minimumValue=0;
    sliderView.value=0;
    sliderView.thumbTintColor=kNavigationBarBackgroundColor;
    //[descreteSliderView addTarget:self action:@selector(customSliderValueChanged) forControlEvents:UIControlEventValueChanged];
    
    sliderView.tintColor=kNavigationBarBackgroundColor;
    
    
    
    sliderView.ticksListener=camelLabelsView;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
