//
//  SalesWorxServersListViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SurveyListViewController.h"
#import "MedRepDefaults.h"
@interface SurveyListViewController ()

@end

@implementation SurveyListViewController
@synthesize contentArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    serverListContentView.layer.cornerRadius=5.0;
    
    
    
    
    //[serverListContentView setHidden:YES];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setHiddenAnimated:NO duration:1.25];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(IBAction)BackButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return contentArray.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    cell.textLabel.font=MedRepSingleLineLabelFont;
    
    cell.textLabel.textColor=MedRepMenuTitleFontColor;
    
    
    cell.textLabel.text=[[contentArray valueForKey:@"Survey_Title"]objectAtIndex:indexPath.row];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.surveyDelegate respondsToSelector:@selector(didSelectSurvey:)]) {
        
        [self.surveyDelegate didSelectSurvey:[contentArray objectAtIndex:indexPath.row]];
    }
    
    [self.navigationController popViewControllerAnimated:NO];
    
}



- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = serverListContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
@end

