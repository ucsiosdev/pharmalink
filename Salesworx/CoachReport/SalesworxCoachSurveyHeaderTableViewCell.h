//
//  SalesworxCoachSurveyHeaderTableViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/26/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
@interface SalesworxCoachSurveyHeaderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *dividerView;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dividerViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageViewLeadingConstraint;

@end
