//
//  SurveyListViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"


@protocol SalesWorxSurveyListViewControllerDelegate <NSObject>

-(void)didSelectSurvey:(CoachSurveyType*)selectedObject;
@end

@interface SurveyListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UIView *serverListContentView;
    IBOutlet UIView *headerView;
}
@property (strong,nonatomic) id <SalesWorxSurveyListViewControllerDelegate>surveyDelegate;
@property(strong,nonatomic) NSMutableArray* contentArray;


@end
