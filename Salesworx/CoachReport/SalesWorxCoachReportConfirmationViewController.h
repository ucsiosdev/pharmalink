//
//  SalesWorxCoachReportConfirmationViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/21/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableView.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SalesWorxSignatureView.h"
#import "MedRepQueries.h"
#import "SalesWorxFieldSalesDashboardViewController.h"
#import "LoginViewController.h"
#import "SalesWorxDashboardViewController.h"
#import "AppControl.h"
#import "SWAppDelegate.h"
#import "MedRepMenuViewController.h"
#import "SWSplitViewController.h"

@interface SalesWorxCoachReportConfirmationViewController : UIViewController<SalesWorxSignatureViewDelegate, UITextViewDelegate>
{
    UIBarButtonItem *saveBarButtonItem;
    IBOutlet SalesWorxTableView *surveyTableView;
    

}
@property(strong,nonatomic) CoachSurveyType* selectedSurveyType;

@property(strong,nonatomic) NSMutableArray* surveyResponsesArray;
@property(strong,nonatomic) NSMutableArray* arrSurveyConfirmationData;
@property (strong, nonatomic) VisitDetails* currentVisitDetails;
@property (nonatomic) BOOL isFromVisit;


@end
