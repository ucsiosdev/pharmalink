//
//  SalesWorxCoachSurveyQTableViewCell.h
//  MedRep
//
//  Created by Prasann on 7/20/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGPCamelLabels7.h"
#import "TGPDiscreteSlider7.h"

@interface SalesWorxCoachSurveyQTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet TGPCamelLabels7 *camelLabelsView;
@property (weak, nonatomic) IBOutlet TGPDiscreteSlider7 *sliderView;

@end
