//
//  SalesWorxCoachReportPharmacyDetailsViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 7/12/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDropShadowView.h"
#import "SalesWorxDefaultButton.h"
#import "SWDefaults.h"
#import "SalesWorxTableView.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"
@protocol PharmacyDetailsDelegate <NSObject>

-(void)didCancel;
-(void)didSelectSurvey:(CoachSurveyType*)selectedSurvey;

@end


@interface SalesWorxCoachReportPharmacyDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id pharmacyDetailsDelegate;
    IBOutlet SalesWorxTableView *surveyTableView;
}
@property(nonatomic) id pharmacyDetailsDelegate;
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *contentView;
- (IBAction)saveButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet SalesWorxDefaultButton *saveButton;
@property (strong, nonatomic) IBOutlet SalesWorxDefaultButton *cancelButton;
- (IBAction)cancelButtonTapped:(id)sender;
@property(strong,nonatomic) NSMutableArray * surveysArray;
@property (strong, nonatomic) NSString* locationID;
@property (strong, nonatomic) VisitDetails* visitDetails;

@end
