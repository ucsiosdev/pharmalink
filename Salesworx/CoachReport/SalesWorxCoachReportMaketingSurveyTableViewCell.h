//
//  SalesWorxCoachReportMaketingSurveyTableViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "LUNSegmentedControl.h"
#import "MedRepTextField.h"
#import "NSString+Additions.h"

@interface SalesWorxCoachReportMaketingSurveyTableViewCell : UITableViewCell
{
    NSMutableArray* optionsArray;
}
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *titleLbl;
@property (strong, nonatomic) IBOutlet LUNSegmentedControl *feedbackSegmentControl;
@property (strong, nonatomic) IBOutlet UIButton *commentsButton;
@property (strong, nonatomic) IBOutlet MedRepTextField *optionsTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *remarksTextField;

@end
