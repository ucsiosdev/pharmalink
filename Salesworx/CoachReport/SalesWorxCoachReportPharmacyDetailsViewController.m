//
//  SalesWorxCoachReportPharmacyDetailsViewController.m
//  MedRep
//
//  Created by Unique Computer Systems on 7/12/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCoachReportPharmacyDetailsViewController.h"

@interface SalesWorxCoachReportPharmacyDetailsViewController ()

@end

@implementation SalesWorxCoachReportPharmacyDetailsViewController
@synthesize contentView,saveButton,cancelButton,surveysArray,locationID,visitDetails;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    if(self.isMovingToParentViewController)
    {
        contentView.backgroundColor=UIViewControllerBackGroundColor;
        [saveButton.titleLabel setFont:NavigationBarButtonItemFont];
        [cancelButton.titleLabel setFont:NavigationBarButtonItemFont];
        
        for (UIView *borderView in contentView.subviews) {
            if ([borderView isKindOfClass:[UIView class]]) {
                if (borderView.tag==101) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                }
            }
        }
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
        
        
        
    }
}

#pragma mark AnimationMethods
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark TableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return surveysArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(14);
    cell.textLabel.textColor=MedRepElementDescriptionLabelColor;
    cell.textLabel.numberOfLines=2;
    CoachSurveyType * currentSurvey = [surveysArray objectAtIndex:indexPath.row];
    cell.textLabel.text=[NSString getValidStringValue:currentSurvey.Survey_Title];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CoachSurveyType * selectedSurvey = [surveysArray objectAtIndex:indexPath.row];
    NSLog(@"selected survey in popover did select %@",selectedSurvey.Survey_Type_Code);
    BOOL isSurveyCompleted=[MedRepQueries isSurveyCompletedForLocation:locationID andSurveyID:selectedSurvey.Survey_ID];
    
    NSLog(@"completed surveys are %@", visitDetails.coachSurveyArray);
    NSPredicate* completedPred=[NSPredicate predicateWithFormat:@"SELF.Survey_ID == %@",selectedSurvey.Survey_ID];
    NSArray* completedSurveyArray=[visitDetails.coachSurveyArray filteredArrayUsingPredicate:completedPred];
    
    //add if survey completed but not inserted in db.
#if DEBUG

    if ([self.pharmacyDetailsDelegate respondsToSelector:@selector(didSelectSurvey:)]) {
        [self.pharmacyDetailsDelegate didSelectSurvey:selectedSurvey];
    }
    [self dismissViewControllerAnimated:YES completion:nil];

#else

    if (isSurveyCompleted || completedSurveyArray.count>0) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Selected survey completed for today, please select another survey" withController:self];
    }
    else
    {
        if ([self.pharmacyDetailsDelegate respondsToSelector:@selector(didSelectSurvey:)]) {
            [self.pharmacyDetailsDelegate didSelectSurvey:selectedSurvey];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }

#endif
    
    
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveButtonTapped:(id)sender {
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];
}


- (void)CloseSyncPopOver
{
    //do what you need to do when animation ends...
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
