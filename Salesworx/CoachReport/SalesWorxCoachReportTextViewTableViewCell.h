//
//  SalesWorxCoachReportTextViewTableViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/21/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepPanelTitle.h"
#import "MedRepTextView.h"

@interface SalesWorxCoachReportTextViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet MedRepPanelTitle *questionLabel;
@property (weak, nonatomic) IBOutlet MedRepTextView *txtViewAnswer;

@end
