//
//  SalesWorxFieldMarketingDailyVisitSummaryViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 11/21/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldMarketingDailyVisitSummaryViewController.h"
#import "SalesWorxFieldMarketingDailyVisitSummaryTableViewCell.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"

@interface SalesWorxFieldMarketingDailyVisitSummaryViewController ()

@end

@implementation SalesWorxFieldMarketingDailyVisitSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrDailyVisit = [[NSMutableArray alloc]init];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                   {
                       NSMutableArray *arrOfTodayVisits = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT distinct Location_ID, Doctor_ID, Contact_ID FROM  PHX_TBL_Actual_Visits where date(Visit_End_Date) = date('now')"];
                       
                       for (NSDictionary *dicLocation in arrOfTodayVisits) {
                           
                           NSMutableDictionary *gpsDict = [NSMutableDictionary dictionary];
                           [gpsDict setObject:[dicLocation valueForKey:@"Location_ID"] forKey:@"Location_ID"];
                           
                           
                           NSMutableArray *arrOfDemonstratedProducts, *arrOfSamplesGiven;
                           
                           if ([NSString isEmpty:[dicLocation valueForKey:@"Contact_ID"]]) {
                               arrOfDemonstratedProducts = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select count(*) As Total_DemonstratedProduct from PHX_TBL_EDetailing As A inner join PHX_TBL_Products As B where B.Product_ID = A.Product_ID And date(Discussed_At) = date('now') and A.Doctor_ID = '%@'", [dicLocation valueForKey:@"Doctor_ID"]]];
                               
                               arrOfSamplesGiven = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT SUM(Issue_Qty) As Total_SamplesGiven FROM PHX_TBL_Issue_Note As A inner join PHX_TBL_Issue_Note_Line_Items As B inner join PHX_TBL_Products As C where A.Doc_No = B.Doc_No and B.Product_ID = C.Product_ID and date(A.End_Time) = date('now') And A.Doctor_ID = '%@'",[dicLocation valueForKey:@"Doctor_ID"]]];
                               
                           } else {
                               arrOfDemonstratedProducts = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select count(*) As Total_DemonstratedProduct from PHX_TBL_EDetailing As A inner join PHX_TBL_Products As B where B.Product_ID = A.Product_ID And date(Discussed_At) = date('now') and A.Contact_ID = '%@'", [dicLocation valueForKey:@"Contact_ID"]]];
                               
                               arrOfSamplesGiven = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT SUM(Issue_Qty) As Total_SamplesGiven FROM PHX_TBL_Issue_Note As A inner join PHX_TBL_Issue_Note_Line_Items As B inner join PHX_TBL_Products As C where A.Doc_No = B.Doc_No and B.Product_ID = C.Product_ID and date(A.End_Time) = date('now') And A.Contact_ID = '%@'",[dicLocation valueForKey:@"Contact_ID"]]];
                           }
                           
                           
                           for (NSDictionary *dicDemonstratedProducts in arrOfDemonstratedProducts)
                           {
                               [gpsDict setObject:[dicDemonstratedProducts valueForKey:@"Total_DemonstratedProduct"] forKey:@"Total_DemonstratedProduct"];
                           }
               
                           for (NSDictionary *dicSamplesGiven in arrOfSamplesGiven)
                           {
                               [gpsDict setObject:[dicSamplesGiven valueForKey:@"Total_SamplesGiven"] forKey:@"Total_SamplesGiven"];
                           }
                           
                           
                           NSMutableArray *arrOfLocation_Name = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT Location_Name FROM  PHX_TBL_Locations where Location_ID = '%@'",[dicLocation valueForKey:@"Location_ID"]]];
                           for (NSDictionary *dicOfLocation in arrOfLocation_Name)
                           {
                               [gpsDict setObject:[dicOfLocation valueForKey:@"Location_Name"] forKey:@"Location_Name"];
                           }
                           
                           NSMutableArray *arrOfCustomer = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Contact_Name FROM  PHX_TBL_Contacts As A inner join PHX_TBL_Actual_Visits As B where A.Contact_ID = B.Contact_ID and A.Contact_ID = '%@' group by A.Contact_Name",[dicLocation valueForKey:@"Contact_ID"]]];
                           for (NSDictionary *dicOfCustomer in arrOfCustomer)
                           {
                               [gpsDict setObject:[dicOfCustomer valueForKey:@"Contact_Name"] forKey:@"Contact_Name"];
                           }
                           
                           NSMutableArray *arrOfDoctor=[[NSMutableArray alloc]init];
                           
                           if ([SWDefaults isDrApprovalAvailable])
                           {
                           arrOfDoctor = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Doctor_Name FROM  PHX_TBL_Doctors As A inner join PHX_TBL_Actual_Visits As B where A.Doctor_ID = B.Doctor_ID and A.Doctor_ID = '%@' and A.Is_Approved = 'Y' group by A.Doctor_Name",[dicLocation valueForKey:@"Doctor_ID"]]];
                           }
                           else
                           {
                               arrOfDoctor = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Doctor_Name FROM  PHX_TBL_Doctors As A inner join PHX_TBL_Actual_Visits As B where A.Doctor_ID = B.Doctor_ID and A.Doctor_ID = '%@' group by A.Doctor_Name",[dicLocation valueForKey:@"Doctor_ID"]]];
                           }
                           for (NSDictionary *dicOfDoctor in arrOfDoctor)
                           {
                               [gpsDict setObject:[dicOfDoctor valueForKey:@"Doctor_Name"] forKey:@"Doctor_Name"];
                           }
                           
                           [arrDailyVisit addObject:gpsDict];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(), ^(void){
                           
                           [tblDailyVisitSummary reloadData];
                       });
                   });
}

#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // custom view for header. will be adjusted to default or specified header height
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1008, 44.0)];
    dailyVisitSummaryTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:dailyVisitSummaryTableHeaderView];
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDailyVisit.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SalesWorxFieldMarketingDailyVisitSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DailyVisitSummaryTableCell"];
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFieldMarketingDailyVisitSummaryTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.lblLocationName.textColor = MedRepMenuTitleFontColor;
    cell.lblCustomerName.textColor = MedRepMenuTitleFontColor;
    cell.lblProductDemonstrated.textColor = MedRepMenuTitleFontColor;
    cell.lblSamplesGiven.textColor = MedRepMenuTitleFontColor;
    cell.contentView.backgroundColor = UITableviewUnSelectedCellBackgroundColor;
    
    
    cell.lblLocationName.text = [NSString getValidStringValue:[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Location_Name"]];
    
    NSString *doctor_Name = [NSString getValidStringValue:[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Doctor_Name"]];
    NSString *contact_Name = [NSString getValidStringValue:[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Contact_Name"]];
                             
    if ([NSString isEmpty:doctor_Name]) {
        cell.lblCustomerName.text = contact_Name;
    } else {
        cell.lblCustomerName.text = doctor_Name;
    }
    cell.lblProductDemonstrated.text = [NSString getValidStringValue:[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Total_DemonstratedProduct"]];
    cell.lblSamplesGiven.text = [NSString getValidStringValue:[[arrDailyVisit objectAtIndex:indexPath.row]objectForKey:@"Total_SamplesGiven"]];
 
    [cell setNeedsLayout];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
