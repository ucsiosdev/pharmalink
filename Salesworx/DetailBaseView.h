//
//  DetailBaseView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailBaseView : UIView <UITableViewDelegate, UITableViewDataSource> {
    UITableView *tableView;
    NSDictionary *parent;
    UIView *headerView;
    NSArray *dataSource;
    BOOL _IsSectionedView;
    UIImageView *productImage;

}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSDictionary *parent;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) UIImageView *productImage;

- (id)initWithParent:(NSDictionary *)row;

@end
