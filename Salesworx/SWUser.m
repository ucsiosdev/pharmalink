//
//  SWUser.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWUser.h"

@implementation SWUser
@synthesize userId;
@synthesize username;
@synthesize password;
@synthesize salesRepId;
@synthesize salesRepWsId;
@synthesize organizationId;
@synthesize pdaRights;
@synthesize isSS;
@synthesize site;
@synthesize controlParams;
@synthesize masOrganizationId;
@synthesize employeeCode;
@synthesize employeeName;
@synthesize employeePhone;
@synthesize currencyCode;
@synthesize decimalDigits;
@synthesize isDCOptional;



@end