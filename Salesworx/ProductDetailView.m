//
//  ProductDetailView.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductDetailView.h"
#import "SWFoundation.h"

@implementation ProductDetailView

//@synthesize serProduct;
- (id)initWithProduct:(NSDictionary *)product {
    self = [super initWithParent:product];
    
    if (self) {
        [self loadDetail];
    }
    
    return self;
}

-(void)loadDetail
{
    //self.tableView.frame = CGRectMake(0, 0, 500, self.frame.size.height);
    //serProduct.delegate=self;

    [self getProductServiceDidGetDetail:[[SWDatabaseManager retrieveManager] dbGetProductDetail:[self.parent stringForKey:@"ItemID"] organizationId:[self.parent stringForKey:@"OrgID"]]];
    priceDict=[[SWDatabaseManager retrieveManager] dbdbGetPriceListProduct:[self.parent stringForKey:@"ItemID"]];

}


- (UIView *)getHeaderView {
    ProductHeaderView *productHeaderView = [[ProductHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60)];
    [productHeaderView.headingLabel setText:[self.parent stringForKey:@"Description"]];
    [productHeaderView.detailLabel setText:[self.parent stringForKey:@"Item_Code"]];
    return productHeaderView;
}
/*
 NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:8081/Mario/floormap?id=%@",baseURL,[imageDict stringForKey:@"@imageName"]]]];
 
 AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",[tempDict stringForKey:@"@floorRefId"],secondString]];
 operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
 [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
 {
 downloadedImage =  [[UIImage alloc] initWithContentsOfFile:path];
 myLocationDict = [[NSMutableDictionary alloc] init];
 [myLocationDict setObject:downloadedImage  forKey:@"mapImage"];
 [myLocationDict setObject:[[mapInfo objectForKey:@"MapCoordinate"] stringForKey:@"@x"]  forKey:@"xLocation"];
 [myLocationDict setObject:[[mapInfo objectForKey:@"MapCoordinate"] stringForKey:@"@y"]  forKey:@"yLocation"];
 [myLocationDict setObject:[tempDict stringForKey:@"@mapHierarchyString"]  forKey:@"floorName"];
 
 if([_delegate respondsToSelector:@selector(didReceiveResponse:)]) {
 [_delegate didReceiveResponse:myLocationDict];
 }
 }
 failure:^(AFHTTPRequestOperation *operation, NSError *error)
 {
 if([_delegate respondsToSelector:@selector(didFailWithError:)]) {
 [_delegate didFailWithError:error];
 }
 }];
 [operation start];
*/
#pragma mark ProductService Delegate
- (void)getProductServiceDidGetDetail:(NSArray *)details
{
    [self setDataSource:[NSArray arrayWithArray:details]];
    [self.tableView reloadData];
    details=nil;
}

#pragma mark UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    UITableViewCell *cell = [super tableView:tv cellForRowAtIndexPath:indexPath];
    
    NSString *title = @"";
    NSString *detail = @"";
    
    NSDictionary *row = [self.dataSource objectAtIndex:0];
    
    if (indexPath.row == 0) {
        title = NSLocalizedString(@"Brand", nil);
        detail = [row stringForKey:@"Brand_Code"];
    } else if (indexPath.row == 1) {
        title = NSLocalizedString(@"Item No.", nil);
        detail = [row stringForKey:@"Item_Code"];
    } else if (indexPath.row == 2) {
        title = NSLocalizedString(@"Size", nil);
        detail = [row stringForKey:@"Item_Size"];
    } else if (indexPath.row == 3) {
        title = NSLocalizedString(@"Description", nil);
        detail = [row stringForKey:@"Description"];
    } else if (indexPath.row == 4) {
        title = NSLocalizedString(@"UOM(S)", nil);
        detail = [row stringForKey:@"UOM"];
    } else if (indexPath.row == 5) {
        title = NSLocalizedString(@"Is MSL", nil);
        detail = [self.parent stringForKey:@"IsMSL"];
    } else if (indexPath.row == 6) {
        title = NSLocalizedString(@"Stock", nil);
        if (![[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
        {
            detail = [row stringForKey:@"Lot_Qty"];
        }
        else 
        {
            detail = @"0";
        }
    } else if (indexPath.row == 7) {
        
        title = NSLocalizedString(@"Wholesale Price", nil);
        if(priceDict.count!=0)
        {
            detail = [priceDict currencyStringForKey:@"sellingPrice"];
        }
        else
        {
             detail = @"No price availble";
        }
    } else if (indexPath.row == 8) {
        title = NSLocalizedString(@"Retail Price", nil);
        if(priceDict.count!=0)
        {
            detail = [priceDict currencyStringForKey:@"listPrice"];
        }
        else
        {
            detail = @"No price availble";
        }
    }
    
    [cell.textLabel setText:title];
    [cell.detailTextLabel setText:detail];
    
    return cell;
}
}

@end