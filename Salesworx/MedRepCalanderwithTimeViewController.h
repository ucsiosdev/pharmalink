//
//  MedRepCalanderwithTimeViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/27/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import <JTCalendar/JTCalendar.h>
#import "MedRepDefaults.h"
#import "MedRepQueries.h"

@protocol MedRepCalenderDelegate <NSObject>
-(void)selectedDateDelegate:(NSString*)selectedDate;
@end

@interface MedRepCalanderwithTimeViewController : UIViewController<JTCalendarDelegate>
{
    id calenderDelegate;
    
    NSMutableDictionary *eventsByDate;
    NSDate *todayDate;
    NSDate *minDate;
    NSDate *maxDate;
    NSMutableArray* datesArray;
}
@property(nonatomic) id calenderDelegate;
@property (strong, nonatomic) IBOutlet UIDatePicker *visitTimePicker;
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarContentViewHeight;
@property (strong, nonatomic) NSMutableArray *disabledWeekDays;
@property (strong, nonatomic) NSDate *dateSelected;

@property(strong,nonatomic) NSDate *minimumDate;


@end
