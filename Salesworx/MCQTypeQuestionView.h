//
//  MCQTypeQuestionView.h
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"


@interface MCQTypeQuestionView : UIView <UITableViewDelegate, UITableViewDataSource>
{
    UITableView *tableViewController;
    NSIndexPath* checkedIndexPath;
    
    UIView *myBackgroundView;
}
@property (nonatomic, strong) UITableView *tableViewController;
@property (nonatomic, strong) NSIndexPath* checkedIndexPath;

@end
