//
//  GroupSectionFooterView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupSectionFooterView : UIView {
    UILabel *titleLabel;
}

@property (nonatomic, strong) UILabel *titleLabel;

- (id)initWithWidth:(CGFloat)width text:(NSString *)text;

@end
