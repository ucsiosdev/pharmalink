//
//  SWCustomerPriceListFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWCustomerPriceListFilterViewController.h"
#import "MedRepDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "NSPredicate+Distinct.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@interface SWCustomerPriceListFilterViewController ()

@end

@implementation SWCustomerPriceListFilterViewController
@synthesize priceListArray,selectedCustomer,previousFilterParametersDict,filterPopOverController;
- (void)viewDidLoad {
    [super viewDidLoad];
    filterParametersDict=[[NSMutableDictionary alloc]init];
    
    NSLog(@"prev filter paramteres dict %@", self.previousFilterParametersDict);
    
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];

    
    NSLog(@"previous filter parameters are %@", previousFilterParametersDict);
    
    if (previousFilterParametersDict.count>0) {
        filterParametersDict=previousFilterParametersDict;
    }
    if([[previousFilterParametersDict valueForKey:@"Item_Code"] length]>0) {
        itemCodeTextFld.text=[previousFilterParametersDict valueForKey:@"Item_Code"];
    }
    
    if([[filterParametersDict valueForKey:@"Description"] length]>0) {
        itemNameTxtFld.text=[previousFilterParametersDict valueForKey:@"Description"];
    }
    
    if([[filterParametersDict valueForKey:@"Brand"] length]>0) {
        brandTxtFld.text=[previousFilterParametersDict valueForKey:@"Brand"];
    }

    if([[filterParametersDict valueForKey:@"Item_UOM"] length]>0) {
        uomTextField.text=[previousFilterParametersDict valueForKey:@"Item_UOM"];
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    
    NSLog(@"brands array is %@", selectedCustomer.brandsArray);
    
}

#pragma mark CLose Filter

-(void)closeFilter
{
    if ([self.delegate respondsToSelector:@selector(customerPriceListFilterDidClose)]) {
        [self.delegate customerPriceListFilterDidClose];
        [self.delegate filteredPriceListParameters:filterParametersDict];

    }
    [filterPopOverController dismissPopoverAnimated:YES];
}

-(void)clearFilter
{
    itemNameTxtFld.text=@"";
    itemCodeTextFld.text=@"";
    brandTxtFld.text=@"";
    uomTextField.text=@"";
    filterParametersDict=[[NSMutableDictionary alloc]init];
}

#pragma mark UITextField Delegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==itemNameTxtFld)
    {
        selectedTextField=@"Item Name";
        selectedPredicateString=@"Description";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    else if (textField==itemCodeTextFld)
    {
        selectedTextField=@"Item Code";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"Item_Code"];
        selectedPredicateString=@"Item_Code";
        return NO;
    }
    else if (textField==brandTxtFld)
    {
        selectedTextField=@"Brand";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"Brand_Code"];
        selectedPredicateString=@"Brand";
        return NO;
    }
    else if (textField==uomTextField)
    {
        selectedTextField=@"UOM";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"Item_UOM"];
        selectedPredicateString=@"Item_UOM";
        return NO;
    }
    return YES;
}

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedTextField;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];
    
    //get distinct keys
        NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray=[[selectedCustomer.customerPriceListArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
       
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:predicateString];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }
}

#pragma mark Selected Filter Data

-(void)selectedFilterValue:(NSString*)selectedString
{
    
    if ([selectedTextField isEqualToString:@"Item Name"]) {
        itemNameTxtFld.text= selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"Item Code"])
    {
        itemCodeTextFld.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    
    else if ([selectedTextField isEqualToString:@"Brand"])
    {
        brandTxtFld.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"UOM"])
    {
        uomTextField.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
}


#pragma mark Button Actions


- (IBAction)searchButtontapped:(id)sender {
    
    
    filteredPriceListArray=[[NSMutableArray alloc]init];
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filter parameters dict is %@",filterParametersDict);
    
    if([[filterParametersDict valueForKey:@"Description"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[filterParametersDict valueForKey:@"Description"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[filterParametersDict valueForKey:@"Item_Code"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Brand"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[filterParametersDict valueForKey:@"Brand"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Item_UOM"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_UOM ==[cd] %@",[filterParametersDict valueForKey:@"Item_UOM"]]];
    }
    
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredPriceListArray=[[selectedCustomer.customerPriceListArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    if (predicateArray.count==0) {
        
        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please select your filter criteria and try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [noFilterAlert show];
    }
    
    else  if (filteredPriceListArray.count>0) {
        if ([self.delegate respondsToSelector:@selector(filteredPriceList:)]) {
            [self.delegate filteredPriceList:filteredPriceListArray];
            [self.delegate filteredPriceListParameters:filterParametersDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
        
    }
    
    
    
}
- (IBAction)resetButtontapped:(id)sender {
    
    previousFilterParametersDict=[[NSMutableDictionary alloc]init];
    filterParametersDict=[[NSMutableDictionary alloc]init];
    if ([self.delegate respondsToSelector:@selector(customerPriceListFilterDidReset)]) {
        [self.delegate customerPriceListFilterDidReset];
        [self.filterPopOverController dismissPopoverAnimated:YES];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
