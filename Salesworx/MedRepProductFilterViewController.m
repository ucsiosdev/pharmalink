//
//  MedRepProductFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/10/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepProductFilterViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepQueries.h"
@interface MedRepProductFilterViewController ()

@end

@implementation MedRepProductFilterViewController
@synthesize filterPopOverController,contentArray,previouslyFilteredContent,uomButtonHeightConstraint,uomLblHeightConstraint,uomTextFieldHeightConstraint,uomTextFieldBottomConstraint,uomBottomConstraint,uomViewBottomConstraint,uomViewHeightConstraint
;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
    
    filterDict=[[NSMutableDictionary alloc]init];
    NSLog(@"previous filter parameters %@", [previouslyFilteredContent description]);


    if ([previouslyFilteredContent valueForKey:@"Demo_Plan_ID"]!=nil)

        
        {
            [filterDict setValue:[previouslyFilteredContent valueForKey:@"Demo_Plan_ID"] forKey:@"Demo_Plan_ID"];
            
            [filterDict setValue:[previouslyFilteredContent valueForKey:@"Description"] forKey:@"Description"];
            
            
            demoPlanTextfield.text=[previouslyFilteredContent valueForKey:@"Description"];
        }
    
    if ([previouslyFilteredContent valueForKey:@"Product_Type"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Product_Type"] forKey:@"Product_Type"];
        
        productTypeTextfield.text=[SWDefaults getValidStringValue:[MedRepQueries fetchAppCOdeforProductType: [previouslyFilteredContent valueForKey:@"Product_Type"]]];
        
        
    }


    
    if ([previouslyFilteredContent valueForKey:@"Primary_UOM"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Primary_UOM"] forKey:@"Primary_UOM"];
        
        UOMTextfield.text=[previouslyFilteredContent valueForKey:@"Primary_UOM"];
        
    }
    
    if ([previouslyFilteredContent valueForKey:@"Brand_Code"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Brand_Code"] forKey:@"Brand_Code"];
        
        productBrandTextfield.text=[previouslyFilteredContent valueForKey:@"Brand_Code"];
        
    }

    if ([previouslyFilteredContent valueForKey:@"Brand_Code"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Brand_Code"] forKey:@"Brand_Code"];
        
        productBrandTextfield.text=[previouslyFilteredContent valueForKey:@"Brand_Code"];
        
    }

    if ([previouslyFilteredContent valueForKey:@"Agency"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Agency"] forKey:@"Agency"];
        
        productCategoryTextfield.text=[previouslyFilteredContent valueForKey:@"Agency"];
        
    }

    
    if ([previouslyFilteredContent valueForKey:@"Stock_Check"]!=nil) {
        
        [filterDict setValue:[previouslyFilteredContent valueForKey:@"Stock_Check"] forKey:@"Stock_Check"];
        
        if ([[previouslyFilteredContent valueForKey:@"Stock_Check"] isEqualToString:@"NO"]) {
            
            [productStockSwitch setOn:NO animated:NO];
        }
        
        
        if ([[previouslyFilteredContent valueForKey:@"Stock_Check"] isEqualToString:@"YES"]) {
            
            [productStockSwitch setOn:YES animated:NO];
        }
        
        
    }

    
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingProductFilter];
    }
    NSLog(@"content dictionary is %@", [self.contentArray description]);
    
    if (self.isEdetailingProduct==YES) {
        uomButtonHeightConstraint.constant=0.0f;
        uomLblHeightConstraint.constant=0.0f;
        uomTextFieldHeightConstraint.constant=0.0f;
        UOMTextfield.rightView.hidden=YES;
        uomBottomConstraint.constant=0.0f;
        uomTextFieldBottomConstraint.constant=0.0f;
        uomViewHeightConstraint.constant=0.0f;
        uomViewBottomConstraint.constant=0.0f;
        self.uomView.hidden=YES;
        
    }else
    {
        uomButtonHeightConstraint.constant=40.0f;
        uomLblHeightConstraint.constant=21.0f;
        uomTextFieldHeightConstraint.constant=30.0f;
        UOMTextfield.rightView.hidden=NO;
        uomBottomConstraint.constant=8.0f;
        uomTextFieldBottomConstraint.constant=0.0f;
        uomViewHeightConstraint.constant=59.0f;
        uomViewBottomConstraint.constant=8.0f;
        self.uomView.hidden=NO;

    }
    
    NSString* stockCheckParameter=[previouslyFilteredContent valueForKey:@"Stock_Check"];
    
    if ([NSString isEmpty:stockCheckParameter]==NO) {
        
        if ([stockCheckParameter isEqualToString:@"YES"]) {
            [filterDict setValue:@"YES" forKey:@"Stock_Check"];
            [productStockSwitch setOn:YES];
        }
        else{
            [filterDict setValue:@"NO" forKey:@"Stock_Check"];
            [productStockSwitch setOn:NO];

        }
        
    }
    
    

    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear",nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    
    
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    
    

}

-(void)closeFilter

{
    
    
    
    if ([self.filteredProductDelegate respondsToSelector:@selector(productFilterDidCancel)]) {
        
        
        //[self.filteredDoctorDelegate CustomPopOverCancelDelegate];
        [self.filteredProductDelegate filteredProductParameters:previouslyFilteredContent];
        
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];
    
//    if ([self.filteredProductDelegate respondsToSelector:@selector(productFilterDidCancel)]) {
//        
//       // [self.filteredProductDelegate productFilterDidCancel];
//        
//        
//
//        
//    }
//    
//
//    if ([self.filteredProductDelegate respondsToSelector:@selector(filteredProductParameters:)]) {
//        
//        [self.filteredProductDelegate filteredProductParameters:previouslyFilteredContent];
//        [filterPopOverController dismissPopoverAnimated:YES];
//
//    }
}

-(void)clearFilter

{
    
    filterDict=[[NSMutableDictionary alloc]init];
    [filterDict setValue:@"Active" forKey:@"Is_Active"];

    productBrandTextfield.text=@"";
    productCategoryTextfield.text=@"";
    UOMTextfield.text=@"";
    demoPlanTextfield.text=@"";
    productTypeTextfield.text=@"";
    
    [productStockSwitch setOn:YES animated:YES];

    
//    if ([self.filteredProductDelegate respondsToSelector:@selector(productFilterDidCancel)]) {
//        
//        [self.filteredProductDelegate productFilterDidCancel];
//        
//       // [filterPopOverController dismissPopoverAnimated:YES];
//        
//    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)typeButtonTapped:(id)sender {
    
    selectedDataString=@"Product_Type";
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Product";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Product";
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Product_Type"]valueForKey:@"Product_Type"];
    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
    }


}

- (IBAction)demoPlanButtonTapped:(id)sender {
    
    
    selectedDataString=@"Demo Plan";

    
    demoPlanArray=[[NSMutableArray alloc]init];
    
    NSString* currentDt=[MedRepQueries fetchDatabaseDateFormat];
    
    
    demoPlanArray=[MedRepQueries fetchDemoPlanforProducts:currentDt];
    
    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Description" ascending:YES];
    NSArray * sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray * sortedArray = [demoPlanArray sortedArrayUsingDescriptors:sortDescriptors];
    demoPlanArray=[sortedArray mutableCopy];
    
    NSMutableArray* demoPlanNamesArray =[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<demoPlanArray.count; i++) {
        [demoPlanNamesArray addObject:[[demoPlanArray  objectAtIndex:i]valueForKey:@"Description"]];
    }
    
    
    
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Demo Plan";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Demo Plan";
    
    NSLog(@"demo plan array is %@", [demoPlanArray description]);
      if (demoPlanNamesArray.count>0) {
        filterDescVC.filterDescArray=demoPlanArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
      else
      {
          [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
      }
}


- (IBAction)unitofMeasurementButtonTapped:(id)sender {
    
    selectedDataString=@"Primary_UOM";
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"UOM";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Unit of Measurement";
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Primary_UOM"]valueForKey:@"Primary_UOM"];
    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }



}

- (IBAction)brandButtonTapped:(id)sender {
    
    selectedDataString=@"Brand_Code";
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Brand";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Brand";
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Brand_Code"]valueForKey:@"Brand_Code"];
    
    NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
    
    
    if (filterDescArray.count>0) {
        
        
        
        for (NSInteger i=0; i<filterDescArray.count; i++) {
            NSString* currentValue=[filterDescArray objectAtIndex:i];
            
            
            NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
            
            if (refinedValue.length>0) {
                [refinedArray addObject:refinedValue];
                
            }
            
        }
        filterDescVC.filterDescArray=refinedArray;

        
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }

    

}


- (IBAction)categoryButtonTapped:(id)sender {
    
    selectedDataString=@"Agency";
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Category";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Category";
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Agency"]valueForKey:@"Agency"];
    
    
    NSMutableArray* refinedArray=[[NSMutableArray alloc]init];

    
    if (filterDescArray.count>0) {
        
        for (NSInteger i=0; i<filterDescArray.count; i++) {
            NSString* currentValue=[filterDescArray objectAtIndex:i];
            
            
            NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
            
            if (refinedValue.length>0) {
                [refinedArray addObject:refinedValue];
                
            }
            
        }

        
        filterDescVC.filterDescArray=refinedArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }

    

}

- (IBAction)stockSwitchTapped:(id)sender {
    

    
}


-(void)selectedDemoPlanDetaislwithIndex:(NSMutableArray*)demoPlanDetails selectedIndex:(NSInteger)index
{
    
    if (demoPlanDetails.count>0) {
        
        demoPlanDetailsArray=[[NSMutableArray alloc]init];
        demoPlanDetailsArray=demoPlanDetails;
        
        NSLog(@"selected demo plan Details in new delegate are %@", [demoPlanDetails objectAtIndex:index]);
        
        
        
        [filterDict setValue:[NSString stringWithFormat:@"%@", [[demoPlanDetails objectAtIndex:index] valueForKey:@"Demo_Plan_ID"]] forKey:@"Demo_Plan_ID"];
        
        
        [filterDict setValue:[NSString stringWithFormat:@"%@", [[demoPlanDetails objectAtIndex:index] valueForKey:@"Description"]] forKey:@"Description"];
        

        
        
        //   [demoPlanButton setTitle:[NSString stringWithFormat:@"%@", [[demoplanDetails objectAtIndex:0] valueForKey:@"Description"]] forState:UIControlStateNormal];
        
        demoPlanTextfield.text=[NSString stringWithFormat:@"%@", [[demoPlanDetails objectAtIndex:index] valueForKey:@"Description"]];
        
        
    }
}

-(void)selectedDemoPlanDetails:(NSMutableArray*)demoplanDetails
{
   
    /*
    if (demoplanDetails.count>0) {
   
        demoPlanDetailsArray=[[NSMutableArray alloc]init];
        demoPlanDetailsArray=demoplanDetails;
        
    NSLog(@"selected demo plan Details are %@", [demoplanDetails description]);
    
    
    
    [filterDict setValue:[NSString stringWithFormat:@"%@", [[demoplanDetails objectAtIndex:0] valueForKey:@"Demo_Plan_ID"]] forKey:@"Demo_Plan_ID"];
    
    
 //   [demoPlanButton setTitle:[NSString stringWithFormat:@"%@", [[demoplanDetails objectAtIndex:0] valueForKey:@"Description"]] forState:UIControlStateNormal];
        
        demoPlanTextfield.text=[NSString stringWithFormat:@"%@", [[demoplanDetails objectAtIndex:0] valueForKey:@"Description"]];

        
    }*/
    
}

-(void)selectedFilterValue:(NSString*)selectedString
{
    NSLog(@"selected string %@", selectedString);
    
    if ([selectedDataString isEqualToString:@"Product_Type"]) {
        
        
        NSString* sampleCodeType=[MedRepQueries fetchAppCodeValueforProductType:selectedString];
        
        [filterDict setValue:sampleCodeType forKey:selectedDataString];
      //  [typeButton setTitle:selectedString forState:UIControlStateNormal];
        productTypeTextfield.text=selectedString;
    }
    
    if ([selectedDataString isEqualToString:@"Demo Plan"]) {
        
        [filterDict setValue:selectedString forKey:selectedDataString];
        //[demoPlanButton setTitle:selectedString forState:UIControlStateNormal];
        demoPlanTextfield.text=selectedString;

    }
    
    if ([selectedDataString isEqualToString:@"Primary_UOM"]) {
        [filterDict setValue:selectedString forKey:selectedDataString];
        //[uomButton setTitle:selectedString forState:UIControlStateNormal];
        UOMTextfield.text=selectedString;
    }
    
    if ([selectedDataString isEqualToString:@"Agency"]) {
        [filterDict setValue:selectedString forKey:selectedDataString];
     //   [categoryButton setTitle:selectedString forState:UIControlStateNormal];
        productCategoryTextfield.text=selectedString;

    }
    if ([selectedDataString isEqualToString:@"Brand_Code"]) {
        [filterDict setValue:selectedString forKey:selectedDataString];
       // [brandButton setTitle:selectedString forState:UIControlStateNormal];
        productBrandTextfield.text=selectedString;

    }
    
}
- (IBAction)searchButtonTapped:(id)sender {
    
    
    
    //build the predicate
    
    
    NSLog(@"saved filters are %@", [filterDict description]);
    
    //start filter
    
    //NSLog(@"content array is %@", [self.contentArray description]);
    

    
    
    NSMutableArray* filteredProductsArray=[[NSMutableArray alloc]init];
    
    
    
    NSString* filter = @"%K == [c] %@";
    
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    if ([[filterDict valueForKey:@"Demo_Plan_ID"]length]>0) {
        
        //get product id from demo plan id
        NSMutableArray* productIdsArray=[MedRepQueries fetchProductIDforSelectedDemoPlan:[filterDict valueForKey:@"Demo_Plan_ID"]];
        
        NSLog(@"product ids for demo plan id are %@", [productIdsArray valueForKey:@"Product_ID"]);
        

//        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Product_ID", [NSString stringWithFormat:@"%@",[[productIdsArray objectAtIndex:0]valueForKey:@"Product_ID"]]]];

        NSPredicate *temp =  [NSPredicate predicateWithFormat:@"(Product_ID IN %@)", [productIdsArray valueForKey:@"Product_ID"]]
;
        
        [predicateArray addObject: temp];
        
    }
    
    
    if([[filterDict valueForKey:@"Agency"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Agency", [filterDict valueForKey:@"Agency"]]];
    }
    
    
    if([[filterDict valueForKey:@"Brand_Code"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Brand_Code", [filterDict valueForKey:@"Brand_Code"]]];
    }
    
    
    if([[filterDict valueForKey:@"Primary_UOM"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Primary_UOM", [NSString stringWithFormat:@"%@",[filterDict valueForKey:@"Primary_UOM"]]]];
    }
    if([[filterDict valueForKey:@"Product_Type"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Product_Type", [NSString stringWithFormat:@"%@",[filterDict valueForKey:@"Product_Type"]]]];
    }
    
    
    
    //stock predicate
    
    if (self.isEdetailingProduct==YES) {
        
        if ([[filterDict valueForKey:@"Stock_Check"]length]>0 && [[filterDict valueForKey:@"Stock_Check"] isEqualToString:@"YES"]) {
            
            
            
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.Lot_Qty.intValue > 0"]];
            
            
        }
        else
        {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.Lot_Qty.intValue == 0"]];
            
        }
    }
    else
    {
    
        NSLog(@"check stock %@", [filterDict valueForKey:@"Stock_Check"]);
        
    if ([NSString isEmpty:[filterDict valueForKey:@"Stock_Check"]]==NO && [[filterDict valueForKey:@"Stock_Check"] isEqualToString:@"YES"]) {
        [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.QTY.intValue > 0"]];
        

    }
    else
    {
        [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.QTY.intValue == 0"]];

    }
    }
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    NSLog(@"predicate is %@", compoundpred);
    
    filteredProductsArray=[[self.contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    NSLog(@"filtered products in filterVC %@", [filteredProductsArray description]);
    
    
    
    if (predicateArray.count==0) {
        
        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please change your filter criteria and try again", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noFilterAlert show];
       
    }
    
    
   else if (filteredProductsArray.count>0 ) {
        
       
       
       if ([self.filteredProductDelegate respondsToSelector:@selector(filteredProduct:)]) {
           
           [self.filteredProductDelegate filteredProduct:filteredProductsArray];
           
           [self.filteredProductDelegate filteredProductParameters:filterDict];
           [self.filterPopOverController dismissPopoverAnimated:YES];
           
           
       }
       
       
       /*
        //now link the demo plan ID
        //searching based on demo plan
        deomPlanID=[NSString stringWithFormat:@"%@",[filterDict valueForKey:@"Demo_Plan_ID"]];
        
       
       
      
           
       
        NSMutableArray* productIDsArray=[MedRepQueries fetchProductIDforSelectedDemoPlan:deomPlanID];
        
        NSLog(@"product ids array %@", [productIDsArray description]);
        
       
       
       
       
        
        NSMutableArray* tempProductArray=[[NSMutableArray alloc]init];
        
        
        //now match with these product ID's with the product ids in product array
       
        for (NSInteger i=0; i<productIDsArray.count; i++) {
            
            
            
            for(NSInteger j=0; j<filteredProductsArray.count; j++)
            {
                NSString *productID = [[filteredProductsArray objectAtIndex:j ]valueForKey:@"Product_ID"];
                
                if ([productID isEqualToString:[[productIDsArray objectAtIndex:i] valueForKey:@"Product_ID"]]) {
                    
                    [tempProductArray addObject:[filteredProductsArray objectAtIndex:j]];
                    
                }
                
            }
            
            
        }
        
        if (tempProductArray.count>0) {
            
            
             if ([self.filteredProductDelegate respondsToSelector:@selector(filteredProduct:)]) {
             
             [self.filteredProductDelegate filteredProduct:tempProductArray];
             [self.filterPopOverController dismissPopoverAnimated:YES];
             
             
             }
            
            else
            {
                UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [noFilterAlert show];
            }
            
        }

        NSLog(@"result after applying demo plan ID is %@", [tempProductArray description]);
       
       
       //continue search even without demoplan ID
       
       if (filteredProductsArray.count>0) {
           
           
           if ([self.filteredProductDelegate respondsToSelector:@selector(filteredProduct:)]) {
               
               [self.filteredProductDelegate filteredProduct:filteredProductsArray];
               [self.filterPopOverController dismissPopoverAnimated:YES];
               
               
           }
       }
       

        
      */
       
        
    }
    
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
        
    }
    

  
       
}

- (IBAction)switchSelected:(UISwitch *)sender {
    
    if ([sender isOn]) {
        
        [filterDict setValue:@"YES" forKey:@"Stock_Check"];
        
    }
    
    else
    {
        [filterDict setValue:@"NO" forKey:@"Stock_Check"];

    }
}
- (IBAction)resetButtonTapped:(id)sender {
    
    if ([self.filteredProductDelegate respondsToSelector:@selector(productFilterDidCancel)]) {
        
        
        [self.filteredProductDelegate productFilterDidCancel];
        
        
        
        
    }
    if ([self.filteredProductDelegate respondsToSelector:@selector(filteredProductParameters:)]) {
        
        [self.filteredProductDelegate filteredProductParameters:nil];

    }
    
    [self.filterPopOverController dismissPopoverAnimated:YES];
}


#pragma mark UITextFieldMethods

-(void)textFieldDidTap:(MedRepTextField*)tappedTextField withTitle:(NSString*)titleString withFilterType:(NSString*)textFieldFilterType withContentArray:(NSMutableArray*)textFieldContentArray
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=textFieldFilterType;
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=titleString;
    filterDescVC.filterDescArray=textFieldContentArray;
    [self.navigationController pushViewController:filterDescVC animated:YES];

}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==productTypeTextfield) {
        selectedDataString=@"Product_Type";
                NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Product_Type"]valueForKey:@"Product_Type"];
        if (filterDescArray.count>0) {
            [self textFieldDidTap:productTypeTextfield withTitle:@"Product" withFilterType:@"Product" withContentArray:filterDescArray];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }

    }
    else if (textField==demoPlanTextfield)
    {
        selectedDataString=@"Demo Plan";
        demoPlanArray=[[NSMutableArray alloc]init];
        NSString* currentDt=[MedRepQueries fetchDatabaseDateFormat];
        demoPlanArray=[MedRepQueries fetchDemoPlanforProducts:currentDt];
        NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Description" ascending:YES];
        NSArray * sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
        NSArray * sortedArray = [demoPlanArray sortedArrayUsingDescriptors:sortDescriptors];
        demoPlanArray=[sortedArray mutableCopy];
        NSMutableArray* demoPlanNamesArray =[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<demoPlanArray.count; i++) {
            [demoPlanNamesArray addObject:[[demoPlanArray  objectAtIndex:i]valueForKey:@"Description"]];
        }
        NSLog(@"demo plan array is %@", [demoPlanArray description]);
        if (demoPlanNamesArray.count>0) {
            [self textFieldDidTap:demoPlanTextfield withTitle:@"Demo Plan" withFilterType:@"Demo Plan" withContentArray:demoPlanArray];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
    }
    else if (textField==UOMTextfield)
    {
        selectedDataString=@"Primary_UOM";
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Primary_UOM"]valueForKey:@"Primary_UOM"];
        if (filterDescArray.count>0) {
            [self textFieldDidTap:UOMTextfield withTitle:@"Unit of Measurement" withFilterType:@"UOM" withContentArray:filterDescArray];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
    }
    else if (textField==productBrandTextfield)
    {
        
        selectedDataString=@"Brand_Code";
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Brand_Code"]valueForKey:@"Brand_Code"];
        NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
        if (filterDescArray.count>0) {
            for (NSInteger i=0; i<filterDescArray.count; i++) {
                NSString* currentValue=[filterDescArray objectAtIndex:i];
                NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
                if (refinedValue.length>0) {
                    [refinedArray addObject:refinedValue];
                }
            }
            [self textFieldDidTap:productBrandTextfield withTitle:@"Brand" withFilterType:@"Brand" withContentArray:refinedArray];
            
        }
        else
        {
            
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
    }
    else if (textField==productCategoryTextfield)
    {
        selectedDataString=@"Agency";
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredProductsContent:@"Agency"]valueForKey:@"Agency"];
        NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
        if (filterDescArray.count>0) {
            
            for (NSInteger i=0; i<filterDescArray.count; i++) {
                NSString* currentValue=[filterDescArray objectAtIndex:i];
                NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
                if (refinedValue.length>0) {
                    [refinedArray addObject:refinedValue];
                }
            }
            [self textFieldDidTap:productCategoryTextfield withTitle:@"Category" withFilterType:@"Category" withContentArray:refinedArray];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
        }
    }
    
    
    return NO;
}

@end
