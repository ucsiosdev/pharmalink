//
//  DistributionUpdateViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/11/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "DistributionUpdateViewController.h"

@interface DistributionUpdateViewController ()

@end

@implementation DistributionUpdateViewController

@synthesize target;
@synthesize action;

- (id)initWithDistributionItem:(NSDictionary *)item {
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) {
       distributionItem=[NSMutableDictionary dictionaryWithDictionary:item];
        [self setTitle:@"Update"];
    }
    
    return self;
}


//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView=nil;
    self.tableView = [[UITableView alloc] init];
     self.tableView.frame = self.view.bounds;
    //self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStyleDone target:self action:@selector(done:)] ];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)] ];
    //[self setContentSizeForViewInPopover:self.tableView.contentSize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
      //[self setContentSizeForViewInPopover:CGSizeMake(350, 200)];
    //[self setContentSizeForViewInPopover:CGSizeMake(350, self.tableView.contentSize.height)];


}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self setContentSizeForViewInPopover:CGSizeMake(350, self.tableView.contentSize.height)];
}



- (void)done:(id)sender {
    if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"Y"]) {
        [((SWTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]]) resignResponder];
        [((SWDateFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]]) resignResponder];
    }
    
    //vales is getting saved only on tap of return key, if save is directly tapped textfield delegate is not getting called
    
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:distributionItem];
#pragma clang diagnostic pop

}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"check textfield text %@", textField.text);
    [distributionItem setValue:textField.text forKey:@"Qty"];
}

- (void)cancel:(id)sender {
     #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action withObject:nil]; 
                #pragma clang diagnostic pop
}

- (void)scroll:(EditableCell *)cell {
    [self.tableView scrollRectToVisible:cell.frame animated:YES];
}
#pragma mark UITableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"Y"]) {
        return 2;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell.textLabel setText:NSLocalizedString(@"Available", nil)];
            
            if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"Y"]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        } else if (indexPath.row == 1) {
            [cell.textLabel setText:@"Out of Stock"];
            
            if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"N"]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        }
    } else {
        if (indexPath.row == 0) {
            cell=nil;
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Quantity", nil)];
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Quantity", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Qty"];
            [((SWTextFieldCell *)cell).textField setText:[distributionItem stringForKey:@"Qty"]];
            [((SWTextFieldCell *)cell).textField setDelegate:self];
        } else if (indexPath.row == 1) {
            [SWDefaults setPaymentType:@"NO"];
            cell=nil;
            cell = [[SWDateFieldCell alloc] initWithReuseIdentifier:@"Date"] ;
            [((SWDateFieldCell *)cell).titleLabel setText:NSLocalizedString(@"Expiry Date", nil)];
            [((SWDateFieldCell *)cell) setKey:@"ExpDate"];
            [((SWDateFieldCell *)cell) setDelegate:self];
            [((SWDateFieldCell *)cell) setDate:[distributionItem objectForKey:@"ExpDate"]];
        }
    }
    
    return cell;
}
}

#pragma mark UITable View delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [distributionItem setValue:@"Y" forKey:@"Avl"];
            [distributionItem setValue:[NSDate date] forKey:@"ExpDate"];
        } else if (indexPath.section == 0 && indexPath.row == 1) {
            [distributionItem setValue:@"N" forKey:@"Avl"];
            [distributionItem setValue:@"" forKey:@"Qty"];
            [distributionItem setValue:@"" forKey:@"ExpDate"];
        }

        [self.tableView reloadData];
        [self setPreferredContentSize:self.tableView.contentSize];
    }
}

#pragma mark EditCell Delegate
- (void)editableCellDidStartUpdate:(EditableCell *)cell {
    [self performSelector:@selector(scroll:) withObject:cell afterDelay:0.4f];
}

- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key {
    [distributionItem setValue:value forKey:key];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [distributionItem setValue:textField.text forKey:@"Qty"];

    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
 
    //OLA! Allow only positive numerical input for quantity
    NSString *allowedCharacters;
    if ([textField.text length]>0)//no decimal point yet, hence allow point
        allowedCharacters = @"0123456789";
    else
        allowedCharacters = @"123456789";//first input character cannot be '0'
    
    if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
    {
        // BasicAlert(@"", @"This field accepts only numeric entries.");
        
        return NO;
    }
    
    
    
    return YES;
    
}


@end