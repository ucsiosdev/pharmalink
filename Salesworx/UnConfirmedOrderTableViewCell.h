//
//  UnConfirmedOrderTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 1/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnConfirmedOrderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *refNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderAmount;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end
