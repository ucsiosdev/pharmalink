

//
//  SWAppDelegate.m
//  Salesworx
//
//  Created by msaad on 5/9/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SWAppDelegate.h"
#import "SWSession.h"
#import "SWDashboard.h"
#import "Flurry.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "LoginViewController.h"
#import "SWDefaults.h"
#import "SSKeychain.h"
#import "TestFlight.h"
#import "AlSeerWelcomeViewController.h"
#import "MedRepMenuViewController.h"
#import "MedRepMenuViewController.h"
#import "MedRepDefaults.h"
#import "DashboardAlSeerViewController.h"
#import "BackgroundDocumentsDownload.h"
#import "SharedUtils.h"
#import "FMDB/FMDBHelper.h"
#import "MedRepQueries.h"
#import "Reachability.h"
#include <CommonCrypto/CommonCrypto.h>
#import "AGPushNoteView.h"
#import "JNKeychain.h"
#import "SalesWorxBackgroundSyncManager.h"
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>
#import <Google/Analytics.h>
#import "SalesWorxTimeManager.h"
#import "MBProgressHUD.h"
#import "SalesWorxNotificationManager.h"

@implementation SWAppDelegate

@synthesize window = _window;
@synthesize sessionManager,activationManager,licenseVC,itemCodeString,currentDownloadingDocumentsArray,currentMediaDownloadingSession,currentLocationCapturedTime,CurrentRunningMedRepVisitDetails,CurrentRunningSalesWorxVisit,isFullSyncMandatory;
@synthesize selectedMenuIndexpath,currentExceutingDataUploadProcess,currentExceutingDataDownloadProcess;

- (double)currentMemoryUsage {
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    
    if(kernReturn == KERN_SUCCESS)
    {
        NSLog(@"current memory usage by salesworx is %f",vmStats.wire_count/1024.0);

        return vmStats.wire_count/1024.0;
    }
    else
    {
        
        return 0;
    }
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSLog(@"Check branch commit");
    NSLog(@"second commit from branch");
    NSLog(@"Third commit from branch");
    NSLog(@"Test commit from master by Neha");
    
    NSLog(@"first commit from Menu_Merch by Neha");

    NSLog(@"first commit in edetailing by prasann");

    
//    NSString* sampleUniCodeString = @"\\U00a0Testnewtest";
//
//    NSData   *asciiDataSample   = [[sampleUniCodeString stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//
//    NSString *asciiStringSample = [[NSString alloc] initWithData:asciiDataSample
//                                                  encoding:NSASCIIStringEncoding];
//    NSLog(@"UNICODE TO ASCII %@", asciiStringSample);
    
    
//    NSMutableArray* coachData=[[SWDatabaseManager retrieveManager]fetchCoachSurveyConfirmationData];
//    NSLog(@"coach data is %@", coachData);
    
    
    /*
    NSMutableArray* tempArray=[[NSMutableArray alloc]init];
    
    NSIndexPath * test = [NSIndexPath indexPathForRow:0 inSection:0];

    
    NSMutableArray* tempArray1=[[NSMutableArray alloc]init];
    
    NSIndexPath * test1 = [NSIndexPath indexPathForRow:0 inSection:0];

    NSString* varStr = @"";
    NSString* varStr1 = @"";

    if ([varStr isEqualToString:varStr1]) {
        
    }
    varStr= [varStr isEqualToString:@"1"]?@"2":@"3";
    
    
    NSPredicate* isSelectedPredicate = [NSPredicate predicateWithFormat:@"SELF.isSelected == 'Y'"];
    NSArray* result = []
    
    
    [tempArray1 setValue:@"YES" forKey:@"Selected"];
    if (![tempArray containsObject:test] ||[tempArray1 containsObject:test1]) {
        
        [tempArray addObject:test];
    }
    else
    {
        
    }
    
    
    for(NSMutableDictionary* currentDict in tempArray)
    {
        
    }*/
    
    if (@available(iOS 13, *)) {
        [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:(74.0/255.0) green:(146.0/255.0) blue:(213.0/255.0) alpha:1.0]} forState:UIControlStateNormal];
        [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTintColor:[UIColor colorWithRed:(74.0/255.0) green:(146.0/255.0) blue:(213.0/255.0) alpha:1.0]];
        
        
    }
    
     pushToCustomers=NO;

    [GMSServices provideAPIKey:@"AIzaSyAOpIPxhcIJ4PCHkIAl5ed_eP2cfu5HYsg"];
   

    //[[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"matchedBeaconsArray"];
  
    NSLog(@"device token is %@ customer id %@ %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"],[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"],[SWDefaults userProfile]);
    self.isNewSync=YES;
    self.isBackgroundProcessRunning=NO;
    currentExceutingDataUploadProcess=KDataUploadProcess_None;
    currentExceutingDataDownloadProcess=kDataDownloadProcess_NONE;
    
    isFullSyncMandatory=NO; /** as default No*/
    [SSKeychain setAccessibilityType:kSecAttrAccessibleAlways];
    
    
    NSDictionary* versionDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString *verisonNumber = [versionDict objectForKey:@"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSString* versionString=[NSString stringWithFormat:@"%@(%@)",verisonNumber,build];
    
    
    NSString * deviceID=[[NSUserDefaults standardUserDefaults] valueForKey:@"savedDeviceID"];
    
    
    if ([NSString isEmpty:deviceID]==NO) {
        [UIPasteboard generalPasteboard].string=deviceID;
        
        deviceIDAlert = [[UIAlertView alloc] initWithTitle:@"New Device ID Available"
                                                        message:deviceID
                                                       delegate:self
                                              cancelButtonTitle:@"Done"
                                              otherButtonTitles:nil];
        
        
       // [deviceIDAlert show];
        

        NSLog(@"DEVICE ID FOR NOTIFICATIONS IS %@", deviceID);
 
    }
    
   
    
    
    
    [SWDefaults writeLogs:[NSString stringWithFormat:@"\n ****** APP LAUNCHED WITH VERSION NUMBER: %@  BY USER: %@ ON: %@   ********  \n", versionString,[SWDefaults username],[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]];
    

    // must be done
    //enable this whenever buid is sent, this will copy logs to device logs file in documents directory
    
    #if DEBUG
        NSLog(@"I'm running in a DEBUG mode");
//    [AppControl retrieveSingleton].ENABLE_BACKGROUND_SYNC = KAppControlsYESCode;
    //currentExceutingDataUploadProcess=KDataUploadProcess_VTRXBS;
    #else
        [SWDefaults redirectLogToDocuments];
    #endif
    
   // [SWDefaults redirectLogToDocuments];

   
   // [SWDefaults checkandDeletePreviousLogs];
    
    [SWDefaults deleteOldDeviceLogFiles];
    
    NSLog(@"last issue note reference is %@", [MedRepDefaults lastIssueNoteReference]);
    
    
    NSLog(@"username and password for activate %@ %@", [SWDefaults usernameForActivate],[SWDefaults passwordForActivate]);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveDownloadNotification:)
                                                 name:@"StartDownLoadNotification"
                                               object:nil];
    

    selectedMenuIndexpath=[NSIndexPath indexPathForRow:0 inSection:0];
    currentDownloadingDocumentsArray=[[NSMutableArray alloc]init];
    //local notification for sync
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
      
    
    
//    [[UIBarButtonItem appearance]setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
//    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    self.locationManager.distanceFilter=kCLDistanceFilterNone;
    [self.locationManager startMonitoringSignificantLocationChanges];
    [self.locationManager startUpdatingLocation];
    
    
    
    
    NSString * test=[SSKeychain passwordForService:@"registereduser" account:@"user"];
    
    [SWDefaults ClearLoginCredentials];

    
    NSLog(@"key chain result is %@", test);
    
    
    
//    UIAlertView * testUserAlert=[[UIAlertView alloc]initWithTitle:@"Alert User" message:test delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [testUserAlert show];
    
    
    
    if ([test isEqualToString:@"registereduser"]) {
        LoginViewController* loginVC=[[LoginViewController alloc]init];
        
        
//      
//                loginVC.txtUserName.text=@"";
//        //
//                loginVC.txtPassword.text=@"";
        
//        [SWDefaults setUsername:@""];
//        [SWDefaults setPassword:@""];
//        [SWDefaults setUsernameForActivate:@""];
//        [SWDefaults setPasswordForActivate:@""];
//        
//        [loginVC.txtUserName setHidden:YES];
//        [loginVC.txtPassword setHidden:YES];
//        
//        loginVC.view.hidden=YES;
        //
        
        
    }
    
//    NewActivationViewController* nVC=[[NewActivationViewController alloc]init];
//    
//    if (nVC.isDemo==NO) {
//        NSLog(@"check bool after act");
//    }
//    
//    BOOL saved = [[NSUserDefaults standardUserDefaults] boolForKey:@"isDemo"];
//
//    if (saved==NO) {
//        NSLog(@"bool is no");
//    }
    
    //
//    if (saved==NO) {
//        NSLog(@"clear text fileds");
//        
//         LoginViewController* loginVC=[[LoginViewController alloc]init];
//      
//         loginVC.txtUserName.text=@"";
//    
//         loginVC.txtPassword.text=@"";
//        
//        [SWDefaults setUsername:nil];
//        [SWDefaults setPassword:nil];
//    }
//    
   // NSLog(@"Bool value: %d",saved);
    
    
//    if ([licenseVC.customerID isEqualToString:@"C15K82659"]) {
//        <#statements#>
//    }
    
    
    
//    [SWDefaults setUsernameForActivate:@"demo"];
//    [SWDefaults setPassword:@"demo123"];
//    
//   LoginViewController* loginVC=[[LoginViewController alloc]init];
////    
//   loginVC.txtUserName.text=@"";
////    
//    loginVC.txtPassword.text=@"";

    
    //
  //  [SWDefaults setUsername:@""];
//   [SWDefaults setPassword:@""];
    
    
   //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"login"];
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
    
    
//    productAddArray = [NSArray arrayWithArray: [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Product_Addl_Info Where Inventory_Item_ID = '%@' AND Attrib_Name='PROD_IMG'",[product stringForKey:@"ItemID"]]]];
    
    
    
//    NSArray* arr=[[NSArray alloc]init];
//
//    arr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat: @"select * from TBL_User"]];
    
//    NSString * myString = [arr componentsJoinedByString:@""];
//    
//    if ([myString isEqualToString:@"Demo"]) {
//        NSLog(@"it is demo");
//    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    
    
    
    
    
if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
   
        // iOS 8 Notifications
//        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//        
//        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        
    }
    

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
       
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

    
//    [application registerForRemoteNotificationTypes:
//     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
//    
//    
    

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    
    
    //test flight integration
   // [TestFlight takeOff:@"1223ace9-0d29-4bfc-8fca-1394ffc53001"];
    
    
//    /**testing test flight by deliberately crashing the app **/
//    
//

   
    
    
//    
//    //**test flight test ends**?//
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    NSLog(@"local notification object in did finsh launching with options %@", localNotif);
    
    
    if (localNotif) {
        // Grab the pushKey:
        // "pushKey" is an NSString declared globally
        // The "pushKey" in the "valueForKey" statement is part of my custom JSON Push Notification pay-load.
        // The value of pushKey will always be a String, which will be the name of the
        // screen I want the App to navigate to. So when a Notification arrives, I go
        // through an IF statement and say "if pushKey='specials' then push the
        // specialsViewController on, etc.
        
        // Here, I parse the "alert" portion of my JSON Push-Notification:
        NSDictionary *tempDict = [localNotif valueForKey:@"aps"];
        
        NSLog(@"aps with local notification %@", tempDict);
        
        
        NSString *pushMessage = [tempDict valueForKey:@"alert"];
        
        
        // Finally, ask user if they wanna see the Push Notification or Cancel it:
        UIAlertView *bam = [[UIAlertView alloc] initWithTitle:@"(from appDidFinishWithOptions)"
                                                      message:pushMessage
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"View Info", nil];
       // [bam show];
        
    }
    
    if (localNotif)
    {
        [self decrementOneBdge];
    }

    
    NSDictionary *apnsBody = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];

    [self fetchPushNotificationContent:apnsBody];
    
    
    /* issues due to flurry in iOS 9.1 check*/
    
    
//    [Flurry setCrashReportingEnabled:YES];
//    
//    [Flurry startSession:@"3BNJXBDV3YJ24RHCW5FZ"];
//
//    [Flurry startSession:@"5HK6K9GDFYBTC2JSV2BJ"];
//    
//    [Flurry logEvent:@"Application Launched"];
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isFullSyncDone = NO;
    single.isFullSyncCollection = NO;
    single.isFullSyncCustomer = NO;
    single.isFullSyncSurvey = NO;
    single.isFullSyncProduct = NO;
    single.isFullSyncDashboard = NO;
    single.isFullSyncMessage = NO;
    single.isDistributionItemGet=YES;
    
    single.databaseIsOpen = 0;
//    [SWDefaults initializeDefaults];
//    [MedRepDefaults initializeMedRepDefaults];

    
    [SWDefaults setAppControl:[[SWDatabaseManager retrieveManager] dbGetAppControl]];
    
    
   // NSLog(@"app control from db %@", [[SWDatabaseManager retrieveManager] dbGetAppControl]);
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    //-------Get app version
    //for med rep we were just updating build number even when app is being installed without deleting the previous version it was checking for version number (2.0) and showing login screen.so check for build number for medrep
    
    
    
    
   // NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    //-------Set app version, if first launch
    if (![SWDefaults versionNumber] || [SWDefaults versionNumber].length == 0)
    {
        [SWDefaults setVersionNumber:[[[SWDefaults alloc]init] getAppVersionWithBuild]];
    }

    //-------If app version is old, replace DB file and change version
    if (![[SWDefaults versionNumber] isEqualToString:[[[SWDefaults alloc]init] getAppVersionWithBuild]])
    {
//        NSError *error;
//        
//        
//        //iOS 8 support
//        NSString *documentsDirectory;
//        
//        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
//            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
//        }
//        
//        else
//        {
//            NSArray *paths = NSSearchPathForDirectoriesInDomains
//            (NSDocumentDirectory, NSUserDomainMask, YES);
//            documentsDirectory = [paths objectAtIndex:0];
//        }
//
//        
//        //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        //NSString *documentsDirectory = [paths objectAtIndex:0];
//        
//        //for actual file
//        NSString *fileName = databaseName;
//        NSString *documentDBFolderPath = [documentsDirectory stringByAppendingPathComponent:fileName];
//        [[NSFileManager defaultManager] removeItemAtPath: documentDBFolderPath error: &error];
//        
//        //for zip file
//        NSString *fileName1 = @"swx.sqlite.gz";
//        NSString *documentDBFolderPath1 = [documentsDirectory stringByAppendingPathComponent:fileName1];
//        [[NSFileManager defaultManager] removeItemAtPath: documentDBFolderPath1 error: &error];
//
//        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
//        
//        [SWDefaults setVersionNumber:avid];
//        [SWDefaults initializeDefaults];
//        [SWDefaults setAppControl:[[SWDatabaseManager retrieveManager] dbGetAppControl]];
//        
//        [MedRepDefaults initializeMedRepDefaults];

    }
    
    //iOS 8 support
    NSString *applicationDocumentsDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        applicationDocumentsDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        applicationDocumentsDir = [paths objectAtIndex:0];
    }
    
    
    //test
    
    NSLog(@"TEST DATE %@", [MedRepDefaults fetchTodaysDate]);
    
    FMDatabase *database = [FMDatabase databaseWithPath:[applicationDocumentsDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database executeUpdate:@"vacuum"];
    
    //NSString *applicationDocumentsDir =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *storePatha = [applicationDocumentsDir stringByAppendingPathComponent:@"Activation_Log.txt"];
    [@"" writeToFile:storePatha atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:@"Sync_Log.txt"];
    [@"" writeToFile:storePath atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];

    //-------Check for license
    //-------Check for license
    
    SalesworxLicenseActivationViewController * licenseActivationVC=[[SalesworxLicenseActivationViewController alloc]init];
    self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:licenseActivationVC] ;
    [self.window setRootViewController:self.mainNavigationController];
    
    //    if ([[SWDefaults licenseKey] count] == 0)
    //    {
    //        //-------For no license, push license controller
    //
    //
    //
    //        /*
    //        licenseVC = [[LicensingViewController alloc] init] ;
    //      //  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:licenseVC] ;
    //        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:licenseVC] ;
    //        [licenseVC setTarget:self];
    //        [licenseVC setAction:@selector(licenceDone)];
    //        [self.window setRootViewController:self.mainNavigationController];*/
    //
    //
    //
    //
    //        SalesworxLicenseActivationViewController * licenseActivationVC=[[SalesworxLicenseActivationViewController alloc]init];
    //
    //        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:licenseActivationVC] ;
    //        [self.window setRootViewController:self.mainNavigationController];
    //
    //
    //    }
    //    else
    //    {
    //        //-------Check for user activation
    //        if ([[SWDefaults isActivationDone] isEqualToString:@"NO"]){
    //
    //            //-------For no activation, push activation controller
    //
    //            [self setActivationManager:[[NewActivationViewController alloc] initWithNoDB] ];
    //            self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:self.activationManager] ;
    //            [activationManager setTarget:self];
    //            [activationManager setAction:@selector(activationDone)];
    //            [self.window setRootViewController:self.mainNavigationController];
    //
    //        }
    //        else{
    //
    //
    //            //-------Push split controller with dashboard controller on center and menu on left
    //
    //
    //
    //
    //            //change dashboard here
    //            SWDashboardViewController *dashboardViewController;
    //
    //            DashboardAlSeerViewController *dashboardAlSeerViewController;
    //
    //
    //
    //
    //
    //
    //           /*
    //            if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
    //
    //
    //                //before displaying dashboard show a welcome screen as suggested by client on 12/4/2015
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //               dashboardAlSeerViewController = [[DashboardAlSeerViewController alloc] init];
    //                self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardAlSeerViewController];
    //
    //
    ////                dashboardViewController = [[SWDashboardViewController alloc] init];
    ////                self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    //
    //
    //
    //                MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
    //                _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
    //
    //                SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
    //
    //                [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
    //
    //                splitViewController=nil;
    //                menuViewController=nil;
    //                dashboardAlSeerViewController=nil;
    //
    //
    //
    //
    //            }*/
    //
    //
    ////            else
    ////            {
    ////
    //
    //
    //                //add a flag for med rep
    //
    //            NSLog(@"check MedRep Flag %hhd", [SWDefaults isMedRep]);
    //
    //          //  NSLog(@"app control flags are %@", [SWDefaults appControl]);
    //
    //                if ([SWDefaults isMedRep]==YES) {
    //
    //
    //
    //                    NSLog(@"activating med rep menu");
    //
    //                    dashboardViewController = [[SWDashboardViewController alloc] init];
    //                    self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    //
    //                    MedRepMenuViewController *menuViewController = [[MedRepMenuViewController alloc] init] ;
    //                    _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
    //
    //                    SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
    //
    //                    [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
    //
    //                    splitViewController=nil;
    //                    menuViewController=nil;
    //                    dashboardViewController=nil;
    //
    //
    //                }
    //
    //
    //                else
    //                {
    //
    //                dashboardViewController = [[SWDashboardViewController alloc] init];
    //                self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    //
    //                MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
    //                _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
    //
    //                SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
    //
    //                [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
    //                
    //                splitViewController=nil;
    //                menuViewController=nil;
    //                dashboardViewController=nil;
    //                
    //                }
    //
    //            }
    //            
    //            //[[NSNotificationCenter defaultCenter] addObserver:dashboardViewController selector:@selector(DownloadingDidComplete:) name:@"DownloadingDidComplete" object:nil];
    //            
    //            
    //            
    //       // }
    //    }
    
    CJSONDeserializer *dejsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *testserverArray = [NSMutableArray arrayWithArray:[dejsonSerializer deserializeAsArray:data error:&error]  ] ;
    if([testserverArray count] == 0)
    
    {
        CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
        NSMutableArray *serverArray = [NSMutableArray array] ;
        NSMutableDictionary *row1 = [NSMutableDictionary dictionary] ;
        [row1 setValue:@"Demo Server" forKey:@"serverName"];
        [row1 setValue:@"swx-db.cloudapp.net:12008" forKey:@"serverLink"];
        //[row1 setValue:@"demo.ucspromotions.com:10108" forKey:@"serverLink"];
        
        NSData *dataDict = [jsonSerializer serializeObject:row1 error:&error];
        NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
        
        NSLog(@"setting selected server dict %@", stringDataDict);

        [SWDefaults setSelectedServerDictionary:stringDataDict];
        
        [serverArray addObject:row1];
        for (int i=1; i<=10; i++){
            @autoreleasepool {
            NSMutableDictionary *row = [NSMutableDictionary dictionary] ;
            [row setValue:[NSString stringWithFormat:@"Location %d",i] forKey:@"serverName"];
            [row setValue:[NSString stringWithFormat:@"192.168.100.%d:10008",i] forKey:@"serverLink"];
            //[serverArray addObject:row];
            }
        }
        NSData *datas = [jsonSerializer serializeObject:serverArray error:&error];
        NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding] ;
        
        //adding dummy server arrays
        //[SWDefaults setServerArray:stringData];
    }
    
    BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];
    if (checkInternetConnectivity==YES && [[SWDefaults isActivationDone] isEqualToString:@"YES"]) {
        [self startMediaDownloadProcess];
    }
    else
    {
        
    }
    
//    [AGPushNoteView showWithNotificationMessage:@"New Media Uploaded Please do a full sync"];

    
    NSString * jnKeyChainLicenseID=[JNKeychain loadValueForKey:@"licenseDeviceID"];

    NSLog(@"activation license id from jn  device keychain %@", [SWDefaults getValidStringValue:jnKeyChainLicenseID]);
    
    /**Background Sync*/
    // Add an observer that will respond to enableBackGroundSync
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableVTRXBackGroundSync:)
                                                 name:KENABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
    
    // Add an observer that will respond to disableBackGroundSync
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disableVTRXBackGroundSync:)
                                                 name:KDISABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];

    // Add an observer that will respond to enableBackGroundSync
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableCOMMBackGroundSync:)
                                                 name:KENABLE_COMM_BS_NOTIFICATIONNameStr object:nil];
    
    // Add an observer that will respond to disableBackGroundSync
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disableCOMMBackGroundSync:)
                                                 name:KDISABLE_COMM_BS_NOTIFICATIONNameStr object:nil];

    
    //add an observer that will responsd to enable background product stock check
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableProdStkBackGroundSync:)
                                                 name:kENABLE_PRODSTK_NOTIFICATIONNameStr object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disableProdStkBackGroundSync:)
                                                 name:kDISABLE_PRODSTK_NOTIFICATIONNameStr object:nil];
    

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableRouteBackGroundSync:)
                                                 name:KENABLE_ROUTE_BS_NOTIFICATIONNameStr object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableRouteCancelBackGroundSync:)
                                                 name:KENABLE_ROUTE_CANCEL_BS_NOTIFICATIONNameStr object:nil];
    
    
    char characterCodeInASCII = 255;
    NSString *stringWithAInIt = [[NSString alloc] initWithBytes:&characterCodeInASCII length:1 encoding:NSASCIIStringEncoding];
    NSLog(@"TEST ASCII CHARACTER %@",stringWithAInIt);
    
    NSString* nbspStr=@"Hello&nbsp;World";
    NSLog(@"nbsp str %@", nbspStr);
    

    
    NSData   *asciiData   = [[stringWithAInIt stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""] dataUsingEncoding:NSASCIIStringEncoding];
    NSString *asciiString = [[NSString alloc] initWithData:asciiData
                                                  encoding:NSASCIIStringEncoding];
    
    
    NSString* testStr=[asciiString stringByAppendingString:@"This is ASCII string"];
    
    NSArray * testAsciiArray=[testStr componentsSeparatedByString:asciiString];
    
    NSLog(@"ascii array is %@",testAsciiArray);
    NSLog(@"UNICODE AFTER:%@", [asciiString stringByAppendingString:@"This is ASCII string"]);
    
    
    NSMutableArray * testArray=[[NSMutableArray alloc]initWithObjects:stringWithAInIt,asciiString, nil];
    NSLog(@"open data array with ascii char %@ \n", [testArray description]);
    

    
   // UA-52407698-2
    
    // Configure tracker from GoogleService-Info.plist.
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-52407698-2"];

    gai.trackUncaughtExceptions = NO;  // report uncaught exceptions
   // gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    

    //[FIRApp configure];

    [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"_UIConstraintBasedLayoutLogUnsatisfiable"];

    

//  //  Once a user is activated successfully logs in and changes the server name from sync pop over and logs out and taps on activate new user, the new user activation screen displays the changed server name, this should be modified to show the server name with which the user has been activated with.
//
//    if ([NSString isEmpty:[SWDefaults activatedServerDictionary]]) {
//        
//        NSLog(@"activated server dict is empty and selected server dict is %@", [SWDefaults selectedServerDictionary]);
//        [SWDefaults setActivatedServerDetailsDictionary:[SWDefaults selectedServerDictionary]];
//        
//        
//    }
    
    
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(timeChanged:) name:UIApplicationSignificantTimeChangeNotification object:nil];
    
    
    NSLog(@"time since boot %f",[[NSProcessInfo processInfo] systemUptime]);
    
    
    //[[[SalesWorxTimeManager alloc]init] startApplicationTimer];
    
//    NSArray *fontFamilies = [UIFont familyNames];
//    
//    for (int i = 0; i < [fontFamilies count]; i++)
//    {
//        NSString *fontFamily = [fontFamilies objectAtIndex:i];
//        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
//        NSLog (@"%@: %@", fontFamily, fontNames);
//    }
    
    
    NSString *osVersionString = [[UIDevice currentDevice] systemVersion];
  
    NSLog(@"device OS version is %@",osVersionString);
    
    
    [Kontakt setAPIKey:kKontactBeaconAPIKey];

    if ([KTKBeaconManager isMonitoringAvailable]) {
        
    // Initiate Beacon Manager
    self.beaconManager = [[KTKBeaconManager alloc] initWithDelegate:self];
    
    // Request Location Authorization
    [self.beaconManager requestLocationWhenInUseAuthorization];
    
    }
    
    // Kontakt.io proximity UUID
    NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:@"f7826da6-4fa2-4e98-8024-bc5b71e0893e"];
    
    // Create region instance
    KTKBeaconRegion *region = [[KTKBeaconRegion alloc] initWithProximityUUID: proximityUUID identifier:@"123"];
    region.notifyOnEntry=YES;
    region.notifyOnExit=YES;
    region.notifyEntryStateOnDisplay=YES;

  

    
    switch ([KTKBeaconManager locationAuthorizationStatus]) {
            // Non-relevant cases are cut
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            if ([KTKBeaconManager isMonitoringAvailable]) {
                [self.beaconManager startMonitoringForRegion:region];
                [self.beaconManager startRangingBeaconsInRegion: region];

            }
            break;
    }
    
    

    
    // Start Monitoring
   // [self.beaconManager startMonitoringForRegion: region];
    
    // You can also start ranging ...
    

    //assert(false);

   

    //test crash

//
    
    //analytics
#if DEBUG
    NSLog(@"I'm running in a DEBUG mode");
#else
#endif
   
    [FIRApp configure];
    /** Disabling the firebase analytics*/
    [[FIRAnalyticsConfiguration sharedInstance] setAnalyticsCollectionEnabled:YES];
    [Fabric with:@[[Crashlytics class]]];

    return YES;
}

-(void)alertStatus:(NSString*)statusString
{
    
}

//- (void)centralManagerDidUpdateState:(CBCentralManager *)central
//{
//    NSString *stateString = nil;
//    switch(self.bluetoothManager.state)
//    {
//        case CBCentralManagerStateResetting:
//            [self alertStatus:@"The connection with the system service was momentarily lost, update imminent." :@"update imminent" :0];
//            break;
//            
//        case CBCentralManagerStateUnsupported:
//            [self alertStatus:@"The platform doesn't support Bluetooth Low Energy.":@"weak Bluetooth Connection" :0];
//            break;
//            
//        case CBCentralManagerStateUnauthorized:
//            [self alertStatus:@"The app is not authorized to use Bluetooth Low Energy.":@"weak Bluetooth Connection" :0];
//            break;
//        case CBCentralManagerStatePoweredOff:
//            stateString = @"Bluetooth is currently powered off.";
//            [self alertStatus:@"Bluetooth is currently powered off , powered ON first.":@"No Bluetooth Connection" :0];
//            break;
//        case CBCentralManagerStatePoweredOn:
//            stateString = @"Bluetooth is currently powered on and available to use.";
//            //  [self alertStatus:@"Bluetooth is currently powered ON.":@" Bluetooth available" :0];
//            
//            break;
//        default:
//            stateString = @"State unknown, update imminent.";
//            break;
//    }
//    
//    
//    NSLog(@"Bluetooth State %@",stateString);
//    
//    
//}





//-(void)timeChanged:(NSNotification*)timeChangeNotification
//{
//    NSLog(@"time change notification is %@", timeChangeNotification.userInfo);
//    
//    [SWDefaults showAlertAfterHidingKeyBoard:@"UIApplicationSignificantTimeChangeNotification called" andMessage:@"Please reset the device time to current time" withController:self.window.visibleViewController];
//
//}


// the function specified in the same class where we defined the addObserver
- (void)enableVTRXBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:KENABLE_V_TRX_BS_NOTIFICATIONNameStr])
    {
        if(V_TRX_BS_Timer==nil)
        {
            AppControl *appcontrol=[AppControl retrieveSingleton];
            
//            NSInteger timerValue=  [appcontrol.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value]?150:90;

            NSInteger timerValue=  [appcontrol.V_TRX_SYNC_INTERVAL integerValue];
            NSLog(@"timer for visits sync %ld",timerValue);
            V_TRX_BS_Timer = [NSTimer timerWithTimeInterval:timerValue
                                                     target:self
                                                   selector:@selector(postVTRXDataToServer)
                                                   userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:V_TRX_BS_Timer forMode:NSRunLoopCommonModes];

        }
        

    }
}


-(void)postVTRXDataToServer
{
    NSLog(@"postVTRXDataToServer in app delegate called");
    if([SWDefaults isVTRXBackgroundSyncRequired] && ![NSString isEmpty:[SWDefaults username]] && [currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_None])
    {
        currentExceutingDataUploadProcess=KDataUploadProcess_VTRXBS;
        SalesWorxBackgroundSyncManager * salesWorxBackgroundSyncManager=[[SalesWorxBackgroundSyncManager alloc]init];
        [salesWorxBackgroundSyncManager postVisitDataToserver];
    }
    else
    {
        // NSLog(@"back ground sync not enabled");
    }
    
}


// the function specified in the same class where we defined the addObserver
- (void)disableVTRXBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:KDISABLE_V_TRX_BS_NOTIFICATIONNameStr])
    {
        if (V_TRX_BS_Timer==nil) {
            [V_TRX_BS_Timer invalidate];
            V_TRX_BS_Timer = nil;
        }
    }
}
// the function specified in the same class where we defined the addObserver
- (void)enableCOMMBackGroundSync:(NSNotification *)notification {
    
    if([notification.name isEqualToString:KENABLE_COMM_BS_NOTIFICATIONNameStr])
    {
        if (COMM_BS_Timer==nil) {
        
            AppControl *appcontrol=[AppControl retrieveSingleton];
            NSInteger timerValue=  [appcontrol.COMM_SYNC_INTERVAL integerValue];
            NSLog(@"timer for communication sync %ld",timerValue);

        COMM_BS_Timer = [NSTimer timerWithTimeInterval:timerValue
                                                 target:self
                                               selector:@selector(postCOMMDataToServer)
                                               userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:COMM_BS_Timer forMode:NSRunLoopCommonModes];
        }
        
    }
}
// the function specified in the same class where we defined the addObserver
- (void)disableCOMMBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:KDISABLE_COMM_BS_NOTIFICATIONNameStr])
    {
        if (COMM_BS_Timer) {
            [COMM_BS_Timer invalidate];
            COMM_BS_Timer = nil;
        }
    }
}

-(void)postCOMMDataToServer
{
    if([currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_None] && [SWDefaults isCommunicationBackgroundSyncRequired])
    {
        [[[SalesWorxCommunicationMessagesSyncManager alloc]init]sendMessagesToserver];

    }
    else
    {
        // NSLog(@"back ground sync not enabled");
    }
    
}

#pragma mark Background Route Reschedule method
// the function specified in the same class where we defined the addObserver
- (void)enableRouteBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:KENABLE_ROUTE_BS_NOTIFICATIONNameStr])
    {
        [self postRouteDataToServer];
    }
}


-(void)postRouteDataToServer
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus != NotReachable)
    {
        NSString *lastSynctimeStr;
        
        if([SWDefaults lastRouteRescheduleBackGroundSyncDate]==nil) {
            lastSynctimeStr = [SWDefaults lastFullSyncDate];
        }
        else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastRouteRescheduleBackGroundSyncDate]]] == NSOrderedAscending) {
            lastSynctimeStr = [SWDefaults lastRouteRescheduleBackGroundSyncDate];
        } else {
            lastSynctimeStr = [SWDefaults lastFullSyncDate];
        }
        
        NSString *currentSyncStarttimeStr = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        NSMutableArray *routesarray = [[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetRescheduleRoutes, lastSynctimeStr,currentSyncStarttimeStr]]];
        
        
        NSLog(@"postRouteDataToServer in app delegate called");
        if(routesarray.count > 0 && ![NSString isEmpty:[SWDefaults username]])
        {
            SalesWorxBackgroundSyncManager *salesWorxBackgroundSyncManager=[[SalesWorxBackgroundSyncManager alloc]init];
            [salesWorxBackgroundSyncManager postRouteDataToserver:routesarray];
        }
    }
}

#pragma mark Background Route Reschedule Cancel method
// the function specified in the same class where we defined the addObserver
- (void)enableRouteCancelBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:KENABLE_ROUTE_CANCEL_BS_NOTIFICATIONNameStr])
    {
        [self postRouteCancelDataToServer];
    }
}

-(void)postRouteCancelDataToServer
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus != NotReachable)
    {
        NSString *lastSynctimeStr;
        if([SWDefaults lastRouteRescheduleCancelBackGroundSyncDate]==nil) {
            lastSynctimeStr = [SWDefaults lastFullSyncDate];
        }
        else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastRouteRescheduleCancelBackGroundSyncDate]]] == NSOrderedAscending) {
            lastSynctimeStr = [SWDefaults lastRouteRescheduleCancelBackGroundSyncDate];
        } else {
            lastSynctimeStr = [SWDefaults lastFullSyncDate];
        }
        
        NSString *currentSyncStarttimeStr=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        
        NSMutableArray *routesarray = [[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetCancelRescheduleRoutes, lastSynctimeStr,currentSyncStarttimeStr]]];
        
        NSLog(@"postRouteCancelDataToServer in app delegate called");
        if(routesarray.count > 0 && ![NSString isEmpty:[SWDefaults username]])
        {
            SalesWorxBackgroundSyncManager *salesWorxBackgroundSyncManager=[[SalesWorxBackgroundSyncManager alloc]init];
            [salesWorxBackgroundSyncManager postRouteCancelDataToserver:routesarray];
        }
    }
}

#pragma mark Background Product stock method

- (void)enableProdStkBackGroundSync:(NSNotification *)notification {
    
    if([notification.name isEqualToString:kENABLE_PRODSTK_NOTIFICATIONNameStr])
    {
        if (prodStkTimer==nil) {
            
            NSInteger stockSycnTimer=[[[AppControl retrieveSingleton]FS_STOCK_SYNC_TIMER] integerValue];
            
            //converting to minutes
            stockSycnTimer=stockSycnTimer*60;
            
            
            prodStkTimer = [NSTimer timerWithTimeInterval:stockSycnTimer
                                                   target:self
                                                 selector:@selector(fetchUpdatedProductStock)
                                                 userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:prodStkTimer forMode:NSRunLoopCommonModes];
        }
        
    }
}
- (void)disableProdStkBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:kENABLE_PRODSTK_NOTIFICATIONNameStr])
    {
        if (prodStkTimer) {
            [prodStkTimer invalidate];
            prodStkTimer = nil;
        }
    }
}

-(void)fetchUpdatedProductStock
{
    if ([SWDefaults isproductStockSyncRequired] && [currentExceutingDataDownloadProcess isEqualToString:kDataDownloadProcess_NONE]) {
        
        [[[SalesWorxProductDataSyncManager alloc] init] prepareRequestForFetchProductData];
        
    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    //NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    self.currentLocation = newLocation;
    currentLocationCapturedTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
}
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    // NSLog(@"location from app delegate %@", [locations lastObject]);
//    
//    self.currentLocation = [locations lastObject];
//    
//}



- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

    Singleton *single= [Singleton retrieveSingleton];
    if (single.databaseIsOpen<2)
    {
        single.databaseIsOpen++;
        mach_port_t host_port;
        mach_msg_type_number_t host_size;
        vm_size_t pagesize;
        
        host_port = mach_host_self();
        host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
        host_page_size(host_port, &pagesize);
        
        vm_statistics_data_t vm_stat;
        
        if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
            NSLog(@"Failed to fetch vm statistics");
        
        /* Stats in bytes */
        natural_t mem_free = vm_stat.free_count * pagesize;
        natural_t mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * pagesize;
        natural_t mem_total = mem_used + mem_free;
        
        //size_t size = mem_free - 2048;
        //void *allocation = malloc(mem_free - 10240);
        //bzero(allocation, size);
        //free(allocation);
        
        int iUsed = round(mem_used/1048576);
        int iFree = round(mem_free/1048576);
        int iTotal = round(mem_total/1048576);
        NSLog(@"applicationDidReceiveMemoryWarning used: %d free: %d total: %d", iUsed, iFree, iTotal);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MEMORY WARNING!!!", nil) message:[NSString stringWithFormat:@" Used Memory : %dmb \nFree Memory : %dmb \n Please save your work then kill and relaunch application.",iUsed,iFree] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
       // [alert show];

    
    }

    
    
}


- (void)licenceDone {
    
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    //    NSString *documentsDir = [paths objectAtIndex:0];
    //    NSString *strDatabaseInfo= [documentsDir stringByAppendingPathComponent:databaseName];
    //    NSFileManager *fileManager = [NSFileManager defaultManager];
    licenseVC.target=nil;
    licenseVC.action=nil;
    licenseVC=nil;

    if ([[SWDefaults isActivationDone] isEqualToString:@"NO"])
    {
        
        [self setActivationManager:[[NewActivationViewController alloc] initWithNoDB] ];
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:self.activationManager] ;
        [activationManager setTarget:self];
        [activationManager setAction:@selector(activationDone)];
        [self.window setRootViewController:self.mainNavigationController];
    }
    else
    {
        
//        if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
//            
//            DashboardAlSeerViewController *dashboardViewController = [[DashboardAlSeerViewController alloc] init];
//            self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
//        }
//        
//        else
//        {
//            SWDashboardViewController *dashboardViewController = [[SWDashboardViewController alloc] init];
//            self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
//        }

        
        
        
        SalesWorxFieldSalesDashboardViewController *dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
        
       // [[NSNotificationCenter defaultCenter] addObserver:dashboardViewController selector:@selector(DownloadingDidComplete:) name:@"DownloadingDidComplete" object:nil];
        
        MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
        _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
        
        SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
        
        [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
    }
}
- (void)activationDone
{
    self.activationManager.target=nil;
    self.activationManager.action=nil;
    
    self.activationManager=nil;
    
    if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
        
//        DashboardAlSeerViewController *dashboardViewController = [[DashboardAlSeerViewController alloc] init];
//        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
        
        SalesWorxFieldSalesDashboardViewController *dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    }
    
    else
    {
        SalesWorxFieldSalesDashboardViewController *dashboardViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init];
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    }

  //add a flag for med rep if new licensing activation is done
    
    
    if ([SWDefaults isMedRep]==YES) {
        
        MedRepMenuViewController* medRepMenu=[[MedRepMenuViewController alloc]init];
        _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:medRepMenu] ;

    }
    
    else
    {
    
    

    MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
    _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
    }

    SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
    
    [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
}

- (void)reminderToDo :(NSString *)customerID
{
    NSMutableArray *reminderArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM 'TBL_Todo' where Remind_At is not null and Ship_Customer_ID = '%@' and datetime(Remind_At) >= datetime('now', 'localtime') order by Remind_At limit 1",customerID]];
    
    if (reminderArray.count > 0) {
        
        NSString *currentDateStr = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        NSString *reminderDate = [[reminderArray objectAtIndex:0] valueForKey:@"Remind_At"];
        
        if ([currentDateStr isEqualToString:reminderDate]) {
            
            NSString *title = [[reminderArray objectAtIndex:0] valueForKey:@"Title"];
            NSString *message = [NSString stringWithFormat:@"To Do %@ task scheduled for now",title];
            
            [SWDefaults showAlertAfterHidingKeyBoard:@"To Do reminder" andMessage:message withController:_window.rootViewController];
        }
    }
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRouteDataFromBackground" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GetSyncStateFromBackground" object:nil];
    NSLog(@"application entering foreground");
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus != NotReachable)
    {
        [self postRouteDataToServer];
        [self postRouteCancelDataToServer];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    
    MBProgressHUD * foregroundActiveHud = [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    foregroundActiveHud.mode = MBProgressHUDModeIndeterminate;
    foregroundActiveHud.labelText = @"Refreshing application. Please wait..";
    
    NSDictionary * notificationsDict = [MedRepQueries fetchTasksDueForTodayForNotifications];
    NSString* titleString = [SWDefaults getValidStringValue:[notificationsDict valueForKey:@"Title"]];
    if (![NSString isEmpty:titleString])
    {
        [SalesWorxNotificationManager createNotificationRequest:notificationsDict];
    }
    
}
- (void)applicationWillTerminate:(UIApplication *)application{
    NSLog(@"application Terminating");
}
-(void)applicationDidEnterBackground:(UIApplication *)application{
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"application entered background  because device locked");
}
- (void)applicationDidBecomeActive:(UIApplication *)application{
    NSLog(@"app did become active");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDidBecomeActive" object:nil];
    [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
}


-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
//    UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"Notification"    message:@"This local notification"
//                                                               delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
   // [notificationAlert show];
    
     NSLog(@"didReceiveLocalNotification called");
    
    if ([[UIApplication sharedApplication] applicationState]==UIApplicationStateActive) {
        [AGPushNoteView showWithNotificationMessage:[notification.userInfo valueForKey:@"Title"]];
        
    }
    NSString* alertMessage=[[notification.userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    
    NSMutableArray * pushNotificationsContentArray=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];

    
    

    if ([NSString isEmpty:alertMessage]==NO) {
        
        FSRMessagedetails * currentMessage=[[FSRMessagedetails alloc]init];
        currentMessage.Message_Title=kPushNotificationTitle;
        currentMessage.Message_Content=alertMessage;
        currentMessage.Message_date=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];

        currentMessage.isFromPushNotifications=YES;
        currentMessage.Message_Read=@"N";

        //split notification title and image url
        
        NSString *odString=[notification.userInfo valueForKey:@"od"];
        
        if ([NSString isEmpty:odString]==NO) {
            NSString *imageUrlString;
            NSString *messageContent;
            NSRange rng = [[notification.userInfo valueForKey:@"od"] rangeOfString:@" "];
            if(rng.location != NSNotFound){
                imageUrlString = [[notification.userInfo valueForKey:@"od"] substringToIndex:rng.location];
                messageContent = [[notification.userInfo valueForKey:@"od"] substringFromIndex:rng.location + 1];
            }
            NSArray *listItems = [[notification.userInfo valueForKey:@"od"] componentsSeparatedByString:@" "];
            if (listItems.count>0) {
           
            NSString* imageURL= imageUrlString;
            NSString* descriptionString =messageContent;
                NSString * imageID=[imageUrlString stringByAppendingString:[NSString createGuid]];
                
        //if od key is not available then alert message should be displayed as content
                currentMessage.Message_Title=alertMessage;
                currentMessage.Message_Content=descriptionString;
                currentMessage.imageID=imageID;
                currentMessage.Push_Notification_ID=[NSString createGuid];

                NSLog(@"push notification id is %@", currentMessage.Push_Notification_ID);
                
                //append a guid to image url to download and display image
                
                
                currentMessage.imageURL=imageURL;
                
            }

        }
        
        
        if (pushNotificationsContentArray.count>0) {
            if ([pushNotificationsContentArray containsObject:currentMessage]) {
                
            }
            else
            {
                
                [pushNotificationsContentArray addObject:currentMessage];
            }
        }
        else
        {
            pushNotificationsContentArray =[[NSMutableArray alloc]init];
            
            if ([pushNotificationsContentArray containsObject:currentMessage]) {
                
            }
            else
            {
            
            [pushNotificationsContentArray addObject:currentMessage];
            }
        }
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushNotificationsContentArray];
        [defaults setObject:data forKey:@"PUSHED_MESSAGES"];
        [defaults synchronize];
    
    }
    
    NSArray *array= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];
    

    NSLog(@"notification content array  after archieving is %@",array);
    
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}
//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //NSLog(@"%@",userInfo);
    
    NSLog(@"did receive remote notification called");
    
    
    [self incrementOneBadge];
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[userInfo valueForKey:@"badge"] integerValue]];
//    NSString *intents = [userInfo valueForKey:@"section"];
//    NSLog(@"Intents %@",intents);
    
    NSLog(@"push notification content in did receive notification is %@",userInfo);
    //json structure  {"aps":{"alert":"test","badge":1,"sound":"default"}}
    
    NSLog(@"push notification received with info %@", userInfo);
    
    
    
    
    
    NSString* alertMessage=[[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    
    NSMutableArray * pushNotificationsContentArray=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];
    
    
    
    
    if ([NSString isEmpty:alertMessage]==NO) {
        
        FSRMessagedetails * currentMessage=[[FSRMessagedetails alloc]init];
        currentMessage.Message_Title=kPushNotificationTitle;
        currentMessage.Message_Content=alertMessage;
        currentMessage.Message_date=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        currentMessage.isFromPushNotifications=YES;
        currentMessage.Message_Read=@"N";
        
        //split notification title and image url
        
        NSString *odString=[userInfo valueForKey:@"od"];
        
        
        
        
        NSData   *asciiData   = [[odString stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *asciiString = [[NSString alloc] initWithData:asciiData
                                                      encoding:NSASCIIStringEncoding];
        
        NSLog(@"HAS UNICODE  :%@", odString);
        
        NSLog(@"UNICODE AFTER:%@", asciiString);
        
        NSMutableArray * testArray=[[NSMutableArray alloc]initWithObjects:odString,asciiString, nil];
        NSLog(@"open data array with ascii char %@ \n", [testArray description]);

        
        BOOL odHasAsciiString=[odString canBeConvertedToEncoding:NSASCIIStringEncoding];
        if (odHasAsciiString==YES) {
            
            // ASCII to NSString
            int asciiCode = 255;
            NSString *convertedString = [NSString stringWithFormat:@"%c", asciiCode]; // A
            
            NSArray *convertedArray = [[userInfo valueForKey:@"od"] componentsSeparatedByString:convertedString];
            NSLog(@"OD Array %@", convertedArray);

        }
        
        
        if ([NSString isEmpty:odString]==NO) {
            NSString *imageUrlString;
            NSString *messageContent;
            NSRange rng = [[userInfo valueForKey:@"od"] rangeOfString:@" "];
            if(rng.location != NSNotFound){
                imageUrlString = [[userInfo valueForKey:@"od"] substringToIndex:rng.location];
                messageContent = [[userInfo valueForKey:@"od"] substringFromIndex:rng.location + 1];
            }
            
            NSArray *listItems = [[userInfo valueForKey:@"od"] componentsSeparatedByString:@" "];
            if (listItems.count>0) {
                
                NSString* imageURL= imageUrlString;
                NSString* descriptionString =messageContent;
                NSString * imageID=[imageUrlString stringByAppendingString:[NSString createGuid]];
                
                //if od key is not available then alert message should be displayed as content
                currentMessage.Message_Title=alertMessage;
                currentMessage.Message_Content=descriptionString;
                currentMessage.imageID=imageID;
                currentMessage.Push_Notification_ID=[NSString createGuid];
                
                NSLog(@"push notification id is %@", currentMessage.Push_Notification_ID);
                
                //append a guid to image url to download and display image
                
                
                currentMessage.imageURL=imageURL;
                
            }
            
        }
        
        
        if (pushNotificationsContentArray.count>0) {
            if ([pushNotificationsContentArray containsObject:currentMessage]) {
                
            }
            else
            {
            [pushNotificationsContentArray addObject:currentMessage];
            }
        }
        else
        {
            pushNotificationsContentArray =[[NSMutableArray alloc]init];
            [pushNotificationsContentArray addObject:currentMessage];
        }
        
        NSLog(@"push notification content array being saved in defaults %@", pushNotificationsContentArray);
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushNotificationsContentArray];
        [defaults setObject:data forKey:@"PUSHED_MESSAGES"];
        [defaults synchronize];
        
        NSArray *array= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];
        NSLog(@"notification content array  after archieving in did receive notification is %@",array);
        
    }
        if ([[UIApplication sharedApplication] applicationState]==UIApplicationStateActive) {
            if ([alertMessage isEqualToString:@"$APP$UPDATE$"]) {
                NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
                NSString *avid = [infoDict stringForKey:@"CFBundleVersion"];
                if([[userInfo valueForKey:@"v"] compare: avid] == NSOrderedDescending){
                    [AGPushNoteView showWithNotificationMessage:[userInfo valueForKey:@"m"]];
                    [AGPushNoteView setMessageAction:^(NSString *message) {
                        if([[userInfo valueForKey:@"v"] compare:avid] == NSOrderedDescending){
                            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[userInfo valueForKey:@"u"]] options:@{} completionHandler:nil];
                        }
                        else{
                            return;
                        }
                    }];
                }
            }else{
                [AGPushNoteView showWithNotificationMessage:alertMessage];
            }
        }
    
   // [[NSNotificationCenter defaultCenter]postNotificationName:@"PUSHNOTIFICATIONARRIVED" object:self];
    
    NSLog(@"log before post notification");
    

    NSLog(@"log after post notification");

    }
    
    

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"did recieve remote notification fetch completion handler called");
    
   
    [self fetchPushNotificationContent:userInfo];
    
    
    completionHandler(UIBackgroundFetchResultNewData);
}

-(void)fetchPushNotificationContent:(NSDictionary*)launchOptions
{
    //NSLog(@"%@",userInfo);
    
    NSLog(@"fetching push notifications data");
    
    
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[userInfo valueForKey:@"badge"] integerValue]];
    //    NSString *intents = [userInfo valueForKey:@"section"];
    //    NSLog(@"Intents %@",intents);
    
    
    NSDictionary* userInfo = launchOptions;

    NSLog(@"push notification content is %@",userInfo);
    //json structure  {"aps":{"alert":"test","badge":1,"sound":"default"}}
    
    NSLog(@"push notification received with info %@", userInfo);
    
    
    
    NSString *odString=[userInfo valueForKey:@"od"];
    
    
    
    NSData   *asciiData   = [[odString stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""] dataUsingEncoding:NSASCIIStringEncoding];
    NSString *asciiString = [[NSString alloc] initWithData:asciiData
                                                  encoding:NSASCIIStringEncoding];
    
    NSLog(@"HAS UNICODE  :%@", odString);
    
    NSLog(@"UNICODE AFTER:%@", asciiString);
    
    NSMutableArray * testArray=[[NSMutableArray alloc]initWithObjects:odString,asciiString, nil];
    NSLog(@"open data array with ascii char %@ \n", [testArray description]);
    NSString *convertedString;
    
    BOOL odHasAsciiString=[odString canBeConvertedToEncoding:NSASCIIStringEncoding];
    if (odHasAsciiString==YES) {
        
        // ASCII to NSString
        int asciiCode = 255;
        convertedString = [NSString stringWithFormat:@"%c", asciiCode]; // A
        
        NSArray *convertedArray = [[userInfo valueForKey:@"od"] componentsSeparatedByString:convertedString];
        NSLog(@"OD Array %@", convertedArray);
        
    }
    

    
    
    NSString* alertMessage=[[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    if ([alertMessage isEqualToString:@"$SYNC$VISITS$"]) {
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        if(networkStatus != NotReachable)
        {
            NSLog(@"postRouteCancelDataToServer in app delegate called");
            if(![NSString isEmpty:[SWDefaults username]])
            {
                SalesWorxBackgroundSyncManager *salesWorxBackgroundSyncManager=[[SalesWorxBackgroundSyncManager alloc]init];
                [salesWorxBackgroundSyncManager syncVisitDataToserver];
                [salesWorxBackgroundSyncManager executeVisitChangeLogDataToserver];
            }
        }
        
    }else  if ([alertMessage isEqualToString:@"$APP$UPDATE$"]) {
        NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
        NSString *avid = [infoDict stringForKey:@"CFBundleVersion"];
        if([[userInfo valueForKey:@"v"] compare: avid] == NSOrderedDescending){
            [AGPushNoteView showWithNotificationMessage:[userInfo valueForKey:@"m"]];
            [AGPushNoteView setMessageAction:^(NSString *message) {
                if([[userInfo valueForKey:@"v"] compare:avid] == NSOrderedDescending){
                    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[userInfo valueForKey:@"u"]] options:@{} completionHandler:nil];
                }
                else{
                    return;
                }
            }];
        }
    }else if ([alertMessage isEqualToString:@"$USER$SYNC$STATE"]){
        NSString *odString = [userInfo valueForKey:@"od"];
        NSArray *listItems = [odString componentsSeparatedByString:@","];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        for( NSString *obj in listItems){
            NSArray *subList = [obj componentsSeparatedByString:@":"];
            NSString *key = [subList objectAtIndex:0];
             NSString *value = [subList objectAtIndex:1];
            [dict setValue:value forKey:key];
        }
        
        AppControl *appControl=[AppControl retrieveSingleton];
        NSString *mystring =  [[dict valueForKey:@"{\"m\""] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        [AGPushNoteView showWithNotificationMessage:mystring];
        appControl.ENABLE_BACKGROUND_SYNC = [[dict valueForKey:@"\"ENABLE_BACKGROUND_SYNC\""]stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        appControl.USER_ENABLED_BACKGROUND_SYNC = [[[dict valueForKey:@"\"USER_ENABLED_BACKGROUND_SYNC\""]stringByReplacingOccurrencesOfString:@"\"" withString:@""]stringByReplacingOccurrencesOfString:@"}" withString:@""];
        [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:[[[dict valueForKey:@"\"USER_ENABLED_BACKGROUND_SYNC\""]stringByReplacingOccurrencesOfString:@"\"" withString:@""]stringByReplacingOccurrencesOfString:@"}" withString:@""]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GetSyncStateFromBackground" object:nil];

    }
        else {
        [self incrementOneBadge];
        
        NSMutableArray * pushNotificationsContentArray=[[NSMutableArray alloc]init];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];
        
        
        
        
        if ([NSString isEmpty:alertMessage]==NO) {
            
            FSRMessagedetails * currentMessage=[[FSRMessagedetails alloc]init];
            currentMessage.Message_Title=kPushNotificationTitle;
            currentMessage.Message_Content=alertMessage;
            currentMessage.Message_date=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            currentMessage.isFromPushNotifications=YES;
            currentMessage.Message_Read=@"N";
            
            //split notification title and image url
            
            NSString *odString=[userInfo valueForKey:@"od"];
            
            if ([NSString isEmpty:odString]==NO) {
                NSString *imageUrlString;
                NSString *messageContent;
                NSRange rng = [[userInfo valueForKey:@"od"] rangeOfString:@" "];
                if(rng.location != NSNotFound){
                    imageUrlString = [[userInfo valueForKey:@"od"] substringToIndex:rng.location];
                    messageContent = [[userInfo valueForKey:@"od"] substringFromIndex:rng.location + 1];
                }
                NSArray *listItems = [[userInfo valueForKey:@"od"] componentsSeparatedByString:convertedString];
                if (listItems.count>0) {
                    
                    NSString* imageURL= imageUrlString;
                    NSString* descriptionString =messageContent;
                    NSString * imageID=[imageUrlString stringByAppendingString:[NSString createGuid]];
                    
                    //if od key is not available then alert message should be displayed as content
                    currentMessage.Message_Title=alertMessage;
                    currentMessage.Message_Content=descriptionString;
                    currentMessage.imageID=imageID;
                    currentMessage.Push_Notification_ID=[NSString createGuid];
                    
                    NSLog(@"push notification id is %@", currentMessage.Push_Notification_ID);
                    
                    //append a guid to image url to download and display image
                    
                    
                    currentMessage.imageURL=imageURL;
                    
                }
                
            }
            
            
            if (pushNotificationsContentArray.count>0) {
                if ([pushNotificationsContentArray containsObject:currentMessage]) {
                    
                }
                else
                {
                    [pushNotificationsContentArray addObject:currentMessage];
                }
            }
            else
            {
                pushNotificationsContentArray =[[NSMutableArray alloc]init];
                if ([pushNotificationsContentArray containsObject:currentMessage]) {
                    
                }
                else
                {
                    [pushNotificationsContentArray addObject:currentMessage];
                }
            }
            
            NSLog(@"push notification content array being saved in defaults %@", pushNotificationsContentArray);
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushNotificationsContentArray];
            [defaults setObject:data forKey:@"PUSHED_MESSAGES"];
            [defaults synchronize];
            
            NSArray *array= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];
            NSLog(@"notification content array  after archieving is %@",array);
            
        }
        
        if ([[UIApplication sharedApplication] applicationState]==UIApplicationStateActive) {
            if ([alertMessage isEqualToString:@"$APP$UPDATE$"]) {
                NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
                NSString *avid = [infoDict stringForKey:@"CFBundleVersion"];
                if([[userInfo valueForKey:@"v"] compare: avid] == NSOrderedDescending){
                    [AGPushNoteView showWithNotificationMessage:[userInfo valueForKey:@"m"]];
                    [AGPushNoteView setMessageAction:^(NSString *message) {
                        if([[userInfo valueForKey:@"v"] compare:avid] == NSOrderedDescending){
                            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[userInfo valueForKey:@"u"]] options:@{} completionHandler:nil];
                        }
                        else{
                            return;
                        }
                    }];
                }
            }else{
                [AGPushNoteView showWithNotificationMessage:alertMessage];
            }
        }
        
        NSLog(@"before calling notification in completion handler");
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"PUSHNOTIFICATIONARRIVED" object:nil userInfo:userInfo];
        
        NSLog(@"after calling notification in completion handler");
    }
}
    

    


-(void) incrementOneBadge{
    NSInteger numberOfBadges = [UIApplication sharedApplication].applicationIconBadgeNumber;
    numberOfBadges +=1;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:numberOfBadges];
}

-(void) decrementOneBdge{
    NSInteger numberOfBadges = [UIApplication sharedApplication].applicationIconBadgeNumber;
    numberOfBadges -=1;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:numberOfBadges];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}


-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark background download methods

-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler{
    
    self.backgroundTransferCompletionHandler = completionHandler;
    
}


-(NSString *)getUTCFormateDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"MMddHHmmss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}


- (void) receiveDownloadNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"StartDownLoadNotification"])    {
         NSLog (@"Successfully received the StartDownLoadNotification notification!");
        //  dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [[[BackgroundDocumentsDownload alloc]init]downloadDocumentsInBackground];
    }
    
}



-(NSMutableArray *) fetchMediaFilesFromDocuments {
    
    NSMutableArray* allMediaFilesArray =[[NSMutableArray alloc]init];
    
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files WHERE Download_Flag='Y' AND Is_Deleted='N'"]];
    
    //ppt file got downloaded
    
    // NSLog(@"downloaded media files %@", [array description]);
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        customer.Download_Media_Type=kDefaultDownloadMediaType;

        [allMediaFilesArray addObject:customer];
        
        
        //POSM Images
        NSMutableArray * posmArray = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"select IFNULL(Custom_Attribute_1,'')AS Custom_Attribute_1 from TBL_App_Codes where code_type = 'POSM_TITLE'"]];
        if (posmArray.count>0) {
            for (NSMutableDictionary* currentDict in posmArray) {
                MediaFile *posmMediaFile = [[MediaFile alloc]init];
                posmMediaFile.Filename=[NSString getValidStringValue:[currentDict valueForKey:@"Custom_Attribute_1"]];
                posmMediaFile.Download_Media_Type=kMerchandisingPOSMDownloadMediaType;
                [allMediaFilesArray addObject:posmMediaFile];
            }
        }
        
        //org logo images
        NSMutableArray * orgLogosArray = [[NSMutableArray alloc] init];
        orgLogosArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Value_1 AS FileName from TBL_Custom_Info where Value_2 IS NOT NULL AND Info_Type = 'ORG_LOGO'"];
        
        for (NSMutableDictionary* orgDict in orgLogosArray) {
            MediaFile *posmMediaFile = [[MediaFile alloc]init];
            posmMediaFile.Filename=[NSString getValidStringValue:[orgDict valueForKey:@"FileName"]];
            posmMediaFile.Download_Media_Type=kOrgLogoDownloadMediaType;
            [allMediaFilesArray addObject:posmMediaFile];
        }
        

    }
    return allMediaFilesArray;
}



-(void)startMediaDownloadProcess
{
    
    if (self.isBackgroundProcessRunning==NO && self.isNewSync==YES) {
        
  
    
    SynViewController * synVc=[[SynViewController alloc]init];
    
        
        NSMutableArray *arrayOfMediaFiles=[NSMutableArray alloc];
        
    
    arrayOfMediaFiles = [self fetchMediaFilesFromDocuments];
    
        
        self.currentDownloadingDocumentsArray=arrayOfMediaFiles;
    
    NSLog(@"array of media files %@", [arrayOfMediaFiles description]);
    

    
    
    self.currentDownloadingDocumentsArray=arrayOfMediaFiles;
    
        if (arrayOfMediaFiles.count>0) {
            
        }
    
    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%lu", (unsigned long)arrayOfMediaFiles.count] forKey:@"getTotalCount"];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"StartDownLoadNotification"
         object:self];
    }];
    }
    
   // [[[BackgroundDocumentsDownload alloc]init]DownloadOrgLogoImages];
   // [[[BackgroundDocumentsDownload alloc]init]downloadMerchandisingPOSMImages];

}


#pragma mark Push Notification methods



-(void)registerforPushNotificationwithDeviceToken:(NSString*)deviceID
{
    NSString* tempUserName=[[SWDatabaseManager retrieveManager] fetchuserNamefromDB];
    NSString* userNameFromDB=[SWDefaults getValidStringValue:tempUserName];
    
    
    
    if ([NSString isEmpty:userNameFromDB]==NO ) {
        
        NSString *deviceCode = @"IPAD";
        
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
        if (deviceToken.length==0) {
            deviceToken = @"";
        }
        
        //adding customer id with device id to distinguish customer
        
        NSString* licenseCustomerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
        
        NSString *deviceUserId = [[NSString stringWithFormat:@"%@_",licenseCustomerID] stringByAppendingString:userNameFromDB];
        NSLog(@"device user id being saved is %@", deviceUserId);
        
        
        NSString *loggedTime = [self getUTCFormateDateforNotification:[NSDate date]];
        NSString *uKey =@"7c4985aa167e77eff6ca99897c3633d327ee1126a77b72d75811aec2497ecad9";
        
        
        NSString *hashParam = [NSString stringWithFormat:@"DeviceCode=%@&DeviceID=%@&DeviceToken=%@&DeviceUserID=%@&LoggedAt=%@&UKey=%@",deviceCode,deviceID,deviceToken,deviceUserId,loggedTime,uKey];
        hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *result =[self hmacWithKey:@"b2b40be65083118d13089cb38c9c1468a37d5152fb72906d4e21fbdd0e8885d1" andData:hashParam];
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        
        if(networkStatus != NotReachable)
        {
            NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys: deviceCode ,@"DeviceCode", deviceID , @"DeviceID", deviceToken , @"DeviceToken", deviceUserId , @"DeviceUserID", loggedTime , @"LoggedAt", uKey, @"UKey", result, @"CData", nil];
            
            NSLog(@"registerforPushNotificationwithDeviceToken Parameter is %@", parameters);
            
            NSString *serverAPI=@"http://swx-db.cloudapp.net:10101/PNS/Data/RegisterDevice";
            
            AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:serverAPI]];
            [client setDefaultHeader:@"contentType" value:@"application/json; charset=utf-8"];
            client.parameterEncoding = AFJSONParameterEncoding;
            
            NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:@"" parameters:parameters];
            
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request,NSHTTPURLResponse *response, id JSON)
                                                 {
                                                     NSLog(@"register device JSON is %@", JSON);
                                                     
                                                     
                                                     [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceToken"]forKey:@"deviceToken"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                     [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceID"]forKey:@"savedDeviceID"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                 }
                                                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse*response, NSError *error, id JSON)
                                                 {
                                                     NSLog(@"Error%@",[error localizedDescription]);
                                                 }];
            [operation start];
        }
        else
        {
            
        }
        
    }else{
        [[NSUserDefaults standardUserDefaults]setValue:deviceID forKey:@"ToSaveDeviceID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
 
}
+ (NSString *)stringFromDeviceToken:(NSData *)deviceToken {
    NSUInteger length = deviceToken.length;
    if (length == 0) {
        return nil;
    }
    const unsigned char *buffer = deviceToken.bytes;
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(length * 2)];
    for (int i = 0; i < length; ++i) {
        [hexString appendFormat:@"%02x", buffer[i]];
    }
    return [hexString copy];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    NSString *deviceID = [SWAppDelegate stringFromDeviceToken:deviceToken];
    [[NSUserDefaults standardUserDefaults]setValue:deviceID forKey:@"deviceID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString* tempUserName=[[SWDatabaseManager retrieveManager] fetchuserNamefromDB];
    NSString* userNameFromDB=[SWDefaults getValidStringValue:tempUserName];
    
    
    
    if ([NSString isEmpty:userNameFromDB]==NO ) {
        
        
       
            NSString *deviceCode = @"IPAD";
            
            NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
            if (deviceToken.length==0) {
                deviceToken = @"";
            }
        
        //adding customer id with device id to distinguish customer
        
        NSString* licenseCustomerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
        
        NSString *deviceUserId = [[NSString stringWithFormat:@"%@_",licenseCustomerID] stringByAppendingString:userNameFromDB];
        NSLog(@"device user id being saved is %@", deviceUserId);
        
            NSString *loggedTime = [self getUTCFormateDateforNotification:[NSDate date]];
            NSString *uKey =@"7c4985aa167e77eff6ca99897c3633d327ee1126a77b72d75811aec2497ecad9";
            
            
            NSString *hashParam = [NSString stringWithFormat:@"DeviceCode=%@&DeviceID=%@&DeviceToken=%@&DeviceUserID=%@&LoggedAt=%@&UKey=%@",deviceCode,deviceID,deviceToken,deviceUserId,loggedTime,uKey];
            hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            NSString *result =[self hmacWithKey:@"b2b40be65083118d13089cb38c9c1468a37d5152fb72906d4e21fbdd0e8885d1" andData:hashParam];
            
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            
            NetworkStatus networkStatus = [reachability currentReachabilityStatus];
            
            if(networkStatus != NotReachable)
            {
                NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys: deviceCode ,@"DeviceCode", deviceID , @"DeviceID", deviceToken , @"DeviceToken", deviceUserId , @"DeviceUserID", loggedTime , @"LoggedAt", uKey, @"UKey", result, @"CData", nil];
                
                NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken Parameter is %@", parameters);
                
                NSString *serverAPI=@"http://swx-db.cloudapp.net:10101/PNS/Data/RegisterDevice";
                
                AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:serverAPI]];
                [client setDefaultHeader:@"contentType" value:@"application/json; charset=utf-8"];
                client.parameterEncoding = AFJSONParameterEncoding;
                
                NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:@"" parameters:parameters];
                
                AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request,NSHTTPURLResponse *response, id JSON)
                                                     {
                                                         
                                                         NSLog(@"Register device response is %@", JSON);
                                                         [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceToken"]forKey:@"deviceToken"];
                                                         [[NSUserDefaults standardUserDefaults] synchronize];
                                                         
                                                         [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceID"]forKey:@"savedDeviceID"];
                                                         [[NSUserDefaults standardUserDefaults] synchronize];
                                                         
                                                     }
                                                                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse*response, NSError *error, id JSON)
                                                     {
                                                         NSLog(@"Error%@",[error localizedDescription]);
                                                     }];
                [operation start];
            }
            else
            {
                
            }
        
    }else{
        [[NSUserDefaults standardUserDefaults]setValue:deviceID forKey:@"ToSaveDeviceID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(NSString *)getUTCFormateDateforNotification:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocaleq];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}

- (NSString *)hmacWithKey:(NSString *)key andData:(NSString *)data
{
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    //NSString *s = [self base64String:[[hash.description stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSString* s = [SWAppDelegate base64forData:hash];
    //NSString *s = [[hash.description stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    // NSString *s = [hash base64EncodedString];
    NSLog(@"%@",s);
    return s;
}

+ (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] = table[(value >> 18) & 0x3F];
        output[theIndex + 1] = table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]; }
- (NSString *)base64String:(NSString *)str
{
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}





//#import "CJSONSerializer.h"
/*
 
 Steps to analyze crash report from apple:
 
 Copy the release .app file which was pushed to the appstore, the .dSYM file that was created at the time of release and the crash report receive from APPLE into a FOLDER.
 
 OPEN terminal application and go to the folder created above (using CD command)
 
 atos -arch armv7 -o YOURAPP.app/YOURAPP MEMORY_LOCATION_OF_CRASH. The memory location should be the one at which the app crashed as per the report.
 
 Ex:  atos -arch armv7  -o 'app name.app'/'app name' 0x0003b508
 
 This would show you the exact line, method name which resulted in crash.
 
 Ex: [classname functionName:]; -510
 

 */
//6   Salesworx                     	0x000f4872 0x37000 + 776306

//#import "CJSONDeserializer.h"



#pragma mark device lock status

#pragma mark Beacon Delegate Methods

- (void)beaconManager:(KTKBeaconManager*)manager didChangeLocationAuthorizationStatus:(CLAuthorizationStatus)status;
{
    // ...
}

- (void)beaconManager:(KTKBeaconManager *)manager didStartMonitoringForRegion:(__kindof KTKBeaconRegion *)region {
    // Do something when monitoring for a particular
    // region is successfully initiated
    NSLog(@"started monitoring region");
    
    
    
    
}

- (void)beaconManager:(KTKBeaconManager *)manager monitoringDidFailForRegion:(__kindof KTKBeaconRegion *)region withError:(NSError *)error {
    // Handle monitoring failing to start for your region
    NSLog(@"failed to monitor region %@", error);
}

- (void)beaconManager:(KTKBeaconManager*)manager didEnterRegion:(__kindof KTKBeaconRegion*)region
{
    NSLog(@"Enter region %@", region);
    
    UIAlertView * test =[[UIAlertView alloc]initWithTitle:@"Beacon Alert" message:@"welcome home" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //[test show];
    
    [manager startRangingBeaconsInRegion:region];

    
}

- (void)beaconManager:(KTKBeaconManager*)manager didExitRegion:(__kindof KTKBeaconRegion*)region
{
    NSLog(@"Exit region %@", region);
    
    UIAlertView * test =[[UIAlertView alloc]initWithTitle:@"Beacon Alert" message:@"bye" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //[test show];
    [manager stopRangingBeaconsInRegion:region];

}

- (void)beaconManager:(KTKBeaconManager*)manager didRangeBeacons:(NSArray <CLBeacon *>*)beacons inRegion:(__kindof KTKBeaconRegion*)region
{
    
    //NSLog(@"detected beacon range %@",[beacons description]);
    beacons=[[NSMutableArray alloc]init]; /** Comment the line later*/

    if (beacons.count>0) {
        
    NSMutableArray * salesWorxBeaconObjectsArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<beacons.count; i++) {
        CLBeacon * currentBeacon = [[CLBeacon alloc]init];
        currentBeacon=[beacons objectAtIndex:i];
        SalesWorxBeacon * salesWorxBeacon =[[SalesWorxBeacon alloc]init];
        salesWorxBeacon.Beacon_UUID=[SWDefaults getValidStringValue:currentBeacon.proximityUUID.UUIDString];
        salesWorxBeacon.Beacon_Major=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentBeacon valueForKey:@"major"]]];
        salesWorxBeacon.Beacon_Minor=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentBeacon valueForKey:@"minor"]]];
        salesWorxBeacon.isVisitStarted=NO;
        salesWorxBeacon.proximity=[NSNumber numberWithDouble:currentBeacon.proximity];
        
        
        salesWorxBeacon.beaconDetectionTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        [salesWorxBeaconObjectsArray addObject:salesWorxBeacon];
    }
    
        
        
        if(salesWorxBeaconObjectsArray.count>0)
        {
            float xmax = -MAXFLOAT;
            float xmin = MAXFLOAT;
            for (NSNumber *num in [salesWorxBeaconObjectsArray  valueForKey:@"proximity"]) {
                float x = num.doubleValue;
                if (x < xmin) xmin = x;
                if (x > xmax) xmax = x;
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.proximity == %@", [NSNumber numberWithDouble:xmax]];
            
           // NSLog(@"matched beacon %@", [salesWorxBeaconObjectsArray filteredArrayUsingPredicate:predicate]);
            
            NSMutableArray * accurateBeaconArray=[[salesWorxBeaconObjectsArray filteredArrayUsingPredicate:predicate] mutableCopy];
            if (accurateBeaconArray.count>0) {
               
                self.currentBeacon=[accurateBeaconArray objectAtIndex:0];
            }
        }
    


    
}
}

-(void)fireBeaconNotification
{
    
}

-(void)pushToCustomersScreen:(NSMutableArray*)beaconsDaatArray
{

//    SWCustomersViewController * customersVC=[[SWCustomersViewController alloc]init];
//    
//    UINavigationController * tempNavController =[[UINavigationController alloc]initWithRootViewController:customersVC];
//    
//    [self.mainNavigationController pushViewController:customersVC animated:YES];
    
    
}


@end
