//
//  SalesWorxBlockedCustomersTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxBlockedCustomersTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblCode;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblName;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblContact;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPhone;

@property(nonatomic) BOOL isHeader;

@end
