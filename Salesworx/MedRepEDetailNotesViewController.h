//
//  MedRepEDetailNotesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepCustomClass.h"

@protocol EDetailingNotesDelegate <NSObject>

-(void)notesDidClose:(NSMutableArray*)updatedNotes;

@end

@interface MedRepEDetailNotesViewController : SWViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>

{
    id delegate;
    
    VisitNotes* notes;
    
    
}

@property (strong, nonatomic) IBOutlet UITableView *notesTblView;
@property(nonatomic) id delegate;
@property(strong,nonatomic)NSMutableArray* notesArray;

@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;

@property(nonatomic) BOOL isVisitCompleted;

@property(nonatomic) BOOL dashBoardViewing;

@property(nonatomic) BOOL isInMultiCalls;

@property(strong,nonatomic) VisitDetails * visitDetails;

@property(strong,nonatomic) UIPopoverController* notesPopOverController;

@end
