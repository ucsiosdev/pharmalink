//
//  SalesWorxBlockedCustomersViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"

@interface SalesWorxBlockedCustomersViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblBlockedCustomer;
@property(strong,nonatomic) NSMutableArray *arrBlockedCustomerData;


@end
