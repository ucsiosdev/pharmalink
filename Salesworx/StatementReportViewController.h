////
////  StatementReportViewController.h
////  Salesworx
////
////  Created by msaad on 6/13/13.
////  Copyright (c) 2013 msaad. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "SWPlatform.h"
//#import "CustomersListViewController.h"
//#import "CustomerPopOverViewController.h"
//
//@interface StatementReportViewController : SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
//{
//    GridView *gridView;
//    IBOutlet UITableView *filterTableView;
//    
//    UIPopoverController *currencyTypePopOver;
//    CustomersListViewController *currencyTypeViewController;
//    NSMutableDictionary *customerDict;
//    
//    NSDate *selectedDate;
//    UIPopoverController *datePickerPopOver;
//    NSString *fromDate;
//    NSString *toDate;
//    BOOL isFromDate;
//    
//    CustomerPopOverViewController *customVC;
//    UIPopoverController *customPopOver;
//    NSString *custType;
//    NSString *DocType;
//    BOOL isCustType;
//    
//    NSMutableArray *dataArray;
//
//}
//
//@end





//
//  StatementReportViewController.h
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "CustomersListViewController.h"
#import "CustomerPopOverViewController.h"

@interface StatementReportViewController : SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
{
    GridView *gridView;
    IBOutlet UITableView *filterTableView;
    
    UIPopoverController *currencyTypePopOver;
    CustomersListViewController *currencyTypeViewController;
    NSMutableDictionary *customerDict;
    
    NSDate *selectedDate;
    UIPopoverController *datePickerPopOver;
    NSString *fromDate;
    NSString *toDate;
    BOOL isFromDate;
    NSMutableArray *temp;
    SWDatePickerViewController *datePickerViewController;
    NSDateComponents *offsetComponents;
    
    CustomerPopOverViewController *customVC;
    UIPopoverController *customPopOver;
    NSString *custType;
    NSString *DocType;
    BOOL isCustType;
    
    NSMutableArray *dataArray;
    
}
@property(strong,nonatomic)SWDatePickerViewController *datePickerViewControllerDate;
@end

