//
//  SalesWorxDistributionCheckNewViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/22/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDistributionCheckNewViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SalesWorxDistributionItemHeaderTableViewCell.h"
#import "MedRepDefaults.h"
#import "SalesWorxDistributionItemNewTableViewCell.h"
#import "SalesWorxDistributionItemTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "SalesWorxDistributionStockViewController.h"
#import "ShowImageViewController.h"


@interface SalesWorxDistributionCheckNewViewController ()

@end

@implementation SalesWorxDistributionCheckNewViewController
@synthesize currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appControl=[AppControl retrieveSingleton];
    indexPathArray=[[NSMutableArray alloc]init];
    
    self.view.backgroundColor=UIViewBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Distribution Check"];
    
    UIBarButtonItem *btnClose = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(handleBack)];
    [btnClose setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = btnClose;
    
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDistributionCheck)];
    [btnSave setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = btnSave;
    
    [self showCustomerDetails];
    
    MSLItemsArray = [[SWDatabaseManager retrieveManager]fetchDistributionCheckItemsForcustomer];
    
    if ([appControl.FS_SHOW_DC_MIN_STOCK isEqualToString:KAppControlsYESCode]) {
        MSLMinStockOfItemsArray = [[SWDatabaseManager retrieveManager]fetchDCMinStockOfMSLItems];
    } else {
        minReqStockTitleLabel.hidden = YES;
        minReqStockLabel.hidden = YES;
    }
    
    fetchDistriButionCheckLocations=[[SWDatabaseManager retrieveManager]fetchDistriButionCheckLocations];
    
    
    selectedDistributionCheckLocation=[fetchDistriButionCheckLocations objectAtIndex:0];
    selectedDistributionCheckLocation.dcItemsArray = MSLItemsArray;
    selectedDistributionCheckLocation.dcItemsUnfilteredArray = MSLItemsArray;
    
    txtLocation.text = selectedDistributionCheckLocation.LocationName;
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
    
    
    if ([MSLItemsArray count] >0)
    {
        [DcTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
        [self tableView:DcTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    else
    {
        txtLocation.hidden = YES;
        DcTableView.hidden = YES;
        availDetailView.hidden = YES;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    @try {
        NSMutableArray *lastDistributionCheckID = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Distribution_Check where Customer_ID = '%@' ORDER BY Checked_On DESC LIMIT 1",currentVisit.visitOptions.customer.Customer_ID]];
        
//        NSMutableArray *arrlastStock = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select sum(Qty) as totalStock from TBL_Distribution_Check_Items where DistributionCheck_ID = '%@'",[[lastDistributionCheckID  objectAtIndex:0] valueForKey:@"DistributionCheck_ID"]]];
        
        NSMutableArray *arrlastStock = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select sum(Qty) as totalStock from TBL_Distribution_Check_Items where Inventory_Item_ID = '%@'",selectedDistributionCheckItem.DcProduct.Inventory_Item_ID]];
        
        lblLastVisitStock.text = [NSString stringWithFormat:@"%@",[[arrlastStock  objectAtIndex:0] valueForKey:@"totalStock"]];
        lblLastVisitOn.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[[lastDistributionCheckID objectAtIndex:0] valueForKey:@"Checked_On"]];
    } @catch (NSException *exception) {
        lblLastVisitStock.text = @"N/A";
        lblLastVisitOn.text = @"N/A";
    }
    
    
    /******  hide location dropdown when ENABLE_DIST_CHECK_MULTI_LOCATION is N ******/
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOCATION isEqualToString:KAppControlsNOCode])
    {
        txtLocation.hidden = YES;
    }
    
    
    /******* check ENABLE_DIST_CHECK_MULTI_LOTS is Y or N *******/
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        btnAdd.backgroundColor = KDistributionCheckAvailabilityColor;
        
        NSArray *arrSegment = [segAvailable subviews];
        [[arrSegment objectAtIndex:0] setTintColor:KDistributionCheckAvailabilityColor];
        [[arrSegment objectAtIndex:1] setTintColor:KDistributionCheckUnAvailabilityColor];
    }
    else{
        [btnAdd setHidden:YES];
    }
    if ([appControl.DEFAULT_AVAILABILITY_IN_DC isEqualToString:KAppControlsYESCode]) {
        for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
            DCItem.itemAvailability = @"Y";
            selectedDistributionCheckLocation.isLocationDCCompleted = YES;
            
            NSMutableArray * DCItemLotsArray=[[NSMutableArray alloc]init];
            DistriButionCheckItemLot *distributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
            distributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
            distributionCheckItemLot.itemAvailability = @"Y";
            [self enableFields];
            segReorder.selectedSegmentIndex = 1;
            distributionCheckItemLot.reOrder = @"N";
            DCItem.reOrder = @"N";
            
            [DCItemLotsArray addObject:distributionCheckItemLot];
            DCItem.dcItemLots = DCItemLotsArray;
            
        }
        [DcTableView reloadData];
        [self updatePieChart];
    }
    
    if (![appControl.FS_SHOW_DC_FILTER isEqualToString:KAppControlsYESCode]) {
        filterButton.hidden = YES;
    }
    
    if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
        segAvailable.hidden = YES;
        
    } else {
        segAvailabelWithThreeOption.hidden = YES;
    }
    
    if ([appControl.FS_SHOW_DC_RO isEqualToString:KAppControlsYESCode]) {
        xHeightConstraintOfLastVisitON.constant = 20;
        xHeightConstraintOfLastVisitStock.constant = 20;
        
        segReorder.selectedSegmentIndex = 1;
    } else {
        xHeightConstraintOfLastVisitON.constant = 30;
        xHeightConstraintOfLastVisitStock.constant = 30;
        
        lblReorder.hidden = YES;
        segReorder.hidden = YES;
        xTopConstraintOfQunatityField.constant = -30;
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    if (flagRetake)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
            
            NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
            UIImage *DcImage = [UIImage imageWithContentsOfFile:savedImagePath];
            if (DcImage) {
                imgView.image = DcImage;
            }
        });
    }
    flagRetake = false;
}
-(void)viewDidAppear:(BOOL)animated
{
    if ([appControl.DEFAULT_AVAILABILITY_IN_DC isEqualToString:KAppControlsYESCode]){
        segAvailable.selectedSegmentIndex = 0;
    }
    if (self.isMovingToParentViewController) {
        [DcTableView reloadData];
        [self loadPieChart];
    }
}
-(void)handleBack
{
    if(anyUpdated)
    {
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [self.navigationController  popViewControllerAnimated:YES];
          
                                        for (DistriButionCheckLocation *DCLocation in fetchDistriButionCheckLocations) {
                                            for (DistriButionCheckItem *DCItem in DCLocation.dcItemsArray)
                                            {
                                                for (DistriButionCheckItemLot *DCItemLot in DCItem.dcItemLots) {
                                                    
                                                    NSFileManager *fileManager = [NSFileManager defaultManager];
                                                    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                                    NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
                                                    
                                                    NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",DCItemLot.DistriButionCheckItemLotId]];
                                                    NSError *error;
                                                    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                                                    if (!success)
                                                    {
                                                        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                                                    }
                                                }
                                            }
                                        }
                                    }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KWarningAlertTitleStr andMessage:@"Would you like to close the form without updating the distribution information?" andActions:actionsArray withController:self];
    }
    else {
        [self.navigationController  popViewControllerAnimated:YES];
    }
}
-(void)saveDistributionCheck
{
    [self.view endEditing:YES];
    [self productsFilterdidReset];
    if (selectedDistributionCheckLocation.isLocationDCCompleted) {
        
        Singleton *single = [Singleton retrieveSingleton];
        single.distributionRef = [[SWDatabaseManager retrieveManager] saveDistributionCheckWithCustomerInfo:currentVisit.visitOptions.customer andDistChectItemInfo:fetchDistriButionCheckLocations];
        single.isDistributionChecked = YES;
        single.isDistributionItemGet = NO;
        
        NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kDistributionCheckTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                            object:self
                                                          userInfo:visitOptionDict];

        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //send event to analytics
                                       [SWDefaults updateGoogleAnalyticsEvent:kFieldSalesDistributionCheckDoneScreenName];
                                       [self.navigationController  popViewControllerAnimated:YES];
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KSuccessAlertTitleStr andMessage:@"Distribution information saved successfully" andActions:actionsArray withController:self];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Kindly complete all distribution check" withController:self];
    }
}
-(void)showCustomerDetails
{
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];

    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableView Data source Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectedDistributionCheckLocation.dcItemsArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44.0)];
        headerView.backgroundColor = kUITableViewHeaderBackgroundColor;
        [headerView.layer setBorderWidth: 1.0];
        [headerView.layer setMasksToBounds:YES];
        [headerView.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
        [view addSubview:headerView];
        
        return view;
    }
    else
    {
        static NSString* identifier=@"invoicesHeaderCell";
        
        SalesWorxDistributionItemHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemHeaderTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell.contentView.layer setBorderWidth: 1.0];
        [cell.contentView.layer setMasksToBounds:YES];
        [cell.contentView.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
        
        return cell;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"Cell";
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        SalesWorxDistributionItemNewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemNewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if ([indexPathArray containsObject:indexPath])
        {
            cell.lblCode.textColor = MedRepMenuTitleSelectedCellTextColor;
            cell.lblDescription.textColor = MedRepMenuTitleSelectedCellTextColor;
            cell.lblQuantity.textColor = MedRepMenuTitleSelectedCellTextColor;
            cell.lblExpiry.textColor = MedRepMenuTitleSelectedCellTextColor;
            [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        }
        else
        {
            cell.lblCode.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            cell.lblDescription.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            cell.lblQuantity.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            cell.lblExpiry.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }
        
        
        DistriButionCheckItem *DCItem= [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
        
        cell.lblCode.text = DCItem.DcProduct.Item_Code;
        cell.lblDescription.text = DCItem.DcProduct.Description;
        cell.lblQuantity.text = DCItem.Quntity;
        
        if ([DCItem.itemAvailability isEqualToString:@"Y"]) {
            cell.statusView.backgroundColor = KItemAvailable;
            @try {
                if(DCItem.dcItemLots!=nil && DCItem.dcItemLots.count>0)
                {
                    NSArray *qtyArray=[DCItem.dcItemLots valueForKey:@"Quntity"];
                    NSInteger Qty=0;
                    for (NSString *x in qtyArray)
                    {
                        Qty += [x integerValue];
                    }
                    cell.lblQuantity.text=[NSString stringWithFormat:@"%ld",(long)Qty ];
                }
            }
            @catch (NSException *exception) {
                cell.lblQuantity.text = DCItem.Quntity;
            }

        } else if ([DCItem.itemAvailability isEqualToString:@"N"]) {
            cell.statusView.backgroundColor = KItemNotAvailable;
            cell.lblQuantity.text=@"";
        } else {
            cell.statusView.backgroundColor = KNoChange;
            cell.lblQuantity.text=@"";

        }

        return cell;
        
    }
    else
    {
        SalesWorxDistributionItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if ([indexPathArray containsObject:indexPath])
        {
            cell.lblCode.textColor = MedRepMenuTitleSelectedCellTextColor;
            cell.lblDescription.textColor = MedRepMenuTitleSelectedCellTextColor;
            cell.lblQuantity.textColor = MedRepMenuTitleSelectedCellTextColor;
            cell.lblExpiry.textColor = MedRepMenuTitleSelectedCellTextColor;
            [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        }
        else
        {
            cell.lblCode.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            cell.lblDescription.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            cell.lblQuantity.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            cell.lblExpiry.textColor = MedRepMenuTitleUnSelectedCellTextColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }

        
        DistriButionCheckItem *DCItem= [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
        
        
        
        cell.lblCode.text = DCItem.DcProduct.Item_Code;
        cell.lblDescription.text = DCItem.DcProduct.Description;
        
        if ([DCItem.itemAvailability isEqualToString:@"Y"]) {
            cell.statusView.backgroundColor = KItemAvailable;
        } else if ([DCItem.itemAvailability isEqualToString:@"N"]) {
            cell.statusView.backgroundColor = KItemNotAvailable;
        } else {
            cell.statusView.backgroundColor = KNoChange;
        }
        
        cell.lblQuantity.text = DCItem.Quntity;
        cell.lblExpiry.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:DCItem.expiryDate];

        return cell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"More" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                //insert your editAction here
                                                @try {
                                                    [self editTableRow:indexPath];
                                                }
                                                @catch (NSException *exception) {
                                                    NSLog(@"%@",exception);
                                                }
                                                
                                            }];
        editAction.backgroundColor = [UIColor redColor];
        return @[editAction];
    }
    return nil;
}

-(void)editTableRow:(NSIndexPath *)indexPath
{
    editIndexPath = indexPath;
    
    [self.view endEditing:YES];
    SalesWorxDistributionStockViewController *salesOrderStockInfoViewController=[[SalesWorxDistributionStockViewController alloc]initWithNibName:@"SalesWorxDistributionStockViewController" bundle:[NSBundle mainBundle]];
    salesOrderStockInfoViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderStockInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderStockInfoViewController.Delegate = self;
    salesOrderStockInfoViewController.selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
    [self.navigationController presentViewController:salesOrderStockInfoViewController animated:NO completion:nil];
}

-(void)updateStockValue:(NSMutableArray *)updatedStockArray
{
    selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:editIndexPath.row];
    
    if ([selectedDistributionCheckItem.dcItemLots count] == 0) {
        
        selectedDistributionCheckLocation.isLocationDCCompleted = NO;
        
        selectedDistributionCheckItem.itemAvailability = nil;
        selectedDistributionCheckItem.Quntity = nil;
        selectedDistributionCheckItem.LotNumber = nil;
        selectedDistributionCheckItem.expiryDate = nil;
    }
    else
    {
        DistriButionCheckItemLot *DcItemLot = [selectedDistributionCheckItem.dcItemLots objectAtIndex:0];
        
        if (DcItemLot.Quntity.length == 0) {
            selectedDistributionCheckItem.itemAvailability = @"N";
            selectedDistributionCheckItem.Quntity = nil;
            selectedDistributionCheckItem.LotNumber = nil;
            selectedDistributionCheckItem.expiryDate = nil;
        }
        else
        {
            selectedDistributionCheckItem.itemAvailability = @"Y";
            selectedDistributionCheckItem.Quntity = DcItemLot.Quntity;
            selectedDistributionCheckItem.LotNumber = DcItemLot.LotNumber;
            selectedDistributionCheckItem.expiryDate = DcItemLot.expiryDate;
        }
    }

    [DcTableView reloadData];
}

#pragma mark UITableView Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if (indexPathArray.count==0)
    {
        if (indexPath==nil)
        {
            [indexPathArray addObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        else
        {
            [indexPathArray addObject:indexPath];
        }
    }
    else
    {
        [indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
    }
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        if ((![selectedDistributionCheckItem.itemAvailability isEqualToString:@"Y"]) && selectedDistributionCheckItem.imageName.length > 0) {
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
            
            NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
            NSError *error;
            BOOL success = [fileManager removeItemAtPath:filePath error:&error];
            if (!success)
            {
                NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
            }
            selectedDistributionCheckItem.imageName = nil;
        }
        
        SalesWorxDistributionItemNewTableViewCell *cell = (SalesWorxDistributionItemNewTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        cell.lblQuantity.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        [self disableFields];
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
        
        segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
        segReorder.selectedSegmentIndex = 1;
        
        txtQuantity.text = nil;
        txtLotNo.text = nil;
        txtExpiryDate.text = nil;
    }
    else
    {
        SalesWorxDistributionItemTableViewCell *cell = (SalesWorxDistributionItemTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        cell.lblQuantity.textColor = [UIColor whiteColor];
        cell.lblExpiry.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];

        if ([selectedDistributionCheckItem.itemAvailability isEqualToString:@"Y"])
        {
            cell.statusView.backgroundColor = KItemAvailable;
            [self enableFields];
            
            if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                DistriButionCheckItemLot *selectedLot = [selectedDistributionCheckItem.dcItemLots objectAtIndex:0];
                if ([selectedLot.itemAvailability isEqualToString:@"L"]) {
                    segAvailabelWithThreeOption.selectedSegmentIndex = 1;
                    segReorder.enabled = NO;
                    segReorder.alpha = 0.7;
                } else {
                    segAvailabelWithThreeOption.selectedSegmentIndex = 2;
                }
            } else {
                segAvailable.selectedSegmentIndex = 0;
            }
        } else if ([selectedDistributionCheckItem.itemAvailability isEqualToString:@"N"])
        {
            cell.statusView.backgroundColor = KItemNotAvailable;
            [self disableFields];
            
            if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                segAvailabelWithThreeOption.selectedSegmentIndex = 0;
            } else {
                segAvailable.selectedSegmentIndex = 1;
            }
        }
        else
        {
            cell.statusView.backgroundColor = KNoChange;
            [self disableFields];
            segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
            segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
        }
        
        if ([selectedDistributionCheckItem.reOrder isEqualToString:@"Y"]) {
            segReorder.selectedSegmentIndex = 0;
        } else {
            segReorder.selectedSegmentIndex = 1;
        }
        
        txtLocation.text = selectedDistributionCheckLocation.LocationName;
        txtQuantity.text = selectedDistributionCheckItem.Quntity;
        txtLotNo.text = selectedDistributionCheckItem.LotNumber;
        txtExpiryDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:selectedDistributionCheckItem.expiryDate];
    }
    
    if (selectedDistributionCheckItem.imageName.length == 0) {
        imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
        
        NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
        UIImage *DcImage = [UIImage imageWithContentsOfFile:savedImagePath];
        if (DcImage) {
            imgView.image = DcImage;
        }
        else {
            imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
        }
    }
    
    if ([appControl.FS_SHOW_DC_MIN_STOCK isEqualToString:KAppControlsYESCode]) {
        [self setMinStockValueOfMSLItems];
    }
    NSMutableArray *arrlastStock = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select sum(Qty) as totalStock from TBL_Distribution_Check_Items where Inventory_Item_ID = '%@'",selectedDistributionCheckItem.DcProduct.Inventory_Item_ID]];
    
    lblLastVisitStock.text = [NSString stringWithFormat:@"%@",[[arrlastStock  objectAtIndex:0] valueForKey:@"totalStock"]];
    
    [tableView reloadData];
}

-(void)setMinStockValueOfMSLItems
{
    Products *selectedProduct = selectedDistributionCheckItem.DcProduct;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.InventoryItemID == [cd] %@ AND SELF.organizationID == [cd] %@",[SWDefaults getValidStringValue:selectedProduct.Inventory_Item_ID],[SWDefaults getValidStringValue:selectedProduct.OrgID]];
    NSLog(@"template predicate is %@", predicate);
    NSMutableArray *filteredCategoriesArray = [[MSLMinStockOfItemsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    if (filteredCategoriesArray.count > 0) {
        
        DistriButionCheckMinStock *minStock = [filteredCategoriesArray objectAtIndex:0];
        minReqStockLabel.text = [SWDefaults getValidStringValue:minStock.attributeValue];

    } else {
        minReqStockLabel.text = @"N/A";
    }
}

#pragma mark
-(void)disableFields
{
    txtQuantity.text = @"";
    txtLotNo.text = @"";
    txtExpiryDate.text = @"";
    
    txtQuantity.enabled = NO;
    txtQuantity.alpha = 0.7;
    
    txtLotNo.enabled = NO;
    txtLotNo.alpha = 0.7;
    
    txtExpiryDate.enabled = NO;
    txtExpiryDate.alpha = 0.7;
    
    segReorder.enabled = NO;
    segReorder.alpha = 0.7;
    
    [imgView removeGestureRecognizer:tap];
}
-(void)enableFields
{
    txtQuantity.enabled = YES;
    txtQuantity.alpha = 1.0;
    
    txtLotNo.enabled = YES;
    txtLotNo.alpha = 1.0;
    
    txtExpiryDate.enabled = YES;
    txtExpiryDate.alpha = 1.0;
    
    segReorder.enabled = YES;
    segReorder.alpha = 1.0;
    
    [imgView addGestureRecognizer:tap];
}

#pragma mark

- (IBAction)segmentReorderValueChanged:(id)sender {
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
    {
        selectedDistributionCheckItemLot = [selectedDistributionCheckItem.dcItemLots objectAtIndex:0];
        
        if (segReorder.selectedSegmentIndex == 0) {
            selectedDistributionCheckItem.reOrder = @"Y";
            selectedDistributionCheckItemLot.reOrder = @"Y";
        } else {
            selectedDistributionCheckItem.reOrder = @"N";
            selectedDistributionCheckItemLot.reOrder = @"N";
        }
    }
}

- (IBAction)segmentValueChanged:(id)sender{
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        btnAdd.enabled = YES;
        btnAdd.alpha = 1.0;
        
        if (segAvailable.selectedSegmentIndex == 0)
        {
            [self enableFields];
            segReorder.selectedSegmentIndex = 1;
        } else
        {
            [self disableFields];
            segReorder.selectedSegmentIndex = 0;
            imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
        }
    }
    else
    {
        NSMutableArray * DCItemLotsArray=[[NSMutableArray alloc]init];
        selectedDistributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
        selectedDistributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
        
        if (segAvailable.selectedSegmentIndex == 0)
        {
            selectedDistributionCheckItem.itemAvailability = @"Y";
            [self enableFields];
            segReorder.selectedSegmentIndex = 1;
            selectedDistributionCheckItemLot.reOrder = @"N";
            selectedDistributionCheckItem.reOrder = @"N";
        } else
        {
            selectedDistributionCheckItem.itemAvailability = @"N";
            selectedDistributionCheckItem.Quntity = nil;
            selectedDistributionCheckItem.LotNumber = nil;
            selectedDistributionCheckItem.expiryDate = nil;
            selectedDistributionCheckItem.imageName = nil;
        
            [self disableFields];
            segReorder.selectedSegmentIndex = 0;
            selectedDistributionCheckItemLot.reOrder = @"Y";
            selectedDistributionCheckItem.reOrder = @"Y";
            imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
        }
        [DCItemLotsArray addObject:selectedDistributionCheckItemLot];
        selectedDistributionCheckItem.dcItemLots = DCItemLotsArray;
        [DcTableView reloadData];
        [self updatePieChart];
    }
}

- (IBAction)segmentValueChangedWithThreeOption:(id)sender{
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        btnAdd.enabled = YES;
        btnAdd.alpha = 1.0;
        
        if (segAvailabelWithThreeOption.selectedSegmentIndex == 0)
        {
            [self disableFields];
            segReorder.selectedSegmentIndex = 0;
            imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
        }  else {
            [self enableFields];
            txtQuantity.text = @"";
            txtLotNo.text = @"";
            txtExpiryDate.text = @"";
            
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                segReorder.enabled = NO;
                segReorder.alpha = 0.7;
                segReorder.selectedSegmentIndex = 0;
            }
            else {
                segReorder.selectedSegmentIndex = 1;
            }
        }
    }
    else
    {
        NSMutableArray * DCItemLotsArray=[[NSMutableArray alloc]init];
        selectedDistributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
        selectedDistributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
        
        if (segAvailabelWithThreeOption.selectedSegmentIndex == 0)
        {
            selectedDistributionCheckItem.itemAvailability = @"N";
            selectedDistributionCheckItem.Quntity = nil;
            selectedDistributionCheckItem.LotNumber = nil;
            selectedDistributionCheckItem.expiryDate = nil;
            selectedDistributionCheckItem.imageName = nil;
            [self disableFields];
            
            segReorder.selectedSegmentIndex = 0;
            selectedDistributionCheckItemLot.reOrder = @"Y";
            selectedDistributionCheckItem.reOrder = @"Y";
            imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
        } else
        {
            selectedDistributionCheckItem.itemAvailability = @"Y";
            [self enableFields];
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                segReorder.enabled = NO;
                segReorder.alpha = 0.7;
                segReorder.selectedSegmentIndex = 0;
                selectedDistributionCheckItemLot.reOrder = @"Y";
                selectedDistributionCheckItem.reOrder = @"Y";
            }
            else {
                segReorder.selectedSegmentIndex = 1;
                selectedDistributionCheckItem.reOrder = @"N";
                selectedDistributionCheckItemLot.reOrder = @"N";
            }
            
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                selectedDistributionCheckItemLot.itemAvailability = @"L";
            } else if (segAvailabelWithThreeOption.selectedSegmentIndex == 2) {
                selectedDistributionCheckItemLot.itemAvailability = @"H";
            }
        }
        [DCItemLotsArray addObject:selectedDistributionCheckItemLot];
        selectedDistributionCheckItem.dcItemLots = DCItemLotsArray;
        [DcTableView reloadData];
        [self updatePieChart];
    }
}

- (IBAction)btnAdd:(id)sender {

    isAddButtonPressed = YES;
    
    if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
        if (segAvailabelWithThreeOption.selectedSegmentIndex == 1 || segAvailabelWithThreeOption.selectedSegmentIndex == 2)
        {
            if (txtQuantity.text.length == 0 && [appControl.MANDATORY_QTY_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Quantity is mandatory" withController:self];
            }
            else if (txtExpiryDate.text.length == 0 && [appControl.MANDATORY_EXP_DT_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Expiry date is mandatory" withController:self];
            }
            else
            {
                [self addItems];
            }
        }
        else
        {
            [self addItems];
        }
        
    } else {
        if (segAvailable.selectedSegmentIndex == 0)
        {
            if (txtQuantity.text.length == 0 && [appControl.MANDATORY_QTY_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Quantity is mandatory" withController:self];
            }
            else if (txtExpiryDate.text.length == 0 && [appControl.MANDATORY_EXP_DT_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Expire date is mandatory" withController:self];
            }
            
            else
            {
               [self addItems];
           }
        }
        else
        {
            [self addItems];
        }
    }
}

-(void)addItems
{
    BOOL duplicacy = NO;
    NSMutableArray * DCItemLotsArray;
    if ([selectedDistributionCheckItem.dcItemLots count] == 0)
    {
        DCItemLotsArray = [[NSMutableArray alloc]init];
    }
    else
    {
        for (DistriButionCheckItemLot *DCItemLot in selectedDistributionCheckItem.dcItemLots) {
            if([[DCItemLot.LotNumber localizedLowercaseString] isEqualToString:[txtLotNo.text localizedLowercaseString]] && [DCItemLot.expiryDate isEqualToString:txtExpiryDate.text])
            {
                duplicacy = YES;
                break;
            }
        }
        
        if (duplicacy)
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KTitleStrAlert andMessage:@"Item is already updated for this Lot No" withController:self];
        }
        else
        {
            DCItemLotsArray = selectedDistributionCheckItem.dcItemLots;
        }
    }
    
    if (duplicacy == NO) {
        
        if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
            
            // three option segment
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 0)
            {
                selectedDistributionCheckItem.itemAvailability = @"N";
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
                
                NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
                NSError *error;
                BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                if (!success)
                {
                    NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                }
                selectedDistributionCheckItem.imageName = nil;
                imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
                
                // remove all existing lots if item availability is N for any location
                DCItemLotsArray = [[NSMutableArray alloc]init];
            } else
            {
                selectedDistributionCheckItem.itemAvailability = @"Y";
                
                // check and delete lot if item availability is N for any location
                
                for (DistriButionCheckItemLot *DCitemLot in selectedDistributionCheckItem.dcItemLots) {
                    if ([DCitemLot.Quntity isEqualToString:@""] || DCitemLot.Quntity == nil) {
                        [selectedDistributionCheckItem.dcItemLots removeObject:DCitemLot];
                    }
                }
            }
        }
        else {
            if (segAvailable.selectedSegmentIndex == 0)
            {
                selectedDistributionCheckItem.itemAvailability = @"Y";
                
                
                // check and delete lot if item availability is N for any location
                
                for (DistriButionCheckItemLot *DCitemLot in selectedDistributionCheckItem.dcItemLots) {
                    if ([DCitemLot.Quntity isEqualToString:@""] || DCitemLot.Quntity == nil) {
                        [selectedDistributionCheckItem.dcItemLots removeObject:DCitemLot];
                    }
                }
                
            } else
            {
                selectedDistributionCheckItem.itemAvailability = @"N";
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
                
                NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
                NSError *error;
                BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                if (!success)
                {
                    NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                }
                selectedDistributionCheckItem.imageName = nil;
                imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
                
                // remove all existing lots if item availability is N for any location
                DCItemLotsArray = [[NSMutableArray alloc]init];
            }
        }
        
        selectedDistributionCheckItem.Quntity = txtQuantity.text;
        selectedDistributionCheckItem.LotNumber = txtLotNo.text;
        selectedDistributionCheckItem.expiryDate = txtExpiryDate.text;
        
        if ([appControl.FS_SHOW_DC_RO isEqualToString:KAppControlsYESCode]) {
            if (segReorder.selectedSegmentIndex == 0) {
                selectedDistributionCheckItem.reOrder = @"Y";
            } else {
                selectedDistributionCheckItem.reOrder = @"N";
            }
        } else {
             selectedDistributionCheckItem.reOrder = @"";
        }
        
        
        selectedDistributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
        selectedDistributionCheckItemLot.Quntity = txtQuantity.text;
        selectedDistributionCheckItemLot.LotNumber = txtLotNo.text;
        selectedDistributionCheckItemLot.expiryDate = txtExpiryDate.text;
        selectedDistributionCheckItemLot.reOrder = selectedDistributionCheckItem.reOrder;
        
        if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
            
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 0) {
                selectedDistributionCheckItemLot.itemAvailability = @"N";
            }
            else if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                selectedDistributionCheckItemLot.itemAvailability = @"L";
            } else if (segAvailabelWithThreeOption.selectedSegmentIndex == 2) {
                selectedDistributionCheckItemLot.itemAvailability = @"H";
            }
        }
        
        if (selectedDistributionCheckItem.imageName.length == 0) {
            selectedDistributionCheckItemLot.isImage = @"N";
        }
        else {
            selectedDistributionCheckItemLot.isImage = @"Y";
        }
        selectedDistributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
        [DCItemLotsArray addObject:selectedDistributionCheckItemLot];
        
        selectedDistributionCheckItem.dcItemLots = DCItemLotsArray;
        
        
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        [DcTableView reloadData];
        [self disableFields];
        segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
        segReorder.selectedSegmentIndex = 1;
        
        [self updatePieChart];
    }
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField;        // return NO to disallow editing.
{
    if(textField == txtLocation)
    {
        [self.view endEditing:YES];
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        NSMutableArray *arrLocation = [[NSMutableArray alloc]init];
        for (DistriButionCheckLocation *DCLocation in fetchDistriButionCheckLocations) {
            [arrLocation addObject:DCLocation.LocationName];
        }
        popOverVC.titleKey = @"Location";
        popOverVC.popOverContentArray = arrLocation;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        UIPopoverController *paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        paymentPopOverController.delegate=self;
        popOverVC.popOverController=paymentPopOverController;
        
        [paymentPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        [paymentPopOverController presentPopoverFromRect:textField.dropdownImageView.frame inView:textField.rightView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        return NO;
    }
    if(textField == txtExpiryDate)
    {
        [self.view endEditing:YES];
        
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = @"Expiry Date";
        
        if (txtExpiryDate.text == nil || [txtExpiryDate.text isEqualToString:@""])
        {
            popOverVC.setMinimumDateCurrentDate=YES;
        } else {
            popOverVC.setMinimumDateCurrentDate=NO;
            popOverVC.previousSelectedDate = [MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:@"yyyy-MM-dd" scrString:selectedDistributionCheckItem.expiryDate];
        }
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        UIPopoverController *paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        paymentPopOverController.delegate=self;
        popOverVC.datePickerPopOverController=paymentPopOverController;
        
        [paymentPopOverController setPopoverContentSize:CGSizeMake(300, 273) animated:YES];
        [paymentPopOverController presentPopoverFromRect:textField.dropdownImageView.frame inView:textField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

        return NO;
    }
    
    return YES;
}

-(void)didSelectDate:(NSString *)selectedDate
{
    [txtExpiryDate setText:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:selectedDate]];

    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
    {
        selectedDistributionCheckItem.expiryDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        selectedDistributionCheckItemLot.expiryDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        [DcTableView reloadData];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtQuantity)
    {
        [textField resignFirstResponder];
        [txtLotNo becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }

    return YES;
}

#define MAX_LENGTH 100
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //OLA! Allow only positive numerical input for quantity
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == txtQuantity)
    {
        NSString *allowedCharacters;
        if ([textField.text length]>0)//no decimal point yet, hence allow point
            allowedCharacters = @"0123456789";
        else
            allowedCharacters = @"123456789";//first input character cannot be '0'
        
        NSString *newText = [textField.text stringByReplacingCharactersInRange: range withString:string];
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
        {
            selectedDistributionCheckItem.Quntity = newText;
            selectedDistributionCheckItemLot.Quntity = newText;
        }
        return (newString.length<=KSalesOrderScreenOrderQuantityTextFieldMaximumDigitsLimit);

    }
    else if (textField == txtLotNo)
    {
        NSString *newText = [textField.text stringByReplacingCharactersInRange: range withString:string];
        if( [newText length]<= MAX_LENGTH ){
           
            if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
            {
                selectedDistributionCheckItem.LotNumber = newText;
                selectedDistributionCheckItemLot.LotNumber = newText;
            }
            return YES;
        }
        // case where text length > MAX_LENGTH
        textField.text = [newText substringToIndex: MAX_LENGTH ];
        
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
        {
            selectedDistributionCheckItem.LotNumber = textField.text;
            selectedDistributionCheckItemLot.LotNumber = textField.text;
        }
        
        return NO;
    }
    return YES;
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 160; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
    {
        if (textField == txtQuantity)
        {
            selectedDistributionCheckItem.Quntity = textField.text;
            selectedDistributionCheckItemLot.Quntity = textField.text;
        } else {
            selectedDistributionCheckItem.LotNumber = textField.text;
            selectedDistributionCheckItemLot.LotNumber = textField.text;
        }
        [DcTableView reloadData];
    }
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    [self productsFilterdidReset];
    
    BOOL isAllDCItemUpdated = YES;
    BOOL isAnyDCItemUpdated = NO;
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        
        if ([DCItem.dcItemLots count] == 0) {
            isAllDCItemUpdated = NO;
            break;
        }
    }
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        
        if ([DCItem.dcItemLots count] > 0) {
            isAnyDCItemUpdated = YES;
            break;
        }
    }
    
    if (isAllDCItemUpdated)
    {
        selectedDistributionCheckLocation.isLocationDCCompleted = YES;
        [self switchLocation:selectedIndexPath];
    } else if(isAnyDCItemUpdated){
        
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        selectedDistributionCheckLocation.dcItemsArray = nil;
                                        [self switchLocation:selectedIndexPath];
                                    }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KWarningAlertTitleStr andMessage:@"Distribution check entries in current location are incomplete. Switching the location will delete the changes made in this location . \n Would you like to discard them?" andActions:actionsArray withController:self];
    }
    else{
        [self switchLocation:selectedIndexPath];
    }
}

-(void)switchLocation:(NSIndexPath*)selectedIndexPath
{
    selectedDistributionCheckLocation = [fetchDistriButionCheckLocations objectAtIndex:selectedIndexPath.row];
    txtLocation.text = selectedDistributionCheckLocation.LocationName;
    
    if (selectedDistributionCheckLocation.isLocationDCCompleted) {
        NSLog(@"Location completed");
    }
    else
    {
        selectedDistributionCheckLocation.dcItemsArray = [[SWDatabaseManager retrieveManager]fetchDistributionCheckItemsForcustomer];
        selectedDistributionCheckLocation.dcItemsUnfilteredArray = selectedDistributionCheckLocation.dcItemsArray;
        
        [self disableFields];
        segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
        segReorder.selectedSegmentIndex = 1;
    }
    
    [DcTableView reloadData];
    
    [DcTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [DcTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
    [self tableView:DcTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [self updatePieChart];
    
    if (selectedDistributionCheckLocation.isLocationDCCompleted) {
        availDetailView.userInteractionEnabled = NO;
        availDetailView.alpha = 0.7;
        [imgView removeGestureRecognizer:tap];
    }
    else
    {
        availDetailView.userInteractionEnabled = YES;
        availDetailView.alpha = 1.0;
        [imgView addGestureRecognizer:tap];
    }
}


#pragma mark image action
- (void)imageTapped:(UIGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
    if ([imgView.image isEqual:[UIImage imageNamed:@"ProductsDefaultImage"]])
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
            imagePicker.allowsEditing = NO;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
    }
    else
    {
        flagRetake = true;
        ShowImageViewController * popOverVC=[[ShowImageViewController alloc]init];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
        
        NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
        UIImage *DcImage = [UIImage imageWithContentsOfFile:savedImagePath];
        if (DcImage) {
            popOverVC.imageName = selectedDistributionCheckItem.imageName;
            popOverVC.image = DcImage;
        }
        
        [self.navigationController pushViewController:popOverVC animated:YES];
    }
}
#pragma mark image delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:(NSString*)kUTTypeImage])
        {
            imgView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *error;
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
            
            if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
            {
                if (selectedDistributionCheckItem.imageName.length ==  0) {
                    selectedDistributionCheckItem.imageName = [NSString createGuid];
                }
            }
            else
            {
                selectedDistributionCheckItem.imageName = selectedDistributionCheckItemLot.DistriButionCheckItemLotId;
            }
            
            NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
            
            float compressionQuality=0.3;
            // Now, save the image to a file.
            
            if(NO == [UIImageJPEGRepresentation(imgView.image,compressionQuality) writeToFile:savedImagePath atomically:YES]) {
                [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", savedImagePath];
            }
            else
            {
                NSLog(@"Image saved");
            }
        });
    }];
}

#pragma mark Load PieChart
-(void)loadPieChart
{
    int updatedDistribution = 0;
    int nonUpdatedDistribution = 0;
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        if ([DCItem.dcItemLots count] > 0) {
            updatedDistribution++;
            anyUpdated = YES;
        }
        else
        {
            nonUpdatedDistribution++;
        }
    }
    
    [pieChart removeFromSuperview];
    pieChart = [[PNCircleChart alloc] initWithFrame:pieChart.frame total:[NSNumber numberWithInt:(updatedDistribution+nonUpdatedDistribution)] current:[NSNumber numberWithInt:updatedDistribution] clockwise:YES shadow:YES shadowColor:[UIColor colorWithRed:(232.0f/255.0f) green:(243.0f/255.0f) blue:(246.0f/255.0f) alpha:1] check:YES lineWidth:@6.0f];
    
    pieChart.backgroundColor = [UIColor clearColor];
    [pieChart setStrokeColor:[UIColor clearColor]];
    [pieChart setStrokeColorGradientStart:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [pieChart strokeChart];
    [parentViewOfCustomerDetail addSubview:pieChart];
    
    
    if (nonUpdatedDistribution == 0) {
        selectedDistributionCheckLocation.isLocationDCCompleted = YES;
    }
    else{
        selectedDistributionCheckLocation.isLocationDCCompleted = NO;
    }
}

-(void)updatePieChart
{
    int updatedDistribution = 0;
    int nonUpdatedDistribution = 0;
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        if ([DCItem.dcItemLots count] > 0) {
            updatedDistribution++;
            anyUpdated = YES;
        }
        else
        {
            nonUpdatedDistribution++;
        }
    }
    [pieChart updateChartByCurrent:[NSNumber numberWithInt:updatedDistribution]];
    
    if (nonUpdatedDistribution == 0) {
        selectedDistributionCheckLocation.isLocationDCCompleted = YES;
    }
    else{
        selectedDistributionCheckLocation.isLocationDCCompleted = NO;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Filter

- (IBAction)filterButtonTapped:(id)sender {

    NSMutableArray *arrDCItems = selectedDistributionCheckLocation.dcItemsUnfilteredArray;
    productsArray = [[NSMutableArray alloc]init];
    
    for (DistriButionCheckItem *dcItem in arrDCItems) {
        [productsArray addObject:dcItem.DcProduct];
    }
    
    if (productsArray.count>0) {
        
        [self.view endEditing:YES];

        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        // Blurred with UIImage+ImageEffects
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        [self.view addSubview:blurredBgImage];
        
        
        SalesWorxDCFilterViewController * popOverVC=[[SalesWorxDCFilterViewController alloc]init];
        popOverVC.delegate=self;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict = previousFilteredParameters;
        }

        popOverVC.productsArray=productsArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        UIPopoverController *filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 380) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
        
        CGRect relativeFrame = [filterButton convertRect:filterButton.bounds toView:self.view];
        
        
        
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }    
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later"  withController:nil];
    }
}

#pragma mark UIPopOver delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

-(void)productsFilterDidClose
{
    [blurredBgImage removeFromSuperview];
}

-(void)filteredProducts:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    [self updateProductTableViewOnFilter:filteredArray];
}

-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    previousFilteredParameters=parametersDict;
}

-(void)productsFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];

    selectedDistributionCheckLocation.dcItemsArray = selectedDistributionCheckLocation.dcItemsUnfilteredArray;
    [DcTableView reloadData];
    
    [DcTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [DcTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
    [self tableView:DcTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [self updatePieChart];
}

-(void)updateProductTableViewOnFilter:(NSMutableArray *)filteredProductsArray
{
    NSMutableArray *arrDCItems = selectedDistributionCheckLocation.dcItemsUnfilteredArray;
    NSMutableArray *filteredMSLArray = [[NSMutableArray alloc]init];

    for (Products *selectedProduct in filteredProductsArray) {
        [filteredMSLArray addObject:[[[arrDCItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(DcProduct == %@)", selectedProduct]]mutableCopy] objectAtIndex:0]];
    }
    
    selectedDistributionCheckLocation.dcItemsArray = filteredMSLArray;
    [DcTableView reloadData];
    
    [DcTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [DcTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
    [self tableView:DcTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [self updatePieChart];
}

@end
