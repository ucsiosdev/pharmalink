//
//  SWVisitManager.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

//78FC035B-88FB-44AB-AE7D-D7E2FA936E08

#import <objc/runtime.h>
#import "SWVisitManager.h"
#import "SWDefaults.h"
#import "Singleton.h"
#import "SalesWorxVisitOptionsViewController.h"
static SWVisitManager *instance;

@implementation SWVisitManager


@synthesize customer;
@synthesize parentViewController;
@synthesize visitID,routeDictionary,isPlannedVisit,visitModuleType;

- (id)init {
    self = [super init];
    
    if (self) {
       // visitStarted = NO;
    }

    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

//
//+ (SWVisitManager *)defaultManager
//{
//    if (!instance) {
//        instance = [[SWVisitManager alloc] init];
//    }
//    
//    return instance;
//}


-(void)startVisitWithCustomerDetails:(SalesWorxVisit*)currentVisit withParent:(UIViewController*)parent
{
    [self setParentViewController:parent];
    currentCustomerVisit=currentVisit;
    [self getVisitServiceDidGetOpenVisit:[[SWDatabaseManager retrieveManager] dbGetOpenVisiteFromCustomerID:currentVisit.visitOptions.customer.Ship_Customer_ID andSiteID:currentVisit.visitOptions.customer.Ship_Site_Use_ID]];
}

- (void)startVisitWithCustomer:(NSDictionary *)c parent:(UIViewController *)parent andChecker:(NSString *)isParent
{
    [self setCustomer:[c mutableCopy]];
    [self setParentViewController:parent];
    Singleton *single = [Singleton retrieveSingleton] ;
    single.currentCustomer = [c mutableCopy] ;
    [SWDefaults setCustomer:[c mutableCopy] ];
    
   

    //routeSer.delegate=self;
    NSLog(@"current customer is %@", self.customer);
    
    [self getVisitServiceDidGetOpenVisit:[[SWDatabaseManager retrieveManager] dbGetOpenVisiteFromCustomerID:[self.customer stringForKey:@"Ship_Customer_ID"] andSiteID:[self.customer stringForKey:@"Ship_Site_Use_ID"]]];
    c=nil;
    parent=nil;
    isParent=nil;
}
- (void)getVisitServiceDidGetOpenVisit:(NSArray *)routes;
{
    Singleton *single = [Singleton retrieveSingleton];
    single.isDistributionChecked = NO;
   // NSLog(@"routes are %@", routes);
    
    if([routes count]!=0)
    {
        NSString *endDate = [[routes objectAtIndex:[routes count]-1] stringForKey:@"Visit_End_Date"];
        if ([endDate isEqualToString:@""]) {
            [self continueOpenVisit:[[routes objectAtIndex:[routes count]-1]stringForKey:@"Actual_Visit_ID"]];
        }
        else
        {
            [self startNewVisit];
        }
    }
    else {
        [self startNewVisit];
        
    }
    routes=nil;
}
- (void)continueOpenVisit:(NSString *)openVisitID{

    

    [SWDefaults clearCurrentVisitID];

    [SWDefaults setCurrentVisitID:openVisitID];
    
    /*
    Class module = NSClassFromString(@"SWVisitOptionsViewController");
    if (module != nil)
    {
        SWVisitOptionsViewController *viewController = [[SWVisitOptionsViewController alloc] init];
        [viewController setCustomer:customer];
       // [self setVisitViewController:viewController];
        [self.navigationController pushViewController:viewController animated:YES];
        viewController=nil;
        visitStarted = YES;
    }
*/
    Class module = NSClassFromString(@"SalesWorxVisitOptionsViewController");
    if (module != nil)
    {
        SalesWorxVisitOptionsViewController * visitOptions=[[SalesWorxVisitOptionsViewController alloc]init];
        currentCustomerVisit.isOpenVisit=YES;
        currentCustomerVisit.Visit_ID=openVisitID;
        visitOptions.salesWorxVisit=currentCustomerVisit;
        
        NSLog(@"check planned visit status whiel pushing %hhd", visitOptions.salesWorxVisit.isPlannedVisit);

        // [self setVisitViewController:viewController];
        [self.navigationController pushViewController:visitOptions animated:YES];
        visitStarted = YES;
    }
    
    
}
- (void)startNewVisit
{
    [SWDefaults clearCurrentVisitID];

    visitID=[NSString createGuid];
    [SWDefaults setCurrentVisitID:visitID];

    //routeSer.delegate=self;
    
    NSLog(@"calling start visit in visit options manager");
    

//    Class module = NSClassFromString(@"SWVisitOptionsViewController");
//    if (module != nil) {
//
//        SWVisitOptionsViewController *viewController = [[SWVisitOptionsViewController alloc] init];
//        viewController.customer = customer;
//        
//       // [self setVisitViewController:viewController];
//        [self.navigationController pushViewController:viewController animated:YES];
//        visitStarted = YES;
//        viewController=nil;
//
//    }
    
    Class module = NSClassFromString(@"SalesWorxVisitOptionsViewController");
    if (module != nil)
    {
//        SalesWorxVisitOptionsViewController * visitOptions=[[SalesWorxVisitOptionsViewController alloc]init];
//        SalesWorxVisit* salesWorxVisit=[[SalesWorxVisit alloc]init];
//        if (isPlannedVisit==YES) {
//            //to be done, currently no data so doing this for test
//            salesWorxVisit.isPlannedVisit=isPlannedVisit;
//        }
//
//        salesWorxVisit.Visit_ID=visitID;
//        VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
//        
//        NSLog(@"check for planned visit ID %@", [customer objectForKey:@"Planned_Visit_ID"]);
//        
//        currentVisitOptions.customer=[SWDefaults fetchCurrentCustObject:customer];
//        
//        NSLog(@"check visit type %@", currentVisitOptions.customer.cashCustomerDictionary);
//        
//        salesWorxVisit.visitOptions=currentVisitOptions;
//        visitOptions.salesWorxVisit=salesWorxVisit;
//        // [self setVisitViewController:viewController];
//        [self.navigationController pushViewController:visitOptions animated:YES];
//        visitStarted = YES;
        
        if ([currentCustomerVisit.Visit_Type isEqualToString:kMerchandisingVisitType]) {
           
            SalesWorxMerchandising*  salesWorxMerchandising=[[SalesWorxMerchandising alloc]init];
            salesWorxMerchandising.Session_ID=[NSString createGuid];
            salesWorxMerchandising.Start_Time=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            
            salesWorxMerchandising.merchandisingArray=[[NSMutableArray alloc]init];
            currentCustomerVisit.visitOptions.salesWorxMerchandising=salesWorxMerchandising;

            SalesWorxMerchandisingHomeViewController * merchHomeVC=[[SalesWorxMerchandisingHomeViewController alloc]init];
            merchHomeVC.currentVisit=currentCustomerVisit;
            visitStarted = YES;

            [self.navigationController pushViewController:merchHomeVC animated:YES];

        }
        else
        {
        
        SalesWorxVisitOptionsViewController * visitOptions=[[SalesWorxVisitOptionsViewController alloc]init];
        
        visitOptions.salesWorxVisit=currentCustomerVisit;
        
        NSLog(@"check planned visit status whiel pushing %hhd", visitOptions.salesWorxVisit.isPlannedVisit);
        
        // [self setVisitViewController:viewController];
        [self.navigationController pushViewController:visitOptions animated:YES];
        visitStarted = YES;
        }
    }

    
    
}

- (void)closeVisit {
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isDistributionChecked = NO;

    
    [self setCustomer:nil];
    visitStarted = NO;
    [self setParentViewController:nil];
   //[self setVisitViewController:nil];
}




@end
