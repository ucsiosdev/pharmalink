//
//  SalesWorxAssortmentBonusManager.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/26/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SalesWorxCustomClass.h"

@interface SalesWorxAssortmentBonusManager : NSObject
/* Returns Array of <SalesworxAssortmentBonusPlan> objects*/
/* Input Array of "<Products>" objects*/
-(NSMutableArray *)FetchAssortmentBonusPlansForCategoryProducts:(NSMutableArray *)productsArray ForCustomer:(Customer *)customer;/**array of "<Products>" objects*/
-(BOOL)IsAnyAssortmentBonusAvailableForTheProduct:(Products *)product AssortmentPlans:(NSMutableArray *)plans;
-(NSMutableDictionary *)FetchNewOrUpdatedAssortmentBonusPlanAvailableForSalesOrder:(SalesOrder *)salesOrder ForProduct:(SalesOrderLineItem *)OrderItem;
-(double)FetchAvailableBonusQuantityForSalesOrder:(SalesOrder *)salesOrder ForPlanId:(NSInteger)PlanId;
-(SalesOrder *)AssignDefaultBonusPlansForSalesOrder:(SalesOrder *)Order;
-(SalesOrder *)UpdateConfirmesSalesOrderLineItems:(SalesOrder *)Order WithDBAssortmentBonusProductsArray:(NSMutableArray *)DBAsstBnsProductsArray AndProductsArray:(NSMutableArray *)ProductsArray;
-(NSDictionary *)UpdateProformaSalesOrderLineItems:(SalesOrder *)Order WithDBAssortmentBonusProductsArray:(NSMutableArray *)DBAsstBnsProductsArray AndProductsArray:(NSMutableArray *)ProductsArray;
-(NSMutableArray *)FetchAssortmentBonusPlansAvailableForProducts:(NSMutableArray *)productsArray/**array of "<Products>" objects*/;
-(NSMutableArray *)FetchAssortmentBonusPlansAvailableForProduct:(NSString *)Item_Code ProductsArray:(NSMutableArray *)productsArray/**array of Products Dictionaries*/;
-(NSInteger )FetchAvailableBonusPlanForProduct:(Products *)product AssortmentPlans:(NSMutableArray *)plans;
@end
