//
//  HTMLViewerViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 9/29/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "HTMLViewerViewController.h"

@interface HTMLViewerViewController ()

@end

@implementation HTMLViewerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_webView loadRequest:_req];
}

#pragma mark button action methods

- (IBAction)closeButtontapped:(id)sender
{
    if(self.isFromProductsScreen){
        [self dismissViewControllerAnimated:YES completion:nil];

    }else{
    if ([self.htmlDelegate respondsToSelector:@selector(willCloseWebPage)]) {
        
        [self.htmlDelegate willCloseWebPage];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
