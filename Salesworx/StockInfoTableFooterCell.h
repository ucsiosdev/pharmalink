//
//  StockInfoTableFooterCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/4/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockInfoTableFooterCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *avblStockLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderQtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *avblStockStatic;
@property (strong, nonatomic) IBOutlet UILabel *orderQtyStatic;
@property (strong, nonatomic) IBOutlet UILabel *allocatedQtyStatic;

@property (strong, nonatomic) IBOutlet UILabel *allocatedQtyLbl;
@end
