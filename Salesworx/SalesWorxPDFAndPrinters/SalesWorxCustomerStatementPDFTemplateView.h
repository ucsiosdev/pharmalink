//
//  PDFPageTemplate.h
//  SalesworxCustomerstatementTest
//
//  Created by Pavan Kumar Singamsetti on 12/7/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableView.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
@interface SalesWorxCustomerStatementPDFTemplateView : UIView{
    IBOutlet UILabel * customerNamelabel;
    IBOutlet UILabel * employeNameLabel;
    IBOutlet UILabel * dateLabel;
    
    IBOutlet UILabel * customerAvailableBalancelabel;
    IBOutlet UILabel * oustandingpaymentlabel;
    IBOutlet UILabel * netAmountLabel;
    
    
    IBOutlet UILabel * customerAvailableBalanceStringlabel;
    IBOutlet UILabel * oustandingpaymentStringlabel;
    IBOutlet UILabel * netAmountStringLabel;
    IBOutlet UILabel * pageNumberLabel;
    IBOutlet UIImageView * LogoImageView;

    IBOutlet SalesWorxTableView * customerStatementTableView;
    NSInteger numberOfRowsInaPage;
    NSInteger StartIndexInCurrentPage;

}
-(void)UpdatePdfPgae;
@property(strong,nonatomic) NSMutableArray *arrCustomersStatement;
@property(strong,nonatomic) NSDictionary *selectedCustomer;
@property (nonatomic) NSInteger pageNumber;
@property BOOL isDataIsModelFromPerviousVC;
@end
