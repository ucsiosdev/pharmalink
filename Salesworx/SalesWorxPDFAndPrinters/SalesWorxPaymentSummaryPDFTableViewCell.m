//
//  SalesWorxPaymentSummaryPDFTableViewCell.m
//  MedRep
//
//  Created by Neha Gupta on 10/5/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxPaymentSummaryPDFTableViewCell.h"

@implementation SalesWorxPaymentSummaryPDFTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
