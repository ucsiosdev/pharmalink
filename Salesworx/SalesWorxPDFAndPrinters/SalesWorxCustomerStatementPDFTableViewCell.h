//
//  SalesWorxCustomerStatementPDFTableViewCell.h
//  SalesworxCustomerstatementTest
//
//  Created by Pavan Kumar Singamsetti on 12/7/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxCustomerStatementPDFTableViewCell : UITableViewCell
@property(strong,nonatomic) IBOutlet UILabel *lblDocumentNo;
@property(strong,nonatomic) IBOutlet UILabel *lblInvoiceDate;
@property(strong,nonatomic) IBOutlet UILabel *lblNetAmount;
@property(strong,nonatomic) IBOutlet UILabel *lblPaidAmount;
@property(strong,nonatomic) IBOutlet UILabel *lblDueDate;
@property(strong,nonatomic) IBOutlet UILabel *lblDueAmount;
@property(strong,nonatomic) IBOutlet UILabel *lblSerialNumber;
@property(strong,nonatomic) IBOutlet UILabel *lblCellSeparator;

@end
