//
//  SalesWorxCustomerAgeingReportPDFTemplateView.m
//  MedRep
//
//  Created by Prasann on 10/5/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCustomerAgeingReportPDFTemplateView.h"
#import "SalesWorxCustomerAgeingReportPDFTableViewCell.h"
#import "MedRepQueries.h"
#import "SWDefaults.h"
@implementation SalesWorxCustomerAgeingReportPDFTemplateView
@synthesize pageNumber,selectedCustomer,arrCustomersAgeingReport;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxCustomerAgeingReportPDFTemplateView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame=frame;
    }
    return self;
}


-(void) UpdatePdfPgae {
    customerName.text= [selectedCustomer valueForKey:@"Customer_Name"];
    dateLabel.text= [MedRepQueries getCurrentDateInDeviceDateWithTimeDisplayFormat ];
    employeeName.text= [SWDefaults username];
    numberOfRowsInaPage = 7;
    StartIndexInCurrentPage = (pageNumber==1) ? 0 : (((pageNumber-1)*numberOfRowsInaPage));
    NSInteger numberOfPages = (arrCustomersAgeingReport.count%15) == 0 ? (arrCustomersAgeingReport.count/15) : (arrCustomersAgeingReport.count/15)+1;
    pageNumberLabel.text = [NSString stringWithFormat:@"Page %ld of %ld",(long)pageNumber,(long)numberOfPages];

    NSMutableArray * orglogoNameArary = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Value_1 AS FileName from TBL_Custom_Info where Value_2 IS NOT NULL AND Info_Type = 'ORG_LOGO' and Info_Key = (select Organization_ID from TBL_user )"];

    if(orglogoNameArary.count >0 ){
        NSString * documentsDirectory = [NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
        NSString * documentPathComponent = [[orglogoNameArary objectAtIndex:0] valueForKey:@"FileName"];
        NSString * filePath = [documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        if([[NSFileManager defaultManager]fileExistsAtPath:filePath]){
            imageView.image = [UIImage imageWithContentsOfFile:filePath];
        }else{

        }
    }

}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfRowsInaPage;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"customerAgeingReport";
    SalesWorxCustomerAgeingReportPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerAgeingReportPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    
    
    
    cell.periodLabel.text=@"PERIOD";
    cell.outstandingLabel.text=@"OUTSTANDING";

    cell.periodLabel.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.outstandingLabel.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];


    cell.seperatorLabel.backgroundColor = [UIColor blackColor];
    tableView.separatorColor = [UIColor clearColor];
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"customerAgeingReport";
    SalesWorxCustomerAgeingReportPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerAgeingReportPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSMutableDictionary *duesDict=[arrCustomersAgeingReport objectAtIndex:0];

    NSMutableArray * prevoiusMonthsArray=[[NSMutableArray alloc]init];
    
    BOOL ageingReportDisplayFormatMonthly=[SWDefaults isAgeingReportDisplayFormatMonthly];
    if (ageingReportDisplayFormatMonthly) {
        prevoiusMonthsArray=[SWDefaults fetchPreviousMonthsforAgeingReport];
    }
    
    if(indexPath.row==0)
    {
        if (ageingReportDisplayFormatMonthly) {
            [cell.periodLabel setText:[prevoiusMonthsArray objectAtIndex:0]];
        }
        else{
            [cell.periodLabel setText:NSLocalizedString(@"30 Days", nil)];
        }
        [cell.outstandingLabel setText:[duesDict currencyStringForKey:@"M0_Due"]];
    }
    else  if(indexPath.row==1)
    {
        if (ageingReportDisplayFormatMonthly) {
            [cell.periodLabel setText:[prevoiusMonthsArray objectAtIndex:1]];
        }
        else{
            [cell.periodLabel setText:NSLocalizedString(@"60 Days", nil)];
        }
        [cell.outstandingLabel setText:[duesDict currencyStringForKey:@"M1_Due"]];
    }
    else  if(indexPath.row==2)
    {
        if (ageingReportDisplayFormatMonthly) {
            [cell.periodLabel setText:[prevoiusMonthsArray objectAtIndex:2]];
        }
        else{
            [cell.periodLabel setText:NSLocalizedString(@"90 Days", nil)];
        }
        [cell.outstandingLabel setText:[duesDict currencyStringForKey:@"M2_Due"]];
    }
    else  if(indexPath.row==3)
    {
        if (ageingReportDisplayFormatMonthly) {
            [cell.periodLabel setText:[prevoiusMonthsArray objectAtIndex:3]];
        }
        else{
            [cell.periodLabel setText:NSLocalizedString(@"120 Days", nil)];
        }
        [cell.outstandingLabel setText:[duesDict currencyStringForKey:@"M3_Due"]];
    }
    else  if(indexPath.row==4)
    {
        if (ageingReportDisplayFormatMonthly) {
            [cell.periodLabel setText:[prevoiusMonthsArray objectAtIndex:4]];
        }
        else{
            [cell.periodLabel setText:NSLocalizedString(@"150 Days", nil)];
        }
        [cell.outstandingLabel setText:[duesDict currencyStringForKey:@"M4_Due"]];
    }
    else if (indexPath.row==5)
    {
        if (ageingReportDisplayFormatMonthly) {
            [cell.periodLabel setText:[prevoiusMonthsArray objectAtIndex:5]];
        }
        else{
            [cell.periodLabel setText:NSLocalizedString(@"180 Days", nil)];
        }
        [cell.outstandingLabel setText:[duesDict currencyStringForKey:@"M5_Due"]];
        
    }
    else  if(indexPath.row==6)
    {
        [cell.periodLabel setText:NSLocalizedString(@"Prior Due", nil)];
        [cell.outstandingLabel setText:[duesDict currencyStringForKey:@"Prior_Due"]];
    }
    return cell;
}

@end
