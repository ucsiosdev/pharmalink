//
//  SalesWorxCustomerAgeingReportPDFTableViewCell.h
//  MedRep
//
//  Created by Prasann on 10/5/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxCustomerAgeingReportPDFTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *periodLabel;
@property (strong, nonatomic) IBOutlet UILabel *outstandingLabel;
@property (strong, nonatomic) IBOutlet UILabel *seperatorLabel;

@end
