//
//  SalesWorxPaymentSummaryPDFTableViewCell.h
//  MedRep
//
//  Created by Neha Gupta on 10/5/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxPaymentSummaryPDFTableViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UILabel *lblSerialNumber;
@property(strong,nonatomic) IBOutlet UILabel *lblDocumentNo;
@property(strong,nonatomic) IBOutlet UILabel *lblDate;
@property(strong,nonatomic) IBOutlet UILabel *lblName;
@property(strong,nonatomic) IBOutlet UILabel *lblType;
@property(strong,nonatomic) IBOutlet UILabel *lblAmount;
@property(strong,nonatomic) IBOutlet UILabel *lblCellSeparator;

@end
