//
//  SalesWorxPaymentCollectionReceiptPDFTableViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/19/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxPaymentCollectionReceiptPDFTableViewCell : UITableViewCell
@property(strong,nonatomic) IBOutlet UILabel *lblDocumentNo;
@property(strong,nonatomic) IBOutlet UILabel *lblInvoiceDate;
@property(strong,nonatomic) IBOutlet UILabel *lblNetAmount;
@property(strong,nonatomic) IBOutlet UILabel *lblPaidAmount;
@property(strong,nonatomic) IBOutlet UILabel *lblDueDate;
@property(strong,nonatomic) IBOutlet UILabel *lblDueAmount;
@property(strong,nonatomic) IBOutlet UILabel *lblSerialNumber;
@property(strong,nonatomic) IBOutlet UILabel *lblCellSeparator;
@property (weak, nonatomic) IBOutlet UILabel *lblUnsettledAmount;

@end
