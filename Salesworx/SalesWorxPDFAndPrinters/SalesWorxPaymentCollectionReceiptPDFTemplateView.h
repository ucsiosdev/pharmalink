//
//  SalesWorxPaymentCollectionReceiptPDFTemplateView.h
//  MedRep
//
//  Created by Unique Computer Systems on 2/19/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableView.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
#import "MedRepElementDescriptionLabel.h"


@interface SalesWorxPaymentCollectionReceiptPDFTemplateView : UIView
{
    IBOutlet UILabel *receiptNumberLabel;
    IBOutlet UILabel * customerNamelabel;
    IBOutlet UILabel * employeNameLabel;
    IBOutlet UILabel * dateLabel;
    IBOutlet UILabel *totalAmount;
    IBOutlet NSLayoutConstraint *tableContentViewHeightConstraint;
    IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
    
    IBOutlet UILabel * customerAvailableBalancelabel;
    IBOutlet UILabel * oustandingpaymentlabel;
    IBOutlet UILabel * netAmountLabel;
    
    IBOutlet UIView *bankDetailsView;
    IBOutlet NSLayoutConstraint *bankDetailsViewHeightConstraint;
    
    IBOutlet UILabel * customerAvailableBalanceStringlabel;
    IBOutlet UILabel * oustandingpaymentStringlabel;
    IBOutlet UILabel * netAmountStringLabel;
    IBOutlet UILabel * pageNumberLabel;
    IBOutlet UIImageView * LogoImageView;
    
    IBOutlet SalesWorxTableView * customerStatementTableView;
    NSInteger numberOfRowsInaPage;
    NSInteger StartIndexInCurrentPage;
    
    IBOutlet MedRepElementDescriptionLabel *bankNameLabel;
    IBOutlet MedRepElementDescriptionLabel *branchNameLabel;
    IBOutlet MedRepElementDescriptionLabel *chequeNumberLabel;
    IBOutlet MedRepElementDescriptionLabel *chequeDateLabel;
    
    IBOutlet UILabel *unsettledAmountTitleLabel;
    
    IBOutlet UILabel *settledAmountTitleLabel;
    
    IBOutlet UILabel *totalAmountTitleLabel;
}
-(void)UpdatePdfPgae;
@property(strong,nonatomic) NSMutableArray *arrCustomersStatement;
@property(strong,nonatomic) NSMutableArray *unfilteredArray;

@property(strong,nonatomic) NSDictionary *collectionInfoDictionary;
@property(strong,nonatomic) NSString *paymentMethod;
@property(strong,nonatomic) NSString *dueAmount;

@property(strong,nonatomic) CustomerPaymentClass *customerPaymentDetails;

@property (strong, nonatomic) IBOutlet UILabel *paymentModeLabel;

@property (strong, nonatomic) IBOutlet UILabel *lblUnsettledAmount;

@property (strong, nonatomic) IBOutlet UILabel *lblSettledAmount;
@property (nonatomic)BOOL isFromReports;

@property(strong,nonatomic) NSDictionary *selectedCustomer;
@property (nonatomic) NSInteger pageNumber;
@property (nonatomic) NSInteger numberOfPages;


@property(strong,nonatomic) PaymentSummary *currentPaymentSummary;
@property(strong,nonatomic) NSMutableArray *invoicesArrayInReports;



@end
