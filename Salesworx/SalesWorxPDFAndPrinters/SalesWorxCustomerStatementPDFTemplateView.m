//
//  PDFPageTemplate.m
//  SalesworxCustomerstatementTest
//
//  Created by Pavan Kumar Singamsetti on 12/7/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "SalesWorxCustomerStatementPDFTemplateView.h"
#import "SalesWorxCustomerStatementPDFTableViewCell.h"
#import "MedRepQueries.h"
@implementation SalesWorxCustomerStatementPDFTemplateView
@synthesize arrCustomersStatement,selectedCustomer,pageNumber;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxCustomerStatementPDFTemplateView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame=frame;
    }
    return self;
}
-(void)UpdatePdfPgae{
    customerNamelabel.text= [selectedCustomer valueForKey:@"Customer_Name"];
    dateLabel.text= [MedRepQueries getCurrentDateInDeviceDateWithTimeDisplayFormat ];
    employeNameLabel.text= [SWDefaults username];
    numberOfRowsInaPage = 15;
    StartIndexInCurrentPage = (pageNumber==1) ? 0 : (((pageNumber-1)*numberOfRowsInaPage));
    customerAvailableBalancelabel.text = [[SWDefaults getValidStringValue:[selectedCustomer valueForKey:@"Avail_Bal"]] currencyString];
    oustandingpaymentlabel.text = [[SWDefaults getValidStringValue:[arrCustomersStatement valueForKeyPath:@"@sum.DueAmount" ]] currencyString];
    netAmountLabel.text = [[NSString stringWithFormat:@"%@",[arrCustomersStatement valueForKeyPath:@"@sum.NetAmount"]] currencyString];
    NSInteger numberOfPages = (arrCustomersStatement.count%15) == 0 ? (arrCustomersStatement.count/15) : (arrCustomersStatement.count/15)+1;
    pageNumberLabel.text = [NSString stringWithFormat:@"Page %ld of %ld",(long)pageNumber,(long)numberOfPages];
    
    if((numberOfRowsInaPage*pageNumber)<arrCustomersStatement.count){
        customerAvailableBalancelabel.hidden=YES;
        oustandingpaymentlabel.hidden=YES;
        netAmountLabel.hidden=YES;
        customerAvailableBalanceStringlabel.hidden=YES;
        oustandingpaymentStringlabel.hidden=YES;
        netAmountStringLabel.hidden=YES;

    }
    
    NSMutableArray * orglogoNameArary = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Value_1 AS FileName from TBL_Custom_Info where Value_2 IS NOT NULL AND Info_Type = 'ORG_LOGO' and Info_Key = (select Organization_ID from TBL_user )"];
    
    if(orglogoNameArary.count >0 ){
        NSString * documentsDirectory = [NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
        NSString * documentPathComponent = [[orglogoNameArary objectAtIndex:0] valueForKey:@"FileName"];
        NSString * filePath = [documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        if([[NSFileManager defaultManager]fileExistsAtPath:filePath]){
            LogoImageView.image = [UIImage imageWithContentsOfFile:filePath];
        }else{
            
        }
    }
    
}
#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfRowsInaPage;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"customerPDFStatementCellIdentifier";
    SalesWorxCustomerStatementPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerStatementPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    

    
    cell.lblSerialNumber.text=@"NO.";
    cell.lblDocumentNo.text=@"DOCUMENT NO.";
    cell.lblInvoiceDate.text=@"INVOICE DATE";
    cell.lblNetAmount.text=@"TOTAL";
    cell.lblPaidAmount.text=@"PAID";
    cell.lblDueDate.text=@"";
    cell.lblDueAmount.text=@"DUE";

    cell.lblSerialNumber.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblDocumentNo.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblInvoiceDate.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblNetAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblPaidAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblDueAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];

    cell.lblCellSeparator.backgroundColor = [UIColor blackColor];
    tableView.separatorColor = [UIColor clearColor];
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"customerPDFStatementCellIdentifier";
    SalesWorxCustomerStatementPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerStatementPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    NSInteger currentRowArrayPostion = StartIndexInCurrentPage+indexPath.row;
    
    if(currentRowArrayPostion < arrCustomersStatement.count ){
        cell.lblSerialNumber.text=[NSString stringWithFormat:@"%ld",currentRowArrayPostion+1];
        cell.lblDocumentNo.text = [SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion]valueForKey:@"Invoice_Ref_No"]];
        cell.lblInvoiceDate.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion]valueForKey:@"InvDate"]];
        cell.lblNetAmount.text = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"NetAmount"]] floatString];
        cell.lblDueAmount.text = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"DueAmount"]] floatString];

        if ([[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"PaidAmt"]) {
            cell.lblPaidAmount.text = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"PaidAmt"]] floatString];
        } else {
            cell.lblPaidAmount.text = @"0.00";
        }
        cell.lblCellSeparator.backgroundColor = [UIColor clearColor];
        tableView.separatorColor = [UIColor clearColor];
        cell.lblDueDate.text = [NSString stringWithFormat:@"DUE DATE: %@",[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion]valueForKey:@"DueDate"]]];
        
        cell.lblSerialNumber.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblDocumentNo.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblInvoiceDate.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblNetAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblPaidAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblDueDate.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblDueAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];

    }else{
        cell.lblSerialNumber.hidden=YES;
        cell.lblDocumentNo.hidden=YES;
        cell.lblInvoiceDate.hidden=YES;
        cell.lblNetAmount.hidden=YES;
        cell.lblPaidAmount.hidden=YES;
        cell.lblDueDate.hidden=YES;
        cell.lblDueAmount.hidden=YES;
        cell.lblCellSeparator.backgroundColor = [UIColor clearColor];

    }


    return cell;
}

@end
