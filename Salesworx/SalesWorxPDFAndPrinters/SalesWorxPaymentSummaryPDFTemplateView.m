//
//  SalesWorxPaymentSummaryPDFTemplateView.m
//  MedRep
//
//  Created by Neha Gupta on 10/5/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxPaymentSummaryPDFTemplateView.h"
#import "SalesWorxPaymentSummaryPDFTableViewCell.h"
#import "MedRepQueries.h"

@implementation SalesWorxPaymentSummaryPDFTemplateView
@synthesize selectedPaymentSummary;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxPaymentSummaryPDFTemplateView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame=frame;
    }
    return self;
}
-(void)UpdatePdfPgae{
    
    customerStatementTableView.layer.borderWidth = 1.0;
    customerStatementTableView.layer.borderColor = UIColor.blackColor.CGColor;
    
    customerNamelabel.text= selectedPaymentSummary.Customer_Name;
    dateLabel.text= [MedRepQueries getCurrentDateInDeviceDateWithTimeDisplayFormat ];
    employeNameLabel.text= [SWDefaults username];
 
    netAmountLabel.text = [[SWDefaults getValidStringValue:selectedPaymentSummary.Collected_Amount] floatString];
    receiptNumberLabel.text = [SWDefaults getValidStringValue:selectedPaymentSummary.Collection_Ref_No];

    NSMutableArray * orglogoNameArary = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Value_1 AS FileName from TBL_Custom_Info where Value_2 IS NOT NULL AND Info_Type = 'ORG_LOGO' and Info_Key = (select Organization_ID from TBL_user )"];
    
    NSLog(@"collected amount is %@ %@", selectedPaymentSummary.Collection_Type, selectedPaymentSummary.Amount);
    
    paymentModeLabel.text = [SWDefaults getValidStringValue:selectedPaymentSummary.Collection_Type];
    
    if([selectedPaymentSummary.Collection_Type.uppercaseString isEqualToString:@"CASH"]){
        chequeDetailsView.hidden=YES;
        chequeDetailsViewHeightConstraint.constant = 0;
    }else{
        chequeDetailsView.hidden=NO;
        chequeDetailsViewHeightConstraint.constant = 74;
        chequeNumberLbl.text=[SWDefaults getValidStringValue:selectedPaymentSummary.Cheque_No];
        chequeDateLbl.text=[SWDefaults getValidStringValue:selectedPaymentSummary.Cheque_Date];
        bankNameLabel.text=[SWDefaults getValidStringValue:selectedPaymentSummary.Bank_Name];
        branchNameLbl.text=[SWDefaults getValidStringValue:selectedPaymentSummary.Bank_Branch];
    }
    
  
    
    
//
//    NSString* totalAmountString=[arrCustomersStatement valueForKeyPath:@"@sum.settledAmountForInvoice"];
//
//    if([[AppControl retrieveSingleton].DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsYESCode]){
//        totalAmount.text=[[NSString getValidStringValue:customerPaymentDetails.customerPaidAmount] currencyString];
//        pageNumberLabel.text = [NSString stringWithFormat:@"Page 1 of 1"];
//
//    }else{
//        totalAmount.text=[[NSString getValidStringValue:totalAmountString] currencyString];
//        NSInteger numberOfPages = (arrCustomersStatement.count%15) == 0 ? (arrCustomersStatement.count/15) : (arrCustomersStatement.count/15)+1;
//        pageNumberLabel.text = [NSString stringWithFormat:@"Page %ld of %ld",(long)pageNumber,(long)numberOfPages];
//        NSDecimalNumber * unSettledAmount = [customerPaymentDetails.customerPaidAmount decimalNumberBySubtracting:customerPaymentDetails.totalAmount];
//        NSLog(@"unsettled amount in pdf is %ld", (long)unSettledAmount.integerValue);
//        lblUnsettledAmount.text = [[SWDefaults getValidStringValue:unSettledAmount.stringValue]currencyString];
//
//
//    }
    
    if(orglogoNameArary.count >0 ){
        NSString * documentsDirectory = [NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
        NSString * documentPathComponent = [[orglogoNameArary objectAtIndex:0] valueForKey:@"FileName"];
        NSString * filePath = [documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        if([[NSFileManager defaultManager]fileExistsAtPath:filePath]){
            LogoImageView.image = [UIImage imageWithContentsOfFile:filePath];
        }else{
            
        }
    }
    
}
#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"paymentSummaryCellIdentifier";
    SalesWorxPaymentSummaryPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentSummaryPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];

    cell.lblSerialNumber.text=@"NO.";
    cell.lblDocumentNo.text=@"DOCUMENT NO.";
    cell.lblDate.text=@"DATE";
    cell.lblName.text=@"NAME";
    cell.lblType.text=@"TYPE";
    cell.lblAmount.text=@"AMOUNT";
    
    cell.lblSerialNumber.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblDocumentNo.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblDate.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblName.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblType.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    
    cell.lblCellSeparator.backgroundColor = [UIColor blackColor];
    tableView.separatorColor = [UIColor clearColor];

    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"paymentSummaryCellIdentifier";
    SalesWorxPaymentSummaryPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentSummaryPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.lblSerialNumber.text = @"1";
    cell.lblDocumentNo.text = [SWDefaults getValidStringValue:selectedPaymentSummary.Collection_Ref_No];
    cell.lblDate.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:selectedPaymentSummary.Collected_OnWithTime];
   // cell.lblName.text = [SWDefaults getValidStringValue:selectedPaymentSummary.Customer_Name];
    cell.lblType.text = [SWDefaults getValidStringValue:selectedPaymentSummary.Collection_Type];
    if ([selectedPaymentSummary.Collection_Type isEqualToString:@"POST-CHQ"]) {
        cell.lblType.text = @"PDC";
    }
    cell.lblAmount.text = [[SWDefaults getValidStringValue:selectedPaymentSummary.Collected_Amount] floatString];
 
    cell.lblSerialNumber.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    cell.lblDocumentNo.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    cell.lblDate.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    //cell.lblName.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    cell.lblType.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    cell.lblAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];

    return cell;
}

@end
