//
//  PDFPageTemplate.m
//  SalesworxCustomerstatementTest
//
//  Created by Unique Computer Systems.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "SalesWorxPaymentCollectionReceiptPDFTemplateView.h"
#import "SalesWorxPaymentCollectionReceiptPDFTableViewCell.h"
#import "MedRepQueries.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"

@implementation SalesWorxPaymentCollectionReceiptPDFTemplateView
@synthesize arrCustomersStatement,selectedCustomer,pageNumber,unfilteredArray,paymentMethod,collectionInfoDictionary,customerPaymentDetails,paymentModeLabel,lblSettledAmount,lblUnsettledAmount,isFromReports,dueAmount,currentPaymentSummary,numberOfPages,invoicesArrayInReports;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxPaymentCollectionReceiptPDFTemplateView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame=frame;
        customerStatementTableView.layer.borderWidth = 1.0;
        customerStatementTableView.layer.borderColor = [UIColor blackColor].CGColor;
    }
    return self;
}

-(int)getUserProfileNumberOfDecimalsStr{
    return [[[SWDefaults userProfile]valueForKey:@"Decimal_Digits"] intValue];
}
-(void)UpdatePdfPgae{
    
#if DEBUG
    //[AppControl retrieveSingleton].DISABLE_COLL_INV_SETTLEMENT = KAppControlsYESCode;
#endif
    
    if (isFromReports){
        
        if([[AppControl retrieveSingleton].DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsYESCode]){
            totalAmount.text=[[NSString getValidStringValue:currentPaymentSummary.Total_Amount] currencyString];
            pageNumberLabel.text = [NSString stringWithFormat:@"Page 1 of 1"];
            lblUnsettledAmount.text = @"";
            unsettledAmountTitleLabel.text =@"";
            settledAmountTitleLabel.text = @"";
            lblSettledAmount.text = @"";

            
        }else{
           // totalAmount.text=[[NSString getValidStringValue:currentPaymentSummary.Total_Amount] currencyString];
            
            totalAmount.text=[[SWDefaults getValidStringValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],currentPaymentSummary.Total_Amount.doubleValue]]]currencyString];

            pageNumberLabel.text = [NSString stringWithFormat:@"Page %ld of %ld",(long)pageNumber,(long)numberOfPages];
            
            if ([[AppControl retrieveSingleton].ALLOW_EXCESS_CASH_COLLECTION isEqualToString:KAppControlsYESCode]){
            NSDecimalNumber * totalAmountNumber = [NSDecimalNumber decimalNumberWithString:currentPaymentSummary.Total_Amount];
            NSMutableArray* invoiceAmounts= [invoicesArrayInReports valueForKey:@"Invoice_Amount"];
            NSDecimalNumber* sumOfInvoices = [NSDecimalNumber zero];
            for (NSString* currentValue in invoiceAmounts){
                if(![NSString isEmpty:currentValue]){
                    sumOfInvoices = [sumOfInvoices decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:currentValue]];
                }
            }
                if ([sumOfInvoices isEqualToNumber:totalAmountNumber]){
                    lblUnsettledAmount.text = @"";
                    unsettledAmountTitleLabel.text = @"";
                }else{
                    NSDecimalNumber * unSettledAmount = [totalAmountNumber decimalNumberBySubtracting:sumOfInvoices];
                    NSLog(@"unsettled amount in pdf is %ld", (long)unSettledAmount.integerValue);
                    //lblUnsettledAmount.text = [[SWDefaults getValidStringValue:unSettledAmount.stringValue]currencyString];
                    lblUnsettledAmount.text=[[SWDefaults getValidStringValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],unSettledAmount.doubleValue]]]currencyString];

                }
                //lblSettledAmount.text = [[SWDefaults getValidStringValue:sumOfInvoices.stringValue]currencyString];
                lblSettledAmount.text=[[SWDefaults getValidStringValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],sumOfInvoices.doubleValue]]]currencyString];

            }else{
                lblUnsettledAmount.text = @"";
                unsettledAmountTitleLabel.text =@"";
                settledAmountTitleLabel.text = @"";
                lblSettledAmount.text = @"";
                
            }
        }
        paymentModeLabel.text= [[SWDefaults getValidStringValue:currentPaymentSummary.Payment_Mode]uppercaseString];
        if([currentPaymentSummary.Payment_Mode.uppercaseString isEqualToString:@"CASH"])
        {
            bankDetailsView.hidden=YES;
            bankDetailsViewHeightConstraint.constant=0.0f;
            tableContentViewHeightConstraint.constant=716.0f;
            tableViewHeightConstraint.constant=607;
        }
        else
        {
            bankNameLabel.text = [[SWDefaults getValidStringValue:currentPaymentSummary.Bank_Name]uppercaseString];
            branchNameLabel.text = [[SWDefaults getValidStringValue:currentPaymentSummary.Bank_Branch] uppercaseString];
            chequeNumberLabel.text = [SWDefaults getValidStringValue:currentPaymentSummary.Cheque_No];
            
            //old code
            //chequeDateLabel.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:currentPaymentSummary.Cheque_Date]];
            
            /******** Ravinder code, 12 June 2019, Check both types date format (with time and without time)
             
             for fix this bug :http://ucssolutions.no-ip.biz:3222/ucsbugtracker/edit_bug.aspx?id=9162
             
             *************/
            
            
            if ([MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:currentPaymentSummary.Cheque_Date]] != nil){
                
                chequeDateLabel.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:currentPaymentSummary.Cheque_Date]];
            }
            else if ([MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:currentPaymentSummary.Cheque_Date]] != nil){
                chequeDateLabel.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:currentPaymentSummary.Cheque_Date]];
            }
            
            

        }
        receiptNumberLabel.text = [SWDefaults getValidStringValue:currentPaymentSummary.Receipt_Number];
        
    }else{
    //customerAvailableBalancelabel.text = [[SWDefaults getValidStringValue:[selectedCustomer valueForKey:@"Avail_Bal"]] currencyString];
   // netAmountLabel.text = [[NSString stringWithFormat:@"%@",[arrCustomersStatement valueForKeyPath:@"@sum.NetAmount"]] currencyString];
    
    NSString* totalAmountString=[arrCustomersStatement valueForKeyPath:@"@sum.settledAmountForInvoice"];
    if([[AppControl retrieveSingleton].DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsYESCode]){
        //totalAmount.text=[[NSString getValidStringValue:customerPaymentDetails.customerPaidAmount] currencyString];
        
        totalAmount.text=[[SWDefaults getValidStringValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],customerPaymentDetails.customerPaidAmount.doubleValue]]]currencyString];

        pageNumberLabel.text = [NSString stringWithFormat:@"Page 1 of 1"];
        lblUnsettledAmount.text = @"";
        unsettledAmountTitleLabel.text =@"";
        settledAmountTitleLabel.text = @"";
        lblSettledAmount.text = @"";


    }else{
        //if settlement is disabled/ excess collection is done then this total amount will fail.
        totalAmount.text=[[NSString getValidStringValue:totalAmountString] currencyString];
        NSInteger numberOfPages = (arrCustomersStatement.count%13) == 0 ? (arrCustomersStatement.count/13) : (arrCustomersStatement.count/13)+1;
        pageNumberLabel.text = [NSString stringWithFormat:@"Page %ld of %ld",(long)pageNumber,(long)numberOfPages];
        if ([[AppControl retrieveSingleton].ALLOW_EXCESS_CASH_COLLECTION isEqualToString:KAppControlsYESCode] || [currentPaymentSummary.Payment_Mode.uppercaseString isEqualToString:@"CURRENT CHEQUE"] || [currentPaymentSummary.Payment_Mode.uppercaseString isEqualToString:@"PDC"]){
            totalAmount.text = [[SWDefaults getValidStringValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],customerPaymentDetails.customerPaidAmount.doubleValue]]]currencyString];
            
            NSDecimalNumber * sumOfInvoices = [NSDecimalNumber zero];
            NSMutableArray* settledAmountArray = [customerPaymentDetails.invoicesArray valueForKey:@"settledAmountForInvoice"];
            for (NSDecimalNumber* currentValue in settledAmountArray){
                if(![currentValue isEqualToNumber:[NSDecimalNumber zero]]){
                    sumOfInvoices = [sumOfInvoices decimalNumberByAdding:currentValue];
                }
            }
            if ([customerPaymentDetails.customerPaidAmount isEqualToNumber:sumOfInvoices]){
                lblUnsettledAmount.text = @"";
                unsettledAmountTitleLabel.text = @"";
            }else{
                NSDecimalNumber * unSettledAmount = [customerPaymentDetails.customerPaidAmount decimalNumberBySubtracting:sumOfInvoices];
                NSLog(@"unsettled amount in pdf is %ld", (long)unSettledAmount.integerValue);
               // lblUnsettledAmount.text = [[SWDefaults getValidStringValue:unSettledAmount.stringValue]currencyString];
                lblUnsettledAmount.text=[[SWDefaults getValidStringValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],unSettledAmount.doubleValue]]]currencyString];

            }
            lblSettledAmount.text=[[SWDefaults getValidStringValue:[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],sumOfInvoices.doubleValue]]]currencyString];

        }else{
            lblUnsettledAmount.text = @"";
            unsettledAmountTitleLabel.text =@"";
            settledAmountTitleLabel.text = @"";
            lblSettledAmount.text = @"";
        }
    }
    oustandingpaymentlabel.text = [[SWDefaults getValidStringValue:[unfilteredArray valueForKeyPath:@"@sum.DueAmount" ]] currencyString];
    paymentModeLabel.text= [[SWDefaults getValidStringValue:customerPaymentDetails.paymentMethod]uppercaseString];
    if((numberOfRowsInaPage*pageNumber)<arrCustomersStatement.count){
        customerAvailableBalancelabel.hidden=YES;
        oustandingpaymentlabel.hidden=YES;
        netAmountLabel.hidden=YES;
        customerAvailableBalanceStringlabel.hidden=YES;
        oustandingpaymentStringlabel.hidden=YES;
        netAmountStringLabel.hidden=YES;
    }
    if([paymentMethod isEqualToString:@"Cash"])
    {
        bankDetailsView.hidden=YES;
        bankDetailsViewHeightConstraint.constant=0.0f;
        tableViewHeightConstraint.constant=607;
    }
    else
    {
        bankNameLabel.text = [NSString isEmpty:[NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Bank_Name"]]]?KNotApplicable:[NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Bank_Name"]];
        branchNameLabel.text = [NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Bank_Branch"]];
        chequeNumberLabel.text = [NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Cheque_No"]];
        
        //old code
        //chequeDateLabel.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Cheque_Date"]]];
        
         /******************
         Ravinder code, 12 June 2019, Check both types date format (with time and without time) for fix this bug :http://ucssolutions.no-ip.biz:3222/ucsbugtracker/edit_bug.aspx?id=9162
        
          ********/
        
        if ([MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Cheque_Date"]]] != nil ){
             chequeDateLabel.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Cheque_Date"]]];
        }
        else if ([MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Cheque_Date"]]] != nil ){
            chequeDateLabel.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[NSString getValidStringValue:[collectionInfoDictionary valueForKey:@"Cheque_Date"]]];
        }
        
        

    }
    receiptNumberLabel.text = [SWDefaults lastCollectionReference];
    }
    numberOfRowsInaPage = 13;
    StartIndexInCurrentPage = (pageNumber==1) ? 0 : (((pageNumber-1)*numberOfRowsInaPage));
    employeNameLabel.text= [[SWDefaults username]uppercaseString];
    customerNamelabel.text= [selectedCustomer valueForKey:@"Customer_Name"];
    dateLabel.text= [MedRepQueries getCurrentDateInDeviceDateWithTimeDisplayFormat ];

    NSMutableArray * orglogoNameArary = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Value_1 AS FileName from TBL_Custom_Info where Value_2 IS NOT NULL AND Info_Type = 'ORG_LOGO' and Info_Key = (select Organization_ID from TBL_user )"];
    
    if(orglogoNameArary.count >0 ){
        NSString * documentsDirectory = [NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
        NSString * documentPathComponent = [[orglogoNameArary objectAtIndex:0] valueForKey:@"FileName"];
        NSString * filePath = [documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        if([[NSFileManager defaultManager]fileExistsAtPath:filePath]){
            LogoImageView.image = [UIImage imageWithContentsOfFile:filePath];
        }else{
            
        }
    }
    
}
#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfRowsInaPage;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"receiptCell";
    SalesWorxPaymentCollectionReceiptPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
//        if( [[AppControl retrieveSingleton].ALLOW_EXCESS_CASH_COLLECTION isEqualToString:KAppControlsYESCode]){
//            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentCollectionReceiptPDFTableViewCell" owner:nil options:nil] lastObject];
//        }else{
//        }
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentCollectionReceiptPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    
    
    
    cell.lblSerialNumber.text=@"NO.";
    cell.lblDocumentNo.text=@"DOCUMENT NO.";
    cell.lblInvoiceDate.text=@"INVOICE DATE";
    cell.lblNetAmount.text=@"TOTAL";
    cell.lblPaidAmount.text=@"AMOUNT";
    cell.lblDueDate.text=@"";
    cell.lblDueAmount.text=@"DUE";
    //cell.lblUnsettledAmount.text = @"UNSETTLED AMOUNT";
    
    cell.lblSerialNumber.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblDocumentNo.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblInvoiceDate.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblNetAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblPaidAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    cell.lblDueAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    //cell.lblUnsettledAmount.font=[UIFont systemFontOfSize:14 weight:UIFontWeightBold];

    cell.lblCellSeparator.backgroundColor = [UIColor blackColor];
    tableView.separatorColor = [UIColor clearColor];
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"receiptCell";
    SalesWorxPaymentCollectionReceiptPDFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
//        if( [[AppControl retrieveSingleton].ALLOW_EXCESS_CASH_COLLECTION isEqualToString:KAppControlsYESCode]){
//            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentCollectionReceiptPDFTableViewCell" owner:nil options:nil] lastObject];
//        }else{
//        }
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentCollectionReceiptPDFTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSInteger currentRowArrayPostion = StartIndexInCurrentPage+indexPath.row;


    if (isFromReports){
        if(currentRowArrayPostion < invoicesArrayInReports.count ){
        PaymentSummary* currentInvData = [invoicesArrayInReports objectAtIndex:indexPath.row];
            cell.lblSerialNumber.text=[NSString stringWithFormat:@"%d",currentRowArrayPostion+1];
        cell.lblDocumentNo.text = [SWDefaults getValidStringValue:currentInvData.Invoice_No];
        cell.lblInvoiceDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:mm:ss" destFormat:kDateFormatWithoutTime scrString:currentInvData.Invoice_Date];
        cell.lblPaidAmount.text = [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],currentInvData.Invoice_Amount.doubleValue]];
        cell.lblCellSeparator.backgroundColor = [UIColor clearColor];
        tableView.separatorColor = [UIColor clearColor];
        cell.lblSerialNumber.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblDocumentNo.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblInvoiceDate.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblPaidAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        }else{
            cell.lblSerialNumber.hidden=YES;
            cell.lblDocumentNo.hidden=YES;
            cell.lblInvoiceDate.hidden=YES;
            cell.lblNetAmount.hidden=YES;
            cell.lblPaidAmount.hidden=YES;
            cell.lblDueDate.hidden=YES;
            cell.lblDueAmount.hidden=YES;
            cell.lblUnsettledAmount.hidden=YES;
            cell.lblCellSeparator.backgroundColor = [UIColor clearColor];
        }

    }else{
    if(currentRowArrayPostion < arrCustomersStatement.count ){
        

        cell.lblSerialNumber.text=[NSString stringWithFormat:@"%d",currentRowArrayPostion+1];
        cell.lblDocumentNo.text = [SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion]valueForKey:@"invoiceNumber"]];
        cell.lblInvoiceDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:mm:ss" destFormat:kDateFormatWithoutTime scrString:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion]valueForKey:@"InvoiceDate"]];
        cell.lblNetAmount.text = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"dueNetAmount"]] floatString];
        //cell.lblDueAmount.text = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"DueAmount"]] floatString];
        
        if ([[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"settledAmountForInvoice"]) {
            cell.lblPaidAmount.text = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"settledAmountForInvoice"]] floatString];
        } else {
            cell.lblPaidAmount.text = @"";
            cell.lblSerialNumber.text = @"";
        }
        
        double totalAmount = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"dueNetAmount"]]doubleValue];
        
        
//        double previousSettledAmount =
//        [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"previousSettledAmount"]] doubleValue];
        
        double paidAmount = [[SWDefaults getValidStringValue:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion] valueForKey:@"settledAmountForInvoice"]] doubleValue];
        
        

        
        double dueAmount = totalAmount-paidAmount;
        
        cell.lblDueAmount.text = [[NSString stringWithFormat:@"%0.2f",dueAmount]floatString];
        
        
        cell.lblCellSeparator.backgroundColor = [UIColor clearColor];
        tableView.separatorColor = [UIColor clearColor];
        cell.lblDueDate.text = [NSString stringWithFormat:@"DUE DATE: %@",[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:mm:ss" destFormat:kDateFormatWithoutTime scrString:[[arrCustomersStatement objectAtIndex:currentRowArrayPostion]valueForKey:@"dueDate"]]];
        
        cell.lblSerialNumber.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblDocumentNo.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblInvoiceDate.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblNetAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblPaidAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblDueDate.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        cell.lblDueAmount.font=[UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        
    }else{
        cell.lblSerialNumber.hidden=YES;
        cell.lblDocumentNo.hidden=YES;
        cell.lblInvoiceDate.hidden=YES;
        cell.lblNetAmount.hidden=YES;
        cell.lblPaidAmount.hidden=YES;
        cell.lblDueDate.hidden=YES;
        cell.lblDueAmount.hidden=YES;
        cell.lblUnsettledAmount.hidden=YES;
        cell.lblCellSeparator.backgroundColor = [UIColor clearColor];
        
    }
    }
    
    
    return cell;
}

@end

