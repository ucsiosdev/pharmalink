//
//  SalesWorxPaymentSummaryPDFTemplateView.h
//  MedRep
//
//  Created by Neha Gupta on 10/5/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableView.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"

@interface SalesWorxPaymentSummaryPDFTemplateView : UIView
{
    IBOutlet UILabel * customerNamelabel;
    IBOutlet UILabel * employeNameLabel;
    IBOutlet UILabel * dateLabel;
    
    IBOutlet UILabel * netAmountLabel;
    
    IBOutlet UIView *chequeDetailsView;
    IBOutlet UILabel *receiptNumberLabel;
    
    IBOutlet UILabel * netAmountStringLabel;
    IBOutlet UIImageView * LogoImageView;
    
    IBOutlet UILabel *paymentModeLabel;
    
    IBOutlet UILabel *unsettledAmountLbl;
    
    IBOutlet UILabel *chequeDateLbl;
    
    IBOutlet UILabel *bankNameLabel;
    
    IBOutlet UILabel *branchNameLbl;
    IBOutlet NSLayoutConstraint *chequeDetailsViewHeightConstraint;
    
    IBOutlet UILabel *chequeNumberLbl;
    
    
    IBOutlet SalesWorxTableView * customerStatementTableView;
}

-(void)UpdatePdfPgae;
@property(strong,nonatomic) Payment_Summary *selectedPaymentSummary;

@end
