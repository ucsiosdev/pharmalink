//
//  SalesWorxCustomerAgeingReportPDFTemplateView.h
//  MedRep
//
//  Created by Prasann on 10/5/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableView.h"
@interface SalesWorxCustomerAgeingReportPDFTemplateView : UIView{
    
    IBOutlet UILabel *customerName;
    IBOutlet UILabel *dateLabel;
    IBOutlet UILabel *employeeName;
    IBOutlet UIImageView *imageView;
    IBOutlet UILabel *pageNumberLabel;
    IBOutlet SalesWorxTableView *ageingReportTableView;
    
    NSInteger numberOfRowsInaPage;
    NSInteger StartIndexInCurrentPage;
    
}

-(void)UpdatePdfPgae;
@property(strong,nonatomic) NSMutableArray *arrCustomersAgeingReport;
@property(strong,nonatomic) NSDictionary *selectedCustomer;
@property (nonatomic) NSInteger pageNumber;

@end
