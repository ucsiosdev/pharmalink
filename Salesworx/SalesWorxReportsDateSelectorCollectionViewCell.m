//
//  SalesWorxReportsDateSelectorCollectionViewCell.m
//  CustomReportDateSelector
//
//  Created by Pavan Kumar Singamsetti on 11/2/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "SalesWorxReportsDateSelectorCollectionViewCell.h"

@implementation SalesWorxReportsDateSelectorCollectionViewCell
- (void)awakeFromNib {
    [super awakeFromNib];

}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxReportsDateSelectorCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
    
}
@end
