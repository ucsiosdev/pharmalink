//
//  SalesWorxMerchandisingViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 3/23/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"
#import "SalesWorxSearchBar.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxImageView.h"
#import "MedRepElementDescriptionLabel.h"
#import <MapKit/MapKit.h>
#import "SalesWorxImageBarButtonItem.h"
#import "SalesWorxCustomClass.h"
#import "SWCustomersFilterViewController.h"
#import "LastVisitDetailsViewController.h"
#import "SalesWorxiBeaconManager.h"
#import "NSString+Additions.h"
#import "SalesWorxCustomerOnsiteVisitOptionsReasonViewController.h"


@interface SalesWorxMerchandisingViewController : UIViewController <UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate> 
{
    AppControl *appControl;
    SalesWorxImageBarButtonItem *startVisitButton;
    Customer *selectedCustomer;
    
  
    IBOutlet UITableView *customersTableView;
    IBOutlet UIButton *customersFilterButton;
    IBOutlet SalesWorxSearchBar *customersSearchBar;
    
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *customerViewTopConstraint;
    
    
    IBOutlet UIView *customerHeaderView;
    IBOutlet MedRepHeader *detailsCustomerNameLbl;
    IBOutlet MedRepProductCodeLabel *detailsCustomerCodeLbl;
    IBOutlet SalesWorxImageView *customerStatusImageView;
    
    
    IBOutlet MedRepElementTitleLabel *lastVisitedOnHeaderLabel;
    IBOutlet MedRepElementDescriptionLabel *lastVisitedDateLabel;
    IBOutlet UIButton *lastvisitDetailsButton;
    
  
    IBOutlet MedRepElementDescriptionLabel *customeraddressLbl;
    IBOutlet MedRepElementDescriptionLabel *customerCityLbl;
    IBOutlet MedRepElementDescriptionLabel *telephoneNumberLbl;
    IBOutlet MKMapView *customerLocationMapView;
    
    
    UIImageView *blurredBgImage;
    NSIndexPath *selectedIndexPath;
    BOOL isSearching;
    UIPopoverController *filterPopOverController;
   
    
    NSMutableArray *customersArray;
    NSMutableArray *filteredCustomers;
    NSMutableArray *indexPathArray;
    NSMutableArray *unfilteredCustomersArray;
    NSMutableDictionary *previousFilterParameters;
    
    
    BOOL isCustomerLocationValid;
    BOOL beaconValidationStatus;
    BOOL validateBeacon;
    BOOL customerLocationValidationDone;
    SalesWorxVisit *salesWorxVisit;
    UIAlertView* openVisitAlert;
    
    NSString* openPlannedVisitID,*openVisitID,*openPlannedDetailID;
}


@end
