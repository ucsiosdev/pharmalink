//
//  MedRepProductVideosViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/17/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepProductVideosViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepProductsHomeCollectionViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>
#import "AppControl.h"
#import "MedRepQueries.h"

@interface MedRepProductVideosViewController ()

@end

@implementation MedRepProductVideosViewController

@synthesize productVideosArray,customPageControl,currentImageIndex,imagesScrollView,selectedIndexPath,productVideosCollectionView,productDataArray,isViewing,moviePlayerView,currentVisit,isCurrentUserPharmacy,isCurrentUserDoctor;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchEmailID];
    
    viewedVideos=[[NSMutableArray alloc]init];
    
    cancelButton.hidden=YES;
    demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_VIDEO_DEFAULT_DEMO_TIME.integerValue;

    //make a file name to write the data to using the documents directory:
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
 
    if (firstFileName.length>0) {
        
    }
    else
    {
        firstFileName = [documentsDirectory stringByAppendingPathComponent:[[productDataArray  objectAtIndex:currentImageIndex]valueForKey:@"File_Name"]];
    }
    
    
    [self reloadHeaderMoviewView:firstFileName];
    
    // Do any additional setup after loading the view from its nib.
    
    
    if (productDataArray.count>0) {
        [productDataArray setValue:@"N" forKey:@"isSelected"];
    }
    
}

-(void)fetchEmailID{
    
    emailIDForAutoInsert = @"";
    
    if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
        
        if (isCurrentUserDoctor){
            emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaDoctorID:self.doctor_ID];
        }else  if (isCurrentUserPharmacy){
            emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaContactID:self.contact_ID];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadProductDetailsWithSelectedIndex:(NSInteger)selectedIndex

{
    
    
    productTitle.text=[NSString stringWithFormat:@"%@", [[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Name"]];
    
    
   
    
    
//    selectedCellIndex=selectedIndex;
//    
//    [productVideosCollectionView reloadData];
    
    
//    [productVideosCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:selectedIndex inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
//    
//
//         
// [productVideosCollectionView.delegate collectionView:productVideosCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:selectedIndex inSection:0]];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingProductVideos];
    }
    
    [self loadProductDetailsWithSelectedIndex:currentImageIndex];

    if (currentImageIndex>0) {
        
    }
    
    else
    {
       // [self loadProductDetailsWithSelectedIndex:0];

    }
    
    NSLog(@"video frame %@", NSStringFromCGRect(moviePlayerView.frame));
    
    [productVideosCollectionView registerClass:[MedRepProductsHomeCollectionViewCell class] forCellWithReuseIdentifier:@"productHomeCell"];
    
    

    
   // NSLog(@"file name is %@", firstFileName);
    
    NSString* mediaType = [[productDataArray valueForKey:@"Media_Type"] objectAtIndex:0];

    NSLog(@"media type %@", mediaType);
    
    
    
    
    if ([mediaType isEqualToString:@"Video"]) {
        
        
        
        
        
        
//        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:firstFileName]];
//        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//        CMTime time = CMTimeMake(1, 1);
//        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
//        CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
//        
//        
//        UIImageView * thumbNailImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 993, 420)];
//        thumbNailImageView.image=[UIImage imageNamed:@"playThumbnail.png"];
//        
//        moviePlayerThumbnailImageView.contentMode=UIViewContentModeScaleToFill;
//        moviePlayerThumbnailImageView.image=thumbnail;
//        NSLog(@"movie player subviews %@", [moviePlayerView.subviews description]);
//        
//        if (moviePlayerView.subviews.count>1) {
//            
//            [[moviePlayerView.subviews lastObject]removeFromSuperview];
//
//        }
//        
//        [moviePlayerView addSubview:thumbNailImageView];
        
    }

    
    
    
    selectedIndexPathArray=[[NSMutableArray alloc]init];
    selectedCellIndex=0;
    
    productVideosCollectionView.backgroundColor = [UIColor clearColor];
    productVideosCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    UITapGestureRecognizer * movieTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(movieTapped)];
    [moviePlayerView addGestureRecognizer:movieTap];
    
    if ([[AppControl retrieveSingleton].DISABLE_EMAIL_PRODUCT_MEDIA isEqualToString:KAppControlsYESCode]){
        
        isViewing = YES;
    }else{
        isViewing = NO;
    }
    
    
    if (isViewing==YES) {
        
        [emailButton setHidden:YES];
    }
    else
    {
        [emailButton setHidden:NO];
    }

    
    if ([productVideosCollectionView.delegate respondsToSelector:@selector(collectionView:didSelectItemAtIndexPath:)])
    {
        if (selectedIndexPath) {
            [productVideosCollectionView selectItemAtIndexPath:selectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
            [productVideosCollectionView.delegate collectionView:productVideosCollectionView didSelectItemAtIndexPath:selectedIndexPath];
        }        
    }
}


-(void)moviePlayBackDidFinish

{
    NSLog(@"selected movie delegate called");
    
    if(self.isFromProductsScreen)
    {
        [self reloadHeaderMoviewView:firstFileName];
    }
    else{
        NSDate *currentTime = [NSDate date];
        NSTimeInterval viewedTime1 = [currentTime timeIntervalSinceDate:playBackTime];
        NSLog(@"viewed Time for movie = %f", viewedTime1);
        long ti = lroundf(viewedTime1);
        int viewedTime = ti % 60;
        
        ProductMediaFile* selectedMedia=[productDataArray objectAtIndex:currentImageIndex];
        if (selectedMedia.viewedTime.length == 0) {
            selectedMedia.viewedTime=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%ld",(long)viewedTime]];
        } else {
            selectedMedia.viewedTime = [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%0.2f",(viewedTime + selectedMedia.viewedTime.doubleValue)]];
        }
        if (selectedMedia.discussedAt.length == 0) {
            selectedMedia.discussedAt = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        }
        [viewedVideos addObject:selectedMedia];
        NSLog(@"viewed videos are %@",viewedVideos);
        [self reloadHeaderMoviewView:firstFileName];
    }
}

-(void)movieTapped
{
   
    NSLog(@"movie player tapped");
//    NSURL* videoURL = [NSURL fileURLWithPath:firstFileName];
//    MPMoviePlayerController* moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
//    [self presentViewController:moviePlayerController animated:YES completion:nil];

//    [moviePlayerController.view setFrame:CGRectMake(0, 0, 993, 420)];
//    [moviePlayerView addSubview:moviePlayer.view];
//    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:moviePlayer.player];
    
    NSURL* documentUrl=[NSURL fileURLWithPath:firstFileName];
    
    NSLog(@"document url is %@", documentUrl);
    
    AVPlayer *avPlayer = [AVPlayer playerWithURL:documentUrl];
    moviePlayer = [[AVPlayerViewController alloc]init];
    moviePlayer.player = avPlayer;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:moviePlayer.player];
    
    
    
    [self presentViewController:moviePlayer animated:YES completion:^{
        self->playBackTime=[NSDate date];
    }];
    
    
    
}

-(void)playMoviewithFileName:(NSString*)fileName
{
    NSURL* documentUrl=[NSURL fileURLWithPath:fileName];
    
    NSLog(@"document url is %@", documentUrl);
    
    AVPlayer *avPlayer = [AVPlayer playerWithURL:documentUrl];
    moviePlayer = [[AVPlayerViewController alloc]init];
    moviePlayer.player = avPlayer;
    
//    moviePlayer = [[MPMoviePlayerViewController alloc]initWithContentURL:documentUrl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish)
        name:AVPlayerItemDidPlayToEndTimeNotification object:moviePlayer.player];
    
    
    
    [self presentViewController:moviePlayer animated:YES completion:nil];

}

- (IBAction)emailButtonTapped:(id)sender {
    
    
    isEmailTapped=YES;
    cancelButton.hidden=NO;
    
    buttonTitle = [sender titleForState:UIControlStateNormal];
    
    
    
    
//    NSPredicate * selectedPredicate = [NSPredicate predicateWithFormat:@"SELF.isSelected == Y"];
//    
//    NSMutableArray * selectedMediaContentArray=[[productDataArray  filteredArrayUsingPredicate:selectedPredicate] mutableCopy];
//    
//    NSLog(@"select media content count is %lu", (unsigned long)selectedMediaContentArray.count);
    

   
    
    if (buttonTitle.length>0 && sendEmail==YES && selectedIndexPathArray.count>0 ) {
        
        [self sendEmail];
    }
    else
    {
       
        
        UIAlertView * emailAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Email", nil) message:NSLocalizedString(@"Please select the media you would like to send as an Email, you can select multiple media at a time", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [emailAlert show];
        
    }
    
    
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    isEmailTapped=NO;
    cancelButton.hidden=YES;
    sendEmail=NO;
    for (NSMutableDictionary * currentDict in productDataArray) {
        NSLog(@"dict being removed is %@", currentDict);
    }
    
    [productDataArray setValue:@"N" forKey:@"isSelected"];
    
    [selectedIndexPathArray removeAllObjects];
    [productVideosCollectionView reloadData];
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];
    
}

- (IBAction)closeButtontapped:(id)sender {
    
    if(self.isFromProductsScreen)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
    NSLog(@"viewed vidoes in close button %@", [viewedVideos description]);
    
    
    if ([self.videosViewedDelegate respondsToSelector:@selector(videosViewedinFullScreenDemo:)]  ) {
        
        NSLog(@"delegate called in close button");
        
        
        [self.videosViewedDelegate videosViewedinFullScreenDemo: viewedVideos];
    }
    
    if ([self.videosViewedDelegate respondsToSelector:@selector(fullScreenVideosDidCancel)]  ) {
        
        [self.videosViewedDelegate fullScreenVideosDidCancel];
    }
    
    
        if ([self.videosViewedDelegate respondsToSelector:@selector(setBorderOnLastViewedImage:)]) {
            [self.videosViewedDelegate setBorderOnLastViewedImage:currentImageIndex];
        }
        
    NSLog(@"viewed images are %@", [ viewedVideos description]);
    [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark UICOllectionview methods


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(147, 150);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  productDataArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    //    static NSString *cellIdentifier = @"productCell";
    static NSString *cellIdentifier = @"productHomeCell";
    
    NSString* mediaType = [[productDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row];
    
    NSLog(@"videos cell for item called ");
    
    
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    cell.placeHolderImageView.hidden=YES;
    
    cell.selectedImgView.image=nil;

    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[productDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
    
   // NSLog(@"file name is %@", fileName);
    
    NSLog(@"media type %@", mediaType);
    
    
    if ([mediaType isEqualToString:@"Video"]) {
        
        
        NSLog(@"current cell frame is %@", NSStringFromCGRect(cell.contentView.frame));
        
//        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:fileName]];
//        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//        CMTime time = CMTimeMake(1, 1);
//        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
//        CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
//        
        cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
        
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        
        
        
        cell.placeHolderImageView.image=[UIImage imageNamed:@"MedRep_VideoThumbnail"];
        
        
        
        
        cell.captionLbl.text=[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];

        
        UIImageView * thumbNailImageView=[[UIImageView alloc]initWithFrame:cell.contentView.frame];
        thumbNailImageView.image=[UIImage imageNamed:@"MedRep_PlayThumbnailCell"];
        
       // [cell.contentView addSubview:thumbNailImageView];
        
        

    }
    
    
    
    cell.backgroundColor=[UIColor clearColor];
    
    
    if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"N"]) {
        cell.selectedImgView.image=nil;
    }
    else
    {
        cell.selectedImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
    }
    
    
//    if ([[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"]==nil) {
//        
//        
//        cell.selectedImgView.image=nil;
//        
//        
//    }
//    
//    
//    else
//    {
//        cell.selectedImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
//    }
    

    
    if (selectedCellIndex==indexPath.row) {
        
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        
    }
    
    else
    {
        cell.layer.borderWidth=0;
        
    }
    
    
    
    
    return cell;
    
    
}


-(void)reloadHeaderMoviewView:(NSString*)fileName
{
    
    NSLog(@"file name is %@", fileName);
    
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:fileName]];

    AVAssetImageGenerator* generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
    
    //Get the 1st frame 3 seconds in
    int frameTimeStart = 20;
    int frameLocation = 21;
    
    //Snatch a frame
    CGImageRef frameRef = [generator copyCGImageAtTime:CMTimeMake(frameTimeStart,frameLocation) actualTime:nil error:nil];
    
    
    
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    
    CMTime thumbnailTime = [asset duration];

    thumbnailTime.value=10000;
    
 
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbnailTime actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    
    
    
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];

    NSData *imageData = UIImagePNGRepresentation([UIImage imageWithCGImage:imageRef]);
   // [imageData writeToFile:savedImagePath atomically:NO];
    
    
    
    moviePlayerThumbnailImageView.image=thumbnail;
    CGImageRelease(imageRef);

// CGImageRef won't be released by ARC
    
}
-(void)loadSelectedCellDetails:(NSInteger)selectedIndex

{
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        imagesScrollView.contentOffset = CGPointMake(imagesScrollView.frame.origin.x+imagesScrollView.frame.size.width*selectedCellIndex, 0);
    } completion:NULL];
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    NSLog(@"current offset %@", NSStringFromCGPoint(imagesScrollView.contentOffset));
    
    

    currentImageIndex=indexPath.row;
    
    selectedCellIndexPath=indexPath;
    [demoTimer invalidate];
    demoTimer=nil;

    if (isEmailTapped==YES) {
        
        
        MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        
        if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"Y"]) {
            [[productDataArray objectAtIndex:indexPath.row]setValue:@"N" forKey:@"isSelected"];
        }
        
        else
        {
            [[productDataArray objectAtIndex:indexPath.row]setValue:@"Y" forKey:@"isSelected"];
        }
        
        
//        if ([[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"]==nil) {
//            
//            [[productDataArray objectAtIndex:indexPath.row]setValue:@"Y" forKey:@"isSelected"];
//            
//        }
//        
//        else
//        {
//            
//            [[productDataArray objectAtIndex:indexPath.row]removeObjectForKey:@"isSelected"];
//            
//            
//        }
        

        
        
        
        if ([selectedIndexPathArray containsObject:indexPath]) {
            
            //already seelected cell
           
            /*
            for (NSInteger i=0; i<cell.contentView.subviews.count; i++) {
                
                
                NSLog(@"tag for views %d", [[cell.contentView.subviews objectAtIndex:i]tag]);
                
                NSInteger selectedCelltag=[[cell.contentView.subviews objectAtIndex:i]tag];
                
                if (selectedCelltag ==indexPath.row+1) {
                    
                    NSLog(@"check subviews  %@", [cell.contentView.subviews description]);
                    
                    NSLog(@"check subviews of this cell %@", [cell.contentView.subviews objectAtIndex:i]);
                    
                    [[cell.contentView.subviews objectAtIndex:i] removeFromSuperview];
                    
                }
                
            }*/
            
            
            NSLog(@"check subviews of already existing cell %@", [cell.contentView.subviews description]);
            [selectedIndexPathArray removeObject:indexPath];
            
            
        }
        
        else
        {
            
            [selectedIndexPathArray addObject:indexPath];
            
//            UIImageView * selectedTickImgView=[[UIImageView alloc]initWithFrame:CGRectMake(156, 2, 30, 30)];
//            selectedTickImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
//            selectedTickImgView.tag=indexPath.row+1;
//           // [cell.contentView addSubview:selectedTickImgView];
            
            sendEmail=YES;
            
            [emailButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
            
            
        }
        
        
        
        
    }
    
    else
    {
        
        [collectionView setAllowsMultipleSelection:NO];

        
        if (demoTimer) {
            
            demoTimer=nil;
        }
        
        //start the timer to check if images for displayed for more than 5 secs
        demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:NO];
        

        
        
//        [self loadSelectedCellDetails:selectedCellIndex];
        
        NSString* selectedFileName= [documentsDirectory stringByAppendingPathComponent:[[productDataArray  objectAtIndex:indexPath.row]valueForKey:@"File_Name"]];
        NSLog(@"selected file name is %@", selectedFileName);
        
        
        firstFileName=selectedFileName;
        [self reloadHeaderMoviewView:selectedFileName];

       // [self playMoviewithFileName:selectedFileName];
        
        selectedCellIndex=indexPath.row;
        
        
        [productVideosCollectionView reloadData];
    }
    
    if (selectedIndexPathArray.count==0) {
        
        [emailButton setTitle:@"Email" forState:UIControlStateNormal];
        isEmailTapped=NO;
        cancelButton.hidden=YES;
        sendEmail=NO;
    }
    
    [productVideosCollectionView reloadData];
    
    
    [self loadProductDetailsWithSelectedIndex:indexPath.row];
    
}


-(void)startFaceTimeTimer

{
    [viewedVideos addObject:[productDataArray objectAtIndex:currentImageIndex]];
    NSLog(@"video viewed for %ld seconds", demoTimeInterval);

}
-(void)sendEmail

{
    //NSString *emailAddres = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaCustomerID:self.currentVisit.visitOptions.customer.Customer_ID];
if ([MFMailComposeViewController canSendMail])
{
    MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
    
    mailController.mailComposeDelegate = self;
    
    [mailController setSubject:@"MedRep"];
    
    [mailController.navigationBar setTintColor:[UIColor blackColor]];
    
    [mailController setToRecipients:@[emailIDForAutoInsert]];
    
    NSString *fileNameString;
    
    NSString* emailBodyString=[[NSString alloc]init];
    
    
    for (NSInteger i=0; i<selectedIndexPathArray.count; i++) {
        
        
        
        
        
        NSIndexPath * currentIndexPath=[selectedIndexPathArray objectAtIndex:i];
        
        NSString* imageName= [NSString stringWithFormat:@"%@",[[productDataArray objectAtIndex:currentIndexPath.row]valueForKey:@"File_Name"] ];
        NSArray *items = [imageName componentsSeparatedByString:@"M_"];   //take the one array for split the string
        
        NSLog(@"splitted items are %@", [items lastObject]);
        
        NSString* refinedFileName=[NSString stringWithFormat:@"File Name : %@",[items lastObject]];

        NSString* downloadUrl=[NSString stringWithFormat:@"Download URL : %@",[[productDataArray objectAtIndex:currentIndexPath.row]valueForKey:@"Download_Url"] ];
        
        NSLog(@"download url is %@", downloadUrl);
        emailBodyString=[[[[emailBodyString stringByAppendingString:refinedFileName] stringByAppendingString:@"\n \n"] stringByAppendingString:downloadUrl] stringByAppendingString:@"\n \n"];
        
      ;
        
        NSLog(@"email url is %@", emailBodyString);

        
    }
    
    [mailController setMessageBody:emailBodyString isHTML:NO];

    
    NSLog(@"email Body string is %@", [emailBodyString description]);
    
    
    [self presentViewController:mailController animated:YES completion:nil];
}else{
    [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
}
}

#pragma mark Mail Composer Delegate


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            
            [emailButton setTitle:@"Email" forState:UIControlStateNormal];
            
            for (NSInteger i=0; i<selectedIndexPathArray.count; i++) {
                NSIndexPath * selectedIndexPathforEmail=[selectedIndexPathArray objectAtIndex:i];
                MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell*)[productVideosCollectionView cellForItemAtIndexPath:selectedIndexPathforEmail];
                //already seelected cell
                //[[cell.contentView.subviews lastObject] removeFromSuperview];
                NSLog(@"check subviews in mail delegate %@", [cell.contentView.subviews description]);
            }
            
            
           
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
                      break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    sendEmail=NO;
    
    isEmailTapped=NO;
    cancelButton.hidden=YES;
    
    [productDataArray setValue:@"N" forKey:@"isSelected"];
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];

    [selectedIndexPathArray removeAllObjects];
    [productVideosCollectionView reloadData];
    
    
    
    [productVideosCollectionView reloadData];

    
    [self dismissViewControllerAnimated:YES completion:NULL];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
