//
//  MedRepCreateLocationViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/1/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CSMapAnnotation.h"
#import "Place.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "SWAppDelegate.h"
#import "MedRepPlacesSearchViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DMBordelLabel/DMBorderLabel.h"
#import "MedRepTextView.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "SalesWorxImageView.h"


@protocol addLocationProtocol <NSObject>

-(void)addLocationData:(NSMutableDictionary*)locationDetails;

@end


@interface MedRepCreateLocationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,UITextViewDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate,MKMapViewDelegate,SelectedLocationCoordinateDelegate,UITextFieldDelegate,SelectedFilterDelegate>

{
    id dismissDelegate;
    
    CGRect oldFrame;
    
      id currentFirstResponder;
    
    NSMutableArray * citiesArray;

    
    NSString* doctorIDforSelectedDoctor;
    
    StringPopOverViewController_ReturnRow* locationPopOver;
    
    UIPopoverController *valuePopOverController;
    
    NSString* popUpString;
    
    SWAppDelegate* appDelegate;
    
    NSMutableArray* locationsarray,*doctorsArray,*refinedDoctorsArray,*emiratesArray,*contactTitlesArray;
    
    Place* place;
    
    CSMapAnnotation *csAnnotation;

    
    id locationDetailsDelegate;
    
    BOOL isPharmacyLocation;
    
    
    NSMutableDictionary * capturedDataDict;
    
    
    IBOutlet MedRepTextField *contactNameTxtFld;
    IBOutlet MedRepTextField *contactEmailAddressTxtFld;
    IBOutlet MedRepTextField *contactTelephoneTxtFld;
    IBOutlet MedRepTextField *typeTextField;
    IBOutlet MedRepTextField *doctorTextField;
    IBOutlet MedRepTextField *cityTextField;
    IBOutlet MedRepTextField *contactTitleTxtFld;
    IBOutlet MedRepTextField *locationNameTxtFld;
    IBOutlet MedRepTextField *email;
    IBOutlet MedRepTextField *telephoneNumberTxtFld;
}

@property (nonatomic, assign) id dismissDelegate;

@property(strong,nonatomic)NSString* selectedLocationLatitude;

@property(strong,nonatomic)NSString* SelectedLocationLongitude;


@property(strong,nonatomic) UIPopoverController* addLocationPopover;

@property(nonatomic,assign) id locationDetailsDelegate;

@property (strong, nonatomic) IBOutlet MedRepTextView *addressTxtView;

@property (strong, nonatomic) IBOutlet UILabel *locationLbl;

@property (strong, nonatomic) IBOutlet UILabel *typeLbl;

@property (strong, nonatomic) IBOutlet UIButton *contactTitleBtn;

@property (strong, nonatomic) IBOutlet UILabel *telephoneLbl;

@property (strong, nonatomic) IBOutlet UILabel *doctorLbl;

@property (strong, nonatomic) IBOutlet MKMapView *locationMapView;

@property (strong, nonatomic) IBOutlet UILabel *emirateLbl;

- (IBAction)contactTitleDisabledEventButton:(id)sender;


- (IBAction)closeButtonTapped:(id)sender;



@property(nonatomic)CLLocationCoordinate2D  locationCoordinates;




@property (strong, nonatomic) IBOutlet DMBorderLabel *contactTitleLbl;

- (IBAction)contactTitleTapped:(id)sender;




@property (strong, nonatomic) IBOutlet UILabel *contactTitleHeader;

@property (strong, nonatomic) IBOutlet UILabel *contactNameHeader;

@property (strong, nonatomic) IBOutlet UILabel *contactEmailHeader;

@property (strong, nonatomic) IBOutlet UILabel *contactTelephoneHeader;

-(void)saveContactData;




@end
