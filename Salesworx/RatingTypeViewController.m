//
//  RatingTypeViewController.m
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "RatingTypeViewController.h"
#import "SWFoundation.h"

@implementation RatingTypeViewController
@synthesize rateLabel = _rateLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        DYRateView *rateView = [[DYRateView alloc] initWithFrame:CGRectMake(0, 200, self.bounds.size.width, 100) fullStar:[UIImage imageNamed:@"StarFullLarge.png"] emptyStar:[UIImage imageNamed:@"StarEmptyLarge.png"]];
        [rateView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

        rateView.padding = 20;
        rateView.alignment = RateViewAlignmentCenter;
        rateView.editable = YES;
        rateView.delegate = self;
        [self addSubview:rateView];
        
        // Set up a label view to display rate
        self.rateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.bounds.size.width, 40)];
        [self.rateLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth ];
        self.rateLabel.backgroundColor = [UIColor clearColor];
        self.rateLabel.textAlignment = NSTextAlignmentCenter;
        self.rateLabel.text = @"How you rate our customer service out of 5 star?";
        [self addSubview:self.rateLabel];

    }
    return self;
}

#pragma mark - DYRateViewDelegate

- (void)rateView:(DYRateView *)rateView changedToNewRate:(NSNumber *)rate {
    //self.rateLabel.text = [NSString stringWithFormat:@"Rate: %d", rate.intValue];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
