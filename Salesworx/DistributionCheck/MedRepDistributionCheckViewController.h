//
//  MedRepDistributionCheckViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxDropShadowView.h"
#import "MedRepTextField.h"
#import "PNCircleChart.h"
#import "AppControl.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxTableView.h"
#import "MedRepButton.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesWorxDCFilterViewController.h"


//#import "SalesWorxCustomClass.h"
//#import "MedRepHeader.h"
//#import "MedRepProductCodeLabel.h"
//
//
//
//#import "MedRepView.h"
//
//
//
//#import "SalesWorxDescriptionLabel1.h"


@interface MedRepDistributionCheckViewController : UIViewController<UIPopoverControllerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    AppControl *appControl;
    
    IBOutlet SalesWorxDropShadowView *parentViewOfPieChart;
    IBOutlet MedRepElementDescriptionLabel *lblLocationName;
    IBOutlet MedRepElementDescriptionLabel *lblContactName;
    IBOutlet PNCircleChart *pieChart;
    
    
    IBOutlet UIButton *filterButton;
    UIImageView *blurredBgImage;
    IBOutlet SalesWorxTableView *productTableView;

    
    IBOutlet UIView *availDetailView;
    IBOutlet UILabel *lblLastVisitStock;
    IBOutlet UILabel *lblLastVisitOn;
    IBOutlet UISegmentedControl *segAvailable;
    IBOutlet MedRepTextField *txtQuantity;
    IBOutlet MedRepTextField *txtLotNo;
    IBOutlet MedRepTextField *txtExpiryDate;
    IBOutlet MedRepButton *btnAdd;
    
    
    IBOutlet UIImageView *productImageView;
    IBOutlet SalesWorxTableViewHeaderView *headerView;
    
    

    
    
    NSMutableArray *fetchDistriButionCheckLocations;
    NSMutableArray *MSLItemsArray;
    
    DistriButionCheckLocation *selectedDistributionCheckLocation;
    DistriButionCheckItem *selectedDistributionCheckItem;
    DistriButionCheckItemLot *selectedDistributionCheckItemLot;
    
    
    NSMutableArray *indexPathArray;
    NSIndexPath *editIndexPath;
    
    BOOL isAddButtonPressed;
    UITapGestureRecognizer *tap;
    BOOL flagRetake;
    BOOL anyUpdated;
    
 
    NSMutableArray*productsArray;
    
    NSMutableDictionary *previousFilteredParameters;    
}
@property(strong,nonatomic) NSMutableDictionary *visitDetailsDict;
@property(strong,nonatomic) VisitDetails *visitDetails;

@property(strong,nonatomic) SalesWorxVisit * currentVisit;

@end
