

//
//  AlSeerSalesOrderViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerSalesOrderViewController.h"
#import "SalesOrderViewController.h"

#import "AnimationUtility.h"
#import "SWDatabaseManager.h"

@interface AlSeerSalesOrderViewController ()

@end

@implementation AlSeerSalesOrderViewController

@synthesize parentViewController,customerDict,preOrdered,preOrderedItems,isOrderConfirmed,wareHouseDataSalesOrderArray,uomButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title= @"Sales Order";
    noPricesAvailableStatus=NO;
    productTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    isPinchDone = NO;
    //check if app control has multi UOM selected
    
    if ([[SWDefaults checkMultiUOM]isEqualToString:@"N"]) {
        
        
        [uomButton setHidden:YES];
    }
     NSLog(@"check customer dict in Al seer view did load %@", [customerDict description]);
    
    Singleton *single = [Singleton retrieveSingleton];

       if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
           
       {
           [uomButton setHidden:YES];
           
       }
    
    
    if ([self.parentViewString isEqualToString:@"SO"])
    {
        
        oldSalesOrderVC = [[AlSeerSalesOrderCalculationViewController alloc] initWithCustomer:self.customerDict andCategory:[SWDefaults productCategory]] ;
    }
    else if ([self.parentViewString isEqualToString:@"MO"]) {
        oldSalesOrderVC = [[AlSeerSalesOrderCalculationViewController alloc] initWithCustomer:self.customerDict andOrders:self.preOrdered andManage:self.isOrderConfirmed];
        
        
        
        // oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
        
        NSLog(@"ware house data in old sales order %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
        
        //OLA!!
        if ([self.isOrderConfirmed isEqualToString:@"confirmed"])
        {
            [addButton setHidden:YES];
            [txtDefBonus setUserInteractionEnabled:NO];
            [txtDiscount setUserInteractionEnabled:NO];
            [txtfldWholesalePrice setUserInteractionEnabled:NO];
            [txtFOCQty setUserInteractionEnabled:NO];
            [txtNotes setUserInteractionEnabled:NO];
            [txtProductQty setUserInteractionEnabled:NO];
            [txtRecBonus setUserInteractionEnabled:NO];
            [focProductBtn setUserInteractionEnabled:NO];
        }
    }
    else if ([self.parentViewString isEqualToString:@"TO"]) {
        oldSalesOrderVC = [[AlSeerSalesOrderCalculationViewController alloc] initWithCustomer:self.customerDict andTemplate:self.preOrdered andTemplateItem:self.preOrderedItems] ;
    }
    
    NSLog(@"check parent view string %@", self.parentViewString);
    
    oldSalesOrderVC.view.frame = bottomOrderView.bounds;
    oldSalesOrderVC.view.clipsToBounds = YES;
    
    
    // moving ware house data to old sales order
    
    NSLog(@"data before moving %@", [wareHouseDataSalesOrderArray description]);
    oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
    
    NSLog(@"data after moving %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
    
    
    [bottomOrderView addSubview:oldSalesOrderVC.view];
    [oldSalesOrderVC didMoveToParentViewController:self];
    [self addChildViewController:oldSalesOrderVC];
    
    
    productArray =[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:[SWDefaults productCategory]]];
    filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray ) {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Brand_Code"]];
        if ( theMutableArray == nil ) {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Brand_Code"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    _collapsedSections = [NSMutableSet new];
    
    bSearchIsOn = NO;
    [self borderView:bonusView];
    [self borderView:stockView];
    [self borderView:salesHistoryView];
    [self borderView:dragParentView];
    // [self borderView:dragButton];
    
    //changed to [customerDict stringForKey:@"Customer_Name"]
    //    NSLog(@"OLA!!******customer name %@",[customerDict stringForKey:@"Customer_Name"]);
    //    NSLog(@"OLA!!******customer name %@",[[SWDefaults customer] stringForKey:@"Customer_Name"]);
    lblCustomerLabel.text = [customerDict stringForKey:@"Customer_Name"];
    lblCustomerLabel.font = RegularFontOfSize(26);
    lblProductName.font = BoldSemiFontOfSize(16);
    lblProductCode.font =BoldSemiFontOfSize(12);
    lblProductBrand.font=BoldSemiFontOfSize(12);
    lblWholesalePrice.font=BoldSemiFontOfSize(12);
    txtfldWholesalePrice.font=BoldSemiFontOfSize(12);
    lblRetailPrice.font=BoldSemiFontOfSize(12);
    lblUOMCode.font=BoldSemiFontOfSize(12);
    lblAvlStock.font=BoldSemiFontOfSize(12);
    lblExpDate.font=BoldSemiFontOfSize(12);
    lblPriceLabel.font=BoldSemiFontOfSize(12);
    
    //    txtProductQty.font=LightFontOfSize(14);
    //    txtDefBonus.font=LightFontOfSize(14);
    //    txtRecBonus.font=LightFontOfSize(14);
    //    txtDiscount.font=LightFontOfSize(14);
    //    txtFOCQty.font=LightFontOfSize(14);
    //    txtNotes.font=LightFontOfSize(14);
    
    lblCustomerLabel.textColor = UIColorFromRGB(0x4A5866);
    lblProductName.textColor = UIColorFromRGB(0x4687281);
    lblProductCode.textColor =UIColorFromRGB(0x4687281);
    lblProductBrand.textColor=UIColorFromRGB(0x4687281);
    lblWholesalePrice.textColor=UIColorFromRGB(0x4687281);
    txtfldWholesalePrice.textColor=UIColorFromRGB(0x4687281);
    lblRetailPrice.textColor=UIColorFromRGB(0x4687281);
    lblUOMCode.textColor=UIColorFromRGB(0x4687281);
    lblAvlStock.textColor=UIColorFromRGB(0x4687281);
    lblExpDate.textColor=UIColorFromRGB(0x4687281);
    lblPriceLabel.textColor=UIColorFromRGB(0x4687281);
    
    txtProductQty.textColor=UIColorFromRGB(0x4687281);
    txtDefBonus.textColor=UIColorFromRGB(0x4687281);
    txtRecBonus.textColor=UIColorFromRGB(0x4687281);
    txtDiscount.textColor=UIColorFromRGB(0x4687281);
    txtFOCQty.textColor=UIColorFromRGB(0x4687281);
    txtNotes.textColor=UIColorFromRGB(0x4687281);
    txtNotes.textColor = [UIColor darkTextColor ];
    
    
    candySearchBar.backgroundColor = UIColorFromRGB(0xF6F7FB);
    
    lblTitleProductCode.textColor=UIColorFromRGB(0x4A5866);
    lblTitleWholesalePrice.textColor=UIColorFromRGB(0x4A5866);
    lblTitleRetailPrice.textColor=UIColorFromRGB(0x4A5866);
    lblTitleUOMCode.textColor=UIColorFromRGB(0x4A5866);
    lblTitleAvlStock.textColor=UIColorFromRGB(0x4A5866);
    lblTitleExpDate.textColor=UIColorFromRGB(0x4A5866);
    txtTitleProductQty.textColor=UIColorFromRGB(0x4A5866);
    txtTitleDefBonus.textColor=UIColorFromRGB(0x4A5866);
    txtTitleRecBonus.textColor=UIColorFromRGB(0x4A5866);
    txtTitleDiscount.textColor=UIColorFromRGB(0x4A5866);
    txtTitleFOCQty.textColor=UIColorFromRGB(0x4A5866);
    txtTitleFOCItem.textColor=UIColorFromRGB(0x4A5866);
    txtTitleNotes.textColor=UIColorFromRGB(0x4A5866);
    
    lblTitleProductCode.font=LightFontOfSize(14);
    lblTitleWholesalePrice.font=LightFontOfSize(14);
    lblTitleRetailPrice.font=LightFontOfSize(14);
    lblTitleUOMCode.font=LightFontOfSize(14);
    lblTitleAvlStock.font=LightFontOfSize(14);
    lblTitleExpDate.font=LightFontOfSize(14);
    txtTitleProductQty.font=LightFontOfSize(14);
    txtTitleDefBonus.font=LightFontOfSize(14);
    txtTitleRecBonus.font=LightFontOfSize(14);
    txtTitleDiscount.font=LightFontOfSize(14);
    txtTitleFOCQty.font=LightFontOfSize(14);
    txtTitleFOCItem.font=LightFontOfSize(14);
    txtTitleNotes.font=LightFontOfSize(14);
    
    lblSelectProduct.font=BoldSemiFontOfSize(20);
    
    dragParentView.backgroundColor = UIColorFromRGB(0x343F5F);
    focProductBtn.titleLabel.font =BoldSemiFontOfSize(14);
    
    [txtNotes.layer setCornerRadius:7.0f];
    [txtNotes.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [txtNotes.layer setBorderWidth:1.0f];
    
    //    [txtfldWholesalePrice setPlaceholder:[NSString stringWithFormat:@"(in %@)",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]]];
    [txtfldWholesalePrice setDelegate:self];
    [txtfldWholesalePrice setKeyboardType:UIKeyboardTypeDecimalPad];
    [txtfldWholesalePrice setHidden:YES];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeVisit:)] ];
    
}


#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    
    if (alertView.tag==100 || alertView.tag==101) {
        
    }
    
    else if (alertView.tag==1000)
        
    {
        
        
        
        
        if (buttonIndex==0) {
            //yes tapped
            [self.navigationController  popViewControllerAnimated:YES];
        }
        else
        {
            
        }
        
        
    }
    
    else{
        
        
        [self.navigationController  popViewControllerAnimated:YES];
        
    }
    
    
    
    //    switch (buttonIndex) {
    //        case 0: {
    //            [self.navigationController  popViewControllerAnimated:YES];
    //            break;
    //        }
    //        default:
    //            break;
    //    }
    
}

- (void)closeVisit:(id)sender {
    
    UIAlertView* closeAlert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert", nil) message:NSLocalizedString(@"Would you like to close this order?", nil) delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    
    closeAlert.tag=1000;
    [closeAlert show];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    NSLog(@"ware house data in sales order %@", [wareHouseDataSalesOrderArray description]);
    
    
    OrderAdditionalInfoViewController * orderVC=[[OrderAdditionalInfoViewController alloc]init];
    
    orderVC.warehouseDataArray=wareHouseDataSalesOrderArray ;
    
    
    NSLog(@"ware house data being pushed to final destination %@", [orderVC.warehouseDataArray description]);
    
    //pushing from new sales order to old sales order
    
    
    
    self.navigationController.toolbarHidden = YES;
    oldSalesOrderVC.delegate = self;
    //txtfldWholesalePrice.delegate=self;
}


-(void)viewDidDisappear:(BOOL)animated
{
    oldSalesOrderVC.delegate = nil;
    txtfldWholesalePrice.delegate=nil;
    productStockViewController.delegate=nil;
}
-(void)borderView:(UIView *)view
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowPath = shadowPath.CGPath;
    [view.layer setBorderColor:UIColorFromRGB(0xE6E6E6).CGColor];
    [view.layer setBorderWidth:2.0f];
    
}

#pragma mark - UITableView

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            return 1;
            
        }
        else
        {
            return [finalProductArray count];
        }
    }
    else
    {
        return 1;
        
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return [filteredCandyArray count];
            
        } else {
            NSArray *tempDict =[productDictionary objectForKey:[finalProductArray objectAtIndex:section]];
            
            return [_collapsedSections containsObject:@(section)] ? [tempDict count] :0 ;
            
        }
    }
    else
    {
        return [productArray count];
        
    }
    
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.01f;
//}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return 0;
        }
        else
        {
            return 40.0;
        }
    }
    else
    {
        return 0;
    }
    
}



//CRASH FIXED
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//}



- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    if (tv.tag==1)
    {
        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            
            UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
            result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
            [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            result.backgroundColor=UIColorFromRGB(0xE0E5EC);
            [result setTitle:[[row objectAtIndex:0] stringForKey:@"Brand_Code"] forState:UIControlStateNormal];
            result.tag = s;
            [result.layer setBorderColor:[UIColor whiteColor].CGColor];
            [result.layer setBorderWidth:1.0f];
            result.titleLabel.font=BoldSemiFontOfSize(16);
            [result setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
            
            [headerView addSubview:result];
            
            return headerView;
        }
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.numberOfLines=2;
        cell.textLabel.font=RegularFontOfSize(5);
        cell.backgroundColor=UIColorFromRGB(0xF6F7FB);
        cell.textLabel.textColor=UIColorFromRGB(0x687281);
        cell.textLabel.textColor=[UIColor darkTextColor ];
        cell.textLabel.font  =  LightFontOfSize(14) ;
    }
    
    
    
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            NSDictionary * row = [filteredCandyArray objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
    }
    else
    {
        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [row stringForKey:@"Description"];
        if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
        {
            cell.textLabel.textColor = [UIColor redColor];
        }
        else
        {
            cell.textLabel.textColor=[UIColor darkTextColor ];
        }
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedUOMReference=nil;
    noPricesAvailableStatus=NO;
    
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    if (tableView.tag==1)
    {
        
        txtProductQty.text=@"";
        txtDefBonus.text=@"";
        txtRecBonus.text=@"";
        txtDiscount.text=@"";
        txtFOCQty.text=@"";
        txtNotes.text=@"";
        [addButton setTitle:@"Add" forState:UIControlStateNormal];
        
        NSDictionary * row ;
        if (bSearchIsOn)
        {
            row = [filteredCandyArray objectAtIndex:indexPath.row];
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            row= [objectsForCountry objectAtIndex:indexPath.row];
        }
        NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
        mainProductDict=nil;
        mainProductDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
        NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
        [mainProductDict setValue:itemID forKey:@"ItemID"];
        
        isDidSelectStock=YES;
        [stockBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
        [stockBtn setSelected:YES];
        
        
        //NSLog(@"OLA!!********* product %@",[produstDetail objectAtIndex:0]);
        
        if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
            [lblWholesalePrice setHidden:YES];
            [txtfldWholesalePrice setHidden:NO];
        }
        else
        {
            [lblWholesalePrice setHidden:NO];
            [txtfldWholesalePrice setHidden:YES];
        }
        // NSLog(@"Product %@",row);
        
        [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];

        
        
        
        
        
        //check this in MO while update
        [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
        
        
    }
    else
    {
        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
        if (![row objectForKey:@"Guid"])
        {
            [row setValue:[NSString createGuid] forKey:@"Guid"];
        }
        if([fBonusProduct objectForKey:@"Qty"])
        {
            [row setValue:[fBonusProduct objectForKey:@"Qty"] forKey:@"Qty"];
        }
        fBonusProduct=[row mutableCopy];
        [focProductBtn setTitle:[row stringForKey:@"Description"] forState:UIControlStateNormal];
        [self buttonAction:focProductBtn];
        //[tableView reloadData];
    }
    // bonusView = productBonusViewController.view;
    [self selectedStringValueDuplicate:lblUOMCode.text];

}

#pragma mark -

- (void)selectedProduct:(NSMutableDictionary *)product
{
    noPricesAvailableStatus=NO;
    selectedUOMReference=nil;
    productReference=[NSMutableDictionary dictionaryWithDictionary:product];
     Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
    {
        //display prev selected uom
        lblUOMCode.text=[product stringForKey:@"Order_Quantity_UOM"];
        
       // [uomButton setTitle:[product stringForKey:@"Order_Quantity_UOM"] forState:UIControlStateNormal];

        
        float selectedWholesalePrice=[[product valueForKey:@"Price"] floatValue];
        
        
        lblWholesalePrice.text=[NSString stringWithFormat:@"%0.2f", selectedWholesalePrice];
        
    }
     else
    {
         //if uom is not selected order captured with default uom and saved, caem to manage order then display the default display uom
        if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"notconfirmed"])

        {
            lblUOMCode.text=[product stringForKey:@"Primary_UOM_Code"];

        }
        
         else
        {
            
             if ([[product stringForKey:@"Item_UOM"] isEqualToString:@""]||[[product stringForKey:@"Item_UOM"] isEqual:[NSNull null]]) {
                
                
                
                NSString* selectedUOMStr=[product stringForKey:@"SelectedUOM"];
                if ([selectedUOMStr isEqualToString:@""]|| [selectedUOMStr isEqual:[NSNull null]]) {
                    lblUOMCode.text=[product stringForKey:@"UOM"];

                    
                }
                
                
            }
            
            
            else
            {
                lblUOMCode.text=[product stringForKey:@"Item_UOM"];
            }
        }
        
    }
    
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    mainProductDict=nil;
    mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
    
    txtfldWholesalePrice.text = @"";
    
    
    
    
    if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
        [lblWholesalePrice setHidden:YES];
        [txtfldWholesalePrice setHidden:NO];
        
        
    }
    else
    {
        [lblWholesalePrice setHidden:NO];
        [txtfldWholesalePrice setHidden:YES];
    }
    
    
    if ([mainProductDict objectForKey:@"Bonus_Product"]) {
        bonusProduct=[mainProductDict objectForKey:@"Bonus_Product"];
    }
    
    if ([mainProductDict objectForKey:@"FOC_Product"]) {
        fBonusProduct=[mainProductDict objectForKey:@"FOC_Product"];
        
    }
    NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
    [mainProductDict setValue:itemID forKey:@"ItemID"];
    
    
    NSString* updatedWholesalePrice;
    
    //Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
        
    {
        
        
        NSLog(@"check is order confirmed %@", self.isOrderConfirmed);
        
        NSLog(@"check net price in confrimed order %@", [mainProductDict valueForKey:@"Unit_Selling_Price"]);
        
        updatedWholesalePrice= [mainProductDict valueForKey:@"Unit_Selling_Price"] ;
        
    }
    
    else
    {
        
        updatedWholesalePrice= [mainProductDict valueForKey:@"Net_Price"] ;
    }
    
    
    
    
    [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];
//    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"]]];
    
    
    
    
    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
    
    
    
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    NSLog(@"quantity text is %@",txtProductQty.text);
    txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
    txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
    txtDiscount.text=[mainProductDict stringForKey:@"Discount"];
    txtFOCQty.text=[fBonusProduct stringForKey:@"Qty"];
    txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
    
     if (txtfldWholesalePrice.isHidden==NO) {
        
        
         NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

         
        [txtfldWholesalePrice setText: [NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,[updatedWholesalePrice doubleValue]]];
        
    }

    
    [addButton setTitle:@"Update" forState:UIControlStateNormal];
    [self selectedStringValueDuplicate:lblUOMCode.text];

     //do thing
}

- (void) dbGetSelectedProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    
    
    NSMutableArray* filteredPriceDetail=[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<priceDetail.count; i++) {
        
        
        if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:selectedUOMReference]) {
            
            [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
            
        }
//        else if  ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"CS"]) {
//            
//            [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
//
//        }
        
        
        
    }
    priceDetail=filteredPriceDetail;
    
    
    
    
    //stock info based on selected UOM
    
       
    
    
    
    
    
    //OLA!! fetching lot info for selected item
    NSArray *stockInfo = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"]];
    
    //NSLog(@"OLA!!******* stock info %@", stockInfo);
    
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Lot_Qty"] forKey:@"Lot_Qty"];
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Expiry_Date"] forKey:@"Expiry_Date"];
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"UOM"] forKey:@"UOM"];
    
    
    //[mainProductDict setObject:selectedUOMReference forKey:@"UOM"];
    

    
    NSString* itemCodeforUOM=[mainProductDict stringForKey:@"Item_Code"];
    NSString* selectedUom=selectedUOMReference;
    
    NSString* conversionQry=[NSString stringWithFormat:@"SELECT Conversion from TBL_Item_Uom where Item_Code='%@' and Item_UOM='%@' ",itemCodeforUOM,selectedUom];
    
    NSMutableArray* consersionArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:conversionQry];
    
    NSLog(@"check the conversion %@", [consersionArr valueForKey:@"Conversion"]);
  
    
    
    if (consersionArr.count>0) {
        
    
    
    
        NSInteger lotQty=[[mainProductDict stringForKey:@"Lot_Qty"] integerValue];
        
        NSInteger conversion=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        
        
        
        NSInteger updatedLotQty= lotQty/conversion;
        
        [mainProductDict setObject:[NSString stringWithFormat:@"%d",updatedLotQty] forKey:@"Lot_Qty"];

        
        
         }
    

    
    
    
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            
            //checking the net price junk value
            
            
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            noPricesAvailableStatus=YES;
            return;

            //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        //[alert show];
        noPricesAvailableStatus=YES;

        return;
        //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    // [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Unit_List_Price"] currencyString]];
    // [lblRetailPrice setText:[[mainProductDict stringForKey:@"Unit_Selling_Price"] currencyString]];
    [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [txtfldWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
    
    
    if([mainProductDict stringForKey:@"SelectedUOM"].length==0)
    {
        [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];

    }
    else
    {
        [lblUOMCode setText:[mainProductDict stringForKey:@"SelectedUOM"]];


    }
    
   // [uomButton setTitle:[mainProductDict stringForKey:@"UOM"] forState:UIControlStateNormal];
    
    
    
    //lot qty for UOM
    
    if (!selectedUOMReference) {
        
        
        
        
        
        
    }
    
    
    
    
    
//    [lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
//    // [lblExpDate setText:[mainProductDict stringForKey:@"Expiry_Date"]];
//    
//    lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
//    
    /** ios*/
     
    if ([[mainProductDict stringForKey:@"Lot_Qty"] floatValue]==0.0)
    {
        [lblAvlStock setText:@"N/A"];
    }
    else
    {
        [lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
    }
    
    if ([[mainProductDict stringForKey:@"Expiry_Date"] floatValue]==0.0)
    {
        lblExpDate.text = @"N/A";
    }
    else
    {
        lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
    }

    
    
}



- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    
    
    
    
    //match with main product dict
    NSMutableArray* filteredPriceDetailUpdate=[[NSMutableArray alloc]init];

    
    NSLog(@"check uom %@", [mainProductDict valueForKey:@"Order_Quantity_UOM"]);
    
    
    
    NSString* checkselectedUOM=[mainProductDict valueForKey:@"Order_Quantity_UOM"];
    
    
    
    for (NSInteger i=0; i<priceDetail.count; i++) {
        
        NSString* uomforPriceDetail=[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i];
        
        if ([uomforPriceDetail isEqualToString:checkselectedUOM]) {
            
            
            [filteredPriceDetailUpdate addObject:[priceDetail objectAtIndex:i]];
            
            
        }
        
        
        
        
    }
    if (filteredPriceDetailUpdate.count>0) {
        
        priceDetail=filteredPriceDetailUpdate;
    }
    
    
    
    
    
    else
    {
    
    NSMutableArray* filteredPriceDetail=[[NSMutableArray alloc]init];
    
    NSMutableArray* filteredPriceDetailMO=[[NSMutableArray alloc]init];

    for (NSInteger i=0; i<priceDetail.count; i++) {
        
        
        if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"EA"]) {
            
            [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
            
        }
        //        else if  ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"CS"]) {
        //
        //            [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
        //
        //        }
        
        
        
    }
    
    if (filteredPriceDetail.count==0) {
        
        
    }
    else
        
    {
    
        if ([self.isOrderConfirmed isEqualToString:@"notconfirmed"]) {
            
            
            
            NSString* selectedUOM=[customerDict stringForKey:@"Order_Quantity_UOM"];
            for (NSInteger i=0; i<priceDetail.count; i++) {
                
                NSLog(@"%@",[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]);
                NSLog(@"%@",selectedUOM);

                if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:selectedUOM])
                {
                    [filteredPriceDetailMO addObject:[priceDetail objectAtIndex:i]];
                }
            
             
        }
            priceDetail=filteredPriceDetailMO;

        
        }
        else
        {
        
            priceDetail=filteredPriceDetail;
        }
    }
    
    
    
    }
    
    //check this object at index 0
    
    
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            
            //risk condition
            
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Unit_Selling_Price"];
            
            
            
            //if UOM picker is present display E/A price by default
            
            if ([selectedUOMReference isEqualToString:@"EA"] || selectedUOMReference==nil) {
                
                
                for (NSInteger i=0; i<priceDetail.count; i++) {
                    
                    if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"EA"]) {
                        
                        //lblUOMCode.text=[[priceDetail valueForKey:@"Unit_Selling_Price"] objectAtIndex:i];
                       // lblWholesalePrice.text=@"EA";
                        [mainProductDict setValue:[[priceDetail objectAtIndex:i]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];

                    }
                    
                }
                
            }
            
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)message:@"No prices availble for selected product." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    // [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Unit_List_Price"] currencyString]];
    // [lblRetailPrice setText:[[mainProductDict stringForKey:@"Unit_Selling_Price"] currencyString]];
      [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
    
    
    
    
    
    //uom  and price of confirmed orders in manage order to be fetched from db
    
    
    
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
        
        
    {

    }
    else
    {

    
        if (!selectedUOMReference) {
            
            
            
            if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"notconfirmed"])
                
            {
                
                //check if this has order qty uom
                
                NSString* orderqtyUom=[mainProductDict stringForKey:@"Order_Quantity_UOM"];
                
                if (orderqtyUom==nil|| [orderqtyUom isEqualToString:@""]||[orderqtyUom isEqual:[NSNull null]]  ) {
                    
                    NSString* tempUom=[mainProductDict stringForKey:@"UOM"];
                    NSLog(@"tempUom----->%@",tempUom);
                    if(tempUom==nil|| [tempUom isEqualToString:@""]||[tempUom isEqual:[NSNull null]] )
                    {
                         NSLog(@"tempUom2----->%@",tempUom);
                    [lblUOMCode setText:[mainProductDict stringForKey:@"Display_UOM"]];
                    }
                    else
                    {
                        NSLog(@"tempUom3----->%@",tempUom);
                        [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
                    }


                }
                
                else
                {
                
                    lblUOMCode.text=[mainProductDict stringForKey:@"Order_Quantity_UOM"];
                }
            }
            
            
            
          else if ([mainProductDict valueForKey:@"UOM"]==nil) {
                
                lblUOMCode.text=[mainProductDict stringForKey:@"Item_UOM"];
            }
            
            else
            {
                
                if ([mainProductDict valueForKey:@"SelectedUOM"]==nil) {
                    [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];

                }
                
                
                else
                {
                    [lblUOMCode setText:[mainProductDict stringForKey:@"SelectedUOM"]];
 
                }
                
                
            }
            [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
            [txtfldWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];

        }
        
        else
        {
            
            
            NSString* selectedUOm=[mainProductDict stringForKey:@"SelectedUOM"];
            
            if ([selectedUOm isEqual:[NSNull null]]|| [selectedUOm isEqualToString:@""]) {
                
                [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];

            }
            
            else
            {
            
            [lblUOMCode setText:[mainProductDict stringForKey:@"SelectedUOM"]];
            }
            
            [lblWholesalePrice setText:[[productReference stringForKey:@"Net_Price"] currencyString]];
            [txtfldWholesalePrice setText:[[productReference stringForKey:@"Net_Price"] currencyString]];
            
        }
    }
    
    
    
    
    //avbl stock based on selected Price UOM
    
    
    if ([[mainProductDict stringForKey:@"Lot_Qty"] floatValue]==0.0)
    {
        [lblAvlStock setText:@"N/A"];
    }
    else
    {
        [lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
    }
    
    if ([[mainProductDict stringForKey:@"Expiry_Date"] floatValue]==0.0)
    {
        lblExpDate.text = @"N/A";
    }
    else
    {
        lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
    }

    //[lblExpDate setText:[mainProductDict stringForKey:@"Expiry_Date"]];
}
- (void)stockAllocated:(NSMutableDictionary *)p {
    mainProductDict = nil;
    mainProductDict=[NSMutableDictionary dictionaryWithDictionary:p];
}
-(void)sectionButtonTouchUpInside:(UIButton*)sender {
    [productTableView beginUpdates];
    int section = sender.tag;
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (!shouldCollapse) {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        //sender.backgroundColor = [UIColor lightGrayColor];
        
        int numOfRows = [productTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
    }
    else {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        
        
        NSInteger collapedSectionsCount=_collapsedSections.count;
        
        
        for (NSInteger i=0; i<collapedSectionsCount; i++) {
            
            NSArray *myArray = [_collapsedSections allObjects];
            
            NSInteger sectionVal=[[myArray objectAtIndex:i] integerValue];
            int numOfRows = [productTableView numberOfRowsInSection:sectionVal];
            NSArray* indexPaths = [self indexPathsForSection:sectionVal withNumberOfRows:numOfRows];
            [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [_collapsedSections removeObject:@(sectionVal)];
            
        }
        
        
        

        
        
        NSArray *tempDict =[productDictionary objectForKey:[finalProductArray objectAtIndex:section]];
        int numOfRows = [tempDict count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    
    [productTableView endUpdates];
    //[_tableView reloadData];
}

#pragma mark - UISearchBar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        //[searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0];
        bSearchIsOn = NO;
        [productTableView reloadData];
        [productTableView setScrollsToTop:YES];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar
{
    bSearchIsOn = YES;
    [candySearchBar resignFirstResponder];
    int len = [ [candySearchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length];
    if (len > 0)
    {
        NSString *searchText = candySearchBar.text;
        
        if ([searchText length] > 0)
        {
            [filteredCandyArray removeAllObjects];
            NSDictionary *element=[NSDictionary dictionary];
            for(element in productArray)
            {
                NSString *customerName = [element objectForKey:@"Description"];
                NSRange r = [customerName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (r.length > 0)
                {
                    [filteredCandyArray addObject:element];
                }
            }
            NSMutableDictionary *sectionSearch = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in filteredCandyArray)
            {
                NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionSearch allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in filteredCandyArray)
            {
                [[sectionSearch objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each isDualBonusAppControlsection array
            for (NSString *key in [sectionSearch allKeys])
            {
                [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
            }
            [productTableView reloadData];
        }
        
    }
    else
    {
        [ candySearchBar resignFirstResponder ];
    }
}

#pragma mark - Button Action
- (void)getProductServiceDiddbGetBonusInfo:(NSArray *)info{
    isBonusRowDeleted=NO;
    bonusInfo=nil;
    bonusInfo=[NSArray arrayWithArray:info];
    AppControl *appControl = [AppControl retrieveSingleton];
    isBonusAppControl = appControl.ALLOW_BONUS_CHANGE;
    isFOCAppControl = appControl.ENABLE_MANUAL_FOC;
    isDiscountAppControl = appControl.ALLOW_DISCOUNT;
    isDualBonusAppControl = appControl.ALLOW_DUAL_FOC;
    isFOCItemAppControl=appControl.ALLOW_MANUAL_FOC_ITEM_CHANGE;
    
    if([isFOCAppControl isEqualToString:@"Y"])
    {
        isFOCAllow=YES;
        checkFOC=YES;
    }
    else
    {
        if ([isDualBonusAppControl isEqualToString:@"N"])
        {
            isFOCAllow=NO;
            checkFOC=NO;
        }
    }
    
    if([isDiscountAppControl isEqualToString:@"Y"])
    {
        isDiscountAllow=YES;
        checkDiscount = YES;
    }
    else
    {
        isDiscountAllow=YES;
        checkDiscount = NO;
    }
    
    if([isFOCItemAppControl isEqualToString:@"Y"])
    {
        isFOCItemAllow=YES;
    }
    else
    {
        isFOCItemAllow=NO;
    }
    
    if([isDualBonusAppControl isEqualToString:@"Y"])
    {
        isDualBonusAllow=YES;
    }
    else
    {
        isDualBonusAllow=NO;
    }
    
    if (bonusInfo.count > 0)
    {
        isBonus = YES;
        if([isBonusAppControl isEqualToString:@"Y"] || [isBonusAppControl isEqualToString:@"I"] || [isBonusAppControl isEqualToString:@"D"])
        {
            isBonusAllow = YES;
            checkBonus = YES;
        }
        else
        {
            isBonusAllow = NO;
            checkBonus = NO;
        }
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        isBonus = NO;
        bonus=0;
        isBonusAllow = NO;
        checkBonus = NO;
    }
    bonusEdit = @"N";
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    if([mainProductDict objectForKey:@"FOC_Product"])
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"FOC_Product"]];
    }
    else
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:mainProductDict];
        [fBonusProduct removeObjectForKey:@"Qty"];
        [fBonusProduct removeObjectForKey:@"Bonus_Product"];
        [fBonusProduct removeObjectForKey:@"StockAllocation"];
    }
    
    if([mainProductDict objectForKey:@"Bonus_Product"])
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"Bonus_Product"]];
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[mainProductDict stringForKey:@"Item_Code"]]];
        [bonusProduct removeObjectForKey:@"Qty"];
        [bonusProduct removeObjectForKey:@"FOC_Product"];
        [bonusProduct removeObjectForKey:@"StockAllocation"];
    }
    [focProductBtn setTitle:[fBonusProduct stringForKey:@"Description"] forState:UIControlStateNormal];
    
    //focProductBtn.titleLabel.text = [fBonusProduct stringForKey:@"Description"];
    //[tableView reloadData];
    info=nil;
    if(isBonusAllow)//APP CONTROL
    {
        if (bonus<=0)
        {
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        else{
            [txtRecBonus setUserInteractionEnabled:YES];
        }
    }
    else
    {
        [txtRecBonus setUserInteractionEnabled:NO];
    }
}

- (void)calculateBonus {
    
    int quantity = 0;
    bonus = 0;
    NSDictionary *tempBonus;
    BOOL isBonusGroup = YES;
    if ([[AppControl retrieveSingleton].ENABLE_BONUS_GROUPS isEqualToString:@"Y"])
    {
        NSString *query = [NSString stringWithFormat:@"Select A.* from TBL_BNS_Promotion AS A INNER JOIN TBL_Customer_Addl_Info As B ON B.Attrib_Value=A.BNS_Plan_ID Where B.Customer_ID = '%@' AND B.Site_Use_ID = '%@'",[customerDict stringForKey:@"Customer_ID"],[customerDict stringForKey:@"Site_Use_ID"]];
        if ([[SWDatabaseManager retrieveManager] fetchDataForQuery:query].count==0)
        {
            isBonusGroup = NO;
        }
        else
        {
            isBonusGroup = YES;
        }
    }
    else
    {
        isBonusGroup = YES;
    }
    
    if (isBonusGroup) {
        if ([mainProductDict objectForKey:@"Qty"])
        {
            quantity = [[mainProductDict objectForKey:@"Qty"] intValue];
            for(int i=0 ; i<[bonusInfo count] ; i++ )
            {
                NSDictionary *row = [bonusInfo objectAtIndex:i];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    
                    if(checkBonus)
                    {
                        isBonusAllow = YES;
                    }
                    if ([[AppControl retrieveSingleton].BONUS_MODE isEqualToString:@"DEFAULT"])
                    {
                        if(i == [bonusInfo count]-1)
                        {
                            if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                            {
                                int dividedValue = quantity / rangeStart ;
                                bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                                tempBonus=row;
                            }
                            
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                            {
                                int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                                bonus = floor(dividedValue) ;
                                tempBonus=row;
                                
                            }
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"POINT"])
                            {
                                bonus = [[row objectForKey:@"Get_Qty"] intValue];
                                tempBonus=row;
                                
                            }
                        }
                        else
                        {
                            bonus = [[row objectForKey:@"Get_Qty"] intValue];
                            tempBonus=row;
                        }
                        
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue] + floorf(([[mainProductDict objectForKey:@"Qty"] intValue] - rangeStart) *[[row objectForKey:@"Get_Add_Per"] floatValue]);
                        tempBonus=row;
                    }
                    break;
                }
                else
                {
                    if(checkBonus)
                    {
                        isBonusAllow = NO;
                    }
                }
            }
        }
        
        if (bonus > 0 && bonusInfo)
        {
            if(![[tempBonus stringForKey:@"Get_Item"] isEqualToString:[bonusProduct stringForKey:@"Item_Code"]])
            {
                NSArray *temBonusArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:[NSString stringWithFormat:@"select * from TBL_Product where Item_Code ='%@'",[tempBonus stringForKey:@"Get_Item"]]];
                bonusProduct=nil;
                bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[temBonusArray objectAtIndex:0]];
                [bonusProduct removeObjectForKey:@"Qty"];
                [bonusProduct removeObjectForKey:@"FOC_Product"];
                [bonusProduct removeObjectForKey:@"StockAllocation"];
            }
            
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Def_Qty"];
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Qty"];
            
            if(!isDualBonusAllow)
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct removeObjectForKey:@"Def_Qty"];
                
            }
            if(isDiscountAllow)
            {
                [mainProductDict setValue:@"0" forKey:@"Discount"];
            }
        }
        else
        {
            [bonusProduct removeObjectForKey:@"Qty"];
            [bonusProduct removeObjectForKey:@"Def_Qty"];
            
        }
        
    }
    
}
- (void)updatePrice{
    
    //NSLog(@"OLA!!******* mainProduct at begin updatePrice %@",mainProductDict);
    
    if ([mainProductDict stringForKey:@"Qty"].length > 0)
    {
        int qty = [[mainProductDict stringForKey:@"Qty"] intValue];
        if (qty > 0)
        {
            
            NSMutableString *wholesalePrice;
            
            
            if (txtfldWholesalePrice.text) {
                if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""]){
                    
                    
                    //unit price issue 1,000 to 1 fixed  by syed (Comma issue)
                    NSString * stringValue = txtfldWholesalePrice.text;
                    NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                    
                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
                }
                else
//                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
                
                
                
                //update netprice
                
                
                
                
                
                
                

            }
            else
                wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
            
            float totalPrice = (double)qty * ([wholesalePrice floatValue]);
            
            NSLog(@"total val %f",[wholesalePrice doubleValue]);
            //            NSLog(@"Price %f",[[mainProductDict stringForKey:@"Net_Price"] doubleValue]);
            //            NSLog(@"Price %f",(double)qty);
            //            NSLog(@"Price %f",totalPrice);
            if(bonus<=0 || !checkDiscount)
            {
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                
                if ([self.parentViewString isEqualToString:@"MO"]) {
                    
                    
                    //                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),[[mainProductDict valueForKey:@"Price"] floatValue] ,NSLocalizedString(@"Discounted Price", nil),[[mainProductDict valueForKey:@"Discounted_Price"] floatValue] ,NSLocalizedString(@"Discounted Amount", nil), [[mainProductDict valueForKey:@"Discounted_Amount"] floatValue]]];
                    
                    
                    
                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),[[mainProductDict valueForKey:@"Net_Price"] floatValue] ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                }
                
                
                else
                {
                    
                    
                    
                    
                    [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                    [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                    [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                    
                    
                    NSLog(@"check dict at price here %@", [mainProductDict description]);
                    
                    
                    
                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                   // lblPriceLabel.text=[NSString stringWithFormat:@"%0.2f",]
                }
            }
            
            else
            {
                //                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                //                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Discounted_Price"];
                //
                //                //why si discount hardcoded as 0.0?
                //
                //                [mainProductDict setValue:@"0.00" forKey:@"Discounted_Amount"];
                //
                //                [lblPriceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Item Price", nil), [mainProductDict currencyStringForKey:@"Price"]]];
                
                
                
                
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                
                
                NSLog(@"check discount amount in main product dict %@", [mainProductDict valueForKey:@"Discounted_Amount"]);
                
                
                
                [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                
                
            }
        }
        else{
            [lblPriceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Item Price", nil), [mainProductDict currencyStringForKey:@"Price"]]];
            
        }
        //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    }
    else
    {
        [lblPriceLabel setText:@""];
        //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
    
    //NSLog(@"OLA!!******* mainProduct at end updatePrice %@",mainProductDict);
}

-(void)saveAsTemplateAction:(id)sender
{
    NSLog(@"Save As Template");
}
-(IBAction)resetAction:(id)sender
{
    //lblProductName.text = @"Product Name";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
    [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
    mainProductDict = nil;
    bonusProduct = nil;
    fBonusProduct = nil;
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
}



-(void)ResetData
{
}


-(IBAction)saveOrderAction:(id)sender
{
    NSLog(@"wholesale price is %@", txtfldWholesalePrice.text);
    
    if ([self.isOrderConfirmed isEqualToString:@"confirmed"]) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Confirmed orders cannot be edited." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    [txtProductQty resignFirstResponder];
    [txtDefBonus resignFirstResponder];
    [txtRecBonus resignFirstResponder];
    [txtDiscount resignFirstResponder];
    [txtFOCQty resignFirstResponder];
    [txtNotes resignFirstResponder];
    
    
    
    
    
    if (![txtProductQty.text isEqualToString:@""]) {
        
        [txtfldWholesalePrice resignFirstResponder];
        
        [self saveProductOrder];
        
        [oldSalesOrderVC productAdded:mainProductDict];
        mainProductDict=nil;
        fBonusProduct= nil;
        bonusProduct = nil;
        [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
        
        [productTableView deselectRowAtIndexPath:[productTableView indexPathForSelectedRow] animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please enter quantity." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        //[ILAlertView showWithTitle:NSLocalizedString(@"Error", nil) message:@"Please enter quantity." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
    }
    
}

- (void)saveProductOrder {
    
    
    
    
    NSLog(@"price list id in save order %@", [customerDict valueForKey:@"Price_List_ID"]);
    NSLog(@"item id in save order is %@", [mainProductDict valueForKey:@"ItemID"]);
    NSLog(@"inventory item id while saving order is %@", [mainProductDict valueForKey:@"Inventory_Item_ID"]);
    
    
    
    
    
    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict valueForKey:@"Inventory_Item_ID"] :[customerDict valueForKey:@"Price_List_ID"]]];
    
    
    
    NSArray* tempProd=[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict valueForKey:@"Inventory_Item_ID"] :[customerDict valueForKey:@"Price_List_ID"]];
    
    NSLog(@"temp prod al seer in save order %@", [tempProd objectAtIndex:0]);
    
    NSMutableArray* testProdArray=[[NSMutableArray alloc] init];
    
   
    for (NSInteger i=0; i<tempProd.count; i++) {
        
        
        if ([[[tempProd valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:selectedUOMReference]) {
            
            
            [testProdArray addObject:[tempProd objectAtIndex:i]];
        }
        
    }

    
    //check price update here
    NSMutableArray* filteredPriceDetailUpdate=[[NSMutableArray alloc]init];
    
    
    NSLog(@"check uom %@", [mainProductDict valueForKey:@"Order_Quantity_UOM"]);
    
    
    
    NSString* checkselectedUOM=[mainProductDict valueForKey:@"Order_Quantity_UOM"];
    
    
    
    for (NSInteger i=0; i<tempProd.count; i++) {
        
        NSString* uomforPriceDetail=[mainProductDict valueForKey:@"Order_Quantity_UOM"];
        
        if ([uomforPriceDetail isEqualToString:checkselectedUOM]) {
            
            
            [filteredPriceDetailUpdate addObject:[tempProd objectAtIndex:i]];
            
            
        }
        
        
        
        
    }
    if (filteredPriceDetailUpdate.count>0) {
        
        tempProd=filteredPriceDetailUpdate;
    }
    
    

    
    
    NSLog(@"test prod array in save order %@", [testProdArray description]);
    
    
    //NSString * uspVal=[[testProdArray valueForKey:@"Unit_Selling_Price" ] objectAtIndex:0];
    
    
    if (testProdArray.count>0) {
        [ mainProductDict setValue:[[testProdArray valueForKey:@"Unit_Selling_Price" ] objectAtIndex:0]   forKey:@"Net_Price"];

        
        if (selectedUOMReference) {
            
            
            //chnage this str here
            [ mainProductDict setValue:selectedUOMReference   forKey:@"Order_Quantity_UOM"];

            
        }
    }
    
    
    
    
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
    isSaveOrder = YES;
    int totalBonus = 0 ;
    [mainProductDict removeObjectForKey:@"FOC_Product"];
    [mainProductDict removeObjectForKey:@"Bonus_Product"];
    
    [mainProductDict setValue:bonusProduct forKey:@"Bonus_Product"];
    [mainProductDict setValue:fBonusProduct forKey:@"FOC_Product"];
    
    BOOL isBothBonus = NO;
    if([bonusProduct objectForKey:@"Qty"])
    {
        totalBonus = [[bonusProduct stringForKey:@"Qty"] intValue];
        if(!isDualBonusAllow)
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
        else if([fBonusProduct objectForKey:@"Qty"])
        {
            isBothBonus = YES;
            totalBonus = totalBonus + [[fBonusProduct stringForKey:@"Qty"] intValue];
        }
        else
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
    }
    else
    {
        [mainProductDict removeObjectForKey:@"Bonus_Product"];
    }
    
    if([fBonusProduct objectForKey:@"Qty"] && !isBothBonus)
    {
        totalBonus = [[fBonusProduct stringForKey:@"Qty"] intValue];
    }
    else if(!isBothBonus)
    {
        [mainProductDict removeObjectForKey:@"FOC_Product"];
    }
    
    
    [mainProductDict setValue:[NSString stringWithFormat:@"%d",totalBonus] forKey:@"Def_Bonus"];
    [mainProductDict setValue:[mainProductDict stringForKey:@"Discount"] forKey:@"DiscountPercent"];
    
    if(![mainProductDict objectForKey:@"StockAllocation"])
    {
        // [mainProductDict setValue:stockItems forKey:@"StockAllocation"];
    }
    else
    {
    }
    //NSLog(@"Product %@",mainProductDict);
    //[self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    //[self.navigationController  popViewControllerAnimated:YES];
    
    
    if(txtfldWholesalePrice.text)
    {
        
        NSMutableString *wholesalePrice;
        if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""])
        {
            
            // NSLog(@"OLA!!***** WP 2 %@",txtfldWholesalePrice.text );
            
            //unit price fix 1,000 to 1 fixed by syed (comma issue)
            NSString * stringValue = txtfldWholesalePrice.text;
            NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
            
            
            
            
            
            //NSString *wholesalePrice = [txtfldWholesalePrice.text substringFromIndex:3];
            [mainProductDict setObject:wholesalePrice forKey:@"Net_Price"];
        }
        
    }
    
    
    
    //fix for price getting updated
    
    
    NSLog(@"UOM code is %@", lblUOMCode.text);
    NSLog(@"wholesale price is %@", lblTitleWholesalePrice.text);
    NSLog(@"wholesale price is %@", txtfldWholesalePrice.text);
    NSLog(@"selected uom is is %@", selectedUOMReference);
    NSLog(@"quantity is  %@", txtProductQty.text);

    
    
    

    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
    

    lblProductName.text = @"";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
}

-(IBAction)buttonAction:(id)sender
{
    [self.view endEditing:YES];
    
    //    if (mainProductDict==nil) {
    //
    //
    //    ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
    //    [stockViewController setDelegate:self];
    //    [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
    //    [stockViewController setTarget:self];
    //    [stockViewController setAction:@selector(stockAllocated:)];
    //
    //    UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
    //    modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //    modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    //    [self presentViewController:modalViewNavController animated:YES completion:nil];
    //
    //    }
    
    
    
    int tag = ((UIButton *)sender).tag;
    switch (tag)
    {
        case 1:
            if (isPproductDropDownToggled)
            {
                isPproductDropDownToggled=NO;
                focProductTableView.hidden=YES;
                
                
                customPopOver=nil;
                
                customPopOver = [[StringPopOverViewController alloc] initWithStyle:UITableViewStylePlain];
                //customPopOver.colorNames = outletArray;// [NSMutableArray arrayWithObjects:@"PERMANENT",@"Evaluation - 30 days", @"Evaluation - 60 days", @"Evaluation - 90 days", nil];
                customPopOver.delegate = self;
                productDropDown.frame=CGRectMake(599, 133, 241, 340);
                
                
                
                
                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,0) withDuration:0.2];
            }
            else
            {
                isPproductDropDownToggled=YES;
                focProductTableView.hidden=NO;
                productDropDown.frame=CGRectMake(599, 133, 241, 340);
                
                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,383) withDuration:0.2];
            }
            break;
        case 11:
        {
            if (mainProductDict.count!=0)
            {
                //NSLog(@"Stock");
                //productStockViewController=nil;
                ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
                [stockViewController setDelegate:self];
                [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
                [stockViewController setTarget:self];
                [stockViewController setAction:@selector(stockAllocated:)];
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
            }
        }
            break;
        case 12:
        {
            if (mainProductDict.count!=0)
            {
                ///NSLog(@"Bonus");
                //productBonusViewController=nil;
                ProductBonusViewController *bonusViewController = [[ProductBonusViewController alloc] initWithProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:bonusViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
            }
            
        }
            break;
        case 13:
        {
            if (mainProductDict.count!=0)
            {
                //NSLog(@"History");
                //customerSalesVC=nil;
                CustomerSalesViewController *salesHistory = [[CustomerSalesViewController alloc] initWithCustomer:customerDict andProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:salesHistory];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
            }
        }
            break;
        case 5:
            //Toggel
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)textViewDidEndEditing:(UITextView *)textView{
    //if ([key isEqualToString:@"lineItemNotes"])
    if(textView==txtNotes)
    {
        [mainProductDict setValue:textView.text forKey:@"lineItemNotes"];
        txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
        
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    NSString* output = nil;
    
    
    
    
    
    //     if (textField.text!=tempStr) {
    //           NSLog(@"clear");
    //    //
    //           tempStr=nil;
    //          NSLog(@"text field after nill is %@", tempStr);
    //
    //
    //     }
    
    
    
    NSLog(@"main product dic desc %@", [mainProductDict description]);
    
    
}





- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if(noPricesAvailableStatus==YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No prices availble for selected product.Please select another product" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];

        return NO;
    }
    if (mainProductDict.count==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select product." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        isErrorSelectProduct = YES;
        [self performSelector:@selector(resignedTextField:) withObject:textField afterDelay:0.0];
        
    }
    
    if (textField==txtDiscount) {
        textField.text=@"";
    }
    
    return YES;
}
-(void)resignedTextField:(UITextField *)textField
{
    [textField resignFirstResponder];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //    NSLog(@"text fileds text in did end %@", tempStr);
    //
    //    if (textField.text!=tempStr) {
    //        NSLog(@"clear");
    //
    //        tempStr=nil;
    //        NSLog(@"text field after nill is %@", tempStr);
    //
    ////        lblProductName.text = @"Product Name";
    ////        lblProductCode.text =@"";
    ////        lblProductBrand.text=@"";
    ////        lblWholesalePrice.text=@"";
    ////        txtfldWholesalePrice.text = @"";
    ////        lblRetailPrice.text=@"";
    ////        lblUOMCode.text=@"";
    ////        lblAvlStock.text=@"";
    ////        lblExpDate.text=@"";
    ////        lblPriceLabel.text=@"";
    //
    //        txtProductQty.text=@"";
    //        txtDefBonus.text=@"";
    //        txtRecBonus.text=@"";
    //        txtDiscount.text=@"";
    //        txtFOCQty.text=@"";
    //        txtNotes.text=@"";
    //        [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
    //        mainProductDict = nil;
    //        bonusProduct = nil;
    //        fBonusProduct = nil;
    //        for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
    //            [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    //        }
    //        for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
    //            [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    //        }
    //        [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
    
    
    
    //[self ResetData];
    // }
    
    //    tempStr=nil;
    //
    //
    //    tempStr=textField.text;
    //    //.. temp=textField.text ;
    //
    //    NSLog(@"temp is %@", textField.text);
    
    
    
    if (mainProductDict.count>0)
    {

    
    NSLog(@"main product dic desc %@", [mainProductDict description]);
    
    //deleting allocated text field data in stock info
    
    [mainProductDict removeObjectForKey:@"StockAllocation"];
    
    NSLog(@"removed %@",[mainProductDict description] );
    
    [textField resignFirstResponder];
    NSCharacterSet *unacceptedInput = nil;
    NSString *zeroString=@"0";
    
    // textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    if (textField==txtFOCQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            
            if ([textField.text length]!=0)
            {
                [fBonusProduct setValue:textField.text forKey:@"Qty"];
                [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
            }
            else
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Bonus quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            [fBonusProduct removeObjectForKey:@"Qty"];
            [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
        }
    }
    if (textField==txtRecBonus)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            [bonusProduct setValue:textField.text forKey:@"Qty"];
            if([isBonusAppControl isEqualToString:@"Y"])
            {
                bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                bonusEdit = @"Y";
            }
            else if([isBonusAppControl isEqualToString:@"I"])
            {
                if([[bonusProduct stringForKey:@"Qty"] intValue] >= bonus)
                {
                    bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                    bonusEdit = @"Y";
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Bonus can increase only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                    [alert show];
                    [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                }
            }
            else if([isBonusAppControl isEqualToString:@"D"])
            {
                if([[bonusProduct stringForKey:@"Qty"] intValue] <= bonus)
                {
                    bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                    bonusEdit = @"Y";
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Bonus can decrease only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                    [alert show];
                    [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                }
            }
            
            if (bonus==0)
            {
                [bonusProduct removeObjectForKey:@"Qty"];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Bonus quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            [bonusProduct removeObjectForKey:@"Qty"];
            
        }
    }
    if (textField==txtDiscount)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            if ([textField.text intValue]>100) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Discount (%) should not be more than 100%" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                [alert show];
                [mainProductDict removeObjectForKey:@"Discount"];
            }
            else
            {
                [mainProductDict setValue:textField.text forKey:@"Discount"];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Discount (%) value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
            [alert show];
            [mainProductDict removeObjectForKey:@"Discount"];
            
        }
    }
    if (textField==txtProductQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            
            [mainProductDict setValue:textField.text forKey:@"Qty"];
            if (bonusInfo.count > 0 || ![[mainProductDict stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
            {
                if([bonusEdit isEqualToString:@"N"])
                {
                    [self calculateBonus];
                }
                else
                {
                    [self calculateBonus];
                    
                    bonusEdit = @"N";
                }
            }
        }
        else
        {
            if (!isErrorSelectProduct) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
            }
            isErrorSelectProduct=NO;
            
            [mainProductDict setValue:@"" forKey:@"Qty"];
        }
    }
    
    
    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProductwithUOM:[mainProductDict stringForKey:@"ItemID"] UOM:selectedUOMReference :[customerDict stringForKey:@"Price_List_ID"]]];
    
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
    
    
    
    
    //Bonus
    if(![fBonusProduct objectForKey:@"Qty"]  || isDualBonusAllow)
    {
        txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
    }
    
    
    //OLA!!
    if(isBonusAllow)//APP CONTROL
    {
        if (bonus<=0){
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        else{
            [txtRecBonus setUserInteractionEnabled:YES];
        }
        
    }
    else
    {
        [txtRecBonus setUserInteractionEnabled:NO];
        
        
    }
    
    
    //MFOC
    if(![bonusProduct objectForKey:@"Qty"] || isDualBonusAllow)
    {
        [txtFOCQty setText:[fBonusProduct stringForKey:@"Qty"]];
    }
    //[((SWTextFieldCell *)cell).secondLabel setText:[fBonusProduct stringForKey:@"Description"]];
    
    if(isFOCAllow)//APP CONTROL
    {
        [txtFOCQty setUserInteractionEnabled:YES];
        focProductBtn.userInteractionEnabled=YES;
    }
    else
    {
        [txtFOCQty setUserInteractionEnabled:NO];
        focProductBtn.userInteractionEnabled=NO;
    }
    
    if(isFOCItemAllow)
    {
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        focProductBtn.userInteractionEnabled=YES;
    }
    else
    {
        focProductBtn.userInteractionEnabled=NO;
    }
    //Discount
    NSString *discountString = [mainProductDict stringForKey:@"Discount"];
    
    [txtDiscount setText: discountString];
    //[((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Discount", nil)];
    
    if([isDiscountAppControl isEqualToString:@"Y"])//APP CONTROL
    {
        [txtDiscount setUserInteractionEnabled:YES];
    }
    else
    {
        [txtDiscount setUserInteractionEnabled:NO];
        //((SWTextFieldCell *)cell).label.textColor = [UIColor grayColor];
    }
    //return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"OLA!!***** WP %@",txtfldWholesalePrice.text );
    
    [textField resignFirstResponder];
    return YES;
}


//restrictiong text view notes to 50 characters

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    else if([[textView text] length] > 49 )
    {
        return NO;
    }
    return YES;
    //return textView.text.length + (text.length - range.length) <= 140;
}


//restrict characters here
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //restricting length to 50
    
    
    
    
    
    NSRange spaceRange = [string rangeOfString:@" "];
    if (spaceRange.location != NSNotFound)
    {
        UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have entered wrong input" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        error.tag=101;
        [error show];
        return NO;
    } //    /* for backspace */
    //    if([string length]==0){
    //        return YES;
    //    }
    //
    //    /*  limit to only numeric characters  */
    //
    //    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //    for (int i = 0; i < [string length]; i++) {
    //        unichar c = [string characterAtIndex:i];
    //        if ([myCharSet characterIsMember:c]) {
    //            return YES;
    //        }
    //    }
    //
    //    UIAlertView *specialCharacterAlert=[[UIAlertView alloc]initWithTitle:@"Special Character Entered" message:@"Please enter numeric value" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //    specialCharacterAlert.tag=100;
    //    [specialCharacterAlert show];
    //
    //
    //    return NO;
    
    
    
    
    if([textField.text hasPrefix:@"0"])
    {
        //
        //        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Allocated quantity cannot be Zero" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //        [alert show];
        //           output = [textField.text substringFromIndex:1];
        //           NSLog(@"out put %@", output);
        //
        textField.text=@"";
        //
        //
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //OLA!
    if ([textField isEqual:txtRecBonus]) {
        if ([txtProductQty.text isEqualToString:@""] || [txtProductQty.text isEqual:[NSNull null]]) {
            return NO;
        }
    }
    
    if ([textField isEqual:txtProductQty]) {
        /*
         NSString *finalText = [txtProductQty.text stringByAppendingString:string];
         
         if ([finalText isEqualToString:@""]) {
         [txtRecBonus setText:@""];
         [txtDefBonus setText:@""];
         [txtRecBonus setUserInteractionEnabled:NO];
         
         }*/
        
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0) {
            // Game on: when you return YES from this, your field will be empty
            [txtRecBonus setText:@""];
            [txtDefBonus setText:@""];
            [txtRecBonus setUserInteractionEnabled:NO];
            
            // [bonusProduct setValue:@"0" forKey:@"Qty"];
            
        }
    }
    
    if ([textField isEqual:txtfldWholesalePrice]) {
        
        NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSLog(@"resulting string would be: %@", resultString);
        NSString *prefixString = [[SWDefaults userProfile]valueForKey:@"Currency_Code"];
        NSRange prefixStringRange = [resultString rangeOfString:prefixString];
        if (prefixStringRange.location == 0) {
            // prefix found at the beginning of result string
            
            NSString *allowedCharacters;
            if ([textField.text componentsSeparatedByString:@"."].count >= 2)//no decimal point yet, hence allow point
                allowedCharacters = @"01234567890";
            else
                allowedCharacters = @"0123456789.";
            
            if ([[string stringByReplacingOccurrencesOfString:prefixString withString:@""] rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
            {
                // BasicAlert(@"", @"This field accepts only numeric entries.");
                
                return NO;
            }
        }
        else
            return NO;
    }
    else if ([textField isEqual:txtDiscount])
    {
        NSString *allowedCharacters = @"0123456789";
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            
            return NO;
        }
        
    }
    return YES;
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        
        if (!isPinchDone)
        {
            isPinchDone=YES;
            //        recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
            //        recognizer.scale = 1;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(251, 8, 773, 690) withDuration:0.5];
            
            //recognizer.view.frame = CGRectMake(258, 8, 756, 690);
        }
        else
        {
            isPinchDone=NO;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(258, 324, 756, 374) withDuration:0.5];
            
            recognizer.view.frame = CGRectMake(258, 324, 756, 374);
            // recognizer.scale = 1;
            
        }
    }
}

#pragma mark - ProductStockViewControllerDelegate methods

//OLA!
-(void)didTapSaveButton
{
    if (isStockToggled)
    {
        [productTableView setUserInteractionEnabled:YES];//enable product table selection when draggable view is hidden. OLA!
        
        isStockToggled=NO;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(1024, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(999, 0, 30, 605) withDuration:0.5];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)UOMPickerButton:(id)sender {
    
    UIButton *btn  = (UIButton *)sender;
    outletPopOver = nil;
    valuePopOverController=nil;
    
    outletPopOver = [[OutletPopoverTableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    
    
    //display values for pop over
    
    NSMutableArray* UOMList=[[SWDatabaseManager retrieveManager] fetchUOMforItem:[NSString stringWithFormat:@"%@",[mainProductDict stringForKey:@"Item_Code"]]];
    NSLog(@"get UOM %@", [UOMList valueForKey:@"Item_UOM"]);
    
    
    
    
    
//    if (UOMList.count==0) {
//        
//        UIAlertView* emptyUOMArr=[[UIAlertView alloc] initWithTitle:@"Select The Order" message:<#(NSString *)#> delegate:<#(id)#> cancelButtonTitle:<#(NSString *)#> otherButtonTitles:<#(NSString *), ...#>, nil]
//        
//    }
    
    
    
    outletPopOver.colorNames = [UOMList valueForKey:@"Item_UOM"];// [NSMutableArray arrayWithObjects:@"PERMANENT",@"Evaluation - 30 days", @"Evaluation - 60 days", @"Evaluation - 90 days", nil];
    outletPopOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:outletPopOver];
        [valuePopOverController presentPopoverFromRect:btn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }

}

-(void)selectedStringValue:(id)value

{
    selectedUOMReference=value;
     [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProductwithUOM:[mainProductDict stringForKey:@"ItemID"] UOM:value :[customerDict stringForKey:@"Price_List_ID"]]];
    [self updatePrice];
     lblUOMCode.text=value;
     [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];

      if (txtfldWholesalePrice.isHidden==NO) {

        [txtfldWholesalePrice setText: [[mainProductDict stringForKey:@"Net_Price"] currencyString]];

    }
    [mainProductDict setObject:selectedUOMReference forKey:@"SelectedUOM"];
     //Dismiss the popover if it's showing.
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    customPopOver.delegate=nil;
    customPopOver=nil;

}
/** duplicate*/
-(void)selectedStringValueDuplicate:(id)value

{
    selectedUOMReference=value;
    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProductwithUOM:[mainProductDict stringForKey:@"ItemID"] UOM:value :[customerDict stringForKey:@"Price_List_ID"]]];
    [self updatePrice];
    lblUOMCode.text=value;
    [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    
    if (txtfldWholesalePrice.isHidden==NO) {
        
        [txtfldWholesalePrice setText: [[mainProductDict stringForKey:@"Net_Price"] currencyString]];
        
    }
    [mainProductDict setObject:selectedUOMReference forKey:@"SelectedUOM"];
    //Dismiss the popover if it's showing.
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    customPopOver.delegate=nil;
    customPopOver=nil;
    
}


@end
