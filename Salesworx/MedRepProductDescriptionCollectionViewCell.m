//
//  MedRepProductDescriptionCollectionViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/12/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepProductDescriptionCollectionViewCell.h"

@implementation MedRepProductDescriptionCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        
        
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MedRepProductDescriptionCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
        
    }
    
    return self;
    
}



@end
