//
//  SalesWorxDistributionItemNewTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/15/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxDistributionItemNewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCode;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblExpiry;
@property (weak, nonatomic) IBOutlet UIView *statusView;

@end
