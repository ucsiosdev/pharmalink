//
//  MedRepPlacesSearchViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Place.h"
#import "CSMapAnnotation.h"

@protocol SelectedLocationCoordinateDelegate <NSObject>

-(void)selectedLocation:(CLLocationCoordinate2D)location;

@end


@interface MedRepPlacesSearchViewController : UIViewController<UISearchBarDelegate,UISearchDisplayDelegate,MKMapViewDelegate>

{
    MKLocalSearch *localSearch;
    MKLocalSearchResponse *results;
    Place* place;
    
    CSMapAnnotation *csAnnotation;
    
    id delegate;

}
@property (strong, nonatomic) IBOutlet UISearchBar *locationsSearchBar;

@property (strong, nonatomic) IBOutlet MKMapView *locationsMapView;

@property (nonatomic, assign) id <SelectedLocationCoordinateDelegate>delegate;

- (IBAction)closeButtonTapped:(id)sender;

@end
