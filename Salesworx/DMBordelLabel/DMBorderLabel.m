#import "DMBorderLabel.h"

@implementation DMBorderLabel

- (void)drawTextInRect:(CGRect)rect {
  UIEdgeInsets insets = {self.topInset, self.leftInset, self.bottomInset, self.rightInset};
  
  return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

- (CGSize) intrinsicContentSize {
  CGSize superSize = [super intrinsicContentSize] ;
  superSize.height += self.topInset + self.bottomInset;
  superSize.width += self.leftInset + self.rightInset;
  return superSize ;
}

@end
