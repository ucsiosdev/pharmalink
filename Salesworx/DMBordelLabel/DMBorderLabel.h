#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface DMBorderLabel : UILabel

@property (nonatomic) IBInspectable CGFloat topInset;
@property (nonatomic) IBInspectable CGFloat bottomInset;
@property (nonatomic) IBInspectable CGFloat leftInset;
@property (nonatomic) IBInspectable CGFloat rightInset;

@end