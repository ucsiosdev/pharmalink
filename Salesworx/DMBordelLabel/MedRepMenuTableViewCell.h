//
//  MedRepMenuTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepMenuTableViewCell : UITableViewCell

{
    BOOL disableBackgroundColorChange;
}
@property (strong, nonatomic) IBOutlet UIView *bgView;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UIImageView *cellImageView;
@property (strong, nonatomic) IBOutlet UIButton *randomButton;
-(void)addDropShadow;

@end
