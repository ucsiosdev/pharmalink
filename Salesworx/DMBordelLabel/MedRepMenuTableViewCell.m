//
//  MedRepMenuTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepMenuTableViewCell.h"
#import "MedRepDefaults.h"

#define ROUND_BUTTON_WIDTH_HEIGHT 45

@implementation MedRepMenuTableViewCell

@synthesize randomButton,bgView;


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //add shadow properties by using layers
    self.layer.shadowOpacity = 0.8f; //self is the custom cell
    self.layer.shadowColor = [UIColor redColor].CGColor; //set the shadow color
    
    // .. other layer properties and effects u want
    
}




- (void)awakeFromNib {
    // Initialization code
    
    
    [super awakeFromNib];
    
    
//    for (UIView * bottomView in self.subviews) {
//        
//        if ([bottomView isKindOfClass:[UIView class]]&& bottomView.tag==1001) {
//            bottomView.layer.shadowColor = [UIColor grayColor].CGColor;
//            bottomView.layer.shadowOffset = CGSizeMake(0, 2);
//            bottomView.layer.shadowOpacity = 1;
//            bottomView.layer.shadowRadius = 1.0;
//            
//        }
//        
//    }
    
//
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    [button setImage:[UIImage imageNamed:@"TimoonPumba.png"] forState:UIControlStateNormal];
//    
//    [button addTarget:self action:@selector(roundButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
//    
//    //width and height should be same value
//    button.frame = CGRectMake(0, 0, ROUND_BUTTON_WIDTH_HEIGHT, ROUND_BUTTON_WIDTH_HEIGHT);
//    
//    //Clip/Clear the other pieces whichever outside the rounded corner
//    button.clipsToBounds = YES;
//    
//    //half of the width
//    button.layer.cornerRadius = ROUND_BUTTON_WIDTH_HEIGHT/2.0f;
//    button.layer.borderColor=[UIColor redColor].CGColor;
//    button.layer.borderWidth=2.0f;
    
   // [self.contentView addSubview:button];
    
    
//    
//    randomButton.layer.cornerRadius = 25;
//    randomButton.layer.borderWidth = 1.0;
//    randomButton.layer.masksToBounds = YES;
//    UIColor* buttonBGColor=[MedRepDefaults generateRandomColor];
//    randomButton.backgroundColor=buttonBGColor;
//    
//    [randomButton setBackgroundImage:[self imageWithColor:buttonBGColor] forState:UIControlStateHighlighted];
//    [randomButton setBackgroundImage:[self imageWithColor:buttonBGColor] forState:UIControlStateSelected];
//    
//    
//    randomButton.layer.borderColor = buttonBGColor.CGColor;
    
    
    
   

    
    
}




- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    
//    
//    
//    UIColor *color = [[self.contentView.subviews lastObject]backgroundColor];
//    [super setSelected:selected animated:animated];
//    
//    if (selected){
//        [self.contentView.subviews lastObject].backgroundColor = color;
//    }
//    
//    UIView * selectedBackgroundView = [[UIView alloc] init];
//    [selectedBackgroundView setBackgroundColor:[UIColor whiteColor]]; // set color here
//    [self setSelectedBackgroundView:selectedBackgroundView];
//
//}
//
//-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
//    UIColor *color = [self.contentView.subviews lastObject].backgroundColor;
//    [super setHighlighted:highlighted animated:animated];
//    
//    if (highlighted){
//        [self.contentView.subviews lastObject].backgroundColor = color;
//    }
//}





@end
