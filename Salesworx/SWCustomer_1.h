//
//  SWCustomer.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWCustomer_1 : NSObject {
    NSInteger customerId;
    NSInteger siteUseId;
    NSString *customerNumber;
    NSString *location;
    NSString *customerName;
    NSString *address;
    NSString *city;
    NSString *postalCode;
    NSString *customerStatus;
    NSString *customerBarCode;
    NSString *department;
    NSString *customerSegmentId;
    NSString *salesDistrictId;
    NSInteger salesRepId;
}

@property (nonatomic, strong) NSString *customerNumber;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *customerName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *customerStatus;
@property (nonatomic, strong) NSString *customerBarCode;
@property (nonatomic, strong) NSString *department;
@property (nonatomic, strong) NSString *customerSegmentId;
@property (nonatomic, strong) NSString *salesDistrictId;

@property (nonatomic, assign) NSInteger customerId;
@property (nonatomic, assign) NSInteger siteUseId;
@property (nonatomic, assign) NSInteger salesRepId;

@end