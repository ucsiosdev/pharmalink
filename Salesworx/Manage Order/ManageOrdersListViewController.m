//
//  ManageOrdersListViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "ManageOrdersListViewController.h"
#import "SWDatabaseManager.h"



@interface ManageOrdersListViewController ()

@end

@implementation ManageOrdersListViewController
@synthesize currentVisit;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    manageOrdersArray=[[NSMutableArray alloc]init];
    categoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:currentVisit];
    currentVisit.visitOptions.salesOrder.productCategoriesArray=[[SWDatabaseManager retrieveManager] fetchProductCategoriesforCustomer:currentVisit];
}
-(void)showNavigationBarTitle
{
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:kManageOrdersScreenTitle];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated

{
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    [self showNavigationBarTitle];
    [self showCustomerDetails];
    isInavlidUnconfirmedOrdersFiltered=NO;
    
   NSArray *confirmOrdersArray=[[SWDatabaseManager retrieveManager] dbGetConfirmOrder:currentVisit.visitOptions.customer.Ship_Customer_ID withSiteId:currentVisit.visitOptions.customer.Ship_Site_Use_ID];
   NSArray *proformaOrdersArray= [[SWDatabaseManager retrieveManager] dbGetPerformaOrder:currentVisit.visitOptions.customer.Ship_Customer_ID withSiteId:currentVisit.visitOptions.customer.Ship_Site_Use_ID];
    

    manageOrdersArray=[[NSMutableArray alloc]init];
    NSMutableArray *tempProformaOrdersArray=[[NSMutableArray alloc]init];
    for (NSDictionary *proformaOrder in proformaOrdersArray) {
        ManageOrderDetails *mangeOrder=[[ManageOrderDetails alloc]init];
        
        /* checking  oproforma order category is exist or not*/
        NSPredicate* orderCategoryPredicate=[NSPredicate predicateWithFormat:@"SELF.Category == [cd] %@ AND SELF.Org_ID == [cd] %@",[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Custom_Attribute_2"]],[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Custom_Attribute_3"]]];
        NSLog(@"template predicate is %@", orderCategoryPredicate);
        NSMutableArray* filteredCategoriesArray=[[categoriesArray filteredArrayUsingPredicate:orderCategoryPredicate] mutableCopy];
        if(filteredCategoriesArray.count!=1) /*if category not exist removing the order*/
        {
            isInavlidUnconfirmedOrdersFiltered=YES;
            continue;
        }
        
        mangeOrder.OrderAmount=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Order_Amt"]];
        mangeOrder.manageOrderStatus=KUnConfirmOrderStatusString;
        mangeOrder.Orig_Sys_Document_Ref=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Orig_Sys_Document_Ref"]];
        mangeOrder.Row_ID=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Row_ID"]];
        mangeOrder.Visit_ID=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Visit_ID"]];
        
        if([[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Custom_Attribute_1"]]isEqualToString:@"F"])
        {
            mangeOrder.FOCParentOrderOrderNumber=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Custom_Attribute_4"]];
            mangeOrder.FOCOrderNumber=nil;
        }
        else
        {
            mangeOrder.FOCParentOrderOrderNumber=nil;
            mangeOrder.FOCOrderNumber=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Custom_Attribute_4"]];
        }

        mangeOrder.Creation_Date=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Last_Update_Date"]];
        mangeOrder.Category=[filteredCategoriesArray objectAtIndex:0];
        [tempProformaOrdersArray addObject:mangeOrder];
    }
    
    NSMutableArray *tempConfirmOrdersArray=[[NSMutableArray alloc]init];

    for (NSDictionary *confirmOrder in confirmOrdersArray) {
        ManageOrderDetails *mangeOrder=[[ManageOrderDetails alloc]init];
        
        /* checking  oproforma order category is exist or not*/
        NSPredicate* orderCategoryPredicate=[NSPredicate predicateWithFormat:@"SELF.Category == [cd] %@ AND SELF.Org_ID == [cd] %@",[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Custom_Attribute_2"]],[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Custom_Attribute_3"]]];
        NSLog(@"template predicate is %@", orderCategoryPredicate);
        NSMutableArray* filteredCategoriesArray=[[categoriesArray filteredArrayUsingPredicate:orderCategoryPredicate] mutableCopy];
        if(filteredCategoriesArray.count!=1) /*if category not exist removing the order*/
        {
            continue;
        }
        mangeOrder.Category=[filteredCategoriesArray objectAtIndex:0];
        mangeOrder.OrderAmount=[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Order_Amt"]];
        mangeOrder.manageOrderStatus=KConfirmOrderStatusString;
        mangeOrder.Orig_Sys_Document_Ref=[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Orig_Sys_Document_Ref"]];
        mangeOrder.Row_ID=[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Row_ID"]];
        mangeOrder.Visit_ID=[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Visit_ID"]];
        
        if([[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Shipping_Instructions"]]isEqualToString:@"F"])
        {
            mangeOrder.FOCParentOrderOrderNumber=[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Custom_Attribute_4"]];
            mangeOrder.FOCOrderNumber=nil;
        }
        else
        {
            mangeOrder.FOCParentOrderOrderNumber=nil;
            mangeOrder.FOCOrderNumber=[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"Custom_Attribute_4"]];
        }

        mangeOrder.Creation_Date=[SWDefaults getValidStringValue:[confirmOrder objectForKey:@"End_Time"]];
        [tempConfirmOrdersArray addObject:mangeOrder];

    }
    
    [manageOrdersArray addObjectsFromArray:tempConfirmOrdersArray];
    [manageOrdersArray addObjectsFromArray:tempProformaOrdersArray];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Creation_Date" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    manageOrdersArray = [[manageOrdersArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    
    NSLog(@"Confirm Orders -- %@ \n proforma Orders -- %@",confirmOrdersArray,proformaOrdersArray);
    currentVisit.visitOptions.manageOrders=manageOrdersArray;
    [self updateOrderCountLabels];
    if(!self.isMovingToParentViewController)
    {
        selectedOrderIndexpath=nil;
        [manageOrdersTableview reloadData];

    }

}
-(void)viewDidAppear:(BOOL)animated
{
    if(isInavlidUnconfirmedOrdersFiltered && self.isMovingToParentViewController)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Invalid proforma orders" andMessage:@"one or more proforma orders have not been loaded due to invalid category/warehouse" withController:self];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showCustomerDetails
{
    customerNameLabel.text=currentVisit.visitOptions.customer.Customer_Name;
    customerCodeLabel.text=currentVisit.visitOptions.customer.Customer_No;
    customerAvailableBalanceLabel.text=[currentVisit.visitOptions.customer.Avail_Bal currencyString];
    
    lblVisitType.layer.cornerRadius = 8.0f;
    [lblVisitType.layer setMasksToBounds:YES];
    lblVisitType.text = NSLocalizedString(currentVisit.visitOptions.customer.Visit_Type, nil);
    
    if ([currentVisit.visitOptions.customer.Cust_Status isEqualToString:@"Y"]) {
        [customerstatusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    }else{
        [customerstatusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
}
#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;

}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44.0)];
        ManageOrdersTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
        [view addSubview:ManageOrdersTableHeaderView];
        return ManageOrdersTableHeaderView;
}   // custom view for header. will be adjusted to default or specified header height



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return manageOrdersArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString* identifier=@"ManageOrderTableCell";
        ManageOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ManageOrdersTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        ManageOrderDetails *manageOrder=[manageOrdersArray objectAtIndex:indexPath.row];
        

        cell.orderamountLabel.text=[manageOrder.OrderAmount currencyString];
        cell.orderNumberLabel.text=manageOrder.Orig_Sys_Document_Ref;
        cell.OrderCreationdateLabel.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:manageOrder.Creation_Date];
        cell.OrderstatusLabel.text=manageOrder.manageOrderStatus;
    
        if(selectedOrderIndexpath!=nil && selectedOrderIndexpath==indexPath)
        {
            /*seclecting the present selected cell*/
            cell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
            
            cell.orderamountLabel.textColor=MedRepMenuTitleSelectedCellTextColor;
            cell.orderNumberLabel.textColor=MedRepMenuTitleSelectedCellTextColor;
            cell.OrderCreationdateLabel.textColor=MedRepMenuTitleSelectedCellTextColor;
            cell.OrderstatusLabel.textColor=MedRepMenuTitleSelectedCellTextColor;
        }
        else
        {
            cell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
            cell.orderamountLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.orderNumberLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.OrderCreationdateLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;
            cell.OrderstatusLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;

        }
    if([manageOrder.manageOrderStatus isEqualToString:KUnConfirmOrderStatusString])
    {
        cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];

    }
    else
    {
        cell.rightButtons = nil;

    }
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
        cell.delegate=self;

        return cell;
        
    
}
-(void)updateTheOrderTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    /* deselecting the previous selected cell*/
    if(selectedOrderIndexpath!=nil)
    {
        ManageOrdersTableViewCell *previousSelectedCell=[manageOrdersTableview cellForRowAtIndexPath:selectedOrderIndexpath];
        previousSelectedCell.contentView.backgroundColor=MedRepUITableviewDeSelectedCellBackgroundColor;
        previousSelectedCell.orderamountLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.orderNumberLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.OrderCreationdateLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        previousSelectedCell.OrderstatusLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;
    }
    
    /*seclecting the present selected cell*/
    ManageOrdersTableViewCell *currentSelectedcell=[manageOrdersTableview cellForRowAtIndexPath:indexpath];
    currentSelectedcell.contentView.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
    currentSelectedcell.orderamountLabel.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.orderNumberLabel.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.OrderCreationdateLabel.textColor=MedRepMenuTitleSelectedCellTextColor;
    currentSelectedcell.OrderstatusLabel.textColor=MedRepMenuTitleSelectedCellTextColor;

    selectedOrderIndexpath=indexpath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self updateTheOrderTableViewSelectedCellIndexpath:indexPath];
    ManageOrderDetails *manageOrder=[manageOrdersArray objectAtIndex:indexPath.row];
    currentVisit.visitOptions.salesOrder=[[SalesOrder alloc]init];
    currentVisit.visitOptions.salesOrder.selectedTemplete=nil;
  
    currentVisit.visitOptions.salesOrder.selectedCategory=manageOrder.Category;
    
    
    if(manageOrder.FOCOrderNumber==nil)/** if foc order*/
    {
        currentVisit.visitOptions.salesOrder.selectedOrderType=kFOCOrderPopOverTitle;
        currentVisit.visitOptions.salesOrder.focParentOrderNumber=manageOrder.FOCParentOrderOrderNumber;
    }
    else
    {
        currentVisit.visitOptions.salesOrder.selectedOrderType=kDefaultOrderPopOverTitle;
        currentVisit.visitOptions.salesOrder.focParentOrderNumber=manageOrder.FOCParentOrderOrderNumber;
    }
    
    currentVisit.visitOptions.salesOrder.isTemplateOrder=NO;
    currentVisit.visitOptions.salesOrder.isManagedOrder=YES;
    currentVisit.visitOptions.salesOrder.confrimationDetails=nil;
    currentVisit.visitOptions.salesOrder.OrderStartTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    currentVisit.visitOptions.salesOrder.SelectedManageOrderDetails=manageOrder;
    currentVisit.visitOptions.salesOrder.SalesOrderLineItemsArray=[[NSMutableArray alloc]init];
    
    SalesWorxSalesOrderViewController* salesOrderVC=[[SalesWorxSalesOrderViewController alloc]init];
    salesOrderVC.currentVisit=currentVisit;
    [self.navigationController pushViewController:salesOrderVC animated:YES];


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (NSInteger)index, fromExpansion ? @"YES" : @"NO");
    
    if (direction == MGSwipeDirectionRightToLeft) {
        
        if(index==0)
        {
            //delete button
            NSIndexPath * path = [manageOrdersTableview indexPathForCell:cell];
            ManageOrderDetails *manageOrder=[manageOrdersArray objectAtIndex:path.row];
            BOOL proformaOrderDeleted=[[SWDatabaseManager retrieveManager] DeleteProformaOrderWithId:manageOrder.Orig_Sys_Document_Ref];
            [manageOrdersArray removeObjectAtIndex:path.row];
            if(proformaOrderDeleted)
            {
                [UIView beginAnimations:@"myAnimationId" context:nil];
                
                [UIView setAnimationDuration:0.5]; // Set duration here
                [CATransaction begin];
                [CATransaction setCompletionBlock:^{
                    NSLog(@"Complete!");
                    [self updateOrderCountLabels];
                }];
                
                [manageOrdersTableview beginUpdates];
                [manageOrdersTableview deleteRowsAtIndexPaths:[[NSArray alloc]initWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
                
                [manageOrdersTableview endUpdates];
                
                [CATransaction commit];
                [UIView commitAnimations];
                [manageOrdersTableview reloadData];
                return NO; //Don't autohide to improve delete expansion animation

            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Unable to delete proforma order" withController:self];
            }
        }
        
    }
    
    return YES;
}
-(void)updateOrderCountLabels
{
    
    NSPredicate *UnconfirmOrdersPredicate=[NSPredicate predicateWithFormat:@"manageOrderStatus=%@",KUnConfirmOrderStatusString];
    NSInteger UnconfrimOrdersCount=[manageOrdersArray filteredArrayUsingPredicate:UnconfirmOrdersPredicate].count;
    
    confirmedOrdersCountLabel.text=[NSString stringWithFormat:@"%d",manageOrdersArray.count-UnconfrimOrdersCount];
    unConfirmedOrdersCountLabel.text=[NSString stringWithFormat:@"%d",UnconfrimOrdersCount];
}
@end
