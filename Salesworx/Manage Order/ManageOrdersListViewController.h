//
//  ManageOrdersListViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxTableViewHeaderView.h"
#import "ManageOrdersTableViewCell.h"
#import "SalesWorxCustomClass.h"
#import "MGSwipeTableCell.h"
#import "SalesWorxSalesOrderViewController.h"
#import "SalesWorxImageView.h"
#import "SalesWorxDescriptionLabel1.h"


@interface ManageOrdersListViewController : UIViewController<MGSwipeTableCellDelegate>
{
    
    
    IBOutlet SalesWorxTableViewHeaderView *ManageOrdersTableHeaderView;

    
    IBOutlet MedRepHeader *customerNameLabel;
    IBOutlet MedRepProductCodeLabel *customerCodeLabel;
    IBOutlet UILabel *customerAvailableBalanceLabel;
    
    IBOutlet MedRepElementDescriptionLabel* confirmedOrdersCountLabel;
    IBOutlet MedRepElementDescriptionLabel* unConfirmedOrdersCountLabel;
    
    IBOutlet SalesWorxImageView *customerstatusImageView;
    IBOutlet UITableView *manageOrdersTableview;

    NSMutableArray* manageOrdersArray;
    
    NSIndexPath *selectedOrderIndexpath;
    NSMutableArray *categoriesArray;
    BOOL isInavlidUnconfirmedOrdersFiltered;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;

}
@property(strong,nonatomic) SalesWorxVisit * currentVisit;

@end
