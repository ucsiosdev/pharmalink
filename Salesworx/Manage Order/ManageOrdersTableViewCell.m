//
//  ManageOrdersTableViewCell.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "ManageOrdersTableViewCell.h"

@implementation ManageOrdersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
