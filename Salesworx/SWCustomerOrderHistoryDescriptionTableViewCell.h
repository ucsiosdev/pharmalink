//
//  SWCustomerOrderHistoryDescriptionTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxSingleLineLabel.h"

@interface SWCustomerOrderHistoryDescriptionTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *dividerImageView;

@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * itemCodeLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * descriptionLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * qtyLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * unitPriceLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * totalLbl;
@property(nonatomic) BOOL isHeader;
@end
