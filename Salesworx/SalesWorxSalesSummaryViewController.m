//
//  SalesWorxSalesSummaryViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSalesSummaryViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxSalesSummaryTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxDateHelper.h"
@interface SalesWorxSalesSummaryViewController ()
{
    SalesWorxDateHelper * dateHelper;
}
@end

@implementation SalesWorxSalesSummaryViewController
@synthesize arrCustomersSalesSummary,currentVisit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    dateHelper=[[SalesWorxDateHelper alloc] init];

    self.view.backgroundColor = UIViewControllerBackGroundColor;
    viewFooter.backgroundColor = MedRepMenuTitleUnSelectedCellTextColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kSalesSummaryTitle localizedCapitalizedString], nil)];
    
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    
    [DateRangeTextField SetCustomDateRangeText];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    lblTotalSalesAmount.font=kSWX_FONT_SEMI_BOLD(20);
    
    
    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
    pieChartPlaceholderImage.hidden = NO;
    parentViewOfPieChart.hidden = YES;
    
    arrCustomerName = [[NSMutableArray alloc]init];
    arrCustomersDetails = [[NSMutableArray alloc]init];
    
    NSMutableArray *customerListArray = [[[SWDatabaseManager retrieveManager] dbGetCollection] mutableCopy];
    for (NSMutableDictionary *currentDict in customerListArray)
    {
        NSMutableDictionary *currentCustDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
        Customer * currentCustomer = [[Customer alloc] init];
        currentCustomer.Address=[currentCustDict valueForKey:@"Address"];
        currentCustomer.Allow_FOC=[currentCustDict valueForKey:@"Allow_FOC"];
        currentCustomer.Avail_Bal= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]];
        currentCustomer.Bill_Credit_Period=[currentCustDict valueForKey:@"Bill_Credit_Period"];
        currentCustomer.Cash_Cust=[currentCustDict valueForKey:@"Cash_Cust"];
        currentCustomer.Chain_Customer_Code=[currentCustDict valueForKey:@"Chain_Customer_Code"];
        currentCustomer.City=[currentCustDict valueForKey:@"City"];
        currentCustomer.Contact=[currentCustDict valueForKey:@"Contact"];
        currentCustomer.Creation_Date=[currentCustDict valueForKey:@"Creation_Date"];
        currentCustomer.Credit_Hold=[currentCustDict valueForKey:@"Credit_Hold"];
        currentCustomer.Credit_Limit= [NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]];
        currentCustomer.Cust_Status=[currentCustDict valueForKey:@"Cust_Status"];
        currentCustomer.Customer_Barcode=[currentCustDict valueForKey:@"Customer_Barcode"];
        currentCustomer.Customer_Class=[currentCustDict valueForKey:@"Customer_Class"];
        currentCustomer.Customer_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]];
        currentCustomer.Customer_Name=[currentCustDict valueForKey:@"Customer_Name"];
        currentCustomer.Customer_No=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]];
        currentCustomer.Customer_OD_Status=[currentCustDict valueForKey:@"Customer_OD_Status"];
        currentCustomer.Customer_Type=[currentCustDict valueForKey:@"Customer_Type"];
        currentCustomer.Location=[currentCustDict valueForKey:@"Location"];
        currentCustomer.Phone=[currentCustDict valueForKey:@"Phone"];
        currentCustomer.Price_List_ID=[currentCustDict valueForKey:@"Price_List_ID"];
        currentCustomer.Site_Use_ID=[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]];
        currentCustomer.Trade_Classification=[currentCustDict valueForKey:@"Trade_Classification"];
        currentCustomer.Ship_Site_Use_ID = [currentCustDict valueForKey:@"Ship_Site_Use_ID"];
        
        [arrCustomersDetails addObject:currentCustomer];
        
        if ([currentCustomer.Customer_Name containsString:currentCustomer.Customer_No]) {
            [arrCustomerName addObject:[currentCustDict valueForKey:@"Customer_Name"]];
        } else {
            [arrCustomerName addObject:[[currentCustDict valueForKey:@"Customer_Name"] stringByAppendingString:[NSString stringWithFormat:@" [%@]",[currentCustDict valueForKey:@"Customer_No"]]]];
        }
    }
    arrCustomerType = [NSMutableArray arrayWithObjects:@"All", @"Cash", @"Credit", nil];
    arrDocumentType = [NSMutableArray arrayWithObjects:@"All", @"Invoice", @"Credit Note", nil];
    
    if (self.isMovingToParentViewController)
    {
        if (currentVisit) {
            txtCustomer.text = [NSString getValidStringValue:currentVisit.visitOptions.customer.Customer_Name];
            txtCustomerType.text = [[NSString getValidStringValue:currentVisit.visitOptions.customer.Cash_Cust] isEqualToString:KAppControlsYESCode]?@"Cash":@"Credit";
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController)
    {
        customerPieChart.backgroundColor = [UIColor clearColor];
        customerPieChart.labelFont = [UIFont boldSystemFontOfSize:14];
        self.slicesForCustomerPotential = [[NSMutableArray alloc]init];
        
        customerPieChart.labelRadius = 25;
        [customerPieChart setDelegate:self];
        [customerPieChart setDataSource:self];
        [customerPieChart setPieCenter:CGPointMake(70, 65)];
        [customerPieChart setShowPercentage:YES];
        [customerPieChart setLabelColor:[UIColor whiteColor]];
        [customerPieChart setUserInteractionEnabled:NO];
        
        self.sliceColorsForCustomerPotential = [NSArray arrayWithObjects:
                                                [UIColor colorWithRed:74.0/255.0 green:145.0/255.0 blue:207.0/255.0 alpha:1],
                                                [UIColor colorWithRed:62.0/255.0 green:193.0/255.0 blue:194.0/255.0 alpha:1],
                                                nil];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            NSMutableArray * customerListArray=[[[SWDatabaseManager retrieveManager]fetchSalesSummaryData] mutableCopy];
            arrCustomersSalesSummary=[[NSMutableArray alloc]init];
            if (customerListArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary * currentDict in customerListArray) {
                    
                    NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    Customer_SalesSummary *currentCustomer = [[Customer_SalesSummary alloc] init];
                    currentCustomer.Orig_Sys_Document_Ref = [currentCustDict valueForKey:@"Orig_Sys_Document_Ref"];
                    currentCustomer.Creation_Date = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:@"yyyy-MM-dd" scrString:[currentCustDict valueForKey:@"Creation_Date"]];
                    currentCustomer.Creation_DateWithTime = [currentCustDict valueForKey:@"Creation_Date"];

                    currentCustomer.Doc_Type = [currentCustDict valueForKey:@"Doc_Type"];
                    currentCustomer.Transaction_Amt = [currentCustDict valueForKey:@"Transaction_Amt"];
                    currentCustomer.Customer_No = [currentCustDict valueForKey:@"Customer_No"];
                    currentCustomer.Customer_Name = [currentCustDict valueForKey:@"Customer_Name"];
                    currentCustomer.Cash_Cust = [currentCustDict valueForKey:@"Cash_Cust"];
                    currentCustomer.Phone = [currentCustDict valueForKey:@"Phone"];
                    currentCustomer.Visit_ID = [currentCustDict valueForKey:@"Visit_ID"];
                    currentCustomer.End_Time = [currentCustDict valueForKey:@"End_Time"];
                    currentCustomer.ERP_Ref_No = [currentCustDict valueForKey:@"ERP_Ref_No"];
                    currentCustomer.Ship_To_Customer_Id = [currentCustDict valueForKey:@"Ship_To_Customer_ID"];
                    currentCustomer.Ship_To_Site_Id = [currentCustDict valueForKey:@"Ship_To_Site_ID"];
                    
                    [arrCustomersSalesSummary addObject:currentCustomer];
                }
            }
            unfilteredCustomersSalesSummaryArray = arrCustomersSalesSummary;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                [MBProgressHUD hideHUDForView:self.view animated:true];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(Creation_Date >= %@) AND (Creation_Date <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]];
                NSMutableArray* filteredArray = [[unfilteredCustomersSalesSummaryArray filteredArrayUsingPredicate:predicate] mutableCopy];
                arrCustomersSalesSummary = filteredArray;
                [tblSalesSummary reloadData];
                
                [self loadPieChart:arrCustomersSalesSummary];
            });
        });
    }
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCustomersSalesSummary.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"salesSummary";
    SalesWorxSalesSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    
    cell.lblDocumentNo.isHeader=YES;
    cell.lblDocumentType.isHeader=YES;
    cell.lblDate.isHeader=YES;
    cell.lblName.isHeader=YES;
    cell.lblAmount.isHeader=YES;
    
    cell.lblDocumentNo.text=NSLocalizedString(@"Document No",nil);
    cell.lblDocumentType.text=NSLocalizedString(@"Document Type",nil);;
    cell.lblDate.text=NSLocalizedString(@"Date",nil);;
    cell.lblName.text=NSLocalizedString(@"Name",nil);;
    cell.lblAmount.text=NSLocalizedString(@"Amount",nil);;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"salesSummary";
    SalesWorxSalesSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    Customer_SalesSummary *data = [arrCustomersSalesSummary objectAtIndex:indexPath.row];

    cell.lblDocumentNo.text = data.ERP_Ref_No;
    if([data.Doc_Type isEqualToString:@"I"])
    {
        cell.lblDocumentType.text = @"Invoice";
    }
    else if([data.Doc_Type isEqualToString:@"R"])
    {
        cell.lblDocumentType.text = @"Credit Note";
    }
    cell.lblDate.text = [NSString stringWithFormat:@"%@", [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:data.Creation_DateWithTime]];
    cell.lblName.text = data.Customer_Name;
    cell.lblAmount.text = [[NSString stringWithFormat:@"%@",data.Transaction_Amt] currencyString];
    
    return cell;
}


#pragma  mark textfield methods
-(BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{
    if(textField == txtCustomer)
    {
        [self textFieldTapped:textField :kCustomerNamePopOverTitle :arrCustomerName :400];
        return NO;
    }
    else if(textField == DateRangeTextField)
    {
        SalesWorxReportsDateSelectorViewController * popOverVC=[[SalesWorxReportsDateSelectorViewController alloc]init];
        popOverVC.delegate=self;
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(450, 570);
        popover.delegate = self;
        popover.sourceView = DateRangeTextField.rightView;
        popover.sourceRect = DateRangeTextField.dropdownImageView.frame;
        popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
        return NO;
    }
    else if(textField == txtCustomerType)
    {
        [self textFieldTapped:textField :kCustomerTypePopOverTitle :arrCustomerType :266];
        return NO;
    }
    else if(textField == txtDocumentType)
    {
        [self textFieldTapped:textField :kDocumentTypePopOverTitle :arrDocumentType :266];
        return NO;
    }
    return YES;
}

#pragma  mark textfield tapped method

-(void)textFieldTapped:(MedRepTextField *)txtField :(NSString *)strPop :(NSMutableArray *)arr :(CGFloat )width
{
    txtCustomer.enabled = YES;
    popString = strPop;

    SalesWorxPopOverViewController *popOverVC = [[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = arr;
    popOverVC.salesWorxPopOverControllerDelegate = self;
    popOverVC.disableSearch = YES;
    
    if([popString isEqualToString:kCustomerNamePopOverTitle])
    {
        popOverVC.disableSearch = NO;
    }
    if([popString isEqualToString:kDocumentTypePopOverTitle]||[popString isEqualToString:kCustomerTypePopOverTitle])
    {
        popOverVC.enableArabicTranslation = YES;
    }
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor = [UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(width, 273);
    popover.delegate = self;
    popOverVC.titleKey = popString;
    popover.sourceView = txtField.rightView;
    popover.sourceRect = txtField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    
    [self presentViewController:nav animated:YES completion:^{
    }];
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popString isEqualToString:kCustomerNamePopOverTitle])
    {
        txtCustomer.text = [NSString stringWithFormat:@"%@",[arrCustomerName objectAtIndex:selectedIndexPath.row]];
    }
    else if ([popString isEqualToString:kCustomerTypePopOverTitle])
    {
        txtCustomerType.text = [NSString stringWithFormat:@"%@",[arrCustomerType objectAtIndex:selectedIndexPath.row]];
    }
    else if ([popString isEqualToString:kDocumentTypePopOverTitle])
    {
        txtDocumentType.text = [NSString stringWithFormat:@"%@",[arrDocumentType objectAtIndex:selectedIndexPath.row]];
    }
}

#pragma mark Pie chart delegate methods

-(void)loadPieChart:(NSArray *)arr
{
    if ([arr count] == 0) {
        pieChartPlaceholderImage.hidden = NO;
        parentViewOfPieChart.hidden = YES;
    } else {
        pieChartPlaceholderImage.hidden = YES;
        parentViewOfPieChart.hidden = NO;     }
    [_slicesForCustomerPotential removeAllObjects];
    [customerPieChart reloadData];
    [tblSalesSummary reloadData];
    
    int invoice = 0, creditNote = 0;
    double salesAmount = 0.0f;
    for (Customer_SalesSummary *currentCustomer in arr) {
        salesAmount = salesAmount + [currentCustomer.Transaction_Amt doubleValue];
        if ([currentCustomer.Doc_Type isEqualToString:@"I"]){
            invoice++;
        }
        else
        {
            creditNote++;
        }
    }
    lblTotalSalesAmount.text = [[NSString stringWithFormat:@"%f",salesAmount] currencyString];
    
    [_slicesForCustomerPotential removeAllObjects];
    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:creditNote]];
    [_slicesForCustomerPotential addObject:[NSNumber numberWithInt:invoice]];
    [customerPieChart reloadData];
    
    lblTotalInvoices.text = [NSString stringWithFormat:@"%d",invoice];
    lblTotalCreditNotes.text = [NSString stringWithFormat:@"%d",creditNote];
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    if (pieChart == customerPieChart && self.slicesForCustomerPotential.count>0) {
        count = self.slicesForCustomerPotential.count;
    }
    return count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    if (pieChart == customerPieChart) {
        count = [[NSString stringWithFormat:@"%@",[self.slicesForCustomerPotential objectAtIndex:index]]floatValue];
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == customerPieChart) {
        colour = [self.sliceColorsForCustomerPotential objectAtIndex:(index % self.sliceColorsForCustomerPotential.count)];
    }
    return colour;
}

#pragma  mark button tapped method

- (IBAction)btnSearch:(id)sender
{
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    if (![txtCustomer.text isEqualToString:@"N/A"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF.Customer_Name",txtCustomer.text];
        NSMutableArray *filteredArray = [[arrCustomersDetails filteredArrayUsingPredicate:predicate] mutableCopy];
        
        if ([filteredArray count] > 0) {
            Customer *selectdCustomer = [filteredArray objectAtIndex:0];
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"Ship_To_Customer_Id == %d and Ship_To_Site_Id == %d",[selectdCustomer.Customer_ID intValue], [selectdCustomer.Ship_Site_Use_ID intValue]]];
        }
    }

    [predicateArray addObject:[NSPredicate predicateWithFormat:@"(Creation_Date >= %@) AND (Creation_Date <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]]];

    if (![txtCustomerType.text isEqualToString:@"N/A"])
    {
        if ([txtCustomerType.text isEqualToString:@"All"]){
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Cash_Cust ==[cd] 'Y' or SELF.Cash_Cust ==[cd] 'N'"]];
        }
        else if ([txtCustomerType.text isEqualToString:@"Cash"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Cash_Cust ==[cd] 'Y'"]];
        }
        else{
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Cash_Cust ==[cd] 'N'"]];
        }
    }
    if (![txtDocumentType.text isEqualToString:@"N/A"])
    {
        if ([txtDocumentType.text isEqualToString:@"All"]){
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'I' or SELF.Doc_Type ==[cd] 'R'"]];
        }
        else if ([txtDocumentType.text isEqualToString:@"Invoice"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'I'"]];
        }
        else{
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] 'R'"]];
        }
    }
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [predicate description]);
    
    NSMutableArray* filteredArray = [[unfilteredCustomersSalesSummaryArray filteredArrayUsingPredicate:predicate] mutableCopy];

    if (filteredArray.count == 0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
    }
    arrCustomersSalesSummary = filteredArray;
    [self loadPieChart:arrCustomersSalesSummary];
}
- (IBAction)btnReset:(id)sender
{
    txtCustomer.text = KNotApplicable;
    txtCustomerType.text = KNotApplicable;
    txtDocumentType.text = KNotApplicable;
    
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    DateRangeTextField.SelecetdCustomDateRangeText = nil;

    [DateRangeTextField SetCustomDateRangeText];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(Creation_Date >= %@) AND (Creation_Date <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]];
    NSMutableArray* filteredArray = [[unfilteredCustomersSalesSummaryArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    arrCustomersSalesSummary = filteredArray;
    [self loadPieChart:arrCustomersSalesSummary];
}
#pragma mark date picker method

-(void)DidUserSelectDateRange:(NSDate *)StartDate EndDate:(NSDate *)EndDate AndCustomRangePeriodText:(NSString *)customPeriodText{

    DateRangeTextField.StartDate = StartDate;
    DateRangeTextField.EndDate = EndDate;
    DateRangeTextField.SelecetdCustomDateRangeText = customPeriodText;
    
    [DateRangeTextField SetCustomDateRangeText];
    
    NSLog(@"StartDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate]);
    NSLog(@"EndDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
