//
//  CustomerTargetView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "SWPlatform.h"
#import "PCPieChart.h"

#import <QuartzCore/QuartzCore.h>




@interface CustomerTargetView : UIView <UITableViewDelegate, UITableViewDataSource,UIPopoverControllerDelegate >
{
    CustomerHeaderView *customerHeaderView;

    UITableView *targetTableView;
    NSDictionary *customer;
    NSMutableArray *targets;
    NSMutableDictionary *customerPie;
    UITableView *categoryTableView;
    UIViewController *popOverContent;
    UIView *backGroundView;
    PCPieChart *pieChartNew;
    NSMutableArray *components;
    float achievmentValue ;
    NSArray* staticLblData;
    
    
    NSIndexPath * selectedIndexPath;

    NSArray * appCtrlFlag;


}
@property (nonatomic, strong) CustomerHeaderView *customerHeaderView;

@property (nonatomic, strong) PCPieChart *pieChartNew;
@property (nonatomic, strong) UIView *backGroundView,*saperatorView;
@property (nonatomic, strong) UITableView *targetTableView;
@property (nonatomic, strong) UITableView *categoryTableView;
@property (nonatomic, strong) NSDictionary *customer;
@property (nonatomic, strong) NSMutableDictionary *customerPie;
@property (nonatomic, strong) NSMutableArray *targets;
@property(nonatomic, strong) NSMutableArray *slices;
@property(nonatomic, strong) NSArray        *sliceColors;

@property(strong,nonatomic)NSMutableArray* descriptionLblsArray,*detailedDescArray;

@property(strong,nonatomic)NSMutableDictionary *detailedDesc;

- (id)initWithCustomer:(NSDictionary *)row;
- (void)loadTarget;
- (void)loadPieChartWithAchievement:(NSDictionary *)pie;

@end