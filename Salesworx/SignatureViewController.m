//
//  SignatureViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/26/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "SignatureViewController.h"
#import "OrderAdditionalInfoViewController.h"
#import "MJPopupBackgroundView.h"
#import "UIViewController+MJPopupViewController.h"

#define kImagesFolder @"Signature"


@interface SignatureViewController ()

@end

@implementation SignatureViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
     pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(16, 40, 958, 113)];
    
    [self.view addSubview:pjrSignView];
    
    
    
   

    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeButtonTapped:(id)sender {
    
    
//    self.glkView=nil;
//    
//    [self.glkView erase];
//    
//    [self.view removeFromSuperview];
    
    
   // [self.navigationController popViewControllerAnimated:YES];
    
//    OrderAdditionalInfoViewController * orderInfoVC=[[OrderAdditionalInfoViewController alloc]init];
//    
//    [orderInfoVC closeButtonTapped];
    
   // [(id)self.view.superview closeButtonTapped];
    
//    NSLog(@"check navigation stack %@", [self.navigationController.viewControllers description]);
//    
    //[self dismissViewControllerAnimated:YES completion:nil];
//    
    
    [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
//
//    self.delegate=nil;
    

    
}




- (NSString*) dbGetImagesDocumentPath
{
    
    //     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}


- (IBAction)saveButtonTapped:(id)sender {
    
    
    NSLog(@"save button tapped in signature");
    
    //if signature already captured display the signature
    
    
    //pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(0, 55, 827, 211)];

    
    
    
    
    
    UIImage* signImage=[pjrSignView getSignatureImage];
    NSData *imageData = UIImagePNGRepresentation(signImage);
//    [self.delegate.form setObject:imageData forKey:@"signature"];
//    isSignature = YES;
//
//        OrderAdditionalInfoViewController * orderInfoVC=[[OrderAdditionalInfoViewController alloc]init];
//    orderInfoVC.isSignature=YES;
//    [orderInfoVC.form setObject:imageData forKey:@"signature"];
    
    
    
    
    [SWDefaults setSignature:imageData];
    
    
     [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    
    
}

- (IBAction)eraseButtonTapped:(id)sender {
    
    signImageView.image=[UIImage imageNamed:@""];

    [signImageView removeFromSuperview];
    
    
    [SWDefaults clearSignature];

    
    [pjrSignView clearSignature];
}
@end
