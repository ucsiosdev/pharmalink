//
//  SWCustomersFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepTextField.h"
#import "SalesWorxDefaultSegmentedControl.h"


@protocol SWFilteredCustomersDelegate <NSObject>

-(void)filteredCustomers:(NSMutableArray*)filteredArray;
-(void)customerFilterDidClose;
-(void)customerFilterdidReset;
-(void)filteredCustomerParameters:(NSMutableDictionary*)parametersDict;
@end

@interface SWCustomersFilterViewController : UIViewController

{
    id delegate;
    IBOutlet MedRepTextField *customerNameTxtFld;
    
    IBOutlet MedRepTextField *customerCodeTextFld;
    NSString* selectedTextField;
    
    IBOutlet MedRepTextField *customerCityTxtFld;
    
    IBOutlet SalesWorxDefaultSegmentedControl *customerStatusSegment;
    
    IBOutlet SalesWorxDefaultSegmentedControl *customerTypeSegment;
    
    NSMutableDictionary * filterParametersDict;
    
    NSString* selectedPredicateString;
    
    NSMutableArray * filteredCustomersArray;

}
@property(strong,nonatomic)NSMutableArray * customersArray;
@property(strong,nonatomic)NSMutableDictionary * previousFilterParametersDict;
@property(nonatomic) id delegate;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;
@property(strong,nonatomic)UINavigationController * filterNavController;
@property(strong,nonatomic) NSString* filterTitle;
- (IBAction)searchButtontapped:(id)sender;

- (IBAction)customerTypeSegmentTapped:(UISegmentedControl *)sender;
- (IBAction)customerStatusSegmentTapped:(UISegmentedControl *)sender;

- (IBAction)resetButtontapped:(id)sender;


@end
