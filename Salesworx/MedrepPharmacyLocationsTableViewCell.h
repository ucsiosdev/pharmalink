//
//  MedrepPharmacyLocationsTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementTitleLabel.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepDefaults.h"

@interface MedrepPharmacyLocationsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *descLbl;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *dividerImgView;

-(void)setSelectedCellStatus;
-(void)setDeSelectedCellStatus;


@end
