//
//  MedRepVisitOptionsCollectionViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDescriptionLabel5_SemiBold.h"
#import "SalesWorxImageView.h"

@interface MedRepVisitOptionsCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *optionImageView;
@property (strong, nonatomic) IBOutlet UIView *layerView;
@property (strong, nonatomic) IBOutlet SalesWorxDescriptionLabel5_SemiBold *optionTitleLbl;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *optionCompletionImageView;

@end

