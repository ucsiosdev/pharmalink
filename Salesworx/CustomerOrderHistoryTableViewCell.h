//
//  CustomerOrderHistoryTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/29/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerOrderHistoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *orderNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderDateLbl;

@property (strong, nonatomic) IBOutlet UILabel *statusLbl;
@end
