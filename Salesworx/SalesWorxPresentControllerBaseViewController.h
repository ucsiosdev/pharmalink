//
//  SalesWorxPresentControllerBaseViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxPresentControllerBaseViewController : UIViewController
{
    IBOutlet UIView *topContentView;
}
-(void)dimissViewControllerWithSlideDownAnimation:(BOOL)Animated;
-(void)AnimationViewDidAppear;
-(void)AnimationViewDidDisAppear;

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType;

@end
