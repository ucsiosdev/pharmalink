//
//  SalesWorxFeedbackViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SurveyQuestionDetails.h"
#import "SurveyResponseDetails.h"
#import "SurveyPageControlViewControl.h"
#import "DataSyncManager.h"
#import "FMDB/FMDBHelper.h"

#import "FMDBHelper.h"
#import "MedRepTextView.h"

#import "TGPCamelLabels7.h"
#import "TGPDiscreteSlider7.h"

@interface SalesWorxFeedbackViewController : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITextViewDelegate,UIAlertViewDelegate,UITextFieldDelegate>
{
    DataSyncManager *appDelegate;
    SurveyQuestionDetails * surveyQuestionDetails;
    SurveyResponseDetails *info;
    
    int viewX;
    int viewY;
    int viewWidth;
    int saveSurveyID;
    UIAlertView* saveSuccess;
    UIAlertView* saveError;
    BOOL surveyIncomplete;
    
    NSMutableArray * textAreaQuestionAnswerArray;
    NSMutableArray * checkBoxQuestionAnswerArray;
    NSMutableArray * radioQuestionAnswerArray;
    NSMutableArray * sliderQuestionAnswerArray;
    
    NSMutableArray * radioQuestionAnswerValidationArray;
    NSMutableArray * sliderQuestionAnswerValidationArray;
    NSMutableArray * checkBoxQuestionAnswerValidationArray;
    NSMutableArray * textAreaQuestionAnswerValidationArray;
    NSMutableArray * textAreaAnswerDescriptionArray;
    NSInteger questionsCount;
    NSInteger answersCount;
    
//    NMRangeSlider* rangeSlider;
//    UITextField *txtForm;
//    UITextField *txtTo;
    
    UIButton *btnStatusForQ;
    int totalQuestions;

    NSMutableArray * walkinSurveyResponses;
    

    TGPCamelLabels7 *camelLabelsView;
    TGPDiscreteSlider7 *descreteSliderView;
}

- (IBAction)dismissButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

//@property (strong, nonatomic) IBOutlet UILabel *questionLbl;
@property (nonatomic, strong) NSArray *surveyQuestionArray,*surveyResponseArray;
@property(strong,nonatomic)NSMutableArray *modifiedSurveyQuestionsArray;
@property(strong,nonatomic) NSString *customerID;

@property(nonatomic,assign)int survetIDNew,qID;

//@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;



-(void)Save;
- (void)buttonClickedForRadio:(UIButton*)button;

-(void)CreateQuestionsView;


@property(nonatomic,strong)NSString* surveyParentLocation;
@property(nonatomic) BOOL isBrandAmbassadorSurvey;
@property(nonatomic,strong) SalesWorxBrandAmbassadorVisit * brandAmbassadorVisit;

-(void)ModifyQuestionResponse;

@end
