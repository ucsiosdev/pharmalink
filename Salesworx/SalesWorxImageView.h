//
//  SalesWorxImageView.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxImageView : UIImageView
@property (nonatomic) IBInspectable BOOL RTLSupport;

@end
