//
//  MapViewController.h
//  Miller
//
//  Created by kadir pekel on 2/7/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Place2.h"
#import "CSMapAnnotation2.h"

@interface MapView2 : UIView <MKMapViewDelegate> {

	MKMapView* mapView;
	UIImageView* routeView;
	NSArray* routes;
	UIColor* lineColor;
    NSMutableArray *RouteArray;
    MKCoordinateRegion region;
    MKCoordinateSpan span;
}

@property (nonatomic, strong) UIColor* lineColor;
@property (nonatomic, strong) MKMapView* mapView;
@property (nonatomic, assign) MKCoordinateRegion region;
@property (nonatomic, assign) MKCoordinateSpan span;
//- (void) showRouteFrom: (Place*) f to:(Place*) t;
-(void)RemoveIT;
@end
