//
//  SalesWorxItemDividerLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxItemDividerLabel.h"

@implementation SalesWorxItemDividerLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    
    [super awakeFromNib];
    
    self.backgroundColor=kUIElementDividerBackgroundColor;
}

@end
