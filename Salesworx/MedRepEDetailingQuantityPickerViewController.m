//
//  MedRepEDetailingQuantityPickerViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/24/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepEDetailingQuantityPickerViewController.h"
#import "MedRepDefaults.h"

@interface MedRepEDetailingQuantityPickerViewController ()

@end

@implementation MedRepEDetailingQuantityPickerViewController
@synthesize qtyArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (qtyArray.count>0) {
        
        return qtyArray.count;
    }
    
    else
    {
        return 0;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.quantityPickerDelegate respondsToSelector:@selector(selectedQuantity:)]) {
        
        [self.quantityPickerDelegate selectedQuantity:indexPath];
        [self.qtyPopoverController dismissPopoverAnimated:YES];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.font=MedRepTitleFont;
    
    cell.textLabel.textColor=MedRepTableViewCellTitleColor;
    
    cell.textLabel.text= [NSString stringWithFormat:@"%@",[qtyArray objectAtIndex:indexPath.row]];
    
    
    return cell;

}

@end
