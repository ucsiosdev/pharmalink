//
//  SalesWorxItemDividerLabel.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"SWDefaults.h"
#import "MedRepDefaults.h"

@interface SalesWorxItemDividerLabel : UILabel

@end
