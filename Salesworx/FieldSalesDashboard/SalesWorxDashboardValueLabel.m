//
//  SalesWorxDashboardValueLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDashboardValueLabel.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"

@implementation SalesWorxDashboardValueLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    
    [super awakeFromNib];
    self.font=FieldSalesDashboardValueLabelFont;
    self.textColor=FieldSalesDashboardValueLabelFontColor;
    //self.textColor=[UIColor redColor];
    
}

@end
