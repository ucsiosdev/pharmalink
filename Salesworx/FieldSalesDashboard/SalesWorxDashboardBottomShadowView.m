//
//  SalesWorxDashboardBottomShadowView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDashboardBottomShadowView.h"
#import <QuartzCore/QuartzCore.h>

@implementation SalesWorxDashboardBottomShadowView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(2, 2);
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowRadius = 1.0;
    self.layer.shadowOffset = CGSizeMake(0, 3);

   
}


@end
