//
//  SalesWorxODOMeterViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/16/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextView.h"
#import "SalesWorxDropShadowView.h"
#import "MedRepQueries.h"

@protocol SalesWorxODOMeterDelegate <NSObject>

-(void)StartVisitAfterPopUp;

@end

@interface SalesWorxODOMeterViewController : UIViewController
{
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *saveButton;
    
    BOOL status;
}
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *contentView;

@property (weak, nonatomic) IBOutlet UITextField *previousReadingTextField;
@property (weak, nonatomic) IBOutlet UITextField *currentReadingTextField;

- (IBAction)SaveButtontapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@property(nonatomic) id ODOMeterDelegate;

@end
