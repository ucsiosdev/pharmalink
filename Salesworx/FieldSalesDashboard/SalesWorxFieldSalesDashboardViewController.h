//
//  SalesWorxFieldSalesDashboardViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/25/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKLineGraph.h"
#import "UIColor+GraphKit.h"
#import "SWDatabaseManager.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxPopOverViewController.h"
#import "SWCustomersViewController.h"
#import "SalesWorxDashboardValueLabel.h"
#import "GIBadgeView.h"
#import "SalesWorxRouteViewController.h"
#import "MedRepPanelTitle.h"
#import "SalesWorxToDoPopUpViewController.h"
#import "SalesWorxDefaultButton.h"
#import "SWRouteViewController.h"
#import "AppControl.h"
@interface SalesWorxFieldSalesDashboardViewController : UIViewController<GKLineGraphDataSource,UIPopoverControllerDelegate,UITextFieldDelegate,SalesWorxPopoverControllerDelegate, XYPieChartDataSource,XYPieChartDelegate>
{
    NSMutableArray * salesTrendArray;
    NSMutableArray * salesDataArray;
    NSMutableArray * returnsDataArray;
    NSMutableArray * collectionsDataArray;

    IBOutlet SalesWorxDefaultButton *routeButton;
    
    IBOutlet UIImageView *syncStatusImageView;
    BOOL isEmptyGraph;
    
    NSMutableArray * tempSalesArray;
    NSMutableArray* tempReturnsArray;
    NSMutableArray * customersArray;
    
    UIBarButtonItem * toDoListButton;
    
    IBOutlet MedRepPanelTitle *monthLabel;
    UIPopoverController * dashboardPopoverController;
    IBOutlet MedRepTextField *customersTextField;
    IBOutlet SalesWorxDashboardValueLabel *totalOutStandingLabel;
    IBOutlet SalesWorxDashboardValueLabel *availableBalanceLabel;
    Customer * selectedCustomer;
    IBOutlet UILabel *plannedVisitsCountLbl;
    IBOutlet UILabel *completedVisitsCountLbl;
    IBOutlet UILabel *outofRouteCountLbl;
    IBOutlet UIView *plannedVisitsView;
    IBOutlet UIView *completedVisitsView;
    IBOutlet UIView *orderView;
    IBOutlet UIView *outofRouteView;
    IBOutlet UIView *returnView;
    IBOutlet UIView *collectionView;
    
    IBOutlet UILabel *dailyorderCountLbl;
    IBOutlet UILabel *dailyOrderValueLbl;
    
    IBOutlet UILabel *dailyReturnValueLbl;
    IBOutlet UILabel *dailyReturnCountLbl;
    
    IBOutlet UILabel *totalReturnsLabel;
    IBOutlet UILabel *dailyCollectionCountLbl;
    
    IBOutlet UILabel *dailyCollectionValueLbl;
    
    IBOutlet UILabel *monthlyCollectionsLabel;
    IBOutlet UILabel *monthlySalesLabel;
    
    IBOutlet SalesWorxDefaultButton *messagesButton;
    IBOutlet UILabel *lastSynchronizationLbl;
    IBOutlet UIView *viewTargetInformation;
    IBOutlet NSLayoutConstraint *widthConstraintOfTargetInfoView;
    IBOutlet NSLayoutConstraint *trailingOfSalesView;
    IBOutlet UIImageView *noDataImage;
    IBOutlet UIView *pieParentView;
    
    UIBarButtonItem *odoMeterButton;
    AppControl *appControl;
    
    
    IBOutlet HMSegmentedControl *targetInformationSegment;
    IBOutlet MedRepPanelTitle *lblTargetInformation;
    IBOutlet SalesWorxItemDividerLabel *dividerImageofTargetInformation;
    
    NSMutableArray *collectionTargetArrayForSelectedCustomer;
}
- (IBAction)routeButtonTapped:(id)sender;
- (IBAction)startVisitTapped:(id)sender;
@property (strong, nonatomic) IBOutlet GKLineGraph *gkLineGraph;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *labels;
- (IBAction)messageTapped:(id)sender;

@property (weak, nonatomic) IBOutlet XYPieChart *TargetPieChart;
@property(nonatomic, strong) NSMutableArray *slicesForCustomerPotential;
@property(nonatomic, strong) NSArray        *sliceColorsForCustomerPotential;

@end
