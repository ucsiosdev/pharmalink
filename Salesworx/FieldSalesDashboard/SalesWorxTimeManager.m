//
//  SalesWorxTimeManager.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxTimeManager.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"

@implementation SalesWorxTimeManager

-(void)startApplicationTimer
{
    lastTimerTriggeredDate=[NSDate date];
    applicationStartDate = [NSDate date];
    startTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(handleApplicationStartTimer:) userInfo:nil repeats:YES];

    
}
- (void)handleApplicationStartTimer:(NSTimer *)theTimer {

    // if difference between lastTimerTriggeredDate and current date more than one minute , system time changed
    
    
    NSDate * currentDeviceDate=[NSDate date];
    NSTimeInterval distanceBetweenDates = [currentDeviceDate timeIntervalSinceDate:lastTimerTriggeredDate];
    NSLog(@"distance between dates %f",distanceBetweenDates);
    
    if (distanceBetweenDates > 65 || distanceBetweenDates < 55) {
        
        NSLog(@"fire time change alert");
        
        SWAppDelegate * appDel =(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
        
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"Device Time Changed" andMessage:@"Please reset the device time to current time" withController:appDel.window.visibleViewController];
        
        
        
    }
    
    currentTimerCount= currentTimerCount+1;
    lastTimerTriggeredDate=[NSDate date];
    
    
    
}

-(void)fetchApplicationCalculatedCurrentTime
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    calendar.timeZone=[NSTimeZone systemTimeZone];
    
    NSDateComponents *comps = [NSDateComponents new];
    comps.minute = currentTimerCount;
    NSDate *currentDeviceCalculatedDate = [calendar dateByAddingComponents:comps toDate:applicationStartDate options:0];
    
    NSLog(@"device calculated date is %@", currentDeviceCalculatedDate);
    
    
    
}


@end
