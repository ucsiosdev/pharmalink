//
//  SalesWorxTimeManager.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesWorxTimeManager : NSObject
{
    NSDate * applicationStartDate;
    
    NSTimer *startTimer;
    
    NSInteger currentTimerCount;
    
    
    NSDate * lastTimerTriggeredDate;
}


-(void)startApplicationTimer;

@end
