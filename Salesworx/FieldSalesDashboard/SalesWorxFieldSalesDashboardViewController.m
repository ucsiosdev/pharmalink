//
//  SalesWorxFieldSalesDashboardViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/25/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxFieldSalesDashboardViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import <QuartzCore/QuartzCore.h>
#import "SalesWorxODOMeterViewController.h"
#import "MedRepQueries.h"
@interface SalesWorxFieldSalesDashboardViewController ()

@end

@implementation SalesWorxFieldSalesDashboardViewController
@synthesize TargetPieChart;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closeSplitView) name:@"COACHSURVEYDIDCOMPLETENOTIFICATION" object:self];
    
//        NSMutableArray* sampleArray = [[NSMutableArray alloc]init];
//        Customer* testCustomer = [[Customer alloc]init];
//       testCustomer.Customer_Name = @"EMIRATES EUROPEAN HOSPITAL L.L.C.-SHARJAH (AL QULA'A,SHARQ STREET,NEAR SHARJAH CO-O)";
//     //   testCustomer.Customer_Name = @"EMIRATES EUROPEAN HOSPITAL L.L.C.-SHARJAH";
//
//        [sampleArray addObject:testCustomer];
//        Customer* selectedCustomer = sampleArray[0];
//        NSArray *filtered = [sampleArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_Name == %@",[SWDefaults getValidStringValue:selectedCustomer.Customer_Name]]]];
//        NSLog(@"filtered array in test crash is %@", filtered);

    NSLog(@"dashboard did load called");
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    appControl=[AppControl retrieveSingleton];
    //[self setupButtons];
    
//    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:plannedVisitsView.bounds];
//    plannedVisitsView.layer.masksToBounds = NO;
//    plannedVisitsView.layer.shadowColor = [UIColor colorWithRed:(19.0/255.0) green:(180.0/255.0) blue:(156.0/255.0) alpha:1].CGColor;
//    plannedVisitsView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
//    plannedVisitsView.layer.shadowOpacity = 0.5f;
//    plannedVisitsView.layer.shadowRadius = 1.0;
//    plannedVisitsView.layer.shadowPath = shadowPath.CGPath;
//    
//    
//    
//    UIBezierPath *completedVisitShadowPath = [UIBezierPath bezierPathWithRect:completedVisitsView.bounds];
//    completedVisitsView.layer.masksToBounds = NO;
//    completedVisitsView.layer.shadowColor = [UIColor colorWithRed:(59.0/255.0) green:(193.0/255.0) blue:(126.0/255.0) alpha:1].CGColor;
//    completedVisitsView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
//    completedVisitsView.layer.shadowOpacity = 0.5f;
//    completedVisitsView.layer.shadowRadius = 1.0;
//    completedVisitsView.layer.shadowPath = completedVisitShadowPath.CGPath;
//
//    
//    UIBezierPath *outodRouteVisitShadowPath = [UIBezierPath bezierPathWithRect:outofRouteView.bounds];
//    outofRouteView.layer.masksToBounds = NO;
//    outofRouteView.layer.shadowColor = [UIColor colorWithRed:(73.0/255.0) green:(135.0/255.0) blue:(190.0/255.0) alpha:1].CGColor;
//    outofRouteView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
//    outofRouteView.layer.shadowOpacity = 0.5f;
//    outofRouteView.layer.shadowRadius = 1.0;
//    outofRouteView.layer.shadowPath = outodRouteVisitShadowPath.CGPath;
//    
    
    
    orderView.layer.borderWidth=1.0;
    orderView.layer.borderColor=[[UIColor colorWithRed:(249/255.0) green:(183/255.0) blue:(60/255.0) alpha:1] CGColor];
    
    
    returnView.layer.borderWidth=1.0;
    returnView.layer.borderColor=[[UIColor colorWithRed:(255.0/255.0) green:(91.0/255.0) blue:(91.0/255.0) alpha:1] CGColor];
    

    
    collectionView.layer.borderWidth=1.0;
    collectionView.layer.borderColor=[[UIColor colorWithRed:(43.0/255.0) green:(155.0/255.0) blue:(216.0/255.0) alpha:1] CGColor];
    
    
    
    
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:@"AppDidBecomeActive" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(loadDashboardData)
                                                name:@"UPDATEDASHBOARDUINOTIFICATIONNAME"
                                              object:nil];

    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Field Sales Dashboard"];

    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleFingerTap.numberOfTapsRequired=1;
    [plannedVisitsView addGestureRecognizer:singleFingerTap];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    SalesWorxRouteViewController *routeVC = [[SalesWorxRouteViewController alloc] init];
//    SWRouteViewController *routeVC = [[SWRouteViewController alloc] init];
    [self.navigationController pushViewController:routeVC animated:YES];
}

-(void)appDidBecomeActive
{
    NSLog(@"become active called");
    
    self.gkLineGraph.layer.sublayers=nil;
    
    [self loadDashboardData];
    
}
-(void)loadDashboardData
{
    [self.gkLineGraph reset];
    customersArray=[[NSMutableArray alloc]init];
    customersArray=[[SWDatabaseManager retrieveManager]fetchCustomerdataforPlannedVisits];
    salesDataArray=[[SWDatabaseManager retrieveManager] fetchSalesTrendWithDocType:kSalesOrderDocType];
    collectionsDataArray = [[SWDatabaseManager retrieveManager] fetchSalesTrendWithDocType:kCollectionDocType];
    
    
    if (customersArray.count>0) {
        customersTextField.isReadOnly=NO;
        routeButton.hidden=NO;

    }
    else{
        customersTextField.text=NSLocalizedString(@"No planned visits today", nil);
        customersTextField.isReadOnly=YES;
        routeButton.hidden=YES;
    
    }
    
    returnsDataArray=[[SWDatabaseManager retrieveManager] fetchSalesTrendWithDocType:kReturnsDocType];
    
    //NSLog(@"sales data array is %@", salesDataArray);

    //NSLog(@"returns data array is %@", returnsDataArray);

    if (salesDataArray.count==0 && returnsDataArray.count==0 && collectionsDataArray.count == 0) {
        isEmptyGraph=YES;

        [self displayEmptyDataGraph];
    }
    else{
       
    
    //adding missing date data
    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[salesDataArray valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [salesDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[returnsDataArray valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [returnsDataArray insertObject:dummyDict atIndex:i];
        }
    }
        
        for (NSInteger i=0; i<tempDates.count; i++) {
            NSString* currentString=[tempDates objectAtIndex:i];
            if ([[collectionsDataArray valueForKey:@"DATE(Collected_On)"] containsObject:currentString]) {
                
            }
            else{
                NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
                NSString *output = @"0.00";
                NSString *newOutput = [NSString stringWithFormat:@"%@", output];
                [dummyDict setObject:newOutput forKey:@"Ammount"];
                [dummyDict setObject:currentString forKey:@"DATE(Collected_On)"];
                [dummyDict setObject:@1 forKey:@"Total"];
                [collectionsDataArray insertObject:dummyDict atIndex:i];
            }
        }
    
    
    //NSLog(@"sales data %@" ,salesDataArray);
    
    //NSLog(@"returns data %@" ,returnsDataArray);
    
        isEmptyGraph=NO;
        
        [self testDataGraph];
        
    }
    if (customersArray.count>0) {
                   [self populateSelectedCustomerData:[customersArray objectAtIndex:0]];

//        if (self.isMovingToParentViewController) {
//            [self populateSelectedCustomerData:[customersArray objectAtIndex:0]];
//        }
    }
    else{
        totalOutStandingLabel.text=@"N/A";
        availableBalanceLabel.text=@"N/A";
        
    }
    [self updateVisitStatistics];
}

-(void)displayEmptyDataGraph
{
    
    
    NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
    currentMonthFormatter.dateFormat=@"MMMM";
    NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
    NSLog(@"current month is %@", currentMonth);
    monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];

    
    NSMutableArray * tempDates=[self fetchDatesofCurrentMonthinDBFormat];
    
    NSMutableArray * testSalesDataArray=[[NSMutableArray alloc]init];

    NSMutableArray * testReturnsDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * testCollectionDataArray=[[NSMutableArray alloc]init];

    NSMutableArray * emptyGraphDataArray=[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[salesDataArray valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testSalesDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[returnsDataArray valueForKey:@"DATE(Creation_Date)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Creation_Date)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testReturnsDataArray insertObject:dummyDict atIndex:i];
        }
    }
    
    for (NSInteger i=0; i<tempDates.count; i++) {
        NSString* currentString=[tempDates objectAtIndex:i];
        if ([[collectionsDataArray valueForKey:@"DATE(Collected_On)"] containsObject:currentString]) {
            
        }
        else{
            NSMutableDictionary * dummyDict=[[NSMutableDictionary alloc]init];
            NSString *output = @"0.00";
            NSString *newOutput = [NSString stringWithFormat:@"%@", output];
            [dummyDict setObject:newOutput forKey:@"Ammount"];
            [dummyDict setObject:currentString forKey:@"DATE(Collected_On)"];
            [dummyDict setObject:@1 forKey:@"Total"];
            [testCollectionDataArray insertObject:dummyDict atIndex:i];
        }
    }

    NSMutableArray * testArray=[[NSMutableArray alloc]init];
    NSMutableArray * tempSalesDataArray=[[NSMutableArray alloc]init];

    NSMutableArray * tempReturnsDataArray=[[NSMutableArray alloc]init];
    
    NSMutableArray * tempCollectionDataArray=[[NSMutableArray alloc]init];

    
    for (NSString * amountStr in [testSalesDataArray valueForKey:@"Ammount"]) {
        [tempSalesDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        [emptyGraphDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];

    }
    for (NSString * amountStr in [testReturnsDataArray valueForKey:@"Ammount"]) {
        
        [tempReturnsDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
        
    }
    
    for (NSString * amountStr in [testCollectionDataArray valueForKey:@"Ammount"]) {
        
        [tempCollectionDataArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
    }
    
    [testArray addObject:tempSalesDataArray];
    [testArray addObject:tempReturnsDataArray];
    [testArray addObject:tempCollectionDataArray];
    
    NSLog(@"test array for dummy graph %@ \n%@ \n%@",tempSalesDataArray,tempReturnsDataArray,emptyGraphDataArray);

    
    [emptyGraphDataArray replaceObjectAtIndex:0 withObject:[NSDecimalNumber numberWithInteger:[@"100" integerValue]]];
    
    [testArray addObject:emptyGraphDataArray];

    
    self.data =testArray;
    
    
    
    
    NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
    
    
    //NSMutableArray * labalesArray=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8", nil];
    
    self.labels = labalesArray;
    
    //self.gkLineGraph.backgroundColor=[UIColor redColor];
    
    NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));
    
    self.gkLineGraph.dataSource = self;
    self.gkLineGraph.lineWidth = 3.0;
    
    self.gkLineGraph.valueLabelCount = 5;
    NSLog(@"trying to draw graph %@", self.gkLineGraph.layer.sublayers);
    [self.gkLineGraph draw];
}


-(void)viewDidAppear:(BOOL)animated{
    
    
    
    

    
    
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    //[self _setupExampleGraph];
   // [self _setupTestingGraphLow];
    
    NSLog(@"did appear called");
    
    [self loadDashboardData];
    [self loadTargetPieChart];
    
    
    if (self.isMovingToParentViewController) {
        
        NSInteger messagesCount=0;
        
        if([[AppControl retrieveSingleton].MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
        {
            NSMutableArray *messageArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select COUNT(*) from (select A.Sender_Name,A.Msg_ID,A.Sender_ID,A.Msg_Title,A.Msg_Body,A.Logged_At AS Sent_at,A.Expires_At,B.Read_At, B.Recipient_ID,B.Reply_Msg_ID from tbl_msg AS A INNER join TBL_Msg_Recipients AS B ON A.msg_id=B.msg_id where A.Sender_ID!='%@' AND  A.Expires_At>date('now') order by A.Logged_At desc) As C  Left join tbl_msg AS D on C.Reply_Msg_ID=D.msg_id Where C.Read_At is NULL",[NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]]]];
            
            if (messageArray.count > 0) {
                messagesCount = [[NSString getValidStringValue:[[messageArray objectAtIndex:0]valueForKey:@"COUNT(*)"]]integerValue];
            }
        }
        else
        {
            NSMutableArray *messageArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select COUNT(*) from TBL_FSR_Messages where Message_Read='N' "];
            
            if (messageArray.count > 0) {
                messagesCount = [[NSString getValidStringValue:[[messageArray objectAtIndex:0]valueForKey:@"COUNT(*)"]]integerValue];
            }
        }


        if (messagesCount>0) {
            messagesButton.hidden=NO;

            // Create your badge and add it as a subview to whatever view you want to badgify.
            GIBadgeView *badge = [GIBadgeView new];
            badge.font = kSWX_FONT_SEMI_BOLD(12);
            badge.textColor = [UIColor whiteColor];
            badge.topOffset=7.0;
            badge.rightOffset=7.0;
            badge.backgroundColor = [UIColor redColor];
            [messagesButton addSubview:badge];
            badge.badgeValue = messagesCount;
            
        }
        else{
            
            messagesButton.hidden=YES;
        }
        

    }
    

    
//    NSDate * launchedTime=[[NSUserDefaults standardUserDefaults] objectForKey:@"LoginTapped"];
//    
//    
//    NSDate * currentTimer=[NSDate date];
//    NSTimeInterval launchTimeTaken = [currentTimer timeIntervalSinceDate:launchedTime];
//    
//    NSLog(@"time taken to login %f", launchTimeTaken);
    
    }


-(void)closeSplitView
{
    NSLog(@"close split called");
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        if (revealController.currentFrontViewPosition !=0)
        {
            [revealController revealToggle:self];
        }
        revealController=nil;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view layoutIfNeeded];
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        } else {
        }
    }
    

    NSString *DateString = [MedRepQueries fetchDatabaseDateFormat];
    
    NSMutableArray *SODReading = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_FSR_Process_Transactions where Trx_Date like '%@%%'",DateString]];
        
    if ([SODReading count] == 0) {
        odoMeterButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"OdoMeter_Start"] style:UIBarButtonItemStylePlain target:self action:@selector(odoMeterButtonTapped)];
        odoMeterButton.tintColor=[UIColor whiteColor];
    } else {
        odoMeterButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"OdoMeter_End"] style:UIBarButtonItemStylePlain target:self action:@selector(odoMeterButtonTapped)];
        odoMeterButton.tintColor=[UIColor whiteColor];
    }    
    
    toDoListButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"TaskIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(toDoListButtonTapped)];
    toDoListButton.tintColor=[UIColor whiteColor];
    
    
    NSMutableArray *rightBarButtons=[[NSMutableArray alloc] init];
    
    if ([[AppControl retrieveSingleton].ENABLE_TODO isEqualToString:KAppControlsYESCode])
        [rightBarButtons addObject:toDoListButton];
//    if([appControl.ODOMETER_READING isEqualToString:KAppControlsYESCode]) // ODO Meter reading taken from the user only on starting of the first visit in a day
//        [rightBarButtons addObject:odoMeterButton];
    
    
    self.navigationItem.rightBarButtonItems=rightBarButtons;

//    trailingOfSalesView.constant = 0;
//    widthConstraintOfTargetInfoView.constant = 0;
    
    
    _slicesForCustomerPotential = [[NSMutableArray alloc]init];
    _sliceColorsForCustomerPotential = [[NSMutableArray alloc]init];
    self.sliceColorsForCustomerPotential = [NSArray arrayWithObjects:
                                            [UIColor colorWithRed:53.0/255.0 green:194.0/255.0 blue:195.0/255.0 alpha:1],
                                            [UIColor colorWithRed:16.0/255.0 green:49.0/255.0 blue:57.0/255.0 alpha:1],
                                            nil];
    
    self.TargetPieChart.backgroundColor = [UIColor clearColor];
    self.TargetPieChart.labelFont = [UIFont boldSystemFontOfSize:14];
    
    self.TargetPieChart.labelRadius = 45;
    [self.TargetPieChart setDelegate:self];
    [self.TargetPieChart setDataSource:self];
    [self.TargetPieChart setPieCenter:CGPointMake(TargetPieChart.frame.size.width/2, TargetPieChart.frame.size.height/2)];
    [self.TargetPieChart setShowPercentage:YES];
    [self.TargetPieChart setLabelColor:[UIColor whiteColor]];
    [self.TargetPieChart setUserInteractionEnabled:NO];
    
    
    // check SHOW_COLLECTION_TARGET flag is Y or N
    if ([appControl.SHOW_COLLECTION_TARGET isEqualToString:KAppControlsYESCode]) {
        targetInformationSegment.hidden = NO;
        lblTargetInformation.hidden = YES;
        dividerImageofTargetInformation.hidden = YES;
        
        
        collectionTargetArrayForSelectedCustomer = [[SWDatabaseManager retrieveManager]fetchCollectionTarget:@""];
        
        targetInformationSegment.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
        targetInformationSegment.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
        targetInformationSegment.sectionTitles = @[NSLocalizedString(@"Sales Target", nil), NSLocalizedString(@"Collection Target", nil)];
        
        targetInformationSegment.selectionIndicatorBoxOpacity=0.0f;
        targetInformationSegment.verticalDividerEnabled=YES;
        targetInformationSegment.verticalDividerWidth=1.0f;
        targetInformationSegment.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
        
        targetInformationSegment.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
        targetInformationSegment.selectionStyle = HMSegmentedControlSelectionStyleBox;
        targetInformationSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        targetInformationSegment.tag = 2;
        targetInformationSegment.selectedSegmentIndex = 0;
        
        targetInformationSegment.layer.shadowColor = [UIColor blackColor].CGColor;
        targetInformationSegment.layer.shadowOffset = CGSizeMake(2, 2);
        targetInformationSegment.layer.shadowOpacity = 0.1;
        targetInformationSegment.layer.shadowRadius = 1.0;
        
        [targetInformationSegment addTarget:self action:@selector(targetInformationSegmentValueChanged:) forControlEvents:UIControlEventValueChanged];
        
    } else {
        lblTargetInformation.hidden = NO;
        dividerImageofTargetInformation.hidden = NO;
        targetInformationSegment.hidden = YES;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    NSLog(@"did dis appear called");
}

-(void)odoMeterButtonTapped
{
    SalesWorxODOMeterViewController *odoMeterVC = [[SalesWorxODOMeterViewController alloc]init];
    odoMeterVC.ODOMeterDelegate = self;
    UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:odoMeterVC];
    mapPopUpViewController.navigationBarHidden = YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    odoMeterVC.view.backgroundColor = KPopUpsBackGroundColor;
    odoMeterVC.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}

-(void)StartVisitAfterPopUp {
    odoMeterButton.image = [UIImage imageNamed:@"OdoMeter_End"];
}

-(void)toDoListButtonTapped
{
    
//    SalesWorxFieldSalesDashboardViewController * graphVC=[[SalesWorxFieldSalesDashboardViewController alloc]init];
//    //graphVC.data=salesTrendArray;
//    [self.navigationController pushViewController:graphVC animated:YES];
    
    
     
     SalesWorxToDoPopUpViewController *presentingView=   [[SalesWorxToDoPopUpViewController alloc]init];
     UINavigationController * toDoPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
     toDoPopUpViewController.navigationBarHidden=YES;
     //presentingView.syncPopdelegate=self;
     toDoPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
     toDoPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
     presentingView.view.backgroundColor = KPopUpsBackGroundColor;
     presentingView.modalPresentationStyle = UIModalPresentationCustom;
     self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
     [self presentViewController:toDoPopUpViewController animated:NO completion:nil];
    
    
}

-(NSMutableArray*)nullFreeArray:(NSMutableArray*)contentArray
{
    for(NSInteger i=0;i<contentArray.count;i++)
    {
        if([[contentArray objectAtIndex:i] isKindOfClass:[NSNull class]])
            [contentArray replaceObjectAtIndex:i withObject:@"0.00"];
    }
    
    return contentArray;
    
}
-(void)testDataGraph
{
    
    if (salesDataArray.count > 0 || returnsDataArray.count > 0 || collectionsDataArray.count > 0) {
        
        
        
        
        
        NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
        currentMonthFormatter.dateFormat=@"MMMM";
        NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
        NSLog(@"current month is %@", currentMonth);
        monthLabel.text=[MedRepDefaults getDefaultStringForEmptyString:currentMonth];
        
        NSMutableArray * refinedSalesArray=[[NSMutableArray alloc]init];
        
        NSMutableArray * refinedReturnsArray=[[NSMutableArray alloc]init];
        NSMutableArray * refinedCollectionsArray=[[NSMutableArray alloc]init];
        
        
        if (salesDataArray.count>0) {
            //this is for first of every month
            
            if (salesDataArray.count==1) {
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:1]];
            }
            for (NSString * amountStr in [salesDataArray valueForKey:@"Ammount"]) {
                
                [refinedSalesArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
                
            }
            
            
            
        }
        if(returnsDataArray.count>0)
        {
            //this is for first of every month
            if (returnsDataArray.count==1) {
                [refinedReturnsArray addObject:[NSDecimalNumber numberWithInteger:1]];
            }
            for (NSString * amountStr in [returnsDataArray valueForKey:@"Ammount"]) {
                
                [refinedReturnsArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
                
            }
            
            
        }
        
        if(collectionsDataArray.count>0)
        {
            //this is for first of every month
            if (collectionsDataArray.count==1) {
                [refinedCollectionsArray addObject:[NSDecimalNumber numberWithInteger:1]];
            }
            for (NSString * amountStr in [collectionsDataArray valueForKey:@"Ammount"]) {
                
                [refinedCollectionsArray addObject:[NSDecimalNumber numberWithInteger:[amountStr integerValue]]];
                
            }
            
            
        }
        
        
        
        
        //
        //    NSLog(@"sales test : %@", [salesDataArray valueForKey:@"Ammount"]);
        //    NSLog(@"Retrun test %@",[returnsDataArray valueForKey:@"Ammount"]);
        
        NSMutableArray * testArray=[[NSMutableArray alloc]init];
        [testArray addObject:refinedReturnsArray];
        [testArray addObject:refinedSalesArray];
        [testArray addObject:refinedCollectionsArray];
        
        
        NSLog(@" test array %@",testArray);
        self.data =testArray;
        
        
        //NSLog(@"sales data array is %@",salesDataArray);
        
        // NSLog(@"retruns data array is %@", returnsDataArray);
        
        
        NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
        
        NSLog(@"labels array is %@",labalesArray);
        
        //NSMutableArray * labalesArray=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8", nil];
        
        self.labels = labalesArray;
        
        //self.gkLineGraph.backgroundColor=[UIColor redColor];
        
        NSLog(@"graph frame is %@", NSStringFromCGRect(self.gkLineGraph.frame));
        
        self.gkLineGraph.dataSource = self;
        self.gkLineGraph.lineWidth = 3.0;
        self.gkLineGraph.startFromZero=YES;
        self.gkLineGraph.valueLabelCount = 5;
        
        [self.gkLineGraph draw];
    }
    else
    {
        
    }
    
}
- (void)_setupExampleGraph {
    
    self.data = @[
                  [salesDataArray valueForKey:@"Ammount"],
                  [returnsDataArray valueForKey:@"Ammount"],
                  [collectionsDataArray valueForKey:@"Ammount"],
                  ];
    
    
    
    //fetching all dates of this month
    
    NSLog(@"data array is %@", self.data);
    
    NSLog(@"current month dates are %@", [self fetchDatesofCurrentMonth]);
    
    NSMutableArray * labalesArray=[self fetchDatesofCurrentMonth];
    self.labels = labalesArray;
    
    self.gkLineGraph.dataSource = self;
    self.gkLineGraph.lineWidth = 3.0;
    
    self.gkLineGraph.valueLabelCount = 10;
    
    [self.gkLineGraph draw];
}

-(NSMutableArray*)fetchDatesofCurrentMonthinDBFormat
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    [fmt setDateFormat:@"yyyy-MM-dd"]; // you can set this to whatever you like
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
        [dates sortUsingSelector:@selector(compare:)];
        
    }
    return dates;

}
-(NSMutableArray*)fetchDatesofCurrentMonth
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"dd"]; // you can set this to whatever you like
    [fmt setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
       // [dates insertObject:@"0" atIndex:0];
        
       // NSLog(@"dates are %@",dates);

        [dates sortUsingSelector:@selector(compare:)];
        
    }
    return dates;
}
- (void)_setupTestingGraphLow {
    
    /*
     A custom max and min values can be achieved by adding
     values for another line and setting its color to clear.
     */
    
    self.data = @[
                  @[@10, @4, @8, @2, @9, @3],
                  @[@1, @2, @3, @4, @5, @6]
                  ];
    //    self.data = @[
    //                  @[@2, @2, @2, @2, @2, @2, @6],
    //                  @[@1, @1, @1, @1, @1, @1, @1]
    //                  ];
    
    self.labels = @[@"2001", @"2002", @"2003", @"2004", @"2005", @"2006", @"2007",@"2001", @"2002", @"2003", @"2004", @"2005", @"2006", @"2007"];
    
    self.gkLineGraph.dataSource = self;
    self.gkLineGraph.lineWidth = 3.0;
    
    //    self.graph.startFromZero = YES;
    self.gkLineGraph.valueLabelCount = 10;
    
    [self.gkLineGraph draw];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Event Handlers



#pragma mark - GKLineGraphDataSource

- (NSInteger)numberOfLines {
    return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index {
    
    if (isEmptyGraph==YES) {
       
        id colors = @[[UIColor colorWithRed:(255.0/255.0) green:(166.0/255.0) blue:(78.0/255.0) alpha:1],
                      [UIColor colorWithRed:(43.0/255.0) green:(155.0/255.0) blue:(216.0/255.0) alpha:1],
                      [UIColor colorWithRed:(75.0/255.0) green:(182.0/255.0) blue:(130.0/255.0) alpha:1],
                      [UIColor clearColor],
                      ];
        return [colors objectAtIndex:index];

    }
    else{
        id colors = @[[UIColor colorWithRed:(255.0/255.0) green:(166.0/255.0) blue:(78.0/255.0) alpha:1],
                      [UIColor colorWithRed:(43.0/255.0) green:(155.0/255.0) blue:(216.0/255.0) alpha:1],
                      [UIColor colorWithRed:(75.0/255.0) green:(182.0/255.0) blue:(130.0/255.0) alpha:1],
                      ];
        return [colors objectAtIndex:index];
 
    }
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index {

    if (isEmptyGraph==YES) {
        return [[@[@1, @1.6,@1.6, @1.6] objectAtIndex:index] doubleValue];

    }
    else{
        return [[@[@1, @1.6, @1.6] objectAtIndex:index] doubleValue];
 
    }

}

- (NSString *)titleForLineAtIndex:(NSInteger)index {

    
    @try {
        return [self.labels objectAtIndex:index];

    } @catch (NSException *exception) {
        return @"";

    } @finally {
        
    }

    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Today's Visit Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self presentPopoverfor:customersTextField withTitle:@"Customers" withContent:customersArray];
    return NO;
}
-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=[contentArray valueForKey:@"Customer_Name"];
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    dashboardPopoverController=nil;
    dashboardPopoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    dashboardPopoverController.delegate=self;
    popOverVC.popOverController=dashboardPopoverController;
    popOverVC.titleKey=popOverString;
    if ([popOverString isEqualToString:kDoctorTitle]) {
        [dashboardPopoverController setPopoverContentSize:CGSizeMake(400, 300) animated:YES];
        
    }
    else{
        [dashboardPopoverController setPopoverContentSize:CGSizeMake(400, 300) animated:YES];
        
    }
    [dashboardPopoverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
    [self populateSelectedCustomerData:[customersArray objectAtIndex:selectedIndexPath.row]];
    
}

-(void)populateSelectedCustomerData:(Customer*)currentSelectedCustomer
{
    selectedCustomer=currentSelectedCustomer;
    customersTextField.text=[MedRepDefaults getDefaultStringForEmptyString:selectedCustomer.Customer_Name];
    
    NSMutableArray * outStandingArray=[[SWDatabaseManager retrieveManager]fetchTotalOutStandingforCustomer:selectedCustomer];
    
    NSLog(@"outstanding array is %@", outStandingArray);
    
    if (outStandingArray.count>0) {
        
        selectedCustomer.totalOutStanding=[SWDefaults getValidStringValue:[[outStandingArray objectAtIndex:0]valueForKey:@"Total"]];
        
        totalOutStandingLabel.text=[[MedRepDefaults getDefaultStringForEmptyString:selectedCustomer.totalOutStanding] currencyString];
        
        
    }
    else
    {
    }
    
    
    availableBalanceLabel.text=[[MedRepDefaults getDefaultStringForEmptyString:selectedCustomer.Avail_Bal] currencyString];
}


- (IBAction)routeButtonTapped:(id)sender {
    
    SalesWorxRouteViewController *routeVC = [[SalesWorxRouteViewController alloc]init];
//    SWRouteViewController *routeVC = [[SWRouteViewController alloc] init];
    routeVC.selectedCustomer = selectedCustomer;
    [self.navigationController pushViewController:routeVC animated:YES];
}

- (IBAction)startVisitTapped:(id)sender {
    
    SWCustomersViewController * customersVC=[[SWCustomersViewController alloc]init];
    customersVC.isFromDashBoard=YES;
    customersVC.selectedCustomerFromDashBoard=  selectedCustomer;
    [self.navigationController pushViewController:customersVC animated:YES];
    
}

#pragma mark DB methods for Count

-(void)updateVisitStatistics
{
    NSString* todaysDate=[MedRepQueries fetchDatabaseDateFormat];
    
    plannedVisitsCountLbl.font=kSWX_FONT_SEMI_BOLD(50);
    completedVisitsCountLbl.font=kSWX_FONT_SEMI_BOLD(50);
    outofRouteCountLbl.font=kSWX_FONT_SEMI_BOLD(50);

    dailyorderCountLbl.font=kSWX_FONT_SEMI_BOLD(50);
    dailyReturnCountLbl.font=kSWX_FONT_SEMI_BOLD(50);
    dailyCollectionCountLbl.font=kSWX_FONT_SEMI_BOLD(50);
    
    dailyOrderValueLbl.font=kSWX_FONT_SEMI_BOLD(16);
    dailyReturnValueLbl.font=kSWX_FONT_SEMI_BOLD(16);
    dailyCollectionValueLbl.font=kSWX_FONT_SEMI_BOLD(16);
    
    
    monthlySalesLabel.font = kSWX_FONT_SEMI_BOLD(22);
    totalReturnsLabel.font = kSWX_FONT_SEMI_BOLD(22);
    lastSynchronizationLbl.font = kSWX_FONT_SEMI_BOLD(22);
    
    
    NSArray *totalPlannedVisitsArray = [[SWDatabaseManager retrieveManager] dbGetTotalPlannedVisits:todaysDate];
    if(totalPlannedVisitsArray.count>0)
    {
        NSString* totalVisitsString=[SWDefaults getValidStringValue:[[totalPlannedVisitsArray objectAtIndex:0] valueForKey:@"Total"]];
        plannedVisitsCountLbl.text=[MedRepDefaults getDefaultStringForEmptyString:totalVisitsString];
    }

    NSArray *totalCompletedVisitsArray = [[SWDatabaseManager retrieveManager] dbGetTotalCompletedVisits:todaysDate];
    if(totalCompletedVisitsArray.count>0)
    {
        NSString* totalCompletedVisitsString=[SWDefaults getValidStringValue:[[totalCompletedVisitsArray objectAtIndex:0] valueForKey:@"Total"]];
        completedVisitsCountLbl.text=[MedRepDefaults getDefaultStringForEmptyString:totalCompletedVisitsString];
    }

    NSArray *totalOutofRouteArray = [[SWDatabaseManager retrieveManager] dbGetCustomerOutOfRouteCount:todaysDate];
    if(totalOutofRouteArray>0)
    {
        NSLog(@"out of route array is %lu", (unsigned long)totalOutofRouteArray.count);
        NSString* outOfRountString=[SWDefaults getValidStringValue:[NSString stringWithFormat: @"%lu", (long)totalOutofRouteArray.count]];
        outofRouteCountLbl.text=[MedRepDefaults getDefaultStringForEmptyString:outOfRountString];

    }
    
    
    NSArray *totalOrderArray = [[SWDatabaseManager retrieveManager] dbGetTotalOrder];
    if (totalOrderArray.count>0) {
        NSLog(@"total orders %@",[[totalOrderArray objectAtIndex:0] stringForKey:@"Total"]);
        dailyorderCountLbl.text=[SWDefaults getValidStringValue:[[totalOrderArray objectAtIndex:0] stringForKey:@"Total"]];
    }
    
    double  dailyOrderValue=[[SWDatabaseManager retrieveManager] fetchTotalTransactionValue:kSalesOrderDocType frequency:NO];
        dailyOrderValueLbl.text=[[NSString stringWithFormat:@"%f",dailyOrderValue] currencyString];
    
    
    
    NSArray *totalReturnArray = [[SWDatabaseManager retrieveManager] dbGetTotalReturn];
    if (totalReturnArray.count>0) {
        NSLog(@"total retuns %@",[[totalReturnArray objectAtIndex:0] stringForKey:@"Total"]);
        dailyReturnCountLbl.text=[SWDefaults getValidStringValue:[[totalReturnArray objectAtIndex:0] stringForKey:@"Total"]];

    }
    double  dailyReturnValue=[[SWDatabaseManager retrieveManager] fetchTotalTransactionValue:kReturnsDocType frequency:NO];
    dailyReturnValueLbl.text=[[NSString stringWithFormat:@"%f",dailyReturnValue] currencyString];
    
    
    
    
    
    NSArray *totalCollectionArray =[[SWDatabaseManager retrieveManager] dbGetTotalCollection];
    if (totalCollectionArray.count>0) {
        NSLog(@"total collections %@",[[totalCollectionArray objectAtIndex:0] stringForKey:@"Total"]);
        dailyCollectionCountLbl.text=[SWDefaults getValidStringValue:[[totalCollectionArray objectAtIndex:0] stringForKey:@"Total"]];
    }
    double  dailyCollectionValue=[[SWDatabaseManager retrieveManager] fetchTotalTransactionValue:kCollectionDocType frequency:NO];
    dailyCollectionValueLbl.text=[[NSString stringWithFormat:@"%f",dailyCollectionValue] currencyString];
    
    
    
    double  monthlySalesValue=[[SWDatabaseManager retrieveManager] fetchTotalTransactionValue:kSalesOrderDocType frequency:YES];
    monthlySalesLabel.text=[[NSString stringWithFormat:@"%f",monthlySalesValue] currencyStringWithoutCurrency];
    
    
    double  monthlyReturnsValue=[[SWDatabaseManager retrieveManager] fetchTotalTransactionValue:kReturnsDocType frequency:YES];
    totalReturnsLabel.text=[[NSString stringWithFormat:@"%f",monthlyReturnsValue] currencyStringWithoutCurrency];
    
    double  monthlyCollectionValue=[[SWDatabaseManager retrieveManager] fetchTotalTransactionValue:kCollectionDocType frequency:YES];
    monthlyCollectionsLabel.text=[[NSString stringWithFormat:@"%f",monthlyCollectionValue] currencyStringWithoutCurrency];
    
    UIImage * syncStatusImage=[[UIImage alloc]init];
    
    
    if([[SWDefaults lastSyncStatus] isEqualToString:@"Failed"])
    {
        lastSynchronizationLbl.textColor=[UIColor redColor];
        syncStatusImage =[UIImage imageNamed:@"SyncStatusFailed"];
    }
    else{
        syncStatusImage=[UIImage imageNamed:@"SyncStatus"];
        lastSynchronizationLbl.textColor=[UIColor colorWithRed:(33.0/255.0) green:(33.0/255.0) blue:(33.0/255.0) alpha:1];
    }
    
    NSString *syncDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm" destFormat:kDateFormatWithTime scrString:[SWDefaults lastSyncDate]];
    
    if(syncDate.length==0)
    {
        lastSynchronizationLbl.text=[SWDefaults lastSyncDate];
    }
    else
    {
        lastSynchronizationLbl.text = syncDate;
    }
    

    
    syncStatusImageView.image=syncStatusImage;
    
    
    
}

- (IBAction)messageTapped:(id)sender {
    
    CommunicationViewControllerNew * communicationsVC=[[CommunicationViewControllerNew alloc] init];
    
    [self.navigationController pushViewController:communicationsVC animated:YES];
}

#pragma mark Pie chart delegate methods

-(void)loadTargetPieChart
{
    [_slicesForCustomerPotential removeAllObjects];
    [TargetPieChart reloadData];
    
    NSMutableArray *arrTargetData = [[SWDatabaseManager retrieveManager] fetchTargetInformationForCurrentMonth];
    
    if (arrTargetData.count > 0 && !([[[arrTargetData valueForKey:@"Balance_To_Go"]objectAtIndex:0] doubleValue]==0 && [[[arrTargetData valueForKey:@"Sales_Value"]objectAtIndex:0] doubleValue]==0)) {
        pieParentView.hidden = NO;
        noDataImage.hidden = YES;
        
        float totalTarget= [[[arrTargetData valueForKey:@"Target_Value"] objectAtIndex:0] floatValue];
        float balanceToGo = [[[arrTargetData valueForKey:@"Balance_To_Go"] objectAtIndex:0] floatValue];

//        if (totalTarget < balanceToGo) {
//            balanceToGo = totalTarget;
//        }
//        float achievedTarget = totalTarget-balanceToGo;
        float achievedTarget=[[[arrTargetData objectAtIndex:0] valueForKey:@"Sales_Value"] floatValue];

        
        [_slicesForCustomerPotential addObject:[NSNumber numberWithFloat:achievedTarget]];
        [_slicesForCustomerPotential addObject:[NSNumber numberWithFloat:balanceToGo]];
        [TargetPieChart reloadData];
    } else {
        pieParentView.hidden = YES;
        noDataImage.hidden = NO;
    }
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    if (pieChart == self.TargetPieChart && self.slicesForCustomerPotential.count>0) {
        count = self.slicesForCustomerPotential.count;
    }
    return count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    if (pieChart == self.TargetPieChart) {
        count = [[NSString stringWithFormat:@"%@",[self.slicesForCustomerPotential objectAtIndex:index]]floatValue];
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == self.TargetPieChart) {
        colour = [self.sliceColorsForCustomerPotential objectAtIndex:(index % self.sliceColorsForCustomerPotential.count)];
    }
    return colour;
}


#pragma mark target Information Segment
- (IBAction)targetInformationSegmentValueChanged:(id)sender {
    
    if (targetInformationSegment.selectedSegmentIndex == 0) {

        [self loadTargetPieChart];
    }
    else if (targetInformationSegment.selectedSegmentIndex == 1)
    {
        [_slicesForCustomerPotential removeAllObjects];
        [self.TargetPieChart reloadData];
        
        if (collectionTargetArrayForSelectedCustomer.count > 0) {
            
            pieParentView.hidden = NO;
            noDataImage.hidden = YES;

            CustomerCollectionTarget *objCollectionTarget = [collectionTargetArrayForSelectedCustomer objectAtIndex:0];
            double targetVal = [objCollectionTarget.Target_Value doubleValue];
            double salesVal = [objCollectionTarget.Achieved_Value doubleValue];
            
            double balanceToGo = targetVal-salesVal;
            if (balanceToGo < 0) {
                balanceToGo = 0;
            }
            
            [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:salesVal]];
            [_slicesForCustomerPotential addObject:[NSNumber numberWithDouble:balanceToGo]];
            [self.TargetPieChart reloadData];
            
            if(targetVal==0 && salesVal==0){
                pieParentView.hidden = YES;
                noDataImage.hidden = NO;
            }
            
        } else {
            pieParentView.hidden = YES;
            noDataImage.hidden = NO;
        }
    }
}

@end
