//
//  SalesWorxODOMeterViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/16/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxODOMeterViewController.h"

@interface SalesWorxODOMeterViewController ()

@end

@implementation SalesWorxODOMeterViewController
@synthesize contentView,previousReadingTextField, currentReadingTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.view.backgroundColor = UIViewControllerBackGroundColor;

    @try {
        NSMutableArray *arrPreviousReading = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"Select max(Odo_Reading) as Odo_Reading from TBL_FSR_Process_Transactions"];
        
        if ([[[arrPreviousReading objectAtIndex:0]valueForKey:@"Odo_Reading"]intValue] == 0) {
            previousReadingTextField.text = @"0";
        } else {
            previousReadingTextField.text = [NSString stringWithFormat:@"%@",[[arrPreviousReading objectAtIndex:0]valueForKey:@"Odo_Reading"]];
        }
        
    } @catch (NSException *exception) {
        previousReadingTextField.text = @"0";
    }
    if ([previousReadingTextField.text isEqualToString:@"0"]) {
        previousReadingTextField.text = [SWDefaults odoMeterPreviousReading].length == 0 ? @"0" : [SWDefaults odoMeterPreviousReading];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    if(self.isMovingToParentViewController)
    {
        contentView.backgroundColor=UIViewControllerBackGroundColor;
        [saveButton.titleLabel setFont:NavigationBarButtonItemFont];
        [cancelButton.titleLabel setFont:NavigationBarButtonItemFont];
        
        for (UIView *borderView in contentView.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                if (borderView.tag==101) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                    
                }
            }
        }
        
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
}

-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}


#pragma mark AnimationMethods
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    UIView* containerView = contentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
#pragma mark buttopnActions

- (IBAction)SaveButtontapped:(id)sender {
    
    if ([currentReadingTextField isFirstResponder]) {
        
        [currentReadingTextField resignFirstResponder];
    }

    if ([NSString isEmpty:currentReadingTextField.text]) {
        [self showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter current reading" withController:nil];
    }
    else if ([currentReadingTextField.text intValue] < [previousReadingTextField.text intValue]) {
        [self showAlertAfterHidingKeyBoard:KInvalidData andMessage:@"Odometer reading should be greater than or equal to Previous Reading" withController:nil];
    }
    else
    {
        NSDictionary * dicUserProfile = [SWDefaults userProfile];
        status = NO;
        
        @try {
     
            NSString *DateString = [MedRepQueries fetchDatabaseDateFormat];        
            
            NSMutableArray *SODReading = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_FSR_Process_Transactions where Trx_Date like '%@%%'",DateString]];
            NSLog(@"todaty readings %@",SODReading);
            NSString *databaseQuery;
            
            if ([SODReading count] == 0) {
                
                databaseQuery = [NSString stringWithFormat:@"insert into TBL_FSR_Process_Transactions (Row_ID, Trx_Type, SalesRep_ID, Emp_Code, Odo_Reading,Trx_Date)  VALUES ('%@', '%@', '%@', '%@', '%@', '%@')", [NSString createGuid], @"SOD", [dicUserProfile stringForKey:@"SalesRep_ID"], [dicUserProfile stringForKey:@"Emp_Code"], currentReadingTextField.text,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
                
            } else if([SODReading count] == 1) {
                databaseQuery = [NSString stringWithFormat:@"insert into TBL_FSR_Process_Transactions (Row_ID, Trx_Type, SalesRep_ID, Emp_Code, Odo_Reading,Trx_Date)  VALUES ('%@', '%@', '%@', '%@', '%@', '%@')", [NSString createGuid], @"EOD", [dicUserProfile stringForKey:@"SalesRep_ID"], [dicUserProfile stringForKey:@"Emp_Code"], currentReadingTextField.text,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
            }
            else {
                
                databaseQuery = [NSString stringWithFormat:@"Update TBL_FSR_Process_Transactions SET Odo_Reading='%@',Trx_Date='%@' WHERE Trx_Date like '%@%%' and Trx_Type = '%@'", currentReadingTextField.text,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],DateString, @"EOD"];
            }
            
            
            status = [FMDBHelper executeNonQuery:databaseQuery];
            
        } @catch (NSException *exception) {
            NSLog(@"Exception while saving notes - %@",exception);
        }
        
        if (status == YES) {
            
            
            [SWDefaults setOdoMeterPreviousReading:currentReadingTextField.text];
            
            [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];
        }
    }
}

- (IBAction)cancelButtonTapped:(id)sender {

    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];
}

- (void)CloseSyncPopOver
{
    //do what you need to do when animation ends...
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    if ([self.ODOMeterDelegate respondsToSelector:@selector(StartVisitAfterPopUp)] && status) {
        [self.ODOMeterDelegate StartVisitAfterPopUp];
    }
}


#pragma mark UITextFieldMethods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }

    NSString *expression = @"^[0-9]*$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
    return (numberOfMatches != 0 && newString.length<=ODOMeterTextFieldLimit);
    

    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self animateTextField:textField up:YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    [self animateTextField:textField up:NO];
}

- (void) animateTextField:(UITextField*)textField up: (BOOL) up
{
    NSInteger movementDistance = 120;
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
