//
//  SalesWorxPanelHeaderDividerLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 1/25/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPanelHeaderDividerLabel.h"
#import "SWDefaults.h"

@implementation SalesWorxPanelHeaderDividerLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    
    [super awakeFromNib];
    self.backgroundColor=kSalesWorxPanelHeaderDividerColor;
    //self.textColor=[UIColor redColor];
    
}


@end
