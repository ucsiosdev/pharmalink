//
//  CustomerPopOverViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CustomerPopOverViewController.h"

@interface CustomerPopOverViewController ()

@end

@implementation CustomerPopOverViewController

@synthesize types;
@synthesize target;
@synthesize action;

- (id)init {
    self = [super init];
    
    if (self) {
        //[self setTypes:[NSArray arrayWithObjects:@"CURRENT CHEQUE", @"CASH", @"PDC", nil]];
        [self setPreferredContentSize:CGSizeMake(250, 130)];
    }
    
    return self;
}



#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.types count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];

        [cell.textLabel setText:[self.types objectAtIndex:indexPath.row]];
        
        return cell;
    }
}

#pragma mark UITableView Deleate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedType = [self.types objectAtIndex:indexPath.row];
    if ([self.target respondsToSelector:self.action])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:selectedType];
#pragma clang diagnostic pop
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end