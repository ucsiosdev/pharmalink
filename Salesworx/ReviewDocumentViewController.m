


//
//  ReviewDocumentViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "ReviewDocumentViewController.h"
#import "SalesOrderNewViewController.h"
#import "CustomerOrderHistoryView.h"
#import "ReviewDocumentReportsViewController.h"
#import "MedRepDefaults.h"
@interface ReviewDocumentViewController ()

@end

@implementation ReviewDocumentViewController
@synthesize datePickerViewControllerDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"review document called");
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:[kReviewDocumentsTitle localizedCapitalizedString]];
    gridView = [[GridView alloc] initWithFrame:CGRectMake(283, 8, 733, 689)];
    [gridView setDataSource:self];
    [gridView setDelegate:self];
    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [gridView.layer setBorderWidth:1.0f];
    [self.view addSubview:gridView];
    
    
    NSLog(@"review document called");
    
    filterTableView.backgroundView=nil;[filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [filterTableView.layer setBorderWidth:1.0f];
    filterTableView.backgroundColor=[UIColor whiteColor];
    
    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    self.datePickerViewControllerDate=datePickerViewController;
    
    datePickerViewController.isRoute=NO;
    datePickerViewController.forReports=YES;
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    navigationController.navigationBar.translucent = NO;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    //NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    //NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    //[offsetComponents setMonth:-1]; // note that I'm setting it to -1
    //fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
    custType=@"All";
    
    //testing to date
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate * currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    //[comps setYear: -18];
    [comps setDay:-1];
    
    // NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
     minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    
    
    
    
    
    
    //toDate=[formatter stringFromDate:minDate];
    
    
    //toDate=[formatter stringFromDate:[NSDate date]];
    dataArray = [NSMutableArray array];
    
    fromDate=[formatter stringFromDate:minDate];
    toDate=[formatter stringFromDate:[NSDate date]];
    
    
    [self LoadDataFromDB];
    
    //  why so much complication what are these unused queries doing
    
       formatter=nil;
    usLocale=nil;
}


-(void)LoadDataFromDB
{
    // NSString *sQry =@" select max(DocNo) AS DocNo,max(DocDate) DocDate, sum(Amount)AS Amount,Doctype, Customer_Name from (  SELECT  A.Collection_Ref_No AS DocNo,A.Collected_On as DocDate, A.Amount AS Amount,C.Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A INNER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC) as X group by Customer_Name";
    
    
    
    
    
    
      //noew set the converted amount
    
    
    
    
    
    
    
    
    NSString *sQry =@"SELECT A.Collected_On AS Start_Time, A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY  A.Collection_Ref_No ASC";
    
    
    
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",minDate]];
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    
    NSLog(@"collection query from did load %@", sQry);
    
    
    
    
    
    
    
    
    //    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
    //    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    
    NSLog(@"data one is %@", [sQry description]);
    [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
    
    NSLog(@"data array for collection did load %@", [dataArray description]);
    
    //    NSString *sQry1 =@" SELECT A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date AS DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name FROM  TBL_Sales_History AS A INNER JOIN TBL_Customer AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type='I'  AND A.Creation_Date>='{1}' AND A.Creation_Date<='{2}' ORDER BY A.Orig_Sys_Document_Ref ASC";
    //    sQry1 = [sQry1 stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
    //    sQry1 = [sQry1 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    //    [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry1]];
    
    
    NSString *sQry2 =@"SELECT A.Start_Time, A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY  A.Orig_Sys_Document_Ref ASC";
    
    NSLog(@"toDate ---%@",fromDate);
    NSLog(@" fromDate---%@",toDate);
    NSString * tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
    NSString * tempToDate=[toDate stringByAppendingString:@" 23:59:59"];

    sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
    sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
    
    
    //    sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
    //    sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    
    
    NSLog(@"return query from did load %@",sQry2);
    
    NSLog(@"data array before adding return %@", [dataArray description]);
    
    
    
    [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry2]];
    NSLog(@"data array after ading return %@", [dataArray description]);
    
    
    
    
    
    NSString *sQry3 =@"SELECT A.Start_Time, A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name, 'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY A.Orig_Sys_Document_Ref ASC ";
    
     tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
     tempToDate=[toDate stringByAppendingString:@" 23:59:59"];

    
    sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
    sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
    
    //    sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
    //    sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    
    
    
    NSLog(@"data array which has collection and return %@", [dataArray description]);
    
    
    [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry3]];
    NSLog(@"complete data array %@", [dataArray description]);
    
    
    NSLog(@"data array at the end is %d", [dataArray count]);
    
    
    
    NSString* multySqry=@"select TBL_Collection.Collection_Ref_NO ,TBL_Collection.Amount,TBL_Collection.Collected_On,TBL_Collection.Customer_ID, (TBL_Doc_Addl_Info.Custom_Attribute_5 *TBL_Collection.Amount) AS Converted_Amount  from TBL_Collection inner join TBL_Doc_Addl_Info on TBL_Collection.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO where TBL_Collection.Collected_On>='2015-04-13 00:00:00' AND TBL_Collection.Collected_On<='{2}'  ";
    
   // multySqry = [multySqry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",tempfromDate]];
    multySqry = [multySqry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    
    NSLog(@"multi sqry is %@", multySqry);
    
    
    
    convertedAmountArray=[[SWDatabaseManager retrieveManager] dbGetDataForReport:multySqry];
    
    //[dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:multySqry]];
    
    NSLog(@"test data array  %@", [dataArray description]);
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:FALSE];
    [dataArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSLog(@"test data array after sorting %@", [dataArray description]);
    

    
    [gridView reloadData];

}


-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"Review Documents");
    //[self viewReportAction];
    //[self LoadDataFromDB];
    
    
    
    //rectifying the query if collection is done through multi currency
    
    
   
    
    //[gridView reloadData];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [self viewReportAction];
}
- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
    
}

//testing touches began method



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touched");
}

- (void)dateChanged:(SWDatePickerViewController *)sender
{
    
    NSLog(@"from date after click %@", fromDate);
    
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        NSString * sampleDate=fromDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            fromDate=sampleDate;
        }
        else
        {
            fromDate=[formatter stringFromDate:sender.selectedDate];

        }
        
        
    }
    else
    {
        NSString* sampleToDate=toDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            toDate=sampleToDate;
        }
        else
        {
            toDate=[formatter stringFromDate:sender.selectedDate];

            
        }
        
        
    }
    [filterTableView reloadData];
    
    if ([formatter stringFromDate:sender.selectedDate]==nil) {
        
    }
    else
    {
        [self viewReportAction];
        
    }
    formatter=nil;
    usLocale=nil;

    
    NSLog(@"from date in date changed is %@", fromDate);
    
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterTableView reloadData];
    [self viewReportAction];
    currencyTypeViewController = nil;
    currencyTypePopOver = nil;
    
}

#pragma TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
    return 4;
    
}
- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
    //    [companyNameLabel setShadowColor:[UIColor whiteColor]];
    //    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:companyNameLabel];
    return headerView;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return NSLocalizedString(@"Filter", nil);
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    filterTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    
    @autoreleasepool
    {
        NSString *CellIdentifier = @"CustomerDetailViewCell";
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
        }
        
        if(indexPath.row==0)
        {
            if(customerDict.count!=0)
            {
                cell.textLabel.text = [customerDict stringForKey:@"Customer_Name"];
            }
            else
            {
                cell.textLabel.text = NSLocalizedString(@"Select Customer", nil);
            }
        }
        else if(indexPath.row==1)
        {
            [cell.textLabel setText:NSLocalizedString(@"From Date", nil)];
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:fromDate]];
        }
        else if(indexPath.row==2)
        {
            [cell.textLabel setText:NSLocalizedString(@"To Date", nil)];
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:toDate]];
        }
        else if(indexPath.row==3)
        {
            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
            [cell.detailTextLabel setText:custType];
        }
        //        else if(indexPath.row==4)
        //        {
        //            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
        //            [cell.detailTextLabel setText:DocType];
        //        }
        else
        {
            [cell.textLabel setText:@"View Report"];
            
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font=LightFontOfSize(14.0f);
        cell.detailTextLabel.font=LightFontOfSize(14.0f);
        
        return cell;
    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0)
    {
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else if(indexPath.row==1)
    {
        isFromDate=YES;
        
        
        //date issue
        
        NSLog(@"From Date is %@", fromDate);
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:fromDate];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;
        
        
        
        
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==2)
    {
        //date issue
        
        
        
        NSLog(@"From Date is %@", toDate);
        
        NSDateFormatter * fromater=[NSDateFormatter new];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:toDate];
        
        
        datePickerViewControllerDate.picker.date=finalDate;
        
        datePickerViewControllerDate.isToDate=YES;

        
        isFromDate=NO;
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==3)
    {
        customVC=nil;
        customVC = [[CustomerPopOverViewController alloc] init] ;
        [customVC setTarget:self];
        [customVC setAction:@selector(typeChanged:)];
        [customVC setPreferredContentSize:CGSizeMake(200,250)];
        
        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
        customPopOver.delegate=self;
        isCustType=YES;
        [customVC setTypes:[NSArray arrayWithObjects:NSLocalizedString(@"All", nil),NSLocalizedString(@"Collection", nil)/*,NSLocalizedString(@"Invoice", nil)*/,NSLocalizedString(@"Sales Returns", nil),NSLocalizedString(@"Sales Order", nil) , nil]];
        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    //    else if(indexPath.row==4)
    //    {
    //        customVC = [[CustomerPopOverViewController alloc] init] ;
    //        [customVC setTarget:self];
    //        [customVC setAction:@selector(typeChanged:)];
    //        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
    //        isCustType=NO;
    //        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil]];
    //        [customPopOver presentPopoverFromRect:CGRectMake(280,235, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //    }
    else
    {
        
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return dataArray.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 6;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0) {
        title =[NSString stringWithFormat:@"    %@",NSLocalizedString(@"Doc No", nil)];
    }
    else if (column == 1) {
        title = NSLocalizedString(@"Doc Type", nil);
    }
    else if (column == 2) {
        title = NSLocalizedString(@"Doc Date", nil);
    }
    else if (column == 3) {
        title = NSLocalizedString(@"Name", nil);
    }else if (column == 4) {
        title = NSLocalizedString(@"Amount", nil);
    }
    else if (column == 5) {
        title = NSLocalizedString(@"Status", nil);
    }
    
    return title;
}
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex
{
    if (columnIndex == 0 || columnIndex == 3) {
        return 20;
    }
    else
    {
        return 15;
    }
    
}
- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    
    NSDictionary *data = [dataArray objectAtIndex:row];
    
   // NSLog(@"data is %@", [data description]);
    
    NSString *value = @"";
    if (column == 0) {
        value = [data stringForKey:@"DocNo"];
    }
    else if (column == 1) {
        value = [data stringForKey:@"Doctype"];
    }
    else if (column == 2) {
        
        //format date and show only date without time
        
        NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd/MM/yyyy" scrString:[data stringForKey:@"DocDate"]];
        
        
        value = refinedDate;
    }
    else if (column == 3) {
        value = [data stringForKey:@"Customer_Name"];
    }
    else if (column == 4) {
        
        
        value = [data currencyStringForKey:@"Amount"];

        
        
//        if (dataArray.count>0) {
//            
//            
//            
//            
//            NSString * collectionData=[data currencyStringForKey: @"Amount"];
//            
//            
//        
//        if ([[data valueForKey:@"Doctype"]isEqualToString:@"Collection"] && !collectionData) {
//            NSString* customerCurrencyCode=[[SWDefaults userProfile]valueForKey:@"Currency_Code"];
//            
//            if ([customerCurrencyCode isEqualToString:@"USD"]) {
//                
//                value = [NSString stringWithFormat:@"$%@", [[convertedAmountArray valueForKey:@"Converted_Amount"] objectAtIndex:row]];
//                
//
//                
//            }
//            
//            else
//            {
//                value = [NSString stringWithFormat:@"%@%@",customerCurrencyCode, [[convertedAmountArray valueForKey:@"Converted_Amount"] objectAtIndex:row]];
//                
//
//            }
//        
//            
//        }
//            
//            else
//            {
//                value = [data currencyStringForKey:@"Amount"];
//  
//            }
//            
//        }
//        else
//        {
//        value = [data currencyStringForKey:@"Amount"];
//        
//        }
        
       

    }
    else if (column == 5) {
        value = [data stringForKey:@"ERP_Status"];
    }
    
    
    
    return value;
}
- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex;
{
    
    
    //remove collections from data array
    
   
    NSMutableDictionary * selectedReportDict=[dataArray objectAtIndex:rowIndex];
    
    
    
    
    
    if ([[[dataArray objectAtIndex:rowIndex] valueForKey:@"Doctype"]isEqualToString:@"Collection"]) {
        
        
    }
    
    else
    {
        
    
    ReviewDocumentReportsViewController* rdVc=[[ReviewDocumentReportsViewController alloc]init];
        
        NSLog(@"data array before removing %@", [dataArray description]);

        
        
        for (NSInteger m =0; m<dataArray.count; m++) {
            
            if ([[[dataArray objectAtIndex:m] valueForKey:@"Doctype"]isEqualToString:@"Collection"]) {
                
                [dataArray removeObjectAtIndex:m];
                rowIndex=rowIndex-1;
                
            }
            
        }
        
        NSLog(@"data array after removing %@", [dataArray description]);
        
        NSInteger updatedIndex=[dataArray indexOfObject:selectedReportDict];
        
        NSLog(@"UPDATED REPORT INDEX IS %d",updatedIndex);
        
        rdVc.reportType=[[dataArray objectAtIndex:updatedIndex]valueForKey:@"Doctype"];
        
        rdVc.selectedIndex=updatedIndex;
    rdVc.ordersArray=dataArray ;
    rdVc.totalContentDataArray=dataArray;
    [self.navigationController pushViewController:rdVc animated:YES];
    }
    
//    
//    CustomerOrderHistoryView *customerOrderHistoryView=[[CustomerOrderHistoryView alloc]init];
//
//    customerOrderHistoryView.testIndexPath=0;
//    [customerOrderHistoryView loadTestView];
    

    
    
    //adding a new controller
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    if ([[[dataArray objectAtIndex:rowIndex] stringForKey:@"Doctype"] isEqualToString:@"Order"])
//    {
//        NSArray *tempArray = [[SWDatabaseManager retrieveManager]dbGetCustomerShipAddressDetailsByCustomer_Name:[[dataArray objectAtIndex:rowIndex]stringForKey:@"Customer_Name"] ];
//        Singleton *single=[Singleton retrieveSingleton];
//        single.visitParentView=@"MO";
//        
//        
//        NSDate * now=[NSDate date];
//        
//        NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
//        
//        [formatter setDateFormat:@"yyy-mm-dd"];
//        
//        NSString * date=[formatter stringFromDate:now];
//        
//        NSLog(@"formatted date is %@", date);
//        
//        
//        
//        single.orderStartDate = [NSDate date];
//        single.cashCustomerDictionary = [NSMutableDictionary new];
//        [single.cashCustomerDictionary setValue:[[dataArray objectAtIndex:rowIndex] stringForKey:@"Credit_Customer_ID"] forKey:@"Customer_ID"];
//        [single.cashCustomerDictionary setValue:[[dataArray objectAtIndex:rowIndex] stringForKey:@"Credit_Site_ID"] forKey:@"Site_Use_ID"];
//        
//        //customerDict=nil;
//        //customerDict = [NSMutableDictionary dictionaryWithDictionary:[dataArray objectAtIndex:0]];
//        
//        NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionaryWithDictionary:[tempArray objectAtIndex:0]];
//        NSString *shipCustomer = [tempDictionary stringForKey:@"Customer_ID"];
//        NSString *shipSite = [tempDictionary stringForKey:@"Site_Use_ID"];
//        [tempDictionary setValue:shipCustomer forKey:@"Ship_Customer_ID"];
//        [tempDictionary setValue:shipSite forKey:@"Ship_Site_Use_ID"];
//        NSLog(@"data array %@", [dataArray description]);
//        
//        //fix this crash here
//        
//        //analysing crash here
//        
//        NSString * orderstr=[[dataArray objectAtIndex:rowIndex]stringForKey:@"DocNo"];
//        
//        
//        
//        
//        
//        //join tBL_order and TBL- order_history to get custom_attribute_2 of tbl order
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        NSString* orderStr1=[NSString stringWithFormat:@"select Custom_Attribute_2 from TBL_Order where TBL_Order.Orig_Sys_Document_Ref = '%@' ",orderstr];
//        
//        
//        
//        //test data here
//        
//        NSArray *testContent=[[SWDatabaseManager retrieveManager]dbGetSalesOrderItems:orderstr];
//        
//        
//        NSLog(@"test array desc is %@", [testContent valueForKey:@"Inventory_Item_ID"]);
//        
//        
//        NSMutableString* getSavedCategoryIDQuery=[NSMutableString stringWithFormat:@" select  category  from tbl_product  where Inventory_Item_ID='%@'",[[testContent valueForKey:@"Inventory_Item_ID"] objectAtIndex:0] ];
//        
//        
//        NSLog(@"got category %@", [[[[SWDatabaseManager retrieveManager]fetchDataForQuery:getSavedCategoryIDQuery]valueForKey:@"Category"] objectAtIndex:0]);
//        
//        
//        
//        
//        
//            
//            
//        
//        NSString *savedCategoryID =[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:getSavedCategoryIDQuery]valueForKey:@"Category"] objectAtIndex:0];
//        
//        // [[[[SWDatabaseManager retrieveManager] fetchDataForQuery:orderStr1 ]objectAtIndex:0] stringForKey:@"Custom_Attribute_2"];
//        
//        NSLog(@"saved string %@", savedCategoryID);
//        
//        [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:tempDictionary] andCategoryName:savedCategoryID andOrderDictionary:[dataArray objectAtIndex:rowIndex] andCustomer:[dataArray objectAtIndex:0]];
//      
//        
//        
//        
//        
//        
//        
//    }
}
- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict andCustomer : (NSMutableDictionary *)customer
{
    // NSLog(@"CAtegory %@",categories);d
    
    //categoryArray=categories;
    
    //Singleton *single = [Singleton retrieveSingleton];
    NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:categories];
    //NSMutableDictionary *category= [NSMutableDictionary new];
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID])
        {
            //category=[row mutableCopy];
            
            [SWDefaults setProductCategory:[row mutableCopy]];
            
            
            //NSLog(@"setProductCategory 1 %@",[SWDefaults productCategory]);
            
            break;
        }
    }
    
    
    [dict setValue:[dict stringForKey:@"DocNo"] forKey:@"Orig_Sys_Document_Ref"];
    [dict setValue:[dict stringForKey:@"DocDate"] forKey:@"Schedule_Ship_Date"];
    
    
    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    newSalesOrder.parentViewString = @"MO";
    newSalesOrder.preOrderedItems=[NSMutableArray new];
    newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
    newSalesOrder.isOrderConfirmed=@"confirmed";
    
    [self.navigationController pushViewController:newSalesOrder animated:YES];
}







//in the below method to fix the reports which for not showing if to and from date are same ,

//REPLACED THIS

//sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
//        sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];


//WITH THIS TO AVOID HHH:MM:SS FORMAT WHICH FIXED THE ISSUE

//sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:fromDate];
//sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:toDate];


-(IBAction)viewReportAction
{
    
    
       
    //try distinct
    //NSLog(@"Selected cuistomer %@",customerDict);
    if (customerDict.count!=0)
    {
        if (custType.length!=0)
        {
            NSString *sQry;
            if([custType isEqualToString:NSLocalizedString(@"All", nil)])
            {
                //sQry =@" SELECT  max(A.Collection_Ref_No) AS DocNo,max(A.Collected_On) as DocDate, sum(A.Amount) AS Amount,C.Customer_Name ,  'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A INNER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' AND A.Customer_Id='{3}'  ORDER BY A.Collection_Ref_No ASC as X group by Customer_Name";
                
                
                
                
             // sQry =@"  select max(DocNo) AS DocNo,max(DocDate) DocDate, sum(Amount)AS Amount,Doctype, Customer_Name from (   SELECT  A.Collection_Ref_No AS DocNo,A.Collected_On as DocDate, A.Amount AS Amount,C.Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A INNER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  WHERE  A.Collected_On>='2014-05-18 10:28:16 +0000 00:00:00' AND A.Collected_On<='2014-05-19 23:59:59' AND A.Customer_ID='2397' ORDER BY A.Collection_Ref_No ASC) as X group by Customer_Name";
                
                
                
                sQry =@"SELECT  A.Collected_On AS Start_Time, A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' AND A.Customer_Id='{3}' ORDER BY A.Collection_Ref_No ASC ";
                
                NSLog(@"squery is view report action of review document is %@", sQry);
              
                
//                NSDateFormatter* sampleFormatter=[[NSDateFormatter alloc]init];
//                NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//                [sampleFormatter setLocale:usLocale];
//                [sampleFormatter setDateFormat:@"yyyy-MM-dd"];
//                
//                NSLog(@"todate here is %@", toDate);
//                
//                NSDate * finalDate=[sampleFormatter dateFromString:toDate];
//                
//                
//                
//                NSDate *newDate = [finalDate dateByAddingTimeInterval:24.0f * 60.0f * 60.0f - 1.0f];
//                
//                NSString * finalToDate=[sampleFormatter stringFromDate:newDate];
              
                
                //NSLog(@"new date is %@", newDate);
                
                
                
                //                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                //                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                
                
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Customer_ID"]];
                //sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
                
                NSLog(@"sQry 1 %@", sQry);
                
                
                [dataArray setArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
                
                NSLog(@"data from sqry 1 %@", [dataArray description]);
                
                //                sQry =@" SELECT A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date AS DocDate, A.Transaction_Amt AS Amount,C.Customer_Name FROM  TBL_Sales_History AS A INNER JOIN TBL_Customer AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type='I'  AND A.Creation_Date>='{1}' AND A.Creation_Date<='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                //                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                //                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                //                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Customer_ID"]];
                //                sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
                
                //[dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
                
                sQry =@"SELECT  A.Start_Time,A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
                
                [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
                NSLog(@"data from sqry 2 %@", [dataArray description]);
                
                sQry =@"SELECT A.Start_Time, A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
                
                NSLog(@"data from sqry 3 %@", [dataArray description]);
                
                [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
                
                
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:FALSE];
                [dataArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                
                NSLog(@"test data array after sorting %@", [dataArray description]);
                

                
                [gridView reloadData];
                
            }
            else if(![custType isEqualToString:NSLocalizedString(@"All", nil)])
            {
                if([custType isEqualToString:NSLocalizedString(@"Collection", nil)])
                {
                    sQry =@" SELECT   A.Collected_On AS Start_Time,A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' AND A.Customer_Id='{3}'  ORDER BY A.Collection_Ref_No ASC  ";
                    
                    
                    NSLog(@"to date if filter is all and collection %@ %@ ", fromDate,toDate);
                    
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                    
                }
                //                else if([custType isEqualToString:@"Invoice"])
                //                {
                //                    sQry =@" SELECT A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date AS DocDate, A.Transaction_Amt AS Amount,C.Customer_Name FROM  TBL_Sales_History AS A INNER JOIN TBL_Customer AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type='I'  AND A.Creation_Date>='{1}' AND A.Creation_Date<='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                //                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                //                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                //
                //                }
                else if([custType isEqualToString:@"Sales Returns"])
                {
                    sQry =@"SELECT  A.Start_Time,A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                    
                }
                else if([custType isEqualToString:NSLocalizedString(@"Sales Order", nil)])
                {
                    sQry =@"SELECT A.Start_Time, A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                }
                
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
                
                dataArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry];
                
                
                
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:FALSE];
                [dataArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                
                NSLog(@"test data array after sorting %@", [dataArray description]);
    
                
                
                [gridView reloadData];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select document type." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
                
                // [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select document type." closeButtonTitle:NSLocalizedString(@"OK", nil) secondButtonTitle:nil tappedButtonAtIndex:nil];
            }
        }
    }
    else if([custType isEqualToString:NSLocalizedString(@"All", nil)])
    {
        dataArray = [NSMutableArray array];
       /* NSString *sQry =@"SELECT  A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(TBL_Doc_Addl_Info.Custom_Attribute_5 *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC";*/
        
        
        
        
        //        sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
        //        sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        
        
        NSString *sQry;
        
        sQry =@"SELECT   A.Collected_On AS Start_Time,A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC ";
        
        
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        
        
        
        [dataArray setArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
        NSLog(@"query for fetching collection when all records tapped %@",[sQry description]);
        
        
        
        
        //        NSString *sQry1 =@" SELECT A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date AS DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name FROM  TBL_Sales_History AS A INNER JOIN TBL_Customer AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type='I'  AND A.Creation_Date>='{1}' AND A.Creation_Date<='{2}' ORDER BY A.Orig_Sys_Document_Ref ASC";
        //        sQry1 = [sQry1 stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
        //        sQry1 = [sQry1 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        //        [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry1]];
        
        
        NSString *sQry2 =@"SELECT  A.Start_Time,A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' ORDER BY A.Orig_Sys_Document_Ref ASC";
        //        sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
        //        sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        
        NSLog(@"from date is %@", fromDate);
        
        
        NSString * tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
        NSString * tempToDate=[toDate stringByAppendingString:@" 23:59:59"];

        
        NSLog(@"to date is %@", toDate);
        
        sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
        sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
        
        
        
        
        [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry2]];
        
        
        
        
        
        
        
        
        
        
        
        //testing sqyery 3
        
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *components = [cal components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];
        
        [components setHour:-24];
        [components setMinute:0];
        [components setSecond:0];
        NSDate *today = [cal dateByAddingComponents:components toDate:[[NSDate alloc] init] options:0]; //This variable should now be pointing at a date object that is the start of today (midnight)
       // NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
        
        
        //this query is culprit which is not returning anything if to and from dates are same
        
        NSString *sQry3 =@"SELECT A.Start_Time, A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' ORDER BY A.Orig_Sys_Document_Ref ASC";
        
        NSLog(@"todate is %@", toDate);
      tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
      tempToDate=[toDate stringByAppendingString:@" 23:59:59"];

        
        //        sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",toDate]];
        //        sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        
        sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
        sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
        
        
        NSLog(@"sqry 3 is %@", sQry3);
        
        [dataArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry3]];
        
        NSLog(@"data from sqry3 %@", [dataArray description]);
        
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:FALSE];
        [dataArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        NSLog(@"test data array after sorting %@", [dataArray description]);
        

        
        
        [gridView reloadData];
        
    }
    else
    {
        NSString *sQry;
        if([custType isEqualToString:NSLocalizedString(@"Collection", nil)])
        {
            sQry =@"SELECT  A.Collected_On AS Start_Time, A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC ";
            
            
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
            
            
            //            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            //            sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
            
        }
        //                else if([custType isEqualToString:@"Invoice"])
        //                {
        //                    sQry =@" SELECT A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date AS DocDate, A.Transaction_Amt AS Amount,C.Customer_Name FROM  TBL_Sales_History AS A INNER JOIN TBL_Customer AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type='I'  AND A.Creation_Date>='{1}' AND A.Creation_Date<='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
        //                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
        //                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        //
        //                }
        else if([custType isEqualToString:@"Sales Returns"])
        {
            sQry =@"SELECT  A.Start_Time,A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name ,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY A.Orig_Sys_Document_Ref ASC";
            
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            sQry= [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
            
            
            //            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            //            sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
            
        }
        else if([custType isEqualToString:NSLocalizedString(@"Sales Order", nil)])
        {
            sQry =@"SELECT  A.Start_Time,A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name ,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' ORDER BY A.Orig_Sys_Document_Ref ASC";
            
            
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            sQry= [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
            
            
            
            //            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            //            sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        }
        
        //        sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Customer_ID"]];
        //        sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
        
        dataArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry];
        
        
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:FALSE];
        [dataArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        NSLog(@"test data array after sorting %@", [dataArray description]);
        
        

        
        [gridView reloadData];
    }
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    //datePickerPopOver=nil;
    customPopOver=nil;
    
    currencyTypeViewController=nil;
    customVC=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}


@end
