//
//  FOCView.h
//  SWCustomer
//
//  Created by msaad on 12/19/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "SWPlatform.h"



@interface CashFormView : SWViewController
{
    UIView *customerListView;
    UIView *formView;
    NSMutableDictionary *customer;
}



@property (nonatomic, strong)  UIView *customerListView;
@property (nonatomic, strong)  UIView *formView;
@property (nonatomic, strong)  NSMutableDictionary *customer;

- (id)initWithCustomer:(NSDictionary *)customerDict;

@end
