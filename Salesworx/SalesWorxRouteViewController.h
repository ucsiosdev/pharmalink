//
//  SalesWorxRouteViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "SWDatabaseManager.h"

#import <MapKit/MapKit.h>
#import "MapView2.h"
#import "Place2.h"
#import "PlaceMark.h"
#import <CoreLocation/CoreLocation.h>
#import "SWFoundation.h"
#import "SWPlatform.h"
#import "ZBarSDK.h"
#import "UIViewController+MJPopupViewController.h"
#import "SalesWorxCashCustomerOnsiteVisitOptionsViewController.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxVisitOptionsViewController.h"
#import "SalesWorxImageBarButtonItem.h"
#import "NSString+Additions.h"
#import "SalesWorxNearByCustomersViewController.h"
#import "SalesWorxRouteDetailsViewController.h"
#import "LocationFilterViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "OpenVisitAlertViewController.h"
#import "SalesWorxRouteRescheduleViewController.h"
#import "SalesWorxBackgroundSyncManager.h"
#import "AppControl.h"

@interface SalesWorxRouteViewController : UIViewController<UIPopoverControllerDelegate,CashCustomerOnsiteVisitOptionsDelegate,ZBarReaderDelegate,OpenVisitDelegate, routeRescheduleDelegate>
{
    AppControl *appControl;
    
    SalesWorxImageBarButtonItem* btnBarCode;
    SalesWorxImageBarButtonItem* btnStartVisit;
    
    NSDate *selecDate;
    UIPopoverController *datePickerPopOver;
    IBOutlet  MapView2 *customerDetailsMapView;
    Place2 *place;
    CSMapAnnotation2 *Annotation;
    ZBarReaderViewController *reader;
    IBOutlet SalesWorxImageView *statusImageView;
    SalesWorxVisit * salesWorxVisit;
    NSIndexPath * selectedIndexPath;
    BOOL isFurtureDate;
    NSMutableArray* indexPathArray;
    
    NSString* openVisitID;
    UIAlertView * openVisitAlert;
    NSString* openPlannedVisitID;
    NSString* openPlannedDetailID;
    
    IBOutlet MedRepElementTitleLabel *lastVisitedOnHeaderLabel;
    IBOutlet MedRepElementDescriptionLabel *lastVisitedDateLabel;
    
    
    SalesWorxRouteDetailsViewController *routeDetailsViewController;
    
    SalesWorxNearByCustomersViewController *nearByCustomersViewController;
    
    NSMutableArray* customersDataDicsArray;
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionBottomMarginConstraint;
    IBOutlet NSLayoutConstraint *NoSelectionLeadingMarginConstraint;
    IBOutlet MedRepElementDescriptionLabel *errorMessageLabel;
    
    BOOL isAppRefresh;
    NSInteger selectedReason;
    NSMutableArray * openVisitsArray;
    Customer* openVisitCustomer;
    NSMutableArray* customersArray;
    NSMutableArray* customerOrderHistoryArray;
    
    NSString *lastSynctimeStr;
    NSString *currentSyncStarttimeStr;
    
    BOOL isVisitStarted;
}
@property(strong,nonatomic) IBOutlet HMSegmentedControl *segmentControl;
@property (nonatomic, strong) IBOutlet UIScrollView *customersScrollView;
@property (strong, nonatomic) IBOutlet UIView *nearByCustomersView;
@property (strong, nonatomic) IBOutlet UIView *detailsView;


@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property(strong,nonatomic) Customer * selectedCustomer;

@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UIButton *btnMinusDate;
@property (weak, nonatomic) IBOutlet UIButton *btnPlusDate;
@property (weak, nonatomic) IBOutlet UITableView *customerTableView;




@end
