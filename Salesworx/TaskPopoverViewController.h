//
//  TaskPopoverViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/7/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TaskPopoverDelegate

-(void)updateTaskData;
@end

@interface TaskPopoverViewController : UIViewController
{
    IBOutlet  UITableView *taskTableView;
    NSMutableArray *taskArray;
    NSMutableArray *filteredTaskArray;
    
    IBOutlet UIView *TopContentView;
    UIImageView * blurredBgImage;
    
    NSMutableDictionary *previousFilterParameters;
    IBOutlet UIButton *filterButton;
    IBOutlet UISearchBar *taskSearchBar;
    BOOL isSearching;
}
@property (strong,nonatomic) id <TaskPopoverDelegate>TaskPopoverDelegate;

@end
