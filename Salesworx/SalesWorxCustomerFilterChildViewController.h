//
//  SalesWorxCustomerFilterChildViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxCustomerFilterChildViewController : UIViewController<UITableViewDataSource,UITableViewDataSource,UISearchBarDelegate>
{
    NSMutableArray * filteredCustomerListArray;
    NSMutableArray * tableRowValues;
    NSMutableArray * filteredTableRowValues;
    IBOutlet UISearchBar *tableSearchBar;
    IBOutlet UITableView *dataTableView;

}
@property (strong,nonatomic) NSMutableArray * customerListArray;
@property (strong,nonatomic) NSString * searchElementName;
@property (strong,nonatomic) NSString * titleString;


@end
