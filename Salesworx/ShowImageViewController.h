//
//  ShowImageViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImageViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(strong,nonatomic) NSString *imageName;
@property(strong,nonatomic) UIImage *image;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property(strong,nonatomic) UIPopoverController * datePickerPopOverController;

@end
