//
//  MedRepCalanderwithTimeViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/27/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "MedRepCalanderwithTimeViewController.h"

@interface MedRepCalanderwithTimeViewController ()

@end

@implementation MedRepCalanderwithTimeViewController
@synthesize visitTimePicker,calendarManager,minimumDate,disabledWeekDays,dateSelected,calendarMenuView,calendarContentView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getDisableWeekDays];
    
    calendarManager = [JTCalendarManager new];
    calendarManager.delegate = self;
    eventsByDate = [NSMutableDictionary new];
    
    // Generate random events sort by date using a dateformatter for the demonstration
    
    // Create a min and max date for limit the calendar, optional
    [self createMinAndMaxDate];
    
    [calendarManager setMenuView:calendarMenuView];
    [calendarManager setContentView:calendarContentView];
    [calendarManager setDate:[self fetchTodaysDate]];
    NSLog(@"calender minimum date is %@", calendarManager.date);
    
    
    // Do any additional setup after loading the view from its nib.
}
-(NSDate*)fetchTodaysDate
{
    
    NSDate* currentDate=[NSDate date];
    NSDateFormatter* systemDateFormat=[[NSDateFormatter alloc]init];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [systemDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [systemDateFormat setLocale:usLocaleq];
    [systemDateFormat setTimeZone:[NSTimeZone localTimeZone]];
    NSString* dateString=[systemDateFormat stringFromDate:currentDate];
    NSDate *refinedDate=[systemDateFormat dateFromString:dateString];
    
    NSLog(@"date sting is %@", refinedDate);
    NSLog(@"default sting is %@", currentDate);
    return refinedDate;
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDateTapped)];
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    
    saveBtn.tintColor=[UIColor whiteColor];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.rightBarButtonItem=saveBtn;
    UINavigationBar *nbar = self.navigationController.navigationBar;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
    
    [visitTimePicker setLocale: [NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    visitTimePicker.datePickerMode=UIDatePickerModeTime;
    visitTimePicker.minimumDate=[NSDate date];
    
    if((dateSelected!=nil && [self isDateHolidayDate:dateSelected]) ||
       dateSelected==nil){/** checking selected date with holiday dates. if holiday time picker date time set to current time*/
       
        if(dateSelected==nil){/** date not selected previously*/
            dateSelected=[self getNextWorkingDay];
            visitTimePicker.date=dateSelected;
        }else{/** user  prevoious selected date is holiday. so making it nil. User need to select the date again. */
            dateSelected=nil;
            visitTimePicker.date=[NSDate date];
        }

        
    }else{
        visitTimePicker.date=dateSelected;
    }
    [calendarManager reload];
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Visit Date"];
}
-(NSDate *)getNextWorkingDay{
    
    NSDate * nextWorkingDate;
    
    if(![self isDateHolidayDate:[NSDate date]]){/** checking todays date holiday status*/
        nextWorkingDate=[NSDate date];
    }else{
        for (NSInteger i=0; i<60; i++) {/** getting next immediate working day in next 60 days*/
          NSDate *tempDate= [calendarManager.dateHelper addToDate:[NSDate date] days:i];
            if(![self isDateHolidayDate:tempDate]){
                nextWorkingDate=tempDate;
                break;
            }
        }
    }
    return nextWorkingDate;
}
-(BOOL)isDateHolidayDate:(NSDate *)date{
    // NSLog(@"dates array is %@",datesArray);
    NSDateFormatter* systemDateFormat=[[NSDateFormatter alloc]init];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [systemDateFormat setDateFormat:kDatabseDefaultDateFormatWithoutTime];
    [systemDateFormat setLocale:usLocaleq];
    [systemDateFormat setTimeZone:[NSTimeZone localTimeZone]];
    NSString* dateString=[systemDateFormat stringFromDate:date];
    NSPredicate * datePredicate=[NSPredicate predicateWithFormat:@"SELF.holidayDate contains %@",dateString];
    NSMutableArray* tempArray=[[datesArray filteredArrayUsingPredicate:datePredicate] mutableCopy];
    if (tempArray.count>0) {
        return YES;
    }
    
    return NO;
}
-(void)saveDateTapped
{
    if (dateSelected != nil) {
        if ([self.calenderDelegate respondsToSelector:@selector(selectedDateDelegate:)]) {
            NSDateFormatter* visitDateFormatter=[[NSDateFormatter alloc]init];
            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [visitDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [visitDateFormatter setLocale:usLocaleq];
            [visitDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString* selectedDateString=[visitDateFormatter stringFromDate:dateSelected];
            
            NSString* refinedDateString=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDateString];
            
            
            NSDateFormatter *visitTimeFormatter = [[NSDateFormatter alloc] init];
            [visitTimeFormatter setDateFormat:@"HH:mm:ss"];
            [visitDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            
            NSString *visitTimeString = [visitTimeFormatter stringFromDate:visitTimePicker.date];
            
            NSString* refinedVisitString=[refinedDateString stringByAppendingString:[NSString stringWithFormat:@" %@",visitTimeString]];
            NSLog(@"visit date is %@", refinedVisitString);
            
            
            
            [self.calenderDelegate selectedDateDelegate:refinedVisitString];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    } else {
        [MedRepDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Please select date" withController:self];
    }
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Calender Control methods

- (void)createMinAndMaxDate
{
    todayDate = [self fetchTodaysDate];
    if (minimumDate == nil) {
        // Min date will be 2 month before today
        minDate = [calendarManager.dateHelper addToDate:[self fetchCalenderYearStartDate] months:0];
    }
    else
    {
        minDate = [calendarManager.dateHelper addToDate:minimumDate days:0];
    }
    // Max date will be 2 month after today
    NSDate * tempDate=[self fetchTodaysDate];
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM"];
    //[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];
    [dateFormatter setLocale:usLocale];
    NSString * formattedDate=[dateFormatter stringFromDate:tempDate];
    NSLog(@"current month number is %@",formattedDate);
    maxDate = [calendarManager.dateHelper addToDate:[self fetchCalenderYearStartDate] months:12];
}
-(NSDate*)fetchCalenderYearStartDate
{
    int yearOffset = 0;
    
    // Date representing right now
    NSDate *date = [NSDate dateWithTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT] sinceDate:[NSDate date]];
    
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents * calComponents = [cal components: NSCalendarUnitYear fromDate:date];
    
    [calComponents setYear:([calComponents year] + yearOffset)];
    
    
    NSDate *startOfYearWithOffset = [cal dateFromComponents:calComponents];
    
    
    return [NSDate dateWithTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT] sinceDate:startOfYearWithOffset];
}


#pragma mark - Buttons callback

- (IBAction)didGoTodayTouch
{
    [calendarManager setDate:todayDate];
}

- (IBAction)didChangeModeTouch
{
    calendarManager.settings.weekModeEnabled = !calendarManager.settings.weekModeEnabled;
    [calendarManager reload];
    
    CGFloat newHeight = 250;
    if(calendarManager.settings.weekModeEnabled){
        newHeight = 85.;
    }
    self.calendarContentViewHeight.constant = newHeight;
    [self.view layoutIfNeeded];
}

// get list of holiday dates coming from TBL_Holiday
-(void)getDisableWeekDays
{
    //adding holidays to disabled days
    NSMutableArray* holidaysArray=[MedRepQueries fetchHolidays];
    //NSLog(@"holiday dates are %@",holidaysArray);
    
    datesArray = [[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<holidaysArray.count; i++) {
        
        Holiday * currentHoliday=[[Holiday alloc]init];
        
        
        NSDateFormatter* holidayDateFormatter=[[NSDateFormatter alloc]init];
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [holidayDateFormatter setDateFormat:kDatabseDefaultDateFormat];
        [holidayDateFormatter setLocale:usLocaleq];
        [holidayDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate* holidayDate=[holidayDateFormatter dateFromString:[MedRepDefaults getValidStringValue:[holidaysArray objectAtIndex:i]]];
        NSCalendar* calender = [NSCalendar currentCalendar];
        NSDateComponents* component = [calender components:NSCalendarUnitWeekday fromDate:holidayDate];
        NSInteger weekDay = [component weekday];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSString *weekdayString = [[formatter weekdaySymbols] objectAtIndex:weekDay - 1];
        //NSLog(@"Day ::: %@", weekdayString);
        currentHoliday.holidayName=[MedRepDefaults getValidStringValue:weekdayString];
        
        currentHoliday.holidayDate=[MedRepDefaults getValidStringValue:[holidaysArray objectAtIndex:i]];
        
        
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:holidayDate];
        NSInteger weekDayCode = [comps weekday];
        // NSLog(@"The week day number: %ld", (long)weekDayCode);
        
        currentHoliday.holidayCode=[NSString stringWithFormat:@"%ld",(long)weekDayCode];
        
        [datesArray addObject:currentHoliday];
    }
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    disabledWeekDays=[[NSMutableArray alloc] init];
    BOOL isHiolidayDate=NO;
    
    
   // NSLog(@"dates array is %@",datesArray);
    NSDateFormatter* systemDateFormat=[[NSDateFormatter alloc]init];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [systemDateFormat setDateFormat:kDatabseDefaultDateFormatWithoutTime];
    [systemDateFormat setLocale:usLocaleq];
    [systemDateFormat setTimeZone:[NSTimeZone localTimeZone]];
    NSString* dateString=[systemDateFormat stringFromDate:dayView.date];
    NSPredicate * datePredicate=[NSPredicate predicateWithFormat:@"SELF.holidayDate contains %@",dateString];
    NSMutableArray* tempArray=[[datesArray filteredArrayUsingPredicate:datePredicate] mutableCopy];
    if (tempArray.count>0) {
        Holiday * refinedHoliday= [tempArray objectAtIndex:0];
        isHiolidayDate=YES;
    }
    /*
    NSMutableArray * tempDisabledWeekDaysArray = [MedRepDefaults fetchDaystoDisable:datesArray];
    
    NSLog(@"temp disable days array is %@",tempDisabledWeekDaysArray);
    NSLog(@"dates  array is %@",datesArray);
    
    NSDateFormatter* systemDateFormat=[[NSDateFormatter alloc]init];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [systemDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [systemDateFormat setLocale:usLocaleq];
    [systemDateFormat setTimeZone:[NSTimeZone localTimeZone]];
    NSString* dateString=[systemDateFormat stringFromDate:dayView.date];
    NSDate *refinedDate=[systemDateFormat dateFromString:dateString];
    
    NSMutableArray* tempDisableArray=[[NSMutableArray alloc]init];
    NSPredicate * datePredicate=[NSPredicate predicateWithFormat:@"SELF contains %@",dateString];
    NSMutableArray* tempArray=[[holidaysArray filteredArrayUsingPredicate:datePredicate] mutableCopy];
    if (tempArray.count>0) {
        NSLog(@"temp array is %@",tempArray);

        NSLog(@"test log response %@",[MedRepDefaults fetchDaystoDisable:tempArray]);
        
        [disabledWeekDays addObjectsFromArray:[MedRepDefaults fetchDaystoDisable:tempArray]];
    }
    
    NSLog(@"temp disable array is %@",disabledWeekDays);
    
     */
    
    NSDate* xDate = dayView.date;
    
    NSInteger dayInt = [[[NSCalendar currentCalendar] components: NSCalendarUnitWeekday fromDate: xDate] weekday];
    //    // Today
    //    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
    //        dayView.circleView.hidden = NO;
    //        dayView.circleView.backgroundColor = [UIColor blueColor];
    //        dayView.dotView.backgroundColor = [UIColor whiteColor];
    //        dayView.textLabel.textColor = [UIColor whiteColor];
    //    }
    //    //Fiday
    //    else

    if ([disabledWeekDays containsObject:[NSString stringWithFormat:@"%ld",(long)dayInt]] || [xDate compare:minDate] == NSOrderedAscending || isHiolidayDate)
    {
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor =[UIColor clearColor] ;
        dayView.textLabel.hidden = NO;
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Selected date
    else if(dateSelected && [calendarManager.dateHelper date:dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.textLabel.hidden = NO;
        
        dayView.circleView.backgroundColor = [UIColor colorWithRed:(61.0/255.0) green:(176.0/255.0) blue:(251.0/255.0) alpha:1.0];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    /*else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
     dayView.circleView.hidden = YES;
     dayView.textLabel.hidden = YES;
     }*/
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [UIColor colorWithRed:(61.0/255.0) green:(176.0/255.0) blue:(251.0/255.0) alpha:1.0];
        dayView.textLabel.hidden = NO;
        
    }
    
    
    if(![calendarManager.dateHelper date:calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.hidden = YES;;
        dayView.textLabel.hidden = YES;
    }
    dayView.dotView.hidden = YES;
    
    
    //    if([self haveEventForDay:dayView.date]){
    //        dayView.dotView.hidden = NO;
    //    }
    //    else{
    //        dayView.dotView.hidden = YES;
    //    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    NSLog(@"date view date is %@",dayView.date);
    
    disabledWeekDays=[[NSMutableArray alloc] init];
    
 
    NSDateFormatter* systemDateFormat=[[NSDateFormatter alloc]init];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [systemDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [systemDateFormat setLocale:usLocaleq];
    [systemDateFormat setTimeZone:[NSTimeZone localTimeZone]];
    NSString* dateString=[systemDateFormat stringFromDate:dayView.date];
    NSPredicate * datePredicate=[NSPredicate predicateWithFormat:@"SELF.holidayDate contains %@",dateString];
    NSMutableArray* tempArray=[[datesArray filteredArrayUsingPredicate:datePredicate] mutableCopy];
    if (tempArray.count>0) {
        //NSLog(@"temp array is %@", tempArray);
        Holiday * refinedHoliday= [tempArray objectAtIndex:0];
        
        [disabledWeekDays addObject:refinedHoliday.holidayCode];
        
    }
    

    
    NSDate* xDate = dayView.date;
    NSInteger dayInt = [[[NSCalendar currentCalendar] components: NSCalendarUnitWeekday fromDate: xDate] weekday];
    //Fiday
    
    //    BOOL dateisPast=NO;
    //    //this disbales previous  package dates
    //    NSDate *now = [NSDate date];
    //    if ([now compare:xDate] == NSOrderedDescending) {
    //        // muteOverDate in the past
    //        NSLog(@"date is past %@", xDate);
    //        dateisPast=YES;
    //    }
    
    
    if ([disabledWeekDays containsObject:[NSString stringWithFormat:@"%ld",(long)dayInt]] || [xDate compare:minDate] == NSOrderedAscending)
    {
        
    }
    else
    {
        dateSelected = dayView.date;
        
        // Animation for the circleView
        dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
        [UIView transitionWithView:dayView
                          duration:.3
                           options:0
                        animations:^{
                            dayView.circleView.transform = CGAffineTransformIdentity;
                            [calendarManager reload];
                        } completion:nil];
        
        
        if([dateSelected compare:[NSDate date]] == NSOrderedAscending){/*user selects the current date which gives  begining of the current date . so selecing the current time which is mimum time for dte picker.*/
            [visitTimePicker setDate:[NSDate date]];
        }else{
            [visitTimePicker setDate:dateSelected];

        }
        

        // Don't change page in week mode because block the selection of days in first and last weeks of the month
        if(calendarManager.settings.weekModeEnabled){
            return;
        }
        
        // Load the previous or next page if touch a day from another month
        
        if(![calendarManager.dateHelper date:calendarContentView.date isTheSameMonthThan:dayView.date]){
            if([calendarContentView.date compare:dayView.date] == NSOrderedAscending){
                [calendarContentView loadNextPageWithAnimation];
            }
            else{
                [calendarContentView loadPreviousPageWithAnimation];
            }
        }
        NSLog(@"date selected is %@",dateSelected);
        if(calenderDelegate!=nil)
        {
            NSLog(@"check date format %@", xDate);
            
        }
        
    }
    
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [calendarManager.dateHelper date:date isEqualOrAfter:minDate andEqualOrBefore:maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Previous page loaded");
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
