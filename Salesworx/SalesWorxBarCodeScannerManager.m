//
//  SalesWorxBarCodeScannerManager.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/22/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxBarCodeScannerManager.h"
#import <AVFoundation/AVFoundation.h>

@implementation SalesWorxBarCodeScannerManager
@synthesize swxBCMdelegate;
-(void)ScanBarCode
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self scanwithZBar];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self scanwithZBar];
                 });
                 
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Camera not accessible" withController:swxBCMdelegate];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        // My own Helper class is used here to pop a dialog in one simple line.
        [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:KCameraAccessRestrictAlertMessage withController:swxBCMdelegate];
    }
    else
    {
        [self camDenied];
    }
}
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    NSString *alertText;
    NSString *alertButton;
    alertText = KCameraAccessRefuseAlertMessage;
    BOOL canOpenSettings =  (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertButton = @"Go";
    }
    else
    {
        alertButton = @"OK";
    }
    
    UIAlertAction* noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(alertButton, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   BOOL canOpenSettings =(UIApplicationOpenSettingsURLString != NULL);
                                   if (canOpenSettings)
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:noAction ,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Alert" andMessage:alertText andActions:actionsArray withController:swxBCMdelegate];
    
}
- (void)BarCodeScannerButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:^{
        
    }];
}
- (CGRect)getScanCrop:(CGRect)rect readerViewBounds:(CGRect)rvBounds{
    CGFloat x,y,width,height;
    x = rect.origin.y / rvBounds.size.height;
    y = 1 - (rect.origin.x + rect.size.width) / rvBounds.size.width;
    width = rect.size.height / rvBounds.size.height;
    height = rect.size.width / rvBounds.size.width;
    return CGRectMake(x, y, width, height);
}
-(void)scanwithZBar
{
    
    
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50,  [UIScreen mainScreen].bounds.size.width, 50)] ;
    customOverlay.backgroundColor = [UIColor grayColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
    toolbar.frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, 50);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    toolbar.backgroundColor=[UIColor clearColor];
    
    // initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(barButtonBackPressed:)
    UIButton * cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0, 80, 50)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton addTarget:swxBCMdelegate action:@selector(BarCodeScannerButtonBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [customOverlay addSubview:cancelButton];
    
    
    
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = swxBCMdelegate;
    reader.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    reader.cameraOverlayView =customOverlay ;
    reader.showsZBarControls=YES;
    
  //  reader.scanCrop = [self getScanCrop:CGRectMake(reader.view.bounds.size.width- 100, reader.view.bounds.size.width-100, 200, 200) readerViewBounds:reader.view.bounds];

    
    
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_EAN2 config: ZBAR_CFG_ENABLE to: 1];
    
    [scanner setSymbology: ZBAR_EAN5
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    [scanner setSymbology: ZBAR_EAN8
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    
    [scanner setSymbology: ZBAR_UPCE
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_ISBN10
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_UPCA
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_ISBN13
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_EAN13
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_COMPOSITE
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_DATABAR
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_DATABAR_EXP
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_CODE39
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_PDF417
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_QRCODE
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_CODE93
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    
    [scanner setSymbology: ZBAR_CODE128
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    [swxBCMdelegate presentViewController:reader animated:YES completion:^{
        
    }];
}

@end
