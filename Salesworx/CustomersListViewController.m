//
//  SWCustomersListViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "CustomersListViewController.h"
#import "SWSection.h"
#import "LocationFilterViewController.h"
#import "SWCustomerListCell.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "SWDefaults.h"
#import "NSObject+NullDictionary.h"
#import "SWCustomerDetailViewController.h"



#define kNavigationFilterLocationButton 1
#define kNavigationFilterCategoryButton 2

@interface CustomersListViewController (){
   SWCustomerDetailViewController *customerDetailViewController ;
}
- (void)locationFilter:(id)sender;
- (void)locationFilterSelected;
- (void)setupToolbar;
- (void)executeSearch:(NSString *)term;
@end

@implementation CustomersListViewController

@synthesize target;
@synthesize action;

- (id)init {
    self = [super init];
    
    if (self) {
        [self setTitle:NSLocalizedString(@"Customers", nil)];
        
        searchData=[NSMutableArray array];
    }
    
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    

    [super viewDidLoad];

    loadingView=nil;
    loadingView=[[SWLoadingView alloc] initWithFrame:self.view.bounds];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    // table view
    tableView=nil;
    tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,44.0, self.view.bounds.size.width, self.view.bounds.size.height-44) style:UITableViewStylePlain] ;
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    [tableView setBackgroundView:bgView];
    
    
    // search display controller
    searchBar=nil;
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0,1024, 44.0f)] ;
    searchController=nil;
    searchController=[[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self] ;
     //[searchController.searchBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    
    [searchController setDelegate:self];
	[searchController setSearchResultsDataSource:self];
    [searchController setSearchResultsDelegate:self];
    [searchController.searchResultsTableView setDelegate:self];
    
    [self.view addSubview:searchBar];
    [self.view addSubview:tableView];
    [self.view addSubview:loadingView];
    // popovers
    LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
    [locationFilterController setTarget:self];
    [locationFilterController setAction:@selector(locationFilterSelected)];
    [locationFilterController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:locationFilterController] ;
    locationFilterPopOver=nil;
    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    locationFilterPopOver.delegate=self;
    // Count label
    infoLabel=nil;
    infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 25)];
    [infoLabel setText:@""];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:BoldSemiFontOfSize(14.0f)];

    
    flexibleSpace=nil;
    totalLabelButton=nil;
    displayActionBarButton=nil;
    displayMapBarButton=nil;
    
    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    totalLabelButton = [[UIBarButtonItem alloc] init];
    totalLabelButton = [UIBarButtonItem labelButtonWithLabel:infoLabel];
    displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_filterImage"] style:UIBarButtonItemStylePlain target:self action:@selector(locationFilter:)] ;
    displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_barcode"] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
    
    [self navigatetodeals];

    [self setupToolbar];

    
    

    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [SWDefaults clearCustomer];
    
    self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
    self.navigationController.toolbar.translucent = NO;
    
    NSLog(@"customer list called");
    
//    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//    [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    //iOS 9 UITableView cell frame getting reset issue
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
        searchController.searchResultsTableView.cellLayoutMarginsFollowReadableWidth=NO;
        
    }

    

}
-(void)navigatetodeals
{
    
    //customerSer.delegate = self;
    
    
    
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

}
- (void)setupToolbar {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }
    NSString *text = [NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"Total Customers", nil), customerList.count];
    
    if ([[SWDefaults locationFilterForCustomerList] length] > 0) {
        text = [NSString stringWithFormat:@"%@ (filtered by: %@)", text, [SWDefaults locationFilterForCustomerList]];
    }
    
    [infoLabel setText:text];
    
    if (!self.navigationController.toolbarHidden) {
        return;
    }
    
    [totalLabelButton setTitle:[infoLabel text]];
    [self setToolbarItems:nil];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton, flexibleSpace, nil]];
    
    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    
    //[self.navigationItem setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithCustomView:buttonsView]  animated:YES];
}

- (void)displayMap:(id)sender {
    

    Singleton *single = [Singleton retrieveSingleton];
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    //reader.showsZBarControls = Y;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)] ;
    customOverlay.backgroundColor = [UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
    toolbar.frame = CGRectMake(0,0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    
    //reader.wantsFullScreenLayout = NO;

    reader.readerView.zoom = 1.0;
    
    reader.showsZBarControls=NO;

    [self presentViewController: reader animated: YES completion:nil];
    
    
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) readers
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    
    
    
    
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    
    for(symbol in results){
        single.valueBarCode=symbol.data;
        // NSString *upcString = symbol.data;
        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
        [locationFilterController setTarget:self];
        [locationFilterController setAction:@selector(filterChangedBarCode)];
        [locationFilterController selectionDone:self];
        
        [reader dismissViewControllerAnimated: YES completion:nil];
        
        single.isBarCode = NO;
    }
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    
    
    tableView.backgroundColor=[UIColor whiteColor];

    
    if (self.target && [self.target respondsToSelector:self.action]) {
        UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];

        [self.navigationItem setLeftBarButtonItem:logoutButton ];
        [self setTitle:NSLocalizedString(@"Select Customer", nil)];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{

    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}



- (void)locationFilter:(id)sender
{
    [locationFilterPopOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


- (void)filterChangedBarCode {

    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:YES animated:YES];
   // [self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;

    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollectionFromReports]];

}
- (void)locationFilterSelected {
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    [[SWDatabaseManager retrieveManager] cancel];
    
    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:YES animated:YES];
    //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;

    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollectionFromReports]];

}
- (void)executeSearch:(NSString *)term
{
    
    Singleton *single = [Singleton retrieveSingleton];
    single.customerIndexPath = 0;
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
    
    [searchData removeAllObjects];
    NSDictionary *element=[NSDictionary dictionary];
    for(element in customerList)
    {
        NSString *customerName = [element objectForKey:@"Customer_Name"];
        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        if (r.length > 0)
        {
            [searchData addObject:element];
        }
    }
    sectionSearch=nil;
    sectionSearch = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    // Loop through the books and create our keys
    for (NSDictionary *book in searchData)
    {
        NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [sectionSearch allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            
            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the books into their respective keys
    for (NSDictionary *book in searchData)
    {
        [[sectionSearch objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
    }
    
    // Sort each section array
    for (NSString *key in [sectionSearch allKeys])
    {
        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
    }
    
    
    
    
}

//- (void)executeSearch:(NSString *)term
//{
//    [searchData removeAllObjects];
//    NSDictionary *element=[NSDictionary dictionary];
//    for(element in customerList)
//    {
//        NSString *customerName = [element objectForKey:@"Customer_Name"];
//        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
//        if (r.length > 0)
//        {
//            [searchData addObject:element];
//        }
//	}
//    sectionSearch=nil;
//    sectionSearch = [[NSMutableDictionary alloc] init];
//    
//    BOOL found;
//    
//    // Loop through the books and create our keys
//    for (NSDictionary *book in searchData)
//    {
//        NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
//        
//        found = NO;
//        
//        for (NSString *str in [sectionSearch allKeys])
//        {
//            if ([str isEqualToString:c])
//            {
//                found = YES;
//            }
//        }
//        
//        if (!found)
//        {
//            
//            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
//        }
//    }
//    
//    // Loop again and sort the books into their respective keys
//    for (NSDictionary *book in searchData)
//    {
//        [[sectionSearch objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
//    }
//    
//    // Sort each section array
//    for (NSString *key in [sectionSearch allKeys])
//    {
//        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
//    }
//
//}

- (void)cancel:(id)sender {
    if (self.target && [self.target respondsToSelector:self.action])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:[NSDictionary dictionary]];
#pragma clang diagnostic pop
        return;
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)clearFilter {
    [SWDefaults setLocationFilterForCustomerList:nil];
    [SWDefaults setPaymentFilterForCustomerList:nil];
    [SWDefaults setStatusFilterForCustomerList:nil];
    [SWDefaults setNameFilterForCustomerList:nil];
    [SWDefaults setCodeFilterForCustomerList:nil];
    [SWDefaults setbarCodeFilterForCustomerList:nil];
    if ([[SWDefaults filterForCustomerList]isEqualToString:@""] || [SWDefaults filterForCustomerList].length == 0 || [SWDefaults filterForCustomerList] == nil) {
         #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;
        
    }
    else {
        [SWDefaults setFilterForCustomerList:nil];
         #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;
        
    }
}
#pragma mark Customer List service delegate
-(void)getListFunction:(NSArray *)temp
{
    [loadingView setHidden:YES];
    customerList = [NSMutableArray arrayWithArray:temp ] ;
    
    if([[SWDefaults filterForCustomerList] isEqualToString:@"BarCode"] && [temp count]==0)
    {
        [reader dismissViewControllerAnimated: YES completion:nil];
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"No customer found."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:@"No customer found." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
        [ErrorAlert show];
        
        [self clearFilter];
        //customerSer.delegate = self;
        [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

        return;
    }
    
    if(![customerList count]==0)
    {
        if ([searchController isActive])
        {
            [self executeSearch:self.searchDisplayController.searchBar.text];
            [searchController.searchResultsTableView reloadData];
        }
        else
        {
            sectionList=nil;
            sectionList = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            @autoreleasepool {
                
                for (NSDictionary *book in customerList)
                {
                    NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
                    
                    found = NO;
                    
                    for (NSString *str in [sectionList allKeys])
                    {
                        if ([str isEqualToString:c])
                        {
                            found = YES;
                        }
                    }
                    
                    if (!found)
                    {
                        [sectionList setValue:[[NSMutableArray alloc] init] forKey:c];
                    }
                }
            }
            
            // Loop again and sort the books into their respective keys
            @autoreleasepool {
                
                
                for (NSDictionary *book in customerList)
                {
                    [[sectionList objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
                }
            }
            // Sort each section array
            @autoreleasepool {
                
                
                for (NSString *key in [sectionList allKeys])
                {
                    [[sectionList objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
                }
            }
        }
    }
    else
    {
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"No customer found."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:@"No customer found." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
        [ErrorAlert show];
    }
    totalRecords=customerList.count;
    [tableView reloadData];
    temp=nil;

}
- (void)customerServiceDidGetList:(NSArray *)cl {
    
    
}

#pragma mark UISearchDisplay Controller delegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self executeSearch:searchString];
    return YES;
}

#pragma mark UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv
{
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        return [[sectionSearch allKeys] count];
    }
    return [[sectionList allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)s
{
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
//        return [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
        
                return [(NSArray*)[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
    }
   // return [customerList count];
       return [(NSArray*)[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Identifier = @"CUSTOMERLISTCELL";
    SWCustomerListCell *cell = [tv dequeueReusableCellWithIdentifier:Identifier];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

   
    
    // [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    cell.accessoryType=UITableViewCellAccessoryNone;
    
    
    if (cell == nil)
    {
        cell = [[SWCustomerListCell alloc] initWithReuseIdentifier:Identifier] ;
        //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
   
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        [cell applyAttributes:[[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    else
    {
        [cell applyAttributes:[[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    
//    
//    tableView.backgroundColor=[UIColor clearColor];
//    
//    NSLog(@"clearing color");
//    
//    cell.backgroundColor=[UIColor clearColor];
//    
//    cell.contentView.backgroundColor=[UIColor clearColor];
    
   // cell background color
    
    
//    UIView *selectionColor = [[UIView alloc] init];
////   selectionColor.backgroundColor = [UIColor colorWithRed:223 green:232 blue:244 alpha:1.0];
//    selectionColor.backgroundColor = [UIColor darkGrayColor];
  
    
    //cell.selectedBackgroundView = selectionColor;
    
    
  

    return cell;
}

- (NSString *)tableView:(UITableView *)tableViews titleForHeaderInSection:(NSInteger)section
{
    if(tableViews == self.searchDisplayController.searchResultsTableView)
    {
        return [[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];

    }
    return [[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    return 60.0f;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tv
{
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        return [[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
    }
    return [[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    AppControl *appControl = [AppControl retrieveSingleton];
    
    
    NSString *Identifier = @"CUSTOMERLISTCELL";
    
    SWCustomerListCell *cell = [tv dequeueReusableCellWithIdentifier:Identifier];
    
    
    
   if ([appControl.CUST_DTL_SCREEN isEqualToString:@"DASHBOARD"]) {
//        
//        
//        AlSeerCustomerDashboardViewController* customerDash=[[AlSeerCustomerDashboardViewController alloc]init];
//        
//        if(tv == self.searchDisplayController.searchResultsTableView)
//        {
//            
//            NSDictionary *row = [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
//            
//            
//        
//        
//        customerDict =[NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithDictionary:row   ]];
//
//    
//        }
//        
//        else
//        {
//            NSDictionary *row = [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
//
//            customerDict=[NSMutableDictionary dictionaryWithDictionary:[row mutableCopy]];
//
//        }
//        
//        NSLog(@"custom dict before dashboard %@", [customerDict description]);
//        
//        customerDash.customerDetailsDictionary=customerDict;
//        
//        
//        [self.navigationController pushViewController:customerDash animated:YES];
    
    
    //customerSer.delegate = self;
    Singleton *single = [Singleton retrieveSingleton];
    single.customerIndexPath = indexPath.row;
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
    
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        
        NSDictionary *row = [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        row=[NSMutableDictionary nullFreeDictionaryWithDictionary:row];

        if (self.target && [self.target respondsToSelector:self.action]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop
            
            //[self dismissViewControllerAnimated:YES completion:nil];
            
            return;
        }
        customerDict =[NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithDictionary:row   ]];
        
        
        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]]];
        
        
    } else
    {
        NSDictionary *row = [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        // NSDictionary *row = [self.customerList objectAtIndex:indexPath.row];
        
        row=[NSMutableDictionary nullFreeDictionaryWithDictionary:row];
        
        
       // NSLog(@"row is %@", row);
        
        
        if (self.target && [self.target respondsToSelector:self.action])
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop
            return;
        }
        
        customerDict=[NSMutableDictionary dictionaryWithDictionary:[row mutableCopy]];
        
        
        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]]];
        
    } 
        
    }
    
    
    else
        
    {
    
       
    //customerSer.delegate = self;
   Singleton *single = [Singleton retrieveSingleton];
    single.customerIndexPath = indexPath.row;
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
    
    if(tv == self.searchDisplayController.searchResultsTableView)
    {
        
        NSDictionary *row = [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        if (self.target && [self.target respondsToSelector:self.action]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop
            
            //[self dismissViewControllerAnimated:YES completion:nil];
            
            return;
        }
        customerDict =[NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithDictionary:row   ]];


        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]]];
        
        
    } else
    {
        NSDictionary *row = [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        // NSDictionary *row = [self.customerList objectAtIndex:indexPath.row];
        if (self.target && [self.target respondsToSelector:self.action])
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop
            return;
        }
        
        customerDict=[NSMutableDictionary dictionaryWithDictionary:[row mutableCopy]];

        
        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]]];
        
    }
}

}
//- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
    
    [SWDefaults setCustomer:customerDict];

    customerDict = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults validateAvailableBalance:orderAmmount]];
    customerDetailViewController=nil;
    customerDetailViewController = [[SWCustomerDetailViewController alloc] initWithCustomer:customerDict] ;
    [self.navigationController pushViewController:customerDetailViewController animated:YES];
   // [self performSelector:@selector(pushCustomerDetailPage) withObject:nil afterDelay:0.0];
    
    orderAmmount=nil;
}

-(void)pushCustomerDetailPage {
    if (customerDetailViewController) {
        NSLocale* currentLoc = [NSLocale currentLocale];
        NSLog(@"***");
        NSLog(@"Push %@",[[NSDate date] descriptionWithLocale:currentLoc]);
        
        [customerDetailViewController view];
        customerDetailViewController=nil;
    }
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    [cell.textLabel setFont:LightFontOfSize(26.0f)];
//    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
