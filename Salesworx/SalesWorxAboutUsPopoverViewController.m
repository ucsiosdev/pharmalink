//
//  SalesWorxAboutUsPopoverViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 12/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxAboutUsPopoverViewController.h"
#import "MedRepDefaults.h"
#import "SWPlatform.h"
#import "SSKeychain.h"
#import "SalesWorxAboutUsTableViewCell.h"

@interface SalesWorxAboutUsPopoverViewController ()

@end

@implementation SalesWorxAboutUsPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    lblCustomerID.text = [self maskCustomerID:[SWDefaults licenseIDForInfo]];
    lblUser.text = [[SWDefaults userProfile] stringForKey:@"Username"];
    lblDeviceID.text = [SSKeychain passwordForService:@"UCS_Salesworx" account:@"user"];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict stringForKey:@"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    lblVersionNo.text= [NSString stringWithFormat:@"Version %@ (%@)",avid,build];
    
    arrAboutOptions = [[NSMutableArray alloc]initWithObjects:@"Rate this App", @"Follow on Twitter", nil];
}

-(NSString *)maskCustomerID:(NSString *)customerID
{
    NSString *maskedCustomerID;
    for (int i = 3; i < customerID.length; i++) {
        
        if (![[[SWDefaults licenseIDForInfo] substringWithRange:NSMakeRange(i, 1)] isEqual:@" "]) {
            
            NSRange range = NSMakeRange(i, 1);
            customerID = [customerID stringByReplacingCharactersInRange:range withString:@"X"];
            maskedCustomerID = customerID;
            
        }
        
    }
    return maskedCustomerID;
}

-(void)viewWillAppear:(BOOL)animated
{
    if(self.isMovingToParentViewController)
    {
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
    }
}

- (IBAction)btnDone:(id)sender {
    [self dimissViewControllerWithSlideDownAnimation:YES];
}


#pragma UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAboutOptions.count;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"idntAboutUs";
    SalesWorxAboutUsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxAboutUsTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.lblCell.text = NSLocalizedString([arrAboutOptions objectAtIndex:indexPath.row], nil);

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strURL;
    if (indexPath.row == 0){
        strURL = @"itms-apps://itunes.apple.com/app/id654722282";//itms-apps://itunes.apple.com/app/id654722282
    } else if (indexPath.row == 1){
        strURL = @"https://twitter.com/ucssolutions/";
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
