//
//  CustomPopOverBackgroundView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/5/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "CustomPopOverBackgroundView.h"

@implementation CustomPopOverBackgroundView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
     Drawing code
}
*/



-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
       
        
        CALayer *layer = self.layer;
        layer.shadowOffset = CGSizeMake(1, 1);
        layer.shadowColor = [[UIColor redColor] CGColor];
        layer.shadowRadius = 2.0f;
        layer.shadowOpacity = 0.50f;
        layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];

        
    }
    return self;
}


@end
