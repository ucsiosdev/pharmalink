//
//  SalesWorxProductsDemonstratedViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 11/15/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxProductsDemonstratedViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxDateHelper.h"
@interface SalesWorxProductsDemonstratedViewController ()
{
    SalesWorxDateHelper * dateHelper;
}
@end

@implementation SalesWorxProductsDemonstratedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     dateHelper=[[SalesWorxDateHelper alloc] init];
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kEDetailingReportTitle localizedCapitalizedString], nil)];
    
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    [DateRangeTextField SetCustomDateRangeText];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];

    arrLocationName = [[NSMutableArray alloc]init];
    arrLocationDetails = [[NSMutableArray alloc]init];
    
    NSMutableArray *locationArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select * from PHX_TBL_Locations order by Location_Name asc"];

    for (NSMutableDictionary *currentDict in locationArray)
    {
        NSMutableDictionary *currentLocationDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
        [arrLocationDetails addObject:currentLocationDict];
        [arrLocationName addObject: [currentDict valueForKey:@"Location_Name"]];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            NSMutableArray *productArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select A.Product_ID, date(A.Discussed_At) AS Discussed_At , A.Location_ID, B.Product_Name from PHX_TBL_EDetailing As A inner join PHX_TBL_Products As B where  B.Product_ID = A.Product_ID"];
            NSMutableArray *arrProduct = [[NSMutableArray alloc]init];
            if (productArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary * currentDict in productArray)
                {
                    NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    [arrProduct addObject:currentCustDict];
                }
            }
            unfilteredProductArray = arrProduct;
            
            
            // fetch Samples Given data
            
            NSMutableArray *samplesGivenArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT B.Issue_Qty, date(A.End_Time) as Discussed_At , A.Location_ID, C.Product_Name FROM PHX_TBL_Issue_Note As A inner join PHX_TBL_Issue_Note_Line_Items As B inner join PHX_TBL_Products As C where A.Doc_No = B.Doc_No and B.Product_ID = C.Product_ID"];
            NSMutableArray *arrSamplesGiven = [[NSMutableArray alloc]init];
            if (samplesGivenArray.count>0) {
                //doing fast enumeration and modifying the dict so added __strong
                for (NSMutableDictionary * currentDict in samplesGivenArray)
                {
                    NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                    [arrSamplesGiven addObject:currentCustDict];
                }
            }
            unfilteredSamplesGivenArray = arrSamplesGiven;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
            });
        });

        
        [self prepareSegmentControl];
        [self prepareChildViewControllers];
    }
}

-(void)prepareSegmentControl
{
    segmentControl.titleTextAttributes = [SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    segmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    segmentControl.sectionTitles = @[NSLocalizedString(@"Products Demonstrated", nil), NSLocalizedString(@"Samples Given", nil)];
    
    segmentControl.selectionIndicatorBoxOpacity=0.0f;
    segmentControl.verticalDividerEnabled=YES;
    segmentControl.verticalDividerWidth=1.0f;
    segmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    segmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    segmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentControl.tag = 3;
    segmentControl.selectedSegmentIndex = 0;
    
    segmentControl.layer.shadowColor = [UIColor blackColor].CGColor;
    segmentControl.layer.shadowOffset = CGSizeMake(2, 2);
    segmentControl.layer.shadowOpacity = 0.1;
    segmentControl.layer.shadowRadius = 1.0;
    
    [segmentControl setIndexChangeBlock:^(NSInteger index) {
        [eDetailingScrollView setContentOffset:CGPointMake(eDetailingScrollView.frame.size.width * index, 0)];
    }];
}

#pragma mark Child Viewcontrollers

-(void)prepareChildViewControllers
{
    [eDetailingScrollView setPagingEnabled:YES];
    
    productsDetailsViewController = [[SalesworxProductDetailsViewController alloc] initWithNibName:@"SalesworxProductDetailsViewController" bundle:[NSBundle mainBundle]];
    productsSampleViewController = [[SalesworxProductSamplesGivenViewController alloc] initWithNibName:@"SalesworxProductSamplesGivenViewController" bundle:[NSBundle mainBundle]];
    
    productsDetailsViewController.view.frame=CGRectMake(0, 0, eDetailingScrollView.frame.size.width, eDetailingScrollView.frame.size.height);
    productsSampleViewController.view.frame=CGRectMake(eDetailingScrollView.frame.size.width, 0, eDetailingScrollView.frame.size.width, eDetailingScrollView.frame.size.height);
    
    [eDetailingScrollView addSubview:productsDetailsViewController.view];
    [eDetailingScrollView addSubview:productsSampleViewController.view];
    
    [self addChildViewController:productsDetailsViewController];
    [self addChildViewController:productsSampleViewController];
}


#pragma mark UIScrollView methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView == eDetailingScrollView){
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        [segmentControl setSelectedSegmentIndex:page animated:YES];
    }
}

#pragma  mark textfield methods
-(BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{
    if(textField == txtLocation)
    {
        [self textFieldTapped];
        return NO;
    }
    else if(textField == DateRangeTextField)
    {
        SalesWorxReportsDateSelectorViewController * popOverVC=[[SalesWorxReportsDateSelectorViewController alloc]init];
        popOverVC.delegate=self;
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(450, 570);
        popover.delegate = self;
        popover.sourceView = DateRangeTextField.rightView;
        popover.sourceRect = DateRangeTextField.dropdownImageView.frame;
        popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
        return NO;
    }
    return YES;
}

#pragma  mark textfield tapped method

-(void)textFieldTapped
{
    txtLocation.enabled = YES;
    
    SalesWorxPopOverViewController *popOverVC = [[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = arrLocationName;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=NO;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(400, 273);
    popover.delegate = self;
    popOverVC.titleKey = @"Location";
    popover.sourceView = txtLocation.rightView;
    popover.sourceRect =txtLocation.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    [self presentViewController:nav animated:YES completion:^{
    }];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    txtLocation.text = [NSString stringWithFormat:@"%@",[arrLocationName objectAtIndex:selectedIndexPath.row]];
}
#pragma  mark button tapped method

- (IBAction)btnSearch:(id)sender {
    NSMutableArray *predicateArray = [NSMutableArray array];

    if (![txtLocation.text isEqualToString:@"N/A"]) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Location_Name contains[cd] %@",txtLocation.text];
        NSMutableArray *filteredArray = [[arrLocationDetails filteredArrayUsingPredicate:predicate] mutableCopy];
    
        if ([filteredArray count] > 0) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"Location_ID == %@",[[filteredArray objectAtIndex:0]valueForKey:@"Location_ID"]]];
        }
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select Location" withController:self];
        return;
    }
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"(Discussed_At >= %@) AND (Discussed_At <= %@)", [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate] , [MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]]];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [predicate description]);
    
    
    // filter demonstrated product data
    NSMutableArray* filteredProductArray = [[unfilteredProductArray filteredArrayUsingPredicate:predicate] mutableCopy];
    filteredProductArray = [filteredProductArray valueForKeyPath:@"@distinctUnionOfObjects.Product_Name"];
    NSMutableArray *sortedProductArray = (NSMutableArray *)[filteredProductArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [productsDetailsViewController updateProductData:sortedProductArray];
 
    
    // filter Samples given array
    NSMutableArray *filteredSamplesGivenArray = [[unfilteredSamplesGivenArray filteredArrayUsingPredicate:predicate] mutableCopy];
    if (filteredSamplesGivenArray.count == 0 && filteredProductArray.count == 0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
    }
    NSMutableArray *distinctProductNames = [filteredSamplesGivenArray valueForKeyPath:@"@distinctUnionOfObjects.Product_Name"];
    NSMutableArray *sortedDistinctProductNameArray = (NSMutableArray *)[distinctProductNames sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableArray *finalSamplesGivenArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<[sortedDistinctProductNameArray count]; i++) {
        int sum = 0;
        for (NSMutableDictionary *dic in filteredSamplesGivenArray) {
            if ([[dic valueForKey:@"Product_Name"] isEqualToString:[sortedDistinctProductNameArray objectAtIndex:i]]) {
                sum += [[dic valueForKey:@"Issue_Qty"] intValue];
            }
        }
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:[NSString stringWithFormat:@"%d",sum] forKey:@"Issue_Qty"];
        [dic setObject:[sortedDistinctProductNameArray objectAtIndex:i] forKey:@"Product_Name"];
        [finalSamplesGivenArray addObject:dic];
    }
    
    [productsSampleViewController updateSamplesGivenData:finalSamplesGivenArray];
    
    [segmentControl setSelectedSegmentIndex:0 animated:YES];
    [eDetailingScrollView setContentOffset:CGPointZero animated:YES];
}

- (IBAction)btnReset:(id)sender {
    txtLocation.text = @"N/A";
    DateRangeTextField.StartDate = [dateHelper addToDate:[NSDate date] days:-1];
    DateRangeTextField.EndDate = [NSDate date];
    DateRangeTextField.SelecetdCustomDateRangeText = nil;
    [DateRangeTextField SetCustomDateRangeText];
    [productsDetailsViewController updateProductData:[[NSMutableArray alloc]init]];
    [productsSampleViewController updateSamplesGivenData:[[NSMutableArray alloc]init]];
    [segmentControl setSelectedSegmentIndex:0 animated:YES];
    [eDetailingScrollView setContentOffset:CGPointZero animated:YES];
}
#pragma mark date picker method

-(void)DidUserSelectDateRange:(NSDate *)StartDate EndDate:(NSDate *)EndDate AndCustomRangePeriodText:(NSString *)customPeriodText{
    
    DateRangeTextField.StartDate = StartDate;
    DateRangeTextField.EndDate = EndDate;
    DateRangeTextField.SelecetdCustomDateRangeText = customPeriodText;
    
    [DateRangeTextField SetCustomDateRangeText];
    
    NSLog(@"StartDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.StartDate]);
    NSLog(@"EndDateStr %@",[MedRepQueries convertNSDatetoDataBaseDateFormat:DateRangeTextField.EndDate]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
