//
//  MedRepElementTitleDescriptionLabel.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepElementTitleDescriptionLabel : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
-(NSString*)text;
-(void)setText:(NSString*)newText;

@end
