//
//  SWCustomersFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWCustomersFilterViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "NSPredicate+Distinct.h"
@interface SWCustomersFilterViewController ()

@end

@implementation SWCustomersFilterViewController
@synthesize filterNavController,filterPopOverController,customersArray,previousFilterParametersDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    filterParametersDict=[[NSMutableDictionary alloc]init];
    
    filteredCustomersArray=[[NSMutableArray alloc]init];
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
    
    
    
    customerStatusSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    customerTypeSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    
    NSLog(@"previous filter parameters are, %@", previousFilterParametersDict);
    
    if (previousFilterParametersDict.count>0) {
        filterParametersDict=previousFilterParametersDict;
    }
    if([[previousFilterParametersDict valueForKey:@"Customer_Name"] length]>0) {
        customerNameTxtFld.text=[previousFilterParametersDict valueForKey:@"Customer_Name"];
    }
    
    if([[filterParametersDict valueForKey:@"Customer_No"] length]>0) {
        customerCodeTextFld.text=[previousFilterParametersDict valueForKey:@"Customer_No"];
    }
    
    if([[filterParametersDict valueForKey:@"City"] length]>0) {
        customerCityTxtFld.text=[previousFilterParametersDict valueForKey:@"City"];
    }
    
    if([[filterParametersDict valueForKey:@"Cust_Status"] length]>0) {
        if ([[filterParametersDict valueForKey:@"Cust_Status"] isEqualToString:@"Open"]) {
            customerStatusSegment.selectedSegmentIndex=0;
        }else{
            customerStatusSegment.selectedSegmentIndex=1;
        }
    }
    
    if([[filterParametersDict valueForKey:@"Cash_Cust"] length]>0) {
        if ([[filterParametersDict valueForKey:@"Cash_Cust"] isEqualToString:@"Cash"]) {
            customerTypeSegment.selectedSegmentIndex=0;
        }else{
            customerTypeSegment.selectedSegmentIndex=1;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesCustomerFilter];
    }
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CLose Filter

-(void)closeFilter
{
    if ([self.delegate respondsToSelector:@selector(customerFilterDidClose)]) {
        [self.delegate customerFilterDidClose];
        [self.delegate filteredCustomerParameters:filterParametersDict];
    }
    [filterPopOverController dismissPopoverAnimated:YES];
}

-(void)clearFilter
{
    customerNameTxtFld.text=@"";
    customerCodeTextFld.text=@"";
    customerCityTxtFld.text=@"";
    customerStatusSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    customerTypeSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    filterParametersDict=[[NSMutableDictionary alloc]init];
}



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==customerNameTxtFld)
    {
        selectedTextField=@"Customer Name";
        selectedPredicateString=@"Customer_Name";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    else if (textField==customerCodeTextFld)
    {
            selectedTextField=@"Customer Code";
            [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"Customer_No"];
            selectedPredicateString=@"Customer_No";
            return NO;
    }
    else if (textField==customerCityTxtFld)
    {
        selectedTextField=@"City";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"City"];
        selectedPredicateString=@"City";
        return NO;
    }
    return YES;
}

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedTextField;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
   
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];

    //get distinct keys
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray=[[customersArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:predicateString];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Data", nil) message:NSLocalizedString(@"Please try later", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }
}

#pragma mark Selected Filter Data

-(void)selectedFilterValue:(NSString*)selectedString
{
    
    if ([selectedTextField isEqualToString:@"Customer Name"]) {
        customerNameTxtFld.text= selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"Customer Code"])
    {
        customerCodeTextFld.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"City"])
    {
        customerCityTxtFld.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
}


- (IBAction)searchButtontapped:(id)sender {
    
    
     filteredCustomersArray=[[NSMutableArray alloc]init];
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filter parameters dict is %@",filterParametersDict);

    if([[filterParametersDict valueForKey:@"Customer_Name"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Customer_Name ==[cd] %@",[filterParametersDict valueForKey:@"Customer_Name"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Customer_No"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Customer_No ==[cd] %@",[filterParametersDict valueForKey:@"Customer_No"]]];
    }
    
    if([[filterParametersDict valueForKey:@"City"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.City ==[cd] %@",[filterParametersDict valueForKey:@"City"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Cash_Cust"] length]>0) {
        
        if ([[filterParametersDict valueForKey:@"Cash_Cust"] isEqualToString:@"Cash"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Cash_Cust ==[cd] %@",@"Y"]];
        }
        else{
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Cash_Cust ==[cd] %@",@"N"]];
        }
    }
    if([[filterParametersDict valueForKey:@"Cust_Status"] length]>0) {
        
        if ([[filterParametersDict valueForKey:@"Cust_Status"] isEqualToString:@"Open"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Cust_Status ==[cd] %@",@"Y"]];
        }
        else{
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Cust_Status ==[cd] %@",@"N"]];
        }
    }
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredCustomersArray=[[customersArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    if (predicateArray.count==0) {
        
        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please change your filter criteria and try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [noFilterAlert show];
    }
    
    else  if (filteredCustomersArray.count>0) {
        if ([self.delegate respondsToSelector:@selector(filteredCustomers:)]) {
            [self.delegate filteredCustomers:filteredCustomersArray];
            [self.delegate filteredCustomerParameters:filterParametersDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
        
    }
    
     

}
- (IBAction)resetButtontapped:(id)sender {
    
    previousFilterParametersDict=[[NSMutableDictionary alloc]init];

    if ([self.delegate respondsToSelector:@selector(customerFilterdidReset)]) {
        [self.delegate customerFilterdidReset];
        [self.filterPopOverController dismissPopoverAnimated:YES];
    }
    
}

- (IBAction)customerTypeSegmentTapped:(UISegmentedControl *)sender {
    
     [filterParametersDict setValue:customerTypeSegment.selectedSegmentIndex==0?kCashTitle:kCreditTitle forKey:@"Cash_Cust"];
    
//    [filterParametersDict setValue:[sender titleForSegmentAtIndex:sender.selectedSegmentIndex] forKey:@"Cash_Cust"];
}

- (IBAction)customerStatusSegmentTapped:(UISegmentedControl *)sender {
    
     [filterParametersDict setValue:customerStatusSegment.selectedSegmentIndex==0?kOpenTitle:kBlockedTitle forKey:@"Cust_Status"];
    
//    [filterParametersDict setValue:[sender titleForSegmentAtIndex:sender.selectedSegmentIndex] forKey:@"Cust_Status"];
}



@end
