//
//  SWCustomerReturnHistoryDescriptionViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/21/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWCustomerReturnHistoryDescriptionViewController.h"

@interface SWCustomerReturnHistoryDescriptionViewController ()

@end

@implementation SWCustomerReturnHistoryDescriptionViewController
@synthesize selectedCustomer,orderLineItems,orderNumber,status;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [closeButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    headerView.backgroundColor=UINavigationBarBackgroundColor;
    orderNumberDescriptionLbl.text=orderNumber;
    
    if ([status isEqualToString:@"N/A"]) {
        statusDescriptionLbl.text=kNewStatus;
    }
    else
    {
        statusDescriptionLbl.text=status;
    }
}

- (IBAction)closeButtonTapped:(id)sender {
    
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

#pragma mark UITableView Methods

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"orderHistoryDescription";
    SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.dividerImageView.hidden=YES;
    cell.isHeader=YES;
    
    cell.itemCodeLbl.isHeader=YES;
    cell.descriptionLbl.isHeader=YES;
    cell.itemCodeLbl.isHeader=YES;
    cell.qtyLbl.isHeader=YES;
    cell.totalLbl.isHeader=YES;
    cell.unitPriceLbl.isHeader=YES;
    
    cell.itemCodeLbl.text=@"Item Code";
    cell.descriptionLbl.text=@"Description";
    cell.qtyLbl.text=@"Qty";
    cell.unitPriceLbl.text=@"Unit Price";
    cell.totalLbl.text=@"Total";
    
    return cell.contentView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectedCustomer.customerOrderHistoryLineItemsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"orderHistoryDescription";
    SWCustomerOrderHistoryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SWCustomerOrderHistoryDescriptionTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.dividerImageView.hidden=YES;
    
    CustomerOrderHistoryLineItems * currentCustomerHistoryLineItems=[selectedCustomer.customerOrderHistoryLineItemsArray objectAtIndex:indexPath.row];
    
    if ([NSString isEmpty:[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Item_Code]]==YES) {
        
        cell.itemCodeLbl.text=KNotApplicable;
    }
    else
    {
        cell.itemCodeLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Item_Code];
        
    }
    
    if ([NSString isEmpty:[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Description]]==YES) {
        cell.descriptionLbl.text=kProductInfoUnavailable;
    }
    else
    {
        cell.descriptionLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Description];
    }
    NSString *uomStr=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Order_Quantity_UOM];
    
    cell.qtyLbl.text=[NSString stringWithFormat:@"%@ %@",currentCustomerHistoryLineItems.Ordered_Quantity,uomStr];

    //cell.qtyLbl.text=[NSString stringWithFormat:@"%@",currentCustomerHistoryLineItems.Ordered_Quantity];
    cell.unitPriceLbl.text=[[SWDefaults getValidStringValue:currentCustomerHistoryLineItems.Unit_Selling_Price]  currencyString];
    cell.totalLbl.text=[[SWDefaults getValidStringValue:currentCustomerHistoryLineItems.Item_Value ]  currencyString];
    
    return cell;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
