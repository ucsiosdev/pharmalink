//
//  MedRepVisitsFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepVisitsDateFilterViewController.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepTextField.h"
#import "MedRepButton.h"

@protocol VisitsFilterDelegate <NSObject>


-(void)filteredVisitsDelegate:(NSMutableArray*)filteredVisits;

-(void)filteredVisitParameters:(NSMutableDictionary*)filterParameters;

-(void)visitsFilterDidCancel;

-(void)visitsFilterDidClose;

@end

@interface MedRepVisitsFilterViewController : UIViewController<VisitDatePickerDelegate,SelectedFilterDelegate,UITextFieldDelegate>

{
    NSString* selectedDataString;
    
    id visitsFilterDelegate;
    IBOutlet UIButton *locationTypeBtn;
    IBOutlet UIButton *visitTypeBtn;
    IBOutlet UIButton *classBtn;
    IBOutlet UIButton *locationBtn;
    IBOutlet UIButton *visitDateBtn;
    
    NSMutableDictionary* plannedVisitDetailsDict;
    
    
    IBOutlet MedRepTextField *visitDateTextField;
    IBOutlet MedRepTextField *visitLocationTextField;
    IBOutlet MedRepTextField *visitTypeTextField;
    IBOutlet MedRepTextField *locationTypeTextField;

    NSMutableArray* locationNamesArray;
    
    
    
    NSMutableArray * filteredVisitsArray;
}
- (IBAction)resetButtonTapped:(id)sender;

@property(strong,nonatomic) id visitsFilterDelegate;
@property (strong, nonatomic) IBOutlet UISegmentedControl *typeSegment;
@property(strong,nonatomic)NSMutableArray* contentArray;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) UINavigationController *filterNavController;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property(strong,nonatomic) NSMutableDictionary* previouslyFilteredContent;

@end
