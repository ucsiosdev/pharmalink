//
//  SalesWorxDescriptionLabel1.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDescriptionLabel1.h"
#import "MedRepDefaults.h"
@implementation SalesWorxDescriptionLabel1

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    self.font=kSWX_FONT_SEMI_BOLD(12);
    self.textColor=[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1];
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(super.text, nil);
    
    if (self.isWhiteText) {
        self.textColor=[UIColor whiteColor];
    }
}
-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
    
    
}

@end
