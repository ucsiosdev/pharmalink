//
//  CustomerHeaderView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 6/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CustomerHeaderView.h"
#import "SWFoundation.h"
#import "SWDefaults.h"

@implementation CustomerHeaderView

@synthesize customer;
@synthesize companyNameLabel;
@synthesize codeLabel;
@synthesize shouldHaveMargins;
@synthesize statusButton;

- (id)initWithFrame:(CGRect)frame andCustomer:(NSDictionary *)c {
    self = [super initWithFrame:frame];
    if (self) {
        [self setCustomer:c];
        [self setBackgroundColor:[UIColor whiteColor]];
        self.companyNameLabel=nil;
        [self setCompanyNameLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        //[self.companyNameLabel setBackgroundColor:[UIColor blueColor]];
        [companyNameLabel setText:[self.customer objectForKey:@"Customer_Name"]];
        [companyNameLabel setBackgroundColor:[UIColor clearColor]];
        [companyNameLabel setTextColor:UIColorFromRGB(GroupHeaderTextColor)];
        [companyNameLabel setShadowColor:[UIColor whiteColor]];
        [companyNameLabel setShadowOffset:CGSizeMake(0, 1)];
        [companyNameLabel setFont:RegularFontOfSize(26.0f)];
        
        self.codeLabel=nil;
        [self setCodeLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        //[self.codeLabel setBackgroundColor:[UIColor greenColor]];
        [codeLabel setText:[self.customer objectForKey:@"Customer_No"]];
        [codeLabel setBackgroundColor:[UIColor clearColor]];
        [codeLabel setTextColor:UIColorFromRGB(GroupHeaderTextColor)];
        [codeLabel setShadowColor:[UIColor whiteColor]];
        [codeLabel setShadowOffset:CGSizeMake(0, 1)];
        [codeLabel setFont:LightFontOfSize(16.0f)];
        
        [self setStatusButton:[UIButton buttonWithType:UIButtonTypeCustom]];
        
        BOOL customerStatus = [[c objectForKey:@"Cust_Status"] isEqualToString:@"Y"];
        BOOL cashCustomer = [[c objectForKey:@"Cash_Cust"] isEqualToString:@"Y"];
        BOOL creditHold = [[c objectForKey:@"Credit_Hold"] isEqualToString:@"Y"];
        
        NSString *imageName = @"Old_transparent";
        
        if (cashCustomer) {
            imageName = @"Old_yellow";
        }
        
        if (!customerStatus || creditHold) {
            imageName = @"Old_red";
        }

        [self.statusButton setImage:[UIImage imageNamed:imageName cache:NO] forState:UIControlStateNormal];
        
        [self addSubview:self.statusButton];
        [self addSubview:self.codeLabel];
        [self addSubview:self.companyNameLabel];
        
        [self setShouldHaveMargins:NO];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect b = self.bounds;
    int margin = 0;
    
    if (shouldHaveMargins) {
        margin = 55;
    }
    
    [self.companyNameLabel setFrame:CGRectMake(margin, 10, b.size.width - (margin * 2), 30)];
    [self.codeLabel setFrame:CGRectMake(margin, 40, b.size.width - (margin * 2), 16)];
    
    CGSize maximumLabelSize = CGSizeMake(self.companyNameLabel.bounds.size.width, 9999);
    
    CGSize expectedLabelSize=[SWDefaults fetchSizewithFontConstrainttoSize:self.companyNameLabel.text fontName:self.companyNameLabel.font labelSize:maximumLabelSize];
    
    
//    CGSize expectedLabelSize = [self.companyNameLabel.text sizeWithFont:self.companyNameLabel.font 
//                                       constrainedToSize:maximumLabelSize
//                                           lineBreakMode:NSLineBreakByWordWrapping];
    
    [self.statusButton setFrame:CGRectMake(expectedLabelSize.width + 65, 18, 13, 13)];
}


@end
