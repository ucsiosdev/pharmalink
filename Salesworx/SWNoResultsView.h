//
//  SWNoResultsView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 6/26/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWNoResultsView : UIView {
    UILabel *titleLabel;
}

@property (nonatomic, strong) UILabel *titleLabel;

@end