//
//  MedRepTextView.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/20/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepTextView.h"
#import "MedRepDefaults.h"
@implementation MedRepTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self. font=MedRepElementDescriptionLabelFont;
    self.textColor=MedRepElementDescriptionLabelColor;
    
    if (self.noBorders==YES) {
        self.backgroundColor=[UIColor whiteColor];

    }
    
 
    else
    {
    self.layer.borderWidth=1.0f;
    self.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    //self.layer.masksToBounds = NO;
    //   self.layer.shadowColor = [UIColor blackColor].CGColor;
    //   self.layer.shadowOpacity = 0.1f;
    //   self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    //   self.layer.shadowRadius = 1.0f;
    // self.layer.shouldRasterize = YES;
    self.layer.cornerRadius=5.0;

 //   self.layoutManager.allowsNonContiguousLayout = NO;
  // self.contentInset = UIEdgeInsetsMake(8, 8, 8, 8);
    }
    self.textAlignment=NSTextAlignmentNatural;

}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (self.isCopyPasteEnable) {
        if (action == @selector(copy:) || action == @selector(paste:) || action == @selector(select:) || action == @selector(selectAll:)||action == @selector(cut:))
            return YES;
    }
    return NO;
}


-(void)setIsReadOnly:(BOOL)status
{
    if(status==YES)
    {
        [self setUserInteractionEnabled:NO];
        
        self.textColor=SalesWorxReadOnlyTextViewTextColor;
        self.backgroundColor=SalesWorxReadOnlyTextViewColor;

    }
    else
    {
        self.backgroundColor=[UIColor whiteColor];

        self.textColor=MedRepElementDescriptionLabelColor;
        [self setUserInteractionEnabled:YES];
    }
}

@end
