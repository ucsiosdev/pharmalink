////
////  SWRouteViewController.m
////  SWRoute
////
////  Created by Irfan Bashir on 5/17/12.
////  Copyright (c) 2012 UCS Solutions. All rights reserved.
////
//
//#import "SWRouteViewController.h"
//#import "CashCutomerViewController.h"
//#import "LocationFilterViewController.h"
//#define sizeFrame 37
//
//@interface SWRouteViewController ()
//- (UIView *)headerView;
//@end
//
//@implementation SWRouteViewController
//
//
//
//- (id)init {
//    self = [super init];
//    if (self)
//    {
//        [self setTitle:NSLocalizedString(@"Route", nil)];
//        selectedDate=[NSDate date];
//        customer = [NSMutableDictionary dictionary];
//    }
//    return self;
//}
//
//
//- (void)viewDidAppear:(BOOL)animated{
//    
//    [super viewDidAppear:animated];
//
//
//}
//- (void)viewWillAppear:(BOOL)animated{
//    
//    Login_Btn.enabled = NO;
//    [SWDefaults clearCustomer];
//
//    [self performSelector:@selector(navigatetodeals) withObject:nil afterDelay:0.0];
//    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
//        //self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
//        //self.navigationController.navigationBar.translucent = NO;
//        
//        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
//        self.navigationController.toolbar.translucent = NO;
//        
//        self.extendedLayoutIncludesOpaqueBars = TRUE;
//        self.extendedLayoutIncludesOpaqueBars = NO;
//        self.edgesForExtendedLayout = UIRectEdgeBottom;
//        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//        
//        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                        [UIColor whiteColor],NSForegroundColorAttributeName,
//                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
//        
//        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
//        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
//        
//       // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
//        [self setNeedsStatusBarAppearanceUpdate];
//        
//    }
//    else
//    {
//        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
//    }
//
////    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
////        
////        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self)
////        {
////            UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
////            [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
////            
////            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"cache:NO] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
////            
////        }
////        else
////        {
////            
////        }
////    }
//
//    [Flurry logEvent:@"Route View"];
//    //[Crittercism leaveBreadcrumb:@"<Route View>"];
//
//}
//-(void)navigatetodeals
//{
//    
//    //routeSer.delegate=self;
//    
//    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
//}
//- (UIView *)headerView {
//    //table header
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width , 50)]  ;
//    
//    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, headerView.bounds.size.width, 40)]  ;
//    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    [companyNameLabel setText:NSLocalizedString(@"Today's Route", nil)];
//    [companyNameLabel setBackgroundColor:[UIColor clearColor]];
//    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
//    [companyNameLabel setShadowColor:[UIColor whiteColor]];
//    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
//    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
//    headerView.backgroundColor = UIColorFromRGB(0xF6F7FB);
//    [headerView addSubview:companyNameLabel];
//    
//    return headerView;
//}
////- (void)dealloc
////{
////    NSLog(@"Dealloc %@",self.title);
////    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////}
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor whiteColor];
//    
//    
//    tableView=[[UITableView alloc]initWithFrame:CGRectMake(20, 70, 300, 552) style:UITableViewStyleGrouped]  ;
//    [tableView setDelegate:self];
//    [tableView setDataSource:self];
//    tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    tableView.layer.borderWidth = 1.0;
//    tableView.backgroundView = nil;
//    tableView.backgroundColor = [UIColor whiteColor];
//    
//    UIBarButtonItem *displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Old_barcode" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)];
//    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, nil];
//    //set up pop over
//    
//    
//    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
//    if([language isEqualToString:@"ar"])
//    {
//        bottomImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"static-back-AR.png" cache:NO]];
//    }
//    else
//    {
//        bottomImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"static-back.png" cache:NO]];
//    }
//    bottomImage.frame = CGRectMake(350, 480, 650, 205);
//    bottomImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    bottomImage.layer.borderWidth = 1.0;
//    
//    visitedPlace=[[UILabel alloc] initWithFrame:CGRectMake(368, 540, 170, 100)]  ;
//    [visitedPlace setText:@""];
//    [visitedPlace setTextAlignment:NSTextAlignmentCenter];
//    [visitedPlace setBackgroundColor:[UIColor clearColor]];
//    [visitedPlace setTextColor:[UIColor whiteColor]];
//    [visitedPlace setFont:BoldFontOfSize(50.0)];
//    
//    remainingPlace=[[UILabel alloc] initWithFrame:CGRectMake(592, 540, 170, 100)]  ;
//    [remainingPlace setText:@""];
//    [remainingPlace setTextAlignment:NSTextAlignmentCenter];
//    [remainingPlace setBackgroundColor:[UIColor clearColor]];
//    [remainingPlace setTextColor:[UIColor whiteColor]];
//    [remainingPlace setFont:BoldFontOfSize(50.0)];
//    
//    coveragePlace=[[UILabel alloc] initWithFrame:CGRectMake(820, 540, 170, 100) ];
//    [coveragePlace setText:@""];
//    [coveragePlace setTextAlignment:NSTextAlignmentCenter];
//    [coveragePlace setBackgroundColor:[UIColor clearColor]];
//    [coveragePlace setTextColor:[UIColor whiteColor]];
//    [coveragePlace setFont:BoldFontOfSize(50.0)];
//    
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    
//    codeLabel = [UIButton buttonWithType:UIButtonTypeCustom];
//    [codeLabel setFrame:CGRectMake(585, 16,200, 38)];
//    [codeLabel setBackgroundImage:[UIImage imageNamed:@"date.png" cache:NO] forState:UIControlStateNormal];
//    [codeLabel setTitle:[formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
//    [codeLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    // [codeLabel setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [codeLabel.titleLabel setFont:BoldSemiFontOfSize(18.0f)];
//    [codeLabel addTarget:self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:codeLabel];
//    
//    
//    minusDate = [UIButton buttonWithType:UIButtonTypeCustom];
//    [minusDate setFrame:CGRectMake(535, 16,50, 38)];
//    minusDate.enabled=NO;
//    [minusDate setBackgroundImage:[UIImage imageNamed:@"Route_MinusIcon" cache:NO] forState:UIControlStateNormal];
//    [minusDate addTarget:self action:@selector(minusDate:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:minusDate];
//    
//    addDate = [UIButton buttonWithType:UIButtonTypeCustom];
//    [addDate setFrame:CGRectMake(785, 16,50, 38)];
//    [addDate setImage:[UIImage imageNamed:@"Route_PlusIcon" cache:NO] forState:UIControlStateNormal];
//    [addDate addTarget:self action:@selector(addDate:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:addDate];
//    
//    
//    customerMapButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [customerMapButton setFrame:CGRectMake(960, 71,39, 43)];
//    [customerMapButton setBackgroundImage:[UIImage imageNamed:@"mappin.png" cache:NO] forState:UIControlStateNormal];
//    [customerMapButton addTarget:self action:@selector(custLocationAction) forControlEvents:UIControlEventTouchUpInside];
//    customerMapButton.enabled = NO;
//    
//    
//    locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [locationButton setFrame:CGRectMake(960, 428,39, 43)];
//    [locationButton setBackgroundImage:[UIImage imageNamed:@"locate-button.png" cache:NO] forState:UIControlStateNormal];
//    [locationButton addTarget:self action:@selector(locationAction) forControlEvents:UIControlEventTouchUpInside];
//    
//    Login_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    //[Login_Btn setTitle: @"Start Visit" forState: UIControlStateNormal];
//    //    if([language isEqualToString:@"ar"])
//    //    {
//    //        [Login_Btn setImage:[UIImage imageNamed:@"start_visit-AR.png" cache:NO] forState:UIControlStateNormal];
//    //    }
//    //    else
//    //    {
//    //        [Login_Btn setImage:[UIImage imageNamed:@"start_visit.png" cache:NO] forState:UIControlStateNormal];
//    //    }
//    [Login_Btn setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];
//    [Login_Btn setTitle:NSLocalizedString(@"Start Visit", nil) forState:UIControlStateNormal];
//    Login_Btn.frame = CGRectMake(20, 630, tableView.frame.size.width, 50);
//    Login_Btn.backgroundColor = [UIColor whiteColor];
//    Login_Btn.titleLabel.textColor = [UIColor whiteColor];
//    Login_Btn.titleLabel.font = [UIFont systemFontOfSize:22.0f];
//    [Login_Btn addTarget:self action:@selector(Login_Btn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
//    formatter=nil;
//    usLocale=nil;
//    
//    mapView = [[MapView2 alloc] initWithFrame:CGRectMake(350,70,650,400)];
//    noResultsView=[[SWNoResultsView alloc] initWithFrame:CGRectMake(0, 60, self.view.bounds.size.height, self.view.bounds.size.width-60)];
//    [noResultsView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
//
// 
//}
//- (void)locationAction{
//    [mapView.mapView setCenterCoordinate:mapView.mapView.userLocation.location.coordinate animated:YES];
//}
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
// return NO;
//}
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {}
//- (void)PlaceAnnotationOnMapView
//{
//    
//    if([routes count]==0)
//    {
//        [mapView removeFromSuperview];
//        [visitedPlace removeFromSuperview];
//        [remainingPlace removeFromSuperview];
//        [coveragePlace removeFromSuperview];
//        [tableView removeFromSuperview];
//        [bottomImage removeFromSuperview];
//        [locationButton removeFromSuperview];
//        [Login_Btn removeFromSuperview];
//        //[customerMapButton removeFromSuperview];
//        [self.view addSubview:noResultsView];
//    }
//    else
//    {
//        mapView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        mapView.layer.borderWidth = 1.0;
//        mapView.backgroundColor = [UIColor clearColor];
//        mapView.mapView.showsUserLocation = YES;
//        [mapView.mapView removeAnnotations:mapView.mapView.annotations];
//        [self performSelector:@selector(locationAction) withObject:nil afterDelay:0.1];
//
//        visitedCustomer = 0;
//        remainningCustomer = 0;
//    
//        for (int i=0; i<[routes count]; i++)
//        {
//            @autoreleasepool {
//            place = [[Place2 alloc] init]  ;
//            place.latitude= [[[routes objectAtIndex:i] objectForKey:@"Cust_Lat"] floatValue];
//            place.longitude =  [[[routes objectAtIndex:i] objectForKey:@"Cust_Long"] floatValue];
//            place.name= [[routes objectAtIndex:i] objectForKey:@"Customer_Name"];
//            
//            NSString *detailString = [[routes objectAtIndex:i] objectForKey:@"City"];
//            NSString *timeString = [[routes objectAtIndex:i] objectForKey:@"Visit_Start_Time"];
//                
//                
//
//           if ([[routes objectAtIndex:i] objectForKey:@"Visit_Start_Time"] != [NSNull null])
//            {
//                NSString *newStr = [timeString substringFromIndex:11];
//                detailString = [detailString stringByAppendingString:[NSString stringWithFormat:@"  Time:%@",newStr] ];
//                place.otherDetail = detailString;
//            }
//            Annotation=[[CSMapAnnotation2 alloc]initWithPlace:place];
//            [mapView.mapView addAnnotation:Annotation];
//            
//            NSString *isVisit = [[routes objectAtIndex:i] objectForKey:@"Visit_Status"];
//            if([isVisit isEqualToString:@"N"])
//            {
//                remainningCustomer = remainningCustomer +1;
//            }
//            else
//            {
//                visitedCustomer= visitedCustomer + 1;
//            }
//            }
//        }
//        [noResultsView removeFromSuperview];
//
//        [self.view addSubview:mapView];
//        [self.view addSubview:tableView];
//        [self.view addSubview:bottomImage];
//        [self.view addSubview:visitedPlace];
//        [self.view addSubview:remainingPlace];
//        [self.view addSubview:coveragePlace];
//        [self.view addSubview:locationButton];
//        [self.view addSubview:Login_Btn];
//       // [self.view addSubview:customerMapButton];
//        double a = ((double)visitedCustomer/(double) [routes count])*100;
//        [visitedPlace setText:[NSString stringWithFormat:@"%d", [routes count]]];
//        [remainingPlace setText:[NSString stringWithFormat:@"%d", visitedCustomer]];
//        [coveragePlace setText:[NSString stringWithFormat:@"%.0f%@ ", a , @"%"]];
//    }
//}
//- (void)centerSelectedCustomerOnMapWithLatitude:(NSString *)lat andLongitude:(NSString *)longt{
//    MKCoordinateSpan span = MKCoordinateSpanMake(0.07f, 0.07f);
//    CLLocationCoordinate2D coordinate = {[lat floatValue],[longt floatValue]};
//    MKCoordinateRegion region = {coordinate, span};
//    MKCoordinateRegion regionThatFits = [mapView.mapView regionThatFits:region];
//   [mapView.mapView setRegion:regionThatFits animated:YES];
//    
//}
//
//- (void)minusDate:(UIButton *)sender{
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    NSDate *previousDate = [selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
//    NSString *preDateString =[formatter stringFromDate:previousDate];
//    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
//    if([todayDate isEqualToString:preDateString])
//    {
//        minusDate.enabled=NO;
//        codeLabel.titleLabel.text =[formatter stringFromDate:[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)]];
//        selectedDate=[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
//        [self dateChanged:nil];
//    }
//    else{
//        codeLabel.titleLabel.text =[formatter stringFromDate:[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)]];
//        selectedDate=[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
//        [self dateChanged:nil];
//    }
//    formatter=nil;
//    usLocale=nil;
//}
//- (void)addDate:(UIButton *)sender{
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    NSDate *previousDate = [selectedDate dateByAddingTimeInterval:(1.0*24*60*60)];
//
//    NSString *preDateString =[formatter stringFromDate:previousDate];
//
//    
//    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
//
//    if(![todayDate isEqualToString:preDateString])
//    {
//        minusDate.enabled=YES;
//    }
//
//    codeLabel.titleLabel.text = [formatter stringFromDate:previousDate];
//    selectedDate=previousDate;
//    [self dateChanged:nil];
//    formatter=nil;
//    usLocale=nil;
//}
//- (void)showInbox:(UIButton *)sender {
//
//    
//}
//- (void)showPicker:(UIButton *)sender {
//    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
//    datePickerViewController.isRoute=YES;
//    [datePickerViewController.picker setDate:selectedDate];
//    datePickerPopOver.delegate=self;
//
//    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
//    navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    navigationController.navigationBar.translucent = NO;
//    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
//    datePickerPopOver.delegate=self;
//    [datePickerPopOver presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//  
//}
//- (void)dateChanged:(SWDatePickerViewController *)sender {
//    
//    if (sender == nil)
//    {
//        //routeSer.delegate=self;
//        //[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate];
//        NSDateFormatter *formatter = [NSDateFormatter new]  ;
//        [formatter setDateFormat:@"yyyy-MM-dd"];
//        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//        [formatter setLocale:usLocale];
//        codeLabel.titleLabel.text = [formatter stringFromDate:selectedDate];
//        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
//        [tableView reloadData];
//    }
//    else
//    {
//        NSDateFormatter *formatter = [NSDateFormatter new]  ;
//        [formatter setDateFormat:@"yyyy-MM-dd"];
//        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//        [formatter setLocale:usLocale];
//        NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
//        
//        NSString *preDateString =[formatter stringFromDate:sender.selectedDate];
//
//        if(![todayDate isEqualToString:preDateString])
//        {
//            minusDate.enabled=YES;
//        }
//        else
//        {
//            minusDate.enabled=NO;
//
//        }
//        selectedDate = sender.selectedDate;
//      
//        codeLabel.titleLabel.text = [formatter stringFromDate:selectedDate];
//        //routeSer.delegate=self;
//        //[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate];
//        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
//
//        [tableView reloadData];
//        formatter=nil;
//        usLocale=nil;
//    }
//    
//    NSDateFormatter* df = [NSDateFormatter new];
//    [df setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [df setLocale:usLocale];
//    NSDate* enteredDate = selectedDate;
//    NSDate * today = [NSDate date];
//    NSComparisonResult result = [today compare:enteredDate];
//    switch (result) 
//    {
//        case NSOrderedAscending:
//            isFurtureDate = YES;
//            break;
//        case NSOrderedDescending:
//            isFurtureDate = NO;
//            break;
//
//        case NSOrderedSame:
//            isFurtureDate = NO;
//            break;
//
//        default:
//            isFurtureDate = NO;
//            break;
//    }
//    df=nil;
//    usLocale=nil;
//}
//#pragma mark SWRouteService Delegate
//- (void)getRouteServiceDidGetRoute:(NSArray *)r{
//    routes=r;
//  //  NSSortDescriptor * sortByRank = [[NSSortDescriptor alloc] initWithKey:@"Visit_Start_Time" ascending:YES selector:@selector(caseInsensitiveCompare:)];
//
//    
//   // NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Visit_Start_Time" ascending:YES];
//   // NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//   // routes = [[routes sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
//    
//    [tableView reloadData];
//    [self PlaceAnnotationOnMapView];
//    
//    [datePickerPopOver dismissPopoverAnimated:YES];
//    r=nil;
//}
//
//-(void)custLocationAction
//{
//    [self centerSelectedCustomerOnMapWithLatitude:[customer stringForKey:@"Cust_Lat"] andLongitude:[customer stringForKey:@"Cust_Long"]];
//}
//#pragma UITableView DataSource
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    return [self headerView];
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 50;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 70;
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return [routes count];
//}
//- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    @autoreleasepool {
//    NSString *CellIdentifier = @"RouteViewCell";
//    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier]  ;
//        cell.textLabel.numberOfLines=2;
//    }
//    
//    NSDictionary *row = [routes objectAtIndex:indexPath.row];
//    
//    [cell.textLabel setText:[row objectForKey:@"Customer_Name"]];
//    
//    if ([row objectForKey:@"Visit_Start_Time"] != [NSNull null])
//    {
//        NSString *timeString = [row stringForKey:@"Visit_Start_Time"];
//        
//        NSString *newStr = [timeString substringFromIndex:11];
//        
//        [cell.detailTextLabel setText:[NSString stringWithFormat:@"Time : %@",newStr]];
//    }
// 
//    if([[row stringForKey:@"Visit_Status"] isEqualToString:@"Y"])
//    {
//        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
//    }
//    else
//    {
//        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//
//    }
//    return cell;
//}
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 60.0f;
//}
//
//#pragma mark UITableView Delegate
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSDictionary *row = [routes objectAtIndex:indexPath.row];
//    customer=[row mutableCopy];
//   Singleton *single = [Singleton retrieveSingleton];
//   //
//    //NSLog(@"%@",row);
//    single.planDetailId = [row stringForKey:@"FSR_Plan_Detail_ID"];
//
//    [self centerSelectedCustomerOnMapWithLatitude:[customer stringForKey:@"Cust_Lat"] andLongitude:[customer stringForKey:@"Cust_Long"]];
//    
//    Login_Btn.enabled = YES;
//    customerMapButton.enabled = YES;
//    Login_Btn.titleLabel.textColor = [UIColor whiteColor];
//}
//- (void)Login_Btn_Clicked:(id)sender{
//    // Code for button CLick which you want to do.
//    
//   Singleton *single = [Singleton retrieveSingleton];
//    // CHECK IF CASH CUSTOMER THEN SHOW FORM
//    if([[customer stringForKey:@"Cash_Cust"]isEqualToString:@"Y"])
//    {
//        single.isCashCustomer = @"Y";
//        CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:customer ];
//        [self.navigationController pushViewController:cashCustomer animated:YES];
//    }
//    else
//    {
//        
//        if(!isFurtureDate)
//        {
//            //customerSer.delegate=self;
//
//            //[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customer  stringForKey:@"Customer_ID"]];
//            [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customer stringForKey:@"Customer_ID"]]];
//
//            //[self setCustomer:[customer  mutableCopy]];
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"You can not start visit for future date." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//            [alert show];
////            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                               message:@"You can not start visit for future date."
////                      closeButtonTitle:NSLocalizedString(@"OK", nil)
////                     secondButtonTitle:nil
////                   tappedButtonAtIndex:nil];
//        }
//    }
//}
//- (void)GetRouteDataFromBackground:(NSNotification *)notification{
//    
//    //routeSer.delegate=self;
//    //[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate];
//    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
//
//
//}
//#pragma Barcode Functions
//- (void)displayMap:(id)sender {
//    
//    Singleton *single = [Singleton retrieveSingleton];
//    
//    single.isBarCode = YES;
//    reader = [ZBarReaderViewController new];
//    reader.readerDelegate = self;
//    //reader.showsZBarControls = Y;
//    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
//    
//    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
//    customOverlay.backgroundColor = [UIColor clearColor];
//    customOverlay.opaque = NO;
//    
//    UIToolbar *toolbar = [[UIToolbar alloc] init];
//    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
//    toolbar.barStyle = UIBarStyleBlackTranslucent;
//    
//    
//    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
//    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
//    
//    [toolbar setItems:arr animated:YES];
//    [customOverlay addSubview:toolbar];
//    reader.cameraOverlayView =customOverlay ;
//    
//    reader.wantsFullScreenLayout = NO;
//
//    reader.readerView.zoom = 1.0;
//    
//    reader.showsZBarControls=NO;
//    
//    [self presentViewController: reader animated: YES completion:nil];
//}
//- (void)barButtonBackPressed:(id)sender
//{
//    [reader dismissViewControllerAnimated: YES completion:nil] ;
//}
//- (void)imagePickerController: (UIImagePickerController*) reader2 didFinishPickingMediaWithInfo: (NSDictionary*) info{
//    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
//    [reader dismissViewControllerAnimated: YES completion:nil];
// 
//    ZBarSymbol *symbol = nil;
//    Singleton *single = [Singleton retrieveSingleton];
//    for(symbol in results){
//        single.valueBarCode=symbol.data;
//        // NSString *upcString = symbol.data;
//        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init];
//        [locationFilterController setTarget:self];
//        [locationFilterController setAction:@selector(filterChangedBarCode)];
//        [locationFilterController selectionDone:self];
//        single.isBarCode = NO;
//    }
//
//}
//- (void)filterChangedBarCode {
//   // [reader dismissViewControllerAnimated: YES];
//
//    [self.navigationController setToolbarHidden:YES animated:YES];
//   // [self.navigationItem setRightBarButtonItem:nil animated:YES];
//    //customerSer.delegate = self;
//
//    //[SWDatabaseManager retrieveManager] dbGetCollection];
//    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
//
//}
//#pragma mark Customer List service delegate
////- (void)customerServiceDidGetList:(NSArray *)cl{
//- (void)getListFunction:(NSArray *)cl
//{
//    if(![cl count]==0)
//    {
//        SWSection *section = [cl objectAtIndex:0];
//        NSDictionary *row = [section.rows objectAtIndex:0];
//        customer=[row mutableCopy] ;
//        BOOL isInList=NO;
//        for (int i=0; i<[routes count]; i++)
//        {
//            @autoreleasepool {
//            NSDictionary *row1 = [routes objectAtIndex:i];
//        
//            if ([[row1 stringForKey:@"Customer_Name"] isEqualToString:[row stringForKey:@"Customer_Name"]]) {
//                isInList=YES;
//            }
//            }
//        
//        }
//        if (isInList){
//            [self performSelector:@selector(Login_Btn_Clicked:) withObject:nil afterDelay:1.0];
//            //[self Login_Btn_Clicked:self];
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:@"%@ is not in today's route list",[row stringForKey:@"Customer_Name"]] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//            [alert show];
//            
////            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                               message:[NSString stringWithFormat:@"%@ is not in today's route list",[row stringForKey:@"Customer_Name"]]
////                      closeButtonTitle:NSLocalizedString(@"OK", nil)
////                     secondButtonTitle:nil
////                   tappedButtonAtIndex:nil];
//            
//        }
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"No customer found" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil)      otherButtonTitles:nil];
//        [alert show];
//        
////        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
////                           message:@"No customer found"
////                  closeButtonTitle:NSLocalizedString(@"OK", nil)
////                 secondButtonTitle:nil
////               tappedButtonAtIndex:nil];
//        
//    }
//    cl=nil;
//    
//}
////- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
//- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
//  
//    [SWDefaults setCustomer:customer];
//    customer = [SWDefaults validateAvailableBalance:orderAmmount];
//
//   Singleton *single = [Singleton retrieveSingleton];
//    single.isCashCustomer = @"N";
//    SWVisitManager *visitManager = [[SWVisitManager alloc] init];
//    [visitManager startVisitWithCustomer:customer parent:self andChecker:@"R"];
//    orderAmmount=nil;
//}
//- (void)applyMapViewMemoryHotFix{
//   /*
//    switch (mapView.mapView.mapType) {
//        case MKMapTypeHybrid:
//        {
//            //NSLog(@"change map 1");
//            mapView.mapView.mapType = MKMapTypeStandard;
//        }
//            
//            break;
//        case MKMapTypeStandard:
//        {
//               //NSLog(@"change map 2");
//            mapView.mapView.mapType = MKMapTypeHybrid;
//        }
//            
//            break;
//        default:
//            break;
//    }
//    mapView.mapView.showsUserLocation = NO;
//    mapView.mapView.delegate = nil;
//    [mapView.mapView removeFromSuperview];
//    mapView.mapView = nil;
//    [mapView RemoveIT];
//    mapView=nil;
//
//    [mapView removeFromSuperview];
//    */
//    
//}
//-(void)viewDidUnload
//{
//}
//-(void)viewDidDisappear:(BOOL)animated
//{
//    //[self applyMapViewMemoryHotFix];
//    [mapView RemoveIT];
//}
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
//{
//    datePickerPopOver = nil;
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    codeLabel.titleLabel.text = [formatter stringFromDate:selectedDate];
//
//}
//
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    return YES;
//}
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        self.view = nil;
//    
//    // Dispose of any resources that can be recreated.
//}
//@end

#import "SWRouteViewController.h"
#import "SWRouteTableViewCell.h"
#import "SalesWorxVisitOptionsToDoTableViewCell.h"
#import "MedRepDefaults.h"
#import "LocationFilterViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "MedRepQueries.h"
#import "OnsiteOptionsViewController.h"
#import "SalesWorxiBeaconManager.h"
#import "SalesWorxODOMeterViewController.h"

@interface SWRouteViewController ()<AVCaptureMetadataOutputObjectsDelegate>

@end

@implementation SWRouteViewController

@synthesize selectedCustomer;
- (id)init {
    self = [super init];
    if (self)
    {
        //[self setTitle:NSLocalizedString(@"Route", nil)];
        selectedDate=[NSDate date];
        customer = [NSMutableDictionary dictionary];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIViewBackGroundColor;
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Route"];
    
//    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, _btnStartVisit.frame.size.height - 2.0f, _btnStartVisit.frame.size.width, 4)];
//    bottomBorder.backgroundColor = [UIColor colorWithRed:(14.0/255.0) green:(146.0/255.0) blue:(103.0/255.0) alpha:1];
//    
//    [_btnStartVisit addSubview:bottomBorder];
    _btnStartVisit.layer.cornerRadius = 4;
    _btnStartVisit.layer.masksToBounds = YES;
    
    _datePickerView.layer.cornerRadius = 4;
    _datePickerView.layer.masksToBounds = YES;
    
    indexPathArray=[[NSMutableArray alloc]init];
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:kDateFormatWithoutTime];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [_btnDate setTitle:[formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
    _btnDate.titleLabel.font=kSWX_FONT_SEMI_BOLD(18);
    _btnStartVisit.titleLabel.font=kSWX_FONT_SEMI_BOLD(16);
}

-(void)becomeActive
{
    NSLog(@"did become active called in route");
    
    [self refreshData];
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesRouteScreenName];

    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(GetRouteDataFromBackground:) name:@"GetRouteDataFromBackground" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    
    [SWDefaults clearCustomer];
    
    
    
    
    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        //self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        //self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        
        // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            
            UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            [leftBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        }
        else
        {
            [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

        }
    }
    btnBarCode = [[SalesWorxImageBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarcodeIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBarCode:)];
    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:btnBarCode, nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [_locationView setNeedsLayout];
    [_locationView layoutIfNeeded];
    
    mapView = [[MapView2 alloc] initWithFrame:CGRectMake(8,50, _locationView.frame.size.width-16, _locationView.frame.size.height-58)];
    mapView.mapView.delegate = self;
    [_locationView addSubview:mapView];
    
    if (self.isMovingToParentViewController) {
        if ([routes count] >0)
        {
            
            
            NSString *selectedCustomerID;
            
            if (![NSString isEmpty:[SWDefaults getValidStringValue:selectedCustomer.Customer_ID]]) {
                selectedCustomerID=[SWDefaults getValidStringValue:selectedCustomer.Customer_ID];
                NSArray *filtered = [routes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_ID == %@",selectedCustomerID]]];
                
                NSInteger indexObj  =[routes indexOfObject:[filtered objectAtIndex:0]];
                NSLog(@"selected index %@", [routes objectAtIndex:indexObj]);
                
                if ([_customerTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
                {
                    
                   
                    
                    [_customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]
                                                    animated:YES
                                              scrollPosition:UITableViewScrollPositionNone];
                    
                    [_customerTableView.delegate tableView:_customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]];
                    
                }
            }
            
            else{
            
            [_customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
            [self tableView:_customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [self centerSelectedCustomerOnMapWithLatitude:[[routes objectAtIndex:0] stringForKey:@"Cust_Lat"]  andLongitude:[[routes objectAtIndex:0] stringForKey:@"Cust_Long"]];
            }
        }
        else
        {
            mapView.hidden = YES;
            btnBarCode.enabled = NO;
            _btnStartVisit.enabled = NO;
            _btnStartVisit.alpha = 0.7;
        }
    }
    else if (routes.count>0)
    {
        if (selectedIndexPath) {
            [_customerTableView selectRowAtIndexPath:selectedIndexPath animated:YES scrollPosition:0];
            [self tableView:_customerTableView didSelectRowAtIndexPath:selectedIndexPath];
        }
        NSLog(@"reloading tableview in route");
    }
    
    
    if (self.isMovingToParentViewController) {
        
        

    }    
}
- (IBAction)btnDate:(id)sender
{
    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=YES;
    [datePickerViewController.picker setDate:selectedDate];
    datePickerPopOver.delegate=self;
    
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    navigationController.navigationBar.translucent = NO;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    [datePickerPopOver presentPopoverFromRect:_btnDate.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
- (IBAction)btnMinusDate:(id)sender
{
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSDate *previousDate = [selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
    NSString *preDateString =[formatter stringFromDate:previousDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    if([todayDate isEqualToString:preDateString])
    {
        btnBarCode.enabled = YES;
        _btnStartVisit.enabled = YES;
        _btnStartVisit.alpha = 1.0;
        _btnMinusDate.enabled=NO;
        selectedDate=[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
        [self dateChanged:nil];
    }
    else
    {
        btnBarCode.enabled = NO;
        _btnStartVisit.enabled = NO;
        _btnStartVisit.alpha = 0.7;
        selectedDate=[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
        [self dateChanged:nil];
    }
    formatter=nil;
    usLocale=nil;
}
- (IBAction)btnPlusDate:(id)sender
{
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSDate *futureDate = [selectedDate dateByAddingTimeInterval:(1.0*24*60*60)];
    NSString *preDateString =[formatter stringFromDate:futureDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    NSArray *arrOfCustomer = [[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:futureDate];
    
    if ([arrOfCustomer count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Data", nil) message:NSLocalizedString(@"No data available", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
        [alert show];
    } else
    {
        mapView.hidden = NO;
        
        routes = arrOfCustomer;
        selectedDate=futureDate;
        [_btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[formatter stringFromDate:selectedDate]] forState:UIControlStateNormal];
        [_customerTableView reloadData];
        [_customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
        [self tableView:_customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        if(![todayDate isEqualToString:preDateString])
        {
            _btnMinusDate.enabled=YES;
            _btnStartVisit.enabled = NO;
            _btnStartVisit.alpha = 0.7;
            btnBarCode.enabled = NO;
        }
    }
    [self loadVisitStatisticsData];
    
    formatter=nil;
    usLocale=nil;
}
- (void)dateChanged:(SWDatePickerViewController *)sender
{
    if (sender == nil)
    {
        NSDateFormatter *formatter = [NSDateFormatter new]  ;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
        [_btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[formatter stringFromDate:selectedDate]] forState:UIControlStateNormal];
        
        if ([routes count] == 0)
        {
            mapView.hidden = YES;
            
            _lblName.text = @"--";
            _lblNumber.text = @"--";
            _lblAddress.text = @"--";
            _lblCreditLimit.text = @"--";
            _lblAvailabelBalance.text = @"--";
            _lblPDCDue.text = @"--";
            _lblOverDue.text = @"--";
            _lblTotalOutstandings.text = @"--";
            _lblCurrentMonthSales.text = @"--";
            
            btnBarCode.enabled = NO;
            _btnStartVisit.enabled = NO;
            _btnStartVisit.alpha = 0.7;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No data available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        } else
        {
            mapView.hidden = NO;
            
            [_customerTableView reloadData];
            [_customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
            [self tableView:_customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
    else
    {
        NSDateFormatter *formatter = [NSDateFormatter new]  ;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
        NSString *preDateString =[formatter stringFromDate:sender.selectedDate];
        
        NSArray *arrOfCustomer = [[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:sender.selectedDate];
        
        if(![todayDate isEqualToString:preDateString])
        {
            _btnMinusDate.enabled=YES;
            _btnStartVisit.enabled = NO;
            _btnStartVisit.alpha = 0.7;
            btnBarCode.enabled = NO;
        }
        else
        {
            _btnMinusDate.enabled=NO;
            _btnStartVisit.enabled = YES;
            _btnStartVisit.alpha = 1.0;
            btnBarCode.enabled = YES;
        }
        
        if ([arrOfCustomer count] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No data available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            _btnMinusDate.enabled=YES;
            _btnStartVisit.enabled = NO;
            _btnStartVisit.alpha = 0.7;
            btnBarCode.enabled = NO;
            
            NSString *dateOnButton = [MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:@"yyyy-MM-dd" scrString:_btnDate.titleLabel.text];
            
            if ([dateOnButton isEqualToString:todayDate]) {
                _btnMinusDate.enabled=NO;
                _btnStartVisit.enabled = YES;
                _btnStartVisit.alpha = 1.0;
                btnBarCode.enabled = YES;
            }
        } else
        {
            mapView.hidden = NO;
            
            routes = arrOfCustomer;
            selectedDate = sender.selectedDate;
            [_btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[formatter stringFromDate:selectedDate]] forState:UIControlStateNormal];
            [_customerTableView reloadData];
            [_customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
            [self tableView:_customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        
        [datePickerPopOver dismissPopoverAnimated:YES];
        formatter=nil;
        usLocale=nil;
    }
    [self loadVisitStatisticsData];
}
-(void)loadVisitStatisticsData
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:selectedDate];
    
    @try {
        NSArray * arrPlannedVisits= [[SWDatabaseManager retrieveManager] dbGetTotalPlannedVisits:dateString];
        if ([arrPlannedVisits count] >0)
        {
            _lblPlannedVisits.text = [[[arrPlannedVisits objectAtIndex:0]valueForKey:@"Total"]stringValue];
        }
        else
        {
            _lblPlannedVisits.text = @"0";
        }
    }
    @catch (NSException *exception) {
        _lblPlannedVisits.text = @"0";
    }
    @try {
        NSArray * arrCompletedVisits= [[SWDatabaseManager retrieveManager] dbGetTotalCompletedVisits:dateString];
        if ([arrCompletedVisits count] >0)
        {
            _lblCompletedVisits.text = [[[arrCompletedVisits objectAtIndex:0]valueForKey:@"Total"]stringValue];
        }
        else
        {
            _lblCompletedVisits.text = @"0";
        }
    }
    @catch (NSException *exception) {
        _lblCompletedVisits.text = @"0";
    }
    
    @try {
        NSArray * arrOutOfRoute= [[SWDatabaseManager retrieveManager] dbGetCustomerOutOfRouteCount:dateString];
        if ([arrOutOfRoute count] >0)
        {
            _lblOutOfRoute.text = [NSString stringWithFormat:@"%d",arrOutOfRoute.count];
        }
        else
        {
            _lblOutOfRoute.text = @"0";
        }
    }
    @catch (NSException *exception) {
        _lblOutOfRoute.text = @"0";
    }
}

-(void)selectedVisitOption:(NSString*)visitOption
{
    
    [customer setValue:visitOption forKey:@"Visit_Type"];
    Singleton *single = [Singleton retrieveSingleton];
    // CHECK IF CASH CUSTOMER THEN SHOW FORM
    if([[customer stringForKey:@"Cash_Cust"]isEqualToString:@"Y"])
    {
        single.isCashCustomer = @"Y";
        CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:customer ];
        [self.navigationController pushViewController:cashCustomer animated:YES];
    }
    else
    {
        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customer stringForKey:@"Customer_ID"]]];
    }
 
}

-(void)startCustomerVisit
{
    NSString *DateString = [MedRepQueries fetchDatabaseDateFormat];    
    NSMutableArray *SODReading = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_FSR_Process_Transactions where Trx_Date like '%@%%'",DateString]];
    AppControl* appControl =[AppControl retrieveSingleton];

    if ([SODReading count] == 0 && [appControl.ODOMETER_READING isEqualToString:KAppControlsYESCode]) {
        SalesWorxODOMeterViewController *odoMeterVC = [[SalesWorxODOMeterViewController alloc]init];
        odoMeterVC.ODOMeterDelegate = self;
        UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:odoMeterVC];
        mapPopUpViewController.navigationBarHidden = YES;
        mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
        mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
        odoMeterVC.view.backgroundColor = KPopUpsBackGroundColor;
        odoMeterVC.modalPresentationStyle = UIModalPresentationCustom;
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:mapPopUpViewController animated:NO completion:nil];
        
    } else {
        
        Singleton *single = [Singleton retrieveSingleton];
        
        if ([appControl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
            
            
            SalesWorxCashCustomerOnsiteVisitOptionsViewController * cashCustOnsiteVC=[[SalesWorxCashCustomerOnsiteVisitOptionsViewController alloc]init];
            cashCustOnsiteVC.delegate=self;
            salesWorxVisit=[[SalesWorxVisit alloc]init];
            VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
            currentVisitOptions.customer=[SWDefaults fetchCurrentCustObject:customer];
            salesWorxVisit.detectedBeacon=[[SalesWorxiBeaconManager retrieveSingleton]detectedBeacon];
            salesWorxVisit.visitOptions=currentVisitOptions;
            salesWorxVisit.isPlannedVisit=NO;
            cashCustOnsiteVC.currentVisit=salesWorxVisit;
            UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:cashCustOnsiteVC];
            mapPopUpViewController.navigationBarHidden=YES;
            //mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
            mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
            //cashCustOnsiteVC.view.backgroundColor = KPopUpsBackGroundColor;
            cashCustOnsiteVC.modalPresentationStyle = UIModalPresentationCustom;
            self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
            [self presentViewController:mapPopUpViewController animated:NO completion:nil];
            
            
            
            
            
            //        SalesWorxCashCustomerOnsiteVisitOptionsViewController * cashCustOnsiteVC=[[SalesWorxCashCustomerOnsiteVisitOptionsViewController alloc]init];
            //        cashCustOnsiteVC.delegate=self;
            //        //            [self.parentViewController presentMJPopoverViewController:cashCustOnsiteVC animationType:MJPopupViewAnimationSlideTopBottom backGroundTap:YES dismissed:nil];
            //
            //        VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
            //        salesWorxVisit=[[SalesWorxVisit alloc]init];
            //        currentVisitOptions.customer=[SWDefaults fetchCurrentCustObject:customer];
            //        salesWorxVisit.visitOptions=currentVisitOptions;
            //        salesWorxVisit.isPlannedVisit=YES;
            //        salesWorxVisit.FSR_Plan_Detail_ID=[[routes objectAtIndex:selectedIndexPath.row] valueForKey:@"FSR_Plan_Detail_ID"];
            //
            //        cashCustOnsiteVC.currentVisit=salesWorxVisit;
            //        cashCustOnsiteVC.view.backgroundColor = [UIColor clearColor];
            //        cashCustOnsiteVC.modalPresentationStyle = UIModalPresentationCustom;
            //
            //        [self.navigationController presentViewController:cashCustOnsiteVC animated:NO completion:nil];
            //
            
            //            if([[customer valueForKey:@"Cash_Cust"] isEqualToString:@"Y"]){
            //
            //
            //            SalesWorxCashCustomerOnsiteVisitOptionsViewController * cashCustOnsiteVC=[[SalesWorxCashCustomerOnsiteVisitOptionsViewController alloc]init];
            //            cashCustOnsiteVC.delegate=self;
            //            //            [self.parentViewController presentMJPopoverViewController:cashCustOnsiteVC animationType:MJPopupViewAnimationSlideTopBottom backGroundTap:YES dismissed:nil];
            //
            //                VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
            //                 salesWorxVisit=[[SalesWorxVisit alloc]init];
            //                currentVisitOptions.customer=[SWDefaults fetchCurrentCustObject:customer];
            //                salesWorxVisit.visitOptions=currentVisitOptions;
            //                salesWorxVisit.isPlannedVisit=YES;
            //                cashCustOnsiteVC.currentVisit=salesWorxVisit;
            //                cashCustOnsiteVC.view.backgroundColor = [UIColor clearColor];
            //            cashCustOnsiteVC.modalPresentationStyle = UIModalPresentationCustom;
            //
            //            [self.navigationController presentViewController:cashCustOnsiteVC animated:NO completion:nil];
            //
            //            }
            //            else
            //            {
            //
            //            OnsiteOptionsViewController* onsiteOptions=[[OnsiteOptionsViewController alloc]init];
            //            onsiteOptions.delegate=self;
            //            [self presentPopupViewController:onsiteOptions animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
            //            }
        }
        else{
            
            single.isCashCustomer = @"N";
            // [[[SWVisitManager alloc] init] startVisitWithCustomer:customer parent:self andChecker:@"D"];
            salesWorxVisit.FSR_Plan_Detail_ID=[[routes objectAtIndex:selectedIndexPath.row] valueForKey:@"FSR_Plan_Detail_ID"];
            
            
            SalesWorxVisitOptionsViewController * visitOptions=[[SalesWorxVisitOptionsViewController alloc]init];
            salesWorxVisit=[[SalesWorxVisit alloc]init];
            VisitOptions* currentVisitOptions=[[VisitOptions alloc]init];
            currentVisitOptions.customer=[SWDefaults fetchCurrentCustObject:customer];
            salesWorxVisit.visitOptions=currentVisitOptions;
            salesWorxVisit.isPlannedVisit=YES;
            visitOptions.salesWorxVisit=salesWorxVisit;
            
            //[self.navigationController pushViewController:visitOptions animated:YES];
            
            [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:salesWorxVisit withParent:self];
            
        }
    }
}

-(void)StartVisitAfterPopUp {
    [self startCustomerVisit];
}

- (IBAction)btnStartVisit:(id)sender
{
    
    //show an alert if there is any open visit to close that visit.
    NSMutableArray * openVisits=[[SWDatabaseManager retrieveManager]fetchOpenVisitsforOtherCustomers:[customer stringForKey:@"Ship_Customer_ID"] ]
    ;
    NSLog(@"open visits count is %@", openVisits);
    if (openVisits.count>0) {
        
        NSString* customerName=[openVisits valueForKey:@"Customer_Name"];
        
        openPlannedDetailID=[openVisits valueForKey:@"FSR_Plan_Detail_ID"];
        
        
        openVisitID=[openVisits valueForKey:@"Actual_Visit_ID"];
        
        if (!openVisitAlert) {
            
            
            NSString * firstText=NSLocalizedString(@"There is an open visit for the customer", nil);
            NSString* secondText=NSLocalizedString(@"would you like to close this visit before starting a new visit", nil);
            NSString* finalString=[NSString stringWithFormat:@"%@\n %@ \n %@",firstText,customerName,secondText ];
            //[SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Open Visit", nil) andMessage:finalString withController:secondText];
            
            
            
            
            openVisitAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Open Visit", nil) message:finalString delegate:self cancelButtonTitle:NSLocalizedString(@"YES", nil) otherButtonTitles:NSLocalizedString(@"NO", nil), nil];
            
            openVisitAlert.delegate=self;
            
            openVisitAlert.tag=300;
            [openVisitAlert show];
        }
        
    }
    else
    {
    [self startCustomerVisit];
    }
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSLog(@"clicked button index %d with tag %d",buttonIndex,alertView.tag);
    openVisitAlert=nil;
    
    NSLog(@"customer details is %@",customer );

    if (alertView.tag==300 && buttonIndex==0) {
        //yes tapped
        //we are updating visit end date to tbl_fsr__actual visits similary if its a planned visit we should update status flag of TBL_FSR_Planned_Visits, get plannev visit id of actula visit if based on fsr_plan_detail id, fetch planned visit id of open visit
        
        
        NSMutableArray* plannedVisitIDArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Planned_Visit_ID from TBL_FSR_Planned_Visits where FSR_Plan_Detail_ID ='%@'",openPlannedDetailID]];
        
        if (plannedVisitIDArray.count>0) {
            
            openPlannedVisitID=[[plannedVisitIDArray objectAtIndex:0]valueForKey:@"Planned_Visit_ID"];
        }
        
        BOOL plannedVisitStatus;
        
        if ([NSString isEmpty:openPlannedVisitID]==NO) {
              plannedVisitStatus=[[SWDatabaseManager retrieveManager]closePlannedVisitwithVisitID:openPlannedVisitID];
        }
        
        
       
        
        
        BOOL visitEndDateStatus=  [[SWDatabaseManager retrieveManager]updateVisitEndDatewithAlert:openVisitID];
        if (visitEndDateStatus==YES || plannedVisitStatus==YES) {
           
            [self startCustomerVisit];
        }
        
        NSLog(@"visit end time updated");
        
        
        
    }
//    else if (alertView.tag==300 && buttonIndex==1)
//    {
//        //[self.navigationController popViewControllerAnimated:YES];
//    }
}


-(void)cashCustomerSelectedVisitOption:(NSString*)visitOption customer:(SalesWorxVisit*)currentVisit
{
    
    NSLog(@"selected visit option is %@", visitOption);
    
    
    if ([visitOption isEqualToString:kTelephonicVisitTitle]) {
        
        [SWDefaults setOnsiteVisitStatus:NO];
    }
    else if ([visitOption isEqualToString:kOnsiteVisitTitle])
    {
        [SWDefaults setOnsiteVisitStatus:YES];
    }
    else{
        
    }
    
    Singleton *single = [Singleton retrieveSingleton];
    
    if ([currentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"Y"]) {
        single.isCashCustomer = @"Y";
    }
    else{
        single.isCashCustomer=@"N";
    }
    
    [[[SWVisitManager alloc]init]startVisitWithCustomerDetails:currentVisit withParent:self];
    
//    [[[SWVisitManager alloc] init] startVisitWithCustomer:[SWDefaults fetchCurrentCustDict:currentVisit.visitOptions.customer] parent:self andChecker:@"D"];
    
    NSLog(@"check customer dict for cash customer %@",[SWDefaults fetchCurrentCustDict:currentVisit.visitOptions.customer]);
    
    
    NSLog(@"visit status is %hhd", [SWDefaults fetchOnsiteVisitStatus]);
    
    
}

- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
    
    [SWDefaults setCustomer:customer];
    customer = [SWDefaults validateAvailableBalance:orderAmmount];
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isCashCustomer = @"N";
    SWVisitManager *visitManager = [[SWVisitManager alloc] init];
    visitManager.isPlannedVisit=YES;
    [visitManager startVisitWithCustomer:customer parent:self andChecker:@"R"];
    
    orderAmmount=nil;
}

#pragma mark SWRouteService Delegate
- (void)getRouteServiceDidGetRoute:(NSArray *)r
{
    routes=r;
    
    
        
    [_customerTableView reloadData];
    
    [datePickerPopOver dismissPopoverAnimated:YES];
    [self loadVisitStatisticsData];
    r=nil;
}
- (void)locationAction{
    [mapView.mapView setCenterCoordinate:mapView.mapView.userLocation.location.coordinate animated:YES];
}

#pragma mark UITableView DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [routes count];
}
//- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (selectedIndexPath.row==indexPath.row) {
//        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
//        [cell setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
//    }
//    
//    else
//    {
//        [cell.contentView setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
//        [cell setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
//    }
//
//}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"todoCell";
    SalesWorxVisitOptionsToDoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxVisitOptionsToDoTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *row = [routes objectAtIndex:indexPath.row];
    [cell.titleLbl setText:[[row objectForKey:@"Customer_Name"] capitalizedString]];
    
    NSString* visitStatusString=[row valueForKey:@"Visit_Status"];
    if ([visitStatusString isEqualToString:@"Y"])
    {
        [cell.statusView setBackgroundColor:KCompletedVisitColor];
      //  [cell setEditing:NO animated:NO];
    }
    else
    {
      //  [cell setEditing:YES animated:NO];
        [cell.statusView setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(128.0/255.0) blue:(255.0/255.0) alpha:1]];
    }
    
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell.titleLbl setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];

    if (selectedIndexPath.row==indexPath.row) {
        
        cell.titleLbl.textColor=MedRepMenuTitleSelectedCellTextColor;
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        [cell.titleLbl setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        [cell setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];

    }
    
    else
    {
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        cell.titleLbl.textColor=MedRepMenuTitleUnSelectedCellTextColor;
        [cell.contentView setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
        [cell.titleLbl setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];
        [cell setBackgroundColor:MedRepUITableviewDeSelectedCellBackgroundColor];

    }

    return cell;
}

#pragma mark
-(NSMutableArray*)fetchTotalOutStandingforCustomer:(NSString*)selectedCustomerID
{
    NSString * outStandingQry=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"Select IFNULL(Total_Due,'N/A')  AS Total, IFNULL(Over_Due,'N/A') AS Over_Due, IFNULL(PDC_Due,'N/A') AS PDC_Due from TBL_Customer_Dues where Customer_ID = '%@'",selectedCustomerID]];
    
    NSLog(@"total outstanding qry %@", outStandingQry);
    
    NSMutableArray *temp=  [[[SWDatabaseManager retrieveManager] fetchDataForQuery:outStandingQry] mutableCopy];
    
    if (temp.count>0) {
        
        return temp;
    }
    else
    {
        return nil;
    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPathArray.count==0)
    {
        if (indexPath==nil)
        {
            [indexPathArray addObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        else
        {
            [indexPathArray addObject:indexPath];
        }
    }
    else
    {
        [indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
    }
    [_customerTableView reloadData];

    NSDictionary *row = [routes objectAtIndex:indexPath.row];
    selectedIndexPath=indexPath;
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSString *selectDate =[formatter stringFromDate:selectedDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    NSString * visitStatus = [row valueForKey:@"Visit_Status"];
    if ([visitStatus isEqualToString:@"Y"] || (![todayDate isEqualToString:selectDate])) {
        
        btnBarCode.enabled = NO;
        _btnStartVisit.enabled = NO;
        _btnStartVisit.alpha = 0.7;
    }
    else{
        btnBarCode.enabled = YES;
        _btnStartVisit.enabled = YES;
        _btnStartVisit.alpha = 1.0;
    }
    
    
    

    
    customer=[row mutableCopy];
    
    NSMutableArray * beaconDetailsArray=[[SWDatabaseManager retrieveManager]fetchBeaconDetailsforCustomers];
    
    NSLog(@"beacon detaisl array in route %@", beaconDetailsArray);
    
    NSString* beaconCustomerID=[SWDefaults getValidStringValue:[customer valueForKey:@"Customer_ID"]];

    NSString* beaconSiteUseID=[SWDefaults getValidStringValue:[customer valueForKey:@"Site_Use_ID"]];

    [self updateCustomerLastVisitDetails:customer];

    if (beaconDetailsArray.count>0) {
        NSPredicate * matchBeaconPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_ID ==  %@ and SELF.Site_Use_ID ==  %@",beaconCustomerID,beaconSiteUseID]];
        NSMutableArray * matchedBeaconDataArray=[[beaconDetailsArray filteredArrayUsingPredicate:matchBeaconPredicate]mutableCopy];
        
        if (matchedBeaconDataArray.count>0) {
            
            NSLog(@"matched beacon in route %@", matchedBeaconDataArray);
            
            NSMutableDictionary * beaconDict=[matchedBeaconDataArray objectAtIndex:0];
            SalesWorxBeacon * currentCustomerBeacon=[[SalesWorxBeacon alloc]init];
            currentCustomerBeacon.Beacon_UUID=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_UUID"]];
            currentCustomerBeacon.Beacon_Major=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Major"]];
            currentCustomerBeacon.Beacon_Minor=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Minor"]];
            [self scanForiBeacon:currentCustomerBeacon];
        }
    }

    
    
    Singleton *single = [Singleton retrieveSingleton];
    
    _lblName.text = [customer stringForKey:@"Customer_Name"];
    if ([customer objectForKey:@"Customer_No"] != [NSNull null])
    {
        _lblNumber.text = [customer stringForKey:@"Customer_No"];
    }
    else
    {
        _lblNumber.text = @"--";
    }
    if ([customer objectForKey:@"Address"] != [NSNull null])
    {
        _lblAddress.text = [customer stringForKey:@"Address"];
    }
    else
    {
        _lblAddress.text = @"--";
    }
    if ([customer objectForKey:@"Credit_Limit"] != [NSNull null])
    {
        _lblCreditLimit.text = [[NSString stringWithFormat:@"%@",[customer stringForKey:@"Credit_Limit"]] currencyString];
    }
    else
    {
        _lblCreditLimit.text = @"--";
    }
    if ([customer objectForKey:@"Avail_Bal"] != [NSNull null])
    {
        _lblAvailabelBalance.text=[[NSString stringWithFormat:@"%@",[customer stringForKey:@"Avail_Bal"]] currencyString];
    }
    else
    {
        _lblAvailabelBalance.text = @"--";
    }
    @try {
        NSMutableArray * outStandingArray = [self fetchTotalOutStandingforCustomer:[customer stringForKey:@"Customer_ID"]];
        if ([outStandingArray count] >0)
        {
            if ([[[[outStandingArray objectAtIndex:0]valueForKey:@"PDC_Due"]stringValue] isEqualToString:@"0"]) {
                _lblPDCDue.text = @"--";
            } else {
                _lblPDCDue.text = [[ NSString stringWithFormat:@"%@",[[outStandingArray objectAtIndex:0]valueForKey:@"PDC_Due"]]currencyString];
            }
            if ([[[[outStandingArray objectAtIndex:0]valueForKey:@"Over_Due"]stringValue] isEqualToString:@"0"]) {
                _lblOverDue.text = @"--";
            } else {
                _lblOverDue.text = [[NSString stringWithFormat:@"%@",[[outStandingArray objectAtIndex:0]valueForKey:@"Over_Due"]] currencyString];
                [customer setValue:[NSString stringWithFormat:@"%@",[[outStandingArray objectAtIndex:0]valueForKey:@"Over_Due"]] forKey:@"Over_Due"];
            }
            if ([[[[outStandingArray objectAtIndex:0]valueForKey:@"Total"]stringValue] isEqualToString:@"0"]) {
                _lblTotalOutstandings.text = @"--";
            } else {
                _lblTotalOutstandings.text = [[NSString stringWithFormat:@"%@",[[[outStandingArray objectAtIndex:0]valueForKey:@"Total"]stringValue]] currencyString];
            }
        }
        else
        {
            _lblPDCDue.text = @"--";
            _lblOverDue.text = @"--";
            _lblTotalOutstandings.text = @"--";
        }
    }
    @catch (NSException *exception) {
        _lblPDCDue.text = @"--";
        _lblOverDue.text = @"--";
        _lblTotalOutstandings.text = @"--";
    }
    @try {
        NSArray * arrTotalSales = [[SWDatabaseManager retrieveManager] dbGetCustomerTotalSalesWithCustomerID:[customer stringForKey:@"Customer_ID"]];
        if ([arrTotalSales count] >0)
        {
            NSLog(@"%@",[[arrTotalSales objectAtIndex:0]valueForKey:@"Bk0_Amt"]);
            
            if ([[arrTotalSales objectAtIndex:0]valueForKey:@"Bk0_Amt"] == [NSNull null])
            {
                _lblCurrentMonthSales.text = @"--";
            } else {
                _lblCurrentMonthSales.text = [[SWDefaults getValidStringValue:[[arrTotalSales objectAtIndex:0]valueForKey:@"Bk0_Amt"]]currencyString];
            }
        }
        else
        {
            _lblCurrentMonthSales.text = @"--";
        }
    }
    @catch (NSException *exception) {
        _lblCurrentMonthSales.text = @"--";
    }
    
    if ([[customer valueForKey:@"Cust_Status"] isEqualToString:@"Y"]) {
        [statusImageView setBackgroundColor:CustomerActiveColor];
    } else {
        [statusImageView setBackgroundColor:[UIColor redColor]];
    }
    
    single.planDetailId = [row stringForKey:@"FSR_Plan_Detail_ID"];
    [self centerSelectedCustomerOnMapWithLatitude:[customer stringForKey:@"Cust_Lat"] andLongitude:[customer stringForKey:@"Cust_Long"]];
    
}

//-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    SWRouteTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.lblTitle.textColor = [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1];
//    cell.accessoryImage.image = [UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
//    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
//}

#pragma mark
- (void)centerSelectedCustomerOnMapWithLatitude:(NSString *)lat andLongitude:(NSString *)longt{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.07f, 0.07f);
    CLLocationCoordinate2D coordinate = {[lat floatValue],[longt floatValue]};
    MKCoordinateRegion region = {coordinate, span};
    MKCoordinateRegion regionThatFits = [mapView.mapView regionThatFits:region];
    [mapView.mapView setRegion:regionThatFits animated:YES];
    
    NSArray *existingpoints = mapView.mapView.annotations;
    
    if ([existingpoints count])
    {
        [mapView.mapView removeAnnotations:existingpoints];
    }
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = coordinate;
    point.title = [customer objectForKey:@"Customer_Name"];
    
    
    NSString *detailString = [customer objectForKey:@"City"];
    NSString *timeString = [customer objectForKey:@"Visit_Start_Time"];
    
    if ([customer objectForKey:@"Visit_Start_Time"] != [NSNull null])
    {
        NSString *newStr = [timeString substringFromIndex:11];
        detailString = [detailString stringByAppendingString:[NSString stringWithFormat:@"  Time:%@",newStr] ];
        place.otherDetail = detailString;
    }
    [mapView.mapView addAnnotation:point];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* custLatValue;
    NSString* custLongValue;
    if (customer) {
        custLatValue =[NSString stringWithFormat:@"%@",[customer valueForKey:@"Cust_Lat"]];
        custLongValue=[NSString stringWithFormat:@"%@",[customer valueForKey:@"Cust_Long"]];
    }
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        NSString* googleMapsUrlString;
        
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication]openURL:googleMapsUrl];
            
        }
    }
    
    else
    {
        if (custLatValue.length>0 && custLongValue.length>0) {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[custLatValue doubleValue],[custLongValue doubleValue]]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
        else
        {
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
    }
}

#pragma Barcode Functions
- (void)btnBarCode:(id)sender
{
    if ([_lblName.text isEqualToString:@"--"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No data available for scanning" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else
    {
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(authStatus == AVAuthorizationStatusAuthorized)
        {
            Singleton *single = [Singleton retrieveSingleton];
            single.isBarCode = YES;
            reader = [ZBarReaderViewController new];
            reader.readerDelegate = self;
            [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
            
            UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
            customOverlay.backgroundColor = [UIColor clearColor];
            customOverlay.opaque = NO;
            
            UIToolbar *toolbar = [[UIToolbar alloc] init];
            toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
            toolbar.barStyle = UIBarStyleBlackTranslucent;
            
            
            UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(barButtonBackPressed:)];
            NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
            
            [toolbar setItems:arr animated:YES];
            [customOverlay addSubview:toolbar];
            reader.cameraOverlayView =customOverlay ;
            
            // reader.wantsFullScreenLayout = NO;
            reader.readerView.zoom = 1.0;
            reader.showsZBarControls=NO;
            
            [self presentViewController: reader animated: YES completion:nil];

        } else if(authStatus == AVAuthorizationStatusDenied)
        {
            // denied
            NSString *alertText;
            NSString *alertButton;
            
            BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
                
                alertButton = @"Go";
            }
            else
            {
                alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
                
                alertButton = @"OK";
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:alertText delegate:self cancelButtonTitle:alertButton otherButtonTitles:nil];
            [alert show];
        }
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag==300) {
        
    }
    
    else
    {
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil] ;
}

#pragma mark - ZBar's Delegate method
- (void)imagePickerController: (UIImagePickerController*) reader2 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    [reader dismissViewControllerAnimated: YES completion:nil];

    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    for(symbol in results){
        single.valueBarCode=symbol.data;
        // NSString *upcString = symbol.data;
        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init];
        [locationFilterController setTarget:self];
        [locationFilterController setAction:@selector(filterChangedBarCode)];
        [locationFilterController selectionDone:self];
        single.isBarCode = NO;
    }
}
- (void)filterChangedBarCode
{
    [self.navigationController setToolbarHidden:YES animated:YES];
    [self getListFunction:[[SWDatabaseManager retrieveManager] fetchCutomerDetailsForBarCode:[SWDefaults barCodeFilterForCustomerList]]];
}
- (void)getListFunction:(NSArray *)cl
{
    if([cl count]!=0)
    {
       // SWSection *section = [cl objectAtIndex:0];
        NSDictionary *row = [cl objectAtIndex:0];
        customer = [row mutableCopy] ;
        BOOL isInList=NO;
        for (int i=0; i<[routes count]; i++)
        {
            @autoreleasepool {
            NSDictionary *row1 = [routes objectAtIndex:i];

            if ([[row1 stringForKey:@"Customer_Name"] isEqualToString:[row stringForKey:@"Customer_Name"]]) {
                isInList=YES;
                [self tableView:_customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            }

        }
        if (isInList){
            [self performSelector:@selector(btnStartVisit:) withObject:nil afterDelay:1.0];
            //[self Login_Btn_Clicked:self];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:@"%@ is not in today's route list",[row stringForKey:@"Customer_Name"]] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Barcode does not match customer. Please try again" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil)      otherButtonTitles:nil];
        [alert show];
    }
    cl=nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Background refresh method

-(void)refreshData
{
    selectedDate=[NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [_btnDate setTitle:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[formatter stringFromDate:selectedDate]] forState:UIControlStateNormal];
    
    [[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate];
    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
    
    if (routes.count>0) {
        if (selectedIndexPath)
        {
            [_customerTableView selectRowAtIndexPath:selectedIndexPath animated:YES scrollPosition:0];
            [self tableView:_customerTableView didSelectRowAtIndexPath:selectedIndexPath];
        }
        else
        {
            [_customerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
            [self tableView:_customerTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
}

- (void)GetRouteDataFromBackground:(NSNotification *)notification{
    
    NSLog(@"application entered foreground");
    
        //routeSer.delegate=self;
    [self refreshData];
    }


#pragma mark Beacon Methods
-(void)scanForiBeacon:(SalesWorxBeacon*)currentCustomerBeacon
{
    NSLog(@"scanning for iBeacon in customers");
    
    [[SalesWorxiBeaconManager retrieveSingleton]initilizeiBeaconManager:currentCustomerBeacon];
    
    NSLog(@"check detected beacon manager %@",[SalesWorxiBeaconManager retrieveSingleton].detectedBeacon );
}
#pragma mark customer last visit
-(void)updateCustomerLastVisitDetails:(NSDictionary *)customerDic{
    __block NSString *lastVisitedDateStr=[[NSString alloc] init];
    lastVisitedDateLabel.text=KNotApplicable;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            Customer *cust=[[Customer alloc]init];
           cust.Customer_ID =[SWDefaults getValidStringValue:[customerDic valueForKey:@"Customer_ID"]];
            cust.Ship_Customer_ID =[SWDefaults getValidStringValue:[customerDic valueForKey:@"Customer_ID"]];

           cust.Ship_Site_Use_ID=[SWDefaults getValidStringValue:[customerDic valueForKey:@"Ship_Site_Use_ID"]];
            lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:cust];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            lastVisitedDateLabel.text=lastVisitedDateStr.length==0?KNotApplicable:lastVisitedDateStr;
        });
    });
    
    
}
@end
