//
//  MedRepMenuViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/27/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepMenuViewController.h"
#import "Category.h"
#import "SWDefaults.h"
#import "SectionInfo.h"
#import "MenuItem.h"
#import "MenuTableViewCell.h"
#import "SWSplitViewController.h"
#import "SWProductListViewController.h"
#import "SWCollectionCustListViewController.h"
#import "SalesWorxReportsViewController.h"
#import "SalesWorxRouteViewController.h"
#import "SettingViewController.h"
#import "ReturnNewViewController.h"
#import "MedRepDoctorsViewController.h"
#import "MedRepPharmaciesViewController.h"
#import "MedRepProductsViewController.h"
#import "MedRepVisitsViewController.h"
#import "MedRepVisitsListViewController.h"
#import "SalesWorxPaymentCollectionViewController.h"
#import "LoginViewController.h"
#import "SWCustomersViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SalesWorxSyncPopoverViewController.h"
#import "SalesWorxProductsViewController.h"
#import "SWDashboardViewController.h"
#import "SalesWorxDashboardViewController.h"
#import "MBProgressHUD.h"
#import "SalesWorxAboutUsPopoverViewController.h"
#import "BrandAmbassadorDashboardViewController.h"
#import "SWRouteViewController.h"
#import "SalesWorxFieldSalesProductsViewController.h"
#import "SalesWorxMerchandisingViewController.h"
#import "MedRep-Swift.h"


#define KCommonMenuOptionsTitle @""
@interface MedRepMenuViewController ()

@end

@implementation MedRepMenuViewController

@synthesize openSectionIndex,sectionInfoArray,categoryList,salesOrderMenuArray,medRepMenuArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor blueColor];
    
    selectedCellIndexpathArray=[[NSMutableArray alloc]init];
    
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Menu", nil)];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    
    
       self.medRepMenuTableview.sectionHeaderHeight = 45;
    self.medRepMenuTableview.sectionFooterHeight = 0;
    self.openSectionIndex = NSNotFound;
    [self.view setBackgroundColor:UIViewControllerBackGroundColor];

    app=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    
    // Do any additional setup after loading the view from its nib.
}
- (void)configureToolbar {
    
    UIButton* infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoButton addTarget:self action:@selector(infoButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    
//    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_Sync" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(synchronizeAction:)] ;
    
    
    UIBarButtonItem *syncButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_Sync"] style:UIBarButtonItemStylePlain target:self action:@selector(displaySyncPopover)] ;
    
   // UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SettingIcon" cache:NO] style:UIBarButtonItemStylePlain target:self action:@selector(settingAction:)] ;
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_Logout"] style:UIBarButtonItemStylePlain target:self action:@selector(logoutTapped)] ;
    
    
    
    [self.navigationController setToolbarHidden:NO animated:YES];
    [self setToolbarItems:[NSArray arrayWithObjects:modalButton,flexibleSpace,syncButton,flexibleSpace,logoutButton, nil] animated:YES];
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]];
        PDAComponents=[PDARIGHTS componentsSeparatedByString:@","];
        
        if([PDAComponents containsObject:KBO_URLRightsCode]){
            [self configureBackOfficeURl];
            boURLHeightConstraint.constant = 48;
        }else{
            boURLHeightConstraint.constant = 0;
        }
    }else{
        boURLHeightConstraint.constant = 0;
    }
    
}

-(void)configureBackOfficeURl{
 
    UITapGestureRecognizer* boGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backofficeUrlButtonTapped)];
    boGesture.numberOfTapsRequired = 1;
    boUrlView.userInteractionEnabled = YES;
    boUrlView.backgroundColor = kNavigationBarBackgroundColor;
    [boUrlView addGestureRecognizer:boGesture];
    
}
-(void)backofficeUrlButtonTapped{
    NSString* bourl = [[SWDatabaseManager retrieveManager]fetchBackEndURL];
    NSURL * backendURl = [NSURL URLWithString:bourl];
    if ([[UIApplication sharedApplication]canOpenURL:backendURl]){
        [[UIApplication sharedApplication]openURL:backendURl];
    }else{
        NSLog(@"unable to open BO url tapped %@",bourl);
    }
}

-(void)displaySyncPopover
{
    SalesWorxSyncPopoverViewController *presentingView=   [[SalesWorxSyncPopoverViewController alloc]init];
    UINavigationController * syncNavController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    syncNavController.navigationBarHidden=YES;
    presentingView.syncPopdelegate=self;
    syncNavController.view.backgroundColor = KPopUpsBackGroundColor;
    syncNavController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:syncNavController animated:NO completion:nil];
}
#pragma mark tool bar button actions


-(void)logoutTapped

{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                   message:@"Are you sure you want to log out?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self logout];
                                                          }];
    
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)logout{
    NSLog(@"log out tapped");
    [SWDefaults UpdateGoogleAnalyticsforScreenName:@"Logout"];
    
    
    UINavigationController *navigationControllerFront;
    
    
    
    Singleton *single = [Singleton retrieveSingleton];
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    
    LoginViewController * loginVC=[[LoginViewController alloc]init];
    
    
    //        dashBViewController = [[SWDashboardViewController alloc] init];
    //        navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:dashBViewController] ;
    
    navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
    
    
    
    
    if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[LoginViewController class]])
    {
        [revealController setFrontViewController:navigationControllerFront animated:YES];
        
    } else {
        [revealController revealToggle:self];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionLogout" object:nil];
}

-(void)infoButtonAction
{
    SalesWorxAboutUsPopoverViewController *presentingView = [[SalesWorxAboutUsPopoverViewController alloc]init];
    presentingView.SalesWorxAboutUsPopoverViewControllerDelegate = self;
    UINavigationController *syncNavController = [[UINavigationController alloc]initWithRootViewController:presentingView];
    syncNavController.navigationBarHidden=YES;
    syncNavController.view.backgroundColor = KPopUpsBackGroundColor;
    syncNavController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:syncNavController animated:NO completion:nil];

    
//    //Singleton *single = [Singleton retrieveSingleton];
//    //revealController=nil;
//    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
//    //    single.splitViewObject=nil;
//    //    single.splitViewObject = revealController;
//    
//    
//    
//    if (self.categoryList != nil)
//    {
//        
//        
//        AboutUsViewController * aboutusVC=[[AboutUsViewController alloc]init];
//        
//        UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:aboutusVC] ;
//        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[aboutusVC class]])
//        {
//            [revealController setFrontViewController:navigationControllerFront animated:YES];
//            
//        }
//        else
//        {
//            [revealController revealToggle:self];
//        }
//    }
}
- (void)synchronizeAction:(id)sender
{
    //single=nil;
    // Singleton *single = [Singleton retrieveSingleton];
    //revealController=nil;
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    //    single.splitViewObject=nil;
    //    single.splitViewObject = revealController;
    
    if (self.categoryList != nil)
    {
        if(synch)
        {
            synch=nil;
        }
        synch = [[SynViewController alloc] init] ;
        [synch setTarget:nil];
        [synch setAction:nil];
        
        UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:synch] ;
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[synch class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
    
}
- (void)settingAction:(id)sender
{
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (self.categoryList != nil)
    {
        
        
        SettingViewController* settingsVC=[[SettingViewController alloc]init];
        
        
        UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:settingsVC] ;
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[settingsVC class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
}


//-(void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    
//        self.navigationController.delegate = nil;
//    }

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.isMovingToParentViewController) {
        [self updateMenu];
    }
}
-(void)updateMenu
{
    appControl=[AppControl retrieveSingleton];
    NSString* collectionFlag=appControl.ENABLE_COLLECTIONS;
    salesOrderMenuArray=[[NSMutableArray alloc]init];
    medRepMenuArray=[[NSMutableArray alloc]init];
    othersMenuArray=[[NSMutableArray alloc]init];
    
    BOOL isCoachSurveyAvailable = NO;
    NSString *empID = [[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
    
    
    NSMutableArray *arrOfCoachSurvey = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Survey where Survey_Type_Code IN('C','Q') and SalesRep_ID= '%@' and (date('now') BETWEEN date(Start_Time) and date(End_Time))",empID]];
    
    if (arrOfCoachSurvey.count > 0) {
        isCoachSurveyAvailable = YES;
    }
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]];
        PDAComponents=[PDARIGHTS componentsSeparatedByString:@","];
        NSLog(@"PDAComponents %@",PDAComponents);
        
        if([PDAComponents containsObject:KFieldSalesSectionPDA_RightsCode])
        {
            
            
            if([PDAComponents containsObject:KFieldSalesDashBoardPDA_RightCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"SalesWorxFieldSalesDashboardViewController" imageName:@"Menu_Dashboard"]];
            if([PDAComponents containsObject:KFieldSalesROUTEPDA_RightCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SalesWorxRouteViewController" imageName:@"Menu_Route"]];
            if([PDAComponents containsObject:KFieldSalesCustomersPDA_RightCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"SWCustomersViewController" imageName:@"Menu_Customer"]];
            if([PDAComponents containsObject:KFieldSalesProductsPDA_RightCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SalesWorxFieldSalesProductsViewController" imageName:@"Menu_Products"]];
            if([PDAComponents containsObject:KPaymentCollectionPDA_RightsCode] && [collectionFlag isEqualToString:KAppControlsYESCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Collection", nil) className:@"SWCollectionCustListViewController" imageName:@"Menu_Collection"]];
            if([PDAComponents containsObject:KSurveyPDA_RightsCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"SalesWorxSurveyViewController" imageName:@"Menu_Survey"]];
        }
        
//         [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Brand Ambassadors", nil) className:@"SalesWorxBrandAmbassadorsViewController" imageName:@"Menu_Visits"]];
//        
        
        //menu items for med rep
        if([PDAComponents containsObject:KFieldMarketingSectionPDA_RightsCode] && [SWDefaults isMedRep])
        {
            medRepMenuArray=[[NSMutableArray alloc]init];
            

            
             if([PDAComponents containsObject:KFieldMarketingDashBoardPDA_RightsCode] ||
                [PDAComponents containsObject:KFieldMarketingVisitsPDA_RightsCode])
            {
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"SalesWorxDashboardViewController" imageName:@"Menu_Dashboard"]];
            }
            else if([PDAComponents containsObject:KBrandAmbassadorVisitsPDA_RightsCode])
            {
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"BrandAmbassadorDashboardViewController" imageName:@"Menu_Dashboard"]];
            }
            else
            {
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"SalesWorxDashboardViewController" imageName:@"Menu_Dashboard"]];
            }
            
            if([PDAComponents containsObject:KFieldMarketingdoctorsPDA_RightsCode])
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Doctors", nil) className:@"MedRepDoctorsViewController" imageName:@"Menu_Doctors"]];
            if([PDAComponents containsObject:KFieldMarketingPharmaciesPDA_RightsCode])
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Pharmacies", nil) className:@"MedRepPharmaciesViewController" imageName:@"Menu_Pharmacies"]];
            if([PDAComponents containsObject:KFieldMarketingProductsPDA_RightsCode])
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"MedRepProductsViewController" imageName:@"Menu_FieldMarketing_Products"]];
            
            
            
            if([PDAComponents containsObject:KFieldMarketingVisitsPDA_RightsCode])
            {
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Visits", nil) className:@"MedRepVisitsListViewController" imageName:@"Menu_Visits"]];
            }
            else if([PDAComponents containsObject:KBrandAmbassadorVisitsPDA_RightsCode])
            {
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Brand Ambassadors", nil) className:@"SalesWorxBrandAmbassadorsViewController" imageName:@"Menu_Visits"]];
            }
            if([PDAComponents containsObject:KCoachReportPDA_RightsCode] && isCoachSurveyAvailable)
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Coach Report", nil) className:@"SalesWorxCoachReportPopoverViewController" imageName:@"Menu_Survey"]];
        }
        
        if(salesOrderMenuArray.count>0 && medRepMenuArray.count>0)
        {
            if([PDAComponents containsObject:KCommunicationSectionPDA_RightsCode])
                [othersMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"]];
            if([PDAComponents containsObject:KReportsSectionPDA_RightsCode])
                [othersMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"]];
        }
        else  if(medRepMenuArray.count>0)
        {
            if([PDAComponents containsObject:KCommunicationSectionPDA_RightsCode])
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"]];
            if([PDAComponents containsObject:KReportsSectionPDA_RightsCode])
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"]];
        }
        else  if(salesOrderMenuArray.count>0)
        {
            if([PDAComponents containsObject:KCommunicationSectionPDA_RightsCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"]];
            if([PDAComponents containsObject:KReportsSectionPDA_RightsCode])
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"]];
        }
        else
        {
            /** not showing communication and reports */
        }
        
        if([PDAComponents containsObject:kMerchandisingPDA_RightsCode])
        {
        [othersMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Merchandising", nil) className:@"SalesWorxMerchandisingViewController" imageName:@"Menu_Merchandising"]];
        }
    }
    else
    {
        
        salesOrderMenuArray=[[NSMutableArray alloc]init];
        [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"SalesWorxFieldSalesDashboardViewController" imageName:@"Menu_Dashboard"]];
        [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SalesWorxRouteViewController" imageName:@"Menu_Route"]];
        [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"SWCustomersViewController" imageName:@"Menu_Customer"]];
        
        
        [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SalesWorxFieldSalesProductsViewController" imageName:@"Menu_Products"]];
        if([collectionFlag isEqualToString:KAppControlsYESCode])
        {
            [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Collection", nil) className:@"SWCollectionCustListViewController" imageName:@"Menu_Collection"]];
        }
        
        
        [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"SalesWorxSurveyViewController" imageName:@"Menu_Survey"]];
        
        if([SWDefaults isMedRep])
        {
            medRepMenuArray=[[NSMutableArray alloc ]initWithObjects:
                             [MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"SalesWorxDashboardViewController" imageName:@"Menu_Dashboard"],
                             
                             [MenuItem itemWithTitle:NSLocalizedString(@"Doctors", nil) className:@"MedRepDoctorsViewController" imageName:@"Menu_Doctors"],
                             
                             [MenuItem itemWithTitle:NSLocalizedString(@"Pharmacies", nil) className:@"MedRepPharmaciesViewController" imageName:@"Menu_Pharmacies"],
                             
                             [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"MedRepProductsViewController" imageName:@"Menu_FieldMarketing_Products"],
                             
                             
                             [MenuItem itemWithTitle:NSLocalizedString(@"Visits", nil) className:@"MedRepVisitsListViewController" imageName:@"Menu_Visits"],nil];
            
            
            if (isCoachSurveyAvailable) {
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Coach Report", nil) className:@"SalesWorxCoachReportPopoverViewController" imageName:@"Menu_Survey"]];
            }
 
        }
        if(salesOrderMenuArray.count>0 && medRepMenuArray.count>0)
        {
                [othersMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"]];
                [othersMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"]];
        }
        else  if(medRepMenuArray.count>0)
        {
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"]];
                [medRepMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"]];
        }
        else  if(salesOrderMenuArray.count>0)
        {
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"]];
                [salesOrderMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"SalesWorxReportsViewController" imageName:@"Menu_Reports"]];
        }
        if([PDAComponents containsObject:kMerchandisingPDA_RightsCode])
        {
        [othersMenuArray addObject:[MenuItem itemWithTitle:NSLocalizedString(@"Merchandising", nil) className:@"SalesWorxMerchandisingViewController" imageName:@"Menu_Merchandising"]];
        }
        
    }
    
    
    [self setCategoryArray];
    
    self.medRepMenuTableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    
    if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.medRepMenuTableview])) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (CategoryLegacy *cat in self.categoryList) {
            SectionInfo *section = [[SectionInfo alloc] init];
            section.category = cat;
            section.open = NO;
            NSNumber *defaultHeight = [NSNumber numberWithInt:44];
            
            if([cat.name isEqualToString:KCommonMenuOptionsTitle])
            {
                section.isAlwaysOpen=YES;
                // section.open = YES;
                defaultHeight=[NSNumber numberWithInt:0];
            }
            NSInteger count = [[section.category list] count];
            for (NSInteger i= 0; i<count; i++) {
                [section insertObject:defaultHeight inRowHeightsAtIndex:i];
            }
            
            [array addObject:section];
        }
        self.sectionInfoArray = array;
    }
    
    for (int i = 0; i < self.categoryList.count; i++) {
        
        CategoryLegacy *cat = self.categoryList[i];
        SectionInfo *section = self.sectionInfoArray[i];
        if (cat.list.count != section.rowHeights.count) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (CategoryLegacy *cat in self.categoryList) {
                SectionInfo *section = [[SectionInfo alloc] init];
                section.category = cat;
                section.open = NO;
                NSNumber *defaultHeight = [NSNumber numberWithInt:44];
                
                if([cat.name isEqualToString:KCommonMenuOptionsTitle])
                {
                    section.isAlwaysOpen=YES;
                    // section.open = YES;
                    defaultHeight=[NSNumber numberWithInt:0];
                }
                NSInteger count = [[section.category list] count];
                for (NSInteger i= 0; i<count; i++) {
                    [section insertObject:defaultHeight inRowHeightsAtIndex:i];
                }
                
                [array addObject:section];
            }
            self.sectionInfoArray = array;
            break;
        }
    }
    
    [self configureToolbar];
}
-(void)viewDidAppear:(BOOL)animated
{
    if(self.sectionInfoArray.count>0 && self.isMovingToParentViewController)
    {
//        SectionInfo *array  = [self.sectionInfoArray objectAtIndex:0];
//        [array.sectionView toggleButtonPressed:TRUE];
        [_medRepMenuTableview reloadData];
    }


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void) setCategoryArray
{

    NSMutableArray *categoryArray = [[NSMutableArray alloc]init];
    NSMutableArray* tempArray=[[NSMutableArray alloc]init];
    NSMutableDictionary* salesOrderDict=[[NSMutableDictionary alloc]init];
    
    if(salesOrderMenuArray.count>0)
    {
        [salesOrderDict setObject:medRepMenuArray.count>0?NSLocalizedString(@"Field Sales", nil):KCommonMenuOptionsTitle  forKey:@"name"];
        
        [salesOrderDict setObject:salesOrderMenuArray forKey:@"list"];
        
        [tempArray addObject:salesOrderDict];
    }
    //second section
    //we can enable or disable medrep here
    if(medRepMenuArray.count>0)
    {
        NSMutableDictionary* medRepDict=[[NSMutableDictionary alloc]init];
        [medRepDict setObject:salesOrderMenuArray.count>0?NSLocalizedString(@"Field Marketing", nil):KCommonMenuOptionsTitle forKey:@"name"];
        [medRepDict setObject:medRepMenuArray forKey:@"list"];
        if(![appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode] || ([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode]&&[PDAComponents containsObject:KFieldMarketingSectionPDA_RightsCode]))
        {
            [tempArray addObject:medRepDict];
        }
    }
    
    
    NSMutableDictionary* OthersMenuDict=[[NSMutableDictionary alloc]init];
    [OthersMenuDict setObject:KCommonMenuOptionsTitle forKey:@"name"];
    [OthersMenuDict setObject:othersMenuArray forKey:@"list"];
    if(othersMenuArray.count>0)
    {
        [tempArray addObject:OthersMenuDict];

    }

    
    for (NSDictionary *dictionary in tempArray) {
        CategoryLegacy *category = [[CategoryLegacy alloc] init];
        category.name = [dictionary objectForKey:@"name"];
        category.list = [dictionary objectForKey:@"list"];
        [categoryArray addObject:category];
    }
    

    
    
    
    
    self.categoryList = categoryArray;
    
    
}



#pragma mark UITableViewMethods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.categoryList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:section];
    NSInteger rows = [[array.category list] count];
    return (array.open || array.isAlwaysOpen) ? rows : 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryLegacy *category = (CategoryLegacy *)[self.categoryList objectAtIndex:indexPath.section];

    
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
//    static NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    
//    Category *category = (Category *)[self.categoryList objectAtIndex:indexPath.section];
//    
//    MenuItem* menuItem=[category.list objectAtIndex:indexPath.row];
//    
//    cell.textLabel.text = menuItem.title;
//    return cell;
//    
    
    NSString *identifier = @"CellIdentifier";
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[MenuTableViewCell alloc] initWithReuseIdentifier:identifier] ;
        
    }
    CategoryLegacy *category = (CategoryLegacy *)[self.categoryList objectAtIndex:indexPath.section];
    MenuItem *menuItem = [category.list objectAtIndex:indexPath.row];

    [cell setImageName:menuItem.imageName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    if ([selectedCellIndexpathArray containsObject:indexPath])
//    {
//        cell.textLabel.textColor= MedRepMenuTitleSelectedCellTextColor;
//    }
//    else
//    {
//        cell.textLabel.textColor= MedRepMenuTitleUnSelectedCellTextColor;
//
//    }

    if([app.selectedMenuIndexpath isEqual:indexPath])
    {
        //[cell setImageName:[NSString stringWithFormat:@"%@_selected", menuItem.imageName]];
        [cell setImageName:menuItem.imageName];


        cell.textLabel.font=kSWX_FONT_SEMI_BOLD(16);
        [cell.textLabel setText:menuItem.title];
        cell.backgroundColor=MedRepUITableviewSelectedCellBackgroundColor;
        cell.textLabel.textColor=MedRepMenuTitleSelectedCellTextColor;

    }
    else
    {
        
        [cell setImageName:menuItem.imageName];

        cell.textLabel.font=kSWX_FONT_SEMI_BOLD(16);
        cell.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
        [cell.textLabel setText:menuItem.title];
        cell.textLabel.textColor=MedRepMenuTitleUnSelectedCellTextColor;

    }
    
    [tableView setSeparatorColor:[UIColor clearColor]];

    return cell;
    
 }
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    [tableView setSeparatorColor:[UIColor clearColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([selectedCellIndexpathArray containsObject:indexPath]) {
        
        [selectedCellIndexpathArray removeObject:indexPath];
    }
    else{
        [selectedCellIndexpathArray addObject:indexPath];
    }
    
   // [tableView reloadData];
    
    NSLog(@"selected cell indexpath array is %@",selectedCellIndexpathArray);
    
    
    
    MenuTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = MedRepMenuTitleSelectedCellTextColor;
    
    NSLog(@"selected cell called");
    

    
    
    synch=nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    Singleton *single = [Singleton retrieveSingleton];
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    
    CategoryLegacy *category = (CategoryLegacy *)[self.categoryList objectAtIndex:indexPath.section];

    MenuItem *menuItem = [category.list objectAtIndex:indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *className = menuItem.className;
    app.selectedMenuIndexpath=indexPath;
    /*
    if([className isEqualToString:@"Logout"])
    {
        
        UINavigationController *navigationControllerFront;
        
     
            dashBViewController = [[SalesWorxFieldSalesDashboardViewController alloc] init] ;
            navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:dashBViewController] ;
      
        
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[dashBViewController class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
            
        } else {
            [revealController revealToggle:self];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionLogout" object:nil];
    }*/
    
    
     if (category.list != nil)
    {
        UIViewController *viewController = nil;
        
        
        
        if([className isEqualToString:@"SalesWorxFieldSalesDashboardViewController"])
        {
            if (single.isFullSyncDashboard) {
            single.isFullSyncDashboard=NO;
//                SWDashboardViewController* dashboardVC=[[SWDashboardViewController alloc]init];
                
               // [self setSliderRootViewController:dashboardVC];

            }
                    
            SalesWorxFieldSalesDashboardViewController* dashboardVC=[[SalesWorxFieldSalesDashboardViewController alloc]init];
            
            [self setSliderRootViewController:dashboardVC];
            
        }
        
        
        else if([className isEqualToString:@"CommunicationViewControllerNew"])
        {
            
            
            CommunicationViewControllerNew* communicationsVC;
            
            if (single.isFullSyncMessage) {
                 communicationsVC=[[CommunicationViewControllerNew alloc]init];
                
                //[self setSliderRootViewController:communicationsVC];
                
                single.isFullSyncMessage=NO;
            }
            communicationsVC=[[CommunicationViewControllerNew alloc]init];
            
            [self setSliderRootViewController:communicationsVC];
            
            
            /*
            SalesWorxCommunicationsParentViewController *communicationsVC;
            
            if (single.isFullSyncMessage) {
                communicationsVC=[[SalesWorxCommunicationsParentViewController alloc]init];
                
                //[self setSliderRootViewController:communicationsVC];
                
                single.isFullSyncMessage=NO;
            }
            communicationsVC=[[SalesWorxCommunicationsParentViewController alloc]init];
            
            [self setSliderRootViewController:communicationsVC];*/

        
        }
        //        else if([className isEqualToString:@"GenCustListViewController"])
        else if([className isEqualToString:@"SalesWorxSurveyViewController"])
        {
            //            GenCustListViewController *genCustList=[[GenCustListViewController alloc]init];
            SalesWorxSurveyViewController *genCustList=[[SalesWorxSurveyViewController alloc]init];
            
            if (single.isFullSyncSurvey)
            {
                SurveyFormViewController1=nil;
                //[self setSliderRootViewController:genCustList];
                single.isFullSyncSurvey=NO;
            }
            [self setSliderRootViewController:genCustList];
        }
        else if([className isEqualToString:@"SalesWorxRouteViewController"])
        {
//            SWRouteViewController* routeVC=[[SWRouteViewController alloc]init];
            SalesWorxRouteViewController *routeVC = [[SalesWorxRouteViewController alloc]init];
            [self setSliderRootViewController:routeVC];
        }
        
        
        
        else if([className isEqualToString:@"SalesWorxFieldSalesProductsViewController"])

        //else if([className isEqualToString:@"SWProductListViewController"])
        {
            SalesWorxFieldSalesProductsViewController * productVC=[[SalesWorxFieldSalesProductsViewController alloc]init];
            
           // SWProductListViewController * productVC=[[SWProductListViewController alloc]init];
            if (single.isFullSyncProduct) {

                
               // [self setSliderRootViewController:productVC];
                
                single.isFullSyncProduct=NO;
            }
            [self setSliderRootViewController:productVC];
        }
        
       // else if([className isEqualToString:@"CustomersListViewController"])
        
        else if([className isEqualToString:@"SWCustomersViewController"])
        {
           // CustomersListViewController * custList=[[CustomersListViewController alloc]init];
            SWCustomersViewController * custList=[[SWCustomersViewController alloc]init];
            
            
            
            if (single.isFullSyncCustomer) {
              //  [self setSliderRootViewController:custList];

                single.isFullSyncCustomer=NO;
            }
            
            [self setSliderRootViewController:custList];
        }
        
        else if([className isEqualToString:@"SalesWorxReportsViewController"])
        {
            
            SalesWorxReportsViewController* reportVC =[[SalesWorxReportsViewController alloc]init];
            //SurveySwiftViewController * reportVC = [[SurveySwiftViewController alloc]init];

            [self setSliderRootViewController:reportVC];
            
        }
        else if([className isEqualToString:@"SalesWorxCoachReportPopoverViewController"])
        {
            
//            BOOL surveyCompletedForToday=[[SWDatabaseManager retrieveManager]surveyCompletedForToday];
//            if (surveyCompletedForToday) {
//                [SWDefaults showAlertAfterHidingKeyBoard:@"Today's Survey Completed" andMessage:@"Only one survey allowed per day" withController:self];
//            }
//            else{
                
                SalesWorxCoachReportPopoverViewController *reportVC = [[SalesWorxCoachReportPopoverViewController alloc]init];
                [self setSliderRootViewController:reportVC];
            /*
            SalesWorxCoachReportPopoverViewController *coachReportVC = [[SalesWorxCoachReportPopoverViewController alloc]init];
            coachReportVC.CoachReportDelegate = self;
            UINavigationController *syncNavController = [[UINavigationController alloc]initWithRootViewController:coachReportVC];
            syncNavController.navigationBarHidden = YES;
            syncNavController.view.backgroundColor = KPopUpsBackGroundColor;
            syncNavController.modalPresentationStyle = UIModalPresentationCustom;
            coachReportVC.view.backgroundColor = KPopUpsBackGroundColor;
            coachReportVC.modalPresentationStyle = UIModalPresentationCustom;
            self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
            [self presentViewController:syncNavController animated:NO completion:nil];*/
                
                
           // }
            
        }
        else if([className isEqualToString:@"SWCollectionCustListViewController"])
        {
            
            
            
            SalesWorxPaymentCollectionViewController* collectionVC=[[SalesWorxPaymentCollectionViewController alloc]init];
            
            if (single.isFullSyncCollection)

            {
                single.isFullSyncCollection=NO;

            }
            [self setSliderRootViewController:collectionVC];
            
//            SalesWorxPaymentCollectionViewController * collectionVC=[[SalesWorxPaymentCollectionViewController alloc]init];
//            
//            if (single.isFullSyncCollection)
//            {
//                [self setSliderRootViewController:collectionVC];
//                
//                single.isFullSyncCollection=NO;
//            }
//            [self setSliderRootViewController:collectionVC];
//
            
            
//            SWCollectionCustListViewController* collectionVC=[[SWCollectionCustListViewController alloc]init];
//            if (single.isFullSyncCollection)
//            {
//                [self setSliderRootViewController:collectionVC];
//
//                single.isFullSyncCollection=NO;
//            }
//            [self setSliderRootViewController:collectionVC];
            
        }
        
        //med rep view controllers
        
        else if ([className isEqualToString:@"SalesWorxDashboardViewController"])
        {
            SalesWorxDashboardViewController* medRepDashBoard=[[SalesWorxDashboardViewController alloc]init];
            [self setSliderRootViewController:medRepDashBoard];
        }
        else if ([className isEqualToString:@"MedRepVisitsListViewController"])
        {
            
//            MedRepVisitsViewController* medRepVisits=[[MedRepVisitsViewController alloc]init];
//            [self setSliderRootViewController:medRepVisits];
            
            MedRepVisitsListViewController* medRepVisits=[[MedRepVisitsListViewController alloc]init];
            [self setSliderRootViewController:medRepVisits];
        }
        
        else if ([className isEqualToString:@"MedRepDoctorsViewController"])
        {
            
            MedRepDoctorsViewController* medRepDoctors=[[MedRepDoctorsViewController alloc]init];
            [self setSliderRootViewController:medRepDoctors];
        }
        
        else if ([className isEqualToString:@"MedRepPharmaciesViewController"])
        {
            
            MedRepPharmaciesViewController* medRepPharmacies=[[MedRepPharmaciesViewController alloc]init];
            [self setSliderRootViewController:medRepPharmacies];
        }
        else if ([className isEqualToString:@"MedRepProductsViewController"])
        {
            
            MedRepProductsViewController* medRepProducts=[[MedRepProductsViewController alloc]init];
            [self setSliderRootViewController:medRepProducts];
        }
        else if ([className isEqualToString:@"SalesWorxBrandAmbassadorsViewController"])
        {
            SalesWorxBrandAmbassadorsViewController * brandAmbassadors=[[SalesWorxBrandAmbassadorsViewController alloc]init];
            [self setSliderRootViewController:brandAmbassadors];
        }
        else if ([className isEqualToString:@"BrandAmbassadorDashboardViewController"])
        {
            BrandAmbassadorDashboardViewController * brandAmbassadors=[[BrandAmbassadorDashboardViewController alloc]init];
            [self setSliderRootViewController:brandAmbassadors];
        }
        else if([className isEqualToString:@"SalesWorxMerchandisingViewController"])
        {
            SalesWorxMerchandisingViewController *merchVC = [[SalesWorxMerchandisingViewController alloc]init];
            [self setSliderRootViewController:merchVC];
        }
    }
    [tableView reloadData];
}





-(void)setSliderRootViewController:(UIViewController*)viewController
{
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    

    
    UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:viewController] ;
    if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[viewController class]])
    {
        [revealController setFrontViewController:navigationControllerFront animated:YES];
    }
    else { // Seems the user attempts to 'switch' to exactly the same controller he came from!
        [revealController revealToggle:self];
        
    }
    
    
    
}

#pragma mark coach report delegate
-(void)CoachNameValidate:(CoachSurveyType*)selectedSurvey {
    SalesWorxCoachReportViewController *reportVC = [[SalesWorxCoachReportViewController alloc]init];
    reportVC.selectedSurveyType=selectedSurvey;
    [self setSliderRootViewController:reportVC];
}


-(void)test

{
   
}
//-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
//    SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
//    return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{    SectionInfo *sectionInfo  = [self.sectionInfoArray objectAtIndex:section];
    if(sectionInfo.isAlwaysOpen)
    return 16.0f;
   
return 50.0f;
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionInfo *sectionInfo  = [self.sectionInfoArray objectAtIndex:section];
    if (!sectionInfo.sectionView)
    {
        NSString *title = sectionInfo.category.name;
        if(title.length>0)
        {
            sectionInfo.sectionView = [[SectionView alloc] initWithFrame:CGRectMake(0, 0, self.medRepMenuTableview.bounds.size.width, 45) WithTitle:title Section:section delegate:self];

        }
        else
        {
            sectionInfo.sectionView = [[SectionView alloc] initWithFrame:CGRectMake(0, 0, 0,0) WithTitle:title Section:section delegate:self];
        }
    }
    return sectionInfo.sectionView;
}


- (void) sectionClosed : (NSInteger) section{
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
    SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    if(sectionInfo.isAlwaysOpen)
        return;
    sectionInfo.open = NO;

    NSInteger countOfRowsToDelete = [self.medRepMenuTableview numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
       // if(section!=([self.medRepMenuTableview numberOfRowsInSection:section]-1)) /* not closing the others menu*/
      //  {
            for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
            //}
        }

        [self.medRepMenuTableview deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    sectionInfo.sectionView.backgroundColor=KTableViewSectionCollapsedColor;
    sectionInfo.sectionView.sectionStatusImageView.image=[UIImage imageNamed:KTableViewCollapsedSectionArrowImageStr];

    self.openSectionIndex = NSNotFound;
}

- (void) sectionOpened : (NSInteger) section
{
    
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:section];
    if(array.isAlwaysOpen)
        return;
    array.open = YES;
    NSInteger count = [array.category.list count];
    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i<count;i++)
    {
        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    NSInteger previousOpenIndex = self.openSectionIndex;
    SectionInfo *prevoiusSection = [[SectionInfo alloc]init];

    if (previousOpenIndex != NSNotFound)
    {
        prevoiusSection = [self.sectionInfoArray objectAtIndex:previousOpenIndex];
        if(!prevoiusSection.isAlwaysOpen)
        {
            prevoiusSection.open = NO;
            NSInteger counts = [prevoiusSection.category.list count];
            [prevoiusSection.sectionView toggleButtonPressed:FALSE];
            for (NSInteger i = 0; i<counts; i++)
            {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenIndex]];
            }
        }
    }
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenIndex == NSNotFound || section < previousOpenIndex)
    {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else
    {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    [self.medRepMenuTableview beginUpdates];
    [self.sectionInfoArray replaceObjectAtIndex:section withObject:array];

    if(!prevoiusSection.isAlwaysOpen || !array.isAlwaysOpen)
    {
        [self.medRepMenuTableview insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:insertAnimation];
        [self.medRepMenuTableview deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];

    }
    prevoiusSection.sectionView.backgroundColor=KTableViewSectionCollapsedColor;
    array.sectionView.backgroundColor=KTableViewSectionExpandColor;
    
    prevoiusSection.sectionView.sectionStatusImageView.image=[UIImage imageNamed:KTableViewCollapsedSectionArrowImageStr];
    array.sectionView.sectionStatusImageView.image=[UIImage imageNamed:KTableViewExapndedSectionArrowImageStr];
    
    
    [self.medRepMenuTableview endUpdates];
    if(!array.isAlwaysOpen)
    {
        self.openSectionIndex = section;
    }

    
}
-(void)refreshPDARights
{
    self.categoryList=[[NSMutableArray alloc]init];
    [_medRepMenuTableview reloadData];
    [AppControl destroyMySingleton];
    appControl=[AppControl retrieveSingleton];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self updateMenu];
    [_medRepMenuTableview reloadData];
    


    
    if(self.categoryList.count>0)
    {
        NSLog(@"Select first row");
        [self tableView:_medRepMenuTableview didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    [_medRepMenuTableview reloadData];

    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSyncLabelsInDashboard) userInfo:nil repeats:NO];
}
-(void)updateSyncLabelsInDashboard
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"UPDATEDASHBOARDUINOTIFICATIONNAME" object:nil];
    
    if(categoryList.count>1)
    {
        MedRepMenuViewController *medrepMenuViewController;
        medrepMenuViewController = [[MedRepMenuViewController alloc] init] ;
        self.navigationController.viewControllers=[[NSMutableArray alloc]initWithObjects:medrepMenuViewController, nil];
    }
}
@end
