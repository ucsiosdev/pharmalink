//
//  SalesWorxPaymentCollectionViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementTitleLabel.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepButton.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepTextField.h"
#import "PJRSignatureView.h"
#import "MedRepView.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "SalesWorxPaymentCollectionFilterViewController.h"
#import "AppControl.h"
#import "SWDefaults.h"
#import "SalesWorxImageAnnimateButton.h"
#import "SalesWorxImageView.h"
#import "LPOImagesGalleryCollectionDetailViewController.h"
#import "SalesWorxDescriptionLabel1.h"
#import "SalesWorxImageBarButtonItem.h"
#import "SalesWorxPaymentCollectionImagePopoverViewController.h"
#import "SalesWorxCustomClass.h"
#import <MessageUI/MessageUI.h>
#import "SalesWorxPaymentCollectionReceiptPDFTemplateView.h"

@interface SalesWorxPaymentCollectionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SalesWorxPopoverControllerDelegate,UIPopoverControllerDelegate,SalesWorxDatePickerDelegate,PaymentCollectionFilterDelegate,UITextFieldDelegate,UIAlertViewDelegate,PJRSignatureViewDelegate, UIPopoverPresentationControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,LPOImagesGalleryCollectionDetailViewControllerDelegate,MFMailComposeViewControllerDelegate>


{
    UIBarButtonItem* leftBtn;
    __weak IBOutlet MedRepView *parentViewOfTable;
    __weak IBOutlet MedRepView *parentViewOfCustomerName;
    
    BOOL isSearching;
    UIImageView * blurredBgImage;
    NSMutableDictionary * previousFilteredParameters;
    UIPopoverController * filterPopOverController;
    NSMutableArray * unfilteredProductsArray;
    NSMutableDictionary *selectedCustomer;
    
    SalesWorxImageBarButtonItem *statementButton;
    BOOL isStatementPopUpIsOpen;
    
    BOOL signatureTouchesBegan;
    
    UIPopoverController * paymentPopOverController;
    
     PJRSignatureView *pjrSignView;
    IBOutlet UIView *pjrSignContainerView;

    IBOutlet UIImageView *currencyDropDownImageView;
    
    NSMutableDictionary * collectionInfoDictionary;
    
    CGRect invoicesViewFrame;
    
    CGRect previousFrame;
    
    CGRect signatureBGViewFrame;
    
    NSString* popOverTitle;
    
    BOOL frameChanged;
    
    AppControl* appCtrl;
    
    BOOL keyboardIsShowing;
    
    NSArray* currencyDetailsArray;
    
    NSIndexPath * selectedCustomerIndexPath;
    NSIndexPath * tempCustomerIndexPath;
    
    
    double collectedAmount;
    
    NSDecimalNumber * unsettledAmount;
    
    double settledAmount;
    
    double totalBalanceAmount;
    
    double tempUnsettledAmount;
    
    NSMutableArray* indexPathArray;
    SalesWorxImageBarButtonItem *saveButton;
    IBOutlet MedRepElementTitleLabel *newTitleUnsettledAmt;
    
    __weak IBOutlet MedRepElementTitleLabel *titleUnsettled;
    IBOutlet MedRepElementDescriptionLabel *unsettledLbl;
    __weak IBOutlet UILabel *newUnsettledLbl;
    
    IBOutlet MedRepElementTitleLabel *newUnSettledAmtLabel;
    __weak IBOutlet MedRepElementTitleLabel *titleInvoiceAMount;
    IBOutlet MedRepElementDescriptionLabel *balanceLbl;
    __weak IBOutlet UILabel *newBalanceLbl;
    
    NSIndexPath *selectedInvoiceIndexPath;
    
    IBOutlet MedRepTextField *bankTextField;
    NSMutableArray* paymentMethodsArray,*currencyArray,*invoicesArray,*tempCustomerListArray;
    
    IBOutlet MedRepTextField *branchTxtFld;
    IBOutlet MedRepTextField *chequeNumberTxtFld;
    IBOutlet MedRepTextField *remarksTextField;
    IBOutlet MedRepElementTitleLabel *remarksLabel;
    IBOutlet NSLayoutConstraint *xRemarksLabelTrailingConstraint;
    IBOutlet NSLayoutConstraint *xRemarksLabelLeadingConstraint;
    IBOutlet NSLayoutConstraint *xRemarksLabelWidthConstraint;
    
    
    CustomerPaymentClass *customerPaymentDetails;
    
    IBOutlet UIView *searchBarContainerView;
    
    NSMutableDictionary* previousFilterParameters;
   // UIPopoverController *statementPopOver;
    
    IBOutlet SalesWorxImageAnnimateButton *signatureClearButton;
    IBOutlet SalesWorxImageAnnimateButton *signatureSaveButton;
    
    BOOL shouldPresentCustomerList;
    NSDictionary *customer;
    IBOutlet NSLayoutConstraint *xSettleMentViewTrailingConstraint;
    IBOutlet NSLayoutConstraint *CustomerTableViewTrailingConstraint;
    IBOutlet NSLayoutConstraint *xCustomerTableViewContainerWidthConstraint;
    IBOutlet NSLayoutConstraint *xSettleMentViewWidthConstraint;
    
    NSMutableArray *collectionImgasesArray;
    IBOutlet UICollectionView *collectionImgesCollectionView;
    NSMutableArray *bankNames;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    
    IBOutlet NSLayoutConstraint *customerViewHeightConstraint;
    IBOutlet NSLayoutConstraint *customerViewTopConstraint;
    IBOutlet NSLayoutConstraint *customerDetailHeightConstraint;
    IBOutlet NSLayoutConstraint *signatureViewHeightConstraint;
    
    NSMutableArray* collectionImagesFromDelegate;
    
    UIBarButtonItem* cameraBarButton;
    NSString *settlementMessage;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xConstraintOfCustomerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xConstraintOfCustomerDetailView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xConstraintOfInvoiceView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xConstraintOfSignatureView;

@property(strong,nonatomic) SalesWorxVisit* currentVisit;
@property (weak, nonatomic) IBOutlet SalesWorxImageView *imageViewCustomerStatus;

@property(strong,nonatomic) NSMutableDictionary* customerListDictionary;
@property (strong, nonatomic) IBOutlet UITableView *customerListTableView;

@property(strong,nonatomic) NSMutableArray* customerListArray;

@property (strong, nonatomic) IBOutlet UISearchBar *customerSearchBar;
@property(strong,nonatomic) NSMutableArray* customerSectionsListArray;
@property (strong, nonatomic) IBOutlet UIButton *customersSearchButton;

@property(strong,nonatomic) NSIndexPath *selectedCustomerIndexPath;
- (IBAction)customersSearchTapped:(id)sender;



@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *customerNameLbl;

@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *customerNumberLbl;
@property (strong, nonatomic) IBOutlet MedRepButton *paymentMethodButton;


@property (strong, nonatomic) IBOutlet MedRepButton *currencyButton;
- (IBAction)currencyButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *paymentButtonDropDownImageView;


@property (weak, nonatomic) IBOutlet MedRepTextField *paymentTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *amountTxtFld;
@property (strong, nonatomic) IBOutlet UITableView *invoicesTableView;
@property (weak, nonatomic) IBOutlet UIButton *paymentButton;
- (IBAction)clearSignatureButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepView *signatureBgView;

- (IBAction)saveSignatureButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *chequeDateButton;
- (IBAction)chequeDateButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepView *customerDetailsHeaderView;
@property (strong, nonatomic) IBOutlet UIImageView *chequeDateDropDownImageView;

@property (strong, nonatomic) IBOutlet MedRepView *invoicesView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *signtureBgViewBottomConstraint;

-(void)convertCurrency:(NSString*)collectedAmountString collectedCurrency:(NSString*)collectedCurrencyString
;

@property (strong, nonatomic) IBOutlet MedRepTextField *currencyTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *chequeDateTextField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *paymentTypeSegmentControl;

- (id)initWithCustomer:(NSDictionary *)customer;

@property (strong, nonatomic) IBOutlet MedRepView *settlementView;

@property (weak, nonatomic) IBOutlet UIButton *btnCaptureChequeImage;

@end
