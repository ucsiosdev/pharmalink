//
//  MenuTableViewCell.m
//  SWDashboard
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "MenuTableViewCell.h"
#import "SWFoundation.h"
#import "MedRepDefaults.h"

@implementation MenuTableViewCell

@synthesize imageName;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
   // [self.textLabel setTextColor:[UIColor darkGrayColor]];
   // [self.textLabel setFont:MedRepElementTitleLabelFont];
    
    if (self.imageName != nil) {
        NSString *imageFile = [NSString stringWithFormat:@"%@", self.imageName];
        
        if (selected) {
            imageFile = [NSString stringWithFormat:@"%@_selected", self.imageName];
        }
        self.imageView.image=[[UIImage imageNamed:imageFile] imageFlippedForRightToLeftLayoutDirection];
    }
}

@end
