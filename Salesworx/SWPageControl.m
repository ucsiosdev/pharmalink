//
//  SWPageControl.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPageControl.h"
#import "SWFoundation.h"
@implementation SWPageControl

@synthesize originalSubviews;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setOriginalSubviews:self.subviews];
        
        for ( UIView *view in self.subviews ) [view removeFromSuperview];
        
        // make sure the view is redrawn not scaled when the device is rotated
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}



- (void) drawRect:(CGRect) iRect {
    UIImage                 *grey, *image, *red;
    int                     i;
    CGRect                  rect;
    
    const CGFloat           kSpacing = 10.0;
    
    iRect = self.bounds;
    
    if ( self.opaque ) {
        [self.backgroundColor set];
        UIRectFill( iRect );
    }
    
    if ( self.hidesForSinglePage && self.numberOfPages == 1 ) return;
    
    red = [UIImage imageNamed: @"Old_red"];
    grey = [UIImage imageNamed: @"Old_yellow"];
    
    rect.size.height = red.size.height;
    rect.size.width = self.numberOfPages * red.size.width + ( self.numberOfPages - 1 ) * kSpacing;
    rect.origin.x = floorf( ( iRect.size.width - rect.size.width ) / 2.0 );
    rect.origin.y = floorf( ( iRect.size.height - rect.size.height ) / 2.0 );
    rect.size.width = red.size.width;
    
    for ( i = 0; i < self.numberOfPages; ++i ) {
        image = i == self.currentPage ? red : grey;
        
        [image drawInRect: rect];
        
        rect.origin.x += red.size.width + kSpacing;
    }
}


// you must override the setCurrentPage: and setNumberOfPages:
// methods to ensure that your control is redrawn when its values change
- (void) setCurrentPage:(NSInteger) iPage {
    [super setCurrentPage: iPage];
    [self setNeedsDisplay];
}


- (void) setNumberOfPages:(NSInteger) iPages {
    [super setNumberOfPages: iPages];
    [self setNeedsDisplay];
}

@end
