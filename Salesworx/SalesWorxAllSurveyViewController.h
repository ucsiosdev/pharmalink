//
//  SalesWorxAllSurveyViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 6/2/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSyncManager.h"
#import "FTPBaseViewController.h"


@interface SalesWorxAllSurveyViewController : FTPBaseViewController
{
    
    DataSyncManager *appDelegate;
    IBOutlet UITableView *tblView;
    NSArray *SurveyArray;
    NSMutableDictionary *customer;
    UIView *myBackgroundView;
    
    //sample button
    
    UIButton * surveyBtn;
    SurveyDetails *infoForNewSurvey;
}
@property(nonatomic,strong) NSArray *SurveyArray;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnStatus;

@property (weak, nonatomic) IBOutlet UICollectionView *surveyCollectionView;

@property(nonatomic,strong) NSMutableDictionary *customer;
@property(strong,nonatomic) NSString* surveyParentLocation;


-(void)ShowSurvey;


@end
