//
//  MedRepStartEDetailingViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepButton.h"
#import "MedRepTextField.h"
#import "MedRepEdetailTaskViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepCustomClass.h"


@interface MedRepStartEDetailingViewController : SWViewController<UIPopoverControllerDelegate,StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate,TaskPopOverDelegate,EDetailingNotesDelegate>

{
    NSTimer * waitTimer;
    
    NSDate * testDate;
    
    IBOutlet UIButton *notesStatusButton;
    UIPopoverController* popOverController,*accompaniedByPopOver;
    
    NSMutableArray* accompaniedByArray;
    IBOutlet UIImageView *taskStatusImageView;
    
    IBOutlet UIButton *taskStatusButton;
    IBOutlet UIButton *notesButton;
    
    IBOutlet UIButton *accompaniedButton;
    IBOutlet UIButton *tasksButton;
    NSString* accompaniedBy;
    
    IBOutlet MedRepTextField *accompaniedTextField;

    
    StringPopOverViewController_ReturnRow* locationPopOver;
    
    NSMutableArray* collectionViewDataArray;
    NSMutableArray* productMediaFilesArray;
    NSMutableArray* productVideoFilesArray;
    NSMutableArray* productPdfFilesArray;
    NSMutableArray* productImagesArray;
    
    NSMutableArray * demoedMediaFiles;
    NSInteger  selectedCellIndex;
    NSMutableArray * imagesPathArray;
    
    bool goToNextScreen;
}

@property (strong, nonatomic) IBOutlet UILabel *locationLbl;

@property (strong, nonatomic) IBOutlet UILabel *doctorLbl;

@property (strong, nonatomic) IBOutlet UILabel *waitTimeLbl;

@property (strong, nonatomic) IBOutlet UITextField *accompaniedByLbl;

@property (strong, nonatomic) IBOutlet UITextView *objectiveTxtView;

@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;

- (IBAction)accompaniedByTapped:(id)sender;

@property(strong,nonatomic) VisitDetails * visitDetails;


@property (strong, nonatomic) IBOutlet UILabel *doctorPharmacyHeaderLbl;

// For Demonstration Plan Media 
@property (strong, nonatomic) IBOutlet UIButton *btnImages;
@property (strong, nonatomic) IBOutlet UIButton *btnVideo;
@property (strong, nonatomic) IBOutlet UIButton *btnPPT;
@property (strong, nonatomic) IBOutlet UICollectionView *demoPlanProductsCollectionView;

@end
