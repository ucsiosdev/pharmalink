//
//  HttpClient.h
//  Cisco
//
//  Created by msaad on 7/8/13.
//  Copyright (c) 2013 Jibran Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFHTTPClient.h"

@interface HttpClient : AFHTTPClient

+ (HttpClient *)sharedManager;

@end