//
//  SalesWorxPushNotificationsDownloadInfo.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSRMessagedetails.h"

@interface SalesWorxPushNotificationsDownloadInfo : NSObject

@property (nonatomic, strong) FSRMessagedetails *fileTitle;

@property (nonatomic, strong) NSString *downloadSource;

@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;

@property (nonatomic, strong) NSData *taskResumeData;

@property (nonatomic) double downloadProgress;

@property (nonatomic) BOOL isDownloading;

@property (nonatomic) BOOL downloadComplete;

@property (nonatomic) unsigned long taskIdentifier;


-(id)initWithFileTitle:(FSRMessagedetails *)title andDownloadSource:(NSString *)source;
@end
