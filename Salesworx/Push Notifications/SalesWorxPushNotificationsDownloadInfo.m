//
//  SalesWorxPushNotificationsDownloadInfo.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxPushNotificationsDownloadInfo.h"

@implementation SalesWorxPushNotificationsDownloadInfo


-(id)initWithFileTitle:(FSRMessagedetails *)title andDownloadSource:(NSString *)source{
    if (self == [super init]) {
        self.fileTitle = title;
        self.downloadSource = source;
        self.downloadProgress = 0.0;
        self.isDownloading = NO;
        self.downloadComplete = NO;
        self.taskIdentifier = -1;
    }
    
    return self;
}

@end
