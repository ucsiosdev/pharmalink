//
//  SWReturnConfirmationViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/9/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "SWReturnConfirmationViewController.h"
#import "AlSeerSignatureViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SWDefaults.h"
#import "AppControl.h"
#import "SWDatabaseManager.h"


@interface SWReturnConfirmationViewController ()

@end

@implementation SWReturnConfirmationViewController

@synthesize lblGoodsCollected,lblReturnType,txtfldCustomerDocRef,customerNameTxtFld,customerIDLbl,customerNameLbl,commentsTxtView,customSignatureView,signVC,customerDict,salesOrderDict,invoiceNumberTxtFld,returnTypeSegment,goodsCollectedSegment,returnTypeNumber,goodsCollectedNumber;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    returnTypeNumber=[[NSNumber alloc]init];
    goodsCollectedNumber=[[NSNumber alloc]init];
    
    
    // Set font-size and font-femily the way you want
    UIFont *objFont = [UIFont fontWithName:@"OpenSans" size:14.0f];
    
    // Add font object to Dictionary
    NSDictionary *dictAttributes = [NSDictionary dictionaryWithObject:objFont forKey:NSFontAttributeName];
    
    // Set dictionary to the titleTextAttributes
    [returnTypeSegment setTitleTextAttributes:dictAttributes forState:UIControlStateNormal];

    [goodsCollectedSegment setTitleTextAttributes:dictAttributes forState:UIControlStateNormal];


    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"customer dict in will appear %@", [customerDict description]);
    
    
    customerNameLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_Name"]];
    customerIDLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_ID"]];
    
    customSignatureView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    customSignatureView.layer.borderWidth = 1.0;
    
    signVC=[[AlSeerSignatureViewController alloc]init];
    
    [customSignatureView addSubview:signVC.view];
    
    
    
    txtfldCustomerDocRef.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    txtfldCustomerDocRef.layer.borderWidth = 1.0;
    
    
    invoiceNumberTxtFld.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    invoiceNumberTxtFld.layer.borderWidth = 1.0;

    
    commentsTxtView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    commentsTxtView.layer.borderWidth = 1.0;
    
    
    customerNameTxtFld.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    customerNameTxtFld.layer.borderWidth = 1.0;
    
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    txtfldCustomerDocRef.leftView=paddingView;
    txtfldCustomerDocRef.leftViewMode=UITextFieldViewModeAlways;
    
    
    UIView *paddingViewShipDate = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    invoiceNumberTxtFld.leftView=paddingViewShipDate;
    invoiceNumberTxtFld.leftViewMode=UITextFieldViewModeAlways;
    
    UIView *paddingViewCustName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    
    customerNameTxtFld.leftView=paddingViewCustName;
    customerNameTxtFld.leftViewMode=UITextFieldViewModeAlways;
    
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = bodySemiBold;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Return Confirmation";
    // emboss in the same way as the native title
    //    [label setShadowColor:[UIColor darkGrayColor]];
    //    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    customerNameLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_Name"]];
    customerIDLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_ID"]];
    
    oldFrame=self.view.frame;

    returnAdditionalInfoDict=[[NSMutableDictionary alloc]init];
    
    
    
    
    
    
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goodsCollectedSegementTapped:(UISegmentedControl *)sender {
    
    
    if (sender.selectedSegmentIndex==0) {
        
        
        [returnAdditionalInfoDict setValue:@"YES" forKey:@"goodCollected"];

    }
    
    else
    {
        [returnAdditionalInfoDict setValue:@"NO" forKey:@"goodCollected"];

    }
    
}

- (IBAction)returnTypeSegmentTapped:(UISegmentedControl *)sender {
    
    
    if (sender.selectedSegmentIndex==0) {
        
        [returnAdditionalInfoDict setValue:@"Replacement" forKey:@"returnType"];

    }
    else
    {    [returnAdditionalInfoDict setValue:@"Credit Note" forKey:@"returnType"];

        
    }
    

    
}
- (IBAction)saveReturnTapped:(id)sender {
    
    
        
        
        NSData* savedImageData=[SWDefaults fetchSignature];
        
      //  NSLog(@"check the saved image tyoe %@", savedImageData);
        
        UIImage *image ;
        
        if (savedImageData) {
            
            
            CGFloat screenScale = [[UIScreen mainScreen] scale];
            image = [UIImage imageWithData:savedImageData scale:screenScale];
            
            
            // NSData *imageData = UIImagePNGRepresentation(savedImageData);
            [returnAdditionalInfoDict setObject:savedImageData forKey:@"signature"];
            
        }
        
        
        
        if([self validateInput])
        {
            info=returnAdditionalInfoDict;
            
            NSLog(@"check sales order class object data %@", [salesOrderDict description]);
            
            
            
            
            
            
            
            
            
            
            
            
            NSMutableDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[salesOrderDict objectForKey:@"info"]]  ;
            [infoOrder setValue:returnAdditionalInfoDict forKey:@"extraInfo"];
            [infoOrder setValue:@"R" forKey:@"order_Status"];
            [salesOrderDict setValue:infoOrder forKey:@"info"];
            
            // NSString *orderRef = [self.salesOrderService saveReturnOrderWithCustomerInfo:self.customerDict andOrderInfo:self.salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
            
            
            //salesSer.delegate=self;
            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
            
            
            
            
            
            
            
            
            NSLog(@"sales order dict with all details %@", [salesOrderDict description]);
            
            
            
            
            
            NSString *orderRef = [[SWDatabaseManager retrieveManager] saveReturnOrderWithCustomerInfo:customerDict andOrderInfo:salesOrderDict andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
            
            [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",orderRef] ];
            
            
            
            UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"RMA Document created successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            alert.tag=10;
            [alert show];
            
        }
        else {
            
            NSMutableString *message= [NSMutableString stringWithString:NSLocalizedString(@"Please fill all required fields", nil)];;
            
            AppControl *appControl = [AppControl retrieveSingleton];
            
            isSignatureOptional = appControl.IS_SIGNATURE_OPTIONAL;
            //if ([[SWDefaults appControl]count]==0)//IS_SIGNATURE_OPTIONAL
            if ([isSignatureOptional isEqualToString:@"Y"])
            {
                if ([returnAdditionalInfoDict objectForKey:@"signature"])
                {
                    if (![returnAdditionalInfoDict objectForKey:@"name"])
                    {
                        message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                    }
                    else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
                    {
                        message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                    }
                }
                
            }
            else{
                
                if (![returnAdditionalInfoDict objectForKey:@"name"])
                {
                    message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
                else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
                {
                    message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
                
                if (![returnAdditionalInfoDict objectForKey:@"signature"])
                {
                    message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
                else if ([[returnAdditionalInfoDict objectForKey:@"signature"] length] < 1)
                {
                    message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
                
            }
            
            
            if (![returnAdditionalInfoDict objectForKey:@"goodCollected"]) {
                
                message=[NSMutableString stringWithString:@"Please Choose Goods Collected Option"];
            }
            
            
            if (![returnAdditionalInfoDict objectForKey:@"returnType"])
                
            {
                message=[NSMutableString stringWithString:@"Please Choose Return Type Option"];

            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            
            //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
            //                           message:NSLocalizedString(@"Please fill all required fields", nil)
            //                  closeButtonTitle:NSLocalizedString(@"OK", nil)
            //                 secondButtonTitle:nil
            //               tappedButtonAtIndex:nil];
        }
        
    }


- (BOOL)validateInput
{
    AppControl *appControl = [AppControl retrieveSingleton];
    
    
    NSLog(@"is sign optional %@", appControl.IS_SIGNATURE_OPTIONAL );
    
    
    isSignatureOptional = appControl.IS_SIGNATURE_OPTIONAL;
    //if ([[SWDefaults appControl]count]==0)//IS_SIGNATURE_OPTIONAL
    if ([isSignatureOptional isEqualToString:@"Y"])
    {
        if ([returnAdditionalInfoDict objectForKey:@"signature"])
        {
            if (![returnAdditionalInfoDict objectForKey:@"name"])
            {
                return NO;
            }
            else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
            {
                return NO;
            }
        }
        
    }
    else{
        
        if (![returnAdditionalInfoDict objectForKey:@"name"])
        {
            return NO;
        }
        else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
        {
            return NO;
        }
        
        if (![returnAdditionalInfoDict objectForKey:@"signature"])
        {
            return NO;
        }
        else if ([[returnAdditionalInfoDict objectForKey:@"signature"] length] < 1)
        {
            return NO;
        }
        
    }
    if (![returnAdditionalInfoDict objectForKey:@"goodCollected"])
    {
        return NO;
    }
    else if ([[returnAdditionalInfoDict objectForKey:@"goodCollected"] length] < 1)
    {
        return NO;
    }
    if (![returnAdditionalInfoDict objectForKey:@"returnType"])
    {
        return NO;
    }
    else if ([[returnAdditionalInfoDict objectForKey:@"returnType"] length] < 1)
    {
        return NO;
    }
    
    return YES;
}


#pragma mark saving signature methods

- (NSString*) dbGetImagesDocumentPath
{
    
    //     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 10)
    {
        if (buttonIndex==[alertView cancelButtonIndex])
        {
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
        }
        else {
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
        }
    }
    else if (alertView.tag == 20)
    {
        NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
    }
}


#pragma mark UITextField Delegate Methods


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==customerNameTxtFld) {
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: 0.2];
        oldFrame=self.view.frame;
        
        self.view.frame = CGRectMake(0,-100,1024,768);
        [UIView commitAnimations];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==customerNameTxtFld) {
        [returnAdditionalInfoDict setObject:textField.text forKey:@"name"];

        
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: 0.2];
        self.view.frame =oldFrame;
        [UIView commitAnimations];

           }
    
    else if (textField==txtfldCustomerDocRef)
    {
        [returnAdditionalInfoDict setObject:textField.text forKey:@"reference"];
    }
    else if (textField==invoiceNumberTxtFld)
    {
        [returnAdditionalInfoDict setObject:textField.text forKey:@"invoice_no"];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField==invoiceNumberTxtFld) {
        
        [textField resignFirstResponder];
        [commentsTxtView becomeFirstResponder];
        return NO;
    }
    else if(textField==customerNameTxtFld)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark UITextView Delegate Methods

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==commentsTxtView) {
        
        [returnAdditionalInfoDict setObject:textView.text forKey:@"comments"];
        
        NSLog(@"comments set %@", [returnAdditionalInfoDict description]);
        
        
    }
}

@end
