//
//  ProductFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "ProductFilterViewController.h"
#import "MedRepQueries.h"
@interface ProductFilterViewController ()

@end

@implementation ProductFilterViewController

@synthesize productsArray,demoPlanTbl,productsPopoverController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
   // NSLog(@"products array in filter VC %@", [productsArray description]);
    
    [demoPlanTbl reloadData];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (productsArray.count>0) {
        
        return productsArray.count;

    }
    
    else
    {
        return 0;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    if ([self.productDelegate respondsToSelector:@selector(selectedDemoPlanDetails:)]) {
     
        [self.productDelegate selectedDemoPlanDetails:indexPath.row];
        [productsPopoverController dismissPopoverAnimated:YES];

        
    }
    
}


#pragma mark Demo Plan Table view methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.font=MedRepFont;
    
    cell.textLabel.text=[productsArray objectAtIndex:indexPath.row] ;

    
    return cell;
    
}



- (IBAction)cancelTapped:(id)sender {
    
    
    [productsPopoverController dismissPopoverAnimated:YES];

}
@end
