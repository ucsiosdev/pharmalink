//
//  SalesWorxBluetoothManager.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxBluetoothManager.h"
#import "SWDefaults.h"
#import "SWAppDelegate.h"
@implementation SalesWorxBluetoothManager

static SalesWorxBluetoothManager *sharedBluetoothSingleton = nil;

+ (SalesWorxBluetoothManager*) retrieveSingleton {
    @synchronized(self) {
        if (sharedBluetoothSingleton == nil) {
            sharedBluetoothSingleton = [[SalesWorxBluetoothManager alloc] init];
        }
    }
    return sharedBluetoothSingleton;
}

-(void)initializeBluetoothManager
{
    self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
    NSLog(@"blue tooth manager initilized");
    [self centralManagerDidUpdateState:self.bluetoothManager];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    // Determine the state of the peripheral
    if ([central state] == CBCentralManagerStatePoweredOff) {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
        [self showAccessAlert];
    }
    else if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
    }
    else if ([central state] == CBCentralManagerStateUnauthorized) {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
        [self showAccessAlert];

    }
    else if ([central state] == CBCentralManagerStateUnknown) {
        NSLog(@"CoreBluetooth BLE state is unknown");
    }
    else if ([central state] == CBCentralManagerStateUnsupported) {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
}

-(void)showAccessAlert
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //2018-09-20 Apple has rejected an update vecause of App-Prefs:root, so updating it to show only settings.
                                  // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Bluetooth"]];
                                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];

                               }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Bluetooth is off" andMessage:@"Please turn on bluetooth" andActions:actionsArray withController:topController];
    

}


@end
