//
//  MedRepProductsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepProductsViewController.h"
#import "ZUUIRevealController.h"
#import "MedRepTableViewCell.h"
#import "MedRepQueries.h"
#import "MedRepProductTableViewCell.h"
#import "MedRepProductsCollectionViewCell.h"
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import "FCWGalleryCollectionDetailViewController.h"
#import "MedRepProductMediaDisplayViewController.h"
#import "MedRepProductImagesViewController.h"
#import <MediaPlayer/MPMoviePlayerController.h>
#import "MedRepDefaults.h"
#import "MedRepMenuTableViewCell.h"
#import "MedRepProductsHomeCollectionViewCell.h"
#import "MedRepUpdatedDesignCell.h"
#import "MedRepPharmaciesFilterViewController.h"
#import "UIImage+ImageEffects.h"
#import "MedRepProductFilterViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepProductHTMLViewController.h"
#import "UIImageView+LocalImageLoading.h"
#import "SalesworxDocumentDisplayManager.h"



@interface MedRepProductsViewController ()
{
    SalesworxDocumentDisplayManager * docDispalayManager;
}
@end

@implementation MedRepProductsViewController

@synthesize productsArray,productsTblView,productSearchBar,productImagesTblView,productMediaCollectionView,productNameLbl,skuLbl,brandLbl,categoryLbl,typeLbl,otcRxLbl,stockLbl,productDescTxtView,productImageView,imagesButton,pptButton,videoButton,productsPopoverController,productImageBGView, btnHTML;

- (void)viewDidLoad {
    [super viewDidLoad];
    docDispalayManager=[[SalesworxDocumentDisplayManager alloc] init];
    thumbnailFolderPath=[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName];
    
    
    [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    productImagesCache =[[NSCache alloc] init];
    
    
    selectedIndexPathArray=[[NSMutableArray alloc]init];
    
    unFilteredProducts=[[NSMutableArray alloc]init];
    
    
    
    productsArray=[[NSMutableArray alloc]init];
    
    
    filteredProducts=[[NSMutableArray alloc]init];
    
    
    
    selectedProductDetails=[[NSMutableDictionary alloc]init];
    
    imagesPathArray=[[NSMutableArray alloc]init];
    productImagesArray=[[NSMutableArray alloc]init];
    
    customSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0, 298, 44)];
    
    customSearchBar.hidden=YES;
    customSearchBar.delegate=self;
    
    productHTMLFilesArray = [[NSMutableArray alloc]init];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"application did receive memory warning in product media");
    
}

-(void)viewDidAppear:(BOOL)animated
{
    self.view.backgroundColor=UIViewControllerBackGroundColor;

    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(66.0/255.0) green:(126.0/255.0) blue:(196.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
}


-(void)closeSplitView
{
    NSLog(@"close splist view tapped");
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
}


-(void)willRevealCalled
{
    NSLog(@"will reveal delegate called");
    
    [productSearchBar setShowsCancelButton:NO animated:YES];
    
    if (isSearching==YES) {
        
    } else {
        [self searchBarCancelButtonClicked:productSearchBar];
    }
}

-(void)onKeyboardHide:(NSNotification *)notification
{

}

-(void)onKeyboardShow:(NSNotification *)notification
{
    [self closeSplitView];
    
    [productSearchBar becomeFirstResponder];
    
    [productSearchBar setShowsCancelButton:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"WillRevealNotification" object:nil];
}

-(void)imageTapped
{
    if(productImagesArray.count>0)
    {
    MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
    imagesVC.isFromProductsScreen=YES;
    imagesVC.productImagesArray=imagesPathArray;
    imagesVC.imagesViewedDelegate=self;
    imagesVC.currentImageIndex=0;
    imagesVC.productDataArray=productImagesArray;
    imagesVC.selectedProductDetails=selectedProductDetails;
    imagesVC.selectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [self presentViewController:imagesVC animated:YES completion:nil];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"No product images available" withController:self];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    productsTblView.rowHeight = UITableViewAutomaticDimension;
    productsTblView.estimatedRowHeight = 70.0;
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingProducts];
        UITapGestureRecognizer * productImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped)];
        productImageView.userInteractionEnabled=YES;
        [productImageView addGestureRecognizer:productImageGesture];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRevealCalled) name:@"WillRevealNotification" object:nil];
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    
    if(!self.isMovingToParentViewController)
    {
        return;
    }
    
    [productDescTxtView setEditable:NO];
    
    
//    if (@available(iOS 13, *)) {
//        productSearchBar.searchTextField.background = [[UIImage alloc] init];
//        productSearchBar.searchTextField.backgroundColor = MedRepSearchBarBackgroundColor;
//    }else{
//        productSearchBar.backgroundImage = [[UIImage alloc] init];
//        productSearchBar.backgroundColor = MedRepSearchBarBackgroundColor;
//    }
  
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
        }
    }
    
    if (self.isMovingToParentViewController) {
        
        NSMutableArray* unsortedArray=[MedRepQueries fetchProductswithStock];
        
        if (unsortedArray.count>0) {
            NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"Product_Name"
                                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
            NSMutableArray *sortedArray = [[unsortedArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
            
            
            productsArray=sortedArray;
            unFilteredProducts=productsArray;
        }
        
        [imagesButton setSelected:YES];
        [pptButton setSelected:NO];
        [videoButton setSelected:NO];
        [btnHTML setSelected:NO];
        
        if ([imagesButton isSelected]) {
            
            imagesButton .layer.borderWidth=2.0;
            imagesButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        }
        
        pptButton.layer.borderWidth=0.0;
        videoButton.layer.borderWidth=0.0;
        btnHTML.layer.borderWidth=0.0;
    }
    
    if (productsArray.count>0) {
        
        [self deselectPreviousCellandSelectFirstCell];
    }

    if (selectedIndexPath!=nil) {
 
    } else {
        if (productsArray.count>0) {
            [productsTblView reloadData];
            [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES  scrollPosition:UITableViewScrollPositionBottom];
            
        }
    }
    
    [productMediaCollectionView registerClass:[MedRepProductsHomeCollectionViewCell class] forCellWithReuseIdentifier:@"productHomeCell"];
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Products";
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Products"];

    
    if ([imagesButton isSelected]==NO && [videoButton isSelected]==NO && [pptButton isSelected]==NO && [btnHTML isSelected]==NO) {
        
    }
    
    
    if ([videoButton isSelected]==YES) {
  
        collectionViewDataArray=[[NSMutableArray alloc]init];
        
        collectionViewDataArray=productVideoFilesArray;
        
        [productMediaCollectionView reloadData];
    }
    
    else if ([pptButton isSelected]==YES)
    {
        collectionViewDataArray=[[NSMutableArray alloc]init];
        
        collectionViewDataArray=productPdfFilesArray;
        
        [productMediaCollectionView reloadData];
    }
    
    
    else if ([imagesButton isSelected]==YES)
    {
        collectionViewDataArray=[[NSMutableArray alloc]init];
        
        collectionViewDataArray=productImagesArray;
        
        [productMediaCollectionView reloadData];
    }
    else if ([btnHTML isSelected]==YES)
    {
        collectionViewDataArray = [[NSMutableArray alloc]init];
        collectionViewDataArray = productHTMLFilesArray;
        [productMediaCollectionView reloadData];
    }
    
    
    if ([selectedButton isEqualToString:@"PPT"]) {
        
        collectionViewDataArray=[[NSMutableArray alloc]init];
        collectionViewDataArray=productPdfFilesArray;
        [productMediaCollectionView reloadData];
    }
    else if ([selectedButton isEqualToString:@"Image"])
    {
        [self imagesButtonTapped:nil];
    }
    
    else if ([selectedButton isEqualToString:@"Video"])
    {
        [self videoImagesButtonTapped:nil];
    }
    else if ([selectedButton isEqualToString:@"HTML"])
    {
        [self btnHTML:nil];
    }
    
    
    for (UIView *borderView in self.view.subviews) {
        
        if ([borderView isKindOfClass:[UIView class]]) {
            
            if (borderView.tag==100 || borderView.tag==101|| borderView.tag==103||borderView.tag==1010 || borderView.tag==10001) {
                
                borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                borderView.layer.shadowOffset = CGSizeMake(2, 2);
                borderView.layer.shadowOpacity = 0.1;
                borderView.layer.shadowRadius = 1.0;
            }
        }
    }
    [self hideNoSelectionView];
}

-(void)deselectPreviousCellandSelectFirstCell
{
    if ([productsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[productsTblView cellForRowAtIndexPath:selectedIndexPath];
        
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }

        [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark UITableView DataSource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES && tableView==productsTblView) {
        
        if (filteredProducts.count>0) {
            return  filteredProducts.count;
            
        } else {
            return 0;
        }
    }
    else if (tableView==productImagesTblView)
    {
        return productImagesArray.count;
    }
    else
    {
        if (productsArray.count>0) {
            return productsArray.count;
            
        } else {
            return 0;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==productImagesTblView) {
        
        static NSString* identifier =@"productCell";
        MedRepProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepProductTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
        }
        //fetch images from documents directory
        
        //iOS 8 support
        
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[productImagesArray objectAtIndex:indexPath.row]];
        
        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
        
        return cell;
    }
    else {
        
        static NSString* identifier=@"updatedDesignCell";
        
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        
        if (isSearching && !NoSelectionView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([selectedIndexPathArray containsObject:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
        
        if (isSearching==YES && tableView==productsTblView && filteredProducts.count>0) {
            
            cell.titleLbl.text=[[[filteredProducts valueForKey:@"Product_Name"]objectAtIndex:indexPath.row] capitalizedString];

            cell.descLbl.text=[[[filteredProducts valueForKey:@"Brand_Code"]objectAtIndex:indexPath.row]capitalizedString];
            
        }
        
        else {
            if (productsArray.count>0) {
                cell.titleLbl.text=[[[productsArray valueForKey:@"Product_Name"]objectAtIndex:indexPath.row] capitalizedString];
                cell.descLbl.text=[[productsArray valueForKey:@"Brand_Code"]objectAtIndex:indexPath.row];
            }
        }

        return cell;
    }
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==productsTblView) {
 
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        cell.titleLbl.textColor=MedRepMenuTitleFontColor;
        cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        
        cell.cellDividerImg.hidden=NO;
        
        return indexPath;
    }
    else {
        return nil;
    }
}

-(void)updateMediaButtonsState
{
    [imagesButton setSelected:YES];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnHTML setSelected:NO];
    
    if ([imagesButton isSelected])
    {
        imagesButton .layer.borderWidth=2.0;
        imagesButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    pptButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideNoSelectionView];
    
    if (indexPath.row==0  ) {
        
    } else {
        [self closeSplitView];
    }
    
    
    if (selectedIndexPathArray.count==0) {
        
        [selectedIndexPathArray addObject:indexPath];
    }
    else {
        selectedIndexPathArray=[[NSMutableArray alloc]init];
        [selectedIndexPathArray addObject:indexPath];
    }
    
    [imagesButton setSelected:YES];
    [videoButton setSelected:NO];
    [pptButton setSelected:NO];
    [btnHTML setSelected:NO];
    
    selectedProductIndexPath=indexPath;
    [productsTblView reloadData];
    
    
    NSMutableArray* currentArray=[[NSMutableArray alloc]init];
    
    
    if (isSearching==YES) {
        
        currentArray=filteredProducts;
        
    }
    else
    {
        currentArray=productsArray;
    }
    
    if (currentArray.count>0) {
        
        selectedProductDetails=[currentArray objectAtIndex:indexPath.row];

        
        NSString* productStock=[NSString stringWithFormat:@"%@",[[currentArray objectAtIndex:indexPath.row] valueForKey:@"QTY"]];
        
        if ([productStock isEqualToString:@"0"]) {
            [productStatusIamgeView setBackgroundColor:[UIColor redColor]];
            
        }
        
        else
        {
            [productStatusIamgeView setBackgroundColor:[UIColor greenColor]];
            
        }

        productNameLbl.text=[[currentArray objectAtIndex:indexPath.row] valueForKey:@"Product_Name"];
        brandLbl.text=[[currentArray objectAtIndex:indexPath.row] valueForKey:@"Brand_Code"];
        categoryLbl.text=[[currentArray objectAtIndex:indexPath.row] valueForKey:@"Agency"];
        
        //fetch desc from app codes

        NSString * appCodeDescription=[MedRepQueries fetchAppCOdeforProductType:[[currentArray objectAtIndex:indexPath.row] valueForKey:@"Product_Type"]];
        
        
        if (appCodeDescription!=nil) {
            
            typeLbl.text=appCodeDescription;
        }
        
        
        skuLbl.text=[[currentArray objectAtIndex:indexPath.row]valueForKey:@"Product_Code"];
        
        
        NSString* empID=[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"];
        
        
        NSString* productID=[[currentArray objectAtIndex:indexPath.row] valueForKey:@"Product_ID"];
        NSMutableArray* tempStockArray=[MedRepQueries fetchEmpStockwithEmpID:empID ProductID:productID];
        NSLog(@"Temp prod stock %@", [tempStockArray description]);
        
        NSInteger lotQty=0;
        
        
        for (NSInteger i=0; i<tempStockArray.count; i++) {
            
            NSInteger tempLotQty=[[NSString stringWithFormat:@"%@",[[tempStockArray objectAtIndex:i] valueForKey:@"Lot_Qty"]] integerValue];
            NSInteger usedQty=[[NSString stringWithFormat:@"%@",[[tempStockArray objectAtIndex:i] valueForKey:@"Used_Qty"]] integerValue];
            
            lotQty=lotQty+tempLotQty;
            
            lotQty=lotQty-usedQty;
        }
        
        if (lotQty==0) {
            stockLbl.text=@"N/A";
        }
        else{
            stockLbl.text=[NSString stringWithFormat:@"%d", lotQty];
        }
    }
    
    selectedIndexPath=indexPath;
    
    
    NSString* selectedProductID;
    
    if (isSearching==YES && tableView==productsTblView && filteredProducts.count>0) {
        
        selectedProductID= [[filteredProducts valueForKey:@"Product_ID"] objectAtIndex:indexPath.row];
    }
    
    else
    {
        if (productsArray.count>0) {
            selectedProductID= [[productsArray valueForKey:@"Product_ID"] objectAtIndex:indexPath.row];
        }
    }
    
    //fetching data from db based on product ID
    
    
    //is product active\
    
    productMediaFilesArray =[[NSMutableArray alloc]init];
    productVideoFilesArray =[[NSMutableArray alloc]init];
    productPdfFilesArray =[[NSMutableArray alloc]init];
    productDetailsArray=[[NSMutableArray alloc]init];
    productImagesArray=[[NSMutableArray alloc]init];
    productHTMLFilesArray=[[NSMutableArray alloc]init];
    collectionViewDataArray=[[NSMutableArray alloc]init];
    [productMediaCollectionView reloadData];
    
    
    //  NSLog(@"product data array is %@", [productsArray description]);
    
    NSString* productDesc=[MedRepQueries fetchDetailedInfoforProduct:selectedProductID];
    
    productDescTxtView.font=MedRepElementDescriptionLabelFont;
    productDescTxtView.textColor=MedRepElementDescriptionLabelColor;
    
    productDescTxtView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    productDescTxtView.text=productDesc;
    
    
    // BOOL isActive=[MedRepQueries isProductActive:selectedProductID];
    BOOL isActive=[[[productsArray valueForKey:@"Status"] objectAtIndex:indexPath.row]isEqualToString:@"Active"];
    if (isActive==YES) {
        
        NSLog(@"product is active");
        
        [self fetchProductDetails:selectedProductID];
    }
    else {
        NSLog(@"product is not active");
    }
    
    [self updateMediaButtonsState ];
    
}


#pragma mark Search Bar Methods


- (IBAction)searchButtonTapped:(id)sender {
    
    customSearchBar.hidden=NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [UIView animateWithDuration:0.25 animations:^{
        customSearchBar.frame = CGRectMake(8, 48, 300, 44);
        [self.view addSubview:customSearchBar];
    }];
}

- (IBAction)refreshButtonTapped:(id)sender {
    
    [self viewWillAppear:YES];
    [productsTblView reloadData];
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {

    [self closeSplitView];
    
    [productSearchBar setShowsCancelButton:YES animated:YES];
    
    if ([searchBar isFirstResponder]) {
  
    } else {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [filteredProducts removeAllObjects];
    
    if([searchText length] != 0) {
        
        [self showNoSelectionView];
        
        isSearching = YES;
        [self searchContent];
    }
    else {
        isSearching = NO;
        [self hideNoSelectionView];
        
        if (productsArray.count>0) {
            [productsTblView reloadData];
            
            [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [productSearchBar setText:@""];
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    if ([searchBar isFirstResponder]) {
        
        [searchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    
    if (productsArray.count>0) {
        [productsTblView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];

    isSearching=YES;
}

-(void)searchContent
{
    NSString *searchString = self.productSearchBar.text;
    NSLog(@"searching with text in medrep products %@",searchString);

    NSString* filter = @"%K CONTAINS[cd] %@";
    
    
    NSPredicate *predicate;// = [NSPredicate predicateWithFormat:@"SELF.Product_Name contains[cd] %@", [NSString stringWithFormat:@"%@",searchString]];
    
    predicate=[SWDefaults fetchMultipartSearchPredicate:searchString withKey:@"Product_Name"];
    
    
    filteredProducts=[[productsArray filteredArrayUsingPredicate:predicate] mutableCopy];

    if (filteredProducts.count>0) {
        [productsTblView reloadData];
        
//        [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
//                                     animated:YES
//                               scrollPosition:UITableViewScrollPositionNone];
//        
//        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//        
//        
//        
//        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:firstIndexPath];
    }
    else if (filteredProducts.count==0)
    {
        isSearching=YES;
        [productsTblView reloadData];
//        [collectionViewDataArray removeAllObjects];
//        [productMediaCollectionView reloadData];
//        [self resetProductData];
    }
}


-(void)resetProductData
{
    productNameLbl.text=@"";
    brandLbl.text=@"";
    categoryLbl.text=@"";
    otcRxLbl.text=@"";
    skuLbl.text=@"";
    typeLbl.text=@"";
    stockLbl.text=@"";
    
    
    productImageView.image=[UIImage imageNamed:@""];
    
    
    productStatusIamgeView.backgroundColor=[UIColor whiteColor];
    
    imagesButton.selected=NO;
    videoButton.selected=NO;
    pptButton.selected=NO;
    [btnHTML setSelected:NO];
    
    [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
    [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
    [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
    [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
    
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
    
}


#pragma mark UICOllectionview methods


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(147, 150);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionViewDataArray.count>0)
    {
        return collectionViewDataArray.count;
    }
    else
    {
        return  collectionViewDataArray.count;
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"productHomeCell";
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    NSString* mediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName;
    NSString* fileTempPath;
    
    if (collectionViewDataArray.count>0) {
        
        if ([[[collectionViewDataArray objectAtIndex:indexPath.row]valueForKey:@"isStaticData"]isEqualToString:@"Y"]) {
            
            mediaType=@"Powerpoint";
            
            // NSLog(@"file name is %@", fileName);
        }
        else{
            
            fileName = [documentsDirectory stringByAppendingPathComponent:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
            NSLog(@"file name in products collectionview is %@", fileName);
            
            
            fileTempPath= [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
            
            NSLog(@"TEMP FILE NAME %@",fileTempPath);
            
        }
    }
    
    else{
        
        fileName = [documentsDirectory stringByAppendingPathComponent:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
        NSLog(@"file name in products collectionview is %@", fileName);
        
        
        fileTempPath= [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]stringByAppendingPathComponent:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
        
        NSLog(@"TEMP FILE NAME %@",fileTempPath);
    }
    
    cell.selectedImgView.image=nil;
    
    NSLog(@"media type is %@", mediaType);
    
    
    if ([mediaType isEqualToString:@"Image"])
    {
        cell.placeHolderImageView.hidden=NO;

        cell.productImageView.image=[UIImage imageWithContentsOfFile:[thumbnailFolderPath stringByAppendingPathComponent:[SWDefaults getValidStringValue:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]]]];
        
        cell.placeHolderImageView.image=[UIImage imageNamed:@"MedRepPlaceHolderEmpty"];
        cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row] valueForKey:@"Caption"];
    }
    
    else if ([mediaType isEqualToString:@"Video"])
    {
        // UIImage* thumbNailforVideo=[self generateThumbImage:fileName];
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        
        cell.captionLbl.text=[[productVideoFilesArray objectAtIndex:indexPath.row] valueForKey:@"Caption"];
        cell.placeHolderImageView.image=[UIImage imageNamed:@"MedRep_VideoThumbnail"];
    }
    
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        
        cell.captionLbl.text=[[productPdfFilesArray objectAtIndex:indexPath.row] valueForKey:@"Caption"];
        cell.placeHolderImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumb"];
    }
    
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        
        cell.productImageView.image=nil;
        cell.placeHolderImageView.hidden=NO;
        cell.selectedImgView.image = nil;
        
        cell.captionLbl.text = [[productHTMLFilesArray objectAtIndex:indexPath.row] valueForKey:@"Caption"];
        cell.placeHolderImageView.image = [UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
    }
    
    
    cell.backgroundColor=[UIColor clearColor];
    if (indexPath.row== selectedCellIndex)
    {
        cell .layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    else
    {
        cell.layer.borderWidth=0;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];

    
    selectedProductIndexPath = [productsTblView indexPathForSelectedRow];
    selectedCellIndex=indexPath.row;
    
        NSString* mediaType = [[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row];
    
    NSLog(@"media type is %@", mediaType);
    
    
        NSString*documentsDirectory;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        
        //make a file name to write the data to using the documents directory:
    
        if ([mediaType isEqualToString:@"Image"])
        {
            [imagesPathArray removeAllObjects];
            for (NSInteger i=0; i<productImagesArray.count; i++) {
                [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i]]];
            }
            
            NSLog(@"product images array in did select %@", [productImagesArray description]);
            
            MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
            imagesVC.isFromProductsScreen=YES;
            imagesVC.productImagesArray=imagesPathArray;
            imagesVC.imagesViewedDelegate=self;
            imagesVC.currentImageIndex=indexPath.row;
            imagesVC.productDataArray=productImagesArray;
            imagesVC.selectedProductDetails=selectedProductDetails;
            imagesVC.selectedIndexPath=indexPath;
            [self presentViewController:imagesVC animated:YES completion:nil];
        }
        
        else if ([mediaType isEqualToString:@"Video"])
        {
            MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
            videosVC.isFromProductsScreen=YES;

            videosVC.videosViewedDelegate=self;
            videosVC.currentImageIndex=indexPath.row;
            videosVC.selectedIndexPath=indexPath;
            videosVC.productDataArray=productVideoFilesArray;
            [self.navigationController presentViewController:videosVC animated:YES completion:nil];
        }
        
        else if ([mediaType isEqualToString:@"Brochure"])
        {
            MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
            pdfVC.isFromProductsScreen=YES;

            pdfVC.pdfViewedDelegate=self;
            pdfVC.productDataArray=productPdfFilesArray;
            pdfVC.currentImageIndex=indexPath.row;
            pdfVC.selectedIndexPath=indexPath;
            [self presentViewController:pdfVC animated:YES completion:nil];
        }
        else if ([mediaType isEqualToString:@"Powerpoint"])
        {
            NSString * selectedFilePath=[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:[[productHTMLFilesArray objectAtIndex:indexPath.row]valueForKey:@"File_Name"]];

            /** Power point slide show files will display in external apps*/
            if([[selectedFilePath lowercaseString] hasSuffix:KppsxFileExtentionStr]){
                [docDispalayManager ShowUserActivityShareViewInView:collectionView SourceRect:cell.frame AndFilePath:selectedFilePath];
            }else{
                MedRepProductHTMLViewController *obj = [[MedRepProductHTMLViewController alloc]init];
                obj.HTMLViewedDelegate = self;
                obj.isFromProductsScreen=YES;

                //TO BE REMOVED AFTER TESTING
                obj.isObjectData=NO;
                obj.productDataArray = productHTMLFilesArray;
                obj.selectedIndexPath = indexPath;
                obj.currentImageIndex=indexPath.row;
                [self presentViewController:obj animated:YES completion:nil];
            }

        }
    
    [productMediaCollectionView reloadData];
}

//- (void)ShowUserActivityShareViewInView:(UIView *)SourceView SourceRect:(CGRect)SourceFrame AndFilePath:(NSString *)filePath{
//    NSURL *imageURL=[NSURL fileURLWithPath:filePath];
//    UIDocumentInteractionController *docIntController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
//    docIntController.delegate = self;
//    [docIntController presentOptionsMenuFromRect:SourceFrame inView:SourceView animated:YES];
//}

#pragma mark MP Movie Player Notification

-(void)moviePlayerDonedoneButtonClicked:(NSNotification*)doneNotification{
 
    NSLog(@"done button animation clicked");
    
    NSNumber *reason = [doneNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited) {
        // Your done button action here
    }
}
- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    NSLog(@"finished playing");
    
    selectedButton=@"Video";
}


#pragma button action methods

- (IBAction)videoImagesButtonTapped:(id)sender {
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:YES];
    [btnHTML setSelected:NO];
    
    
    if ([videoButton isSelected]) {
        
        videoButton .layer.borderWidth=2.0;
        videoButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    
    imagesButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productVideoFilesArray;
    
    [productMediaCollectionView reloadData];
}

- (IBAction)pdfImagesButtonTapped:(id)sender {
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:YES];
    [videoButton setSelected:NO];
    [btnHTML setSelected:NO];
    
    
    if ([pptButton isSelected]) {
        
        pptButton .layer.borderWidth=2.0;
        pptButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
    
    
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productPdfFilesArray;
    
    
    [productMediaCollectionView reloadData];
}

- (IBAction)imagesButtonTapped:(id)sender {
    
    [imagesButton setSelected:YES];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnHTML setSelected:NO];
    
    if ([imagesButton isSelected]) {
        
        imagesButton .layer.borderWidth=2.0;
        imagesButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
   
    pptButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    btnHTML.layer.borderWidth=0.0;
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productImagesArray;
    
    [productMediaCollectionView reloadData];
}


-(UIImage *)generateThumbImage : (NSString *)filepath
{
    NSURL *url = [NSURL fileURLWithPath:filepath];
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = 2000;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

-(UIImage*)generateThumbImageforPdf:(NSString*)filePath

{
    NSURL* pdfFileUrl = [NSURL fileURLWithPath:filePath];
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
    CGPDFPageRef page;
    
    CGRect aRect = CGRectMake(0, 0, 160, 160); // thumbnail size
    UIGraphicsBeginImageContext(aRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIImage* thumbnailImage;
    
    
    NSUInteger totalNum = CGPDFDocumentGetNumberOfPages(pdf);
    
    for(int i = 0; i < totalNum; i++ ) {
        
        
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0.0, aRect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        CGContextSetGrayFillColor(context, 1.0, 1.0);
        CGContextFillRect(context, aRect);
        
        
        // Grab the first PDF page
        page = CGPDFDocumentGetPage(pdf, i + 1);
        CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFMediaBox, aRect, 0, true);
        // And apply the transform.
        CGContextConcatCTM(context, pdfTransform);
        
        CGContextDrawPDFPage(context, page);
        
        // Create the new UIImage from the context
        thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
        
        //Use thumbnailImage (e.g. drawing, saving it to a file, etc)
        
        CGContextRestoreGState(context);
    }
    
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(pdf);
    
    return thumbnailImage;
}


#pragma mark Top Level Data Methods



-(void)displayUpdatedData:(NSIndexPath*)selectedIndex

{
    NSMutableDictionary* currentDict=[[NSMutableDictionary alloc]init];
    
    if (isSearching==YES) {
        
        currentDict=[filteredProducts objectAtIndex:selectedIndex.row];
    }
    
    else
    {
        currentDict = [productsArray objectAtIndex:selectedIndex.row];
        
    }
    
    NSLog(@"current data is %@", [currentDict description]);
    
    
    
}
-(void)fetchProductDetails:(NSString*)productID
{
    //now fetch the media files associated with this product


    productMediaFilesArray=[MedRepQueries fetchMediaFilesforProduct:productID];

    if (productMediaFilesArray.count>0) {
        
        for (NSInteger i=0; i<productMediaFilesArray.count;i++) {
            
            NSString* mediaType = [[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i];
            
            
            if ([mediaType isEqualToString:@"Image"]) {
                
                NSMutableDictionary* imagesDataDict=[[NSMutableDictionary alloc]init];
                
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"File_Name"] objectAtIndex:i] forKey:@"File_Name"];
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] forKey:@"Media_Type"];
                
                
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Caption"] objectAtIndex:i] forKey:@"Caption"];
                
                
                
                
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_ID"] objectAtIndex:i] forKey:@"Product_ID"];
                
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Name"] objectAtIndex:i] forKey:@"Product_Name"];
                
                
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Code"] objectAtIndex:i] forKey:@"Product_Code"];
                
                
                [imagesDataDict setValue:[[productMediaFilesArray valueForKey:@"Detailed_Info"] objectAtIndex:i] forKey:@"Detailed_Info"];

                [productImagesArray addObject:imagesDataDict];
            }
            
            else if ([mediaType isEqualToString:@"Video"])
            {
                
                NSMutableDictionary* videosDataDict=[[NSMutableDictionary alloc]init];
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"File_Name"] objectAtIndex:i] forKey:@"File_Name"];
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] forKey:@"Media_Type"];
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Caption"] objectAtIndex:i] forKey:@"Caption"];
                
                
                
                NSString* mediaFileID=[[productMediaFilesArray  objectAtIndex:i]valueForKey:@"Media_File_ID"];
                
                
                NSString* downloadUrlforImage=[[[NSUserDefaults standardUserDefaults]valueForKey:@"ServerAPILink"] stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mediaFileID,@"M"]];
                
                
                
                [videosDataDict setValue:downloadUrlforImage forKey:@"Download_Url"];
                
                
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_ID"] objectAtIndex:i] forKey:@"Product_ID"];
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Name"] objectAtIndex:i] forKey:@"Product_Name"];
                
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Code"] objectAtIndex:i] forKey:@"Product_Code"];
                
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Detailed_Info"] objectAtIndex:i] forKey:@"Detailed_Info"];
  
                [productVideoFilesArray addObject:videosDataDict];
                
                
                if (productVideoFilesArray.count>0) {
                    
                    [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
                }
                
            }
            
            else if ([mediaType isEqualToString:@"Brochure"])
            {
                
                NSMutableDictionary* pdfDataDict=[[NSMutableDictionary alloc]init];
                
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"File_Name"] objectAtIndex:i] forKey:@"File_Name"];
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] forKey:@"Media_Type"];
                
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Caption"] objectAtIndex:i] forKey:@"Caption"];
                
                
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_ID"] objectAtIndex:i] forKey:@"Product_ID"];
                
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Name"] objectAtIndex:i] forKey:@"Product_Name"];
                
                
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Code"] objectAtIndex:i] forKey:@"Product_Code"];
                
                
                [pdfDataDict setValue:[[productMediaFilesArray valueForKey:@"Detailed_Info"] objectAtIndex:i] forKey:@"Detailed_Info"];

                
                [productPdfFilesArray addObject:pdfDataDict];

            }
            else if ([mediaType isEqualToString:@"Powerpoint"])
            {
                NSMutableDictionary* videosDataDict=[[NSMutableDictionary alloc]init];
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"File_Name"] objectAtIndex:i] forKey:@"File_Name"];
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Media_Type"] objectAtIndex:i] forKey:@"Media_Type"];
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Caption"] objectAtIndex:i] forKey:@"Caption"];
                
                
                
                NSString* mediaFileID=[[productMediaFilesArray  objectAtIndex:i]valueForKey:@"Media_File_ID"];
                
                
                NSString* downloadUrlforImage=[[[NSUserDefaults standardUserDefaults]valueForKey:@"ServerAPILink"] stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mediaFileID,@"M"]];
                
                
                
                [videosDataDict setValue:downloadUrlforImage forKey:@"Download_Url"];
                
                
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_ID"] objectAtIndex:i] forKey:@"Product_ID"];
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Name"] objectAtIndex:i] forKey:@"Product_Name"];
                
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Product_Code"] objectAtIndex:i] forKey:@"Product_Code"];
                
                
                [videosDataDict setValue:[[productMediaFilesArray valueForKey:@"Detailed_Info"] objectAtIndex:i] forKey:@"Detailed_Info"];
                
                [productHTMLFilesArray addObject:videosDataDict];
                
                
                NSMutableDictionary * cssDrivenMediaFile=[[NSMutableDictionary alloc]init];
                [cssDrivenMediaFile setObject:@"CSS_Driven_HTML5" forKey:@"Product_Name"];
                [cssDrivenMediaFile setObject:@"CSS_Driven_HTML5" forKey:@"Caption"];
                [cssDrivenMediaFile setObject:@"CSS_Driven_HTML5" forKey:@"File_Name"];
                
                
                [cssDrivenMediaFile setObject:@"Powerpoint" forKey:@"Media_Type"];
                
                [cssDrivenMediaFile setObject:@"Y" forKey:@"isStaticData"];
                
                
                NSMutableDictionary * jsDrivenMediaFile=[[NSMutableDictionary alloc]init];
                [jsDrivenMediaFile setObject:@"JS_Driven_HTML5" forKey:@"Product_Name"];
                [jsDrivenMediaFile setObject:@"JS_Driven_HTML5" forKey:@"Caption"];
                [jsDrivenMediaFile setObject:@"JS_Driven_HTML5" forKey:@"File_Name"];
                
                [jsDrivenMediaFile setObject:@"Powerpoint" forKey:@"Media_Type"];
                
                [jsDrivenMediaFile setObject:@"Y" forKey:@"isStaticData"];
                
                
                NSLog(@"product html files are %@", productHTMLFilesArray);
                
                
                if (productHTMLFilesArray.count>0) {
                    
                    [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
                }
                
            }
            
            if (productImagesArray.count==0) {
                [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
            } else
            {
                [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
            }
            
            
            if (productVideoFilesArray.count==0) {
                [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
            }
            else {
                [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
            }
            
  
            
            if (productPdfFilesArray.count==0) {
                [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
            }
            else {
                [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
            }
            
            
            if (productHTMLFilesArray.count==0) {
                [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
            }
            else {
                [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
            }
            
            
            //display default image
            
            productImageView.contentMode=UIViewContentModeScaleAspectFit;
            
            
            if (productImagesArray.count>0) {
                
                productImageView.image=[UIImage imageWithContentsOfFile:[thumbnailFolderPath stringByAppendingPathComponent:[SWDefaults getValidStringValue:[[productImagesArray  objectAtIndex:0]valueForKey:@"File_Name"]]]];
            }
            else {
                productImageView.image=[UIImage imageNamed:@"Default_ImgIcon"];
            }

            collectionViewDataArray=[[NSMutableArray alloc]init];
            
            collectionViewDataArray=productImagesArray;

            [productMediaCollectionView reloadData];
            
        }
    }
    else {
        if (productMediaFilesArray.count==0) {
            
            productImageView.image=nil;
            
            [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
            [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
            [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
            [btnHTML setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
            
            productImageView.image=[UIImage imageNamed:@"Default_ImgIcon"];
        }
        collectionViewDataArray=[[NSMutableArray alloc]init];
        
        collectionViewDataArray=productMediaFilesArray;
    }
    
    //fetch top level data items

    productDetailsArray=[MedRepQueries fetchProductDetails:productID];
    
    if (productDetailsArray.count>0) {
        brandLbl.text=[NSString stringWithFormat:@"%@", [[productDetailsArray valueForKey:@"Brand_Code"] objectAtIndex:0]];
        categoryLbl.text=[NSString stringWithFormat:@"%@", [[productDetailsArray valueForKey:@"Agency"] objectAtIndex:0]];
    }
}

-(void)removeKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (IBAction)filterButtonTapped:(id)sender {
    
    if (productsArray.count>0) {
        
        NSLog(@"test");
        
        [self closeSplitView];
        
        [self removeKeyBoardObserver];
        

        if (selectedIndexPathArray.count>0) {
            [selectedIndexPathArray replaceObjectAtIndex:0 withObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        }

        UIButton* filterBtn=(UIButton*)sender;
        
        NSMutableArray* demoPlanNamesArray=[[NSMutableArray alloc]init];
        
        
        demoPlanArray=[[NSMutableArray alloc]init];
        
        NSString* currentDate=[MedRepQueries fetchDatabaseDateFormat];
        
        
        demoPlanArray=[MedRepQueries fetchDemoPlanforProducts:currentDate];
        for (NSInteger i=0; i<demoPlanArray.count; i++) {
            
            [demoPlanNamesArray addObject:[[demoPlanArray  objectAtIndex:i]valueForKey:@"Description"]];
        }
        
        
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        
        [self.view addSubview:blurredBgImage];

        
        [productSearchBar setShowsCancelButton:NO animated:YES];
        productSearchBar.text=@"";
        
        [self.view endEditing:YES];
        isSearching=NO;
        [self hideNoSelectionView];
        
        [productsTblView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
        
        
        MedRepProductFilterViewController * popOverVC=[[MedRepProductFilterViewController alloc]init];
        
        //fetch products with stock
        
        
        popOverVC.contentArray=unFilteredProducts;
        
        popOverVC.filteredProductDelegate=self;
        
        popOverVC.navController=self.navigationController;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        
        
        productsPopoverController=nil;
        productsPopoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        productsPopoverController.delegate=self;
        popOverVC.filterPopOverController=productsPopoverController;
        popOverVC.filteredProductDelegate=self;
        popOverVC.previouslyFilteredContent=previousFilteredParameters;
        [productsPopoverController setPopoverContentSize:CGSizeMake(366, 600) animated:YES];
        
        
        CGRect btnFrame=filterBtn.frame;
        
        btnFrame.origin.x=btnFrame.origin.x+3;
        
        demoPlanArray=[[NSMutableArray alloc]init];
        
        NSString* currentDt=[MedRepQueries fetchDatabaseDateFormat];
        
        
        demoPlanArray=[MedRepQueries fetchDemoPlanforProducts:currentDt];
        
        NSMutableArray* tempArray=[[NSMutableArray alloc]init];
        
        
        
        for (NSInteger i=0; i<tempArray.count; i++) {
            
            [demoPlanNamesArray addObject:[[demoPlanArray  objectAtIndex:i]valueForKey:@"Description"]];
        }
        

        CGRect relativeFrame = [filterBtn convertRect:filterBtn.bounds toView:self.view];
        
        [productsPopoverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
    }
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    if (popoverController == productsPopoverController) {
        
        return NO;
    }
    else
    {
        return YES;
    }
}


-(void)selectedDemoPlanDetaislwithIndex:(NSMutableArray*)demoPlanDetails selectedIndex:(NSInteger)index
{
    
}

-(void)selectedDemoPlanDetails:(NSInteger)selectedIndex
{
    NSMutableArray* productIDsArray=[MedRepQueries fetchProductIDforSelectedDemoPlan:[[demoPlanArray objectAtIndex:selectedIndex] valueForKey:@"Demo_Plan_ID"]];
    
    NSMutableArray* tempProductArray=[[NSMutableArray alloc]init];
    
    
    //now match with these product ID's with the product ids in product array
    
    for (NSInteger i=0; i<productIDsArray.count; i++) {

        for(NSInteger j=0; j<productsArray.count; j++)
        {
            NSString *productID = [[productsArray objectAtIndex:j ]valueForKey:@"Product_ID"];
            
            if ([productID isEqualToString:[[productIDsArray objectAtIndex:i] valueForKey:@"Product_ID"]]) {
                
                [tempProductArray addObject:[productsArray objectAtIndex:j]];
            }
        }
    }
    
    if (tempProductArray.count>0) {
        
        [productsArray removeAllObjects];
        
        productsArray=tempProductArray;
        
        [productsTblView reloadData];
        
        if ([productsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
            
            [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}



-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
}

#pragma mark Filter Product delegate methods

-(void)productFilterDidCancel
{
    [self addKeyBoardObserver];
    
    [blurredBgImage removeFromSuperview];
    
    [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    
    [productsPopoverController dismissPopoverAnimated:YES];
    
    isSearching=NO;
    [self hideNoSelectionView];
    
    productsArray=unFilteredProducts;
    
    [productsTblView reloadData];
    
    if ([productsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        
        [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

-(void)filteredProduct:(NSMutableArray *)filteredContent
{
    [self addKeyBoardObserver];
    
    if (filteredContent.count>0) {
        
        isFiltered=YES;
        
        [productsPopoverController dismissPopoverAnimated:YES];
        
        [blurredBgImage removeFromSuperview];
        
        productsArray=[[NSMutableArray alloc]init];
        
        productsArray=filteredContent;

        [productsTblView reloadData];
        
        if ([productsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
            
            [productsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        [productsTblView.delegate tableView:productsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [self.medRepFilterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    }
}

#pragma mark PDF Viewer Cancel Delegate

-(void)PDFViewerDidCancel
{
    NSLog(@"cancel viewer called");
    
    selectedButton=@"PPT";
}

-(void)fullScreenImagesDidCancel
{
    NSLog(@"cancel viewer called for images");

    selectedButton=@"Image";
}

-(void)fullScreenVideosDidCancel
{
    selectedButton=@"Video";
}

-(void)PDFViewedinFullScreenDemo:(NSMutableArray*)imagesArray
{
    
}

-(void)fullScreenPDFDidCancel
{
    selectedButton=@"PPT";
}

-(void)filteredProductParameters:(NSMutableDictionary*)filterParameters
{
    self.view.alpha=1.0;
    
    if ([self.view.subviews containsObject:blurredBgImage]) {
        [blurredBgImage removeFromSuperview];
        
    }
    previousFilteredParameters=filterParameters;
}

#pragma mark button HTML action 

- (IBAction)btnHTML:(id)sender
{
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnHTML setSelected:YES];
    
    if ([btnHTML isSelected])
    {
        btnHTML.layer.borderWidth=2.0;
        btnHTML.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    imagesButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    
    collectionViewDataArray = [[NSMutableArray alloc]init];
    collectionViewDataArray = productHTMLFilesArray;
    [productMediaCollectionView reloadData];
}

#pragma mark HTML's Viewed delegate

-(void)fullScreenHTMLDidCancel
{
    selectedButton=@"HTML";
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    customerViewTopConstraint.constant = 8;
}
-(void)showNoSelectionView
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant = self.view.frame.size.height-16;
    customerViewTopConstraint.constant = 1000;
}

@end
