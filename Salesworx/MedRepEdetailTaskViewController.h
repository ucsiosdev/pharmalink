//
//  MedRepEdetailTaskViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"


#import "MedRepEdetailTaskViewController.h"
#import "MedRepEdetailingTaskDetailViewController.h"
#import "MedRepCustomClass.h"

@protocol TaskPopOverDelegate <NSObject>

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray;


@end

@interface MedRepEdetailTaskViewController : SWViewController<UITableViewDataSource,UITableViewDelegate,VisitTaskDelegate>


{
    id delegate;
}

@property (strong, nonatomic) IBOutlet UITableView *tasksTblView;

@property(strong,nonatomic)NSMutableArray* tasksArray;

@property(strong,nonatomic) id delegate;

@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;

@property(nonatomic) BOOL isVisitCompleted;

@property(nonatomic) BOOL isVisitsList;

@property(nonatomic) BOOL isPopOverPresentationController;


@property(strong,nonatomic) UIPopoverController * tasksPopOverController;

@property(strong,nonatomic) NSMutableArray* taskObjectsArray;

@property(nonatomic) BOOL dashBoardViewing;

@property(nonatomic) BOOL isInMultiCalls;

@property(strong,nonatomic) VisitDetails * visitDetails;

@end
