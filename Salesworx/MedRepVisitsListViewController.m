//
//  MedRepVisitsListViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepVisitsListViewController.h"
#import "MedRepVisitsViewController.h"
#import "MedRepTableViewCell.h"
#import "MedRepDefaults.h"
#import "MedRepStartEDetailingViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "MedRepEdetailTaskViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SWAppDelegate.h"
#import "MedRepVisitsFilterViewController.h"
#import "MedRepProductImagesViewController.h"
#import "MedRepProductPDFViewController.h"
#import "MedRepProductVideosViewController.h"
#import "MedRepRescheduleViewController.h"
#import "MedRepVisitTableViewCell.h"
#import "SalesWorxCustomerOnsiteVisitOptionsReasonViewController.h"
#import "MedRepVisitOptionsViewController.h"
#import "MedRep-Swift.h"
#import "MedRepDoctorsLastVisitDetailsPopOverViewController.h"


@interface MedRepVisitsListViewController ()

@end

@implementation MedRepVisitsListViewController
@synthesize locationLbl,doctorLbl,plannedVisitTimeLbl,demonstrationPlanLbl,visitTypeLbl,objectiveTxtView,classLbl,tradeChannelLbl,idLbl,otcRxLbl,emirateLbl,visitLocationMapView,contactNumberLbl,emailLbl,lastVisitedOnLbl,nextPlannedVisitLbl,searchBarVisit,visitsTblView,addressTxtView,currentVisitDetailsDict,visitEndDateLbl,visitEndDateValueLbl,tblBgView,blurMask,blurredBgImage, selectedVisitDetailsDict, isFromDashboard;

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    noVisitSelectedLbl.text=@"No Visit Selected";
    appcontrol=[AppControl retrieveSingleton];
    if (self.isMovingToParentViewController) {
        MedrepNoVisitsReasonDBArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select * from TBL_App_Codes where Code_Type='NO_VISITS_REASON'"];

        if ([filteredPlannedVisitsArray count] >0 &&selectedVisitDetailsDict != nil)
        {
            NSString *selectedPlannedVisitID;
            
            if (![NSString isEmpty:[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Planned_Visit_ID"]]]) {
                selectedPlannedVisitID = [SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Planned_Visit_ID"]];
                NSArray *filtered = [filteredPlannedVisitsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Planned_Visit_ID == '%@'",selectedPlannedVisitID]]];
                
                NSInteger indexObj = [filteredPlannedVisitsArray indexOfObject:[filtered objectAtIndex:0]];
                NSLog(@"selected index %@", [filteredPlannedVisitsArray objectAtIndex:indexObj]);
                
                if ([visitsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
                {
                    [visitsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]
                                                    animated:YES
                                              scrollPosition:UITableViewScrollPositionNone];
                    
                    [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:indexObj inSection:0]];
                }
            }
            
            else{
                
                [self deselectPreviousCellandSelectFirstCell];
            }
        }
        else
        {
            [self deselectPreviousCellandSelectFirstCell];
        }
        
    }
}

-(void)receiveVisitDidFinishNotification:(NSNotification*)visitFinishNotidication
{
    NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];
    plannedVisitsArray =[MedRepQueries fetchAllPlannedVisitsforFilter];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [previousFilteredParameters setValue:currentDateinDisplayFormat forKey:@"Visit_Date"];
    [self searchContent];
       NSInteger indexOfObject=[[filteredPlannedVisitsArray valueForKey:@"Planned_Visit_ID"] indexOfObject:[visitFinishNotidication.object valueForKey:@"Planned_Visit_ID"]];
    if(indexOfObject==NSIntegerMax)
    {
        if(filteredPlannedVisitsArray.count>0)
        {
            [self tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [visitsTblView reloadData];
        }
    }
    else
    {
        [self tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:indexOfObject inSection:0]];
        [visitsTblView reloadData];
        [visitsTblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexOfObject inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
 
    }
}


-(void)appLaunchedFromBackground
{
    NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];
    filteredPlannedVisitsArray=[[NSMutableArray alloc]init];
    plannedVisitsArray=[[NSMutableArray alloc]init];
    plannedVisitsArray=[MedRepQueries fetchAllPlannedVisitsforFilter];
    unfilteredVisitsArray=plannedVisitsArray;
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [previousFilteredParameters setValue:currentDateinDisplayFormat forKey:@"Visit_Date"];
    [self searchContent];
    
    if(filteredPlannedVisitsArray.count>0)
    {
        [self tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [visitsTblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        [visitsTblView reloadData];
    }


}



- (void)viewDidLoad {
    [super viewDidLoad];
    MedrepNoVisitsReasonDBArray=[[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appLaunchedFromBackground)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveVisitDidFinishNotification:)
                                                 name:@"VisitDidFinishNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveVisitDidFailNotification:)
                                                 name:@"VisitDidFailNotification"
                                               object:nil];
    
    currentVisitTasksArray=[[NSMutableArray alloc]init];
    unfilteredVisitsArray=[[NSMutableArray alloc]init];
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Actual_Visit_ID"];
    
    
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    
    if (@available(iOS 13, *)) {
        searchBarVisit.searchTextField.background = [[UIImage alloc] init];
        searchBarVisit.searchTextField.backgroundColor = MedRepSearchBarBackgroundColor;
    }else{
        searchBarVisit.backgroundImage = [[UIImage alloc] init];
        searchBarVisit.backgroundColor = MedRepSearchBarBackgroundColor;
    }
    
    
    
    
    
    selectedVisitCallsArray=[[NSMutableArray alloc]init];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)selectedIndexDelegate:(id)selectedIndex
{
    //selected visit
    self.startedVisitDict=(NSMutableDictionary*)selectedIndex;
    searchBarVisit.delegate=nil;
    visitFinished=YES;
    self.startedVisitIndexStr=[NSString stringWithFormat:@"%@", [self.startedVisitDict valueForKey:@"Selected_Index"]];
    NSLog(@"started visit index %@", self.startedVisitDict);
}

-(void)visitDidFailedWithIndex:(id)selectedIndex
{
    NSLog(@"failed visit indexpath is %ld", (long)selectedIndexforDemoPlan);
}



-(void)viewWillDisappear:(BOOL)animated
{
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingVisits];
        
    }
    
    
    visitsTblView.rowHeight = UITableViewAutomaticDimension;
    visitsTblView.estimatedRowHeight = 70.0;

    
    NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];

    if(self.isMovingToParentViewController)
    {
        filteredPlannedVisitsArray=[[NSMutableArray alloc]init];
        plannedVisitsArray=[[NSMutableArray alloc]init];
        plannedVisitsArray=[MedRepQueries fetchAllPlannedVisitsforFilter];
        unfilteredVisitsArray=plannedVisitsArray;
        [previousFilteredParameters setValue:currentDateinDisplayFormat forKey:@"Visit_Date"];
        [self searchContent];
        plannedVisitTimeLbl.textColor=LastVisitedOnTextColor;
        lastVisitedOnLbl.textColor=LastVisitedAtTextColor;
    }
    
    
    if(self.isMovingToParentViewController)
    {
        
        NSLog(@"started visit dict is %@",[self.startedVisitDict description] );
        
        tasksButton.layer.borderWidth=1.0f;
        notesButton.layer.borderWidth=1.0f;
        tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
        notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
        
        
        
        notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
        notesButton.layer.shadowOffset = CGSizeMake(2, 2);
        notesButton.layer.shadowOpacity = 0.1;
        notesButton.layer.shadowRadius = 1.0;
        
        
        
        tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
        tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
        tasksButton.layer.shadowOpacity = 0.1;
        tasksButton.layer.shadowRadius = 1.0;
        
        
        self.view.backgroundColor=UIViewControllerBackGroundColor;
        medrepNoVisitDetailsView.backgroundColor=UIViewControllerBackGroundColor;

        
        if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
            
            if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
                UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
                
                [self.navigationItem setLeftBarButtonItem:leftBtn];
                
            } else {
            }
        }
        
        
//        UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
//        
//        [self.navigationItem setLeftBarButtonItem:leftBtn];
        
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
        {
            [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
        }

        
        createVisitButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"PlusIcon_White"] style:UIBarButtonItemStylePlain target:self action:@selector(plusButtonTapped:)];
        
        startVisitButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_StartVisit"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitTapped)];
        
        
        editVisitButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NoteIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(editVisitButtonTapped:)];
        
        
        
        
        [self.navigationItem setRightBarButtonItems:@[startVisitButton,createVisitButton,editVisitButton]];
        self.view.backgroundColor=UIViewControllerBackGroundColor;
        
        UINavigationBar *nbar = self.navigationController.navigationBar;
        
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            //iOS 7
            nbar.barTintColor = [UIColor colorWithRed:(66.0/255.0) green:(126.0/255.0) blue:(196.0/255.0) alpha:1]; // bar color
            nbar.translucent = NO;
            
            nbar.tintColor = [UIColor whiteColor]; //bar button item color
            
        } else {
            //ios 4,5,6
            nbar.tintColor = [UIColor whiteColor];
            
            
        }
        
        visitsTblView.delegate=self;
        visitsTblView.dataSource=self;
        
        visitsTblView.allowsMultipleSelectionDuringEditing = NO;

        
        UITapGestureRecognizer* presentationGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(presentationTapped)];
        presentationPlaceHolderView.userInteractionEnabled=YES;
        [presentationPlaceHolderView addGestureRecognizer:presentationGesture];
    }

}

-(void)presentationTapped
{
    
    SalesWorxPresentationHomeViewController * tempVC=[[SalesWorxPresentationHomeViewController alloc]init];
    [self.navigationController pushViewController:tempVC animated:YES];
    
    NSLog(@"visit status is %@", visitDetails.Visit_Status);
    /*
    NSString* visitStatusString=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Visit_Status"]];
    if ([visitStatusString isEqualToString:@"N"]) {
    
        SalesWorxPresentationViewController * presentationVC=[[SalesWorxPresentationViewController alloc]init];
        if (visitDetails == nil) {
            visitDetails=[MedRepDefaults fetchVisitDetailsObject:selectedVisitDetailsDict];
        }
        NSLog(@"presentation array is %@",visitDetails.presentationArray);
        presentationVC.presentationDelegate=self;
        presentationVC.visitDetails=visitDetails;
        presentationVC.presentationNavController=self.navigationController;
        [self.navigationController pushViewController:presentationVC animated:YES];

   
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"Visit Completed" andMessage:@"Presentation can't be created/viewed for completed visits" withController:self];
    }
     */
}

-(void)updatedVisitWithPresentations:(VisitDetails*)updatedDetails
{
    visitDetails=updatedDetails;
}


-(void)createdPresentations:(NSMutableArray*)presentationsArray
{
    
    visitDetails.fieldMarketingPresentationsArray=[[NSMutableArray alloc]init];
    
    FieldMarketingPresentation * currentPresentation=[[FieldMarketingPresentation alloc]init];
    currentPresentation.presentationID=[NSString createGuid];
    currentPresentation.presentationContentArray=presentationsArray;
    [visitDetails.fieldMarketingPresentationsArray addObject:currentPresentation];
    
//    visitDetails.presentationArray=presentationsArray;
}
-(void)startVisitTapped
{
  
    /*
//    //this is just to test presentation
    SalesWorxPresentationViewController * presentationVC=[[SalesWorxPresentationViewController alloc]init];
    
    if (visitDetails == nil) {
        visitDetails=[MedRepDefaults fetchVisitDetailsObject:selectedVisitDetailsDict];
    }
    presentationVC.visitDetails=visitDetails;
    presentationVC.presentationNavController=self.navigationController;
    [self.navigationController pushViewController:presentationVC animated:YES];
    //[self.navigationController presentViewController:presentationVC animated:YES completion:nil];
    return;
    */
    
    
    if (visitDetails == nil) {
        visitDetails=[MedRepDefaults fetchVisitDetailsObject:selectedVisitDetailsDict];
    }
    
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
        [UIView animateWithDuration:0 animations: ^{
            [self.view endEditing:YES];
            
        } completion: ^(BOOL finished) {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Start Visit?", nil)
                                          message:NSLocalizedString(@"Would you like to start the visit?", nil)
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KConfirmationAlertYESButtonTitle, nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                            if ([appcontrol.VALIDATE_MEDREP_CUST_LOC isEqualToString:KAppControlsYESCode]) {
                                                // check location service is enabled or not
                                                
                                                [self validateCustomerLocation];
                                                
                                            }else{
                                                [self startVisit:YES];
                                            }
                                        }];
            [alert addAction:yesAction];
            UIAlertAction* noAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KConfirmationAlertNOButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                       }];
            [alert addAction:noAction];
            [self presentViewController:alert animated:YES completion:nil];
        }];
    }
}
-(void)validateCustomerLocation
{
    BOOL showAlertSetting = false;
    NSString *alertTitle;
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusDenied:
                showAlertSetting = true;

                alertTitle = kLocationAccessRefuseAlertMessage;
                break;
            case kCLAuthorizationStatusRestricted:
                showAlertSetting = true;
                
                alertTitle = kLocationAccessRefuseAlertMessage;
                break;
            case kCLAuthorizationStatusAuthorizedAlways:

                break;
            case kCLAuthorizationStatusAuthorizedWhenInUse:

                break;
            case kCLAuthorizationStatusNotDetermined:
                showAlertSetting = true;
                
                alertTitle = kLocationAccessRefuseAlertMessage;
                break;
            default:
                break;
        }
    } else {
        
        showAlertSetting = true;
        alertTitle = kLocationAlertTitle;
    }


    if(showAlertSetting)
    {
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Settings", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       BOOL canOpenSettings =(UIApplicationOpenSettingsURLString != NULL);
                                       if (canOpenSettings)
                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                   }];
        UIAlertAction* CancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           
                                       }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(alertTitle, nil) andMessage:@"" andActions:actionsArray withController:self];
    }
    else
    {
        
        // check Sales Rep is in customer location
        
        NSString* customerLatitude=[NSString stringWithFormat:@"%@",visitDetails.Latitude];
        NSString* customerLongitude=[NSString stringWithFormat:@"%@",visitDetails.Longitude];
        
        
        //check if customer location is available
        SWAppDelegate *appDel = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];

        double validationRange=100;
        
        CLLocationCoordinate2D customerLocation= CLLocationCoordinate2DMake([customerLatitude doubleValue], [customerLongitude doubleValue]);
        
        NSLog(@"current location lat: %f  long: %f and customer location  lat %@ long %@", appDel.currentLocation.coordinate.latitude,appDel.currentLocation.coordinate.longitude,customerLatitude,customerLongitude );
        
        
        if (CLLocationCoordinate2DIsValid(customerLocation)) {
            NSLog(@"customer Coordinate valid");
            
            BOOL isInLocation = [self location:appDel.currentLocation isNearCoordinate:customerLocation withRadius:validationRange];
            if (isInLocation) {
                
                NSLog(@"user is in customer location");
                
                [self startVisit:YES];
            } else {
                
                NSLog(@"Sales Rep is not in customer location, displaying reason VC");
                
                SalesWorxCustomerOnsiteVisitOptionsReasonViewController *reasonVC= [[SalesWorxCustomerOnsiteVisitOptionsReasonViewController alloc]init];
                reasonVC.selectedReason=self;
                reasonVC.isMarketVisit = YES;
                reasonVC.currentVisitDetails = visitDetails;
                
                UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:reasonVC];
                mapPopUpViewController.navigationBarHidden=YES;
                mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
                reasonVC.modalPresentationStyle = UIModalPresentationCustom;
                self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
                [self presentViewController:mapPopUpViewController animated:NO completion:nil];
            }
        }
    }
}

- (BOOL)location:(CLLocation *)location isNearCoordinate:(CLLocationCoordinate2D)coordinate withRadius:(CLLocationDistance)radius
{
    CLCircularRegion *circularRegion = [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius identifier:@"radiusCheck"];
    
    return [circularRegion containsCoordinate:coordinate];
}

-(void)selectedReasonDelegate:(id)currentVisitDetails
{
    visitDetails = currentVisitDetails;
    
    NSString *onsiteExceptionReason = [SWDefaults getValidStringValue:visitDetails.onSiteExceptionReason];
    
    if ([NSString isEmpty:onsiteExceptionReason] == NO) {
        
        //reason code selected to move to start visit
        [self startVisit:YES];
    }
    
    NSLog(@"selected reason for onsite is %@",visitDetails.onSiteExceptionReason);
}


-(void)plusButtonTapped:(id)sender

{
    [self.view endEditing:YES];
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    MedRepVisitsViewController * medRepVisitsVC=[[MedRepVisitsViewController alloc]init];
    medRepVisitsVC.currentVisitEditType=KVisitCreate;
    medRepVisitsVC.createVisitNavigationController=self.navigationController;
    medRepVisitsVC.dismissDelegate=self;
    /** editing visit not allowed*/
    medRepVisitsVC.isUpdatingVisit=NO;
    
    UINavigationController * popOverNavigationCroller= [[[SWDefaults alloc] init]getPresenationControllerWithNavigationController:self WithRootViewController:medRepVisitsVC WithSourceView:nil WithSourceRect:CGRectMake(0, 0, 0, 0) WithBarButtonItem:createVisitButton WithPopOverContentSize:CGSizeMake(345, 485)];
    [self presentViewController:popOverNavigationCroller animated:YES completion:^{
        NSLog(@"Popover Controller shown");
    }];
    
    [self disableNavigationBarOptionsOnPopOverPresentation];
    NSLog(@"");

    
    /*
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:medRepVisitsVC];
    createVisitPopOverController=nil;
    createVisitPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    createVisitPopOverController.delegate=self;
    medRepVisitsVC.popOverController=createVisitPopOverController;

    [createVisitPopOverController setPopoverContentSize:CGSizeMake(345, 485) animated:YES];
    [createVisitPopOverController presentPopoverFromBarButtonItem:createVisitButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];*/
    
}


-(void)editVisitButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    MedRepVisitsViewController * medRepVisitsVC=[[MedRepVisitsViewController alloc]init];
    medRepVisitsVC.currentVisitEditType=KVisitUpdate;
    medRepVisitsVC.dismissDelegate=self;
    medRepVisitsVC.selectedEditVisitDetails= selectedVisitDetailsDict;
    medRepVisitsVC.isUpdatingVisit=YES;
    medRepVisitsVC.createVisitNavigationController=self.navigationController;
    /*
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:medRepVisitsVC];
    createVisitPopOverController=nil;
    createVisitPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    createVisitPopOverController.delegate=self;
    medRepVisitsVC.dismissDelegate=self;
    medRepVisitsVC.popOverController=createVisitPopOverController;

    medRepVisitsVC.selectedEditVisitDetails= selectedVisitDetailsDict;
    medRepVisitsVC.isUpdatingVisit=YES;
    [createVisitPopOverController setPopoverContentSize:CGSizeMake(345, 485) animated:YES];
    [createVisitPopOverController presentPopoverFromBarButtonItem:editVisitButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];*/
    
    UINavigationController * popOverNavigationCroller= [[[SWDefaults alloc] init]getPresenationControllerWithNavigationController:self WithRootViewController:medRepVisitsVC WithSourceView:nil WithSourceRect:CGRectMake(0, 0, 0, 0) WithBarButtonItem:editVisitButton WithPopOverContentSize:CGSizeMake(345, 485)];
    [self presentViewController:popOverNavigationCroller animated:YES completion:^{
        NSLog(@"Popover Controller shown");
    }];
    
    [self disableNavigationBarOptionsOnPopOverPresentation];
}
-(void)RescheduleVisitButtonTapped{
    CGRect rectOfCellInTableView = [visitsTblView rectForRowAtIndexPath:selectedVisitIndexPath];
    CGRect rectOfCellInSuperview = [visitsTblView convertRect:rectOfCellInTableView toView:self.view];
    
    MedRepVisitsViewController * medRepVisitsVC=[[MedRepVisitsViewController alloc]init];
    medRepVisitsVC.currentVisitEditType=KVisitReschedule;
    medRepVisitsVC.dismissDelegate=self;
    medRepVisitsVC.isUpdatingVisit=YES;

    medRepVisitsVC.createVisitNavigationController=self.navigationController;
    medRepVisitsVC.selectedEditVisitDetails= selectedVisitDetailsDict;
    
    UINavigationController * popOverNavigationCroller= [[[SWDefaults alloc] init]getPresenationControllerWithNavigationController:self WithRootViewController:medRepVisitsVC WithSourceView:self.view WithSourceRect:rectOfCellInSuperview WithBarButtonItem:nil WithPopOverContentSize:CGSizeMake(345, 485)];
    [self presentViewController:popOverNavigationCroller animated:YES completion:^{
        NSLog(@"Popover Controller shown");
    }];
    
    [self disableNavigationBarOptionsOnPopOverPresentation];

    /*
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:medRepVisitsVC];
    createVisitPopOverController=nil;
    createVisitPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    createVisitPopOverController.delegate=self;
    medRepVisitsVC.dismissDelegate=self;
    medRepVisitsVC.popOverController=createVisitPopOverController;
    medRepVisitsVC.selectedEditVisitDetails= selectedVisitDetailsDict;
    medRepVisitsVC.isUpdatingVisit=YES;
    [createVisitPopOverController setPopoverContentSize:CGSizeMake(345, 485) animated:YES];
     [createVisitPopOverController presentPopoverFromRect:rectOfCellInSuperview inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];*/
    
    

}

-(void)createVisitDidCancel
{
    [blurredBgImage removeFromSuperview];
    [self SetFirstIndexVisitSelected];
    [self searchContent];
    [self EnableNavigationBarOptionsOnPopOverDismissal];
}

-(NSString*)fetchVisitStartTime

{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    return formattedDateString;
}

-(void)startVisit:(BOOL)Value
{
    //capture start visit start time
    
    if (visitDetails == nil) {
        visitDetails=[MedRepDefaults fetchVisitDetailsObject:selectedVisitDetailsDict];
    }
    
    [MedRepDefaults clearVisitStartTime];
    
    if ([createVisitPopOverController isPopoverVisible]==YES) {
        [createVisitPopOverController dismissPopoverAnimated:YES];
    }
    
    NSDate *visitStartdate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:visitStartdate];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    
    
    NSString * hourString = [NSString stringWithFormat:@"%@",[numberFormatter stringFromNumber:[NSNumber numberWithInteger:hour]]];
    
    NSString * minuteString = [NSString stringWithFormat:@"%@",[numberFormatter stringFromNumber:[NSNumber numberWithInteger:minute]]];
    
    
   // NSString* visitStartTimeStr=[[hourString stringByAppendingString:@":"] stringByAppendingString:minuteString];
    
    //    NSString* visitStartTimeStr=[[[NSString stringWithFormat:@"%ld",(long)hour] stringByAppendingString:@":"] stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)minute]];
    
    
    
    [MedRepDefaults setVisitStartTime:[self fetchVisitStartTime]];
    
    
    
    //clearing timers and doctor signatures if any
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"faceTimeTimer"];
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"waitTimer"];
    
    
    
    [MedRepDefaults clearDoctorSignature];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_Start_Date"];
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Call_End_Date"];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Updated_Tasks"];
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Updated_Notes"];
    
    
    
    //capture call start date and time
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
    
    [[NSUserDefaults standardUserDefaults]setObject:callStartDate forKey:@"Call_Start_Date"];
    
    
    
   
    
    NSString*locationType =[selectedVisitDetailsDict valueForKey:@"Location_Type"];
    
    //as suggested by manish, all tasks should be based on doctor id for hospital and contact id for pharmacy
    //29/02/2016
    visitDetails.taskArray=[[NSMutableArray alloc]init];
    visitDetails.notesArray=[[NSMutableArray alloc]init];

    
    visitDetails.taskArray=currentVisitTasksArray;
    
    NSMutableArray * tempNotesArray=[[NSMutableArray alloc]init];
    
    

    
    if ([locationType isEqualToString:kPharmacyLocation]) {
        //get note objects
        tempNotesArray=[MedRepQueries fetchNotesObjectswithLocationID:visitDetails.Location_ID andContactID:@""];
    }
    
    else if([locationType isEqualToString:kDoctorLocation])
    {
        tempNotesArray=[MedRepQueries fetchNoteObjectwithLocationID:visitDetails.Location_ID andDoctorID:@""];

    }
    
    
    
    NSLog(@"notes array in start visit %@", visitDetails.notesArray);
    
    
    NSMutableArray * locationContactsArray=[[NSMutableArray alloc]init];
    
    //fetch objects
    NSMutableArray * doctorObjectsArray=[MedRepQueries fetchDoctorObjectswithLocationID:visitDetails.Location_ID];
    
    
    if ([visitDetails.Location_Type isEqualToString:kDoctorLocation]) {
        NSPredicate * selectedDoctorPredicate=[NSPredicate predicateWithFormat:@"SELF.Doctor_ID == [cd] %@ ", visitDetails.Doctor_ID];
        NSMutableArray * selectedDoctorArray=[[doctorObjectsArray filteredArrayUsingPredicate:selectedDoctorPredicate] mutableCopy];
        if (selectedDoctorArray.count>0) {
            visitDetails.selectedDoctor=[selectedDoctorArray objectAtIndex:0];
            
        }
        if (doctorObjectsArray.count>0) {
            visitDetails.doctorsforLocationArray=doctorObjectsArray;
        }
        
    }
    else if ([visitDetails.Location_Type isEqualToString:kPharmacyLocation])
    {
        locationContactsArray=[MedRepQueries fetchContactsObjectsforPharmacyWithLocationID:[selectedVisitDetailsDict valueForKey:@"Location_ID"]];
        
    }
    
    // MedRepStartEDetailingViewController * startEdetailing=[[MedRepStartEDetailingViewController alloc]init];

    if ([[selectedVisitDetailsDict valueForKey:@"Location_Type"] isEqualToString:kDoctorLocation]) {
        Doctors * selectedDoctor=[[Doctors alloc]init];
        
        NSString* selectedDoctorID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Doctor_ID"]];
        if ([NSString isEmpty:selectedDoctorID]==YES) {
            
            selectedDoctor=nil;
        }
        else
        {
            
            selectedDoctor.Doctor_ID =[MedRepDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Doctor_ID"]];
            selectedDoctor.Doctor_Name =[MedRepDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Doctor_Name"]];
            visitDetails.selectedDoctor=selectedDoctor;
        }
    }
    else if ([[selectedVisitDetailsDict valueForKey:@"Location_Type"] isEqualToString:kPharmacyLocation])
    {
        Contact * selectedContact=[[Contact alloc]init];
        
        NSString* selectedContactID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Contact_ID"]];
        if ([NSString isEmpty:selectedContactID]==YES) {
            
            selectedContact=nil;
        }
        else
        {
            
            selectedContact.Contact_ID=[MedRepDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Contact_ID"]];
            selectedContact.Contact_Name=[MedRepDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Pharmacy_Contact_Name"]];
            visitDetails.selectedContact=selectedContact;
        }
        
        
        
    }
        
    if ([appcontrol.FM_ENABLE_DISTRIBUTION_CHECK isEqualToString:KAppControlsYESCode] && [[selectedVisitDetailsDict valueForKey:@"Location_Type"] isEqualToString:@"P"]) {
        
        MedRepVisitOptionsViewController *visitOptionsVC = [[MedRepVisitOptionsViewController alloc]init];
        visitOptionsVC.visitDetailsDict=selectedVisitDetailsDict;
        visitOptionsVC.visitDetails=visitDetails;
        visitOptionsVC.visitDetails.taskArray=currentVisitTasksArray;
        visitOptionsVC.visitDetails.notesArray=tempNotesArray;
        visitOptionsVC.visitDetails.locationContactsArray=locationContactsArray;
        if (isFromDashboard) {
            visitOptionsVC.isFromDashboard = YES;
        }
        [self.navigationController pushViewController:visitOptionsVC animated:YES];
    } else {
        
        MedRepVisitsStartMultipleCallsViewController *startEdetailing = [[MedRepVisitsStartMultipleCallsViewController alloc]init];
        startEdetailing.visitDetailsDict=selectedVisitDetailsDict;
        startEdetailing.visitDetails=visitDetails;
        startEdetailing.visitDetails.taskArray=currentVisitTasksArray;
        startEdetailing.visitDetails.notesArray=tempNotesArray;
        startEdetailing.visitDetails.locationContactsArray=locationContactsArray;
        if (isFromDashboard) {
            startEdetailing.isFromDashboard = YES;
        }
        [self.navigationController pushViewController:startEdetailing animated:YES];
    }
}


-(void)newVisitCreated:(BOOL)value StartVisit:(BOOL)isVisitWithNewParameters AndPlannedVisitId:(NSString *)Planned_Visit_ID
{
    plannedVisitsArray=[MedRepQueries fetchAllPlannedVisitsforFilter];
    if (value==YES) {
        [blurredBgImage removeFromSuperview];
        NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];
        if(isVisitWithNewParameters)
        {
            previousFilteredParameters=[[NSMutableDictionary alloc]init];
            [previousFilteredParameters setValue:currentDateinDisplayFormat forKey:@"Visit_Date"];
            [self searchContent];
            
            NSInteger indexOfObject=[[filteredPlannedVisitsArray valueForKey:@"Planned_Visit_ID"] indexOfObject:Planned_Visit_ID];

            if(indexOfObject==NSIntegerMax)
            {
                if(filteredPlannedVisitsArray.count>0)
                {
                    [self tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    [visitsTblView reloadData];
                }
            }
            else
            {
                [self tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:indexOfObject inSection:0]];
                [visitsTblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexOfObject inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                selectedVisitIndexPath=[NSIndexPath indexPathForRow:indexOfObject inSection:0];
                [self tableView:visitsTblView didSelectRowAtIndexPath:selectedVisitIndexPath];
                [visitsTblView reloadData];
                [self startVisit:YES];
                
            }
        }
        else
        {
            [self SetFirstIndexVisitSelected];
            [self searchContent];
        }

    }
    [self EnableNavigationBarOptionsOnPopOverDismissal];
}
-(void)VisitUpdated:(BOOL)value
{
    if (value==YES) {
        [blurredBgImage removeFromSuperview];
        plannedVisitsArray=[MedRepQueries fetchAllPlannedVisitsforFilter];
        [self searchContent];
        [self deselectPreviousCellandSelectFirstCell];
    }
    [self EnableNavigationBarOptionsOnPopOverDismissal];

}

- (IBAction)taskButtontapped:(id)sender {
    
    if (filteredPlannedVisitsArray.count>0) {
        popOverController=nil;
        MedRepEdetailTaskViewController * edetailTaskVC=[[MedRepEdetailTaskViewController alloc]init];  NSMutableDictionary *VisitDetailsDict=[[NSMutableDictionary alloc]initWithDictionary:selectedVisitDetailsDict copyItems:YES];
        [VisitDetailsDict setValue:@"" forKey:@"Contact_ID"];
        [VisitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
        edetailTaskVC.visitDetails=[MedRepDefaults fetchVisitDetailsObject:selectedVisitDetailsDict];
        NSString* locationType=[selectedVisitDetailsDict valueForKey:@"Location_Type"];
        if ([locationType isEqualToString:@"D"])
        {
            currentVisitTasksArray=[MedRepQueries fetchTaskObjectsforLocationID:[selectedVisitDetailsDict valueForKey:@"Location_ID"] andDoctorID:@""];
            edetailTaskVC.visitDetails.taskArray=[currentVisitTasksArray mutableCopy];

        }
        else
        {
            currentVisitTasksArray=[MedRepQueries fetchTaskObjectswithLocationID:[selectedVisitDetailsDict valueForKey:@"Location_ID"] andContactID:@""];
            edetailTaskVC.visitDetails.taskArray=[currentVisitTasksArray mutableCopy];
        }
        edetailTaskVC.isInMultiCalls=YES;
        edetailTaskVC.visitDetailsDict=VisitDetailsDict;
        edetailTaskVC.tasksArray=edetailTaskVC.visitDetails.taskArray;
        edetailTaskVC.delegate=self;
        UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
        popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
        [popOverController setDelegate:self];
        [popOverController setPopoverContentSize:CGSizeMake(376, 600) animated:YES];
        edetailTaskVC.tasksPopOverController=popOverController;
        
        CGRect relativeFrame = [tasksButton convertRect:tasksButton.bounds toView:self.view];

        if ([SWDefaults isRTL]) {
            [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        }
        else
        {
            [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
        }

        
    }

    else
    {
        [self showAlertAfterHidingKeyBoard:KDataNotAvailableMessageTitleString andMessage:KTaskDataNotAvailableMessageString withController:nil];
    }
}

- (IBAction)notesButtonTapped:(id)sender {
    
    if (filteredPlannedVisitsArray.count>0) {
            popOverController=nil;
            MedRepEDetailNotesViewController * edetailTaskVC=[[MedRepEDetailNotesViewController alloc]init];
            NSMutableDictionary *VisitDetailsDict=[[NSMutableDictionary alloc]initWithDictionary:selectedVisitDetailsDict copyItems:YES];
            [VisitDetailsDict setValue:@"" forKey:@"Contact_ID"];
            [VisitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
            edetailTaskVC.isInMultiCalls=YES;
            edetailTaskVC.visitDetails=[MedRepDefaults fetchVisitDetailsObject:selectedVisitDetailsDict];
            edetailTaskVC.visitDetailsDict=VisitDetailsDict;
            edetailTaskVC.delegate=self;
            UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
            popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
            [popOverController setDelegate:self];
            [popOverController setPopoverContentSize:CGSizeMake(376, 350) animated:YES];
            edetailTaskVC.notesPopOverController=popOverController;
            [popOverController presentPopoverFromRect:notesButton.frame inView:notesandTaksButtonContainerView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [self showAlertAfterHidingKeyBoard:KDataNotAvailableMessageTitleString andMessage:KNotesDataNotAvailableMessageString withController:nil];
    }
    
    
}

#pragma mark UIPopOver Controller delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}



#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==VisitCallsTableView)
        return 44.0f;
    else
        return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView==VisitCallsTableView)
    {
        return selectedVisitCallsArray.count;
    }
    else
    {
//        [self hideNoVisitErrorMessageView];
        if(filteredPlannedVisitsArray.count>0)
        {
            return filteredPlannedVisitsArray.count;
        }
        else
        {
            [self AllowVisitEditing:NO];
            [self showNoVisitErrorMessageView];
            return 0;
        }
    }
}





-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    if(tableView==VisitCallsTableView)
    {
        return 44.0f;
    }
    return 0.1f;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView==VisitCallsTableView)
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44.0)];
        VisitCallsTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
        [view addSubview:VisitCallsTableHeaderView];
        return VisitCallsTableHeaderView;
    }
    return [[UIView alloc]initWithFrame:CGRectZero];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==VisitCallsTableView)
    {
        static NSString* identifier=@"VisitCallICelldentifier";
        MedRepPlannedVisitsMultipleCallsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepPlannedVisitsMultipleCallsTableViewCell" owner:nil options:nil] firstObject];
        }
        
            NSMutableDictionary *callDetailsDic=[selectedVisitCallsArray objectAtIndex:indexPath.row];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            NSString* locationType=[callDetailsDic valueForKey:@"Location_Type"];
            NSString* demoPlanID=[callDetailsDic valueForKey:@"Demo_Plan_ID"];
        
            if ([locationType isEqualToString:@"D"]) {
                cell.visitCallContactNameLbl.text=[callDetailsDic valueForKey:@"Doctor_Name"];
                cell.visitCallContactClassLbl.text=[callDetailsDic valueForKey:@"Doctor_Class"];
            }
            
            else if ([locationType isEqualToString:@"P"])
            {
                cell.visitCallContactNameLbl.text=[callDetailsDic valueForKey:@"Pharmacy_Contact_Name"];
                cell.visitCallContactClassLbl.text=[callDetailsDic valueForKey:@"Class"];
            }
            NSString* visitTime=[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"hh:mm a" scrString:[callDetailsDic valueForKey:@"ActualVisitCallStartTime"]]];
            cell.visitCallStartTimeLbl.text=visitTime;
            
            NSString* demoPlanNameStr=[MedRepQueries fetchDemoPlanNamewithID:demoPlanID];
            
            cell.visitCallDemoPlanLbl.text=[MedRepDefaults getDefaultStringForEmptyString:demoPlanNameStr];
            
            cell.specializationLbl.text=[MedRepDefaults getDefaultStringForEmptyString:[callDetailsDic valueForKey:@"Specialization"]];
            
            cell.visitCallFaceTimeLbl.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults timeBetweenSatrtTime:[MedRepDefaults getValidStringValue:[callDetailsDic valueForKey:@"ActualVisitCallStartTime"]] andEndTime:[MedRepDefaults getValidStringValue:[callDetailsDic valueForKey:@"ActualVisitCallEndTime"]]]];
            

        
        return cell;
        
    }
    else
    {
        
        static NSString* identifier=@"updatedDesignCell";
        
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
        }
        [cell setCellStatusViewType:KCustomColorStatusView];
        
        currentVisitDict=[filteredPlannedVisitsArray objectAtIndex:indexPath.row];
        NSString* visitLocationType=[currentVisitDict valueForKey:@"Location_Type"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString* visitDateStr=[NSString stringWithFormat:@"%@",[currentVisitDict valueForKey:@"Visit_Date"]];
        NSString* currentDateTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        NSDate* visitDate=[MedRepQueries convertNSStringtoNSDate:visitDateStr];
        NSDate* currentDate=[MedRepQueries convertNSStringtoNSDate:currentDateTime];
        NSString* visitStatusString=[currentVisitDict valueForKey:@"Visit_Status"];
        
        
        
        if([[currentVisitDict valueForKey:@"Location_ID"]isEqualToString:appcontrol.MEDREP_NO_VISIT_LOCATION]){
            [cell.statusView setBackgroundColor:[UIColor purpleColor]];
            [cell setEditing:NO animated:NO];
        }else if ([visitStatusString isEqualToString:@"Y"]){
            [cell.statusView setBackgroundColor:KCompletedVisitColor];
            [cell setEditing:NO animated:NO];
        }else if ([visitDate compare:currentDate]==NSOrderedAscending){
            [cell setEditing:YES animated:NO];
            [cell.statusView setBackgroundColor:KLateVisitColor];
        }else{
            [cell setEditing:YES animated:NO];
            [cell.statusView setBackgroundColor:KFutureVisitColor];
        }
        
        
        
        
        currentVisitDict=[filteredPlannedVisitsArray objectAtIndex:indexPath.row];
        
        NSLog(@"current visit location type %@", [currentVisitDict valueForKey:@"Location_Type"]);
        
        visitLocationType=[currentVisitDict valueForKey:@"Location_Type"];
        
        if([[currentVisitDict valueForKey:@"Location_ID"]isEqualToString:appcontrol.MEDREP_NO_VISIT_LOCATION]){
            cell.descLbl.text=[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:visitDateStr];

        }else if ([visitLocationType isEqualToString:@"D"]){
            cell.descLbl.text = [currentVisitDict valueForKey:@"Doctor_Name"];
        }else{
            cell.descLbl.text = [currentVisitDict valueForKey:@"Pharmacy_Contact_Name"];
        }
        
        if([[currentVisitDict valueForKey:@"Location_ID"]isEqualToString:appcontrol.MEDREP_NO_VISIT_LOCATION] &&
           MedrepNoVisitsReasonDBArray.count>0){
            NSMutableArray * NovisitCodeDescriptionArray= [[MedrepNoVisitsReasonDBArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Code_Value=%@",[currentVisitDict valueForKey:@"Visit_Type"]]] mutableCopy];
            if(NovisitCodeDescriptionArray.count>0)
                cell.titleLbl.text= [[NovisitCodeDescriptionArray objectAtIndex:0] valueForKey:@"Code_Description"];
            else{
                cell.titleLbl.text=@"No visit";
            }
        }else{
            cell.titleLbl.text=[currentVisitDict valueForKey:@"Location_Name"];
        }
        
        if (isSearching && !NoVisitMessageView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([selectedVisitIndexPath isEqual:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
        return cell;
    }
}





- (nullable NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==visitsTblView)
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        cell.titleLbl.textColor=MedRepMenuTitleFontColor;
        cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        
        cell.cellDividerImg.hidden=NO;
        
        return indexPath;
    }
    return NULL;
    
}



- (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    
    if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}





-(void)loadSelectedVisitDetails:(NSMutableDictionary*)selectedVisit

{
    
    NSString* locationType=[selectedVisit valueForKey:@"Location_Type"];
    
    NSString* visitType=[selectedVisit valueForKey:@"Visit_Type"];
    if ([visitType isEqualToString:@"R"]) {
        visitTypeImageView.image=[UIImage imageNamed:@"Visit_MultiAttendees"];
    }
    else
    {
        visitTypeImageView.image=[UIImage imageNamed:@"Visit_SingleAttendees"];
    }
    
    
    NSMutableArray* visitEndDate=[MedRepQueries fetchVisitEndDateFromActualVisits:[selectedVisit valueForKey:@"Location_ID"]];
    if (visitEndDate.count>0) {
        NSString* endDateStr=[[visitEndDate objectAtIndex:0] valueForKey:@"Visit_End_Date"];
        NSString* refinedEndDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:endDateStr];
        lastVisitedOnLbl.text=refinedEndDate;
    }
    
    else
    {
        lastVisitedOnLbl.text=KNotApplicable;
    }
    
    
    
    if ([locationType isEqualToString:@"D"]) {
        doctorLbl.text=[selectedVisit valueForKey:@"Doctor_Name"];
        classLbl.text=[selectedVisit valueForKey:@"Doctor_Class"];
        [LocationTypeImageView setImage:[UIImage imageNamed:@"Visit_HospitalIcon"]];
        LocationTypeLabel.text=@"HOSPITAL";
    }
    
    else if ([locationType isEqualToString:@"P"])
    {
        doctorLbl.text=[selectedVisit valueForKey:@"Pharmacy_Contact_Name"];
        classLbl.text=[selectedVisit valueForKey:@"Class"];
        [LocationTypeImageView setImage:[UIImage imageNamed:@"Visit_PharmacyIcon"]];
        LocationTypeLabel.text=@"PHARMACY";

    }
  //  [LocationTypeImageView setImage:[UIImage imageNamed:@"PresentationCellIcon"]];

    
    
    locationLbl.text=[selectedVisit valueForKey:@"Location_Name"];
    tradeChannelLbl.text=[selectedVisit valueForKey:@"Trade_Channel"];
    otcRxLbl.text=[selectedVisit valueForKey:@"OTC"];
    
    NSString* visitTime=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[selectedVisit valueForKey:@"Visit_Date"]];
    
    plannedVisitTimeLbl.text=visitTime;
    
    objectiveTxtView.text=[selectedVisit valueForKey:@"Objective"];
    
    
    NSLog(@"demo plan id is %@",[selectedVisit valueForKey:@"Demo_Plan_ID"]);
    
    
    NSString* demoPlanNameStr=[MedRepQueries fetchDemoPlanNamewithID:[selectedVisit valueForKey:@"Demo_Plan_ID"]];
    
    
    NSLog(@"demo plan desc %@", demoPlanNameStr);
    
    if (demoPlanNameStr.length>0) {
        
        demoPlanDescLbl.text=[NSString stringWithFormat:@"%@",demoPlanNameStr];
    }
    else
    {
        demoPlanDescLbl.text=@"N/A";
        
    }
    
    
    //to be done
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==VisitCallsTableView)
    {
        
    }
    else
    {
        [self hideNoVisitErrorMessageView];
     
        @try {
            [self tableView:tableView willDeselectRowAtIndexPath:selectedVisitIndexPath];
        }
        @catch (NSException *exception) {
            
        }
        //MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        selectedVisitIndexPath=indexPath;

        cellSelected=YES;
        selectedIndexforDemoPlan=indexPath.row;
        
        visitDetails=nil;
        
        selectedVisitDetailsDict=[filteredPlannedVisitsArray objectAtIndex:indexPath.row];
        NSString* visitDateStr=[NSString stringWithFormat:@"%@",[[filteredPlannedVisitsArray objectAtIndex:indexPath.row] valueForKey:@"Visit_Date"]];
        NSDate* visitDate=[MedRepQueries convertNSStringtoNSDate:visitDateStr];

        
        if([[selectedVisitDetailsDict valueForKey:@"Location_ID"]isEqualToString:appcontrol.MEDREP_NO_VISIT_LOCATION]){
            [medrepNoVisitDetailsView setHidden:NO];
            NSMutableArray * NovisitCodeDescriptionArray= [[MedrepNoVisitsReasonDBArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Code_Value=%@",[selectedVisitDetailsDict valueForKey:@"Visit_Type"]]] mutableCopy];
            if(NovisitCodeDescriptionArray.count>0)
                medrepNoVisitReasonLabel.text= [[NovisitCodeDescriptionArray objectAtIndex:0] valueForKey:@"Code_Description"];
            else{
                medrepNoVisitReasonLabel.text=@"No visit";
            }
            [startVisitButton setEnabled:NO];
            [self AllowVisitEditing:NO];
            medrepNoVisitDateLabel.text=[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:visitDateStr];
            medrepNoVisitCommentsTextView.text=[selectedVisitDetailsDict valueForKey:@"Objective"];
            [medrepNoVisitCommentsTextView setIsReadOnly:NO];
            [medrepNoVisitCommentsTextView setEditable:NO];
            [visitsTblView reloadData];
            
        }else{
            [medrepNoVisitDetailsView setHidden:YES];
            
            //compare visit tyme and current ime to check if a visit is missed
            
            
            NSString* currentDateTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            
            // NSLog(@"visit date and current date is %@ %@", visitDateStr,currentDateTime);
            
            
            NSDate* currentDate=[MedRepQueries convertNSStringtoNSDate:currentDateTime];
            
            
            
            NSString* visitStatusString=[[filteredPlannedVisitsArray objectAtIndex:indexPath.row] valueForKey:@"Visit_Status"];
            if (filteredPlannedVisitsArray.count>0)
            {
                if ([visitStatusString isEqualToString:@"Y"]) {
                    [visitStatusImageView setBackgroundColor:KCompletedVisitColor];
                }
                else if ([visitDate compare:currentDate]==NSOrderedAscending)
                {
                    [visitStatusImageView setBackgroundColor:KLateVisitColor];
                }
                else
                {
                    [visitStatusImageView setBackgroundColor:KFutureVisitColor];
                }
            }
            else
            {
                [visitStatusImageView setBackgroundColor:KNoVisitColor];
            }
            
            selectedVisitCallsArray=[MedRepQueries fetchCallsInformationFroSelectedVisit:[selectedVisitDetailsDict valueForKey:@"Planned_Visit_ID"]];
            [selectedVisitDetailsDict setObject:indexPath forKey:@"Selected_Index"];
            [self loadSelectedVisitDetails:selectedVisitDetailsDict];
            
            [self showLastVisitDetails:[selectedVisitDetailsDict valueForKey:@"Actual_Visit_ID"]];
            [self lastVisitNotes:[selectedVisitDetailsDict valueForKey:@"Actual_Visit_ID"]];

            
            NSDateComponents *visitDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:visitDate];
            NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
            
            if ([visitStatusString isEqualToString:@"Y"]) {
                [startVisitButton setEnabled:NO];
                [self AllowVisitEditing:NO];
            }
            else if([today day] == [visitDay day] &&
                    [today month] == [visitDay month] &&
                    [today year] == [visitDay year]) {
                [startVisitButton setEnabled:YES];
                [self AllowVisitEditing:NO];
            }
            else if(([today year] < [visitDay year]) || (([today year] <= [visitDay year]) && ([today month] < [visitDay month])) || (([today year] <= [visitDay year]) && ([today month] <= [visitDay month]) && ([today day] < [visitDay day])))
            {
                [startVisitButton setEnabled:NO];
                [self AllowVisitEditing:YES];
            }
            else
            {
                [startVisitButton setEnabled:NO];
                [self AllowVisitEditing:NO];
            }
            NSString * locationLatitude=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Latitude"]];
            NSString * locationLongitude=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Longitude"]];
            
            
            if(([locationLatitude isEqualToString:@"0"] || [locationLatitude isEqualToString:@""]) &&
               ([locationLongitude isEqualToString:@"0"] || [locationLongitude isEqualToString:@""]))
            {
                [LocationMapButton setImage:[UIImage imageNamed:KVisitListLocationMapInActiveImage] forState:UIControlStateNormal];
                [LocationMapButton setUserInteractionEnabled:NO];
                LocaionMapLabel.textColor=[UIColor colorWithRed:(179.0/255.0) green:(197.0/255.0) blue:(207.0/255.0) alpha:1.0];
            }
            else
            {
                [LocationMapButton setImage:[UIImage imageNamed:KVisitListLocationMapActiveImage] forState:UIControlStateNormal];
                [LocationMapButton setUserInteractionEnabled:YES];
                LocaionMapLabel.textColor=[UIColor colorWithRed:(82.0/255.0) green:(186.0/255.0) blue:(179.0/255.0) alpha:1.0];
                
            }
            
            [VisitCallsTableView reloadData];
            [self refreshTaskandNotesStatus:selectedVisitDetailsDict];
            
            [visitsTblView reloadData];

        }
        
    }
}
-(void)AllowVisitEditing:(BOOL)allowEdit
{
    if(allowEdit)
    {
        [editVisitButton setEnabled:YES];
    }
    else
    {
        [editVisitButton setEnabled:NO];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==visitsTblView)
    {
        NSMutableDictionary * cusrrentVisit;
        NSString *visitStatusString;
        cusrrentVisit=[filteredPlannedVisitsArray objectAtIndex:indexPath.row];
        //compare visit tyme and current ime to check if a visit is missed
        visitStatusString=[cusrrentVisit valueForKey:@"Visit_Status"];
        NSString* visitDateStr=[NSString stringWithFormat:@"%@",[cusrrentVisit valueForKey:@"Visit_Date"]];
        
        NSDate* visitDate=[MedRepQueries convertNSStringtoNSDate:visitDateStr];
        
       // NSDateComponents *visitDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:visitDate];
       // NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
        
        
        
        if ([visitStatusString isEqualToString:@"Y"] || [[cusrrentVisit valueForKey:@"Location_ID"] isEqualToString:appcontrol.MEDREP_NO_VISIT_LOCATION]) {
            return NO;
        }
        else if ([[SWDefaults beginningOfDay:[NSDate date]] compare:visitDate]==NSOrderedAscending){
            return YES;
        }
        else
        {
            return NO;
        }
        
        return YES;

    }
    else
    {
        return NO;
    }
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Reschedule" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                        {
                                            //insert your editAction here
                                            @try {
                                                [self editTableRow:indexPath];
                                            }
                                            @catch (NSException *exception) {
                                                NSLog(@"%@",exception);
                                            }
                                            
                                        }];
    editAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              //insert your deleteAction here
                                              
                                              @try {
                                                  NSMutableDictionary *dic;
                                                  dic = [filteredPlannedVisitsArray objectAtIndex:indexPath.row];
                                                  [filteredPlannedVisitsArray removeObjectAtIndex:indexPath.row];

                                                  
                                                  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                                  BOOL success = [MedRepQueries deleteFutureVisit:dic];
                                                  if (success == YES)
                                                  {
                                                      plannedVisitsArray=[MedRepQueries fetchAllPlannedVisitsforFilter];
                                                      unfilteredVisitsArray=plannedVisitsArray;
                                                      [self searchContent];
                                                      if ([filteredPlannedVisitsArray count] > 0)
                                                      {
                                                          [visitsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                                                          [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                                                      }
                                                      else
                                                      {
                                                          locationLbl.text = KNotApplicable;
                                                          doctorLbl.text = KNotApplicable;
                                                          classLbl.text = KNotApplicable;
                                                          tradeChannelLbl.text = KNotApplicable;
                                                          otcRxLbl.text = KNotApplicable;
                                                          lastVisitedOnLbl.text = KNotApplicable;
                                                          plannedVisitTimeLbl.text = KNotApplicable;
                                                      }
                                                  }
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"%@",exception);
                                              }
                                          }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    if([appcontrol.DISABLE_FM_VISIT_DELETE isEqualToString:KAppControlsYESCode]){
        return @[editAction];
    }
    return @[deleteAction,editAction];
}
-(void)editTableRow:(NSIndexPath *)indexPath
{
    [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:indexPath];
    [self RescheduleVisitButtonTapped];

   
//    [CATransaction begin];
//    [visitsTblView beginUpdates];
//    [CATransaction setCompletionBlock: ^{
//        // Code to be executed upon completion
//        [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:indexPath];
//        [self RescheduleVisitButtonTapped];
//    }];
//     [visitsTblView setEditing:NO animated:NO];
//    [visitsTblView endUpdates];
//    [CATransaction commit];
    
//    NSMutableDictionary *dic;
//    
//    dic = [filteredPlannedVisitsArray objectAtIndex:indexPath.row];
//    
//    CGRect rectOfCellInTableView = [visitsTblView rectForRowAtIndexPath:indexPath];
//   // CGRect rectOfCellInSuperview = [visitsTblView convertRect:rectOfCellInTableView toView:[visitsTblView superview]];
//    
//    
//    
//    CGRect rectOfCellInSuperview = [visitsTblView convertRect:rectOfCellInTableView toView:self.view];
//
//    //blur the view here
//    
//    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
//    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
//    
//    
//    MedRepRescheduleViewController * obj=[[MedRepRescheduleViewController alloc]init];
//    obj.createVisitNavigationController=self.navigationController;
//    obj.locationType = [dic valueForKey:@"Location_Type"];
//    obj.locationID = [dic valueForKey:@"Location_ID"];
//    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:obj];
//    
//    
//    createVisitPopOverController=nil;
//    createVisitPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
//    createVisitPopOverController.delegate = self;
//    obj.dismissDelegate = self;
//    obj.popOverController = createVisitPopOverController;
//    obj.visitDetailsDict = dic;
//    [createVisitPopOverController setPopoverContentSize:CGSizeMake(345, 600) animated:YES];
//    [createVisitPopOverController presentPopoverFromRect:rectOfCellInSuperview inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}
-(void)closeReschedulePopover
{
    plannedVisitsArray=[MedRepQueries fetchAllPlannedVisitsforFilter];
    unfilteredVisitsArray=plannedVisitsArray;
    [self searchContent];
    [self EnableNavigationBarOptionsOnPopOverDismissal];
}


#pragma mark UISearch Bar Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBarVisit setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
        
    } else {
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if([searchText length] != 0) {
        
        [self showNoVisitErrorMessageView];
        
        isSearching = YES;
        [self searchContent];
    }
    else {
        isSearching = NO;
        [self hideNoVisitErrorMessageView];
        
        [self searchContent];
        [self deselectPreviousCellandSelectFirstCell];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBarVisit setText:@""];
    [searchBarVisit setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    [self hideNoVisitErrorMessageView];
    
    [self.view endEditing:YES];
    if (plannedVisitsArray.count>0) {
        [self searchContent];
        [self deselectPreviousCellandSelectFirstCell];
    }
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    isSearching=YES;
}

-(void)searchContent
{
    NSLog(@"searching with text in visits list popup %@",searchBarVisit.text);

    NSString* filter = @"%K == [c] %@";
    NSMutableArray *predicateArray = [NSMutableArray array];
    if([[previousFilteredParameters valueForKey:@"Location_Type"] length]>0) {
        
        // location type may be doctor or pharmacy so filter here by contact ID
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Location_Type", [previousFilteredParameters valueForKey:@"Location_Type"]]];
    }
    
    
    if([[previousFilteredParameters valueForKey:@"Visit_Location"] length]>0) {
        
        
        //get location ID from predicate
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Location_ID", [NSString stringWithFormat:@"%@",[previousFilteredParameters valueForKey:@"Location_ID"]]]];
        
    }
    
    if([[previousFilteredParameters valueForKey:@"Visit_Type"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Visit_Type", [NSString stringWithFormat:@"%@",[previousFilteredParameters valueForKey:@"Visit_Type"]]]];
    }
    if([[previousFilteredParameters valueForKey:@"Visit_Date"] length]>0) {
        NSLog(@"visit date details %@",[previousFilteredParameters valueForKey:@"Visit_Date"]);
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Visit_Date Contains[cd] %@",[previousFilteredParameters valueForKey:@"Visit_Date"]]];
    }
    
    NSString *searchString = searchBarVisit.text;
    if([searchString isEqualToString:@""])
    {
        
    }
    else
    {
        //allowing user to be able to search either by doctor/contact name
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Address contains [cd] %@ OR SELF.Location_Name contains [cd] %@ OR SELF.Doctor_Name contains [cd] %@ OR SELF.Pharmacy_Contact_Name contains [cd] %@", [NSString stringWithFormat:@"%@",searchString],[NSString stringWithFormat:@"%@",searchString],[SWDefaults getValidStringValue:searchString],[SWDefaults getValidStringValue:searchString]];
        [predicateArray addObject:predicate];
    }
    
   

    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"compound predicate array in visits filter is %@", [compoundpred description]);
    filteredPlannedVisitsArray=[[plannedVisitsArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    NSLog(@"filtered planned visits array %@ %@",compoundpred,filteredPlannedVisitsArray);
    
    [visitsTblView reloadData];
    [self UpdateScreentitle];
}



#pragma mark Visit Filter Methods
- (IBAction)visitsFilterButtontapped:(id)sender {
    [self.view endEditing:YES];
    [searchBarVisit setShowsCancelButton:NO animated:YES];
    searchBarVisit.text=@"";
    [self searchContent];
    
    
    /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
    
    // Blurred with UIImage+ImageEffects
    blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
    blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
    [self.view addSubview:blurredBgImage];
    
    MedRepVisitsFilterViewController * popOverVC=[[MedRepVisitsFilterViewController alloc]init];
    popOverVC.contentArray=plannedVisitsArray;
    popOverVC.visitsFilterDelegate=self;
    
    if (previousFilteredParameters.count>0) {
        popOverVC.previouslyFilteredContent=previousFilteredParameters;
    }
    popOverVC.filterNavController=self.navigationController;
    popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    filterPopOverController=nil;
    filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    filterPopOverController.delegate=self;
    popOverVC.filterPopOverController=filterPopOverController;
    [filterPopOverController setPopoverContentSize:CGSizeMake(366, 440) animated:YES];
    
    
    
    CGRect relativeFrame = [visitsFilterbtn convertRect:visitsFilterbtn.bounds toView:self.view];

    [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
 }



-(void)filteredVisitsDelegate:(NSMutableArray *)filteredVisits
{
    [filterPopOverController dismissPopoverAnimated:YES];
    [blurredBgImage removeFromSuperview];
    [self searchContent];
    [self deselectPreviousCellandSelectFirstCell];

}

-(void)filteredVisitParameters:(NSMutableDictionary*)filterParameters
{
    self.view.alpha=1.0;
    if ([self.view.subviews containsObject:blurredBgImage]) {
        [blurredBgImage removeFromSuperview];
    }
    previousFilteredParameters=filterParameters;
}

-(void)visitsFilterDidClose
{
    [filterPopOverController dismissPopoverAnimated:YES];
    [blurredBgImage removeFromSuperview];
    [self deselectPreviousCellandSelectFirstCell];
}

-(void)visitsFilterDidCancel
{
    [filterPopOverController dismissPopoverAnimated:YES];
    [blurredBgImage removeFromSuperview];
    [filterPopOverController dismissPopoverAnimated:YES];
    [self searchContent];
    [self deselectPreviousCellandSelectFirstCell];

}



#pragma mark Status Methods



#pragma Mark Task popover controller delegate

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray;
{
    currentVisitTasksArray = tasksArray;
    
    if (tasksArray.count>0)
    {
        NSPredicate * updatedTasksPredicate = [NSPredicate predicateWithFormat:@"SELF.isUpdated  =='YES' "];
        NSMutableArray *updatedTasksArray = [[tasksArray filteredArrayUsingPredicate:updatedTasksPredicate] mutableCopy];
        NSLog(@"updated tasks are %@", updatedTasksArray);
        
        if (updatedTasksArray.count>0)
        {
            NSString *documentDir;
            if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                documentDir = [paths objectAtIndex:0];
            }
            else
            {
                NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
                documentDir = [filePath path] ;
            }
            
            FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
            [database open];
            BOOL updatedTasksSuccessfully;
            
            NSString* assignedTo=[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"];
            NSString* userID=[[SWDefaults userProfile]valueForKey:@"User_ID"];
            NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++)
            {
                VisitTask * currentTask=[updatedTasksArray objectAtIndex:i];
                
                @try {
                    
                    updatedTasksSuccessfully =  [database executeUpdate:@"INSERT OR Replace INTO PHX_TBL_Tasks (Task_ID,Title, Description,Category,Priority,Start_Time, Status,Assigned_To,Emp_ID, Location_ID,Show_Reminder,IS_Active,Is_deleted,Created_At,Created_By,Doctor_ID,Contact_ID,End_Time,Last_Updated_At,Last_Updated_By,Custom_Attribute_1)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", currentTask.Task_ID, currentTask.Title==nil?@"":currentTask.Title, currentTask.Description==nil?@"":currentTask.Description, currentTask.Category==nil?@"":currentTask.Category, currentTask.Priority==nil?@"":currentTask.Priority, currentTask.Start_Time, currentTask.Status==nil?@"":currentTask.Status,assignedTo, assignedTo, currentTask.Location_ID==nil?@"":currentTask.Location_ID, @"N",@"Y",@"N", currentTask.Start_Time, createdBy, [NSString isEmpty:[selectedVisitDetailsDict valueForKey:@"Doctor_ID"]]?[NSNull null]: [selectedVisitDetailsDict valueForKey:@"Doctor_ID"],[NSString isEmpty:[selectedVisitDetailsDict valueForKey:@"Contact_ID"]]?[NSNull null]: [selectedVisitDetailsDict valueForKey:@"Contact_ID"], currentTask.End_Time, currentTask.Start_Time, userID, @"IPAD",nil];
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
            }
            
            //after updating in visit addl info update the status in task table as well.
            
            BOOL updatetaskStatus=NO;
            
            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [formatterTime setLocale:usLocaleq];
            NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
            
            for (NSInteger i=0; i<updatedTasksArray.count; i++)
            {
                VisitTask * task=[updatedTasksArray objectAtIndex:i];
                @try
                {
                    updatetaskStatus =  [database executeUpdate:@"Update  PHX_TBL_Tasks set Status = ?, Last_Updated_At=?, Last_Updated_By=?  where Task_ID='%@'",task.Status,createdAt,createdBy,task.Task_ID, nil];
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    [database close];
                }
            }
        }
    }
    //[self loadSelectedVisitDetails:selectedVisitDetailsDict];
    [self refreshTaskandNotesStatus:selectedVisitDetailsDict];

}

#pragma mark UIAlertController Methods
-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:KAlertOkButtonTitle
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    
}
#pragma mark ButtonActions
-(IBAction)LocationMapViewButtonTapped:(id)sender
{
    [self displayMapViewPopUpView];
}
-(IBAction)ContcatDetailsButtonTapped:(id)sender
{
    [self displayContactPopUpView];
}

#pragma mark popUpViewMethods
-(void)displayMapViewPopUpView
{
    MedRepVisitsLocationMapPopUpViewController *presentingView=   [[MedRepVisitsLocationMapPopUpViewController alloc]init];
    presentingView.VisitDetailsDictionary=selectedVisitDetailsDict;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    mapPopUpViewController.navigationBarHidden=YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}
-(void)displayContactPopUpView
{
    MedRepVisitContactDetailsPopUpViewController *presentingView=   [[MedRepVisitContactDetailsPopUpViewController alloc]init];
    presentingView.VisitDetailsDictionary=selectedVisitDetailsDict;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    mapPopUpViewController.navigationBarHidden=YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}
-(void)showNoVisitErrorMessageView
{
    [NoVisitMessageView setHidden:NO];
    [startVisitButton setEnabled:NO];
}
-(void)hideNoVisitErrorMessageView
{
    [NoVisitMessageView setHidden:YES];
}

-(void)disableNavigationBarOptionsOnPopOverPresentation
{

    [startVisitButton setEnabled:NO];
    [editVisitButton setEnabled:NO];
    [createVisitButton setEnabled:NO];

    [self.navigationItem.leftBarButtonItem setEnabled:NO];
}
-(void)EnableNavigationBarOptionsOnPopOverDismissal
{
    [startVisitButton setEnabled:YES];
    [editVisitButton setEnabled:YES];
    [createVisitButton setEnabled:YES];
    
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
    
    if(filteredPlannedVisitsArray.count>0)
    {
        if(selectedVisitIndexPath.row>=filteredPlannedVisitsArray.count)
            [self tableView:visitsTblView didSelectRowAtIndexPath:selectedVisitIndexPath];
        else
            [self SetFirstIndexVisitSelected];

    }
    else
    {
        [startVisitButton setEnabled:NO];
        [editVisitButton setEnabled:NO];


    }
}
#pragma mark DefaultSelections
-(void)SetFirstIndexVisitSelected
{
    selectedVisitIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [self deselectPreviousCellandSelectFirstCell];
}





-(void)deselectPreviousCellandSelectFirstCell
{
    if (filteredPlannedVisitsArray.count>0 && [visitsTblView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[visitsTblView cellForRowAtIndexPath:selectedVisitIndexPath];
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            cell.cellDividerImg.hidden=NO;
        }
        [visitsTblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                   animated:YES
                             scrollPosition:UITableViewScrollPositionNone];
        [visitsTblView.delegate tableView:visitsTblView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}


-(void)UpdateScreentitle
{
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    NSString* currentDateinDisplayFormat=[MedRepQueries fetchDatabaseDateFormat];


    NSString * titleText=[[NSString alloc]init];
    
    if([previousFilteredParameters valueForKey:@"Visit_Date"]!=nil)
    {
        NSString* refinedCurrentDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[previousFilteredParameters valueForKey:@"Visit_Date"]];
        titleText = [NSLocalizedString(@"Planned Visits", nil) stringByAppendingString:[NSString stringWithFormat:@": %@",refinedCurrentDate]];
        
        
        if([[previousFilteredParameters valueForKey:@"Visit_Date"] isEqualToString:currentDateinDisplayFormat])
        {
            [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        }
        else
        {
            [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
        }
    }
    else
    {
        titleText = NSLocalizedString(@"Planned Visits", nil);
        [visitsFilterbtn setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    }
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(titleText, nil)];
    

}

-(void)refreshTaskandNotesStatus:(NSMutableDictionary*)currentVisitDetails
{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){

    NSString*locationType =[selectedVisitDetailsDict valueForKey:@"Location_Type"];
    VisitDetails *tempVisitDetails=[MedRepDefaults fetchVisitDetailsObject:currentVisitDetails];

        

        
    if ([locationType isEqualToString:kDoctorLocation])
    {
        currentVisitTasksArray=[MedRepQueries fetchTaskObjectsforLocationID:[selectedVisitDetailsDict valueForKey:@"Location_ID"] andDoctorID:@""];
        tempVisitDetails.taskArray=[currentVisitTasksArray mutableCopy];

        
        tempVisitDetails.notesArray=[MedRepQueries fetchNoteObjectwithLocationID:tempVisitDetails.Location_ID andDoctorID:@""];
        
        
    }
    
    else if ([locationType isEqualToString:kPharmacyLocation])
    {
        currentVisitTasksArray=[MedRepQueries fetchTaskObjectswithLocationID:[selectedVisitDetailsDict valueForKey:@"Location_ID"] andContactID:@""];
        tempVisitDetails.taskArray=[currentVisitTasksArray mutableCopy];

        
        
        tempVisitDetails.notesArray=[MedRepQueries fetchNotesObjectswithLocationID:tempVisitDetails.Location_ID andContactID:@""];
        
        
    }

        dispatch_async(dispatch_get_main_queue(), ^(void){

    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodeForTaskandNotes:tempVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
            
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }

        });
    });

}
#pragma mark UIPopoverPresentationControllerDelegateMethods
- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    return NO;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView  * __nonnull * __nonnull)view{
    
    
    //    NSLog(@"view %@",view);
    //    NSLog(@"view.subViews %@",view);
    
    
}
-(void)lastVisitNotes:(NSString*)visitID
{
   lastVisitNote = [MedRepQueries fetchLastVisitNote:visitID];
}
-(void)showLastVisitDetails:(NSString*)selectedLocationID
{
    lastVisitDetailsDict = [MedRepQueries fetchLastVisitDetailsforLocation:selectedLocationID];
    NSLog(@"last visit details data is %@",lastVisitDetailsDict);
    
    NSString* lastVisitOnString=[NSString getValidStringValue:[lastVisitDetailsDict valueForKey:@"Last_Visited_On"]];
    if (![NSString isEmpty:lastVisitOnString]) {
        NSString* refinedLastVisitedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:lastVisitOnString];
        lastVisitedOnLbl.text=[NSString getValidStringValue:refinedLastVisitedDate];
    }
}
- (IBAction)lastVisitDetailsButtonTapped:(id)sender {
    
    NSString* locationID = [NSString getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Location_ID"]];
    
    if([[AppControl retrieveSingleton].FM_ENABLE_VISIT_HISTORY_REPORT isEqualToString:KAppControlsYESCode]){
        
        FMVisitHistoryViewController * visitHistoryVC =  [[FMVisitHistoryViewController alloc]init];
        CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
        NSError *error;
        NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
        NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
        
        NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
        NSURL *url = [NSURL URLWithString:strurl];
        NSString *strParams =[[NSString alloc] init];
        NSString *strName=@"sync_MC_ExecGetVisitHistory";
        NSString* username = [SWDefaults username];
        NSString* password = [SWDefaults password];
        NSString* userID = [[SWDefaults userProfile]valueForKey:@"SalesRep_ID"];
        NSMutableDictionary* paramatersDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:userID,@"EmpID",@"P",@"Type",locationID,@"DoctorContactID", nil];
        NSArray* valuesArray = paramatersDict.allValues;
        NSArray* keysArray = paramatersDict.allKeys;
        
        
        for (NSInteger i=0; i<paramatersDict.count; i++) {
            strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[keysArray objectAtIndex:i]];
            strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[valuesArray objectAtIndex:i]];
            
        }
        NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
        NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"ORDER_HISTORY_PARAMETERS_DICT"];
        
        NSString *requestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_VisitHistory",[testServerDict stringForKey:@"name"],strName,strParams];
        
        NSDictionary* orderHistoryDict = [[NSDictionary alloc]initWithObjectsAndKeys:url,@"SalesWorx_Order_History_URL",requestString,@"SalesWorx_Order_History_Paramater_String", nil];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:orderHistoryDict] forKey:@"ORDER_HISTORY_PARAMETERS_DICT"];
        
        [self.navigationController pushViewController:visitHistoryVC animated:YES];
    }
    
    else{
        MedRepDoctorsLastVisitDetailsPopOverViewController * popOverVC=[[MedRepDoctorsLastVisitDetailsPopOverViewController alloc]init];
        popOverVC.lastVisitDetailsDict=lastVisitDetailsDict;
        popOverVC.notesString = lastVisitNote;
        popOverVC.isVisitScreen = YES;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        nav.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popover = nav.popoverPresentationController;
        popover.backgroundColor=[UIColor whiteColor];
        popOverVC.preferredContentSize = CGSizeMake(410, 380);
        popover.delegate = self;
        popover.sourceView = lastVisitDetailsButton.superview;
        popover.sourceRect = lastVisitDetailsButton.frame;
        popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
    }
    
}

#pragma mark Pending visits

/*The requirement for pending visits is to show the list of doctors whose completed visits (PHX_TBL_Actual_Visits) is less then actual visits (Custom_Attribute_1 in tbl_app Codes with key Visits_Req)*/


@end
