//
//  AlSeerSalesOrderViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProductBonusViewController.h"

#import "ProductStockViewController.h"


#import "CustomerSalesViewController.h"

#import "SalesOrderViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "OutletPopoverTableViewController.h"
#import "StringPopOverViewController.h"
#import "AlSeerSalesOrderCalculationViewController.h"

#define NUMERIC @"1234567890"

@interface AlSeerSalesOrderViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate,StringPopOverDelegate>




{
    
    

    
    IBOutlet UILabel *lblProductName;
    IBOutlet UILabel *lblProductCode;
    IBOutlet UILabel *lblProductBrand;
    IBOutlet UILabel *lblWholesalePrice;
    IBOutlet UITextField *txtfldWholesalePrice;
    IBOutlet UILabel *lblRetailPrice;
    IBOutlet UILabel *lblUOMCode;
    IBOutlet UILabel *lblAvlStock;
    IBOutlet UILabel *lblExpDate;
    IBOutlet UILabel *lblPriceLabel;
    IBOutlet UILabel *lblCustomerLabel;
    IBOutlet UILabel *lblSelectProduct;
    
    IBOutlet UILabel *lblTitleProductCode;
    IBOutlet UILabel *lblTitleWholesalePrice;
    IBOutlet UILabel *lblTitleRetailPrice;
    IBOutlet UILabel *lblTitleUOMCode;
    IBOutlet UILabel *lblTitleAvlStock;
    IBOutlet UILabel *lblTitleExpDate;
    IBOutlet UILabel *txtTitleProductQty;
    IBOutlet UILabel *txtTitleDefBonus;
    IBOutlet UILabel *txtTitleRecBonus;
    IBOutlet UILabel *txtTitleDiscount;
    IBOutlet UILabel *txtTitleFOCQty;
    IBOutlet UILabel *txtTitleFOCItem;
    IBOutlet UILabel *txtTitleNotes;
    
    IBOutlet UITextField *txtProductQty;
    IBOutlet UITextField *txtDefBonus;
    IBOutlet UITextField *txtRecBonus;
    IBOutlet UITextField *txtDiscount;
    IBOutlet UITextField *txtFOCQty;
    
    IBOutlet UITextView *txtNotes;
    
    IBOutlet UIView *productDropDown;
    IBOutlet UIView *salesHistoryView;
    IBOutlet UIView *bonusView;
    IBOutlet UIView *stockView;
    IBOutlet UIView *bottomOrderView;
    
    IBOutlet UIView *dragParentView;
    IBOutlet UIView *dragChildView;
    
    IBOutlet UIButton *salesHistoryBtn;
    IBOutlet UIButton *bonusBtn;
    IBOutlet UIButton *stockBtn;
    IBOutlet UIButton *focProductBtn;
    IBOutlet UIButton *dragButton;
    IBOutlet UIButton *addButton;
    
    IBOutlet UITableView *productTableView;
    IBOutlet UITableView *focProductTableView;
    IBOutlet UISearchBar *candySearchBar;
    BOOL bSearchIsOn;
    
    
    
    BOOL isPproductDropDownToggled;
    BOOL isSalesHistoryToggled;
    BOOL isBonusToggled;
    BOOL isStockToggled;
    BOOL isErrorSelectProduct;
    
    ProductBonusViewController *productBonusViewController;
    ProductStockViewController *productStockViewController;
    CustomerSalesViewController *customerSalesVC;
//    SalesOrderViewController *oldSalesOrderVC;
    AlSeerSalesOrderCalculationViewController *oldSalesOrderVC;

    
    NSMutableDictionary *productDictionary;
    NSMutableDictionary *mainProductDict;
    
    NSMutableArray *productArray;
    NSMutableArray *filteredCandyArray;
    NSMutableArray *finalProductArray;
    NSMutableSet* _collapsedSections;
    
    int bonus;
    BOOL isBonus;
    BOOL scrollViewDelegateFreezed;
    BOOL isPriceCel;
    BOOL isComment;
    BOOL isSaveOrder;
    BOOL isPopover;
    BOOL isBonusAllow;
    BOOL isDiscountAllow;
    BOOL isFOCAllow;
    BOOL isFOCItemAllow;
    BOOL isDualBonusAllow;
    BOOL isAddedRows;
    BOOL isBonusRowDeleted;
    BOOL checkDiscount;
    BOOL checkFOC;
    BOOL checkBonus;
    BOOL isDidSelectStock;
    BOOL isDidSelectBonus;
    BOOL isDidSelectHistory;
    BOOL isPinchDone;
    
    NSString *isBonusAppControl;
    NSString *isFOCAppControl;
    NSString *isDiscountAppControl;
    NSString *isFOCItemAppControl;
    NSString *isDualBonusAppControl;
    NSString *bonusEdit;
    NSString *isLineItemNotes;
    NSString *showHistory;
    BOOL isShowHistory;
    
    NSArray *bonusInfo;
    NSMutableDictionary *bonusProduct;
    NSMutableDictionary *fBonusProduct;
    
    int temp;
    NSString * tempStr;
    ///////////////////////////////////////////
    //Sales Order Objects

    
    
    OutletPopoverTableViewController* outletPopOver;
    UIPopoverController *valuePopOverController;
    StringPopOverViewController *customPopOver;

    NSString* selectedUOMReference;
    
    
    NSMutableDictionary *productReference;
    
    BOOL noPricesAvailableStatus;
}


@property (nonatomic,strong)NSMutableDictionary *customerDict;
@property (nonatomic ,strong)NSString *parentViewString;
@property (nonatomic ,strong)NSMutableDictionary *preOrdered;
@property (nonatomic ,strong)NSMutableArray *preOrderedItems;
@property (nonatomic ,strong)NSString *isOrderConfirmed;
@property(strong,nonatomic)NSMutableArray * wareHouseDataSalesOrderArray;
-(IBAction)buttonAction:(id)sender;
-(void)ResetData;
@property (strong, nonatomic) IBOutlet UIButton *uomButton;


@end
