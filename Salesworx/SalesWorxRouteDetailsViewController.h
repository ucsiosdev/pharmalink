//
//  SalesWorxRouteDetailsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementTitleLabel.h"
#import "MedRepElementDescriptionLabel.h"
#import "MapView2.h"
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"
#import "NSString+Additions.h"
#import "Singleton.h"
#import "SWDatabaseManager.h"
#import "SWAppDelegate.h"
#import "SalesWorxImageView.h"
#import "SalesWorxiBeaconManager.h"
#import "SalesWorxCashCustomerOnsiteVisitOptionsViewController.h"
#import "SalesWorxODOMeterViewController.h"
#import "SalesWorxVisitOptionsViewController.h"

#define kRouteLabelDefaultText @"--"


@interface SalesWorxRouteDetailsViewController : UIViewController<CashCustomerOnsiteVisitOptionsDelegate>
{
    Customer* selectedCustomer;
    NSString* openPlannedDetailID;
    NSString* openVisitID;
    NSString* openPlannedVisitID;
    SalesWorxVisit* salesWorxVisit;
    
    IBOutlet UILabel *lblReschedule;
    IBOutlet UIImageView *routeRescheduleImage;
}
-(void)updateCustomerDetails:(NSMutableDictionary*)selectedCustomerDict;

@property (strong, nonatomic) IBOutlet SalesWorxImageView *statusImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;

@property (strong, nonatomic) IBOutlet UIButton *btnCustomerStatus;

@property (strong, nonatomic) IBOutlet UILabel *lblCreditLimit;
@property (strong, nonatomic) IBOutlet UILabel *lblAvailabelBalance;
@property (strong, nonatomic) IBOutlet UILabel *lblPDCDue;
@property (strong, nonatomic) IBOutlet UILabel *lblOverDue;

@property (strong, nonatomic) IBOutlet UILabel *lblTotalOutstandings;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentMonthSales;
@property (strong, nonatomic) IBOutlet UILabel *lblPlannedVisits;
@property (strong, nonatomic) IBOutlet UILabel *lblCompletedVisits;
@property (strong, nonatomic) IBOutlet UILabel *lblOutOfRoute;

@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *lastVisitedOnHeaderLabel;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *lastVisitedDateLabel;

@property (strong, nonatomic) IBOutlet  MKMapView *locationMapView;

-(void)updateSelectedCustomerData:(Customer*)selectedCustomerObject withSelectedDate:(NSDate*)selectedDateObject;

@end
