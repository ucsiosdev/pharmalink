//
//  SalesWorxRouteDetailsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxRouteDetailsViewController.h"

@interface SalesWorxRouteDetailsViewController ()

@end

@implementation SalesWorxRouteDetailsViewController

@synthesize lblCreditLimit,lblAvailabelBalance,lblPDCDue,lblOverDue,lastVisitedDateLabel,lastVisitedOnHeaderLabel,lblTotalOutstandings,lblCurrentMonthSales,lblCompletedVisits,lblOutOfRoute,lblPlannedVisits,lblName,lblNumber,lblAddress,statusImageView,locationMapView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateSelectedCustomerData:(Customer*)selectedCustomerObject withSelectedDate:(NSDate*)selectedDateObject
{
    selectedCustomer=selectedCustomerObject;

    lblReschedule.hidden = YES;
    routeRescheduleImage.hidden = YES;
    
    
    if ([selectedCustomerObject.visitStatus isEqualToString:@"N"]) {
        NSMutableArray *arrRescheduledVisitsForSelectedCustomer = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Visit_Change_Log where Customer_ID = '%@' and Site_Use_ID = '%@' and date(Visit_Date) = date('now')", selectedCustomer.Customer_ID, selectedCustomer.Ship_Site_Use_ID]];
        
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        
        NSString *selectDate = [formatter stringFromDate:selectedDateObject];
        NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
        
        if (![todayDate isEqualToString:selectDate]) {
            
            // rescheduling checks ignore for future visits
            arrRescheduledVisitsForSelectedCustomer = [[NSMutableArray alloc]init];
        }
        
        if (arrRescheduledVisitsForSelectedCustomer.count > 0) {
            
            lblReschedule.hidden = NO;
            routeRescheduleImage.hidden = NO;
            
            NSMutableDictionary *dictObj = [arrRescheduledVisitsForSelectedCustomer objectAtIndex:0];
            NSString *approvalStatus = [NSString getValidStringValue:[dictObj valueForKey:@"Approval_Status"]];
            
            if ([approvalStatus isEqualToString:@"N"]) {
                if ([[dictObj valueForKey:@"Change_Type"] isEqualToString:@"R"] && [[dictObj valueForKey:@"Proc_Status"] isEqualToString:@"Y"]) {
                    lblReschedule.text = @"Route Reschedule Approval Pending";
                } else if ([[dictObj valueForKey:@"Change_Type"] isEqualToString:@"D"] && [[dictObj valueForKey:@"Proc_Status"] isEqualToString:@"Y"]) {
                    lblReschedule.text = @"Route Delete Approval Pending";
                } else if ([[dictObj valueForKey:@"Change_Type"] isEqualToString:@"D"] && [[dictObj valueForKey:@"Proc_Status"] isEqualToString:@"N"]) {
                    lblReschedule.text = @"Route Delete Request Pending";
                } else {
                    lblReschedule.text = @"Route Reschedule Request Pending";
                }
            } else {
                lblReschedule.hidden = YES;
                routeRescheduleImage.hidden = YES;
//                if ([approvalStatus isEqualToString:@"A"]) {
//                    lblReschedule.text = @"Cancel Reschedule Request Approved";
//                } else if ([approvalStatus isEqualToString:@"R"]) {
//                    lblReschedule.text = @"Cancel Reschedule Request Rejected";
//                } else if ([approvalStatus isEqualToString:@"C"]) {
//                    lblReschedule.text = @"Cancel Reschedule Request Cancelled";
//                } else if ([approvalStatus isEqualToString:@"V"]) {
//                    lblReschedule.text = @"Cancel Reschedule Visit Already Completed";
//                } else if ([approvalStatus isEqualToString:@"X"]) {
//                    lblReschedule.text = @"Cancel Reschedule Request Auto Rejected";
//                }
            }
        }
    }
    
    Singleton *single = [Singleton retrieveSingleton];
    lblName.text = [NSString getValidStringValue:selectedCustomerObject.Customer_Name];
    lblNumber.text = [NSString getValidRouteStringValue:selectedCustomerObject.Customer_No];
    lblAddress.text = [NSString getValidRouteStringValue:selectedCustomerObject.Address];
    lblCreditLimit.text=[[NSString getValidRouteStringValue:selectedCustomerObject.Credit_Limit] currencyString];
    lblAvailabelBalance.text=[[NSString getValidRouteStringValue:selectedCustomerObject.Avail_Bal] currencyString];
    if ([[NSString getValidStringValue:selectedCustomerObject.Cust_Status] isEqualToString:@"Y"]) {
        [statusImageView setBackgroundColor:kActiveCustomerBackgroundColor];
    } else {
        [statusImageView setBackgroundColor:kBlockedCustomerBackgroundColor];
    }
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSMutableArray * outStandingArray = [self fetchTotalOutStandingforCustomer:[NSString getValidStringValue:selectedCustomerObject.Customer_ID]];            /** Outstandings*/

        NSMutableArray* totalSalesArray=[[[SWDatabaseManager retrieveManager] dbGetCustomerTotalSalesWithCustomerID:selectedCustomerObject.Customer_ID] mutableCopy];/** Monthly Sales*/
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        NSString *dateString = [dateFormat stringFromDate:selectedDateObject];
        NSArray * arrPlannedVisits= [[SWDatabaseManager retrieveManager] dbGetTotalPlannedVisits:dateString];  /** Planned Visits*/
        NSArray * arrCompletedVisits= [[SWDatabaseManager retrieveManager] dbGetTotalCompletedVisits:dateString]; /** Completed Visits*/
        NSArray * arrOutOfRoute= [[SWDatabaseManager retrieveManager] dbGetCustomerOutOfRouteCount:dateString];  /** Out Of Route*/

        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            
            /** Outstandings*/

            if (outStandingArray.count>0) {
                NSMutableDictionary* outstandingDict=[outStandingArray objectAtIndex:0];
                NSString* pdcDueString=[NSString getValidStringValue:[outstandingDict valueForKey:@"PDC_Due"]];
                NSString* overDueString=[NSString getValidStringValue:[outstandingDict valueForKey:@"Over_Due"]];
                NSString* outStandingString=[NSString getValidStringValue:[outstandingDict valueForKey:@"Total"]];
                
                lblPDCDue.text=[pdcDueString isEqualToString:@"0"]||[NSString isEmpty:pdcDueString]?kRouteLabelDefaultText:[pdcDueString currencyString];
                lblOverDue.text=[overDueString isEqualToString:@"0"] || [NSString isEmpty:overDueString]?kRouteLabelDefaultText:[overDueString currencyString];
                lblTotalOutstandings.text=[outStandingString isEqualToString:@"0"] || [NSString isEmpty:outStandingString]?kRouteLabelDefaultText:[outStandingString currencyString];
            }else{
                lblPDCDue.text=kRouteLabelDefaultText;
                lblOverDue.text=kRouteLabelDefaultText;
                lblTotalOutstandings.text=kRouteLabelDefaultText;
            }
            
            /** Monthly Sales*/
            if (totalSalesArray.count>0) {
                NSMutableDictionary* totalSalesDict=[totalSalesArray objectAtIndex:0];
                NSString* currentMonthSalesString=[NSString getValidStringValue:[totalSalesDict valueForKey:@"Bk0_Amt"]];
                lblCurrentMonthSales.text=[currentMonthSalesString isEqualToString:@"0"] || [NSString isEmpty:currentMonthSalesString]?kRouteLabelDefaultText:[currentMonthSalesString currencyString];
            }else{
                lblCurrentMonthSales.text = kRouteLabelDefaultText;
            }
            
            /** Planned Visits*/
            if (arrPlannedVisits.count>0) {
                lblPlannedVisits.text = [[[arrPlannedVisits objectAtIndex:0]valueForKey:@"Total"]stringValue];
            }
            else{
                lblPlannedVisits.text = @"0";
            }
            
            /** Completed Visits*/
            if (arrCompletedVisits.count >0){
                lblCompletedVisits.text = [[[arrCompletedVisits objectAtIndex:0]valueForKey:@"Total"]stringValue];
            }
            else{
                lblCompletedVisits.text = @"0";
            }
            
            /** Out Of Route*/
            if (arrOutOfRoute.count >0){
                lblOutOfRoute.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrOutOfRoute.count];
            }else{
                lblOutOfRoute.text = @"0";
            }
        });
    });
    
    
    
    
    
    
    
    
   
    
    
   
    

    
    single.planDetailId = selectedCustomerObject.FSR_Plan_Detail_ID;
    



   
    
    
    
    [self updateCustomerLastVisitDetails:selectedCustomerObject];
    
    [self updateMapData];
    
    [self updateBeaconData];
}

-(void)updateMapData
{
    double selectedCustLat = [[SWDefaults getValidStringValue:selectedCustomer.Cust_Lat] doubleValue];
    double selectedCustLong = [[SWDefaults getValidStringValue:selectedCustomer.Cust_Long] doubleValue];
    
    CLLocationCoordinate2D selectedCustomerCoordinate= CLLocationCoordinate2DMake(selectedCustLat, selectedCustLong);
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = selectedCustomerCoordinate;
    Pin.title=[SWDefaults getValidStringValue:selectedCustomer.Customer_Name];
    
    [locationMapView removeAnnotations:locationMapView.annotations];
    [locationMapView addAnnotation:Pin];
    
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (selectedCustomerCoordinate, 500, 500);
    [locationMapView setRegion:regionCustom animated:YES];
}
-(void)updateBeaconData{
    
    NSString* beaconCustomerID=[SWDefaults getValidStringValue:selectedCustomer.Customer_ID];
    NSString* beaconSiteUseID=[SWDefaults getValidStringValue:selectedCustomer.Site_Use_ID];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSMutableArray * beaconDetailsArray=[[SWDatabaseManager retrieveManager]fetchBeaconDetailsforCustomers];
        NSLog(@"beacon detaisl array in route %@", beaconDetailsArray);
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            if (beaconDetailsArray.count>0) {
                NSPredicate * matchBeaconPredicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Customer_ID ==  %@ and SELF.Site_Use_ID ==  %@",beaconCustomerID,beaconSiteUseID]];
                NSMutableArray * matchedBeaconDataArray=[[beaconDetailsArray filteredArrayUsingPredicate:matchBeaconPredicate]mutableCopy];
                if (matchedBeaconDataArray.count>0) {
                    NSLog(@"matched beacon in route %@", matchedBeaconDataArray);
                    NSMutableDictionary * beaconDict=[matchedBeaconDataArray objectAtIndex:0];
                    SalesWorxBeacon * currentCustomerBeacon=[[SalesWorxBeacon alloc]init];
                    currentCustomerBeacon.Beacon_UUID=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_UUID"]];
                    currentCustomerBeacon.Beacon_Major=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Major"]];
                    currentCustomerBeacon.Beacon_Minor=[SWDefaults getValidStringValue:[beaconDict valueForKey:@"Beacon_Minor"]];
                    [self scanForiBeacon:currentCustomerBeacon];
                }
            }
        });
    });
}
#pragma mark Beacon Methods
-(void)scanForiBeacon:(SalesWorxBeacon*)currentCustomerBeacon
{
    NSLog(@"scanning for iBeacon in customers");
    
    [[SalesWorxiBeaconManager retrieveSingleton]initilizeiBeaconManager:currentCustomerBeacon];
    
    NSLog(@"check detected beacon manager %@",[SalesWorxiBeaconManager retrieveSingleton].detectedBeacon );
}

#pragma mark customer last visit
-(void)updateCustomerLastVisitDetails:(Customer*)selectCust{
    __block NSString *lastVisitedDateStr=[[NSString alloc] init];
    lastVisitedDateLabel.text=KNotApplicable;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        Customer *cust=[[Customer alloc]init];
        cust.Customer_ID =[SWDefaults getValidStringValue:selectCust.Customer_ID];
        cust.Ship_Customer_ID =[SWDefaults getValidStringValue:selectCust.Customer_ID];
        
        cust.Ship_Site_Use_ID=[SWDefaults getValidStringValue:selectCust.Ship_Site_Use_ID];
        lastVisitedDateStr=[SWDefaults getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:cust];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            lastVisitedDateLabel.text=lastVisitedDateStr.length==0?KNotApplicable:lastVisitedDateStr;
        });
    });
    
}

-(NSMutableArray*)fetchTotalOutStandingforCustomer:(NSString*)selectedCustomerID
{
    NSString * outStandingQry=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"Select IFNULL(Total_Due,'N/A')  AS Total, IFNULL(Over_Due,'N/A') AS Over_Due, IFNULL(PDC_Due,'N/A') AS PDC_Due from TBL_Customer_Dues where Customer_ID = '%@'",selectedCustomerID]];
    
    NSLog(@"total outstanding qry %@", outStandingQry);
    
    NSMutableArray *temp=  [[[SWDatabaseManager retrieveManager] fetchDataForQuery:outStandingQry] mutableCopy];
    
    if (temp.count>0) {
        
        return temp;
    }
    else
    {
        return nil;
    }
}


#pragma mark Map View Methods

- (void)mapView:(MKMapView *)currentMapView didSelectAnnotationView:(MKAnnotationView *)view
{
    SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];

    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        NSString* googleMapsUrlString;
        
        if (![NSString isEmpty:[NSString getValidStringValue:selectedCustomer.Cust_Lat]] && [NSString isEmpty:[NSString getValidStringValue:selectedCustomer.Cust_Long]]) {
            
            googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[selectedCustomer.Cust_Lat doubleValue],[selectedCustomer.Cust_Long doubleValue]];
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        }
        else
        {
            NSURL *googleMapsUrl=[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
              [[UIApplication sharedApplication]openURL:googleMapsUrl];
        }
    }
    else
    {
        if (![NSString isEmpty:[NSString getValidStringValue:selectedCustomer.Cust_Lat]] && [NSString isEmpty:[NSString getValidStringValue:selectedCustomer.Cust_Long]]) {
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude,[selectedCustomer.Cust_Lat doubleValue],[selectedCustomer.Cust_Long doubleValue]]];
             [[UIApplication sharedApplication] openURL:googleUrl];
        }
        else
        {
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%f+%f",appDel.currentLocation.coordinate.latitude ,appDel.currentLocation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleUrl];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
