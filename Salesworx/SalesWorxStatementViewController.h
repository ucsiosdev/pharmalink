//
//  SalesWorxStatementViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"
#import "SalesWorxCustomerDuesTableViewCell.h"
#import <MessageUI/MessageUI.h>

@interface SalesWorxStatementViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    NSDictionary *customer;
    NSMutableArray *invoices;
    UILabel *balanceLabel;
    
    double _total;
    double _balance;
    IBOutlet HMSegmentedControl *statementSegmentControl;
    
    NSMutableArray * customerDuesArray;
    
    NSMutableDictionary*  duesDict;
    
}

@property (weak, nonatomic) IBOutlet UITableView *statementTableView;
@property(strong,nonatomic) Customer * selectedCustomer;
@property(strong,nonatomic ) IBOutlet UIScrollView *statementScrollView;
@property(strong,nonatomic) UIPopoverPresentationController * popOverController;
@property(assign,nonatomic) double totalAmtPaid;

- (id)initWithCustomer:(NSDictionary *)customer;




@end
