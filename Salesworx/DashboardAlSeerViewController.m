



//
//  DashboardAlSeerViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "DashboardAlSeerViewController.h"
#import "ZUUIRevealController.h"
#import "SWDatabaseManager.h"
#import "ChartTableViewCell.h"
#import "DataTableViewCell.h"
#import "DataHeaderTableViewCell.h"
#import "GraphHeaderTableViewCell.h"
#import "AlSeerWelcomeViewController.h"
#import "AlSeerDashboardChartTableViewCell.h"


@interface DashboardAlSeerViewController ()

@end

@implementation DashboardAlSeerViewController




@synthesize agencyArray,chartView,syncStatusLbl,lastSyncDateLbl,achievementPercentLbl,totalInvoicesLbl,previousDaySaleLbl,balancetoGoLbl,dailysalestoGoLbl,achLbl,targetArray,totalTargetLbl,salesTargetItemsArray,plannedLbl,completedLbl,outofRouteLbl,adherenceLbl,orderCount,returnCount,dailyTargetValue,targetAchievedLbl,userNameLbl;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //check user profile
    

    
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BOOL checkWelcome=[[NSUserDefaults standardUserDefaults]boolForKey:@"welcomeDisplayed"];
    
    if (checkWelcome==NO) {
        welcomeVC=[[AlSeerWelcomeViewController alloc]init];
        [self presentViewController:welcomeVC animated:YES completion:nil];
        [self performSelector:@selector(hideWelcomeView) withObject:nil afterDelay:2.0];
    }
    
    
    NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
    
    NSLog(@"user profile is %@", userCurrencyCode );
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)hideWelcomeView

{
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"welcomeDisplayed"];

        self.view.backgroundColor=[UIColor clearColor];
        [self.dashTblView reloadData];
        [self viewDidLoad];
        [self viewWillAppear:YES];
    }];
}



-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}


-(void)populateToplevelLabels
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    dateString = [dateFormat stringFromDate:today];
    
    NSArray *temp1 = [[SWDatabaseManager retrieveManager] dbGetTotalPlannedVisits:dateString];
    if([temp1 count]==0)
    {
        plannedLbl.text = @"0";
    }
    else
    {
        plannedLbl.text = [[temp1 objectAtIndex:0] stringForKey:@"Total"];
    }

    
    
    //completed visits
    
    NSArray *temp5 = [[SWDatabaseManager retrieveManager] dbGetTotalCompletedVisits:dateString];
    if([temp5 count]==0)
    {
        completedLbl.text = @"0";
    }
    else
    {
        completedLbl.text = [[temp5 objectAtIndex:0] stringForKey:@"Total"];
    }

    
    
    
    
    //calculate adherence
    
    
    
    
    NSString * plannedStr= [NSString stringWithFormat:@"%@", [[temp1 valueForKey:@"Total"] objectAtIndex:0]];
    NSString * completedStr= [NSString stringWithFormat:@"%@", [[temp5 valueForKey:@"Total"] objectAtIndex:0]];

    
    
    
    double plannedVisits= [[[temp1 valueForKey:@"Total"] objectAtIndex:0] doubleValue];
    
    
    double completedVisits= [[[temp5 valueForKey:@"Total"] objectAtIndex:0] doubleValue];

    double Adherence= (completedVisits/plannedVisits)*100;
    
    if ([plannedStr isEqualToString:@"0"]|| [completedStr isEqualToString:@"0"]) {
       
        adherenceLbl.text=@"0";


    }
    
    else
    {
        adherenceLbl.text=[[NSString stringWithFormat:@"%0.0f", Adherence] stringByAppendingString:@"%"];

        
        
    }
    
    
    
    
    
    
    
    //out of route
    
    
    NSArray *temp6 = [[SWDatabaseManager retrieveManager] dbGetCustomerOutOfRoute:dateString];
    if([temp6 count]==0)
    {
        outofRouteLbl.text = @"0";
    }
    else
    {
        outofRouteLbl.text = [temp6 objectAtIndex:0];
    }
    
    
    //total order
    NSArray *temp7 = [[SWDatabaseManager retrieveManager] dbGetTotalOrder];
    if([temp7 count]==0)
    {
        orderCount.text = @"0";
    }
    else
    {
        orderCount.text = [[temp7 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    
    
    NSArray *temp8 = [[SWDatabaseManager retrieveManager] dbGetTotalReturn];
    if([temp8 count]==0)
    {
        returnCount.text = @"0";
    }
    else
    {
        returnCount.text = [[temp8 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    
    
    
    
    
    
    
    
    
    //target achieved
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value] FROM TBL_Sales_Target_Items";
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    if (targetdatArry.count==0) {
        
        //targetAchievedLbl.text=[NSString stringWithFormat:@"%@",[targetdatArry valueForKey:@"Target_Value"]];
        
    }
    
    
    
    
    
}

-(void)fetchSalesTargets

{
//    NSString* salesTargetsStr=@"Select * from TBL_Sales_Target_Items";

    
    
    NSString* salesTargetsStr=@"SELECT Classification_1 ,SUM(Custom_Attribute_8) AS Custom_Attribute_8 , SUM(Custom_Attribute_7) AS Custom_Attribute_7 ,SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items GROUP BY Classification_1 ORDER BY Classification_1";

    salesTargetItemsArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesTargetsStr];
    
    
    if (salesTargetItemsArray.count>0) {
        
        NSLog(@"check sales target %@", [salesTargetItemsArray description]);
        
        
        
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Old_nav_top"] forBarMetrics:UIBarMetricsDefault];
    
    
    NSString* userName=[[SWDefaults userProfile] stringForKey:@"Username"];
    
    if (userName) {
        userNameLbl.text=userName;

        
    }
    

//    arrayForFirstMonth=[[NSMutableArray alloc]initWithObjects:@"1",@"2", nil];
//    arrayForSecondMonth=[[NSMutableArray alloc]initWithObjects:@"1",@"2", nil];
//    arrayForThirdMonth=[[NSMutableArray alloc]initWithObjects:@"1",@"2", nil];
//    
//    
//    
//    
//    
//    /*** Small Chart ****/
//    self.myBarChart.delegate=self;
//    self.myBarChart.barGraphStyle=BAR_GRAPH_STYLE_GROUPED;
//    self.myBarChart.barLabelStyle=BAR_LABEL_STYLE1;
//    self.myBarChart.tag=10;
//    self.myBarChart.margin=MIMMarginMake(0, -20, -10, 0);
//    self.myBarChart.glossStyle=GLOSS_STYLE_1;
//    self.myBarChart.xTitleStyle=XTitleStyle2;
//    [self.myBarChart drawBarChart];
//    self.myBarChart.backgroundColor = [UIColor clearColor];
//    
//    self.myBarChart.titleLabel.text = @"One";
//    self.myBarChart.titleLabel1.text = @"Two";
//    self.myBarChart.titleLabel2.text = @"Three";
////
//    self.myBarChart.titleLabel.frame = CGRectMake(65, 190, 100, 20);
//    self.myBarChart.titleLabel1.frame = CGRectMake(205, 190, 100, 20);
//    self.myBarChart.titleLabel2.frame = CGRectMake(330, 190, 100, 20);
//    
//    self.myBarChart.plannedViewColor.frame = CGRectMake(380,5, 30, 10);
//    self.myBarChart.completedViewColor.frame = CGRectMake(380, 20, 30, 10);
//    // self.myBarChart.unPlanedViewColor.frame = CGRectMake(380, 35, 30, 10);
//    //self.myBarChart.missedViewColor.frame = CGRectMake(380, 50, 30, 10);
//    
//    self.myBarChart.plannedViewColorLable.frame = CGRectMake(415,0, 100, 20);
//    self.myBarChart.completedViewColorLable.frame = CGRectMake(415, 15, 100, 20);
//    //self.myBarChart.unPlanedViewColorLable.frame = CGRectMake(415, 30, 100, 20);
//    //self.myBarChart.missedViewColorLable.frame = CGRectMake(415, 45, 100, 20);
//    
//    self.myBarChart.plannedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
//    self.myBarChart.completedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    //self.myBarChart.unPlanedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    //self.myBarChart.missedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    self.navigationController.navigationBar.translucent = NO;
    
    
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.title=@"Dashboard";
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Old_nav_top"] forBarMetrics:UIBarMetricsDefault];
    
    
    [syncStatusLbl setText:[SWDefaults lastSyncStatus]];
    [lastSyncDateLbl setText:[SWDefaults lastSyncDate]];
    
    
    [self fetchSalesTargets];
    
    
    [self populateToplevelLabels];
    
    //fetch total Target
    
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items";
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    
    
    if (targetdatArry.count>0) {
        
        
        //        NSInteger testInt =[[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]] integerValue];
        //
        //       // NSString* test=[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]];
        //
        //
        //        totalTargetLbl.text=[[NSNumber numberWithInt:test] descriptionWithLocale:[NSLocale currentLocale]];
        //
        //        //[SWDefaults formatWithThousandSeparator:testInt];
        //        [@1234567 descriptionWithLocale:[NSLocale currentLocale]];  // 1,234,567
        
        
        
        NSInteger tempTarget=[[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]] integerValue];
        
        
        NSString* strNumberOfUsers = [self formatWithThousandSeparator:tempTarget];
        
        //totalTargetLbl.text=strNumberOfUsers;
        
        totalTargetLbl.text= [[NSNumber numberWithInt:tempTarget] descriptionWithLocale:[NSLocale currentLocale]];
        
        
        
        //        totalTargetLbl.text=[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]];
        
        //calculate balance
        
        
        
        if ([[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] isEqual:[NSNull null]]) {
            
            
        }
        
        else
        {
        
        float totalTarget=[[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] floatValue];
        
        float achievedTarget=[[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] floatValue];
        //
        float balanceTogo= totalTarget-achievedTarget;
        
        float achievementPercent=(achievedTarget/totalTarget)*100;
        
        achLbl.text=[[NSString stringWithFormat:@"%0.2f",achievementPercent] stringByAppendingString:@"%"];
        
   
        
        
        int myInt = (int) balanceTogo;
        
        
        
        
        NSString* strNumber = [self formatWithThousandSeparator:myInt];
        
        //balancetoGoLbl.text=strNumber;
        
        
        }
        
        
        //fetch btg from table
        
        
        NSString* btgQry=@"select sum(custom_Attribute_7) AS Balance_To_Go from TBL_Sales_Target_Items";
        
        NSMutableArray* btgArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:btgQry];
        if (btgArray.count>0) {
            
            if ([[[btgArray objectAtIndex:0]valueForKey:@"Balance_To_Go"] isEqual:[NSNull null]]) {
                
                
            }
            
            else
            {
                
                
                
            
                
                double btgDouble=[[[btgArray objectAtIndex:0]valueForKey:@"Balance_To_Go"] doubleValue];
                
                balancetoGoLbl.text=[NSString stringWithFormat:@"%0.0f", btgDouble];
            }

            
            
        }
        
        
        
        //balancetoGoLbl.text=[NSString stringWithFormat:@"%0.0f",balanceTogo];
        
        
        //calculate previous day sales
        
        
        NSString* prevDaySalesQuery=@"SELECT  sum(Custom_Attribute_6) As [Previous_Day_Sales]  FROM TBL_Sales_Target_Items";
        NSMutableArray* prevDaySales=[[SWDatabaseManager retrieveManager]fetchDataForQuery:prevDaySalesQuery];
        
        if (prevDaySales.count>0) {
            
            
            
            
            
            
            NSInteger tempTarget=[[NSString stringWithFormat:@"%@",[[prevDaySales valueForKey:@"Previous_Day_Sales"] objectAtIndex:0]]integerValue];
            
            
            NSString* strNumberOfUsers = [self formatWithThousandSeparator:tempTarget];
            
            previousDaySaleLbl.text=strNumberOfUsers;
            
            
            
            
            
            
            
            
        }
        
        
        
        
        
        //calculate target achieved from tbl_order_History
        
        
        
        
        
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        
        [dateformate setDateFormat:@"YYYY-MM-dd"];
       // NSString* dummytimeStr=@" 00:00:00";

        NSString *dateString=[dateformate stringFromDate:[NSDate date]];
        
        
        NSLog(@"check time stamp %@", dateString);
        
        
        NSString* queryStr=@"SELECT SUM(Transaction_Amt) FROM TBL_Order_History where Doc_Type='I' and Creation_Date='2015-02-13'";
        
        
        
        NSString* orderQry=[NSString stringWithFormat:@"SELECT SUM(Transaction_Amt) AS Target_Achieved FROM TBL_Order_History where Doc_Type='I' and Creation_Date like  '%%%@%%'",dateString];
        
        NSLog(@"order qry is %@", orderQry);
        
        NSMutableArray* achArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:orderQry];
        if (achArr.count==0|| [[achArr valueForKey:@"Target_Achieved"] isEqual:[NSNull null]]) {
            
        }
        
        {
            
            // NSInteger testTar=[NSString stringWithFormat:@"%d",[[achArr valueForKey:@"Target_Value"] integerValue]];
            
            
            id test=[achArr valueForKey:@"Target_Achieved"];
            
            
            NSLog(@"test is %@", [test objectAtIndex:0]);
            
            
            if ([test objectAtIndex:0]==nil || [test objectAtIndex:0]==[NSNull null]) {
                
                
                targetAchievedLbl.text=@"0";
                
            }
            
            else
            {
                
                
                
                
                double targetAchieved=[[test objectAtIndex:0] doubleValue];
                
                targetAchievedLbl.text= [NSString stringWithFormat:@"%0.2f",targetAchieved];
            }
            
            
        }
        
        
        
        
        //calculate Achievement  daily sales to go
        
        
        
        
        NSString* dsgQry=@"SELECT SUM(Custom_Attribute_8) As [Daily_Sales_To_Go], SUM(Sales_Value) AS Sales_Value FROM TBL_Sales_Target_Items";
        dsgArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:dsgQry];
        
        
        if (dsgArr.count>0) {
            
            
            
            // dailysalestoGoLbl.text=[NSString stringWithFormat:@"%@", [[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0]];
            
            
            
            if ([[[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0] isEqual:[NSNull null]]) {
                
                
                
            }
            
            
            else
            {
            NSInteger tempTargetDsg=[[NSString stringWithFormat:@"%@", [[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0]]integerValue];
            
            
            NSString* dsgStr = [self formatWithThousandSeparator:tempTargetDsg];
            
            
            dailysalestoGoLbl.text= [[NSNumber numberWithInt:tempTargetDsg] descriptionWithLocale:[NSLocale currentLocale]];
            
            
            
            
            
            
            
            dailyTargetValue.text=[NSString stringWithFormat:@"%0.0f", [[[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0] floatValue]];
            
            
            
            
            
            
            
            
            // NSInteger dailtTarget=[[NSString stringWithFormat:@"%d", [[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0]integerValue]];
            
            
            // NSString* dsgStr = [self formatWithThousandSeparator:tempTargetDsg];
            
            
            dailyTargetValue.text= [[NSNumber numberWithInt:tempTargetDsg] descriptionWithLocale:[NSLocale currentLocale]];
            
            
            
            
                
            
            
            NSInteger tempTarget=[[NSString stringWithFormat:@"%@", [[dsgArr valueForKey:@"Sales_Value"] objectAtIndex:0]]integerValue];
            
            
            NSString* strNumberOfUsers = [self formatWithThousandSeparator:tempTarget];
            
            totalInvoicesLbl.text=strNumberOfUsers;
            
            }
            //            totalInvoicesLbl.text=[NSString stringWithFormat:@"%@", [[dsgArr valueForKey:@"Sales_Value"] objectAtIndex:0]];
            
        }
        
        
        
        
        //        NSString* totaltarget=[targetdatArry valueForKey:@"Target_Value"] ;
        //        NSString* achievedValue=[targetdatArry valueForKey:@"Sales_Value"] ;
        //
        //        float balancetoGo=totaltarget-achievedValue;
        //
        //        balancetoGoLbl.text=[NSString stringWithFormat:@"%f",balancetoGo];
        
        
        
    }
    
    
    NSMutableArray* agencyDetails=[[SWDatabaseManager retrieveManager] fetchAgencies];
    NSLog(@"Agancies are %@", [agencyDetails valueForKey:@"Classification_1"]);
    
    targetArray=[agencyDetails valueForKey:@"Target_Value"];
    agencyArray =[agencyDetails valueForKey:@"Agency"];
    
    NSLog(@"agency array is %@", [agencyArray description]);
    
    
    // self.myBarChart=[[MIMBarGraph alloc]init];
    // [self.myBarChart createLabels:agencyArray];
    
    
    
    
    
    //    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
    //                                                          BoldSemiFontOfSize(13.0f), UITextAttributeFont,
    //                                                          nil] forState:UIControlStateNormal];
    //
    //    [UIBarButtonItem appearance]setTitleTextAttributes:<#(NSDictionary *)#> forState:<#(UIControlState)#>
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
            [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
            
            
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
            
            
            //            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"cache:NO] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
            
        } else {
        }
    }
    
    
    
    
    
    
    
    
    

    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - DELEGATE METHODS

//
//-(NSArray *)valuesForGraph:(id)graph
//{
//   // NSArray *yValuesArray ;
//
//    if([(MIMBarGraph *)graph tag]>=10 && [(MIMBarGraph *)graph tag]<=11)
//    {
////        yValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:[arrayForFirstMonth objectAtIndex:0],[arrayForFirstMonth objectAtIndex:1], nil],[NSArray arrayWithObjects:[arrayForSecondMonth objectAtIndex:0],[arrayForSecondMonth objectAtIndex:1], nil],[NSArray arrayWithObjects:[arrayForThirdMonth objectAtIndex:0],[arrayForThirdMonth objectAtIndex:1], nil],nil];
//
//        //yValuesArray=[[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6", nil];
//
//        yValuesArray=[[NSArray alloc]initWithObjects:@"10000",@"21000",@"24000",@"11000",@"5000",@"2000",@"9000",@"4000",@"10000",@"17000",@"15000",@"11000",nil];
//
//    }
//
//
//    return yValuesArray;
//}
//
//-(NSArray *)valuesForXAxis:(id)graph
//{
//    NSArray *xValuesArray=nil;
//
//    if([(MIMBarGraph *)graph tag]>=10 && [(MIMBarGraph *)graph tag]<=11)
//        xValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",@" 1000"],@"Feb",@"Mar",@"Apr", nil], nil];
//
//    return xValuesArray;
//}
//
//-(NSArray *)titlesForXAxis:(id)graph
//{
//    NSArray *xValuesArray;
//
//    if([(MIMBarGraph *)graph tag]>=10 && [(MIMBarGraph *)graph tag]<=11)
//
//        xValuesArray=[[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6", nil];
//
//
////        xValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForFirstMonth objectAtIndex:0]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForFirstMonth objectAtIndex:1]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForFirstMonth objectAtIndex:2]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForFirstMonth objectAtIndex:3]]], nil],[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForSecondMonth objectAtIndex:0]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForSecondMonth objectAtIndex:1]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForSecondMonth objectAtIndex:2]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForSecondMonth objectAtIndex:3]]], nil],[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForThirdMonth objectAtIndex:0]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForThirdMonth objectAtIndex:1]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForThirdMonth objectAtIndex:2]]],[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@" %@",[arrayForThirdMonth objectAtIndex:3]]], nil], nil];
//
//    return xValuesArray;
//
//}
//
//
//-(NSDictionary *)barProperties:(id)graph;
//{
//    return barProperty;
//}
//
//-(NSDictionary *)horizontalLinesProperties:(id)graph
//{
//    if([(MIMBarGraph *)graph tag]==10)
//        return [NSDictionary dictionaryWithObjectsAndKeys:@"1,2",@"dotted", nil];
//
//    return nil;
//}
//
//-(NSDictionary *)xAxisProperties:(id)graph
//{
//    if([(MIMBarGraph *)graph tag]==12)
//        return [NSDictionary dictionaryWithObjectsAndKeys:@"0.95,0.95,0.95",@"groupTitleBgColor", nil];
//
//    return nil;
//}
//
//-(NSDictionary *)animationOnBars:(id)graph
//{
//    if([(MIMBarGraph *)graph tag]==10)
//        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:BAR_ANIMATION_VGROW_STYLE],[NSNumber numberWithFloat:0.1],[NSNumber numberWithFloat:0.5], nil] forKeys:[NSArray arrayWithObjects:@"type",@"animationDelay",@"animationDuration" ,nil] ];
//    return nil;
//}
//


#pragma mark - DELEGATE METHODS
//
//
//-(NSArray *)valuesForGraph:(id)graph
//{
//    // NSArray *yValuesArray;
//    
//    if([(MIMBarGraph *)graph tag]>=10 && [(MIMBarGraph *)graph tag]<=11)
//    {
//        yValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:@"10000",@"21000", nil],[NSArray arrayWithObjects:@"5000",@"2000", nil],[NSArray arrayWithObjects:@"10000",@"17000", nil],nil];
//        
//       
//        
//        NSLog(@"yval Array is %@", [yValuesArray description]);
//        
//    
//        
//    }
//    
//    
//    
//    
//    
//    return yValuesArray;
//    
//    
//    
//}
//
//-(NSArray *)valuesForXAxis:(id)graph
//{
//    // NSArray *xValuesArray=nil;
//    
//    if([(MIMBarGraph *)graph tag]>=10 && [(MIMBarGraph *)graph tag]<=11)
//        xValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil], nil];
//    
//    
//    if([(MIMBarGraph *)graph tag]>=12)
//        xValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil],[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil],[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil], nil];
//    
//    
//    
//    
//    return xValuesArray;
//}
//
//-(NSArray *)titlesForXAxis:(id)graph
//{
//    // NSArray *xValuesArray;
//    
//    if([(MIMBarGraph *)graph tag]>=10 && [(MIMBarGraph *)graph tag]<=11)
//        xValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil], nil];
//    
//    
//    if([(MIMBarGraph *)graph tag]>=12)
//        xValuesArray=[[NSArray alloc]initWithObjects:[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil],[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil],[NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr", nil],[NSArray arrayWithObjects:@"May",@"Jun",@"Jul",@"Aug", nil],[NSArray arrayWithObjects: @"Sep",@"Oct",@"Nov",@"Dec", nil], nil];
//    
//    
//    return xValuesArray;
//    
//}
//
//-(NSArray *)grouptitlesForXAxis:(id)graph
//{
//    NSArray *gValuesArray=nil;
//    
//    if([(MIMBarGraph *)graph tag]>=12)
//        gValuesArray=[[NSArray alloc]initWithObjects:@"Q1 2009",@" Q2 2009",@"Q3 2009",@"Q1 2010",@"Q2 2010",@"Q3 2010",@"Q1 2011",@"Q2 2011",@"Q3 2011", nil];
//    
//    return gValuesArray;
//    
//}
//
//-(NSDictionary *)barProperties:(id)graph;
//{
//    return barProperty;
//}
//
//-(NSDictionary *)horizontalLinesProperties:(id)graph
//{
//    if([(MIMBarGraph *)graph tag]==10)
//        return [NSDictionary dictionaryWithObjectsAndKeys:@"1,2",@"dotted", nil];
//    
//    if([(MIMBarGraph *)graph tag]==11)
//        return [NSDictionary dictionaryWithObjectsAndKeys:@"4,1",@"dotted", nil];
//    
//    if([(MIMBarGraph *)graph tag]==12)
//        return [NSDictionary dictionaryWithObjectsAndKeys:@"1,4",@"dotted", nil];
//    
//    return nil;
//}
//-(UILabel *)createLabelWithText:(NSString *)text
//{
//    //    UILabel *a=[[UILabel alloc]initWithFrame:CGRectMake(5, myTableView.frame.size.width * 0.5 + 20, 310, 20)];
//    //    [a setBackgroundColor:[UIColor clearColor]];
//    //    [a setText:text];
//    //    a.numberOfLines=5;
//    //    [a setTextAlignment:UITextAlignmentCenter];
//    //    [a setTextColor:[UIColor blackColor]];
//    //    [a setFont:[UIFont fontWithName:@"Helvetica" size:12]];
//    //    [a setMinimumFontSize:8];
//    //    return a;
//    
//}
//-(NSDictionary *)xAxisProperties:(id)graph
//{
//    if([(MIMBarGraph *)graph tag]==12)
//        return [NSDictionary dictionaryWithObjectsAndKeys:@"0.95,0.95,0.95",@"groupTitleBgColor", nil];
//    
//    return nil;
//}
//
//-(NSDictionary *)animationOnBars:(id)graph
//{
//    if([(MIMBarGraph *)graph tag]==10)
//        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:BAR_ANIMATION_VGROW_STYLE],[NSNumber numberWithFloat:1.0],[NSNumber numberWithFloat:1.0], nil] forKeys:[NSArray arrayWithObjects:@"type",@"animationDelay",@"animationDuration" ,nil] ];
//    else if([(MIMBarGraph *)graph tag]==13)
//        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:BAR_ANIMATION_VGROW_STYLE],[NSNumber numberWithFloat:1.0], nil] forKeys:[NSArray arrayWithObjects:@"type",@"animationDuration" ,nil] ];
//    return nil;
//}




#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag==1) {
        
        return agencyArray.count;
    }
    
    else
    {
    
    return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ChartTableViewCell";
    static NSString *datacellIdentifier = @"dataTableViewCell";

    
    tableView.allowsSelection = NO;
    

    
    
//    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_graph_bg"]];
//    [tempImageView setFrame:tableView.frame];

//    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"Old_graph_bg"]];
//    tableView.backgroundColor = background;
    
    //tableView.backgroundView = tempImageView;
    
    if (tableView.tag==1) {
        DataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DataTableViewCell" owner:nil options:nil] firstObject];
            
            //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
           
            if ([[salesTargetItemsArray valueForKey:@"Classification_1"]isEqual:[NSNull null]] || [salesTargetItemsArray valueForKey:@"Classification_1"]==nil) {
                
                cell.agencyLbl.text=@"-";
            }
            
            else
            {
                cell.agencyLbl.text=
                [[salesTargetItemsArray valueForKey:@"Classification_1"] objectAtIndex:indexPath.row];
                

            }
            
            
                        
            
            
            
            
            NSString* dailyTargetStr=[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row];
            NSInteger dailyTarget=0;
            
            if (!dailyTargetStr) {
                
                
            }
            
            else
            {
            
             dailyTarget=[dailyTargetStr integerValue];
            }
            
            
            if (!dailyTarget) {
                
                cell.targetLbl.text=@"0";
                
            }
            
            else
            {
                 cell.targetLbl.text= [[NSNumber numberWithInt:dailyTarget] descriptionWithLocale:[NSLocale currentLocale]];
            }
           
            
            
            if ([[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] isEqual:[NSNull null]]|| [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]==nil) {
                
                
                
                
                cell.MTDsalesLbl.text=@"0";
                
                cell.achPercentLbl.text=@"0";
                cell.dailySalesLbl.text=@"0";
                
                cell.btgLbl.text=@"0";
            }
            
            
            
            else
            {
            NSInteger dailySale=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]integerValue];
            
            
            cell.MTDsalesLbl.text= [[NSNumber numberWithInt:dailySale] descriptionWithLocale:[NSLocale currentLocale]];
            
           
            
            
            //cell.targetLbl.text=  [NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] ];
            
            
           // cell.MTDsalesLbl.text=  [NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] ];

           
            
            float totalTarget=[[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] floatValue];
            
            float achievedTarget=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] floatValue];
            //
            //float balanceTogo= totalTarget-achievedTarget;
            
            float achievementPercent=(achievedTarget/totalTarget)*100;
            
            
                if (totalTarget==0) {
                    
                    cell.achPercentLbl.text=@"0.00%";
                }
                
                else
                {
                
            cell.achPercentLbl.text=[[NSString stringWithFormat:@"%0.2f", achievementPercent] stringByAppendingString:@"%"];
            
                }
            
            
            NSInteger tempTargetDsg=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row]]integerValue];
            
            
          
            
            cell.btgLbl.text= [[NSNumber numberWithInt:tempTargetDsg] descriptionWithLocale:[NSLocale currentLocale]];

            
            
            NSInteger dailySalestoGo=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_8"] objectAtIndex:indexPath.row]]integerValue];
            

            cell.dailySalesLbl.text= [[NSNumber numberWithInt:dailySalestoGo] descriptionWithLocale:[NSLocale currentLocale]];

            
            
          //  float btgVal=[[[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row] floatValue];

            
          //  cell.btgLbl.text=[NSString stringWithFormat:@"%0.0f",btgVal];
            
            }
            
            
        }
        
        return cell;
    }
    
    else
    {
        AlSeerDashboardChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerDashboardChartTableViewCell" owner:nil options:nil] firstObject];
        }
        
        [cell configUI:indexPath];
        
        
        return cell;

        
    }
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==0) {
        
        return 200;
    }
    
    else
    {
    return 50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
   
    if (tableView.tag==1) {
        static NSString *datacellIdentifier = @"dataTableViewCell";

        DataHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DataHeaderTableViewCell" owner:nil options:nil] firstObject];
            
            //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
        }
        
        return cell;

    }
    
    
    else
    {
    
//    CGRect frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 10);
//    UIImageView *bgImg = [[UIImageView alloc]initWithFrame:frame];
//    //label.text = section ? @"Bar":@"Line";
//        
//        bgImg.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Old_title_bg"]];
//        
//        //bgImg.backgroundColor=[UIColor colorWithRed:(91/255.0) green:(91/255.0) blue:(104/255.0) alpha:1];
//    return bgImg;
        
        static NSString *datacellIdentifier = @"dataTableViewCell";
        
        GraphHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GraphHeaderTableViewCell" owner:nil options:nil] firstObject];
            
            //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
        }
        
        return cell;

    }
}



@end
