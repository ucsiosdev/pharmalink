//
//  SWRouteTableViewCell.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWRouteTableViewCell.h"

@implementation SWRouteTableViewCell

@synthesize lblTitle = _lblTitle;
@synthesize accessoryImage = _accessoryImage;
@synthesize dividerImage = _dividerImage;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
