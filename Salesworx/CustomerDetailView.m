//
//  CustomerDetailView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CustomerDetailView.h"
#import "SWFoundation.h"


@implementation CustomerDetailView

@synthesize tableView;
@synthesize customer;
@synthesize customerHeaderView;

- (id)initWithCustomer:(NSMutableDictionary *)row {
    self = [super initWithFrame:CGRectZero];
    if (self)
    {
        
        self.customer = [NSMutableDictionary dictionaryWithDictionary:row  ] ;
        //customerSer.delegate = self;
        //[self setCustomer:[row mutableCopy]];
        
        self.backgroundColor = [UIColor whiteColor];

        self.tableView=nil;
        [self setTableView:[[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.bounds.size.width, self.bounds.size.height) style:UITableViewStyleGrouped] ];
        [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.tableView setDelegate:self];
        [self.tableView setDataSource:self];
        self.tableView.backgroundView = nil;
        self.tableView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.tableView];
        
        self.customerHeaderView=nil;
        [self setCustomerHeaderView:[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:self.customer] ];
        [self.customerHeaderView setShouldHaveMargins:YES];
        [self addSubview:self.customerHeaderView];

        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[self.customer stringForKey:@"Customer_ID"]]];

    }
    
    return self;
}



-(void)willMoveToSuperview:(UIView *)newSuperview
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
        
    }
}

#pragma UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 2;
        case 1:
            return 2;
        case 2:
            return 4;
        case 3:
            return 3;
        case 4:
            return 1;
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }
    
       

    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell.textLabel setText:NSLocalizedString(@"Address", nil)];
            if([self.customer objectForKey:@"Address"] == (id)[NSNull null] || [[self.customer objectForKey:@"Address"] length] == 0 )
            {
                [cell.detailTextLabel setText:@"Not Defined"];
            }
            else
            {
                [cell.detailTextLabel setText:[self.customer objectForKey:@"Address"]];
            }
        } else if (indexPath.row == 1) {
            [cell.textLabel setText:NSLocalizedString(@"City", nil)];
            [cell.detailTextLabel setText:[self.customer objectForKey:@"City"]];
            
            
            //for City = "Not Defined" *****OLA!!

            if([self.customer objectForKey:@"City"] == (id)[NSNull null] || [[self.customer objectForKey:@"City"] length] == 0 )
            {
                [cell.detailTextLabel setText:@"Not Defined"];
            }
            else {
                [cell.detailTextLabel setText:[self.customer objectForKey:@"City"]];
            }
            
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [cell.textLabel setText:NSLocalizedString(@"P.O Box", nil)];
            if([self.customer objectForKey:@"Postal_Code"] == (id)[NSNull null] || [[self.customer objectForKey:@"Postal_Code"] length] == 0 )
            {
                [cell.detailTextLabel setText:@"Not Defined"];
            }
            else {
                [cell.detailTextLabel setText:[self.customer objectForKey:@"Postal_Code"]];
            }
        } else if (indexPath.row == 1) {
            [cell.textLabel setText:NSLocalizedString(@"Location", nil)
];
            
            if([self.customer objectForKey:@"Location"] == (id)[NSNull null] || [[self.customer objectForKey:@"Location"] length] == 0 )
            {
                [cell.detailTextLabel setText:@"Not Defined"];
            }
            else {
                [cell.detailTextLabel setText:[self.customer objectForKey:@"Location"]];
            }
        }
    }
    else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            [cell.textLabel setText:NSLocalizedString(@"Credit Limit", nil)];
            [cell.detailTextLabel setText:[self.customer currencyStringForKey:@"Credit_Limit"]];
        } else if (indexPath.row == 1) {
            [cell.textLabel setText:NSLocalizedString(@"Available Balance", nil)];
            [cell.detailTextLabel setText:[[SWDefaults availableBalance] currencyString]];
            //[cell.detailTextLabel setText:[self.customer currencyStringForKey:@"Avail_Bal"]];
        } else if (indexPath.row == 2) {
            [cell.textLabel setText:NSLocalizedString(@"PDC Due", nil)];
            if ([[self.customer stringForKey:@"PDC_Due"] isEqualToString:@"N/A"])
            {
                [cell.detailTextLabel setText:[self.customer stringForKey:@"PDC_Due"]];

            }
            else
            {
                [cell.detailTextLabel setText:[self.customer currencyStringForKey:@"PDC_Due"]];
            }
        }
            else if (indexPath.row == 3) {
            [cell.textLabel setText:NSLocalizedString(@"Overdue", nil)];
            if ([[self.customer stringForKey:@"Over_Due"] isEqualToString:@"N/A"])
            {
                [cell.detailTextLabel setText:[self.customer stringForKey:@"Over_Due"]];
            }
            else
            {
                [cell.detailTextLabel setText:[self.customer currencyStringForKey:@"Over_Due"]];
            }
          //  [cell.detailTextLabel setText:[self.customer currencyStringForKey:@"Over_Due"]];
        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            BOOL customerStatus = [[self.customer objectForKey:@"Cust_Status"] isEqualToString:@"Y"];
            BOOL cashCustomer = [[self.customer objectForKey:@"Cash_Cust"] isEqualToString:@"Y"];
            BOOL creditHold = [[self.customer objectForKey:@"Credit_Hold"] isEqualToString:@"Y"];
            
            NSString *imageName = @"Old_transparent";
            
            if (cashCustomer) {
                imageName = @"Old_yellow";
            }
            
            if (!customerStatus || creditHold) {
                imageName = @"Old_red";
            }
            cell.imageView.frame = CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y-10, cell.imageView.frame.size.width, cell.imageView.frame.size.height+20);
            [cell.imageView setImage:[UIImage imageNamed:imageName cache:NO]];
            
            [cell.textLabel setText:@"Customer Status"];
        }
    }

    return cell;
}
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tv deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}

//- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{

    [SWDefaults setCustomer:self.customer];

    self.customer = [SWDefaults validateAvailableBalance:orderAmmount];


    
    [self.tableView reloadData];
    orderAmmount=nil;
}
@end
