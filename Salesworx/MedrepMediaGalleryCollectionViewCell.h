//
//  MedrepMediaGalleryCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/5/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedrepMediaGalleryCollectionViewCell : UICollectionViewCell<UIScrollViewDelegate>
{
}
@property (strong,nonatomic) IBOutlet UIScrollView *mediaScrollview;
@property (strong,nonatomic)  IBOutlet UIImageView *mediaImageView;

@end
