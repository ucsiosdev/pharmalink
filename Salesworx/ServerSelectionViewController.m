//
//  ServerSelectionViewController.m
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ServerSelectionViewController.h"
#import "SWPlatform.h"
//#import "CJSONSerializer.h"
//#import "CJSONDeserializer.h"

@interface ServerSelectionViewController ()

@end

@implementation ServerSelectionViewController

@synthesize target;
@synthesize selectionChanged;

- (id)init {
    self = [super init];
    
    if (self) {
        [self setTitle:@"Select a Server"];
        CJSONDeserializer *jsonSerializer = [CJSONDeserializer deserializer];
        NSError *error;
        NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];

       serverArray = [NSMutableArray arrayWithArray:[jsonSerializer deserializeAsArray:data error:&error]  ];
    }
    
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [serverArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell)
    {
        cell=nil;
    }
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.textLabel.font = LightFontOfSize(14);

    [cell.textLabel setText:[[serverArray objectAtIndex:indexPath.row]stringForKey:@"serverName"]];
    [cell.detailTextLabel setText:[[serverArray objectAtIndex:indexPath.row]stringForKey:@"serverLink"]];
    return cell;
    }
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.selectionChanged withObject:[serverArray objectAtIndex:indexPath.row]];
#pragma clang diagnostic pop

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
