//
//  SWRouteTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepView.h"
#import "SalesWorxImageView.h"
@interface SWRouteTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *dividerImage;
@property (weak, nonatomic) IBOutlet SalesWorxImageView *accessoryImage;
@property (weak, nonatomic) IBOutlet MedRepView *statusView;


@end
