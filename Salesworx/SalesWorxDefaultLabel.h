//
//  SalesWorxDefaultLabel.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 8/3/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"

@interface SalesWorxDefaultLabel : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
@property (nonatomic) IBInspectable BOOL isTextEndingWithExtraSpecialCharacter;

-(NSString*)text;
-(void)setText:(NSString*)newText;

@end
