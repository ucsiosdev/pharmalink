//
//  SalesWorxTargetVSAchievementViewController.m
//  MedRep
//
//  Created by Nethra on 9/12/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxTargetVSAchievementViewController.h"
#import "TargetVSAchievementTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "SWDatabaseManager.h"

@interface SalesWorxTargetVSAchievementViewController ()

@end

@implementation SalesWorxTargetVSAchievementViewController
@synthesize arrDocumentType;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIViewControllerBackGroundColor;
    viewFooter.backgroundColor = MedRepMenuTitleUnSelectedCellTextColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([kTargetVSAchievmentTitle localizedCapitalizedString], nil)];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    lblTotalAmount.font=kSWX_FONT_SEMI_BOLD(20);
    txtTargetType.text = @"Annually";
    targetType = @"A";
    NSMutableArray *currency = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"Select Currency_Code  from TBL_User"];
    if(currency.count>0){
        NSString *currencyValue = [[currency objectAtIndex:0]valueForKey:@"Currency_Code"];
        lblCurrency1.text = currencyValue;
        lblCurrency2.text = currencyValue;
        lblCurrency3.text = currencyValue;
        lblCurrency4.text = currencyValue;

    }
    arrDocumentType1 = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Code_Value,Code_Description from TBL_App_Codes where Code_Type = 'COMMISSION_FREQUENCY'"];//
    if(arrDocumentType1.count>0)
    {
        arrDocumentType = [[NSMutableArray alloc]init];
        for (NSMutableDictionary* currentDict in arrDocumentType1) {
            NSString *targetDescription = [currentDict valueForKey:@"Code_Description"];
            NSString *categoryType = [currentDict valueForKey:@"Code_Value"];
            [arrDocumentType addObject:targetDescription];
        }
    }
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    int year = [components year];
    txtYearlyCategory.text = [NSString stringWithFormat:@"%i",year];
    components.year = [components year]-1;
    int previousYear = [components year];
    yearlyArr = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:year],[NSNumber numberWithInt:previousYear], nil];
    
    monthlyArr = [NSMutableArray arrayWithObjects:@"Jan",@"Feb",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December", nil];
    
    quartelyArr = [NSMutableArray arrayWithObjects:@"Q1",@"Q2",@"Q3",@"Q4", nil];
    

}
- (IBAction)btnSearch:(id)sender {
   // NSMutableArray *customerListArray=[[[SWDatabaseManager retrieveManager]fetchTargetVSAchievementReport:@"A" year:2019 period:nil] mutableCopy];
    
    NSString* missingString=[[NSString alloc]init];
    
    if ([NSString isEmpty:txtTargetType.text]) {
        
        missingString=@"Frequency";
    }
    else if ([NSString isEmpty:txtYearlyCategory.text]) {
        
        missingString=@"Year";
    }
    else if ([NSString isEmpty:txtYPeriodCategory.text] && ![targetType isEqual:@"A"]) {
        
        missingString=@"Month/Quarter";
    }
    if ([NSString isEmpty:missingString]==NO) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please enter %@",missingString] withController:self];
    }
    else{
    
    
    NSMutableArray *totalTarget = [[SWDatabaseManager retrieveManager]fetchTotalReport:targetType year:txtYearlyCategory.text period:row];
    if(totalTarget.count > 0){
        for(NSMutableDictionary *currentDict in totalTarget){
            if([[currentDict valueForKey:@"TotalTarget"] isKindOfClass:[NSNull class]]){
                lblTotalAmount.text = [NSString stringWithFormat:@"N/A"];
            }
            else{
                lblTotalAmount.text = [self converToCommaString:[[currentDict valueForKey:@"TotalTarget"]floatValue]];
            }
            if([[currentDict valueForKey:@"SalesValue"]isKindOfClass:[NSNull class]]){
                lblSalesAmount.text = [NSString stringWithFormat:@"N/A"];
            }
            else
            {
                lblSalesAmount.text = [self converToCommaString:[[currentDict valueForKey:@"SalesValue"]floatValue]];
            }
            if([[currentDict valueForKey:@"Ideal"]isKindOfClass:[NSNull class]]){
                lblIdealAmount.text = [NSString stringWithFormat:@"N/A"];
            }
            else{
                lblIdealAmount.text = [ self converToCommaString:[[currentDict valueForKey:@"Ideal"]floatValue]];

            }
            if([[currentDict valueForKey:@"Actual"] isKindOfClass:[NSNull class]]){
                lblActaulAmount.text = [NSString stringWithFormat:@"N/A"];
            }
            else{
        
                lblActaulAmount.text = [ self converToCommaString:[[currentDict valueForKey:@"Actual"]floatValue]];
            }

        }
    }
    else{
            lblTotalAmount.text = @"N/A";
            lblSalesAmount.text = @"N/A";
            lblIdealAmount.text = @"N/A";
            lblActaulAmount.text = @"N/A";
        
        }
        
   
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        NSMutableArray *customerListArray=[[[SWDatabaseManager retrieveManager]fetchTargetVSAchievementReport:targetType year:txtYearlyCategory.text period:row] mutableCopy];
        arrBlockedCustomerData = [[NSMutableArray alloc]init];
        if (customerListArray.count>0) {
            //doing fast enumeration and modifying the dict so added __strong
            for (NSMutableDictionary * currentDict in customerListArray)
            {
                NSMutableDictionary *currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                TargetVsAchievement *currentCustomer = [[TargetVsAchievement alloc] init];
                currentCustomer.product_Family = [currentCustDict valueForKey:@"Product_Family"];
                currentCustomer.target = [currentCustDict valueForKey:@"Target"];
                currentCustomer.sales_Total = [currentCustDict valueForKey:@"Sales_Total"];
                currentCustomer.ach = [currentCustDict valueForKey:@"Ach"];
                currentCustomer.rate = [currentCustDict valueForKey:@"Rate"];
                currentCustomer.ideal = [currentCustDict valueForKey:@"Ideal"];
                currentCustomer.actual = [currentCustDict valueForKey:@"Actual"];
                [arrBlockedCustomerData addObject:currentCustomer];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            
            [tblTargetVSAchievement reloadData];
        });
    });
}
    
}
- (IBAction)btnReset:(id)sender {
    txtTargetType.text = @"";
    txtYearlyCategory.text = @"";
    txtYPeriodCategory.text = @"";
    [arrBlockedCustomerData removeAllObjects];
    [tblTargetVSAchievement reloadData];
    
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrBlockedCustomerData.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"targetVSAchievementCell";
    TargetVSAchievementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"TargetVSAchievementTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height-1, cell.contentView.frame.size.width, 1)];
    [separator setBackgroundColor:[UIColor colorWithRed:203.0/255.0 green:216.0/255.0 blue:223.0/255.0 alpha:1.0]];
    [cell.contentView addSubview:separator];
    cell.isHeader=YES;
   
    cell.lblProduct.isHeader=YES;
    cell.lblTarget.isHeader=YES;
    cell.lblSalesValue.isHeader=YES;
    cell.lblPercentageArch.isHeader=YES;
    cell.lblPercentage.isHeader=YES;
    cell.lblIdeal.isHeader=YES;
    cell.lblActual.isHeader=YES;
    
    cell.lblProduct.text = NSLocalizedString(@"Classification", nil);
    cell.lblTarget.text = NSLocalizedString(@"Target", nil);;
    cell.lblSalesValue.text = NSLocalizedString(@"Sales Value", nil);;
    cell.lblPercentageArch.text = NSLocalizedString(@"Achievement%", nil);;
    cell.lblPercentage.text = NSLocalizedString(@"Comm%", nil);;
    cell.lblIdeal.text = NSLocalizedString(@"Ideal Comm", nil);;
    cell.lblActual.text = NSLocalizedString(@"Actual Comm", nil);;

    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"targetVSAchievementCell";
    TargetVSAchievementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"TargetVSAchievementTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TargetVsAchievement *data = [arrBlockedCustomerData objectAtIndex:indexPath.row];
    cell.lblProduct.text = data.product_Family;
    cell.lblTarget.text = [self converToCommaString:[data.target floatValue]];

    cell.lblSalesValue.text = [self converToCommaString:[data.sales_Total floatValue]];
    cell.lblPercentageArch.text = [self converToCommaString:[data.ach floatValue]] ;
    cell.lblPercentage.text = [ NSString stringWithFormat:@"%@",data.rate];
    cell.lblIdeal.text = [ self converToCommaString:[data.ideal floatValue]];
    cell.lblActual.text = [self converToCommaString:[data.actual floatValue]];

    
    // Change double to nsnumber:
    

   // cell.lblAmount.text = [[NSString stringWithFormat:@"%@",data.Amount] currencyString];
   // NSString* shippingInstructions=[NSString getValidStringValue:data.Shipping_Instructions];
    
//    if ([shippingInstructions isEqualToString:@"N/A"]) {
//        cell.lblStatus.text=kNewStatus;
//    }
//    else if ([shippingInstructions isEqualToString:@""]) {
//        cell.lblStatus.text= KNotApplicable;
//    }
//    else
//    {
//        cell.lblStatus.text=shippingInstructions;
//    }
    
    // cell.lblStatus.text = data.ERP_Status;
    
    return cell;
}
-(NSString *)converToCommaString:(float)floatNumber{

    NSNumber *num = [NSNumber numberWithDouble:floatNumber];
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
    numFormatter.usesGroupingSeparator = YES;
    [numFormatter setGroupingSeparator:@","];
    [numFormatter setGroupingSize:3];
    
    // Get the formatted string:
    NSString *stringNum = [numFormatter stringFromNumber:num];
    
    NSLog(@"%@",stringNum);
    return stringNum;
}

#pragma  mark textfield methods
-(BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField
{
    if(textField == txtTargetType)
    {
        [self textFieldTapped:textField :kAchivementVsTargetFrequencyPopOverTitle :arrDocumentType :266];
        txtYearlyCategory.text = @"";
        txtYPeriodCategory.text = @"";
        targetType = @"A";
        row = nil;
        return NO;
    }else if(textField == txtYearlyCategory){
//        targetType=@"A";
        [self textFieldTapped:textField :kYeralyPopOverTitle :yearlyArr :266];
    }
    else if(textField == txtYPeriodCategory)
    {
        if([txtTargetType.text isEqualToString:@"Monthly"]){
            targetType=@"M";
            lblSelectMonthOrQuarter.text = @"Select Month";
            [self textFieldTapped:textField :kMonthQuarterPopOverTitle :monthlyArr :266];
        }
        else if([txtTargetType.text isEqualToString:@"Quarterly"])
        {
            targetType=@"Q";
            lblSelectMonthOrQuarter.text = @"Select Quarter";
            [self textFieldTapped:textField :kMonthQuarterPopOverTitle :quartelyArr :266];
        }
    }
    return NO;
}

#pragma  mark textfield tapped method

-(void)
textFieldTapped:(MedRepTextField *)txtField :(NSString *)strPop :(NSMutableArray *)arr :(CGFloat )width
{
//    txtCustomer.enabled = YES;
    popString = strPop;
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=arr;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    
    if([popString isEqualToString:kCustomerNamePopOverTitle])
    {
        popOverVC.disableSearch=NO;
    }

    if([strPop isEqualToString:kDocumentTypePopOverTitle])
    {
        popOverVC.enableArabicTranslation=YES;
    }
    
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(width, 273);
    popover.delegate = self;
    popOverVC.titleKey=popString;
    popover.sourceView = txtField.rightView;
    popover.sourceRect =txtField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    
    [self presentViewController:nav animated:YES completion:^{
    }];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([popString isEqualToString:kAchivementVsTargetFrequencyPopOverTitle])
    {
        txtTargetType.text = [NSString stringWithFormat:@"%@",[arrDocumentType objectAtIndex:selectedIndexPath.row]];
        
        if([txtTargetType.text isEqualToString:@"Annually"]){
            targetType=@"A";
            txtYPeriodCategory.hidden = YES;
            lblSelectMonthOrQuarter.hidden = YES;
        }
        else if([txtTargetType.text isEqualToString:@"Monthly"]){
            targetType=@"M";
            txtYPeriodCategory.hidden = NO;
            lblSelectMonthOrQuarter.hidden = NO;
        }
        else{
            targetType=@"Q";
            txtYPeriodCategory.hidden = NO;
            lblSelectMonthOrQuarter.hidden = NO;
        }
        
        if([targetType isEqualToString:@"M"]){
            lblSelectMonthOrQuarter.text = @"Select Month";
        }
        else  if([targetType isEqualToString:@"Q"]){
             lblSelectMonthOrQuarter.text = @"Select Quarter";
        }
    }
    else if ([popString isEqualToString:kYeralyPopOverTitle])
    {
        txtYearlyCategory.text = [NSString stringWithFormat:@"%@",[yearlyArr objectAtIndex:selectedIndexPath.row]];

    }
    else if([popString isEqualToString:kMonthQuarterPopOverTitle])
    {
        if([txtTargetType.text isEqualToString:@"Annually"])
        {
            row = nil;
            
            txtYearlyCategory.text = [NSString stringWithFormat:@"%@",[yearlyArr objectAtIndex:selectedIndexPath.row]];
        }else if([txtTargetType.text isEqualToString:@"Monthly"]){
            row = selectedIndexPath.row+1;
            txtYPeriodCategory.text = [NSString stringWithFormat:@"%@",[monthlyArr objectAtIndex:selectedIndexPath.row]];
        }
        else if([txtTargetType.text isEqualToString:@"Quarterly"])
        {
            row = selectedIndexPath.row+1;
            txtYPeriodCategory.text = [NSString stringWithFormat:@"%@",[quartelyArr objectAtIndex:selectedIndexPath.row]];

        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
