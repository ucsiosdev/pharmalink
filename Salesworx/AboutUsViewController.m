//
//  AboutUsViewController.m
//  Salesworx
//
//  Created by msaad on 6/12/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "AboutUsViewController.h"
#import "SWPlatform.h"
#import "UIDevice+IdentifierAddition.h"
#import "UIDeviceHardware.h"
#import "SSKeychain.h"
@interface AboutUsViewController ()

@end

@implementation AboutUsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
    [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    NSLog(@"Device %@",[h platformString]);

}
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"About Us";
        self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.hidesBackButton = YES;
    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          BoldSemiFontOfSize(13.0f), UITextAttributeFont,
                                                          nil] forState:UIControlStateNormal];
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
    {
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self)
        {
            UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
          //  [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
            
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
        }
        else {
            
        }
    }
    
    
    self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];
    
    
    customerLabel.font = BoldSemiFontOfSize(16);
    deviceLabel.font = BoldSemiFontOfSize(16);
    userLabel.font = BoldSemiFontOfSize(16);
    versionLabel.font = BoldFontOfSize(22);
    
    customerLabel.textColor = UIColorFromRGB(0x007095);
    deviceLabel.textColor = UIColorFromRGB(0x007095);
    userLabel.textColor = UIColorFromRGB(0x007095);
    versionLabel.textColor = UIColorFromRGB(0x007095);
    
    customerLabel.text = [SWDefaults licenseIDForInfo];
    userLabel.text = [[SWDefaults userProfile] stringForKey:@"Username"];
    deviceLabel.text = [SSKeychain passwordForService:@"UCS_Salesworx" account:@"user"];//[[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict stringForKey:@"CFBundleShortVersionString"];
    
    
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    
    
    versionLabel.text= [NSString stringWithFormat:@"%@ (%@)",avid,build];
    
   // versionLabel.text = avid;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    
    AppControl *appCtrl=[AppControl retrieveSingleton];
    if ([appCtrl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
        
       // mainImage.image=[UIImage imageNamed:@"aboutUsAlSeer.png"];
        
        mainImage.image=[UIImage imageNamed:@"AboutUs_EN"];

    }
    
    else
    {
    
    
    if([SWDefaults isRTL])
    {
        mainImage.image=[UIImage imageNamed:@"AboutUs_AR"];
    }
    else
    {
        mainImage.image=[UIImage imageNamed:@"AboutUs_EN"];
        NSLog(@"About us");
    }
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)actionEmailComposer
{    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        AppControl *appCtrl=[AppControl retrieveSingleton];
        if ([appCtrl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
            
//            [mailViewController setSubject:@"Saleswars"];

            [mailViewController setSubject:@"MedRep"];

        }
        else
        {
            [mailViewController setSubject:@"SalesWorx"];

        }
        
        
        [mailViewController setMessageBody:@"Kindly provide additional information on SalesWarx \n Regards" isHTML:NO];
        NSArray *toRecipients = [NSArray arrayWithObjects:@"info@ucssolutions.com",nil];
        [mailViewController setToRecipients:toRecipients];
        [[mailViewController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];

        //[self presentModalViewController:mailViewController animated:YES];
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
    else
    {
        
        //NSLog(@"Device is unable to send email in its current state.");
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[self becomeFirstResponder];
	//[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}


@end
