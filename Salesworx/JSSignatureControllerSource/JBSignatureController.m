//
//  JBSignatureController.m
//  JBSignatureController
//
//  Created by Jesse Bunch on 12/10/11.
//  Copyright (c) 2011 Jesse Bunch. All rights reserved.
//

#import "JBSignatureController.h"
#import "JBSignatureView.h"
#import "SWDefaults.h"
#pragma mark - *** Private Interface ***

@interface JBSignatureController() {
@private
	__strong JBSignatureView *signatureView_;
	__strong UIImageView *signaturePanelBackgroundImageView_;
	__strong UIImage *portraitBackgroundImage_, *landscapeBackgroundImage_;
	__strong UIButton *confirmButton_, *cancelButton_ , *clearButton_;
	__unsafe_unretained id<JBSignatureControllerDelegate> delegate_;
}

// The view responsible for handling signature sketching
@property(nonatomic,strong) JBSignatureView *signatureView;

// The background image underneathe the sketch
@property(nonatomic,strong) UIImageView *signaturePanelBackgroundImageView;

// Private Methods
-(void)didTapCanfirmButton;
-(void)didTapCancelButton;

@end



@implementation JBSignatureController
#define kImagesFolder @"Signature"

@synthesize
signaturePanelBackgroundImageView = signaturePanelBackgroundImageView_,
signatureView = signatureView_,
portraitBackgroundImage = portraitBackgroundImage_,
landscapeBackgroundImage = landscapeBackgroundImage_,
confirmButton = confirmButton_,
cancelButton = cancelButton_,
delegate = delegate_,
clearButton = clearButton_;


#pragma mark - *** Initializers ***

/**
 * Designated initializer
 * @author Jesse Bunch
 **/
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
	}
	
	return self;
	
}

/**
 * Initializer
 * @author Jesse Bunch
 **/
-(id)init {
	return [self initWithNibName:nil bundle:nil];
}




#pragma mark - *** View Lifecycle ***

/**
 * Since we're not using a nib. We need to load our views manually.
 * @author Jesse Bunch
 **/

/**
 * Setup the view heirarchy
 * @author Jesse Bunch
 **/
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
	
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
	
	// Background images
	//self.portraitBackgroundImage = [UIImage imageNamed:@"bg-signature-portrait"] ;
	//self.landscapeBackgroundImage = [UIImage imageNamed:@"bg-signature-landscape"];
	self.signaturePanelBackgroundImageView = [[UIImageView alloc] initWithImage:self.portraitBackgroundImage];
	
	// Signature view
	self.signatureView = [[JBSignatureView alloc] init];
	
    //self.signatureView.backgroundColor = [UIColor blueColor];
    
    // Cancel
	self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [self.cancelButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [self.cancelButton setFrame:CGRectMake(720, 10,60, 50)];
    self.cancelButton.backgroundColor = [UIColor clearColor];
    
    //Clear
    self.clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
       [self.clearButton setImage:[UIImage imageNamed:@"Old_SignatureErase"] forState:UIControlStateNormal];
    [self.clearButton setFrame:CGRectMake(627, 270,60, 50)];
    self.clearButton.backgroundColor = [UIColor redColor];
    
	// Confirm
	self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [self.confirmButton setImage:[UIImage imageNamed:@"TickIcon"] forState:UIControlStateNormal];
	[self.confirmButton sizeToFit];
	[self.confirmButton setFrame:CGRectMake(720,270,60, 50)];
    self.confirmButton.backgroundColor = [UIColor redColor];
	// Background Image
    
	[self.signaturePanelBackgroundImageView setFrame:CGRectMake(150,10, 630,311 )];
	//[self.signaturePanelBackgroundImageView setContentMode:UIViewContentModeTopLeft];
    
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {

    
    
    self.signaturePanelBackgroundImageView.image=[UIImage imageNamed:@"Old_Signature_pad_AR"];
    }
    
    else
    {
        self.signaturePanelBackgroundImageView.image=[UIImage imageNamed:@"Old_Signature_pad"];

        
    }
	[self.view addSubview:self.signaturePanelBackgroundImageView];
	
	// Signature View
	[self.signatureView setFrame:CGRectMake(115,50, 580,200 )];
	[self.view addSubview:self.signatureView];
	
	// Buttons
	[self.view addSubview:self.cancelButton];
	[self.view addSubview:self.confirmButton];
	[self.view addSubview:self.clearButton];
	
	// Button actions
	[self.confirmButton addTarget:self action:@selector(didTapCanfirmButton) forControlEvents:UIControlEventTouchUpInside];
	[self.cancelButton addTarget:self action:@selector(didTapCancelButton) forControlEvents:UIControlEventTouchUpInside];
    [self.clearButton addTarget:self action:@selector(didTapClearButton) forControlEvents:UIControlEventTouchUpInside];
}

/**
 * Support for different orientations
 * @author Jesse Bunch
 **/
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation { 	
	return YES;
}

/**
 * Upon rotation, switch out the background image
 * @author Jesse Bunch
 **/
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	
	if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
		toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
		[self.signaturePanelBackgroundImageView setImage:self.landscapeBackgroundImage];
	} 
	
}

/**
 * After rotation, we need to adjust the signature view's frame to fill.
 * @author Jesse Bunch
 **/
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	//[self.signatureView setFrame:self.view.bounds];
	//[self.signatureView setNeedsDisplay];
}


#pragma mark - *** Actions ***

/**
 * Upon confirmation, message the delegate with the image of the signature.
 * @author Jesse Bunch
 **/
-(void)didTapCanfirmButton {
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(signatureConfirmed:signatureController:)]) {
		UIImage *signatureImage = [self.signatureView getSignatureImage];
		[self.delegate signatureConfirmed:signatureImage signatureController:self];
	}
	
}

/**
 * Upon cancellation, message the delegate.
 * @author Jesse Bunch
 **/
-(void)didTapCancelButton {
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(signatureCancelled:)]) {
		[self.delegate signatureCancelled:self];
	}
	
}
-(void)didTapClearButton {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(signatureCleared:signatureController:)]) {
		UIImage *signatureImage = [self.signatureView getSignatureImage];
		[self.delegate signatureCleared:signatureImage signatureController:self];
	}
	
    
}


#pragma mark - *** Public Methods ***

/**
 * Clears the signature from the signature view. If the delegate is subscribed,
 * this method also messages the delegate with the image before it's cleared.
 * @author Jesse Bunch
 **/
-(void)clearSignature {
	

	[self.signatureView clearSignature];
}
- (NSString*) dbGetImagesDocumentPath
{
    
//     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
     path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
     if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
           [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
         }
     path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
     return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
   NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}

@end
