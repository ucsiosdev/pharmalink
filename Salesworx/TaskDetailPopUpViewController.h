//
//  TaskDetailPopUpViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepQueries.h"
#import "MedRepTextField.h"
#import "MedRepTextView.h"
#import "SWDefaults.h"

@interface TaskDetailPopUpViewController : UIViewController <UIPopoverControllerDelegate>
{
    IBOutlet UIView *TopContentView;
    IBOutlet MedRepTextField *titleTxtFld;
    IBOutlet MedRepTextField *dueDateTxtFld;
    IBOutlet MedRepTextView *InformationTxtFld;
    IBOutlet MedRepTextField *statusTxtFld;
    IBOutlet MedRepTextField *categoryTxtFld;
    IBOutlet MedRepTextField *priorityTxtFld;
    IBOutlet SalesWorxDefaultButton *btnSave;
    IBOutlet SalesWorxDefaultButton *btnCancel;
    
    NSString *popoverString;
    UIPopoverController * datePickerPopOverController,*statusPopOverController;
    NSMutableArray *statusArray, *categoryArray, *priorityArray;
}

@property(strong,nonatomic) VisitTask *selectedTask;


@end

