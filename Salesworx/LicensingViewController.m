//
//  LicensingViewController.m
//  SWSession
//
//  Created by msaad on 3/17/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "LicensingViewController.h"


#import "SWFoundation.h"
#import "SWPlatform.h"
#import "NewActivationViewController.h"
#import "SWSessionConstants.h"
#import "TypeViewController.h"
#import "FMDBHelper.h"
#import "SharedUtils.h"

#define kcustomerIDField 1
#define klicenceTypeField 2

@interface LicensingViewController ()

@end

@implementation LicensingViewController
@synthesize customerID;

@synthesize target,action;

- (id)init {
    self = [super init];
    
    if (self) {
        
        AppControl *appCtrl=[AppControl retrieveSingleton];
        
        
        NSString* appCtrlQry=@"select * from TBL_App_Control";
        
        NSArray* appCtrlArray=[FMDBHelper executeQuery:appCtrlQry];
        
        

        NSLog(@"app control in licensing %@",appCtrlArray);
        if ([appCtrl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
            
           // [self setTitle:@"SalesWars - License Activation"];
             [self setTitle:@"MedRep - License Activation"];
            
        }
        else
        {
            //no chance of app control database didnt even download (actionation not done yet)
            [self setTitle:@"SalesWorx - License Activation"];
            
        }

       // [self setTitle:@"MedRep - License Activation"];

        
        //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        verifyLicenceController = nil;
        verifyLicenceController = [[SynchroniseViewController alloc] init] ;
        
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    loadingView=nil;
    loadingView=[[SWLoadingView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    loadingView.loadingLabel.text = @"Please Wait..";
    [Flurry logEvent:@"Licensing View"];
    //[Crittercism leaveBreadcrumb:@"<Licensing View>"];

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
        
    return NO;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];

    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
   // NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    NSString * avid = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];

    
    ClientVersion =avid;
    customerID = @"C16K82563";
    licenceType = @"Evaluation - 30 days";
    tableView=nil;
    
    NSLog(@"bounds are %@ %@",NSStringFromCGRect([[UIScreen mainScreen] bounds]),NSStringFromCGRect(self.view.bounds) );
    
    
    
    tableView=[[UITableView alloc] initWithFrame:[[UIScreen mainScreen] bounds] style:UITableViewStyleGrouped];

//    tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor whiteColor];
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        tableView.cellLayoutMarginsFollowReadableWidth = NO;

    }

    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [tableView setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 200)] ];
    tableView.scrollEnabled=NO;
    [self.view addSubview:tableView];
    
    
    UILabel *titleLable = [[UILabel alloc] initWithFrame:CGRectMake(50, 100, 300, 100)] ;
    titleLable.font = BoldSemiFontOfSize(20.0);
    
    AppControl *appCtrl=[AppControl retrieveSingleton];
    
    
    //no chance of app control database didnt even download (actionation not done yet)
//    titleLable.text = @"SalesWars - License Activation";

    [titleLable setText:@"MedRep - License Activation"];


    if ([appCtrl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
        
       // titleLable.text = @"SalesWars - License Activation";
        
        [titleLable setText:@"MedRep - License Activation"];

    }
    else
    {
       // titleLable.text = @"Salesworx - License Activation";
        
    }

    
    titleLable.backgroundColor = [UIColor clearColor];
    titleLable.textColor = UIColorFromRGB(0x4E566C);
    [self.view addSubview:titleLable];
    
    if ([SWDefaults username].length > 0) {
     //   self.customerID = [SWDefaults username];
     //   self.licenceType = [SWDefaults password];
    }
    activateButton=nil;
    activateButton = [[UIButton alloc] init]  ;
    activateButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [activateButton setTitle:@"Activate" forState:UIControlStateNormal];
    [activateButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
    [activateButton addTarget:self action:@selector(selectServerButton) forControlEvents:UIControlEventTouchUpInside];
    activateButton.frame = CGRectMake(770,400,200 ,50);
    [self.view addSubview:activateButton];
    
}



#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *IDENT = @"SessionCellIdent";
    UITableViewCell *cell;
    
   // if (!cell) {
        
        if (indexPath.section == 0) {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;

            ((SWTextFieldCell *)cell).textField.placeholder = @"Customer ID";
            ((SWTextFieldCell *)cell).textField.tag = kcustomerIDField;
            ((SWTextFieldCell *)cell).textField.text = customerID;
            ((SWTextFieldCell *)cell).textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            ((SWTextFieldCell *)cell).textField.delegate = self;
        } else {
            
            
            cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDENT];
            [cell setFrame:CGRectMake(0, 0, 1024, 768)];

            cell.textLabel.text = licenceType;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        
   // }
    
    return cell;
}
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
    if(s==0)
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Customer ID"] ;
        return sectionHeader;
    }
    else
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Licence Type"] ;
        return sectionHeader;
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)s {
    
    if(s==1)
       {
           GroupSectionFooterView *footerView = [[GroupSectionFooterView alloc] initWithWidth:tv.bounds.size.width text:[NSString stringWithFormat:@"Powered by: Unique Computer Systems  \n Version : %@ ",ClientVersion]] ;
    
           return footerView;
       }
    else
    {
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 35.0f;
}

#pragma mark UITableView Deleate
- (void)tableView:(UITableView *)tableVieww didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section==1)
    {
        //[self.view endEditing:YES];

        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

        
        //shifted from after present popover code to before present popover code. OLA!
        [self.view endEditing:YES];
        
        TypeViewController *serverSelectionViewController = [[TypeViewController alloc] init]  ;
        [serverSelectionViewController setTarget:self];
        [serverSelectionViewController setAction:@selector(typeChanged:)];
        
        //[serverSelectionViewController setContentSizeForViewInPopover:CGSizeMake(300, self.view.bounds.size.height / 2)];
        
        UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:serverSelectionViewController]  ;
        popoverController=nil;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
        [popoverController presentPopoverFromRect:cell.bounds inView:cell permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        popoverController.delegate=self;

        [tableView deselectRowAtIndexPath:indexPath animated:NO];
       
    }
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC
{
    popoverController=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
-(void)selectServerButton
{
   // [verifyLicence getServerDate];
   // return;
    
    
    
    BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];
    
    if (checkInternetConnectivity==YES) {
        
   
    
    BOOL isValid=NO;
    NSString *lincesnValue ;
    NSString *timePeriod = @"";
    if (customerID.length > 0 && licenceType.length > 0)
    {
        // customerID
        if([licenceType isEqualToString:@"Evaluation - 30 days"])
        {
            lincesnValue = @"EVAL_TIME";
            timePeriod = @"30";
        }
        else if([licenceType isEqualToString:@"Evaluation - 60 days"])
        {
            lincesnValue = @"EVAL_TIME";
            timePeriod = @"60";
        }
        else if([licenceType isEqualToString:@"Evaluation - 90 days"])
        {
            lincesnValue = @"EVAL_TIME";
            timePeriod = @"90";
        }
        else
        {
            lincesnValue = @"PERMANENT";
            timePeriod = @"0";
        }
       // [sessionService verifyLogin:customerID andPassword:licenceType];
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        isValid=[verifyLicenceController startLicenceingWithCustomerIF:customerID andLicenceType:lincesnValue andLicenceLimit:timePeriod];
        
        if (isValid)
        {
//            [ILAlertView showWithTitle:NSLocalizedString(@"Message", nil)
//                               message:@"License activation completed successfully"
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:^(NSInteger buttonIndex) {
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
//                       [self.target performSelector:self.action withObject:nil];
//#pragma clang diagnostic pop
//                   }];
            
            

            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:@"License activation completed successfully" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            alert.tag=10000;
            
            [alert show];


        }
        else
        {
            //[[[UIAlertView alloc] initWithTitle:@"License validation error" message:@"You license has been expired or contact your administrator for further assistance " delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
            //[[NSNotificationCenter defaultCenter] postNotificationName:kSessionActivationErrorNotification object:nil];
            
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        }
    }
    else
    {
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Please fill all fields"
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please fill all fields" message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
      //  [alert release];
    }
}
    else
    {
        UIAlertView* internetAlert=[[UIAlertView alloc]initWithTitle:@"Internet unavailable" message:@"Please connect to the internet and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        internetAlert.tag=1000;
        [internetAlert show];
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag==1000) {
        
    }
    
    else
    {
    
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                           [self.target performSelector:self.action withObject:nil];
    #pragma clang diagnostic pop
    }
}
- (void)typeChanged:(NSString *)type {
    [popoverController dismissPopoverAnimated:YES];

    licenceType=type;
    [tableView reloadData];
}


#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField.tag == kcustomerIDField) {
        customerID=textField.text;
        
        
    } else {
        licenceType=textField.text;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
        return YES;
}


-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
