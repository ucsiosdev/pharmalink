//
//  MedRepProductImagesViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepProductImagesViewController.h"
#import "SWDefaults.h"
#import <MessageUI/MessageUI.h>
#import "MedRepProductDescriptionCollectionViewCell.h"
#define VIEW_FOR_ZOOM_TAG (1)
#import "MedRepProductsHomeCollectionViewCell.h"
#import "MedRepDefaults.h"
#import "AppControl.h"
#import "MedRepQueries.h"

@interface MedRepProductImagesViewController ()

@end

@implementation MedRepProductImagesViewController
@synthesize productImagesArray,customPageControl,currentImageIndex,imagesScrollView,selectedIndexPath,productImagesCollectionView,productDataArray,isViewing,selectedProductDetails,isCurrentUserPharmacy,isCurrentUserDoctor;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchEmailID];
    
    
    timerDate=[NSDate date];
    
    NSLog(@"product images called");
    
    cancelButton.hidden=YES;
    demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_IMAGE_DEFAULT_DEMO_TIME.integerValue;
    
    productDescTxtView.font=MedRepElementDescriptionLabelFont;
    productDescTxtView.textColor=MedRepElementDescriptionLabelColor;
    productDescTxtView.text=[selectedProductDetails valueForKey:@"item_Metadata"];
    // Do any additional setup after loading the view from its nib.
    
    
    if (productDataArray.count>0) {
        [productDataArray setValue:@"N" forKey:@"isSelected"];
    }
    
}

-(void)fetchEmailID{
 
   emailIDForAutoInsert = @"";
    
   if ([[AppControl retrieveSingleton].POPULATE_CUSTOMER_EMAIL isEqualToString:KAppControlsYESCode]){
    
        if (isCurrentUserDoctor){
            emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaDoctorID:self.doctor_ID];
        }else  if (isCurrentUserPharmacy){
             emailIDForAutoInsert = [[SWDatabaseManager retrieveManager]fetchEmailAddressViaContactID:self.contact_ID];
        }
    
   }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"APP RECEIVED MEMORY WARNING");
    
    
}


- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}


- (void) pinch:(UIPinchGestureRecognizer*)recognizer {
    
    NSLog(@"pinch gesture called");
    
    
}
- (IBAction)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    
    NSLog(@"rotation gesture called");
    
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
    recognizer.rotation = 0;
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


-(void)loadProductDetailsWithSelectedIndex:(NSInteger)selectedIndex
{
    
    NSString* productCodeStr=[NSString stringWithFormat:@"%@",[[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Code"] ];
    
  //  NSLog(@"product code is %@", productCodeStr);
    
    
    if (productCodeStr.length>0) {
        
       
    }
    
    else
    {
        
        
        
    }
    
   
    
    productHeaderTitle.text=[NSString stringWithFormat:@"%@", [[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Name"] ];

    
    NSString* productCode=[NSString stringWithFormat:@"%@",[[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Code"]];
    
    
    NSString* productNameStr=[NSString stringWithFormat:@"%@", [[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Name"]];
    
    
    titleTextView.text=[NSString stringWithFormat:@"%@", [[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Name"]];
    titleTextView.font=MedRepElementTitleLabelFont;
    titleTextView.textColor=MedRepElementTitleLabelFontColor;
    
    
    
    
    
    NSMutableAttributedString* productNameAttrStr =
    [[NSMutableAttributedString alloc] initWithString:productNameStr];
    [productNameAttrStr addAttribute:NSFontAttributeName
                           value:MedRepElementTitleLabelFont
                           range:NSMakeRange(0, productNameAttrStr.length)];
    
    [productNameAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepElementTitleLabelFontColor range:NSMakeRange(0, productNameAttrStr.length)];
    
    
    NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@"\n" attributes:@{NSFontAttributeName : kSWX_FONT_SEMI_BOLD(8)}];
    [productNameAttrStr appendAttributedString:atrStr];
    
    
   NSMutableAttributedString* productAttrStr =
    [[NSMutableAttributedString alloc] initWithString:productCode];
    [productAttrStr addAttribute:NSFontAttributeName
                   value:MedRepElementDescriptionLabelFont
                   range:NSMakeRange(0, productAttrStr.length)];

    [productAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepElementDescriptionLabelColor range:NSMakeRange(0, productAttrStr.length)];
    
    
    
    
    if (productCode.length>0) {
        

        [productNameAttrStr appendAttributedString:productAttrStr];
        
        
        
        
        NSLog(@"title text is %@", productNameAttrStr);
        
        
        titleTextView.attributedText=productNameAttrStr;

        
        
        
        NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@"\n" attributes:@{NSFontAttributeName : kSWX_FONT_SEMI_BOLD(8)}];
        [productNameAttrStr appendAttributedString:atrStr];
        
        //static Description text
        
        
        NSMutableAttributedString* productNameAttrStr =
        [[NSMutableAttributedString alloc] initWithString:@"Description"];
        [productNameAttrStr addAttribute:NSFontAttributeName
                                   value:MedRepElementTitleLabelFont
                                   range:NSMakeRange(0, productNameAttrStr.length)];
        
        [productNameAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepElementTitleLabelFontColor range:NSMakeRange(0, productNameAttrStr.length)];
        
        
        
        //add new line
        
        NSAttributedString *atrnewLineStr = [[NSAttributedString alloc]initWithString:@"\n" attributes:@{NSFontAttributeName : kSWX_FONT_SEMI_BOLD(8)}];
        [productNameAttrStr appendAttributedString:atrnewLineStr];
        
        
        //add description text
        
        
        NSAttributedString *descLineStr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@", [[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Detailed_Info"]]
                                                                         attributes:@{NSFontAttributeName : MedRepElementDescriptionLabelFont}];
        
        
        [productNameAttrStr addAttribute:NSForegroundColorAttributeName value:MedRepPanelTitleLabelFontColor range:NSMakeRange(0, productNameAttrStr.length)];
        
        [productNameAttrStr appendAttributedString:descLineStr];
        

    }
    
    
    
    else
    {
        
        
      
  
        
        
        

        titleTextView.text=[NSString stringWithFormat:@"%@", [[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Product_Name"]];
        
    }
    
  
    //titleTextView.text=@"50 ML ENTERAL DRAINAGE BAG";

    
    
//    [productDescriptionTextView setFont:MedRepElementDescriptionLabelFont];
//    [productDescriptionTextView setTextColor:MedRepElementDescriptionLabelColor];
//    
//    productDescriptionTextView.userInteractionEnabled=NO;
    

    CGPoint p = CGPointZero;
    
    NSString *description = [NSString getValidStringValue:[[productDataArray objectAtIndex:selectedIndex] valueForKey:@"Detailed_Info"]];
    productDescriptionTextView.text=description.length == 0 ? @"N/A" : description;
    
    [productDescriptionTextView setContentOffset:p animated:NO];
    [productDescriptionTextView scrollRangeToVisible:NSMakeRange(0, 0)];
    
    NSLog(@"product description text view is %@", productDescriptionTextView.text);
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    

    
//    productDescriptionTextView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    

    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingProductImages];
    }
   
    
    [productImagesCollectionView registerClass:[MedRepProductsHomeCollectionViewCell class] forCellWithReuseIdentifier:@"productHomeCell"];

    [productImagesCollectionView setAllowsMultipleSelection:YES];
    
    
    NSLog(@"product images array is %@", [productImagesArray description]);
  
    
    [self loadProductDetailsWithSelectedIndex:currentImageIndex];
    
    
    NSLog(@"product data array in product images %@", [productDataArray objectAtIndex:currentImageIndex]);
    
    
    
    
    selectedIndexPathArray=[[NSMutableArray alloc]init];
    selectedCellIndex=0;
    
    productImagesCollectionView.backgroundColor = [UIColor clearColor];
    productImagesCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSLog(@"selected indexpath is %@",selectedIndexPath);

    NSLog(@"product data array count is %d", productDataArray.count);
    
    
//    if (selectedIndexPath) {
//        
////        [productImagesCollectionView reloadData];
////        
//            [productImagesCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:currentImageIndex inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
//            [self collectionView:productImagesCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:currentImageIndex inSection:0]];
//        
//       
//    }
    
    
    if ([productImagesCollectionView.delegate respondsToSelector:@selector(collectionView:didSelectItemAtIndexPath:)])
        
    {
        
        
        if (selectedIndexPath) {
        [productImagesCollectionView selectItemAtIndexPath:selectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
    
    
    [productImagesCollectionView.delegate collectionView:productImagesCollectionView didSelectItemAtIndexPath:selectedIndexPath];
    
        }
    
    
    }
    if ([[AppControl retrieveSingleton].DISABLE_EMAIL_PRODUCT_MEDIA isEqualToString:KAppControlsYESCode]){

    isViewing = YES;
    }else{
        isViewing = NO;
    }
    
    if (isViewing==YES) {
        
        [emailButton setHidden:YES];
    }
    else
    {
        [emailButton setHidden:NO];
    }
    
    
    if (!selectedCellIndexPath) {
        
        
    }
    
    else
    {
        
    
        
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
}
- (IBAction)emailButtonTapped:(id)sender {

    
    isEmailTapped=YES;
    
    cancelButton.hidden=NO;
    buttonTitle = [sender titleForState:UIControlStateNormal];

    
    
    
    if (buttonTitle.length>0 && sendEmail==YES ) {
      
        [self sendEmail];
    }
    else
    {
        UIAlertView * emailAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Email", nil) message:NSLocalizedString(@"Please select the media you would like to send as an Email, you can select multiple media at a time", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [emailAlert show];
        
    }
    
    
}



-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:YES];
    
    
    viewedImages=[[NSMutableArray alloc]init];
    
  //  NSLog(@"product data array is %@",[productDataArray description]);
    
    NSInteger numberOfPages = productDataArray.count;
    
    customPageControl.numberOfPages=productDataArray.count;
    
    if (![customPageControl respondsToSelector:@selector(semanticContentAttribute)] && [UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
        //customPageControl.transform = CGAffineTransformMakeScale(-1, 1);
    }
    
    
    imagesScrollView.pagingEnabled = YES;
    imagesScrollView.backgroundColor=[UIColor whiteColor];
    
    
   

    imagesScrollView.delegate=self;

    imagesScrollView.contentSize = CGSizeMake(numberOfPages * imagesScrollView.frame.size.width, imagesScrollView.frame.size.height);
    
  //  NSLog(@"content size of scrollview is %@", NSStringFromCGSize(imagesScrollView.contentSize));
    
   // NSLog(@"images scrollview frame is %@", NSStringFromCGRect(imagesScrollView.frame));
    
    
    
//    for (UIGestureRecognizer* recognizer in [imagesScrollView gestureRecognizers]) {
//        if ([recognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
//            [recognizer setEnabled:NO];
//        }
//    }
//    
//    // Add our own
//    UIPinchGestureRecognizer* pinchRecognizer =
//    [[UIPinchGestureRecognizer alloc] initWithTarget:self
//                                              action:@selector(pinch:)];
//    [imagesScrollView addGestureRecognizer:pinchRecognizer];
    NSInteger imageX = 0;
    
    zoomableView=[[UIView alloc]initWithFrame:imagesScrollView.frame];
    
   // NSLog(@"number of pages are %d", numberOfPages);
    
    NSString* imagePathStr;
    NSString *documentsDirectory = nil;

    
    
    if (productDataArray.count>0) {
        // Get dir
        //iOS 8 support
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        
        
        
       
   // NSLog(@"imagesScrollView subviews %@", [imagesScrollView.subviews description]);
    
    
    imagesScrollView.delegate=self;
    
   
        UIRotationGestureRecognizer  *rotation = [[UIRotationGestureRecognizer alloc]initWithTarget:self action:@selector(handleRotate:)];
        rotation.delegate =self;
        
        imagesScrollView.backgroundColor=[UIColor clearColor];
        
       // [imagesScrollView addGestureRecognizer:rotation];
        
    imagesScrollView.pagingEnabled = YES;

    imagesScrollView.showsHorizontalScrollIndicator = NO;
    imagesScrollView.showsVerticalScrollIndicator = NO;
    
    CGRect innerScrollFrame = imagesScrollView.bounds;
    
    for (NSInteger i = 0; i < numberOfPages; i++) {
        
       // NSString* imagePath=[productImagesArray objectAtIndex:i];
       // NSLog(@"image path is %@", imagePath);
        
        
        
        imagePathStr=[documentsDirectory stringByAppendingPathComponent:[[productDataArray  objectAtIndex:i]valueForKey:@"File_Name"] ];

       // NSLog(@"image file path %@", imagePathStr);
        
        UIImage* currentImage=[UIImage imageWithContentsOfFile:imagePathStr];
        
        
        imageForZooming = [[UIImageView alloc] initWithImage:currentImage];
        
        //NSLog(@"subviews of images scroll view is %@", [imagesScrollView.subviews description]);
        
       
        
        
         [imageForZooming setFrame:CGRectMake(8, 0, imagesScrollView.frame.size.width, imagesScrollView.frame.size.height)];
        
        imageForZooming.tag = VIEW_FOR_ZOOM_TAG;
        imageForZooming.contentMode=UIViewContentModeScaleAspectFit;
        imageForZooming.backgroundColor=[UIColor clearColor];
        
        

        pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
        pageScrollView.minimumZoomScale = 1.0f;
        
        pageScrollView.backgroundColor=[UIColor clearColor];
        
        pageScrollView.maximumZoomScale = 6.0f;
        pageScrollView.zoomScale = 1.0f;
        pageScrollView.tag=i;
        pageScrollView.contentSize = imageForZooming.bounds.size;
        pageScrollView.delegate = self;
        pageScrollView.showsHorizontalScrollIndicator = NO;
        pageScrollView.showsVerticalScrollIndicator = NO;
        [pageScrollView addSubview:imageForZooming];
        
        
               
        [imagesScrollView addSubview:pageScrollView];
        
        if (i < numberOfPages-1) {
            innerScrollFrame.origin.x += innerScrollFrame.size.width;
        }
        
        
       // NSLog(@" inner scrollview frame %@", NSStringFromCGRect(innerScrollFrame));
        
    }
    
        
        
}

    
     imagesScrollView.contentOffset = CGPointMake(imagesScrollView.frame.size.width * selectedIndexPath.row, imagesScrollView.frame.origin.y);
    if (!selectedIndexPath) {
        customPageControl.currentPage=0;
        
    }
    else{
        customPageControl.currentPage=selectedIndexPath.row;
        
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return [scrollView.subviews objectAtIndex:0];
}
-(void)handleSwipeFrom

{
    
    NSLog(@"SWIPE GESTURE CALLED");
    //
   // [self resetZoomScale];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewDidLayoutSubviews{
    self.automaticallyAdjustsScrollViewInsets = NO;
}

#pragma mark UIScrollView methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //NSLog(@"did end dragging called : will decelerate %hhd",decelerate);
    

}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.isFromProductsScreen) {
        
    }else{
        if (demoTimer) {
            
            demoTimer=nil;
        }
        NSLog(@"Demo timer invalidated for images");
        
        //start the timer to check if images for displayed for more than 5 secs, get the data from app control
        demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:NO];
        
        //calculate the time spent on this file
        NSDate *currentTime = [NSDate date];
        NSTimeInterval viewedTime1 = [currentTime timeIntervalSinceDate:viewingStartTime];
        NSLog(@"viewed Time for image = %f", viewedTime1);
        long ti = lroundf(viewedTime1);
        int viewedTime = ti % 60;
        
        if (selectedMediaFile.viewedTime == nil) {
            selectedMediaFile.viewedTime= [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%ld",(long)viewedTime]];
        } else {
            selectedMediaFile.viewedTime = [NSString stringWithFormat:@"%0.2f",(viewedTime + selectedMediaFile.viewedTime.doubleValue)];
        }
        
        if (selectedMediaFile.discussedAt == nil) {
            selectedMediaFile.discussedAt = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        }
        [productDataArray replaceObjectAtIndex:currentImageIndex withObject:selectedMediaFile];
        NSLog(@"will begin dragging called");
        
        // [self performSelector:@selector(testTimer) withObject:self afterDelay:5.0];
        
        
        
        //    id currentScrollView=[imagesScrollView.subviews objectAtIndex:selectedCellIndex];
        //
        //    UIScrollView * temp = currentScrollView;
        //
        //    temp.zoomScale=1.0;
    }
}




-(void)resetZoomScale
{
    for (UIScrollView *i in imagesScrollView.subviews){
        if([i isKindOfClass:[UIScrollView class]]){
            UIScrollView *zoomableScrollView = (UIScrollView *)i;
            zoomableScrollView.minimumZoomScale = 1.0f;
            zoomableScrollView.backgroundColor=[UIColor clearColor];
            zoomableScrollView.maximumZoomScale = 6.0f;
            zoomableScrollView.zoomScale = 1.0f;
            zoomableScrollView.contentSize = imageForZooming.bounds.size;
            zoomableScrollView.delegate = self;
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    
    
   // NSLog(@"did scroll called");
    
    
    //running on mainrunloop beacuse timer will be paused when scrollview is being scrolled
    //[[NSRunLoop mainRunLoop] addTimer:demoTimer forMode:NSRunLoopCommonModes];
    
   // NSLog(@"scrollview did scroll called");
    

    
}




- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = imagesScrollView.frame.size.width;
    float fractionalPage = imagesScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    customPageControl.currentPage = page;
    [imagesScrollView setContentOffset: CGPointMake(imagesScrollView.contentOffset.x,0)];
    
    currentImageIndex= page;
    
    selectedCellIndex=page;
    
    [self loadProductDetailsWithSelectedIndex:currentImageIndex];
    
    if (scrollView==imagesScrollView) {
        [productImagesCollectionView reloadData];
    }
    else
    {
        
    }
    
    [productImagesCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:currentImageIndex inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [productImagesCollectionView.delegate collectionView:productImagesCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:currentImageIndex inSection:0]];
    
    NSLog(@"did end decelerating called");
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    NSLog(@"did end called");
    
    
   }


-(void)testTimer
{
   // NSLog(@"5 secs done");
    
    
    
}
-(void)startFaceTimeTimer
{
       NSLog(@"image viewed for %ld seconds", demoTimeInterval);
    
    //NSLog(@"viewed image index is %d", currentImageIndex);
    
    
    CGFloat pageWidth = 1024;
    //float fractionalPage = imagesScrollView.contentOffset.x / pageWidth;
   // NSInteger page = lround(fractionalPage);

    
    [viewedImages addObject:[productDataArray objectAtIndex:currentImageIndex]];
    
    
    [demoTimer invalidate];
    
    demoTimer=nil;
    
    
    
    
    /*NSDate * currentTimer=[[NSUserDefaults standardUserDefaults]objectForKey:@"faceTimeTimer"];
     NSDate *now = [NSDate date];
     NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
     
     
     NSInteger ti = (NSInteger)elapsedTimeDisplayingPoints;
     NSInteger seconds = ti % 60;
     
     if (seconds>=5) {
     
     NSLog(@"DEMO DONE");
     
     [demoTimer invalidate];
     
     demoTimer=nil;
     
     }
     
     // NSLog(@"demo time counter ticking %@",[self stringFromTimeInterval:elapsedTimeDisplayingPoints]);
     */
    
    
    
}

- (IBAction)closeButtontapped:(id)sender {
    
    //calculate the time spent on last file, as it wont be scrolled.
    if(self.isFromProductsScreen)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        if (viewingStartTime == nil) {
            viewingStartTime = [NSDate date];
        }        
        NSDate *currentTime = [NSDate date];
//        NSTimeInterval viewedTime = [currentTime timeIntervalSinceDate:viewingStartTime];
//        NSLog(@"viewed Time for image in close button tap = %f", viewedTime);
        NSTimeInterval viewedTime1 = [currentTime timeIntervalSinceDate:viewingStartTime];
        NSLog(@"viewed Time for image in close button tap = %f", viewedTime1);
        long ti = lroundf(viewedTime1);
        int viewedTime = ti % 60;
        
        ProductMediaFile* lastScrollMediaFile=[productDataArray objectAtIndex:currentImageIndex];
        if (lastScrollMediaFile.viewedTime == nil) {
            lastScrollMediaFile.viewedTime=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%ld",(long)viewedTime]];
        } else {
            lastScrollMediaFile.viewedTime = [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%0.2f",(viewedTime + selectedMediaFile.viewedTime.doubleValue)]];
        }
        if (lastScrollMediaFile.discussedAt == nil) {
            lastScrollMediaFile.discussedAt = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        }
        [productDataArray replaceObjectAtIndex:currentImageIndex withObject:lastScrollMediaFile];
        
        //now fetch the time viewed
        NSPredicate* timePredicate=[NSPredicate predicateWithFormat:@"self.viewedTime.integerValue>0"];
        NSArray* filteredArray=[productDataArray filteredArrayUsingPredicate:timePredicate];
        NSLog(@"filtered array with viewed time is %@", filteredArray);
        
        
        
        
        for (NSInteger i=0; i<viewedImages.count; i++) {
            ProductMediaFile* currentFile=[viewedImages objectAtIndex:i];
            //viewed time
            NSPredicate* timePredicate=[NSPredicate predicateWithFormat:@"self.Media_File_ID == %@ ",currentFile.Media_File_ID];
            NSArray* contentArray=[filteredArray filteredArrayUsingPredicate:timePredicate];
            NSLog(@"filtered array with viewed time is %@", filteredArray);
            if (contentArray.count>0) {
                ProductMediaFile* matchedFile=[contentArray objectAtIndex:0];
                currentFile.viewedTime=matchedFile.viewedTime;
                if (currentFile.discussedAt == nil) {
                    currentFile.discussedAt = [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
                }
            }
            [viewedImages replaceObjectAtIndex:i withObject:currentFile];
        }
        
        
        if ([self.imagesViewedDelegate respondsToSelector:@selector(imagesViewedinFullScreenDemo:)]  ) {
            
            [self.imagesViewedDelegate imagesViewedinFullScreenDemo:viewedImages];
        }
        
        if ([self.imagesViewedDelegate respondsToSelector:@selector(fullScreenImagesDidCancel)]  ) {
            
            [self.imagesViewedDelegate fullScreenImagesDidCancel];
        }
        
        
        if ([self.imagesViewedDelegate respondsToSelector:@selector(setBorderOnLastViewedImage:)]) {
            [self.imagesViewedDelegate setBorderOnLastViewedImage:currentImageIndex];
        }
        
        // NSLog(@"viewed images are %@", [viewedImages description]);
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark UICOllectionview methods


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(147, 150);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (productDataArray.count>0) {
        return  productDataArray.count;
    }
    else{
        return 0;
    }
}




- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    //    static NSString *cellIdentifier = @"productCell";
    
    if (indexPath.row==currentImageIndex) {
        
    }
    
    else
    {
        [self resetZoomScale];
    }
    
    static NSString *cellIdentifier = @"productHomeCell";
    
    NSString* mediaType = [[productDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row];
    
    
    MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    
       

    
    
   
    
    
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[productDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
    
  //  NSLog(@"file name is %@", fileName);
    
   // NSLog(@"media type %@", mediaType);
    
    

    
    if ([mediaType isEqualToString:@"Image"]) {
        
       // cell.productImageView.backgroundColor=[UIColor clearColor];
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul), ^(void) {
//                NSData *data = [NSData dataWithContentsOfFile:fileName options:NSDataReadingUncached error:nil];// [NSData dataWithContentsOfFile:fileName];
//                UIImage *image = [UIImage imageWithData:data];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                     cell.productImageView.image = image;
//                });
//        });
//        

        //this resolved memory issue.
//        NSData* imageData= [NSData dataWithContentsOfFile:fileName];
//        UIImage *originalImage = [UIImage imageWithData:imageData];
//        CGSize destinationSize = CGSizeMake(145, 108);
//        UIGraphicsBeginImageContext(destinationSize);
//        [originalImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
//        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();

        
        //fetching compressed image from thumbnails folder
        NSString* thumbnailImageName = [[productDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
        
        cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
        
//        cell.productImageView.image=newImage;
        
        cell.captionLbl.text=[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    
    if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"N"]) {

        
        cell.selectedImgView.image=nil;


    }
    
    
    else
    {
        cell.selectedImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
    }
    
    cell.backgroundColor=[UIColor clearColor];
    
   

    if (indexPath.row== selectedCellIndex) {
        
        cell.layer.borderWidth=2.0;
        cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        
//        [self resetZoomScale];
        
    }
    
    else
    {
        cell.layer.borderWidth=0;

    }
    
    
    //cell.productImageView.backgroundColor=ProductImageViewBackgroundColor;

    
    return cell;
    
    
}

-(void)loadSelectedCellDetails:(NSInteger)selectedIndex

{
    NSLog(@"load selected cell details called");
   // imagesScrollView.zoomScale=6.0;

  //  NSLog(@"subviews for images scroll view %@",[[[imagesScrollView.subviews objectAtIndex:0] subviews]description]);
    
    
    
     imagesScrollView.contentOffset = CGPointMake(imagesScrollView.frame.origin.x+imagesScrollView.frame.size.width*selectedCellIndex, 0);
    
   // [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        
        
        
      //  NSLog(@"image scroll view frame %@", NSStringFromCGRect(imagesScrollView.frame));
        
        
       
        
//        imagesScrollView.contentSize=CGSizeMake(649, 487);
        
        
        
//        imagesScrollView.contentSize=CGSizeMake(imagesScrollView.frame.size.width, imagesScrollView.frame.size.height);
        
//        NSLog(@"contest offset: %@ content Size : %@", NSStringFromCGPoint(imagesScrollView.contentOffset),NSStringFromCGSize(imagesScrollView.contentSize) );
//        
        
        
       //
        
      //  NSLog(@"scrollview content offset is %@", NSStringFromCGPoint(imagesScrollView.contentOffset));
        
  //  } completion:NULL];
    
    

}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"prev scrollview zoom scale %@", [imagesScrollView.subviews objectAtIndex:indexPath.row]);
    
  //  UIScrollView * previousZoomedScrollView=[imagesScrollView.subviews objectAtIndex:indexPath.row ];
    
    
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
//    NSLog(@"current offset %@", NSStringFromCGPoint(imagesScrollView.contentOffset));
    
    
//    pageScrollView.zoomScale=1.0;
//    
//    imagesScrollView.minimumZoomScale=1.0;
//    imagesScrollView.maximumZoomScale=6.0;
    
   // [imagesScrollView setZoomScale:1.0];
    
   
    
   // NSLog(@"zooming image %@", [imagesScrollView.subviews objectAtIndex:indexPath.row ]);
    [demoTimer invalidate];
    demoTimer=nil;
    selectedCellIndexPath=indexPath;
    
    if (isEmailTapped==YES) {
        
   
   // MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    


        
        
        if ([[[productDataArray objectAtIndex:indexPath.row]valueForKey:@"isSelected"] isEqualToString:@"Y"]) {
            
            [[productDataArray objectAtIndex:indexPath.row]setValue:@"N" forKey:@"isSelected"];

        }
        
        else
        {
            
            [[productDataArray objectAtIndex:indexPath.row]setValue:@"Y" forKey:@"isSelected"];

            //[[productDataArray objectAtIndex:indexPath.row]removeObjectForKey:@"isSelected"];
            

        }
    
        
        
    
    if ([selectedIndexPathArray containsObject:indexPath]) {
        
        //already seelected cell
        
        [selectedIndexPathArray removeObject:indexPath];
        
        /*
        for (NSInteger i=0; i<cell.contentView.subviews.count; i++) {
            
            
            NSLog(@"tag for views %d", [[cell.contentView.subviews objectAtIndex:i]tag]);
            
            NSInteger selectedCelltag=[[cell.contentView.subviews objectAtIndex:i]tag];
            
            if (selectedCelltag ==indexPath.row+1) {
                
                NSLog(@"check subviews  %@", [cell.contentView.subviews description]);
                
                NSLog(@"check subviews of this cell %@", [cell.contentView.subviews objectAtIndex:i]);
                
                [[cell.contentView.subviews objectAtIndex:i] removeFromSuperview];
                
            }
            
        }
        
        NSLog(@"check subviews of already existing cell %@", [cell.contentView.subviews description]);
        [selectedIndexPathArray removeObject:indexPath];

        */
        
    }
    
    else
    {
    
    [selectedIndexPathArray addObject:indexPath];
        
//        UIImageView * selectedTickImgView=[[UIImageView alloc]initWithFrame:CGRectMake(146, 2, 30, 30)];
//        selectedTickImgView.image=[UIImage imageNamed:@"Tick_CollectionView"];
//        selectedTickImgView.tag=indexPath.row+1;
//        [cell.contentView addSubview:selectedTickImgView];
        
        sendEmail=YES;
        [emailButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];

        
    }
    
    
        
        [productImagesCollectionView reloadData];
    
    
    }
    
    else
    {
       

    
        [collectionView setAllowsMultipleSelection:NO];

        selectedCellIndex=indexPath.row;
        
        
        if (demoTimer) {
            
            demoTimer=nil;
        }
        
        //start the timer to check if images for displayed for more than 5 secs
        demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:NO];
        
        selectedMediaFile=[productDataArray objectAtIndex:indexPath.row];
        currentImageIndex = indexPath.row;
        customPageControl.currentPage = currentImageIndex;
        [imagesScrollView setContentOffset: CGPointMake(imagesScrollView.contentOffset.x,0)];
        
        viewingStartTime=[NSDate date];
        
        
        

        
        
     [self loadProductDetailsWithSelectedIndex:indexPath.row];
      [self loadSelectedCellDetails:selectedCellIndex];
        [productImagesCollectionView reloadData];

    
    }
    
    if (selectedIndexPathArray.count==0) {
        
        [emailButton setTitle:@"Email" forState:UIControlStateNormal];
        isEmailTapped=NO;
        cancelButton.hidden=YES;
        sendEmail=NO;
    }
}

-(void)sendEmail

{
    if ([MFMailComposeViewController canSendMail])
    {
        
        
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        
        
        mailController.mailComposeDelegate = self;
        
        [mailController setToRecipients:@[emailIDForAutoInsert]];

        
        [mailController setSubject:@"SalesWorx"];
        
        [mailController setMessageBody:[NSString stringWithFormat:@"Product Name: %@", [[productDataArray objectAtIndex:selectedIndexPath.row] valueForKey:@"Product_Name"]] isHTML:NO];
        
        
        
        NSString *fileNameString;
        
        
        for (NSInteger i=0; i<selectedIndexPathArray.count; i++) {
            
            NSIndexPath * currentIndexPath=[selectedIndexPathArray objectAtIndex:i];
            
            NSString* imageName= [NSString stringWithFormat:@"%@",[[productDataArray valueForKey:@"File_Name"] objectAtIndex:currentIndexPath.row]];
            
            fileNameString=[[MedRepDefaults fetchDocumentsDirectory] stringByAppendingPathComponent:[[productDataArray valueForKey:@"File_Name"] objectAtIndex:currentIndexPath.row]];
            
            UIImage * currentImage=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileNameString]];
            
            NSData *currentImageData = UIImageJPEGRepresentation(currentImage ,1.0);
            
            [mailController addAttachmentData:currentImageData mimeType:@"image/jpeg" fileName:imageName];
            
        }
        
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        
    }
    
}

#pragma mark Mail Composer Delegate


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    
    
    switch (result) {
           
            
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
       
                       break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];
    
    for (NSInteger i=0; i<selectedIndexPathArray.count; i++) {
        NSIndexPath * selectedIndexPathforEmail=[selectedIndexPathArray objectAtIndex:i];
        MedRepProductsHomeCollectionViewCell *cell = (MedRepProductsHomeCollectionViewCell*)[productImagesCollectionView cellForItemAtIndexPath:selectedIndexPathforEmail];
        //already seelected cell
        //[[cell.contentView.subviews lastObject] removeFromSuperview];
        NSLog(@"check subviews in mail delegate %@", [cell.contentView.subviews description]);
    }
    
    isEmailTapped=NO;
    cancelButton.hidden=YES;
    
    sendEmail=NO;

    
  //clearing the selection
    [productDataArray setValue:@"N" forKey:@"isSelected"];

    
    
    
//    for (NSMutableDictionary * currentDict in productDataArray) {
//        [currentDict removeObjectForKey:@"isSelected"];
//    }
    
    [selectedIndexPathArray removeAllObjects];
    
    [productImagesCollectionView reloadData];
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];

    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    isEmailTapped=NO;
    cancelButton.hidden=YES;
    sendEmail=NO;
    for (NSMutableDictionary * currentDict in productDataArray) {
        NSLog(@"dict being removed is %@", currentDict);
        

    }
    //[productDataArray setValue:nil forKey:@"isSelected"];

    [productDataArray setValue:@"N" forKey:@"isSelected"];

    [selectedIndexPathArray removeAllObjects];
    [productImagesCollectionView reloadData];
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];
    
}


@end
