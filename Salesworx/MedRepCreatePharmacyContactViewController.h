//
//  MedRepCreatePharmacyContactViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/10/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepCustomClass.h"
@protocol CreateContactDelegate <NSObject>

-(void)createdContactforPharmacy:(NSMutableDictionary*)pharmacyContactDetails;

@end

@interface MedRepCreatePharmacyContactViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>


{
    id delegate;
    
    CGRect oldFrame;
}
@property (strong, nonatomic) IBOutlet MedRepTextField *locationNameTxtFld;
@property (strong, nonatomic) IBOutlet MedRepTextField *contactNameTxtFld;
@property (strong, nonatomic) IBOutlet MedRepTextField *contactEmailTxtFld;
@property (strong, nonatomic) IBOutlet UITextField *contactTelephoneTxtFld;
@property(strong,nonatomic) NSMutableDictionary * contactDetailsDict;
@property (strong, nonatomic) IBOutlet MedRepTextField *contactTitleTxtFld;
@property(strong,nonatomic) NSMutableDictionary* locationDetailsDictionary;
@property(strong,nonatomic) NSString* contactID;
@property(strong,nonatomic) id delegate;
@property(nonatomic) BOOL isFromMultiCalls;
@property(strong,nonatomic) UIPopoverController * createContactPopOverController;

@end
