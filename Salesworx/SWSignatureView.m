//
//  SWSignatureView.m
//  SWFoundation
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWSignatureView.h"
#import <QuartzCore/QuartzCore.h>
#import "SWFoundation.h"

#define DEFAULT_COLOR [UIColor blackColor]
#define DEFAULT_WIDTH 3.0f

@implementation SWSignatureView

@synthesize delegate= _delegate;
@synthesize toolbar;

CGPoint midPoint(CGPoint p1, CGPoint p2) {
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIImageView *signPlaceHolderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_autograph_x"]] ;
        [signPlaceHolderImageView setFrame:CGRectMake(200, 160, 500, 103)];
        [signPlaceHolderImageView setContentMode:UIViewContentModeScaleAspectFit];
        
        [self addSubview:signPlaceHolderImageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 160, 200, 25)] ;
        [label setText:@"Signature"];
        [label setTextAlignment:NSTextAlignmentLeft];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor darkTextColor]];
        [label setFont:LightFontOfSize(14.0f)];
        [label setShadowColor:[UIColor lightTextColor]];
        [label setShadowOffset:CGSizeMake(0, -1)];
        
        [self addSubview:label];
        
        // toolbar
        [self setToolbar:[[UIToolbar alloc] init] ];
        [self.toolbar setTranslucent:YES];
        [self.toolbar setBarStyle:UIBarStyleBlackTranslucent];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)] ;
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)] ;
        
        [self.toolbar setItems:[NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil]];
        
        [self addSubview:self.toolbar];
    }
    return self;
}



- (void)layoutSubviews {
    [super layoutSubviews];
    [self.toolbar setFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
}

#pragma mark TouchDelegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    
    previousPoint1 = [touch previousLocationInView:self];
    previousPoint2 = [touch previousLocationInView:self];
    currentPoint = [touch locationInView:self];
    
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch  = [touches anyObject];
    
    previousPoint2  = previousPoint1;
    previousPoint1  = [touch previousLocationInView:self];
    currentPoint    = [touch locationInView:self];
    
    // calculate mid point
    CGPoint mid1    = midPoint(previousPoint1, previousPoint2); 
    CGPoint mid2    = midPoint(currentPoint, previousPoint1);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, mid1.x, mid1.y);
    CGPathAddQuadCurveToPoint(path, NULL, previousPoint1.x, previousPoint1.y, mid2.x, mid2.y);
    CGRect bounds = CGPathGetBoundingBox(path);
    CGPathRelease(path);
    
    CGRect drawBox = bounds;
    
    //Pad our values so the bounding box respects our line width
    drawBox.origin.x        -= DEFAULT_WIDTH * 2;
    drawBox.origin.y        -= DEFAULT_WIDTH * 2;
    drawBox.size.width      += DEFAULT_WIDTH * 4;
    drawBox.size.height     += DEFAULT_WIDTH * 4;
    
    UIGraphicsBeginImageContext(drawBox.size);
	[self.layer renderInContext:UIGraphicsGetCurrentContext()];
	curImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
    [self setNeedsDisplayInRect:drawBox];
}

#pragma View Delegate
- (void)drawRect:(CGRect)rect {
    [curImage drawAtPoint:CGPointMake(0, 0)];
    CGPoint mid1 = midPoint(previousPoint1, previousPoint2); 
    CGPoint mid2 = midPoint(currentPoint, previousPoint1);
    
    CGContextRef context = UIGraphicsGetCurrentContext(); 
        
    [self.layer renderInContext:context];
    
    CGContextMoveToPoint(context, mid1.x, mid1.y);
    CGContextAddQuadCurveToPoint(context, previousPoint1.x, previousPoint1.y, mid2.x, mid2.y); 
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, DEFAULT_WIDTH);
    CGContextSetStrokeColorWithColor(context, DEFAULT_COLOR.CGColor);
    
    CGContextStrokePath(context);
    
    [super drawRect:rect];
    
}

#pragma mark - Toolbar Selectors

- (void)done {
    if ([self.delegate respondsToSelector:@selector(signatureViewDidFinishWithImage:)]) {
        [self.delegate signatureViewDidFinishWithImage:nil];
    }
}

- (void)cancel {
    [self setClearsContextBeforeDrawing:YES];
    //[self setNeedsDisplay];
    
    if ([self.delegate respondsToSelector:@selector(signatureViewDidFinishWithCancle:)]) {
        [self.delegate signatureViewDidFinishWithCancle];
    }
}

@end
