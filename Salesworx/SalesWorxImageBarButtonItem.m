//
//  SalesWorxImageBarButtonItem.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 1/19/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxImageBarButtonItem.h"

@implementation SalesWorxImageBarButtonItem
- (instancetype)initWithImage:(nullable UIImage *)image style:(UIBarButtonItemStyle)style target:(nullable id)target action:(nullable SEL)action
{
    if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) 
        {
            self=[super initWithImage:[image imageFlippedForRightToLeftLayoutDirection] style:style target:target action:action];
        }
        else
        {
            self=[super initWithImage:image style:style target:target action:action];

        }
   return self;
}

@end
