//
//  SalesWorxPaymentCollectionInvoicesTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxPaymentCollectionInvoicesTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *documentNumberLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *invoiceDateLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *balanceAmountLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *payedAmountLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dividerImageView;
@property (strong, nonatomic) IBOutlet UITextField *settledAmountTxtFld;

@end
