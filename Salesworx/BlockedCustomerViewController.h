//
//  BlockedCustomerViewController.h
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "CustomersListViewController.h"
@interface BlockedCustomerViewController : SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
{
    GridView *gridView;
    IBOutlet UITableView *filterTableView;
    
    UIPopoverController *currencyTypePopOver;
    CustomersListViewController *currencyTypeViewController;
    NSMutableArray *dataArray;
}
@end
