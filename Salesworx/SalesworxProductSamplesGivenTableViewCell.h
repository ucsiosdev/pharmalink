//
//  SalesworxProductSamplesGivenTableViewCell.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesworxProductSamplesGivenTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblProductName;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblSamplesGiven;

@property(nonatomic) BOOL isHeader;

@end
