//
//  SalesWorxStatementHeaderTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxStatementHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocumentNo;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblInvoiceDate;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblBalanceAmt;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblPaidAmt;

@end
