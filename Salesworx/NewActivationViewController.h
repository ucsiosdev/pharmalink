//
//  NewActivationViewController.h
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWSession.h"
#import <MedRepButton.h>
#import "MedRepTextField.h"
#import "SWPlatform.h"
#import "TYMProgressBarView.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepView.h"
#import "MedRepCustomClass.h"
#import "SharedUtils.h"

@interface NewActivationViewController : FTPBaseViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIPopoverControllerDelegate,SynViewControllerDelegate,MBProgressHUDDelegate,UIAlertViewDelegate>
{
    NSString* customerID;
    
    IBOutlet UIButton *serverNameButton;
    IBOutlet UIView *medRepLeftView;
    IBOutlet UIProgressView *activationProgressView;
    IBOutlet TYMProgressBarView *tymProgressBarView;
    IBOutlet MedRepElementDescriptionLabel *downloadStatusLbl;
    UITableView *tableView;
    UIPopoverController *popoverController;
    UIWindow *window;
    UIViewController *applicationViewController;
    NSString *login;
    NSString *password;
    NSMutableDictionary *serverDict;
    NSString *editServerLink;
    NSString *editServerName1;
    NSMutableDictionary *newServerDict;
    SWLoadingView *loadingView;
    SynViewController *viewSync;
    DataSyncManager *appData;
    UIView *_footerView;
    UIImageView *bacgroundBar;
    MBProgressHUD *progressHUD;
    NSMutableDictionary *editServerDict;
    //       id target;
    SEL action;
    IBOutlet MedRepButton *activateButton;
    BOOL isActivation;
    BOOL isRootView;
    UIView *myBackgroundView;
    
    IBOutlet UILabel *lblVersion;
    
    UIAlertView* invalidAlert;
    IBOutlet MedRepButton *synLocationButton;

    NSMutableArray * serverDetailsArray;
    
    IBOutlet UIImageView *synLocationDropDownImage;

    
    IBOutlet MedRepTextField *usernameTextField;
    
    IBOutlet MedRepTextField *passwordTextField;
    
    IBOutlet MedRepTextField *servernameTextField;
    
    IBOutlet MedRepTextField *serverLocationTextField;
    
    IBOutlet NSLayoutConstraint *loginDetailsViewTopConstraint;
    
    NSString * selectedServerName,* selectedServerLink;
    
    SyncLoctions * syncLocations;
    
    NSArray *copiedArray;
    
    BOOL comingfromNewActivation;
    
    NSMutableArray* syncLocationsWebserviceArray;

}
@property (weak, nonatomic) IBOutlet MedRepView *allFieldsView;

@property (nonatomic, unsafe_unretained) id target;
- (IBAction)syncLocationButtonTapped:(id)sender;

@property(strong,nonatomic)    UITextView *logTextView;

- (IBAction)serverNameButtonTapped:(id)sender;

@property(nonatomic)    BOOL alertDisplayed;

@property(nonatomic) BOOL invalidCredentials;

- (void)showActivate;

@property (nonatomic, assign) SEL action;
//demo logout test
@property(nonatomic,assign)BOOL *isDemo;
- (id)initWithNoDB;

@end

