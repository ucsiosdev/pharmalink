//
//  MedRepEDetailingStartCallViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 8/5/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepEDetailingStartCallViewController.h"
#import "MedRepProductsCollectionViewCell.h"
#import "SWDefaults.h"
#import "MedRepProductImagesViewController.h"
#import "MedRepProductMediaDisplayViewController.h"
#import <MediaPlayer/MPMoviePlayerController.h>
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "MedRepCollectionViewCheckMarkViewController.h"
#import "MedRepEDetailingEndVisitViewController.h"
#import "MedRepEDetailNotesViewController.h"
#import "GraphHeaderCustomerDashTableViewCell.h"
#import "SamplesGivenHeaderTableViewCell.h"
#import "SalesWorxImageBarButtonItem.h"
#import "MedRepVisitsEndCallViewController.h"
#import "SalesworxDocumentDisplayManager.h"
#import "AppControl.h"
#import "MedRep-Swift.h"

@interface MedRepEDetailingStartCallViewController ()
{
    SalesworxDocumentDisplayManager * docDispalayManager;
    AppControl *appControl;
}
@end

@implementation MedRepEDetailingStartCallViewController

@synthesize faceTimeLbl,visitDetailsDict,locationLbl,doctorLbl;
@synthesize demoPlanProductsCollectionView,imagesButton,pptButton,videoButton,samplesTblView,qtyTxtFld,avblQtyLbl,expiryDateLbl,borderView,tblBorderView,addButton,btnPresentation,btnProduct, isFromDashboard,customPresentationsButton,visitDetails;


-(void)demoedMediaNotificationHandler:(NSNotification*)notification
{
    NSMutableArray* demoedMedia=notification.object;
    
    [demoedMediaFiles addObjectsFromArray:demoedMedia];
    
    NSLog(@"demoed media files from notification %@",demoedMedia);
    
}
-(void)savedCoachSurveyDetails:(NSNotification*)notification
{
    NSMutableDictionary* coachSurveyDict = [[NSMutableDictionary alloc]init];
    coachSurveyDict=notification.object;
    NSMutableArray* surveyResponsesArray=[coachSurveyDict valueForKey:@"Survey_Responses_Array"];
    NSMutableArray* surveyConfirmationArray=[coachSurveyDict valueForKey:@"Survey_Confirmation_Array"];
    if (visitDetails.coachSurveyArray.count>0) {
        [visitDetails.coachSurveyArray addObjectsFromArray:surveyResponsesArray];
        [visitDetails.coachSurveyConfirmationArray addObjectsFromArray:surveyConfirmationArray];
    }
    else
    {
        visitDetails.coachSurveyArray=surveyResponsesArray;
        visitDetails.coachSurveyConfirmationArray=surveyConfirmationArray;
    }
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MedRepDefaults setSignatureCapturedForCurrentCall:NO];

    appControl = [AppControl retrieveSingleton];
    docDispalayManager=[[SalesworxDocumentDisplayManager alloc] init];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:MedRepStartCallTitle];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(demoedMediaNotificationHandler:) name:@"DEMOED_MEDIA" object:nil];

    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(savedCoachSurveyDetails:) name:@"COACH_SURVEY" object:nil];

    
    isTableViewSampleProductSelected=NO;
    callDate=[NSDate date];
    
    demoedMediaFiles=[[NSMutableArray alloc]init];
    
    sampleProductsArray=[[NSMutableArray alloc]init];
    VisitGivenSampleProductsArray=[[NSMutableArray alloc]init];
    VisitSamplesProductsArray=[[NSMutableArray alloc]init];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:callDate forKey:@"faceTimeTimer"];
    
    
    
    faceTimeTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startFaceTimeTimer) userInfo:nil repeats:YES];
    
    NSLog(@"facetime = %f",faceTimeTimer);
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
    
    [[NSUserDefaults standardUserDefaults]setObject:callStartDate forKey:@"EDetailing_Start_Time"];
    
    [CancelButton setHidden:YES];
    QtyTextField.delegate=self;
    // Do any additional setup after loading the view from its nib.
    appDelegate.CurrentRunningMedRepVisitDetails=[[VisitDetails alloc]init];
    productMediaTextField.text = @"All Products";
    
    
    
    productVideoFilesArray =[[NSMutableArray alloc]init];
    productPdfFilesArray =[[NSMutableArray alloc]init];
    productDetailsArray=[[NSMutableArray alloc]init];
    presentationsArray=[[NSMutableArray alloc]init];
    demoPlanMediaFiles=[[NSMutableArray alloc]init];
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [self fetchMediaFilesData];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (productMediaFilesArray.count>0) {
                
                productImagesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
                
                
                productVideoFilesArray= [[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
                
                productPdfFilesArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
                
                presentationsArray=[[productMediaFilesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
                
                
                
                if (productImagesArray.count==0) {
                    [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
                }
                else
                {
                    [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
                }
                if (productVideoFilesArray.count==0) {
                    [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
                }
                else
                {
                    [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
                }
                if (productPdfFilesArray.count==0) {
                    [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
                }
                
                else
                {
                    [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
                }
                
                if (presentationsArray.count==0) {
                    [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
                }
                
                else
                {
                    [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
                }
                
                [btnProduct setBackgroundImage:[UIImage imageNamed:@"MedRep_AllMediaBtnActive"] forState:UIControlStateNormal];
                btnProduct.layer.borderWidth=0.0;
                [self imagesButtonTapped:nil];
                
            }
            else
            {
                
                [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
                [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
                [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
                [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
                [btnProduct setBackgroundImage:[UIImage imageNamed:@"MedRep_AllMediaBtnInActive"] forState:UIControlStateNormal];
                
                
            }
            
            
            [customPresentationsButton setBackgroundImage:[UIImage imageNamed:@"PresentationIconActive"] forState:UIControlStateNormal];
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
    
    imagesPathArray=[[NSMutableArray alloc]init];
    productImagesArray=[[NSMutableArray alloc]init];
    
    
    [demoPlanProductsCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    
    [demoPlanProductsCollectionView registerClass:[MedRepVisitsStartMultipleCallsPresentationCollectionViewCell class] forCellWithReuseIdentifier:@"presentationCell"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
}

-(void)viewWillAppear:(BOOL)animated
{
    // NSLog(@"demoed media files are %@", demoedMediaFiles);
    
    faceTimeLbl.font=kSWX_FONT_SEMI_BOLD(18);
    
    //what is task or notes created in this screen, so get that query here as well
    appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    if (self.isMovingToParentViewController) {
        
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingStartCall];
        
        appDelegate.CurrentRunningMedRepVisitDetails=self.visitDetails;
        NSString* taskStatus=[visitDetailsDict valueForKey:@"TASK_STATUS"];
        NSString* notesAvailable=[visitDetailsDict valueForKey:@"Notes_Available"];
        if ([taskStatus isEqualToString:@"OVER_DUE"]) {
            [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
        }
        
        
        else if ([taskStatus isEqualToString:@"PENDING"])
            
        {
            [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
        }
        if ([notesAvailable isEqualToString:@"YES"]) {
            [notesStatusButton setBackgroundColor:KNOTESAVAILABLECOLOR];
        }
        else
        {
            [notesStatusButton setBackgroundColor:KNOTESUNAVAILABLECOLOR];
        }
        [self refreshTaskandNotesStatus:self.visitDetails];
        
        
        if (collectionViewDataArray.count>0) {
            [demoPlanProductsCollectionView reloadData];
            
        }
    }
    else
    {
        self.visitDetails.taskArray=appDelegate.CurrentRunningMedRepVisitDetails.taskArray;
        self.visitDetails.notesArray=appDelegate.CurrentRunningMedRepVisitDetails.notesArray;
        [demoPlanProductsCollectionView reloadData];
    }
    
    if (visitDetailsDict.count>0) {
        
        // NSLog(@"visit details in start call %@", [visitDetailsDict description]);
        locationLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Location_Name"]];
        
        
        if ([[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
            
            // doctorLbl.text=@"N/A";
            
            //this might be a pharmacy
            
            NSLog(@"this is a pharmacy in start call");
            
            doctorPharmacyHeaderlbl.text=@"Contact Name";
            
            doctorLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Pharmacy_Contact_Name"]];
            
        }
        else
        {
            
            doctorLbl.text=[NSString stringWithFormat:@"%@", [visitDetailsDict valueForKey:@"Doctor_Name"]];
            
        }
        
    }
    self.samplesLbl.backgroundColor=[UIColor colorWithRed:(234.0/255.0) green:(237.0/255.0) blue:(242.0/255.0) alpha:1];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    qtyTxtFld.leftView = paddingView;
    qtyTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    SalesWorxImageBarButtonItem* startVisitBarButtonItem=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MedRep_EndCall"] style:UIBarButtonItemStylePlain target:self action:@selector(endCallTapped)];
    
        SalesWorxImageBarButtonItem* surveyBarButton=[[SalesWorxImageBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_Survey"] style:UIBarButtonItemStylePlain target:self action:@selector(surveyButtonTapped)];
    
    surveysArray = [[SWDatabaseManager retrieveManager]fetchMedicinaSurveys:[visitDetailsDict valueForKey:@"Location_ID"]];

    
    self.navigationItem.rightBarButtonItem=startVisitBarButtonItem;

    if([[AppControl retrieveSingleton].ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]];
        NSArray*  PDAComponents=[PDARIGHTS componentsSeparatedByString:@","];
        if([PDAComponents containsObject:KFieldMarketingCoachSurveyPDA_RightsCode] && [[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Location_Type"]] isEqualToString:kPharmacyLocation])
        {
            self.navigationItem.rightBarButtonItems=@[startVisitBarButtonItem,surveyBarButton];

        }
    }
    else
    {
    }
    
    
    selectedSamplesArray=[[NSMutableArray alloc]init];
    
    
    selectedSamplesDictionary=[[NSMutableDictionary alloc]init];
    
    //selectedSampleProductsArray=[[NSMutableArray alloc]init];
    
    empStockArray=[[NSMutableArray alloc]init];
    
    
    
    
    qtyTxtFld.layer.borderColor=UITextFieldBorderColor.CGColor;
    
    qtyTxtFld.layer.borderWidth = 1.0;
    
    tempSelectedProducts=[[NSMutableArray alloc]init];
    
    
    tempSelectedProducts=sampleProductsArray;
    
    // NSLog(@"temp selected samples are %@", [tempSelectedProducts description]);
    
    NSLog(@"edetailing samples are %@", sampleProductsArray);
    
    selectedIndexValue=0;
    
    UIBarButtonItem *closeVisitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close Call", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeVisitTapped)];
    
    
    [closeVisitButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem=closeVisitButton;
    
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    self.navigationController.navigationBar.topItem.title = @"";
    
    
    
    tasksButton.layer.borderWidth=1.0f;
    notesButton.layer.borderWidth=1.0f;
    tasksButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    notesButton.layer.borderColor=[MedRepTaskandNotesButtonBorderColor CGColor];
    
    
    
    notesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    notesButton.layer.shadowOffset = CGSizeMake(2, 2);
    notesButton.layer.shadowOpacity = 0.1;
    notesButton.layer.shadowRadius = 1.0;
    
    
    
    tasksButton.layer.shadowColor = [UIColor blackColor].CGColor;
    tasksButton.layer.shadowOffset = CGSizeMake(2, 2);
    tasksButton.layer.shadowOpacity = 0.1;
    tasksButton.layer.shadowRadius = 1.0;
    
    
    avblQtyImgView.layer.shadowColor = [UIColor blackColor].CGColor;
    avblQtyImgView.layer.shadowOffset = CGSizeMake(2, 2);
    avblQtyImgView.layer.shadowOpacity = 0.1;
    avblQtyImgView.layer.shadowRadius = 1.0;
    
    
    expiryImgView.layer.shadowColor = [UIColor blackColor].CGColor;
    expiryImgView.layer.shadowOffset = CGSizeMake(2, 2);
    expiryImgView.layer.shadowOpacity = 0.1;
    expiryImgView.layer.shadowRadius = 1.0;
    
    
    
    for (UIView *currentView in self.view.subviews) {
        
        if ([currentView isKindOfClass:[UIView class]]) {
            
            
            if (currentView.tag==1 || currentView.tag==2|| currentView.tag==3 ||currentView.tag==4 ) {
                
                currentView.layer.shadowColor = [UIColor blackColor].CGColor;
                currentView.layer.shadowOffset = CGSizeMake(2, 2);
                currentView.layer.shadowOpacity = 0.1;
                currentView.layer.shadowRadius = 1.0;
                
            }
        }
    }
    
    
    samplesBgView.layer.shadowColor = [UIColor blackColor].CGColor;
    samplesBgView.layer.shadowOffset = CGSizeMake(0, 2);
    samplesBgView.layer.shadowOpacity = 0.1;
    samplesBgView.layer.shadowRadius = 1.0;
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    NSLog(@"Medis files count in start call  Images : %lu Videos: %lu PDF: %lu",(unsigned long) productImagesArray.count,(unsigned long)productVideoFilesArray.count,(unsigned long)productPdfFilesArray.count);
    
    if(![appControl.FM_ENABLE_PRESENTATION_MODULE isEqualToString:KAppControlsYESCode]){
        [customPresentationsButton setHidden:YES];
        xPPTButtonTrailingMArginConstraintToParentView.constant = 8.0;
    }
    
    if([appControl.ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode]){
        NSMutableArray *tempItemTypeArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select Code_Value, Code_Description from TBL_App_Codes where Code_Type = 'Product_Type'"];
        ItemTypeArray = [[NSMutableArray alloc]init];
        ItemCodeValueArray = [[NSMutableArray alloc]init];
        for (NSMutableDictionary *dicItem in tempItemTypeArray) {
            [ItemTypeArray addObject:[dicItem valueForKey:@"Code_Description"]];
            [ItemCodeValueArray addObject:[dicItem valueForKey:@"Code_Value"]];
        }
        
        productTextField.userInteractionEnabled = NO;
        lotNumberTextField.userInteractionEnabled = NO;
        QtyTextField.userInteractionEnabled = NO;
        productTextField.alpha = 0.7;
        lotNumberTextField.alpha = 0.7;
        QtyTextField.alpha = 0.7;
    } else {
        
        [itemTypeView setHidden:YES];
        xItemViewWidthConstraint.constant = 0.0;
        xItemViewTrailingConstraint.constant = 0.0;
        
        productTextField.userInteractionEnabled = YES;
        lotNumberTextField.userInteractionEnabled = YES;
        QtyTextField.userInteractionEnabled = YES;
        productTextField.alpha = 1.0;
        lotNumberTextField.alpha = 1.0;
        QtyTextField.alpha = 1.0;
        
        sampleProductsArray = [MedRepQueries fetchSamplesforEdetailing:@""];
        if (sampleProductsArray.count > 0) {
            
            VisitSamplesProductsArray=[self ConvertSampleProductsDataToObjects:sampleProductsArray];
            if (VisitSamplesProductsArray.count==1) {
                avblQtyLbl.text=@"";
                QtyTextField.enabled=YES;
                addButton.enabled=YES;
                selectedLotNumberIndex=nil;
                SampleProductClass *product=[[SampleProductClass alloc]init];
                product=[VisitSamplesProductsArray objectAtIndex:0];
                NSString *productId=product.ProductId;
                [self setSampleProductDetails:productId];
            }
        }
    }
}

-(void)surveyButtonTapped
{
    
    NSPredicate* distinctPredicate=[NSPredicate predicateForDistinctWithProperty:@"Survey_ID"];
    
    NSMutableArray* distinctArray = [[visitDetails.coachSurveyArray filteredArrayUsingPredicate:distinctPredicate]mutableCopy];
    NSLog(@"distinct array is %@",distinctArray);
    
    
    
    if (surveysArray.count>0) {
        
    
    NSLog(@"survey button tapped");
//    if(visitDetails.coachSurveyArray.count>0)
//    {
//        visitDetails.coachSurveyArray=[[NSMutableArray alloc]init];
//        visitDetails.coachSurveyConfirmationArray=[[NSMutableArray alloc]init];
//    }
    SalesWorxCoachReportPharmacyDetailsViewController *pharmacyDetailsVC = [[SalesWorxCoachReportPharmacyDetailsViewController alloc]init];
        
    pharmacyDetailsVC.locationID=[NSString getValidStringValue:[visitDetailsDict valueForKey:@"Location_ID"]];
    pharmacyDetailsVC.pharmacyDetailsDelegate = self;
        pharmacyDetailsVC.visitDetails=visitDetails;
    UINavigationController *mapPopUpViewController = [[UINavigationController alloc]initWithRootViewController:pharmacyDetailsVC];
    mapPopUpViewController.navigationBarHidden = YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    pharmacyDetailsVC.view.backgroundColor = KPopUpsBackGroundColor;
    pharmacyDetailsVC.modalPresentationStyle = UIModalPresentationCustom;
    pharmacyDetailsVC.surveysArray=surveysArray;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
    }
    else
    {
        
    }
}
-(void)didSelectSurvey:(CoachSurveyType*)selectedSurvey
{
 
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Selected_Survey_ID"];
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Selected_Survey_Title"];
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"is_From_Visit"];


    if ([[AppControl retrieveSingleton].FM_SURVEY_VERSION isEqualToString:@"2"]){
        SalesWorxCoachReportViewController * reportVC=[[SalesWorxCoachReportViewController alloc]init];
        reportVC.selectedSurveyType=selectedSurvey;
        reportVC.isFromVisit=YES;
        [self.navigationController pushViewController:reportVC animated:YES];
    }else{
     SurveySwiftDetailsViewController *surveyVC  =  [[SurveySwiftDetailsViewController alloc]init];
    [[NSUserDefaults standardUserDefaults]setValue:selectedSurvey.Survey_ID forKey:@"Selected_Survey_ID"];
    [[NSUserDefaults standardUserDefaults]setValue:selectedSurvey.Survey_Title forKey:@"Selected_Survey_Title"];
    [[NSUserDefaults standardUserDefaults]setValue:@"Y" forKey:@"is_From_Visit"];

    [self.navigationController pushViewController:surveyVC animated:YES];

    }
}

-(void)closeVisitTapped
{
    BOOL deviceTimeisValid =  [SWDefaults isDeviceTimeValid];
    if (!deviceTimeisValid)
    {
        [SWDefaults showDeviceTimeInvalidAlertinViewController:self];
    }
    else
    {
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self displayContactPopUpView];
                                   }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)] ,nil];
        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Close Call" andMessage:@"Would you like to close this call?" andActions:actionsArray withController:self];
    }
}

-(void)displayContactPopUpView
{
    MedRepVisitsEndCallViewController *presentingView = [[MedRepVisitsEndCallViewController alloc]init];
    presentingView.visitDetailsDict=visitDetailsDict;
    presentingView.currentVisit=visitDetails;
    presentingView.visitCompletedDelegate=self;
    presentingView.isCloseFromEdetailingStartCall = YES;
    UINavigationController * mapPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    mapPopUpViewController.navigationBarHidden=YES;
    mapPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    mapPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:mapPopUpViewController animated:NO completion:nil];
}

-(void)visitCompletedWithOutCalls:(BOOL)status
{
    if (status==YES) {
        
        if ([appControl.FM_VISIT_WITH_PLANNED_DOCTOR_ONLY isEqualToString:KAppControlsYESCode]) {
            BOOL status=[MedRepQueries updateVisitStatus:visitDetails];
            if (status==YES) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"VisitDidFinishNotification" object:visitDetails];
                
                if ([appControl.FM_ENABLE_DISTRIBUTION_CHECK isEqualToString:KAppControlsYESCode] && [[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
                    
                    if (isFromDashboard) {
                        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
                    } else{
                        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
                    }
                } else {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }
        } else {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"CallDidFinishNotification" object:visitDetails];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)fetchMediaFilesData
{
    NSString* demoPlanID=[visitDetailsDict valueForKey:@"Demo_Plan_ID"];
    if (demoPlanID!=nil)
    {
        productMediaFilesArray=[MedRepQueries fetchMediaFileswithDemoPlanID:demoPlanID];
        unfilteredMediaArray =[[NSMutableArray alloc]init];
        //add dummy product with name all products
        ProductMediaFile* allMediaFile = [[ProductMediaFile alloc]init];
        allMediaFile.Product_Name = @"All Products";
        allMediaFile.Entity_ID_1 = @"1";
        unfilteredMediaArray = productMediaFilesArray;
        [unfilteredMediaArray insertObject:allMediaFile atIndex:0];

    }
}

-(void)endCallTapped
{
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *callStartDate =  [formatterTime stringFromDate:[NSDate date]];
    
    [[NSUserDefaults standardUserDefaults]setObject:callStartDate forKey:@"EDetailing_End_Time"];
    
    MedRepEDetailingEndVisitViewController * endCallVC=[[MedRepEDetailingEndVisitViewController alloc]init];
    NSLog(@"demonstrated products being pushed %@",demoedMediaFiles);
    
    endCallVC.demonstratedProductsArray = demoedMediaFiles;
    endCallVC.samplesGivenArray = VisitGivenSampleProductsArray;
    endCallVC.visitDetailsDict=visitDetailsDict;
    if (isFromDashboard) {
        endCallVC.isFromDashboard = YES;
    }
    
    endCallVC.visitDetails.taskArray=self.visitDetails.taskArray;
    endCallVC.visitDetails.notesArray=self.visitDetails.notesArray;
    endCallVC.visitDetails=self.visitDetails;
    self.visitDetails.samplesArray = VisitGivenSampleProductsArray;
    [self.navigationController pushViewController:endCallVC animated:YES];
}

-(void)startFaceTimeTimer
{
    NSDate * currentTimer=[[NSUserDefaults standardUserDefaults]objectForKey:@"faceTimeTimer"];
    NSDate *now = [NSDate date];
    NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
    NSLog(@"facetime = %f",elapsedTimeDisplayingPoints);
    // NSLog(@"wait time counter ticking %@",[self stringFromTimeInterval:elapsedTimeDisplayingPoints]);
    
    faceTimeLbl.text=[self stringFromTimeInterval:elapsedTimeDisplayingPoints];
    
    
}
- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}



#pragma mark Navigation Bar button Actions
- (IBAction)taskButtonTapped:(id)sender {
    
    popOverController=nil;
    MedRepEdetailTaskViewController * edetailTaskVC=[[MedRepEdetailTaskViewController alloc]init];
    edetailTaskVC.visitDetails=self.visitDetails;
    edetailTaskVC.tasksArray=self.visitDetails.taskArray;
    edetailTaskVC.delegate=self;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(376, 600) animated:YES];
    edetailTaskVC.tasksPopOverController=popOverController;
    
    
    
    CGRect relativeFrame = [tasksButton convertRect:tasksButton.bounds toView:self.view];
    
    if ([SWDefaults isRTL]) {
        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else
    {
        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    }
    
    
    
}


- (IBAction)notesButtonTapped:(id)sender {
    popOverController=nil;
    MedRepEDetailNotesViewController * edetailTaskVC=[[MedRepEDetailNotesViewController alloc]init];
    edetailTaskVC.visitDetails=self.visitDetails;
    edetailTaskVC.delegate=self;
    edetailTaskVC.notesArray=self.visitDetails.notesArray;
    UINavigationController* navController=[[UINavigationController alloc]initWithRootViewController:edetailTaskVC];
    popOverController=[[UIPopoverController alloc]initWithContentViewController:navController];
    [popOverController setDelegate:self];
    [popOverController setPopoverContentSize:CGSizeMake(376, 350) animated:YES];
    edetailTaskVC.notesPopOverController=popOverController;
    CGRect relativeFrame = [notesButton convertRect:notesButton.bounds toView:self.view];
    
    [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

#pragma button action methods

- (IBAction)videoImagesButtonTapped:(id)sender {
    
    
    
    
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:YES];
    [customPresentationsButton setSelected:NO];
    
    [btnPresentation setSelected:NO];
    btnPresentation.layer.borderWidth=0.0;
    
    if ([videoButton isSelected]) {
        
        videoButton .layer.borderWidth=2.0;
        videoButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    
    imagesButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    customPresentationsButton.layer.borderWidth=0.0f;
    
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productVideoFilesArray;
    
    
    // clearSelectedCells=YES;
    
    
    //                if (cell.subviews.count>1) {
    //
    //                    [[cell.subviews objectAtIndex:1]removeFromSuperview];
    //                }
    
    [demoPlanProductsCollectionView reloadData];
    
    
}

- (IBAction)pdfImagesButtonTapped:(id)sender {
    
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:YES];
    [videoButton setSelected:NO];
    [customPresentationsButton setSelected:NO];
    
    [btnPresentation setSelected:NO];
    btnPresentation.layer.borderWidth=0.0;
    
    
    if ([pptButton isSelected]) {
        
        pptButton .layer.borderWidth=2.0;
        pptButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    customPresentationsButton.layer.borderWidth=0.0f;
    
    
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productPdfFilesArray;
    
    
    [demoPlanProductsCollectionView reloadData];
}

- (IBAction)imagesButtonTapped:(id)sender {
    
    
    // clearSelectedCells=YES;
    
    
    [imagesButton setSelected:YES];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnPresentation setSelected:NO];
    [customPresentationsButton setSelected:NO];
    
    
    if ([imagesButton isSelected]) {
        
        imagesButton .layer.borderWidth=2.0;
        imagesButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    
    
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnPresentation.layer.borderWidth=0.0;
    customPresentationsButton.layer.borderWidth=0.0f;
    
    
    
    
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    
    collectionViewDataArray=productImagesArray;
    
    
    [demoPlanProductsCollectionView reloadData];
}



-(UIImage *)generateThumbImage : (NSString *)filepath
{
    NSURL *url = [NSURL fileURLWithPath:filepath];
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = 2000;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

-(UIImage*)generateThumbImageforPdf:(NSString*)filePath

{
    NSURL* pdfFileUrl = [NSURL fileURLWithPath:filePath];
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
    CGPDFPageRef page;
    
    CGRect aRect = CGRectMake(0, 0, 200, 200); // thumbnail size
    UIGraphicsBeginImageContext(aRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIImage* thumbnailImage;
    
    
    NSUInteger totalNum = CGPDFDocumentGetNumberOfPages(pdf);
    
    for(int i = 0; i < totalNum; i++ ) {
        
        
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0.0, aRect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        CGContextSetGrayFillColor(context, 1.0, 1.0);
        CGContextFillRect(context, aRect);
        
        
        // Grab the first PDF page
        page = CGPDFDocumentGetPage(pdf, i + 1);
        CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFMediaBox, aRect, 0, true);
        // And apply the transform.
        CGContextConcatCTM(context, pdfTransform);
        
        CGContextDrawPDFPage(context, page);
        
        // Create the new UIImage from the context
        thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
        
        //Use thumbnailImage (e.g. drawing, saving it to a file, etc)
        
        CGContextRestoreGState(context);
        
    }
    
    
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(pdf);
    
    return thumbnailImage;
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return  collectionViewDataArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(200, 185);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (customPresentationsButton.isSelected) {
        
        static NSString *cellIdentifier = @"productCell";
        MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
        cell.productImageView.image=[UIImage imageNamed:@"PresentationStartCallImage"];
        FieldMarketingPresentation * currentPresentation=[self.visitDetails.fieldMarketingPresentationsArray objectAtIndex:indexPath.row];
        if ([currentPresentation.isSelected isEqualToString:KAppControlsYESCode]) {
            cell.selectedImgView.hidden=NO;
            
        }
        else{
            cell.selectedImgView.hidden=YES;
            
        }
        
        cell.captionLbl.text=[SWDefaults getValidStringValue:currentPresentation.presentationName];
        return cell;
    }
    else{
        
        static NSString *cellIdentifier = @"productCell";
        NSString* mediaType = [SWDefaults getValidStringValue:[[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ]];
        MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
        cell.selectedImgView.hidden=YES;
        NSString*documentsDirectory;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        if ([mediaType isEqualToString:@"Image"]) {
            NSString* thumbnailImageName = [[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
            cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
            cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
            cell.placeHolderImageView.hidden=NO;
            cell.productImgViewTopConstraint.constant=2.0;
            cell.productimageViewLeadingConstraint.constant=2.0;
            cell.productImageViewTrailingConstraint.constant=2.0;
        }
        else if ([mediaType isEqualToString:@"Video"])
        {
            cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
            cell.captionLbl.text=[[productVideoFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
            cell.productImgViewTopConstraint.constant=0.0;
            cell.productimageViewLeadingConstraint.constant=0.0;
            cell.productImageViewTrailingConstraint.constant=0.0;
        }
        else if ([mediaType isEqualToString:@"Brochure"])
        {
            cell.productImgViewTopConstraint.constant=0.0;
            cell.productimageViewLeadingConstraint.constant=0.0;
            cell.productImageViewTrailingConstraint.constant=0.0;
            cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
            cell.captionLbl.text=[[productPdfFilesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        }
        else if ([mediaType isEqualToString:@"Powerpoint"])
        {
            cell.productImgViewTopConstraint.constant=0.0;
            cell.productimageViewLeadingConstraint.constant=0.0;
            cell.productImageViewTrailingConstraint.constant=0.0;
            cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
            cell.captionLbl.text=[[presentationsArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        }
        if (demoedMediaFiles.count>0) {
            
            for (NSInteger i=0; i<demoedMediaFiles.count; i++) {
                NSString* selectedFileName=[SWDefaults getValidStringValue:[[demoedMediaFiles objectAtIndex:i] valueForKey:@"Media_File_ID"]];
                
                // in case of custom presentation is tapped which is created by default by stiching together all the files, its crashing here, because custom presentation object returns a string array with media file ids, instead of Product media object, so [[demoedMediaFiles objectAtIndex:i] valueForKey:@"Media_File_ID"] crashes in case of custom presentation, this has been fixed by updating the notification in salesworxpresentation child view controller to return media file object instead of media file id.
                
                NSString * currentFileName=[SWDefaults getValidStringValue:[[collectionViewDataArray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID"] ];
               // NSString * currentFileName=[SWDefaults getValidStringValue:[[collectionViewDataArray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID"]] ;

               // NSLog(@"selected file name : %@  current file name : %@", selectedFileName,currentFileName);
                if ([selectedFileName isEqualToString:currentFileName]) {
                    cell.selectedImgView.hidden=NO;
                }
                else
                {
                    
                }
            }
        }
        cell.productImageView.backgroundColor=[UIColor clearColor];
        if (indexPath.row== selectedCellIndex) {
            cell .layer.borderWidth=2.0;
            cell.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
        }
        else
        {
            cell.layer.borderWidth=0;
        }
        return cell;
    }
}
-(void)demoedMedia:(NSMutableArray*)demoedMedia
{
    
    NSLog(@"demoed media from presentation %@", demoedMedia);
    
    for (NSInteger i=0; i<demoedMedia.count; i++) {
        NSString* selectedFileID=[demoedMedia objectAtIndex:i] ;
        if ([demoedMediaFiles containsObject:selectedFileID]) {
            
        }
        else
        {
            [demoedMediaFiles addObject:selectedFileID];
        }
    }
    NSLog(@"demoed media with start call is %@", demoedMediaFiles);
    
}

-(void)pushCustomPresentation{
    
    
    
    
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if (customPresentationsButton.isSelected) {
        
        FieldMarketingPresentation * currentPresentation=[self.visitDetails.fieldMarketingPresentationsArray objectAtIndex:indexPath.row];
        currentPresentation.isSelected=KAppControlsYESCode;
        currentPresentation.presentationContentArray=[[self.visitDetails.fieldMarketingPresentationsArray valueForKey:@"presentationContentArray"] objectAtIndex:indexPath.row];
        [self.visitDetails.fieldMarketingPresentationsArray replaceObjectAtIndex:indexPath.row withObject:currentPresentation];
        
        NSMutableArray* currentPresentationMedia=[[self.visitDetails.fieldMarketingPresentationsArray valueForKey:@"presentationContentArray"] objectAtIndex:indexPath.row];
        
        /*
         UCSPresentationViewerViewController * viewerVC=[[UCSPresentationViewerViewController alloc]init];
         viewerVC.demoedMediaDelegate=self;
         
         viewerVC.MediaFiles=currentPresentationMedia;
         [self.navigationController pushViewController:viewerVC animated:YES];*/
        
        
        SalesWorxPresentationHomeViewController * tempVC=[[SalesWorxPresentationHomeViewController alloc]init];
        tempVC.mediaArray=currentPresentationMedia;
        [self.navigationController pushViewController:tempVC animated:YES];
        
        
    }
    else
    {
        selectedCellIndex=indexPath.row;
        SelectedProductMediaFile=[collectionViewDataArray objectAtIndex:indexPath.row];
        SelectedProductMediaFile.isDemoed=YES;
        NSLog(@"data after replacing %@", [collectionViewDataArray objectAtIndex:indexPath.row]);
        
        NSString* selectedFileID=[[collectionViewDataArray objectAtIndex:indexPath.row]valueForKey:@"Media_File_ID" ] ;
        if ([demoedMediaFiles containsObject:selectedFileID]) {
            
        }
        else
        {
            // [demoedMediaFiles addObject:selectedFileID];
        }
        NSString* mediaType = [SWDefaults getValidStringValue:[[collectionViewDataArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row]] ;
        NSString*documentsDirectory;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[collectionViewDataArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row] ];
        
        
        if ([mediaType isEqualToString:@"Image"]) {
            [imagesPathArray removeAllObjects];
            for (NSInteger i=0; i<productImagesArray.count; i++) {
                
                // [imagesPathArray addObject:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i]];
                [imagesPathArray addObject: [documentsDirectory stringByAppendingPathComponent:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:i] ]];
            }
            NSLog(@"product images array before selecting %@", [productImagesArray description]);
            MedRepProductImagesViewController* imagesVC=[[MedRepProductImagesViewController alloc]init];
            imagesVC.productImagesArray=imagesPathArray;
            imagesVC.imagesViewedDelegate=self;
            
            imagesVC.productDataArray=productImagesArray;
            imagesVC.currentImageIndex=indexPath.row;
            imagesVC.selectedIndexPath=indexPath;
            [self presentViewController:imagesVC animated:YES completion:nil];
        }
        
        else if ([mediaType isEqualToString:@"Video"])
        {
            
            MedRepProductVideosViewController * videosVC=[[MedRepProductVideosViewController alloc]init];
            videosVC.videosViewedDelegate=self;
            videosVC.currentImageIndex=indexPath.row;
            videosVC.selectedIndexPath=indexPath;
            videosVC.productDataArray=productVideoFilesArray;
            
            [self.navigationController presentViewController:videosVC animated:YES completion:nil];
            
            
            
            /*
             NSURL* documentUrl=[NSURL fileURLWithPath:fileName];
             
             
             moviePlayer = [[MPMoviePlayerViewController
             alloc]initWithContentURL:documentUrl];
             
             [[NSNotificationCenter defaultCenter] addObserver:self
             selector:@selector(moviePlayBackDidFinish:)
             name:MPMoviePlayerPlaybackDidFinishNotification
             object:moviePlayer];
             
             
             
             
             [self presentViewController:moviePlayer animated:YES completion:nil];*/
            
            
            
            
        }
        
        else if ([mediaType isEqualToString:@"Brochure"])
        {
            
            
            
            
            if (productPdfFilesArray.count>0) {
                
                MedRepProductPDFViewController* pdfVC=[[MedRepProductPDFViewController alloc]init];
                pdfVC.pdfViewedDelegate=self;
                
                pdfVC.productDataArray=productPdfFilesArray;
                
                pdfVC.selectedIndexPath=indexPath;
                [self presentViewController:pdfVC animated:YES completion:nil];
                
            }
            
            else
            {
                
                UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No PDF files are available for the selected demo plan" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [noDataAvblAlert show];
            }
            
            
            
            
            
            /*
             MedRepProductMediaDisplayViewController * mediaDisplayVC=[[MedRepProductMediaDisplayViewController alloc]init];
             mediaDisplayVC.filePath=fileName;
             
             [self presentViewController:mediaDisplayVC animated:YES completion:nil];*/
            
            
            
        }
        
        else if ([mediaType isEqualToString:@"Powerpoint"])
        {
            if (presentationsArray.count>0) {
                
                NSString * selectedFilePath=[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:[[presentationsArray objectAtIndex:indexPath.row]valueForKey:@"File_Name"]];
                
                /** Power point slide show files will display in external apps*/
                if([[selectedFilePath lowercaseString] hasSuffix:KppsxFileExtentionStr]){
                    [docDispalayManager ShowUserActivityShareViewInView:collectionView SourceRect:cell.frame AndFilePath:selectedFilePath];
                }else{
                    MedRepProductHTMLViewController *obj = [[MedRepProductHTMLViewController alloc]init];
                    obj.HTMLViewedDelegate = self;
                    obj.productDataArray = presentationsArray;
                    obj.showAllProducts=YES;
                    obj.isObjectData=YES;
                    
                    obj.selectedIndexPath = indexPath;
                    obj.currentImageIndex=indexPath.row;
                    [self presentViewController:obj animated:YES completion:nil];
                }
            }
            else
            {
                UIAlertView* noDataAvblAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"No presentation files are available " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [noDataAvblAlert show];
            }
        }
    }
    
    //  [demoPlanProductsCollectionView reloadData];
    
    
    
}

-(void)setBorderOnLastViewedImage:(NSInteger)index {
    
    [demoPlanProductsCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [demoPlanProductsCollectionView.delegate collectionView:demoPlanProductsCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
}

//-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
//{
//
//
//    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
//    popOverVC.popOverContentArray=contentArray;
//    popOverVC.salesWorxPopOverControllerDelegate=self;
//    popOverVC.disableSearch=YES;
//
//    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
//    multiCallsPopOverController=nil;
//    multiCallsPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
//    multiCallsPopOverController.delegate=self;
//    popOverVC.popOverController=multiCallsPopOverController;
//    popOverVC.titleKey=popOverString;
//    if ([popOverString isEqualToString:kDoctorTitle]) {
//        [multiCallsPopOverController setPopoverContentSize:CGSizeMake(400, 273) animated:YES];
//
//    }
//    else{
//        [multiCallsPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
//
//    }
//    [multiCallsPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//}


-(void)productDropDownTapped
{
    popOverController=nil;
    QtyTextField.enabled=YES;
    addButton.enabled=YES;
    popString=@"Product";
    
    
    [self presentPopoverfor:productTextField withTitle:kFieldMarketingEdetailinigProductTitle withContent:ProductNamesArray];
    
    
    //    locationPopOver = nil;
    //    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:400];
    //
    //    CGRect relativeFrame = [productTextField convertRect:productTextField.bounds toView:self.view];
    //
    //
    //    NSLog(@"product names array is %@",ProductNamesArray);
    //
    //    locationPopOver.colorNames = ProductNamesArray;
    //    locationPopOver.delegate = self;
    //    if (popOverController == nil) {
    //        popOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
    //        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //        popOverController.delegate=self;
    //    }
    //    else {
    //        //The color picker popover is showing. Hide it.
    //        [popOverController dismissPopoverAnimated:YES];
    //        popOverController = nil;
    //        popOverController.delegate=nil;
    //    }
    
}

-(void)ItemTypeDropDownTapped
{
    popOverController=nil;
    popString=@"Product Type";
    
    [self presentPopoverfor:itemTypeTextField withTitle:kFieldMarketingEdetailinigItemTypeTitle withContent:ItemTypeArray];
}


#pragma mark ImagesViewed Delegate

-(void)imagesViewedinFullScreenDemo:(NSMutableArray*)imagesArray

{
    NSLog(@"demoed media files before adding %@", demoedMediaFiles);
    NSLog(@"demoed media files before adding  in delegate%@", imagesArray);
    
    for (NSInteger i=0; i<imagesArray.count; i++) {
        
        NSString* selectedFileID=[[imagesArray objectAtIndex:i]valueForKey:@"Media_File_ID"] ;
        
        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else
        {
             [demoedMediaFiles addObject:[imagesArray objectAtIndex:i]];
        }
        
//        if ([demoedMediaFiles containsObject:selectedFileID]) {
//
//        }
//        else
//        {
//           // [demoedMediaFiles addObject:[imagesArray objectAtIndex:i]];
//        }
    }
    
    NSLog(@"demoed files are %@", [NSString getValidStringValue:[demoedMediaFiles valueForKey:@"viewedTime"]]);
    NSLog(@"images viewed in delegate are %@", [NSString getValidStringValue:[imagesArray valueForKey:@"viewedTime"]]);
    
}


#pragma mark Viewed Videos delegate

-(void)videosViewedinFullScreenDemo:(NSMutableArray *)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        
        ProductMediaFile* currentFile=[imagesArray objectAtIndex:i];
        
        NSString* selectedFileID=[NSString getValidStringValue:currentFile.Media_File_ID] ;
        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else
        {
            [demoedMediaFiles addObject:[imagesArray objectAtIndex:i]];
        }
//        if ([demoedMediaFiles containsObject:selectedFileID]) {
//
//        }
//        else
//        {
//            [demoedMediaFiles addObject:currentFile];
//
//        }
    }
    
    NSLog(@"demoed files are %@", [demoedMediaFiles description]);
    
    NSLog(@"videos viewed in delegate are %@", [imagesArray description]);
    
}


#pragma mark PDF's Viewed delegate

-(void)PDFViewedinFullScreenDemo:(NSMutableArray*)imagesArray
{
    for (NSInteger i=0; i<imagesArray.count; i++) {
        ProductMediaFile* selectedMedia=[imagesArray objectAtIndex:i];
        
        NSString* selectedFileID=[NSString getValidStringValue:selectedMedia.Media_File_ID] ;
        
        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else
        {
            [demoedMediaFiles addObject:[imagesArray objectAtIndex:i]];
        }
        
//        if ([demoedMediaFiles containsObject:selectedFileID]) {
//
//        }
//        else
//        {
//            [demoedMediaFiles addObject:selectedMedia];
//
//        }
        
        
    }
    
    NSLog(@"demoed files are %@", [demoedMediaFiles description]);
    
    NSLog(@"pdf viewed in delegate are %@", [imagesArray description]);
    
    
}
-(void)htmlViewedinDemo:(NSMutableArray*)htmlArray
{
    for (NSInteger i=0; i<htmlArray.count; i++) {
        ProductMediaFile* selectedMedia=[htmlArray objectAtIndex:i];
        
        NSString* selectedFileID=[NSString getValidStringValue:selectedMedia.Media_File_ID] ;

        NSMutableArray *existingFileArray=[[demoedMediaFiles filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_File_ID == %@",selectedFileID]]mutableCopy];
        if (existingFileArray.count>0) {
            
        }
        else
        {
            [demoedMediaFiles addObject:[htmlArray objectAtIndex:i]];
        }
//        if ([demoedMediaFiles containsObject:selectedFileID]) {
//
//        }
//        else
//        {
//            [demoedMediaFiles addObject:selectedMedia];
//
//        }
        
        
    }
    
    NSLog(@"demoed files are %@", [demoedMediaFiles description]);
    
    NSLog(@"pdf viewed in delegate are %@", [htmlArray description]);
    
    
}

#pragma mark demoed products out of demo plan

-(void)demoedProductsOutOfDemoPlan:(NSMutableArray*)productsArray
{
    for (NSInteger i=0; i<productsArray.count; i++) {
        
        ProductMediaFile* selectedMedia=[productsArray objectAtIndex:i];
        
       // NSString* selectedFileID=[productsArray objectAtIndex:i] ;
        
        if ([demoedMediaFiles containsObject:selectedMedia.Media_File_ID]) {
            
        }
        else
        {
            [demoedMediaFiles addObject:selectedMedia];
        }
    }
    
    NSLog(@"demoed delegate data %@", productsArray);
}

#pragma mark string popover delegate

-(void)setSampleProductDetails:(NSString *)sampleProductId
{
    
    
    NSPredicate *ProductLotsPredicate = [NSPredicate
                                         predicateWithFormat:@"ProductId==%@",sampleProductId];
    NSMutableArray *tempVisitSamplesProductsArray=[VisitSamplesProductsArray mutableCopy];
    [tempVisitSamplesProductsArray filterUsingPredicate:ProductLotsPredicate];
    ProductLotsArray=[tempVisitSamplesProductsArray  mutableCopy];
    
    if (ProductLotsArray.count>0) {
        NSLog(@"product lots array is %@", [ProductLotsArray description]);
        
        SampleProductClass *selectedProduct=[ProductLotsArray objectAtIndex:0];
        selectedProductID=selectedProduct.ProductId;
        [self setSelectedProductLotDetails:selectedProduct.productLotNumber];

    }
    
    
}
-(void)setSelectedProductLotDetails:(NSString *)sampleProductLotNumber
{
    
    NSPredicate *ProductLotsPredicate = [NSPredicate
                                         predicateWithFormat:@"ProductId==%@ AND productLotNumber==%@",selectedProductID,sampleProductLotNumber];
    NSMutableArray *tempProductLotsArray=[ProductLotsArray mutableCopy];
    [tempProductLotsArray filterUsingPredicate:ProductLotsPredicate];
    
    if (tempProductLotsArray.count > 0) {
        SampleProductClass *selectedProduct=[tempProductLotsArray objectAtIndex:0];
        
        NSLog(@"selected product avbl qty %@", selectedProduct.productAvailbleQty);
        
        lotNumberTextField.text=selectedProduct.productLotNumber;
        selectedLotNumberforProductID=selectedProduct.productLotNumber;
        selectedProductID=selectedProduct.ProductId;
        
        NSInteger avblQty=[selectedProduct.productAvailbleQty integerValue];
        avblQtyLbl.text=[NSString stringWithFormat:@"%d",avblQty];
        productTextField.text=selectedProduct.productName;
        expiryDateLbl.text=selectedProduct.DisplayExpiryDate;
        
        if(QtyTextField.text.integerValue>selectedProduct.productAvailbleQty.integerValue)
            QtyTextField.text=selectedProduct.productAvailbleQty;
        else if (selectedProduct.QuantityEntered.intValue > 0)
            QtyTextField.text=selectedProduct.QuantityEntered;
        else if (selectedProduct.QuantityEntered.intValue == 0)
            QtyTextField.text=@"";

        if(!isTableViewSampleProductSelected)
        {
            [addButton setTitle:NSLocalizedString(@"Add", nil) forState:UIControlStateNormal];
        }
        else
        {
            [addButton setTitle:NSLocalizedString(@"Update", nil) forState:UIControlStateNormal];
        }
    }
}
-(void)selectedStringValue:(NSIndexPath *)indexPath

{
    if ([popString isEqualToString:@"Product"]) {
        
        avblQtyLbl.text=@"";
        QtyTextField.enabled=YES;
        addButton.enabled=YES;
        selectedIndexValue=indexPath.row;
        selectedLotNumberIndex=nil;
        NSString *productId=[ProductIdsArray objectAtIndex:indexPath.row];
        [self setSampleProductDetails:productId];
        
    }
    
    
    else if ([popString isEqualToString:@"QTY"])
    {
        
        
        QtyTextField.text=[NSString stringWithFormat:@"%@", [qtyArray objectAtIndex:indexPath.row]];
        NSInteger avblQtyVal=[avblQtyLbl.text integerValue];
        NSInteger enteredQty=[QtyTextField.text integerValue];
        
        if (enteredQty>avblQtyVal) {
            
            QtyTextField.text=@"";
            UIAlertView* qtyMisMatch=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Quantity Unavailable", nil) message:NSLocalizedString(@"Entered quantity cannot be more than available quanity", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [qtyMisMatch show];
            
        }
        
    }
    
    else
    {
        NSLog(@"Selected Lot Number %@", [[ProductLotsArray objectAtIndex:indexPath.row]valueForKey:@"productLotNumber"]);
        [self setSelectedProductLotDetails:[[ProductLotsArray objectAtIndex:indexPath.row]valueForKey:@"productLotNumber"]];
        
    }
    if (popOverController)
    {
        [popOverController dismissPopoverAnimated:YES];
        popOverController = nil;
        popOverController.delegate=nil;
    }
    locationPopOver.delegate=nil;
    locationPopOver=nil;
}


-(NSInteger)fetchLotQuantityforSelectedLotNumber:(NSIndexPath*)indexpath StockArray:(NSMutableArray*)stockArray

{
    NSInteger lotQty=0;
    NSInteger avblStockQty=0;
    
    NSInteger usedQty=0;
    
    avblStockQty= avblStockQty+[[NSString stringWithFormat:@"%@", [[empStockArray objectAtIndex:indexpath.row] valueForKey:@"Lot_Qty"]] integerValue];
    
    usedQty= usedQty+[[NSString stringWithFormat:@"%@", [[empStockArray objectAtIndex:indexpath.row] valueForKey:@"Used_Qty"]] integerValue];
    
    NSLog(@"qty is %d %d",avblStockQty,usedQty);
    
    lotQty=avblStockQty-usedQty;
    
    return lotQty;
    
    
    
    
}

//- (IBAction)qtyButtonTapped:(id)sender {

-(void)qtyDropDownTapped
{
    
    popOverController=nil;
    
    popString=@"QTY";
    
    
    
    
    qtyArray=[[NSMutableArray alloc]init];
    
    NSInteger avblStockQty=0;
    
    NSInteger usedQty=0;
    
    NSLog(@"avbl stock is %@", [empStockArray description]);
    
    
    if (empStockArray.count>0) {
        
        
        
        if (selectedLotNumberIndex) {
            
            avblStockQty= avblStockQty+[[NSString stringWithFormat:@"%@", [[empStockArray objectAtIndex:selectedLotNumberIndex.row] valueForKey:@"Lot_Qty"]] integerValue];
            
            usedQty= usedQty+[[NSString stringWithFormat:@"%@", [[empStockArray objectAtIndex:selectedLotNumberIndex.row] valueForKey:@"Used_Qty"]] integerValue];
            
        }
        
        else
        {
            avblStockQty= avblStockQty+[[NSString stringWithFormat:@"%@", [[empStockArray objectAtIndex:0] valueForKey:@"Lot_Qty"]] integerValue];
            
            usedQty= usedQty+[[NSString stringWithFormat:@"%@", [[empStockArray objectAtIndex:0] valueForKey:@"Used_Qty"]] integerValue];
        }
        
        
        //for (NSInteger i=0; i<empStockArray.count; i++) {
        
        
        
        
        
        NSLog(@"qty is %d %d",avblStockQty,usedQty);
        
        NSInteger finalQty=avblStockQty-usedQty;
        
        
        NSLog(@"final qty %d", finalQty);
        
        
        
        for (NSInteger i=1; i<=finalQty; i++) {
            [qtyArray addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
        
        
        MedRepEDetailingQuantityPickerViewController * popOverVC=[[MedRepEDetailingQuantityPickerViewController alloc]init];
        
        popOverVC.qtyArray=qtyArray;
        popOverVC.quantityPickerDelegate=self;
        
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        
        [filterPopOverController setPopoverContentSize:CGSizeMake(200, 300) animated:YES];
        
        
        
        
        popOverVC.qtyPopoverController=filterPopOverController;
        
        
        [filterPopOverController presentPopoverFromRect:QtyTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        
    }
    
    
    else
    {
        UIAlertView* noProductAlert=[[UIAlertView alloc]initWithTitle:@"Missing Data" message:@"Please select Product and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:
                                     nil];
        [noProductAlert show];
    }
    
    
    
    
}

-(void)selectedQuantity:(NSIndexPath*)indexPath
{
    // [qtyButton setTitle:[NSString stringWithFormat:@"%@",[qtyArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    QtyTextField.text=[NSString stringWithFormat:@"%@",[qtyArray objectAtIndex:indexPath.row]];
}


-(NSString*)stringByTrimmingLeadingWhitespace:(NSString*)string {
    NSInteger i = 0;
    
    while ((i < [string length])
           && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[string characterAtIndex:i]]) {
        i++;
    }
    return [string substringFromIndex:i];
}

- (IBAction)addProductButtontapped:(id)sender {
    
    
    [self.view endEditing:YES];
    
    if (QtyTextField.text.length==0 || lotNumberTextField.text.length==0 || productTextField.text.length==0 || avblQtyLbl.text.length==0) {
        
        
        NSString* missingString;
        
        if (avblQtyLbl.text.length==0|| QtyTextField.text.length==0)
            
        {
            missingString=@"Please add quantity and try again";
            
        }
        
        if (missingString.length>0  ) {
            
            UIAlertView* qtyAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(missingString, nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
            [qtyAlert show];
        }
        
        else{
            UIAlertView* qtyAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please enter all details", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [qtyAlert show];
        }
        
    }
    else if([appControl.ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode] && itemTypeTextField.text.length == 0){
        
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Missing Data", nil) andMessage:NSLocalizedString(@"Please select product type and try again", nil) withController:self];
    }
    else {
        
        NSPredicate *SampleGivenPredicate = [NSPredicate
                                             predicateWithFormat:@"ProductId==%@ AND productLotNumber==%@",selectedProductID,selectedLotNumberforProductID];
        NSMutableArray *tempVisitGivenSampleProductsArray=[VisitGivenSampleProductsArray mutableCopy];
        
        NSLog(@"isTableViewSampleProductSelected %d",isTableViewSampleProductSelected);
        //
        if(isTableViewSampleProductSelected==YES)
        {
            [tempVisitGivenSampleProductsArray removeObject:tableSelectedProductDetails];
        }
        
          NSInteger filterCount=  [tempVisitGivenSampleProductsArray filteredArrayUsingPredicate:SampleGivenPredicate].count;

            if(filterCount==0)
            {
                SampleProductClass *product=[VisitSamplesProductsArray filteredArrayUsingPredicate:SampleGivenPredicate].firstObject;
                product.QuantityEntered=QtyTextField.text;
                product.ItemType = itemTypeTextField.text;

                [tempVisitGivenSampleProductsArray addObject:product];
                VisitGivenSampleProductsArray=tempVisitGivenSampleProductsArray;
                [self clearTheFields];
                isTableViewSampleProductSelected=NO;

                [CancelButton setHidden:YES];
                [self.addProductsBtn setTitle:@"ADD" forState:UIControlStateNormal];
                tableSelectedProductDetails=nil;

                if([appControl.ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode])
                {
                    productTextField.userInteractionEnabled = NO;
                    lotNumberTextField.userInteractionEnabled = NO;
                    QtyTextField.userInteractionEnabled = NO;
                    productTextField.alpha = 0.7;
                    lotNumberTextField.alpha = 0.7;
                    QtyTextField.alpha = 0.7;
                }
            }
            else
            {
                UIAlertView* demoAlert=[[UIAlertView alloc]initWithTitle:@"Duplicate Sample" message:@"This product already exist in samples" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [demoAlert show];
                return;
            }

        
       // }
        
        
//        NSInteger filterCount=  [tempVisitGivenSampleProductsArray filteredArrayUsingPredicate:SampleGivenPredicate].count;
//
//        SampleProductClass *product=[VisitSamplesProductsArray filteredArrayUsingPredicate:SampleGivenPredicate].firstObject;
//        product.QuantityEntered=QtyTextField.text;
//
//        product.ItemType = itemTypeTextField.text;
//
//
//
//
//        if(filterCount==0)
//        {
//            [tempVisitGivenSampleProductsArray addObject:product];
//            VisitGivenSampleProductsArray=tempVisitGivenSampleProductsArray;
//            [self clearTheFields];
//            isTableViewSampleProductSelected=NO;
//
//            [CancelButton setHidden:YES];
//            [self.addProductsBtn setTitle:@"ADD" forState:UIControlStateNormal];
//            tableSelectedProductDetails=nil;
//
//            productTextField.userInteractionEnabled = NO;
//            lotNumberTextField.userInteractionEnabled = NO;
//            QtyTextField.userInteractionEnabled = NO;
//            productTextField.alpha = 0.7;
//            lotNumberTextField.alpha = 0.7;
//            QtyTextField.alpha = 0.7;
//        }
//        else
//        {
//            UIAlertView* demoAlert=[[UIAlertView alloc]initWithTitle:@"Duplicate Sample" message:@"This product already exist in samples" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [demoAlert show];
//            return;
//        }
        
        
        
        
        isTableViewSampleProductSelected=NO;
    }
    [addButton setTitle:@"ADD" forState:UIControlStateNormal];
    
}

//- (IBAction)lotNumberDropDownTapped:(id)sender {
-(void)productsTextFieldTapped{
    
    popOverController=nil;
    popString=@"PRODUCT_MEDIA";
    
    NSPredicate* distinctPredicate=[NSPredicate predicateForDistinctWithProperty:@"Product_Name"];
    distinctMediaArray = [[unfilteredMediaArray filteredArrayUsingPredicate:distinctPredicate]mutableCopy];
   //unfilteredMediaArray = distinctProductsArray;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Product_Name" ascending:YES];
     NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     distinctMediaArray = [[distinctMediaArray sortedArrayUsingDescriptors:sortDescriptors]mutableCopy];

    [self presentPopoverfor:productMediaTextField withTitle:@"Product Name" withContent:[distinctMediaArray valueForKey:@"Product_Name"]];
    
}
-(void)lotNumberTextFieldTapped
{
    popOverController=nil;
    popString=@"LOT";
    
    [self presentPopoverfor:lotNumberTextField withTitle:kFieldMarketingEdetailingLotNumberTitle withContent:[ProductLotsArray valueForKey:@"productLotNumber"]];
    
    
    //    locationPopOver = nil;
    //    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    //
    //
    //    locationPopOver.colorNames = [ProductLotsArray valueForKey:@"productLotNumber"];
    //
    //    locationPopOver.delegate = self;
    //
    //
    //    CGRect relativeFrame = [lotNumberTextField convertRect:lotNumberTextField.bounds toView:self.view];
    //
    //    if (popOverController == nil) {
    //        //The color picker popover is not showing. Show it.
    //        popOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
    //        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //        popOverController.delegate=self;
    //
    //    }
    //    else {
    //        //The color picker popover is showing. Hide it.
    //        [popOverController dismissPopoverAnimated:YES];
    //        popOverController = nil;
    //        popOverController.delegate=nil;
    //    }
    
    
    
}

#pragma mark popover methods

-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    NSLog(@"content array being passed %@",contentArray);
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=NO;
    if ([popOverString isEqualToString:kDoctorTitle]) {
        popOverVC.isDoctorFilter=YES;
    }
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    multiCallsPopOverController=nil;
    multiCallsPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    multiCallsPopOverController.delegate=self;
    popOverVC.popOverController=multiCallsPopOverController;
    popOverVC.titleKey=popOverString;
    popOverVC.popOverSize = CGSizeMake(400, 273);
    [multiCallsPopOverController setPopoverContentSize:CGSizeMake(400, 273) animated:YES];
    [multiCallsPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSInteger  currentProductIndex= selectedIndexPath.row;
    
    
    if ([popString isEqualToString:@"Product"]) {
        avblQtyLbl.text=@"";
        QtyTextField.enabled=YES;
        addButton.enabled=YES;
        selectedIndexValue=currentProductIndex;
        selectedLotNumberIndex=nil;
        NSString *productId=[ProductIdsArray objectAtIndex:currentProductIndex];
        [self setSampleProductDetails:productId];
    }
    else if ([popString isEqualToString:@"LOT"])
    {
        NSLog(@"Selected Lot Number %@", [[ProductLotsArray objectAtIndex:currentProductIndex]valueForKey:@"productLotNumber"]);
        
        [self setSelectedProductLotDetails:[[ProductLotsArray objectAtIndex:currentProductIndex]valueForKey:@"productLotNumber"]];
    }
    else if ([popString isEqualToString:@"Product Type"])
    {
        itemTypeTextField.text = [NSString getValidStringValue:[ItemTypeArray objectAtIndex:currentProductIndex]];
        
        sampleProductsArray = [MedRepQueries fetchSamplesforEdetailing:[NSString getValidStringValue:[ItemCodeValueArray objectAtIndex:currentProductIndex]]];
        
        if (sampleProductsArray.count==0) {
            
            productTextField.userInteractionEnabled = NO;
            lotNumberTextField.userInteractionEnabled = NO;
            QtyTextField.userInteractionEnabled = NO;
            productTextField.alpha = 0.7;
            lotNumberTextField.alpha = 0.7;
            QtyTextField.alpha = 0.7;
            [SWDefaults showAlertAfterHidingKeyBoard:@"Products" andMessage:@"No product for selected item type" withController:self];
        }
        else
        {
            productTextField.userInteractionEnabled = YES;
            lotNumberTextField.userInteractionEnabled = YES;
            QtyTextField.userInteractionEnabled = YES;
            
            productTextField.alpha = 1.0;
            lotNumberTextField.alpha = 1.0;
            QtyTextField.alpha = 1.0;
            
            VisitSamplesProductsArray=[self ConvertSampleProductsDataToObjects:sampleProductsArray];
            if (VisitSamplesProductsArray.count==1) {
                avblQtyLbl.text=@"";
                QtyTextField.enabled=YES;
                addButton.enabled=YES;
                selectedLotNumberIndex=nil;
                SampleProductClass *product=[[SampleProductClass alloc]init];
                product=[VisitSamplesProductsArray objectAtIndex:0];
                NSString *productId=product.ProductId;
                [self setSampleProductDetails:productId];
            }
        }
        QtyTextField.text=@"";
        lotNumberTextField.text=@"";
        avblQtyLbl.text=@"";
        expiryDateLbl.text=@"";
        productTextField.text=@"";
    }else if ([popString isEqualToString:@"PRODUCT_MEDIA"]){
        NSString* selectedProductID = [NSString getValidStringValue:[[distinctMediaArray objectAtIndex:currentProductIndex]valueForKey:@"Entity_ID_1"]];
        NSString* selectedProductName = @"";
       // NSLog(@"selected product id: %@\n product name: %@\n All products %@", selectedProductID,selectedProductName,[unfilteredMediaArray valueForKey:@"Product_Name"]);
        NSMutableArray * filteredArray = [[NSMutableArray alloc]init];
        if([selectedProductID isEqualToString:@"1"]){
            //all products selected.
            filteredArray = unfilteredMediaArray;
        }else{
        filteredArray = [[unfilteredMediaArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Entity_ID_1 = %@",selectedProductID]]mutableCopy];
        //filteredArray = collectionViewDataArray;

        }
        
        if(filteredArray.count>0){
            
            selectedProductName = [NSString getValidStringValue:[[filteredArray objectAtIndex:0]valueForKey:@"Product_Name"]];
            NSLog(@"selected product name %@",selectedProductName);

            productMediaTextField.text = selectedProductName;


        productImagesArray=[[filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
        productVideoFilesArray= [[filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
        
        productPdfFilesArray=[[filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
        
        presentationsArray=[[filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
        
        
        
        if (productImagesArray.count==0) {
            [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
        }
        else
        {
            [imagesButton setBackgroundImage:[UIImage imageNamed:@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
        }
        if (productVideoFilesArray.count==0) {
            [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
        }
        else
        {
            [videoButton setBackgroundImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
        }
        if (productPdfFilesArray.count==0) {
            [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
        }
        
        else
        {
            [pptButton setBackgroundImage:[UIImage imageNamed:@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
        }
        
        if (presentationsArray.count==0) {
            [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
        }
        
        else
        {
            [btnPresentation setBackgroundImage:[UIImage imageNamed:@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
        }
        
        [btnProduct setBackgroundImage:[UIImage imageNamed:@"MedRep_AllMediaBtnActive"] forState:UIControlStateNormal];
        btnProduct.layer.borderWidth=0.0;
        [self imagesButtonTapped:nil];
        }
    }
}

#pragma mark UITextfiled delegate methods


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if([newString length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    if([newString isEqualToString:@" "])
    {
        return NO;
        
    }
    
    if(textField==QtyTextField)
    {
        NSInteger EnteredQuantity=newString.integerValue;
        NSInteger availableQty=avblQtyLbl.text.integerValue;
        
        NSString *expression = @"^[0-9]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return ((numberOfMatches != 0 && EnteredQuantity<=availableQty));
    }
    
    return YES;
}
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    textField.text = [textField.text stringByTrimmingCharactersInSet:
//                      [NSCharacterSet whitespaceCharacterSet]];
//    if(textField==currentMileageTextField)
//    {
//        if([appDelegate.jobCardDetails.lastSrMileage doubleValue]>=[currentMileageTextField.text doubleValue] )
//        {
//            [self showAlertViewWithTitle:KInvalidDataAlertTitle withMessage:[NSString stringWithFormat:@"Current mileage should be greater than last service mileage - %@",appDelegate.jobCardDetails.lastSrMileage]];
//        }
//        appDelegate.jobCardDetails.latestnewMileage=textField.text;
//    }
//}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    [self animateTextField:textField up:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    NSLog(@"samples selected in did select row %@", [selectedSamplesDictionary description]);
    
    
    //
    //    if ([avblQtyLbl.text isEqualToString:@"N/A"]) {
    //
    //        for (NSInteger i=0; i<sampleProductsArray.count; i++) {
    //            NSMutableDictionary* tempDic=[sampleProductsArray  objectAtIndex:i];
    //            if ([[tempDic valueForKey:@"Product_ID"] isEqualToString:selectedProductID]) {
    //                [tempDic setObject:textField.text forKey:@"QTY"];
    //                [tempDic setObject:avblQtyLbl.text forKey:@"LOT_QTY"];
    //                [tempDic setObject:lotNumberTextField.text forKey:@"LOT_No"];
    //                [tempDic setObject:expiryDateLbl.text forKey:@"Expiry_Date"];
    //                [sampleProductsArray replaceObjectAtIndex:i withObject:tempDic];
    //            }
    //
    //        }
    //    }
    //
    //
    //    else
    //    {
    //
    //    if (textField ==qtyTxtFld) {
    //
    //
    //
    //
    //    }
    //
    //    else
    //    {
    //
    //
    //
    //    for (NSInteger i=0; i<sampleProductsArray.count; i++) {
    //        NSMutableDictionary* tempDic=[sampleProductsArray  objectAtIndex:i];
    //        if ([[tempDic valueForKey:@"Product_ID"] isEqualToString:selectedProductID]) {
    //            [tempDic setObject:textField.text forKey:@"QTY"];
    //            [tempDic setObject:avblQtyLbl.text forKey:@"LOT_QTY"];
    //            [tempDic setObject:lotNumberTextField.text forKey:@"LOT_No"];
    //            [tempDic setObject:expiryDateLbl.text forKey:@"Expiry_Date"];
    //            [sampleProductsArray replaceObjectAtIndex:i withObject:tempDic];
    //        }
    //
    //    }
    //
    //    }
    //
    //
    //
    //    }
    //
    //    //[selectedSamplesDictionary setValue:textField.text forKey:@"Quantity"];
    
    [self animateTextField:textField up:NO];
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 350; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark UITableView Methods

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 35;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([appControl.ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode]){
        return 187;
    } else {
        return 128;
    }
}


//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//
//    static NSString *datacellIdentifier = @"samplesHeader";
//
//    SamplesGivenHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
//
//    if(cell == nil) {
//        cell = [[[NSBundle mainBundle]loadNibNamed:@"SamplesGivenHeaderTableViewCell" owner:nil options:nil] firstObject];
//        //cell.contentView.backgroundColor=[UIColor colorWithRed:(239.0/255.0) green:(242.0/255.0) blue:(248.0/255.0) alpha:1];
//
//        //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
//    }
//
//    return cell.contentView;
//
//
////
////    UIView * headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, samplesTblView.frame.size.width, 45)];
////    headerView.backgroundColor=[UIColor colorWithRed:(234.0/255.0) green:(237.0/255.0) blue:(242.0/255.0) alpha:1];
////
////    UILabel* headerLbl=[[UILabel alloc]initWithFrame:CGRectMake(14, 12, 359, 21)];
////    headerLbl.font=bodySemiBold;
////    headerLbl.textColor=[UIColor colorWithRed:(76.0/255.0) green:(82.0/255.0) blue:(100.0/255.0) alpha:1];
////    headerLbl.text=@"Samples Given";
////
////    [headerView addSubview:headerLbl];
////       return headerView;
//
//
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return VisitGivenSampleProductsArray.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"samplesCell";
    
    
    SamplesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* bundleArray=[[NSBundle mainBundle]loadNibNamed:@"SamplesTableViewCell" owner:self options:nil];
        
        if([appControl.ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode]){
            cell=[bundleArray objectAtIndex:1];
        } else {
            cell=[bundleArray objectAtIndex:0];
        }
    }
    
    tableView.allowsMultipleSelectionDuringEditing = NO;
    
    SampleProductClass *tempProduct=[VisitGivenSampleProductsArray objectAtIndex:indexPath.row];
    
    cell.expiryLbl.text=tempProduct.DisplayExpiryDate == nil ? @"N/A" : tempProduct.DisplayExpiryDate;
    cell.lotNumberLbl.text=tempProduct.productLotNumber;
    cell.titleLbl.text= tempProduct.productName;
    cell.qtyLbl.text=tempProduct.QuantityEntered;
    
    if([appControl.ENABLE_ITEM_TYPE isEqualToString:KAppControlsYESCode]){
        cell.itemTypeLbl.text = tempProduct.ItemType;
    }
    
    return cell;
    
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [VisitGivenSampleProductsArray removeObjectAtIndex:indexPath.row];
    [CancelButton setHidden:YES];
    [self clearTheFields];
    [addButton setTitle:@"ADD" forState:UIControlStateNormal];
    isTableViewSampleProductSelected=NO;
    [samplesTblView reloadData];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    productTextField.userInteractionEnabled = YES;
    lotNumberTextField.userInteractionEnabled = YES;
    QtyTextField.userInteractionEnabled = YES;
    
    productTextField.alpha = 1.0;
    lotNumberTextField.alpha = 1.0;
    QtyTextField.alpha = 1.0;
    
    [productButton setTitle:@"" forState:UIControlStateNormal];
    [CancelButton setHidden:NO];
    isTableViewSampleProductSelected=YES;
    
    SampleProductClass *product=[VisitGivenSampleProductsArray objectAtIndex:indexPath.row];
    [self setSampleProductDetails:product.ProductId];
    [self setSelectedProductLotDetails:product.productLotNumber];
    QtyTextField.text=product.QuantityEntered;
    itemTypeTextField.text=product.ItemType;
    tableSelectedProductDetails=product;
}


-(IBAction)cancelButtonTapped:(id)sender
{
    [CancelButton setHidden:YES];
    [self clearTheFields];
    [addButton setTitle:@"ADD" forState:UIControlStateNormal];
    isTableViewSampleProductSelected=NO;
    [samplesTblView reloadData];
}

#pragma stock deduction methods


//Requirement : allow user to give sample only if entered qty is less that avbl qty for the selected lot , and update the Used_Qty column in TBL_Emp_Stock where UsedQty=UsedQty+givenQty and Lot Qty=LotQty-givenqty

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==productTextField)
    {
        [self productDropDownTapped];
        return NO;
    }
    else if(textField==lotNumberTextField)
    {
        [self lotNumberTextFieldTapped];
        return NO;
        
    }
    else if(textField==productMediaTextField)
    {
        [self productsTextFieldTapped];
        return NO;
        
    }
    else if(textField==QtyTextField)
    {
        //[self qtyDropDownTapped];
        return YES;
    }
    if(textField==itemTypeTextField)
    {
        [self ItemTypeDropDownTapped];
        return NO;
    }
    
    return YES;
}



-(NSMutableArray *)ConvertSampleProductsDataToObjects:(NSMutableArray *)sampleProducts
{
    ProductIdsArray=[[NSMutableArray alloc]init];
    ProductNamesArray=[[NSMutableArray alloc]init];
    NSMutableArray *ProductsArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<sampleProducts.count; i++) {
        NSDictionary *tempProduct=[sampleProducts objectAtIndex:i];
        SampleProductClass *product=[[SampleProductClass alloc]init];
        product.Product_Code=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Product_Code"]];
        product.productName=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Product_Name"]];
        product.QuantityEntered=@"0";
        product.ExpiryDate=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Expiry_Date"]];
        product.Used_Qty=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Used_Qty"]];
        NSString *lotQty=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Lot_Qty"]];
        NSString *usedQuantity=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Used_Qty"]];
        product.ProductId=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Product_ID"]];
        
        
        //qty already updated while updating stock no need to do lot-used here
        
        product.productAvailbleQty=[NSString stringWithFormat:@"%d",[lotQty integerValue]-[usedQuantity integerValue]];
        
        //product.productAvailbleQty=[NSString stringWithFormat:@"%@", lotQty];
        
        product.productLotNumber=[MedRepDefaults getValidStringValue:[tempProduct valueForKey:@"Lot_No"]];
        product.DisplayExpiryDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:product.ExpiryDate];
        
        
        
        
        if([product.productAvailbleQty integerValue]>0)
        {
            if(![ProductIdsArray containsObject:product.ProductId])
            {
                [ProductIdsArray addObject:product.ProductId];
                [ProductNamesArray addObject:product.productName];
                
            }
            [ProductsArray addObject:product];
            
        }
    }
    
    
    return ProductsArray;
    
}


-(void)clearTheFields
{
    selectedProductID=@"";
    selectedLotNumberforProductID=@"";
    ProductLotsArray=[[NSMutableArray alloc]init];
    QtyTextField.text=@"";
    lotNumberTextField.text=@"";
    avblQtyLbl.text=@"";
    expiryDateLbl.text=@"";
    productTextField.text=@"";
    itemTypeTextField.text=@"";
    [samplesTblView reloadData];
    
}



#pragma Mark Task popover controller delegate

-(void)popOverControlDidClose:(NSMutableArray*)tasksArray
{
    self.visitDetails.taskArray=tasksArray;
    appDelegate.CurrentRunningMedRepVisitDetails=self.visitDetails;
    NSLog(@"task array in popover did close in start call %@", self.visitDetails.taskArray);
    [self refreshTaskandNotesStatus:self.visitDetails];
    
}

-(void)notesDidClose:(NSMutableArray*)updatedNotes;

{
    self.visitDetails.notesArray=updatedNotes;
    appDelegate.CurrentRunningMedRepVisitDetails=self.visitDetails;
    if (self.visitDetails.notesArray.count>0) {
        //[self updateStatusforTasksandNotes];
        [self refreshTaskandNotesStatus:self.visitDetails];
    }
}

-(void)refreshTaskandNotesStatus:(VisitDetails*)currentVisitDetails
{
    NSMutableDictionary * statusCodesDict=[MedRepQueries fetchStatusCodeForTaskandNotes:currentVisitDetails];
    if ([[statusCodesDict valueForKey:KNOTESSTATUSKEY] isEqualToString:KNOTESAVAILABLE]) {
        [notesStatusImageView setBackgroundColor:KNOTESAVAILABLECOLOR];
    }
    else{
        [notesStatusImageView setBackgroundColor:KNOTESUNAVAILABLECOLOR];
    }
    if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKCLOSED])
    {
        [taskStatusImageView setBackgroundColor:KTASKCOMPLETEDCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKOVERDUE])
    {
        [taskStatusImageView setBackgroundColor:KTASKOVERDUECOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKPENDING]) {
        
        [taskStatusImageView setBackgroundColor:KTASKPENDINGCOLOR];
    }
    else if ([[statusCodesDict valueForKey:KTASKSTATUSKEY] isEqualToString:KTASKUNAVAILABLE]) {
        
        [taskStatusImageView setBackgroundColor:KTASKUNAVAILABLECOLOR];
    }
    
}

- (IBAction)productsButtonTapped:(id)sender {
    
    //
    //    [imagesButton setSelected:NO];
    //    [pptButton setSelected:NO];
    //    [videoButton setSelected:NO];
    //    [btnProduct setSelected:YES];
    //    [btnPresentation setSelected:NO];
    //
    //    [customPresentationsButton setSelected:NO];
    //
    //    if ([btnProduct isSelected]) {
    //
    //        btnProduct .layer.borderWidth=2.0;
    //        btnProduct.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    //    }
    //    imagesButton.layer.borderWidth=0.0;
    //    videoButton.layer.borderWidth=0.0;
    //    pptButton.layer.borderWidth=0.0;
    //    btnPresentation.layer.borderWidth=0.0f;
    //    customPresentationsButton.layer.borderWidth=0.0f;
    
    MedRepEDetailingProductsViewController * edetailingProducts= [[MedRepEDetailingProductsViewController alloc]init];
    edetailingProducts.delegate=self;
    UINavigationController * mediaNavController=[[UINavigationController alloc]initWithRootViewController:edetailingProducts];
    [self.navigationController presentViewController:mediaNavController animated:YES completion:nil];
}

- (IBAction)btnPresentationTapped:(id)sender {
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnProduct setSelected:NO];
    [btnPresentation setSelected:YES];
    [customPresentationsButton setSelected:NO];
    
    if ([btnPresentation isSelected]) {
        
        btnPresentation .layer.borderWidth=2.0;
        btnPresentation.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnProduct.layer.borderWidth=0.0f;
    customPresentationsButton.layer.borderWidth=0.0f;
    collectionViewDataArray=[[NSMutableArray alloc]init];
    collectionViewDataArray=presentationsArray;
    
    
    [demoPlanProductsCollectionView reloadData];
    
}

//- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    if (popoverController==popOverController) {
//        return NO;
//
//    }
//    else
//
//        return YES;
//}



- (IBAction)customPresentationsButtontapped:(id)sender {
    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [btnProduct setSelected:NO];
    [btnPresentation setSelected:NO];
    [customPresentationsButton setSelected:YES];
    
    if ([customPresentationsButton isSelected]) {
        
        customPresentationsButton .layer.borderWidth=2.0;
        customPresentationsButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    btnProduct.layer.borderWidth=0.0f;
    btnPresentation.layer.borderWidth=0.0f;
    
    collectionViewDataArray=[[NSMutableArray alloc]init];
    collectionViewDataArray=self.visitDetails.fieldMarketingPresentationsArray;
    //NSLog(@"media file id in button tap %@", [collectionViewDataArray valueForKey:@"Media_File_ID"]);
    
    [demoPlanProductsCollectionView reloadData];
}
@end

