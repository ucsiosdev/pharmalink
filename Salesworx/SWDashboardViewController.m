//
//  SWDashboardViewController.m
//  SWDashboard
//
//  Created by Irfan Bashir on 5/6/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDashboardViewController.h"
//#import "SWCustomer/SWCustomer.h"
#import "MedRepDefaults.h"
#import "SWCustomersViewController.h"
#import "MedRepMenuViewController.h"
#import "AppControl.h"
#import "SalesWorxToDoPopUpViewController.h"
@interface SWDashboardViewController ()
{
    UIViewController *sortViewController;
    UILabel *alle;
    UIButton *notificationButton;
}
@end

@implementation SWDashboardViewController

@synthesize delegate = _delegate;
#define kMovetKey @"moveViewAnimation"



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}





- (void)viewDidLoad{
    [super viewDidLoad];
    //[self.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    isFirst =YES;
    appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    sortViewController=nil;
    sortViewController = [[UIViewController alloc] init];
    sortViewController.title = NSLocalizedString(@"Select ", nil);
    
    
   
    
    
    sortPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
    [sortViewController.view addSubview:sortPickerView];
    sortViewController.contentSizeForViewInPopover = CGSizeMake(320, 216);
    sortPickerView.delegate = self;
    sortPickerView.dataSource = self;
    sortPickerView.showsSelectionIndicator = YES;
    //customerSer.delegate = self;
    
    
    
   
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Field Sales Dashboard", nil)];
    

    
      
   
   // [self getData];
    customerDict = [NSMutableDictionary dictionary];
    customerArray = [NSMutableArray array];
    if([SWDefaults isRTL])
    {
        mainImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_Dashboard_bg_AR"cache:NO]];
    }
    else 
    {
//        if (isVisitAvailble) {
//            mainImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_Dashboard_bg"cache:NO]];
//        }
//        else
//        {
//             mainImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Dashboard_disable.png"cache:NO]];
//        }
        mainImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_Dashboard_bg"cache:NO]];

       
    }
    mainImage.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [mainImage setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:mainImage];
    
    
    
    plannedLabel=[[UILabel alloc] initWithFrame:CGRectMake(35, 60, 170, 100)];
    [plannedLabel setText:@""];
    [plannedLabel setTextAlignment:NSTextAlignmentCenter];
    [plannedLabel setBackgroundColor:[UIColor clearColor]];
    [plannedLabel setTextColor:[UIColor whiteColor]];
    [plannedLabel setFont:BoldFontOfSize(50.0)];
    [self.view addSubview:plannedLabel];

    completedLabel=[[UILabel alloc] initWithFrame:CGRectMake(240, 60, 170, 100)]  ;
    [completedLabel setText:@""];
    [completedLabel setTextAlignment:NSTextAlignmentCenter];
    [completedLabel setBackgroundColor:[UIColor clearColor]];
    [completedLabel setTextColor:[UIColor whiteColor]];
    [completedLabel setFont:BoldFontOfSize(50.0)];
    [self.view addSubview:completedLabel];
    
    outOfRouteLabel=[[UILabel alloc] initWithFrame:CGRectMake(440, 60, 170, 100)]  ;
    [outOfRouteLabel setText:@""];
    [outOfRouteLabel setTextAlignment:NSTextAlignmentCenter];
    [outOfRouteLabel setBackgroundColor:[UIColor clearColor]];
    [outOfRouteLabel setTextColor:[UIColor whiteColor]];
    [outOfRouteLabel setFont:BoldFontOfSize(50.0)];
    [self.view addSubview:outOfRouteLabel];
    
    
    notificationButton = [[UIButton alloc] init]  ;
    notificationButton:[UIButton buttonWithType:UIButtonTypeCustom];
    [notificationButton addTarget:self action:@selector(showInbox) forControlEvents:UIControlEventTouchUpInside];
    notificationButton.backgroundColor = [UIColor clearColor];
    notificationButton.frame = CGRectMake(975, 0, 45    , 45);
    [notificationButton setBackgroundImage:[UIImage imageNamed:@"Old_mail_notification"] forState:UIControlStateNormal];
    

    
    alle = [[UILabel alloc] initWithFrame:CGRectMake(999.5,5, 20, 20)];
    
    if([[AppControl retrieveSingleton].MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        alle.text=[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select COUNT(*) from (select A.Sender_Name,A.Msg_ID,A.Sender_ID,A.Msg_Title,A.Msg_Body,A.Logged_At AS Sent_at,A.Expires_At,B.Read_At, B.Recipient_ID,B.Reply_Msg_ID from tbl_msg AS A INNER join TBL_Msg_Recipients AS B ON A.msg_id=B.msg_id where A.Sender_ID!='%@' AND  A.Expires_At>date('now') order by A.Logged_At desc) As C  Left join tbl_msg AS D on C.Reply_Msg_ID=D.msg_id Where C.Read_At is NULL",[NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]]]]objectAtIndex:0]stringForKey:@"COUNT(*)"];

    }
    else
    {
        alle.text = [[[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select COUNT(*) from TBL_FSR_Messages where Message_Read='N' "] objectAtIndex:0] stringForKey:@"COUNT(*)"];

    }
    
    
    
    alle.backgroundColor = [UIColor clearColor];
    alle.textColor=[UIColor whiteColor];
    alle.font=BoldFontOfSize(12);
    alle.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:notificationButton];
    [self.view addSubview:alle];


    
    selectCustomerButton = [[UIButton alloc] init]  ;
    selectCustomerButton:[UIButton buttonWithType:UIButtonTypeCustom];
    [selectCustomerButton addTarget:self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
    selectCustomerButton.backgroundColor = [UIColor clearColor];
    selectCustomerButton.frame = CGRectMake(670, 63, 330, 35);
    [self.view addSubview:selectCustomerButton];
    
    detailCustomerButton = [[UIButton alloc] init]  ;
    detailCustomerButton:[UIButton buttonWithType:UIButtonTypeCustom];
    [detailCustomerButton addTarget:self action:@selector(showDetail:) forControlEvents:UIControlEventTouchUpInside];
    //[detailCustomerButton setTitle: NSLocalizedString(@"Details", nil)forState:UIControlStateNormal];
    //[detailCustomerButton setBackgroundImage:[UIImage imageNamed:@"Old_green_button" cache:NO] forState:UIControlStateNormal];

    detailCustomerButton.backgroundColor = [UIColor clearColor];
    detailCustomerButton.frame = CGRectMake(945, 145, 50, 50);
    [self.view addSubview:detailCustomerButton];
    

    customerNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(678, 63, 290, 35)];
    [customerNameLabel setText:@""];
    [customerNameLabel setTextAlignment:NSTextAlignmentLeft];
    [customerNameLabel setBackgroundColor:[UIColor clearColor]];
    [customerNameLabel setTextColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]];
    [customerNameLabel setFont:BoldSemiFontOfSize(18.0)];
    [self.view addSubview:customerNameLabel];

    customerStatusLabel=[[UILabel alloc] initWithFrame:CGRectMake(795, 109, 160, 25)] ;
    [customerStatusLabel setText:@""];
    [customerStatusLabel setTextAlignment:NSTextAlignmentLeft];
    [customerStatusLabel setBackgroundColor:[UIColor clearColor]];
    [customerStatusLabel setTextColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]];
    [customerStatusLabel setFont:RegularFontOfSize(20.0)];
    [self.view addSubview:customerStatusLabel];
    
    
    if([SWDefaults isRTL])
    {
        customerOutstandingLabel=[[UILabel alloc] initWithFrame:CGRectMake(840, 138, 150, 25)]  ;
    }
    else
    {
        customerOutstandingLabel=[[UILabel alloc] initWithFrame:CGRectMake(805, 138, 150, 25)]  ;
    }
    
    [customerOutstandingLabel setText:@""];
    [customerOutstandingLabel setTextAlignment:NSTextAlignmentLeft];
    [customerOutstandingLabel setBackgroundColor:[UIColor clearColor]];
    [customerOutstandingLabel setTextColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]];
    [customerOutstandingLabel setFont:RegularFontOfSize(20.0)];
    [self.view addSubview:customerOutstandingLabel];
    
    if([SWDefaults isRTL])
    {
        customerSalesLabel=[[UILabel alloc] initWithFrame:CGRectMake(770, 166, 160, 25)]  ;
    }
    else
    {
        customerSalesLabel=[[UILabel alloc] initWithFrame:CGRectMake(752, 166, 160, 25)]  ;
    }
    [customerSalesLabel setText:@""];
    [customerSalesLabel setTextAlignment:NSTextAlignmentLeft];
    [customerSalesLabel setBackgroundColor:[UIColor clearColor]];
    [customerSalesLabel setTextColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]];
    [customerSalesLabel setFont:RegularFontOfSize(20.0)];
    [self.view addSubview:customerSalesLabel];
    
    
    
    orderLabel=[[UILabel alloc] initWithFrame:CGRectMake(35, 550, 170, 100)]  ;
    [orderLabel setText:@""];
    [orderLabel setTextAlignment:NSTextAlignmentCenter];
    [orderLabel setBackgroundColor:[UIColor clearColor]];
    [orderLabel setTextColor:[UIColor whiteColor]];
    [orderLabel setFont:BoldFontOfSize(50.0)];
    [self.view addSubview:orderLabel];

    
    returnLabel=[[UILabel alloc] initWithFrame:CGRectMake(240, 550, 170, 100)]  ;
    [returnLabel setText:@""];
    [returnLabel setTextAlignment:NSTextAlignmentCenter];
    [returnLabel setBackgroundColor:[UIColor clearColor]];
    [returnLabel setTextColor:[UIColor whiteColor]];
    [returnLabel setFont:BoldFontOfSize(50.0)];
    [self.view addSubview:returnLabel];

    
    collectionLabel=[[UILabel alloc] initWithFrame:CGRectMake(440, 550, 170, 100)]  ;
    [collectionLabel setText:@""];
    [collectionLabel setTextAlignment:NSTextAlignmentCenter];
    [collectionLabel setBackgroundColor:[UIColor clearColor]];
    [collectionLabel setTextColor:[UIColor whiteColor]];
    [collectionLabel setFont:BoldFontOfSize(50.0)];
    [self.view addSubview:collectionLabel];
    
    syncDateLabel=[[UILabel alloc] initWithFrame:CGRectMake(672, 580, 300, 30)]  ;
    [syncDateLabel setTextAlignment:NSTextAlignmentLeft];
    [syncDateLabel setBackgroundColor:[UIColor clearColor]];
    [syncDateLabel setTextColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]];
    [syncDateLabel setFont:BoldFontOfSize(20.0)];
    [self.view addSubview:syncDateLabel];
    
    
    syncStatusLabel=[[UILabel alloc] initWithFrame:CGRectMake(672, 640, 300, 30)]  ;
    [syncStatusLabel setTextAlignment:NSTextAlignmentLeft];
    [syncStatusLabel setBackgroundColor:[UIColor clearColor]];
    [syncStatusLabel setTextColor:[UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]];
    [syncStatusLabel setFont:BoldFontOfSize(20.0)];
    [self.view addSubview:syncStatusLabel];
    
    monthLabel=[[UILabel alloc] initWithFrame:CGRectMake(770, 217, 220, 32)]  ;
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MMMM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    [monthLabel setText:dateString];
    [monthLabel setTextAlignment:NSTextAlignmentCenter];
    [monthLabel setBackgroundColor:[UIColor clearColor]];
    [monthLabel setTextColor:[UIColor grayColor]];
    [monthLabel setFont:BoldFontOfSize(22.0)];
    [self.view addSubview:monthLabel];
    
    formatter=nil;
    usLocale=nil;
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:@"UPDATEDASHBOARDUINOTIFICATIONNAME"
                                             object:nil];
    
    
    
    
    //customer button
    
     customerDetailsButton=[[UIButton alloc]initWithFrame:CGRectMake(948, 144, 42, 42)];
    [customerDetailsButton addTarget:self action:@selector(showDetail:) forControlEvents:UIControlEventTouchUpInside];
    customerDetailsButton.tag=100;
    [customerDetailsButton setBackgroundImage:[UIImage imageNamed:@"Old_DashboardCustomerDetails"] forState:UIControlStateNormal];
    [self.view addSubview:customerDetailsButton];
    
}

-(void)showInbox
{
    CommunicationViewControllerNew * obj = [[CommunicationViewControllerNew alloc]initWithNibName:@"CommunicationViewControllerNew" bundle:nil] ;
    [self.navigationController pushViewController:obj animated:YES];
}

-(void)updateView
{
    [syncStatusLabel setText:[SWDefaults lastSyncStatus]];
    [syncDateLabel setText:[SWDefaults lastSyncDate]];
    [SWDefaults clearCustomer];
    
    
    [sortPickerView selectRow:0 inComponent:0 animated:YES];
    [sortPickerView reloadAllComponents];
    
    NSLog(@"customer array in view will appear %@", [customerArray description]);
    
    //[self getData];
    NSLog(@"customer dict is %@", [customerDict description]);
    
    if (self.isMovingToParentViewController) {
        [self getData];
        //[self navigatetodeals];
        
        customerNameLabel.text = [customerDict stringForKey:@"Customer_Name"];
        
    }
    else
    {
    }
    
    
    //[self.navigationController setToolbarHidden:YES animated:YES];
    
    [Flurry logEvent:@"Dashboard View"];
    // [Crittercism leaveBreadcrumb:@"<Dashboard View>"];
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    if([[AppControl retrieveSingleton].MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        alle.text=[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select COUNT(*) from (select A.Sender_Name,A.Msg_ID,A.Sender_ID,A.Msg_Title,A.Msg_Body,A.Logged_At AS Sent_at,A.Expires_At,B.Read_At, B.Recipient_ID,B.Reply_Msg_ID from tbl_msg AS A INNER join TBL_Msg_Recipients AS B ON A.msg_id=B.msg_id where A.Sender_ID!='%@' AND  A.Expires_At>date('now') order by A.Logged_At desc) As C  Left join tbl_msg AS D on C.Reply_Msg_ID=D.msg_id Where C.Read_At is NULL",[NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]]]]objectAtIndex:0]stringForKey:@"COUNT(*)"];
        
    }
    else
    {
        alle.text = [[[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select COUNT(*) from TBL_FSR_Messages where Message_Read='N' "] objectAtIndex:0] stringForKey:@"COUNT(*)"];
        
    }

    if ([alle.text isEqualToString:@"0"])
    {
        
        [alle removeFromSuperview];
        [notificationButton removeFromSuperview];
        
    }

    
}

- (void)viewWillAppear:(BOOL)animated{
    
   
    [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesDashboardScreenName];

    
    [syncStatusLabel setText:[SWDefaults lastSyncStatus]];
    [syncDateLabel setText:[SWDefaults lastSyncDate]];
    [SWDefaults clearCustomer];

  
    [sortPickerView selectRow:0 inComponent:0 animated:YES];
    [sortPickerView reloadAllComponents];
    
    NSLog(@"customer array in view will appear %@", [customerArray description]);
    
   //[self getData];
    NSLog(@"customer dict is %@", [customerDict description]);
    
    [self getData];

    if (self.isMovingToParentViewController) {
        //[self navigatetodeals];

        if ([NSString isEmpty:[customerDict stringForKey:@"Customer_Name"]]==NO) {
            customerNameLabel.text = [customerDict stringForKey:@"Customer_Name"];
   
        }
        

    }
    else
    {
    }

    
    //[self.navigationController setToolbarHidden:YES animated:YES];

    [Flurry logEvent:@"Dashboard View"];
   // [Crittercism leaveBreadcrumb:@"<Dashboard View>"];
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    if([[AppControl retrieveSingleton].MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        alle.text=[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select COUNT(*) from (select A.Sender_Name,A.Msg_ID,A.Sender_ID,A.Msg_Title,A.Msg_Body,A.Logged_At AS Sent_at,A.Expires_At,B.Read_At, B.Recipient_ID,B.Reply_Msg_ID from tbl_msg AS A INNER join TBL_Msg_Recipients AS B ON A.msg_id=B.msg_id where A.Sender_ID!='%@' AND  A.Expires_At>date('now') order by A.Logged_At desc) As C  Left join tbl_msg AS D on C.Reply_Msg_ID=D.msg_id Where C.Read_At is NULL",[NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]]]]objectAtIndex:0]stringForKey:@"COUNT(*)"];
        
    }
    else
    {
        alle.text = [[[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select COUNT(*) from TBL_FSR_Messages where Message_Read='N' "] objectAtIndex:0] stringForKey:@"COUNT(*)"];
        
    }

    if ([alle.text isEqualToString:@"0"])
    {

        [alle removeFromSuperview];
        [notificationButton removeFromSuperview];

    }

    if(self.isMovingToParentViewController)
    {
        
    }
    
    toDoListButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"TaskIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(toDoListButtonTapped)];
    toDoListButton.tintColor=[UIColor whiteColor];
    if ([[AppControl retrieveSingleton].ENABLE_TODO isEqualToString:KAppControlsYESCode])
        self.navigationItem.rightBarButtonItem=toDoListButton;

   
}
-(void)toDoListButtonTapped
{
    
//    SalesWorxFieldSalesDashboardViewController * graphVC=[[SalesWorxFieldSalesDashboardViewController alloc]init];
//    //graphVC.data=salesTrendArray;
//    [self.navigationController pushViewController:graphVC animated:YES];
  
    
    
    SalesWorxToDoPopUpViewController *presentingView=   [[SalesWorxToDoPopUpViewController alloc]init];
    UINavigationController * toDoPopUpViewController=[[UINavigationController alloc]initWithRootViewController:presentingView];
    toDoPopUpViewController.navigationBarHidden=YES;
    //presentingView.syncPopdelegate=self;
    toDoPopUpViewController.view.backgroundColor = KPopUpsBackGroundColor;
    toDoPopUpViewController.modalPresentationStyle = UIModalPresentationCustom;
    presentingView.view.backgroundColor = KPopUpsBackGroundColor;
    presentingView.modalPresentationStyle = UIModalPresentationCustom;
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:toDoPopUpViewController animated:NO completion:nil];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if (isVisitAvailble) {
        
        [detailCustomerButton setEnabled:YES];
        customerDetailsButton.hidden=NO;
    }
    else
    {
        [detailCustomerButton setEnabled:NO];
        customerDetailsButton.hidden=YES;
        customerStatusLabel.text=KNotApplicable;
        customerOutstandingLabel.text=KNotApplicable;
        customerSalesLabel.text=KNotApplicable;
       
    }
    
    
}

-(void)becomeActive
{
    NSLog(@"app did become active called in dashboard");
    
    [self updateView];
    [self getData];
    
    if (isVisitAvailble) {
        [detailCustomerButton setEnabled:YES];
          customerDetailsButton.hidden=NO;
        
    }
    else
    {
        [detailCustomerButton setEnabled:NO];
        customerDetailsButton.hidden=YES;
        customerStatusLabel.text=KNotApplicable;
        customerOutstandingLabel.text=KNotApplicable;
        customerSalesLabel.text=KNotApplicable;
        
    }
    

}
-(void)navigatetodeals
{
    
    
    [self getData];
}
- (void)showPicker:(UIButton *)sender{
    

    
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:sortViewController]  ;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    
    datePickerPopOver.popoverContentSize=CGSizeMake(350, 300);
    
    
    
    [datePickerPopOver presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
- (void)showDetail:(UIButton *)sender{
    if(isVisitAvailble)
    {
        NSArray *temp = [[SWDatabaseManager retrieveManager] dbGgtDetailofCustomer:[customerDict stringForKey:@"Customer_ID"] andSite:[customerDict stringForKey:@"Site_Use_ID"]];
        
        
        //customerSer.delegate=self;
        //[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customerDict  stringForKey:@"Customer_ID"]];
        [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customerDict stringForKey:@"Customer_ID"]]];
           }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Today you have no visits" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Today you have no visits."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
    }
}
//- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount
{
    
    [SWDefaults setCustomer:customerDict];

    if (selectedCustomerDict) {
        customerDict=selectedCustomerDict;
    }
    else{
    customerDict = [SWDefaults validateAvailableBalance:orderAmmount];
    }

    Class module = NSClassFromString(@"SWCustomerDetailViewController");
    UIViewController *viewController = (UIViewController *)[[module alloc] initWithCustomer:customerDict];
    //[self.navigationController pushViewController:viewController animated:YES];
    [SWDefaults setCustomer:customerDict];
    orderAmmount=nil;
    
    //push to new screen with current customer id
    
    NSLog(@"selected customer details are %@", customerDict);
    
    SWCustomersViewController * customersVC=[[SWCustomersViewController alloc]init];
    
    customersVC.isFromDashBoard=YES;
    
    customersVC.selectedCustomerFromDashBoard=  [SWDefaults fetchCurrentCustObject:customerDict];
    
    
    [self.navigationController pushViewController:customersVC animated:YES];
    
    
    
}
- (void)getData
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    dateString = [dateFormat stringFromDate:today];
    
    NSArray *temp =  [[SWDatabaseManager retrieveManager] dbGetListofCustomer];
    if([temp count]==0)
    {
        
        [customerArray removeAllObjects];
        [sortPickerView reloadAllComponents];
        customerNameLabel.text = @"No Customers";
        customerStatusLabel.text = @"";
        customerOutstandingLabel.text=@"";
        
        
        isVisitAvailble=NO;
        if(isFirst)
        {
            
        

        }
        
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        NSString *dateString = [dateFormat stringFromDate:today];
        
        NSArray *temp1 = [[SWDatabaseManager retrieveManager] dbGetTotalPlannedVisits:dateString];
        if([temp1 count]==0)
        {
            plannedLabel.text = @"0";
        }
        else
        {
            plannedLabel.text = [[temp1 objectAtIndex:0] stringForKey:@"Total"];
        }
        
        temp1=nil;
    }
    else
    {
        isVisitAvailble=YES;
        customerArray = [temp mutableCopy] ;
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Visit_Start_Time" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        customerArray = [[customerArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy] ;
        sortDescriptor=nil;
        
        customerDict = [customerArray objectAtIndex:0];
        
        if(isFirst)
        {
            customerNameLabel.text = [[customerArray objectAtIndex:0] stringForKey:@"Customer_Name"];
        }
        
        NSArray *temp2 =[[SWDatabaseManager retrieveManager] dbGetCustomerStatus:[[customerArray objectAtIndex:0] stringForKey:@"Customer_ID"]];
        if(![temp2 count]==0)
        {
            BOOL customerStatus = [[[temp2 objectAtIndex:0] objectForKey:@"Cust_Status"] isEqualToString:@"Y"];
            BOOL cashCustomer = [[[temp2 objectAtIndex:0] objectForKey:@"Cash_Cust"] isEqualToString:@"Y"];
            BOOL creditHold = [[[temp2 objectAtIndex:0] objectForKey:@"Credit_Hold"] isEqualToString:@"Y"];
            
            NSString *imageName = @"Open";
            
            if (cashCustomer) {
                imageName = @"Cash";
            }
            
            if (!customerStatus || creditHold) {
                imageName = @"Blocked";
            }
            customerStatusLabel.text = imageName;
        }
        temp2=nil;

        NSArray *temp3 =[[SWDatabaseManager retrieveManager] dbGetCustomerSales:[customerDict stringForKey:@"Customer_ID"]];
        if([temp3 count]==0)
        {
            customerSalesLabel.text = [@"0" currencyString];
        }
        else
        {
            customerSalesLabel.text = [[[temp3 objectAtIndex:0] stringForKey:@"Total"] currencyString];
        }
        temp3=nil;

        NSArray *temp4 =[[SWDatabaseManager retrieveManager] dbGetCustomerOutstanding:[customerDict stringForKey:@"Customer_ID"]];
        if([temp4 count]==0)
        {
            customerOutstandingLabel.text = [@"0" currencyString];
        }
        else
        {
            customerOutstandingLabel.text = [[[temp4 objectAtIndex:0] stringForKey:@"Total"] currencyString];
        }
        temp4=nil;
        
      
 
        NSArray *temp1 = [[SWDatabaseManager retrieveManager] dbGetTotalPlannedVisits:dateString];

        if([temp1 count]==0)
        {
            plannedLabel.text = @"0";
        }
        else
        {
            plannedLabel.text = [[temp1 objectAtIndex:0] stringForKey:@"Total"];
        }
        temp1=nil;
    }
    
    NSArray *temp5 = [[SWDatabaseManager retrieveManager] dbGetTotalCompletedVisits:dateString];
    if([temp5 count]==0)
    {
        completedLabel.text = @"0";
    }
    else
    {
        completedLabel.text = [[temp5 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    NSLog(@"date string while getting out of route %@", dateString);
    
    NSArray *temp6 = [[SWDatabaseManager retrieveManager] dbGetCustomerOutOfRouteCount:dateString];
    if([temp6 count]==0)
    {
        outOfRouteLabel.text = @"0";
    }
    else
    {
        outOfRouteLabel.text = [NSString stringWithFormat:@"%d",temp6.count];
    }
    
    NSArray *temp7 = [[SWDatabaseManager retrieveManager] dbGetTotalOrder];
    if([temp7 count]==0)
    {
        orderLabel.text = @"0";
    }
    else
    {
        orderLabel.text = [[temp7 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    NSArray *temp8 = [[SWDatabaseManager retrieveManager] dbGetTotalReturn];
    if([temp8 count]==0)
    {
        returnLabel.text = @"0";
    }
    else
    {
        returnLabel.text = [[temp8 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    NSArray *temp9 =[[SWDatabaseManager retrieveManager] dbGetTotalCollection];
    if([temp9 count]==0)
    {
        collectionLabel.text = @"0";
    }
    else
    {
        collectionLabel.text = [[temp9 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];

    @try {
        [self loadLineChartData];
    }
    @catch (NSException *exception) {
        
    }
    
//    NSArray *temp10 = [[SWDatabaseManager retrieveManager] dbGetSaleTrend];
//    if(isFirst)
//    {
//        if (lineChartView)
//        {
//            [lineChartView removeFromSuperview];
//        }
//        NSMutableArray *maxValueArray = [NSMutableArray array];
//        for (int i =0 ; i <[temp10 count]; i++)
//        {
//            NSDictionary *data = [temp10 objectAtIndex:i];
//            [maxValueArray addObject:[NSNumber numberWithFloat: [[data stringForKey:@"Ammount"] floatValue]]] ;
//        }
//        
//        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO];
//        NSArray *sortDescriptors = [NSArray arrayWithObject:sort];
//        NSArray *sortedArray = [maxValueArray sortedArrayUsingDescriptors:sortDescriptors];
//        lineChartView = [[PCLineChartView alloc] initWithFrame:CGRectMake(20, 250,0, 240)];
//        if([sortedArray count]==0)
//        {
//            lineChartView.maxValue=100;
//            lineChartView.minValue =0;
//            lineChartView.interval =20;
//            NSInteger nsi = 5;
//            lineChartView.numYIntervals = nsi;
//        }
//        else
//        {
//            double maxiValues = [[sortedArray objectAtIndex:0] floatValue];
//            maxiValues = (int)roundf(maxiValues);
//            int divisible;
//            if(maxiValues >1000)
//            {
//                divisible= maxiValues/1000;
//                maxiValues = 1000  * (divisible+1);
//            }
//            else
//            {
//                divisible= maxiValues/100;
//                maxiValues = 100  * (divisible+1);
//            }
//            
//            lineChartView.maxValue=maxiValues;
//            lineChartView.minValue = 0;
//            lineChartView.interval =maxiValues/5;
//            NSInteger nsi = 5;
//            lineChartView.numYIntervals = nsi;
//        }
//        //[lineChartView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
//        lineChartView.backgroundColor = [UIColor clearColor];
//        [self.view addSubview:lineChartView];
//        [self moveFrame:lineChartView withFrame:CGRectMake(20, 250,self.view.frame.size.width-40, 240) withDuration:1.2];
//        
//        NSMutableArray *components = [NSMutableArray array];
//        NSMutableArray *point = [NSMutableArray array];
//        NSMutableArray *xPoints = [NSMutableArray array];
//        
//        NSDateFormatter *formatter = [NSDateFormatter new] ;
//        [formatter setDateFormat:@"dd"];
//        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//        [formatter setLocale:usLocale];
//        NSDateFormatter *formatter1 = [NSDateFormatter new];
//        [formatter1 setDateFormat:@"yyyy-MM-dd"];
//        NSLocale *usLocale1 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//        [formatter1 setLocale:usLocale1];
//        for (int i =0 ; i <[temp10 count]; i++)
//        {
//            NSDictionary *data = [temp10 objectAtIndex:i];
//            NSDate *date = [formatter1 dateFromString:[data stringForKey:@"DATE(Creation_Date)"]];
//            [xPoints addObject:[formatter stringFromDate:date]];
//            
//            [point addObject:[data stringForKey:@"Ammount"]];
//        }
//        
//        component = [[PCLineChartViewComponent alloc] init];
//        [component setPoints:point];
//        [component setShouldLabelValues:NO];
//        [component setColour:PCColorOrange];
//        [components addObject:component];
//        [lineChartView setComponents:components];
//        [lineChartView setXLabels:xPoints];
//        
//        formatter=nil;
//        usLocale=nil;
//        formatter1=nil;
//        usLocale1=nil;
//    }
    
    temp=nil;temp5=nil;temp6=nil;temp7=nil;temp8=nil;temp9=nil;//temp10=nil;

}

- (void)pickerView:(UIPickerView *)pV didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(![customerArray count]==0)
        
    {
        isFirst=NO;
        
        customerDict = [customerArray objectAtIndex:row];
        selectedCustomerDict=customerDict;

        selectedCustomerIndex=row;
        
        NSLog(@"selected customer dict in did select row %@", customerDict);
        
        customerNameLabel.text = [customerDict stringForKey:@"Customer_Name"];
        NSArray *temp2 = [[SWDatabaseManager retrieveManager] dbGetCustomerStatus:[customerDict stringForKey:@"Customer_ID"]];
        if(![temp2 count]==0)
        {
            BOOL customerStatus = [[[temp2 objectAtIndex:0] objectForKey:@"Cust_Status"] isEqualToString:@"Y"];
            BOOL cashCustomer = [[[temp2 objectAtIndex:0] objectForKey:@"Cash_Cust"] isEqualToString:@"Y"];
            BOOL creditHold = [[[temp2 objectAtIndex:0] objectForKey:@"Credit_Hold"] isEqualToString:@"Y"];
            
            NSString *imageName = @"Open";
            
            if (cashCustomer) {
                imageName = @"Cash";
            }
            
            if (!customerStatus || creditHold) {
                imageName = @"Blocked";
            }
            customerStatusLabel.text = imageName;
        }
        temp2=nil;
        [datePickerPopOver dismissPopoverAnimated:YES];
        [pV reloadAllComponents];
        
        
        //Added following code to refresh outsatnding and total sales. OLA!!
        
        NSArray *temp3 =[[SWDatabaseManager retrieveManager] dbGetCustomerSales:[customerDict stringForKey:@"Customer_ID"]];
        if([temp3 count]==0)
        {
            customerSalesLabel.text = [@"0" currencyString];
        }
        else
        {
            customerSalesLabel.text = [[[temp3 objectAtIndex:0] stringForKey:@"Total"] currencyString];
        }
        temp3=nil;
        
        NSArray *temp4 =[[SWDatabaseManager retrieveManager] dbGetCustomerOutstanding:[customerDict stringForKey:@"Customer_ID"]];
        if([temp4 count]==0)
        {
            customerOutstandingLabel.text = [@"0" currencyString];
        }
        else
        {
            customerOutstandingLabel.text = [[[temp4 objectAtIndex:0] stringForKey:@"Total"] currencyString];
        }
        temp4=nil;


    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if([customerArray count]==0)
    {
        return 1;
    }
    else
    {
        return [customerArray count];

    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if([customerArray count]==0)
    {
        return @"Today you have no visits";
    }
    else
    {
        return [[customerArray objectAtIndex:row] stringForKey:@"Customer_Name"];
    }
}
- (void)DashboardViewDelegateGetData;{
    [self getData];
}
- (void)DownloadingDidComplete:(NSNotification *)notification{

    [self getData];
    
    
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    datePickerPopOver=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}

#pragma mark Load Line Chart
-(void)loadLineChartData
{
    if(isFirst)
    {
        if (_myGraph)
        {
            [_myGraph removeFromSuperview];
        }
        
        _myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(30, 265,self.view.frame.size.width-63, 220)];
        _myGraph.delegate = self;
        _myGraph.dataSource = self;
        [self.view addSubview:_myGraph];
        
        _xPoints = [[NSMutableArray alloc]init];
        _ypoints = [[NSMutableArray alloc]init];
        
        if ([[AppControl retrieveSingleton].ENABLE_TGT_ACVMT_DASHBOARD isEqualToString:@"Y"])
        {
            NSDateComponents* components = [[NSCalendar autoupdatingCurrentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
            NSLog(@"month is %ld", (long)[components month]);
            
            NSMutableArray* targetArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items where Month='%ld'",(long)[components month]]];
            NSLog(@"%lu",(unsigned long)[targetArray count]);
            
            salesTrendArray=targetArray;
            
            [_xPoints addObject:@"0"];
            [_ypoints addObject:@"0"];
            
            if ([targetArray count] > 0)
            {
                float totalTarget = [[NSString stringWithFormat:@"%@",[[targetArray valueForKey:@"Target_Value"] objectAtIndex:0]] floatValue];
                float achievedTarget = [[NSString stringWithFormat:@"%@",[[targetArray valueForKey:@"Sales_Value"] objectAtIndex:0]] floatValue];
  
                [_xPoints addObject:[NSString stringWithFormat:@"%.1f",totalTarget]];
                [_ypoints addObject:[NSString stringWithFormat:@"%.1f",achievedTarget]];
            }
        }
        else
        {
            NSArray *temp = [[SWDatabaseManager retrieveManager] dbGetSaleTrend];
            
            NSDateFormatter *formatter = [NSDateFormatter new] ;
            [formatter setDateFormat:@"dd"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatter setLocale:usLocale];
            NSDateFormatter *formatter1 = [NSDateFormatter new];
            [formatter1 setDateFormat:@"yyyy-MM-dd"];
            NSLocale *usLocale1 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatter1 setLocale:usLocale1];
            
            if ([temp count] == 0)
            {
                [_xPoints addObject:@"0"];
                [_xPoints addObject:[formatter stringFromDate:[NSDate date]]];
                
                [_ypoints addObject:@"0"];
                [_ypoints addObject:@"0"];
            }
            else if ([temp count] == 1)
            {
                [_xPoints addObject:@"0"];
                [_ypoints addObject:@"0"];
            }
            
            for (int i =0 ; i <[temp count]; i++)
            {
                NSDictionary *data = [temp objectAtIndex:i];
                NSDate *date = [formatter1 dateFromString:[data stringForKey:@"DATE(Creation_Date)"]];
                [_xPoints addObject:[formatter stringFromDate:date]];
                [_ypoints addObject:[data stringForKey:@"Ammount"]];
            }
            
            formatter=nil;
            usLocale=nil;
            formatter1=nil;
            usLocale1=nil;
            temp = nil;
        }
        
        // Create a gradient to apply to the bottom portion of the graph
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        size_t num_locations = 2;
        CGFloat locations[2] = { 0.0, 1.0 };
        CGFloat components[8] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0 };
        
        // Apply the gradient to the bottom portion of the graph
        
        _myGraph.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
        
        // Enable and disable various graph properties and axis displays
        
        self.myGraph.enableTouchReport = YES;
        self.myGraph.enablePopUpReport = YES;
        self.myGraph.enableYAxisLabel = YES;
        self.myGraph.enableXAxisLabel = YES;
        self.myGraph.enableBezierCurve = YES;
        self.myGraph.alphaTop = 1;
        self.myGraph.alphaBottom = 1;
        self.myGraph.colorTop = UINavigationBarBackgroundColor;
        self.myGraph.colorBottom = UINavigationBarBackgroundColor;
        self.myGraph.colorLine = [UIColor whiteColor];
        self.myGraph.alphaLine = 1;
        self.myGraph.widthLine = 4;
        self.myGraph.colorPoint = [UIColor colorWithRed:(228.0/255.0) green:(228.0/255.0) blue:(228.0/255.0) alpha:1];
        self.myGraph.colorXaxisLabel = [UIColor whiteColor];
        self.myGraph.colorYaxisLabel = [UIColor whiteColor];
        self.myGraph.sizePoint = 10;
        self.myGraph.widthReferenceLines = 1;
        
        
        self.myGraph.autoScaleYAxis = YES;
        self.myGraph.alwaysDisplayDots = NO;
        self.myGraph.enableReferenceXAxisLines = YES;
        self.myGraph.enableReferenceYAxisLines = YES;
        self.myGraph.enableReferenceAxisFrame = YES;
        
        
        // Draw an average line
        self.myGraph.averageLine.enableAverageLine = YES;
        self.myGraph.averageLine.alpha = 0.6;
        self.myGraph.averageLine.color = [UIColor darkGrayColor];
        self.myGraph.averageLine.width = 2.5;
        self.myGraph.averageLine.dashPattern = @[@(2),@(2)];
        
        // Set the graph's animation style to draw, fade, or none
        self.myGraph.animationGraphStyle = BEMLineAnimationDraw;
        
        // Dash the y reference lines
        self.myGraph.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
        
        // Show the y axis values with this format string
        self.myGraph.formatStringForValues = @"%.1f";
    }
}

#pragma mark - SimpleLineGraph Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
    return (int)[_ypoints count];
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
    return [[_ypoints objectAtIndex:index]intValue];
}

#pragma mark - SimpleLineGraph Delegate

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
    return 0;
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index
{
    return [_xPoints objectAtIndex:index];
}

#pragma mark

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
