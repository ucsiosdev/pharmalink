//
//  SalesWorxPaymentCollectionImagePopOverTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/27/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"



@interface SalesWorxPaymentCollectionImagePopOverTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *transactionImageView;

@end
