//
//  SalesWorxDailyVisitSummaryNotesPopoverViewController.h
//  MedRep
//
//  Created by UshyakuMB2 on 23/07/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxDailyVisitSummaryNotesPopoverViewController : UIViewController
{
    IBOutlet UITableView *tblVisitNotes;
    NSMutableArray *arrVisitNotes;
}

@property(nonatomic,strong) NSString *selectedCustomerID;

@end
