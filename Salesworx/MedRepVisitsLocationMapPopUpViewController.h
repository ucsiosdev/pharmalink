//
//  MedRepVisitsLocationMapPopUpViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxPresentControllerBaseViewController.h"

@interface MedRepVisitsLocationMapPopUpViewController : SalesWorxPresentControllerBaseViewController
{
    CLLocationCoordinate2D  coord;
}

@property (strong, nonatomic) IBOutlet MKMapView *visitLocationMapView;
@property (strong, nonatomic) IBOutlet  NSMutableDictionary* VisitDetailsDictionary;
@property(nonatomic) BOOL isBrandAmbassadorVisit;

@property(strong,nonatomic) SalesWorxBrandAmbassadorVisit * currentBAVisit;
- (IBAction)doneButtontapped:(id)sender;

@end
