//
//  ServerSelectionViewController.h
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerSelectionViewController : UITableViewController
{
//       id target;
    SEL selectionChanged;
    NSMutableArray *serverArray;
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL selectionChanged;

@end