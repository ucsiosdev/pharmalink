//
//  SectionView.m
//  CustomTableTest
//
//  Created by Punit Sindhwani on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SectionView.h"
#import <QuartzCore/QuartzCore.h>
#import "SWFoundation.h"
#import "MedRepDefaults.h"

@implementation SectionView

@synthesize section;
@synthesize sectionTitle;
@synthesize discButton;
@synthesize delegate;

+ (Class)layerClass {
    
    return [CAGradientLayer class];
}

- (id)initWithFrame:(CGRect)frame WithTitle: (NSString *) title Section:(NSInteger)sectionNumber delegate: (id <SectionView>) Delegate
{
    self = [super initWithFrame:CGRectMake(0, 0, 150, 10)];

    if (self && title.length>0) {
        self = [super initWithFrame:frame];

        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(discButtonPressed:)];
        [self addGestureRecognizer:tapGesture];
        
        self.userInteractionEnabled = YES;

        self.section = sectionNumber;
        self.delegate = Delegate;

        CGRect LabelFrame = self.bounds;
        LabelFrame.size.width -= 50;
        CGRectInset(LabelFrame, 0.0, 5.0);
        
        
        UIView *separatorView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 8)];
        
        separatorView.backgroundColor = [UIColor whiteColor];
        [self addSubview:separatorView];

        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 14, 150, 30)];
        label.text = title;
//        label.font = [UIFont boldSystemFontOfSize:16.0]
        label.font = MedRepDescriptionLabelFont;

        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentLeft;
        [self addSubview:label];
        self.sectionTitle = label;
        
        CGRect buttonFrame = CGRectMake(LabelFrame.size.width, 0, 50, LabelFrame.size.height);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = buttonFrame;
        [button setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(discButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        self.discButton = button;
        static NSMutableArray *colors = nil;
        if (colors == nil) {
            colors = [[NSMutableArray alloc] initWithCapacity:3];
            UIColor *color = nil;
            color = KSalesOrderProductTableViewBrandCodeCellCollapsedColor;
            [colors addObject:(id)[color CGColor]];
            
            color = KSalesOrderProductTableViewBrandCodeCellCollapsedColor;
            [colors addObject:(id)[color CGColor]];
            color = KSalesOrderProductTableViewBrandCodeCellCollapsedColor;
            [colors addObject:(id)[color CGColor]];
            
            //            color = [UIColor colorWithRed:0.50 green:0.54 blue:0.58 alpha:1];
            //            [colors addObject:(id)[color CGColor]];
            //            color = [UIColor colorWithRed:0.15 green:0.20 blue:0.23 alpha:1];
            //            [colors addObject:(id)[color CGColor]];
            
            //            color =  UIColorFromRGB(0x2A3949);
            //            [colors addObject:(id)[color CGColor]];
            //            color =  UIColorFromRGB(0x2A3949);
            //            [colors addObject:(id)[color CGColor]];
            //            color =  UIColorFromRGB(0x2A3949);
            //            [colors addObject:(id)[color CGColor]];
            
            
        }
        
        CGRect imageFrame = CGRectMake(frame.size.width-40, ((frame.size.height/2)-5)+8, 18  , 10);
        UIImageView *arrowimage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:KTableViewCollapsedSectionArrowImageStr]];
        arrowimage.tintColor=[UIColor whiteColor];
        arrowimage.frame=imageFrame;
        [self addSubview:arrowimage];

        self.sectionStatusImageView = arrowimage;
        self.backgroundColor=KTableViewSectionCollapsedColor;

//        [(CAGradientLayer *)self.layer setColors:colors];
//        [(CAGradientLayer *)self.layer setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];

    }
    else
    {
       
    }

    return self;
}

- (void) discButtonPressed : (id) sender
{
    [self toggleButtonPressed:TRUE];
}

- (void) toggleButtonPressed : (BOOL) flag
{
    self.discButton.selected = !self.discButton.selected;
    if(flag)
    {
        if (self.discButton.selected) 
        {
            if ([self.delegate respondsToSelector:@selector(sectionOpened:)]) 
            {
                [self.delegate sectionOpened:self.section];
            }
        } else
        {
            if ([self.delegate respondsToSelector:@selector(sectionClosed:)]) 
            {
                [self.delegate sectionClosed:self.section];
            }
        }
    }
}

@end
