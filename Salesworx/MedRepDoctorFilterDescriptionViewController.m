//
//  MedRepDoctorFilterDescriptionViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/3/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@interface MedRepDoctorFilterDescriptionViewController ()

@end

@implementation MedRepDoctorFilterDescriptionViewController
@synthesize filterDescTitle,filterDescArray,refinedFilterDescArray,isEdetailing,searchBarText;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    NSLog(@"Doctors called");
    
    NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:  kSWX_FONT_REGULAR(16), NSFontAttributeName, nil];
    CGSize requestedTitleSize = [self.descTitle sizeWithAttributes:textTitleOptions];
    CGFloat titleWidth = MIN(self.view.frame.size.width, requestedTitleSize.width);
    CGRect frame = CGRectMake(0, 0, titleWidth, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = NSLocalizedString(self.descTitle, nil);
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:self.descTitle];

    //self.navigationItem.titleView = label;

    
//    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
//    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    if (isEdetailing==YES) {
        
        
    }
    
    
    self.navigationController.navigationBar.topItem.title = @"";


    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([self.filterType isEqualToString:kCloseVisitFilterType])
    {
        [self.view setFrame:self.customRect];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{

    [[[SWDefaults alloc]init ] AddBackButtonToViewcontroller:self];
    
    if ([self respondsToSelector:@selector(localizedCaseInsensitiveCompare:)]) {

    }
   // NSLog(@"filter desc array si %@", filterDescArray);
    //[filterDescArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    

    // NSLog(@"count inside filter is %@", [filterDescArray description]);

    
    
    refinedFilterDescArray=[[NSMutableArray alloc]init];
    [self.filterDescTblView flashScrollIndicators];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    if ([self.filterType isEqualToString:@"Doctor_Trade_Channel"]) {
        
        [self.filterDescTblView setFrame:CGRectMake(0, 44, 366, 400)];

    }
    
    
    
    if ([self.filterType isEqualToString:@"Pharmacy"]) {
        
        
    }
    
    
    
    else if ([self.filterType isEqualToString:@"Demo Plan"]||[self.filterType isEqualToString:@"UOM"]||[self.filterType isEqualToString:@"Product"]||[self.filterType isEqualToString:@"Category"]||[self.filterType isEqualToString:@"Brand"])
    {
        [self.filterDescTblView setFrame:CGRectMake(0, 44, 366, 600)];
 
    }
    
   
    else if ([self.filterType isEqualToString:@"Visit"])
        
    {
        
        
        if (self.customRect.size.width>0 ) {
            
            self.preferredContentSize=self.customRect.size;
            
        }
        
        NSLog(@"current frame in desc %@", NSStringFromCGRect(self.searchDisplayController.searchBar.frame));

        //self.view.frame=self.customRect;
        
        CGRect tempRect=self.filterSearchBar.frame;
        
        tempRect.size.width=self.customRect.size.width;
        
        self.searchDisplayController.searchBar.frame=CGRectMake(0, 0, tempRect.size.width, 44);
        
        self.searchDisplayController.searchResultsTableView.frame=CGRectMake(0, 44, self.customRect.size.width,self.customRect.size.height);
        
        
        [self.filterDescTblView setFrame:CGRectMake(0, 44, self.customRect.size.width,self.customRect.size.height-50)];
    }

    
   // NSLog(@"content in desc %d", [self.filterDescArray count]);
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//-(void)viewWillAppear:(BOOL)animated
//{
//    
//    
//  
//
//   
//
//   
//    
//   // [self.filterPopOverController setPopoverContentSize:CGSizeMake(366, 400) animated:YES];
//
//  //  self.preferredContentSize=CGSizeMake(300, 300);
//}

-(void)viewWillDisappear:(BOOL)animated
{
    //self.preferredContentSize=CGSizeZero;
    
    NSLog(@"selected view dis appearing");
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UITableView DataSource Methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    

    if (isSearching == YES) {

        if (refinedFilterDescArray.count>0) {

            return refinedFilterDescArray.count;
        }
        else
        {
            return 0;
        }

    }

    else
    {
    
    if (filterDescArray.count>0) {
        
        return filterDescArray.count;
    }
    
    else
    {
        return 0;
    }
    
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    
    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(14);
    cell.textLabel.textColor=MedRepElementDescriptionLabelColor;
    cell.detailTextLabel.font = MedRepElementTitleLabelFont;
    cell.detailTextLabel.textColor = MedRepMenuTitleFontColor;
    
    
    if (isSearching == YES)
    {
        if ([_descTitle isEqualToString:@"Contact"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[refinedFilterDescArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]]capitalizedString];
        }
        else if ([_filterType isEqualToString:@"New Location"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[refinedFilterDescArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]]capitalizedString];
        }
        else  if ([self.filterType isEqualToString:@"Demo Plan"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[refinedFilterDescArray objectAtIndex:indexPath.row] valueForKey:@"Description"]]capitalizedString];
        }
        else if ([self.filterType isEqualToString:@"Visit Location"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[refinedFilterDescArray objectAtIndex:indexPath.row] valueForKey:@"Location_Name"]]capitalizedString];
        }
        else
        {
            if ([_filterType isEqualToString:@"Visit"] && [_descTitle isEqualToString:@"Doctors"])
            {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];

                cell.textLabel.font=kSWX_FONT_SEMI_BOLD(14);
                cell.textLabel.textColor=MedRepMenuTitleDescFontColor;
                cell.detailTextLabel.font = kSWX_FONT_SEMI_BOLD(14);
                cell.detailTextLabel.textColor = MedRepMenuTitleFontColor;


                cell.textLabel.text = [[NSString stringWithFormat:@"%@",[[refinedFilterDescArray objectAtIndex:indexPath.row]objectAtIndex:0]]capitalizedString];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[[refinedFilterDescArray objectAtIndex:indexPath.row]objectAtIndex:1]];

            }
            else
            {
                cell.textLabel.text = [NSString stringWithFormat:@"%@",[refinedFilterDescArray objectAtIndex:indexPath.row]];
            }
        }
    }
    else
    {
        if ([self.descTitle isEqualToString:@"Contact"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[filterDescArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]]capitalizedString];
        }
        else if ([self.filterType isEqualToString:@"New Location"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[filterDescArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]]capitalizedString];
        }
        else   if ([self.filterType isEqualToString:@"Demo Plan"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[filterDescArray objectAtIndex:indexPath.row] valueForKey:@"Description"]]capitalizedString];
        }
        else if ([self.filterType isEqualToString:@"Visit Location"])
        {
            cell.textLabel.text=[[NSString stringWithFormat:@"%@",[[filterDescArray objectAtIndex:indexPath.row] valueForKey:@"Location_Name"]]capitalizedString];
        }
        else if ([self.descTitle isEqualToString:@"Specialization"])
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[filterDescArray objectAtIndex:indexPath.row]];
        }
        else
        {
            @try {
                if ([_filterType isEqualToString:@"Visit"] && [_descTitle isEqualToString:@"Doctors"])
                {
                    cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
                    
                    
                    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(14);
                    cell.textLabel.textColor=MedRepMenuTitleDescFontColor;
                    cell.detailTextLabel.font = kSWX_FONT_SEMI_BOLD(14);
                    cell.detailTextLabel.textColor = MedRepMenuTitleFontColor;

                    cell.textLabel.text = [[NSString stringWithFormat:@"%@",[[filterDescArray objectAtIndex:indexPath.row]objectAtIndex:0]]capitalizedString];
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[[filterDescArray objectAtIndex:indexPath.row]objectAtIndex:1]];
                    
                }
                else if ([_filterType isEqualToString:@"Visit"] && [_descTitle isEqualToString:@"Demo Plan"])
                {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@",[filterDescArray objectAtIndex:indexPath.row]];
                }
                else
                {
                    cell.textLabel.text = [[NSString stringWithFormat:@"%@",[filterDescArray objectAtIndex:indexPath.row]]capitalizedString];
                }
            }
            @catch (NSException *exception) {
                
            }
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_filterType isEqualToString:@"Visit"] && [_descTitle isEqualToString:@"Doctors"])
    {
        return 60.0f;
    }
    else
    {
        return 44.0f;
    }
}

#pragma mark UITableView Delegate methods


-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
    // [textField release] // uncomment if not using ARC
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self dismissKeyboard];

    
    if (isSearching == YES) {



        if (self.isCreateVisit==YES) {


//            dispatch_async(dispatch_get_main_queue(), ^(void){


                if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedValueforCreateVisit:)]) {


                    NSString* selectedStr=[refinedFilterDescArray objectAtIndex:indexPath.row];

                    NSLog(@"selected string vales in filter %@", selectedStr);





                    NSPredicate * selectedStringPredicate=[NSPredicate predicateWithFormat:@"self ==[c] '%@'",selectedStr];

                    NSArray* selectedPredicateArray=[filterDescArray filteredArrayUsingPredicate:selectedStringPredicate];

                    NSLog(@"predicate response %@", [selectedPredicateArray description]);


                    NSLog(@"test index %d", [filterDescArray indexOfObject:selectedStr]);


                    NSLog(@"test selected index %@", [filterDescArray objectAtIndex:[filterDescArray indexOfObject:selectedStr]]);

                    NSInteger selectedIndex=[filterDescArray indexOfObject:selectedStr];



                    NSIndexPath *matchedindexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];




                    [self.selectedFilterDelegate selectedValueforCreateVisit:matchedindexPath];



                    //filter desc array is too long to use a loop go with predicate
                    //                for (NSInteger i=0; i<filterDescArray.count; i++) {
                    //
                    //                    NSString* currentString=[filterDescArray objectAtIndex:i];
                    //
                    //                    if ([currentString isEqualToString:selectedStr]) {
                    //
                    //
                    //                        NSIndexPath *matchedindexPath = [NSIndexPath indexPathForRow:i inSection:0];
                    //
                    //                        [self dismissKeyboard];
                    //
                    //                        [self.selectedFilterDelegate selectedValueforCreateVisit:matchedindexPath];
                    //
                    //                    }
                    //
                    //
                    //                }

                }

//            });




        }


        if ([self.filterType isEqualToString:@"Demo Plan"]) {


            if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedDemoPlanDetails:)]) {

                [self.selectedFilterDelegate selectedDemoPlanDetails:refinedFilterDescArray];


            }

           if( [self.selectedFilterDelegate respondsToSelector:@selector(selectedDemoPlanDetaislwithIndex:selectedIndex:)])
            {
                [self.selectedFilterDelegate selectedDemoPlanDetaislwithIndex:refinedFilterDescArray selectedIndex:indexPath.row];


            }


        }

        else if ([self.filterType isEqualToString:@"Visit Location"])
        {

            if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedVisitLocationDetails:)]) {



                [self.selectedFilterDelegate selectedVisitLocationDetails:[refinedFilterDescArray objectAtIndex:indexPath.row]];



            }

        }


        else
        {

        if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedFilterValue:)]) {

            [self.selectedFilterDelegate selectedFilterValue:[refinedFilterDescArray objectAtIndex:indexPath.row]];
        }
        }


        if ([self.filterType isEqualToString:kCloseVisitFilterType]) {
            [self.navigationController popViewControllerAnimated:NO];


        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];

        }


    }

    else
    {
        
        
        if (self.isCreateVisit==YES) {
            
            if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedValueforCreateVisit:)]) {
                
                [self.selectedFilterDelegate selectedValueforCreateVisit:indexPath];
            }
        }
        
        if ([self.filterType isEqualToString:@"Demo Plan"]) {
            
            
            if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedDemoPlanDetails:)]) {
                
                [self.selectedFilterDelegate selectedDemoPlanDetails:filterDescArray];
            }
            
            if( [self.selectedFilterDelegate respondsToSelector:@selector(selectedDemoPlanDetaislwithIndex:selectedIndex:)])
            {
                [self.selectedFilterDelegate selectedDemoPlanDetaislwithIndex:filterDescArray selectedIndex:indexPath.row];
                
                
            }
            
            
        }
        
        else if ([self.filterType isEqualToString:@"Visit Location"])
        {
           
            
            
            
            if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedVisitLocationDetails:)]) {
                

                
                [self.selectedFilterDelegate selectedVisitLocationDetails:[filterDescArray objectAtIndex:indexPath.row]];

                
                
            }
            
        }
        
        else
        {
        if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedFilterValue:)]) {
            
            [self.selectedFilterDelegate selectedFilterValue:[filterDescArray objectAtIndex:indexPath.row]];
        }
        }
        
        if ([self.filterType isEqualToString:kCloseVisitFilterType]) {
            [self.navigationController popViewControllerAnimated:NO];
            
            
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
            
        }

    }
    
    
}




#pragma mark Search DisplayController methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    
    if (searchText.length == 0) {
        isSearching = NO;
        [_filterDescTblView reloadData];
    }else{
        isSearching = YES;
        [self searchContent];
    }
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBarText setShowsCancelButton:YES animated:YES];
    if ([searchBarText isFirstResponder]) {
    }
    else{
      [searchBarText becomeFirstResponder];
    }
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    NSString* newText = searchBarText.text;
    if (newText.length >0) {
        
    }else{
        isSearching = NO;
        [self.filterDescTblView reloadData];
    }
}

 

    
    - (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
        
        [searchBarText setText:@""];
        [searchBarText setShowsCancelButton:NO animated:YES];
        refinedFilterDescArray = [[NSMutableArray alloc]init];
        if ([searchBarText isFirstResponder]) {
            [searchBarText resignFirstResponder];
        }
        
        [self.view endEditing:YES];
        [self searchContent];
    }
    
    
-(void)searchContent
{
    
    NSString *searchText = searchBarText.text;
    
    [refinedFilterDescArray removeAllObjects];
    if ([self.descTitle isEqualToString:@"Contact"] || [self.filterType isEqualToString:@"New Location"])
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.Contact_Name contains[cd] %@", searchText];
        NSLog(@"predicate for contact is %@", resultPredicate);
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    else if ([self.filterType isEqualToString:@"Visit Location"])
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.Location_Name contains[cd] %@", searchText];
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    else if ([_filterType isEqualToString:@"Visit"] && [_descTitle isEqualToString:@"Doctors"])
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ANY SELF contains[cd] %@", searchText];
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }else if ([_filterType isEqualToString:@"Product-Name"]){
        NSPredicate *resultPredicate = [SWDefaults fetchMultipartSearchPredicate:searchText withKey:@""];
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }else{
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    [_filterDescTblView reloadData];
}



//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//
//       [searchBar setText:@""];
//       [searchBar setShowsCancelButton:NO animated:YES];
//       refinedFilterDescArray=[[NSMutableArray alloc]init];
//       if ([searchBar isFirstResponder]) {
//           [searchBar resignFirstResponder];
//       }
//
//       [self.view endEditing:YES];
////       [self searchContent];
//   }


//- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView
//{
//    tableView.frame = CGRectMake(self.filterDescTblView.frame.origin.x, self.filterDescTblView.frame.origin.y-44, self.filterDescTblView.frame.size.width, self.filterDescTblView.frame.size.height);
//}
//
//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    if ([self.descTitle isEqualToString:@"Contact"] || [self.filterType isEqualToString:@"New Location"])
//    {
//        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.Contact_Name contains[cd] %@", searchText];
//        NSLog(@"predicate for contact is %@", resultPredicate);
//        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
//    }
//    else if ([self.filterType isEqualToString:@"Visit Location"])
//    {
//        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.Location_Name contains[cd] %@", searchText];
//        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
//    }
//    else if ([_filterType isEqualToString:@"Visit"] && [_descTitle isEqualToString:@"Doctors"])
//    {
//        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ANY SELF contains[cd] %@", searchText];
//        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
//    }else if ([_filterType isEqualToString:@"Product-Name"]){
//        NSPredicate *resultPredicate = [SWDefaults fetchMultipartSearchPredicate:searchText withKey:@""];
//        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
//    }else{
//        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
//        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
//    }
//}
//
//#pragma mark - UISearchDisplayController delegate methods
//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
//
//    return YES;
//}




@end
