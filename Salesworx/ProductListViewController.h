//
//  ProductListViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWViewController.h"
#import "SWLoadingView.h"

#import "ZBarSDK.h"
#import "Singleton.h"


@interface ProductListViewController : SWViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, UIPopoverControllerDelegate , ZBarReaderDelegate , UIImagePickerControllerDelegate>
{
    NSArray                     *products;
    UIView                      *myBackgroundView;
    UITableView                 *mainTableView;
    UISearchDisplayController   *searchController;
    UISearchBar                 *searchBar;
    NSMutableArray              *searchData;
    int                          totalRecords;
    ZBarReaderViewController    *reader;
    //id                         target;
    SEL                          action;
   // SWSection                   *section;
    SWLoadingView               *loadingView;
    UIPopoverController         *filterPopOver;
    UIPopoverController         *locationFilterPopOver;
    UILabel                     *countLabel;
    NSDictionary                *category;
    UIImageView                 *resultImage;
    UITextView                  *resultText;
    BOOL                         isBarCode;
   // Singleton                   *single;
    UIBarButtonItem             *totalLabelButton ;
    UIBarButtonItem             *flexibleSpace;
    UIBarButtonItem             *displayActionBarButton  ;
    UIBarButtonItem             *displayMapBarButton ;
    NSMutableDictionary         *sectionList;
    NSMutableDictionary         *sectionSearch;
    
    NSMutableArray* selectedData;

    //UISearchBar *candySearchBar;
    BOOL bSearchIsOn;
    NSMutableArray *productArray;
    NSMutableArray *filteredCandyArray;
    NSMutableArray *finalProductArray;
    NSMutableSet* _collapsedSections;
    NSMutableDictionary *productDictionary;

}
@property (nonatomic, strong) NSArray *products;
@property (nonatomic, strong) NSMutableArray *searchData;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
@property (nonatomic, strong) NSDictionary *category;

- (id)initWithCategory:(NSDictionary *)c;

@end
