//
//  MedRepDateRangeTextfield.h
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 1/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"

@interface MedRepDateRangeTextfield : MedRepTextField
@property (strong,nonatomic) NSDate * StartDate;
@property (strong,nonatomic) NSDate * EndDate;
@property (strong,nonatomic) NSString * SelecetdCustomDateRangeText;
-(void)SetCustomDateRangeText;
@end
