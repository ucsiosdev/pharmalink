//
//  ReportViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "ReportViewController.h"
#import "TTSlidingPageTitle.h"
#import "TTSlidingPage.h"
#import "SalesSummaryViewController.h"
#import "BlockedCustomerViewController.h"
#import "StatementReportViewController.h"
#import "PaymentReportViewController.h"
#import "ReviewDocumentViewController.h"
#import "TTScrollSlidingPagesController.h"

@interface ReportViewController ()

@end

@implementation ReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Reports", nil);
    self.view.backgroundColor =  [UIColor whiteColor];
    
    NSLog(@"report called");
    
    TTScrollSlidingPagesController *slider = [[TTScrollSlidingPagesController alloc] init];
    slider.titleScrollerItemWidth=310;
    slider.titleScrollerBackgroundColour = UIColorFromRGB(0xE0E5EC);
    slider.disableTitleScrollerShadow = YES;
    slider.disableUIPageControl = YES;
    slider.titleScrollerTextColour = [UIColor darkGrayColor];
    slider.dataSource = self;
    slider.view.frame = self.view.bounds;
    
    [self.view addSubview:slider.view];
    [self addChildViewController:slider];
    
    
    
    
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
//    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//    [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
}
#pragma mark TTSlidingPagesDataSource methods
-(int)numberOfPagesForSlidingPagesViewController:(TTScrollSlidingPagesController *)source{
    return 5; //just return 7 pages as an example
}

-(TTSlidingPage *)pageForSlidingPagesViewController:(TTScrollSlidingPagesController*)source atIndex:(int)index{
    UIViewController *viewController;
    switch (index) {
        case 4:
            viewController = [[SalesSummaryViewController alloc] initWithNibName:@"SalesSummaryViewController" bundle:nil];
            break;
        case 1:
            viewController = [[BlockedCustomerViewController alloc] initWithNibName:@"BlockedCustomerViewController" bundle:nil];
            break;
        case 2:
            viewController = [[StatementReportViewController alloc] initWithNibName:@"StatementReportViewController" bundle:nil];
            break;
        case 3:
            viewController = [[PaymentReportViewController alloc] initWithNibName:@"PaymentReportViewController" bundle:nil];
            break;
        case 0:
            viewController = [[ReviewDocumentViewController alloc] initWithNibName:@"ReviewDocumentViewController" bundle:nil];
            break;
        default:
            break;
    }

    
    return [[TTSlidingPage alloc] initWithContentViewController:viewController];
}

-(TTSlidingPageTitle *)titleForSlidingPagesViewController:(TTScrollSlidingPagesController *)source atIndex:(int)index{
    TTSlidingPageTitle *title;
 
        //all other pages just use a simple text header
        switch (index) {
            case 4:
                title = [[TTSlidingPageTitle alloc] initWithHeaderText:NSLocalizedString(@"Sales Summary", nil)];
                break;
            case 1:
                title = [[TTSlidingPageTitle alloc] initWithHeaderText:NSLocalizedString(@"Blocked Customers", nil)];
                break;
            case 2:
                title = [[TTSlidingPageTitle alloc] initWithHeaderText:NSLocalizedString(@"Customer Statement", nil)];
                break;
            case 3:
                title = [[TTSlidingPageTitle alloc] initWithHeaderText:NSLocalizedString(@"Payment Summary", nil)];
                break;
            case 0:
                title = [[TTSlidingPageTitle alloc] initWithHeaderText:NSLocalizedString(@"Review Documents", nil)];
                break;
            default:
                break;
        }
        
    
    return title;
}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
