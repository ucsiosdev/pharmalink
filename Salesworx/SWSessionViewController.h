//
//  SWSessionViewController.h
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"

@interface SWSessionViewController : SWViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIAlertViewDelegate> {
    UITableView *tableView;
    NSString *login;
    NSString *password;
    SWLoadingView *loadingView;
    NSString *ClientVersion;
    
}
@end