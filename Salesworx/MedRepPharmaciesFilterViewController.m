//
//  MedRepPharmaciesFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/9/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepPharmaciesFilterViewController.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
@interface MedRepPharmaciesFilterViewController ()

@end

@implementation MedRepPharmaciesFilterViewController
@synthesize typeSegment,filterNavController,filterPopOverController,filterTitle,locationsBtn,tradeChannelBtn,previousFilteredParameters;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:self.filterTitle];
    
    typeSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    
    
    filterDict=[[NSMutableDictionary alloc]init];
    [filterDict setValue:@"Active" forKey:@"Is_Active"];
    
    [statusSwitch setOn:YES];
    
    
    NSLog(@"previously filtered pharmacy parameters are %@", [previousFilteredParameters description]);
    
    
    if ([previousFilteredParameters valueForKey:@"Is_Active"]!=nil) {
        
        [filterDict setValue:[previousFilteredParameters valueForKey:@"Is_Active"] forKey:@"Is_Active"];
        
        
        if ([[previousFilteredParameters valueForKey:@"Is_Active"] isEqualToString:@"InActive"]) {
            
            [statusSwitch setOn:NO animated:NO];
            
        }
        
        else if ([[previousFilteredParameters valueForKey:@"Active"] isEqualToString:@"Active"])
            
        {
            [statusSwitch setOn:YES animated:NO];
            
        }
        
    }
    
    
    if ([previousFilteredParameters valueForKey:@"Trade Channel"]!=nil) {
        
        [filterDict setValue:[previousFilteredParameters valueForKey:@"Trade Channel"] forKey:@"Trade Channel"];
        tradeChannelTextField.text=[previousFilteredParameters valueForKey:@"Trade Channel"];
    }
    
    
    
    if ([previousFilteredParameters valueForKey:@"Locations"]!=nil) {
        
        [filterDict setValue:[previousFilteredParameters valueForKey:@"Locations"] forKey:@"Locations"];
        locationTextField.text=[previousFilteredParameters valueForKey:@"Locations"];
    }
    
    
    if ([previousFilteredParameters valueForKey:@"Type"]!=nil) {
        
        [filterDict setValue:[previousFilteredParameters valueForKey:@"Type"] forKey:@"Type"];
        
        if ([[previousFilteredParameters valueForKey:@"Type"]isEqualToString:@"Rx"]) {
            
            typeSegment.selectedSegmentIndex=1;
        }
        
        else if ([[previousFilteredParameters valueForKey:@"Type"]isEqualToString:@"OTC"]) {
            
            typeSegment.selectedSegmentIndex=0;
        }
    }
    
    


    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






-(void)viewWillAppear:(BOOL)animated
{
    
  //NSLog(@"content array is %@", [self.contentArray description]);
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingPharmcyFilter];
    }
    
    UIFont *font = MedRepSegmentControlFont;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    
    
    [typeSegment setTitleTextAttributes:attributes
                                        forState:UIControlStateNormal];
    
    
    [typeSegment setTitleTextAttributes:attributes
                                 forState:UIControlStateNormal];
    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear",nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    
    
    
    
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                               forState:UIControlStateNormal];
    
    clearFilterBtn.tintColor=[UIColor whiteColor];
    
    
    
    
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close",nil)  style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];

}


-(void)closeFilter

{
    
    
    if ([self.pharmacyFilterDelegate respondsToSelector:@selector(pharmacyFilterDidCancel)]) {
        
      //  [self.pharmacyFilterDelegate pharmacyFilterDidCancel];
        
        
        [self.pharmacyFilterDelegate filteredPharmacyParameters:previousFilteredParameters];
        
        [filterPopOverController dismissPopoverAnimated:YES];
        
    }
}

-(void)clearFilter

{
    
    tradeChannelTextField.text=@"";
    locationTextField.text=@"";
    typeSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    
    [statusSwitch setOn:YES animated:YES];
    
    
    filterDict=[[NSMutableDictionary alloc]init];
    
    
    [filterDict setValue:@"Active" forKey:@"Is_Active"];

    
    /*if ([self.pharmacyFilterDelegate respondsToSelector:@selector(pharmacyFilterDidCancel)]) {
     
        [self.pharmacyFilterDelegate pharmacyFilterDidCancel];
        
        [filterPopOverController dismissPopoverAnimated:YES];
        
    }*/

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)statusSegmentSwitch:(UISegmentedControl *)sender {
    
    
}


- (IBAction)tradeChannelButtonTapped:(id)sender {
    
    selectedDataString=@"Trade Channel";
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Pharmacy";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredContentforPharmacy:@"Custom_Attribute_1"] valueForKey:@"Custom_Attribute_1"];
    
    NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
    
    
    if (filterDescArray.count>0) {
        
        for (NSInteger i=0; i<filterDescArray.count; i++) {
            NSString* currentValue=[filterDescArray objectAtIndex:i];
            
            
            NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
            
            if (refinedValue.length>0) {
                [refinedArray addObject:refinedValue];
                
            }
            
        }
        
        NSLog(@"refined array in trade channel %@", [refinedArray description]);
        
        
        filterDescVC.filterDescArray=refinedArray;

        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }


    
}


- (IBAction)searchButtonTapped:(id)sender {
    
    NSLog(@"saved filters are %@", [filterDict description]);
    
    //start filter
    
    //NSLog(@"content array is %@", [self.contentArray description]);
    
    
    
    NSMutableArray* filteredPharmaciesArray=[[NSMutableArray alloc]init];
    
    
    
    NSString* filter = @"%K == [c] %@";
    
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    
    
    
       if([[filterDict valueForKey:@"Trade Channel"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"TradeChannel", [filterDict valueForKey:@"Trade Channel"]]];
    }
    
    
    if([[filterDict valueForKey:@"Locations"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Address_1", [filterDict valueForKey:@"Locations"]]];
    }

    
    if([[filterDict valueForKey:@"Type"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"OTCRX", [filterDict valueForKey:@"Type"]]];
    }
    if([[filterDict valueForKey:@"Is_Active"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Is_Active", [NSString stringWithFormat:@"%@",[filterDict valueForKey:@"Is_Active"]]]];
    }
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    
    NSLog(@"compound pred for pharmacies %@", [compoundpred description]);

    //NSLog(@"content pred %@", [self.contentArray description]);

    
    filteredPharmaciesArray=[[self.contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
   // NSLog(@"filtered doctors in filterVC %@", [filteredPharmaciesArray description]);
    
    if (predicateArray.count==0) {
        
        

        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:NSLocalizedString(@"Please change your filter criteria and try again", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noFilterAlert show];
    }
    
    
    
    
  else  if (filteredPharmaciesArray.count>0) {
        
        
        if ([self.pharmacyFilterDelegate respondsToSelector:@selector(filteredPharmacyDetails:)]) {
            
            [self.pharmacyFilterDelegate filteredPharmacyDetails:filteredPharmaciesArray];
            
            [self.pharmacyFilterDelegate filteredPharmacyParameters:filterDict];
            
            [self.filterPopOverController dismissPopoverAnimated:YES];
            
            
        }
        
    }
    
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];

        
    }

}


-(void)selectedFilterValue:(NSString*)selectedString
{
    NSLog(@"select value is %@", selectedString);
    
    if ([selectedDataString isEqualToString:@"Trade Channel"]) {
        
       // [tradeChannelBtn setTitle:[NSString stringWithFormat:@"%@", selectedString] forState:UIControlStateNormal];
        tradeChannelTextField.text=[NSString stringWithFormat:@"%@", selectedString] ;
        [filterDict setValue:selectedString forKey:selectedDataString];
        
    }
    else if ([selectedDataString isEqualToString:@"Locations"]) {
        
       // [locationsBtn setTitle:[NSString stringWithFormat:@"%@", selectedString] forState:UIControlStateNormal];
        
        if ([selectedString hasPrefix:@" "]) {
            
            selectedString=[selectedString substringFromIndex:1];
        }
        
        locationTextField.text=[NSString stringWithFormat:@"%@", selectedString] ;
        
        [filterDict setValue:selectedString forKey:selectedDataString];
        
    }
    
   

    
}

- (IBAction)locationButtonTapped:(id)sender {
    
    selectedDataString=@"Locations";
    
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    filterDescVC.filterType=@"Pharmacy";
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredContentforPharmacy:@"Address_1"] valueForKey:@"Address_1"];
    

    
    
    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        
        [self.navigationController pushViewController:filterDescVC animated:YES];
        
    }
    
    
    else
    {
//        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [unavailableAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }

    
    
}

- (IBAction)statusSwitchTapped:(id)sender {
    
    BOOL state = [sender isOn];
    
    NSLog(@"Switch is %hhd", state);
    
    if (state == YES) {
        
        NSLog(@"This Active");
        
        [filterDict setValue:@"Active" forKey:@"Is_Active"];
        
    }
    else{
        [filterDict setValue:@"InActive" forKey:@"Is_Active"];
        
        
        NSLog(@"This is InActive");
    }
    
    
    
}

- (IBAction)segmentSwitch:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        NSLog(@"This OTC");
        
        [filterDict setValue:@"OTC" forKey:@"Type"];
        
    }
    else{
        [filterDict setValue:@"Rx" forKey:@"Type"];
        
        
        NSLog(@"This is Rx");
    }
    
    
}

- (IBAction)resetButtonTapped:(id)sender {
    
    
//    
//    if ([self.pharmacyFilterDelegate respondsToSelector:@selector(CustomPopOverCancelDelegate)]) {
//        
//        
//        
//        [self.pharmacyFilterDelegate filteredPharmacyDetails:nil];
//        
//        [self.pharmacyFilterDelegate pharmacyFilterDidCancel];
//
//        
//    }
//    [self.filterPopOverController dismissPopoverAnimated:YES];
//    
//
    
    
    
    if ([self.pharmacyFilterDelegate respondsToSelector:@selector(pharmacyFilterDidCancel)]) {
        
        //[self.pharmacyFilterDelegate pharmacyFilterDidCancel];
        
        [self.pharmacyFilterDelegate pharmacyFilterDidReset];
        
        
        
        [filterPopOverController dismissPopoverAnimated:YES];
        
    }
    
    
}


#pragma mark UItextFieldMethods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    NSLog(@"text field should begin called in pharmacy filter");
    
    if (textField==locationTextField) {
        selectedDataString=@"Locations";

    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedDataString;
    filterDescVC.filterType=@"Pharmacy";
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    filterDescArray=[[MedRepQueries fetchFilteredContentforPharmacy:@"Address_1"] valueForKey:@"Address_1"];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
    }
    
    }
    else{
        
        selectedDataString=@"Trade Channel";
        
        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
        filterDescVC.filterPopOverController=self.filterPopOverController;
        filterDescVC.filterType=@"Pharmacy";
        filterDescVC.selectedFilterDelegate=self;
        filterDescVC.descTitle=selectedDataString;
        
        NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
        filterDescArray=[[MedRepQueries fetchFilteredContentforPharmacy:@"Custom_Attribute_1"] valueForKey:@"Custom_Attribute_1"];
        
        NSMutableArray* refinedArray=[[NSMutableArray alloc]init];
        
        
        if (filterDescArray.count>0) {
            
            for (NSInteger i=0; i<filterDescArray.count; i++) {
                NSString* currentValue=[filterDescArray objectAtIndex:i];
                
                
                NSString* refinedValue=[MedRepDefaults getValidStringValue:currentValue];
                
                if (refinedValue.length>0) {
                    [refinedArray addObject:refinedValue];
                    
                }
                
            }
            
            NSLog(@"refined array in trade channel %@", [refinedArray description]);
            filterDescVC.filterDescArray=refinedArray;
            [self.navigationController pushViewController:filterDescVC animated:YES];
            
        }
        
        
        
        else
        {
            //        UIAlertView* unavailableAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //        [unavailableAlert show];
            
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
            
        }
    }

    return NO;
}


@end
