//
//  SalesWorxPaymentCollectionInvoicesTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxPaymentCollectionInvoicesTableViewCell.h"

@implementation SalesWorxPaymentCollectionInvoicesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)becomeFirstResponder
{
    return [self.settledAmountTxtFld becomeFirstResponder];
}

@end
