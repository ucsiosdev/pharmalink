//
//  SalesWorxProductsDemonstratedViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 11/15/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "XYPieChart/XYPieChart.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "HMSegmentedControl.h"
#import "SalesworxProductDetailsViewController.h"
#import "SalesworxProductSamplesGivenViewController.h"
#import "SalesWorxReportsDateSelectorViewController.h"
#import "MedRepDateRangeTextfield.h"
@interface SalesWorxProductsDemonstratedViewController : UIViewController <UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate,SalesWorxReportsDateSelectorViewControllerDelegate>//StringPopOverViewController_ReturnRow, SalesWorxDatePickerDelegate>
{
    NSString *datePopOverTitle;
    NSMutableArray *unfilteredProductArray, *unfilteredSamplesGivenArray, *arrLocationName, *arrLocationDetails;
    
    IBOutlet HMSegmentedControl *segmentControl;
    IBOutlet MedRepTextField *txtLocation;
    IBOutlet MedRepDateRangeTextfield *DateRangeTextField;
    IBOutlet UIScrollView *eDetailingScrollView;
    
    SalesworxProductDetailsViewController *productsDetailsViewController;
    SalesworxProductSamplesGivenViewController *productsSampleViewController;
}
@end
