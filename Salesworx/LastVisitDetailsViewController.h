//
//  LastVisitDetailsViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 1/22/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SalesWorxCustomClass.h"
#import "AppControl.h"
#import "MedRepTextView.h"


@interface LastVisitDetailsViewController : UIViewController
@property(strong,nonatomic) Customer * selectedCustomer;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *contactNameLabel;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *contactNumberLabel;
@property (weak, nonatomic) IBOutlet MedRepTextView *visitNotesTextView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *XcontactDetailsViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *XcontactDetailsBottomConstraint;

@end
