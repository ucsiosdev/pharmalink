//
//  MedRepVisitsDateFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/16/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepVisitsDateFilterViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@interface MedRepVisitsDateFilterViewController ()

@end

@implementation MedRepVisitsDateFilterViewController
@synthesize visitsDatePicker,datePickerMode,previouslySelectedDate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    NSString * titletext=[[NSString alloc]init];
    
    if (self.titleString.length>0) {
        titletext=NSLocalizedString(self.titleString, nil);
    }
    else{
    
    titletext = NSLocalizedString(@"Visit Date", nil);
    }
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:titletext];
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];

    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDateTapped)];
    
    
    
    [visitsDatePicker setLocale: [NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];

    
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    
    saveBtn.tintColor=[UIColor whiteColor];
    
//    [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:self]setTitleTextAttributes:barattributes forState:forState:UIControlStateNormal
    
   	
    self.navigationController.navigationBar.topItem.title = @"";

    
    self.navigationItem.rightBarButtonItem=saveBtn;
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
        
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }

    
    NSLog(@"previously selected date is %@",previouslySelectedDate );
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    
  
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:previouslySelectedDate];
    
    if (dateFromString) {
        
       // visitsDatePicker.minimumDate=[NSDate date];

        visitsDatePicker.date=dateFromString;
    }
    
    if ([self.titleString isEqualToString:@"Task Date"]) {
      
        
        visitsDatePicker.datePickerMode=UIDatePickerModeDate;
        
        visitsDatePicker.minimumDate=[NSDate date];
        

    }


    if ([datePickerMode isEqualToString:@"dateandTime"] ) {
        
    visitsDatePicker.datePickerMode=UIDatePickerModeDateAndTime;
        
        visitsDatePicker.minimumDate=[NSDate date];
    }
    
    
}


-(void)backButtontapped

{
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)saveDateTapped
{
    
    NSLog(@"date with local timezone is: %@", [visitsDatePicker.date descriptionWithLocale:[NSLocale systemLocale]]);


    
    NSLog(@"selected date is %@", visitsDatePicker.date);
    
    if ([self.visitDateDelegate respondsToSelector:@selector(selectedVisitDateDelegate:)]) {
        
        NSString *todayString;
        
        //refine date as per DB format
        
       
        
        if ([datePickerMode isEqualToString:@"dateandTime"]) {

        NSDateFormatter  *customDateFormatter = [[NSDateFormatter alloc] init];
            
            //to be done
            if (self.isBrandAmbassadorVisit) {
                [customDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
                customDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
                todayString = [NSString stringWithFormat:@"%@",[customDateFormatter stringFromDate:visitsDatePicker.date]];
            }
            else{
            
       // [customDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [customDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
        customDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];

        todayString = [NSString stringWithFormat:@" %@",[customDateFormatter stringFromDate:visitsDatePicker.date]];
            }
            
            
            
//            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
//            myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
//            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
//            
//            todayString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:visitsDatePicker.date]];
        }
        
        else if ([datePickerMode isEqualToString:kTodoTitle])
        {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];

            
            //to be done
            //[myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            
           NSString* tempDate = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:visitsDatePicker.date]];
            
            todayString=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"yyyy-MM-dd" scrString:tempDate];
            
  
        }
        
       else if (self.isVisitFilter==YES) {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd";
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
            
            
            //to be done
            //[myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            
            todayString = [NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:visitsDatePicker.date]];
            
            NSLog(@"selected visit date string being popped %@", todayString);
        }
        else
        {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
            
            //to be done
            //[myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
            
            todayString = [NSString stringWithFormat:@" %@",[myDateFormatter stringFromDate:visitsDatePicker.date]];
            

        }
        
        NSLog(@"refined date in delegate is %@", todayString);

        [self.visitDateDelegate selectedVisitDateDelegate:todayString];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
