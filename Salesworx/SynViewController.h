//
//  ViewController.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestResponseManager.h"
#import "AppConstantsList.h"
#import "DataSyncManager.h"
#import "Status.h"
#import "SWPlatform.h"
#import "AFHTTPClient.h" 
#import "AFHTTPRequestOperation.h"
#import "AFJSONRequestOperation.h"

@protocol SynViewControllerDelegate <NSObject>
@optional
- (void)updateActivationTextDelegate:(NSString *)synchDate;
- (void)lastSychDelegate:(NSString *)synchDate;
- (void)updateProgressDelegateWithType:(MBProgressHUDMode)type andTitle:(NSString *)title andDetail:(NSString *)detail andProgress:(float)progress ;
@end


@interface SynViewController : SWViewController <UITextFieldDelegate,UIPopoverControllerDelegate,UIAlertViewDelegate>
{
      id < SynViewControllerDelegate > delegate;
    
    
    int isFromSync,isForSyncUpload,uploadFileCount;
    int paramCount;
    //id target;
    SEL action;
    //
    BOOL isByteRecived;
    BOOL isActivated;
    BOOL isSendOrder;
    BOOL isCompleted;
    BOOL isByteSent;
    float progressBarFloatValue;

    DataSyncManager *appData;
    Status *status_;
    AlertType alertType;
    NSString *strDeviceID;
    NSMutableArray *procParamArray,*procValueArray;
    NSDictionary *syncStatusDict;
    UITextField *txtProcParams,*txtProcValues;
    UIImageView *activityImageView;
    UIImageView *activityImageView2;
    NSString *passwordActivate;
    NSString *usernameActivate;
    NSString *serverAPI;
    UIButton *startSynch;
    UIButton *startSynchOrder;
    UIButton *selectServer;
    UIProgressView *progressBar;
    UITextView *logTextView;
    UILabel *lastSyncLable;
    UILabel *lastSyncStatusLable;
    UILabel *lastSyncType;
    UILabel *lastSyncOrderSent;
    UILabel *pendingOrder;
    UILabel *pendingCollection;
    UILabel *pendingReturn;
    NSString *statusSynString;
    UIPopoverController *popoverController;
    NSMutableDictionary *serverDict;
    NSString *progressDetail;
    NSString *orderCountString;
    NSString *returnCountString;
    NSString *collectionCountString;
    NSTimer *timerObj;
    NSMutableArray *resultArray;
    
    AFHTTPClient *request;    //--add new line by paracha
    AFHTTPRequestOperation *operation;
    BOOL isSuccessfull;
    UIPopoverController *locationFilterPopOver;
    
    UIAlertView *ErrorAlert;

    
    NSMutableArray* mediaFilesToDeleteArray,*allMediaFilesArray;
    Singleton *single;


}

@property (unsafe_unretained) id < SynViewControllerDelegate > delegate;
@property (nonatomic, strong) AFHTTPClient *request;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
-(void)setRequestForSyncActivateWithUserName:(NSString *)username andPassword:(NSString *)password;
-(void)setRequestForSyncUpload;
-(void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue;
-(void)sendRequestForComplete;
-(void)sendRequestForUploadFile;
-(void)sendRequestForDownload;
-(void)sendRequestForInitiate;
-(void)sendRequestForStatus;
-(void)sendRequestForUploadData;
-(void)sendRequestForActivate;
-(void)finalLogPrint:(NSString *)text;
-(void)cancelHTTPRequest;
-(NSMutableArray *) fetchMediaFilesFromDocuments;
- (void)RequestFailureAlert:(NSString *)Message;
@end
