//
//  MedRepVisitsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepVisitsViewController.h"
#import "ZUUIRevealController.h"
#import "MedRepTableViewCell.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepQueries.h"
#import "MedRepCreateVisitViewController.h"
#import "MedRepCreateLocationViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SWDefaults.h"
#import "MedRepCreateDoctorViewController.h"
#import "MedRepStartEDetailingViewController.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "NSPredicate+Distinct.h"



@interface MedRepVisitsViewController ()

@end

@implementation MedRepVisitsViewController


@synthesize locationLbl,doctorLbl,dateLbl,demonstrationPlan,objectiveTextView,textViewScrollView,doctorPickerBtn,datePickerBtn,addDoctorBtn,popOverController,visitDetailsDict,dateFieldButton,isUpdatingVisit,selectedEditVisitDetails,currentVisitEditType;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    visitTypeArray=[[NSMutableArray alloc]initWithObjects:@"Normal",@"Round Table", nil];
    textViewScrollView.delegate=self;
    isPendingVisitEnabled = YES;
    visitDetailsDict=[[NSMutableDictionary alloc]init];
    demoPlanArray=[[NSMutableArray alloc]init];
    
    
    doctorPickerBtn.enabled=YES;
    
    appCntrl = [AppControl retrieveSingleton];
    
    objectiveWordLimit=[NSString getValidStringValue:appCntrl.FM_VISIT_CONCLUSION_WORD_LIMIT];
    
    
    if(currentVisitEditType==KVisitCreate){
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Create Visit", nil)];
    }else if(currentVisitEditType==KVisitUpdate){
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Edit Visit", nil)];
    }else{
        self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Reschedule Visit", nil)];
    }

    
    pharmacyContactDetailsArray=[[NSMutableArray alloc]init];
    [visitDetailsDict setValue:@"" forKey:@"Objective"];
    
    locationsarray=[[NSMutableArray alloc]initWithObjects:@"Clinic",@"Hospital",@"Pharmacy", nil];
    
    
    //fetech locations
    
    locationsarray=[MedRepQueries fetchLocations];
    
    
    /********************* data for all Doctors and Pharmacies when Pending Visits toggle is not enable ************************/
    
    pharmacyContactNamesArray=[[NSMutableArray alloc]init];
    doctorDetailsArray=[[NSMutableArray alloc]init];
    
    doctorDetailsArray = [MedRepQueries fetchDoctorsData];
   // doctorDetailsArray = [MedRepQueries fetchDoctorsDataWhenPendingVisitsEnabled];

    pharmacyContactNamesArray=[MedRepQueries fetchAllPharmacyDetails];
    
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:@"Doctor_ID"];
    doctorDetailsArray=[[doctorDetailsArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    tempContactsArray=[[NSMutableArray alloc]init];
    
    for (NSInteger i=0; i<doctorDetailsArray.count; i++)
    {
        NSMutableDictionary* currentDict=[doctorDetailsArray objectAtIndex:i];
        [currentDict setValue:@"D" forKey:@"Location_Type"];
        [tempContactsArray addObject:currentDict];
    }
    
    for (NSInteger i=0; i<pharmacyContactNamesArray.count; i++)
    {
        NSMutableDictionary* currentDict=[pharmacyContactNamesArray objectAtIndex:i];
        [tempContactsArray addObject:currentDict];
    }
    
    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Contact_Name" ascending:YES];
    NSArray * sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray * sortedArray = [tempContactsArray sortedArrayUsingDescriptors:sortDescriptors];
    tempContactsArray=[sortedArray mutableCopy];
    
    
    
    /********************* data for all Doctors and Pharmacies when Pending Visits toggle is enable ************************/

    arrOfDoctorsWhenPendingVisitsEnabled = [MedRepQueries fetchDoctorsDataWhenPendingVisitsEnabled];
    arrOfPharmacyWhenPendingVisitsEnabled = [MedRepQueries fetchAllPharmacyDetailsWhenPendingVisitsEnabled];
    
    tempArrayWhenPendingVisitsEnabled  = [[NSMutableArray alloc]init];
    [tempArrayWhenPendingVisitsEnabled addObjectsFromArray:arrOfDoctorsWhenPendingVisitsEnabled];
    [tempArrayWhenPendingVisitsEnabled addObjectsFromArray:arrOfPharmacyWhenPendingVisitsEnabled];
    
   /*
    for (NSInteger i=0; i<arrOfDoctorsWhenPendingVisitsEnabled.count; i++)
    {
        NSMutableDictionary* currentDict = [arrOfDoctorsWhenPendingVisitsEnabled objectAtIndex:i];
        
        if ([[currentDict stringForKey:@"CompletedVisit"]intValue] < [[currentDict stringForKey:@"Visits_Req"]intValue])
        {
            NSDictionary *newDict = [[NSMutableDictionary alloc]init];
            [newDict setValue:[currentDict stringForKey:@"Contact_Name"] forKey:@"Contact_Name"];
            [newDict setValue:[currentDict stringForKey:@"Doctor_ID"] forKey:@"Doctor_ID"];
            [newDict setValue:[currentDict stringForKey:@"Location_ID"] forKey:@"Location_ID"];
            [newDict setValue:@"D" forKey:@"Location_Type"];
            
            [tempArrayWhenPendingVisitsEnabled addObject:newDict];
        }
    }
    
    for (NSInteger i=0; i<arrOfPharmacyWhenPendingVisitsEnabled.count; i++)
    {
        NSMutableDictionary* currentDict = [arrOfPharmacyWhenPendingVisitsEnabled objectAtIndex:i];
        
        if ([[currentDict stringForKey:@"CompletedVisit"]intValue] < [[currentDict stringForKey:@"Visits_Req"]intValue])
        {
            NSDictionary *newDict = [[NSMutableDictionary alloc]init];
            [newDict setValue:[currentDict stringForKey:@"Contact_Name"] forKey:@"Contact_Name"];
            [newDict setValue:[currentDict stringForKey:@"Contact_ID"] forKey:@"Contact_ID"];
            [newDict setValue:[currentDict stringForKey:@"Location_ID"] forKey:@"Location_ID"];
            [newDict setValue:[currentDict stringForKey:@"Location_Type"] forKey:@"Location_Type"];
            [newDict setValue:[currentDict stringForKey:@"Contact_Code"] forKey:@"Contact_Code"];
            
            [tempArrayWhenPendingVisitsEnabled addObject:newDict];
        }
    }
    */
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"Contact_Name" ascending:YES];
    NSArray * sortDescriptor = [NSArray arrayWithObject:descriptor];
    tempArrayWhenPendingVisitsEnabled = [[tempArrayWhenPendingVisitsEnabled sortedArrayUsingDescriptors:sortDescriptor]mutableCopy];
    
    if(currentVisitEditType==KVisitCreate){
        /** initializing viistdetails dic*/
        [visitDetailsDict setValue:@"" forKey:@"Contact_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Contact_Name"];
        [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Doctor_Name"];
        [visitDetailsDict setValue:@"" forKey:@"Demo_Plan_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Location_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Location_Name"];
        [visitDetailsDict setValue:@"" forKey:@"Location_Type"];
        [visitDetailsDict setValue:@"" forKey:@"Objective"];
        [visitDetailsDict setValue:@"" forKey:@"Visit_Date"];
        [visitDetailsDict setValue:@"" forKey:@"Visit_Status"];
        [visitDetailsDict setValue:@"" forKey:@"Visit_Type"];
    }
    
    if ([appCntrl.DISABLE_FM_CREATE_NEW_LOCATION isEqualToString:KAppControlsYESCode]) {
        AddLocationButton.enabled = NO;
    }
    
    if ([appCntrl.DISABLE_FM_CREATE_NEW_CONTACT isEqualToString:KAppControlsYESCode] && [appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode]) {
        [addDoctorBtn setEnabled:NO];
    }
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)becomeActive
{
        [self dismissViewControllerAnimated:YES completion:^{
            [self.dismissDelegate createVisitDidCancel];
            
        }];;
   
}


-(void)viewWillAppear:(BOOL)animated
{
    
    self.preferredContentSize=CGSizeMake(376, 600);
    if (self.navigationController){
        self.navigationController.preferredContentSize = self.preferredContentSize;
    }
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingCreateVisit];
    }
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(becomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];

    
    self.view.layer.shadowOpacity = 0.0;
    

    
    
    
    [[[UILabel appearance]text]capitalizedString];
    [[[[UIButton appearance]titleLabel] text]capitalizedString];
    

    
    UIBarButtonItem* cancelBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(close)];
    
    
    
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:(57/255.0) green:(141/255.0) blue:(213/255.0) alpha:1];

    }
    
    
    UIBarButtonItem* saveBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    
    
    [saveBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    
    [cancelBarButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    

    
    [self.navigationItem setRightBarButtonItem:saveBarButtonItem];
    [self.navigationItem setLeftBarButtonItem:cancelBarButtonItem];
    

    
    popOverController.delegate=self;
    
    if (isNewLocation==YES || isNewDoctor==YES) {
        
        //doctorPickerBtn.enabled=NO;
        datePickerBtn.enabled=NO;
        dateFieldButton.enabled=NO;
        doctorLbl.enabled=NO;
        dateLbl.enabled=NO;
        dateLbl.text=@"";
    }
    

    
    
    
    
    for (UIView * currentLbl in self.view.subviews) {
        
        if ([currentLbl isKindOfClass:[UILabel class]]) {
            
            
            if (currentLbl.tag==4||currentLbl.tag==1||currentLbl.tag==2||currentLbl.tag==3) {
                
                
                
                CALayer *bottomBorder = [CALayer layer];
                bottomBorder.frame = CGRectMake(0.0f, currentLbl.frame.size.height - 1, currentLbl.frame.size.width, 1.0f);
                bottomBorder.backgroundColor = [UIColor colorWithRed:(201.0/255.0) green:(209.0/255.0) blue:(222.0/0/255.0) alpha:1].CGColor;
                [currentLbl.layer addSublayer:bottomBorder];
                
            }
            
            
        }
        
    }
    refinedDoctorsArray=[[NSMutableArray alloc]init];
    
    for (NSMutableDictionary * currentDict in doctorsArray) {
    
        if ([currentDict objectForKey:@"Doctor_Name"]) {
            
            [refinedDoctorsArray addObject:[currentDict objectForKey:@"Doctor_Name"]];
            
        }
    }
    
    objectiveTxtBgView.layer.borderWidth=1.0f;
    objectiveTxtBgView.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    objectiveTxtBgView.layer.shadowColor = [UIColor blackColor].CGColor;
    objectiveTxtBgView.layer.shadowOffset = CGSizeMake(2, 2);
    objectiveTxtBgView.layer.shadowOpacity = 0.1;
    objectiveTxtBgView.layer.shadowRadius = 1.0;
    
    for (UIView * currentButton in self.view.subviews) {
        
        if ([currentButton isKindOfClass:[UIButton class]]) {
            
            
            if (currentButton.tag==1||currentButton.tag==2||currentButton.tag==3|| currentButton.tag==4||currentButton.tag==5 || currentButton.tag==6) {
                
                currentButton.layer.borderWidth=1.0f;
                currentButton.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
                currentButton.layer.shadowColor = [UIColor blackColor].CGColor;
                currentButton.layer.shadowOffset = CGSizeMake(2, 2);
                currentButton.layer.shadowOpacity = 0.1;
                currentButton.layer.shadowRadius = 1.0;


            }
        }
    }
    
    
    /** hiding novisits segmntedcontrol if novisitlocation not available*/
    if(currentVisitEditType==KVisitUpdate ||
       currentVisitEditType==KVisitReschedule ||
       ![appCntrl.ENABLE_MEDREP_NO_VISIT_OPTION isEqualToString:KAppControlsYESCode] ||
       [[[[SWDefaults alloc] init] getDummyMedrepVisitLocationFromPhxTblLocationTable]count]==0){
        _xPendingVisitsLabelTopMarginConstraint.constant=13;
        [NovisitSegmgmentedControl setHidden:YES];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    if((currentVisitEditType==KVisitUpdate || currentVisitEditType==KVisitReschedule) && self.isMovingToParentViewController)
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            existingDoctorChanged=NO;
            isNewLocation=NO;
            locationIDforExistingLocationNewDoc=nil;
            locationIDforNewLocExistingDoc=nil;
            
            newLocationDataDict=nil;
            newDrDataDict=nil;
            
            isFutureVisit=YES;
            isNewDoctor=NO;
            isNewContact=NO;
            noContactAvailableforLoction=NO;
            cretedDoctorID=nil;
            
            
            
            if([[selectedEditVisitDetails valueForKey:@"Location_Type"] isEqualToString:@"D"])
            {
                [visitDetailsDict setValue:nil forKey:@"Contact_ID"];
                [visitDetailsDict setValue:nil forKey:@"Contact_Name"];
                [visitDetailsDict setValue:[SWDefaults getValidStringValue:[selectedEditVisitDetails valueForKey:@"Doctor_ID"]] forKey:@"Doctor_ID"];
                [visitDetailsDict setValue:[SWDefaults getValidStringValue:[selectedEditVisitDetails valueForKey:@"Doctor_Name"]] forKey:@"Doctor_Name"];
            }
            else
            {
                [visitDetailsDict setValue:[SWDefaults getValidStringValue:[selectedEditVisitDetails valueForKey:@"Contact_ID"]] forKey:@"Contact_ID"];
                [visitDetailsDict setValue:[SWDefaults getValidStringValue:[selectedEditVisitDetails valueForKey:@"Pharmacy_Contact_Name"]] forKey:@"Contact_Name"];
                [visitDetailsDict setValue:nil forKey:@"Doctor_ID"];
                [visitDetailsDict setValue:nil forKey:@"Doctor_Name"];
            }
            
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Demo_Plan_ID"] forKey:@"Demo_Plan_ID"];
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Location_ID"] forKey:@"Location_ID"];
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Location_Name"] forKey:@"Location_Name"];
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Location_Type"] forKey:@"Location_Type"];
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Objective"] forKey:@"Objective"];
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Visit_Date"] forKey:@"Visit_Date"];
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Visit_Status"] forKey:@"Visit_Status"];
            [visitDetailsDict setValue:[selectedEditVisitDetails valueForKey:@"Visit_Type"] forKey:@"Visit_Type"];
            NSString* demoPlanNameStr=[MedRepQueries fetchDemoPlanNamewithID:[visitDetailsDict valueForKey:@"Demo_Plan_ID"]];
            [visitDetailsDict setValue:demoPlanNameStr forKey:@"Demonstration_Plan"];
            locationType=[visitDetailsDict valueForKey:@"Location_Type"];
            NSMutableArray *visitTypesAppcodesObjectsArray=[MedRepQueries getTBLAppCodesDataForCodeType:@"VISIT_TYPE"];
            NSPredicate *visitTypePredicate=[NSPredicate predicateWithFormat:@"Code_Value==[cd]%@",[visitDetailsDict valueForKey:@"Visit_Type"]];
            NSArray *tempVisitTypeAppcodesObjectsArray=[visitTypesAppcodesObjectsArray filteredArrayUsingPredicate:visitTypePredicate];
            NSString *visitType;
            if(tempVisitTypeAppcodesObjectsArray.count==1)
            {
                TBLAppCode *visitTypeAppcode=[tempVisitTypeAppcodesObjectsArray objectAtIndex:0];
                [visitDetailsDict setValue:visitTypeAppcode.Code_Value forKey:@"Visit_Type"];
                visitType=visitTypeAppcode.Code_Description;
            }
            else
            {
                showVisitTypeNotAvailableAlertMessage=YES;
            }
            selectedLocationID=[visitDetailsDict valueForKey:@"Location_ID"];
            locationIDforExistingLocationNewDoc=selectedLocationID;
            dispatch_async(dispatch_get_main_queue(), ^(void){
                locationTextField.text=[visitDetailsDict valueForKey:@"Location_Name"];
                objectiveTextView.text=[visitDetailsDict valueForKey:@"Objective"];
                
                NSLog(@"setting date 3 %@", [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd/MM/yyyy hh:mm a" scrString:[visitDetailsDict valueForKey:@"Visit_Date"]]);

                dateTextField.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[visitDetailsDict valueForKey:@"Visit_Date"]];
                visitTypeTextField.text=visitType;
                demoPlanTextField.text=[visitDetailsDict valueForKey:@"Demonstration_Plan"];
                doctorTextField.text=[[selectedEditVisitDetails valueForKey:@"Location_Type"] isEqualToString:@"D"]?[visitDetailsDict valueForKey:@"Doctor_Name"]:[visitDetailsDict valueForKey:@"Contact_Name"];
                [self getDemoPlansForDate:[visitDetailsDict valueForKey:@"Visit_Date"]];
                [self FetchLocationDetailsForSelectedLoctaion];
                [self FetchDoctorOrContactDetailsForSelectedLocation];
            });
        });
        
    }
    
    if(currentVisitEditType==KVisitReschedule){
        [locationTextField setIsReadOnly:YES];
        [visitTypeTextField setIsReadOnly:YES];
        [addDoctorBtn setHidden:YES];
        [AddLocationButton setHidden:YES];
        [objectiveTextView setIsReadOnly:YES];
        [pendingVisitSwitch setEnabled:NO];
    }

}

-(void)FetchLocationDetailsForSelectedLoctaion
{
    
    if ([[visitDetailsDict valueForKey:@"Location_Type"] isEqualToString:@"D"]) {
        NSString* selectedDocID=[visitDetailsDict valueForKey:@"Doctor_ID"];
        NSMutableArray* tempLocationsArray=[MedRepQueries fetchLocationDetailsforDoctorID:selectedDocID];
        if (tempLocationsArray.count>0) {
            locationsarray=tempLocationsArray;
        }
    }
    else if([[visitDetailsDict valueForKey:@"Location_Type"] isEqualToString:@"P"])
    {
        NSString* selectedLocID=[visitDetailsDict valueForKey:@"Location_ID"];
        NSPredicate * locationsPred=[NSPredicate predicateWithFormat:@"Self.Location_ID == %@", selectedLocID];
        NSArray* testArray=[locationsarray filteredArrayUsingPredicate:locationsPred];
        locationsarray=[testArray mutableCopy];
    }
}
-(void)FetchDoctorOrContactDetailsForSelectedLocation
{
    if ([locationType isEqualToString:@"P"]) {
        
        //            addDoctorBtn.enabled=NO;
        //            doctorPickerBtn.enabled=NO;
        
        
        //****check with param **///
        
        
        //fetch contact details for pharmacy with location ID, beware there may be a pharmacy location without any contact, if there is a pharmacy without contacts how to create a visit for that pharmacy?, and if am creating a new location(pharmacy) then what abt its contacts? do i need to create a contact associated with that pharmacy and insert data in PHX_TBL_LOCATION_CONTACT_MAP  and TBL_Contacts?
        
        
        //answer to above query, creating a contact is being added to screen , now user has to create a contact whicle creating location.and relavent data is being added to PHX_TBL_Contacts,PHX_TBL_LOCATION_CONTACT_MAP, so when ever user tries to create visit for pharmacy check get contactID from  PHX_TBL_LOCATION_CONTACT_MAP based on location ID and based on that contact ID fetch details from PHX_TBL_Contacts
        
        pharmacyContactDetailsArray=[MedRepQueries fetchContactsforPharmacywithLocationID:[visitDetailsDict valueForKey:@"Location_ID"]];
        
        if (pharmacyContactDetailsArray.count>0) {
            noContactAvailableforLoction=NO;
        }
        
        else
        {
            noContactAvailableforLoction=YES;
            
        }
        
        
        pharmacyContactNamesArray=[[NSMutableArray alloc]init];
        
        
        for (NSInteger i=0; i<pharmacyContactDetailsArray.count; i++) {
            
            [pharmacyContactNamesArray addObject:[[pharmacyContactDetailsArray objectAtIndex:i] valueForKey:@"Contact_Name"]];
            
            
        }
        
        if ([[visitDetailsDict valueForKey:@"Doctor_ID"] length]>0||[[visitDetailsDict valueForKey:@"Contact_ID"] length]>0 ){
            
            //doctor/contact already selected before selecting location so dont reset
            
        }
        
        else
        {
            [visitDetailsDict removeObjectForKey:@"Doctor_Name"];
            [visitDetailsDict removeObjectForKey:@"Doctor_ID"];
            doctorTextField.text=@"";
            
        }
        
    }
    
    else
    {
        [doctorTextField setIsReadOnly:NO];
        if ([appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode] ) {
            [addDoctorBtn setEnabled:YES];
        }
        
        doctorsArray=[MedRepQueries fetchDoctorswithLocationID:[visitDetailsDict valueForKey:@"Location_ID"]];
        
        if (doctorsArray.count==0) {
            
            noContactAvailableforLoction=YES;
        }
        
        else
        {
            noContactAvailableforLoction=NO;
        }
        
        
        
    }
    

}
-(void)close
{
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action){

                                       [self dismissViewControllerAnimated:YES completion:^{
                                           if ([self.dismissDelegate respondsToSelector:@selector(createVisitDidCancel)]){
                                               [self.dismissDelegate createVisitDidCancel];
                                           }
                                       }];;
                               }];
    UIAlertAction* CancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
    [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:KVisitCreationPopOverCancelAlertViewTitleStr andMessage:KVisitCreationPopOverCancelAlertViewMessageStr andActions:actionsArray withController:self];
   
    
}


-(void)save
{
    //make objective optional suggested by manish on 21/2/2016
    
    [self.view endEditing:YES];
    
    NSString* missingString;
    
    
    if (locationTextField.text.length==0) {
        
        missingString =@"Please enter Location";
    }
    else if ([appCntrl.ALLOW_FM_VISIT_WITHOUT_CONT_DR isEqualToString:KAppControlsNOCode] && doctorTextField.text.length == 0) {
            missingString =@"Doctor/Contact";
    }
    
    else  if (dateTextField.text.length==0) {
        
        missingString =@"Please enter Date";
    }
    else   if (demoPlanTextField.text.length==0) {
        
        missingString =@"Please enter Demo Plan";
    }
    else if (visitTypeTextField.text.trimString.length==0) {
        if(NovisitSegmgmentedControl.selectedSegmentIndex==1)
            missingString =@"Please enter Reason";
        else
            missingString =@"Please enter Visit Type";
    }
    else  if (objectiveTextView.text.trimString.length==0 && ([appCntrl.IS_FM_OBJECTIVE_MANDATORY isEqualToString:KAppControlsYESCode] || NovisitSegmgmentedControl.selectedSegmentIndex==1)) {
        
        if(NovisitSegmgmentedControl.selectedSegmentIndex==1){
            missingString =@"Please enter Comments";

        }else{
            missingString =@"Please enter Objective";
        }
    }
    else
    {
        
    }
    
    BOOL wordLimitValid= [self validateWordLengthforTextView];

    if (missingString.length > 0) {
        [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:missingString withController:self];
    }
   /* else if ((!wordLimitValid && [appCntrl.IS_FM_OBJECTIVE_MANDATORY isEqualToString:KAppControlsYESCode] && [appCntrl.FM_ENABLE_WORD_LIMIT isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:objectiveWordLimit]))
    {
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           self.navigationItem.rightBarButtonItem.enabled=YES;
                                           
                                       }];
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"Objective",nil)
                                      message:[NSString stringWithFormat:@"Minimum %@ words required in objective",objectiveWordLimit]
                                      preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:^{
            [objectiveTextView becomeFirstResponder];
        }];
    }*/
    else {
        self.navigationItem.rightBarButtonItem.enabled=NO;
        [visitDetailsDict setValue:objectiveTextView.text forKey:@"Objective"];
        
        // contact id to be doctor id if visiting a doctor, and contact id to be location id if visiting pharmacy
        
        //this has been chnaged now as we have contact id while creating a location
        
        NSString* contactID;
        
        if ([locationType isEqualToString:@"P"]) {
            contactID=[visitDetailsDict valueForKey:@"Contact_ID"];
        }
        
        else if ([locationType isEqualToString:@"D"])
        {
            contactID=[visitDetailsDict valueForKey:@"Contact_ID"];
            
        }
        
        [visitDetailsDict setValue:contactID forKey:@"Contact_ID"];
        
        [visitDetailsDict setValue:@"N" forKey:@"Visit_Status"];
        
        //check captured data
        NSLog(@"visit details dictionary is %@", [visitDetailsDict description]);
        
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr];
        [formatterTime setDateFormat:@"dd/MM/yyyy hh:mm a"];
        [formatterTime setLocale:usLocaleq];
        NSString* visitDateStr=[[NSString alloc]init];
        if (dateTextField.text.length>0) {
            visitDateStr=[MedRepDefaults refineDateFormat:kDateFormatWithTime destFormat:kDateFormatWithTime scrString:dateTextField.text];
        }
        else
        {
            visitDateStr=[formatterTime stringFromDate:[NSDate date]];
        }
        
        [visitDetailsDict setValue:visitDateStr forKey:@"Visit_Date"];
        
        if (locationType) {
            [visitDetailsDict setValue:locationType forKey:@"Location_Type"];
        }
        visitDetailsDict=[[[MedRepDefaults alloc]init]DeleteEmptyStringKeysInDic:visitDetailsDict];
        NSMutableDictionary *tempVisitDetails=[visitDetailsDict mutableCopy];
        BOOL updateOldVisitDetails=YES;
        if(currentVisitEditType==KVisitUpdate || currentVisitEditType==KVisitReschedule)
        {
            
            updateOldVisitDetails=[MedRepQueries updateVisitDetailsToOldvisit:[selectedEditVisitDetails valueForKey:@"Planned_Visit_ID"]];
            [tempVisitDetails setValue:[selectedEditVisitDetails valueForKey:@"Planned_Visit_ID"] forKey:@"Old_Visit_ID"];
        }
        
        if(updateOldVisitDetails)
        {
            plannedVisitIDfromCreateVisit=[MedRepQueries createVisitandReturnPlannedVisitID:tempVisitDetails];
        }
        else
        {
            
            [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:KVisitCreationVisitDetailsNotAvailableMessageStr withController:self];
        }
        
        if (isNewLocation ==YES && isNewDoctor== NO && [[SWDefaults getValidStringValue:[visitDetailsDict valueForKey:@"Location_Type"]] isEqualToString:kDoctorLocation] ) {
            //link location with existing doctor
            NSString* doctorID=[visitDetailsDict valueForKey:@"Doctor_ID"];
            if ([NSString isEmpty:doctorID]==NO) {
                BOOL status=[MedRepQueries insertDoctorLocationMapwithLocationID:[visitDetailsDict valueForKey:@"Location_ID"] andDoctorID:[visitDetailsDict valueForKey:@"Doctor_ID"]];
                if (status==YES) {
                    
                    NSLog(@"New location mapped to existing doctor successfully");
                    
                }
            }
        }
        
        
        if (plannedVisitIDfromCreateVisit.length>0) {
            
            [visitDetailsDict setObject:plannedVisitIDfromCreateVisit forKey:@"Planned_Visit_ID"];
            
        }
        
        
        if (plannedVisitIDfromCreateVisit.length>0) {
            NSLog(@"visit created successfully");
            
            
            
            if ([objectiveTextView isFirstResponder]) {
                
                [objectiveTextView resignFirstResponder];
                
                
            }
            
            //  NSLog(@"visit details dict before saving visit %@", [visitDetailsDict description]);
            
            
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                
                UIAlertAction* okAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                               self.navigationItem.rightBarButtonItem.enabled=YES;

                                                   [self dismissViewControllerAnimated:YES completion:^{
                                                       if([self.dismissDelegate respondsToSelector:@selector(newVisitCreated:StartVisit:AndPlannedVisitId:)])
                                                       {
                                                           if (isNewLocation==YES || isNewDoctor==YES  || isNewContact==YES)
                                                           {
                                                               [self.dismissDelegate newVisitCreated:YES StartVisit:YES AndPlannedVisitId:[visitDetailsDict valueForKey:@"Planned_Visit_ID"]];
                                                           }
                                                           else if (currentVisitEditType==KVisitUpdate || currentVisitEditType==KVisitReschedule)
                                                           {
                                                               [self.dismissDelegate VisitUpdated:YES ];
                                                           }
                                                           else
                                                           {
                                                               [self.dismissDelegate newVisitCreated:YES StartVisit:NO AndPlannedVisitId:[visitDetailsDict valueForKey:@"Planned_Visit_ID"]];
                                                           }
                                                       }
                                                   }];;
                                               
                                           }];
                NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,nil];
                [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:KKVisitCreatedSuccessAlertTitkeStr andMessage:currentVisitEditType==KVisitCreate?KVisitCreatedSuccessAlertMessage:KVisitUpdatedSuccessAlertMessage andActions:actionsArray withController:self];
                
                
            });
        }
        
        else
        {
            NSLog(@"VISIT CREATING FAILED");
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {

}


#pragma mark - keyboard movements
- (void)keyboardDidShow:(NSNotification *)notification
{
    
    NSLog(@"popover content view %@ ", popOverController.contentViewController.view);
    
    
    
    oldFrame=self.view.frame;
    
    
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, -300, self.view.frame.size.width, self.view.frame.size.height+500);
        
        
    } completion:^(BOOL finished) {
        NSLog(@"animation completion called");

        [self.view layoutIfNeeded];
        

        NSLog(@"view frame is %@", NSStringFromCGRect(self.view.frame));
    }];
    

    

    
}

-(void)removeKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    
}


-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.view cache:YES];
    self.view.frame = oldFrame;
    [UIView commitAnimations];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView DataSource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (plannedVisitsArray.count>0) {
        return plannedVisitsArray.count;
    }
    else
    {
    return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"medRepCell";
    
    MedRepTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepTableViewCell" owner:nil options:nil] firstObject];
        
    }
    
    
    NSString* doctorID=[[plannedVisitsArray valueForKey:@"Doctor_ID"] objectAtIndex:indexPath.row];
    
    NSString* doctorName=[MedRepQueries fetchDoctorwithID:doctorID];
    
    if (doctorName.length>0) {
        
        cell.titleLbl.text=doctorName;
    }
    
    //cell.titleLbl.text=@"TEST";
    return cell;

}

-(NSString*)fetchVisitStartTime

{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    return formattedDateString;
}

#pragma mark - StringPopOver Delegate

-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDatabseDefaultDateFormat scrString:selectedDate];
    [self getDemoPlansForDate:refinedDate];
    dateTextField.text = [MedRepDefaults refineDateFormat:@"dd/MM/yyyy hh:mm a" destFormat:kDateFormatWithTime scrString:selectedDate];
    [visitDetailsDict setValue:refinedDate forKey:@"Visit_Date"];
    if(NovisitSegmgmentedControl.selectedSegmentIndex==1){
        [self AddDummyDemoPlanFoNoVisit];
    }
    else if (demoPlanSpecializationMatched == NO)
    {
        [self discardSelectedDemoPlan];

    }

}
-(void)getDemoPlansForDate:(NSString *)refinedDate
{
    
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyy-MM-dd HH:mm:ss"];
    [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr]];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithName:@"GMT"];
    [df1 setTimeZone:gmt];
    
    NSDate *dtPostDate = [df1 dateFromString:refinedDate];
    NSLog(@"new date %@", dtPostDate);
    
    NSDate *selectedVisitdate = [df1 dateFromString:refinedDate];
    demoPlanArray =[[NSMutableArray alloc]init];
    
//    BOOL isDemoAvbl=[MedRepDefaults isDemoPlanAvailableforDate:dtPostDate];
//    if (isDemoAvbl==YES)
//    {
//    }
    
    demoPlanArray=[MedRepQueries fetchDemoPlansforSelectedDate:selectedVisitdate];

    //select a demo plan based on doctor specialization added as part of requirement from INMA on 2018-09-27
    //flag FM_ENABLE_DEMO_PLAN_SPECILIZATION added
    NSString* enableDemoPlanSpecialization = [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_DEMO_PLAN_SPECIALIZATION];
    NSString* selectedDoctorSpecialization = [NSString getValidStringValue:[visitDetailsDict valueForKey:@"Specialization"]];

    if ([enableDemoPlanSpecialization isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:selectedDoctorSpecialization])
    {
        NSLog(@"demoplan Array is %@ and selectedDoctor is %@", demoPlanArray,visitDetailsDict);
        NSPredicate* selectedDemoPlanPredicate = [NSPredicate predicateWithFormat:@"SELF.Specialization ==[cd] %@",selectedDoctorSpecialization];
        NSMutableArray* selectedDemoPlanArray = [[demoPlanArray filteredArrayUsingPredicate:selectedDemoPlanPredicate] mutableCopy];
        NSLog(@"demo plan based on specialization is %@",selectedDemoPlanArray);
        
        if (selectedDemoPlanArray.count>0)
        {
            NSMutableDictionary* matchedDemoPlanDictionary = [selectedDemoPlanArray objectAtIndex:0];
            NSString* demoPlanDescription = [NSString getValidStringValue:[matchedDemoPlanDictionary valueForKey:@"Description"]];
            NSString* demoplanID = [NSString getValidStringValue:[matchedDemoPlanDictionary valueForKey:@"Demo_Plan_ID"]];
           
            demoPlanTextField.text= demoPlanDescription;
            [visitDetailsDict setValue:demoPlanDescription forKey:@"Demonstration_Plan"];
            [visitDetailsDict setValue:demoplanID forKey:@"Demo_Plan_ID"];
            demoPlanSpecializationMatched = YES;
        }
        
        
    }
    
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:selectedVisitdate];
    if (today==YES)
    {
        isFutureVisit=NO;
    }
    else if (today==NO)
    {
        isFutureVisit=YES;
    }

}

#pragma mark selected value delegate

-(void)selectedValueforCreateVisit:(NSIndexPath *)indexPath
{
    
   
    
    
    if ([popUpString isEqualToString:@"New Location"])
    {
        doctorTextField.text=[NSString stringWithFormat:@"%@", [[doctorsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]];
        [visitDetailsDict setValue:[[doctorsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"] forKey:@"Doctor_Name"];
        [visitDetailsDict setValue:[[doctorsArray objectAtIndex:indexPath.row] valueForKey:@"Doctor_ID"] forKey:@"Doctor_ID"];

    }
    else  if ([popUpString isEqualToString:@"Contact"])
    {
        //the following logic is for suppose there is a doctor ahmed who works in different locations, and while creating visit user first selected doctor as ahmed, now we need to show all the locations where ahmed works, written a saperate query for that, instaed of displaying multiple records for ahmad in doctors list, refined ahmad using predicate with doctor id and  wrote a query to fetch locations for ahmad. this case will not be valid for pharmacy beacuse a particular contact can not work at multiple pharmacies.
        [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Doctor_Name"];

        NSMutableArray *tempArrayContact;
        
        if (isPendingVisitEnabled){
            tempArrayContact = tempArrayWhenPendingVisitsEnabled;
        } else {
            tempArrayContact = tempContactsArray;
        }
        
        NSString * selectedContactType = [[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Location_Type"];
        
        if ([selectedContactType isEqualToString:@"D"])
        {
            
            NSString* selectedDocID = [[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Doctor_ID"];
            NSLog(@"selected doc id is %@", selectedDocID);
            
            NSMutableArray* tempLocationsArray = [MedRepQueries fetchLocationDetailsforDoctorID:selectedDocID];
            if (tempLocationsArray.count>0)
            {
                locationsarray=tempLocationsArray;
            }
        }
        else
        {
            NSString* selectedLocID = [[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Location_ID"];
            NSPredicate * locationsPred = [NSPredicate predicateWithFormat:@"Self.Location_ID == %@", selectedLocID];
            
            NSArray* testArray = [locationsarray filteredArrayUsingPredicate:locationsPred];
            locationsarray = [testArray mutableCopy];
        }
        
        doctorTextField.text=[NSString stringWithFormat:@"%@", [[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]];
        
        NSString* selectedLocType=[[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Location_Type"];
        if ([selectedLocType isEqualToString:@"D"])
        {
            [visitDetailsDict setValue:[[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"] forKey:@"Doctor_Name"];
            [visitDetailsDict setValue:[[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Doctor_ID"] forKey:@"Doctor_ID"];
            [visitDetailsDict setValue:[NSString getValidStringValue:[[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Specialization"]] forKey:@"Specialization"];

            doctorContactTitle.text=kDoctorTitle;

        }
        
        else
        {
            [visitDetailsDict setValue:[[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"] forKey:@"Contact_Name"];
            [visitDetailsDict setValue:[[tempArrayContact objectAtIndex:indexPath.row] valueForKey:@"Contact_ID"] forKey:@"Contact_ID"];
            doctorContactTitle.text=kContactTitle;

        }
        isNewContact=NO;


    }
    else if ([popUpString isEqualToString:@"Location"])
    {
        if ([appCntrl.DISABLE_FM_CREATE_NEW_CONTACT isEqualToString:KAppControlsYESCode] && [appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode]) {
            addDoctorBtn.enabled = NO;
        }else{
          addDoctorBtn.enabled = YES;
        }
        
        demoPlanSpecializationMatched = NO;
        [self discardSelectedDemoPlan];

        locationLastselectedIndexPath = indexPath;
        locationTextField.text=[[NSString stringWithFormat:@" %@", [[locationsarray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row]]capitalizedString];
        isNewLocation=NO;
        //now fetch doctors based on this location
 
        locationType=[NSString stringWithFormat:@"%@", [[locationsarray valueForKey:@"Location_Type"] objectAtIndex:indexPath.row]];
        
        if ([locationType isEqualToString:@"P"])
        {
            //****check with param **///
            
            
            //fetch contact details for pharmacy with location ID, beware there may be a pharmacy location without any contact, if there is a pharmacy without contacts how to create a visit for that pharmacy?, and if am creating a new location(pharmacy) then what abt its contacts? do i need to create a contact associated with that pharmacy and insert data in PHX_TBL_LOCATION_CONTACT_MAP  and TBL_Contacts?
            
            
            //answer to above query, creating a contact is being added to screen , now user has to create a contact whicle creating location.and relavent data is being added to PHX_TBL_Contacts,PHX_TBL_LOCATION_CONTACT_MAP, so when ever user tries to create visit for pharmacy check get contactID from  PHX_TBL_LOCATION_CONTACT_MAP based on location ID and based on that contact ID fetch details from PHX_TBL_Contacts

            /*
             no need to check visit require and completed for pharmacies
             
            if (isPendingVisitEnabled)
            {
                pharmacyContactDetailsArray = [[NSMutableArray alloc]init];
                NSMutableArray *arr = [MedRepQueries fetchContactsforPharmacywithLocationIdWhenPendingVisitsEnabled:[[locationsarray valueForKey:@"Location_ID"] objectAtIndex:indexPath.row]];
                
                for (NSInteger i=0; i<arr.count; i++)
                {
                    NSMutableDictionary* currentDict = [arr objectAtIndex:i];
                    
                    if ([[currentDict stringForKey:@"CompletedVisit"]intValue] < [[currentDict stringForKey:@"Visits_Req"]intValue])
                    {
                        NSDictionary *newDict = [[NSMutableDictionary alloc]init];
                        [newDict setValue:[currentDict stringForKey:@"Contact_Name"] forKey:@"Contact_Name"];
                        [newDict setValue:[currentDict stringForKey:@"Contact_ID"] forKey:@"Contact_ID"];
                        [newDict setValue:[currentDict stringForKey:@"Location_ID"] forKey:@"Location_ID"];
                        [newDict setValue:[currentDict stringForKey:@"Contact_Code"] forKey:@"Contact_Code"];
                        
                        [pharmacyContactDetailsArray addObject:newDict];
                    }
                }
            }
            else
            {
                pharmacyContactDetailsArray = [MedRepQueries fetchContactsforPharmacywithLocationID:[[locationsarray valueForKey:@"Location_ID"] objectAtIndex:indexPath.row]];
            }
            
            */
            
            pharmacyContactDetailsArray = [MedRepQueries fetchContactsforPharmacywithLocationID:[[locationsarray valueForKey:@"Location_ID"] objectAtIndex:indexPath.row]];
            
            if (pharmacyContactDetailsArray.count>0) {
                noContactAvailableforLoction=NO;
            }
            else
            {
                noContactAvailableforLoction=YES;
            }
            
            pharmacyContactNamesArray=[[NSMutableArray alloc]init];
            
            for (NSInteger i=0; i<pharmacyContactDetailsArray.count; i++)
            {
                [pharmacyContactNamesArray addObject:[[pharmacyContactDetailsArray objectAtIndex:i] valueForKey:@"Contact_Name"]];
            }
            
            if ( [[visitDetailsDict valueForKey:@"Doctor_ID"] length]>0||[[visitDetailsDict valueForKey:@"Contact_ID"] length]>0 )
            {
                //doctor/contact already selected before selecting location so dont reset
            }
            else
            {
                [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
                [visitDetailsDict setValue:@"" forKey:@"Doctor_Name"];

                doctorTextField.text=@"";
            }
        }
        else
        {
            [doctorTextField setIsReadOnly:NO];
            
            if ([appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode] && [appCntrl.DISABLE_FM_CREATE_NEW_CONTACT isEqualToString:KAppControlsYESCode]) {
                addDoctorBtn.enabled = NO;
            }
            
            if (isPendingVisitEnabled)
            {
                doctorsArray = [[NSMutableArray alloc]init];
                doctorsArray = [MedRepQueries fetchDoctorswithLocationIDWhenPendingVisitsEnabled:[[locationsarray valueForKey:@"Location_ID"] objectAtIndex:indexPath.row]];
                
               // NSLog(@"pending visit response %@", doctorsArray);
                
               /*
                for (NSInteger i=0; i<arr.count; i++)
                {
                    NSMutableDictionary* currentDict = [arr objectAtIndex:i];
                    if ([[currentDict stringForKey:@"CompletedVisit"]intValue] < [[currentDict stringForKey:@"Visits_Req"]intValue])
                    {
                        NSDictionary *newDict = [[NSMutableDictionary alloc]init];
                        [newDict setValue:[currentDict stringForKey:@"Address_1"] forKey:@"Address_1"];
                        [newDict setValue:[currentDict stringForKey:@"Address_2"] forKey:@"Address_2"];
                        [newDict setValue:[currentDict stringForKey:@"Address_3"] forKey:@"Address_3"];
                        [newDict setValue:[currentDict stringForKey:@"City"] forKey:@"City"];
                        [newDict setValue:[currentDict stringForKey:@"Class"] forKey:@"Class"];
                        [newDict setValue:[currentDict stringForKey:@"Created_At"] forKey:@"Created_At"];
                        [newDict setValue:[currentDict stringForKey:@"Doctor_Code"] forKey:@"Doctor_Code"];
                        [newDict setValue:[currentDict stringForKey:@"Doctor_ID"] forKey:@"Doctor_ID"];
                        [newDict setValue:[currentDict stringForKey:@"Doctor_Name"] forKey:@"Doctor_Name"];
                        [newDict setValue:[currentDict stringForKey:@"Email"] forKey:@"Email"];
                        [newDict setValue:[currentDict stringForKey:@"IS_Active"] forKey:@"IS_Active"];
                        [newDict setValue:[currentDict stringForKey:@"IS_Deleted"] forKey:@"IS_Deleted"];
                        [newDict setValue:[currentDict stringForKey:@"Name_2"] forKey:@"Name_2"];
                        [newDict setValue:[currentDict stringForKey:@"Name_3"] forKey:@"Name_3"];
                        [newDict setValue:[currentDict stringForKey:@"OTCRX"] forKey:@"OTCRX"];
                        [newDict setValue:[currentDict stringForKey:@"Phone"] forKey:@"Phone"];
                        [newDict setValue:[currentDict stringForKey:@"Postal_Code"] forKey:@"Postal_Code"];
                        [newDict setValue:[currentDict stringForKey:@"Rating_Info"] forKey:@"Rating_Info"];
                        [newDict setValue:[currentDict stringForKey:@"Specialization"] forKey:@"Specialization"];
                        [newDict setValue:[currentDict stringForKey:@"State"] forKey:@"State"];
                        [newDict setValue:[currentDict stringForKey:@"Title"] forKey:@"Title"];
                        [newDict setValue:[currentDict stringForKey:@"Trade_Channel"] forKey:@"Trade_Channel"];
                        
                        [doctorsArray addObject:newDict];
                    }
                }
                */
            }
            else
            {
                doctorsArray=[MedRepQueries fetchDoctorswithLocationID:[[locationsarray valueForKey:@"Location_ID"] objectAtIndex:indexPath.row]];
            }
            
            if (doctorsArray.count==0) {
                
                noContactAvailableforLoction=YES;
            }
            else
            {
                noContactAvailableforLoction=NO;
            }
        }
        
        
        locationIDforExistingLocationNewDoc=[[locationsarray objectAtIndex:indexPath.row] valueForKey:@"Location_ID"];
        
        NSLog(@"doctors for selected location %@", [doctorsArray description]);
        
        [visitDetailsDict setValue:[[locationsarray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row] forKey:@"Location_Name"];
        [visitDetailsDict setValue:[[locationsarray valueForKey:@"Location_ID"] objectAtIndex:indexPath.row] forKey:@"Location_ID"];
        [visitDetailsDict setValue:locationType forKey:@"Location_Type"];

        
        //validate this case, if location is selected first and then doctor is selected, now again if user taps on location and changes to a pharmacy, doctor details are cleared if user taps on another hospital, previous selected doctor is still there so here after selecting location check if the selected doctor id matches with location in location contact map, if not then clear doctor credentials, and make the field empty
        
        
        if ([locationType isEqualToString:@"P"])
        {
            BOOL contactMatcheswithLocation=[MedRepQueries contactMatcheswithLocation:[visitDetailsDict valueForKey:@"Location_ID"] andDoctorID:[visitDetailsDict valueForKey:@"Contact_ID"]];
            
            NSLog(@"CONTACT MATCHES WITH LOCATION? %hhd", contactMatcheswithLocation);
            
            if (contactMatcheswithLocation) {
                
            }
            else
            {
                [visitDetailsDict removeObjectForKey:@"Contact_ID"];
                [visitDetailsDict removeObjectForKey:@"Contact_Name"];
                doctorLbl.text=@"";
                doctorTextField.text=@"";
            }
            doctorContactTitle.text=kContactTitle;

        }
        else
        {
            BOOL  doctorMatcheswithLocation=[MedRepQueries doctorMatcheswithLocation:[visitDetailsDict valueForKey:@"Location_ID"] andDoctorID:[visitDetailsDict valueForKey:@"Doctor_ID"]];
            if (doctorMatcheswithLocation)
            {
                
            }
            else
            {
                [visitDetailsDict removeObjectForKey:@"Doctor_ID"];
                [visitDetailsDict removeObjectForKey:@"Doctor_Name"];
                doctorLbl.text=@"";
                doctorTextField.text=@"";
            }
            doctorContactTitle.text=kDoctorTitle;

            
            NSLog(@"SELECTED DOCTOR MATCHES WITH LOCATION? %hhd",doctorMatcheswithLocation );
            NSLog(@"doctor id in location tapped is %@", [visitDetailsDict valueForKey:@"Doctor_ID"]);
            NSLog(@"location id in location tapped is %@", [visitDetailsDict valueForKey:@"Location_ID"]);
        }
        
        if (locationpopoverController)
        {
            [locationpopoverController dismissPopoverAnimated:YES];
            locationpopoverController = nil;
            locationpopoverController.delegate=nil;
        }
        
        isNewLocation=NO;
        if(isNewDoctor==NO)
        {
            datePickerBtn.enabled=YES;
            dateTextField.enabled=YES;
        }
        else
        {
            datePickerBtn.enabled=NO;
            dateTextField.enabled=NO;
        }
    }
    
    else if ([popUpString isEqualToString:@"Demo Plan"])
    {
        demoPlanTextField.text=[NSString stringWithFormat:@"%@", [[demoPlanArray valueForKey:@"Description"] objectAtIndex:indexPath.row]];
        [visitDetailsDict setValue:[[demoPlanArray valueForKey:@"Description"] objectAtIndex:indexPath.row] forKey:@"Demonstration_Plan"];
        [visitDetailsDict setValue:[[demoPlanArray valueForKey:@"Demo_Plan_ID"] objectAtIndex:indexPath.row] forKey:@"Demo_Plan_ID"];
        
        if (demonstrationPlanpopOverController)
        {
            [demonstrationPlanpopOverController dismissPopoverAnimated:YES];
            demonstrationPlanpopOverController = nil;
            demonstrationPlanpopOverController.delegate=nil;
        }
    }
    else if ([popUpString isEqualToString:@"Doctors"])
    {
        doctorTextField.text=[[NSString stringWithFormat:@" %@", [[doctorsArray valueForKey:@"Doctor_Name"] objectAtIndex:indexPath.row]]capitalizedString];
        [visitDetailsDict setValue:[[doctorsArray valueForKey:@"Doctor_Name"] objectAtIndex:indexPath.row] forKey:@"Doctor_Name"];
        [visitDetailsDict setValue:[[doctorsArray valueForKey:@"Doctor_ID"] objectAtIndex:indexPath.row] forKey:@"Doctor_ID"];
        [visitDetailsDict setValue:[NSString getValidStringValue:[[doctorsArray valueForKey:@"Specialization"] objectAtIndex:indexPath.row]] forKey:@"Specialization"];

        //select a demo plan based on doctor specialization added as part of requirement from INMA on 2018-09-27
         //flag FM_ENABLE_DEMO_PLAN_SPECILIZATION added
         NSString* enableDemoPlanSpecialization = [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_DEMO_PLAN_SPECIALIZATION];
         NSString* selectedDoctorSpecialization = [NSString getValidStringValue:[visitDetailsDict valueForKey:@"Specialization"]];
         
         if ([enableDemoPlanSpecialization isEqualToString:KAppControlsYESCode] && ![NSString isEmpty:selectedDoctorSpecialization])
         {
         NSLog(@"demoplan Array is %@ and selectedDoctor is %@", demoPlanArray,visitDetailsDict);
         NSPredicate* selectedDemoPlanPredicate = [NSPredicate predicateWithFormat:@"SELF.Specialization ==[cd] %@",selectedDoctorSpecialization];
         NSMutableArray* selectedDemoPlanArray = [[demoPlanArray filteredArrayUsingPredicate:selectedDemoPlanPredicate] mutableCopy];
         NSLog(@"demo plan based on specialization is %@",selectedDemoPlanArray);
         
         if (selectedDemoPlanArray.count>0)
         {
         NSMutableDictionary* matchedDemoPlanDictionary = [selectedDemoPlanArray objectAtIndex:0];
         NSString* demoPlanDescription = [NSString getValidStringValue:[matchedDemoPlanDictionary valueForKey:@"Description"]];
         NSString* demoplanID = [NSString getValidStringValue:[matchedDemoPlanDictionary valueForKey:@"Demo_Plan_ID"]];
         
         demoPlanTextField.text= demoPlanDescription;
         [visitDetailsDict setValue:demoPlanDescription forKey:@"Demonstration_Plan"];
         [visitDetailsDict setValue:demoplanID forKey:@"Demo_Plan_ID"];
         demoPlanSpecializationMatched = YES;
         }
         
         
         }
        NSLog(@"doctor ID in visit details %@", [visitDetailsDict valueForKey:@"Doctor_ID"]);
        
        if (doctorpopOverController)
        {
            [doctorpopOverController dismissPopoverAnimated:YES];
            doctorpopOverController = nil;
            doctorpopOverController.delegate=nil;
        }
        isNewDoctor=NO;
        if(isNewLocation==NO)
        {
            datePickerBtn.enabled=YES;
            dateTextField.enabled=YES;
        }
        else
        {
            datePickerBtn.enabled=NO;
            dateTextField.enabled=NO;
        }
        isNewContact=NO;
        doctorContactTitle.text=kDoctorTitle;
        
        //fetch demo plans linked to doctor

    }
    else if ([popUpString isEqualToString:@"Pharmacy"])
    {
        if ([locationType isEqualToString:@"P"])
        {
            //****check with param **///
            
            
            //fetch contact details for pharmacy with location ID, beware there may be a pharmacy location without any contact, if there is a pharmacy without contacts how to create a visit for that pharmacy?, and if am creating a new location(pharmacy) then what abt its contacts? do i need to create a contact associated with that pharmacy and insert data in PHX_TBL_LOCATION_CONTACT_MAP  and TBL_Contacts?
            
            
            //answer to above query, creating a contact is being added to screen , now user has to create a contact whicle creating location.and relavent data is being added to PHX_TBL_Contacts,PHX_TBL_LOCATION_CONTACT_MAP, so when ever user tries to create visit for pharmacy check get contactID from  PHX_TBL_LOCATION_CONTACT_MAP based on location ID and based on that contact ID fetch details from PHX_TBL_Contacts
            
            
            
            //            pharmacyContactDetailsArray=[MedRepQueries fetchContactsforPharmacywithLocationID:[[locationsarray valueForKey:@"Location_ID"] objectAtIndex:indexPath.row]];
            //
            //            NSLog(@"Contact Details for %@, %@ ",[[locationsarray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row],[pharmacyContactDetailsArray description]);
            //
            //
            //            pharmacyContactNamesArray=[[NSMutableArray alloc]init];
            //
            //
            //            for (NSInteger i=0; i<pharmacyContactDetailsArray.count; i++) {
            //
            //                [pharmacyContactNamesArray addObject:[[pharmacyContactDetailsArray objectAtIndex:i] valueForKey:@"Contact_Name"]];
            //
            //
            //            }
            
            
            
            
            doctorTextField.text=[[NSString stringWithFormat:@" %@", [[pharmacyContactDetailsArray valueForKey:@"Contact_Name"] objectAtIndex:indexPath.row]]capitalizedString];
            [visitDetailsDict setValue:[[pharmacyContactDetailsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"] forKey:@"Contact_Name"];
            [visitDetailsDict setValue:[[pharmacyContactDetailsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_ID"] forKey:@"Contact_ID"];
            
            if (doctorpopOverController)
            {
                [doctorpopOverController dismissPopoverAnimated:YES];
                doctorpopOverController = nil;
                doctorpopOverController.delegate=nil;
            }
            isNewDoctor=NO;
            if(isNewLocation==NO)
            {
                dateTextField.enabled=YES;
                dateFieldButton.enabled=YES;
            }
            else
            {
                dateTextField.enabled=NO;
                dateFieldButton.enabled=NO;
            }
            doctorContactTitle.text=kContactTitle;

        }
    }
    else if ([popUpString isEqualToString:@"Visit Type"])
    {
        visitTypeTextField.text=[[NSString stringWithFormat:@" %@", [visitTypeArray objectAtIndex:indexPath.row]] capitalizedString];
        if(NovisitSegmgmentedControl.selectedSegmentIndex==1){
            [visitDetailsDict setValue:[[MedrepNoVisitsReasonDBArray objectAtIndex:indexPath.row] valueForKey:@"Code_Value"] forKey:@"Visit_Type"];
        }else{
            NSString* visitTypeStr=[[NSString alloc]init];
            if (indexPath.row==0){
                visitTypeStr=@"N";
            }
            else{
                visitTypeStr=@"R";
            }
            [visitDetailsDict setValue:visitTypeStr forKey:@"Visit_Type"];
        }
        
         if (visitTypepopOverController)
        {
            [visitTypepopOverController dismissPopoverAnimated:YES];
            visitTypepopOverController = nil;
            visitTypepopOverController.delegate=nil;
        }
    }
    
    NSLog(@"selected index %ld", (long)indexPath.row);
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    locationPopOver.delegate=nil;
    locationPopOver=nil;
    
    if (![NSString isEmpty:locationTextField.text] && ![NSString isEmpty:doctorTextField.text]) {
        
        
      //  NSString* previousVisitObjective=[NSString getValidStringValue:[MedRepQueries fetchPreviousVisitObjective:visitDetailsDict]];
        
       // objectiveTextView.text=previousVisitObjective;
        
       // NSLog(@"visit details dict in delegate is %@", previousVisitObjective);
    }
}

- (IBAction)NoVisitSegmentedControlValueChanged:(UISegmentedControl *)sender {

    [pendingVisitSwitch setOn:YES];
    [self toggleForPlannedVisit:pendingVisitSwitch];
    [pendingVisitSwitch setOn:NO];
    [self toggleForPlannedVisit:pendingVisitSwitch];
    [pendingVisitSwitch setOn:YES];
    [self toggleForPlannedVisit:pendingVisitSwitch];
    
    
    
        /** initializing viistdetails dic*/
        [visitDetailsDict setValue:@"" forKey:@"Contact_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Contact_Name"];
        [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Doctor_Name"];
        [visitDetailsDict setValue:@"" forKey:@"Demo_Plan_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Location_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Location_Name"];
        [visitDetailsDict setValue:@"" forKey:@"Location_Type"];
        [visitDetailsDict setValue:@"" forKey:@"Objective"];
        [visitDetailsDict setValue:@"" forKey:@"Visit_Date"];
        [visitDetailsDict setValue:@"" forKey:@"Visit_Status"];
        [visitDetailsDict setValue:@"" forKey:@"Visit_Type"];
        
        visitTypeTextField.text=@"";
        locationTextField.text=@"";
        doctorTextField.text=@"";
        dateTextField.text=@"";
        demoPlanTextField.text=@"";
        objectiveTextView.text=@"";
    

    if(sender.selectedSegmentIndex==1){

        visitTypeArray=[[NSMutableArray alloc]init];
        MedrepNoVisitsReasonDBArray=[[NSMutableArray alloc]init];
        MedrepNoVisitsReasonDBArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select * from TBL_App_Codes where Code_Type='NO_VISITS_REASON'"];
        visitTypeArray=[MedrepNoVisitsReasonDBArray valueForKey:@"Code_Description"];
        NSLog(@"visitTypeArray %@",visitTypeArray);
        NSDictionary *dummyLocationDetails= [[[SWDefaults alloc] init] getDummyMedrepVisitLocationFromPhxTblLocationTable];
        
        [visitDetailsDict setValue:[dummyLocationDetails valueForKey:@"Location_ID"] forKey:@"Location_ID"];
        [visitDetailsDict setValue:[dummyLocationDetails valueForKey:@"N/A"] forKey:@"Location_Name"];
        [visitDetailsDict setValue:[dummyLocationDetails valueForKey:@"Location_Type"] forKey:@"Location_Type"];
        [visitDetailsDict setValue:@"0" forKey:@"Demo_Plan_ID"];
        locationTextField.text=KNotApplicable;
        demoPlanTextField.text=KNotApplicable;
        
        [pendingVisitSwitch setEnabled:NO];
        [visitTypeTextField setIsReadOnly:NO];
        [locationTextField setIsReadOnly:YES];
        [doctorTextField setIsReadOnly:YES];
        [dateTextField setIsReadOnly:NO];
        [demoPlanTextField setIsReadOnly:YES];
        objectiveTextViewHeadingLabel.text=@"Comments";
        visitTypeTextfieldHeadingLabel.text=@"Reason";
        [AddLocationButton setEnabled:NO];
        [addDoctorBtn setEnabled:NO];
    }else{
        visitTypeArray=[[NSMutableArray alloc]initWithObjects:@"Normal",@"Round Table", nil];

        [visitTypeTextField setIsReadOnly:NO];
        [locationTextField setIsReadOnly:NO];
        [doctorTextField setIsReadOnly:NO];
        [dateTextField setIsReadOnly:NO];
        [demoPlanTextField setIsReadOnly:NO];
        [AddLocationButton setHidden:NO];
        [addDoctorBtn setHidden:NO];
        objectiveTextViewHeadingLabel.text=@"Objective";
        visitTypeTextfieldHeadingLabel.text=@"Visit Type";
        [pendingVisitSwitch setEnabled:YES];

        if([appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode] && [appCntrl.DISABLE_FM_CREATE_NEW_CONTACT isEqualToString:KAppControlsYESCode]){
            [addDoctorBtn setEnabled:NO];
        }else{
            [addDoctorBtn setEnabled:YES];
        }
        if([appCntrl.DISABLE_FM_CREATE_NEW_LOCATION isEqualToString:KAppControlsYESCode]){
            [AddLocationButton setEnabled:NO];
        }else{
            [AddLocationButton setEnabled:YES];
        }
    }
    

}




#pragma mark UIDatePicker delegate methods


-(void)cancel
{
    datepicker = nil;
    [datePickerpopOverController dismissPopoverAnimated:YES];
    datePickerpopOverController = nil;
}

-(void)done{
    
    NSLog(@"dat is %@", datepicker.date);

    
    demoPlanArray =[[NSMutableArray alloc]init];
    
    BOOL isDemoAvbl=[MedRepDefaults isDemoPlanAvailableforDate:datepicker.date];
    
    if (isDemoAvbl==YES) {
        
        NSLog(@"DEMO AVBL");
        
        
        
        if (isNewDoctor==YES || isNewLocation ==YES) {
            
            
        }
        demoPlanArray=[MedRepQueries fetchDemoPlansforSelectedDate:datepicker.date];

        
    }
    
    
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:datepicker.date];
    
    if (today==YES) {
        isFutureVisit=NO;
        
    }
    
    else if (today==NO)
    {
        isFutureVisit=YES;
    }
    
    NSDateFormatter  *customDateFormatter = [[NSDateFormatter alloc] init];
    [customDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [customDateFormatter setDateFormat:kDateFormatWithTime];
    dateLbl.text = [NSString stringWithFormat:@"%@",[customDateFormatter stringFromDate:datepicker.date]];
    
    datepicker = nil;
    [datePickerpopOverController dismissPopoverAnimated:YES];
    datePickerpopOverController = nil;
    
    
    
    [visitDetailsDict setValue:[MedRepDefaults refineDateFormat:kDateFormatWithTime destFormat:kDatabseDefaultDateFormat scrString:dateLbl.text] forKey:@"Visit_Date"];
    
    //selectedDateFromPicker = datepicker.date;
}


#pragma mark UITextView Delegate



- (void)textViewDidBeginEditing:(UITextView *)textView
{
        NSLog(@"Started editing target!");

}

#pragma mark UIPopOver Controller delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController == doctorpopOverController || popoverController==locationpopoverController|| popoverController==datePickerpopOverController|| popoverController==demonstrationPlanpopOverController|| popoverController==visitTypepopOverController) {
        
        return YES;
    }
    else
    {
    return NO;
    }
}
-(BOOL)validateWordLengthforTextView
{
    BOOL valid=NO;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+" options:0 error:&error];
    NSUInteger wordCount = [regex numberOfMatchesInString:objectiveTextView.text
                                                  options:0
                                                    range:NSMakeRange(0, [objectiveTextView.text length])];
    NSLog(@"word count is %lu",(unsigned long)wordCount);
    
    if (wordCount<[objectiveWordLimit integerValue]) {
        valid=NO;
    }
    else
    {
        valid = YES;
    }
    return valid;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    //[textViewScrollView setContentOffset:CGPointZero animated:YES];
    
    //[self animateTextField: textView up: NO];
    
    
    
    
    [visitDetailsDict setValue:textView.text forKey:@"Objective"];
    
    [textView resignFirstResponder];

   
    

}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    if([newString isEqualToString:@" "] || [newString isEqualToString:@"\n"])
    {
        return NO;
    }
    
    return (newString.length <= ObjectiveTextFieldLimit);
    
    return YES;
}

- (void) animateTextField: (UITextView*) textField up: (BOOL) up
{
    const int movementDistance = 200; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark Custom Delegate Methods

-(void)addLocationData:(NSMutableDictionary*)locationDetails
{
    newLocationDataDict=[[NSMutableDictionary alloc]init];
    newLocationDataDict=locationDetails;

    doctorTextField.text=@"";
    isNewLocation=YES;
   
    /* clearing the old selection doctor details*/
    [visitDetailsDict setValue:@"" forKey:@"Contact_ID"];
    [visitDetailsDict setValue:@"" forKey:@"ContactName"];
    [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
    [visitDetailsDict setValue:@"" forKey:@"Doctor"];

    
    /* set the location details*/
    [visitDetailsDict setValue:[locationDetails valueForKey:@"Location_ID"] forKey:@"Location_ID"];
    [visitDetailsDict setValue:[locationDetails valueForKey:@"Location_Name"] forKey:@"Location_Name"];
    [visitDetailsDict setValue:[locationDetails valueForKey:@"Location_Type"] forKey:@"Location_Type"];
    
    if ([[locationDetails valueForKey:@"Location_Type"] isEqualToString:@"Clinic"]||[[locationDetails valueForKey:@"Location_Type"] isEqualToString:@"Hospital"]) {
        [visitDetailsDict setValue:kDoctorLocation forKey:@"Location_Type"];
        doctorContactTitle.text=kDoctorTitle;
    }
    else if ([[locationDetails valueForKey:@"Location_Type"] isEqualToString:@"Pharmacy"])
    {
        [visitDetailsDict setValue:kPharmacyLocation forKey:@"Location_Type"];
        doctorContactTitle.text=kContactTitle;
    }
    locationTextField.text=[[NSString stringWithFormat:@"%@", [locationDetails valueForKey:@"Location_Name"]] capitalizedString];

    /** disable  location selection*/
    AddLocationButton.enabled=NO;
    [locationTextField setIsReadOnly:YES];
    
    /* disable doctor/contact selection */
    [doctorTextField setIsReadOnly:YES];
    doctorPickerBtn.enabled=NO;
    
    
    //new location created
    locationDetailsDictionary=[[NSMutableDictionary alloc]init];
    locationDetailsDictionary=locationDetails;
    
    /** show doctor/contcat name  and set add doctorbutton state*/
    
    if([[locationDetails valueForKey:@"Location_Type"] isEqualToString:@"Clinic"] || [[locationDetails valueForKey:@"Location_Type"] isEqualToString:@"Hospital"])
    {
        NSString* doctorNameStr=[locationDetails valueForKey:@"Doctor"];

        locationType=@"D";
        NSLog(@"doctorId is %@",[locationDetails valueForKey:@"Doctor_ID"]);
        if([doctorNameStr isEqualToString:@"New Doctor"])
        {
            if ([appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode] ) {
                [addDoctorBtn setEnabled:YES];
            }
            doctorTextField.text=[doctorNameStr capitalizedString];
        }
        else if ([[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Doctor_ID"] ]length]>0) {
            doctorTextField.text=[doctorNameStr capitalizedString];
            [addDoctorBtn setEnabled:NO];
            [visitDetailsDict setValue:[locationDetails valueForKey:@"Doctor_ID"] forKey:@"Doctor_ID"];
        }
        else
        {
            if ([appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode] ) {
                [addDoctorBtn setEnabled:YES];
            }
        }
    }
    else
    {
        locationType=@"P";
        NSLog(@"doctorId is %@",[locationDetails valueForKey:@"Contact_ID"]);

        if ([[SWDefaults getValidStringValue:[locationDetails valueForKey:@"Contact_ID"]] length]>0)
        {
            doctorTextField.text=[[NSString stringWithFormat:@"%@", [locationDetails valueForKey:@"ContactName"]] capitalizedString];
            [visitDetailsDict setValue:[locationDetails valueForKey:@"Contact_ID"] forKey:@"Contact_ID"];
            addDoctorBtn.enabled=NO;
        }

        else
        {
            if ([appCntrl.DISABLE_FM_CREATE_NEW_CONTACT isEqualToString:KAppControlsYESCode] ) {
                [addDoctorBtn setEnabled:NO];
            }else{
                [addDoctorBtn setEnabled:YES];
            }
        }
    }
    
    /* select current date disable date selection*/
    NSString* tempDate=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:tempDate];
    dateTextField.text=refinedDate;
    
    NSLog(@"setting date 4 %@", refinedDate);
    [visitDetailsDict setValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"Visit_Date"];
    datePickerBtn.enabled=NO;
    dateTextField.enabled=NO;
    [dateTextField setIsReadOnly:YES];
    
    


}


-(void)addDoctorData:(NSMutableDictionary*)locationDetails

{
    
    /* disable creating new contact/doctor*/
    if ([appCntrl.DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode] ) {
        [addDoctorBtn setEnabled:NO];
    }
//    addDoctorBtn.enabled=NO;
    
    /* disable doctor/contact selection */
    [doctorTextField setIsReadOnly:YES];
    doctorPickerBtn.enabled=NO;

    /** disable  location selection*/
    AddLocationButton.enabled=NO;
    [locationTextField setIsReadOnly:YES];

    
    
    isNewDoctor=YES;
    
    //now fill date field
    
    [visitDetailsDict setValue:@"" forKey:@"Contact_ID"];
    [visitDetailsDict setValue:@"" forKey:@"ContactName"];

    NSString* tempDate=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:tempDate];
    NSLog(@"setting date 1 %@", refinedDate);
    dateTextField.text=refinedDate;
    [visitDetailsDict setValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"Visit_Date"];
    NSLog(@"doctor added %@", [locationDetails description]);
    doctorTextField.text=[[locationDetails valueForKey:@"DoctorName"] capitalizedString];
    
    cretedDoctorID=[[locationDetails valueForKey:@"Doctor_ID"] capitalizedString];
    
    [visitDetailsDict setValue:cretedDoctorID forKey:@"Doctor_ID"];
    newDrDataDict=[[NSMutableDictionary alloc]init];
    
    newDrDataDict=locationDetails;
    
    if (isNewDoctor==YES) {
        
        datePickerBtn.enabled=NO;
        dateTextField.enabled=NO;
        [dateTextField setIsReadOnly:YES];

    }
    
    doctorContactTitle.text=kDoctorTitle;

    
}


- (void)visitTypeTextFieldTapped {
    
    
    
    popUpString=@"Visit Type";
    
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    
    locationPopOver.colorNames = visitTypeArray;
    
    locationPopOver.delegate = self;
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    if(NovisitSegmgmentedControl.selectedSegmentIndex==1){
        filterDescVC.descTitle=@"Reason";

    }else{
        filterDescVC.descTitle=@"Visit Type";
    }
    
    filterDescVC.customRect=self.view.frame;
    NSLog(@"current frame %@", NSStringFromCGRect(self.view.frame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=visitTypeArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];

}

- (void)locationTextFieldTapped {
    
    
    //clear previous selectiong is exists
    
    
   // doctorTextField.text=@"";
    
    

    
    //to be done
    
//    [visitDetailsDict setValue:@"" forKey:@"Doctor_Name"];
//    
//    [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
    
    [visitDetailsDict setValue:@"" forKey:@"Contact_Name"];
    
    popUpString=@"Location";
    
    
    locationpopoverController=nil;
    
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    
    
    
    NSMutableArray* locationNamesArray=[[NSMutableArray alloc]init];
    
    
//    for (NSInteger i=0; i<locationsarray.count; i++) {
//        
//        [locationNamesArray addObject:[[locationsarray valueForKey:@"Location_Name"] objectAtIndex:i]];
//    }
//
    
    
    locationNamesArray=[locationsarray valueForKey:@"Location_Name"];

    
   // NSLog(@"location names array is %@", locationNamesArray);
    
    [locationNamesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    filterDescVC.customRect=self.view.frame;
    NSLog(@"current frame %@", NSStringFromCGRect(self.view.frame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=locationNamesArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
}



- (void)doctorTextFieldTapped
{
    NSMutableArray* contentArray=[[NSMutableArray alloc]init];
    
    if ([locationType isEqualToString:@"P"])
    {
        popUpString=@"Pharmacy";
        
        doctorpopOverController=nil;
        locationPopOver = nil;
        locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
        locationPopOver.colorNames = pharmacyContactNamesArray;
        contentArray=[pharmacyContactDetailsArray valueForKey:@"Contact_Name"];
        locationPopOver.delegate = self;
        
        NSLog(@"content array in Contacts %@ %@", pharmacyContactDetailsArray,pharmacyContactNamesArray);
        
        //        if (doctorpopOverController == nil) {
        //            //The color picker popover is not showing. Show it.
        //            doctorpopOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
        //            [doctorpopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        //            doctorpopOverController.delegate=self;
        //
        //        }
        //        else {
        //            //The color picker popover is showing. Hide it.
        //            [doctorpopOverController dismissPopoverAnimated:YES];
        //            doctorpopOverController = nil;
        //            doctorpopOverController.delegate=nil;
        //        }
    }
    else
    {
        popUpString=@"Doctors";
        
        existingDoctorChanged=YES;
        doctorpopOverController=nil;
        locationPopOver = nil;
        locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
        NSMutableArray* doctorNamesArray=[[NSMutableArray alloc]init];
        

        for (NSInteger i=0; i<doctorsArray.count; i++)
        {
            NSMutableArray *arrDoctorWithSpecilization  = [[NSMutableArray alloc]init];
            [arrDoctorWithSpecilization addObject:[[doctorsArray valueForKey:@"Doctor_Name"] objectAtIndex:i]];
            [arrDoctorWithSpecilization addObject:[[doctorsArray valueForKey:@"Specialization"] objectAtIndex:i]];
            
            [doctorNamesArray addObject:arrDoctorWithSpecilization];
        }

        contentArray=doctorNamesArray;
        locationPopOver.colorNames = doctorNamesArray;
        locationPopOver.delegate = self;
        
        //    if (doctorpopOverController == nil) {
        //        //The color picker popover is not showing. Show it.
        //        doctorpopOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
        //        [doctorpopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        //        doctorpopOverController.delegate=self;
        //
        //    }
        //    else {
        //        //The color picker popover is showing. Hide it.
        //        [doctorpopOverController dismissPopoverAnimated:YES];
        //        doctorpopOverController = nil;
        //        doctorpopOverController.delegate=nil;
        //    }
    }
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.customRect=self.view.frame;
    
    
    NSLog(@"current frame %@", NSStringFromCGRect(self.view.frame));
    filterDescVC.isCreateVisit=YES;
    
    if (noContactAvailableforLoction==YES)
    {
        NSLog(@"no contact available");
    }
    else if (isNewLocation==YES)
    {
        popUpString=@"New Location";
        
        filterDescVC.filterType=@"New Location";
        doctorsArray=[MedRepQueries fetchDoctorsforCreateLocation];
        filterDescVC.filterDescArray=doctorsArray;
        
        NSLog(@"content array in new location %@", contentArray);
    }
    else if (contentArray.count==0)
    {
        if (isPendingVisitEnabled)
        {
            filterDescVC.filterDescArray = tempArrayWhenPendingVisitsEnabled;
        } else {
            filterDescVC.filterDescArray = tempContactsArray;
        }
        popUpString=@"Contact";
    }
    else
    {
        filterDescVC.filterDescArray=contentArray;
    }
    
    filterDescVC.descTitle=popUpString;
    NSLog(@"doctors array for new location count %lu", (unsigned long)contentArray.count);
    [self.navigationController pushViewController:filterDescVC animated:YES];
}


-(IBAction)dateandTimeBtnTapped:(id)sender
{
    [self dateandTimeTextFieldTapped];
}

- (void)dateandTimeTextFieldTapped{
    
    if ([appCntrl.FM_ENABLE_HOLIDAY isEqualToString:KAppControlsYESCode]) {
        MedRepCalanderwithTimeViewController* calenderVC=[[MedRepCalanderwithTimeViewController alloc]init];
        calenderVC.dateSelected = [MedRepDefaults convertToDateFrom:[visitDetailsDict valueForKey:@"Visit_Date"]];
        calenderVC.calenderDelegate=self;
        calenderVC.minimumDate=[SWDefaults beginningOfDay:[NSDate date]];
        [self.navigationController pushViewController:calenderVC animated:YES];
    } else {
        MedRepVisitsDateFilterViewController * dateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
        dateFilterVC.datePickerMode=@"dateandTime";
        dateFilterVC.visitDateDelegate=self;
        [self.navigationController pushViewController:dateFilterVC animated:YES];
    }
}
-(void)selectedDateDelegate:(NSString*)selectedDate
{
    NSLog(@"selected date in visits is %@", selectedDate);
    [self getDemoPlansForDate:[MedRepDefaults getValidStringValue:selectedDate]];
    dateTextField.text = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:selectedDate];
    
    [visitDetailsDict setValue:[MedRepDefaults getValidStringValue:selectedDate] forKey:@"Visit_Date"];
    if(NovisitSegmgmentedControl.selectedSegmentIndex==1){
        [self AddDummyDemoPlanFoNoVisit];
    }else if (demoPlanSpecializationMatched == NO){
        [self discardSelectedDemoPlan];
    }
    
}

- (void)demonstrationPlantextFieldTapped {
    
    
    if (dateTextField.text.length==0) {
        [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:KVisitCreationDemoPlanDateDependencyAlertMessageStr withController:self];
    }
    else
    {
    
    popUpString=@"Demo Plan";
    
    
    demonstrationPlanpopOverController=nil;
    
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    
    
    
    NSMutableArray* demoPlanNamesArray=[[NSMutableArray alloc]init];
    
    NSLog(@"demo plan data when button tapped %@", [demoPlanArray description]);
    
    if (demoPlanArray.count==0) {
        
        
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
        [df1 setDateFormat:@"yyy-MM-dd HH:mm:ss"];
        [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:KEnglishLocaleStr]];
        
        NSTimeZone *gmt = [NSTimeZone timeZoneWithName:@"GMT"];
        [df1 setTimeZone:gmt];

        NSString *convertedDateString = [df1 stringFromDate:[NSDate date]];// here convert date in

        NSDate * convertedDate=[df1 dateFromString:convertedDateString];
        
        
    if (isNewDoctor==YES || isNewLocation ==YES) {
        
        BOOL isDemoAvbl=[MedRepDefaults isDemoPlanAvailableforDate:convertedDate];

        if (isDemoAvbl==YES) {
            
            demoPlanArray=[MedRepQueries fetchDemoPlansforSelectedDate:convertedDate];
            
            NSLog(@"demo plan array in new doctor %@", [demoPlanArray description]);
            
        }
        
       
        
        
    }
    
    }
    
    for (NSInteger i=0; i<demoPlanArray.count; i++) {
        
        [demoPlanNamesArray addObject:[[demoPlanArray valueForKey:@"Description"] objectAtIndex:i]];
    }
    
    // NSLog(@"location names are %@",[demoPlanNamesArray description]);
    
    
    locationPopOver.colorNames = demoPlanNamesArray;
    
    locationPopOver.delegate = self;
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    filterDescVC.customRect=self.view.frame;
    NSLog(@"current frame %@", NSStringFromCGRect(self.view.frame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=demoPlanNamesArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
    //    if (demonstrationPlanpopOverController == nil) {
    //        //The color picker popover is not showing. Show it.
    //        demonstrationPlanpopOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
    //        [demonstrationPlanpopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    //        demonstrationPlanpopOverController.delegate=self;
    //    }
    //    else {
    //        //The color picker popover is showing. Hide it.
    //        [demonstrationPlanpopOverController dismissPopoverAnimated:YES];
    //        demonstrationPlanpopOverController = nil;
    //        demonstrationPlanpopOverController.delegate=nil;
    //    }
    //
    
    }
}

- (IBAction)addLocationTapped:(id)sender {
    //new location tapped so disable doctor selection , date n time
    MedRepCreateLocationViewController* createLocationVC=[[MedRepCreateLocationViewController alloc]init];
    createLocationVC.locationDetailsDelegate=self;
    createLocationVC.dismissDelegate=self;
    createLocationVC.locationDetailsDelegate=self;
    [self.navigationController pushViewController:createLocationVC animated:YES];
 }




- (IBAction)addDoctorTapped:(id)sender {
    
    NSLog(@"location label text is %@", locationLbl.text);
    
    
    if (locationTextField.text.length==0) {

        [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:KVisitCreationMissingLocationAlertMessageStr withController:self];
    }
    
    else
    {
        
        
        if ([[visitDetailsDict valueForKey:@"Location_Type"]isEqualToString:@"P"]) {
            
            
            
            MedRepCreatePharmacyContactViewController * createPharmacyContactVC=[[MedRepCreatePharmacyContactViewController alloc]init];
            createPharmacyContactVC.delegate=self;
            createPharmacyContactVC.locationDetailsDictionary=visitDetailsDict;
            [self.navigationController pushViewController:createPharmacyContactVC animated:YES];
            
            
            
            
        }
        
        else
        {
        
        
        
        //check if location is selected
        
        NSLog(@"location details dict %@", [locationDetailsDictionary description]);
        
        
        
        /*
         MedRepCreateDoctorViewController * doctorVC=[[MedRepCreateDoctorViewController alloc]init];
         doctorVC.dismissDelegate=self;
         
         [self.navigationController pushViewController:doctorVC animated:YES];*/
        
        
        UIButton* doctorBtn=(UIButton*)sender;
        
        MedRepCreateDoctorViewController* createLocationVC=[[MedRepCreateDoctorViewController alloc]init];
        createLocationVC.dismissDelegate=self;
        
        if (locationIDforExistingLocationNewDoc.length>0) {
            createLocationVC.selectedLocationID=locationIDforExistingLocationNewDoc;
            createLocationVC.locationName=locationTextField.text;
            
        }
        
        
        if (isNewLocation==YES) {
            
            //if new location is created disbale the location dropdown if creating new doctor and display the newly created location name
            
            
            createLocationVC.locationName=[locationDetailsDictionary valueForKey:@"Location_Name"];
            createLocationVC.createdLocatedID=[locationDetailsDictionary valueForKey:@"Location_ID"];
            
        }
        
        //    popOverController=nil;
        //    popOverController=[[UIPopoverController alloc] initWithContentViewController:createLocationVC];
        //    [popOverController setDelegate:self];
        //    [popOverController setPopoverContentSize:CGSizeMake(500, 624) animated:YES];
        //    
        //    createLocationVC.createDoctorPopover=popOverController;
        
        NSLog(@"location details before pushing to doctor %@", [locationDetailsDictionary description]);
        
        
        
        [popOverController presentPopoverFromRect:doctorBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        [self.navigationController pushViewController:createLocationVC animated:YES];
        
    }
    
    
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    if(textField==visitTypeTextField)
    {
        [self visitTypeTextFieldTapped];
    }
    else if(textField==locationTextField)
    {
        [self locationTextFieldTapped];
    }
    else if(textField==doctorTextField)
    {
        [self doctorTextFieldTapped];
    }
    else if(textField==dateTextField)
    {
        [self dateandTimeTextFieldTapped];
    }
    else if(textField==demoPlanTextField)
    {
        [self demonstrationPlantextFieldTapped];
    }
    
    return NO;
}

#pragma mark Pharmacy contact delegate

-(void)createdContactforPharmacy:(NSMutableDictionary*)pharmacyContactDetails
{
    isNewContact=YES;

    /* disable creating new contact/doctor*/
    if ([appCntrl.DISABLE_FM_CREATE_NEW_CONTACT isEqualToString:KAppControlsYESCode] ) {
        [addDoctorBtn setEnabled:NO];
    }
    
    /* disable doctor/contact selection */
    [doctorTextField setIsReadOnly:YES];
    doctorPickerBtn.enabled=NO;
    
    /** disable  location selection*/
    AddLocationButton.enabled=NO;
    [locationTextField setIsReadOnly:YES];

    
    
    NSLog(@"pharmacies created %@", pharmacyContactDetails);
    [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
    [visitDetailsDict setValue:@"" forKey:@"Doctor"];

    
    doctorContactTitle.text=kContactTitle;

    [visitDetailsDict setValue:[pharmacyContactDetails valueForKey:@"Contact_ID"]  forKey:@"Contact_ID"];
    
    doctorTextField.text=[pharmacyContactDetails valueForKey:@"Contact_Name"];
    
    NSString* tempDate=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:tempDate];
    NSLog(@"setting date 2 %@", refinedDate);

    dateTextField.text=refinedDate;
    
    [visitDetailsDict setValue:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat] forKey:@"Visit_Date"];
    
    datePickerBtn.enabled=NO;
    dateTextField.enabled=NO;
    [dateTextField setIsReadOnly:YES];

    if (demoPlanArray.count==0)
    {
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
        [df1 setDateFormat:@"yyy-MM-dd HH:mm:ss"];
        [df1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        
        NSTimeZone *gmt = [NSTimeZone timeZoneWithName:@"GMT"];
        [df1 setTimeZone:gmt];
        
        NSString *convertedDateString = [df1 stringFromDate:[NSDate date]];// here convert date in
        NSDate * convertedDate=[df1 dateFromString:convertedDateString];
        
        BOOL isDemoAvbl=[MedRepDefaults isDemoPlanAvailableforDate:convertedDate];
        if (isDemoAvbl==YES)
        {
            demoPlanArray=[MedRepQueries fetchDemoPlansforSelectedDate:convertedDate];
            NSLog(@"demo plan array in new doctor %@", [demoPlanArray description]);
        }
    }
}
- (IBAction)toggleForPlannedVisit:(UISwitch *)sender
{
    
    if([doctorTextField.text isEqualToString:@"New Doctor"]&& !doctorTextField.enabled)
    {
        
    }
    else
    {
        doctorTextField.text = @"";
        [visitDetailsDict setValue:@"" forKey:@"Contact_ID"];
        [visitDetailsDict setValue:@"" forKey:@"ContactName"];
        [visitDetailsDict setValue:@"" forKey:@"Doctor_ID"];
        [visitDetailsDict setValue:@"" forKey:@"Doctor"];
    }
    if (sender.on) {
        isPendingVisitEnabled = YES;
        if (locationTextField.text.length > 0 && !isNewLocation ) {
            popUpString = @"Location";
            [self selectedValueforCreateVisit:locationLastselectedIndexPath];
        }
    }
    else
    {
        isPendingVisitEnabled = NO;
        if (locationTextField.text.length > 0 && !isNewLocation )
        {
            popUpString = @"Location";
            [self selectedValueforCreateVisit:locationLastselectedIndexPath];
        }
    }
    locationsarray = [MedRepQueries fetchLocations];
}
-(void)discardSelectedDemoPlan
{
    demoPlanTextField.text=@"";
    [visitDetailsDict setValue:@"" forKey:@"Demonstration_Plan"];
    [visitDetailsDict setValue:@"" forKey:@"Demo_Plan_ID"];

}
-(void)AddDummyDemoPlanFoNoVisit
{
    demoPlanTextField.text=KNotApplicable;
    [visitDetailsDict setValue:KNotApplicable forKey:@"Demonstration_Plan"];
    [visitDetailsDict setValue:@"0" forKey:@"Demo_Plan_ID"];
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
@end
