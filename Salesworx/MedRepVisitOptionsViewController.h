//
//  MedRepVisitOptionsViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 2/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepCustomClass.h"

@interface MedRepVisitOptionsViewController : UIViewController
{
    IBOutlet UICollectionView *visitOptionsCollectionView;
    NSMutableArray *visitOptionsCollectionViewArray;
    
    BOOL isEdetailingDone;
}

@property(strong,nonatomic)NSMutableDictionary* visitDetailsDict;
@property(strong,nonatomic) VisitDetails * visitDetails;
@property(nonatomic) BOOL isFromDashboard;

@end
