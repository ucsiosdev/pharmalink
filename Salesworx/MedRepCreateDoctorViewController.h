//
//  MedRepCreateDoctorViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/13/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDatabaseManager.h"

#import "StringPopOverViewController_ReturnRow.h"

#import "MedRepDoctorFilterDescriptionViewController.h"

#import "MedRepTextField.h"
@protocol createDoctorDelegate <NSObject>

-(void)addDoctorData:(NSMutableDictionary*)locationDetails;

@end



@interface MedRepCreateDoctorViewController : UIViewController<StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,UITextFieldDelegate,UIAlertViewDelegate,SelectedFilterDelegate,UITextFieldDelegate>

{
    id dismissDelegate;
    NSString* popUpString;
    UIPopoverController * valuePopOverController;
    
    CGRect oldFrame;
    
    StringPopOverViewController_ReturnRow * popOver;
    
    IBOutlet UIButton *tradeChannelButton;
    NSString* doctorID;
    IBOutlet UIButton *classButton;
    IBOutlet UIButton *specilizationButton;
    
    
    IBOutlet MedRepTextField *locationTxtFld;
    IBOutlet MedRepTextField *specializationTxtFld;
    IBOutlet MedRepTextField *classTxtFld;
    IBOutlet MedRepTextField *tradeChannelTxtFld;
    IBOutlet UILabel *cityTitleLbl;
    IBOutlet MedRepTextField *cityTextField;
    IBOutlet NSLayoutConstraint *xSpecializationTopConstraint;
    IBOutlet NSLayoutConstraint *xConstraintTopOtcRX;
    IBOutlet UILabel *visitRequiredLabel;
    IBOutlet UIScrollView *scrollView;
    
    NSMutableArray *emiratesArray;
}

@property(strong,nonatomic)NSString* selectedLocationID;

@property (nonatomic, assign) id dismissDelegate;

@property (strong, nonatomic) IBOutlet UIButton *locationButton;

@property(strong,nonatomic) NSString* locationName;

@property(strong,nonatomic) NSString* createdLocatedID;

@property(strong,nonatomic)UIPopoverController* createDoctorPopover;

@property(strong,nonatomic)NSMutableArray* doctorArray;

@property(strong,nonatomic)NSMutableArray* specilizationArray;

@property(strong,nonatomic)NSMutableArray* tradeChannelArray;

@property(strong,nonatomic)NSMutableArray* OTCRXArray;

@property(strong,nonatomic)NSMutableArray* classArray;

@property(strong,nonatomic)NSMutableArray* locationsArray;

@property(strong,nonatomic)NSMutableDictionary * doctorDictionary;


@property (strong, nonatomic) IBOutlet MedRepTextField *nameTxtFld;

@property (strong, nonatomic) IBOutlet UILabel *specilizationLbl;

@property (strong, nonatomic) IBOutlet MedRepTextField *mohIDTxtFld;

@property (strong, nonatomic) IBOutlet MedRepTextField *telephoneTxtFld;

@property (strong, nonatomic) IBOutlet MedRepTextField *emailTxtFld;

@property (strong, nonatomic) IBOutlet UILabel *locationLbl;

@property (strong, nonatomic) IBOutlet UILabel *classLbl;

@property (strong, nonatomic) IBOutlet UILabel *tradeChannelLbl;


@property(strong,nonatomic) NSString* titleKey;

@property(nonatomic) BOOL isFromMultiCalls;

- (IBAction)specilizatioButtonTapped:(id)sender;

- (IBAction)locationButtonTapped:(id)sender;

- (IBAction)clssButtonTapped:(id)sender;

- (IBAction)tradeChannelButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepTextField *visitsRequiredTextField;

@property (strong, nonatomic) IBOutlet UISegmentedControl *otcRxSegment;

//- (IBAction)otxRxSegmentTapped:(id)sender;

- (void)otxRxSegmentTapped:(UISegmentedControl *)segment;

- (IBAction)saveButtonTapped:(id)sender;

- (IBAction)closeButtonTapped:(id)sender;

@end
