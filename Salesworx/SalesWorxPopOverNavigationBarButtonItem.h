//
//  SalesWorxPopOverNavigationBarButtonItem.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/8/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxPopOverNavigationBarButtonItem : UIBarButtonItem
@property (strong,nonatomic) UIButton * sourceButton;

@end
