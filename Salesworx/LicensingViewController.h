//
//  LicensingViewController.h
//  SWSession
//
//  Created by msaad on 3/17/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"

@interface LicensingViewController : SWViewController <UIPopoverControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIAlertViewDelegate> {
    UITableView *tableView;
   
    NSString *licenceType;
    SWLoadingView *loadingView;
    UIPopoverController *popoverController;
    UIButton *activateButton;
    SynchroniseViewController *verifyLicenceController;
    
    //       id target;
    SEL action;
    
    NSString *ClientVersion;
}
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
@property(strong,nonatomic)  NSString *customerID;


@end