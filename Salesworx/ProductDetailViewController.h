//
//  ProductDetailViewController.h
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "ProductDetailView.h"

@interface ProductDetailViewController : SWViewController <UIScrollViewDelegate, UIActionSheetDelegate> {
    NSDictionary *product;
    UIScrollView *detailScroller;
    UIPageControl *pageControl;
    UISegmentedControl *segments;
    UIActionSheet *actions;
    
    //Views
    ProductDetailView *productDetailView;
    ProductStockView *productStockView;
    ProductBonusView *productBonusView;
    ProductTargetView *productTargetView;
    int pages;
    NSString *isProductTargetEnable;
    UIBarButtonItem *flexibleSpace;
    UIBarButtonItem *lastSyncButton ;
    NSArray *productAddArray;
    BOOL isProductImageAvailble;
}
- (id)initWithProduct:(NSDictionary *)product;

@end