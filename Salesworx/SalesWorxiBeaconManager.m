//
//  SalesWorxiBeaconManager.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxiBeaconManager.h"
#import "SWDefaults.h"
#import "SWAppDelegate.h"

@implementation SalesWorxiBeaconManager

static SalesWorxiBeaconManager *sharedBluetoothSingleton = nil;

+ (SalesWorxiBeaconManager*) retrieveSingleton {
    @synchronized(self) {
        if (sharedBluetoothSingleton == nil) {
            sharedBluetoothSingleton = [[SalesWorxiBeaconManager alloc] init];
        }
    }
    return sharedBluetoothSingleton;
}
+ (void) destroyBeaconSingleton
{
    sharedBluetoothSingleton = nil;
}
-(void)initilizeiBeaconManager:(SalesWorxBeacon*)currentBeacon
{
    self.customerBeaconfromDB=currentBeacon;
    
    
    if ([KTKBeaconManager isMonitoringAvailable]) {
        // Initiate Beacon Manager
        self.beaconManager = [[KTKBeaconManager alloc] initWithDelegate:self];
        // Request Location Authorization
        [self.beaconManager requestLocationWhenInUseAuthorization];
        
        
    }
    // Kontakt.io proximity UUID
    NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:currentBeacon.Beacon_UUID];
    
    // Create region instance
    KTKBeaconRegion *region = [[KTKBeaconRegion alloc] initWithProximityUUID: proximityUUID identifier:@"123"];
    region.notifyOnEntry=YES;
    region.notifyOnExit=YES;
    region.notifyEntryStateOnDisplay=YES;
    
    switch ([KTKBeaconManager locationAuthorizationStatus]) {
            // Non-relevant cases are cut
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            if ([KTKBeaconManager isMonitoringAvailable]) {
                [self.beaconManager startMonitoringForRegion:region];
                [self.beaconManager startRangingBeaconsInRegion: region];
                
            }
            break;
    }
}
#pragma mark Beacon Delegate Methods

- (void)beaconManager:(KTKBeaconManager*)manager didChangeLocationAuthorizationStatus:(CLAuthorizationStatus)status;
{
    // ...
}

- (void)beaconManager:(KTKBeaconManager *)manager didStartMonitoringForRegion:(__kindof KTKBeaconRegion *)region {
    // Do something when monitoring for a particular
    // region is successfully initiated
    NSLog(@"started monitoring region");
    
    
    
    
}

- (void)beaconManager:(KTKBeaconManager *)manager monitoringDidFailForRegion:(__kindof KTKBeaconRegion *)region withError:(NSError *)error {
    // Handle monitoring failing to start for your region
    NSLog(@"failed to monitor region %@", error);
}

- (void)beaconManager:(KTKBeaconManager*)manager didEnterRegion:(__kindof KTKBeaconRegion*)region
{
    NSLog(@"Enter region %@", region);
    
    UIAlertView * test =[[UIAlertView alloc]initWithTitle:@"Beacon Alert" message:@"welcome home" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //[test show];
    
    [manager startRangingBeaconsInRegion:region];
    
    
}

- (void)beaconManager:(KTKBeaconManager*)manager didExitRegion:(__kindof KTKBeaconRegion*)region
{
    NSLog(@"Exit region %@", region);
    
    UIAlertView * test =[[UIAlertView alloc]initWithTitle:@"Beacon Alert" message:@"bye" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //[test show];
    [manager stopRangingBeaconsInRegion:region];
    
}

- (void)beaconManager:(KTKBeaconManager*)manager didRangeBeacons:(NSArray <CLBeacon *>*)beacons inRegion:(__kindof KTKBeaconRegion*)region
{
    
    NSLog(@"detected beacon range in beacon manager %@",[beacons description]);
    
    NSLog(@"db beacon %@",self.customerBeaconfromDB);
    
    
    beacons = [[NSArray alloc] init];
    
    if (beacons.count>0) {
        
        NSMutableArray * salesWorxBeaconObjectsArray=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<beacons.count; i++) {
            CLBeacon * currentBeacon = [[CLBeacon alloc]init];
            currentBeacon=[beacons objectAtIndex:i];
            SalesWorxBeacon * salesWorxBeacon =[[SalesWorxBeacon alloc]init];
            salesWorxBeacon.Beacon_UUID=[SWDefaults getValidStringValue:currentBeacon.proximityUUID.UUIDString];
            salesWorxBeacon.Beacon_Major=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentBeacon valueForKey:@"major"]]];
            salesWorxBeacon.Beacon_Minor=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentBeacon valueForKey:@"minor"]]];
            salesWorxBeacon.isVisitStarted=NO;
            salesWorxBeacon.proximity=[NSNumber numberWithDouble:currentBeacon.proximity];
            salesWorxBeacon.beaconDetectionTime=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
            [salesWorxBeaconObjectsArray addObject:salesWorxBeacon];
        }
        
        if(salesWorxBeaconObjectsArray.count>0)
        {
            
        NSPredicate * matchPrecicate=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Beacon_Major.integerValue ==  %ld and SELF.Beacon_Minor.integerValue ==   %ld",(long)self.customerBeaconfromDB.Beacon_Major.integerValue,(long)self.customerBeaconfromDB.Beacon_Minor.integerValue]];
        
        NSMutableArray * tempMatchArray=[[salesWorxBeaconObjectsArray filteredArrayUsingPredicate:matchPrecicate] mutableCopy];
        
            if (tempMatchArray.count>0) {
                self.detectedBeacon=[tempMatchArray objectAtIndex:0];

            }
        
    }
    
        
        /*
        if(salesWorxBeaconObjectsArray.count>0)
        {
            float xmax = -MAXFLOAT;
            float xmin = MAXFLOAT;
            for (NSNumber *num in [salesWorxBeaconObjectsArray  valueForKey:@"proximity"]) {
                float x = num.doubleValue;
                if (x < xmin) xmin = x;
                if (x > xmax) xmax = x;
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.proximity == %@", [NSNumber numberWithDouble:xmax]];
            
            NSLog(@"matched beacon %@", [salesWorxBeaconObjectsArray filteredArrayUsingPredicate:predicate]);
            
            NSMutableArray * accurateBeaconArray=[[salesWorxBeaconObjectsArray filteredArrayUsingPredicate:predicate] mutableCopy];
            if (accurateBeaconArray.count>0) {
                self.detectedBeacon=[accurateBeaconArray objectAtIndex:0];
            }
        }
        
        
        
        
        NSLog(@"becon range detected in manager %@", [beacons description]);
         */

    }

    
}

@end
