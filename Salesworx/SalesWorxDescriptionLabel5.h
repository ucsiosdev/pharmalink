//
//  SalesWorxDescriptionLabel5.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 7/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWDefaults.h"

@interface SalesWorxDescriptionLabel5 : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
-(NSString*)text;
-(void)setText:(NSString*)newText;

@end
