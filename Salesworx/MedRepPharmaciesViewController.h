//
//  MedRepPharmaciesViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import <MapKit/MapKit.h>
#import "SWAppDelegate.h"
#import "MedRepMenuTableViewCell.h"
#import "MedRepPharmaciesFilterViewController.h"
#import "UIImage+ImageEffects.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "SalesWorxImageView.h"
#import "SalesWorxImageBarButtonItem.h"
#import "SalesWorxPopOverNavigationBarButtonItem.h"

@interface MedRepPharmaciesViewController : UIViewController<MKAnnotation,MKMapViewDelegate,UIPopoverControllerDelegate,PharmacyFilterDelegate,SelectedFilterDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource, UIPopoverPresentationControllerDelegate>

{
    SWAppDelegate * appDel;
    
    NSMutableArray* filteredPharmacies;
    NSIndexPath * selectedPharmacyIndexPath;
    
    
    IBOutlet SalesWorxImageView *pharmacyStatusImageView;
    BOOL isSearching;
    
    UISearchBar *customSearchBar;
    
    IBOutlet UIView *pharmacyHeaderView;
    
    NSMutableDictionary* initialPharmacyDict;
    
    UIPopoverController * filterPopOverController, *popOverController;
    
    IBOutlet UISearchBar *pharmaciesSearchBar;
     CLLocationCoordinate2D pharmacyCoords;
    
    NSMutableArray * indexPathArray;
    
    NSMutableDictionary* previousFilteredParameters;
    
    NSMutableArray* unFilteredPharmaciesArray;
    
    NSMutableArray* pharmacyContactsArray;
    
    IBOutlet UITableView *contactsTblView;
    BOOL isFiltered;
    
    NSMutableArray * currentVisitTasksArray;
    NSMutableDictionary* selectedVisitDetailsDict;
    
    NSMutableArray * locationIndexArray;

    NSString *contactName;
    
    
    SalesWorxPopOverNavigationBarButtonItem *taskPopOverButton;
    SalesWorxPopOverNavigationBarButtonItem *notePopOverButton;
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *customerViewTopConstraint;
    
    NSMutableDictionary *lastVisitDetailsDict;
    IBOutlet UIButton *lastVisitDetailsButton;
}
@property (strong, nonatomic) IBOutlet UITableView *pharmaciesTablevIew;
@property (strong, nonatomic) IBOutlet UISearchBar *pharmaciesSearchBar;
@property (strong, nonatomic) IBOutlet UITextView *locationAddressTxtView;

@property (strong, nonatomic) IBOutlet UILabel *pharmacyNameLbl;

@property (strong, nonatomic) IBOutlet UILabel *pharmacyIDLbl;

@property (strong, nonatomic) IBOutlet UILabel *pharmacyStatusLbl;
@property (strong, nonatomic) IBOutlet UILabel *emirateLbl;

@property (strong, nonatomic) IBOutlet UILabel *contactNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *emailAddressLbl;

@property (strong, nonatomic) IBOutlet UILabel *classLbl;

@property (strong, nonatomic) IBOutlet UILabel *otcRXLbl;
@property (strong, nonatomic) IBOutlet UILabel *tradeChannelLbl;

@property (strong, nonatomic) IBOutlet UITableView *locationsTblView;

@property (strong, nonatomic) IBOutlet MKMapView *pharmaciesMapView;

@property (strong, nonatomic) IBOutlet UILabel *visitsRequiredLbl;

@property (strong, nonatomic) IBOutlet UILabel *visitsCompletedLbl;

@property (strong, nonatomic) IBOutlet UILabel *lastVisitedOnLbl;



@property(strong,nonatomic)NSMutableArray* pharmaciesArray,*pharmacyLocationsArray;

@property (strong, nonatomic) IBOutlet UIImageView *pharmacyImageView;

- (IBAction)filterButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *medRepFilterButton;



@property UIView *blurMask;
@property UIImageView *blurredBgImage;



































@end
