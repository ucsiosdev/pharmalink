//
//  CustomPopOverViewController.h
//  MathMonsters
//
//  Created by Transferred on 1/12/13.
//  Copyright (c) 2013 Designated Nerd Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StringPopOverDelegate <NSObject>
@required
-(void)selectedStringValue:(id)value;
@end

@interface StringPopOverViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *colorNames;
@property (nonatomic, weak) id<StringPopOverDelegate> delegate;
@end
