//
//  AlseerReturnCaluclationViewController.h
//  Salesworx
//
//  Created by Pavan Kumar Singamsetti on 2/26/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "ProductReturnViewController.h"
#import "ReturnAdditionalViewController.h"
@protocol AlseerReturnControllerDelegate <NSObject>
- (void)selectedProduct:(NSMutableDictionary *)product;
@end

@interface AlseerReturnCaluclationViewController : SWGridViewController < UIAlertViewDelegate , GridViewDataSource , GridViewDelegate>
{
    NSMutableDictionary *customer;
    NSDictionary *category;
    UILabel *totalPriceLabel;
    //
    ProductListViewController *productListViewController;
    UINavigationController *navigationControllerS;
    ProductReturnViewController *orderViewController;
    ReturnAdditionalViewController *orderAdditionalInfoViewController ;
    float totalOrderAmount;
    
    UIBarButtonItem *flexibleSpace  ;
    UIBarButtonItem *totalLabelButton1 ;
    
    UIBarButtonItem *totalLabelButton ;
    UIButton *saveButton  ;
    NSMutableDictionary *productDict;
    UILabel *totalProduct;
    
}

@property (unsafe_unretained) id <AlseerReturnControllerDelegate> delegate;

- (id)initWithCustomer:(NSDictionary *)customerDict andCategory:(NSDictionary *)categoryDict;
- (void)productAdded:(NSMutableDictionary *)q;
@end
