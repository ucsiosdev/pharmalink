//
//  SWReturnConfirmationViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/9/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlSeerSignatureViewController.h"

@interface SWReturnConfirmationViewController : UIViewController<UITextViewDelegate,UIAlertViewDelegate,UITextFieldDelegate>

{
    NSMutableDictionary *salesOrder;
    NSMutableDictionary *info;
    NSMutableDictionary *returnAdditionalInfoDict;
    NSString *performaOrderRef;
    UIImage *signatureSaveImage;
    NSString *isSignatureOptional;
    
    
    CGRect oldFrame;

}

@property (strong, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *customerIDLbl;
@property (strong, nonatomic) IBOutlet UITextField *txtfldCustomerDocRef;
@property (strong, nonatomic) IBOutlet UITextView *commentsTxtView;
@property (strong, nonatomic) IBOutlet UITextField *invoiceNumberTxtFld;
@property (strong, nonatomic) IBOutlet UIView *customSignatureView;
@property (strong, nonatomic) IBOutlet UITextField *customerNameTxtFld;


@property(strong,nonatomic) NSNumber* returnTypeNumber,*goodsCollectedNumber;

- (IBAction)goodsCollectedSegementTapped:(UISegmentedControl *)sender;

- (IBAction)returnTypeSegmentTapped:(UISegmentedControl *)sender;


@property (strong, nonatomic) IBOutlet UILabel *lblGoodsCollected;

@property (strong, nonatomic) IBOutlet UILabel *lblReturnType;
@property(strong,nonatomic)AlSeerSignatureViewController* signVC;

@property(strong,nonatomic)NSMutableDictionary* customerDict,*salesOrderDict;
@property (strong, nonatomic) IBOutlet UISegmentedControl *returnTypeSegment;

@property (strong, nonatomic) IBOutlet UISegmentedControl *goodsCollectedSegment;

- (IBAction)saveReturnTapped:(id)sender;

@end
