//
//  SWPageControl.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWPageControl : UIPageControl {
    NSArray *originalSubviews;
}

@property (nonatomic, strong) NSArray *originalSubviews;

@end