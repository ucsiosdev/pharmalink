//
//  SearchResultsTableViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SearchResultsTableViewController.h"
#import "MedRepDefaults.h"
@interface SearchResultsTableViewController ()

@property (nonatomic, strong) NSArray *array;

@end

@implementation SearchResultsTableViewController
@synthesize searchResults;
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.searchResults count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.font=MedRepTitleFont;
    cell.textLabel.textColor=MedRepTableViewCellTitleColor;
    
    cell.textLabel.text=[self.searchResults objectAtIndex:indexPath.row];
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}
@end