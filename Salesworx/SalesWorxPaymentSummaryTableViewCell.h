//
//  SalesWorxPaymentSummaryTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/11/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "SWDefaults.h"

@interface SalesWorxPaymentSummaryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDocumentNo;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDate;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblName;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblType;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblAmount;

@property(nonatomic) BOOL isHeader;


@end
