//
//  MedRepTextView.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/20/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SalesWorxDefaultTextView.h"
IB_DESIGNABLE
@interface MedRepTextView : SalesWorxDefaultTextView
@property (nonatomic,setter=setIsReadOnly:) IBInspectable BOOL isReadOnly;
@property (nonatomic) IBInspectable BOOL noBorders;
@property (nonatomic) IBInspectable BOOL isCopyPasteEnable;

@end
