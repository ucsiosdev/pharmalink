//
//  MedRepDefaults.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImage+ImageEffects.h"
#import "MedRepCustomClass.h"

//color codings





#define kSWX_FONT_REGULAR(s) [UIFont fontWithName:@"WeblySleekUISemilight" size:s]

#define kSWX_FONT_SEMI_BOLD(s) [UIFont fontWithName:@"WeblySleekUISemibold" size:s]




#define ACCEPTABLE_CHARACTERS_WITHSPACE @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.,@:"

//check Distribution check Item is available or not
#define KItemAvailable [UIColor greenColor]
#define KItemNotAvailable [UIColor redColor]
#define KNoChange [UIColor whiteColor]
#define KWaitTimeColor [UIColor greenColor]



//check Customer is active or not
//#define MedRepCustomerStatusColor [UIColor colorWithRed:(18.0/255.0) green:(184.0/255.0) blue:(118.0/255.0) alpha:1]


#define ACCEPTABLE_CHARACTERS_ServerName @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@/&: "


#define ACCEPTABLE_CHARACTERS_URL @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@/&:"

#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.@"

#define ACCEPTABLE_NUMBERS @"0123456789."




#define UITextViewBorderColor [UIColor colorWithRed:(238.0/255.0) green:(238.0/255.0) blue:(238.0/255.0) alpha:1]

#define ActivationTextFieldMaxLength @"30"

#define UIMapViewViewBorderColor [UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]

#define UITextViewBackgroundColor [UIColor colorWithRed:(242.0/255.0) green:(248.0/255.0) blue:(253.0/255.0) alpha:1]

#define UISearchBarBorderColor [UIColor colorWithRed:(204.0/255.0) green:(204.0/255.0) blue:(204.0/255.0) alpha:1]


#define UITextFieldBorderColor [UIColor colorWithRed:(173.0/255.0) green:(193.0/255.0) blue:(200.0/255.0) alpha:1]

#define UITextFieldDarkBorderColor [UIColor colorWithRed:(181.0/255.0) green:(202.0/255.0) blue:(210.0/255.0) alpha:1]

#define  UIViewBorderColor [UIColor colorWithRed:(238.0/255.0) green:(238.0/255.0) blue:(238.0/255.0) alpha:1]


#define UINavigationBarBackgroundColor [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]

#define ROUND_BUTTON_WIDTH_HEIGHT 46


#define MedRepTitleColor [UIColor colorWithRed:(139.0/255.0) green:(145.0/255.0) blue:(160.0/255.0) alpha:1]

#define MedRepTitleDescriptionColor [UIColor colorWithRed:(67.0/255.0) green:(72.0/255.0) blue:(88.0/255.0) alpha:1]

#define  UIViewControllerBackGroundColor [UIColor colorWithRed:(240.0/255.0) green:(240.0/255.0) blue:(240.0/255.0) alpha:1]




#define  MedRepFont kSWX_FONT_REGULAR(14)

#define MedRepSearchBarBackgroundColor [UIColor colorWithRed:(202.0/255.0) green:(209.0/255.0) blue:(221.0/255.0) alpha:1]

#define MedRepTaskandNotesButtonBorderColor [UIColor colorWithRed:(82.0/255.0) green:(186.0/255.0) blue:(179.0/255.0) alpha:1]



#define  MedRepSegmentControlFont kSWX_FONT_REGULAR(14)








//#define MedRepUITableviewSelectedCellBackgroundColor [UIColor colorWithRed:(81.0/255.0) green:(102.0/255.0) blue:(136.0/255.0) alpha:1]

#define MedRepUITableviewSelectedCellTextColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]

#define MedRepUITableviewSelectedCellStatusViewBackgroundColor [UIColor colorWithRed:(74.0/255.0) green:(144.0/255.0) blue:(206.0/255.0) alpha:1]

#define MedRepUITableviewSelectedCellBackgroundColor [UIColor colorWithRed:(228.0/255.0) green:(237.0/255.0) blue:(254.0/255.0) alpha:1]

#define MedRepUITableviewDeSelectedCellBackgroundColor [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1]


#define UITableviewUnSelectedCellBackgroundColor [UIColor whiteColor]


#define MedRepDescriptionLabelFontColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]





#define MedRepProductCodeLabelFontColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]




#define MedRepHeaderTitleFontColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]

#define LastVisitedOnTextColor [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]
#define LastVisitedAtTextColor [UIColor colorWithRed:(255.0/255.0) green:(114.0/255.0) blue:(71.0/255.0) alpha:1]



#define MedRepPanelTitleLabelFontColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]

//to be done

#define SurveyStatusFont kSWX_FONT_REGULAR(16)










#define MedRepProductDescriptionFont kSWX_FONT_REGULAR(19)

#define ProductNameFont  kSWX_FONT_REGULAR(22)



#define TaskandNotesTextColor [UIColor colorWithRed:(82.0/255.0) green:(186.0/255.0) blue:(179.0/255.0) alpha:1]

#define MedRepElementDescriptionLabelColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]



#define textViewMaximumSize 250

#define ProductImageViewBackgroundColor [UIColor colorWithRed:(242.0/255.0) green:(243.0/255.0) blue:(247.0/255.0) alpha:1]





#define documentsFolderName @"Documents"

#define kSyncLocationsUrl @"http://www.ucssolutions.com/licman/get-sync-locations.php"


#define KCompletedVisitColor [UIColor greenColor]
#define KLateVisitColor [UIColor redColor]
#define KFutureVisitColor [UIColor yellowColor]
#define KNoVisitColor [UIColor whiteColor]

#define KTaskAvailableColor [UIColor yellowColor]

#define MedRepEndVisitTitle @"End Call"
#define MedRepEndCallTitle @"eDetailing Confirmation"
#define MedRepStartCallTitle @"eDetailing"


#define KTASKOVERDUECOLOR [UIColor redColor]
#define KTASKPENDINGCOLOR [UIColor greenColor]
#define KTASKUNAVAILABLECOLOR [UIColor whiteColor]
#define KTASKCOMPLETEDCOLOR [UIColor greenColor]


#define KTASKPENDING @"PENDING"
#define KTASKCLOSED @"CLOSED"
#define KTASKOVERDUE @"OVERDUE"
#define KTASKUNAVAILABLE @"UNAVAILABLE"

#define KNOTESAVAILABLE @"AVAILABLE"
#define KNOTESUNAVAILABLE @"UNAVAILABLE"
#define KTASKSTATUSKEY @"TASK_STATUS"
#define KNOTESSTATUSKEY @"NOTES_STATUS"


#define KNOTESAVAILABLECOLOR [UIColor greenColor]
#define KNOTESUNAVAILABLECOLOR [UIColor whiteColor]

#define kDoctorLocation @"D"
#define kPharmacyLocation @"P"
#define kCreateDoctor @"Create Doctor"
#define kCreateContact @"Create Contact"
#define kMissingDoctorTitle @"Doctor"
#define kMissingContactTitle @"Contact"
#define kCloseVisitFilterType @"Close Visit"
#define kCloseTitle @"Close"
#define kReasonTitle @"Reason"

#define kDoctorName @"Doctor"
#define kContactName @"Contact"

#define kNewDoctor @"New Doctor"


#define kStockCheckTitle @"STOCK CHECK"
#define kEDetailingTitle @"EDETAILING"


#define kDateFormatWithoutTime @"dd MMM, yyyy"
#define kDateFormatWithTime @"dd MMM, yyyy hh:mm a"

#define kDatabseDefaultDateFormatWithoutTime @"yyyy-MM-dd"
#define kDatabseDefaultDateFormat @"yyyy-MM-dd HH:mm:ss"
//Text entry limits

// merchandising survey picture comment
#define SurveyPictureCommentTextViewLimit 1000
#define SurveyPictureMarkCommentTextViewLimit 500
#define MerchandisingCommentTextViewLimit 1000


// coach name
#define CoachNameTextFieldLimit 100
#define CoachConfirmationTextFieldLimit 1000


//Create Visit

#define ObjectiveTextFieldLimit 999
#define NotesTextViewLimit 2000

//notes
#define NoteTitleTextFieldLimit 50
#define NoteInformationTextFieldLimit 2000

//tasks
#define TaskTitleTextFieldLimit 100
#define TaskInformationTextFieldLimit 250


//visit
#define VisitConclusionNotesTextFieldLimit 2000
#define MeetingAttendeestextFieldLimit 1000

#define KDataNotAvailableMessageTitleString @"Data Unavailable"
#define KTaskDataNotAvailableMessageString @"Task data unavailable"
#define KNotesDataNotAvailableMessageString @"Notes data unavailable"
#define KConfirmationAlertYESButtonTitle @"Yes"
#define KConfirmationAlertNOButtonTitle @"No"
#define KAlertOkButtonTitle @"Ok"
#define KConfirmationAlertIgnoreButtonTitle @"Ignore"

#define KPopUpsBackGroundColor [UIColor colorWithRed:(86.0/255.0) green:(86.0/255.0)  blue:(86.0/255.0)  alpha:0.5]
//location

#define LocationNameTextFieldLimit 250
#define TelephoneTextFieldLimit 50
#define AddressTextFieldLimit 250

//contact
#define ODOMeterTextFieldLimit 9

#define ContactTitleTextFieldLimit 50
#define ContactNameTextFieldLimit 250
#define ContactEmailAddressTextFieldLimit 250
#define ContactTelephoneTextFieldLimit 50


//doctor
#define DoctorNameTextFieldLimit 250
#define MohIDTextFieldLimit 50
#define EmailTextFieldLimit 250




//login

#define UsernameTextFieldLimit 50
#define PasswordTextFieldLimit 50


#define kLocationAlertTitle @"Turn On Location Services to Allow “SalesWorx” to Determine Your Location"
#define kLocationAlertMessage @"Salesworx would like to use your location."
#define kLocationAccessRefuseAlertMessage @"You previously choose not to use your phone's location with the SalesWorx app. To use this feature, tap Settings and turn on the location in the privacy section."


#define KVisitCreationPopOverCancelAlertViewTitleStr @"Close Visit?"
#define KVisitCreationPopOverCancelAlertViewMessageStr  @"Unsaved data will be deleted"
#define KVisitCreationVisitDetailsNotAvailableMessageStr @"Selected visit details are not exist in database"
#define KVisitCreatedSuccessAlertMessage @"Visit created successfully"

#define KVisitUpdatedSuccessAlertMessage @"Visit updated successfully"
#define KKVisitCreatedSuccessAlertTitkeStr @"Success"
#define KVisitCreationDemoPlanDateDependencyAlertMessageStr @"Please select date before selecting demo plan"
#define KVisitCreationMissingLocationAlertMessageStr @"Please choose location"
#define KLocationCreationConfirmationAlertMessageStr @"Would you like to save data?"
#define KLocationCreationConfirmationAlertTitleStr @"Save Data?"
#define KErrorAlertTitlestr @"Error"
#define KLocationCreationErrorAlertMessageStr @"Error while creating location, please try again"
#define KInvalidEmailAlertTitleStr @"Invalid Email"
#define KInvalidEmailAlertMessageStr  @"Please Enter Valid Email Address"



#define KVisitListLocationMapInActiveImage @"Visit_MapInactive"
#define KVisitListLocationMapActiveImage @"Visit_MapActive"
#define KVisitListLocationContcatActiveImage  @"Visit_ContactIcon"
#define kDoctorTitle @"Doctor"
#define kContactTitle @"Contact"
#ifdef DEBUG
#define DebugLog(s, ...) NSLog(s, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)
#endif



@interface MedRepDefaults : NSObject

+(NSMutableArray*)filterArray:(NSMutableArray*)contentArray searchText:(NSString*)searchText;
+ (BOOL)isValidEmail;
+(UIColor*)generateRandomColor;
+(NSString*)fetchInitials:(NSString*)name;
+(NSString*) refineDateFormat:(NSString *) srcFormat destFormat:(NSString *)destFormat scrString:(NSString *) srcString;

+ (UIViewController *)backViewController:(UINavigationController*)navController;


+ (NSMutableArray*)fetchFirstandLastDatesofCurrentMonth:(NSDate *)date;

+(NSDate *) startOfMonth;
+ (NSDate *) endOfMonth;

+(BOOL)isDemoPlanAvailableforDate:(NSDate*)selectedDate;


+ (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate;
+(void)setDoctorSignature:(id)signatureData;
+(void)clearDoctorSignature;
+(id)fetchDoctorSignature;
+(void)setSignatureCapturedForCurrentCall:(BOOL)isSignatureCaptured;
+(BOOL)isSignatureCapturedForCurrentCall;

+(void)setVisitStartTime:(NSString*)startTime;
+(NSString*)fetchVisitStartTime;
+(void)clearVisitStartTime;


+ (void)setIssueNoteReference:(NSString *)ref;
+ (NSString *)lastIssueNoteReference;
+ (void)initializeMedRepDefaults;

+(NSString*)fetchTodaysDate;


+ (UIImage *)takeSnapshotOfView:(UIView *)view;
+ (UIImage *)blurWithImageEffects:(UIImage *)image;
+(NSString*)fetchDocumentsDirectory;
+(UIImage*)generateThumbImageforPdf:(NSString*)filePath;

+(NSDictionary*)fetchBarAttributes;
+(NSString *)getValidStringValue:(id)inputstr;
+ (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate;

+(BOOL)isDeviceAvailableSpaceMoreThanBufferSizeAfterThisFileDownload:(NSString *)fileSize;
+(BOOL) validateEmail:(NSString*) emailString;
+(NSString *)getDefaultStringForEmptyString:(NSString *)str;
+(VisitDetails*)fetchVisitDetailsObject:(NSMutableDictionary*)selectedVisitDetailsDict;

+(NSString*) timeBetweenSatrtTime:(NSString *)startTime andEndTime:(NSString*)endTime;
+(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController;
+(void) ShowConfirmationAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController;
+(UIAlertAction*)GetDefaultCancelAlertActionWithTitle:(NSString *)title;
-(NSMutableDictionary *)DeleteEmptyStringKeysInDic:(NSMutableDictionary *)dic;
+(NSMutableArray*)fetchDaystoDisable:(NSMutableArray*)weekDayStringsArray;

+(NSDate *) convertToDateFrom:(NSString *) dateString;

#pragma mark Webly Fonts

#define MedRepSingleLineLabelFont kSWX_FONT_SEMI_BOLD(14)
#define MedRepElementTitleLabelFont kSWX_FONT_SEMI_BOLD(16)
#define MedRepHeaderTitleFont kSWX_FONT_SEMI_BOLD(16)
#define MedRepProductCodeLabelFont kSWX_FONT_SEMI_BOLD(14)
#define MedRepElementDescriptionLabelFont kSWX_FONT_SEMI_BOLD(16)
#define MedRepPanelTitleLabelFont kSWX_FONT_REGULAR(18)
#define MedRepSingleLineLabelSemiBoldFont kSWX_FONT_SEMI_BOLD(16)
#define  MedRepTitleFont kSWX_FONT_REGULAR(18)
#define NavigationHeaderTitleFont kSWX_FONT_SEMI_BOLD(19)
#define MedRepDescriptionLabelFont kSWX_FONT_REGULAR(19)


#define MedRepMenuTitleFontColor [UIColor colorWithRed:(74.0/255.0) green:(84.0/255.0) blue:(120.0/255.0) alpha:1]
#define MedRepMenuTitleDescFontColor [UIColor colorWithRed:(89.0/255.0) green:(114.0/255.0) blue:(153.0/255.0) alpha:1]
#define MedRepDescriptionLabelUnselectedColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]

#define MedRepElementTitleLabelFontColor [UIColor colorWithRed:(74.0/255.0) green:(84.0/255.0) blue:(120.0/255.0) alpha:1]
#define MedRepMenuTitleSelectedCellTextColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]
#define MedRepMenuTitleUnSelectedCellTextColor [UIColor colorWithRed:(74.0/255.0) green:(84.0/255.0) blue:(120.0/255.0) alpha:1]
#define MedRepSingleLineLabelSemiBoldFontColor [UIColor colorWithRed:(74.0/255.0) green:(84.0/255.0) blue:(120.0/255.0) alpha:1]
#define MedRepTableViewCellTitleColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]


#define MedRepButtonFont kSWX_FONT_SEMI_BOLD(18)

#define FieldSalesDashboardValueLabelFont kSWX_FONT_SEMI_BOLD(22)

#define FieldSalesDashboardValueLabelFontColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]



@end


