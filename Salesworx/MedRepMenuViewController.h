//
//  MedRepMenuViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/27/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWFoundation.h"

#import "SectionView.h"
#import "SynViewController.h"
#import "AboutUsViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SWAppDelegate.h"
#import "AppControl.h"
#import "SalesWorxCommunicationsParentViewController.h"
#import "SalesWorxBrandAmbassadorsViewController.h"
#import "SalesWorxFieldSalesDashboardViewController.h"
#import "SalesWorxDashboardViewController.h"
#import "SalesWorxCoachReportViewController.h"
#import "SalesWorxCoachReportPopoverViewController.h"

@interface MedRepMenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SectionView, CoachReportViewControllerDelegate>


{
    NSString *isCollectionEnable;
    SynViewController *synch;
    UIViewController *CommunicationViewControllerNew1,*SurveyFormViewController1,*SWDashboardViewController1,*SWProductListViewController1,*SWCustomerListViewController1,*SWCollectionCustListViewController1,*newSalesOrder1,*dashboardAlSeerViewController1;
    AppControl *appControl;
    SWAppDelegate *app;
    NSArray *PDAComponents;
    NSMutableArray *othersMenuArray;
    NSMutableArray * selectedCellIndexpathArray;
    IBOutlet UIView *boUrlView;
    IBOutlet NSLayoutConstraint *boURLHeightConstraint;
}
@property (strong, nonatomic) IBOutlet UITableView *medRepMenuTableview;



@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray *categoryList;



@property(strong,nonatomic)NSMutableArray* salesOrderMenuArray;

@property(strong,nonatomic)NSMutableArray* medRepMenuArray;
-(void)displaySyncPopover;


- (void) setCategoryArray;

@end
