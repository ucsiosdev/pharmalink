//
//  SalesWorxDistributionStockTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxDistributionStockTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *LotNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *QuantityLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ExpiryDateLbl;


@end
