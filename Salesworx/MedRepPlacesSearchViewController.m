//
//  MedRepPlacesSearchViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepPlacesSearchViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "MedRepCreateLocationViewController.h"
#import "MedRepDefaults.h"

@interface MedRepPlacesSearchViewController ()

@end

@implementation MedRepPlacesSearchViewController

@synthesize locationsMapView,locationsSearchBar;
- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    
    [self.searchDisplayController setDelegate:self];
    [locationsSearchBar setDelegate:self];
    
    // Zoom the map to current location.
    [locationsMapView setShowsUserLocation:YES];
    [locationsMapView setUserInteractionEnabled:YES];
    [locationsMapView setUserTrackingMode:MKUserTrackingModeFollow];
    
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 2.0; //user needs to press for 2 seconds
    [locationsMapView addGestureRecognizer:lpgr];

    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveLocation)];
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    self.navigationItem.rightBarButtonItem=saveBtn;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Location Map", nil)];
 
}

-(void)saveLocation
{
    CSMapAnnotation * currentAnnotation=[[CSMapAnnotation alloc]init];
    if(locationsMapView.annotations.count>0)
    {
    currentAnnotation=[locationsMapView.annotations objectAtIndex:0];
    
    NSLog(@"check pin location in disappear %f %f ", currentAnnotation.coordinate.latitude,currentAnnotation.coordinate.longitude);
    
    CLLocationCoordinate2D selectedLoc;
    
    selectedLoc.latitude=currentAnnotation.coordinate.latitude;
    
    selectedLoc.longitude=currentAnnotation.coordinate.longitude;
    
    if ([self.delegate respondsToSelector:@selector(selectedLocation:)]) {
        
        [self.delegate selectedLocation:selectedLoc];
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertView *locationnotselectedalert=[[UIAlertView alloc]initWithTitle:@"Missing data" message:@"Location details not set" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [locationnotselectedalert show];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Search Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    // Cancel any previous searches.
    [localSearch cancel];
    
    // Perform a new search.
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = searchBar.text;
    request.region = locationsMapView.region;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (error != nil) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Map Error",nil)
                                        message:[error localizedDescription]
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
            return;
        }
        
        if ([response.mapItems count] == 0) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Results",nil)
                                        message:nil
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
            return;
        }
        
        results = response;
        
        NSLog(@"places results are %@", [results.mapItems description]);
        
        
        [self.searchDisplayController.searchResultsTableView reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [results.mapItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *IDENTIFIER = @"SearchResultsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENTIFIER];
    }
    
    MKMapItem *item = results.mapItems[indexPath.row];
    
    cell.textLabel.text = item.name;
    cell.detailTextLabel.text = item.placemark.addressDictionary[@"Street"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.searchDisplayController setActive:NO animated:YES];
    
    MKMapItem *item = results.mapItems[indexPath.row];
   
    NSMutableArray * annotationsToRemove = [ locationsMapView.annotations mutableCopy ] ;
    [ annotationsToRemove removeObject:locationsMapView.userLocation ] ;
    [ locationsMapView removeAnnotations:annotationsToRemove ] ;
    
    [locationsMapView addAnnotation:item.placemark];
    [locationsMapView selectAnnotation:item.placemark animated:YES];
    
    
    
    
    [locationsMapView setCenterCoordinate:item.placemark.location.coordinate animated:YES];
    
    [locationsMapView setUserTrackingMode:MKUserTrackingModeNone];
    
    MKCoordinateRegion adjustedRegion = [locationsMapView regionThatFits:MKCoordinateRegionMakeWithDistance(item.placemark.location.coordinate, 5000, 5000)];
    [locationsMapView setRegion:adjustedRegion animated:YES];
    
    
   
    
}


#pragma mark Map Delegate methods

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:locationsMapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [locationsMapView convertPoint:touchPoint toCoordinateFromView:locationsMapView];
    place = [[Place alloc] init] ;
    place.latitude= touchMapCoordinate.latitude;
    place.longitude =  touchMapCoordinate.longitude;
    //place.name= locationLbl.text;
    place.otherDetail = @"";
    
    NSMutableArray * annotationsToRemove = [ locationsMapView.annotations mutableCopy ] ;
    [ annotationsToRemove removeObject:locationsMapView.userLocation ] ;
    [ locationsMapView removeAnnotations:annotationsToRemove ] ;

    
    [locationsMapView removeAnnotation:csAnnotation];
    csAnnotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
    [locationsMapView addAnnotation:csAnnotation];
}


-(void) viewWillDisappear:(BOOL)animated {
}




- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    CSMapAnnotation * currentAnnotation=[[CSMapAnnotation alloc]init];
    currentAnnotation=[mapView.annotations objectAtIndex:0];
    
    NSLog(@"check pin location %f %f ", currentAnnotation.coordinate.latitude,currentAnnotation.coordinate.longitude);
    
    
    
    NSLog(@"pin tapped ");
    
    
}


- (IBAction)closeButtonTapped:(id)sender {
    [delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}
@end
