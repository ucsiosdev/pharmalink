//
//  ProductReturnViewController.m
//  SWCustomer
//
//  Created by msaad on 1/2/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "ProductReturnViewController.h"
#import "ReturnTypeViewController.h"
#import "ProductBonusViewController.h"
@interface ProductReturnViewController ()

@end
#define NUMERIC                 @"1234567890"

@implementation ProductReturnViewController
@synthesize action,target;
//@synthesize serProduct;

- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    if (self)
    {
        
        //serProduct.delegate=self;
        product=[NSMutableDictionary dictionaryWithDictionary:p];
        [[SWDatabaseManager retrieveManager] dbGetBonusInfo:[product stringForKey:@"Item_Code"]];
        
        [self setTitle:NSLocalizedString(@"Add Product Return", nil)];
        bonusString=@"0";
        isPopover = NO;
        isSaveOrder = NO;
        
        
        
        ProductHeaderView *productHeaderView = [[ProductHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 60)];
        [productHeaderView.headingLabel setText:[product stringForKey:@"Description"]];
        [productHeaderView.detailLabel setText:[product stringForKey:@"Item_Code"]];
        
        tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [tableView setDelegate:self];
        [tableView setDataSource:self];
        [tableView setTableHeaderView:productHeaderView];
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:tableView];
        
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)]];
        
        [self updatePrice];
        [SWDefaults setPaymentType:nil];
        
        ReturnTypeViewController *collectionTypeViewController = [[ReturnTypeViewController alloc] initWithEXP];
        [collectionTypeViewController setTarget:self];
        [collectionTypeViewController setAction:@selector(collectionTypeChanged:)];
        
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
        collectionTypePopOver.delegate=self;
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *dateString =  [formatter stringFromDate:[NSDate date]];
        [product setValue:dateString forKey:@"EXPDate"];
        
        // [product setValue:@"" forKey:@"lot"]; commented out for proper Lot Number display. OLA!!
        formatter=nil;
        usLocale=nil;

    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    
   }
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:NO animated:animated];

}
- (void)collectionTypeChanged:(NSDictionary *)newType
{
    isPopover = YES;
    [collectionTypePopOver dismissPopoverAnimated:YES];
    ////NSLog(@"%@",newType);
    

    [product setValue:[newType stringForKey:@"Description"] forKey:@"reason"];
    [product setValue:[newType stringForKey:@"RMA_Lot_Type"] forKey:@"RMA_Lot_Type"];
    [tableView beginUpdates];
    [self reloadRowAtIndex:6 andSection:0];
    [tableView endUpdates];
}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}
- (void)cancel:(id)sender
{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
}
- (void)save:(id)sender
{
    
    if([self validateInput])
    {
        isSaveOrder = YES;
    
        [((SWTextFieldCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]) resignResponder];
        if(bonus>0)
        {
            [((SWTextFieldCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]) resignResponder];
        }
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:product];
#pragma clang diagnostic pop

        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please fill all reqiured fields" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]   show];
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Please fill all reqiured fields"
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
    }
}
- (void)updatePrice
{
    
    if ([product stringForKey:@"Qty"].length > 0)
    {
        int qty = [[product stringForKey:@"Qty"] intValue];
        if (qty > 0)
        {
            double totalPrice = (double)qty * ([[product objectForKey:@"Net_Price"] doubleValue]);
            [product setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
            [priceFooterView.titleLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Credit Note Value", nil),[product currencyStringForKey:@"Price"]]];
        }
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)] animated:YES];
    }
    else
    {
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
}
#pragma mark UITableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0)
    {
        return 7;
    }
    else
    {
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
    
    if(section==0)
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:NSLocalizedString(@"Product Info", nil)];
        return sectionHeader;
    }
    else
    {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
 
    
    return 28.0f;
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)section {
    
    if(section==0)
    {
       priceFooterView=[[GroupSectionFooterView alloc] initWithWidth:tv.bounds.size.width text:[product currencyStringForKey:@"Price"]] ;
        return priceFooterView;
    }
    else
    {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 28.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"Identifier";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        
        if(indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
                cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]   ;
            }
            else
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier]   ;
            }
        }
        else
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier]   ;

        }
       
    }
    
    if (indexPath.section == 0)
    {
        //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (indexPath.row == 0)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]   ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Quantity", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Qty"];
            [((SWTextFieldCell *)cell).textField setText:[product stringForKey:@"Qty"]];
            [((SWTextFieldCell *)cell).label setText:[NSString stringWithFormat:@"*%@",NSLocalizedString(@"Quantity", nil) ]];
        }
        else if (indexPath.row == 1)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]   ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Bonus"];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Bonus", nil)];
            [((SWTextFieldCell *)cell).textField setText:[product stringForKey:@"Bonus"]];
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Bonus", nil)];
        }
        
        else if (indexPath.row == 2)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]   ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"UnitPrice"];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Unit Price", nil)];
            [((SWTextFieldCell *)cell).textField setText:[product currencyStringForKey:@"Net_Price"]];
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Wholesale Price", nil)];
            if([[product stringForKey:@"Control_1"] isEqualToString:@"Y"])
            {
                ((SWTextFieldCell *)cell).textField.userInteractionEnabled = YES;
            }
            else
            {
                ((SWTextFieldCell *)cell).textField.userInteractionEnabled = NO;
            }
        }
        else if (indexPath.row == 3)
        {
            
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]   ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell).textField setText:[product currencyStringForKey:@"List_Price"]];
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Retail Price", nil)];
            [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:NO];
        }
        else if (indexPath.row == 4)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]   ;
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"lot"];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Lot No", nil)];
            [((SWTextFieldCell *)cell).textField setText:[product stringForKey:@"lot"]];
            AppControl *appControl = [AppControl retrieveSingleton];
            if (appControl.IS_LOT_OPTIONAL) {
                [((SWTextFieldCell *)cell).label setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Lot", nil)]];
            }
            else
            {
                [((SWTextFieldCell *)cell).label setText:[NSString stringWithFormat:@"*%@",NSLocalizedString(@"Lot", nil)]];
            }
        }
        else if (indexPath.row == 5)
        {
            [SWDefaults setPaymentType:@""];
            cell = [[SWDateFieldCell alloc] initWithReuseIdentifier:@"Date"];
            [((SWDateFieldCell *)cell).titleLabel setText:NSLocalizedString(@"Expiry Date", nil)];
            [((SWDateFieldCell *)cell) setKey:@"ExpDate"];
            [((SWDateFieldCell *)cell) setDelegate:self];
            [((SWDateFieldCell *)cell) setDate:[product objectForKey:@"ExpDate"]];
        }
        else if (indexPath.row == 6)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]   ;
            [((SWTextFieldCell *)cell) setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"reason"];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Tap to select reason for return", nil)];
            [((SWTextFieldCell *)cell).textField setText:[product stringForKey:@"reason"]];
            [((SWTextFieldCell *)cell).textField setUserInteractionEnabled:NO];
            [((SWTextFieldCell *)cell).label setText:[NSString stringWithFormat:@"*%@",NSLocalizedString(@"Reason for Return", nil)]];
        }
    }
    
    else if (indexPath.section == 1)
    {
        //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        [cell.textLabel setText:NSLocalizedString(@"Prices and Bonus", nil)];
        [cell.detailTextLabel setText:NSLocalizedString(@"Review Price and Bonus Information", nil)];
    }
    return cell;
}
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && indexPath.row == 6)
    {
        UITableViewCell *cell = [tableView1 cellForRowAtIndexPath:indexPath];
        [collectionTypePopOver presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        ProductBonusViewController *viewController = [[ProductBonusViewController alloc] initWithProduct:product];
        UINavigationController *navigationControllerr = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self.navigationController presentViewController:navigationControllerr animated:YES completion:nil];
    }

}
#pragma mark ProductListView Controller action

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



#pragma mark EditableCell Delegate

- (void)editableCellDidStartUpdate:(EditableCell *)cell
{
    [self.navigationItem setRightBarButtonItem:nil animated:YES];
}
- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key
{

    [product setValue:value forKey:key];
    NSCharacterSet *unacceptedInput = nil;
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    
    if ([key isEqualToString:@"Qty"])
    {
        if ([[value componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1)
        {
            [self reloadRowAtIndex:0 andSection:0];
            [self updatePrice];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Quantity value should have only numeric" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
//            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                               message:@"Quantity should be a numeric value only."
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:nil];
            [product removeObjectForKey:@"Qty"];
            [self reloadRowAtIndex:0 andSection:0];
            [self updatePrice];

        }
    }
    if ([key isEqualToString:@"UnitPrice"])
    {
        [product setValue:value forKey:@"Net_Price"];
        [tableView beginUpdates];
        [self reloadRowAtIndex:2 andSection:0];
        [tableView endUpdates];
        [self updatePrice];
    }
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]    animated:YES];
}

- (void)reloadRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (BOOL)validateInput
{
    AppControl *appControl = [AppControl retrieveSingleton];
    if ([appControl.IS_LOT_OPTIONAL isEqualToString:@"Y"])
    {
        if (![product objectForKey:@"reason"])
        {
            return NO;
        }
        else if ([[product objectForKey:@"reason"] length] < 1)
        {
            return NO;
        }
        else if ([[product objectForKey:@"Qty"] length] < 1)
        {
            return NO;
        }
    }
    else
    {
        if (![product objectForKey:@"reason"])
        {
            return NO;
        }
        else if ([[product objectForKey:@"reason"] length] < 1)
        {
            return NO;
        }
        else if ([[product objectForKey:@"Qty"] length] < 1)
        {
            return NO;
        }
        else if ([[product objectForKey:@"lot"] length] < 1)
        {
            return NO;
        }

    }
    return YES;
}


@end
