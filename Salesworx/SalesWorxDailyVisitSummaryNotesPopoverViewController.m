//
//  SalesWorxDailyVisitSummaryNotesPopoverViewController.m
//  MedRep
//
//  Created by UshyakuMB2 on 23/07/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDailyVisitSummaryNotesPopoverViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"

@interface SalesWorxDailyVisitSummaryNotesPopoverViewController ()

@end

@implementation SalesWorxDailyVisitSummaryNotesPopoverViewController
@synthesize selectedCustomerID;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrVisitNotes = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT IFNULL(Text_Notes,'N/A') AS Visit_Notes FROM TBL_FSR_Actual_Visits where date(Visit_Start_Date) = date('now') and Customer_ID = '%@'",selectedCustomerID]];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString([@"Visit Notes" localizedCapitalizedString], nil)];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    self.view.backgroundColor = UIViewBackGroundColor;
    [self.navigationItem setLeftBarButtonItem:closeButton];
}

-(void)closeButtonTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrVisitNotes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = MedRepSingleLineLabelFont;
        cell.textLabel.textColor = MedRepMenuTitleFontColor;
    }
    cell.textLabel.text = [[arrVisitNotes objectAtIndex:indexPath.row]valueForKey:@"Visit_Notes"];
  
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
