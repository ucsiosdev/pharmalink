//
//  SalesWorxToDoDetailPopUpViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 10/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxToDoDetailPopUpViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#define kTitle @"Title"
#define kDescription @"Description"
#define kMessageTitle @"Message"
#define kDateTitle @"Date"
#define kStatus @"Status"
#define kSubmitTitle @"Submit"
#define kNameTitle @"Name"
@interface SalesWorxToDoDetailPopUpViewController ()

@end

@implementation SalesWorxToDoDetailPopUpViewController
@synthesize currentToDoItem;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    
    [self setHiddenAnimated:NO duration:1.25];
    titleTextfield.text=currentToDoItem.Title;
    DescriptionTextView.text=currentToDoItem.Description;
    statusTextField.text=currentToDoItem.Status;
   // dateTextField.text=[MedRepQueries date]
    
    NSMutableArray * statusArray=   [MedRepQueries fetchTaskAppCodes:@"Todo_Status"];
    if (statusArray.count>0) {
        
        NSPredicate * refinedPred=[NSPredicate predicateWithFormat:@"SELF != %@",kNewStatus];
        toDoStatusArray=[[statusArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
        
    }
    dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[SWDefaults getValidStringValue:currentToDoItem.Todo_Date]];
    
    
    if([currentToDoItem.Status isEqualToString:kCompletedStatisticsTitle] || [currentToDoItem.Status isEqualToString:kClosedStatisticsTitle])

    {
        [DescriptionTextView setIsReadOnly:YES];
        [titleTextfield setIsReadOnly:YES];
        [dateTextField setIsReadOnly:YES];
        [statusTextField setIsReadOnly:YES];
        [saveButton setHidden:YES];
    }
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = TopContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];

}


#pragma mark UITextField Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //currentToDo.isUpdated=kYesTitle;
    
    if (textField ==dateTextField) {
        
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = @"Status";
        popOverVC.datePickerMode=kTodoTitle;
        
        //        if (_txtExpiryDate.text == nil || [_txtExpiryDate.text isEqualToString:@""])
        //        {
        //        } else {
        //            popOverVC.setMinimumDateCurrentDate=NO;
        //            popOverVC.previousSelectedDate = previousSelectedDate;
        //        }
        popOverVC.setMinimumDateCurrentDate=YES;
        
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate=self;
        popOverVC.datePickerPopOverController=datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:dateTextField.dropdownImageView.frame inView:dateTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        return NO;
    }
    else if (textField==statusTextField)
    {
        SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray=toDoStatusArray;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        statusPopOverController=nil;
        statusPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        statusPopOverController.delegate=self;
        popOverVC.popOverController=statusPopOverController;
        popOverVC.titleKey=@"STATUS";
        [statusPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        
        [statusPopOverController presentPopoverFromRect:statusTextField.dropdownImageView.frame inView:statusTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        return NO;
    }
    else
        return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==titleTextfield) {
        NSString* titleText=[SWDefaults getValidStringValue:textField.text];
        currentToDoItem.Title=titleText;
        [titleTextfield resignFirstResponder];
        [DescriptionTextView becomeFirstResponder];
        
    }
}

#pragma mark UITextView Delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == titleTextfield) {
        [titleTextfield resignFirstResponder];
        [DescriptionTextView becomeFirstResponder];
        return NO;
    }
    else
        return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // currentToDo.isUpdated=kYesTitle;
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==DescriptionTextView) {
        NSString* messageText=[SWDefaults getValidStringValue:textView.text];
        currentToDoItem.Description=messageText;
    }
}

#pragma mark Selected Date Delegate
-(void)didSelectDate:(NSString*)selectedDate
{
    if ([NSString isEmpty:selectedDate]==NO) {
        
        dateTextField.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithTime scrString:[SWDefaults getValidStringValue:selectedDate]];
        currentToDoItem.Todo_Date = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[SWDefaults getValidStringValue:selectedDate]];;
        
    }
}
#pragma mark Status Delegate
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSString* selectedStatus=[toDoStatusArray objectAtIndex:selectedIndexPath.row];
    if ([NSString isEmpty:selectedStatus]==NO) {
        statusTextField.text=[SWDefaults getValidStringValue:selectedStatus];
        currentToDoItem.Status=selectedStatus;
    }
}


- (IBAction)saveButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    //validations
    NSString* Title=currentToDoItem.Title;
    NSString* Description=currentToDoItem.Description;
    NSString* Date=currentToDoItem.Todo_Date;
    NSString* Status=currentToDoItem.Status;
    
    NSString* missingContent=[[NSString alloc]init];
    
    if ([NSString isEmpty:Title]) {
        missingContent=kTitle;
    }
    else if ([NSString isEmpty:Description]) {
        missingContent=kDescription;
    }
    else if ([NSString isEmpty:Date]) {
        missingContent=kDateTitle;
    }
    else if ([NSString isEmpty:Status]) {
        missingContent=kStatus;
    }
    else
    {
        
    }
    
    if ([NSString isEmpty:missingContent]==NO) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:[NSString stringWithFormat:@"Please enter %@",missingContent] withController:self];
    }
    
    else
    {
        

        
        currentToDoItem.Last_Updated_At=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        
        
        //save data to database
        if ([currentToDoItem.isUpdated isEqualToString:kYesTitle]) {
            
            NSLog(@"already existing row_ID %@", currentToDoItem.Row_ID);
            
        }
        else if([currentToDoItem.isNew isEqualToString:kYesTitle])
            
        {
            currentToDoItem.Row_ID=[NSString createGuid];
            
        }
        BOOL insertStatus=[SWDatabaseManager InsertToDoList:currentToDoItem];
        
        if (insertStatus==YES) {
            
            NSLog(@"insert successfull in To DO");
            
            NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kTodoTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                                object:self
                                                              userInfo:visitOptionDict];
            
            [self cancelButtonTapped:cancelButton];
            
        }
    }
}


@end
