//
//  ViewController+Swizzle.m
//  TryingOutStuff
//


#import "UIViewController+Logging.h"
#import <objc/runtime.h>

@implementation UIViewController (Logging)
+ (void)load {
    static dispatch_once_t once_token;
    dispatch_once(&once_token,  ^{
        Method original, swizzled;
        original = class_getInstanceMethod(self, @selector(viewDidLoad));
        swizzled = class_getInstanceMethod(self, @selector(swizzle_viewDidLoad));
        method_exchangeImplementations(original, swizzled);
    });
}
- (void) swizzle_viewDidLoad {
    [self swizzle_viewDidLoad];
    
    if ( [self isKindOfClass:[UIViewController class]]) {
        NSLog(@"\n\n\n *** Class name:: %@  ***\n\n\n", [self class]);
    }
    
    
}
@end
