//
//  MedRepElementDescriptionLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepElementDescriptionLabel.h"
#import "MedRepDefaults.h"

@implementation MedRepElementDescriptionLabel
@synthesize EnableReadMore;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];

    self.font=MedRepElementDescriptionLabelFont;
    self.textColor=MedRepDescriptionLabelFontColor;
    if(self.RTLSupport)
        super.text=NSLocalizedString(super.text, nil);
    
    if(EnableReadMore){
        self.userInteractionEnabled=YES;
       // UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ShowFullText)];//your action selector
       // [tap setNumberOfTapsRequired:2];
      //  self.userInteractionEnabled= YES;
        //[self addGestureRecognizer:tap];
    }
    
//    CATransition *animation = [CATransition animation];
//    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    animation.type = kCATransitionFade;
//    animation.duration = 0.75;
//    [self.layer addAnimation:animation forKey:@"kCATransitionFade"];
}
-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
    
    
   // originalText=newText;
    
    if(EnableReadMore){
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(addReadMoreStringToUILabel) userInfo:nil repeats:NO];
    }
}
-(void)readMoreDidClickedGesture{
    [self removeGestureRecognizer:readMoreGesture];
    readMoreGesture=nil;
    
    [readMoreLabel removeFromSuperview];

        self.numberOfLines=0;
        self.text=self.text;
}
- (BOOL)isTruncated
{
    ;
    CGRect perfectSize = [self.text boundingRectWithSize:CGSizeMake(self.bounds.size.width, NSIntegerMax) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.font} context:nil];
    if (perfectSize.size.height > self.bounds.size.height) {
        return YES;
    }
    

    return NO;
}
- (void)addReadMoreStringToUILabel
{
    if(EnableReadMore && self.frame.size.width > 150 && [self isTruncated])
    {
        [readMoreLabel removeFromSuperview];
        readMoreLabel=[[UILabel alloc]init];
        [readMoreLabel setFont:kSWX_FONT_SEMI_BOLD(14)];
        [readMoreLabel setFrame:CGRectMake(self.frame.size.width-85,self.frame.size.height-18, 85, 18)];
        [readMoreLabel setText:@"...More"];
        [readMoreLabel setTextAlignment:NSTextAlignmentCenter];
        [readMoreLabel setBackgroundColor:[UIColor whiteColor]];
        [readMoreLabel setTextColor:[UIColor grayColor]];

        [readMoreLabel   setUserInteractionEnabled:NO];
        
        
        
        [self addSubview:readMoreLabel];
        
        
        readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(readMoreDidClickedGesture)];
        readMoreGesture.numberOfTapsRequired = 1;
        [self addGestureRecognizer:readMoreGesture];
        self.userInteractionEnabled = YES;
        
    }
    else {
        [self removeGestureRecognizer:readMoreGesture];
        readMoreGesture=nil;
        [readMoreLabel removeFromSuperview];
        NSLog(@"No need for 'Read More'...");
    }
}
@end
