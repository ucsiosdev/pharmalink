//
//  SalesWorxTitleLabel4.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxTitleLabel4.h"
#import "MedRepDefaults.h"

@implementation SalesWorxTitleLabel4

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    self.font=kSWX_FONT_SEMI_BOLD(15);
    self.textColor=MedRepElementTitleLabelFontColor;
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && super.text.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[super.text characterAtIndex:super.text.length-1]];
            NSString *nString=[super.text stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(super.text, nil);
            
        }
    }
    
    
}

-(NSString*)text{
    return super.text;
}
-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && newText.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[newText characterAtIndex:newText.length-1]];
            NSString *nString=[newText stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(newText, nil);
            
        }
    }
    else
        super.text = newText;
    
    
}



@end
