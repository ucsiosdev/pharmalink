//
//  SalesWorxFieldSalesProductsStockPositionTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/18/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxFieldSalesProductsStockPositionTableViewCell : UITableViewCell

@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *OrgCodeLabel;
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *PoQtyLabel;
@property (strong, nonatomic)   IBOutlet SalesWorxSingleLineLabel *awaitingForShippingLabel;
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *reserveLabel;
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *onHoldLabel;
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *orgNameLabel;
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *expiryDateLabel;

@end
