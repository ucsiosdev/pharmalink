//
//  SalesWorxFieldSalesProductsViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/16/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldSalesProductsViewController.h"
#import "MedRepUpdatedDesignCell.h"
#import "SWDatabaseManager.h"
#import "SalesWorxProductsFilterViewController.h"
#import "SalesWorxBarCodeScannerManager.h"

#import "SalesWorxFieldsSalesProductsDetailsViewController.h"
#import "SalesWorxFieldsSalesProductsStockDetailsViewController.h"
#import "SalesWorxFieldsSalesProductsBonusDetailsViewController.h"
#import "SalesWorxAssortmentBonusInfoViewController.h"
#import "SalesWorxSalesOrderProductsTableViewBrandcodeCell.h"
#import "SalesWorxProductNameSearchPopOverViewController.h"

@interface SalesWorxFieldSalesProductsViewController ()
{
    AppControl *appControl;
    
    SalesWorxFieldsSalesProductsDetailsViewController * productDetailsChildViewcontroller;
    SalesWorxFieldsSalesProductsStockDetailsViewController * productStockDetailsChildViewcontroller;
    SalesWorxFieldsSalesProductsBonusDetailsViewController * productBonusDetailsChildViewcontroller;
    
}
@end

@implementation SalesWorxFieldSalesProductsViewController
@synthesize pagingScrollView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    proSearchType=ProductSerachNormal;
    allowSearchBarEditing=YES;
    productsTableViewDataArray=[[NSMutableArray alloc]init];
    filteredProductsTableViewDataArray=[[NSMutableArray alloc]init];
    productsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    filteredProductsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
    
    appControl = [AppControl retrieveSingleton];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    [pagingScrollView setDelegate:self];
    
    if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        [self AddActivityIndicatorInProductSearchBar];
    }
    
    productsTableView.rowHeight = UITableViewAutomaticDimension;
    productsTableView.estimatedRowHeight = 70.0;
    errorMessageLabel.text=@"";

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    /** fetching product details */
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        productsTableViewDataArray=  [[SWDatabaseManager retrieveManager] fetchProductsDetails];
        filteredProductsTableViewDataArray=[productsTableViewDataArray mutableCopy];

        if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
            productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsTableViewDataArray valueForKeyPath:@"@distinctUnionOfObjects.Brand_Code"]];
            productsBrandCodesArray= [[NSMutableArray alloc]initWithArray:[productsBrandCodesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
            
            for (int i = 0; i<productsBrandCodesArray.count; i++) {
                [productsArrayGroupedByBrandCode addObject:[[NSMutableArray alloc]init]];
            }
            filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
                
                if (productsBrandCodesArray.count == 0) {
                    errorMessageLabel.text=@"No Products Available";
                } else {
                    if(productsBrandCodesArray.count==1) {
                        [self ExpandFirstProductsSection];
                    }
                    else {
                        ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
                    }
                    [productsTableView reloadData];
                    [self preparePagingScrollViewChildViewsWithProduct:[self CovertProductDicToProductsObject:[filteredProductsTableViewDataArray objectAtIndex:0]]];
                    [self hideNoSelectionView];
                    [self showNoSelectionViewWithMessage];
                }
            } else {
                if(filteredProductsTableViewDataArray.count>0){
                    productTableViewSelectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
                    [productsTableView reloadData];
                    [self preparePagingScrollViewChildViewsWithProduct:[self CovertProductDicToProductsObject:[filteredProductsTableViewDataArray objectAtIndex:0]]];
                    [self hideNoSelectionView];
                }else{
                    errorMessageLabel.text=@"No Products Available";
                }
            }
        });
    });
}

-(void)ExpandFirstProductsSection{
    ProductTableViewExpandedSectionsArray =[[NSMutableArray alloc]init];
    [ProductTableViewExpandedSectionsArray addObject:@"0"];
    filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
    [self AddProductObjectsToGroupByArrayAtIndex:0];
}

-(void)AddProductObjectsToGroupByArrayAtIndex:(NSInteger)brandCodeLocation
{
    NSPredicate *brandCodeProductPredicate=[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[productsBrandCodesArray objectAtIndex:brandCodeLocation]];
    NSMutableArray * tempProductsArray=[[NSMutableArray alloc]initWithArray:[filteredProductsTableViewDataArray filteredArrayUsingPredicate:brandCodeProductPredicate]];
    
    [filteredProductsArrayGroupedByBrandCode replaceObjectAtIndex:brandCodeLocation  withObject:tempProductsArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UI Customization
-(void)CustomizeNavigationBar{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
        } else {
        }
    }
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Products", nil)];

}

-(void)CustomizeDetsilsSegmentedControl{
    productDetailsSegmentControl.titleTextAttributes =[SWDefaults fetchHmSegmentControlSegmentTitleTextAttributes];
    productDetailsSegmentControl.selectedTitleTextAttributes = [SWDefaults fetchHmSegmentControlSelectedTitleTextAttributes];
    
    
    productDetailsSegmentControl.sectionTitles = @[NSLocalizedString(@"Details", nil), NSLocalizedString(@"Stock Info", nil), NSLocalizedString(@"Bonus/Price Info", nil)];
    
    
    productDetailsSegmentControl.selectionIndicatorBoxOpacity=0.0f;
    productDetailsSegmentControl.verticalDividerEnabled=YES;
    productDetailsSegmentControl.verticalDividerWidth=1.0f;
    productDetailsSegmentControl.verticalDividerColor=[UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1];
    
    productDetailsSegmentControl.selectionIndicatorColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1];
    productDetailsSegmentControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    productDetailsSegmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    productDetailsSegmentControl.tag = 3;
    productDetailsSegmentControl.selectedSegmentIndex = 0;
    
    
    productDetailsSegmentControl.layer.shadowColor = [UIColor blackColor].CGColor;
    productDetailsSegmentControl.layer.shadowOffset = CGSizeMake(2, 2);
    productDetailsSegmentControl.layer.shadowOpacity = 0.1;
    productDetailsSegmentControl.layer.shadowRadius = 1.0;
    
    
    [productDetailsSegmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    __weak typeof(self) weakSelf = self;
    [productDetailsSegmentControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.pagingScrollView setContentOffset:CGPointMake(pagingScrollView.frame.size.width * index, 0)];
        
    }];

}
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Segmnted Control tapped");
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self CustomizeNavigationBar];

    if(self.isMovingToParentViewController){
        
        [self CustomizeDetsilsSegmentedControl];
        
        if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode]){
            xSearchBarTrailingspaceToFilterButton.constant=47;
            [barcodeScanButton setHidden:NO];
        }
        else{
            xSearchBarTrailingspaceToFilterButton.constant=8;
            [barcodeScanButton setHidden:YES];
        }
    }
    
    
    
    
}
-(void)preparePagingScrollViewChildViewsWithProduct :(Products *)product{
    
    [pagingScrollView setPagingEnabled:YES];
    [pagingScrollView setContentSize:CGSizeMake(pagingScrollView.frame.size.width*3, pagingScrollView.frame.size.height)];

    productDetailsChildViewcontroller=[[SalesWorxFieldsSalesProductsDetailsViewController alloc] initWithNibName:@"SalesWorxFieldsSalesProductsDetailsViewController" bundle:[NSBundle mainBundle]];
    productStockDetailsChildViewcontroller=[[SalesWorxFieldsSalesProductsStockDetailsViewController alloc] initWithNibName:@"SalesWorxFieldsSalesProductsStockDetailsViewController" bundle:[NSBundle mainBundle]];
    productBonusDetailsChildViewcontroller=[[SalesWorxFieldsSalesProductsBonusDetailsViewController alloc] initWithNibName:@"SalesWorxFieldsSalesProductsBonusDetailsViewController" bundle:[NSBundle mainBundle]];
    
    
    productDetailsChildViewcontroller.view.frame=CGRectMake(4, 0, pagingScrollView.frame.size.width-8, pagingScrollView.frame.size.height);
    productStockDetailsChildViewcontroller.view.frame=CGRectMake(pagingScrollView.frame.size.width+4, 0, pagingScrollView.frame.size.width-8, pagingScrollView.frame.size.height);
    productBonusDetailsChildViewcontroller.view.frame=CGRectMake((pagingScrollView.frame.size.width *2)+4, 0, pagingScrollView.frame.size.width-8, pagingScrollView.frame.size.height);
    
    productDetailsChildViewcontroller.currentProduct=product;
    productStockDetailsChildViewcontroller.currentProduct=product;
    productBonusDetailsChildViewcontroller.currentProduct=product;
    productBonusDetailsChildViewcontroller.productsArray=productsTableViewDataArray;
    
    [pagingScrollView addSubview:productDetailsChildViewcontroller.view];
    [pagingScrollView addSubview:productStockDetailsChildViewcontroller.view];
    [pagingScrollView addSubview:productBonusDetailsChildViewcontroller.view];
    [self addChildViewController:productDetailsChildViewcontroller];
    [self addChildViewController:productStockDetailsChildViewcontroller];
    [self addChildViewController:productBonusDetailsChildViewcontroller];
    
}
#pragma mark UITableView Methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==productsTableView && [appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        return productsBrandCodesArray.count;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==productsTableView) {
        if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
            if(![ProductTableViewExpandedSectionsArray containsObject:[NSString stringWithFormat:@"%ld",(long)section]])
                return 1;
            else
                return [(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:section]count]+1;
        } else {
            return filteredProductsTableViewDataArray.count;
        }
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==productsTableView && [appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        if(indexPath.row==0)
        {
            return 44.0;
        }
        return UITableViewAutomaticDimension;
    }
    return UITableViewAutomaticDimension;

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==productsTableView && [appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        return 2.0f;
    }
    return 0;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == productsTableView && [appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        if(indexPath.row==0)
        {
            static NSString* identifier=@"BrandCodeCell";
            SalesWorxSalesOrderProductsTableViewBrandcodeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesOrderProductsTableViewBrandcodeCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.brandCodeLabel.text=[productsBrandCodesArray objectAtIndex:indexPath.section];
            cell.brandCodeLabel.font=MedRepTitleFont;
            if(![ProductTableViewExpandedSectionsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section ]])
            {
                [self updateBrandCodeCell:cell isExpanded:NO];
            }
            else
            {
                [self updateBrandCodeCell:cell isExpanded:YES];
            }
            return cell;
        }
        else
        {
            Products *currentProduct;
            NSMutableArray *arrProductsByBrandCode = [filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section];
            if (arrProductsByBrandCode.count > 0) {
                currentProduct = [arrProductsByBrandCode objectAtIndex:indexPath.row-1];
            }
            
            static NSString* identifier=@"updatedDesignCell";
            MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            if(![productTableViewSelectedIndexPath isEqual:indexPath] || productTableViewSelectedIndexPath==nil)
            {
                [cell setDeSelectedCellStatus];
            }
            else {
                [cell setSelectedCellStatus];
            }
            
            cell.titleLbl.text=[[NSString stringWithFormat:@"%@",[currentProduct valueForKey:@"Description"]] capitalizedString];
            cell.descLbl.text=[NSString stringWithFormat:@"%@",[currentProduct valueForKey:@"Item_Code"]];
            return cell;
        }
    } else {
        static NSString* identifier=@"updatedDesignCell";
        MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if(productTableViewSelectedIndexPath!=nil && productTableViewSelectedIndexPath.section==indexPath.section && productTableViewSelectedIndexPath.row==indexPath.row){
            [cell setSelectedCellStatus];
        }else{
            [cell setDeSelectedCellStatus];
        }
        
        NSDictionary *currentProduct=[filteredProductsTableViewDataArray objectAtIndex:indexPath.row];
        
        cell.titleLbl.text=[[NSString stringWithFormat:@"%@",[currentProduct valueForKey:@"Description"]] capitalizedString];
        cell.descLbl.text=[NSString stringWithFormat:@"%@",[currentProduct valueForKey:@"Item_Code"]];
        
        return cell;
    }
}

-(void)updateBrandCodeCell:(SalesWorxSalesOrderProductsTableViewBrandcodeCell*)cell isExpanded:(BOOL)isExpanded
{
    if(!isExpanded)
    {
        cell.expandCollapseArrowImageView.image=[UIImage imageNamed:KSalesOrderProductTableViewDownArrowImageName];
        cell.backgroundColor=KSalesOrderProductTableViewBrandCodeCellCollapsedColor;
        cell.brandCodeLabel.textColor=KSalesOrderProductTableViewBrandCodeCellCollapsedTextColor;
    }
    else
    {
        cell.expandCollapseArrowImageView.image=[UIImage imageNamed:KSalesOrderProductTableViewUpArrowImageName];
        cell.backgroundColor=KSalesOrderProductTableViewBrandCodeCellExpandColor;
        cell.brandCodeLabel.textColor=KSalesOrderProductTableViewBrandCodeCellExpandTextColor;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==productsTableView && [appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode])
    {
        if(indexPath.row==0)
        {
            [self.view endEditing:YES];
            SalesWorxSalesOrderProductsTableViewBrandcodeCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            NSMutableArray *productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
            
            if(ProductTableViewExpandedSectionsArray.count>0)
            {
                NSInteger expnadedSection=[[ProductTableViewExpandedSectionsArray objectAtIndex:0]integerValue];
                cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:expnadedSection]];
                
                for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:expnadedSection]count]; i++) {
                    NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:expnadedSection];
                    [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                }
                [tableView beginUpdates];
                [ProductTableViewExpandedSectionsArray removeAllObjects];
                [tableView deleteRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                [self updateBrandCodeCell:cell isExpanded:NO];
                [tableView endUpdates];
                
                if(expnadedSection!=indexPath.section)
                {
                    cell = [tableView cellForRowAtIndexPath:indexPath];
                    
                    productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
                    
                    [self AddProductObjectsToGroupByArrayAtIndex:indexPath.section];
                    for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section]count]; i++) {
                        NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:indexPath.section];
                        [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                    }
                    [tableView beginUpdates];
                    [ProductTableViewExpandedSectionsArray addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
                    [tableView insertRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self updateBrandCodeCell:cell isExpanded:YES];
                    [tableView endUpdates];
                    
                }
                
            }
            else
            {
                cell = [tableView cellForRowAtIndexPath:indexPath];
                
                productsIndexPathsForSelectedBrand=[[NSMutableArray alloc]init];
                [self AddProductObjectsToGroupByArrayAtIndex:indexPath.section];
                
                for (NSInteger i=0; i<[(NSMutableArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section]count]; i++) {
                    NSIndexPath *productIndexPath = [NSIndexPath indexPathForRow:i+1 inSection:indexPath.section];
                    [productsIndexPathsForSelectedBrand addObject:productIndexPath];
                }
                [tableView beginUpdates];
                [ProductTableViewExpandedSectionsArray addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
                [tableView insertRowsAtIndexPaths:productsIndexPathsForSelectedBrand withRowAnimation:UITableViewRowAnimationAutomatic];
                [self updateBrandCodeCell:cell isExpanded:YES];
                [tableView endUpdates];
            }
            
        }
        else
        {
            [self hideNoSelectionView];
            [self.view endEditing:YES];
            
            NSMutableArray *arrProductsByBrandCode = [filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section];
            Products *tempPro;
            
            if (arrProductsByBrandCode.count > 0) {
                tempPro = [[self CovertProductDicToProductsObject:[arrProductsByBrandCode objectAtIndex:indexPath.row-1]] copy];
            }

            productDetailsChildViewcontroller.currentProduct=tempPro;
            productStockDetailsChildViewcontroller.currentProduct=tempPro;
            productBonusDetailsChildViewcontroller.currentProduct=tempPro;
            productBonusDetailsChildViewcontroller.productsArray=productsTableViewDataArray;
            
            
            [productDetailsChildViewcontroller UpdateProductDetailsOnTableViewCellSelection];
            [productStockDetailsChildViewcontroller UpdateStockDetailsWithAnimation:productDetailsSegmentControl.selectedSegmentIndex==1?YES:NO];
            [productBonusDetailsChildViewcontroller UpdatebonusDetailsWithAnimation:productDetailsSegmentControl.selectedSegmentIndex==2?YES:NO];
            [self updateTheProductTableViewSelectedCellIndexpath:indexPath];
        }
    } else if (tableView==productsTableView) {
        [self hideNoSelectionView];
        [self.view endEditing:YES];

        Products *tempPro=[self CovertProductDicToProductsObject:[filteredProductsTableViewDataArray objectAtIndex:indexPath.row]];
        productDetailsChildViewcontroller.currentProduct=tempPro;
        productStockDetailsChildViewcontroller.currentProduct=tempPro;
        productBonusDetailsChildViewcontroller.currentProduct=tempPro;
        productBonusDetailsChildViewcontroller.productsArray=productsTableViewDataArray;


        [productDetailsChildViewcontroller UpdateProductDetailsOnTableViewCellSelection];
        [productStockDetailsChildViewcontroller UpdateStockDetailsWithAnimation:productDetailsSegmentControl.selectedSegmentIndex==1?YES:NO];
        [productBonusDetailsChildViewcontroller UpdatebonusDetailsWithAnimation:productDetailsSegmentControl.selectedSegmentIndex==2?YES:NO];
        [self updateTheProductTableViewSelectedCellIndexpath:indexPath];
    }
}

-(void)updateTheProductTableViewSelectedCellIndexpath:(NSIndexPath*)indexpath
{
    /* deselecting the previous selected cell*/
    if(productTableViewSelectedIndexPath!=nil){
        MedRepUpdatedDesignCell *previousSelectedCell=[productsTableView cellForRowAtIndexPath:productTableViewSelectedIndexPath];
        [previousSelectedCell setDeSelectedCellStatus];
        previousSelectedCell.cellDividerImg.hidden=NO;
    }
    
    /*seclecting the present selected cell*/
    MedRepUpdatedDesignCell *currentSelectedcell=[productsTableView cellForRowAtIndexPath:indexpath];
    [currentSelectedcell setSelectedCellStatus];
    currentSelectedcell.cellDividerImg.hidden=YES;
    productTableViewSelectedIndexPath=indexpath;
}


#pragma mark SearchBarDelegate Methods
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if([appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode] && [appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode])
    {
        if(!isSearchBarClearButtonTapped)
        {
            NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:@""];
            NSMutableArray *popOverProductsArray;
            if(predicateArray.count>0)
            {
                NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
                NSMutableArray *tempProductsArray=[productsTableViewDataArray mutableCopy];
                popOverProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
            }
            else{
                popOverProductsArray=productsTableViewDataArray;
            }
            
            SalesWorxProductNameSearchPopOverViewController * popOverVC=[[SalesWorxProductNameSearchPopOverViewController alloc]init];
            popOverVC.popOverContentArray=popOverProductsArray;
            popOverVC.salesWorxPopOverControllerDelegate=self;
            popOverVC.disableSearch=NO;
            popOverVC.isComingFromProductsScreen = true;
            popOverVC.popoverType=KVisitOptionsProductNameSearchPopOverTitle;
            
            UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
            FOCPopOverController=nil;
            UIPopoverController *FOCPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
            FOCPopOverController.delegate=self;
            popOverVC.popOverController=FOCPopOverController;
            popOverVC.titleKey=KVisitOptionsProductNameSearchPopOverTitle;
            [FOCPopOverController setPopoverContentSize:CGSizeMake(300, 500) animated:YES];
            [FOCPopOverController presentPopoverFromRect:searchBar.frame inView:searchBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            [self.view resignFirstResponder];
        }
        //[self filterButtonTapped:searchBar];
        isSearchBarClearButtonTapped=NO;
        
        return NO;
        
    }
    else{
        return YES;
    }
}

-(void)didSelectProductNameSearchPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:@""];
    NSMutableArray *popOverProductsArray;
    if(predicateArray.count>0)
    {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        NSMutableArray *tempProductsArray=[productsTableViewDataArray mutableCopy];
        popOverProductsArray=[[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    else{
        popOverProductsArray=productsTableViewDataArray;
    }
    
    productsSearchBar.text=[[popOverProductsArray valueForKey:@"Description"]objectAtIndex:selectedIndexPath.row];
    proSearchType=ProductSearchByPopOver;
    [self searchProductsContent:productsSearchBar.text];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [productsSearchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
    }
    else{
        [searchBar becomeFirstResponder];
    }
    
    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode]){
        xSearchBarTrailingspaceToFilterButton.constant=16;
        [barcodeScanButton setHidden:YES];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [productsSearchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode] && [appControl.SHOW_PRODUCT_NAME_SEARCH_POPOVER isEqualToString:KAppControlsYESCode]) {
        if([searchText length] == 0) {
            // clearButton Tapped
            isSearchBarClearButtonTapped=YES;
        }
    }
    
    if([searchText length] != 0) {
        productTableViewSelectedIndexPath=nil;
        [self showNoSelectionViewWithMessage];
        [self searchProductsContent:searchBar.text];
    }
    else {
        if (productsTableViewDataArray.count>0) {
            [self searchProductsContent:@""];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [productsSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self.view endEditing:YES];
    if (productsTableViewDataArray.count>0) {
        [self searchProductsContent:@""];
    }
    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode])
    {
        xSearchBarTrailingspaceToFilterButton.constant=47;
        [barcodeScanButton setHidden:NO];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
}

-(NSMutableArray *)getSearchFieldPredicateArray:(NSString *)searchString
{
    NSString* filter = @"%K CONTAINS[cd] %@";
    
    
    NSMutableArray *predicateArray=[[NSMutableArray alloc]init];
    if([[previousFilteredParameters valueForKey:@"Brand_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[previousFilteredParameters valueForKey:@"Brand_Code"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Description"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[previousFilteredParameters valueForKey:@"Description"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Item_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[previousFilteredParameters valueForKey:@"Item_Code"]]];
    }
    
    if([[previousFilteredParameters valueForKey:@"Stock"] length]>0) {
        
        if ([[previousFilteredParameters valueForKey:@"Stock"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue == 0"]];
        }
    }
    if([[previousFilteredParameters valueForKey:@"Bonus"] length]>0) {
        
        if ([[previousFilteredParameters valueForKey:@"Bonus"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue == 0"]];
        }
    }
    
    NSString* promoItemStatus=[previousFilteredParameters valueForKey:@"Promo_Item"];
    
    if ([NSString isEmpty:promoItemStatus]==NO) {
        
        if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsYESCode]];
        }
        
        else if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kUnAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsNOCode]];
        }
        else
        {
            
        }
    }
    
    NSPredicate *BrandCodeSearchPredicate = [NSPredicate predicateWithFormat:filter, @"Brand_Code", searchString];
    NSPredicate *DescriptionSearchPredicate = [SWDefaults fetchMultipartSearchPredicate:searchString withKey:@"Description"];
    NSPredicate *BarCodeSearchPredicate = [NSPredicate predicateWithFormat:filter, @"ProductBarCode", searchString];
    
    NSMutableArray *searchBarPredicatesArray=[[NSMutableArray alloc]init];
    if(![searchString isEqualToString:@""])
    {
        [searchBarPredicatesArray addObject:BrandCodeSearchPredicate];
        [searchBarPredicatesArray addObject:DescriptionSearchPredicate];
        [searchBarPredicatesArray addObject:BarCodeSearchPredicate];
        
    }
    if(predicateArray.count>0 || searchBarPredicatesArray.count>0)
    {
        if(searchBarPredicatesArray.count>0)
        {
            NSPredicate *searchBarPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:searchBarPredicatesArray];
            [predicateArray addObject:searchBarPredicate];
        }
    }
    
    return predicateArray;
}

-(void)searchProductsContent:(NSString*)searchString
{
    if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        if (searchString.length>=0 || searchString.length==0) {
            
            NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:searchString];
            if(predicateArray.count>0)
            {
                NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
                NSMutableArray *tempProductsArray = [productsTableViewDataArray mutableCopy];
                filteredProductsTableViewDataArray = [[tempProductsArray filteredArrayUsingPredicate:predicate] mutableCopy];
                if (filteredProductsTableViewDataArray.count>0) {
                    [self updateProductTableViewOnFilter];
                }
                else if (filteredProductsTableViewDataArray.count==0)
                {
                    [self updateProductTableViewOnFilter];
                }
            }
            else
            {
                filteredProductsTableViewDataArray=productsTableViewDataArray;
                [self updateProductTableViewOnFilter];
            }
            
            productTableViewSelectedIndexPath = nil;
            [self showNoSelectionViewWithMessage];
        }
    } else {
        NSMutableArray *predicateArray= [self getSearchFieldPredicateArray:searchString];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        filteredProductsTableViewDataArray=[[productsTableViewDataArray filteredArrayUsingPredicate:predicate] mutableCopy];
        [productsTableView reloadData];
    }
}

-(void)updateProductTableViewOnFilter
{
    productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsTableViewDataArray valueForKeyPath:@"@distinctUnionOfObjects.Brand_Code"]];
    productsBrandCodesArray= [[NSMutableArray alloc]initWithArray:[productsBrandCodesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    
    
    
    /** removing invalid brandcodes*/
    filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
    allowSearchBarEditing=NO;
    [self showActivityIndicatorInSearchBar];
    [productsTableView reloadData];
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        filteredProductsArrayGroupedByBrandCode=[productsArrayGroupedByBrandCode mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            productTableViewSelectedIndexPath = nil;
            allowSearchBarEditing=YES;
            [self hideActivityIndicatorInSearchBar];
            if(productsBrandCodesArray.count==1)
            {
                [self ExpandFirstProductsSection];
            }
            else
            {
                ProductTableViewExpandedSectionsArray=[[NSMutableArray alloc]init];
            }
            
            [productsTableView reloadData];
            
            /** if only one product is there in table view , selecting the first product*/
            if(filteredProductsTableViewDataArray.count==1 && (proSearchType==ProductSearchByPopOver || proSearchType==ProductSearchByBarCode))
            {
                [self tableView:productsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                proSearchType=ProductSerachNormal;
            }
        });
    });
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0); // called before text changes
{
    if(allowSearchBarEditing)
        return YES;
    else
        return NO;
}

-(void)AddActivityIndicatorInProductSearchBar
{
    searchBarActivitySpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //set frame for activity indicator
    [searchBarActivitySpinner setFrame:CGRectMake(100, 200, 100, 100)];
    [productsTableView addSubview: searchBarActivitySpinner];
}
-(void)showActivityIndicatorInSearchBar
{
    [productsTableView setUserInteractionEnabled:NO];
    [filterButton setUserInteractionEnabled:NO];
    [searchBarActivitySpinner startAnimating];
}
-(void)hideActivityIndicatorInSearchBar
{
    [productsTableView setUserInteractionEnabled:YES];
    [filterButton setUserInteractionEnabled:YES];
    [searchBarActivitySpinner stopAnimating];
}

#pragma mark Filter Button Action

- (IBAction)filterButtonTapped:(id)sender {
    
    if (productsTableViewDataArray.count>0) {
        [productsSearchBar setShowsCancelButton:NO animated:YES];
        productsSearchBar.text=@"";
        [productsSearchBar resignFirstResponder];
        [self.view endEditing:YES];
        [self searchProductsContent:@""];
        
        productTableViewSelectedIndexPath = nil;
        
        UIButton* searchBtn=(id)sender;
        
        SalesWorxProductsFilterViewController * popOverVC=[[SalesWorxProductsFilterViewController alloc]init];
        popOverVC.delegate=self;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict=previousFilteredParameters;
            
        }
        popOverVC.productsArray=productsTableViewDataArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        
        AppControl * appControl=[AppControl retrieveSingleton];
        NSString* showPromoView=appControl.ENABLE_PROMO_ITEM_VALIDITY;
        if ([NSString isEmpty:showPromoView]==NO) {
            if ([showPromoView isEqualToString:KAppControlsYESCode]) {
                
                [filterPopOverController setPopoverContentSize:CGSizeMake(366, 530) animated:YES];
                
            }
            else
            {
                [filterPopOverController setPopoverContentSize:CGSizeMake(366, 490) animated:YES];
                
            }
        }
        
        //[filterPopOverController setPopoverContentSize:CGSizeMake(366, 560) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
        CGRect currentFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];
        
        [filterPopOverController presentPopoverFromRect:currentFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];
    }
}


-(void)filteredProducts:(NSMutableArray*)filteredArray
{
    if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
        filteredProductsTableViewDataArray=filteredArray;
        [self updateProductTableViewOnFilter];
    } else {
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
        filteredProductsTableViewDataArray=filteredArray;
        productTableViewSelectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [productsTableView reloadData];
        
        
        if(filteredProductsTableViewDataArray.count>0){
            [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:productTableViewSelectedIndexPath];
        }
    }
}
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    previousFilteredParameters=parametersDict;
}
-(void)productsFilterdidReset
{
    if ([appControl.FS_PRODUCT_GROUP_BY_BRAND isEqualToString:KAppControlsYESCode]) {
        previousFilteredParameters=[[NSMutableDictionary alloc]init];
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        filteredProductsTableViewDataArray=productsTableViewDataArray;
        [self updateProductTableViewOnFilter];
    } else {
        previousFilteredParameters=[[NSMutableDictionary alloc]init];
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
        productTableViewSelectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        filteredProductsTableViewDataArray=productsTableViewDataArray;
        [productsTableView reloadData];
        if(filteredProductsTableViewDataArray.count>0){
            [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:productTableViewSelectedIndexPath];
        }
    }
}
-(void)productsFilterDidClose
{
    
    
}

#pragma mark Barcode_Scan
-(IBAction)barCodeScanButtonTapped:(id)sender
{
    
    [self.view endEditing:YES];
    SalesWorxBarCodeScannerManager *barcodeManager=[[SalesWorxBarCodeScannerManager alloc]init];
    barcodeManager.swxBCMdelegate=self;
    [barcodeManager ScanBarCode];
}

- (void)BarCodeScannerButtonBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    [reader dismissViewControllerAnimated:YES completion:^{
        
        NSLog(@"scanned bar code is %@", symbol.data);
        NSLog(@"symbol is %@", symbol.typeName);
        
        NSPredicate *barCodePredicate=[NSPredicate predicateWithFormat:@"SELF.ProductBarCode ==[cd] %@",symbol.data];
        if([[productsTableViewDataArray filteredArrayUsingPredicate:barCodePredicate] count]>0)
        {
            previousFilteredParameters=[[NSMutableDictionary alloc]init];
            productsSearchBar.text=symbol.data;
            proSearchType=ProductSearchByBarCode;
            [self searchProductsContent:productsSearchBar.text];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"No product found" withController:self];
        }
    }];
}
#pragma mark UIScrollView
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView==pagingScrollView){
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        [productDetailsSegmentControl setSelectedSegmentIndex:page animated:YES];
    }
}
#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionBottomMarginConstraint.constant=KZero;
}
-(void)showNoSelectionViewWithMessage
{
    [NoSelectionView setHidden:NO];
    NoSelectionLeadingMarginConstraint.constant=264;
    NoSelectionBottomMarginConstraint.constant=8;
}
-(Products *)CovertProductDicToProductsObject:(NSDictionary *)currentProductDict{
    
    /** converting database product dic to products object*/
    Products * currentProduct=[[Products alloc]init];

    currentProduct.Brand_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Brand_Code"]];
    currentProduct.Promo_Item=[currentProductDict valueForKey:@"Promo_Item"];
    currentProduct.selectedUOM=[currentProductDict valueForKey:@"selectedUOM"];
    currentProduct.Discount=[currentProductDict valueForKey:@"Discount"];
    currentProduct.Agency=[currentProductDict valueForKey:@"Agency"];
    currentProduct.Category=[currentProductDict valueForKey:@"Category"];
    currentProduct.Description=[currentProductDict valueForKey:@"Description"];
    currentProduct.IsMSL=[currentProductDict valueForKey:@"IsMSL"];
    currentProduct.ItemID=[currentProductDict valueForKey:@"ItemID"];
    currentProduct.Item_Code=[currentProductDict valueForKey:@"Item_Code"];
    currentProduct.OrgID=[currentProductDict valueForKey:@"OrgID"];
    currentProduct.Sts=[currentProductDict valueForKey:@"Sts"];
    currentProduct.Inventory_Item_ID=[currentProductDict valueForKey:@"Inventory_Item_ID"];
    currentProduct.primaryUOM=[currentProductDict valueForKey:@"primaryUOM"];
    currentProduct.bonus=[currentProductDict valueForKey:@"bonus"];
    currentProduct.stock=[currentProductDict valueForKey:@"stock"];
    currentProduct.ProductBarCode=[currentProductDict valueForKey:@"ProductBarCode"];
    currentProduct.specialDiscount=[currentProductDict valueForKey:@"specialDiscount"];
    currentProduct.OnOrderQty=[currentProductDict valueForKey:@"OnOrderQty"];
    currentProduct.maxExpiryDate=[currentProductDict valueForKey:@"maxExpiryDate"];
    currentProduct.nearestExpiryDate=[currentProductDict valueForKey:@"nearestExpiryDate"];
    
    if( [appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsNOCode]){
        ProductUOM *UOM=[[ProductUOM alloc]init];
        UOM.Item_UOM=currentProduct.primaryUOM;
        UOM.Item_UOM_ID=@"1000";
        UOM.Item_Code=currentProduct.Item_Code;
        UOM.Organization_ID=currentProduct.OrgID;
        UOM.Conversion=@"1";
        UOM.isPrimaryUOM=@"Y";
        currentProduct.ProductUOMArray=[[NSMutableArray alloc] initWithObjects:UOM, nil];
    }else{
        currentProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager] getUOMsForProduct:currentProduct];
    }
    

    return currentProduct;
}
#pragma mark MultiUOM
-(void)UpdateProductDetailsOnUOMSelection:(ProductUOM *)proUOM
{
    /** PRIMARY UOM OBJECT*/
    Products *tempPro=[self CovertProductDicToProductsObject:[filteredProductsTableViewDataArray objectAtIndex:productTableViewSelectedIndexPath.row]];
    
    
    /** CONEVRTING TO SELECTED UOM OBJECT*/
    tempPro=[self getUpdatedProductDetails:tempPro ToSelectedUOM:proUOM];
    
    
    productDetailsChildViewcontroller.currentProduct=tempPro;
    productStockDetailsChildViewcontroller.currentProduct=tempPro;
    productBonusDetailsChildViewcontroller.currentProduct=tempPro;
    productBonusDetailsChildViewcontroller.productsArray=productsTableViewDataArray;
    
    
    [productDetailsChildViewcontroller UpdateProductDetailsOnTableViewCellSelection];
    [productStockDetailsChildViewcontroller UpdateStockDetailsWithAnimation:productDetailsSegmentControl.selectedSegmentIndex==1?YES:NO];
    [productBonusDetailsChildViewcontroller UpdatebonusDetailsWithAnimation:productDetailsSegmentControl.selectedSegmentIndex==2?YES:NO];
    
}
-(Products *)getUpdatedProductDetails:(Products *)product ToSelectedUOM:(ProductUOM *)proUOM
{
    Products *pro=[(Products *)product copy];
    /** FETCH PRIMARY UOM OBJECT*/
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
    NSMutableArray *UOMarray=[product.ProductUOMArray mutableCopy];
    NSMutableArray *filtredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    
    /*CONEVRT STOCK RELATIVE TO SELECTED UOM*/
    ProductUOM *primaryUOMObj=[filtredArray objectAtIndex:0];
    pro.stock=[NSString stringWithFormat:@"%ld",(long)[[NSString stringWithFormat:@"%f",([pro.stock doubleValue]*[primaryUOMObj.Conversion doubleValue])/[proUOM.Conversion doubleValue]] integerValue]];
    pro.selectedUOM=proUOM.Item_UOM;
    return  pro;
    
}

@end
