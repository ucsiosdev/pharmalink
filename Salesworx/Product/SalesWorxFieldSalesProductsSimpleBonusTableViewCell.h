//
//  SalesWorxFieldSalesProductsSimpleBonusTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/20/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxFieldSalesProductsSimpleBonusTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *schemeLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *schemeTypeLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *validFromLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *validToLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *UOMLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *FromQtyLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ToQtyLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *BonusQtyLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *BonusItemLabel;

@end
