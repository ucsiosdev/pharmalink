//
//  SalesWorxFieldSalesProductsStockPODetailsTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/18/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxFieldSalesProductsStockPODetailsTableViewCell : UITableViewCell
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *poNumberLabel;
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *PoDueDateLabel;
@property (strong, nonatomic)   IBOutlet SalesWorxSingleLineLabel *poDateLabel;
@property (strong, nonatomic)  IBOutlet SalesWorxSingleLineLabel *poQtyLabel;

@end
