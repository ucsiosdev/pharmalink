//
//  SalesWorxFieldsSalesProductsStockDetailsViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldsSalesProductsStockDetailsViewController.h"
#import "SalesWorxFieldSalesProductsStockLotsTableViewCell.h"
#import "SalesWorxFieldSalesProductsStockPositionTableViewCell.h"
#import "SalesWorxFieldSalesProductsStockPODetailsTableViewCell.h"
#import "AppControl.h"
#import "SWDatabaseManager.h"
#import "MedRepQueries.h"
@interface SalesWorxFieldsSalesProductsStockDetailsViewController ()
{
    AppControl *appControl;
}
@end

@implementation SalesWorxFieldsSalesProductsStockDetailsViewController
@synthesize currentProduct;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    stockLotsArray=[[NSMutableArray alloc]init];
    wareHouseWiseStockArray=[[NSMutableArray alloc]init];
    poDetailsArray=[[NSMutableArray alloc]init];
    appControl=[[AppControl alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [self UpdateStockLotsForProductWithAnimation:NO];
    [self UpdateWareHouseStockLotsProductWithAnimation:NO];
    [self UpdatePOStockProductWithAnimation:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewDidAppear:(BOOL)animated{

}
-(void)UpdateStockDetailsWithAnimation:(BOOL)Animation{

    Animation=NO;
    [self UpdateStockLotsForProductWithAnimation:Animation];
    [self UpdateWareHouseStockLotsProductWithAnimation:Animation];
    [self UpdatePOStockProductWithAnimation:Animation];

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==stockLotsTableView){
        return stockLotsArray.count;
    }else if(tableView==warehouseWiseStockPositionTableView) {
        return wareHouseWiseStockArray.count;
    }else if(tableView==PODetailsTableView) {
        return poDetailsArray.count;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView==stockLotsTableView){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        StockLotsTableHeader.backgroundColor=kUITableViewHeaderBackgroundColor;
        [view addSubview:StockLotsTableHeader];
        return StockLotsTableHeader;
    }else if(tableView==warehouseWiseStockPositionTableView) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        StockPositionTableHeader.backgroundColor=kUITableViewHeaderBackgroundColor;
        [view addSubview:StockPositionTableHeader];
        return StockPositionTableHeader;
    }else if(tableView==PODetailsTableView) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        StockPODetailsTableHeader.backgroundColor=kUITableViewHeaderBackgroundColor;
        [view addSubview:StockPODetailsTableHeader];
        return StockPODetailsTableHeader;
    }
    
    
    return [[UIView alloc]initWithFrame:CGRectZero];
    
}   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if(tableView==stockLotsTableView){
        static NSString* identifier=@"StockLotsTableCellIdentifier";
        SalesWorxFieldSalesProductsStockLotsTableViewCell *cell = (SalesWorxFieldSalesProductsStockLotsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFieldSalesProductsStockLotsTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        NSDictionary *currentStockDict= [stockLotsArray objectAtIndex:indexPath.row];
        
        Stock * lot=[[Stock alloc]init];
        //NSLog(@"refined stock dict is %@", currentStockDict);
        lot.Lot_No=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Lot_No"]];
        lot.Lot_Qty=[NSString stringWithFormat:@"%0.0f",[[currentStockDict valueForKey:@"Lot_Qty"] doubleValue]];
        lot.Org_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Org_ID"]];
        lot.Stock_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Stock_ID"]];
        lot.Expiry_Date=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Expiry_Date"]];
        lot.warehouse=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Description"]];

        
        cell.lotNumberLabel.text=lot.Lot_No;
        cell.QtyLabel.text=[NSString stringWithFormat:@"%@ %@",lot.Lot_Qty,currentProduct.primaryUOM];
        cell.expDateLabel.text=[lot.Expiry_Date isEqualToString:@""]?KNotApplicable:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:lot.Expiry_Date];
        cell.warehouseLabel.text=lot.warehouse;
        
        
        cell.lotNumberLabel.textColor=MedRepMenuTitleFontColor;
        cell.QtyLabel.textColor=MedRepMenuTitleFontColor;
        cell.expDateLabel.textColor=MedRepMenuTitleFontColor;
        cell.warehouseLabel.textColor=MedRepMenuTitleFontColor;
        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
        return cell;

    }else if(tableView==warehouseWiseStockPositionTableView) {
        static NSString* identifier=@"StockPositionTableCellIdentifier";
        SalesWorxFieldSalesProductsStockPositionTableViewCell *cell = (SalesWorxFieldSalesProductsStockPositionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFieldSalesProductsStockPositionTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        NSDictionary *wareHouseStock= [wareHouseWiseStockArray objectAtIndex:indexPath.row];
        
        
        cell.OrgCodeLabel.text=[wareHouseStock valueForKey:@"OrganizationID"];
        cell.PoQtyLabel.text=[NSString stringWithFormat:@"%@ %@",[wareHouseStock valueForKey:@"OSPoQty"],currentProduct.primaryUOM];
        cell.awaitingForShippingLabel.text=[NSString stringWithFormat:@"%@ %@",[wareHouseStock valueForKey:@"AWShipQty"],currentProduct.primaryUOM];
        cell.reserveLabel.text=[NSString stringWithFormat:@"%@ %@",[wareHouseStock valueForKey:@"ReserveQty"],currentProduct.primaryUOM];
        cell.onHoldLabel.text=[NSString stringWithFormat:@"%@ %@",[wareHouseStock valueForKey:@"OnHoldQty"],currentProduct.primaryUOM];
        cell.orgNameLabel.text=[wareHouseStock valueForKey:@"WareHouseID"];
        cell.expiryDateLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[wareHouseStock valueForKey:@"expiryDateLabel"]]];

        return cell;

        
    }else if(tableView==PODetailsTableView) {
        static NSString* identifier=@"PoTableCellIdentifier";
        SalesWorxFieldSalesProductsStockPODetailsTableViewCell *cell = (SalesWorxFieldSalesProductsStockPODetailsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFieldSalesProductsStockPODetailsTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        NSDictionary *poStock= [poDetailsArray objectAtIndex:indexPath.row];

        cell.poNumberLabel.text=[poStock valueForKey:@"PoNumber"];
        cell.poDateLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[poStock valueForKey:@"PODate"]]];
        cell.PoDueDateLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:[poStock valueForKey:@"PODueDate"]]];
        cell.poQtyLabel.text=[NSString stringWithFormat:@"%@ %@",[poStock valueForKey:@"OSPoQty"],currentProduct.primaryUOM];

        return cell;

    }
    return nil;
}

/** Stock Lots*/
-(void)UpdateStockLotsForProductWithAnimation:(BOOL)Animated
{
    
    stockLotsArray=[[NSMutableArray alloc]init];
  __block  NSMutableArray * tempProductStockArray=[[NSMutableArray alloc]init];;
    /** fetching product details */
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:currentProduct WareHouseId:KNotApplicable];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            stockLotsArray=tempProductStockArray;
            if (Animated) {
                [stockLotsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            }else{
                [stockLotsTableView reloadData];
            }

        });
      
    });
}


/** Ware house Stock*/
-(void)UpdateWareHouseStockLotsProductWithAnimation:(BOOL)Animated
{
    wareHouseWiseStockArray=[[NSMutableArray alloc]init];
    __block  NSMutableArray * tempProductStockArray=[[NSMutableArray alloc]init];;
    /** fetching product details */
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchProductWareHouseStock:currentProduct];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            wareHouseWiseStockArray=tempProductStockArray;
            if (Animated) {
                [warehouseWiseStockPositionTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            }else{
                [warehouseWiseStockPositionTableView reloadData];
            }
        });
    });
}
/** PO Stock*/
-(void)UpdatePOStockProductWithAnimation:(BOOL)Animated
{
    poDetailsArray=[[NSMutableArray alloc]init];
    __block  NSMutableArray * tempProductStockArray=[[NSMutableArray alloc]init];;
    /** fetching product details */
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        tempProductStockArray= [[SWDatabaseManager retrieveManager]fetchProductPOStock:currentProduct];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            poDetailsArray=tempProductStockArray;
            if (Animated) {
                [PODetailsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            }else{
                [PODetailsTableView reloadData];
            }
        });
    });
}

@end
