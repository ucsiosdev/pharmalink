//
//  SalesWorxProductsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepHeader.h"
#import "XYPieChart.h"
#import <MedRepTextField.h>
#import "StringPopOverViewController_ReturnRow.h"
#import "SalesWorxProductsFilterViewController.h"
#import "SalesWorxSingleLineLabel.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxImageView.h"
#import "ZBarSDK.h"
#import "SalesWorxBarCodeScannerManager.h"
@interface SalesWorxProductsViewController : UIViewController<XYPieChartDataSource,XYPieChartDelegate,UITableViewDataSource,UITableViewDelegate,StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,SalesWorxProductsFilterViewControllerDelegate,UIScrollViewDelegate>

{
    NSMutableArray * productsArray;

    IBOutlet MedRepElementDescriptionLabel *uomLbl;
    IBOutlet SalesWorxSingleLineLabel *onOrderQtyLbl;
    IBOutlet MedRepTextField *uomTextField;
    NSMutableArray * unfilteredProductsArray;
    NSMutableArray * filteredProductsArray;
    NSMutableArray * productPriceListArray;
    NSMutableArray * productBonusArray;
    IBOutlet UIButton *stockStatusBtn;
    IBOutlet SalesWorxSingleLineLabel *specialDiscountContentLbl;
    
    IBOutlet SalesWorxImageView *statusImageView;
    IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
    IBOutlet MedRepElementTitleLabel *onOrderQtyTitleLbl;
    IBOutlet NSLayoutConstraint *stockViewTrailingConstraint;
    
    IBOutlet NSLayoutConstraint *imageViewBottomConstraint;
    IBOutlet NSLayoutConstraint *targetViewHeightConstraint;
    IBOutlet NSLayoutConstraint *targetViewTrailingConstraint;
    IBOutlet NSLayoutConstraint *targetViewWidthConstraint;
    IBOutlet SalesWorxSingleLineLabel *onOrderQuantityLabel;
    IBOutlet MedRepElementTitleLabel *specialDiscountLbl;
    IBOutlet UIImageView *productImageView;
    NSMutableArray * indexPathArray;
    NSMutableArray* productStockArray;
    IBOutlet UITableView *productsTableView;
    IBOutlet UITableView *stockTableView;
    IBOutlet UITableView *bonusTableView;
    Products* selectedProduct;
    PriceList* selectProductPriceList;
    IBOutlet MedRepHeader *productNameLbl;
    IBOutlet MedRepElementDescriptionLabel *brandLbl;
    IBOutlet MedRepElementDescriptionLabel *agencyLbl;
//    IBOutlet MedRepElementDescriptionLabel *uomLbl;
    IBOutlet MedRepElementDescriptionLabel *mslLbl;
    IBOutlet MedRepElementDescriptionLabel *stockLbl;
    IBOutlet MedRepElementDescriptionLabel *wholesalePriceLbl;
    IBOutlet MedRepElementDescriptionLabel *retailPriceLbl;
    
    IBOutlet UIView *targetView;
    NSInteger refinedStock;
    NSInteger refinedOnOrderStock;
    
    NSString* selectedMediaFileID;
    
    IBOutlet UIView *stockView;
    
    BOOL isSearching;
    
    IBOutlet XYPieChart *targetPieChart;
    NSString* showTarget;
    
    NSMutableArray *slicesForTarget;
     NSArray        *sliceColorsForTarget;
    
    IBOutlet UISearchBar *productsSearchBar;
    IBOutlet UIButton *filterButton;
    
    NSIndexPath * selectedIndexPath;
    
    IBOutlet SalesWorxSingleLineLabel *discountLbl;
    UIPopoverController * popOverController;
    StringPopOverViewController_ReturnRow * locationPopOver;
    NSString* popString;
    NSMutableArray* uomsArray;
    NSString* selectedUOM;
    
    UIImageView * blurredBgImage;
    NSMutableDictionary * previousFilteredParameters;
    UIPopoverController * filterPopOverController;
    
    ZBarReaderViewController *reader;
    ProductSearchType proSearchType;
    IBOutlet NSLayoutConstraint *xSearchBarTrailingspaceToFilterButton;
    IBOutlet SalesWorxDefaultButton *barcodeScanButton;

    IBOutlet SalesWorxDefaultButton *assortmentBnsButton;
    
    IBOutlet UIView *NoSelectionView;
    IBOutlet NSLayoutConstraint *NoSelectionHeightConstraint;
    IBOutlet NSLayoutConstraint *NoSelectionBottomMarginConstraint;
    IBOutlet NSLayoutConstraint *bonusViewHeightConstraint;
    
    NSMutableArray *AssortmentPlansArray;
}
- (IBAction)filterButtonTapped:(id)sender;
- (void)BarCodeScannerButtonBackPressed:(id)sender;
-(IBAction)barCodeScanButtonTapped:(id)sender;
- (IBAction)AssortmentBnsButtonTapped:(id)sender;

@end
