//
//  SalesWorxFieldsSalesProductsBonusDetailsViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldsSalesProductsBonusDetailsViewController.h"
#import "SWDatabaseManager.h"
#import "AppControl.h"
#import "SalesWorxFieldSalesProductsSimpleBonusTableViewCell.h"
#import "SalesWorxFieldSalesProductsPriceListTableViewCell.h"
#import "SalesWorxAssortmentBonusManager.h"
#import "SalesWorxAssortmentBonusInfoViewController.h"
@interface SalesWorxFieldsSalesProductsBonusDetailsViewController ()
{
    AppControl *appControl;
}
@end

@implementation SalesWorxFieldsSalesProductsBonusDetailsViewController
@synthesize currentProduct,productsArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=[UIColor clearColor];
    productPriceListArray=[[NSMutableArray alloc]init];
    productSimpleBonusDetailsArray=[[NSMutableArray alloc]init];
    appControl=[AppControl retrieveSingleton];
    assortmentBonusPlansArray=[[NSMutableArray alloc]init];
}
-(void)viewWillAppear:(BOOL)animated{
    [self UpdatebonusDetailsWithAnimation:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)UpdatebonusDetailsWithAnimation:(BOOL)Animation{
    Animation=NO;
    productPriceListArray=[[SWDatabaseManager retrieveManager]fetchavAvailableProductPriceLists:currentProduct];
    productPriceListDescriptionArray = [[SWDatabaseManager retrieveManager]fetchavAvailableProductPriceListsDescription:currentProduct];
    NSMutableArray *tempGenericProductPriceListsArray= [[productPriceListArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Is_Generic='Y'"]] mutableCopy];
    
    
     productSimpleBonusDetailsArray=[[[SWDatabaseManager retrieveManager] dbGetBonusInfo:currentProduct.Item_Code] mutableCopy];
    
    /** populating generic price list data*/
    if(tempGenericProductPriceListsArray.count>0){
        NSDictionary *genericPriceList=[tempGenericProductPriceListsArray objectAtIndex:0];
        productWholesalePriceLabel.text=[[NSString stringWithFormat:@"%@",[genericPriceList valueForKey:@"Unit_Selling_Price"]] currencyString];
        productRetailPriceLabel.text=[[NSString stringWithFormat:@"%@",[genericPriceList valueForKey:@"Unit_List_Price"]] currencyString];
    }else{
        productWholesalePriceLabel.text=KNotApplicable;
        productRetailPriceLabel.text=KNotApplicable;
    }

    productDefaultDicountLabel.text=[appControl.ALLOW_DISCOUNT isEqualToString:KAppControlsYESCode]?[[NSString stringWithFormat:@"%@",currentProduct.Discount] percentageString]:KNotApplicable;
    productSpecialDicountLabel.text=[appControl.ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsYESCode]?[[NSString stringWithFormat:@"%@",currentProduct.specialDiscount] percentageString]:KNotApplicable;
    
    if(Animation){
        [productPriceListsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        [SimpleBonusTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];

    }else{
        [productPriceListsTableView reloadData];
        [SimpleBonusTableView reloadData];

    }
    
    
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
        assortmentBonusPlansArray=[self FetchAssortmentBonusPlans];
        if(assortmentBonusPlansArray.count>0){
            [AssortmentBpnusDetailsButton setHidden:NO];
        }else{
            [AssortmentBpnusDetailsButton setHidden:YES];
        }
    }else{
        [AssortmentBpnusDetailsButton setHidden:YES];
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==SimpleBonusTableView){
        return productSimpleBonusDetailsArray.count;
    }else if(tableView==productPriceListsTableView) {
        return productPriceListArray.count;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView==SimpleBonusTableView){
        return SimpleBonusTableHeader;
    }else if(tableView==productPriceListsTableView) {
        return PriceListTableHeader;
    }
    
    
    return [[UIView alloc]initWithFrame:CGRectZero];
    
}   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if(tableView==SimpleBonusTableView){
        static NSString* identifier=@"SimpleBonusTableCellIdentifier";
        SalesWorxFieldSalesProductsSimpleBonusTableViewCell *cell = (SalesWorxFieldSalesProductsSimpleBonusTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFieldSalesProductsSimpleBonusTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        NSMutableDictionary *bonusItemInfo=[productSimpleBonusDetailsArray objectAtIndex:indexPath.row];
        ProductBonusItem * bonusItem=[[ProductBonusItem alloc]init];
        bonusItem.Description=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Description"]];
        bonusItem.Get_Add_Per=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Add_Per"]];
        bonusItem.Get_Item=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Item"]];
        bonusItem.Price_Break_Type_Code=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Price_Break_Type_Code"]];
        bonusItem.Get_Qty=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_Qty"]];
        bonusItem.Prom_Qty_From=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Prom_Qty_From"]];
        bonusItem.Prom_Qty_To=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Prom_Qty_To"]];
        bonusItem.Plan_Name=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Plan_Name"]];
        bonusItem.Get_UOM=[SWDefaults getValidStringValue:[bonusItemInfo valueForKey:@"Get_UOM"]];
        
        cell.schemeLabel.text=bonusItem.Plan_Name;
        cell.schemeTypeLabel.text=bonusItem.Price_Break_Type_Code;
        cell.validFromLabel.text=KNotApplicable;
        cell.validToLabel.text=KNotApplicable;
        cell.UOMLabel.text=bonusItem.Get_UOM;
        cell.FromQtyLabel.text=bonusItem.Prom_Qty_From;
        cell.ToQtyLabel.text=bonusItem.Prom_Qty_To;
        cell.BonusQtyLabel.text=bonusItem.Get_Qty;
        cell.BonusItemLabel.text=bonusItem.Description;
        
        cell.BonusQtyLabel.textColor=MedRepMenuTitleFontColor;
        cell.ToQtyLabel.textColor=MedRepMenuTitleFontColor;
        cell.FromQtyLabel.textColor=MedRepMenuTitleFontColor;
        cell.UOMLabel.textColor=MedRepMenuTitleFontColor;
        cell.validToLabel.textColor=MedRepMenuTitleFontColor;
        cell.validFromLabel.textColor=MedRepMenuTitleFontColor;
        cell.schemeTypeLabel.textColor=MedRepMenuTitleFontColor;
        cell.schemeLabel.textColor=MedRepMenuTitleFontColor;
        cell.BonusItemLabel.textColor=MedRepMenuTitleFontColor;

        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;

        return cell;
        
    }else if(tableView==productPriceListsTableView) {
        static NSString* identifier=@"PriceListTableCellIdentifier";
        SalesWorxFieldSalesProductsPriceListTableViewCell *cell = (SalesWorxFieldSalesProductsPriceListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFieldSalesProductsPriceListTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
       
                NSDictionary *priceListDict=[productPriceListArray objectAtIndex:indexPath.row];
                NSDictionary *priceListDescriptionDict=[productPriceListDescriptionArray objectAtIndex:indexPath.row];
                PriceList * priceList=[[PriceList alloc]init];
                priceList.Price_List_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Price_List_ID"]];
                priceList.Inventory_Item_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Inventory_Item_ID"]];
                priceList.Organization_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Organization_ID"]];
                priceList.Item_UOM=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Item_UOM"]];
                priceList.Unit_Selling_Price=[[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Unit_Selling_Price"]] currencyString];
                //priceList.Unit_List_Price=[[[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Unit_List_Price"]] currencyString] currencyString];
        
                priceList.Unit_List_Price=[[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Unit_List_Price"]] currencyString];
        
                cell.priceListNameLabel.text=[priceListDescriptionDict valueForKey:@"Description"];//@"Standard";
                cell.UOMLabel.text=[NSString stringWithFormat:@"%@",currentProduct.selectedUOM];
                cell.wholeSalePrice.text=priceList.Unit_Selling_Price;
                cell.RetailPriceLabel.text=priceList.Unit_List_Price;
                cell.ValidFromLabel.text=KNotApplicable;
                cell.validToLabel.text=KNotApplicable;

        
                cell.priceListNameLabel.textColor=MedRepMenuTitleFontColor;
                cell.UOMLabel.textColor=MedRepMenuTitleFontColor;
                cell.wholeSalePrice.textColor=MedRepMenuTitleFontColor;
                cell.RetailPriceLabel.textColor=MedRepMenuTitleFontColor;
                cell.ValidFromLabel.textColor=MedRepMenuTitleFontColor;
                cell.validToLabel.textColor=MedRepMenuTitleFontColor;
                cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;

        
        
        return cell;
        
        
    }
    return nil;
}

- (IBAction)AssortMentBonusButtonTapped:(id)sender{
   
   if(assortmentBonusPlansArray.count>0){
        SalesWorxAssortmentBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesWorxAssortmentBonusInfoViewController alloc]initWithNibName:@"SalesWorxAssortmentBonusInfoViewController" bundle:[NSBundle mainBundle]];
        salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
       salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
       
       SalesworxAssortmentBonusPlan *plan=[assortmentBonusPlansArray objectAtIndex:0];
       
       for (NSInteger i=0; i<plan.OfferProductsArray.count; i++) {
           [plan.OfferProductsArray replaceObjectAtIndex:i withObject:[self CovertProductDicToProductsObject:[plan.OfferProductsArray objectAtIndex:i]]];
       }
       
       for (NSInteger i=0; i<plan.bonusProductsArray.count; i++) {
           [plan.bonusProductsArray replaceObjectAtIndex:i withObject:[self CovertProductDicToProductsObject:[plan.bonusProductsArray objectAtIndex:i]]];
       }
       
       salesOrderBonusInfoViewController.plan=[assortmentBonusPlansArray objectAtIndex:0];
       [self.parentViewController.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
  
   }
    
}
-(NSMutableArray *)FetchAssortmentBonusPlans{
    NSMutableArray *plansArray=
    [[[SalesWorxAssortmentBonusManager alloc] init] FetchAssortmentBonusPlansAvailableForProduct:currentProduct.Item_Code ProductsArray:productsArray];
    return plansArray;
}

-(NSString* )getProductSpecailDisount
{
    NSString* specialDiscount;

    
    if ([appControl.ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsYESCode]){
        specialDiscount=[[SWDatabaseManager retrieveManager] dbGetSpecialDiscount:currentProduct.Inventory_Item_ID];
    }
    else{
        specialDiscount=@"0.0";
    }
    return specialDiscount;
}

-(Products *)CovertProductDicToProductsObject:(NSDictionary *)currentProductDict{
    
    /** converting database product dic to products object*/
    Products *pro=[[Products alloc]init];
    
    pro.Brand_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Brand_Code"]];
    pro.Promo_Item=[currentProductDict valueForKey:@"Promo_Item"];
    pro.selectedUOM=[currentProductDict valueForKey:@"selectedUOM"];
    pro.Discount=[currentProductDict valueForKey:@"Discount"];
    pro.Agency=[currentProductDict valueForKey:@"Agency"];
    pro.Category=[currentProductDict valueForKey:@"Category"];
    pro.Description=[currentProductDict valueForKey:@"Description"];
    pro.IsMSL=[currentProductDict valueForKey:@"IsMSL"];
    pro.ItemID=[currentProductDict valueForKey:@"ItemID"];
    pro.Item_Code=[currentProductDict valueForKey:@"Item_Code"];
    pro.OrgID=[currentProductDict valueForKey:@"OrgID"];
    pro.Sts=[currentProductDict valueForKey:@"Sts"];
    pro.Inventory_Item_ID=[currentProductDict valueForKey:@"Inventory_Item_ID"];
    pro.primaryUOM=[currentProductDict valueForKey:@"primaryUOM"];
    pro.bonus=[currentProductDict valueForKey:@"bonus"];
    pro.stock=[currentProductDict valueForKey:@"stock"];
    pro.ProductBarCode=[currentProductDict valueForKey:@"ProductBarCode"];
    pro.specialDiscount=[currentProductDict valueForKey:@"specialDiscount"];
    pro.OnOrderQty=[currentProductDict valueForKey:@"OnOrderQty"];
    pro.maxExpiryDate=[currentProductDict valueForKey:@"maxExpiryDate"];
    pro.nearestExpiryDate=[currentProductDict valueForKey:@"nearestExpiryDate"];
    
    return pro;
}

@end
