//
//  SalesWorxFieldsSalesProductsDetailsViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepHeader.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepTextField.h"
#import "XYPieChart.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxImageView.h"
#import "MedRepButton.h"

@interface SalesWorxFieldsSalesProductsDetailsViewController : UIViewController<XYPieChartDelegate,XYPieChartDataSource>
{
    NSString* selectedMediaFileID;
    IBOutlet UIImageView *productImageView;
    IBOutlet MedRepHeader *productNameLabel;
    IBOutlet MedRepElementDescriptionLabel *productBrandNameLabel;
    IBOutlet MedRepElementDescriptionLabel *productAgencyNameLabel;
    IBOutlet MedRepTextField *productUomsTextField;
    IBOutlet MedRepElementDescriptionLabel *productMSLStatusLabel;
    IBOutlet MedRepElementDescriptionLabel *productStockLabel;
    IBOutlet MedRepElementDescriptionLabel *productOnOrderQtyLabel;
    IBOutlet MedRepElementDescriptionLabel *productWholesalePriceLabel;
    IBOutlet MedRepElementDescriptionLabel *productRetailPriceLabel;
    IBOutlet MedRepElementDescriptionLabel *productDefaultDicountLabel;
    IBOutlet MedRepElementDescriptionLabel *productSpecialDicountLabel;
    IBOutlet MedRepElementDescriptionLabel *productNearestExpiryLabel;
    IBOutlet MedRepElementDescriptionLabel *productMaximumExpiryDateLabel;
    IBOutlet MedRepElementDescriptionLabel *productUOMLabel;

    IBOutlet XYPieChart *targetPieChart;
    IBOutlet UIImageView *targetPieChartNoDataImageView;
    IBOutlet NSLayoutConstraint *xpieChartWidthConstraint;
    IBOutlet UIView *pieChartParentView;
    IBOutlet SalesWorxImageView *statusImageView;

    NSMutableArray *slicesForTarget;
    NSArray        *sliceColorsForTarget;
    IBOutlet MedRepElementTitleLabel *targrtTypeHeaderLabel;
    IBOutlet MedRepElementDescriptionLabel *targetTypeValueLabel;
    NSString *popOverTitle;
    UIPopoverController * UOMPopOverController;

    IBOutlet MedRepButton *btnMedia;
}
@property (strong,nonatomic) Products * currentProduct;
-(void)UpdateProductDetailsOnTableViewCellSelection;
-(IBAction)MoreInfoButtonTapped:(id)sender;
@end
