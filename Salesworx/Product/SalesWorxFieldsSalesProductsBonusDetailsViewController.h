//
//  SalesWorxFieldsSalesProductsBonusDetailsViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxTableView.h"
#import "SalesWorxTableViewHeaderView.h"
@interface SalesWorxFieldsSalesProductsBonusDetailsViewController : UIViewController
{
    IBOutlet MedRepElementDescriptionLabel *productWholesalePriceLabel;
    IBOutlet MedRepElementDescriptionLabel *productRetailPriceLabel;
    IBOutlet MedRepElementDescriptionLabel *productDefaultDicountLabel;
    IBOutlet MedRepElementDescriptionLabel *productSpecialDicountLabel;
    NSMutableArray *productPriceListArray;
    NSMutableArray *productSimpleBonusDetailsArray;
    NSMutableArray *productPriceListDescriptionArray;


    IBOutlet SalesWorxTableView *SimpleBonusTableView;
    IBOutlet SalesWorxTableView *productPriceListsTableView;
    
    IBOutlet SalesWorxTableViewHeaderView *SimpleBonusTableHeader;
    IBOutlet SalesWorxTableViewHeaderView *PriceListTableHeader;
    IBOutlet UIButton *AssortmentBpnusDetailsButton;
    NSMutableArray *assortmentBonusPlansArray;
}
- (IBAction)AssortMentBonusButtonTapped:(id)sender;
@property (strong,nonatomic) Products * currentProduct;
@property (strong,nonatomic) NSMutableArray * productsArray;

-(void)UpdatebonusDetailsWithAnimation:(BOOL)Animation;
@end
