//
//  SalesWorxFieldSalesProdcutsMoreDetailsViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldSalesProdcutsMoreDetailsViewController.h"
#import "SWDefaults.h"
@interface SalesWorxFieldSalesProdcutsMoreDetailsViewController ()

@end

@implementation SalesWorxFieldSalesProdcutsMoreDetailsViewController
@synthesize currentProduct;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    productDescriptionLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:currentProduct.Description];
    productFormLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:currentProduct.FormDesc];
    productTradeNameLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:currentProduct.TradeName];
    productGenericNameLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:currentProduct.GenericName];
}

- (IBAction)DoneButtonTapped:(id)sender {
    [self dimissViewControllerWithSlideDownAnimation:YES];
}
@end
