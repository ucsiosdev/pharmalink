//
//  SalesWorxBonusTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"
#import "SWDefaults.h"
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxBonusTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *dividerImageView;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *fromLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *toLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *bonusLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *typeLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *bonusItemLbl;

@property(nonatomic) BOOL isHeader;
@end
