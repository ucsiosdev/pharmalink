//
//  SalesWorxFieldsSalesProductsStockDetailsViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "SalesWorxTableView.h"
#import "SalesWorxTableViewHeaderView.h"
@interface SalesWorxFieldsSalesProductsStockDetailsViewController : UIViewController
{
    IBOutlet SalesWorxTableView *stockLotsTableView;
    IBOutlet SalesWorxTableView *PODetailsTableView;
    
    IBOutlet SalesWorxTableView *warehouseWiseStockPositionTableView;
    IBOutlet SalesWorxTableViewHeaderView *StockLotsTableHeader;
    IBOutlet SalesWorxTableViewHeaderView *StockPositionTableHeader;
    IBOutlet SalesWorxTableViewHeaderView *StockPODetailsTableHeader;
    
    
    NSMutableArray *stockLotsArray;
    NSMutableArray *wareHouseWiseStockArray;
    NSMutableArray *poDetailsArray;
}
@property (strong,nonatomic) Products * currentProduct;
-(void)UpdateStockDetailsWithAnimation:(BOOL)Animation;
@end
