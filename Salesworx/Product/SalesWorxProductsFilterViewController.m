//
//  SalesWorxProductsFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxProductsFilterViewController.h"
#import <MedRepDefaults.h>
#import "SWDefaults.h"
#import "NSPredicate+Distinct.h"
@interface SalesWorxProductsFilterViewController ()

@end

@implementation SalesWorxProductsFilterViewController
@synthesize productsArray,previousFilterParametersDict,filterPopOverController;
- (void)viewDidLoad {
    [super viewDidLoad];
    filterParametersDict=[[NSMutableDictionary alloc]init];

    
    filteredProductsArray=[[NSMutableArray alloc]init];
  
    stockSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    bonusSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    [promoItemSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];

    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];


    // Do any additional setup after loading the view from its nib.
    
    
    
    NSLog(@"previous filter parameters are, %@", previousFilterParametersDict);
    
    if (previousFilterParametersDict.count>0) {
        filterParametersDict=previousFilterParametersDict;
    }
    if([[previousFilterParametersDict valueForKey:@"Brand_Code"] length]>0) {
        brandCodeTxtFld.text=[previousFilterParametersDict valueForKey:@"Brand_Code"];
    }
    
    if([[filterParametersDict valueForKey:@"Description"] length]>0) {
        productNameTxtFld.text=[previousFilterParametersDict valueForKey:@"Description"];
    }
    
    if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
        itemCodeTxtFld.text=[previousFilterParametersDict valueForKey:@"Item_Code"];
    }
    
    if([[filterParametersDict valueForKey:@"Stock"] length]>0) {
        if ([[filterParametersDict valueForKey:@"Stock"] isEqualToString:@"Available"]) {
            
            stockSegment.selectedSegmentIndex=0;
        }else{
            stockSegment.selectedSegmentIndex=1;
            
        }
    }
    
    if([[filterParametersDict valueForKey:@"Bonus"] length]>0) {
        if ([[filterParametersDict valueForKey:@"Bonus"] isEqualToString:@"Available"]) {
            bonusSegment.selectedSegmentIndex=0;
            
        }else{
            bonusSegment.selectedSegmentIndex=1;
        }
    }
    
    AppControl * appControl=[AppControl retrieveSingleton];
    NSString* showPromoView=appControl.ENABLE_PROMO_ITEM_VALIDITY;
    //showPromoView=@"N";
    if ([NSString isEmpty:showPromoView]==NO) {
        
        if ([showPromoView isEqualToString:KAppControlsYESCode]) {

            NSString* promoItem=[filterParametersDict valueForKey:@"Promo_Item"];
            if ([NSString isEmpty:promoItem]==NO) {
                
                if ([promoItem isEqualToString:kAvailableCode]) {
                    
                    [promoItemSegment setSelectedSegmentIndex:0];
                }
                else if ([promoItem isEqualToString:kUnAvailableCode])
                {
                    [promoItemSegment setSelectedSegmentIndex:1];
                }
                else
                {

                }
                
            }
            else
            {
                if ([showPromoView isEqualToString:KAppControlsYESCode]) {
                    promoviewHeightConstraint.constant=65;
                    promoView.hidden=NO;
                    [promoItemSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
                    
                }
                else
                {
                    promoviewHeightConstraint.constant=0.0f;
                    promoView.hidden=YES;
                    
                }
            }
            
        }
        else
        {
            promoviewHeightConstraint.constant=0.0f;
            promoView.hidden=YES;

        }
    }
    else
    {
        promoviewHeightConstraint.constant=0.0f;
        promoView.hidden=YES;
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{

    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesProductsFilter];
    }
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    [clearFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    
    AppControl * appControl=[AppControl retrieveSingleton];
    NSString* showPromoView=appControl.ENABLE_PROMO_ITEM_VALIDITY;
    //showPromoView=@"N";
    if ([NSString isEmpty:showPromoView]==NO) {
        
       
        
    }
    
    
}


-(void)closeFilter
{
    if ([self.delegate respondsToSelector:@selector(productsFilterDidClose)]) {
        [self.delegate productsFilterDidClose];
        //[self.delegate filteredProductParameters:filterParametersDict];
    }
    [filterPopOverController dismissPopoverAnimated:YES];
}

-(void)clearFilter
{
    productNameTxtFld.text=@"";
    brandCodeTxtFld.text=@"";
    itemCodeTxtFld.text=@"";
    [promoItemSwitch setOn:NO];

    stockSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    bonusSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    promoItemSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    filterParametersDict=[[NSMutableDictionary alloc]init];
}


#pragma UItextField Methods


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==productNameTxtFld)
    {
        selectedTextField=@"Product Name";
        selectedPredicateString=@"Description";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    else if (textField==brandCodeTxtFld)
    {
        selectedTextField=@"Brand Code";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"Brand_Code"];
        selectedPredicateString=@"Brand_Code";
        return NO;
    }
    else if (textField==itemCodeTxtFld)
    {
        selectedTextField=@"Item Code";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:@"Item_Code"];
        selectedPredicateString=@"Item_Code";
        return NO;
    }
    return YES;
}

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedTextField;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    if(tappedTextField==productNameTxtFld){
        filterDescVC.filterType=@"Product-Name";
    }
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];
    
    //get distinct keys
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray=[[productsArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:predicateString];
    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Selected Filter Data

-(void)selectedFilterValue:(NSString*)selectedString
{
    
    if ([selectedTextField isEqualToString:@"Product Name"]) {
        productNameTxtFld.text= selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"Brand Code"])
    {
        brandCodeTxtFld.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"Item Code"])
    {
        itemCodeTxtFld.text=selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
}



- (IBAction)searchButtonTapped:(id)sender {
    NSLog(@"filter parameters for products filter %@", filterParametersDict);
    filteredProductsArray=[[NSMutableArray alloc]init];
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filter parameters dict is %@",filterParametersDict);
    
    
    if([[filterParametersDict valueForKey:@"Brand_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[filterParametersDict valueForKey:@"Brand_Code"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Description"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",[filterParametersDict valueForKey:@"Description"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[filterParametersDict valueForKey:@"Item_Code"]]];
    }
    NSString* promoItemStatus=[filterParametersDict valueForKey:@"Promo_Item"];
    
    if ([NSString isEmpty:promoItemStatus]==NO) {
        
        if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsYESCode]];
        }
        
       else if ([[SWDefaults getValidStringValue:promoItemStatus] isEqualToString:kUnAvailableCode]) {
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Promo_Item == [cd] %@ ",KAppControlsNOCode]];
        }
        else
        {
            
        }
        
    }
    
    
    if([[filterParametersDict valueForKey:@"Stock"] length]>0) {
        
        if ([[filterParametersDict valueForKey:@"Stock"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.stock.intValue == 0"]];
        }
    }
    if([[filterParametersDict valueForKey:@"Bonus"] length]>0) {
        
        if ([[filterParametersDict valueForKey:@"Bonus"] isEqualToString:@"Available"]) {
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue > 0"]];
        }
        else{
            [predicateArray addObject: [NSPredicate predicateWithFormat:@"SELF.bonus.intValue == 0"]];
        }
    }
    
    
    
   
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredProductsArray=[[productsArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    if (predicateArray.count==0) {
        
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:@"Please select filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
         [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select your filter criteria and try again" withController:self];
    }
    
    else  if (filteredProductsArray.count>0) {
        NSLog(@"filtered products count %lu",(unsigned long)filteredProductsArray.count);

        if ([self.delegate respondsToSelector:@selector(filteredProducts:)]) {
            [self.delegate filteredProducts:filteredProductsArray];
            [self.delegate filteredProductParameters:filterParametersDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];

        
    }
    
    

}

- (IBAction)promoItemSegmentTapped:(UISegmentedControl*)sender {
    
    [filterParametersDict setValue:promoItemSegment.selectedSegmentIndex==0?kAvailableCode:kUnAvailableCode forKey:@"Promo_Item"];
}
- (IBAction)stockSegmentTapped:(UISegmentedControl *)sender {

    [filterParametersDict setValue:stockSegment.selectedSegmentIndex==0?kAvailableCode:kUnAvailableCode forKey:@"Stock"];
}



- (IBAction)bonusSegmentTapped:(UISegmentedControl *)sender {
     [filterParametersDict setValue:bonusSegment.selectedSegmentIndex==0?kAvailableCode:kUnAvailableCode forKey:@"Bonus"];
}


- (IBAction)resetButtonTapped:(id)sender {
    
    previousFilterParametersDict=[[NSMutableDictionary alloc]init];
    
    if ([self.delegate respondsToSelector:@selector(productsFilterdidReset)]) {
        [self.delegate productsFilterdidReset];
        [self.filterPopOverController dismissPopoverAnimated:YES];
    }
}

- (IBAction)promoItemSwitchTapped:(UISwitch *)sender {
    
    if (sender.isOn) {
        [filterParametersDict setValue:@"Y" forKey:@"Promo_Item"];
    }
    else
    {
        [filterParametersDict setValue:@"N" forKey:@"Promo_Item"];
    }
}
@end
