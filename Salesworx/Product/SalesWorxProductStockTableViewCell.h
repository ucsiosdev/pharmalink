//
//  SalesWorxProductStockTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepElementDescriptionLabel.h"

#import <SalesWorxSingleLineLabel.h>
#import "SWDefaults.h"
@interface SalesWorxProductStockTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *dividerImageView;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *lotNumberLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *quantityLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *expiryLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *warehouseLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *onOrderQtyLbl;

@property(nonatomic) BOOL isHeader;

@end
