//
//  SalesWorxProductsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxProductsViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepUpdatedDesignCell.h"
#import "SalesWorxProductStockTableViewCell.h"
#import "SalesWorxBonusTableViewCell.h"
#import <NSObject+NullDictionary.h>
#import "SalesWorxProductStockUpdatedTableViewCell.h"
#import "MedRepProductImagesViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SalesWorxAssortmentBonusManager.h"
#import "SalesWorxAssortmentBonusInfoViewController.h"
@interface SalesWorxProductsViewController ()

@end



@implementation SalesWorxProductsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    proSearchType=ProductSerachNormal;
    AssortmentPlansArray=[[NSMutableArray alloc]init];

//    NSMutableArray* crashArray=[[NSMutableArray alloc]init];
//    
//    NSString* crashStr=[crashArray objectAtIndex:0];
    
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Products", nil)];

    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_NavIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
        } else {
        }
    }
    
//    NSLog(@"check products %@",[[SWDatabaseManager retrieveManager]fetchProductsArray]);
    
    indexPathArray=[[NSMutableArray alloc]init];
    filteredProductsArray=[[NSMutableArray alloc]init];
    unfilteredProductsArray=[[NSMutableArray alloc]init];
    productPriceListArray=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)didbecomeActive
{
    NSLog(@"did become active called in salesworx products");
    [self refreshProductsData];
}
-(void)refreshProductsData
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        productsArray=[[SWDatabaseManager retrieveManager]fetchProductsArray];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            
            [blurredBgImage removeFromSuperview];
            
            previousFilteredParameters=[[NSMutableDictionary alloc]init];
            [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
            
            if ([filterPopOverController isPopoverVisible]) {
                
                [filterPopOverController dismissPopoverAnimated:YES];
            }
            
            if (productsArray.count>0) {
                [productsTableView reloadData];
                [self deselectPreviousCellandSelectFirstCell];
                
                NSPredicate *testPromotion=[NSPredicate predicateWithFormat:@"SELF.Promo_Item == 'Y' "];
                NSArray* testarray=[productsArray filteredArrayUsingPredicate:testPromotion];
                NSLog(@"promo products are %@", testarray);
                
                
                
                unfilteredProductsArray=productsArray;
            }
            
        });
    });

}


-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"salesworx products view will appear called");
    

    [super viewWillAppear:YES];

    
    productsTableView.delegate=self;
    productsTableView.rowHeight = UITableViewAutomaticDimension;
    productsTableView.estimatedRowHeight = 70.0;

    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesProductsScreenName];
        
        
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(didbecomeActive)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    

    
    for (UIView *borderView in self.view.subviews) {
        
        if ([borderView isKindOfClass:[UIView class]]) {
            if (borderView.tag==101|| borderView.tag==102|| borderView.tag==103|| borderView.tag==104 || borderView.tag==105 || borderView.tag==110  ) {
                borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                borderView.layer.shadowOffset = CGSizeMake(2, 2);
                borderView.layer.shadowOpacity = 0.1;
                borderView.layer.shadowRadius = 1.0;
                
            }
        }
        
           }
    
    
    
//    stockTableView.layer.borderColor = UITextViewBorderColor.CGColor;
//    stockTableView.layer.borderWidth = 2.0;
//    stockTableView.layer.cornerRadius = 1.0;
//    
//    bonusTableView.layer.borderColor = UITextViewBorderColor.CGColor;
//    bonusTableView.layer.borderWidth = 2.0;
//    bonusTableView.layer.cornerRadius = 1.0;
    
    AppControl *appControl = [AppControl retrieveSingleton];
    showTarget = appControl.SHOW_PRODUCT_TARGET;

    
    
    if (self.isMovingToParentViewController) {
        
    //showTarget =@"Y";
    if ([showTarget isEqualToString:@"Y"]) {
//        stockTableView.frame=CGRectMake(8, 41, 435, 151);
//        stockView.frame=CGRectMake(266, 292, 451, 199);
        //targetView.hidden=NO;
        
        targetViewWidthConstraint.constant=291.0;
        targetViewTrailingConstraint.constant=8.0;
        imageViewHeightConstraint.constant=276.0;


    }
    else
    {
//        stockView.frame=CGRectMake(266, 292, 748, 197);
//        stockTableView.frame=CGRectMake(8, 41, 732, 148);
//        
        targetViewWidthConstraint.constant=0.0;
        targetViewTrailingConstraint.constant=0.0;
       // targetViewHeightConstraint.constant=targetViewHeightConstraint.constant-15.0;
       // imageViewBottomConstraint.constant=8.0f;
        

        imageViewHeightConstraint.constant=imageViewHeightConstraint.constant+3;
        
       // targetView.hidden=YES;
        
    }
    }
    NSLog(@"stockView frame is %@", NSStringFromCGRect(stockView.frame));
    NSLog(@"table frame is %@", NSStringFromCGRect(stockTableView.frame));

    
    if(self.isMovingToParentViewController)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            productsArray=[[SWDatabaseManager retrieveManager]fetchProductsArray];
            
            if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode]){
                SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
                AssortmentPlansArray = [bnsManager FetchAssortmentBonusPlansAvailableForProducts:productsArray];
            }

            
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                if (productsArray.count>0) {
                    [productsTableView reloadData];
                    [self deselectPreviousCellandSelectFirstCell];
                    
                    NSPredicate *testPromotion=[NSPredicate predicateWithFormat:@"SELF.Promo_Item == 'Y' "];
                    NSArray* testarray=[productsArray filteredArrayUsingPredicate:testPromotion];
                    NSLog(@"promo products are %@", testarray);
                    
                    
                    
                    unfilteredProductsArray=productsArray;
                }
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            });
        });
    
        
        slicesForTarget=[[NSMutableArray alloc]init];
        sliceColorsForTarget=[NSArray arrayWithObjects:
                              [UIColor colorWithRed:53.0/255.0 green:194.0/255.0 blue:195.0/255.0 alpha:1],
                              [UIColor colorWithRed:16.0/255.0 green:49.0/255.0 blue:57.0/255.0 alpha:1],
                              nil];
        
        targetPieChart.labelRadius = 30;
        [targetPieChart setDelegate:self];
        [targetPieChart setDataSource:self];
        targetPieChart.labelFont =[UIFont boldSystemFontOfSize:14];
        [targetPieChart setPieCenter:CGPointMake(50, 60)];
        [targetPieChart setShowPercentage:YES];
        [targetPieChart setLabelColor:[UIColor whiteColor]];
        [targetPieChart setUserInteractionEnabled:NO];
        [targetPieChart reloadData];
        
        [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    }
    
    UITapGestureRecognizer * imageGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped)];
    
    [productImageView addGestureRecognizer:imageGesture];

    if ([[SWDefaults checkMultiUOM]isEqualToString:@"N"]) {
        
        uomTextField.hidden=YES;
        uomLbl.hidden=NO;
    }
    else
    {
        uomTextField.hidden=NO;
        uomLbl.hidden=YES;
    }
    
    
    NSLog(@"check on order qty %@",appControl.SHOW_ON_ORDER_QTY);
    
    if ([NSString isEmpty:appControl.SHOW_ON_ORDER_QTY]==NO) {
        
        if ([appControl.SHOW_ON_ORDER_QTY isEqualToString:@"Y"]) {
            
        }
        else
        {
            onOrderQuantityLabel.hidden=YES;
            onOrderQtyTitleLbl.hidden=YES;
        }
    }
    
    if(self.isMovingToParentViewController){
        if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode]){
            xSearchBarTrailingspaceToFilterButton.constant=47;
            [barcodeScanButton setHidden:NO];
        }
        else{
            xSearchBarTrailingspaceToFilterButton.constant=8;
            [barcodeScanButton setHidden:YES];
        }
    }

    [self hideNoSelectionView];
}

-(void)imageTapped
{
    NSLog(@"image gesture tapped");
    
    
    /*
     
     {
     Caption = "Cg 210";
     "Detailed_Info" = "For men and women who wish to reduce hair shedding, increase hair thickness, improve healthy hair and promote new healthy hair.
     \n
     \nFeatures:
     \n- Clinically proven patented botanical extracts
     \n- Hypoallergenic and paraben free
     \n- Easy to use
     \n
     \nAvailable in 2 fragrances:
     \n- Mint (Men)
     \n- Berry (Women)";
     "File_Name" = "21c003d9-cd61-44a7-9ff2-7856c14d632a_M_CG210-1.jpg";
     "Media_Type" = Image;
     "Product_Code" = "RGH-0002";
     "Product_ID" = "9ED63954-9906-4AD1-9BE1-83822B1C1EA7";
     "Product_Name" = "Cg 210-Hair Lotion 80ml  - Bottle";
     },
     
     */
    if (selectedProduct.productImagesArray.count>0) {
        
        MedRepProductImagesViewController * imageVC=[[MedRepProductImagesViewController alloc]init];
        NSMutableArray* productDataArray=[[NSMutableArray alloc]init];

        
        for (NSInteger i=0; i<selectedProduct.productImagesArray.count; i++) {
            
        ProductMediaFile* mediaFile= [selectedProduct.productImagesArray objectAtIndex:i];
        
        NSMutableDictionary* mediaFileDict=[[NSMutableDictionary alloc]init];
        [mediaFileDict setValue:mediaFile.Filename forKey:@"File_Name"];
        [mediaFileDict setValue:mediaFile.Media_Type forKey:@"Media_Type"];
        [mediaFileDict setValue:mediaFile.Caption forKey:@"Caption"];
        [mediaFileDict setValue:selectedProduct.Item_Code forKey:@"Product_Code"];
        [mediaFileDict setValue:selectedProduct.Description forKey:@"Product_Name"];
        [mediaFileDict setValue:@"N/A" forKey:@"Detailed_Info"];
        
        [productDataArray addObject:mediaFileDict];
        
        }
        imageVC.productDataArray=productDataArray;
        
        //fetch default image index to display the same image in description
        NSLog(@"product data array is %@",selectedProduct.productImagesArray);

        NSLog(@"media file id is %@", selectedMediaFileID);
        __block NSUInteger index = NSUIntegerMax;
        
        [selectedProduct.productImagesArray enumerateObjectsUsingBlock: ^ (ProductMediaFile* product, NSUInteger idx, BOOL* stop) {
            if([product.Media_File_ID isEqualToString:selectedMediaFileID])
            {
                index = idx;
                *stop = YES;
            }
        }];
        
        NSLog(@"index is %d", index);
        imageVC.selectedIndexPath=[NSIndexPath indexPathForRow:index inSection:0];
        
        [self presentViewController:imageVC animated:YES completion:nil];
        
    }
    else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:kNoData andMessage:@"No product images available" withController:self];
        }
}

-(void)viewDidAppear:(BOOL)animated
{
    
   // [self addKeyBoardObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Methods


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==productsTableView) {
        
        if (isSearching==YES && filteredProductsArray.count>0) {
            return filteredProductsArray.count;
        }
        else if (isSearching==NO && productsArray.count>0)
        {
            return productsArray.count;
        }
        else
        {
            return 0;
        }
    }
    else if (tableView==stockTableView)
    {
        if (selectedProduct.productStockArray.count>0) {
            return selectedProduct.productStockArray.count;
        }
        else
        {
            return 0;
        }
    }
    
    else if (tableView==bonusTableView)
    {
        if (selectedProduct.productBonusArray.count>0) {
            return selectedProduct.productBonusArray.count;
        }
        else
        {
            return 0;
        }
    }
    
    else
    {
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==stockTableView||tableView==bonusTableView) {
        return 44.0f;
    }
    else
    {
        return UITableViewAutomaticDimension;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==stockTableView||tableView==bonusTableView) {
        
        return 47.0f;
    }
    else
    {
        return 0;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==stockTableView) {
        
        if ([showTarget isEqualToString:@"Y"]) {
            static NSString* identifier=@"updatedStockCell";
            SalesWorxProductStockUpdatedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            //if(cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxProductStockUpdatedTableViewCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            //}
            [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];

            cell.isHeader=YES;
            
            cell.lotNumberLbl.text=@"Lot No";
            cell.quantityLbl.text=@"Qty";
           // cell.onOrderQtyLbl.text=@"On Order";
            cell.expiryLbl.text=@"Expiry";
            cell.warehouseLbl.text=@"W/H";
            return cell;
        }
        else
        {
        static NSString* identifier=@"stockCell";
        SalesWorxProductStockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        //if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxProductStockTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
       // }
            cell.isHeader=YES;
            
            [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
       
        cell.lotNumberLbl.text=@"Lot No";
        cell.quantityLbl.text=@"Quantity";
        cell.onOrderQtyLbl.text=@"On Order";
        cell.expiryLbl.text=@"Expiry";
        cell.warehouseLbl.text=@"W/H";
        return cell;
        }
    }
    
    else if (tableView==bonusTableView)
    {
        static NSString* identifier=@"bonusCell";
        SalesWorxBonusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        //if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBonusTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
       // }
        [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
       
        cell.isHeader=YES;
        
        cell.fromLbl.text=@"From";
        cell.toLbl.text=@"To";
        cell.bonusLbl.text=@"Bonus";
        cell.typeLbl.text=@"Type";
        cell.bonusItemLbl.text=@"Bonus Item";
        return cell;
    }
    else{
        
        return nil;
    }
    
   }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==productsTableView) {
   
    Products * currentProduct;
    
    if (isSearching==YES) {
         currentProduct=[filteredProductsArray objectAtIndex:indexPath.row];
    }
    else{
        currentProduct=[productsArray objectAtIndex:indexPath.row];

    }
    
    static NSString* identifier=@"updatedDesignCell";
    MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
        if (isSearching && !NoSelectionView.hidden) {
            [cell setDeSelectedCellStatus];
        } else {
            if ([indexPathArray containsObject:indexPath]) {
                [cell setSelectedCellStatus];
            }
            else
            {
                [cell setDeSelectedCellStatus];
            }
        }
    
    cell.titleLbl.text=[[NSString stringWithFormat:@"%@",currentProduct.Description] capitalizedString];
    cell.descLbl.text=[NSString stringWithFormat:@"%@",currentProduct.Item_Code];
    
    return cell;
    
    }
   else if (tableView==stockTableView) {
       
       Stock * selectedProductStock=[selectedProduct.productStockArray objectAtIndex:indexPath.row];
       NSLog(@"selected stock data %@", selectedProductStock.Lot_No);
       NSLog(@"conversion data for selected product %@", selectedProduct.conversionValue);

       if ([showTarget isEqualToString:@"Y"]) {
           static NSString* identifier=@"updatedStockCell";
           SalesWorxProductStockUpdatedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
           if(cell == nil) {
               cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxProductStockUpdatedTableViewCell" owner:nil options:nil] firstObject];
               cell.selectionStyle=UITableViewCellSelectionStyleNone;
           }
           cell.lotNumberLbl.text=selectedProductStock.Lot_No;
           
           
         

//           if (selectedProduct.conversionValue>0) {
//               
//               
//               NSInteger updatedStockwithUOM=[selectedProductStock.Lot_Qty integerValue]/selectedProduct.conversionValue;
//               cell.quantityLbl.text=[NSString stringWithFormat:@"%ld",(long)updatedStockwithUOM];
//               
//               refinedStock=refinedStock+updatedStockwithUOM;
//               
//               //this is rounded value 
//               stockLbl.text=[NSString stringWithFormat:@"%ld", (long)refinedStock];
//
//               NSInteger updatedOnOrderQuantitywithUOM=[selectedProductStock.On_Order_Qty integerValue]/selectedProduct.conversionValue;
//               
//               
//               cell.onOrderQtyLbl.text=[NSString stringWithFormat:@"%ld",(long)updatedOnOrderQuantitywithUOM];
//           }
//           
//           else{
//               cell.quantityLbl.text=[NSString stringWithFormat:@"%@ %@",selectedProductStock.Lot_Qty,selectedProduct.primaryUOM];
//               cell.onOrderQtyLbl.text=[NSString stringWithFormat:@"%@ %@",selectedProductStock.On_Order_Qty,selectedProduct.primaryUOM];
//           }
           
           
           cell.quantityLbl.text=[NSString stringWithFormat:@"%@ %@",selectedProductStock.Lot_Qty,selectedProduct.primaryUOM];
           cell.onOrderQtyLbl.text=[NSString stringWithFormat:@"%@ %@",selectedProductStock.On_Order_Qty,selectedProduct.primaryUOM];
          
           cell.expiryLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:selectedProductStock.Expiry_Date];
           cell.warehouseLbl.text=selectedProductStock.Org_ID;
           return cell;

       }
       else{
        static NSString* identifier=@"stockCell";
        SalesWorxProductStockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxProductStockTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
           cell.lotNumberLbl.text=selectedProductStock.Lot_No;
           
           cell.quantityLbl.text=[NSString stringWithFormat:@"%@ %@",selectedProductStock.Lot_Qty,selectedProduct.primaryUOM];
           cell.onOrderQtyLbl.text=[NSString stringWithFormat:@"%@ %@",selectedProductStock.On_Order_Qty,selectedProduct.primaryUOM];
           
           cell.expiryLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:selectedProductStock.Expiry_Date];
           cell.warehouseLbl.text=selectedProductStock.warehouse;

           return cell;

       }
    }
    
   else if (tableView==bonusTableView)
   {
       static NSString* identifier=@"bonusCell";
       SalesWorxBonusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
       if(cell == nil) {
           cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxBonusTableViewCell" owner:nil options:nil] firstObject];
           cell.selectionStyle=UITableViewCellSelectionStyleNone;
       }
       
       BonusItem * bonusItem=[[BonusItem alloc]init];
       bonusItem=[selectedProduct.productBonusArray objectAtIndex:indexPath.row];
       cell.fromLbl.text=[NSString stringWithFormat:@"%@ %@",bonusItem.Prom_Qty_From,selectedProduct.primaryUOM];
       cell.toLbl.text=[NSString stringWithFormat:@"%@ %@",bonusItem.Prom_Qty_To,selectedProduct.primaryUOM];
       cell.bonusLbl.text=bonusItem.Get_Qty;
       
       cell.typeLbl.text=bonusItem.Price_Break_Type_Code;
       cell.bonusItemLbl.text=bonusItem.Description;
       return cell;
   }
   else
    {
        return nil;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (tableView==productsTableView) {
    
        [self hideNoSelectionView];
            
        selectedProduct=[[Products alloc]init];
        if (isSearching==YES) {
            selectedProduct=[filteredProductsArray objectAtIndex:indexPath.row];
     
        }
        else
        {
        selectedProduct=[productsArray objectAtIndex:indexPath.row];
        }
        if (indexPathArray.count==0) {
            [indexPathArray addObject:indexPath];
        }
        else
        {
            indexPathArray=[[NSMutableArray alloc]init];
            [indexPathArray addObject:indexPath];
            //[indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
        }
            selectedIndexPath=indexPath;
            selectedUOM=selectedProduct.selectedUOM;
            uomTextField.text=selectedUOM;
            uomLbl.text=selectedUOM;
            //display rounded value to avoid confusion
            refinedStock =0;
            refinedOnOrderStock=0;
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        cell.titleLbl.textColor=[UIColor whiteColor];
        [cell.descLbl setTextColor:[UIColor whiteColor]];
        cell.cellDividerImg.hidden=YES;
        cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        [self populateSelectedProductDetails:selectedProduct];
        [self fetchTargetDataforProduct:selectedProduct];
        [self fetchProductPriceListData:selectedProduct];
        [self fetchBonusDataforProduct:selectedProduct];
        [self fetchImagesforProduct:selectedProduct];
        
        SalesWorxAssortmentBonusManager *bnsManager=[[SalesWorxAssortmentBonusManager alloc]init];
        if(AssortmentPlansArray.count>0 && [bnsManager IsAnyAssortmentBonusAvailableForTheProduct:selectedProduct AssortmentPlans:AssortmentPlansArray]){
            [assortmentBnsButton setHidden:NO];
        }else {
            [assortmentBnsButton setHidden:YES];
        }
        
        [productsTableView reloadData];
    }
    else{

    }
}

#pragma mark selected Product data

-(void)populateSelectedProductDetails:(Products*)selectedProd
{
    [self fetchStockDataforItemID:selectedProduct];
    productNameLbl.text=selectedProduct.Description;
    brandLbl.text=selectedProduct.Brand_Code;
    agencyLbl.text=selectedProduct.Agency;
    mslLbl.text=selectedProduct.IsMSL;
    //check if special discount applicable
    
    NSString* allowSpecialDiscount=[[[SWDefaults appControl] objectAtIndex:0] valueForKey:@"ALLOW_SPECIAL_DISCOUNT"];
    if ([allowSpecialDiscount isEqualToString:@"Y"]) {
        
        specialDiscountContentLbl.hidden=NO;
        specialDiscountLbl.hidden=NO;
        specialDiscountContentLbl.textColor=kDarkTitleColor;
        NSString* specialDiscount=[[SWDatabaseManager retrieveManager]dbGetSpecialDiscount:selectedProduct.Inventory_Item_ID];
        NSLog(@"special discount for selected product is %@", specialDiscount);
        if (specialDiscount.length>0) {
            specialDiscountContentLbl.text=[[NSString stringWithFormat:@"%0.2f", [specialDiscount doubleValue]] stringByAppendingString:@"%"];
        }
        else
        {
            specialDiscountContentLbl.text=[@"0.00" stringByAppendingString:@"%"];
        }
    }
    else
    {
        specialDiscountContentLbl.hidden=YES;
        specialDiscountLbl.hidden=YES;
    }
    discountLbl.textColor=kDarkTitleColor;
    discountLbl.text=[selectedProduct.Discount stringByAppendingString:@"%"];
    NSString* onOrderQty=[[SWDatabaseManager retrieveManager]dbGetOnOrderQuantityOfProduct:selectedProduct.Inventory_Item_ID];
    NSLog(@"on order qty is %@", onOrderQty);
    onOrderQtyLbl.text=[NSString stringWithFormat:@"%@ %@",onOrderQty,selectedProduct.primaryUOM];
    
    //on order quanity to be based on flag
    AppControl * appControl=[AppControl retrieveSingleton];
    
    NSLog(@"check on order qty %@",appControl.SHOW_ON_ORDER_QTY);
    
    if ([NSString isEmpty:appControl.SHOW_ON_ORDER_QTY]==NO) {
        
        if ([appControl.SHOW_ON_ORDER_QTY isEqualToString:@"Y"]) {
            
            if ([NSString isEmpty:[SWDefaults getValidStringValue:onOrderQty]]==YES) {
                onOrderQuantityLabel.text= @"N/A";

                
            }
            else
            {
            
            onOrderQuantityLabel.text= [SWDefaults getValidStringValue:onOrderQty];
            }
            
        }
        
        else
        {
            
        }
    }
    
    
}


#pragma mark Database Methods

-(void)fetchStockDataforItemID:(Products*)selectedProd
{
    NSInteger productStockValue=0;
    
    selectedProduct.productStockArray=[[NSMutableArray alloc]init];

    NSMutableArray * tempProductStockArray=[[SWDatabaseManager retrieveManager]fetchStockwithOnOrderQty:selectedProduct WareHouseId:KNotApplicable];
    
//    NSMutableArray * tempProductStockArray=[[[SWDatabaseManager retrieveManager]dbGetStockInfo:selectedProduct.ItemID] mutableCopy];
   // NSLog(@"stock array in fetchStockDataforItemID %@", tempProductStockArray);

    if (tempProductStockArray.count>0) {
        
        for (NSMutableDictionary * currentStockDict in tempProductStockArray) {
            
            Stock * productStock=[[Stock alloc]init];

            NSMutableDictionary * refinedDict=[NSMutableDictionary nullFreeDictionaryWithDictionary:currentStockDict];
           // NSLog(@"refined stock dict is %@", refinedDict);
            productStock.Lot_No=[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"Lot_No"]];
            productStock.Lot_Qty=[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"Lot_Qty"]];
            productStock.Org_ID=[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"Org_ID"]];
            productStock.Stock_ID=[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"Stock_ID"]];
            productStock.Expiry_Date=[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"Expiry_Date"]];
            productStock.On_Order_Qty=[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"On_Order_Qty"]];
            
            productStock.warehouse=[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"Description"]];
            
            productStockValue= productStockValue+[productStock.Lot_Qty integerValue];
            
            [selectedProduct.productStockArray addObject:productStock];
        }
        
        //check if UOM has been changed
        if (selectedUOM.length>0) {
            //get conversion based on selected UOM
            
        }
        
        selectedProduct.Lot_Qty=[NSString stringWithFormat:@"%d", productStockValue];
        
        stockLbl.text=[NSString stringWithFormat:@"%d", productStockValue];
        NSLog(@"selected product stock is %d",productStockValue);
    }
    
    
   
    
    if (selectedProduct.productStockArray.count>0) {
        
        [stockTableView reloadData];
       // [stockStatusBtn setBackgroundColor:[UIColor greenColor]];
        
        statusImageView.backgroundColor=[UIColor greenColor];

    }
    else if (selectedProduct.productStockArray.count==0)
    {
        stockLbl.text=@"N/A";
        //[stockStatusBtn setBackgroundColor:[UIColor redColor]];
        statusImageView.backgroundColor=[UIColor redColor];

        [stockTableView reloadData];

    }
    else{
        //[stockStatusBtn setBackgroundColor:[UIColor whiteColor]];
        statusImageView.backgroundColor=[UIColor whiteColor];


    }
    
    
}


-(void)fetchProductPriceListData:(Products*)selectedProd
{
    productPriceListArray=[[NSMutableArray alloc]init];
    wholesalePriceLbl.text=KNotApplicable;
    retailPriceLbl.text=KNotApplicable;
    productPriceListArray=[[SWDatabaseManager retrieveManager]fetchProductGenericPriceList:selectedProd];
    if (productPriceListArray.count>0) {
        
        NSLog(@"product price list response is %@", productPriceListArray);
        
        selectedProduct.productPriceListArray=[[NSMutableArray alloc]initWithArray:productPriceListArray];
        
        //now refine using a predicate and display the price based on selected uom
        NSPredicate * uomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM ==[cd] %@",selectedUOM];
        
        NSMutableArray* filteredPredicateArray=[[selectedProduct.productPriceListArray filteredArrayUsingPredicate:uomPredicate] mutableCopy];
        NSLog(@"filtered UOM data is %@", filteredPredicateArray);
        if (filteredPredicateArray.count>0) {
            PriceList* priceList=[filteredPredicateArray objectAtIndex:0];
            wholesalePriceLbl.text=[priceList.Unit_Selling_Price currencyString];
            retailPriceLbl.text=[priceList.Unit_List_Price currencyString];
        }
    }
    
}
-(void)fetchTargetDataforProduct:(Products*)selectedProd
{
    
    [slicesForTarget removeAllObjects];
    [targetPieChart reloadData];
    NSMutableArray * targetArray=[[NSMutableArray alloc] init];
    
    AppControl * appControl=[AppControl retrieveSingleton];

    if ([appControl.FSR_TARGET_TYPE isEqualToString:@"B"] ) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:selectedProduct.Brand_Code] mutableCopy];
    } else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"C"]) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:selectedProduct.Category] mutableCopy];
    } else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"N"]) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:selectedProduct.Agency] mutableCopy];
    }else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"P"]) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:selectedProduct.Item_Code] mutableCopy];
    }
    
    NSLog(@"target array count for product %d", targetArray.count);
    if (targetArray.count>0) {

       // NSLog(@"product target data %@", targetArray);
    double balanceToGo =[[targetArray valueForKey:@"Balance_To_Go"] doubleValue];
    double Sales_Value =[[targetArray valueForKey:@"Sales_Value"] doubleValue];
    [slicesForTarget removeAllObjects];
    [slicesForTarget addObject:[NSNumber numberWithDouble:Sales_Value]];
    [slicesForTarget addObject:[NSNumber numberWithDouble:balanceToGo]];
    [targetPieChart reloadData];
    
    }
   
    
}

-(void)fetchBonusDataforProduct:(Products*)selectedProd
{
    
    NSMutableArray* bonusArray=[[[SWDatabaseManager retrieveManager] fetchBonusforSelectedProduct:selectedProd] mutableCopy];
   // NSLog(@"bonus array count is %@", bonusArray);
    [SWDefaults writeLogs:[bonusArray description]];
    selectedProduct.productBonusArray=[[NSMutableArray alloc]initWithArray:bonusArray];
    if (selectedProduct.productBonusArray.count>0) {
        [bonusTableView reloadData];
    }
    else if (selectedProduct.productBonusArray.count==0)
    {
        [bonusTableView reloadData];
    }
   
    
}

-(void)fetchImagesforProduct:(Products*)selectedProd
{
    NSMutableArray* selectedImagesArray=[[SWDatabaseManager retrieveManager]fetchProductImagesforSelectedProduct:selectedProd];
    NSLog(@"selected imagesArray is %@",selectedImagesArray);
    ProductMediaFile * mediaFile=[[ProductMediaFile alloc]init];
    
    if (selectedImagesArray.count>0) {
        selectedProduct.productImagesArray=[[NSMutableArray alloc]initWithArray:selectedImagesArray];
        NSLog(@"selected product images %@",selectedProduct.productImagesArray);
        //display default image, conditon is Custom_Attribute_2 is Y for default image
        
        NSPredicate * defaultImagePredicate=[NSPredicate predicateWithFormat:@"SELF.Custom_Attribute_2 == 'Y'"];
        NSArray* filteredArray=[selectedProduct.productImagesArray filteredArrayUsingPredicate:defaultImagePredicate];
        if (filteredArray.count>0) {
             mediaFile=[filteredArray objectAtIndex:0];
        }
        else
        {
            mediaFile=[selectedProduct.productImagesArray objectAtIndex:0];
        }
        NSLog(@"default image id is %@", mediaFile.Media_File_ID);
        selectedMediaFileID=mediaFile.Media_File_ID;
        
        productImageView.image=[UIImage imageNamed:mediaFile.File_Path];
    }
    else{
        productImageView.image=[UIImage imageNamed:@"ProductsDefaultImage"];
    }
}

#pragma mark UISearch Methods
-(void)closeSplitView
{
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0)
    {
        [revealController revealToggle:self];
    }
    revealController=nil;
}

-(void)deselectPreviousCellandSelectFirstCell
{
    if ([productsTableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        
        
        MedRepUpdatedDesignCell *cell = (MedRepUpdatedDesignCell *)[productsTableView cellForRowAtIndexPath:selectedIndexPath];
        
        if (cell) {
            cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
            cell.titleLbl.textColor=MedRepMenuTitleFontColor;
            cell.descLbl.textColor=MedRepMenuTitleDescFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            cell.cellDividerImg.hidden=NO;
            
            NSLog(@"PREVIOUS CELL BEING DESELECTED");
        }

        [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                     animated:YES
                               scrollPosition:UITableViewScrollPositionNone];
        
        [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self closeSplitView];
    [productsSearchBar setShowsCancelButton:YES animated:YES];
    if ([searchBar isFirstResponder]) {
    }
    else
    {
        [searchBar becomeFirstResponder];
    }
    AppControl * appControl=[AppControl retrieveSingleton];

    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode])
    {
        xSearchBarTrailingspaceToFilterButton.constant=16;
        [barcodeScanButton setHidden:YES];
    }
    

}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if([searchText length] != 0) {
        
        [self showNoSelectionViewWithMessage];

        isSearching = YES;
        [self searchContent];
    }
    else {
        [self hideNoSelectionView];
        
        isSearching = NO;
        if (productsArray.count>0) {
            [productsTableView reloadData];
            [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                            animated:YES
                                      scrollPosition:UITableViewScrollPositionNone];
            NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:firstIndexPath];
 
        }
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [productsSearchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
    [self hideNoSelectionView];
    
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (productsArray.count>0) {
        [productsTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
    }
    AppControl * appControl=[AppControl retrieveSingleton];

    if([appControl.ENABLE_PRODUCT_BARCODE_SCAN isEqualToString:KAppControlsYESCode])
    {
        xSearchBarTrailingspaceToFilterButton.constant=47;
        [barcodeScanButton setHidden:NO];
    }

    
}




- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked with text %@",searchBar.text);
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching=YES;
}

-(void)searchContent
{
    NSString *searchString = productsSearchBar.text;
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSLog(@"searching with text in fs product search %@",searchString);

    
    NSMutableArray *componentsPredicateArray=[[NSMutableArray alloc]init];
    NSPredicate *searchStringComponentsPredicate;

    NSArray *searchTextComponentsArray=[searchString componentsSeparatedByString:@"*"];
   // NSLog(@"search text components are %@", searchTextComponentsArray);
    
    
    for (NSInteger i=0; i<searchTextComponentsArray.count; i++) {
        
        if([[[searchTextComponentsArray objectAtIndex:i]trimString]isEqualToString:@""])
        {
            
        }
        else
        {
            NSPredicate *componentesPredicate = [NSPredicate predicateWithFormat:filter, @"Description", [searchTextComponentsArray objectAtIndex:i]];
            [componentsPredicateArray addObject:componentesPredicate];
        }
       
       // NSLog(@"components predicate array %@", componentsPredicateArray);


    }
    
    searchStringComponentsPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:componentsPredicateArray];
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Brand_Code", searchString];
    NSPredicate *barCodePredicate=[NSPredicate predicateWithFormat:@"SELF.ProductBarCode ==[cd] %@",searchString];

    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1,barCodePredicate, [SWDefaults fetchMultipartSearchPredicate:searchString withKey:@"Description"]]];
    

    filteredProductsArray=[[productsArray filteredArrayUsingPredicate:predicate] mutableCopy];
  //  NSLog(@"filtered Products count %d", filteredProductsArray.count);
    if (filteredProductsArray.count>0) {
        uomTextField.enabled=YES;
        [productsTableView reloadData];
/*        [productsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                        animated:YES
                                  scrollPosition:UITableViewScrollPositionNone];
        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [productsTableView.delegate tableView:productsTableView didSelectRowAtIndexPath:firstIndexPath];
 */
        
    }
    else if (filteredProductsArray.count==0)
    {
        isSearching=YES;
        [productsTableView reloadData];
//        [self resetProductDetails];
        
        
    }
    
}
-(void)resetProductDetails
{
    productNameLbl.text=@"N/A";
    brandLbl.text=@"N/A";
    discountLbl.text=@"N/A";
    agencyLbl.text=@"N/A";
    uomLbl.text=@"N/A";
    mslLbl.text=@"N/A";
    stockLbl.text=@"N/A";
    wholesalePriceLbl.text=@"N/A";
    retailPriceLbl.text=@"N/A";
    uomTextField.text=@"N/A";
    uomTextField.enabled=NO;
    [slicesForTarget removeAllObjects];
    [targetPieChart reloadData];
    
}

#pragma mark Pie chart delegate methods

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    
    NSLog(@"number of slices called %@",slicesForTarget);
    
    if (pieChart == targetPieChart && slicesForTarget.count>0) {
        count = slicesForTarget.count;
    }
    else
    {
    }
    
    return count;
    
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    
    if (pieChart == targetPieChart) {
        count = [[NSString stringWithFormat:@"%@",[slicesForTarget objectAtIndex:index]]floatValue];
    }
    else
    {
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == targetPieChart) {
        colour = [sliceColorsForTarget objectAtIndex:(index % sliceColorsForTarget.count)];
    }
    return colour;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITextField Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==uomTextField)
    {
        [self uomTextFieldTapped];
        return NO;
    }
    return YES;
}



#pragma mark MultiUOM
-(void)UpdatedProductStockDetails:(Products *)product onUOMSelection:(ProductUOM *)proUOM
{
        Products *pro=[product copy];
        NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
        NSMutableArray *UOMarray=[pro.ProductUOMArray mutableCopy];
        NSMutableArray *filtredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
        
        if(filtredArray.count==1)
        {
            ProductUOM *primaryUOMObj=[filtredArray objectAtIndex:0];
            
            stockLbl.text=[NSString stringWithFormat:@"%ld",(long)[[NSString stringWithFormat:@"%f",([pro.stock doubleValue]*[primaryUOMObj.Conversion doubleValue])/[proUOM.Conversion doubleValue]] integerValue]];
        }
}


-(void)uomTextFieldTapped
{
    
    
    selectedProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:[selectedProduct copy]];
    
    uomsArray=[selectedProduct.ProductUOMArray valueForKey:@"Item_UOM"];
    
    if (uomsArray.count>0) {
   
    popOverController=nil;
    popString=[[NSString alloc]init];
    popString=@"UOM";
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc]initwithStyle:UITableViewStylePlain andContentSize:CGSizeMake(200, 200)];
    
    locationPopOver.colorNames = uomsArray;
        
    CGRect relativeFrame = [uomTextField convertRect:uomTextField.bounds toView:self.view];
        
        NSLog(@"relative frame for uom is %@", NSStringFromCGRect(relativeFrame));
        
    locationPopOver.delegate = self;
    if (popOverController == nil) {
        popOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
        [popOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        popOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [popOverController dismissPopoverAnimated:YES];
        popOverController = nil;
        popOverController.delegate=nil;
    }
    
}
}




-(void)selectedStringValue:(NSIndexPath *)indexPath
{
    refinedStock=0;
    
    if ([popString isEqualToString:@"UOM"]) {
        
        selectedUOM=[uomsArray objectAtIndex:indexPath.row];
        
        uomTextField.text=selectedUOM;
        //now fetch price for this uom
        
        selectedProduct.selectedUOM=selectedUOM;

        
        [self fetchProductPriceListData:selectedProduct];
        [self UpdatedProductStockDetails:selectedProduct onUOMSelection:[selectedProduct.ProductUOMArray objectAtIndex:indexPath.row]];
        [stockTableView reloadData];
        


    }
    if (popOverController)
    {
        [popOverController dismissPopoverAnimated:YES];
        popOverController = nil;
        popOverController.delegate=nil;
    }
    locationPopOver.delegate=nil;
    locationPopOver=nil;
    

}

#pragma mark Filter Button Action



- (IBAction)filterButtonTapped:(id)sender {
    [self closeSplitView];
    
    NSLog(@"unfiltered data in %@", unfilteredProductsArray);
    isSearching=NO;
    [self hideNoSelectionView];
    
    if (productsArray.count>0) {
        [productsSearchBar setShowsCancelButton:NO animated:YES];
        productsSearchBar.text=@"";
        [productsSearchBar resignFirstResponder];
        [self.view endEditing:YES];
        [productsTableView reloadData];
        [self deselectPreviousCellandSelectFirstCell];
        
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        // Blurred with UIImage+ImageEffects
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        //blurredBgImage.image = [self blurWithImageEffects:[self takeSnapshotOfView:[self view]]];
        
        [self.view addSubview:blurredBgImage];
        
        UIButton* searchBtn=(id)sender;
        
        SalesWorxProductsFilterViewController * popOverVC=[[SalesWorxProductsFilterViewController alloc]init];
        popOverVC.delegate=self;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict=previousFilteredParameters;
            
        }
        popOverVC.productsArray=unfilteredProductsArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        
        AppControl * appControl=[AppControl retrieveSingleton];
        NSString* showPromoView=appControl.ENABLE_PROMO_ITEM_VALIDITY;
        if ([NSString isEmpty:showPromoView]==NO) {
            if ([showPromoView isEqualToString:KAppControlsYESCode]) {

                [filterPopOverController setPopoverContentSize:CGSizeMake(366, 530) animated:YES];

            }
            else
            {
                [filterPopOverController setPopoverContentSize:CGSizeMake(366, 490) animated:YES];
 
            }
        }
        
        //[filterPopOverController setPopoverContentSize:CGSizeMake(366, 560) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
        CGRect currentFrame = [searchBtn convertRect:searchBtn.bounds toView:self.view];

        [filterPopOverController presentPopoverFromRect:currentFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
    else{
        
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Data" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try later" withController:self];

    }

}


-(void)filteredProducts:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    productsArray=filteredArray;
    [productsTableView reloadData];
    NSLog(@"customers array count in filter %d",productsArray.count);
    [self deselectPreviousCellandSelectFirstCell];
}
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    previousFilteredParameters=parametersDict;
}
-(void)productsFilterdidReset
{
    [blurredBgImage removeFromSuperview];

    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    productsArray=unfilteredProductsArray;
    [productsTableView reloadData];
    [self deselectPreviousCellandSelectFirstCell];

}
-(void)productsFilterDidClose
{
    [blurredBgImage removeFromSuperview];

}

#pragma mark UIPopOver delegate

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController == filterPopOverController) {
        
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    
    [productsSearchBar setShowsCancelButton:NO animated:YES];
}

-(void)addKeyBoardObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark UIScrollView Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==productsTableView) {
        
        [self.view endEditing:YES];
    }
}

#pragma mark Barcode_Scan
-(IBAction)barCodeScanButtonTapped:(id)sender
{
    
    [self.view endEditing:YES];
    SalesWorxBarCodeScannerManager *barcodeManager=[[SalesWorxBarCodeScannerManager alloc]init];
    barcodeManager.swxBCMdelegate=self;
    [barcodeManager ScanBarCode];
}

- (IBAction)AssortmentBnsButtonTapped:(id)sender {
    NSMutableArray  *ProductAssortmentPlans=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select A.Assortment_Plan_ID from TBL_BNS_Assortment_Items AS A left join  tbl_product AS B on A.Item_Code=B.Item_Code Where A.Item_Code='%@' and Is_Get_Item='N'",selectedProduct.Item_Code]];
    if(ProductAssortmentPlans.count>0)
    {
        ProductAssortmentPlans= [ProductAssortmentPlans valueForKey:@"Assortment_Plan_ID"];
        NSMutableArray *availableBnsPlansarray=[[AssortmentPlansArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PlanId IN %@", ProductAssortmentPlans]] mutableCopy];
        if(availableBnsPlansarray.count>0){
            SalesWorxAssortmentBonusInfoViewController *salesOrderBonusInfoViewController=[[SalesWorxAssortmentBonusInfoViewController alloc]initWithNibName:@"SalesWorxAssortmentBonusInfoViewController" bundle:[NSBundle mainBundle]];
            salesOrderBonusInfoViewController.view.backgroundColor = [UIColor clearColor];
            salesOrderBonusInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
            salesOrderBonusInfoViewController.plan=[availableBnsPlansarray objectAtIndex:0];
            [self.navigationController presentViewController:salesOrderBonusInfoViewController animated:NO completion:nil];
        }
    }
}

- (void)BarCodeScannerButtonBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    [reader dismissViewControllerAnimated:YES completion:^{
        
        NSLog(@"scanned bar code is %@", symbol.data);
        NSLog(@"symbol is %@", symbol.typeName);
        
        NSPredicate *barCodePredicate=[NSPredicate predicateWithFormat:@"SELF.ProductBarCode ==[cd] %@",symbol.data];
        if([[unfilteredProductsArray filteredArrayUsingPredicate:barCodePredicate] count]>0)
        {
            previousFilteredParameters=[[NSMutableDictionary alloc]init];
            productsSearchBar.text=symbol.data;
            [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
            productsArray=unfilteredProductsArray;
            isSearching=YES;
            [self searchContent];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"No product found" withController:self];
        }
    }];
}

#pragma mark show/hide No selection view
-(void)hideNoSelectionView
{
    [NoSelectionView setHidden:YES];
    NoSelectionHeightConstraint.constant=KZero;
    NoSelectionBottomMarginConstraint.constant=KZero;
    bonusViewHeightConstraint.constant = self.view.frame.size.height - targetViewHeightConstraint.constant - imageViewHeightConstraint.constant - 32;
}
-(void)showNoSelectionViewWithMessage
{
    [NoSelectionView setHidden:NO];
    NoSelectionHeightConstraint.constant=self.view.frame.size.height-16;
    NoSelectionBottomMarginConstraint.constant=8;
    bonusViewHeightConstraint.constant = self.view.frame.size.height - targetViewHeightConstraint.constant - imageViewHeightConstraint.constant - 32;
}

@end
