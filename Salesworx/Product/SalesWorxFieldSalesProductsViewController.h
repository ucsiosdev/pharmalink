//
//  SalesWorxFieldSalesProductsViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/16/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDefaultButton.h"
#import "HMSegmentedControl.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"

@interface SalesWorxFieldSalesProductsViewController : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource, UIPopoverControllerDelegate>
{
    IBOutlet UITableView *productsTableView;
    IBOutlet UISearchBar *productsSearchBar;
    IBOutlet SalesWorxDefaultButton *barcodeScanButton;
    IBOutlet UIView *NoSelectionView;
    UIPopoverController* filterPopOverController, *FOCPopOverController;
    IBOutlet UIButton *filterButton;
    IBOutlet HMSegmentedControl * productDetailsSegmentControl;

    NSMutableDictionary * previousFilteredParameters;
    NSMutableArray *productsTableViewDataArray;
    NSMutableArray *filteredProductsTableViewDataArray;
    
    UIActivityIndicatorView *searchBarActivitySpinner;
    ProductSearchType proSearchType;
    BOOL isSearchBarClearButtonTapped; /* do not use BOOL other than for product search popover */
    BOOL allowSearchBarEditing;
    NSMutableArray *productsBrandCodesArray;
    NSMutableArray* filteredProductsArrayGroupedByBrandCode;
    NSMutableArray* productsArrayGroupedByBrandCode;
    NSMutableArray *ProductTableViewExpandedSectionsArray;
    
    NSIndexPath * productTableViewSelectedIndexPath;
    IBOutlet NSLayoutConstraint *xSearchBarTrailingspaceToFilterButton;
    IBOutlet NSLayoutConstraint *NoSelectionBottomMarginConstraint;
    IBOutlet NSLayoutConstraint *NoSelectionLeadingMarginConstraint;
    IBOutlet NSLayoutConstraint *NoSelectionTrailingMarginConstraint;

    IBOutlet MedRepElementDescriptionLabel *errorMessageLabel;
}
- (IBAction)filterButtonTapped:(id)sender;
-(IBAction)barCodeScanButtonTapped:(id)sender;
@property (weak,nonatomic) IBOutlet UIScrollView *pagingScrollView;
-(void)UpdateProductDetailsOnUOMSelection:(ProductUOM *)proUOM;
@end
