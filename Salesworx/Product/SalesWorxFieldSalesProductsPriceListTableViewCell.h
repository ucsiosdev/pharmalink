//
//  SalesWorxFieldSalesProductsPriceListTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/20/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxFieldSalesProductsPriceListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *priceListNameLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *wholeSalePrice;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *RetailPriceLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *validToLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *UOMLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ValidFromLabel;
@end
