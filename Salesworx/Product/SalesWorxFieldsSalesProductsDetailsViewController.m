//
//  SalesWorxFieldsSalesProductsDetailsViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/17/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxFieldsSalesProductsDetailsViewController.h"
#import "SWDatabaseManager.h"
#import "AppControl.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxFieldSalesProdcutsMoreDetailsViewController.h"
#import "SalesWorxFieldSalesProductsViewController.h"
#import "SalesWorxProductMediaViewController.h"

@interface SalesWorxFieldsSalesProductsDetailsViewController ()
{
    AppControl *appControl;
}
@end

@implementation SalesWorxFieldsSalesProductsDetailsViewController
@synthesize currentProduct;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // self.view.backgroundColor=[UIColor redColor];
    appControl=[AppControl retrieveSingleton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)viewWillAppear:(BOOL)animated{
    [self PreparePieChart];
    [self PopulateProductDetails];
    
 }
-(void)UpdateProductDetailsOnTableViewCellSelection{
    [self PopulateProductDetails];
}
-(void)PopulateProductDetails{
    
    
    /** Basic Details*/
    productNameLabel.text=currentProduct.Description;
    productStockLabel.text= [NSString stringWithFormat:@"%@ %@",currentProduct.stock,currentProduct.primaryUOM];
    productBrandNameLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:[NSString stringWithFormat:@"%@",currentProduct.Brand_Code]];
    productMSLStatusLabel.text=[NSString stringWithFormat:@"%@",currentProduct.IsMSL];
    productAgencyNameLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:[NSString stringWithFormat:@"%@",currentProduct.Agency]];
    productOnOrderQtyLabel.text= [appControl.SHOW_ON_ORDER isEqualToString:KAppControlsYESCode]?[NSString stringWithFormat:@"%@ %@",currentProduct.OnOrderQty,currentProduct.primaryUOM]:KNotApplicable;
    productDefaultDicountLabel.text=[appControl.ALLOW_DISCOUNT isEqualToString:KAppControlsYESCode]?[[NSString stringWithFormat:@"%@",currentProduct.Discount] percentageString]:KNotApplicable;
    productSpecialDicountLabel.text=[appControl.ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsYESCode]?[[NSString stringWithFormat:@"%@",currentProduct.specialDiscount] percentageString]:KNotApplicable;
    productNearestExpiryLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:currentProduct.nearestExpiryDate]]];
    productMaximumExpiryDateLabel.text=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:[MedRepDefaults getDefaultStringForEmptyString:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:currentProduct.maxExpiryDate]]];

    /** Image*/
    [self fetchImagesforProduct];
    
    /** Target Chart*/
    if([appControl.SHOW_PRODUCT_TARGET isEqualToString:KAppControlsYESCode]){
        [self fetchTargetDataforProduct:currentProduct];
    }else{
        targetTypeValueLabel.text=@"";
        targrtTypeHeaderLabel.text=@"";
        [targetPieChart setHidden:YES];
        [targetPieChartNoDataImageView setHidden:NO];
    }

    /** UOM*/
    if([appControl.ENABLE_MULTI_UOM isEqualToString:KAppControlsYESCode]){
        
        productUomsTextField.text=currentProduct.selectedUOM;
        [productUOMLabel setHidden:YES];
        
        currentProduct.ProductUOMArray=[[SWDatabaseManager retrieveManager]getUOMsForProduct:currentProduct];
        if(currentProduct.ProductUOMArray.count==1){
            [productUomsTextField setIsReadOnly:YES];
        }else{
            [productUomsTextField setIsReadOnly:NO];
        }
        
    }else{
        [productUomsTextField setHidden:YES];
        productUOMLabel.text=currentProduct.primaryUOM;
    }
    
    if (currentProduct.stock.integerValue==0) {
        statusImageView.backgroundColor=[UIColor redColor];
    }
    else{
        statusImageView.backgroundColor=[UIColor greenColor];
    }
    if ([appControl.FS_ENABLE_PRODUCT_MEDIA isEqualToString:KAppControlsYESCode]) {
        NSMutableArray *selectedProductMediaArray = [[SWDatabaseManager retrieveManager] fetchMediaFilesforProduct:currentProduct.ItemID];
        if (selectedProductMediaArray.count == 0) {
            btnMedia.hidden = YES;
        } else {
            btnMedia.hidden = NO;
        }
    }else{
        btnMedia.hidden = YES;
    }
    
    
    /** pricing*/
    [self UpdateProductPriceListDetails];
}

-(void)fetchImagesforProduct{
    NSMutableArray* selectedImagesArray=[[SWDatabaseManager retrieveManager]fetchProductImagesforSelectedProduct:currentProduct];
    NSLog(@"selected imagesArray is %@",selectedImagesArray);
    ProductMediaFile * mediaFile=[[ProductMediaFile alloc]init];
    
    if (selectedImagesArray.count>0) {
        currentProduct.productImagesArray=[[NSMutableArray alloc]initWithArray:selectedImagesArray];
        NSLog(@"selected product images %@",currentProduct.productImagesArray);
        //display default image, conditon is Custom_Attribute_2 is Y for default image
        
        NSPredicate * defaultImagePredicate=[NSPredicate predicateWithFormat:@"SELF.Custom_Attribute_2 == 'Y'"];
        NSArray* filteredArray=[currentProduct.productImagesArray filteredArrayUsingPredicate:defaultImagePredicate];
        if (filteredArray.count>0) {
            mediaFile=[filteredArray objectAtIndex:0];
        }
        else
        {
            mediaFile=[currentProduct.productImagesArray objectAtIndex:0];
        }
        NSLog(@"default image id is %@", mediaFile.Media_File_ID);
        selectedMediaFileID=mediaFile.Media_File_ID;
        
        productImageView.image=[UIImage imageNamed:mediaFile.File_Path];
    }
    else{
        productImageView.image=[UIImage imageNamed:@"ProductsDefaultImage"];
    }
}


#pragma mark TargetPieChart Methods
-(void)PreparePieChart{
    
  //  if(targetPieChart==nil){
        slicesForTarget=[[NSMutableArray alloc]init];
        sliceColorsForTarget=[NSArray arrayWithObjects:
                              [UIColor colorWithRed:53.0/255.0 green:194.0/255.0 blue:195.0/255.0 alpha:1],
                              [UIColor colorWithRed:16.0/255.0 green:49.0/255.0 blue:57.0/255.0 alpha:1],
                              nil];

        targetPieChart.labelRadius = 30;
        [targetPieChart setDelegate:self];
        [targetPieChart setDataSource:self];
        targetPieChart.labelFont =[UIFont boldSystemFontOfSize:14];
        [targetPieChart setPieCenter:CGPointMake(targetPieChart.bounds.size.width/2, targetPieChart.bounds.size.height/2)];
        [targetPieChart setShowPercentage:YES];
        [targetPieChart setLabelColor:[UIColor whiteColor]];
        [targetPieChart setUserInteractionEnabled:NO];
        [targetPieChart reloadData];
  //  }


}
-(void)fetchTargetDataforProduct:(Products*)selectedProd
{
    
    [slicesForTarget removeAllObjects];
    [targetPieChart reloadData];
    
    
    NSDictionary * targetArray=[[NSDictionary alloc] init];
    
    
    if ([appControl.FSR_TARGET_TYPE isEqualToString:@"B"] ) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:currentProduct.Brand_Code] mutableCopy];
        targetTypeValueLabel.text=currentProduct.Brand_Code;
        targrtTypeHeaderLabel.text=@"Brand:";
    } else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"C"]) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:currentProduct.Category] mutableCopy];
        targetTypeValueLabel.text=currentProduct.Category;
        targrtTypeHeaderLabel.text=@"Category:";


    } else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"N"]) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:currentProduct.Agency] mutableCopy];
        targetTypeValueLabel.text=currentProduct.Agency;
        targrtTypeHeaderLabel.text=@"Agency:";


    }else if ([appControl.FSR_TARGET_TYPE isEqualToString:@"P"]) {
        
        targetArray=[[[SWDatabaseManager retrieveManager] fetchProductTarget:currentProduct.Item_Code] mutableCopy];
        targetTypeValueLabel.text=@"";
        targrtTypeHeaderLabel.text=@"";

    }else{
        targetTypeValueLabel.text=@"";
        targrtTypeHeaderLabel.text=@"";
    }
    
    
    NSLog(@"target array count for product %lu", (unsigned long)targetArray);
    if (targetArray.count>0 && [targetArray valueForKey:@"NumberOFTargetValues"]!=nil &&  [[targetArray valueForKey:@"NumberOFTargetValues"] integerValue]>0 && !([[targetArray valueForKey:@"Balance_To_Go"] doubleValue]==0 && [[targetArray valueForKey:@"Sales_Value"] doubleValue]==0)) {
        [targetPieChart setHidden:NO];
        [targetPieChartNoDataImageView setHidden:YES];

        // NSLog(@"product target data %@", targetArray);
        double targetVal =[[targetArray valueForKey:@"Target_Value"] doubleValue];
        double balanceToGo =[[targetArray valueForKey:@"Balance_To_Go"] doubleValue];
//        double Sales_Value =[[targetArray valueForKey:@"Sales_Value"] doubleValue];
        
        if (targetVal < balanceToGo) {
            balanceToGo = targetVal;
        }
        double Sales_Value = targetVal-balanceToGo;
        
        [slicesForTarget addObject:[NSNumber numberWithDouble:Sales_Value]];
        [slicesForTarget addObject:[NSNumber numberWithDouble:balanceToGo]];
        [targetPieChart reloadData];
        
    }else{
        [targetPieChart setHidden:YES];
        [targetPieChartNoDataImageView setHidden:NO];
    }
    
    
}
#pragma mark Pie chart delegate methods

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    NSInteger count = 0;
    
    NSLog(@"number of slices called %@",slicesForTarget);
    
    if (pieChart == targetPieChart && slicesForTarget.count>0) {
        count = slicesForTarget.count;
    }
    else
    {
    }
    
    return count;
    
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat count = 0;
    
    if (pieChart == targetPieChart) {
        count = [[NSString stringWithFormat:@"%@",[slicesForTarget objectAtIndex:index]]floatValue];
    }
    else
    {
    }
    return count;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    UIColor * colour;
    if (pieChart == targetPieChart) {
        colour = [sliceColorsForTarget objectAtIndex:(index % sliceColorsForTarget.count)];
    }
    return colour;
}
#pragma mark UITextField Delegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==productUomsTextField) {
        popOverTitle=KSalesOrderUOMPopOverTitle;
        [self presentPopoverfor:productUomsTextField withTitle:KSalesOrderUOMPopOverTitle withContent:[currentProduct.ProductUOMArray valueForKey:@"Item_UOM"]];
        return NO;
    }
    return YES;
}



-(void)presentPopoverfor:(MedRepTextField*)selectedTextField withTitle:(NSString*)popOverString withContent:(NSMutableArray*)contentArray
{
    
    
    
    [self.view endEditing:YES];
    
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray=contentArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.popoverType=popOverString;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    
    popOverVC.titleKey=popOverString;
    if([popOverString isEqualToString:KSalesOrderUOMPopOverTitle])
    {
        popOverVC.preferredContentSize = CGSizeMake(250, 250);
        popOverVC.disableSearch=YES;
        
    }
    else{
        popOverVC.preferredContentSize = CGSizeMake(300, 300);
        popOverVC.disableSearch=NO;
    }
    popover.delegate = self;
    popOverVC.titleKey=popOverString;
    popover.sourceView = selectedTextField.rightView;
    popover.sourceRect =selectedTextField.dropdownImageView.frame;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{
        
        
    }];
    
}





-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
    
    if ([popOverTitle isEqualToString:KSalesOrderUOMPopOverTitle]) {
        
        [self updateProductAndOrderDetailsUIOnUOMSelection:[currentProduct.ProductUOMArray objectAtIndex:selectedIndexPath.row]];
        
        
        [UOMPopOverController dismissPopoverAnimated:YES];
        UOMPopOverController=nil;
    }
}
-(void)updateProductAndOrderDetailsUIOnUOMSelection:(ProductUOM *)UOMObj{
    currentProduct.selectedUOM=UOMObj.Item_UOM;
    NSLog(@"currentProduct.selectedUOM %@",currentProduct.selectedUOM);
    
    if([self.parentViewController isKindOfClass:[SalesWorxFieldSalesProductsViewController class]]){
        [(SalesWorxFieldSalesProductsViewController *)self.parentViewController UpdateProductDetailsOnUOMSelection:UOMObj];
    }
    
    
}
-(void)UpdateProductPriceListDetails{
    NSMutableArray *productPriceListArray=[[SWDatabaseManager retrieveManager]fetchavAvailableProductPriceLists:currentProduct];

    NSMutableArray *tempGenericProductPriceListsArray= [[productPriceListArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Is_Generic='Y'"]] mutableCopy];
    
    /** populating generic price list data*/
    if(tempGenericProductPriceListsArray.count>0){
        NSDictionary *genericPriceList=[tempGenericProductPriceListsArray objectAtIndex:0];
        productWholesalePriceLabel.text=[[NSString stringWithFormat:@"%@",[genericPriceList valueForKey:@"Unit_Selling_Price"]] currencyString];
        productRetailPriceLabel.text=[[NSString stringWithFormat:@"%@",[genericPriceList valueForKey:@"Unit_List_Price"]] currencyString];
    }else{
        productWholesalePriceLabel.text=KNotApplicable;
        productRetailPriceLabel.text=KNotApplicable;
    }

}
-(IBAction)MoreInfoButtonTapped:(id)sender{
    SalesWorxFieldSalesProdcutsMoreDetailsViewController *moreDetailsController=[[SalesWorxFieldSalesProdcutsMoreDetailsViewController alloc]init];
    moreDetailsController.view.backgroundColor = [UIColor clearColor];
    moreDetailsController.modalPresentationStyle = UIModalPresentationCustom;
    moreDetailsController.currentProduct=currentProduct;
    [self.navigationController presentViewController:moreDetailsController animated:NO completion:nil];
}

- (IBAction)mediaButtonTapped:(id)sender {
    SalesWorxProductMediaViewController *productMediaViewController = [[SalesWorxProductMediaViewController alloc]init];
    productMediaViewController.selectedProductID = [NSString getValidStringValue:currentProduct.ItemID];
    productMediaViewController.isFromProductsScreen = true;
    [self.navigationController pushViewController:productMediaViewController animated:YES];
}

@end
