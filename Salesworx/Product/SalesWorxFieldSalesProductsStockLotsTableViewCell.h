//
//  SalesWorxFieldSalesProductsStockLotsTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/18/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
@interface SalesWorxFieldSalesProductsStockLotsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *lotNumberLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *QtyLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *expDateLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *warehouseLabel;

@end
