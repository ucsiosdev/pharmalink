//
//  SalesWorxFieldSalesProdcutsMoreDetailsViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/23/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxPresentControllerBaseViewController.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
@interface SalesWorxFieldSalesProdcutsMoreDetailsViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet MedRepElementDescriptionLabel *productDescriptionLabel;
    IBOutlet MedRepElementDescriptionLabel *productTradeNameLabel;
    IBOutlet MedRepElementDescriptionLabel *productGenericNameLabel;
    IBOutlet MedRepElementDescriptionLabel *productFormLabel;
    
}
- (IBAction)DoneButtonTapped:(id)sender;
@property (strong,nonatomic) Products *currentProduct;
@end
