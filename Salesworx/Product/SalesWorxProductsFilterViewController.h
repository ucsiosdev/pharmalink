//
//  SalesWorxProductsFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepButton.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "AppControl.h"
#import "NSString+Additions.h"
#import "SalesWorxDefaultSegmentedControl.h"
@protocol SalesWorxProductsFilterViewControllerDelegate <NSObject>

-(void)filteredProducts:(NSMutableArray*)filteredArray;
-(void)productsFilterDidClose;
-(void)productsFilterdidReset;
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict;


@end


@interface SalesWorxProductsFilterViewController : UIViewController<SelectedFilterDelegate>
{
    NSMutableDictionary * filterParametersDict;
    NSMutableArray * filteredProductsArray;
    IBOutlet MedRepTextField *productNameTxtFld;
    
    IBOutlet MedRepTextField *brandCodeTxtFld;
    
    IBOutlet MedRepTextField *itemCodeTxtFld;
    IBOutlet SalesWorxDefaultSegmentedControl *stockSegment;
    
    IBOutlet SalesWorxDefaultSegmentedControl *bonusSegment;
    
    IBOutlet UISwitch *stockSwitch;
    IBOutlet SalesWorxDefaultSegmentedControl *promoItemSegment;
    
    IBOutlet UISwitch *bonusSwitch;
    NSString* selectedTextField;
    NSString* selectedPredicateString;
    
    IBOutlet UIView *promoView;
    IBOutlet NSLayoutConstraint *submitViewTopConstraint;
    IBOutlet NSLayoutConstraint *promoviewHeightConstraint;
    IBOutlet UISwitch *promoItemSwitch;
}
- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)promoItemSegmentTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepButton *resetButtontapped;

- (IBAction)bonusSwitchTapped:(id)sender;
@property(strong,nonatomic)NSMutableArray * productsArray;
@property(strong,nonatomic)NSMutableDictionary * previousFilterParametersDict;
@property(nonatomic) id delegate;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;
@property(strong,nonatomic)UINavigationController * filterNavController;
@property(strong,nonatomic) NSString* filterTitle;

@end
