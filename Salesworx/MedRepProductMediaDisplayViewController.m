//
//  MedRepProductMediaDisplayViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepProductMediaDisplayViewController.h"
#import "SWDefaults.h"
#import "MedRepProductImageDisplayTableViewCell.h"
@interface MedRepProductMediaDisplayViewController ()

@end

@implementation MedRepProductMediaDisplayViewController

@synthesize pdfWebView,filePath,productImagesArray,selectedIndexPath,isImage,imagesTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    
    
    if (isImage==YES) {
        
        
        imagesTableView.hidden=NO;
        
        //hide the webview show tableview custom cell for images
        
        pdfWebView.hidden=YES;
        
        if (productImagesArray.count>0) {
            
            imagesTableView.delegate=self;
            imagesTableView.dataSource=self;
            imagesTableView.transform = CGAffineTransformMakeRotation(-M_PI * 0.5);

            [imagesTableView reloadData];
        }
      
    
    }
    
    else
    {
    
        imagesTableView.hidden=YES;

        NSLog(@"file path is %@", [filePath description]);
        
        
    NSURL *targetURL = [NSURL fileURLWithPath:filePath];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [pdfWebView loadRequest:request];
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeButtonTapped:(id)sender {
    
    if ([self.pdfViewerdelegate respondsToSelector:@selector(PDFViewerDidCancel)]) {
        
        [self.pdfViewerdelegate PDFViewerDidCancel];
        
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productImagesArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
   
        
        
        static NSString* identifier =@"productCell";
        MedRepProductImageDisplayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepProductImageDisplayTableViewCell" owner:nil options:nil] firstObject];
            
        }
        //fetch images from documents directory
        
        
        //iOS 8 support
        
        
        NSString*documentsDirectory;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        
        
        
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[productImagesArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row]];
        
       // NSLog(@"file name is %@", fileName);
        
        cell.productImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfFile:fileName]];
    
        return cell;
        
    }
    

@end
