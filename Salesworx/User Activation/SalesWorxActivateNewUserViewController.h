//
//  SalesWorxActivateNewUserViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

@class SalesWorxActivationManager;

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepButton.h"
#import "MedRepCustomClass.h"
#import "NSString+Additions.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxActivationManager.h"
#import "MedRepQueries.h"
#import <Google/Analytics.h>


@interface SalesWorxActivateNewUserViewController : UIViewController<UITextFieldDelegate,UIPopoverControllerDelegate,UIAlertViewDelegate>

{
    IBOutlet MedRepTextField *usernameTextField;
    
    IBOutlet TYMProgressBarView *syncProgressBar;

    
    UIAlertView * databaseUploadSuccessAlert;
    
    IBOutlet UILabel *firstTimeuseLbl;
    
    IBOutlet UIView *firstTimeUseView;
    IBOutlet MedRepElementDescriptionLabel *syncStatusLbl;

    
    IBOutlet MedRepTextField *passwordTextField;
    
    IBOutlet MedRepView *credentialsView;
    IBOutlet MedRepTextField *servernameTextField;
    
    IBOutlet MedRepTextField *serverLocationTextField;
    
    IBOutlet TYMProgressBarView *activationProgressBar;
    IBOutlet NSLayoutConstraint *loginDetailsViewTopConstraint;
    
    IBOutlet MedRepButton *activateButton;
    
    SyncLoctions * syncLocations;
    
    IBOutlet UILabel *lblVersion;

    
    NSMutableArray* serverLocationsArray;
    
    NSMutableArray * serverLocationsResponseArray;
    
    UIPopoverController* popoverController;
    
    NSMutableDictionary * userDetailsDict;

    
    SalesWorxActivationManager * activationManager;
    
    NSString* currentSyncStatus;
    
    NSString* lastSyncStausString;


}
@property(nonatomic) BOOL isActivationNewUserFromLogin;
@property(nonatomic) BOOL isFromLogin;

-(IBAction)activateNewUserTapped:(id)sender;
@end
