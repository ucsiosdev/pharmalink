//
//  SalesWorxActivateNewUserViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxActivateNewUserViewController.h"
#import "SWDefaults.h"
#import "SWFoundation.h"
#import "LoginViewController.h"
#import "UIDeviceHardware.h"
#import "SWAppDelegate.h"
#import "SalesWorxDeviceValidationClass.h"
@interface SalesWorxActivateNewUserViewController ()
{
    SWAppDelegate * appdel;

}
@end

@implementation SalesWorxActivateNewUserViewController
@synthesize isActivationNewUserFromLogin,isFromLogin;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appdel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];

   // [self fetchSyncLocations];

    syncProgressBar.barBorderWidth=2.0;
    syncProgressBar.barBorderColor=UIViewControllerBackGroundColor;
    syncProgressBar.barBackgroundColor=UIViewControllerBackGroundColor;
    syncProgressBar.barFillColor=[UIColor clearColor];
    syncProgressBar.barInnerBorderColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    
    
    syncProgressBar.usesRoundedCorners=YES;
    
    
    
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(RecieveSyncStatusUpdate:) name:KNewActivationNotificationNameStr object:nil];
    
    
    firstTimeUseView.hidden=YES;
    firstTimeuseLbl.hidden=YES;
    credentialsView.hidden=NO;
    
    userDetailsDict=[[NSMutableDictionary alloc]init];
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:NSLocalizedString(kActivateNewUserTitle, nil)];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }

    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }

    if (isActivationNewUserFromLogin==YES) {
        
        [activateButton setTitle:@"Upload Database" forState:UIControlStateNormal];
        usernameTextField.text=[[SWDefaults userProfile] stringForKey:@"Username"];
        
        
        passwordTextField.text=[SWDefaults passwordForActivate];
        
        [usernameTextField setIsReadOnly:YES];
        [passwordTextField setIsReadOnly:YES];
        [servernameTextField setIsReadOnly:YES];
        [serverLocationTextField setIsReadOnly:YES];
        
        
        
       
        
        CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
        NSError *error;
        NSData* dataDict ;
        
//        if ([NSString isEmpty:[SWDefaults activatedServerDictionary]]) {
//             dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
//  
//        }
//        else{
//           NSData*  dataDict = [[SWDefaults activatedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
// 
//        }
        
           dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *selectedServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]] ;
    
        NSLog(@"selected server dict in act %@", selectedServerDict);
        
        servernameTextField.text=[selectedServerDict valueForKey:@"name"];
        
        serverLocationTextField.text=[selectedServerDict valueForKey:@"url"];
        

        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(RecieveUploadDatabaseNotification:) name:kUploadDatabaseExistingUserNotificationName object:nil];
        
    }
    
    lblVersion.text= [NSString stringWithFormat:@"Version  %@",[[[SWDefaults alloc]init] getAppVersionWithBuild]];
    
    UIDeviceHardware *hardware=[[UIDeviceHardware alloc] init];
    NSLog(@"Device is %@",[hardware platformString]);
    
    if ([[hardware platformString] isEqualToString:@"Simulator"]) {
        
        // [self fetchSyncLocations];
        
    }
    
    NSString* defaultsCustomerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    
    
    NSLog(@"server locations before didload %@", serverLocationsArray);
    
    
    if ([defaultsCustomerID isEqualToString:@"C16K82563"] )//&&[[hardware platformString] isEqualToString:@"Simulator"])
    {
       
        
    }
    
    
  NSMutableArray* syncObjectsArray=  [self fetchSyncLocationswithSyncRequest];
    

    if (syncObjectsArray.count>0) {
     for (NSInteger i=0; i<syncObjectsArray.count; i++) {
        
        SyncLoctions *syncLoc =[[SyncLoctions alloc]init];
        
        syncLoc=[syncObjectsArray objectAtIndex:i];
        
        if ([syncLoc.url isEqualToString:@"swx-db.cloudapp.net:12008"]) {
            
            //this is demo user
            
            if ([defaultsCustomerID isEqualToString:@"C16K82563"] /*||[[hardware platformString] isEqualToString:@"Simulator"]*/)
            {

                firstTimeUseView.hidden=NO;
                firstTimeuseLbl.hidden=NO;
                firstTimeUseView.backgroundColor=[UIColor whiteColor];
                credentialsView.hidden=YES;
                
            NSMutableDictionary* selectedServerDictionary=[[NSMutableDictionary alloc]init];

            [selectedServerDictionary setValue:syncLoc.name forKey:@"name"];
            [selectedServerDictionary setValue:syncLoc.url forKey:@"url"];
            [selectedServerDictionary setValue:syncLoc.seq forKey:@"seq"];
            
                usernameTextField.text=@"demo";
                passwordTextField.text=@"demo123";
                servernameTextField.text=syncLoc.name;
                serverLocationTextField.text=syncLoc.url;
                
                [userDetailsDict setValue:@"demo" forKey:@"User_Name"];
                [userDetailsDict setValue:@"demo123" forKey:@"Password"];
                
                
                
                [userDetailsDict setValue:syncLoc.name forKey:@"Server_Name"];
                [userDetailsDict setValue:syncLoc.url forKey:@"Server_Url"];
   
                
            CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
            NSError * error;
            
            NSData *dataDict = [jsonSerializer serializeObject:selectedServerDictionary error:&error];
            NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
            [SWDefaults setSelectedServerDictionary:stringDataDict];
            
                [SWDefaults setActivatedServerDetailsDictionary:stringDataDict];
                
            
                [self startActivationProcess];
            }
            
        }
        
    }
    
    
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [SWDefaults UpdateGoogleAnalyticsforScreenName:kUserActivationScreenName];

    
    
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *selectedServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
    NSLog(@"selected server dict in act %@", selectedServerDict);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITextField Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==servernameTextField) {
       
        
        if (serverLocationsArray.count>0) {
            [self showServersPopover];
        }
        
        return NO;

    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if ([usernameTextField isFirstResponder]) {
        
        [usernameTextField resignFirstResponder];
        [passwordTextField becomeFirstResponder];
    }
    
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==usernameTextField) {
        [userDetailsDict setValue:textField.text forKey:@"User_Name"];
    }
   else if (textField==passwordTextField) {
        [userDetailsDict setValue:textField.text forKey:@"Password"];
    }
}

-(void)showServersPopover
{
    if (serverLocationsArray.count>0) {
        
        SyncLoctions * locations=[serverLocationsArray objectAtIndex:0];
        
        if ([NSString isEmpty:locations.name]==NO) {
            
            SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
            popOverVC.popOverContentArray=serverLocationsArray;
            
            popOverVC.popoverType=@"Activation";
            popOverVC.salesWorxPopOverControllerDelegate=self;
            popOverVC.disableSearch=YES;
            popOverVC.headerTitle=@"Servers";
            UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
            popoverController=nil;
            popoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
            popoverController.delegate=self;
            popOverVC.popOverController=popoverController;
            
            [popoverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
            
            [popoverController presentPopoverFromRect:servernameTextField.dropdownImageView.frame inView:servernameTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else
        {
            UIAlertView * noLocationsAlert=[[UIAlertView alloc]initWithTitle:@"No Locations Available" message:@"Please connect to internet and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [noLocationsAlert show];
        }
        
        
    }
    
    
    else
    {
        
        UIAlertView * noLocationsAlert=[[UIAlertView alloc]initWithTitle:@"No Locations Available" message:@"Please connect to internet and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noLocationsAlert show];
    }
    

}

#pragma mark Server Selection delegate methods

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
    
    syncLocations=[serverLocationsArray objectAtIndex:selectedIndexPath.row];
    NSLog(@"selected popover is %@",syncLocations.name );
    NSMutableDictionary* selectedServerDictionary=[[NSMutableDictionary alloc]init];
    
    [selectedServerDictionary setValue:syncLocations.name forKey:@"name"];
    [selectedServerDictionary setValue:syncLocations.url forKey:@"url"];
    [selectedServerDictionary setValue:syncLocations.seq forKey:@"seq"];
    
    servernameTextField.text=syncLocations.name;
    serverLocationTextField.text=syncLocations.url;
    [userDetailsDict setValue:syncLocations.name forKey:@"Server_Name"];
    [userDetailsDict setValue:syncLocations.url forKey:@"Server_Url"];
    
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError * error;

    NSData *dataDict = [jsonSerializer serializeObject:selectedServerDictionary error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
    [SWDefaults setSelectedServerDictionary:stringDataDict];
    
    [SWDefaults setActivatedServerDetailsDictionary:stringDataDict];

    NSLog(@"setting server dict %@", stringDataDict);
    
    
    
}


#pragma mark Fetch server urls




-(NSMutableArray*)fetchSyncLocationswithSyncRequest
{
    serverLocationsArray=[[NSMutableArray alloc]init];
    NSString* customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    NSString* syncUrlString=[NSString stringWithFormat:@"http://www.ucssolutions.com/licman/get-sync-locations.php?cid=%@&avid=%@",customerID,@"19"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:syncUrlString]];
    
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSURLResponse* response;
    NSError* error;
    
    NSData* dat=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    
    serverLocationsResponseArray=[NSJSONSerialization JSONObjectWithData:dat options:NSJSONReadingMutableContainers error:nil];
    
    
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSData *datas = [jsonSerializer serializeObject:serverLocationsResponseArray error:&error];
    NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
    [SWDefaults setServerArray:stringData];
    
    for (NSInteger i=0; i<serverLocationsResponseArray.count; i++) {
        NSMutableDictionary * currentDict=[serverLocationsResponseArray objectAtIndex:i];
        syncLocations=[SyncLoctions new];
        syncLocations.name=[currentDict valueForKey:@"name"];
        syncLocations.seq=[currentDict valueForKey:@"seq"];
        syncLocations.url=[currentDict valueForKey:@"url"];
        [serverLocationsArray addObject:syncLocations];
    }
    
    
    return serverLocationsArray;
    

}
-(void)fetchSyncLocations
{
    serverLocationsArray=[[NSMutableArray alloc]init];
    serverLocationsResponseArray=[[NSMutableArray alloc]init];
    
    NSString* customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    NSString* syncUrlString=[NSString stringWithFormat:@"http://www.ucssolutions.com/licman/get-sync-locations.php?cid=%@&avid=%@",customerID,@"19"];
    
    NSMutableURLRequest * syncUrlRequest=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:syncUrlString]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:syncUrlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
            {
            NSError* dataerror;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&dataerror];
                       if (!dataerror) {
                serverLocationsResponseArray=json.copy;
               CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
               NSData *datas = [jsonSerializer serializeObject:serverLocationsResponseArray error:&error];
               NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
               [SWDefaults setServerArray:stringData];
                           
                for (NSInteger i=0; i<serverLocationsResponseArray.count; i++) {
                NSMutableDictionary * currentDict=[serverLocationsResponseArray objectAtIndex:i];
                syncLocations=[SyncLoctions new];
                syncLocations.name=[currentDict valueForKey:@"name"];
                syncLocations.seq=[currentDict valueForKey:@"seq"];
                syncLocations.url=[currentDict valueForKey:@"url"];
                [serverLocationsArray addObject:syncLocations];
            }
                                              
            }
                
        else{
            NSLog(@"error is %@", dataerror.debugDescription);
        }
            
                                          
            }];
    [dataTask resume];
    
}
#pragma mark Activation button Methods

-(void)startActivationProcess
{
    
   // [self.navigationItem setHidesBackButton:YES animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

   syncProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];

    if (isActivationNewUserFromLogin==YES) {
        
        //this is activation new user again, upload existing users database
        
        activationManager=[[SalesWorxActivationManager alloc]init];
        
        UIDeviceHardware *hardware=[[UIDeviceHardware alloc] init];
        NSLog(@"Device is %@",[hardware platformString]);
        
    
        if ([activationManager licenseVerification] || [[hardware platformString] isEqualToString:@"Simulator"])
        {

            currentSyncStatus=@"";
            lastSyncStausString=@"";
            [SWDefaults setLastSyncType:@"Full Sync"];
            syncProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
            
            NSLog(@"%@",[SWDefaults customer]);
            NSLog(@"License Verified .. OK");
            
            
            [self writeSyncLogToTextFile:[NSString stringWithFormat:@"User ID : %@",[[SWDefaults userProfile] stringForKey:@"Username"]] WithTimeStamp:NO];
            [self writeSyncLogToTextFile:[NSString stringWithFormat:@"Customer ID : %@ \n",[SWDefaults licenseIDForInfo]] WithTimeStamp:NO];
            
            [self writeSyncLogToTextFile:@"Sync log:" WithTimeStamp:NO];
            
            
            [activationManager prepareRequestForSyncUpload];
            
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [self showAlertAfterHidingKeyBoard:KLIcenseValidationErrorAlerttitleStr andMessage:KLIcenseValidationErrorAlertMessageStr withController:nil];
            [self.view setUserInteractionEnabled:YES];
            
            
        }
        
        
    }
    else
    {
        
        
        [SWDefaults setUsername:usernameTextField.text];
        [SWDefaults setUsernameForActivate:usernameTextField.text];
        [SWDefaults setPassword:passwordTextField.text];
        [SWDefaults setPasswordForActivate:passwordTextField.text];
        
        NSLog(@"user details is %@ %@",[SWDefaults usernameForActivate],[SWDefaults passwordForActivate]);
        
        
        NSString* userName=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"User_Name"]];
        NSString* password=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"Password"]];
        NSString* serverName=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"Server_Name"]];
        NSString* serverUrl=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"Server_Url"]];
        
        NSString* missingString=[[NSString alloc]init];
        
        if ([NSString isEmpty:userName]==YES) {
            missingString=@"User Name";
        }
        else if ([NSString isEmpty:password]==YES) {
            missingString=@"password";
        }
        else  if ([NSString isEmpty:serverName]==YES) {
            missingString=@"Server Name";
        }
        else if ([NSString isEmpty:serverUrl]==YES) {
            missingString= @"Server Url";
        }
        
        if ([NSString isEmpty:missingString]==NO) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertView * missingAlert=[[UIAlertView alloc]initWithTitle:@"Missing Data" message:[NSString stringWithFormat:@"Please enter %@",missingString] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [missingAlert show];
            
        }
        else
        {
            //[self.view setUserInteractionEnabled:NO];
            activationManager=[[SalesWorxActivationManager alloc]init];
            
            UIDeviceHardware *hardware=[[UIDeviceHardware alloc] init];
            NSLog(@"Device is %@",[hardware platformString]);
            
            
            if ([activationManager licenseVerification] || [[hardware platformString] isEqualToString:@"Simulator"])
            {
                
                currentSyncStatus=@"";
                lastSyncStausString=@"";
                [SWDefaults setLastSyncType:@"Full Sync"];
                activationProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
                
                NSLog(@"%@",[SWDefaults customer]);
                NSLog(@"License Verified .. OK");
                [self writeSyncLogToTextFile:[NSString stringWithFormat:@"User ID : %@",[[SWDefaults userProfile] stringForKey:@"Username"]] WithTimeStamp:NO];
                [self writeSyncLogToTextFile:[NSString stringWithFormat:@"Customer ID : %@ \n",[SWDefaults licenseIDForInfo]] WithTimeStamp:NO];
                
                [self writeSyncLogToTextFile:@"Sync log:" WithTimeStamp:NO];
                
                [activationManager sendRequestforActivate:userDetailsDict];
                
                //[syncManager prepareRequestForSyncUpload];
                
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                [self showAlertAfterHidingKeyBoard:KLIcenseValidationErrorAlerttitleStr andMessage:KLIcenseValidationErrorAlertMessageStr withController:nil];

                [self.view setUserInteractionEnabled:YES];
                
            }
            
        }
    }

}

-(IBAction)activateNewUserTapped:(id)sender
{
    [self startActivationProcess];
}


-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:KAlertOkButtonTitle
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}


-(void)writeSyncLogToTextFile:(NSString *)textString WithTimeStamp:(BOOL)isTimeStampRequired
{
    if([lastSyncStausString isEqualToString:textString])
    {
        
    }
    else
    {
        lastSyncStausString=textString;
        //get the documents directory:
        NSString *slashN = @"\n";
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *dateString =  [formatter stringFromDate:[NSDate date]];
        if(isTimeStampRequired)
        {
            slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@: %@",dateString , textString]];
            
        }
        else
        {
            slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ " , textString]];
            
        }
        currentSyncStatus=[currentSyncStatus stringByAppendingString:slashN];
        
        //iOS 8 support
        NSString *documentsDirectory;
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [NSString stringWithFormat:@"%@/Sync_Log.txt",documentsDirectory];
        
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
        [fileHandler seekToEndOfFile];
        [fileHandler writeData:[slashN dataUsingEncoding:NSUTF8StringEncoding]];
        [fileHandler closeFile];
        formatter=nil;
        usLocale=nil;
    }
    
}


- (void)RecieveSyncStatusUpdate:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:KNewActivationNotificationNameStr])
    {
        NSDictionary *statusDic = (NSDictionary *)notification.object;
        //[self showSyncProgress];
        
        
        if([[statusDic valueForKey:KSyncDic_ShowAlertStr]isEqualToString:KSync_YesCode])
        {
            [self showAlertAfterHidingKeyBoard:KSync_ErrorAlertTitleStr andMessage:[statusDic valueForKey:KSync_AlertMessageStr] withController:nil];
            //[self updateLastSyncDetails:NO];
            syncStatusLbl.text = [KSync_SyncStausStr stringByAppendingString:NSLocalizedString(@"Failed", nil)];
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSync_AlertMessageStr] WithTimeStamp:YES];
            [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Failed"] WithTimeStamp:YES];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        else
        {
            syncStatusLbl.text=[statusDic valueForKey:KSyncDic_SyncStausStr];
            syncProgressBar.progress=[[statusDic valueForKey:KSyncDic_SyncProgressStr] floatValue];
            NSLog(@"sync progress is %@",[statusDic valueForKey:@"SyncProgress"]);
            
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSyncDic_SyncStausStr] WithTimeStamp:YES];
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_DatabaseUploadedStatusStr])
            {
                [activationManager sendRequestForInitiateDownloadDatabase];
            }
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_CompleteStatusStr])
            {
               // [self updateLastSyncDetails:YES];
                // [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Successful"] WithTimeStamp:YES];
                NSLog(@"Activation completed successfully");
                [[NSNotificationCenter defaultCenter] removeObserver:self name:KNewActivationNotificationNameStr object:nil];
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [SWDefaults setLastSyncStatus:KNotApplicable];
                [SWDefaults setLastSyncOrderSent:KNotApplicable];
                [SWDefaults setLastSyncType:KNotApplicable];
                [SWDefaults setLastSyncDate:@"Never"];
                [SWDefaults setLastFullSyncDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];

                //set login view controller
                
                //re register for push notification with user name
                
                SWAppDelegate * appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
                
                [appDel registerforPushNotificationwithDeviceToken:[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceID"]];
                
                
                if (isFromLogin==YES) {
                    
                   UIAlertView * ActivatedSuccessAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"User Activated Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    ActivatedSuccessAlert.tag=201;
                    [ActivatedSuccessAlert show];
                    
                    
                }
                else
                {
                    
                    UIAlertView * activationSuccessAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"User Activated Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    activationSuccessAlert.tag=202;
                    [activationSuccessAlert show];
                    
                   
                    

                }
                
                
              

            }
            
        }
    }
}


#pragma mark Upload database notification method

- (void)RecieveUploadDatabaseNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kUploadDatabaseExistingUserNotificationName])
    {
        NSDictionary *statusDic = (NSDictionary *)notification.object;
        //[self showSyncProgress];
        //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        
        if([[statusDic valueForKey:KSyncDic_ShowAlertStr]isEqualToString:KSync_YesCode])
        {
            [self showAlertAfterHidingKeyBoard:KSync_ErrorAlertTitleStr andMessage:[statusDic valueForKey:KSync_AlertMessageStr] withController:nil];
            //[self updateLastSyncDetails:NO];
            syncStatusLbl.text = [KSync_SyncStausStr stringByAppendingString:NSLocalizedString(@"Failed", nil)];
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSync_AlertMessageStr] WithTimeStamp:YES];
            [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Failed"] WithTimeStamp:YES];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        else
        {
            syncStatusLbl.text=[statusDic valueForKey:KSyncDic_SyncStausStr];
            syncProgressBar.progress=[[statusDic valueForKey:KSyncDic_SyncProgressStr] floatValue];
            NSLog (@"Successfully received the NewAcatibvationStatusNotification!");
            NSLog(@"sync progress is %@",statusDic);
            
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSyncDic_SyncStausStr] WithTimeStamp:YES];
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_DatabaseUploadedStatusStr])
            {
               // [activationManager sendre];
            }
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KAllFilesUploadedStr]
               ||[[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString: KNoFilesToUploadedStr])
            {
                
                NSLog(@"upload database successful");
                [self.navigationItem setHidesBackButton:YES];
                self.navigationItem.leftBarButtonItems=nil;
                usernameTextField.text=@"";
                passwordTextField.text=@"";
                servernameTextField.text=@"";
                serverLocationTextField.text=@"";
                [activateButton setTitle:@"Activate" forState:UIControlStateNormal];
                syncProgressBar.progress=0.0;
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                //delete previous users database
                
                NSString * databasePath=[SWDefaults dbGetDatabasePath];
                
                NSString* databaseZipFilePath=[SWDefaults dbGetDatabaseZipFilePath];
                
                NSError * error;
                
                NSError * databaseError;
                
                BOOL databaseDeleted=NO;
                
                BOOL databaseZipFileDeleted=[[NSFileManager defaultManager]removeItemAtPath:databaseZipFilePath error:&error];
                if (databaseZipFileDeleted==YES) {
                    
                    NSLog(@"database zip file deleted");
                    
                    databaseDeleted=[[NSFileManager defaultManager]removeItemAtPath:databasePath error:&databaseError];
                    
                    if (databaseDeleted==YES) {
                        
                        NSLog(@"database file deleted");
                    }
                    
                }

                
                
                
                
                syncStatusLbl.text=@"";
                
                if (!databaseUploadSuccessAlert) {
                               databaseUploadSuccessAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Database Uploaded Successfully, please enter credentials to activate new user" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                databaseUploadSuccessAlert.tag=999;
                [databaseUploadSuccessAlert show];
                    [self updateFullSyncMandatoryFlag];

                }
                
                
            }
            
        }
    }
}
-(void)updateFullSyncMandatoryFlag
{
    if(appdel.isFullSyncMandatory)
    {
        appdel.isFullSyncMandatory=NO;
        [SWDefaults setVersionNumber:[[[SWDefaults alloc]init] getAppVersionWithBuild]];
    }
}



-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {

    if (alertView.tag==999) {
        databaseUploadSuccessAlert=nil;
    }
    //do whatever with the result
  else  if (alertView.tag==202) {
        LoginViewController *loginVC = [[LoginViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
        [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
        
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==999) {
        isActivationNewUserFromLogin=NO;
        
        
        [usernameTextField setIsReadOnly:NO];
        [passwordTextField setIsReadOnly:NO];
        [servernameTextField setIsReadOnly:NO];
        [serverLocationTextField setIsReadOnly:NO];
        

        
    }
    else if (alertView.tag==201)
    {
        [self.navigationController popViewControllerAnimated:YES];

    }
}

@end
