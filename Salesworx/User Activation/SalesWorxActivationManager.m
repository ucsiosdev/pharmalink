//
//  SalesWorxActivationManager.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/19/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxActivationManager.h"

@implementation SalesWorxActivationManager

#pragma License Verification

-(BOOL)licenseVerification
{
    BOOL isValid = YES;
    
    //NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    //avid  = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSLog(@"license key is %@",[SWDefaults licenseKey]);
    
    NSLog(@"License customer id is %@",[SWDefaults licenseCustomerID]);
    
    NSLog(@"device id is %@",[[DataSyncManager sharedManager]getDeviceID]);
    
    
    if(![[[SWDefaults licenseKey] stringForKey:@"avid"] isEqualToString:@"19"])
    {
        NSLog(@"avid doesnt match %@",[SWDefaults getValidStringValue:[[SWDefaults licenseKey] stringForKey:@"avid"]]);
        isValid=NO;
    }
    
    if(![[[SWDefaults licenseKey] stringForKey:@"cid"] isEqualToString:[SWDefaults licenseCustomerID]])
    {
        NSLog(@"customer id  doesnt match %@ %@",[SWDefaults getValidStringValue:[[SWDefaults licenseKey] stringForKey:@"cid"]],[SWDefaults getValidStringValue:[SWDefaults licenseCustomerID]] );
        
        isValid=NO;
    }
    
    if(![[[SWDefaults licenseKey] stringForKey:@"sid"] isEqualToString:[[DataSyncManager sharedManager]getDeviceID]])
    {
        NSLog(@"device id doesnt match %@ %@",[SWDefaults getValidStringValue:[[SWDefaults licenseKey] stringForKey:@"sid"]],[SWDefaults getValidStringValue:[[DataSyncManager sharedManager]getDeviceID]]);
        
        isValid=NO;
    }
    
    if([[[SWDefaults licenseKey] stringForKey:@"lt"] isEqualToString:@"EVAL_TIME"])
    {
        NSString *lDateString= [[SWDefaults licenseKey] stringForKey:@"date"];
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSDate *lDate = [formatter dateFromString:lDateString];
        int lLimit = [[[SWDefaults licenseKey] stringForKey:@"ll"] intValue];
        lDate = [lDate dateByAddingTimeInterval:60*60*24*lLimit];
        NSDate *now =  [self   getServerDate];
        //sample date
        
        NSLog(@"Todays's date %@ and license date is %@",now,lDate);
        
        
        NSComparisonResult result = [now compare:lDate];
        switch (result)
        {
            case NSOrderedAscending:
                NSLog(@"Future Date");
                break;
            case NSOrderedDescending:
                NSLog(@"Earlier Date");
                isValid=NO;
                break;
            case NSOrderedSame:
                NSLog(@"Today/Null Date Passed");
                break;
            default:
                NSLog(@"Error Comparing Dates");
                break;
        }
        formatter=nil;
        usLocale=nil;
    }
    
    
    // return isValid;
    return YES;
}
- (NSDate *)getServerDate
{
    //http://www.ucssolutions.com/licman/get-time.jsp
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.ucssolutions.com/licman/get-time.jsp"]];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *json_string2 = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSString *finalString = [self stringBetweenString:@"<DATE>" andString:@"</DATE>" andMainString:json_string2];
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSDate *lDate = [formatter dateFromString:finalString];
    formatter=nil;
    usLocale=nil;
    return lDate;
}

-(NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end andMainString:(NSString *)mainString
{
    NSScanner* scanner = [NSScanner scannerWithString:mainString];
    [scanner setCharactersToBeSkipped:nil];
    [scanner scanUpToString:start intoString:NULL];
    if ([scanner scanString:start intoString:NULL])
    {
        NSString* result = nil;
        if ([scanner scanUpToString:end intoString:&result])
        {
            return result;
        }
    }
    return nil;
}

#pragma mark upload database methods



#pragma mark Activate Method

-(void)sendRequestforActivate:(id)parameters
{
    NSString* currentDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    
    NSMutableDictionary * paramDict=parameters;
    
    NSString* userName=[SWDefaults getValidStringValue:[paramDict valueForKey:@"User_Name"]];
    NSString* password=[[ DataSyncManager sharedManager]sha1:[SWDefaults getValidStringValue:[paramDict valueForKey:@"Password"]]];
    NSString*serverName=[SWDefaults getValidStringValue:[paramDict valueForKey:@"Server_Name"]];
    NSString* serverLocation=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"http://%@/SWX/Sync/Activate",[paramDict valueForKey:@"Server_Url"]]];
    
    
    serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[paramDict valueForKey:@"Server_Url"]];
    
    NSLog(@"Server Api is %@", serverAPI);
    
    
    [[NSUserDefaults standardUserDefaults] setValue:serverAPI forKey:@"ServerAPILink"];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            userName, @"Username",
                            password, @"Password",
                            currentDeviceID, @"DeviceID",
                            [self getAppVersionWithBuild], @"ClientVersion",
                            @"Activation", @"SyncType",
                            serverName, @"SyncLocation",
                            nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverLocation]];
    
    
    NSLog(@"actication request with parameters %@ %@", request,params);
    
    NSLog(@"server name is %@",serverLocation);
    
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
    // Checking the format
    NSString *urlString =  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSData *requestBodyData = [urlString dataUsingEncoding:NSUTF8StringEncoding];
    
    request.HTTPBody = requestBodyData;
    

    
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        NSError* error;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        

        
        if([data length] >0 && [httpResponse statusCode] == 200) /*sucess*/
        {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&error];
            
            
            syncStatusResponse=[json mutableCopy];
            
            [self sendRequestForInitiateDownloadDatabase];
            
            
            NSLog(@"send request for activate response is %@",json);
        }
        else
        {
            NSLog(@"error with status code during activation %@ %ld",connectionError,(long)[httpResponse statusCode]);
            
            NSLog(@"json data %@", data);
            
            if(data.length>0)
            {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                       error:&error];
                NSLog(@"error activation JSON %@",json);
                
                

            }
            
            
            NSString* errorResponseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

            NSLog(@"response string %@", errorResponseString);
            
            
            if (![NSString isEmpty:errorResponseString]) {
               
                [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                 [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:errorResponseString]];
            }
            else{
                NSLog(@"status code description is %@", [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);
                [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                 [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%ld)",KSync_URLRequestFailureAlertMessageStr,connectionError.localizedDescription,(long)httpResponse.statusCode]]];
            }
        }
        
    }];
    
    
    
    
//    NSURLSession *session = [NSURLSession sharedSession];
//    //populate json
//    NSError *jsonError;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&jsonError];
//    //populate the json data in the setHTTPBody:jsonData
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://ucstestapps.ddns.net:10008/Activate"]];
//    [request setHTTPMethod:@"POST"];
//    [request setHTTPBody:jsonData];
//    //Send data with the request that contains the json data
//    
//    
//    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        // Do your stuff...
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//
//        if([data length] >0 && [httpResponse statusCode] == 200) /*sucess*/
//        {
//            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
//                                                                 options:kNilOptions
//                                                                   error:&error];
//            NSLog(@"send request for activate response is %@",json);
//        }
//        else
//        {
//            NSLog(@"error is %d",httpResponse.statusCode);
//            
//        }
//    }] resume];
//    
    
    
    
    
    
}


#pragma UploadDatabase
#pragma SyncInstantiate

- (void)sendRequestForInitiateUploadDatabaseSync
{
    NSString *strurl =[serverAPI stringByAppendingString:@"Initiate"];
    
    NSURL *url = [NSURL URLWithString:strurl];
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    AFHTTPClient * request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [syncStatusResponse objectForKey:@"SyncReferenceNo"], @"SyncReferenceNo",
                            nil];
    
    [request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         syncStatusResponse = [responseString JSONValue];
         
         NSString *strSyncNO=[syncStatusResponse objectForKey:@"SyncReferenceNo"];
         if(!([strSyncNO length]==0))
         {
             NSLog(@"send request for complete called from sync refno send request for initiate");
             [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
              [self prepareStausDictionary:KSync_DatabaseDownloadInitiationStatusStr Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
             [self sendRequestForUploadDatabseSyncStatus];
         }
         else
         {
             [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
              [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:KSync_NoSyncrefrencenoAlertMessageStr]];
         }
         
     }
              failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                  NSLog(@"completion request with error %@ %@", operationQ.responseString,error.debugDescription);
                  [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                   [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
              }];
}




#pragma SyncStatus

- (void)sendRequestForUploadDatabseSyncStatus
{
    

        NSString *strUrl =[serverAPI stringByAppendingFormat:@"status/%@",[syncStatusResponse objectForKey:@"SyncReferenceNo"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
        AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
        [request getPath:nil parameters:nil success:^(AFHTTPRequestOperation *operationQ, id responseObject)
         {
             NSString *responseString = operationQ.responseString;
             syncStatusResponse = [responseString JSONValue];
             if([[syncStatusResponse objectForKey:@"SyncStatus"] isEqualToString:@"D"])
             {
                 
                 NSMutableArray *syncImagesObjectsArray=[SWDefaults getSyncImagesObjects];
                 if(syncImagesObjectsArray.count>0)
                 {
                     [self SendRequestForUploadFiles:syncImagesObjectsArray];
                 }
                 else
                 {
                     [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                      [self prepareStausDictionary:KNoFilesToUploadedStr Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
                 }
             }
             else if([[syncStatusResponse objectForKey:@"SyncStatus"] isEqualToString:@"F"])
             {
                 // [self finalLogPrint:[appData.statusDict stringForKey:@"ProcessResponse"] ];
                 [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                  [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[syncStatusResponse stringForKey:@"ProcessResponse"]]];
             }
             else
             {
                 [self performSelector:@selector(sendRequestForUploadDatabseSyncStatus) withObject:nil afterDelay:1.0];
             }
             //  }
             
         } failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
             NSLog(@"completion request with error %@ %@", operationQ.responseString,error.debugDescription);
             [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
              [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
         }];
        
        
        if(![KStatusSynString isEqualToString:[syncStatusResponse stringForKey:@"SyncStatus"]])
        {
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"N"])
            {
                
                [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                 [self prepareStausDictionary:KSync_PendingStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"C"])
            {
                
                [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                 [self prepareStausDictionary:KSync_CopyingStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"S"])
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                 [self prepareStausDictionary:KSync_SynchronizingStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
             if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"P"])
            {
                
                //   [self finalLogPrint:@"Sync Status : Preparing output"];
                [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                 [self prepareStausDictionary:KSync_PreparingoutputStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
                
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqual:@"1"])
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
                 [self prepareStausDictionary:@"Linking with application" Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
        }
        if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqual:@"1"])
        {
            //  [DataSyncManager sharedManager].progressHUD.labelText = @"PLease wait...";
            //  [DataSyncManager sharedManager].progressHUD.detailsLabelText = @"Linking with application";
            [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
             [self prepareStausDictionary:@"Linking with application" Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
        }
        
}
- (void)prepareRequestForSyncUpload
{
    NSLog(@"send request for sync upload called");
    [ZipManager convertSqliteToNSDATA];
    NSLog(@"connecting from full sync");
    NSString *strDatabaseInfo= [[DataSyncManager sharedManager] dbGetZipDatabasePath];
    if([strDatabaseInfo isEqualToString:@"Database does not exist"])
    {
        [[DataSyncManager sharedManager]UserAlert:@"Please activate the process first"];
    }
    else
    {
        [self sendRequestForUploadData];
    }
}
- (void)sendRequestForUploadData
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]];
    
    serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadDataGZ"];
    NSString *path = [[DataSyncManager sharedManager] dbGetZipDatabasePath];
    //NSLog(@"File Path %@",path);
    
    
    NSLog(@"uploading database for url is %@", strUrl);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [[SWDefaults userProfile] stringForKey:@"Username"], @"Username",
                            [[SWDefaults userProfile] stringForKey:@"Password"], @"Password",
                            strDeviceID, @"DeviceID",
                            [self getAppVersionWithBuild], @"ClientVersion",
                            @"Full Sync", @"SyncType",
                            [testServerDict stringForKey:@"name"], @"SyncLocation",
                            nil];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [request multipartFormRequestWithMethod:@"POST"
                                                                        path:nil
                                                                  parameters:params
                                                   constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:@"swx"
                                                                  fileName:@"swx"
                                                                  mimeType:@"application/x-gzip"];
                                      }
                                      ];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         
         float upLoadProgress = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
         [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
          [self prepareStausDictionary:KSync_UploadingDatabaseStatusStr Progress:upLoadProgress ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
         
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         
         
         NSLog(@"success with response string %@", operationQ.responseString);
         syncStatusResponse = [operationQ.responseString JSONValue];
         NSString *strSyncNO=[syncStatusResponse objectForKey:@"SyncReferenceNo"];
         
         if(!([strSyncNO length]==0))
         {
             NSLog(@"Databse Uploaded Successfully");
             [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
              [self prepareStausDictionary:KSync_DatabaseUploadedStatusStr Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
             
             
             [self sendRequestForInitiateUploadDatabaseSync];
             
         }
         else
         {
             [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
              [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:KSync_NoSyncrefrencenoAlertMessageStr]];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"completion request with error %@ %@", operationQ.responseString,error.debugDescription);
         [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
          [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
     }];
    
    [operation start];
    
}
- (void)SendRequestForUploadFiles:(NSMutableArray *)syncImageObjectsArray
{
    SyncImage *syncImageObj=[[syncImageObjectsArray objectAtIndex:0] copy];
    NSString *uploadStatustype;
    if([syncImageObj.imageSyncType isEqualToString:KDistributionImagesFileType])
    {
        uploadStatustype=KSync_UploadingDistributionImagesStatusStr;
    }
    if([syncImageObj.imageSyncType isEqualToString:KLPOImageSyncFileType])
    {
        uploadStatustype=KSync_UploadingLPOImagesStatusStr;
    }
    else if([syncImageObj.imageSyncType isEqualToString:KCollectionImageSyncFileType])
    {
        uploadStatustype=KSync_UploadingCollectionImagesStatusStr;
    }
    else if([syncImageObj.imageSyncType isEqualToString:KSignatureSyncFileType])
    {
        uploadStatustype=KSync_UploadingFilesStatusStr;
    }
    else
    {
        uploadStatustype=KSync_UploadingFilesStatusStr;
        
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:KNewSyncNotificationNameStr object:
     [self prepareStausDictionary:uploadStatustype Progress:0.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    
    
    //uploading signature here
    
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSString *path = syncImageObj.imagePath;
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [[SWDefaults userProfile] stringForKey:@"Username"], @"Username",
                            [[SWDefaults userProfile] stringForKey:@"Password"], @"Password",
                            strDeviceID, @"DeviceID",
                            [self getAppVersionWithBuild], @"ClientVersion",
                            syncImageObj.imageSyncType,@"FileType" ,
                            nil];
    
    
    NSLog(@"upload file parameters %@", [params description]);
    
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [request multipartFormRequestWithMethod:@"POST"
                                                                        path:nil
                                                                  parameters:params
                                                   constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:[path lastPathComponent]
                                                                  fileName:[path lastPathComponent]
                                                                  mimeType:syncImageObj.imageMIMEType];
                                      }
                                      ];
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         float downloadProgress = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
         [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
          [self prepareStausDictionary:uploadStatustype Progress:downloadProgress ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         NSMutableArray *tempSyncImageObjectsArray= [syncImageObjectsArray mutableCopy];
         [tempSyncImageObjectsArray removeObjectAtIndex:0];
         
         [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
          [self prepareStausDictionary:[NSString stringWithFormat:@"%@ uploaded successfully",[syncImageObj.imagePath lastPathComponent]] Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
         
         if(tempSyncImageObjectsArray.count>0)
         {
             [self SendRequestForUploadFiles:tempSyncImageObjectsArray];
         }
         else
         {
             [self deleteSyncedImages];
             [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
              [self prepareStausDictionary:KNoFilesToUploadedStr Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];

         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [[NSNotificationCenter defaultCenter]postNotificationName:kUploadDatabaseExistingUserNotificationName object:
          [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%ld)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,(long)error.code]]];
     }];
    
    [operation start];
    
    
}
-(void)deleteSyncedImages
{
    [SWDefaults deleteSyncImagesInaFolder:KSignatureImagesFolderName];
    [SWDefaults deleteSyncImagesInaFolder:KDCImagesFolderName];
    [SWDefaults deleteSyncImagesInaFolder:KLPOImagesFolderName];
    [SWDefaults deleteSyncImagesInaFolder:KCollectionImagesFolderName];
    [SWDefaults deleteSyncImagesInaFolder:kCollectionReceiptFolderName];
    
    
}



#pragma SyncInstantiate

- (void)sendRequestForInitiateDownloadDatabase
{
    NSString *strurl =[serverAPI stringByAppendingString:@"Initiate"];
    
    NSURL *url = [NSURL URLWithString:strurl];
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    AFHTTPClient * request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [syncStatusResponse objectForKey:@"SyncReferenceNo"], @"SyncReferenceNo",
                            nil];
    
    [request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         syncStatusResponse = [responseString JSONValue];
         
         NSString *strSyncNO=[syncStatusResponse objectForKey:@"SyncReferenceNo"];
         if(!([strSyncNO length]==0))
         {
             NSLog(@"send request for complete called from sync refno send request for initiate");
             [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
              [self prepareStausDictionary:KSync_DatabaseDownloadInitiationStatusStr Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
             [self sendRequestForStatus];
         }
         else
         {
             [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
              [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:KSync_NoSyncrefrencenoAlertMessageStr]];
         }
         
     }
              failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                  NSLog(@"completion request with error %@ %@", operationQ.responseString,error.debugDescription);
                  [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                   [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
              }];
}




#pragma SyncStatus

- (void)sendRequestForStatus
{
    
    if(isCompleted)
    {
        [self ProcessCompleted];
        //[DataSyncManager sharedManager].progressHUD.progress = [[appData.statusDict objectForKey:@"CurrentProgress"] floatValue];
    }
    else
    {
        NSString *strUrl =[serverAPI stringByAppendingFormat:@"status/%@",[syncStatusResponse objectForKey:@"SyncReferenceNo"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
        AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
        [request getPath:nil parameters:nil success:^(AFHTTPRequestOperation *operationQ, id responseObject)
         {
             NSString *responseString = operationQ.responseString;
             syncStatusResponse = [responseString JSONValue];
             //             if(single.isFromSync==1)
             //             {
             //                 if([[syncStatusResponse objectForKey:@"SyncStatus"] isEqualToString:@"D"])
             //                 {
             //                   //  [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
             //
             //                    // [self sendRequestForDownload];
             //                 }
             //                 else if([[syncStatusResponse objectForKey:@"SyncStatus"] isEqualToString:@"F"])
             //                 {
             //                     if (!single.isActivated)
             //                     {
             //                         //[self writeActivationLogToTextFile:[appData.statusDict stringForKey:@"ProcessResponse"]];
             //                         UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:[appData.statusDict stringForKey:@"ProcessResponse"] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
             //                         [ErrorAlert show];
             //#pragma clang diagnostic push
             //#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
             //                         [self.target performSelector:self.action withObject:@"error"];
             //#pragma clang diagnostic pop
             //                     }
             //                 }
             //                 else
             //                 {
             //
             //                     [self performSelector:@selector(sendRequestForStatus) withObject:nil afterDelay:1.0];
             //                     // [self sendRequestForStatus];
             //                 }
             //
             //             }
             //             else
             //             {
             if([[syncStatusResponse objectForKey:@"SyncStatus"] isEqualToString:@"D"])
             {
                 // [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
                 
                 NSLog(@"send request for download called 1020");
                 [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                  [self prepareStausDictionary:KSync_ReadyfordownloadStatusStr Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
                 [self sendRequestForDownload];
                 
                 
             }
             else if([[syncStatusResponse objectForKey:@"SyncStatus"] isEqualToString:@"F"])
             {
                 // [self finalLogPrint:[appData.statusDict stringForKey:@"ProcessResponse"] ];
                 [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                  [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[syncStatusResponse stringForKey:@"ProcessResponse"]]];
             }
             else
             {
                 [self performSelector:@selector(sendRequestForStatus) withObject:nil afterDelay:1.0];
             }
             //  }
             
         } failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
             NSLog(@"completion request with error %@ %@", operationQ.responseString,error.debugDescription);
             [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
              [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
         }];
        
        
        if(![KStatusSynString isEqualToString:[syncStatusResponse stringForKey:@"SyncStatus"]])
        {
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"N"])
            {
                // [self finalLogPrint:@"Sync Status : Pending"];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                 [self prepareStausDictionary:KSync_PendingStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"C"])
            {
                //                if(single.isFromSync==2)
                //                {
                //                    [self finalLogPrint:@"Database has been uploaded successfully"];
                //                }
                //  [self finalLogPrint:[NSString stringWithFormat:@"Sync Reference # %@",[appData.statusDict stringForKey:@"SyncReferenceNo"]]];
                // [self finalLogPrint:[NSString stringWithFormat:@"Byte Sent : %@ bytes",[appData.statusDict stringForKey:@"BytesReceived"]]];
                // [self finalLogPrint:@"Sync Status : Copying"];
                
                
                [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                 [self prepareStausDictionary:KSync_CopyingStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"S"])
            {
                // [self finalLogPrint:@"Sync Status : Synchronizing"];
                [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                 [self prepareStausDictionary:KSync_SynchronizingStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"D"])
            {
                // [self finalLogPrint:@"Sync Status : Downloaded"];
                //   [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[syncStatusResponse stringForKey:@"BytesSent"]]];
                
                
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"F"])
            {
                
                //    [self finalLogPrint:[appData.statusDict stringForKey:@"ProcessResponse"]];
                //   [self finalLogPrint:@"Sync Status : Failed"];
                
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqualToString:@"P"])
            {
                
                //   [self finalLogPrint:@"Sync Status : Preparing output"];
                [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                 [self prepareStausDictionary:KSync_PreparingoutputStatus Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
                
            }
            if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqual:@"1"])
            {
                //[DataSyncManager sharedManager].progressHUD.detailsLabelText = @"Linking with application";
                [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                 [self prepareStausDictionary:@"Linking with application" Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
            }
        }
        if([[syncStatusResponse stringForKey:@"SyncStatus"] isEqual:@"1"])
        {
            //  [DataSyncManager sharedManager].progressHUD.labelText = @"PLease wait...";
            //  [DataSyncManager sharedManager].progressHUD.detailsLabelText = @"Linking with application";
            [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
             [self prepareStausDictionary:@"Linking with application" Progress:[[syncStatusResponse objectForKey:@"CurrentProgress"] floatValue] ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
        }
        
    }
}



#pragma DownloadDatabase

- (void)sendRequestForDownload
{
    [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
     [self prepareStausDictionary:KSync_DownloadingDatabaseStatusStr Progress:0.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    
    // [self finalLogPrint:@"Sync Status : Downloading database..."];
    
    NSString *strUrl =[serverAPI stringByAppendingFormat:@"DownloadGZ/%@",[syncStatusResponse objectForKey:@"SyncReferenceNo"]];
    
    NSString *resourceDocPath;
    
    
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        resourceDocPath=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    }
    
    //NSString *pdfName =databaseName;
    NSString *pdfName = @"swx.sqlite.gz";
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
    
    
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:request123] ;
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation setDownloadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         float downloadProgress = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
         [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
          [self prepareStausDictionary:KSync_DownloadingDatabaseStatusStr Progress:downloadProgress ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject) {
        //[self finalLogPrint:@"Sync Status : Database Downloaded"];
        [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
         [self prepareStausDictionary:KSync_DatabaseDownloadedStatusStr Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
        [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
         [self prepareStausDictionary:[NSString stringWithFormat:@"Data received: %@ bytes",[syncStatusResponse stringForKey:@"BytesSent"]] Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
        // //NSLog(@"Upload response %@", appData.statusDict);
        [DataSyncManager sharedManager].progressHUD.progress = 1.0;
        [ZipManager convertNSDataToSQLite];
        
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        NSString *resourceDocPath;
        
        
        //iOS 8 support
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        
        else
        {
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        
        
        
        
        //            NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        NSString *pdfName = databaseName;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {

            NSString *temp =[[SWDatabaseManager retrieveManager] verifyLogin:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate]];
            
            
            
            if ([temp isEqualToString:@"Done"]) {
                
                [self sendRequestForExecWithUserName:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate] andProcName:@"sync_MC_GetLastDocumentReferenceAll" andProcParam:@"SalesRepID" andProcValue:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
            }
            
        }
        
       

        [self downloadMedia];
        
        //NSLog(@"Successfully downloaded file to %@", filePath);
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                                         NSLog(@"completion request with error %@ %@", operationQ.responseString,error.debugDescription);
                                         [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                                          [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
                                         
                                     }];
    
    [operation start];
    
    
}




#pragma SyncCompleteMethod
- (void)sendRequestForSyncComplete
{
    [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
     [self prepareStausDictionary:KSync_CompletingStatusStr Progress:0.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    NSString *strurl =[serverAPI stringByAppendingString:@"Complete"];
    
    NSLog(@"send request for complete url %@", strurl);
    
    NSURL *url = [NSURL URLWithString:strurl];
    
    // self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [syncStatusResponse objectForKey:@"SyncReferenceNo"], @"SyncReferenceNo",
                            nil];
    
    [request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         syncStatusResponse= [responseString JSONValue];
         
         NSLog(@"Upload response %@", syncStatusResponse);
         
         isCompleted=YES;
         uploadSignatureImageFileCount=0;
         [self sendRequestForStatus];
         
         
     }
              failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                  //NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
                  
                  NSLog(@"completion request with error %@ %@", operationQ.responseString,error.debugDescription);
                  [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
                   [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:operationQ.responseString]];
                  
              }];
    
    
}



#pragma DownloadMedia

-(void)downloadMedia {
    
    NSLog(@"download media called");
    
    
    
    //Delete Media Files for is_delted= Y
    NSMutableArray * arrayToDelete = [self fetchMediaFilesToDelete];
    for (MediaFile * mToDelete in arrayToDelete) {
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        // Get dir
        NSString *resourceDocPath;
        //iOS 8 support
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        NSString *fileName =mToDelete.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:fileName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            NSError *error = nil;
            if(![fileManager removeItemAtPath: filePath error:&error]) {
                NSLog(@"Delete failed:%@", error);
            } else {
                NSLog(@"image removed: %@", filePath);
                //Delete from Table
                NSString *updateMediaFile =[NSString stringWithFormat:@"DELETE FROM TBL_Media_Files WHERE Media_File_ID='%@'",mToDelete.Media_File_ID];
                BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
                if (status) {
                }
            }
        }
        else
        {
        }
    }
    
    //update the table for files which already exists, in case if media download process is going on and user tapped on full sync again.
    
    NSMutableArray * arrayOfMediaFiles=[[NSMutableArray alloc]init];
    
    NSMutableArray* arrayofDownloadedMediaFiles= [self fetchDownloadedMediaFiles];
    
    for (MediaFile * mToDownload in arrayofDownloadedMediaFiles) {
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Get dir
        NSString *resourceDocPath;
        
        //iOS 8 support
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        NSString *fileName =mToDownload.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:fileName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            
            //just update the db
            
            NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mToDownload.Media_File_ID];
            BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
            if (status) {
                
                NSLog(@"db updated for media file  without downloading %@", mToDownload.Media_File_ID);
                
            }
        }
        else
        {
            
        }
    }
    
    SWAppDelegate *app=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"is Background process running %hhd, is now sync %hhd",app.isBackgroundProcessRunning,app.isNewSync );
    
    
    [app startMediaDownloadProcess];
    
}

-(NSMutableArray *) fetchMediaFilesToDelete {
    
    
    NSMutableArray *mediaFilesToDeleteArray=[[NSMutableArray alloc]init];
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files WHERE Is_Deleted='Y'"]];
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [mediaFilesToDeleteArray addObject:customer];
    }
    return mediaFilesToDeleteArray;
}

-(NSMutableArray*)fetchDownloadedMediaFiles

{
    NSMutableArray* mediaFilesToDownloadArray=[[NSMutableArray alloc]init];
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files  where Is_Deleted ='N'  "]];
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [mediaFilesToDownloadArray addObject:customer];
    }
    return mediaFilesToDownloadArray;
}






- (void)ProcessCompleted
{
    
    
    /* its needed*/
    // [SWDefaults setLastSyncOrderSent:orderCountString];
    //    [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
    [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
     [self prepareStausDictionary:KSync_CompleteStatusStr Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    //  [self finalLogPrint:@"Sync Status : Completed"];
    
    
    
    //update the nsuser defauls for collection count as well because tbl collection will have data, even after full sync, it will show all the collections
    
    [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"collectionCount"];
    
    
    [SWDefaults setAppControl:[[SWDatabaseManager retrieveManager] dbGetAppControl]];
    [AppControl destroyMySingleton];
    

    
}



- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue
{
    
    [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
     [self prepareStausDictionary:@"Retrieving orders references number" Progress:0.2 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strValues=[[NSString alloc] init];
    NSString *strName=procName;
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",procPram]; //
    strValues=[strValues stringByAppendingFormat:@"&ProcValues=%@",procValue];
    NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData *dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,[self getAppVersionWithBuild],@"Get_LastDocumentReferenceAll",[testServerDict stringForKey:@"name"],strName,strProcedureParameter];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray*  resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSString *ProcResponse = [[resultArray objectAtIndex:0] stringForKey:@"ProcResponse"];
         
         if([procName isEqualToString:@"sync_MC_GetLastDocumentReferenceAll"])
         {
             resultArray = [responseText JSONValue];
             
             
             for (int i =0 ; i <[resultArray count]; i++)
             {
                     NSDictionary *data = [resultArray objectAtIndex:i];
                     
                     
                     NSLog(@"SYNC RESPONSE DATA, CHECK FOR DOC NUMBER FOR (I) FOR ADDING SAMPLES GIVEN IN MEDREP %@", [data description]);
                     
                     
                     if([[data stringForKey:@"Doc_Type"] isEqualToString:@"ISSUE_NOTE"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             
                             [MedRepDefaults setIssueNoteReference:[data stringForKey:@"Doc_Reference_No"]];
                             
                         }
                         else
                         {
                             [MedRepDefaults setIssueNoteReference:@"M000I0000000000"];
                             
                         }
                     }
                     
                     
                     
                     if([[data stringForKey:@"Doc_Type"] isEqualToString:@"COLLECTION"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastCollectionReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastCollectionReference:@"M000C0000000000"];
                         }
                     }
                     
                     else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"ORDER"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastOrderReference:@"M000S0000000000"];
                         }
                     }
                     else  if([[data stringForKey:@"Doc_Type"] isEqualToString:@"PROFORMA_ORDER"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastPerformaOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastPerformaOrderReference:@"M000D0000000000"];
                         }
                     }
                     else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"RMA"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastReturnOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastReturnOrderReference:@"M000R0000000000"];
                         }
                     }
                     
                 
             }

             [[NSNotificationCenter defaultCenter]postNotificationName:KNewActivationNotificationNameStr object:
              [self prepareStausDictionary:@"Retrieving orders references number" Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
             
             [self ProcessCompleted];
             
             
             
             
         }
         
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         
         [[NSNotificationCenter defaultCenter]postNotificationName:KSendOrdersNotificationNameStr object:
          [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"Process failed.\n %@ (%d)",error.localizedDescription,error.code]]];
         
     }];
    
    //call start on your request operation
    [operation start];
    
    
}







- (void)sendRequestForUplodOrdersSignatureFiles
{
    
    [[NSNotificationCenter defaultCenter]postNotificationName:KSendOrdersNotificationNameStr object:
     [self prepareStausDictionary:KSync_UploadingFilesStatusStr Progress:0.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    
    //uploading signature here
    
    NSArray *signatureImageArray= [[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSString *path = [signatureImageArray objectAtIndex:uploadSignatureImageFileCount];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [[SWDefaults userProfile] stringForKey:@"Username"], @"Username",
                            [[SWDefaults userProfile] stringForKey:@"Password"], @"Password",
                            strDeviceID, @"DeviceID",
                            [self getAppVersionWithBuild], @"ClientVersion",
                            FileType, @"FileType",
                            nil];
    
    
    NSLog(@"upload file parameters %@", [params description]);
    
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [request multipartFormRequestWithMethod:@"POST"
                                                                        path:nil
                                                                  parameters:params
                                                   constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:[path lastPathComponent]
                                                                  fileName:[path lastPathComponent]
                                                                  mimeType:@"image/jpeg"];
                                      }
                                      ];
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         float downloadProgress = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
         [[NSNotificationCenter defaultCenter]postNotificationName:KSendOrdersNotificationNameStr object:
          [self prepareStausDictionary:KSync_UploadingFilesStatusStr Progress:downloadProgress ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         ////NSLog(@"Upload response %@", appData.statusDict);
         
         NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
         NSString *strImageName =[signatureArray objectAtIndex:uploadSignatureImageFileCount];
         
         //  [self finalLogPrint:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]]];
         [[NSNotificationCenter defaultCenter]postNotificationName:KSendOrdersNotificationNameStr object:
          [self prepareStausDictionary:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]] Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
         
         if(uploadSignatureImageFileCount != [signatureArray count]-1)
         {
             uploadSignatureImageFileCount++;
             [self sendRequestForUplodOrdersSignatureFiles];
         }
         else
         {
             
             NSFileManager *fm = [NSFileManager defaultManager];
             NSString *directory;
             //iOS 8 support
             
             if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                 directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Signature/"];
             }
             
             else
             {
                 directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature/"];
             }
             
             NSError *error = nil;
             for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
             {@autoreleasepool {
                 BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
                 if (!success || error)
                 {
                     //NSLog(NSLocalizedString(@"Error", nil));
                 }
             }
             }
             
             [[NSNotificationCenter defaultCenter]postNotificationName:KSendOrdersNotificationNameStr object:
              [self prepareStausDictionary:KSendOrders_OrdersUploadSuccesAlertMessage Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [[NSNotificationCenter defaultCenter]postNotificationName:KSendOrdersNotificationNameStr object:
          [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
     }];
    
    [operation start];
    
    
}




-(NSString *)getAppVersionWithBuild
{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    NSLog(@"updating client version %@", [NSString stringWithFormat:@"%@(%@)", version,build]);
    
    NSString * AppVersion =[NSString stringWithFormat:@"%@(%@)", version,build];
    
    return AppVersion;
}


-(NSDictionary *)prepareStausDictionary:(NSString*)statusStr Progress:(float)Progress ShowAlert:(NSString *)ShowAlert WithTitle:(NSString *)Title Message:(NSString *)Message
{
    NSDictionary *statusDic=[[NSDictionary alloc]initWithObjectsAndKeys:statusStr,KSyncDic_SyncStausStr,[NSString stringWithFormat:@"%f",Progress],KSyncDic_SyncProgressStr,ShowAlert,KSyncDic_ShowAlertStr,Title,KSync_AlertTitleStr,Message,KSync_AlertMessageStr,nil];
    return statusDic;
}



#pragma mark deleting uploaded signature images

-(void)deleteSignatureImages
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory;
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Signature/"];
    }
    
    else
    {
        directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature/"];
    }
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
    {@autoreleasepool {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error)
        {
            //NSLog(NSLocalizedString(@"Error", nil));
        }
    }
    }
}






@end
