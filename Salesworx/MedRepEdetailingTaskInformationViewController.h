//
//  MedRepEdetailingTaskInformationViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/22/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol TaskInformationDelegate <NSObject>


-(void)taskDetailsInformation:(NSString*)taskInfo;

@end

@interface MedRepEdetailingTaskInformationViewController : UIViewController<UITextViewDelegate>

{
    id taskInformationDelegate;
}
@property (strong, nonatomic) IBOutlet UITextView *customTextView;

@property(nonatomic) id taskInformationDelegate;

@property(nonatomic) BOOL isVisitCompleted;

@property(nonatomic) BOOL isViewing;

@end
