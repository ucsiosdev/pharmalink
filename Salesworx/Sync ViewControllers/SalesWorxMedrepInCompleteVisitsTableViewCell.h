//
//  SalesWorxMedrepInCompleteVisitsTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "SalesWorxSingleLineLabel.h"
#import "MedRepTextView.h"
#import "SalesWorxDescriptionLabel1.h"
#import "MedRepElementTitleDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"
@interface SalesWorxMedrepInCompleteVisitsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MedRepElementTitleLabel *locationNameLabel;
@property (strong, nonatomic) IBOutlet MedRepElementTitleDescriptionLabel *dcotorOrContactNameLabel;
@property (strong, nonatomic) IBOutlet SalesWorxDescriptionLabel1 *visitDateLabel;
@property (strong, nonatomic) IBOutlet MedRepTextField *commentsTextField;
@property (strong, nonatomic) IBOutlet MedRepTextView *commentsTextView;
@property (strong, nonatomic) IBOutlet UIButton *VisitCommentsCopyToAllButton;
@property (strong, nonatomic) IBOutlet MedRepElementTitleDescriptionLabel *contactNameLbl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xCommentsTextViewTrailingMarginConstarint;

@end
