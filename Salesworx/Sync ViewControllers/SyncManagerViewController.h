//
//  SyncManagerViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 3/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//2

#import <Foundation/Foundation.h>
#import "SWDefaults.h"
#import "NSDictionary+Additions.h"
#import "DataSyncManager.h"
#import "ZipManager.h"
#import "Singleton.h"
#import "CJSONDeserializer.h"
#import "HttpClient.h"
#import "AFHTTPRequestOperation.h"
#import "SWDefaults.h"
#import "SBJsonParser.h"
#import "NSString+SBJSON.h"
#import "SWAppDelegate.h"
#import "DataType.h"
#import "FMDBHelper.h"
#import "SWDefaults.h"



#define KSyncDic_SyncStausStr @"SyncStatus"
#define KSyncDic_SyncProgressStr @"SyncProgress"
#define KSyncDic_ShowAlertStr @"ShowAlert"
#define KSync_AlertTitleStr @"AlertTitle"
#define KSync_AlertMessageStr @"AlertMessage"
#define KSync_YesCode @"Y"
#define KSync_NoCode @"N"
#define KSync_UploadingDatabaseStatusStr @"Uploading database..."
#define KSync_DatabaseUploadedStatusStr @"Database uploaded successfully"
#define KSync_DatabaseDownloadInitiationStatusStr @"Initiating sync"
#define KSync_DownloadingDatabaseStatusStr @"Downloading database..."
#define KSync_DatabaseDownloadedStatusStr @"Database downloaded successfully"
#define KSync_UploadingFilesStatusStr @"Uploading signature files"
#define KSync_UploadingMerchandisingFilesStatusStr @"Uploading merchandising files"

#define KSync_CompletingStatusStr @"Updating completion status"
#define KSync_CompleteStatusStr @"Sync completed successfully"
#define KSync_SyncStausStr @"Sync status: "
#define KSync_URLRequestFailureAlertMessageStr @"Please check server settings or Internet connection and try again."
#define KSync_NoSyncrefrencenoAlertMessageStr @"Please start the process again require Syncrefrenceno."
#define KSync_UploadingDistributionImagesStatusStr @"Uploading distribution images"


#define KSync_SyncFailureContactAdminAlertMessageStr @" Sync process failed. Please try again or contact your administrator for assistance "
#define KSync_ErrorAlertTitleStr @"Error"
#define KStatusSynString @"200"

#define KSync_PendingStatus @"Sync status: Pending"
#define KSync_CopyingStatus @"Sync status: Copying"
#define KSync_SynchronizingStatus @"Sync status: Synchronizing"
#define KSync_PreparingoutputStatus @"Sync status: Preparing output"
#define KSync_ReadyfordownloadStatusStr @"Sync status: Ready for download"

#define KSendOrders_StartingUploadStatus @"Starting order upload"
#define KSendOrders_TakeOrdersFirstAlertMessage @"There are no pending orders"
#define KSendOrders_OrdersSentSuccesAlertMessage @"Orders sent successfully"
#define KSendOrders_OrdersUploadSuccesAlertMessage @"Orders upload completed successfully"
#import "BackgroundDocumentsDownload.h"



#define kSQLdbGetConfirmOrderS @"Select * from TBL_Order"

#define kSQLdbGetConfirmOrderItems @"SELECT * FROM TBL_Order_Line_Items  ORDER BY Orig_Sys_Document_Ref ASC"

#define kSQLdbGetConfirmOrderLots @"SELECT * FROM TBL_Order_Lots  ORDER BY Orig_Sys_Document_Ref ASC"
#define kSQLdbGetUnConfirmOrderS @"Select * from TBL_Proforma_Order"

@interface SyncManagerViewController : NSObject
{
    NSString *serverAPI;
    NSString *strDeviceID;
    NSMutableDictionary *syncStatusResponse;
    NSString* syncReferenceNumberFromUploadDatabase;
    BOOL isCompleted;
    NSInteger uploadSignatureImageFileCount;
    NSInteger uploadDistributionImageFileCount;

    NSString *orderCountString;
    NSMutableArray *itemArray;
    NSMutableArray *lotArray;
    NSMutableArray *orderArray;
    
}
@property(strong,nonatomic) NSString * selectedServerDetailsString;

-(BOOL)licenseVerification;
- (void)prepareRequestForSyncUpload;
- (void)sendRequestForInitiateDownloadDatabase;

-(void)prepareRequestForSendOrders;
@end
