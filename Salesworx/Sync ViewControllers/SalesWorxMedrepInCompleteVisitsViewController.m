//
//  SalesWorxMedrepInCompleteVisitsViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxMedrepInCompleteVisitsViewController.h"
#import "MedRepQueries.h"
#import "SalesWorxMedrepInCompleteVisitsTableViewCell.h"
#import "SWDefaults.h"
@interface SalesWorxMedrepInCompleteVisitsViewController ()

@end

@implementation SalesWorxMedrepInCompleteVisitsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    inCompleteVisitsArray=[[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)BackButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)viewWillAppear:(BOOL)animated

{
    [self setHiddenAnimated:NO duration:1.25];
    [incompleteVisitsconatinerView setBackgroundColor:UIViewControllerBackGroundColor];
    incompleteVisitsconatinerView.layer.cornerRadius=5.0f;
    
    inCompleteVisitsArray=[MedRepQueries getInCompleteVisits];
    
    
    /** these below lines are copied from salesworxtableview class . if anychnages applied there in salesworxtableview update here also*/
    incompleteVisitsTableView.layer.borderColor = kUITableViewBorderColor.CGColor;
    incompleteVisitsTableView.layer.borderWidth = 1.0;
    incompleteVisitsTableView.layer.cornerRadius = 1.0;
    incompleteVisitsTableView.layer.masksToBounds=YES;
    incompleteVisitsTableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    incompleteVisitsTableView.separatorColor=kUITableViewSaperatorColor;
    incompleteVisitsTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);

}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = incompleteVisitsconatinerView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, commentsTableHeaderView.frame.size.width, commentsTableHeaderView.frame.size.height)];
    
    commentsTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:commentsTableHeaderView];
    return commentsTableHeaderView;
}   // custom view for he

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return inCompleteVisitsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"MedrepInCompleteVisitsTableViewCellIdentifier";
    SalesWorxMedrepInCompleteVisitsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxMedrepInCompleteVisitsTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDictionary *currentInComplteVisit=[inCompleteVisitsArray objectAtIndex:indexPath.row];
    cell.locationNameLabel.text=[[currentInComplteVisit valueForKey:@"Location_Name"] capitalizedString];
  //  cell.visitDateLabel.text=[currentInComplteVisit valueForKey:@"Visit_Date"];
    cell.visitDateLabel.text=[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:[currentInComplteVisit valueForKey:@"Visit_Date"]];

    cell.commentsTextView.text=[SWDefaults getValidStringValue:[currentInComplteVisit valueForKey:@"Comments"]];
    cell.commentsTextView.delegate=self;
    cell.commentsTextView.tag=indexPath.row;
    [cell.VisitCommentsCopyToAllButton addTarget:self action:@selector(CopyFirstVisitCommentsAll) forControlEvents:UIControlEventTouchUpInside];
    if(indexPath.row==0 && inCompleteVisitsArray.count>1){
        
        cell.xCommentsTextViewTrailingMarginConstarint.constant=42;
        [cell.VisitCommentsCopyToAllButton setHidden:NO];
    }else{
        cell.xCommentsTextViewTrailingMarginConstarint.constant=8;
        [cell.VisitCommentsCopyToAllButton setHidden:YES];
    }
    
    NSString* locationType=[SWDefaults getValidStringValue:[currentInComplteVisit valueForKey:@"Location_Type"]];
    
    if ([locationType isEqualToString:@"D"]) {
        cell.contactNameLbl.text=[SWDefaults getValidStringValue:[currentInComplteVisit valueForKey:@"Doctor_Name"]];
    }
    else{
        cell.contactNameLbl.text=[SWDefaults getValidStringValue:[currentInComplteVisit valueForKey:@"Contact_Name"]];
    }
    
    
    return cell;
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    textView.text=[textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    [[inCompleteVisitsArray objectAtIndex:textView.tag]setValue:textView.text forKey:@"Comments"];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return textView.text.length + (text.length - range.length) <= 500;
}
- (void)textViewDidChange:(UITextView *)textView{
    [[inCompleteVisitsArray objectAtIndex:textView.tag]setValue:textView.text forKey:@"Comments"];
}

-(void)CopyFirstVisitCommentsAll{
    if([[inCompleteVisitsArray objectAtIndex:0] valueForKey:@"Comments"]!=nil &&
       [[[[inCompleteVisitsArray objectAtIndex:0] valueForKey:@"Comments"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]!=0){
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [inCompleteVisitsArray setValue:[[inCompleteVisitsArray objectAtIndex:0]valueForKey:@"Comments"] forKey:@"Comments"];
                                        [incompleteVisitsTableView reloadData];
                                        
                                    }];
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Copy to all" andMessage:@"Would you like to update the same comments for all visits" andActions:[[NSMutableArray alloc] initWithObjects:yesAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:KAlertNoButtonTitle], nil] withController:self];
        
    }else{
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please enter comments" withController:self];
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
-(IBAction)DoneButtonTapped:(id)sender{
    
    BOOL isCommentsUpdatedForAllVisits=YES;
    
//    for (NSInteger i=0; i<inCompleteVisitsArray.count; i++) {
//        NSDictionary *inCompletVisit=[inCompleteVisitsArray objectAtIndex:i];
//        if([inCompletVisit valueForKey:@"Comments"]==nil ||
//           [[[inCompletVisit valueForKey:@"Comments"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
//            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please provide comments to all visits" withController:self];
//            [incompleteVisitsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//            isCommentsUpdatedForAllVisits=NO;
//            break;
//        }
//    }
    
    
    if(!isCommentsUpdatedForAllVisits){
        
    }else{
        NSDictionary *userInfo=[SWDefaults userProfile];
        NSString *nUserId = [userInfo stringForKey:@"User_ID"];

        FMDatabase *database = [self getDatabase];
        [database open];
        [database beginTransaction];
        BOOL InCompleteVisitItemSaved=YES;
        for (NSInteger i = 0; i < inCompleteVisitsArray.count; i++){
            NSDictionary *inCompletVisit=[inCompleteVisitsArray objectAtIndex:i];
            
            NSString *comments=[[SWDefaults getValidStringValue:[inCompletVisit valueForKey:@"Comments"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            comments=[[comments componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];

            /** valid comments entered OR Prevoius Date visit*/
            if((comments.length!=0) ||
               ![[inCompletVisit valueForKey:@"Visit_Date"] hasPrefix:[MedRepQueries fetchCurrentDateInDatabaseFormat]]){
                
                
                NSString *InCompleteItemQuery =@"UPDATE PHX_TBL_Planned_Visits SET Custom_Attribute_1=?, Last_Updated_At=?, Last_Updated_By=? WHERE Planned_Visit_ID=?";
                NSMutableArray *insertInCompleteVisitItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                                     comments.length==0?KNotApplicable:comments,
                                                                     [MedRepQueries fetchCurrentDateTimeinDatabaseFormat],
                                                                     [NSNumber numberWithInteger:nUserId.integerValue],
                                                                     [inCompletVisit valueForKey:@"Planned_Visit_ID"],
                                                                     nil];
                InCompleteVisitItemSaved= [database executeUpdate:InCompleteItemQuery withArgumentsInArray:insertInCompleteVisitItemParameters];
                if(!InCompleteVisitItemSaved){
                    InCompleteVisitItemSaved=NO;
                    break;
                }else{
                    InCompleteVisitItemSaved=YES;
                }
            }
        }
        if(InCompleteVisitItemSaved){
            [database commit];
            [database close];
            [self.navigationController popViewControllerAnimated:NO];
            [_medrepInCompleteVisitsViewControllerDelegate MedreRepInCompleteVisitsCommentsUpdated];
        }else{
            [database rollback];
            [SWDefaults showAlertAfterHidingKeyBoard:KDBErrorAlertTitleStr andMessage:@"Unable to save comments" withController:self];
        }
    }
    
}



- (FMDatabase *)getDatabase
{
    //iOS 8 support
    NSString *documentDir;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }else{
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    return db;
}
@end
