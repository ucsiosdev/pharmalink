//
//  SalesWorxSyncPopoverViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TYMProgressBarView.h"
#import "MedRepDefaults.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepTextField.h"
#import "SWDatabaseManager.h"
#import "UIViewController+MJPopupViewController.h"
#import "SyncManagerViewController.h"
#import <MessageUI/MessageUI.h>
#import "SalesWorxServersListViewController.h"
#import "SalesWorxMediaSyncViewController.h"
#import "MedRepDefaults.h"
#import "SyncManagerViewController.h"
#import "ConfirmOrderViewController.h"
#import "SalesWorxSyncUnconfirmedOrdersViewController.h"
#import "UIImage+animatedGIF.h"
#import "SWAppDelegate.h"
#import "SalesWorxMedrepInCompleteVisitsViewController.h"

@protocol SalesWorxSyncPopoverViewControllerDelegate <NSObject>
-(void)refreshPDARights;
@end

@interface SalesWorxSyncPopoverViewController : UIViewController<UIPopoverControllerDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,SalesWorxServersListViewControllerDelegate,SalesWorxSyncUnconfirmedOrdersViewControllerDelegate,SalesWorxMedrepInCompleteVisitsViewControllerDelegate>

{
    
    
    IBOutlet UIView *backgroundSyncView;
    IBOutlet UIImageView *backgroundSyncImageView;
    
    IBOutlet TYMProgressBarView *syncProgressBar;
    IBOutlet TYMProgressBarView *mediaSyncProgressBar;

    IBOutlet MedRepElementDescriptionLabel *syncStatusLbl;
    IBOutlet MedRepTextField *serverUrltextField;
    
    IBOutlet UILabel *ordersPendingLbl;
    
    IBOutlet UILabel *returnsPendingLbl;
    
    IBOutlet UILabel *collectionsPendingLbl;
    
    IBOutlet UITableView *syncLocationsTblView;
    id delegate;
    IBOutlet UISearchBar *syncLocationsSearchBar;
    
    IBOutlet UIView *syncView;
    IBOutlet UIView *SyncPopoverContentView;

    IBOutlet UILabel *selectServerLbl;
    IBOutlet UILabel *lastSyncOnLbl;
    IBOutlet UILabel *statusLbl;
    IBOutlet UILabel *syncTypeLbl;

    IBOutlet UIView *serverLocationsView;
    NSMutableArray * syncLocationsArray;
    
    UIPopoverController * popoverController;
    
   IBOutlet SalesWorxDefaultButton *doneButton;
    
    
    
    IBOutlet NSLayoutConstraint * SyncViewHeghtConstraint;
    
    
    IBOutlet UILabel *TotalMediaFilesCountLabel;
    IBOutlet UILabel *DownloadedMediaFilesCountLabel;
    IBOutlet UILabel *failedMediaFilesCountLabel;

    NSString *orderCountString;
    NSString *returnCountString;
    NSString *collectionCountString;
    NSMutableDictionary *syncStatusResponse;
    SyncManagerViewController *syncManager;
    
    NSString *currentSyncStatus;
    NSString *lastSyncStausString;
    
    BOOL isUserConfirmedTheUnconfirmOrders;
    AppControl *appControl;
    
    NSData * animatedSendOrdersGifData;
    
    IBOutlet UIImageView *fullSyncButtonImageView;
    IBOutlet UIImageView *SendOrdersButtonImageView;
    SWAppDelegate *appDel;
    BOOL isSqliteUpdated;
    
    IBOutlet NSLayoutConstraint *xFullSyncImageTrailingMarginConstraint;
    
    NSString *syncSelectedServerDetails;
    NSString *syncSelectedServerFromPopUp;
    
    BOOL IsInCompleteVisitsCommentsUpdated;
    
    NSTimer* bgSyncTimer;
    BOOL isBackgroundSyncAnimationRunning;
    
}
- (IBAction)doneButtontapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *headerView;
- (IBAction)fullSyncButtontapped:(id)sender;
- (IBAction)sendOrdersButtonTapped:(id)sender;
@property(nonatomic) id delegate;
- (IBAction)sendLogsTapped:(id)sender;
-(void)updateLastSyncDetails:(BOOL)SyncStatusFlag;
@property (strong,nonatomic) id <SalesWorxSyncPopoverViewControllerDelegate>syncPopdelegate;

@end
