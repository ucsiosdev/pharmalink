//
//  SalesWorxServersListViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/3/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CJSONDeserializer.h"
#import "SWDefaults.h"
#import "CJSONSerializer.h"
@protocol SalesWorxServersListViewControllerDelegate <NSObject>


-(void)updateSelectedServerDetails:(NSString*)selectedServer;
@end

@interface SalesWorxServersListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *syncLocationsArray;
    IBOutlet UIView *serverListContentView;
    IBOutlet UIView *headerView;
}
@property (strong,nonatomic) id <SalesWorxServersListViewControllerDelegate>salesWorxServersListViewControllerDelegate;
@end
