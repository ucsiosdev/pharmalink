//
//  SalesWorxCommunicationMessagesSyncManager.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/6/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWDatabaseManager.h"
#import "AFHTTPRequestOperation.h"
#import "XMLWriter.h"
#import "SWDefaults.h"
@interface SalesWorxCommunicationMessagesSyncManager : NSObject
{
    NSString *lastSynctimeStr;
    NSString *currentSyncStarttimeStr;
    BOOL isV2ReciepeintsDataRecieved;
}
-(void)sendMessagesToserver;
@end
