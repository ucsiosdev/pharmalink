//
//  SalesWorxSyncPopoverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/13/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSyncPopoverViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "MedRepCustomClass.h"
#import "SalesWorxPopOverViewController.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "SyncManagerViewController.h"
#import "AGPushNoteView.h"
#import "SalesWorxDeviceValidationClass.h"
#import "SalesWorxDeviceValidationClass.h"
#import "SalesWorxMedrepInCompleteVisitsViewController.h"
@interface SalesWorxSyncPopoverViewController ()

@end

@implementation SalesWorxSyncPopoverViewController
@synthesize headerView;
- (void)viewDidLoad {
    [super viewDidLoad];
    appDel=(SWAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    NSString *syncDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm" destFormat:kDateFormatWithTime scrString:[SWDefaults lastSyncDate]];
    
    if(syncDate.length==0)
    {
        lastSyncOnLbl.text=[SWDefaults lastSyncDate];
    }
    else
    {
        lastSyncOnLbl.text = syncDate;
    }
    IsInCompleteVisitsCommentsUpdated=NO;
    
    ordersPendingLbl.text=[[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnsPendingLbl.text=[[SWDatabaseManager retrieveManager] dbGetReturnCount];
    syncTypeLbl.text=NSLocalizedString([SWDefaults lastSyncType],nil);
    SyncPopoverContentView.layer.cornerRadius=5.0;
    [SyncPopoverContentView setBackgroundColor:UIViewControllerBackGroundColor];
    appControl=[AppControl retrieveSingleton];
    /* populating servers array*/
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *ServersArray = [NSMutableArray arrayWithArray:[djsonSerializer deserializeAsArray:data error:&error]  ];
    syncLocationsArray=ServersArray;
    
    syncStatusLbl.text=NSLocalizedString(@"Sync Status", nil);
    
    /** Getting selected server details*/
    
    NSData *SelectedSeverData = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
    

    selectServerLbl.text=NSLocalizedString(@"Select the server with which data is to be synchronized", nil);
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"syncLocation"];
    
    
    if (savedValue == nil){

   serverUrltextField.text=[SelectedServersDetailsjson valueForKey:@"name"]==nil?@"":[SelectedServersDetailsjson valueForKey:@"name"];
    }
    else{
        NSData *SelectedSeverData = [savedValue dataUsingEncoding:NSUTF8StringEncoding];
        
        id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
        serverUrltextField.text = [SelectedServersDetailsjson valueForKey:@"name"]==nil?@"":[SelectedServersDetailsjson valueForKey:@"name"];
    }
    // Do any additional setup after loading the view from its nib.
    
    
    /**Progress bar style*/
    
    syncProgressBar.barBorderWidth=2.0;
    syncProgressBar.barBorderColor=UIViewControllerBackGroundColor;
    syncProgressBar.barBackgroundColor=UIViewControllerBackGroundColor;
    syncProgressBar.barFillColor=[UIColor clearColor];
    syncProgressBar.barInnerBorderColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    
    
    syncProgressBar.usesRoundedCorners=YES;
    
    
    mediaSyncProgressBar.barBorderWidth=2.0;
    mediaSyncProgressBar.barBorderColor=UIViewControllerBackGroundColor;
    mediaSyncProgressBar.barBackgroundColor=UIViewControllerBackGroundColor;
    mediaSyncProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    mediaSyncProgressBar.barInnerBorderColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    
    
    mediaSyncProgressBar.usesRoundedCorners=YES;

  //  [self hideSyncProgress];
    
    
    
    
    /** Orders and returns count*/
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    NSInteger collectionCount=[[NSUserDefaults standardUserDefaults]integerForKey:@"collectionCount"];
    if (collectionCount) {
        collectionCountString=[NSString stringWithFormat:@"%ld",(long)collectionCount];
    }
    
    else
    {
        collectionCountString=@"0";
    }

    ordersPendingLbl.text=orderCountString;
    returnsPendingLbl.text=returnCountString;
    collectionsPendingLbl.text=collectionCountString;
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(RecieveSyncStatusUpdate:) name:KNewSyncNotificationNameStr object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(RecieveSendOrdersStatusUpdate:) name:KSendOrdersNotificationNameStr object:nil];

    
    statusLbl.text=NSLocalizedString([SWDefaults lastSyncStatus], nil);

    if([[SWDefaults lastSyncStatus] isEqualToString:@"Successful"])
    {
        [statusLbl setTextColor:[UIColor colorWithRed:0.0 green:(186.0/255.0) blue:(120.0/255.0) alpha:1.0]];
        

    }
    else if([[SWDefaults lastSyncStatus] isEqualToString:@"N/A"])
    {
        statusLbl.text=NSLocalizedString(@"Never", nil);
        lastSyncOnLbl.text=@"N/A";

    }
    else

    {
        [statusLbl setTextColor:[UIColor redColor]];
    }
    isUserConfirmedTheUnconfirmOrders=NO;
    
    animatedSendOrdersGifData=[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SYNCSendOrders" ofType:@"gif"]];
    
    /** setting the tap gestures*/
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendOrdersButtonTapped:)];
    tapRecognizer.numberOfTapsRequired=1;
    [SendOrdersButtonImageView addGestureRecognizer:tapRecognizer];
    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fullSyncButtontapped:)];
    tapRecognizer1.numberOfTapsRequired=1;
    [fullSyncButtonImageView addGestureRecognizer:tapRecognizer1];

    
    
    if (savedValue == nil){
        syncSelectedServerDetails=[SWDefaults selectedServerDictionary];
    }
    else{
        syncSelectedServerDetails = savedValue;
    }
    backgroundSyncView.hidden = YES;

    //appDel.currentExceutingDataUploadProcess = KDataUploadProcess_SendOrders;
    //adding notification to disable send orders/full sync if background sync is in progress
    // Post a notification to enableBackGroundSync
//    NSLog(@"posting background sync notification from sync popover");
//    // [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchEnableFlag];
//#ifdef DEBUG
//    [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchEnableFlag];
//#endif
//    [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];

    bgSyncTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkBackgroundActivity) userInfo:nil repeats:YES];
    
    
//KDataUploadProcess_FullSync

}

-(void)checkBackgroundActivity{
    if(![appDel.currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_None] && ![appDel.currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_FullSync] && ![appDel.currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_SendOrders])
//    if([appDel.currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_VTRXBS])
    {
        NSLog(@"starting bg sync animation");
        backgroundSyncView.hidden = NO;
        if (!isBackgroundSyncAnimationRunning) {
            [self StartBackgroundSyncAnimation];
        }
    }else{
        backgroundSyncView.hidden = YES;
        [self StopBackgroundSyncAnimation];
    }
}

- (void)RecieveSyncStatusUpdate:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:KNewSyncNotificationNameStr])
    {
        NSDictionary *statusDic = (NSDictionary *)notification.object;
        //[self showSyncProgress];
        
        
        if([[statusDic valueForKey:KSyncDic_ShowAlertStr]isEqualToString:KSync_YesCode])
        {
            [self StopFullSyncAnimation];
            syncManager=nil;

            [self showAlertAfterHidingKeyBoard:KSync_ErrorAlertTitleStr andMessage:[statusDic valueForKey:KSync_AlertMessageStr] withController:nil];
            [self updateLastSyncDetails:NO];
            syncStatusLbl.text = [KSync_SyncStausStr stringByAppendingString:@"Failed"];
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSync_AlertMessageStr] WithTimeStamp:YES];
            [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Failed"] WithTimeStamp:YES];
        }
        else
        {
            IsInCompleteVisitsCommentsUpdated=NO;
            syncStatusLbl.text=[statusDic valueForKey:KSyncDic_SyncStausStr];
            syncProgressBar.progress=[[statusDic valueForKey:KSyncDic_SyncProgressStr] floatValue];
            //NSLog (@"Successfully received the NewSyncStatusNotification!");
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSyncDic_SyncStausStr] WithTimeStamp:YES];
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_DatabaseUploadedStatusStr])
            {
                NSLog(@"initiating download database with status dict %@",statusDic);
                if (syncManager) {
                    [syncManager sendRequestForInitiateDownloadDatabase];
                    syncManager=nil;
                }
            }
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_CompleteStatusStr])
            {
                [self StopFullSyncAnimation];
                [self updateFullSyncMandatoryFlag];
                [SWDefaults setAppControl:[[SWDatabaseManager retrieveManager] dbGetAppControl]];
                [AppControl destroyMySingleton];
                appControl=[AppControl retrieveSingleton];
                isSqliteUpdated=YES;
                [self updateLastSyncDetails:YES];
                [SWDefaults setLastFullSyncDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
               // [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Successful"] WithTimeStamp:YES];
            }
            
        }
    }
}
-(void)updateFullSyncMandatoryFlag
{
    if(appDel.isFullSyncMandatory)
    {
        appDel.isFullSyncMandatory=NO;
        [SWDefaults setVersionNumber:[[[SWDefaults alloc]init] getAppVersionWithBuild]];
        [doneButton setHidden:NO];
    }
}
- (void)RecieveSendOrdersStatusUpdate:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:KSendOrdersNotificationNameStr])
    {
        NSDictionary *statusDic = (NSDictionary *)notification.object;
       // [self showSyncProgress];
        
        
        if([[statusDic valueForKey:KSyncDic_ShowAlertStr]isEqualToString:KSync_YesCode])
        {
            SendOrdersButtonImageView.image=[UIImage imageNamed:@"Send_Orders.png"];

           [self showAlertAfterHidingKeyBoard:KSync_ErrorAlertTitleStr andMessage:[statusDic valueForKey:KSync_AlertMessageStr] withController:nil];
            [self updateLastSyncDetails:NO];
            syncStatusLbl.text = [KSync_SyncStausStr stringByAppendingString:@"Failed"];
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSync_AlertMessageStr] WithTimeStamp:YES];
            [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Failed"] WithTimeStamp:YES];
        }
        else
        {
            syncStatusLbl.text=[statusDic valueForKey:KSyncDic_SyncStausStr];
            syncProgressBar.progress=[[statusDic valueForKey:KSyncDic_SyncProgressStr] floatValue];
            NSLog (@"Successfully received the KSendOrdersNotificationNameStr!");
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSyncDic_SyncStausStr] WithTimeStamp:YES];

            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSendOrders_OrdersUploadSuccesAlertMessage])
            {
                [self updateLastSyncDetails:YES];
                SendOrdersButtonImageView.image=[UIImage imageNamed:@"Send_Orders.png"];

            }
            
        }
    }
}

-(void)updateLastSyncDetails:(BOOL)SyncStatusFlag
{
    [self.view setUserInteractionEnabled:YES];
    appDel.currentExceutingDataUploadProcess=KDataUploadProcess_None;

    if(SyncStatusFlag)
    {
        [SWDefaults setLastSyncStatus:@"Successful"];
        [statusLbl setTextColor:[UIColor colorWithRed:0.0 green:(186.0/255.0) blue:(120.0/255.0) alpha:1.0]];
        [self postFullSyncDataToGoogleAnalytics];
        //[SWDefaults UpdateGoogleAnalyticsforScreenName:kDataSyncDoneScreenName];

    }
    else
    {
        [SWDefaults setLastSyncStatus:@"Failed"];
        syncProgressBar.progress=1.0;
        [statusLbl setTextColor:[UIColor redColor]];

    }
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    [SWDefaults setLastSyncDate:dateString];
    lastSyncOnLbl.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm" destFormat:kDateFormatWithTime scrString:[SWDefaults lastSyncDate]];
    statusLbl.text = NSLocalizedString([SWDefaults lastSyncStatus], nil);
    [SWDefaults setCollectionCounter:0];
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    // collectionCountString = [[SWDatabaseManager retrieveManager] dbdbGetCollectionCount];
    collectionCountString=nil;
    collectionCountString = @"0";
    [ordersPendingLbl setText:orderCountString];
    [returnsPendingLbl setText:returnCountString];
    
    syncTypeLbl.text=NSLocalizedString([SWDefaults lastSyncType],nil);
    
    if([appControl.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode])
    {
        // Post a notification to enableBackGroundSync
        NSLog(@"posting background sync notification from sync popover");
       // [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchEnableFlag];
       
        [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_COMM_BS_NOTIFICATIONNameStr object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kENABLE_PRODSTK_NOTIFICATIONNameStr object:nil];

    }
    else
    {
        /** disabeling the background */
        [[NSNotificationCenter defaultCenter] postNotificationName:KDISABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:KDISABLE_COMM_BS_NOTIFICATIONNameStr object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDISABLE_PRODSTK_NOTIFICATIONNameStr object:nil];

    }
}

-(void)doneButtonTapped
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KNewSyncNotificationNameStr object:nil];

}
-(void)viewWillAppear:(BOOL)animated
{

    returnsPendingLbl.font=kSWX_FONT_SEMI_BOLD(44);
    ordersPendingLbl.font=kSWX_FONT_SEMI_BOLD(44);
    
    
    if(self.isMovingToParentViewController)
    {
        SyncPopoverContentView.backgroundColor=UIViewControllerBackGroundColor;

        for (UIView *borderView in SyncPopoverContentView.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                if (borderView.tag==101|| borderView.tag==102|| borderView.tag==103|| borderView.tag==104 || borderView.tag==105) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                    
                }
            }
            
        }
        
        [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
        [self StopFullSyncAnimation];

    }
    else
    {
        [self setHiddenAnimated:YES duration:1.0 transitionType:kCATransitionReveal];
        [SyncPopoverContentView setHidden:NO];
    }
    
    if (appDel.isFullSyncMandatory)
    {
        [doneButton setHidden:YES];
        [SWDefaults showAlertAfterHidingKeyBoard:@"" andMessage:KVersionUpdate_MandatoryFullSync_AlertMessageStr withController:self];
    }
   // [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showMediaSyncStatus) userInfo:nil repeats:YES];

    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]];
        NSArray *PDAComponents=[PDARIGHTS componentsSeparatedByString:@","];
        NSLog(@"PDAComponents %@",PDAComponents);
        
        if(![PDAComponents containsObject:KFieldSalesSectionPDA_RightsCode])
        {
            xFullSyncImageTrailingMarginConstraint.constant=155.0;
            [SendOrdersButtonImageView setHidden:YES];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];

    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
    
    
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
   
    [SyncPopoverContentView setHidden:YES];

    if (syncLocationsArray.count>0) {

        SalesWorxServersListViewController *ServersListViewController=[[SalesWorxServersListViewController alloc]initWithNibName:@"SalesWorxServersListViewController" bundle:[NSBundle mainBundle]];
        ServersListViewController.salesWorxServersListViewControllerDelegate=self;
        [self.navigationController pushViewController:ServersListViewController animated:NO];
    }
        return NO;
    
    
}
-(void)updateSelectedServerDetails:(NSString*)selectedServer
{
    NSData *SelectedSeverData = [selectedServer dataUsingEncoding:NSUTF8StringEncoding];
    
    id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
    
    NSString *selectedServerLocation=[SelectedServersDetailsjson valueForKey:@"name"]==nil?@"":[SelectedServersDetailsjson valueForKey:@"name"];
    NSLog(@"%@",selectedServerLocation);
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"syncLocation"];
    
    NSData *defaultServerData = [savedValue dataUsingEncoding:NSUTF8StringEncoding];
    id DefaultServersDetailsjson;
    NSString *defaultServerLocation;
    if (defaultServerData != nil){
    DefaultServersDetailsjson = [NSJSONSerialization JSONObjectWithData:defaultServerData options:0 error:nil];
    
    defaultServerLocation=[DefaultServersDetailsjson valueForKey:@"name"]==nil?@"":[DefaultServersDetailsjson valueForKey:@"name"];
    NSLog(@"%@",defaultServerLocation);
    }
    if([selectedServerLocation isEqualToString:defaultServerLocation]){
        serverUrltextField.text=[DefaultServersDetailsjson valueForKey:@"name"]==nil?@"":[DefaultServersDetailsjson valueForKey:@"name"];
    }else{
        
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                   message:@"Do you want to change server location to default location?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self changeServerLocation:selectedServer];
                                                      }];
    
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [self changeServerLocationWithoutDefaultValue:selectedServer];
                                                     }];
    
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self presentViewController:alert animated:YES completion:nil];
    }
    
}
-(void)changeServerLocation:(NSString *)selectedServer{
    /** Getting selected server details*/
    [self.navigationController popViewControllerAnimated:NO];
    NSLog(@"selected server in popover %@", selectedServer);
    
   // syncSelectedServerFromPopUp = selectedServer;
    
    [[NSUserDefaults standardUserDefaults]setObject:selectedServer forKey:@"syncLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //    NSData *SelectedSeverData = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    syncSelectedServerDetails=selectedServer;
    NSData *SelectedSeverData = [selectedServer dataUsingEncoding:NSUTF8StringEncoding];
    
    id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
    
    serverUrltextField.text=[SelectedServersDetailsjson valueForKey:@"name"]==nil?@"":[SelectedServersDetailsjson valueForKey:@"name"];
}
-(void)changeServerLocationWithoutDefaultValue:(NSString *)selectedServer{
    [self.navigationController popViewControllerAnimated:NO];
    NSLog(@"selected server in popover %@", selectedServer);
    
    // syncSelectedServerFromPopUp = selectedServer;
    [self.navigationController popViewControllerAnimated:NO];
    NSLog(@"selected server in popover %@", selectedServer);
    
    // syncSelectedServerFromPopUp = selectedServer;
    
//    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"syncLocation"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //    NSData *SelectedSeverData = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    syncSelectedServerDetails=selectedServer;
    NSData *SelectedSeverData = [selectedServer dataUsingEncoding:NSUTF8StringEncoding];
    
    id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
    
    serverUrltextField.text=[SelectedServersDetailsjson valueForKey:@"name"]==nil?@"":[SelectedServersDetailsjson valueForKey:@"name"];
}


-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
    SyncLoctions * selectedSyncLocation=[syncLocationsArray objectAtIndex:selectedIndexPath.row];
    
    serverUrltextField.text=selectedSyncLocation.name;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return syncLocationsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SyncLoctions * currentSyncLocation=
    [syncLocationsArray objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.numberOfLines=2;
        cell.textLabel.font=RegularFontOfSize(5);
        cell.backgroundColor=UIColorFromRGB(0xF6F7FB);
        cell.textLabel.textColor=UIColorFromRGB(0x687281);
        cell.textLabel.textColor=[UIColor darkTextColor ];
        cell.textLabel.font  =  LightFontOfSize(14) ;
    }
    
    cell.textLabel.text=currentSyncLocation.name;
    return cell;
    
    
}


-(void)postFullSyncDataToGoogleAnalytics
{
    NSMutableDictionary * googleAnalyticsDict=[[NSMutableDictionary alloc]init];
    [googleAnalyticsDict setValue:[[SWDefaults userProfile]valueForKey:@"User_Name"] forKey:@"username"];
    [googleAnalyticsDict setValue:[SWDefaults licenseIDForInfo] forKey:@"Customer_ID"];

    NSData *SelectedSeverData = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
    
    [googleAnalyticsDict setValue:[SelectedServersDetailsjson valueForKey:@"name"] forKey:@"Server_Name"];
    
    [googleAnalyticsDict setValue:[SelectedServersDetailsjson valueForKey:@"url"] forKey:@"Server_Url"];
    
    [googleAnalyticsDict setValue:[[DataSyncManager sharedManager]getDeviceID] forKey:@"Device_ID"];
    //add an event to track sync
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Full Sync Done"     // Event category (required)
                                                          action:[SWDefaults getValidStringValue:[googleAnalyticsDict valueForKey:@"Server_Name"]]  // Event action
                                                           label:[SWDefaults getValidStringValue:[googleAnalyticsDict valueForKey:@"Server_Url"]]          // Event label
                                                           value:nil] build]];    // Event value
}
- (IBAction)fullSyncButtontapped:(id)sender {
    
    if(![appDel.currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_None])
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Data Synchronization" andMessage:@"Background Sync is active. Kindly try again later." withController:self];
        return;
    }
    
    
    syncStatusLbl.text=@"";

    [self.view setUserInteractionEnabled:NO];
    syncManager=[[SyncManagerViewController alloc]init];
    NSString* savedValue = [[NSUserDefaults standardUserDefaults]
                            objectForKey:@"syncLocation"];
   // if(savedValue == nil){
        syncManager.selectedServerDetailsString=syncSelectedServerDetails;
//    }else{
//        syncManager.selectedServerDetailsString = savedValue;
//    }
    
    
    NSArray *tempProformaOrders = [[[SWDatabaseManager alloc]init] fetchDataForQuery:kSQLdbGetUnConfirmOrderS];
    
    if([appControl.ENABLE_MEDREP_INCOMPLETE_VISIT_COMMENTS isEqualToString:KAppControlsYESCode] && [[MedRepQueries getInCompleteVisits] count]>0 && !IsInCompleteVisitsCommentsUpdated){
        SalesWorxMedrepInCompleteVisitsViewController *medrepInCompleteVisitsViewController=[[SalesWorxMedrepInCompleteVisitsViewController alloc]initWithNibName:@"SalesWorxMedrepInCompleteVisitsViewController" bundle:[NSBundle mainBundle]];
        [SyncPopoverContentView setHidden:YES];
        medrepInCompleteVisitsViewController.medrepInCompleteVisitsViewControllerDelegate=self;
        [self.navigationController pushViewController:medrepInCompleteVisitsViewController animated:NO];
        [self.view setUserInteractionEnabled:YES];
    }
    else if(tempProformaOrders.count>0 && [appControl.SHOW_UNCONFIRMED_ORDER_SCREEN_ONSYNC isEqualToString:KAppControlsYESCode] && !isUserConfirmedTheUnconfirmOrders)
    {
        SalesWorxSyncUnconfirmedOrdersViewController *syncUnconfirmedOrdersViewController=[[SalesWorxSyncUnconfirmedOrdersViewController alloc]initWithNibName:@"SalesWorxSyncUnconfirmedOrdersViewController" bundle:[NSBundle mainBundle]];
        [SyncPopoverContentView setHidden:YES];
        syncUnconfirmedOrdersViewController.syncType=@"Full Sync";
        syncUnconfirmedOrdersViewController.salesWorxSyncUnconfirmedOrdersViewControllerDelegate=self;
        [self.navigationController pushViewController:syncUnconfirmedOrdersViewController animated:NO];
        [self.view setUserInteractionEnabled:YES];

        
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_ROUTE_BS_NOTIFICATIONNameStr object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_ROUTE_CANCEL_BS_NOTIFICATIONNameStr object:nil];
        
       __block BOOL isValidLicense=NO;
        __block NSString* ValidDeviceServiceReponse;

        [self StartFullSyncAnimation];
        syncStatusLbl.text=@"Validating device information";
        syncProgressBar.progress=0.0;

        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            NSString *applicationDocumentsDir;
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                applicationDocumentsDir=[SWDefaults applicationDocumentsDirectory];
            }
            
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains
                (NSDocumentDirectory, NSUserDomainMask, YES);
                applicationDocumentsDir = [paths objectAtIndex:0];
            }
            
            isValidLicense=[syncManager licenseVerification];
            ValidDeviceServiceReponse=[[[SalesWorxDeviceValidationClass alloc]init] ValidateDevice];
            FMDatabase *database = [FMDatabase databaseWithPath:[applicationDocumentsDir stringByAppendingPathComponent:@"swx.sqlite"]];
            [database executeQuery:@"vacuum"];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isValidLicense && [ValidDeviceServiceReponse isEqualToString:@"Success"])
                {
                    
                    currentSyncStatus=@"";
                    lastSyncStausString=@"";
                    [SWDefaults setLastSyncType:@"Full Sync"];
                    syncProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
                    
                    NSLog(@"%@",[SWDefaults customer]);
                    NSLog(@"License Verified .. OK");
                    [self writeSyncLogToTextFile:[NSString stringWithFormat:@"User ID : %@",[[SWDefaults userProfile] stringForKey:@"Username"]] WithTimeStamp:NO];
                    [self writeSyncLogToTextFile:[NSString stringWithFormat:@"Customer ID : %@ \n",[SWDefaults licenseIDForInfo]] WithTimeStamp:NO];
                    
                    [self writeSyncLogToTextFile:@"Sync log:" WithTimeStamp:NO];
                    appDel.currentExceutingDataUploadProcess=KDataUploadProcess_FullSync;

                    [syncManager prepareRequestForSyncUpload];

                }
                else if(![ValidDeviceServiceReponse isEqualToString:@"Success"]) {
                    [self showAlertAfterHidingKeyBoard:KSalesWorxDeviceValidationErrorMessage andMessage:ValidDeviceServiceReponse withController:nil];
                    [self.view setUserInteractionEnabled:YES];
                    [self StopFullSyncAnimation];
                    syncStatusLbl.text=@"Device Validation failed";
                }
                else
                {
                    [self showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"Your license has been expired or contact your administrator for further assistance" withController:nil];
                    [self.view setUserInteractionEnabled:YES];
                    [self StopFullSyncAnimation];
                    syncStatusLbl.text=@"Validation failed";

                }
                
                
            });
        });
        

    }
}

- (IBAction)sendOrdersButtonTapped:(id)sender {
    if(![appDel.currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_None] )
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Data Synchronization" andMessage:@"Background Sync is active. Kindly try again later." withController:self];
        return;
    }



    [self.view setUserInteractionEnabled:NO];
    syncStatusLbl.text=@"";

    NSArray *tempOrders = [[[SWDatabaseManager alloc]init] fetchDataForQuery:kSQLdbGetConfirmOrderS];
    NSArray *tempProformaOrders = [[[SWDatabaseManager alloc]init] fetchDataForQuery:kSQLdbGetUnConfirmOrderS];
    syncManager=[[SyncManagerViewController alloc]init];
    NSString* savedValue = [[NSUserDefaults standardUserDefaults]
                            objectForKey:@"syncLocation"];
    if(savedValue == nil)
    {
    syncManager.selectedServerDetailsString=syncSelectedServerDetails;
    }
    else
    {
        syncManager.selectedServerDetailsString = savedValue;
    }

    if(tempOrders.count==0)
    {
        
        [self showAlertAfterHidingKeyBoard:KSync_ErrorAlertTitleStr andMessage:KSendOrders_TakeOrdersFirstAlertMessage withController:nil];
        [self.view setUserInteractionEnabled:YES];

    }
    else if(tempProformaOrders.count>0 && [appControl.SHOW_UNCONFIRMED_ORDER_SCREEN_ONSYNC isEqualToString:KAppControlsYESCode] && !isUserConfirmedTheUnconfirmOrders)
    {
        SalesWorxSyncUnconfirmedOrdersViewController *syncUnconfirmedOrdersViewController=[[SalesWorxSyncUnconfirmedOrdersViewController alloc]initWithNibName:@"SalesWorxSyncUnconfirmedOrdersViewController" bundle:[NSBundle mainBundle]];
        syncUnconfirmedOrdersViewController.salesWorxSyncUnconfirmedOrdersViewControllerDelegate=self;
        [SyncPopoverContentView setHidden:YES];
        syncUnconfirmedOrdersViewController.syncType=@"Send Orders";
        [self.navigationController pushViewController:syncUnconfirmedOrdersViewController animated:NO];
        [self.view setUserInteractionEnabled:YES];

    }
    else
    {
        SendOrdersButtonImageView.image =  [UIImage animatedImageWithAnimatedGIFData:(NSData *)animatedSendOrdersGifData];

        [self.view setUserInteractionEnabled:NO];
        currentSyncStatus=@"";
        lastSyncStausString=@"";
        [SWDefaults setLastSyncType:@"Send Orders"];
        syncProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
        syncStatusLbl.text=@"Validating device information";
        syncProgressBar.progress=0.0;

        __block BOOL isValidLicense=NO;
        __block NSString* ValidDeviceServiceReponse;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            isValidLicense=[syncManager licenseVerification];
            ValidDeviceServiceReponse=[[[SalesWorxDeviceValidationClass alloc]init] ValidateDevice];

            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isValidLicense && [ValidDeviceServiceReponse isEqualToString:@"Success"])
                {
                    NSLog(@"%@",[SWDefaults customer]);
                    NSLog(@"License Verified .. OK");
                    [self writeSyncLogToTextFile:[NSString stringWithFormat:@"User ID : %@",[[SWDefaults userProfile] stringForKey:@"Username"]] WithTimeStamp:NO];
                    [self writeSyncLogToTextFile:[NSString stringWithFormat:@"Customer ID : %@ \n",[SWDefaults licenseIDForInfo]] WithTimeStamp:NO];
                    [self writeSyncLogToTextFile:@"Send orders Log :" WithTimeStamp:NO];
                    appDel.currentExceutingDataUploadProcess=KDataUploadProcess_SendOrders;
                    [syncManager prepareRequestForSendOrders];


                }
                else if(![ValidDeviceServiceReponse isEqualToString:@"Success"]) {
                    [self showAlertAfterHidingKeyBoard:KSalesWorxDeviceValidationErrorMessage andMessage:ValidDeviceServiceReponse withController:nil];
                    [self.view setUserInteractionEnabled:YES];
                    SendOrdersButtonImageView.image=[UIImage imageNamed:@"Send_Orders.png"];
                    syncStatusLbl.text=@"Device Validation failed";
                }
                else
                {
                    [self showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"Your license has been expired or contact your administrator for further assistance" withController:nil];
                    [self.view setUserInteractionEnabled:YES];
                    SendOrdersButtonImageView.image=[UIImage imageNamed:@"Send_Orders.png"];
                    syncStatusLbl.text=@"Validation failed";

                  }
            });
        });
    }
   
}

- (IBAction)sendLogsTapped:(id)sender {
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"SalesWorx Sync Report"];
        //NSArray *toRecipients = [NSArray arrayWithObjects:self.email.text, nil];
        //[mailer setToRecipients:toRecipients];
        [mailer setTitle:@"SalesWorx"];
        [mailer setMessageBody:currentSyncStatus isHTML:NO];
        if (mailer) {
            [self presentViewController:mailer animated:YES completion:nil];
        }
    }
    else
    {
        [self showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:nil];
    }

    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneButtontapped:(id)sender {
    

    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseSyncPopOver) userInfo:nil repeats:NO];
}
- (void)CloseSyncPopOver
{
    //do what you need to do when animation ends...
  [self dismissViewControllerAnimated:NO completion:^{
      if(isSqliteUpdated)
      {
          [_syncPopdelegate refreshPDARights];
      }
      
      else{
          [[NSNotificationCenter defaultCenter]postNotificationName:@"UPDATEDASHBOARDUINOTIFICATIONNAME" object:nil];

      }
  }];

}
-(void)MedreRepInCompleteVisitsCommentsUpdated{
    IsInCompleteVisitsCommentsUpdated=YES;
    [self fullSyncButtontapped:nil];
}

-(void)hideSyncProgress
{

    [syncView setHidden:YES];
    SyncViewHeghtConstraint.constant=0;
}


-(void)showSyncProgress
{

    SyncViewHeghtConstraint.constant=83;

    [syncView setHidden:NO];
}

- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;

    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");

    UIView* containerView = SyncPopoverContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}



-(void)writeSyncLogToTextFile:(NSString *)textString WithTimeStamp:(BOOL)isTimeStampRequired
{
    if([lastSyncStausString isEqualToString:textString])
    {
        
    }
    else
    {
        lastSyncStausString=textString;
        //get the documents directory:
        NSString *slashN = @"\n";
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *dateString =  [formatter stringFromDate:[NSDate date]];
        if(isTimeStampRequired)
        {
            slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@: %@",dateString , textString]];

        }
        else
        {
            slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ " , textString]];

        }
        currentSyncStatus=[currentSyncStatus stringByAppendingString:slashN];

        //iOS 8 support
        NSString *documentsDirectory;
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [NSString stringWithFormat:@"%@/Sync_Log.txt",documentsDirectory];
        
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
        [fileHandler seekToEndOfFile];
        [fileHandler writeData:[slashN dataUsingEncoding:NSUTF8StringEncoding]];
        [fileHandler closeFile];
        formatter=nil;
        usLocale=nil;
    }

}

/*media sync*/
-(IBAction)MediaSyncButtonTapped:(id)sender
{
    
    SalesWorxMediaSyncViewController *mediaSyncViewController=[[SalesWorxMediaSyncViewController alloc]initWithNibName:@"SalesWorxMediaSyncViewController" bundle:[NSBundle mainBundle]];
    [SyncPopoverContentView setHidden:YES];
    [self.navigationController pushViewController:mediaSyncViewController animated:NO];

}
-(void)ordersConfirmationCompletedForSyncType:(NSString *)syncType
{
    isUserConfirmedTheUnconfirmOrders=YES;

    if([syncType isEqualToString:@"Full Sync"]){
        [self fullSyncButtontapped:nil];
    }
    else{
        [self sendOrdersButtonTapped:nil];
    }
}


-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle, nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:KAlertOkButtonTitle
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
-(void)StartFullSyncAnimation
{
    
    fullSyncButtonImageView.layer.sublayers=nil;

    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0  ];
    rotationAnimation.repeatCount = INFINITY;
    rotationAnimation.speed=0.2;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(37,10,60,60)];
    imgView.image = [UIImage imageNamed:@"Sync_Rotator"];
    [imgView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [fullSyncButtonImageView.layer addSublayer:[imgView layer]];
}

-(void)StopFullSyncAnimation
{
    [fullSyncButtonImageView.layer removeAllAnimations];
    fullSyncButtonImageView.layer.sublayers=nil;

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(37,10,60,60)];
    imgView.image = [UIImage imageNamed:@"Sync_Rotator"];
    [fullSyncButtonImageView.layer addSublayer:[imgView layer]];

}

#pragma Mark Bavkground Sync Methods
-(void)StartBackgroundSyncAnimation
{
    isBackgroundSyncAnimationRunning=YES;
    backgroundSyncImageView.layer.sublayers=nil;
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0  ];
    rotationAnimation.repeatCount = INFINITY;
    rotationAnimation.speed=0.2;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(37,10,60,60)];
    imgView.image = [UIImage imageNamed:@"Sync_Rotator"];
    [imgView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [backgroundSyncImageView.layer addSublayer:[imgView layer]];
}

-(void)StopBackgroundSyncAnimation
{
    [backgroundSyncImageView.layer removeAllAnimations];
    backgroundSyncImageView.layer.sublayers=nil;
    isBackgroundSyncAnimationRunning=NO;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(37,10,60,60)];
    imgView.image = [UIImage imageNamed:@"Sync_Rotator"];
    [backgroundSyncImageView.layer addSublayer:[imgView layer]];
    
}

@end
