//
//  SalesWorxSyncUnconfirmedOrdersViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"
#import "SWDatabaseManager.h"
#import "SalesWorxSyncUnconfirmedOrdersTableViewCell.h"
#import "FMDatabase.h"
#import "MedRepQueries.h"
@protocol SalesWorxSyncUnconfirmedOrdersViewControllerDelegate <NSObject>
-(void)ordersConfirmationCompletedForSyncType:(NSString*)syncType;
@end
@interface SalesWorxSyncUnconfirmedOrdersViewController : UIViewController
{
   IBOutlet UIView *UnconfirmedOrdersTableHeaderView;
    NSMutableArray * proformaOrdersArray;
    NSMutableArray * selectedIndexpathsArray;
    IBOutlet  UIView *proformaListContentView;
    IBOutlet UIView *headerView;
    NSMutableArray * proformaOrdersDataArray;

}
-(IBAction)DoneButtonTapped:(id)sender;
@property (strong,nonatomic) id <SalesWorxSyncUnconfirmedOrdersViewControllerDelegate>salesWorxSyncUnconfirmedOrdersViewControllerDelegate;
@property (strong,nonatomic) NSString *syncType;

@end
