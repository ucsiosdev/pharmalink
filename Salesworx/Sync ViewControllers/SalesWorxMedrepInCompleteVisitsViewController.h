//
//  SalesWorxMedrepInCompleteVisitsViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/13/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxNavigationButton.h"
#import "SalesWorxTableView.h"
#import "SalesWorxTableViewHeaderView.h"
#import "TPKeyboardAvoidingTableView.h"


@protocol SalesWorxMedrepInCompleteVisitsViewControllerDelegate <NSObject>
-(void)MedreRepInCompleteVisitsCommentsUpdated;
@end
@interface SalesWorxMedrepInCompleteVisitsViewController : UIViewController<UITextViewDelegate>
{
    IBOutlet SalesWorxNavigationButton *cancelButton;
    IBOutlet SalesWorxNavigationButton *doneButton;
    IBOutlet TPKeyboardAvoidingTableView *incompleteVisitsTableView;
    NSMutableArray *inCompleteVisitsArray;
    IBOutlet UIView *incompleteVisitsconatinerView;
    IBOutlet SalesWorxTableViewHeaderView *commentsTableHeaderView;
}

@property (strong,nonatomic) id <SalesWorxMedrepInCompleteVisitsViewControllerDelegate>medrepInCompleteVisitsViewControllerDelegate;
@end
