//
//  SalesWorxCommunicationMessagesSyncManager.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/6/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxCommunicationMessagesSyncManager.h"
#import "MedRepQueries.h"
#import "MedRepQueries.h"
#import "AppControl.h"
@implementation SalesWorxCommunicationMessagesSyncManager



-(void)sendMessagesToserver
{
    AppControl *appControl=[AppControl retrieveSingleton];
    if([appControl.MSG_MODULE_VERSION isEqualToString:KCommunicationVersion2Value])
    {
        [self sendRequestForMCExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendFSRMessages" andProcDic:[self getProcedureDictionaryForV2Messsages]];
        
        [self sendRequestForExecGetV2FSRRecipientMessagesWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecGetMsgRecipients" andProcDic:[self getProcedureDictionaryForGETV2FSRMesssages]];

       
        [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(getV2TBLMSG) userInfo:nil repeats:NO];
    }
    else
    {
        [self sendRequestForCOMMExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendMessages" andProcDic:[self getProcedureDictionaryForInMessages]];
        [self sendRequestForExecGetFSRMessagesWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecGetFSRMessages" andProcDic:[self getProcedureDictionaryForGETFSRMesssages]];
    }
}
-(void)getV2TBLMSG
{
    if(isV2ReciepeintsDataRecieved)
    {
            [self sendRequestForExecGetV2FSRMessagesWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecGetMsg" andProcDic:[self getProcedureDictionaryForGETV2FSRMesssages]];
    }

}
#pragma mark Version1Messages

-(NSDictionary *)getProcedureDictionaryForInMessages
{
    
    
    if([SWDefaults lastCOMMBackGroundSyncDate]==nil)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
    }
    else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastCOMMBackGroundSyncDate]]] == NSOrderedAscending)
    {
        lastSynctimeStr=[SWDefaults lastCOMMBackGroundSyncDate];
        
    }
    else
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
        
    }
    
    
    currentSyncStarttimeStr=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    
    /*** ncomingMessagesarray = [[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString */
    NSMutableArray *IncomingMessagesarray = [[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetIncommingMessages, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *xmlInMessagesString=[self prepareXMLStringForInMessages:IncomingMessagesarray];
    NSString  *XMLInMessagesPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)xmlInMessagesString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    /*** repliedMessages*/
    NSMutableArray *repliedMessagesArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetFSRRepliedMessages, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *xmlRepliedMessagesString=[self prepareXMLStringForRepliedMessages:repliedMessagesArray];
    NSString  *XMLRepliedMessagesPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)xmlRepliedMessagesString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));


    
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"InMessageInfo",@"MessegeList", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:XMLInMessagesPassingString,XMLRepliedMessagesPassingString,nil];
    
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
    
}

//
-(NSString *)prepareXMLStringForInMessages:(NSMutableArray *)MessagesArray{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"IM"];

    if([MessagesArray count]!=0)
    {
        for (int l=0 ; l<[MessagesArray count] ; l++)
        {
            
            NSMutableDictionary * messageDictionary = [NSMutableDictionary dictionaryWithDictionary:[MessagesArray objectAtIndex:l]];
           
                [xmlWriter writeStartElement:@"M"];
                
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Message_ID"]];
                [xmlWriter writeEndElement];
                
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"SalesRep_ID"]];
                [xmlWriter writeEndElement];
                
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
                
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Rcpt_User_ID"]];
                [xmlWriter writeEndElement];
                
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Message_Title"]];
                [xmlWriter writeEndElement];
                
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Message_Content"]];
                [xmlWriter writeEndElement];
                
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Message_Date"]];
                [xmlWriter writeEndElement];
                
                [xmlWriter writeEndElement];//message
        
        }
    }
    [xmlWriter writeEndElement];//messages

    return   [xmlWriter toString];
    
}
//
#pragma mark replied Messages
-(NSString *)prepareXMLStringForRepliedMessages:(NSMutableArray *)MessagesArray{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    
        [xmlWriter writeStartElement:@"Messages"];
        for (int l=0 ; l<[MessagesArray count] ; l++)
        {
            
            NSMutableDictionary * messageDictionary = [NSMutableDictionary dictionaryWithDictionary:[MessagesArray objectAtIndex:l]];
            
            [xmlWriter writeStartElement:@"Message"];

            [xmlWriter writeStartElement:@"C1"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Message_ID"]];
            [xmlWriter writeEndElement];
                
            
            [xmlWriter writeStartElement:@"C2"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"SalesRep_ID"]];
            [xmlWriter writeEndElement];
            
            if (![[messageDictionary stringForKey:@"Message_Read"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Message_Read"].length !=0)
            {
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Message_Read"]];
                [xmlWriter writeEndElement];
            }


            [xmlWriter writeStartElement:@"C8"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Message_Reply"]];
            [xmlWriter writeEndElement];
            
            
        if (![[messageDictionary stringForKey:@"Reply_Date"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Reply_Date"].length !=0)
            {
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Reply_Date"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[messageDictionary stringForKey:@"Emp_code"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Emp_code"].length !=0)
            {
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Emp_code"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//message
                
            
        }
        [xmlWriter writeEndElement];//messages
    return   [xmlWriter toString];
    
}


- (void)sendRequestForCOMMExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"InMessageInfo",@"MessegeList", nil];
    
    for (NSInteger i=0; i<procedureParametersArray.count; i++) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];
        
    }
    
   // strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRep_ID"];
   // strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    
    
    
    // NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Send_Messages",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"background sync Response %@",resultArray);

         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             [SWDefaults setLastCOMMBackGroundSyncDate:currentSyncStarttimeStr];
         }
     }
        failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@" background sync error  in communication background sync %@",error);
     }];
    
    //call start on your request operation
    [operation start];
}

#pragma mark new messages methods

-(NSDictionary *)getProcedureDictionaryForGETFSRMesssages
{
    NSArray *messageDataArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select max(Message_Date) AS Message_Date  from TBL_FSR_Messages"];
    NSString *lastfsrsynctime;
    if(messageDataArray.count>0)
    {
        lastfsrsynctime=[[messageDataArray valueForKey:@"Message_Date"] objectAtIndex:0];
        if([[SWDefaults getValidStringValue:lastfsrsynctime] length]==0)
        {
            lastfsrsynctime=[SWDefaults lastFullSyncDate];
        }
    }
    else
    {
        lastfsrsynctime=[SWDefaults lastFullSyncDate];
    }
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"SalesRepID",@"LastSyncTime", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] ,lastfsrsynctime,nil];
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
}


- (void)sendRequestForExecGetFSRMessagesWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
   NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];

    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];


    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
        for (NSString *key in procDic) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",key];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:key]];
        
    }
    
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];

    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_FSRMessages",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];


    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;

    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];

    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         
         
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"GET FSR Messages Response %@",resultArray);

         NSMutableArray *tempResultsArray=[[NSMutableArray alloc]init];
         
         NSArray *fsrMessagesIdsArray= [[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Message_ID from TBL_FSR_Messages"] valueForKey:@"Message_ID"];
         
         for (NSInteger i=0; i<resultArray.count; i++) {
                if(![fsrMessagesIdsArray containsObject:[[resultArray objectAtIndex:i] valueForKey:@"Message_ID"]])
             {
                 [tempResultsArray addObject:[resultArray objectAtIndex:i]];
             }
             
         }
         
         if(tempResultsArray.count>0)
             [[SWDatabaseManager retrieveManager]InsertFSRNewMessages:tempResultsArray];
         
         NSLog(@"GET FSR Messages Response %@",tempResultsArray);
     }
        failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {

         NSLog(@"GET FSR messages error %@",error.localizedDescription);

     }];

    //call start on your request operation
    [operation start];
}







#pragma  mark communicationmodule2
-(NSDictionary *)getProcedureDictionaryForV2Messsages{
    if([SWDefaults lastCOMMBackGroundSyncDate]==nil)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
    }
    else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastCOMMBackGroundSyncDate]]] == NSOrderedAscending)
    {
        lastSynctimeStr=[SWDefaults lastCOMMBackGroundSyncDate];
        
    }
    else
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
        
    }
    
    
    currentSyncStarttimeStr=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSMutableArray *tblMsgsArray = [[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Msg where Logged_At > '%@' AND Logged_At < '%@'", lastSynctimeStr,currentSyncStarttimeStr]]];
    
    
    [tblMsgsArray addObjectsFromArray:[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Msg where parent_msg_id in (select msg_id from TBL_Msg where Logged_At > '%@' AND Logged_At < '%@')", lastSynctimeStr,currentSyncStarttimeStr]]]];
    
    NSArray  *messagesIdArray = [tblMsgsArray valueForKey:@"Msg_ID"];
    
    NSMutableArray *tempMessagesIdsArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<messagesIdArray.count; i++) {
        [ tempMessagesIdsArray addObject:[NSString stringWithFormat:@"'%@'",[messagesIdArray objectAtIndex:i]] ];
    }
    
    NSString *joinedString = [tempMessagesIdsArray componentsJoinedByString:@","];
    
    NSMutableArray *tblRecpMsgsArray = [[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Msg_Recipients where msg_id in (%@)", joinedString]]];
    
    
    NSString *xmlInMessagesString=[self prepareXMLStringForV2Messages:tblMsgsArray andReciepients:tblRecpMsgsArray];
    NSLog(@"xmlInMessagesString %@",xmlInMessagesString);
    NSString  *XMLInMessagesPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)xmlInMessagesString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"MessageInfo", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:XMLInMessagesPassingString,nil];
    
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
    
    
}
-(NSString *)prepareXMLStringForV2Messages:(NSMutableArray *)TblMsgArray andReciepients:(NSMutableArray *)TBLMsgReciepientsArray{
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"Messages"];
    
    if([TblMsgArray count]!=0)
    {
        for (int l=0 ; l<[TblMsgArray count] ; l++)
        {
            
            NSMutableDictionary * messageDictionary = [NSMutableDictionary dictionaryWithDictionary:[TblMsgArray objectAtIndex:l]];
            
            [xmlWriter writeStartElement:@"M"];
            
            [xmlWriter writeStartElement:@"C1"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Msg_ID"]];
            [xmlWriter writeEndElement];
            
            [xmlWriter writeStartElement:@"C2"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Msg_Title"]];
            [xmlWriter writeEndElement];
            
            [xmlWriter writeStartElement:@"C3"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Msg_Body"]];
            [xmlWriter writeEndElement];
            
            [xmlWriter writeStartElement:@"C4"];
            [xmlWriter writeCharacters:[NSString stringWithFormat:@"%@",[messageDictionary stringForKey:@"Sender_ID"]]];
            [xmlWriter writeEndElement];
            
            [xmlWriter writeStartElement:@"C5"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Sender_Name"]];
            [xmlWriter writeEndElement];
            
            [xmlWriter writeStartElement:@"C6"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Sent_At"]];
            [xmlWriter writeEndElement];
            
            [xmlWriter writeStartElement:@"C7"];
            [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Expires_At"]];
            [xmlWriter writeEndElement];
            
            
            if (![[messageDictionary stringForKey:@"Parent_Msg_ID"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Parent_Msg_ID"].length !=0)
            {
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Parent_Msg_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[messageDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Custom_Attribute_1"].length !=0)
            {
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }if (![[messageDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Custom_Attribute_2"].length !=0)
            {
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }if (![[messageDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Custom_Attribute_3"].length !=0)
            {
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[messageDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Status"].length !=0)
            {
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[messageDictionary stringForKey:@"Logged_At"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Logged_At"].length !=0)
            {
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Logged_At"]];
                [xmlWriter writeEndElement];
            }
            
            
            
            NSString *tempMessageId=[messageDictionary stringForKey:@"Msg_ID"];
            NSLog(@"tempMessageId %@",tempMessageId);

            NSPredicate *messageIdPredicate=[NSPredicate predicateWithFormat:@"SELF.Msg_ID == %@",tempMessageId];
            NSLog(@"messageIdPredicate %@",messageIdPredicate);
            
            NSMutableArray* tempMessageRecipientsArray=[[TBLMsgReciepientsArray filteredArrayUsingPredicate:messageIdPredicate] mutableCopy];

            
            
            [xmlWriter writeStartElement:@"Recipients"];
            
            if([tempMessageRecipientsArray count]!=0)
            {
                for (int l=0 ; l<[tempMessageRecipientsArray count] ; l++)
                {
                    
                    NSMutableDictionary * messageDictionary = [NSMutableDictionary dictionaryWithDictionary:[tempMessageRecipientsArray objectAtIndex:l]];
                    
                    [xmlWriter writeStartElement:@"R"];
                    
                    [xmlWriter writeStartElement:@"C1"];
                    [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Msg_Rcpt_ID"]];
                    [xmlWriter writeEndElement];
                    
                    [xmlWriter writeStartElement:@"C2"];
                    [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Msg_ID"]];
                    [xmlWriter writeEndElement];
                    
                    [xmlWriter writeStartElement:@"C3"];
                    [xmlWriter writeCharacters:[NSString stringWithFormat:@"%@",[messageDictionary stringForKey:@"Recipient_ID"]]];
                    [xmlWriter writeEndElement];
                    
                    [xmlWriter writeStartElement:@"C4"];
                    [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Recipient_Name"]];
                    [xmlWriter writeEndElement];
                    
                    if (![[messageDictionary stringForKey:@"Read_At"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Read_At"].length !=0)
                    {
                        [xmlWriter writeStartElement:@"C5"];
                        [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Read_At"]];
                        [xmlWriter writeEndElement];
                    } if (![[messageDictionary stringForKey:@"Reply_Msg_ID"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Reply_Msg_ID"].length !=0)
                    {
                        [xmlWriter writeStartElement:@"C6"];
                        [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Reply_Msg_ID"]];
                        [xmlWriter writeEndElement];
                    }
                    if (![[messageDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Custom_Attribute_1"].length !=0)
                    {
                        [xmlWriter writeStartElement:@"C7"];
                        [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Custom_Attribute_1"]];
                        [xmlWriter writeEndElement];
                    }if (![[messageDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Custom_Attribute_2"].length !=0)
                    {
                        [xmlWriter writeStartElement:@"C8"];
                        [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Custom_Attribute_2"]];
                        [xmlWriter writeEndElement];
                    }if (![[messageDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [messageDictionary stringForKey:@"Custom_Attribute_3"].length !=0)
                    {
                        [xmlWriter writeStartElement:@"C9"];
                        [xmlWriter writeCharacters:[messageDictionary stringForKey:@"Custom_Attribute_3"]];
                        [xmlWriter writeEndElement];
                    }
                    [xmlWriter writeEndElement];//message
                    
                }
            }
            [xmlWriter writeEndElement];//messages

            
            
            
            
            
            
            
            [xmlWriter writeEndElement];//message
            
        }
    }
    [xmlWriter writeEndElement];//messages
    
    
    
    
    
    
    return   [xmlWriter toString];
    
}
- (void)sendRequestForMCExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"MessageInfo" ,nil];
    
    for (NSInteger i=0; i<procedureParametersArray.count; i++) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];
        
    }
    
    // strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRep_ID"];
    // strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    
    
    
    // NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Send_FSRMessages",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"background sync V2_msg  Response %@",resultArray);
         
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             [SWDefaults setLastCOMMBackGroundSyncDate:currentSyncStarttimeStr];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@" background sync error in sendRequestForMCExecWithUserName comminication module %@",error);
     }];
    
    //call start on your request operation
    [operation start];
}

/** get new messages*/
-(NSDictionary *)getProcedureDictionaryForGETV2FSRMesssages
{
    NSArray *messageDataArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"sselect max(Logged_At) AS Message_Date from TBL_Msg where  sender_id!='%@'",[[SWDefaults userProfile] stringForKey:@"User_ID"]]];
    NSString *lastfsrsynctime;
    if(messageDataArray.count>0)
    {
        lastfsrsynctime=[[messageDataArray valueForKey:@"Message_Date"] objectAtIndex:0];
        if([[SWDefaults getValidStringValue:lastfsrsynctime] length]==0)
        {
            lastfsrsynctime=[SWDefaults lastFullSyncDate];
        }
    }
    else
    {
        lastfsrsynctime=[SWDefaults lastFullSyncDate];
    }
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"SalesRepID",@"LastSyncTime", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] ,lastfsrsynctime,nil];
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
}
- (void)sendRequestForExecGetV2FSRMessagesWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    for (NSString *key in procDic) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",key];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:key]];
        
    }
    
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_Msg",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         
         
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"GET tbl_msg FSR Messages Response %@",resultArray);
         
         NSMutableArray *tempResultsArray=[[NSMutableArray alloc]init];
         
         NSArray *fsrMessagesIdsArray= [[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Msg_ID from TBL_Msg"] valueForKey:@"Msg_ID"];
         
         for (NSInteger i=0; i<resultArray.count; i++) {
             if(![fsrMessagesIdsArray containsObject:[[[resultArray objectAtIndex:i] valueForKey:@"Msg_ID"] uppercaseString]])
             {
                 [tempResultsArray addObject:[resultArray objectAtIndex:i]];
             }
             
         }
         if(tempResultsArray.count>0)
         {
             [SWDefaults setLastCOMMBackGroundSyncDate:currentSyncStarttimeStr];

         }
         if(tempResultsArray.count>0)
         {
             [[SWDatabaseManager retrieveManager]InsertV2FSRTBLMSGs:tempResultsArray];
             [[NSNotificationCenter defaultCenter]postNotificationName:@"PUSHNOTIFICATIONARRIVED" object:nil userInfo:nil];

         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         
         NSLog(@"GET FSR tbl_msg messages error %@",error.localizedDescription);
         
     }];
    
    //call start on your request operation
    [operation start];
}

- (void)sendRequestForExecGetV2FSRRecipientMessagesWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    for (NSString *key in procDic) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",key];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:key]];
        
    }
    
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_MsgRecipients",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         
         isV2ReciepeintsDataRecieved=YES;
         
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"GET FSR  Messages Reciepient Response %@",resultArray);
         
         NSMutableArray *tempResultsArray=[[NSMutableArray alloc]init];
         
        NSArray *fsrMessagesIdsArray= [[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Msg_Rcpt_ID from TBL_Msg_Recipients"] valueForKey:@"Msg_Rcpt_ID"];
         
         for (NSInteger i=0; i<resultArray.count; i++) {
             if(![fsrMessagesIdsArray containsObject:[[[resultArray objectAtIndex:i] valueForKey:@"Msg_Rcpt_ID"] uppercaseString]])
             {
                [tempResultsArray addObject:[resultArray objectAtIndex:i]];
             }
            
        }
         
         if(tempResultsArray.count>0)
             [[SWDatabaseManager retrieveManager]InsertV2FSRTBLMSGReciepients:tempResultsArray];
         
         NSLog(@"GET FSR Messages Response %@",tempResultsArray);
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         
         NSLog(@"GET FSR Reciepient messages error %@",error.localizedDescription);
         
     }];
    
    //call start on your request operation
    [operation start];
}

@end
