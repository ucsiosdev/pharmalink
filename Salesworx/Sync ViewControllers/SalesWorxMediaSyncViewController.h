//
//  SalesWorxMediaSyncViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TYMProgressBarView.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepDefaults.h"
#import "XYPieChart.h"
#import "KNCirclePercentView.h"
#import "SWAppDelegate.h"
#import "UIImage+animatedGIF.h"
#import "SharedUtils.h"
#import "BackgroundDocumentsDownload.h"
@interface SalesWorxMediaSyncViewController : UIViewController<XYPieChartDelegate,XYPieChartDataSource>
{
    IBOutlet UILabel *TotalMediaFilesCountLabel;
    IBOutlet UILabel *DownloadedMediaFilesCountLabel;
    IBOutlet UILabel *failedMediaFilesCountLabel;
    IBOutlet TYMProgressBarView *mediaSyncProgressBar;
    IBOutlet UILabel *syncStatusLbl;
    NSTimer *SyncTimer;
    IBOutlet UIView *mediaSyncPopoverContentView;
    IBOutlet KNCirclePercentView *mediaSyncChart;
    IBOutlet UIView *mediaSyncChartContainerView;
    NSArray * sliceColorsForMediaSyncChart;
    IBOutlet UILabel *mediaFilesStringLabel;
    IBOutlet UILabel *mediaLastSyncedLabel;
    IBOutlet UIButton *syncButton;
    IBOutlet UIImageView *syncButtonBackgroundImage;
    NSData *animatedSyncGifData;
    BOOL isSyncAnimatedImageShowing;
    float mediaDownloadPercantage;
    IBOutlet UIView *headerView;
    
    
    IBOutlet UILabel *visitsAndTrxsStringLabel;
    IBOutlet UILabel *visitsAndTrxsLastSyncedLabel;
    IBOutlet UISwitch *visitsAndTrxsEnableSwitch;
    IBOutlet UIView *visitsAndTrxsSyncView;
    
    
    IBOutlet UILabel *CommunicationStringLabel;
    IBOutlet UILabel *CommunicationLastSyncedLabel;
    IBOutlet UISwitch *CommunicationSyncEnableSwitch;
    IBOutlet UIView *CommunicationSyncView;
    
    IBOutlet UILabel *defaultSyncLocationLabel;



}
- (IBAction)VisitsAndTransactionsSyncSettingsChanging:(id)sender;

-(IBAction)mediaFilesSyncButtonTapped:(id)sender;
@end
