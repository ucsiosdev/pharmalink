//
//  SalesWorxSyncUnconfirmedOrdersTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDescriptionLabel4.h"
#import "SalesWorxTitleLabel4.h"

#import "MedRepElementTitleLabel.h"
@interface SalesWorxSyncUnconfirmedOrdersTableViewCell : UITableViewCell
@property (strong,nonatomic)IBOutlet SalesWorxDescriptionLabel4 * orderNumberLabel;
@property (strong,nonatomic)IBOutlet SalesWorxDescriptionLabel4 * orderamountLabel;
@property (strong,nonatomic)IBOutlet SalesWorxDescriptionLabel4 * CustomerNameLabel;
@property (strong,nonatomic)IBOutlet SalesWorxDescriptionLabel4 * OrderDateLabel;

@property (strong,nonatomic)IBOutlet SalesWorxTitleLabel4 * docReferenceNumberStingLabel;
@property (strong,nonatomic)IBOutlet SalesWorxTitleLabel4 * orderamountStringLabel;
@property (strong,nonatomic)IBOutlet SalesWorxTitleLabel4 * CustomerNameStringLabel;
@property (strong,nonatomic)IBOutlet SalesWorxTitleLabel4 * OrderDateStringLabel;


@property (strong,nonatomic)IBOutlet UIImageView * selectionImageView;

@end
