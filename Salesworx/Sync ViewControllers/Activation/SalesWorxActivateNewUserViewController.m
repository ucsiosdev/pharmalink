//
//  SalesWorxActivateNewUserViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/19/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxActivateNewUserViewController.h"
#import "SWDefaults.h"
#import "SWFoundation.h"
#import "LoginViewController.h"
@interface SalesWorxActivateNewUserViewController ()

@end

@implementation SalesWorxActivateNewUserViewController
@synthesize isActivationNewUserFromLogin,isFromLogin;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self fetchSyncLocations];

    syncProgressBar.barFillColor=[UIColor clearColor];

    
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(RecieveSyncStatusUpdate:) name:KNewSyncNotificationNameStr object:nil];
    
    
    
    
    userDetailsDict=[[NSMutableDictionary alloc]init];
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = kActivateNewUserTitle;
    self.navigationItem.titleView=label;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }

    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }

    if (isActivationNewUserFromLogin==YES) {
        
        [activateButton setTitle:@"Upload Database" forState:UIControlStateNormal];
        usernameTextField.text=[[SWDefaults userProfile] stringForKey:@"Username"];
        
        
        passwordTextField.text=[SWDefaults passwordForActivate];
        
        [usernameTextField setIsReadOnly:YES];
        [passwordTextField setIsReadOnly:YES];
        [servernameTextField setIsReadOnly:YES];
        [serverLocationTextField setIsReadOnly:YES];
        
        
        
        
        CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
        NSError *error;
        NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *selectedServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
        NSLog(@"selected server dict in act %@", selectedServerDict);
        
        servernameTextField.text=[selectedServerDict valueForKey:@"name"];
        
        serverLocationTextField.text=[selectedServerDict valueForKey:@"url"];
        

        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(RecieveUploadDatabaseNotification:) name:kUploadDatabaseExistingUserNotificationName object:nil];
        
    }
    
    syncProgressBar.barFillColor=[UIColor clearColor];

    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    // NSString * avid = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    NSLog(@"version in new activation %@", avid);
    
    NSString*  ClientVersion =avid;
    
    
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    
    
    lblVersion.text= [NSString stringWithFormat:@"Version  %@ (%@)",ClientVersion,build];
    

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITextField Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==servernameTextField) {
       
        
        if (serverLocationsArray.count>0) {
            [self showServersPopover];
        }
        
        return NO;

    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if ([usernameTextField isFirstResponder]) {
        
        [usernameTextField resignFirstResponder];
        [passwordTextField becomeFirstResponder];
    }
    
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==usernameTextField) {
        [userDetailsDict setValue:textField.text forKey:@"User_Name"];
    }
   else if (textField==passwordTextField) {
        [userDetailsDict setValue:textField.text forKey:@"Password"];
    }
}

-(void)showServersPopover
{
    if (serverLocationsArray.count>0) {
        
        SyncLoctions * locations=[serverLocationsArray objectAtIndex:0];
        
        if ([NSString isEmpty:locations.name]==NO) {
            
            SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
            popOverVC.popOverContentArray=serverLocationsArray;
            
            popOverVC.popoverType=@"Activation";
            popOverVC.salesWorxPopOverControllerDelegate=self;
            popOverVC.disableSearch=YES;
            popOverVC.headerTitle=@"Servers";
            UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
            popoverController=nil;
            popoverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
            popoverController.delegate=self;
            popOverVC.popOverController=popoverController;
            
            [popoverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
            
            [popoverController presentPopoverFromRect:servernameTextField.dropdownImageView.frame inView:servernameTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else
        {
            UIAlertView * noLocationsAlert=[[UIAlertView alloc]initWithTitle:@"No Locations Available" message:@"Please connect to internet and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [noLocationsAlert show];
        }
        
        
    }
    
    
    else
    {
        
        UIAlertView * noLocationsAlert=[[UIAlertView alloc]initWithTitle:@"No Locations Available" message:@"Please connect to internet and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noLocationsAlert show];
    }
    

}

#pragma mark Server Selection delegate methods

-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    
    
    syncLocations=[serverLocationsArray objectAtIndex:selectedIndexPath.row];
    NSLog(@"selected popover is %@",syncLocations.name );
    NSMutableDictionary* selectedServerDictionary=[[NSMutableDictionary alloc]init];
    
    [selectedServerDictionary setValue:syncLocations.name forKey:@"name"];
    [selectedServerDictionary setValue:syncLocations.url forKey:@"url"];
    [selectedServerDictionary setValue:syncLocations.seq forKey:@"seq"];
    
    servernameTextField.text=syncLocations.name;
    serverLocationTextField.text=syncLocations.url;
    [userDetailsDict setValue:syncLocations.name forKey:@"Server_Name"];
    [userDetailsDict setValue:syncLocations.url forKey:@"Server_Url"];
    
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError * error;

    NSData *dataDict = [jsonSerializer serializeObject:selectedServerDictionary error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
    [SWDefaults setSelectedServerDictionary:stringDataDict];
    
    NSLog(@"setting server dict %@", stringDataDict);
    
    
    
}


#pragma mark Fetch server urls

-(void)fetchSyncLocations
{
    serverLocationsArray=[[NSMutableArray alloc]init];
    serverLocationsResponseArray=[[NSMutableArray alloc]init];
    
    NSString* customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    NSString* syncUrlString=[NSString stringWithFormat:@"http://www.ucssolutions.com/licman/get-sync-locations.php?cid=%@&avid=%@",customerID,@"19"];
    
    NSMutableURLRequest * syncUrlRequest=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:syncUrlString]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:syncUrlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
            {
            NSError* dataerror;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&dataerror];
                       if (!dataerror) {
                serverLocationsResponseArray=json.copy;
                for (NSInteger i=0; i<serverLocationsResponseArray.count; i++) {
                NSMutableDictionary * currentDict=[serverLocationsResponseArray objectAtIndex:i];
                syncLocations=[SyncLoctions new];
                syncLocations.name=[currentDict valueForKey:@"name"];
                syncLocations.seq=[currentDict valueForKey:@"seq"];
                syncLocations.url=[currentDict valueForKey:@"url"];
                [serverLocationsArray addObject:syncLocations];
            }
                                              
            }
                
        else{
            NSLog(@"error is %@", dataerror.debugDescription);
        }
            
                                          
            }];
    [dataTask resume];
    
}
#pragma mark Activation button Methods

-(void)startActivationProcess
{
    
   // [self.navigationItem setHidesBackButton:YES animated:YES];

    syncProgressBar.barInnerBorderColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    syncProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];

    if (isActivationNewUserFromLogin==YES) {
        
        //this is activation new user again, upload existing users database
        
        activationManager=[[SalesWorxActivationManager alloc]init];
        
        if ([activationManager licenseVerification])
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            currentSyncStatus=@"";
            lastSyncStausString=@"";
            [SWDefaults setLastSyncType:NSLocalizedString(@"Full Sync", nil)];
            activationProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
            
            NSLog(@"%@",[SWDefaults customer]);
            NSLog(@"License Verified .. OK");
            [self writeSyncLogToTextFile:[NSString stringWithFormat:@"User ID : %@",[[SWDefaults userProfile] stringForKey:@"Username"]] WithTimeStamp:NO];
            [self writeSyncLogToTextFile:[NSString stringWithFormat:@"Customer ID : %@ \n",[SWDefaults licenseIDForInfo]] WithTimeStamp:NO];
            
            [self writeSyncLogToTextFile:@"Sync log:" WithTimeStamp:NO];
            
            
            [activationManager prepareRequestForSyncUpload];
            
        }
        else
        {
            [self showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"Your license has been expired or contact your administrator for further assistance" withController:nil];
            [self.view setUserInteractionEnabled:YES];
            
        }
        
        
    }
    else
    {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        
        [SWDefaults setUsernameForActivate:usernameTextField.text];
        [SWDefaults setPasswordForActivate:passwordTextField.text];
        
        NSLog(@"user details is %@ %@",[SWDefaults usernameForActivate],[SWDefaults passwordForActivate]);
        
        
        NSString* userName=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"User_Name"]];
        NSString* password=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"Password"]];
        NSString* serverName=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"Server_Name"]];
        NSString* serverUrl=[SWDefaults getValidStringValue:[userDetailsDict valueForKey:@"Server_Url"]];
        
        NSString* missingString=[[NSString alloc]init];
        
        if ([NSString isEmpty:userName]==YES) {
            missingString=@"User Name";
        }
        else if ([NSString isEmpty:password]==YES) {
            missingString=@"password";
        }
        else  if ([NSString isEmpty:serverName]==YES) {
            missingString=@"Server Name";
        }
        else if ([NSString isEmpty:serverUrl]==YES) {
            missingString= @"Server Url";
        }
        
        if ([NSString isEmpty:missingString]==NO) {
            UIAlertView * missingAlert=[[UIAlertView alloc]initWithTitle:@"Missing Data" message:[NSString stringWithFormat:@"Please enter %@",missingString] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [missingAlert show];
            
        }
        else
        {
            //[self.view setUserInteractionEnabled:NO];
            activationManager=[[SalesWorxActivationManager alloc]init];
            
            if ([activationManager licenseVerification])
            {
                
                currentSyncStatus=@"";
                lastSyncStausString=@"";
                [SWDefaults setLastSyncType:NSLocalizedString(@"Full Sync", nil)];
                activationProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
                
                NSLog(@"%@",[SWDefaults customer]);
                NSLog(@"License Verified .. OK");
                [self writeSyncLogToTextFile:[NSString stringWithFormat:@"User ID : %@",[[SWDefaults userProfile] stringForKey:@"Username"]] WithTimeStamp:NO];
                [self writeSyncLogToTextFile:[NSString stringWithFormat:@"Customer ID : %@ \n",[SWDefaults licenseIDForInfo]] WithTimeStamp:NO];
                
                [self writeSyncLogToTextFile:@"Sync log:" WithTimeStamp:NO];
                
                [activationManager sendRequestforActivate:userDetailsDict];
                
                //[syncManager prepareRequestForSyncUpload];
                
            }
            else
            {
                [self showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"Your license has been expired or contact your administrator for further assistance" withController:nil];
                [self.view setUserInteractionEnabled:YES];
                
            }
            
        }
    }

}

-(IBAction)activateNewUserTapped:(id)sender
{
    
    [self startActivationProcess];
}


-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:alertTitle
                                      message:alertMessage
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}


-(void)writeSyncLogToTextFile:(NSString *)textString WithTimeStamp:(BOOL)isTimeStampRequired
{
    if([lastSyncStausString isEqualToString:textString])
    {
        
    }
    else
    {
        lastSyncStausString=textString;
        //get the documents directory:
        NSString *slashN = @"\n";
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *dateString =  [formatter stringFromDate:[NSDate date]];
        if(isTimeStampRequired)
        {
            slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@: %@",dateString , textString]];
            
        }
        else
        {
            slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ " , textString]];
            
        }
        currentSyncStatus=[currentSyncStatus stringByAppendingString:slashN];
        
        //iOS 8 support
        NSString *documentsDirectory;
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [NSString stringWithFormat:@"%@/Sync_Log.txt",documentsDirectory];
        
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
        [fileHandler seekToEndOfFile];
        [fileHandler writeData:[slashN dataUsingEncoding:NSUTF8StringEncoding]];
        [fileHandler closeFile];
        formatter=nil;
        usLocale=nil;
    }
    
}


- (void)RecieveSyncStatusUpdate:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:KNewSyncNotificationNameStr])
    {
        NSDictionary *statusDic = (NSDictionary *)notification.object;
        //[self showSyncProgress];
        
        
        if([[statusDic valueForKey:KSyncDic_ShowAlertStr]isEqualToString:KSync_YesCode])
        {
            [self showAlertAfterHidingKeyBoard:NSLocalizedString(KSync_ErrorAlertTitleStr, nil) andMessage:[statusDic valueForKey:KSync_AlertMessageStr] withController:nil];
            //[self updateLastSyncDetails:NO];
            syncStatusLbl.text = [KSync_SyncStausStr stringByAppendingString:NSLocalizedString(@"Failed", nil)];
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSync_AlertMessageStr] WithTimeStamp:YES];
            [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Failed"] WithTimeStamp:YES];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        else
        {
            syncStatusLbl.text=[statusDic valueForKey:KSyncDic_SyncStausStr];
            syncProgressBar.progress=[[statusDic valueForKey:KSyncDic_SyncProgressStr] floatValue];
            NSLog (@"Successfully received the NewSyncStatusNotification!");
            NSLog(@"sync progress is %@",[statusDic valueForKey:@"SyncProgress"]);
            
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSyncDic_SyncStausStr] WithTimeStamp:YES];
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_DatabaseUploadedStatusStr])
            {
                [activationManager sendRequestForInitiateDownloadDatabase];
            }
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_CompleteStatusStr])
            {
               // [self updateLastSyncDetails:YES];
                // [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Successful"] WithTimeStamp:YES];
                NSLog(@"Activation completed successfully");
                [[NSNotificationCenter defaultCenter] removeObserver:self name:KNewSyncNotificationNameStr object:nil];
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                NSDateFormatter *formatter = [NSDateFormatter new];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [formatter setLocale:usLocale];
                NSString *dateString =  [formatter stringFromDate:[NSDate date]];
                [SWDefaults setLastSyncDate:dateString];
                
                //set login view controller
                
                if (isFromLogin==YES) {
                    
                    UIAlertView * successAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"User Activated Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    successAlert.tag=201;
                    [successAlert show];
                    
                    
                }
                else
                {
                    
//                    UIAlertView * successAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"User Activated Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                    successAlert.tag=202;
//                    [successAlert show];
                    
                    LoginViewController *loginVC = [[LoginViewController alloc] init];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
                    [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
                    

                }
                
                
              

            }
            
        }
    }
}


#pragma mark Upload database notification method

- (void)RecieveUploadDatabaseNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kUploadDatabaseExistingUserNotificationName])
    {
        NSDictionary *statusDic = (NSDictionary *)notification.object;
        //[self showSyncProgress];
        //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        
        if([[statusDic valueForKey:KSyncDic_ShowAlertStr]isEqualToString:KSync_YesCode])
        {
            [self showAlertAfterHidingKeyBoard:NSLocalizedString(KSync_ErrorAlertTitleStr, nil) andMessage:[statusDic valueForKey:KSync_AlertMessageStr] withController:nil];
            //[self updateLastSyncDetails:NO];
            syncStatusLbl.text = [KSync_SyncStausStr stringByAppendingString:NSLocalizedString(@"Failed", nil)];
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSync_AlertMessageStr] WithTimeStamp:YES];
            [self writeSyncLogToTextFile:[KSync_SyncStausStr stringByAppendingString:@"Failed"] WithTimeStamp:YES];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        else
        {
            syncStatusLbl.text=[statusDic valueForKey:KSyncDic_SyncStausStr];
            syncProgressBar.progress=[[statusDic valueForKey:KSyncDic_SyncProgressStr] floatValue];
            NSLog (@"Successfully received the NewSyncStatusNotification!");
            NSLog(@"sync progress is %@",statusDic);
            
            [self writeSyncLogToTextFile:[statusDic valueForKey:KSyncDic_SyncStausStr] WithTimeStamp:YES];
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KSync_DatabaseUploadedStatusStr])
            {
               // [activationManager sendre];
            }
            if([[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString:KAllFilesUploadedStr]
               ||[[statusDic valueForKey:KSyncDic_SyncStausStr]isEqualToString: KNoFilesToUploadedStr])
            {
                
                NSLog(@"upload database successful");
                
                usernameTextField.text=@"";
                passwordTextField.text=@"";
                servernameTextField.text=@"";
                serverLocationTextField.text=@"";
                [activateButton setTitle:@"Activate" forState:UIControlStateNormal];
                syncProgressBar.progress=0.0;
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                //delete previous users database
                
                NSString * databasePath=[SWDefaults dbGetDatabasePath];
                
                NSString* databaseZipFilePath=[SWDefaults dbGetDatabaseZipFilePath];
                
                NSError * error;
                
                NSError * databaseError;
                
                BOOL databaseDeleted=NO;
                
                BOOL databaseZipFileDeleted=[[NSFileManager defaultManager]removeItemAtPath:databaseZipFilePath error:&error];
                if (databaseZipFileDeleted==YES) {
                    
                    NSLog(@"database zip file deleted");
                    
                    databaseDeleted=[[NSFileManager defaultManager]removeItemAtPath:databasePath error:&databaseError];
                    
                    if (databaseDeleted==YES) {
                        
                        NSLog(@"database file deleted");
                    }
                    
                }

                
                
                
                
                syncStatusLbl.text=@"";
                
                if (!databaseUploadSuccessAlert) {
                               databaseUploadSuccessAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Database Uploaded Successfully, please enter credentials to activate new user" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                databaseUploadSuccessAlert.tag=999;
                [databaseUploadSuccessAlert show];
                }
                
                
            }
            
        }
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {

    if (alertView.tag==999) {
        databaseUploadSuccessAlert=nil;
    }
    //do whatever with the result
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==999) {
        isActivationNewUserFromLogin=NO;
        
        
        [usernameTextField setIsReadOnly:NO];
        [passwordTextField setIsReadOnly:NO];
        [servernameTextField setIsReadOnly:NO];
        [serverLocationTextField setIsReadOnly:NO];
        

        
    }
    else if (alertView.tag==201)
    {
        [self.navigationController popViewControllerAnimated:YES];

    }
}

@end
