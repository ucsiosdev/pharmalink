//
//  SalesWorxSyncUnconfirmedOrdersViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxSyncUnconfirmedOrdersViewController.h"
#import "MBProgressHUD.h"
@interface SalesWorxSyncUnconfirmedOrdersViewController ()

@end

@implementation SalesWorxSyncUnconfirmedOrdersViewController
@synthesize syncType;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    proformaOrdersArray=[[NSMutableArray alloc]init];
    selectedIndexpathsArray=[[NSMutableArray alloc]init];
    proformaOrdersDataArray=[[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setHiddenAnimated:NO duration:1.25];
    proformaOrdersDataArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT A.*,B.Customer_Name FROM TBL_Proforma_Order AS A Inner Join TBL_Customer AS B where A.Ship_To_Customer_ID=B.Customer_ID"] ;
    NSMutableArray *tempProformaOrdersArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0  ;i<proformaOrdersDataArray.count   ;i++) {
        
        NSDictionary *proformaOrder=[proformaOrdersDataArray objectAtIndex:i];
        Customer *customerDetails=[[Customer alloc]init];
        customerDetails.Customer_Name=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Customer_Name"]];

        NSMutableArray *customerInvoiceDetails = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Sales_District_ID,A.Postal_Code ,A.Location,A.Dept,A.Customer_Segment_ID,B.Customer_NO,B.Customer_barCode, A.Cust_lat,A.Cust_long, A.City,A.Address,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  where A.Customer_ID='%@' and A.Site_Use_ID='%@' ORDER BY A.Customer_Name",[proformaOrder valueForKey:@"Ship_To_Customer_ID"],[proformaOrder valueForKey:@"Ship_To_Site_ID"]]];
        if(customerInvoiceDetails.count==0)
        {
            customerDetails.Customer_ID=@"";
            customerDetails.Site_Use_ID=@"";
        }
        else
        {
            customerDetails.Customer_ID=[SWDefaults getValidStringValue:[[customerInvoiceDetails valueForKey:@"Customer_ID"] objectAtIndex:0]];
            customerDetails.Site_Use_ID=[SWDefaults getValidStringValue:[[customerInvoiceDetails valueForKey:@"Site_Use_ID"] objectAtIndex:0]];

        }
        if(customerDetails.Customer_ID.length==0 || customerDetails.Site_Use_ID==0)
        {
        
        }
        else
        {
        [proformaOrder setValue:customerDetails.Customer_ID forKey:@"Customer_ID"];
        [proformaOrder setValue:customerDetails.Site_Use_ID forKey:@"Site_Use_ID"];
        [proformaOrdersDataArray replaceObjectAtIndex:i withObject:proformaOrder];

        proformaOrder=[proformaOrdersDataArray objectAtIndex:i];
        ManageOrderDetails *mangeOrder=[[ManageOrderDetails alloc]init];
        mangeOrder.OrderAmount=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Order_Amt"]];
        mangeOrder.manageOrderStatus=KUnConfirmOrderStatusString;
        mangeOrder.Orig_Sys_Document_Ref=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Orig_Sys_Document_Ref"]];
        mangeOrder.Row_ID=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Row_ID"]];
        mangeOrder.Visit_ID=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Visit_ID"]];
        mangeOrder.FOCOrderNumber=nil;
        mangeOrder.FOCParentOrderOrderNumber=nil;
        mangeOrder.Creation_Date=[SWDefaults getValidStringValue:[proformaOrder objectForKey:@"Last_Update_Date"]];
        mangeOrder.customerDetails=customerDetails;
        [tempProformaOrdersArray addObject:mangeOrder];
        }
        
    }
    proformaOrdersArray=tempProformaOrdersArray;
    [proformaListContentView setBackgroundColor:UIViewControllerBackGroundColor];
    proformaListContentView.layer.cornerRadius=5.0f;
}
-(void)viewDidAppear:(BOOL)animated
{
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
    
    
    
    
}
-(IBAction)BackButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;
        
    }
    else
    {
        transition.type = kCATransitionFade;
        
    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = proformaListContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

#pragma mark UITableViewDelegateMethods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   // return 60.0f;
    return 0.01f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 108.0f;
    
}
//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
////    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60.0f)];
////    UnconfirmedOrdersTableHeaderView.backgroundColor=kUITableViewHeaderBackgroundColor;
////    view.backgroundColor=[UIColor whiteColor];
////    [view addSubview:UnconfirmedOrdersTableHeaderView];
////        return view;
//
//    
//    return UnconfirmedOrdersTableHeaderView;
//}   // custom view for header. will be adjusted to default or specified header height



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return proformaOrdersArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"UnconfirmedOrdersTableViewCell";
    SalesWorxSyncUnconfirmedOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSyncUnconfirmedOrdersTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    ManageOrderDetails *manageOrder=[proformaOrdersArray objectAtIndex:indexPath.row];
    
    
    cell.orderamountLabel.text=[manageOrder.OrderAmount currencyString];
    cell.orderNumberLabel.text=manageOrder.Orig_Sys_Document_Ref;
    cell.OrderDateLabel.text=manageOrder.Creation_Date;
    cell.CustomerNameLabel.text=manageOrder.customerDetails.Customer_Name;
    
    if([selectedIndexpathsArray containsObject:indexPath])
    {
        cell.selectionImageView.image=[UIImage imageNamed:@"Sync_ GreenTickMark"];
        
    }
    else
    {
        cell.selectionImageView.image=[UIImage imageNamed:@"Sync_GreenUnTickMark"];

    }
    
    
    return cell;
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([selectedIndexpathsArray containsObject:indexPath])
    {
        SalesWorxSyncUnconfirmedOrdersTableViewCell *cell=(SalesWorxSyncUnconfirmedOrdersTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        [selectedIndexpathsArray removeObject:indexPath];
        cell.selectionImageView.image=[UIImage imageNamed:@"Sync_GreenUnTickMark"];
    }
    else
    {
        SalesWorxSyncUnconfirmedOrdersTableViewCell *cell=(SalesWorxSyncUnconfirmedOrdersTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        [selectedIndexpathsArray addObject:indexPath];
        [UIView transitionWithView:cell.selectionImageView
                          duration:0.2f
                           options:UIViewAnimationOptionTransitionFlipFromTop
                        animations:^{
                            cell.selectionImageView.image=[UIImage imageNamed:@"Sync_ GreenTickMark"];
                        } completion:NULL];
    }
  
}
-(IBAction)DoneButtonTapped:(id)sender
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        for (NSInteger j=0; j<selectedIndexpathsArray.count; j++) {
            NSIndexPath *selectedIndexpath=  [selectedIndexpathsArray objectAtIndex:j];
           // [[SWDatabaseManager retrieveManager] saveConfirmedOrder:[proformaOrdersDataArray objectAtIndex:selectedIndexpath.row]];
            
          BOOL isSucess=  [self ConfirmProfrmaOrder:[[proformaOrdersDataArray objectAtIndex:selectedIndexpath.row] valueForKey:@"Orig_Sys_Document_Ref"]];
            if(isSucess)
            {
                [[SWDatabaseManager retrieveManager]deleteSalesPerformaOrderItems:[[proformaOrdersDataArray objectAtIndex:selectedIndexpath.row] stringForKey:@"Orig_Sys_Document_Ref"]];

            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.navigationController popViewControllerAnimated:NO];
            [_salesWorxSyncUnconfirmedOrdersViewControllerDelegate ordersConfirmationCompletedForSyncType:syncType];
        });

    });

}
-(BOOL)ConfirmProfrmaOrder:(NSString *)proformaOrderDocRefNumber
{
    NSDictionary *proformaTblOrderDBData=[[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select C.Site_Use_ID, P.* from TBL_Proforma_Order AS P LEFT JOIN TBL_Customer AS C ON P.Ship_To_Customer_ID=C.Customer_ID where Orig_Sys_Document_Ref='%@'",proformaOrderDocRefNumber]] objectAtIndex:0];
    
    NSMutableArray *proformaTblOrderLineItemsDBData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",proformaOrderDocRefNumber]];

    
    NSMutableArray *proformaTblLotsDBData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Lots where Orig_Sys_Document_Ref='%@'",proformaOrderDocRefNumber]];

    
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    NSDictionary *userInfo=[SWDefaults userProfile];
    NSString *orderID = [NSString createGuid];
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    if(nUserId.length == 1)
    {nUserId = [NSString stringWithFormat:@"00%@",nUserId];}
    else if(nUserId.length == 2)
    {nUserId = [NSString stringWithFormat:@"0%@",nUserId];}
    
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
    
    if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
    {
        [SWDefaults setLastOrderReference:lastOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
        if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
        {
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
    }
    NSString *dateString =  [MedRepQueries fetchDatabaseDateFormat];
    NSString *dateTimeString =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];

    
    
   NSString *  InsertOrderQuery= @"INSERT INTO TBL_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number,Shipping_Instructions,Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    
    NSMutableArray *insertOrderParameters=[[NSMutableArray alloc]initWithObjects:
                                           orderID,
                                           lastOrderReference,
                                           [proformaTblOrderDBData valueForKey:@"Creation_Date"],
                                           [proformaTblOrderDBData valueForKey:@"Created_By"],
                                           [proformaTblOrderDBData valueForKey:@"Ship_To_Customer_ID"] ,
                                           [proformaTblOrderDBData valueForKey:@"Ship_To_Site_ID"] ,
                                           [proformaTblOrderDBData valueForKey:@"Ship_To_Customer_ID"] ,
                                           [proformaTblOrderDBData valueForKey:@"Site_Use_ID"] ,
                                           @"",/**Comments*/
                                           dateString,
                                           dateString,
                                           @"N",
                                           [proformaTblOrderDBData valueForKey:@"Start_Time"],
                                           dateTimeString,
                                           @"",
                                           @"[SIGN:N]",
                                           [proformaTblOrderDBData valueForKey:@"Emp_Code"],
                                           [proformaTblOrderDBData valueForKey:@"Order_Amt"],
                                           [proformaTblOrderDBData valueForKey:@"Visit_ID"],
                                           dateString,
                                           [proformaTblOrderDBData valueForKey:@"Credit_Customer_ID"],
                                           [proformaTblOrderDBData valueForKey:@"Credit_Site_ID"],
                                           dateString,
                                           [userInfo stringForKey:@"SalesRep_ID"],
                                           [proformaTblOrderDBData valueForKey:@"Custom_Attribute_2"],
                                           [proformaTblOrderDBData valueForKey:@"Custom_Attribute_3"],
                                           @"0.0",
                                           @"",
                                           [proformaTblOrderDBData valueForKey:@"Custom_Attribute_1"],
                                           [NSNull null],
                                           nil];
    
    BOOL isOrderDetailsSaved= [database executeUpdate:InsertOrderQuery withArgumentsInArray:insertOrderParameters];
    
    if(isOrderDetailsSaved)
    {
        NSLog(@"Saved success");
    }
    
    BOOL orderLineItemsSaved=NO;
    for (NSInteger i = 0; i < proformaTblOrderLineItemsDBData.count; i++)
    {
        
        NSDictionary *orderItem=[proformaTblOrderLineItemsDBData objectAtIndex:i];
        NSString *OrderLineItemQuery =/* kSQLSaveOrderLineItem*/@"INSERT INTO TBL_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID, Unit_Selling_Price, Def_Bonus,Calc_Price_Flag, Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2,'Assortment_Plan_ID',VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code, Custom_Attribute_4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        
        
        NSMutableArray *insertOrderLineItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                       [NSString createGuid],
                                                       lastOrderReference,
                                                       [orderItem valueForKey:@"Line_Number"],
                                                       [orderItem valueForKey:@"Order_Quantity_UOM"],
                                                       [orderItem valueForKey:@"Display_UOM"],
                                                       [orderItem valueForKey:@"Ordered_Quantity"] ,
                                                       [orderItem valueForKey:@"Inventory_Item_ID"] ,
                                                       [orderItem valueForKey:@"Unit_Selling_Price"] ,
                                                       [orderItem valueForKey:@"Def_Bonus"],
                                                       [orderItem valueForKey:@"Calc_Price_Flag"],
                                                       [orderItem valueForKey:@"Custom_Attribute_3"],
                                                       [orderItem valueForKey:@"Custom_Attribute_1"],
                                                       [orderItem valueForKey:@"Custom_Attribute_2"],
                                                       [orderItem valueForKey:@"Assortment_Plan_ID"],
                                                       [orderItem valueForKey:@"VAT_Amount"],
                                                       [orderItem valueForKey:@"VAT_Percentage"],
                                                       [orderItem valueForKey:@"VAT_Level"],
                                                       [orderItem valueForKey:@"VAT_Code"],
                                                       [orderItem valueForKey:@"Custom_Attribute_4"],
                                                       nil];

        
        BOOL orderLineItemSaved= [database executeUpdate:OrderLineItemQuery withArgumentsInArray:insertOrderLineItemParameters];
        if(!orderLineItemSaved)
        {
            orderLineItemsSaved=NO;
            break;
        }
        else
        {
            orderLineItemsSaved=YES;
        }
    }
    
    
    BOOL insertLotStatus=YES;
    for (NSInteger j = 0; j < proformaTblLotsDBData.count; j++)
    {
        NSDictionary *assignLot=[proformaTblLotsDBData objectAtIndex:j];
        NSString *InsertLotQuery = /*kSQLSaveOrderLotItem */@"INSERT INTO TBL_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES (?,?,?,?,?,?,?)";
        NSMutableArray *InsertLotItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                 [NSString createGuid],
                                                 lastOrderReference,
                                                 [assignLot valueForKey:@"Line_Number"],
                                                 [assignLot valueForKey:@"Lot_Number"],
                                                 [assignLot valueForKey:@"Ordered_Quantity"],
                                                 [assignLot valueForKey:@"Is_FIFO"] ,
                                                 [assignLot stringForKey:@"Org_ID"],
                                                 nil];
        insertLotStatus= [database executeUpdate:InsertLotQuery withArgumentsInArray:InsertLotItemParameters];
        if(!insertLotStatus)
        {
            orderLineItemsSaved=NO;
            break;
        }
        
    }
    
    
    BOOL isOrderHistorySaved=  [self SaveProformaOrderDetailsIntoOrderhistorttable:proformaTblOrderDBData WithOrderLineItems:proformaTblOrderLineItemsDBData DatabaseObj:database WithOrderRowID:orderID andOrderRef:lastOrderReference];
    
    if(!isOrderDetailsSaved ||!orderLineItemsSaved  || !isOrderHistorySaved)
    {
        [database rollback];
        [SWDefaults showAlertAfterHidingKeyBoard:@"DB Error" andMessage:@"Unable to save order history" withController:self];
        
        return NO;
    }
    else
    {
        [SWDefaults updateGoogleAnalyticsEvent:kSalesOrderTakenEventName];
        [SWDefaults updateGoogleAnalyticsEvent:kSalesOrderTakenFromUnConfirmedOrdersPopOverEventName];

        [database commit];
        [database close];
        return YES;
    }

    

}
-(BOOL)SaveProformaOrderDetailsIntoOrderhistorttable:(NSDictionary *)proformaTblOrderDBData WithOrderLineItems:(NSMutableArray *)proformaTblOrderLineItemsDBData DatabaseObj:(FMDatabase *)database WithOrderRowID:(NSString *)OrderRowID andOrderRef:(NSString *)lastOrderReference
{
    
    
    NSDictionary *userInfo=[SWDefaults userProfile];
    
    
    NSString * dummyTimeStr=@" 00:00:00";
    NSString *dateString =  [MedRepQueries fetchDatabaseDateFormat] ;
    dateString=[dateString stringByAppendingString:dummyTimeStr];
    NSString *dateTimeString =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] ;
    NSDateComponents *components=[gregCalendar components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSDate *lastDateofMonth= [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    NSString *lastDateMonthString =  [formatter stringFromDate:lastDateofMonth];

    
    
    NSString *        OrderHistoryInsertQuery = @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date, Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp, Custom_Attribute_2,Custom_Attribute_3,Custom_Attribute_6) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    
    NSMutableArray *insertOrderParameters=[[NSMutableArray alloc]initWithObjects:
                                           OrderRowID,
                                           lastOrderReference,
                                           @"I",
                                           dateString,
                                           [proformaTblOrderDBData stringForKey:@"Created_By"],
                                           dateString,
                                           dateString ,
                                           dateString,
                                           [proformaTblOrderDBData valueForKey:@"Ship_To_Customer_ID"] ,
                                           [proformaTblOrderDBData valueForKey:@"Ship_To_Site_ID"],
                                           [proformaTblOrderDBData valueForKey:@"Ship_To_Customer_ID"],
                                           [proformaTblOrderDBData valueForKey:@"Site_Use_ID"] ,
                                           @"N",
                                           [proformaTblOrderDBData valueForKey:@"Start_Time"],
                                           dateTimeString,
                                           @"",
                                           [userInfo stringForKey:@"Emp_Code"],
                                           @"N",
                                           [proformaTblOrderDBData valueForKey:@"Order_Amt"],
                                           lastDateMonthString,
                                           [proformaTblOrderDBData valueForKey:@"Credit_Customer_ID"],
                                           [proformaTblOrderDBData valueForKey:@"Credit_Site_ID"],
                                           [proformaTblOrderDBData valueForKey:@"Visit_ID"],
                                           dateString,
                                           nil];
    

        [insertOrderParameters addObject:[proformaTblOrderDBData valueForKey:@"Custom_Attribute_2"]];
        [insertOrderParameters addObject:[proformaTblOrderDBData valueForKey:@"Custom_Attribute_3"]];
        [insertOrderParameters addObject:[proformaTblOrderDBData valueForKey:@"Custom_Attribute_1"]];
        
    
    BOOL isOrderHistoryDetailsSaved= [database executeUpdate:OrderHistoryInsertQuery withArgumentsInArray:insertOrderParameters];
    
    BOOL orderHistoryLineItemsSaved=NO;
    for (NSInteger i = 0; i < proformaTblOrderLineItemsDBData.count; i++)
    {
        
        NSDictionary *orderItem=[proformaTblOrderLineItemsDBData objectAtIndex:i];
        
        
        NSString *OrderHistoryLineItemQuery = /*kSQLSaveOrderHistoryLineItem*/@"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3,Assortment_Plan_ID,VAT_Amount,VAT_Percentage,VAT_Level,VAT_Code,Custom_Attribute_6) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        double iTemValue = ([[orderItem valueForKey:@"Ordered_Quantity"] doubleValue] * [[orderItem valueForKey:@"Unit_Selling_Price"] doubleValue])-([[orderItem valueForKey:@"Ordered_Quantity"] doubleValue] * [[orderItem valueForKey:@"Unit_Selling_Price"] doubleValue] *[[SWDefaults getValidStringValue:[orderItem valueForKey:@"Custom_Attribute_3"]] doubleValue]);

        NSMutableArray *insertOrderLineItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                       [NSString createGuid],
                                                       lastOrderReference,
                                                       [orderItem valueForKey:@"Line_Number"],
                                                       [orderItem valueForKey:@"Inventory_Item_ID"] ,
                                                       [orderItem valueForKey:@"Order_Quantity_UOM"],
                                                       [orderItem valueForKey:@"Ordered_Quantity"],
                                                       [orderItem valueForKey:@"Calc_Price_Flag"],
                                                       [orderItem valueForKey:@"Unit_Selling_Price"] ,
                                                       [[[NSString stringWithFormat:@"%0.2f",iTemValue]  floatString] AmountStringWithOutComas],
                                                       @"",
                                                       @"",
                                                       nil];

            [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"Custom_Attribute_1"]];
            [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"Custom_Attribute_2"]];
            [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"Custom_Attribute_3"]];
            [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"Assortment_Plan_ID"]];
        
        [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"VAT_Amount"]];
        [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"VAT_Percentage"]];
        [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"VAT_Level"]];
        [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"VAT_Code"]];
        [insertOrderLineItemParameters addObject:[orderItem valueForKey:@"Custom_Attribute_6"]];

        
        NSLog(@"insert order line items %@", insertOrderParameters);
        
        
        BOOL orderHistoryLineItemSaved= [database executeUpdate:OrderHistoryLineItemQuery withArgumentsInArray:insertOrderLineItemParameters];
        
        if(!orderHistoryLineItemSaved)
        {
            orderHistoryLineItemsSaved=NO;
            break;
        }
        else
        {
            orderHistoryLineItemsSaved=YES;
        }
    }
    
    if(orderHistoryLineItemsSaved && isOrderHistoryDetailsSaved)
        return YES;
    
    return NO;
}
- (FMDatabase *)getDatabase
{
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    
    //    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}

@end
