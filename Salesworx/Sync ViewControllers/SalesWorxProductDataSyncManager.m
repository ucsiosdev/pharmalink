//
//  SalesWorxProductDataSyncManager.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/23/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxProductDataSyncManager.h"

@implementation SalesWorxProductDataSyncManager


-(void)prepareRequestForFetchProductData
{
    NSLog(@"fetcjing stock data");
    
    NSDictionary * procedureParametersDict=[self getProcedureDictionaryForStockSync];
    
    NSString* userNameString= [[SWDefaults userProfile]valueForKey:@"Username"];

    NSString* passwordString= [[SWDefaults userProfile]valueForKey:@"Password"];

    
    [self sendRequestForDownloadDataExecWithUserName:userNameString andPassword:passwordString andProcName:kStockSyncProcedureName andProcDic:procedureParametersDict];
    
}


-(NSDictionary *)getProcedureDictionaryForStockSync
{
//    if([SWDefaults lastFSRStockBackGroundSyncDate]==nil)
//    {
//        lastSynctimeStr=[SWDefaults lastFullSyncDate];
//    }
//    else if ([[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFSRStockBackGroundSyncDate]]] == NSOrderedAscending)
//    {
//        lastSynctimeStr=[SWDefaults lastFSRStockBackGroundSyncDate];
//        
//    }
//    else
//    {
//        lastSynctimeStr=[SWDefaults lastFullSyncDate];
//    }
    
    
    //the last sync time should be based on last_Updated_At from tbl_product_Stock
    
    lastSynctimeStr=[[SWDatabaseManager retrieveManager]fetchLastStockSyncDate];
    
    //lastSynctimeStr=@"2016-01-01 01:01:01";
    
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"SalesRepID",@"LastSyncTime", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"],lastSynctimeStr,nil];
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
    
}


- (void)sendRequestForDownloadDataExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];

    if (checkInternetConnectivity) {
        SWAppDelegate * appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
        appDelegate.currentExceutingDataDownloadProcess=kDataDownloadProcess_PRODSTK;
        
        
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"SalesRepID",@"LastSyncTime", nil];
    
    for (NSInteger i=0; i<procedureParametersArray.count; i++) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];
    }
    
        
        NSString *encode = [strParams stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSLog(@"encoded url is %@", encode);
        
        NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
        NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
        
        NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,password,strDeviceID,ClientVersion,@"Get_FSR_ProductStock",[testServerDict stringForKey:@"name"],strName,encode];
        
        NSLog(@"request string for stock sync %@",myRequestString);
        
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"background sync Response %@",resultArray);
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             //[SWDefaults setlastFSRStockBackGroundSyncDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
             NSMutableArray * stockSyncProductsArray=[[NSMutableArray alloc] init];
             
             for (NSInteger i=0; i<resultArray.count; i++) {
                 
                 NSMutableDictionary * currentProductDict=[resultArray objectAtIndex:i];
                 SalesWorxProductStockSync * currentProduct=[[SalesWorxProductStockSync alloc] init];
                 
                 currentProduct.Custom_Attribute_1=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Custom_Attribute_1"]];
                 currentProduct.Custom_Attribute_2=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Custom_Attribute_2"]];
                 currentProduct.Custom_Attribute_3=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Custom_Attribute_3"]];
                 currentProduct.Custom_Attribute_4=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Custom_Attribute_4"]];
                 currentProduct.Custom_Attribute_5=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Custom_Attribute_5"]];
                 currentProduct.Custom_Attribute_6=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Custom_Attribute_6"]];
                 
                 
                 NSString* refinedExpiryDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Expiry_Date"]]];
                 
                // NSLog(@"refined expiry date is %@", refinedExpiryDate);
                 
                 
                 NSString * refinedLastUpdatedAt=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Last_Updated_At"]]];
                 
                 //NSLog(@"refined last updated date is %@", refinedLastUpdatedAt);

                 currentProduct.Expiry_Date=[SWDefaults getValidStringValue:refinedExpiryDate];
                 currentProduct.Item_ID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Item_ID"]];
                 
                 
                 currentProduct.Last_Updated_At=[SWDefaults getValidStringValue:refinedLastUpdatedAt];
                 currentProduct.Lot_No=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Lot_No"]];
                 currentProduct.Lot_Qty=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Lot_Qty"]];
                 currentProduct.Org_ID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Org_ID"]];
                 
                 
                 NSString * refinedStockID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Stock_ID"]];
                 
                 currentProduct.Stock_ID=[refinedStockID uppercaseString];
                 
                 [stockSyncProductsArray addObject:currentProduct];
                 
                 
             }
             [SWDefaults setLastProductStockBackGroundSyncDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];

             BOOL status=[[SWDatabaseManager retrieveManager] updateStockfromStockSync:stockSyncProductsArray];
             NSLog(@"stock sync status %hhd", status);
             appDelegate.currentExceutingDataDownloadProcess=kDataDownloadProcess_NONE;

         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@" background sync error  sendRequestForDownloadDataExecWithUserName in SalesWorxProductDataSyncManager%@",error);
         appDelegate.currentExceutingDataDownloadProcess=kDataDownloadProcess_NONE;

     }];
    
    //call start on your request operation
    [operation start];
    }
    
    else{
        NSLog(@"tried to start product data fetch background process, but internet unavailable");
    }
}

-(NSString*)fetchStringByReplacingT:(NSString*)sourceString
{
    NSString* refinedString=[[NSString alloc]init];
    
    if ([sourceString containsString:@"T"]) {
        refinedString=[sourceString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        return refinedString;
    }
    else{
       return sourceString;
    }
    
}



@end
