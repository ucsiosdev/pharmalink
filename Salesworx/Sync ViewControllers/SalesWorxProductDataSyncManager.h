//
//  SalesWorxProductDataSyncManager.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/23/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWDatabaseManager.h"
#import "AFHTTPRequestOperation.h"
#import "SWDefaults.h"
#import "SharedUtils.h"
#import "SWAppDelegate.h"

@interface SalesWorxProductDataSyncManager : NSObject
{
    NSString *lastSynctimeStr;
}
- (void)sendRequestForDownloadDataExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic;
-(void)prepareRequestForFetchProductData;

@end
