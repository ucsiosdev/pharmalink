//
//  SalesWorxMediaSyncViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxMediaSyncViewController.h"
#import "AppControl.h"

@interface SalesWorxMediaSyncViewController ()

@end

@implementation SalesWorxMediaSyncViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self showMediaSyncStatus];
    SyncTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(showMediaSyncStatus) userInfo:nil repeats:YES];
    
    mediaSyncProgressBar.barBorderWidth=2.0;
    mediaSyncProgressBar.barBorderColor=UIViewControllerBackGroundColor;
    mediaSyncProgressBar.barBackgroundColor=UIViewControllerBackGroundColor;
    mediaSyncProgressBar.barFillColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    mediaSyncProgressBar.barInnerBorderColor=[UIColor colorWithRed:(71/255.0) green:(144/255.0) blue:(210/255.0) alpha:1];
    
    
    mediaSyncProgressBar.usesRoundedCorners=YES;

    
    [mediaSyncPopoverContentView setHidden:YES];
    
    isSyncAnimatedImageShowing=NO;
    
    mediaFilesStringLabel.textColor=MedRepMenuTitleFontColor;
    mediaLastSyncedLabel.textColor=MedRepMenuTitleDescFontColor;
    animatedSyncGifData=[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sync-animated" ofType:@"gif"]];
    visitsAndTrxsStringLabel.textColor=MedRepMenuTitleFontColor;
    visitsAndTrxsLastSyncedLabel.textColor=MedRepMenuTitleDescFontColor;
    CommunicationStringLabel.textColor=MedRepMenuTitleFontColor;
    CommunicationLastSyncedLabel.textColor=MedRepMenuTitleDescFontColor;

    
    [mediaSyncPopoverContentView setBackgroundColor:UIViewControllerBackGroundColor];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(GetSyncStateFromBackground:) name:@"GetSyncStateFromBackground" object:nil];
}
- (void)GetSyncStateFromBackground:(NSNotification *)notification{
    AppControl *appcontrol=[AppControl retrieveSingleton];
    NSLog(@"application entered foreground");
    if([appcontrol.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsNOCode])
    {
        visitsAndTrxsEnableSwitch.enabled = NO;
    }else{
        visitsAndTrxsEnableSwitch.enabled = YES;
    }
    
    if([appcontrol.USER_ENABLED_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode] || [[SWDefaults VisitsAndTransactionsSyncSwitchStatus]isEqualToString:KVisitsAndTrxsSettingsSwitchEnableFlag])
    {
        [visitsAndTrxsEnableSwitch setOn:YES];
        [visitsAndTrxsEnableSwitch setSelected:YES];
        
    }
    else
    {
        [visitsAndTrxsEnableSwitch setOn:NO];
        [visitsAndTrxsEnableSwitch setSelected:NO];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    AppControl *appcontrol=[AppControl retrieveSingleton];
    if(self.isMovingToParentViewController)
    {
        mediaSyncPopoverContentView.backgroundColor=UIViewControllerBackGroundColor;
        
        for (UIView *borderView in mediaSyncPopoverContentView.subviews) {
            
            if ([borderView isKindOfClass:[UIView class]]) {
                if (borderView.tag==101|| borderView.tag==102|| borderView.tag==103|| borderView.tag==104 || borderView.tag==105) {
                    borderView.layer.shadowColor = [UIColor blackColor].CGColor;
                    borderView.layer.shadowOffset = CGSizeMake(2, 2);
                    borderView.layer.shadowOpacity = 0.1;
                    borderView.layer.shadowRadius = 1.0;
                    
                }
            }
            
        }
        if([appcontrol.USER_ENABLED_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode] || [[SWDefaults VisitsAndTransactionsSyncSwitchStatus]isEqualToString:KVisitsAndTrxsSettingsSwitchEnableFlag])
        {
            [visitsAndTrxsEnableSwitch setOn:YES];
            [visitsAndTrxsEnableSwitch setSelected:YES];

        }
        else
        {
            [visitsAndTrxsEnableSwitch setOn:NO];
            [visitsAndTrxsEnableSwitch setSelected:NO];

        }
        [visitsAndTrxsEnableSwitch addTarget:self action:@selector(VisitsAndTransactionsSyncSettingsChanging:) forControlEvents:UIControlEventValueChanged];
        
        
        if([[SWDefaults CommunicationSyncSwitchStatus]isEqualToString:KCommunicationsSettingsSwitchEnableFlag])
        {
            [CommunicationSyncEnableSwitch setOn:YES];
            [CommunicationSyncEnableSwitch setSelected:YES];
            
        }
        else
        {
            [CommunicationSyncEnableSwitch setOn:NO];
            [CommunicationSyncEnableSwitch setSelected:NO];
            
        }
        [CommunicationSyncEnableSwitch addTarget:self action:@selector(CommunicationSyncSettingsChanging:) forControlEvents:UIControlEventValueChanged];
        

    }
    [self setHiddenAnimated:NO duration:1.25];
    
    
    syncButtonBackgroundImage.layer.shadowColor = [UIColor blackColor].CGColor;
    syncButtonBackgroundImage.layer.shadowOffset = CGSizeMake(2, 2);
    syncButtonBackgroundImage.layer.shadowOpacity = 0.1;
    syncButtonBackgroundImage.layer.shadowRadius = 1.0;

    mediaSyncPopoverContentView.layer.cornerRadius=6.0;
    visitsAndTrxsLastSyncedLabel.text=[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last synced", nil),[SWDefaults lastVTRXBackGroundSyncDate]==nil?NSLocalizedString(@"Never", nil):[SWDefaults lastVTRXBackGroundSyncDate]];
    CommunicationLastSyncedLabel.text=[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last synced", nil),[SWDefaults lastCOMMBackGroundSyncDate]==nil?NSLocalizedString(@"Never", nil):[SWDefaults lastCOMMBackGroundSyncDate]];
    if([appcontrol.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsNOCode])
    {
        visitsAndTrxsEnableSwitch.enabled = NO;
    }else{
        visitsAndTrxsEnableSwitch.enabled = YES;
    }


}
-(void)restoreSwitchStatus:(NSString*)status{
    if([status isEqualToString:KVisitsAndTrxsSettingsSwitchEnableFlag]){
        [visitsAndTrxsEnableSwitch setOn:YES];
        [visitsAndTrxsEnableSwitch setSelected:YES];
        
        
    }else{
        [visitsAndTrxsEnableSwitch setOn:NO];
        [visitsAndTrxsEnableSwitch setSelected:NO];
    }
}
    
-(void)viewDidAppear:(BOOL)animated
{
    // Create the path (with only the top-left corner rounded)
    headerView.layer.backgroundColor=[UINavigationBarBackgroundColor CGColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(5, 5)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    headerView.layer.mask = maskLayer;
    
    if(self.isMovingToParentViewController)
    {
        [self showMediaSyncStatus];
        SyncTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(showMediaSyncStatus) userInfo:nil repeats:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showMediaSyncStatus
{

    mediaLastSyncedLabel.text=[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last synced", nil),[[[BackgroundDocumentsDownload alloc]init]getLastMediaSyncDate]];
    TotalMediaFilesCountLabel.textColor=[UIColor colorWithRed:(67.0/255.0) green:(72.0/255.0) blue:(80.0/255.0) alpha:1.0];
    failedMediaFilesCountLabel.textColor=[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
    DownloadedMediaFilesCountLabel.textColor=[UIColor colorWithRed:0.0 green:(186.0/255.0) blue:(120.0/255.0) alpha:1.0];

    NSInteger totalMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"getTotalCount"] integerValue];
    NSInteger failedMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"FailedFilesCountOfCurrentDownloadTask"] integerValue];
    NSInteger downloadedMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"] integerValue];
    TotalMediaFilesCountLabel.text=[NSString stringWithFormat:@"%d",totalMediaFiles];
    failedMediaFilesCountLabel.text=[NSString stringWithFormat:@"%d",failedMediaFiles];
    DownloadedMediaFilesCountLabel.text=[NSString stringWithFormat:@"%d",downloadedMediaFiles];

    
    if(totalMediaFiles==0)
    {
        totalMediaFiles=1;
        downloadedMediaFiles=1;
        failedMediaFiles=0;
    }
    
    mediaDownloadPercantage=(float)(downloadedMediaFiles)/(float)totalMediaFiles;
    mediaSyncProgressBar.progress=mediaDownloadPercantage;

    if(totalMediaFiles==0)
    {
        mediaSyncProgressBar.progress=0.0;
        syncStatusLbl.text=@"No files to download";
    }
    else if(mediaDownloadPercantage==1)
    {
       syncStatusLbl.text=@"Completed";
    }
    else
    {
        syncStatusLbl.text=@"Downloading";
    }

    [mediaSyncChart drawCircleWithPercent:mediaDownloadPercantage*100
                                               duration:2
                                              lineWidth:5
                                              clockwise:YES
                                                lineCap:kCALineCapRound
                                              fillColor:[UIColor clearColor]
                                            strokeColor:[UIColor colorWithRed:53.0/255.0 green:194.0/255.0 blue:195.0/255.0 alpha:1]
                                         animatedColors:nil];
    mediaSyncChart.percentLabel.textColor=[UIColor grayColor];
    mediaSyncChart.percentLabel.font=kSWX_FONT_REGULAR(10);
    
    
    
    
    
    SWAppDelegate *app=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if(app.isBackgroundProcessRunning)
    {
        BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];
        if(mediaDownloadPercantage==1.0)
        {
            syncButtonBackgroundImage.image=nil;
        }
        else if (checkInternetConnectivity==NO) {
            isSyncAnimatedImageShowing=NO;
            [syncButtonBackgroundImage setImage:[UIImage imageNamed:@"sync-static"]];
        }
        else
        {
            if(!isSyncAnimatedImageShowing)
            {
                isSyncAnimatedImageShowing=YES;
                syncButtonBackgroundImage.image =  [UIImage animatedImageWithAnimatedGIFData:(NSData *)animatedSyncGifData];
            }
        }
        
        [syncButton setHidden:NO];
    }
    else
    {
        if(mediaDownloadPercantage==1.0)
        {
            syncButtonBackgroundImage.image=nil;
        }
        else
        {
            isSyncAnimatedImageShowing=NO;
            [syncButtonBackgroundImage setImage:[UIImage imageNamed:@"sync-static"]];

        }
        [syncButton setHidden:YES];

    }

}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    
    NSInteger totalMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"getTotalCount"] integerValue];
    NSInteger failedMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"FailedFilesCountOfCurrentDownloadTask"] integerValue];
    NSInteger downloadedMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"] integerValue];
    float mediaDownloadPercantage=(float)(downloadedMediaFiles+failedMediaFiles)/(float)totalMediaFiles;

    
    if(totalMediaFiles==0)
    {
        return 1;
    }
    else if(mediaDownloadPercantage==1)
    {
        return 1;
    }
    else
    {
        return 2;
    }


}
- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return [sliceColorsForMediaSyncChart objectAtIndex:(index)];
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    NSInteger totalMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"getTotalCount"] integerValue];
    NSInteger downloadedMediaFiles=[[[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"] integerValue];
    NSLog(@"total media count %ld downloaded media count %ld", (long)totalMediaFiles,(long)downloadedMediaFiles);
    if(totalMediaFiles==0)
    {
        return 1.0;
    }
    
    
    if(index==0)
    {
        return ((float)downloadedMediaFiles/(float)totalMediaFiles);
    }
    else if(index==1)
    {
        return (float)(totalMediaFiles-downloadedMediaFiles)/(float)totalMediaFiles;
    }
    return 1.0;

}
-(void)viewDidDisappear:(BOOL)animated
{
    [SyncTimer invalidate];
    SyncTimer=nil;
}
-(IBAction)BackButtonTapped:(id)sender
{
//    [self setHiddenAnimated:YES duration:1.0];
//    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(MoveBack) userInfo:nil repeats:YES];
    [self.navigationController popViewControllerAnimated:NO];

}
-(void)MoveBack
{
    [self.navigationController popViewControllerAnimated:NO];

}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    if (NO == hide)
    {
        transition.type = kCATransitionReveal;

    }
    else
    {
        transition.type = kCATransitionFade;

    }
    
    transition.subtype = (hide ? @"fromLeft" : @"fromRight");
    UIView* containerView = mediaSyncPopoverContentView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}

- (IBAction)VisitsAndTransactionsSyncSettingsChanging:(id)sender {

    
    NSLog(@"Swicth action");
    AppControl *appcontrol=[AppControl retrieveSingleton];
    if(visitsAndTrxsEnableSwitch.isOn && (![visitsAndTrxsEnableSwitch isSelected]))
    {
        NSLog(@"Swicth action on");
        [visitsAndTrxsEnableSwitch setSelected:YES];

        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [visitsAndTrxsEnableSwitch setOn:YES];
                                        [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchEnableFlag];
                                        appcontrol.USER_ENABLED_BACKGROUND_SYNC = KVisitsAndTrxsSettingsSwitchEnableFlag;
                                        [visitsAndTrxsEnableSwitch setSelected:YES];
                                        [self sendDeviceSettingToServer];
                                        
                                    }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [visitsAndTrxsEnableSwitch setOn:NO];
                                       [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchDisableFlag];
                                       appcontrol.USER_ENABLED_BACKGROUND_SYNC = KVisitsAndTrxsSettingsSwitchDisableFlag;
                                       [visitsAndTrxsEnableSwitch setSelected:NO];

                                       
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Visits and transactions auto sync" andMessage:@"Would you like to enable the Visits and transactions auto sync" andActions:actionsArray withController:self];
    }
    else if ((!visitsAndTrxsEnableSwitch.isOn) && [visitsAndTrxsEnableSwitch isSelected])
    {
        NSLog(@"Swicth action off");
        [visitsAndTrxsEnableSwitch setSelected:NO];

        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [visitsAndTrxsEnableSwitch setOn:NO];
                                        [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchDisableFlag];
                                        appcontrol.USER_ENABLED_BACKGROUND_SYNC = KVisitsAndTrxsSettingsSwitchDisableFlag;
                                        [visitsAndTrxsEnableSwitch setSelected:NO];
                                        [self sendDeviceSettingToServer];
                                        
                                    }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [visitsAndTrxsEnableSwitch setOn:YES];
                                       [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchEnableFlag];
                                     appcontrol.USER_ENABLED_BACKGROUND_SYNC = KVisitsAndTrxsSettingsSwitchEnableFlag;
                                       [visitsAndTrxsEnableSwitch setSelected:YES];

                                       
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Visits and transactions auto sync" andMessage:@"Would you like to disable the Visits and transactions auto sync" andActions:actionsArray withController:self];

    }
}
-(void)sendDeviceSettingToServer{
    [self sendDeviceSyncSetting];
    
}
-(void)sendDeviceSyncSetting
{
    [self sendDeviceSyncSettingData:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecGetDeviceSyncSettings" andProcDic:[self getProcedureForDeviceSyncSetting]];
}
-(NSDictionary *)getProcedureForDeviceSyncSetting{
    AppControl *appControl = [AppControl retrieveSingleton];
    NSString *deviceSync = appControl.ENABLE_BACKGROUND_SYNC;
    NSString *currentState = [SWDefaults VisitsAndTransactionsSyncSwitchStatus];
    
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"SalesRepID",@"DeviceSync",@"CurrentState", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"],deviceSync,currentState,nil];
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
    
}
- (void)sendDeviceSyncSettingData:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];
    
    if (checkInternetConnectivity) {
        SWAppDelegate * appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
        appDelegate.currentExceutingDataDownloadProcess=kDataDownloadProcess_PRODSTK;
        
        
        CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
        NSError *error;
        NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
        NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
        
        NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
        NSURL *url = [NSURL URLWithString:strurl];
        AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
        
        NSString *strParams =[[NSString alloc] init];
        NSString *strName=procName;
        NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"SalesRepID",@"DeviceSync",@"CurrentState", nil];
        
        for (NSInteger i=0; i<procedureParametersArray.count; i++) {
            strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
            strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];
        }
        
        
        NSString *encode = [strParams stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSLog(@"encoded url is %@", encode);
        
        NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
        NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
        
        NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Get_DeviceSyncSettings",[testServerDict stringForKey:@"name"],strName,encode];
        
        NSLog(@"request string for Get_DeviceSyncSettings %@",myRequestString);
        
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
        NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
        [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:myRequestData];
        
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
        
        [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:
         ^(AFHTTPRequestOperation *operationQ,id responseObject)
         {
             
             NSString *responseText = [operationQ responseString];
             NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
             NSLog(@"background sync Response of Get_DeviceSyncSettings %@",resultArray);
             if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
             {
                 
                 
             }
         }
                                         failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
         {
             if( [[SWDefaults VisitsAndTransactionsSyncSwitchStatus]isEqualToString:KVisitsAndTrxsSettingsSwitchEnableFlag])
             {
                 [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchDisableFlag];
             }else{
                 [SWDefaults setVisitsAndTransactionsSyncSwitchStatus:KVisitsAndTrxsSettingsSwitchEnableFlag];
             }
             NSString *Status = [SWDefaults VisitsAndTransactionsSyncSwitchStatus];
             [self restoreSwitchStatus:Status];
             NSLog(@" background sync error  sendRequestForDeviceSetting in SalesWorxProductBackgroundSyncManager%@",error);
             
         }];
        
        //call start on your request operation
        [operation start];
    }
    
    else{
        NSLog(@"tried to start product data fetch background process, but internet unavailable");
    }
}
- (IBAction)CommunicationSyncSettingsChanging:(id)sender {
    
    
    if(CommunicationSyncEnableSwitch.isOn && (![CommunicationSyncEnableSwitch isSelected]))
    {
        [CommunicationSyncEnableSwitch setSelected:YES];
        
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [CommunicationSyncEnableSwitch setOn:YES];
                                        [SWDefaults setCommunicationSyncSwitchStatus:KCommunicationsSettingsSwitchEnableFlag];
                                        [CommunicationSyncEnableSwitch setSelected:YES];
                                        
                                        
                                    }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [CommunicationSyncEnableSwitch setOn:NO];
                                       [SWDefaults setCommunicationSyncSwitchStatus:KCommunicationsSettingsSwitchDisableFlag];
                                       [CommunicationSyncEnableSwitch setSelected:NO];
                                       
                                       
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Messages auto sync" andMessage:@"Would you like to enable the Messages auto sync" andActions:actionsArray withController:self];
    }
    else if ((!CommunicationSyncEnableSwitch.isOn) && [CommunicationSyncEnableSwitch isSelected])
    {
        [CommunicationSyncEnableSwitch setSelected:NO];
        
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [CommunicationSyncEnableSwitch setOn:NO];
                                        [SWDefaults setCommunicationSyncSwitchStatus:KCommunicationsSettingsSwitchDisableFlag];
                                        [CommunicationSyncEnableSwitch setSelected:NO];
                                        
                                        
                                        
                                    }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [CommunicationSyncEnableSwitch setOn:YES];
                                       [SWDefaults setCommunicationSyncSwitchStatus:KCommunicationsSettingsSwitchEnableFlag];
                                       [CommunicationSyncEnableSwitch setSelected:YES];
                                       
                                       
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Messages auto sync" andMessage:@"Would you like to disable the Messages auto sync" andActions:actionsArray withController:self];
        
    }
}

-(IBAction)mediaFilesSyncButtonTapped:(id)sender
{
    BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];
    
    SWAppDelegate *app=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if(app.isBackgroundProcessRunning   && checkInternetConnectivity==NO)
    {
        if (checkInternetConnectivity==YES) {
            if(mediaDownloadPercantage<1)
            {
                SWAppDelegate *app=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
                NSLog(@"is Background process running %hhd, is now sync %hhd",app.isBackgroundProcessRunning,app.isNewSync );
                [app startMediaDownloadProcess];
            }
            
            UIAlertView *noNetWorkAlert=[[UIAlertView alloc]initWithTitle:@"Please wait" message:@"Background sync will start in 2-5 seconds" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [noNetWorkAlert show];
        }
        else
        {
            UIAlertView *noNetWorkAlert=[[UIAlertView alloc]initWithTitle:@"No network" message:@"Please check your network connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [noNetWorkAlert show];
        }
    }
}
@end
