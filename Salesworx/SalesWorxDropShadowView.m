//
//  SalesWorxDropShadowView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDropShadowView.h"

@implementation SalesWorxDropShadowView
@synthesize DropShadowPosition,shadowColor,setCustomColor;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
  
    if (setCustomColor==YES) {
        NSLog(@"custom color set");
        self.layer.shadowColor = shadowColor.CGColor;

    }
    else{
        self.layer.shadowColor = [UIColor blackColor].CGColor;
 
    }
    self.layer.shadowColor = [UIColor blackColor].CGColor;

    self.layer.shadowOffset = CGSizeMake(2, 2);
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowRadius = 1.0;
    
    if([DropShadowPosition isEqualToString:@"T"])
    {
        self.layer.shadowOffset = CGSizeMake(0, -3);
    }
    else if([DropShadowPosition isEqualToString:@"B"])
    {
        self.layer.shadowOffset = CGSizeMake(0, 3);
    }
    else if([DropShadowPosition isEqualToString:@"R"])
    {
        self.layer.shadowOffset = CGSizeMake(3, 0);
    }
    else if([DropShadowPosition isEqualToString:@"L"])
    {
         self.layer.shadowOffset = CGSizeMake(-3, 0);
    }
    else if([DropShadowPosition isEqualToString:@"TR"])
    {
        self.layer.shadowOffset = CGSizeMake(3, -3);

    }
    else if([DropShadowPosition isEqualToString:@"TL"])
    {
        self.layer.shadowOffset = CGSizeMake(-3, -3);
        
    }
    else if([DropShadowPosition isEqualToString:@"BR"])
    {
        self.layer.shadowOffset = CGSizeMake(3, 3);
        
    }
    else if([DropShadowPosition isEqualToString:@"BL"])
    {
        self.layer.shadowOffset = CGSizeMake(-3, 3);
        
    }
}

@end
