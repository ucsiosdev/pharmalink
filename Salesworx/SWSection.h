//
//  SWSection.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWSection : NSObject {
    NSString *sectionName;
    NSArray *rows;
}

@property (nonatomic, strong) NSString *sectionName;
@property (nonatomic, strong) NSArray *rows;

@end