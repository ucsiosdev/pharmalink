//
//  CurrencyViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CurrencyViewController.h"
@interface CurrencyViewController ()

@end

#define NUMERIC                 @"1234567890"

@implementation CurrencyViewController

@synthesize types;
@synthesize target;
@synthesize action;

- (id)init {
    self = [super init];
    
    if (self)
    {
        self.title = @"Select Currency";
        [self setTypes:[NSArray arrayWithObjects:@"AED", @"USD", @"ILS", @"EUR", @"PKR",@"INR", nil]];
        
        [self setTableView:[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped] ];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.backgroundView = nil;
        [self setPreferredContentSize:CGSizeMake(250, 130)];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save)] ];
    }
    
    return self;
}
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    
    [self setTypes:[[SWDatabaseManager retrieveManager] dbGetMultiCurrency]] ;
    
   //NSLog(@"Type %@",self.types);
    for (int i=0;i<[self.types count]; i++)
    {
        if ([[[self.types objectAtIndex:i] stringForKey:@"CCode"] isEqualToString:@"AED"])
        {
            currencyString = [NSMutableDictionary dictionaryWithDictionary:[self.types objectAtIndex:i]] ;
            checkedIndexPath = [NSIndexPath indexPathForRow:i      inSection:1] ;

            UITableViewCell* uncheckCell = [self.tableView
                                            cellForRowAtIndexPath:checkedIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    [self.tableView reloadData];
}


-(void)save
{
    [self.view endEditing:YES];

    if(amountString.length!=0 && currencyString.count!=0)
    {
        if ([self.target respondsToSelector:self.action])
        {

            [currencyString setValue:amountString forKey:@"AString"];
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:currencyString];
            #pragma clang diagnostic pop
        }
    }
    else if (amountString.length==0)
    {
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Please fill amount field."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please fill amount field." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
        [ErrorAlert show];
        
    }
    else if (currencyString.count==0)
    {
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Please select currency."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        
        
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please select currency." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
        [ErrorAlert show];
    }
}
#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section==1)
    {
        return [self.types count];
    }
    else
    {
        return 1;
    }
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
    
    NSString *title = @"";
    
    if (section == 1) {
        title =  @"Currency";
    } else if (section == 0) {
        title =  NSLocalizedString(@"Amount", nil);
    }
  GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:title] ;
    sectionHeader.titleLabel.textAlignment = NSTextAlignmentLeft;
    return sectionHeader;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool
    {
        if(indexPath.section==1)
        {
            UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];

            [cell.textLabel setText:[[self.types objectAtIndex:indexPath.row] stringForKey:@"CCode"]];
            if([checkedIndexPath isEqual:indexPath])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            return cell;
        }
        else
        {
            SWTextFieldCell *cell = nil;//[tv dequeueReusableCellWithIdentifier:IDENT];
            NSString *IDENT = @"CollectionCellIdent";

            if (!cell) {
                cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
            }
            cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [cell.textField setKeyboardType:UIKeyboardTypeNumberPad];
            cell.textField.returnKeyType = UIReturnKeyDone;
            [cell setDelegate:self];
            [cell.textField setText:amountString];
            return cell;

        }
        
    }
}

#pragma mark UITableView Deleate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(indexPath.section==1)
   {
       currencyString = [self.types objectAtIndex:indexPath.row] ;
       if(checkedIndexPath)
       {
           UITableViewCell* uncheckCell = [tableView
                                           cellForRowAtIndexPath:checkedIndexPath];
           uncheckCell.accessoryType = UITableViewCellAccessoryNone;
       }
//       if([checkedIndexPath isEqual:indexPath])
//       {
//           checkedIndexPath = nil;
//       }
//       else
//       {
           UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
           cell.accessoryType = UITableViewCellAccessoryCheckmark;
           checkedIndexPath = indexPath;
//       }
       [tableView deselectRowAtIndexPath:indexPath animated:YES];
   }
}
 
#pragma mark EditCell Delegate

- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key {
    NSCharacterSet *unacceptedInput = nil;
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    if ([[value componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1)
    {
        amountString = value;
    
    }
    else
    {
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Amount should be a numeric value only."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:@"Amount should be a numeric value only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
        [ErrorAlert show];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end