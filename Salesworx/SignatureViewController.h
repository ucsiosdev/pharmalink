//
//  SignatureViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/26/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPSSignatureView.h"
#import "PJRSignatureView.h"





@interface SignatureViewController : UIViewController



{
    id delegate;
    PJRSignatureView * pjrSignView;
    
    UIImageView* signImageView;
    
}


@property(strong,nonatomic)UIImage* capturedImage;
@property (strong,nonatomic) id delegate;

- (IBAction)closeButtonTapped:(id)sender;
- (IBAction)saveButtonTapped:(id)sender;
- (IBAction)eraseButtonTapped:(id)sender;

@end
