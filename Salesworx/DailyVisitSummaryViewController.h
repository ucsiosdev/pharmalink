//
//  DailyVisitSummaryViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "SalesWorxFieldSalesDailyVisitSummaryViewController.h"
#import "SalesWorxFieldMarketingDailyVisitSummaryViewController.h"

@interface DailyVisitSummaryViewController : UIViewController<UIScrollViewDelegate>
{
    SalesWorxFieldSalesDailyVisitSummaryViewController *fieldSalesView;
    SalesWorxFieldMarketingDailyVisitSummaryViewController *fieldMarketingView;
    
    IBOutlet NSLayoutConstraint *segmentTopConstraint;
}

@property(strong,nonatomic) IBOutlet HMSegmentedControl *segmentControl;
@property (nonatomic, strong) IBOutlet UIScrollView *contentScrollView;

@end
