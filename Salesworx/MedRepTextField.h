//
//  MedRepTextField.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface MedRepTextField : UITextField<UITextFieldDelegate>
{
    
}
@property (strong,nonatomic) UIImageView *dropdownImageView;
//@property (nonatomic,setter=setIsDropDown:) IBInspectable BOOL isDropDown;
@property (nonatomic,strong,setter=setDropDownImage:) IBInspectable NSString *DropDownImage;
@property (nonatomic,setter=setIsReadOnly:) IBInspectable BOOL isReadOnly;
@property (nonatomic,setter=setisMandatory:) IBInspectable BOOL isMandatory;
@property (nonatomic) IBInspectable BOOL enableCopyPaste;

@property (strong,nonatomic) CALayer *readOnlyLayer;
-(void)showTextfieldAsLabel;
//@property (nonatomic) IBInspectable BOOL EnableRTLTextAlignment;
//@property (nonatomic) IBInspectable BOOL EnableRTLTranslation;

//@property (nonatomic) IBInspectable BOOL isTextEndingWithExtraSpecialCharacter;

@property (nonatomic) IBInspectable BOOL isCollectionViewTextField;
@property (nonatomic) IBInspectable BOOL EnableTextTransLation;

@property (nonatomic) IBInspectable BOOL bottonBorder;

@property (nonatomic,strong,setter=setPlaceHolder:) IBInspectable NSString *placeHolder;

@property (nonatomic, strong) UIView * separatorView;


-(NSString*)text;
-(void)setText:(NSString*)newText;

//for merchandising
@property  NSString *identifier;

@end
