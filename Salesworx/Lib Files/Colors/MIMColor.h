 
//  MIMColor.h
//  MIMChartLib
//
//  Created by  Unique Computer Systems  on 11/08/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MIMColorClass.h"


@interface MIMColor : NSObject {
    
}
+(MIMColorClass *)GetMIMColorAtIndex:(int)index;
+(NSDictionary *)GetColorAtIndex:(int)index;
+(void)InitColors;
+(void)InitGreenTintColors;
+(void)InitFragmentedBarColors;
+(NSInteger)sizeOfColorArray;
+(void)nonAdjacentGradient;
+(void)lightGradients;
+(void)nonAdjacentPlainColors;

@end
