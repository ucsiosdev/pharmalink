 
//  LineInfoBox.m
//  MIMChartLib
//
//  Created by  Unique Computer Systems  on 15/08/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import "LineInfoBox.h"


@implementation LineInfoBox
@synthesize lineArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createColorArray];
    }
    return self;
}

-(void)createColorArray
{
    colorArray=[[NSMutableArray alloc]init];
    
    [colorArray addObject:[UIColor redColor]];
    [colorArray addObject:[UIColor greenColor]];
    [colorArray addObject:[UIColor blueColor]];
    [colorArray addObject:[UIColor yellowColor]];
    [colorArray addObject:[UIColor magentaColor]];
    [colorArray addObject:[UIColor darkGrayColor]];
    [colorArray addObject:[UIColor orangeColor]];
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetBlendMode(context,kCGBlendModeNormal);

    
    // Drawing code
    for (int i=0; i<[lineArray count]; i++) {
        
        //Draw the line
        UIColor *lineColor=[colorArray objectAtIndex:i];
        CGContextSetFillColorWithColor(context, lineColor.CGColor);
        CGContextAddRect(context, CGRectMake(5, 30* i + 10, 20, 10));  
        CGContextFillPath(context);
        
        
        //Stick the Label
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(30, 30* i, 200, 30)];
        [label setText:[lineArray objectAtIndex:i]];
        [label setBackgroundColor:[UIColor blackColor]];
        [label setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        [label setTextColor:[UIColor whiteColor]];
        [self addSubview:label];
    }
    
}


- (void)dealloc
{
    ////[super dealloc];
}

@end
