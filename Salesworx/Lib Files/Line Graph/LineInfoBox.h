 
//  LineInfoBox.h
//  MIMChartLib
//
//  Created by  Unique Computer Systems  on 15/08/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LineInfoBox : UIView {
    
    NSMutableArray *colorArray;
    NSArray *lineArray;
}
@property(nonatomic,retain)    NSArray *lineArray;
-(void)createColorArray;
@end
