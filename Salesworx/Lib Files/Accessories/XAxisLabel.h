 
//  XAxisLabel.h
//  MIMChartLib
//
//  Created by  Unique Computer Systems  on 15/08/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface XAxisLabel : UIView {
    
    NSString *text;
    NSInteger labelTag;
    NSInteger style;
    float width;
    float height;
    float  offset;
    
    BOOL lineChart;
    UIColor *xTitleColor;
    UIColor *mBackgroundColor;
    float fontSize;
}

-(void)drawTitleWithColor:(UIColor *)color;

@property(nonatomic,retain)    NSString *text;
@property(nonatomic,assign)    NSInteger labelTag;
@property(nonatomic,assign)    NSInteger style;
@property(nonatomic,assign)    float width;
@property(nonatomic,assign)    float height;
@property(nonatomic,assign)     float  offset;

@property(nonatomic,assign)     BOOL lineChart;
@property(nonatomic,retain) UIColor *mBackgroundColor;
@property(nonatomic,assign) float fontSize;

@end
