 
//  XAxisBand.h
//  MIM2D Library
//
//  Created by  Unique Computer Systems  on 08/07/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import "MIMColorClass.h"
#import "Constant.h"

@interface XAxisBand : UIView {
    
    NSArray *xElements;
    float _tileWidth;
    float _gridWidth;
    float scalingFactor;
    float gapDistance;//Gap between two labels;
    float groupGapDistance;
    float barWidth;
    int style;
    BOOL lineChart;
    BOOL barChart;
    BOOL groupBarChart;
    BOOL stackedBarChart;
    
    BOOL xIsString;
    float lineWidth;
    float xAxisHeight;
    
    UIColor *lineColor;
    NSDictionary *properties;
    NSArray *groupTitles;
    float groupTitleOffset;
    UIColor *groupTitleColor;
    UIColor *groupTitleBgColor;
    BOOL hideSticks;
    
    float fontSize;
    float xoffset;
    
}
@property(nonatomic,retain)NSArray *xElements;
@property(nonatomic,assign)float scalingFactor;
@property(nonatomic,assign)int style;
@property(nonatomic,assign)BOOL lineChart;
@property(nonatomic,assign)BOOL barChart;
@property(nonatomic,assign)BOOL xIsString;
@property(nonatomic,assign)float gapDistance;//Gap between two labels;
@property(nonatomic,assign)float lineWidth;
@property(nonatomic,retain)UIColor *lineColor;
@property(nonatomic,retain)NSDictionary *properties;
@property(nonatomic,retain)NSArray *groupTitles;
@property(nonatomic,assign)float groupTitleOffset;
@property(nonatomic,assign) float fontSize;


-(void)drawXAxis:(CGContextRef)ctx;


@end
