//
//  Singleton.h
//  Chyea
//
//  Created by Usman Ahmed on 5/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Singleton : NSObject
{
    int frameWidth;
    int frameHeight;
    int customerIndexPath ; 
    int mainScreenWidth;
    int mainScreenHeight;
    int pieChartHiegth ;
    int pieChartWidht ;
    int pieChartX ;
    int pieChartY ;
    int isFromSync;
    int isForSyncUpload;
    int databaseIsOpen;

    id splitViewObject;
    
    BOOL shouldPresentCustomerList;
    BOOL isSaveOrder;
    BOOL isBarCode;
    BOOL isDueScreen;
    BOOL isManageOrder;
    BOOL isActivated;
    BOOL isFullSyncDone;
    
    NSMutableDictionary *currentCustomer;
    NSMutableDictionary *cashCustomerDictionary;
    NSMutableDictionary *FOC_Product_Dict;

    NSArray *cashCustomerList;
    NSArray *savedOrderArray;
    NSMutableArray *savedOrderTemplateArray;

    NSString *valueBarCode;
    NSString *invoiceDate;
    NSString *invoiceValue;
    NSString *paymentType;
    NSString *stockAvailble;
    NSString *productBarPopOver ;
    NSString *visitParentView;
    NSString *savedPerformaCurrentOrder;
    NSString *isCashCustomer;
    NSString *popToCash;
    NSString *lastSyncDate;
    NSString *manageOrderRefNumber;
    NSString *planDetailId;
    NSString *isSendOrderDone;
    NSString *xmlParsedString;
    NSDate   *orderStartDate;
    
    BOOL isFullSyncDashboard;
    BOOL isFullSyncCustomer;
    BOOL isFullSyncProduct;
    BOOL isFullSyncCollection;
    BOOL isFullSyncSurvey;
    BOOL isFullSyncMessage;
    BOOL isDistributionChecked;
    BOOL isDistributionItemGet;
    BOOL isSurveyTaken;
    
    NSString *distributionRef;
}

@property (nonatomic, assign) BOOL isActivated;
@property (nonatomic, assign) BOOL isDistributionChecked;
@property (nonatomic, strong) NSString *distributionRef;

@property(nonatomic,assign)     BOOL orderHistoryDividerHidden;;

@property (nonatomic, assign) int isFromSync;
@property (nonatomic, assign) int isForSyncUpload;
@property (nonatomic, assign) int frameWidth;
@property (nonatomic, assign) int customerIndexPath;
@property (nonatomic, assign) int frameHeight;
@property (nonatomic, assign) int mainScreenWidth;
@property (nonatomic, assign) int mainScreenHeight;
@property (nonatomic, assign) int pieChartHiegth;
@property (nonatomic, assign) int pieChartWidht;
@property (nonatomic, assign) int pieChartX;
@property (nonatomic, assign) int pieChartY;
@property (nonatomic, assign) int databaseIsOpen;

//@property (nonatomic, strong) id splitViewObject;
@property (nonatomic, strong) NSString *xmlParsedString;
@property (nonatomic, strong) NSString *valueBarCode;
@property (nonatomic, strong) NSString *invoiceDate;
@property (nonatomic, strong) NSString *invoiceValue;
@property (nonatomic, strong) NSString *paymentType;
@property (nonatomic, strong) NSMutableDictionary *currentCustomer;
@property (nonatomic, strong) NSString *stockAvailble;
@property (nonatomic, strong) NSString *productBarPopOver;
@property (nonatomic, strong) NSString *visitParentView;
@property (nonatomic, strong) NSString *savedPerformaCurrentOrder;
@property (nonatomic, strong) NSArray *cashCustomerList;
@property (nonatomic, strong) NSString *isCashCustomer;
@property (nonatomic, strong) NSMutableDictionary *cashCustomerDictionary;
@property (nonatomic, strong) NSString *popToCash;
@property (nonatomic, strong) NSMutableDictionary *FOC_Product_Dict;
@property (nonatomic, strong) NSString *lastSyncDate;
@property (nonatomic, strong) NSString *manageOrderRefNumber;
@property (nonatomic, strong) NSString *planDetailId;
@property (nonatomic, strong) NSString *isSendOrderDone;
@property (nonatomic, strong) NSDate *orderStartDate;
@property (nonatomic, readwrite) BOOL shouldPresentCustomerList;
@property (nonatomic, readwrite) BOOL isFullSyncDone;
@property (nonatomic, readwrite) BOOL isBarCode;
@property (nonatomic, readwrite) BOOL isSaveOrder;
@property (nonatomic, readwrite) BOOL isDueScreen;
@property (nonatomic, readwrite) BOOL isManageOrder;
@property (nonatomic, readwrite) BOOL isFullSyncDashboard;
@property (nonatomic, readwrite) BOOL isFullSyncCustomer;
@property (nonatomic, readwrite) BOOL isFullSyncProduct;
@property (nonatomic, readwrite) BOOL isFullSyncCollection;
@property (nonatomic, readwrite) BOOL isFullSyncSurvey;
@property (nonatomic, readwrite) BOOL isFullSyncMessage;
@property (nonatomic, readwrite) BOOL isDistributionItemGet;
@property (nonatomic, readwrite) BOOL isSurveyTaken;
@property (nonatomic, readwrite) BOOL isTemplateOrder;

@property(nonatomic,strong) NSString * selectedWarehouse;
@property (nonatomic, strong) NSArray *savedOrderArray;
@property (nonatomic, strong)NSMutableArray *savedOrderTemplateArray;

@property (nonatomic, readwrite) BOOL startBeaconVisit;


//-(void)cleanObject;
+ (Singleton*) retrieveSingleton;
+ (BOOL)connectedToInternet;
//+ (void)getFreeMemory;
+ (void) destroyMySingleton;


@end
