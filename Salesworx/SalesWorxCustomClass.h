//
//  SalesWorxCustomClass.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedRepCustomClass.h"
#import "JSONModel.h"

#import "DataType.h"

@interface SalesWorxCustomClass : NSObject

@end

@interface SalesWorxBeacon : NSObject
@property (strong,nonatomic)   NSString * Beacon_UUID;
@property (strong,nonatomic)   NSString * Beacon_Major;
@property (strong,nonatomic)   NSString * Beacon_Minor;
@property (strong,nonatomic)   NSString * visitStartTime;
@property (strong,nonatomic)   NSString * beaconDetectionTime;
@property(strong,nonatomic) NSNumber* proximity;
@property (nonatomic)   BOOL isVisitStarted;
@end

@interface PriceList : NSObject
@property(strong,nonatomic) NSString *Price_List_ID,*Inventory_Item_ID,*Organization_ID,*Item_UOM,*Unit_Selling_Price,*Unit_List_Price,*Is_Generic;
@end

@interface Stock : NSObject
@property(strong,nonatomic) NSString *Org_ID,*Lot_Qty,*Lot_No,*Expiry_Date,*Stock_ID,*On_Order_Qty,*warehouse,*lotNumberWithExpiryDate;
@property(strong,nonatomic) NSDecimalNumber *lotQuantityNumber;
@end

@interface BonusItem : NSObject
@property(strong,nonatomic) NSString *Description,*Get_Add_Per,*Get_Item,*Get_Qty,*Price_Break_Type_Code,*Prom_Qty_From,*Prom_Qty_To;

@end

@interface ProductMediaFile : NSObject

@property(strong,nonatomic) NSString *Media_File_ID,*Entity_ID_1,*Entity_ID_2,*Entity_Type,*Media_Type,*Filename,*Caption,*Thumbnail,*Is_Deleted,*Download_Flag,*Custom_Attribute_1,*Custom_Attribute_2,*Custom_Attribute_3,*Custom_Attribute_4,*File_Path,*Product_Name,*Product_Code,*Brand_Code,*File_Name,*Detailed_Info,*isSelected,*Download_Url,*isStaticData;
@property(nonatomic) BOOL isDemoed;
@property(nonatomic) BOOL isEmailed;
@property(nonatomic) BOOL isStartNode;
@property(nonatomic) BOOL isEndNode;
@property(strong,nonatomic) NSString * viewedTime;
@property(strong,nonatomic) NSString * discussedAt;
@property(strong,nonatomic) NSString * Agency;
@end

@interface ProductUOM  : NSObject
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * Organization_ID;
@property(strong,nonatomic) NSString * Item_UOM_ID;
@property(strong,nonatomic) NSString * Item_UOM;
@property(strong,nonatomic) NSString * Conversion;
@property(strong,nonatomic) NSString * isPrimaryUOM;
@end

@interface Products  : JSONModel
@property(strong,nonatomic) NSString <Optional>* Agency;
@property(strong,nonatomic) NSString *Inventory_Item_ID;
@property(strong,nonatomic) NSString <Optional>* Brand_Code;
@property(strong,nonatomic) NSString <Optional>* Category;
@property(strong,nonatomic) NSString <Optional>* Description;
@property(strong,nonatomic) NSString <Optional>* IsMSL;
@property(strong,nonatomic) NSString <Optional>* ItemID;
@property(strong,nonatomic) NSString <Optional>* Lot_Qty;
@property(strong,nonatomic) NSString <Optional>* Item_Code;
@property(strong,nonatomic) NSString <Optional>* OrgID;
@property(strong,nonatomic) NSString <Optional>* Sts;
@property(strong,nonatomic) NSString <Optional>* Discount;
@property(strong,nonatomic) NSString * selectedUOM;
@property(strong,nonatomic) NSString * primaryUOM;
@property(nonatomic)  NSNumber <Optional>*  conversionValue;
@property(strong,nonatomic) NSMutableArray <Optional>* productStockArray;
@property(strong,nonatomic) NSMutableArray <Optional>* productBonusArray;
@property(strong,nonatomic) NSMutableArray <Optional>* productPriceListArray;
@property(nonatomic,strong) NSString <Optional>* stock;
@property(nonatomic,strong) NSString <Optional>* bonus;
@property(strong,nonatomic) NSMutableArray <Optional>* productImagesArray;
@property(strong,nonatomic) ProductMediaFile <Optional>* productMediaFile;
@property(strong,nonatomic) Stock <Optional>*productStock;
@property(strong,nonatomic)PriceList <Optional>*productPriceList;
@property(strong,nonatomic)NSString <Optional>*nearestExpiryDate;
@property(nonatomic)NSString <Optional>* isSellingPriceFieldEditable;
@property(nonatomic,strong) NSString <Optional>* specialDiscount;
@property(strong,strong) NSString <Optional>* RESTRICTED;
@property(strong,strong) NSString <Optional>* Product_ID;
@property(strong,strong) NSString <Optional>* Product_Type;
@property(strong,strong) NSString <Optional>* Product_Name;
@property(strong,strong) NSString <Optional>* Promo_Item;
@property(strong,nonatomic) NSMutableArray <Optional>*ProductUOMArray;
@property(strong,nonatomic) NSString <Optional>*ProductBarCode;
@property(strong,nonatomic) NSString <Optional>*OnOrderQty;
@property(strong,nonatomic) NSString <Optional>*maxExpiryDate;

@property(strong,nonatomic) NSString <Optional>*TradeName;
@property(strong,nonatomic) NSString <Optional>*FormDesc;
@property(strong,nonatomic) NSString <Optional>*GenericName;

@property(strong,nonatomic) NSString <Optional>*REMOVE_IN_FOC_LIST;
@property(strong,nonatomic) NSString <Optional>*RESTRICTED_FOR_RETURN;




@end

@interface CustomerCategory : NSObject
@property(strong,nonatomic) NSString * Category;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Item_No;
@property(strong,nonatomic) NSString * Org_ID;
@end


@interface SelectedBrand : NSObject

@property(strong,nonatomic) NSString * brandCode;
@property(strong,nonatomic) NSString * Sales_Value;
@property(strong,nonatomic) NSString * Balance_To_Go;
@property(strong,nonatomic) NSString * Target_Value;


@end


@interface CustomerOrderHistory : NSObject

@property(strong,nonatomic) NSString * Customer_Name;
@property(strong,nonatomic) NSString * Creation_Date;
@property(strong,nonatomic) NSString * End_Time;
@property(strong,nonatomic) NSString * ERP_Status;
@property(strong,nonatomic) NSString * FSR;
@property(strong,nonatomic) NSString * Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString * Transaction_Amt;
@property(strong,nonatomic) NSString * Shipping_Instructions;
@property(strong,nonatomic) NSString * ERP_Ref_Number;

@end



@interface CustomerOrderHistoryLineItems : NSObject

@property(strong,nonatomic) NSString * Calc_Price_Flag;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Inventory_Item_ID;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * Order_Quantity_UOM;
@property(strong,nonatomic) NSString * Ordered_Quantity;
@property(strong,nonatomic) NSString * Unit_Selling_Price;
@property(strong,nonatomic) NSString * Item_Value;

@end

@interface ToDo : NSObject

@property(strong,nonatomic) NSString *Visit_ID;
@property(strong,nonatomic) NSString *Row_ID;
@property(strong,nonatomic)NSString * Title;
@property(strong,nonatomic)NSString * Description;
@property(strong,nonatomic)NSString * Todo_Date;
@property(strong,nonatomic)NSString * Inv_Customer_ID;
@property(strong,nonatomic)NSString * Inv_Site_Use_ID;
@property(strong,nonatomic)NSString * Created_At;
@property(strong,nonatomic)NSString * Created_By;
@property(strong,nonatomic)NSString * Last_Updated_At;
@property(strong,nonatomic)NSString * Last_Updated_By;
@property(strong,nonatomic)NSString * Status;
@property(strong,nonatomic)NSString * Ship_Customer_ID;
@property(strong,nonatomic)NSString * Ship_Site_Use_ID;

@property(strong,nonatomic)NSString * isUpdated;
@property(strong,nonatomic)NSString * isNew;
@property(strong,nonatomic)NSString * customerName;
@property(strong,nonatomic)NSString * remindAt;


@end


@interface CustomerOrderHistoryInvoiceLineItems : NSObject

@property(strong,nonatomic) NSString * Inventory_Item_ID;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Expiry_Date;
@property(strong,nonatomic) NSString * Order_Quantity_UOM;
@property(strong,nonatomic) NSString * Ordered_Quantity;
@property(strong,nonatomic) NSString * Calc_Price_Flag;
@property(strong,nonatomic) NSString * Unit_Selling_Price;
@property(strong,nonatomic) NSString * Item_Value;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * ERP_Ref_No;
@property(strong,nonatomic) NSString * Order_Date;

@end

@interface SWCustomerPriceList : NSObject

@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Brand_Code;
@property(strong,nonatomic) NSString * Is_Generic;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * Item_UOM;
@property(strong,nonatomic) NSString * Unit_List_Price;
@property(strong,nonatomic) NSString * Unit_Selling_Price;

@end


@interface CustomerDues : NSObject

@property(strong,nonatomic)NSMutableDictionary * duesDictionary;


@end

@interface Customer : NSObject

@property(strong,nonatomic) NSMutableDictionary * cashCustomerDictionary;
@property(strong,nonatomic) NSString* cash_Name;
@property(strong,nonatomic) NSString* cash_Phone;
@property(strong,nonatomic) NSString* cash_Contact;
@property(strong,nonatomic) NSString* cash_Address;
@property(strong,nonatomic) NSString* cash_Fax;


@property(strong,nonatomic) NSString * Planned_visit_ID;
@property(strong,nonatomic) NSString * Visit_Type;
@property(strong,nonatomic)NSMutableArray* customerDuesArray;
@property(strong,nonatomic) NSString * Address;
@property(strong,nonatomic) NSString * Allow_FOC;
@property(strong,nonatomic) NSString * Avail_Bal;
@property(strong,nonatomic) NSString * Customer_Name;
@property(strong,nonatomic) NSString * Bill_Credit_Period;
@property(strong,nonatomic) NSString * Total_Due;
@property(strong,nonatomic) NSString * Prior_Due;
@property(strong,nonatomic) NSString * PDC_Due;
@property(strong,nonatomic) NSString * Cash_Cust;
@property(strong,nonatomic) NSString * Chain_Customer_Code;
@property(strong,nonatomic) NSString * City;
@property(strong,nonatomic) NSString * Contact;
@property(strong,nonatomic) NSString * Creation_Date;
@property(strong,nonatomic) NSString * Credit_Hold;
@property(strong,nonatomic) NSString * Credit_Limit;
@property(strong,nonatomic) NSString * Cust_Lat;
@property(strong,nonatomic) NSString * Cust_Long;
@property(strong,nonatomic) NSString * Cust_Status;
@property(strong,nonatomic) NSString * Customer_Barcode;
@property(strong,nonatomic) NSString * Customer_Class;
@property(strong,nonatomic) NSString * Customer_ID;
@property(strong,nonatomic) NSString * Customer_No;
@property(strong,nonatomic) NSString * Customer_OD_Status;
@property(strong,nonatomic) NSString * Customer_Segment_ID;
@property(strong,nonatomic) NSString * Customer_Type;
@property(strong,nonatomic) NSString * Dept;
@property(strong,nonatomic) NSString * Location;
@property(strong,nonatomic) NSString * Phone;
@property(strong,nonatomic) NSString * Postal_Code;
@property(strong,nonatomic) NSString * Price_List_ID;
@property(strong,nonatomic) NSString * SalesRep_ID;
@property(strong,nonatomic) NSString * Sales_District_ID;
@property(strong,nonatomic) NSString * Ship_Customer_ID;
@property(strong,nonatomic) NSString * Ship_Site_Use_ID;
@property(strong,nonatomic) NSString * Site_Use_ID;
@property(strong,nonatomic) NSString * Sync_Timestamp;
@property(strong,nonatomic) NSString * Trade_Classification;
@property(strong,nonatomic) NSString * Trade_License_Expiry;
@property(strong,nonatomic) CustomerCategory * customerCategory;
@property(strong,nonatomic) NSMutableArray * categoriesArray;
@property(strong,nonatomic) NSMutableArray * brandsArray;

@property(strong,nonatomic)CustomerOrderHistory * customerOrderHistory;
@property(strong,nonatomic)CustomerOrderHistoryLineItems * customerOrderHistoryLineItems;
@property(strong,nonatomic)CustomerOrderHistoryInvoiceLineItems * customerOrderHistoryInvoiceLineItems;

@property(strong,nonatomic) NSMutableArray * customerOrderHistoryArray;
@property(strong,nonatomic) NSMutableArray * customerOrderHistoryLineItemsArray;
@property(strong,nonatomic) NSMutableArray * customerOrderHistoryInvoiceLineItemsArray;

@property(strong,nonatomic) NSMutableArray * customerPriceListArray;
@property(strong,nonatomic) CustomerDues* customerDues;

@property(strong,nonatomic)SelectedBrand* selectedBrand;
@property(strong,nonatomic) NSString* targetValue;
@property(strong,nonatomic) NSString* salesValue;
@property(strong,nonatomic) NSString* balanceToGoValue;
@property(strong,nonatomic) NSString* totalOutStanding;
@property(strong,nonatomic) NSString* Over_Due;
@property(nonatomic) double totalOrderAmount;
@property BOOL isBonusAvailable;

@property(strong,nonatomic) NSString* IS_LOC_RNG;
@property(strong,nonatomic) NSString * CUST_LOC_RNG;

@property(strong,nonatomic) SalesWorxBeacon * currentCustomerBeacon;

@property(strong,nonatomic) NSString *FSR_Plan_Detail_ID;
@property(strong,nonatomic) NSString *Visit_Date;
@property(strong,nonatomic) NSString *Visit_End_Time;
@property(strong,nonatomic) NSString *Visit_Start_Time;
@property(strong,nonatomic) NSString *Visit_Status;
@property(strong,nonatomic) NSString *Visit_Note;
@property(strong,nonatomic) NSString * isDelegatedCustomer;
@property(strong,nonatomic) NSString * Area;


//this is for route
@property(strong,nonatomic) NSString * visitStatus;
@property(strong,nonatomic) NSString * distanceFromCurrentLocation;
@property(strong,nonatomic) NSString * lastVisitedOn;

//this is for handling today visit is yes/no
@property BOOL isTodayVisit;


@end



//Visit options

@interface ProductCategories : NSObject

@property(strong,nonatomic)NSString* Category;

@property(strong,nonatomic)NSString* Description;

@property(strong,nonatomic)NSString* Org_ID;

@property(strong,nonatomic)NSString* Item_No;

@property(strong,nonatomic) NSString * isSelected;
@end

@interface DistributionCheck : NSObject
@property(strong,nonatomic) NSString* Item_Code;
@property(strong,nonatomic) NSString* Description;
@property(strong,nonatomic) NSString* Avl;
@property(strong,nonatomic) NSString* Qty;
@property(strong,nonatomic) NSString* LotNo;
@property(strong,nonatomic) NSString* ExpDate;
@property(strong,nonatomic) Stock *ItemStock;

@end


@interface SalesOrderTemplateLineItems : NSObject
@property(strong,nonatomic) NSString* Order_Template_ID;
@property(strong,nonatomic) NSString* Inventory_Item_ID;
@property(strong,nonatomic) NSString* Organization_ID;
@property(strong,nonatomic) NSString* Quantity;
@property(strong,nonatomic) NSString* Custom_Attribute_1;
@property(strong,nonatomic) NSString* Custom_Attribute_2;
@property(strong,nonatomic) NSString* Custom_Attribute_3;
@property(strong,nonatomic) NSString* Unit_Selling_Price;
@property(strong,nonatomic) NSString* WholeSale_Price;
@property(strong,nonatomic) Products* selectedProduct;

@end

@interface SalesOrderTemplete : NSObject

@property(strong,nonatomic) NSString* Order_Template_ID;
@property(strong,nonatomic) NSString* Template_Name;
@property(strong,nonatomic) NSString* SalesRep_ID;
@property(strong,nonatomic) NSString* Custom_Attribute_1;
@property(strong,nonatomic) NSString* Custom_Attribute_2;
@property(strong,nonatomic) NSString* Custom_Attribute_3;

@end

@interface SalesWorxParentOrder : NSObject
@property(strong,nonatomic) NSString*Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString*Transaction_Amt;

@end

@interface SalesOrderInitialization : NSObject


@end
@interface SalesOrderConfirmationDetails : NSObject
{
    
}
@property(nonatomic,strong) NSString* DocReferenceNumber;
@property(nonatomic,strong) NSString* shipDate;
@property(nonatomic,strong) NSString* signeeName;
@property(nonatomic,strong) NSString* comments;
@property(nonatomic,strong) NSString *isWholeSaleOrder;
@property(nonatomic,strong) NSString *SkipConsolidation;
@property(nonatomic) BOOL isCustomerSigned;
@property(nonatomic) BOOL isLPOImagesCaptured;

@end

@interface ReturnsConfirmationDetails : NSObject
{
    
}
@property(nonatomic,strong) NSString* DocReferenceNumber;
@property(nonatomic,strong) NSString* shipDate;
@property(nonatomic,strong) NSString* signeeName;
@property(nonatomic,strong) NSString* comments;
@property(nonatomic,strong) NSString *returnType;
@property(nonatomic,strong) NSString *isGoodsCollected;
@property(nonatomic,strong) NSString* InvoicesNumbers;
@property(nonatomic) BOOL isCustomerSigned;


@end

@interface SalesWorxReturn : NSObject
@property(strong,nonatomic) NSMutableArray* productCategoriesArray;
@property(strong,nonatomic) ProductCategories* selectedCategory;
@property(strong,nonatomic) NSMutableArray* returnLineItemsArray;
@property (strong,nonatomic)ReturnsConfirmationDetails *confrimationDetails;
@property(strong,nonatomic) NSString* ReturnStartTime;
@property(strong,nonatomic) NSMutableArray *AssortmentPlansArray;

@end

@interface SalesOrderLineItemVATCharge : NSObject
@property(nonatomic) NSDecimalNumber * VATCharge;
@property(nonatomic) NSString * VATCode;
@property(nonatomic) NSString * VATLevel;
@property(nonatomic) NSString * VATRuleLevel;
@end

@interface SalesWorxReturnsMatchedInVoice : NSObject

@property (strong,nonatomic) NSString *invoiceDate;
@property (strong,nonatomic) NSString *invoiceNumber;
@property (strong,nonatomic) NSString *quantity;

@end

@interface ReturnsLineItem : NSObject
{
    
}
@property (strong,nonatomic) Products *returnProduct;
@property (nonatomic) Products *bonusProduct;
@property (nonatomic) double defaultBonusQty;
@property (nonatomic) double recievedBonusQty;
@property (nonatomic) NSDecimalNumber * returnProductQty;
@property (strong,nonatomic) NSDecimalNumber * returnItemTotalamount;
@property (strong,nonatomic) NSDecimalNumber * returnItemNetamount;
@property (strong,nonatomic) NSDecimalNumber * returnItemVATChargeamount;
@property (strong,nonatomic) NSMutableArray* bonusDetailsArray;
@property (strong,nonatomic) NSString* lotNumber;
@property (strong,nonatomic) NSString* expiryDate;
@property (strong,nonatomic) NSString* reasonForReturn;
@property (strong,nonatomic) NSString* reasonForReturnDescription;
@property (strong,nonatomic) SalesWorxReturnsMatchedInVoice * selectedInvoice;
@property (strong,nonatomic) NSString *invoiceReason;
@property (nonatomic,strong) SalesOrderLineItemVATCharge * VATChargeObj;

@end


@interface ManageOrderDetails : NSObject
@property(strong,nonatomic) NSString * manageOrderStatus;
@property(strong,nonatomic) NSString * Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString * Row_ID;
@property(strong,nonatomic) NSString * Visit_ID;
@property(strong,nonatomic) NSString * FOCOrderNumber;
@property(strong,nonatomic) NSString * FOCParentOrderOrderNumber;
@property(strong,nonatomic) NSString * OrderAmount;
@property(strong,nonatomic) NSString * Creation_Date;
@property(strong,nonatomic) ProductCategories * Category;
@property(strong,nonatomic) Customer * customerDetails;

@end
@interface SalesWorxReOrder : NSObject
@property(strong,nonatomic) NSString * Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString * OrderAmount;
@property(strong,nonatomic) NSString * Creation_Date;
@end
@interface SalesOrder : NSObject

@property(strong,nonatomic) NSMutableArray* productCategoriesArray;
@property(strong,nonatomic) NSMutableArray* salesOrderTempleteArray;
@property(strong,nonatomic) SalesOrderTemplete * selectedTemplete;
@property(strong,nonatomic) ProductCategories* selectedCategory;
@property(strong,nonatomic) NSString* selectedOrderType;
@property(strong,nonatomic) NSString* focParentOrderNumber;
@property(nonatomic) BOOL isTemplateOrder;
@property(nonatomic) BOOL isManagedOrder;
@property(strong,nonatomic) NSMutableArray* SalesOrderLineItemsArray;
@property (strong,nonatomic)SalesOrderConfirmationDetails *confrimationDetails;
@property(strong,nonatomic) NSString* OrderStartTime;
@property(strong,nonatomic) ManageOrderDetails* SelectedManageOrderDetails;
@property(strong,nonatomic) SalesWorxReOrder *reOrderDetails;

@property(strong,nonatomic) NSMutableArray *AssortmentBonusProductsArray;
@property(strong,nonatomic) NSMutableArray *AssortmentPlansArray;
@end

@interface SalesOrderLineItems : NSObject
@property(strong,nonatomic) NSMutableArray* productCategoriesArray;
@property(strong,nonatomic) NSMutableArray* salesOrderTempleteArray;
@property(strong,nonatomic) SalesOrderTemplete * selectedTemplete;
@property(strong,nonatomic) NSMutableArray* salesOrderTemplateLineItemsArray;
@property(strong,nonatomic) ProductCategories* selectedCategory;
@property(strong,nonatomic) NSString* selectedOrderType;
@property(strong,nonatomic) NSString* focOrderNumber;
@end

@interface OrderHistory : NSObject

@end

@interface Collection : NSObject

@end

@interface Survey : NSObject

@end

@interface SalesWorxMerchandising : NSObject
@property(strong,nonatomic)NSString* Session_ID;
@property(strong,nonatomic)NSString* Start_Time;

@property(strong,nonatomic) NSMutableArray* merchandisingArray;


@end


@interface FieldSalesContact : NSObject
@property(strong,nonatomic) NSString* contactName;
@property(strong,nonatomic) NSString* contactNumber;

@end

@interface VisitOptions : NSObject
@property(strong,nonatomic)FieldSalesContact* contact;

@property(strong,nonatomic)Customer* customer;
@property(strong,nonatomic)DistributionCheck* distributionCheck;
@property(strong,nonatomic)SalesOrder* salesOrder;
@property(strong,nonatomic)NSMutableArray* manageOrders;
@property(strong,nonatomic)OrderHistory* orderHistory;
@property(strong,nonatomic)Collection* collection;
@property(strong,nonatomic)Survey* survey;
@property(strong,nonatomic)SalesWorxReturn *returnOrder;
@property(strong,nonatomic)ToDo * toDo;
@property(strong,nonatomic) NSMutableArray* toDoArray;
@property(strong,nonatomic) SalesWorxMerchandising* salesWorxMerchandising;

@end


/*Merchandising sub modules*/

@interface MerchandisingAvailability : NSObject


@property(strong,nonatomic) NSString* Description;
@property(strong,nonatomic) NSString* Inventory_Item_ID;

@property(strong,nonatomic) NSDecimalNumber* Stock;
@property(strong,nonatomic) NSDecimalNumber* Previous_Stock;
@property(strong,nonatomic) NSDecimalNumber* Stock_Varience;
@property(strong,nonatomic) NSString* Brand_Code;
@property(strong,nonatomic) NSDecimalNumber* Price;
@property(strong,nonatomic) NSDecimalNumber* Previous_Price;
@property(strong,nonatomic) NSDecimalNumber* Price_Varience;
@property(strong,nonatomic) NSString* Position;
@property(strong,nonatomic) NSString* isUpdated;

@end

@interface MerchandisingBrandSharePromotion : NSObject
@property(strong,nonatomic) NSString *isPromotionImplemented;

@end

@interface MerchandisingBrandShareOfSelf : NSObject
@property(strong,nonatomic) NSString *calculationIn;
@property(strong,nonatomic) NSDecimalNumber *brandShare;
@property(strong,nonatomic) NSDecimalNumber *category;
@property(strong,nonatomic) NSDecimalNumber *shareOfShelf;
@property(strong,nonatomic) NSDecimalNumber *marketShare;
@end

@interface MerchandisingBrandShare : NSObject
@property(strong,nonatomic) NSString *brand_Share;
@property(strong,nonatomic) NSString *category_Share;
@property(strong,nonatomic) NSString *SOS_Percentage;
@property(strong,nonatomic) NSString *marketShare_Percentage;
@property(strong,nonatomic) NSString *quality_Rating;
@property(strong,nonatomic) NSString *promotion;
@property(strong,nonatomic) NSString* isUpdated;
@property(strong,nonatomic) MerchandisingBrandShareOfSelf* selectedSOS;


@end

@interface MerchandisingPlanogram : NSObject
@property(nonatomic) MediaFile* sourceImage;
@property(strong,nonatomic) NSString* capturedImageName;
@property(strong,nonatomic) NSString* imageMatched;
@property(strong,nonatomic) NSString* isUpdated;

//this is for local reference not linked to db or sync
@property(strong,nonatomic) NSString* imageID;
@property(strong,nonatomic) NSString* isSelected;

@end

@interface MerchandisingPOS : NSObject

@property(strong,nonatomic) NSString* comments;
@property(strong,nonatomic) NSString* posImplemented;
@property(strong,nonatomic) NSString* promotion;
@property(strong,nonatomic) NSString* productPosition;
@property(strong,nonatomic) NSString* selectedBrandCode;
@property(strong,nonatomic) NSString* isUpdated;

@property(strong,nonatomic) NSString* posMaterialSelected;
@property(strong,nonatomic) NSString* promotionMaterialSelected;
@property(strong,nonatomic) NSString* positionDetailsSelected;


@property(strong,nonatomic) NSMutableArray* posMaterialArray;
@property(strong,nonatomic) NSMutableArray* promotionMaterialArray;
@property(strong,nonatomic) NSMutableArray* positionDetailsArray;

@end

@interface MerchandisingPOSMaterial : NSObject
@property(strong,nonatomic) NSString* isSelected;
@property(strong,nonatomic) NSString* title;
@property(strong,nonatomic) NSString* brandCode;
@property(strong,nonatomic) NSString* imageName;
@end


@interface MerchandisingPromotionMaterial : NSObject
@property(strong,nonatomic) NSString* isSelected;
@property(strong,nonatomic) NSString* title;
@property(strong,nonatomic) NSString* brandCode;

@end

@interface MerchandisingPositionDetails : NSObject
@property(strong,nonatomic) NSString* isSelected;
@property(strong,nonatomic) NSString* title;
@property(strong,nonatomic) NSString* brandCode;

@end


@interface MerchandisingPlanogramObjectDetection : NSObject
@property(strong,nonatomic) UIImage* matchedImage;
@end


@interface MerchandisingCompetitorPictures : NSObject
@property(strong,nonatomic) NSString* Image_File_Name;
@property(strong,nonatomic) UIImage* capturedImage;
@end

@interface MerchandisingCompetitionInfo : NSObject

@property(strong,nonatomic) NSString* brandName;
@property(strong,nonatomic) NSString* productName;
@property(strong,nonatomic) NSString* price;
@property(strong,nonatomic) NSString* threatLevel;
@property(strong,nonatomic) NSString* stockLevel;
@property(strong,nonatomic) NSString* shelfPlacement;
@property(strong,nonatomic) NSString* activePromotion;
@property(strong,nonatomic) NSString* notes;
@property(strong,nonatomic) NSMutableArray* imagesArray;

//this is for local reference not linked to db
@property(strong,nonatomic) NSString* isUpdating;

@end

@interface MerchandisingInStoreActivity : NSObject
@property (strong,nonatomic) NSString * StoreCondition;
@property (strong,nonatomic) NSString * StoreConditionNotes;
@property (nonatomic) BOOL   ManagerInteractionStatus;
@property (strong,nonatomic) NSString *  managerInteractionNotes;
@property (nonatomic) BOOL   NewSKUListingstatus;
@property (strong,nonatomic) NSString *  NewSKUListingNotes;
@property (nonatomic) BOOL   NewPromoActivationStatus;
@property (strong,nonatomic) NSString *  NewPromoActivationstartDate;
@property (strong,nonatomic) NSString *  NewPromoActivationEndDate;
@property (strong,nonatomic) NSString *  NewPromoActivationNotes;
@property (strong,nonatomic) NSString * isUpdated;


@end

@interface MerchandisingSurveyPicturesMark : NSObject
@property (strong,nonatomic) NSString *markId;
@property (strong,nonatomic) NSString *markDescription;
@property (nonatomic) CGRect markViewframe;
@property (nonatomic) CGRect markViewframeWithRespectToImageView;
@end

@interface MerchandisingSurveyPictures : NSObject
@property(strong,nonatomic) NSString *imageName;
@property(strong,nonatomic) NSMutableArray *imageMarkArray;
@property(strong,nonatomic) NSString *imageDesc;
@property(strong,nonatomic) NSString *productName;
@end

@interface MerchandisingComments : NSObject
@property(strong,nonatomic) NSString *productName;
@property(strong,nonatomic) NSString *comment;
@end




@interface SalesWorxMerchandisingBrand : NSObject

@property(strong,nonatomic) NSString* Brand_Code;
@property(strong,nonatomic) NSMutableArray* availabilityArray;

@property(strong,nonatomic)MerchandisingBrandShare *brandShare;
@property(strong,nonatomic)MerchandisingPlanogram *planogram;
@property(strong,nonatomic)MerchandisingPOS *pos;
@property(strong,nonatomic) NSMutableArray* planogramDetailsArray;

@property(strong,nonatomic) NSMutableArray* competitionInfoArray;

@property(strong,nonatomic)MerchandisingInStoreActivity *inStoreActivity;
@property(strong,nonatomic)NSMutableArray *surveyPicturesArray;
@property(strong,nonatomic)NSMutableArray *commentsArray;

//this is for local reference
@property(strong,nonatomic)NSMutableArray *productArray;

@end



@interface SalesWorxVisit  : NSObject

@property(strong,nonatomic) VisitOptions * visitOptions;
@property(strong,nonatomic) NSString* Visit_ID;
@property(strong,nonatomic) NSString* Visit_Type;
@property(strong,nonatomic) NSString* Accompanied_By_User_ID;
@property(strong,nonatomic) NSString* Accompanied_By_User_Name;
@property(nonatomic) BOOL isPlannedVisit;
@property(nonatomic) NSString* FSR_Plan_Detail_ID;
@property(strong,nonatomic) NSString* onSiteExceptionReason;
@property(nonatomic) BOOL isSalesOrderCompleted;
@property(nonatomic) BOOL isDistributionCheckCompleted;
@property(nonatomic) BOOL isManageOrderCompleted;
@property(nonatomic) BOOL isReturnsCompleted;
@property(nonatomic) BOOL isSurveyCompleted;
@property(nonatomic) BOOL isCollectionCompleted;
@property(nonatomic) BOOL isOrderHistoryAccessed;
@property(nonatomic) BOOL isTODOCompleted;
@property(nonatomic) BOOL isFSRinCustomerLocation;
@property(nonatomic) BOOL beaconValidationSuccessfull;
@property(nonatomic) BOOL beaconAvailable;
@property(nonatomic) BOOL isReportsAccessed;
@property(nonatomic) BOOL isProductMediaAccessed;


@property(strong,nonatomic)SalesWorxBeacon * currentCustomerBeaconfromDB;
@property(strong,nonatomic)SalesWorxBeacon * detectedBeacon;
@property(nonatomic) BOOL isOpenVisit;


@end



@interface SalesOrderLineItem : NSObject
{
    
}
@property(strong,nonatomic) NSString * Guid;
@property(strong,nonatomic) Products *OrderProduct;
@property(strong,nonatomic) Products *manualFocProduct;
@property(nonatomic) double manualFOCQty;
@property(nonatomic,strong) Products *bonusProduct;
@property(nonatomic) double defaultBonusQty;
@property(nonatomic) double requestedBonusQty;
@property(nonatomic) double productDiscount;
@property(nonatomic) double productSelleingPrice;
@property(nonatomic,strong) NSDecimalNumber * OrderProductQty;
@property(nonatomic,strong) NSDecimalNumber * OrderItemTotalamount;
@property(nonatomic,strong) NSDecimalNumber * OrderItemDiscountamount;
@property(nonatomic,strong) NSDecimalNumber * OrderItemVATChargeamount;
@property(nonatomic,strong) NSDecimalNumber * OrderItemNetamount;
@property(nonatomic) double appliedDiscount;
@property(strong,nonatomic) NSMutableArray* bonusDetailsArray;
@property(nonatomic) NSString* NotesStr;
@property(strong,nonatomic) NSMutableArray* AssignedLotsArray;
@property(nonatomic) NSInteger  AppliedAssortMentPlanID;
@property(nonatomic) BOOL  showApliedAssortMentPlanBonusProducts;
@property(nonatomic) BOOL  isAssortMentBonusSavedByUser;
@property(nonatomic) BOOL  isOHDuplicateItemEntryAlertConfirmedByUser;/** this flag will be checked only for newly adding Items*/
@property(nonatomic) BOOL  isDuplicateItemEntryChcekAlertConfirmedByUser;/** this flag will be checked only for newly adding Items*/
@property(nonatomic,strong) SalesOrderLineItemVATCharge * VATChargeObj;
@property(strong,nonatomic) NSString *proximityNotes;
- (id)copy;
@end



@interface ProductBonusItem : NSObject
{
    
}
@property(strong,nonatomic) NSString* Description;
@property(strong,nonatomic) NSString* Get_Add_Per;
@property(strong,nonatomic) NSString* Get_Item;
@property(strong,nonatomic) NSString* Get_Qty;
@property(strong,nonatomic) NSString* Get_UOM;
@property(strong,nonatomic) NSString* Price_Break_Type_Code;
@property(strong,nonatomic) NSString* Prom_Qty_From;
@property(strong,nonatomic) NSString* Prom_Qty_To;
@property(strong,nonatomic) NSString* Plan_Name;
@property(strong,nonatomic) Products *bonusProduct;

@end

@interface SalesOrderAssignedLot : NSObject
{
    
}
@property(strong,nonatomic) Stock* lot;
@property(nonatomic) NSDecimalNumber * assignedQuantity;
- (id)copy;


@end

@interface RMALotType : NSObject
{
    
}
@property(strong,nonatomic) NSString* lotType;
@property(strong,nonatomic) NSString* Description;
@end


@interface SalesWorxReasonCode : NSObject
{
    
}
@property(strong,nonatomic) NSString* Reasoncode;
@property(strong,nonatomic) NSString* Description;
@property(strong,nonatomic) NSString* Purpose;

@end


@interface DailyVisitSummaryReport : NSObject
@property(strong,nonatomic) NSString * Customer_ID;
@property(strong,nonatomic) NSString * Customer_Name;
@property(strong,nonatomic) NSString * Payment;
@property(strong,nonatomic) NSString * PaymentValue;
@property(strong,nonatomic) NSString * DC;
@property(strong,nonatomic) NSString * Survey;
@property(strong,nonatomic) NSString * Order;
@property(strong,nonatomic) NSString * OrderValue;
@property(strong,nonatomic) NSString * Return;
@property(strong,nonatomic) NSString * ReturnValue;

@end
@interface SyncImage : NSObject
@property (strong,nonatomic)   NSString * imagePath;
@property (strong,nonatomic)   NSString * imageSyncType;
@property (nonatomic)   BOOL isUploaded ;
@property (nonatomic)   NSString * imageMIMEType ;
@property (nonatomic)   NSInteger uploadFailureCount ;

@end

@interface SalesWorxBrandAmbassadorLocation : NSObject
@property (strong,nonatomic)   NSString * Location_ID;
@property (strong,nonatomic)   NSString * Location_Name;
@property (strong,nonatomic)   NSString * Latitude;
@property (strong,nonatomic)   NSString * Longitude;
@property (strong,nonatomic)   NSString * City;
@property (strong,nonatomic)   NSString * Address;
@property (strong,nonatomic)   NSString * Phone;


@end

@interface SalesWorxBrandAmbassadorTask : NSObject
@property (strong,nonatomic) NSString* Task_ID;
@property (strong,nonatomic) NSString* Title;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Category;
@property (strong,nonatomic) NSString* Priority;
@property (strong,nonatomic) NSString* Start_Time;
@property (strong,nonatomic) NSString* Status;
@property (strong,nonatomic) NSString* Assigned_To;
@property (strong,nonatomic) NSString* Emp_ID;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Show_Reminder;
@property (strong,nonatomic) NSString* IS_Active;
@property (strong,nonatomic) NSString* Is_deleted;
@property (strong,nonatomic) NSString* Doctor_ID;
@property (strong,nonatomic) NSString* Doctor_Name;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Contact_Name;
@property (strong,nonatomic) NSString* Task_Location_Type;
@property (strong,nonatomic) NSString* End_Time;
@property (strong,nonatomic) NSString* Last_Updated_At;
@property (strong,nonatomic) NSString* Last_Updated_By;
@property (strong,nonatomic) NSString* Created_At;
@property (strong,nonatomic) NSString* isUpdated;
@end


@interface salesWorxBrandAmbassadorNotes : NSObject
@property (strong,nonatomic) NSString* Note_ID;
@property (strong,nonatomic) NSString* Title;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Category;
@property (strong,nonatomic) NSString* Emp_ID;
@property (strong,nonatomic) NSString* Visit_ID;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Doctor_ID;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Created_At;
@property (strong,nonatomic) NSString* Created_By;
@property (strong,nonatomic) NSString* Last_Updated_By;
@property (strong,nonatomic) NSString* Last_Updated_At;
@property (strong,nonatomic) NSString* isUpdated;
@end


@interface SalesWorxBrandAmbassadorWalkinCustomerVisit : NSObject
@property (strong,nonatomic)   NSString * Walkin_Session_ID;
@property (strong,nonatomic)   NSString * Actual_Visit_ID;
@property (strong,nonatomic)   NSString * Start_Time;
@property (strong,nonatomic)   NSString * End_Time;
@property (strong,nonatomic)   NSString * Latitude;
@property (strong,nonatomic)   NSString * Longitude;
@property (strong,nonatomic)   NSString * SalesRep_ID;
@property (strong,nonatomic)   NSString * Customer_Name;
@property (strong,nonatomic)   NSString * Customer_Email;
@property (strong,nonatomic)   NSString * Customer_Phone;
@property (strong,nonatomic)   NSMutableArray * surveyArray;

//this is not db property this is for reference
@property (strong,nonatomic)   NSString * Planned_Visit_ID;
@property (strong,nonatomic)   NSString * sellOutGiven;
@property (strong,nonatomic)   NSString * samplesGiven;





@end

@interface SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses : NSObject

@property (strong,nonatomic)   NSString * Walkin_Response_ID;
@property (strong,nonatomic)   NSString * Actual_Visit_ID;
@property (strong,nonatomic)   NSString * Survey_ID;
@property (strong,nonatomic)   NSString * Question_ID;
@property (strong,nonatomic)   NSString * Response;
@property (strong,nonatomic)   NSString * SalesRep_ID;
@property (strong,nonatomic)   NSString * Survey_Timestamp;
@property (strong,nonatomic)   NSString * Walkin_Session_ID;
@property (strong,nonatomic)   NSString * Customer_Phone;

@end



@interface SalesWorxBrandAmbassadorVisit : NSObject
@property (strong,nonatomic)   SalesWorxBrandAmbassadorLocation * selectedLocation;
@property (strong,nonatomic)   DemoPlan * selectedDemoPlan;
@property (strong,nonatomic)   NSString * Date;
@property (strong,nonatomic)   NSString * Objective;
@property (strong,nonatomic)   NSString * Planned_Visit_ID;
@property (strong,nonatomic)   NSString * Actual_Visit_ID;
@property (strong,nonatomic)   NSString * Previous_Planned_Visit_ID;
@property (strong,nonatomic)   NSString * Emp_ID;
@property (strong,nonatomic)   NSString * Visit_Status;
@property(strong,nonatomic)     NSString * Visit_Conclusion_Notes;
@property(strong,nonatomic)     NSString * Visit_Rating;
@property (strong,nonatomic)   NSMutableArray * tasksArray;
@property (strong,nonatomic)   NSMutableArray * notesArray;
@property (strong,nonatomic)    SalesWorxBrandAmbassadorWalkinCustomerVisit * walkinCustomer;
@property (strong,nonatomic)   NSMutableArray * demoedArray;
@property (strong,nonatomic)   NSMutableArray * samplesGivenArray;
@property(strong,nonatomic)     SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses * surveyResponses;
@property(strong,nonatomic)     NSString* currentCallStartTime;
@property(strong,nonatomic)     NSString* waitTime;
@property(strong,nonatomic)     NSString* Call_Start_Date;
@property(strong,nonatomic)     NSString* Call_End_Date;
@property(strong,nonatomic)     NSString* EDetailing_Start_Time;
@property(strong,nonatomic)     NSString* EDetailing_End_Time;
@property(strong,nonatomic)     NSMutableArray* walkCustomerVisitsArray;
@property(strong,nonatomic)     NSMutableArray* walkinSurveyResponsesArray;
@property(strong,nonatomic)     NSString* Old_Visit_ID;





@end


@interface SalesWorxGoogleAnalytics : NSObject

@property(strong,nonatomic)     NSString* categoryName;
@property(strong,nonatomic)     NSString* screenName;
@property(strong,nonatomic)     NSString* Action;
@property(strong,nonatomic)     NSString* label;
@property(strong,nonatomic)     NSString* userName;
@property(strong,nonatomic)     NSString* userID;
@property(nonatomic)     NSInteger value;


@end

@interface SalesWorxSalesTrend : NSObject
@property(strong,nonatomic)     NSString* Amount;
@property(strong,nonatomic)     NSString* creationDate;
@property(strong,nonatomic)     NSString* NumberofRecords;
@end

@interface SalesWorxProductStockSync : NSObject
@property(strong,nonatomic)     NSString* Custom_Attribute_1;
@property(strong,nonatomic)     NSString* Custom_Attribute_2;
@property(strong,nonatomic)     NSString* Custom_Attribute_3;
@property(strong,nonatomic)     NSString* Custom_Attribute_4;
@property(strong,nonatomic)     NSString* Custom_Attribute_5;
@property(strong,nonatomic)     NSString* Custom_Attribute_6;
@property(strong,nonatomic)     NSString* Expiry_Date;
@property(strong,nonatomic)     NSString* Item_ID;
@property(strong,nonatomic)     NSString* Last_Updated_At;
@property(strong,nonatomic)     NSString* Lot_No;
@property(strong,nonatomic)     NSString* Lot_Qty;
@property(strong,nonatomic)     NSString* Org_ID;
@property(strong,nonatomic)     NSString* Stock_ID;

@end

@interface Customer_SalesSummary : NSObject

@property(strong,nonatomic) NSString *Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString *Creation_Date;
@property(strong,nonatomic) NSString *Creation_DateWithTime;

@property(strong,nonatomic) NSString *Doc_Type;
@property(strong,nonatomic) NSString *Transaction_Amt;
@property(strong,nonatomic) NSString *Customer_No;
@property(strong,nonatomic) NSString *Customer_Name;
@property(strong,nonatomic) NSString *Cash_Cust;
@property(strong,nonatomic) NSString *Phone;
@property(strong,nonatomic) NSString *Visit_ID;
@property(strong,nonatomic) NSString *End_Time;
@property(strong,nonatomic) NSString *ERP_Ref_No;
@property(strong,nonatomic) NSString *Ship_To_Customer_Id;
@property(strong,nonatomic) NSString *Ship_To_Site_Id;

@end

@interface Customer_Statement : NSObject

@property(strong,nonatomic) NSString *Invoice_Ref_No;
@property(strong,nonatomic) NSString *NetAmount;
@property(strong,nonatomic) NSString *PaidAmt;
@property(strong,nonatomic) NSString *InvDate;
@property(strong,nonatomic) NSString *Customer_ID;
@property(strong,nonatomic) NSString *InvDateWithTime;

//ravinder code
@property(strong,nonatomic) NSString *DueAmount;
@property(strong,nonatomic) NSString *DueDate;

@end

@interface Blocked_Statement : NSObject

@property(strong,nonatomic) NSString *Contact;
@property(strong,nonatomic) NSString *Customer_ID;
@property(strong,nonatomic) NSString *Customer_Name;
@property(strong,nonatomic) NSString *Phone;

@end

@interface Review_Documents : NSObject

@property(strong,nonatomic) NSString *Amount;
@property(strong,nonatomic) NSString *Customer_Name;
@property(strong,nonatomic) NSString *Doc_Date;
@property(strong,nonatomic) NSString *Doc_DateWithTime;
@property(strong,nonatomic) NSString *Doc_No;
@property(strong,nonatomic) NSString *Doc_Type;
@property(strong,nonatomic) NSString *ERP_Status;
@property(strong,nonatomic) NSString *Start_Time;
@property(strong,nonatomic) NSString *Shipping_Instructions;
@property(strong,nonatomic) NSString *Ship_To_Customer_Id;
@property(strong,nonatomic) NSString *Ship_To_Site_Id;
@end

@interface Payment_Summary : NSObject

@property(strong,nonatomic) NSString *Amount;
@property(strong,nonatomic) NSString *Collected_Amount;
@property(strong,nonatomic) NSString *Collected_On;
@property(strong,nonatomic) NSString *Collected_OnWithTime;
@property(strong,nonatomic) NSString *Collection_ID;

@property(strong,nonatomic) NSString *Collection_Ref_No;
@property(strong,nonatomic) NSString *Collection_Type;
@property(strong,nonatomic) NSString *Customer_ID;
@property(strong,nonatomic) NSString *Customer_Name;
@property(strong,nonatomic) NSString *DocDate;
@property(strong,nonatomic) NSString *Doctype;
@property(strong,nonatomic) NSString *Start_Time;
@property(strong,nonatomic) NSString *customerID;
@property(strong,nonatomic) NSString *customerTotalPendingAmount;
@property(strong,nonatomic) NSString *Customer_No;

//bank details
@property(strong,nonatomic) NSString *Cheque_No;
@property(strong,nonatomic) NSString *Cheque_Date;
@property(strong,nonatomic) NSString *Bank_Name;
@property(strong,nonatomic) NSString *Bank_Branch;



@end

@interface TargetVsAchievement : NSObject

@property(strong,nonatomic) NSString *product_Family;
@property(strong,nonatomic) NSString *target;
@property(strong,nonatomic) NSString *sales_Total;
@property(strong,nonatomic) NSString *rate;
@property(strong,nonatomic) NSString *ach;
@property(strong,nonatomic) NSString *ideal;
@property(strong,nonatomic) NSString *actual;

@end


@interface SalesWorxFieldSalesAccompaniedBy : NSObject
@property(strong,nonatomic) NSString *User_ID;
@property(strong,nonatomic) NSString *Username;
@property(strong,nonatomic) NSString *Password;
@property(strong,nonatomic) NSString *PDA_Rights;
@property(strong,nonatomic) NSString *is_SS;
@end


@interface DistriButionCheckItem : NSObject
@property(strong,nonatomic) Products *DcProduct;
@property(strong,nonatomic) NSString *itemAvailability;
@property(strong,nonatomic) NSString *Quntity;
@property(strong,nonatomic) NSString *LotNumber;
@property(strong,nonatomic) NSString *expiryDate;
@property(strong,nonatomic) NSString *imageName;
@property(strong,nonatomic) NSString *reOrder;
@property(nonatomic) NSMutableArray  *dcItemLots;
@end

@interface DistriButionCheckMinStock : NSObject
@property(strong,nonatomic) NSString *InventoryItemID;
@property(strong,nonatomic) NSString *organizationID;
@property(strong,nonatomic) NSString *attributeValue;
@end

@interface DistriButionCheckItemLot : NSObject
@property(strong,nonatomic) NSString *Quntity;
@property(strong,nonatomic) NSString *LotNumber;
@property(strong,nonatomic) NSString *expiryDate;
@property(strong,nonatomic) NSString *isImage;
@property(strong,nonatomic) NSString * DistriButionCheckItemLotId;
@property(strong,nonatomic) NSString *reOrder;
@property(strong,nonatomic) NSString *itemAvailability;

@end

@interface DistriButionCheckLocation : NSObject
@property(strong,nonatomic) NSString* LocationName;
@property(nonatomic) NSString * LocationID;
@property(strong,nonatomic) NSMutableArray *dcItemsArray;
@property(strong,nonatomic) NSMutableArray *dcItemsUnfilteredArray;
@property(nonatomic) BOOL isLocationDCCompleted;

@end

#pragma mark Assortment_bonus

@interface SalesworxAssortmentBonusProduct :NSObject
//@property
@property(nonatomic) NSInteger  PlanId;
@property(strong,nonatomic) Products* product;
@property(nonatomic) NSDecimalNumber * assignedQty;
@property(nonatomic) NSDecimalNumber * PlanDefaultBnsQty;/**This property should only used for DB transactions*/
@end

@interface SalesworxAssortmentBonusSlab :NSObject
@property(nonatomic) NSString * Prom_Qty_From;
@property(nonatomic) NSString * Prom_Qty_To;
@property(nonatomic) NSString * Get_Qty;
@property(strong,nonatomic) NSString *Price_Break_Type_Code;
@end

@interface SalesworxAssortmentBonusPlan :NSObject
//@property
@property(nonatomic) NSInteger  PlanId;
@property(strong,nonatomic) NSMutableArray  *bonusProductsArray;
@property(strong,nonatomic) NSMutableArray  *OfferProductsArray;
@property(strong,nonatomic) NSMutableArray  *MandatoryOfferProductItemCodesArray;

@property(strong,nonatomic) NSMutableArray  *SlabsArray;
@property(strong,nonatomic) NSDecimalNumber *qtyTobeAssigned;
-(double)CalculateBonusForQty:(double)quantity;
@end

@interface ProductObjectsArrayCustomClass :JSONModel
@property(strong,nonatomic) NSMutableArray <Products *> *productsArray;
@end


@interface CustomerCollectionTarget : NSObject
@property(strong,nonatomic) NSString* Customer_No;
@property(strong,nonatomic) NSString* Target_Month;
@property(strong,nonatomic) NSString* Target_Year;
@property(strong,nonatomic) NSString* Target_Value;
@property(strong,nonatomic) NSString* Achieved_Value;
@property(strong,nonatomic) NSString* Balance_ToGo;

@end


@interface PaymentImage : NSObject
@property(strong,nonatomic) NSString* title;
@property(strong,nonatomic) UIImage* image;
@property(strong,nonatomic) NSString* imageType;
@property(strong,nonatomic) NSString* imageID;

@end

@interface CoachSurveyType : NSObject
@property(strong,nonatomic) NSString* Survey_Title;
@property(strong,nonatomic) NSString* Survey_Type_Code;
@property(strong,nonatomic) NSString* questionType;
@property(strong,nonatomic) NSString* Survey_ID;

@end

@interface CoachSurveySection : NSObject
@property(strong,nonatomic) NSString* sectionTitle;
@property(strong,nonatomic) NSMutableArray *questionsArray;

@end

@interface CoachQuestion: NSObject

@end

@interface CoachSection:NSObject

@end

@interface CoachSurvey: NSObject
@property(strong,nonatomic) NSString* level;
@property(strong,nonatomic) NSString* name;
@property(strong,nonatomic) NSMutableArray *objectsArray;
@end


@interface CoachSurveyConfirmationQuestion : NSObject

@property(strong,nonatomic) NSString* Question_ID;
@property(strong,nonatomic) NSString* Question_Text;
@property(strong,nonatomic) NSString* Response_Type_ID;
@property(strong,nonatomic) NSString* Response_ID;
@property(strong,nonatomic) NSString* Response_Display_Type;
@property(strong,nonatomic) NSString* Survey_ID;
@property(strong,nonatomic) NSString* Response_Text;
@property(nonatomic) BOOL isSignatureSaved;
@property(nonatomic) NSInteger signature_Section;
@property(strong,nonatomic) NSString* image_Name;
@end

@interface PaymentSummary : NSObject
@property(strong,nonatomic) NSString* Payment_Mode;
@property(strong,nonatomic) NSString* Receipt_Number;
@property(strong,nonatomic) NSString* Cheque_No;
@property(strong,nonatomic) NSString* Cheque_Date;
@property(strong,nonatomic) NSString* Bank_Name;
@property(strong,nonatomic) NSString* Bank_Branch;
@property(strong,nonatomic) NSString* Total_Amount;
@property(strong,nonatomic) NSString* Invoice_No;
@property(strong,nonatomic) NSString* Invoice_Date;
@property(strong,nonatomic) NSString* Invoice_Amount;
@property(strong,nonatomic) NSString* SumOfTotalInvoices;

@end


@interface RouteRescheduleDetails : NSObject
@property(strong,nonatomic) NSString* Visit_Change_ID;
@property(strong,nonatomic) NSString* FSR_Plan_Detail_ID;
@property(strong,nonatomic) NSString* Visit_Date;
@property(strong,nonatomic) NSString* Customer_ID;
@property(strong,nonatomic) NSString* Site_Use_ID;
@property(strong,nonatomic) NSString* SalesRep_ID;
@property(strong,nonatomic) NSString* Change_Type;
@property(strong,nonatomic) NSString* New_Visit_Date;
@property(strong,nonatomic) NSString* Proc_Status;
@property(strong,nonatomic) NSString* Approval_Status;
@property(strong,nonatomic) NSString* Requested_At;
@property(strong,nonatomic) NSString* Requested_By;
@property(strong,nonatomic) NSString* Last_Updated_At;
@property(strong,nonatomic) NSString* Last_Updated_By;
@property(strong,nonatomic) NSString* Approval_Action_At;
@property(strong,nonatomic) NSString* Approval_Action_By;
@property(strong,nonatomic) NSString* Cancelled_At;
@property(strong,nonatomic) NSString* Cancelled_By;
@property(strong,nonatomic) NSString* Reason;
@property(strong,nonatomic) NSString* Comments;
@property(strong,nonatomic) NSString* Remarks;

@end
