//
//  SWDistributionCheckViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/31/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWDistributionCheckViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "MedRepDefaults.h"
#import "ShowImageViewController.h"
#import "SalesWorxDistributionItemHeaderTableViewCell.h"
#import "SalesWorxDistributionItemTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxDistributionItemNewTableViewCell.h"
#import "SalesWorxDistributionStockViewController.h"



@interface SWDistributionCheckViewController ()

@end

@implementation SWDistributionCheckViewController

- (id)initWithCustomer:(NSDictionary *)c {
    self = [super init];
    
    if (self) {
        customer=c;
        [self setTitle:NSLocalizedString(@"Distribution Check", nil)];
    }
    
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // BOOL isVisitStated = [[SWVisitManager defaultManager]isVisit];
    
    self.view.backgroundColor=UIViewBackGroundColor;
    
    ///left-arrow.png
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(handleBack:)];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    
    appControl =[AppControl retrieveSingleton];
    itemDataWithLocation = [[NSMutableDictionary alloc]init];

    [_itemTableView.layer setBorderWidth: 1.0];
    [_itemTableView.layer setCornerRadius:5.0f];
    [_itemTableView.layer setMasksToBounds:YES];
    [_itemTableView.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
    [_gridMainView addSubview:_itemTableView];

    indexPathArray=[[NSMutableArray alloc]init];
    _lblName.text = [customer valueForKey:@"Customer_Name"];
    _lblNumber.text = [customer valueForKey:@"Customer_No"];
    _txtDropdown.text = @"Shelf";
    
    
    @try {
        NSMutableArray *lastDistributionCheckID = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Distribution_Check where Customer_ID = '%@' ORDER BY Checked_On DESC LIMIT 1",[customer valueForKey:@"Customer_ID"]]];
        
        NSMutableArray *arrlastStock = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select sum(Qty) as totalStock from TBL_Distribution_Check_Items where DistributionCheck_ID = '%@'",[[lastDistributionCheckID  objectAtIndex:0] valueForKey:@"DistributionCheck_ID"]]];
        
        _lblLastVisitStock.text = [NSString stringWithFormat:@"%@",[[arrlastStock  objectAtIndex:0] valueForKey:@"totalStock"]];
        _lblLastVisitOn.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:[[lastDistributionCheckID objectAtIndex:0] valueForKey:@"Checked_On"]];
    } @catch (NSException *exception) {
        _lblLastVisitStock.text = @"N/A";
        _lblLastVisitOn.text = @"N/A";
    }
    
    items = [[SWDatabaseManager retrieveManager] dbGetDistributionCollection];
    [self loadPieChart];
    
    _imgView.userInteractionEnabled = YES;
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
    
    if ([items count] >0)
    {
        [_itemTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
        [self tableView:_itemTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    else
    {
        _txtDropdown.hidden = YES;
        _btnDropDown.hidden = YES;
        _itemTableView.hidden = YES;
        _detailView.hidden = YES;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    /****** check ENABLE_DIST_CHECK_MULTI_LOCATION is Y or N ******/
    
    appControl =[AppControl retrieveSingleton];
    
    _arrayOfLocation= [[NSMutableArray alloc]init];
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOCATION isEqualToString:@"Y"])
    {
        NSLog(@"Yes");
        NSArray *arrOfLocations = [self fetchListValueOfLocations];
        for (int i = 0; i<[arrOfLocations count]; i++)
        {
            NSString *str = [self fetchListOfLocations:[arrOfLocations objectAtIndex:i]];
            [_arrayOfLocation addObject:str];
        }
    }
    else
    {
        _txtDropdown.hidden = YES;
        _btnDropDown.hidden = YES;
    }
    
    locationKey = @"S";
    
    
    /************************************************************/
    /******* check ENABLE_DIST_CHECK_MULTI_LOTS is Y or N *******/
    /************************************************************/
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
//        [_detailMainView setFrame:CGRectMake(_detailMainView.frame.origin.x, _detailMainView.frame.origin.y, _detailMainView.frame.size.width, _detailMainView.frame.size.height+44)];
//        [_ImageMainView setFrame:CGRectMake(_ImageMainView.frame.origin.x, _ImageMainView.frame.origin.y+44, _ImageMainView.frame.size.width, _ImageMainView.frame.size.height-44)];
//        
//        _lblImage.hidden = YES;
//        _dividerLineForImageView.hidden = YES;
//        
//        [_imgView setFrame:CGRectMake(_imgView.frame.origin.x, 8, _imgView.frame.size.width, _imgView.frame.size.height+10)];

//        btnAdd = [[UIButton alloc]initWithFrame:CGRectMake(230.0, 290.0, 80.0, 30.0)];
//        [btnAdd addTarget:self action:@selector(btnAddAvailability:) forControlEvents:UIControlEventTouchUpInside];
//        [btnAdd setTitle:@"Add" forState:UIControlStateNormal];
//        btnAdd.backgroundColor = KDistributionCheckAvailabilityColor;
//        btnAdd.layer.cornerRadius = 4;
//        btnAdd.enabled = NO;
//        btnAdd.alpha = 0.7;
//        [_detailMainView addSubview:btnAdd];
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        btnAdd.backgroundColor = KDistributionCheckAvailabilityColor;
        [btnAdd addTarget:self action:@selector(btnAddAvailability:) forControlEvents:UIControlEventTouchUpInside];
        [btnAdd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        NSArray *arri = [_tabAvailable subviews];
        [[arri objectAtIndex:0] setTintColor:KDistributionCheckUnAvailabilityColor];
        [[arri objectAtIndex:1] setTintColor:KDistributionCheckAvailabilityColor];
    }
    else{
        [btnAdd setHidden:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Distribution Check  View"];
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesDistributionCheckScreenName];
    }

    
    if (flagRetake)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
            
            NSString *savedImagePath;
            if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
            {
                savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[tempDistributionItem valueForKey:@"DistributionCheckLine_ID"]]];
            }
            else
            {
                savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[distributionItem valueForKey:@"DistributionCheckLine_ID"]]];
            }
            _imgView.image = [UIImage imageWithContentsOfFile:savedImagePath];
        });
    }
    flagRetake = false;
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here

    // make sure you do this!
    // pop the controller
    if(anyUpdated)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Would you like to close the form without updating the distribution information?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Yes", nil) otherButtonTitles:NSLocalizedString(@"No", nil),nil];
        alert.tag=1;
        [alert show];
    }
    else {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
}
#pragma mark
-(void)disableFields
{
    _txtQuantity.text = @"";
    _txtLotNo.text = @"";
    _txtExpiryDate.text = @"";
    
    _txtQuantity.enabled = NO;
    _txtQuantity.alpha = 0.7;
    
    _txtLotNo.enabled = NO;
    _txtLotNo.alpha = 0.7;
    
    _txtExpiryDate.enabled = NO;
    _txtExpiryDate.alpha = 0.7;
    
    [_imgView removeGestureRecognizer:tap];
}
-(void)enableFields
{
    _txtQuantity.enabled = YES;
    _txtQuantity.alpha = 1.0;
    
    _txtLotNo.enabled = YES;
    _txtLotNo.alpha = 1.0;
    
    _txtExpiryDate.enabled = YES;
    _txtExpiryDate.alpha = 1.0;
    
    [_imgView addGestureRecognizer:tap];
}

#pragma mark Load PieChart
-(void)loadPieChart
{
    shouldAllowSave = YES;
    
    int updatedDistribution = 0;
    int nonUpdatedDistribution = 0;
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        @try {
            for (int i = 0; i<[items count]; i++)
            {
                if ([itemDataWithLocation objectForKey:[NSNumber numberWithInt:i]] != nil)
                {
                    updatedDistribution++;
                    anyUpdated = YES;
                } else {
                    nonUpdatedDistribution++;
                    shouldAllowSave = NO;
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@",exception);
        }
    }
    else
    {
        @try {
            for (int i = 0; i<[items count]; i++) {
                if ([[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:i]]objectForKey:@"S"] valueForKey:@"Avl"] != nil || [[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:i]]objectForKey:@"B"] valueForKey:@"Avl"] != nil)
                {
                    updatedDistribution++;
                    anyUpdated = YES;
                } else {
                    nonUpdatedDistribution++;
                    shouldAllowSave = NO;
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@",exception);
        }
    }
    
    [pieChartNew removeFromSuperview];
    pieChartNew = [[PNCircleChart alloc] initWithFrame:CGRectMake(0, 0, 90, 90) total:[NSNumber numberWithInt:(updatedDistribution+nonUpdatedDistribution)] current:[NSNumber numberWithInt:updatedDistribution] clockwise:NO shadow:YES shadowColor:[UIColor lightGrayColor] check:YES lineWidth:@8.0f];
    
    pieChartNew.backgroundColor = [UIColor clearColor];
    [pieChartNew setStrokeColor:[UIColor clearColor]];
    [pieChartNew setStrokeColorGradientStart:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [pieChartNew strokeChart];
    [checkCompletionPieChartContainerView addSubview:pieChartNew];

    if (shouldAllowSave)
    {
        allUpdated=YES;
    }
}

#pragma mark Button Actions
- (IBAction)tabAvailable:(id)sender
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        btnAdd.enabled = YES;
        btnAdd.alpha = 1.0;
        
        tempDistributionItem = [[NSMutableDictionary alloc]init];
        tempDistributionItem = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:selectedRowIndex]];
        
        if (_tabAvailable.selectedSegmentIndex == 0)
        {
            //[tempDistributionItem setValue:@"Y" forKey:@"Avl"];
            [self enableFields];
        } else
        {
            //[tempDistributionItem setValue:@"N" forKey:@"Avl"];
            [self disableFields];
        }

        [tempDistributionItem setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
        [tempDistributionItem setValue:nil forKey:@"Qty"];
        [tempDistributionItem setValue:nil forKey:@"LotNo"];
        [tempDistributionItem setValue:nil forKey:@"ExpDate"];
        [tempDistributionItem setValue:nil forKey:@"Image"];
    }
    else
    {
        if (_tabAvailable.selectedSegmentIndex == 0)
        {
            if ([distributionItem valueForKey:@"Image"] == nil)
            {
                [distributionItem setValue:@"N" forKey:@"Image"];
            }
            [distributionItem setValue:@"Y" forKey:@"Avl"];
            [self enableFields];
        } else
        {
            [distributionItem setValue:@"N" forKey:@"Avl"];
            [distributionItem setValue:@"" forKey:@"Qty"];
            [distributionItem setValue:@"" forKey:@"LotNo"];
            [distributionItem setValue:@"N" forKey:@"Image"];
            [distributionItem setValue:@"" forKey:@"ExpDate"];
            
            _imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
            [self disableFields];
        }
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
}
- (IBAction)btnDropDown:(id)sender
{
    [self.view endEditing:YES];
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = _arrayOfLocation;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    paymentPopOverController=nil;
    paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.popOverController=paymentPopOverController;
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [paymentPopOverController presentPopoverFromRect:_txtDropdown.dropdownImageView.frame inView:_txtDropdown.rightView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
- (IBAction)btnExpiryDate:(id)sender
{
    [self.view endEditing:YES];
    
    popOverTitle=@"Check Date";
    NSLog(@"payment method tapped");
    
    SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
    popOverVC.didSelectDateDelegate=self;
    popOverVC.titleString = @"Expiry Date";
    
    if (_txtExpiryDate.text == nil || [_txtExpiryDate.text isEqualToString:@""])
    {
        popOverVC.setMinimumDateCurrentDate=YES;
    } else {
        popOverVC.setMinimumDateCurrentDate=NO;
        popOverVC.previousSelectedDate = previousSelectedDate;
    }
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    paymentPopOverController=nil;
    paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.datePickerPopOverController=paymentPopOverController;
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
    [paymentPopOverController presentPopoverFromRect:_txtExpiryDate.dropdownImageView.frame inView:_txtExpiryDate.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)didSelectDate:(NSString *)selectedDate
{
    previousSelectedDate = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:@"yyyy-MM-dd" scrString:selectedDate];
    [_txtExpiryDate setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:MM:ss" destFormat:kDateFormatWithoutTime scrString:selectedDate]];
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        [tempDistributionItem setValue:previousSelectedDate forKey:@"ExpDate"];
    }
    else
    {
        [distributionItem setValue:previousSelectedDate forKey:@"ExpDate"];
        [self checkImage];
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
}

#pragma mark UITableView Data source Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return items.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44.0)];
        _headerView.backgroundColor = kUITableViewHeaderBackgroundColor;
        [_headerView.layer setBorderWidth: 1.0];
        [_headerView.layer setMasksToBounds:YES];
        [_headerView.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
        [view addSubview:_headerView];
        
        return view;
    }
    else
    {
        static NSString* identifier=@"invoicesHeaderCell";
        
        SalesWorxDistributionItemHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemHeaderTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell.contentView.layer setBorderWidth: 1.0];
        [cell.contentView.layer setMasksToBounds:YES];
        [cell.contentView.layer setBorderColor:UITextFieldDarkBorderColor.CGColor];
        
        return cell;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"Cell";
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        SalesWorxDistributionItemNewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemNewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        if ([indexPathArray containsObject:indexPath])
        {
            cell.lblCode.textColor = [UIColor whiteColor];
            cell.lblDescription.textColor = [UIColor whiteColor];
            cell.lblQuantity.textColor = [UIColor whiteColor];
            [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        }
        else
        {
            cell.lblCode.textColor = MedRepDescriptionLabelFontColor;
            cell.lblDescription.textColor = MedRepDescriptionLabelFontColor;
            cell.lblQuantity.textColor = MedRepDescriptionLabelFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }
        
        cell.lblCode.text = [[items objectAtIndex:indexPath.row]stringForKey:@"Item_Code"];
        cell.lblDescription.text = [[items objectAtIndex:indexPath.row]stringForKey:@"Description"];
        
        
       // [itemDataWithLocation objectForKey:[NSNumber numberWithInteger:indexPath.row]]
        
        NSMutableArray * qtyArray=[[itemDataWithLocation objectForKey:[NSNumber numberWithInteger:indexPath.row]] valueForKey:@"Qty"];;
        
        if (qtyArray.count>0) {
            NSInteger updatedQty=0;
            for (NSInteger i=0; i<qtyArray.count; i++) {
                updatedQty=updatedQty+[[SWDefaults getValidStringValue:[qtyArray objectAtIndex:i] ]integerValue];
            }
            if (updatedQty>0) {
                cell.lblQuantity.text = [NSString stringWithFormat:@"%ld",(long)updatedQty];
  
            }
            else{
                cell.lblQuantity.text = @"";
  
            }

        }
        else{
            cell.lblQuantity.text = [[items objectAtIndex:indexPath.row]stringForKey:@"Qty"];
  
        }
        
        
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"Y"])
        {
            cell.statusView.backgroundColor = KItemAvailable;
        }
        else if([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"N"])
        {
            cell.statusView.backgroundColor = KItemNotAvailable;
        }
        else
        {
            cell.statusView.backgroundColor = KNoChange;
        }
        return cell;
        
    }
    else
    {
        SalesWorxDistributionItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if ([indexPathArray containsObject:indexPath])
        {
            cell.lblCode.textColor = [UIColor whiteColor];
            cell.lblDescription.textColor = [UIColor whiteColor];
            cell.lblQuantity.textColor = [UIColor whiteColor];
            cell.lblExpiry.textColor = [UIColor whiteColor];
            [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        }
        else
        {
            cell.lblCode.textColor = MedRepDescriptionLabelFontColor;
            cell.lblDescription.textColor = MedRepDescriptionLabelFontColor;
            cell.lblQuantity.textColor = MedRepDescriptionLabelFontColor;
            cell.lblExpiry.textColor = MedRepDescriptionLabelFontColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }
        
        cell.lblCode.text = [[items objectAtIndex:indexPath.row]stringForKey:@"Item_Code"];
        cell.lblDescription.text = [[items objectAtIndex:indexPath.row]stringForKey:@"Description"];
        cell.lblQuantity.text = [[items objectAtIndex:indexPath.row]stringForKey:@"Qty"];
        cell.lblExpiry.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[[items objectAtIndex:indexPath.row]stringForKey:@"ExpDate"]];
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"Y"])
        {
            cell.statusView.backgroundColor = KItemAvailable;
        }
        else if([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"N"])
        {
            cell.statusView.backgroundColor = KItemNotAvailable;
        }
        else
        {
            cell.statusView.backgroundColor = KNoChange;
        }
        return cell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"More" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                //insert your editAction here
                                                @try {
                                                    [self editTableRow:indexPath];
                                                }
                                                @catch (NSException *exception) {
                                                    NSLog(@"%@",exception);
                                                }
                                                
                                            }];
        editAction.backgroundColor = [UIColor redColor];
        return @[editAction];
    }
    return nil;
}
-(void)editTableRow:(NSIndexPath *)indexPath
{
    editIndexPath = indexPath;
    [self.view endEditing:YES];
//    SalesWorxDistributionStockViewController *salesOrderStockInfoViewController=[[SalesWorxDistributionStockViewController alloc]initWithNibName:@"SalesWorxDistributionStockViewController" bundle:[NSBundle mainBundle]];
//    salesOrderStockInfoViewController.view.backgroundColor = [UIColor clearColor];
//    salesOrderStockInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
//    salesOrderStockInfoViewController.Delegate = self;
//    salesOrderStockInfoViewController.strCode = [[items objectAtIndex:indexPath.row] valueForKey:@"Item_Code"];
//    salesOrderStockInfoViewController.strDescription = [[items objectAtIndex:indexPath.row] valueForKey:@"Description"];
//    salesOrderStockInfoViewController.stockLotsArray = [itemDataWithLocation objectForKey:[NSNumber numberWithInteger:indexPath.row]];
//    [self.navigationController presentViewController:salesOrderStockInfoViewController animated:NO completion:nil];
}
-(void)updateStockValue:(NSMutableArray *)updatedStockArray
{
    [itemDataWithLocation setObject:updatedStockArray forKey:[NSNumber numberWithInteger:editIndexPath.row]];
    @try
    {
        NSArray *allKeys = [itemDataWithLocation allKeys];
        for (int i = 0; i<[allKeys count]; i++)
        {
            NSArray *arrOfStock = [itemDataWithLocation objectForKey:[allKeys objectAtIndex:i]];
            if ([arrOfStock count] == 0)
            {
                tempDistributionItem = nil;
                [itemDataWithLocation removeObjectForKey:[allKeys objectAtIndex:i]];
                [self loadPieChart];
                
                NSDictionary *dict = [items objectAtIndex:[[allKeys objectAtIndex:i]intValue]];
                
                [dict setValue:nil forKey:@"Avl"];
                [dict setValue:nil forKey:@"DistributionCheckLine_ID"];
                [dict setValue:nil forKey:@"Qty"];
                [dict setValue:nil forKey:@"LotNo"];
                [dict setValue:nil forKey:@"ExpDate"];
                [dict setValue:nil forKey:@"Loc"];
                
                tempDistributionItem = [items objectAtIndex:[[allKeys objectAtIndex:i]intValue]];
                [_itemTableView reloadData];
            }
            else if(editIndexPath.row == [[allKeys objectAtIndex:i]intValue])
            {
                NSDictionary *dict = [items objectAtIndex:editIndexPath.row];
                
                if ([[[arrOfStock objectAtIndex:0]valueForKey:@"Avl"] isEqualToString:@"N"])
                {
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"Avl"] forKey:@"Avl"];
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"DistributionCheckLine_ID"] forKey:@"DistributionCheckLine_ID"];
                    [dict setValue:nil forKey:@"Qty"];
                    [dict setValue:nil forKey:@"LotNo"];
                    [dict setValue:nil forKey:@"ExpDate"];
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"Loc"] forKey:@"Loc"];
                }
                else
                {
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"Avl"] forKey:@"Avl"];
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"DistributionCheckLine_ID"] forKey:@"DistributionCheckLine_ID"];
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"Qty"] forKey:@"Qty"];
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"LotNo"] forKey:@"LotNo"];
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"ExpDate"] forKey:@"ExpDate"];
                    [dict setValue:[[arrOfStock objectAtIndex:0]valueForKey:@"Loc"] forKey:@"Loc"];
                }
                tempDistributionItem = [items objectAtIndex:editIndexPath.row];
                [_itemTableView reloadData];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}

#pragma mark UITableView Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    
    [UIView setAnimationsEnabled:YES];
    if (indexPathArray.count==0)
    {
        if (indexPath==nil)
        {
            [indexPathArray addObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        else
        {
            [indexPathArray addObject:indexPath];
        }
    }
    else
    {
        [indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
    }
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        SalesWorxDistributionItemNewTableViewCell *cell = (SalesWorxDistributionItemNewTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        cell.lblQuantity.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        tempDistributionItem = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:indexPath.row]];
        
        if (!tempDistributionItem[@"DistributionCheckLine_ID"])
        {
            [tempDistributionItem setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
        }
        selectedIndex = indexPath;
        selectedRowIndex = (int) indexPath.row;

        [self disableFields];
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;

        _txtDropdown.text = @"Shelf";
        
        _txtQuantity.text = nil;
        _txtLotNo.text = nil;
        _txtExpiryDate.text = nil;
        _imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
    }
    else
    {
        SalesWorxDistributionItemTableViewCell *cell = (SalesWorxDistributionItemTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        cell.lblQuantity.textColor = [UIColor whiteColor];
        cell.lblExpiry.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        distributionItem=[NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:indexPath.row]];
        
        if (!distributionItem[@"DistributionCheckLine_ID"])
        {
            [distributionItem setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
        }
        selectedIndex = indexPath;
        selectedRowIndex = (int)indexPath.row;
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"Y"])
        {
            cell.statusView.backgroundColor = KItemAvailable;
            [self enableFields];
            _tabAvailable.selectedSegmentIndex = 0;
        } else if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"N"])
        {
            cell.statusView.backgroundColor = KItemNotAvailable;
            [self disableFields];
            _tabAvailable.selectedSegmentIndex = 1;
        }
        else
        {
            cell.statusView.backgroundColor = KNoChange;
            [self disableFields];
            _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        }
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Loc"] isEqualToString:@"B"]) {
            _txtDropdown.text = @"Back Room";
        } else {
            _txtDropdown.text = @"Shelf";
        }
        
        _txtQuantity.text = [[items objectAtIndex:indexPath.row] valueForKey:@"Qty"];
        _txtLotNo.text = [[items objectAtIndex:indexPath.row] valueForKey:@"LotNo"];
        _txtExpiryDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[[items objectAtIndex:indexPath.row] valueForKey:@"ExpDate"]];
        
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Image"] isEqualToString:@"Y"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString *key;
                if ([_txtDropdown.text isEqualToString:@"Shelf"])
                {
                    key = @"S";
                } else {
                    key = @"B";
                }
                @try {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
                    NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:key] valueForKey:@"DistributionCheckLine_ID"]]];
                    _imgView.image = [UIImage imageWithContentsOfFile:savedImagePath];
                }
                @catch (NSException *exception)
                {
                    _imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
                    NSLog(@"%@",exception);
                }
            });
        } else
        {
            _imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
        }
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        [self itemUpdate:tempDistributionItem];
    }
    else
    {
        [self itemUpdate:distributionItem];
        //[self loadPieChart];
    }
}

#pragma mark update item details
-(void)itemUpdate:(NSMutableDictionary *)item
{
    if (item)
    {
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
        {
            [_itemTableView reloadData];
            if ([item valueForKey:@"Avl"] != nil)
            {
                if (![item valueForKey:@"Loc"])
                {
                    if ([_txtDropdown.text isEqualToString:@"Shelf"])
                    {
                        [item setValue:@"S" forKey:@"Loc"];
                    } else {
                        [item setValue:@"B" forKey:@"Loc"];
                    }
                }
                
                NSMutableArray *array = [NSMutableArray arrayWithArray:items];
                [array replaceObjectAtIndex:selectedRowIndex withObject:item];
                
                items = array;
                [_itemTableView reloadData];
                
                NSMutableDictionary *firstOne = [NSMutableDictionary dictionary];
                [firstOne setObject:item forKey:[item valueForKey:@"Loc"]];
                
                
                if ([itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]])
                {
                    if (isAddButtonPressed)
                    {
                        stockArray = [itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]];
                        
                        [stockArray addObject:item];
                        [itemDataWithLocation setObject:stockArray forKey:[NSNumber numberWithInt:selectedRowIndex]];
                    }
                } else
                {
                    if (isAddButtonPressed)
                    {
                        stockArray  = [[NSMutableArray alloc]init];
                        [stockArray addObject:item];
                        [itemDataWithLocation setObject:stockArray forKey:[NSNumber numberWithInt:selectedRowIndex]];
                        [self loadPieChart];
                    }
                }
                isAddButtonPressed = false;
            }
        }
        else
        {
            if (![item valueForKey:@"Loc"])
            {
                if ([_txtDropdown.text isEqualToString:@"Shelf"])
                {
                    [item setValue:@"S" forKey:@"Loc"];
                } else {
                    [item setValue:@"B" forKey:@"Loc"];
                }
            }

            if (([item valueForKey:@"Avl"] == nil) && ((_txtQuantity.text.length >0) || (_txtLotNo.text.length >0) || (_txtExpiryDate.text.length >0)))
            {
                [item setValue:@"Y" forKey:@"Avl"];
            }
            
            NSMutableArray *array = [NSMutableArray arrayWithArray:items];
            [array replaceObjectAtIndex:selectedRowIndex withObject:item];
            
            items = array;
            [_itemTableView reloadData];
            
            NSMutableDictionary *firstOne = [NSMutableDictionary dictionary];
            [firstOne setObject:item forKey:[item valueForKey:@"Loc"]];
            
            if ([item valueForKey:@"Avl"] != nil)
            {
                if ([itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]])
                {
                    [[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]] setObject:item forKey:[item valueForKey:@"Loc"]];
                } else
                {
                    [itemDataWithLocation setObject:firstOne forKey:[NSNumber numberWithInt:selectedRowIndex]];
                    [self loadPieChart];
                }
            }
        }
    }
}


#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    if(textField==_txtDropdown)
    {
        [self btnDropDown:textField];
        return NO;
    }
    if(textField==_txtExpiryDate)
    {
        [self btnExpiryDate:textField];
        return NO;
    }

    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtQuantity)
    {
        [textField resignFirstResponder];
        [_txtLotNo becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"N"])
    {
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
    return YES;
}

#define MAX_LENGTH 100
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //OLA! Allow only positive numerical input for quantity
    
    if (textField == _txtQuantity)
    {
        NSString *allowedCharacters;
        if ([textField.text length]>0)//no decimal point yet, hence allow point
            allowedCharacters = @"0123456789";
        else
            allowedCharacters = @"123456789";//first input character cannot be '0'
        
        NSString *newText = [textField.text stringByReplacingCharactersInRange: range withString:string];
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
        {
            [tempDistributionItem setValue:newText forKey:@"Qty"];
        }
        else
        {
            [distributionItem setValue:newText forKey:@"Qty"];
        }
    }
    else if (textField == _txtLotNo)
    {
        NSString *newText = [textField.text stringByReplacingCharactersInRange: range withString:string];
        if( [newText length]<= MAX_LENGTH ){
            if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
            {
                [tempDistributionItem setValue:newText forKey:@"LotNo"];
            }
            else
            {
                [distributionItem setValue:newText forKey:@"LotNo"];
            }
            return YES;
        }
        // case where text length > MAX_LENGTH
        textField.text = [newText substringToIndex: MAX_LENGTH ];
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
        {
            [tempDistributionItem setValue:textField.text forKey:@"LotNo"];
        }
        else
        {
            [distributionItem setValue:textField.text forKey:@"LotNo"];
        }
        return NO;
    }
    return YES;
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 220; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
    //[self.view setFrame:CGRectMake(0, -50, 1024, 768)];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
   // [self.view setFrame:CGRectMake(0, 65, 1024, 768)];
    
    [self animateTextField:textField up:NO];

    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        if (textField == _txtQuantity)
        {
            [tempDistributionItem setValue:textField.text forKey:@"Qty"];
        } else {
            [tempDistributionItem setValue:textField.text forKey:@"LotNo"];
        }
    }
    else
    {
        if (textField == _txtQuantity)
        {
            [distributionItem setValue:textField.text forKey:@"Qty"];
        } else {
            [distributionItem setValue:textField.text forKey:@"LotNo"];
        }
        [self checkImage];
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
}
#pragma mark alert delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1) {
        if (buttonIndex==0) {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController  popViewControllerAnimated:YES];
            
            @try {
                for (int i=0; i<[items count]; i++)
                {
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
                    
                    NSString *str = [[items objectAtIndex:i]valueForKey:@"DistributionCheckLine_ID"];
                    NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",str]];
                    NSError *error;
                    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                    if (!success)
                    {
                        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
        }
        else {
        }
    }
}
#pragma mark
- (void)save:(id)sender
 {
     if (allUpdated) {
         [self.view endEditing:YES];
         allUpdated = NO;
         Singleton *single = [Singleton retrieveSingleton];
         single.distributionRef = [[SWDatabaseManager retrieveManager] saveDistributionCheckWithCustomerInfo:customer andDistChectItemInfo:itemDataWithLocation];
         single.isDistributionChecked = YES;
         single.isDistributionItemGet = NO;
         
         NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kDistributionCheckTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
         [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                             object:self
                                                           userInfo:visitOptionDict];

        // [SWDefaults showAlertAfterHidingKeyBoard:@"Message" andMessage:@"Distribution information saved successfully" withController:self];
         [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Distribution information saved successfully", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
         [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
         [self.navigationController  popViewControllerAnimated:YES];
     }
     else
     {
         [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Kindly complete all distribution check", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
     }
}

#pragma mark UIPopOverDelegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC {
    popoverController=nil;
    selectedRowIndex = -1;
}
-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
    }
    else
    {
        @try {
            if (![_txtDropdown.text isEqualToString:[_arrayOfLocation objectAtIndex:selectedIndexPath.row]])
            {
                _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
                
                if ([_txtDropdown.text isEqualToString:@"Shelf"])
                {
                    locationKey = @"S";
                } else {
                    locationKey = @"B";
                }
                
                if ([[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey])
                {
                    NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
                    [mutDict setDictionary:distributionItem];
                    distributionItem = nil;
                    distributionItem = mutDict;
                    
                    if ([[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Image"] isEqualToString:@"Y"])
                    {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
                        NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"DistributionCheckLine_ID"]]];
                        _imgView.image = [UIImage imageWithContentsOfFile:savedImagePath];
                        [distributionItem setValue:@"Y" forKey:@"Image"];
                    } else
                    {
                        _imgView.image = [UIImage imageNamed:@"ProductsDefaultImage"];
                        [distributionItem setValue:@"N" forKey:@"Image"];
                    }
                    
                    if ([[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Avl"] isEqualToString:@"Y"])
                    {
                        _tabAvailable.selectedSegmentIndex = 0;
                        [self enableFields];
                        [distributionItem setValue:@"Y" forKey:@"Avl"];
                        
                    } else if ([[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Avl"] isEqualToString:@"N"])
                    {
                        _tabAvailable.selectedSegmentIndex = 1;
                        [self disableFields];
                        [distributionItem setValue:@"N" forKey:@"Avl"];
                    }
                    else
                    {
                        [distributionItem setValue:@"" forKey:@"Image"];
                        _tabAvailable.selectedSegmentIndex = -1;
                        [self disableFields];
                        [distributionItem setValue:nil forKey:@"Avl"];
                    }
                    _txtQuantity.text = [[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Qty"];
                    _txtLotNo.text = [[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"LotNo"];
                    _txtExpiryDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"ExpDate"]];
                    
                    [distributionItem setValue:[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"DistributionCheckLine_ID"] forKey:@"DistributionCheckLine_ID"];
                    [distributionItem setValue:_txtQuantity.text forKey:@"Qty"];
                    [distributionItem setValue:_txtLotNo.text forKey:@"LotNo"];
                    [distributionItem setValue:[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"ExpDate"] forKey:@"ExpDate"];
                    
                }
                else
                {
                    NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
                    [mutDict setDictionary:distributionItem];
                    [mutDict setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
                    distributionItem = nil;
                    distributionItem = mutDict;
                    
                    _tabAvailable.selectedSegmentIndex = -1;
                    [self disableFields];
                    _txtQuantity.text = @"";
                    _txtLotNo.text = @"";
                    _txtExpiryDate.text = @"";
                    [distributionItem setValue:nil forKey:@"Qty"];
                    [distributionItem setValue:nil forKey:@"LotNo"];
                    [distributionItem setValue:nil forKey:@"ExpDate"];
                    [distributionItem setValue:nil forKey:@"Image"];
                    [distributionItem setValue:nil forKey:@"Avl"];
                    _imgView.image = [UIImage imageNamed:@"ProductsDefaultImage" ];
                }
                
                NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
                [mutDict setDictionary:distributionItem];
                [mutDict setValue:locationKey forKey:@"Loc"];
                distributionItem = nil;
                distributionItem = mutDict;
                //[self itemUpdate:distributionItem];
            }
            else
            {
                _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
            }
        }
        @catch (NSException *exception) {
            _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
        }
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
}

#pragma mark
-(NSMutableArray*)fetchListValueOfLocations
{
    NSString* specilizationQry=[NSString stringWithFormat:@"select IFNULL(code_Value,'N/A') AS Dist_Check_Locations from TBL_App_Codes  where code_type ='DIST_CHECK_LOCATIONS'"];
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
    if (contentArray.count>0)
    {
        NSLog(@"Dist Check Locations  are %@", [contentArray description]);
        NSMutableArray* tempArry=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<contentArray.count; i++)
        {
            [tempArry addObject:[[contentArray valueForKey:@"Dist_Check_Locations"] objectAtIndex:i]];
        }
        return tempArry;
    }
    {
        return 0;
    }
}
-(NSString*)fetchListOfLocations:(NSString*)productType
{
    NSString* appCodeDesc;
    
    NSString* appCodeQry=[NSString stringWithFormat:@"select  Code_Description from TBL_App_Codes  where Code_Type='DIST_CHECK_LOCATIONS' and Code_Value='%@'",productType];
    //NSLog(@"qry for app code %@", appCodeQry);
    
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    if (appCodeArray.count>0)
    {
        appCodeDesc=[[appCodeArray objectAtIndex:0]valueForKey:@"Code_Description"];
        return appCodeDesc;
    }
    else
    {
        return nil;
    }
}

#pragma mark
#pragma mark Add Availability
-(void) btnAddAvailability:(UIButton*)sender
{
   [self.view endEditing:YES];
    isAddButtonPressed = YES;
    
    if (_tabAvailable.selectedSegmentIndex == 0)
    {
        if (_txtQuantity.text.length == 0 || _txtExpiryDate.text.length == 0) {
            isAddButtonPressed = NO;
            [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Quantity and Expiry Date are mandatory" withController:self];
        }
        else
        {
            [self addItems];
        }
    }
    else
    {
        [self addItems];
    }
}
-(void)addItems
{
    BOOL duplicacy = NO;
    
    if ([_txtDropdown.text isEqualToString:@"Shelf"])
    {
        [tempDistributionItem setValue:@"S" forKey:@"Loc"];
    } else {
        [tempDistributionItem setValue:@"B" forKey:@"Loc"];
    }
    
    if (_tabAvailable.selectedSegmentIndex == 0)
    {
        [tempDistributionItem setValue:@"Y" forKey:@"Avl"];
    } else if (_tabAvailable.selectedSegmentIndex == 1)
    {
        [tempDistributionItem setValue:@"N" forKey:@"Avl"];
    }
    
    @try {
        NSMutableArray *arr = [itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]];
        if ([arr count] == 0)
        {
            btnAdd.enabled = NO;
            btnAdd.alpha = 0.7;
            
            [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
            [self disableFields];
            _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        }
        for (int i = 0; i<[arr count]; i++)
        {
            if ([[[[arr objectAtIndex:i]valueForKey:@"LotNo"]localizedLowercaseString] isEqualToString:[[tempDistributionItem valueForKey:@"LotNo"]localizedLowercaseString]] && [[[arr objectAtIndex:i]valueForKey:@"ExpDate"] isEqualToString:[tempDistributionItem valueForKey:@"ExpDate"]])
            {
                duplicacy = YES;
                break;
            }
            else
            {
                duplicacy = NO;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    if (duplicacy)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Item is already updated for this Lot No" withController:self];
    }
    else
    {
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
        [self disableFields];
        _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
    }
}

#pragma mark
#pragma mark image action
- (void)imageTapped:(UIGestureRecognizer *)gestureRecognizer
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        if ([_imgView.image isEqual:[UIImage imageNamed:@"ProductsDefaultImage"]])
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.videoQuality=UIImagePickerControllerQualityTypeLow;
                imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }
        else
        {
            flagRetake = true;
            ShowImageViewController * popOverVC=[[ShowImageViewController alloc]init];
            popOverVC.imageName = [tempDistributionItem valueForKey:@"DistributionCheckLine_ID"];
            popOverVC.image = _imgView.image;
            [self.navigationController pushViewController:popOverVC animated:YES];
        }
    }
    else
    {
        if ([_imgView.image isEqual:[UIImage imageNamed:@"ProductsDefaultImage"]])
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.videoQuality=UIImagePickerControllerQualityTypeLow;

                imagePicker.delegate = self;
                imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }
        else
        {
            flagRetake = true;
            ShowImageViewController * popOverVC=[[ShowImageViewController alloc]init];
            popOverVC.imageName = [distributionItem valueForKey:@"DistributionCheckLine_ID"];
            popOverVC.image = _imgView.image;
            [self.navigationController pushViewController:popOverVC animated:YES];
        }
    }
}
#pragma mark image delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:(NSString*)kUTTypeImage])
        {
            _imgView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self saveImageInDirectory];
        });
    }];
}
- (void)saveImageInDirectory
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[tempDistributionItem valueForKey:@"DistributionCheckLine_ID"]]];
        
        
        
        NSData *data=UIImageJPEGRepresentation(_imgView.image, 1.0);
        NSLog(@"%lu",(unsigned long)data.length);
        
        float compressionQuality=0.3;
        // Now, save the image to a file.
        if(NO == [UIImageJPEGRepresentation(_imgView.image,compressionQuality) writeToFile:savedImagePath atomically:YES]) {
            [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", savedImagePath];
        }
        else
        {
            NSLog(@"Unable to save Image");
        }

        
   
        
//        NSData *imageData = UIImageJPEGRepresentation(_imgView.image, 0.7);/*UIImagePNGRepresentation(_imgView.image);*/
//        
//        [imageData writeToFile:savedImagePath atomically:NO];
        
        
        
        
        
        
        
//        if ([tempDistributionItem valueForKey:@"Avl"] == nil) {
//            [tempDistributionItem setValue:@"Y" forKey:@"Avl"];
//        }
        [tempDistributionItem setValue:@"Y" forKey:@"Image"];
        //[self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
    else
    {
        NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[distributionItem valueForKey:@"DistributionCheckLine_ID"]]];
//        NSData *imageData = UIImageJPEGRepresentation(_imgView.image, 0.7);/*UIImagePNGRepresentation(_imgView.image);*/
//        
//        
//        
//        [imageData writeToFile:savedImagePath atomically:NO];
//        
        
        
        
        
        
        
        NSData *data=UIImageJPEGRepresentation(_imgView.image, 1.0);
        NSLog(@"%lu",(unsigned long)data.length);
        
        float compressionQuality=0.3;
        // Now, save the image to a file.
        if(NO == [UIImageJPEGRepresentation(_imgView.image,compressionQuality) writeToFile:savedImagePath atomically:YES]) {
            [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", savedImagePath];
        }
        else
        {
            NSLog(@"Unable to save Image");
        }
        
        
        if ([distributionItem valueForKey:@"Avl"] == nil) {
            [distributionItem setValue:@"Y" forKey:@"Avl"];
        }
        [distributionItem setValue:@"Y" forKey:@"Image"];
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
}
-(void)checkImage
{
    if ([distributionItem valueForKey:@"Image"] == nil)
    {
        [distributionItem setValue:@"N" forKey:@"Image"];
    }
}

#pragma mark
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
