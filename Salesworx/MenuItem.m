//
//  MenuItem.m
//  SWDashboard
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

@synthesize title;
@synthesize className;
@synthesize imageName;


+ (MenuItem *)itemWithTitle:(NSString *)t className:(NSString *)c imageName:(NSString *)i {
    MenuItem *menuItem = [[MenuItem alloc] init];
    
    [menuItem setTitle:t];
    [menuItem setClassName:c];
    [menuItem setImageName:i];
    
    
    return menuItem;
}
@end