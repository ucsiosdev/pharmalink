//
//  CurrencyViewController.h
//  Salesworx
//
//  Created by msaad on 6/10/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SWPlatform.h"

@interface CurrencyViewController : SWTableViewController<EditableCellDelegate>
{
    NSArray *types;
    // id target;
    SEL action;
    
    NSString *amountString;
    NSMutableDictionary *currencyString;
    NSIndexPath* checkedIndexPath;
}

@property (nonatomic, strong) NSArray *types;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
@end