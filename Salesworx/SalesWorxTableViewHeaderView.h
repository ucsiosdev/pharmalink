//
//  SalesWorxTableViewHeaderView.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 4/12/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
@interface SalesWorxTableViewHeaderView : UIView
{
}
@property (nonatomic,setter=setIsHeaderOnTopOfTheTable:) IBInspectable BOOL isHeaderOutSideOfTheTable;
@end
