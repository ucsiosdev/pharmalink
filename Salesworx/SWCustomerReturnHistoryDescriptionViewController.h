//
//  SWCustomerReturnHistoryDescriptionViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 11/21/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepElementTitleDescriptionLabel.h"
#import "SalesWorxTableView.h"
#import "SalesWorxNavigationHeaderLabel.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxNavigationButton.h"
#import "SWCustomerOrderHistoryDescriptionTableViewCell.h"
#import "NSString+Additions.h"
#import "SalesWorxPresentControllerBaseViewController.h"


@interface SWCustomerReturnHistoryDescriptionViewController : SalesWorxPresentControllerBaseViewController
{
    IBOutlet SalesWorxNavigationHeaderLabel *titleLabel;
    IBOutlet SalesWorxNavigationButton *closeButton;
    IBOutlet MedRepElementTitleLabel *orderNumberTitleLbl;
    IBOutlet MedRepElementDescriptionLabel *orderNumberDescriptionLbl;
    IBOutlet SalesWorxTableView *orderDescTblView;
    IBOutlet MedRepElementTitleLabel *statusTitleLbl;
    IBOutlet MedRepElementDescriptionLabel *statusDescriptionLbl;
    IBOutlet UIView *headerView;
}
- (IBAction)closeButtonTapped:(id)sender;
@property(strong,nonatomic) Customer * selectedCustomer;
@property(strong,nonatomic) NSString* orderNumber;
@property(strong,nonatomic) NSString* status;
@property(strong,nonatomic) NSMutableArray* orderLineItems;


@end
