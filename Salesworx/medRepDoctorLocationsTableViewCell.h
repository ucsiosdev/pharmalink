//
//  medRepDoctorLocationsTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/2/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface medRepDoctorLocationsTableViewCell : UITableViewCell

-(void)addDropShadow;
@property (strong, nonatomic) IBOutlet UILabel *doctorLocationTitleLbl;

@property (strong, nonatomic) IBOutlet UILabel *doctorAddressLbl;
@end
