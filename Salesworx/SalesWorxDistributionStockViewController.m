//
//  SalesWorxDistributionStockViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/18/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDistributionStockViewController.h"
#import "NSString+Additions.h"

@interface SalesWorxDistributionStockViewController ()

@end

@implementation SalesWorxDistributionStockViewController
@synthesize selectedDistributionCheckItem, isStockCheck;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appControl = [AppControl retrieveSingleton];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (isStockCheck) {
        productNameLabel.text = [NSString getValidStringValue:[selectedDistributionCheckItem.DcProduct valueForKey:@"Product_Name"]];
        productCodeLabel.text = [NSString getValidStringValue:[selectedDistributionCheckItem.DcProduct valueForKey:@"Product_Code"]];
    } else {
        productNameLabel.text = selectedDistributionCheckItem.DcProduct.Description;
        productCodeLabel.text = selectedDistributionCheckItem.DcProduct.Item_Code;
    }
}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectedDistributionCheckItem.dcItemLots.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, stockTableHeaderViewWithDistribution.frame.size.width, stockTableHeaderViewWithDistribution.frame.size.height)];
    stockTableHeaderViewWithDistribution.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:stockTableHeaderViewWithDistribution];
    return view;
    
}   // custom view for he
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"stockLotCell";
    SalesWorxDistributionStockTableViewCell *cell = (SalesWorxDistributionStockTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionStockTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    DistriButionCheckItemLot *DCItemLot =[selectedDistributionCheckItem.dcItemLots objectAtIndex:indexPath.row];
    cell.LotNumberLbl.text = [DCItemLot.LotNumber isEqualToString:@""] || DCItemLot.LotNumber == nil ? KNotApplicable:DCItemLot.LotNumber;
    cell.QuantityLbl.text = [DCItemLot.Quntity isEqualToString:@""] || DCItemLot.Quntity == nil ? KNotApplicable:DCItemLot.Quntity;
    cell.ExpiryDateLbl.text = [DCItemLot.expiryDate isEqualToString:@""] || DCItemLot.expiryDate == nil ? KNotApplicable:DCItemLot.expiryDate;
    
    
    cell.LotNumberLbl.textColor=MedRepMenuTitleFontColor;
    cell.QuantityLbl.textColor=MedRepMenuTitleFontColor;
    cell.ExpiryDateLbl.textColor=MedRepMenuTitleFontColor;
    cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
    return cell;
    
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              //insert your deleteAction here
                                              @try {
                                                  [selectedDistributionCheckItem.dcItemLots removeObjectAtIndex:indexPath.row];
                                                  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                            
                                                  if ([self.Delegate respondsToSelector:@selector(updateStockValue:)])
                                                  {
                                                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                          [self.Delegate updateStockValue:selectedDistributionCheckItem.dcItemLots];
                                                      });
                                                  }
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"%@",exception);
                                              }
                                          }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction];
}
-(void)updateStockArray
{
    NSMutableArray *tempArray = selectedDistributionCheckItem.dcItemLots;
    
    @try
    {
        selectedDistributionCheckItem.dcItemLots = [[NSMutableArray alloc]init];
        if (tempArray.count>0)
        {
            for (NSMutableDictionary * currentStockDict in tempArray)
            {
                @try {
                    Stock * productStock=[[Stock alloc]init];
                    productStock.Lot_No=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"LotNo"]];
                    productStock.Lot_Qty=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Qty"]];
                    productStock.Org_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Organization_ID"]];
                    productStock.Expiry_Date=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"ExpDate"]];
                    productStock.warehouse=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Description"]];
                    
                    [selectedDistributionCheckItem.dcItemLots addObject:productStock];
                }
                @catch (NSException *exception) {
                    [selectedDistributionCheckItem.dcItemLots addObject:currentStockDict];
                }
            }
        }
    }
    @catch (NSException *exception) {
        selectedDistributionCheckItem.dcItemLots = tempArray;
    }

    [stockTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
