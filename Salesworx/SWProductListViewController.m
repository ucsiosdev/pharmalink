//
//  SWProductListViewController.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWProductListViewController.h"
#import "SWPlatform.h"
//#import "SWDashboard/SWSplitViewController.h"

@interface SWProductListViewController ()

@end

@implementation SWProductListViewController

//- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
//    
//    if (revealController.currentFrontViewPosition !=0) {
//        [revealController revealToggle:self];
//    }
//    
////    NSDictionary *product = nil;
////    if(tv == self.searchDisplayController.searchResultsTableView){
////        
////        product=[[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
////        
////    } else
////    {
////        product=[[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
////    }
//    NSDictionary * row ;
//    if (bSearchIsOn)
//    {
//        row = [filteredCandyArray objectAtIndex:indexPath.row];
//    }
//    else
//    {
//        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
//        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
//        row= [objectsForCountry objectAtIndex:indexPath.row];
//    }
//    NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
//    NSDictionary * product =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
//    if ([product stringForKey:@"Inventory_Item_ID"])
//    {
//        NSString *itemID = [product stringForKey:@"Inventory_Item_ID"];
//        [product setValue:itemID forKey:@"ItemID"];
//    }
//  
//    if ([product stringForKey:@"Organization_ID"]) {
//        NSString *itemID = [product stringForKey:@"Organization_ID"];
//        [product setValue:itemID forKey:@"OrgID"];
//    }
//    
//     productDetailViewController = [[ProductDetailViewController alloc] initWithProduct:product] ;
//    [self.navigationController pushViewController:productDetailViewController animated:YES];
//    
//    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
//    [self.navigationController.navigationBar setTranslucent:NO];
//
//    
//}
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0) {
        [revealController revealToggle:self];
    }
    
    //    NSDictionary *product = nil;
    //    if(tv == self.searchDisplayController.searchResultsTableView){
    //
    //        product=[[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    //
    //    } else
    //    {
    //        product=[[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    //    }
    NSDictionary * row ;
    if (bSearchIsOn)
    {
        
        
        
        
        row = [searchData objectAtIndex:indexPath.row];
        
        
        
        // row = [filteredCandyArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        row= [objectsForCountry objectAtIndex:indexPath.row];
    }
    NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
    NSDictionary * product =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
    if ([product stringForKey:@"Inventory_Item_ID"])
    {
        NSString *itemID = [product stringForKey:@"Inventory_Item_ID"];
        [product setValue:itemID forKey:@"ItemID"];
    }
    
    if ([product stringForKey:@"Organization_ID"]) {
        NSString *itemID = [product stringForKey:@"Organization_ID"];
        [product setValue:itemID forKey:@"OrgID"];
    }
    
    productDetailViewController = [[ProductDetailViewController alloc] initWithProduct:product] ;
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end