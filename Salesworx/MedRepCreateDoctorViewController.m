//
//  MedRepCreateDoctorViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/13/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepCreateDoctorViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "NSString+Additions.h"


@interface MedRepCreateDoctorViewController ()

@end

@implementation MedRepCreateDoctorViewController
@synthesize doctorArray,specilizationArray,tradeChannelArray,OTCRXArray,classArray,nameTxtFld,specilizationLbl,mohIDTxtFld,telephoneTxtFld,emailTxtFld,locationLbl,locationName,classLbl,tradeChannelLbl,otcRxSegment,locationsArray,doctorDictionary,createdLocatedID,selectedLocationID,titleKey,isFromMultiCalls,visitsRequiredTextField;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    oldFrame=self.view.frame;
    
    doctorDictionary=[[NSMutableDictionary alloc]init];

    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    
    NSString * titleText;
    
    if ([NSString isEmpty:titleKey]==NO) {
        titleText = NSLocalizedString(titleKey, nil);
    }
    else
    {
        titleText = NSLocalizedString(@"Create Doctor", nil);
    }
//    createDoctorScrollView.isScrollEnabled == YES;
//    [createDoctorScrollView setContentOffset:CGPointMake(376,723) animated:YES];

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:titleText];
    // Do any additional setup after loading the view from its nib.
    
    
    if ([[[AppControl retrieveSingleton]ENABLE_VISIT_REQUIRED_PER_MONTH_FIELD] isEqualToString:KAppControlsNOCode]) {
        xConstraintTopOtcRX.constant = 8.0;
        visitsRequiredTextField.hidden = YES;
        visitRequiredLabel.hidden = YES;
    }
    
    if ([[[AppControl retrieveSingleton]MANDATORY_CITY_DOCTOR_CREATION] isEqualToString:KAppControlsYESCode]) {
        NSMutableArray *unsortedEmiratesArray = [MedRepQueries fetchCitiesData];
        
        NSSortDescriptor *valueDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES];
        emiratesArray = [[unsortedEmiratesArray sortedArrayUsingDescriptors:@[valueDescriptor]] mutableCopy];
    } else {
        xSpecializationTopConstraint.constant = 8.0;
        cityTextField.hidden = YES;
        cityTitleLbl.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)cancelButtonTapped
{
    [self.createDoctorPopover dismissPopoverAnimated:YES];
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.preferredContentSize=CGSizeMake(376, 720);
    if (self.navigationController){
        self.navigationController.preferredContentSize = self.preferredContentSize;
    }
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingCreateDoctor];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    
    
    if (isFromMultiCalls==YES) {
        
        UIBarButtonItem* cancelDoctorButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonTapped)];
        
        [cancelDoctorButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
        
        self.navigationItem.leftBarButtonItem=cancelDoctorButton;
    }
    else
    {
        
    }
    otcRxSegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    
    
    
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:MedRepTitleFont, NSFontAttributeName, nil] forState:UIControlStateNormal];

    
    
    for (UITextField* currentTxtFld in self.view.subviews) {
        
        
        if ([currentTxtFld isKindOfClass:[UITextField class]]) {
            
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, 20)];
            currentTxtFld.leftView = paddingView;
            currentTxtFld.leftViewMode = UITextFieldViewModeAlways;
        }
        
    }
    
    self.navigationController.navigationBar.topItem.title = @"";

    
    UIBarButtonItem* saveDoctorButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    
    [saveDoctorButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=saveDoctorButton;
    
    
    
    doctorArray=[[NSMutableArray alloc]init];
    specilizationArray=[[NSMutableArray alloc]init];
    tradeChannelArray=[[NSMutableArray alloc]init];
    OTCRXArray=[[NSMutableArray alloc]init];
    classArray=[[NSMutableArray alloc]init];

//new location created
    
    

    
    if (locationName.length>0) {
        
        self.locationButton.enabled=NO;
       // locationLbl.text=[NSString stringWithFormat:@" %@",locationName];
        
      //  [self.locationButton setTitle:locationName forState:UIControlStateNormal];
        locationTxtFld.text=locationName;
        [doctorDictionary setValue:selectedLocationID forKey:@"Location"];
        locationTxtFld.enabled=NO;
    }
    
    else
    {
        locationTxtFld.enabled=NO;
        self.locationButton.enabled=YES;
    }
    
    
    //fetch location from tbl
    

    locationsArray=[[NSMutableArray alloc]initWithObjects:@"Clinic",@"Hospital",@"Pharmacy", nil];

    
    locationsArray=[MedRepQueries fetchLocations];
    
    
    
    
    specilizationArray=[self fetchSpecilizations];
    tradeChannelArray=[self fetchTradeChannels];
    OTCRXArray=[self fetchOTCRX];
    classArray=[self fetchClass];
    
    if (OTCRXArray.count>0) {
        NSLog(@"otc rx array is %@", [OTCRXArray description]);
        
        [otcRxSegment removeAllSegments];
        
        [otcRxSegment insertSegmentWithTitle:[NSString stringWithFormat:@"%@", [OTCRXArray objectAtIndex:0]] atIndex:0 animated:YES];
        
         [otcRxSegment insertSegmentWithTitle:[NSString stringWithFormat:@"%@", [OTCRXArray objectAtIndex:1]] atIndex:1 animated:YES];
        
        
    }
    
    
    
            
                
    for (UIView * currentLbl in self.view.subviews) {
        
        if ([currentLbl isKindOfClass:[UILabel class]]) {
            
            
            if (currentLbl.tag==4||currentLbl.tag==5||currentLbl.tag==2||currentLbl.tag==6) {
                
                
                    CALayer *bottomBorder = [CALayer layer];
                    bottomBorder.frame = CGRectMake(0.0f, currentLbl.frame.size.height - 1, currentLbl.frame.size.width, 1.0f);
                    bottomBorder.backgroundColor = [UIColor colorWithRed:(201.0/255.0) green:(209.0/255.0) blue:(222.0/0/255.0) alpha:1].CGColor;
                    [currentLbl.layer addSublayer:bottomBorder];

//                currentLbl.layer.borderColor = [UIColor blackColor].CGColor;
//                currentLbl.layer.borderWidth = 1.0;
                
                       
            
        }
        
    }
        
        else if ([currentLbl isKindOfClass:[UITextField class]])
        {
            
            if (currentLbl.tag==100||currentLbl.tag==101||currentLbl.tag==102||currentLbl.tag==103||currentLbl.tag==104) {
                
                
//                CALayer *bottomBorder = [CALayer layer];
//                bottomBorder.frame = CGRectMake(0.0f, currentLbl.frame.size.height - 1, currentLbl.frame.size.width, 1.0f);
//                bottomBorder.backgroundColor = [UIColor colorWithRed:(201.0/255.0) green:(209.0/255.0) blue:(222.0/0/255.0) alpha:1].CGColor;
//                [currentLbl.layer addSublayer:bottomBorder];
                
                //                currentLbl.layer.borderColor = [UIColor blackColor].CGColor;
                //                currentLbl.layer.borderWidth = 1.0;
                
                
                
            }

        }
        
    
    }
    
    
//    CGSize size = CGSizeMake(500,624); // size of view in popover
//    self.preferredContentSize = size;
    
    
    
    
    /*On 29.05/2016 there was a requirement of having the classification if new doctors ans N and in the back end it will show an alert to choose the classification if creation date is more than 30 days, fo us to show drop down ir disbaled interaction we are using app control flags
    
    NEW_Doctor_Class in app control, if this flag has some value show the value as is, if it doesnt have value or it is empty then show the dropdown to pick doctor class.
     
     
     */
    
    NSString* doctorClassFlag=[[AppControl retrieveSingleton]valueForKey:@"NEW_DOCTOR_CLASS"];
    
    classButton.userInteractionEnabled=YES;
    classTxtFld.userInteractionEnabled=YES;
    
    if ([NSString isEmpty:doctorClassFlag]==YES) {
       //if empty then show drop down
    }
    else
    {
        //default to flag value disable dropdown
        
        classButton.userInteractionEnabled=NO;
        classTxtFld.userInteractionEnabled=NO;
        classTxtFld.isReadOnly=YES;
        classTxtFld.text=doctorClassFlag;
        
        [doctorDictionary setValue:doctorClassFlag forKey:@"Class"];

    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if(textField == telephoneTxtFld && newLength>ContactTelephoneTextFieldLimit)
    {
        return NO;
    }
    else if(textField == nameTxtFld && newLength>DoctorNameTextFieldLimit)
    {
        return NO;
    }
    
    else if (textField==mohIDTxtFld && newLength>MohIDTextFieldLimit)
        
    {
        return NO;
    }
    
    else if (textField==emailTxtFld && newLength>EmailTextFieldLimit)
    {
        return NO;
    }
    else if (textField== visitsRequiredTextField && newLength > ContactTelephoneTextFieldLimit)
    {
        return NO;
    }
    
    if([newString isEqualToString:@" "])
    {
        return NO;
        
    }

    
    if (textField==telephoneTxtFld ) {
       
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    
    else if(textField==emailTxtFld)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    else if (textField==visitsRequiredTextField ) {
        
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    
    else
    {
        return YES;
    }
    
   
}



#pragma mark Database methods


-(NSMutableArray*)fetchSpecilizations

{
    NSString* specilizationQry=[NSString stringWithFormat:@"select IFNULL(code_Value,'N/A') AS Specialization from TBL_App_Codes  where code_type ='SPECIALIZATION'"];
   NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
    if (contentArray.count>0) {
        
        
        NSMutableArray* tempArray=[[NSMutableArray alloc]init];
        
        for (NSInteger i=0; i<contentArray.count; i++) {
            [tempArray addObject:[[contentArray valueForKey:@"Specialization"] objectAtIndex:i] ];
            
            
        }
       // NSLog(@"temp array is %@", [tempArray description]);
        

        return tempArray;
        
    }
    else
    {
        return 0;
    }
}

-(NSMutableArray*)fetchTradeChannels
{
    NSString* specilizationQry=[NSString stringWithFormat:@"select IFNULL(code_Value,'N/A') AS Trade_Channel from TBL_App_Codes  where code_type ='TRADE_CHANNEL'"];
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
    if (contentArray.count>0) {
        NSLog(@"trade channels  are %@", [contentArray description]);
        NSMutableArray* tempArry=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<contentArray.count; i++) {
            [tempArry addObject:[[contentArray valueForKey:@"Trade_Channel"] objectAtIndex:i] ];
            
        }
       // NSLog(@"temp array is %@", [tempArry description]);
        
        
        return tempArry;
    }
    else
    {
        return 0;
    }
}

-(NSMutableArray*)fetchOTCRX
{
    NSString* specilizationQry=[NSString stringWithFormat:@"select IFNULL(code_Value,'N/A') AS OTCRX from TBL_App_Codes  where code_type ='OTC_RX'"];
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
    if (contentArray.count>0) {
        NSLog(@"OTC RX  are %@", [contentArray description]);
        NSMutableArray* tempArry=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<contentArray.count; i++) {
            [tempArry addObject:[[contentArray valueForKey:@"OTCRX"] objectAtIndex:i] ];
            
        }
       // NSLog(@"temp array is %@", [tempArry description]);
        
        
        return tempArry;

    }
    else
    {
        return 0;
    }
}

-(NSMutableArray*)fetchClass
{
    NSString* specilizationQry=[NSString stringWithFormat:@"select IFNULL(code_Value,'N/A') AS CLASS from TBL_App_Codes  where code_type ='CLASS'"];
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
    NSMutableArray* tempArry=[[NSMutableArray alloc]init];
       if (contentArray.count>0) {
        NSLog(@"class are %@", [contentArray description]);
           for (NSInteger i=0; i<contentArray.count; i++) {
               [tempArry addObject:[[contentArray valueForKey:@"CLASS"] objectAtIndex:i] ];
               
               
           }
          // NSLog(@"temp array is %@", [tempArry description]);
           
           
           return tempArry;
        
    }
    else
    {
        return 0;
    }
}

#pragma mark UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.tag==1000) {
        
        if([self.dismissDelegate respondsToSelector:@selector(addDoctorData:)])
        {
            
            //[self.dismissDelegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
            
            
            //[self.createDoctorPopover dismissPopoverAnimated:YES];
            
            
            //now add doctor location map data
            
            
            if (doctorDictionary.count>0) {
                
                
                
                NSString* locationIDforMapping=[[NSString alloc]init];
                
                if (createdLocatedID.length==0) {
                    
                    locationIDforMapping=selectedLocationID;
                }
                
                else
                {
                    locationIDforMapping=createdLocatedID;
                }
                
                BOOL status=[MedRepQueries insertDoctorLocationMapwithLocationID:locationIDforMapping andDoctorID:[doctorDictionary valueForKey:@"Doctor_ID"]];
                
                
                if (status==YES) {
                    NSLog(@" doctor location mapped successfully");

                }
                
            }
            
            [self.dismissDelegate addDoctorData:doctorDictionary];

            
            if (isFromMultiCalls==YES) {
                
                [self.createDoctorPopover dismissPopoverAnimated:YES];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];

            }
            
        }
        

    }
    
    
    
}
- (IBAction)specilizatioButtonTapped:(id)sender {
    
    
    valuePopOverController=nil;

    
    if ([nameTxtFld isFirstResponder]==YES) {
        
        if (nameTxtFld.text.length>0) {
            [doctorDictionary setValue:nameTxtFld.text forKey:@"DoctorName"];

            
        }

        [nameTxtFld resignFirstResponder];
    }
    
    popUpString=@"Specialization";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=specilizationArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    

    
    /*
    UIButton* locationBtn=(UIButton*)sender;
    popOver = nil;
    popOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    popOver.colorNames = specilizationArray;
    popOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:popOver];
        [valuePopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }*/
    
}

- (IBAction)locationButtonTapped:(id)sender {
    
    
    valuePopOverController=nil;

    
    popUpString=@"Location";
    popOver = nil;
    popOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    popOver.colorNames = [locationsArray valueForKey:@"Location_Name"];
    
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=[locationsArray valueForKey:@"Location_Name"];
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
    /*
    popOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:popOver];
        [valuePopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }*/

}

- (IBAction)clssButtonTapped:(id)sender {
    
    valuePopOverController=nil;
    
    
    popUpString=@"Class";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=classArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
    /*
    UIButton* locationBtn=(UIButton*)sender;
    popOver = nil;
    popOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    popOver.colorNames = classArray;
    popOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:popOver];
        [valuePopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }*/
}

- (IBAction)tradeChannelButtonTapped:(id)sender {
    
    valuePopOverController=nil;

    popUpString=@"TradeChannel";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Trade Channel";
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=tradeChannelArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    
    /*
    UIButton* locationBtn=(UIButton*)sender;
    popOver = nil;
    popOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    popOver.colorNames = tradeChannelArray;
    popOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:popOver];
        [valuePopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }*/
}
- (void)otxRxSegmentTapped:(UISegmentedControl *)segment
{
    
    if(segment.selectedSegmentIndex == 0 )
    {
        //OTC tapped
        [doctorDictionary setValue:[OTCRXArray objectAtIndex:segment.selectedSegmentIndex] forKey:@"OTCRX"];

    }
    
    else
    {
        //RX Tapped
        [doctorDictionary setValue:[OTCRXArray objectAtIndex:segment.selectedSegmentIndex] forKey:@"OTCRX"];

    }

}

- (IBAction)saveButtonTapped {
    
    NSLog(@"check doctor dictionary %@", [doctorDictionary description]);
    
    [self.view endEditing:YES];
    NSString* missingField=[[NSString alloc]init];
    
    if ([doctorDictionary valueForKey:@"DoctorName"] == nil ||[[doctorDictionary valueForKey:@"DoctorName"] isEqualToString:@""] ) {
        
        missingField=@"Please enter Doctor Name";
    }
    
    //moh id not mandatory as suggested by harpal 8/3/2016
    
//    else if ([doctorDictionary valueForKey:@"MOHID"]==nil ||[[doctorDictionary valueForKey:@"MOHID"] isEqualToString:@""])
//    {
//        missingField=@"MOHID";
//
//    }
    
    
    
  
    
    
    
    else if ([doctorDictionary valueForKey:@"Telephone"] == nil ||[[doctorDictionary valueForKey:@"Telephone"] isEqualToString:@""] )
    {
        missingField=@"Please enter Telephone";

    }


    
    else if ([doctorDictionary valueForKey:@"Email"] == nil ||[[doctorDictionary valueForKey:@"Email"] isEqualToString:@""]  )
    {
        missingField=@"Please enter Email Address";

    }
    
    
    else if ([doctorDictionary valueForKey:@"Location"] == nil||[[doctorDictionary valueForKey:@"Location"] isEqualToString:@""] )
    {
        
        //is location already created from create location?
        
        if (createdLocatedID.length>0) {
            [doctorDictionary setValue:createdLocatedID forKey:@"Location"];

        }
        
        else
        {
            missingField=@"Please enter Location";

        }

    }
    
    
    else if ([[[AppControl retrieveSingleton]MANDATORY_CITY_DOCTOR_CREATION] isEqualToString:KAppControlsYESCode] && cityTextField.text.length == 0)
    {
        missingField=@"Please enter City";
    }
    
   else  if ([doctorDictionary valueForKey:@"Specialization"] == nil ||[[doctorDictionary valueForKey:@"Specialization"] isEqualToString:@""] )
    {
        missingField=@"Please enter Specialization";
        
    }
    
    
    else if ([doctorDictionary valueForKey:@"Class"] == nil ||[[doctorDictionary valueForKey:@"Class"] isEqualToString:@""] )
    {
        missingField=@"Please enter Class";

    }
    
    else if ([[[AppControl retrieveSingleton]FM_DISABLE_CREATE_DR_TRADE_CHANNEL_VALIDATION] isEqualToString:KAppControlsNOCode] && ([doctorDictionary valueForKey:@"TradeChannel"] == nil || [[doctorDictionary valueForKey:@"TradeChannel"] isEqualToString:@""]) )
    {
        missingField=@"Please enter Trade Channel";
    }
    
    else if ([[[AppControl retrieveSingleton]ENABLE_VISIT_REQUIRED_PER_MONTH_FIELD] isEqualToString:KAppControlsYESCode] && ([doctorDictionary valueForKey:@"VisitRequired"] == nil ||[[doctorDictionary valueForKey:@"VisitRequired"] isEqualToString:@""]) )
    {
        missingField=@"Please enter Visits Required";
    }
    else if ([doctorDictionary valueForKey:@"OTCRX"] == nil ||[[doctorDictionary valueForKey:@"OTCRX"] isEqualToString:@""] )
    {
        missingField=@"Please enter OTCRX";
    }

    
    if (missingField.length>0) {
        
//        UIAlertView* missingAlert=[[UIAlertView alloc]initWithTitle:@"Missing Data" message:[NSString stringWithFormat:@"Please Enter %@ ",missingField] delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
//        [missingAlert show];
        
        
         [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:NSLocalizedString(missingField, nil) withController:self];
        
    }
    
    else
    {
        //all data present and valid now insert into table custom attributes
        
        if ([[[AppControl retrieveSingleton]ENABLE_VISIT_REQUIRED_PER_MONTH_FIELD] isEqualToString:KAppControlsNOCode]) {
            [doctorDictionary setValue:[NSNull null] forKey:@"VisitRequired"];
        }
        
        doctorID=  [NSString createGuid];

        [doctorDictionary setValue:doctorID forKey:@"Doctor_ID"];
        
        
        
                   
        
        BOOL success=[MedRepQueries insertDoctors:doctorDictionary];
        
        if (success==YES) {
            NSLog(@"inserted doctor data");
            
            UIAlertView* successAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"Doctor Created Successfully", nil) delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
            successAlert.tag=1000;
            [successAlert show];
            
            //save doctor details in keychain to check the 30 day period and show an alert to user to update the classification for newly created doctor
            
            NSMutableArray* keyChainDoctorsArray=[[NSMutableArray alloc]init];
            
            [keyChainDoctorsArray addObject:doctorDictionary];
            
            //[JNKeychain saveValue:keyChainDoctorsArray forKey:@"MedRepDoctors"];
            
            
    }

    
    
    }
    
    
    
}

- (IBAction)closeButtonTapped:(id)sender {
    
    [self.dismissDelegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

#pragma mark String Popover delegate



-(void)selectedValueforCreateVisit:(NSIndexPath*)indexPath
{
    
    
    if ([popUpString isEqualToString:@"Specialization"]) {
        
       // [specilizationButton setTitle:[NSString stringWithFormat:@" %@", [specilizationArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        specializationTxtFld.text=[NSString stringWithFormat:@"%@", [specilizationArray objectAtIndex:indexPath.row]];
        
//        specilizationLbl.text=[NSString stringWithFormat:@" %@", [specilizationArray objectAtIndex:indexPath.row]];
        
        NSLog(@"specialization label is %@", [NSString stringWithFormat:@"%@", [specilizationArray objectAtIndex:indexPath.row]]);
        
        
        [doctorDictionary setValue:[specilizationArray objectAtIndex:indexPath.row] forKey:@"Specialization"];
        
        NSLog(@"doctor dict after Specialization is %@", [doctorDictionary description]);
        
        
    }
    
    else if ([popUpString isEqualToString:@"Location"])
    {
        
        
//        locationLbl.text=[NSString stringWithFormat:@" %@", [[locationsArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row]];
      //  [self.locationButton setTitle:[[locationsArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        locationTxtFld.text=[[locationsArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row];
        [doctorDictionary setValue:[[locationsArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row] forKey:@"Location"];
        
        
    }
    
    
    else if ([popUpString isEqualToString:@"Class"])
    {
      //  [classButton setTitle:[NSString stringWithFormat:@" %@", [classArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        
        classTxtFld.text=[NSString stringWithFormat:@"%@", [classArray objectAtIndex:indexPath.row]] ;
       // classLbl.text=[NSString stringWithFormat:@" %@", [classArray objectAtIndex:indexPath.row]];
        
        [doctorDictionary setValue:[classArray objectAtIndex:indexPath.row] forKey:@"Class"];
        
    }
    
    
    else if ([popUpString isEqualToString:@"TradeChannel"])
    {
        
        //[tradeChannelButton setTitle:[NSString stringWithFormat:@" %@", [tradeChannelArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        tradeChannelTxtFld.text=[NSString stringWithFormat:@"%@", [tradeChannelArray objectAtIndex:indexPath.row]] ;
//        tradeChannelLbl.text=[NSString stringWithFormat:@" %@", [tradeChannelArray objectAtIndex:indexPath.row]];
        
        [doctorDictionary setValue:[tradeChannelArray objectAtIndex:indexPath.row] forKey:@"TradeChannel"];
        
        
    }
    
    else if ([popUpString isEqualToString:@"City"])
    {
        cityTextField.text=[NSString stringWithFormat:@"%@", [emiratesArray objectAtIndex:indexPath.row]];
        [doctorDictionary setValue:cityTextField.text forKey:@"City"];
    }
    else
    {
        
    }
}



-(void)selectedStringValue:(NSIndexPath *)indexPath

{
    
    if ([popUpString isEqualToString:@"Specialization"]) {
        
        specilizationLbl.text=[NSString stringWithFormat:@" %@", [specilizationArray objectAtIndex:indexPath.row]];
        
        [doctorDictionary setValue:[specilizationArray objectAtIndex:indexPath.row] forKey:@"Specialization"];
        
        
    }
    
    else if ([popUpString isEqualToString:@"Location"])
    {
    
        
        locationLbl.text=[NSString stringWithFormat:@" %@", [[locationsArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row]];
        [doctorDictionary setValue:[[locationsArray valueForKey:@"Location_Name"] objectAtIndex:indexPath.row] forKey:@"Location"];

        
    }
    
    
    else if ([popUpString isEqualToString:@"Class"])
    {
        classLbl.text=[NSString stringWithFormat:@" %@", [classArray objectAtIndex:indexPath.row]];
        
          [doctorDictionary setValue:[classArray objectAtIndex:indexPath.row] forKey:@"Class"];

    }
    
    
    else if ([popUpString isEqualToString:@"TradeChannel"])
    {
        tradeChannelLbl.text=[NSString stringWithFormat:@" %@", [tradeChannelArray objectAtIndex:indexPath.row]];
        
        [doctorDictionary setValue:[tradeChannelArray objectAtIndex:indexPath.row] forKey:@"TradeChannel"];

        
    }
    else if ([popUpString isEqualToString:@"City"])
    {
        cityTextField.text=[NSString stringWithFormat:@"%@", [emiratesArray objectAtIndex:indexPath.row]];
        [doctorDictionary setValue:cityTextField.text forKey:@"City"];
    }
    else
    {
        
    }
    
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    popOver.delegate=nil;
    popOver=nil;
}

#pragma mark UITextView Delegate


- (void)viewWillDisappear:(BOOL)animated {

}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==nameTxtFld) {
        
        [doctorDictionary setValue:nameTxtFld.text forKey:@"DoctorName"];

    }
    
    else if (textField==mohIDTxtFld)
    {
            [doctorDictionary setValue:mohIDTxtFld.text forKey:@"MOHID"];
    }
    
    else if (textField==telephoneTxtFld)
    {
        
        [doctorDictionary setValue:telephoneTxtFld.text forKey:@"Telephone"];
        
        
    }
    else if (textField ==emailTxtFld)
    {
        
        //validate email
        
        if (emailTxtFld.text.length>0) {
            
            BOOL emailValid=[self validateEmail:emailTxtFld.text];
            
            if (emailValid==YES) {
                [doctorDictionary setValue:emailTxtFld.text forKey:@"Email"];
                [emailTxtFld resignFirstResponder];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Invalid Email" andMessage:@"Please enter valid Email Address" withController:self];
            }
        }
    }
    else if (textField==visitsRequiredTextField)
    {
        
        [doctorDictionary setValue:visitsRequiredTextField.text forKey:@"VisitRequired"];
    }
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    if(textField == cityTextField)
    {
        [self cityDropDownTapped];
        return NO;
    }
    
    return YES;
}

-(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==nameTxtFld) {
        
        [doctorDictionary setValue:nameTxtFld.text forKey:@"DoctorName"];
        
        [nameTxtFld resignFirstResponder];
        [mohIDTxtFld becomeFirstResponder];

    }
    
    else if (textField==mohIDTxtFld)
    {
        [doctorDictionary setValue:mohIDTxtFld.text forKey:@"MOHID"];
        
        [mohIDTxtFld resignFirstResponder];
        [telephoneTxtFld becomeFirstResponder];
    }
    
    else if (textField==telephoneTxtFld)
    {
        
        [doctorDictionary setValue:telephoneTxtFld.text forKey:@"Telephone"];
        [telephoneTxtFld resignFirstResponder];
        [emailTxtFld becomeFirstResponder];
    }
    else if (textField ==emailTxtFld)
    {
        if ([self validateEmail:emailTxtFld.text]) {
            [doctorDictionary setValue:emailTxtFld.text forKey:@"Email"];
        }
        [emailTxtFld resignFirstResponder];
        [visitsRequiredTextField becomeFirstResponder];

    }
    else if (textField==visitsRequiredTextField)
    {
        [doctorDictionary setValue:visitsRequiredTextField.text forKey:@"VisitRequired"];
        [visitsRequiredTextField resignFirstResponder];
    }

    
    return YES;
}

  -(void)cityDropDownTapped
{
    popUpString=@"City";

    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    CGRect typeFrame=self.view.frame;
        
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=emiratesArray;
    [self.navigationController pushViewController:filterDescVC animated:YES];
}

#pragma mark Doctorr creation delegate

@end
