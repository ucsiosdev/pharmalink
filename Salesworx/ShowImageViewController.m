//
//  ShowImageViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/4/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "ShowImageViewController.h"
#import "SWDefaults.h"

@interface ShowImageViewController ()

@end

@implementation ShowImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:@"Retake" style:UIBarButtonItemStylePlain target:self action:@selector(btnRetake)];
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    saveBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=saveBtn;
    
    _imgView.image = _image;
}
-(void)viewWillAppear:(BOOL)animated
{
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)btnRetake
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.videoQuality=UIImagePickerControllerQualityTypeLow;

        imagePicker.delegate = self;
        imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}


#pragma mark image delegate
#pragma mark image delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:(NSString*)kUTTypeImage])
        {
            _imgView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *error;
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
            
            NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",_imageName]];
            
            float compressionQuality=0.3;
            // Now, save the image to a file.
            if(NO == [UIImageJPEGRepresentation(_imgView.image,compressionQuality) writeToFile:savedImagePath atomically:YES]) {
                [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", savedImagePath];
            }
            else
            {
                NSLog(@"Image saved");
            }
        });
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
