//
//  CustomerOrderHistoryCell.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@class CustomerOrderHistoryCell;

@protocol CustomerOrderHistoryCellDelegate <NSObject>
@optional
- (void)customerOrderHistoryCellDidSelectItems:(CustomerOrderHistoryCell *)cell withInvoice:(NSDictionary *)invoice;
- (void)customerOrderHistoryCellDidSelectInvoice:(CustomerOrderHistoryCell *)cell withInvoice:(NSDictionary *)invoice;

@end

@interface CustomerOrderHistoryCell : UITableViewCell {
    UILabel *refLabel;
    UILabel *dateLabel;
    UILabel *amountLabel;
    UILabel *statusLabel;
    
    UIButton *itemsButton;
    UIButton *invoiceButton;
    
    NSDictionary *invoice;
    id <CustomerOrderHistoryCellDelegate> delegate;
}
@property (unsafe_unretained) id <CustomerOrderHistoryCellDelegate> delegate;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)bindInvoice:(NSDictionary *)invoice;
//- (void)itemsAction:(UIButton *)sender;
//- (void)invoiceAction:(UIButton *)sender;
- (void)itemsAction;
- (void)invoiceAction;
@end