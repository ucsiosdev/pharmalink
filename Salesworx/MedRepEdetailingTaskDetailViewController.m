//
//  MedRepEdetailingTaskDetailViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepEdetailingTaskDetailViewController.h"
#import "MedRepEDetailTaskDetailViewTableViewCell.h"
#import "MedRepEdetailTaskStatusViewController.h"
#import "MedRepQueries.h"
#import "MedRepDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepEdetailingTaskInformationViewController.h"

@interface MedRepEdetailingTaskDetailViewController ()

@end

@implementation MedRepEdetailingTaskDetailViewController

@synthesize taskDetailTblView,savedTaskDictionary,isVisitCompleted,isUpdatingTask,isTaskClosed,dashBoardViewing,tasksArray,selectedTaskIndexPath,currentVisit,selectedTask;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentTask=[[VisitTask alloc]init];

    
    
    visitTask=[[VisitTask alloc]init];
    

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Task Details"];
    
    taskContentDictionary=[[NSMutableDictionary alloc]init];
    

    
    if (isVisitCompleted==YES) {
        taskDetailTblView.userInteractionEnabled=NO;
    }
    else
    {
        taskDetailTblView.userInteractionEnabled=YES;
        
        
        if (isUpdatingTask==YES) {
            
            self.navigationItem.rightBarButtonItem.title=NSLocalizedString(@"Update", nil);
        }
        
        else
        {
            //[taskTitleTxtFld becomeFirstResponder];
            
        }
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingEdetailingTaskDetails];
    }
    
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    
//    if ([currentVisit.Location_Type isEqualToString:kDoctorLocation]) {
//  
//    if ([selectedTask.Doctor_ID isEqualToString:currentVisit.selectedDoctor.Doctor_ID]) {
//        
//        isTaskforCurrentDoctor=YES;
//    }
//    else
//    {
//        isTaskforCurrentDoctor=NO;
//    }
//    }
//    
//    else if ([currentVisit.Location_Type isEqualToString:kPharmacyLocation])
//    {
//        if ([selectedTask.Contact_ID isEqualToString:currentVisit.selectedContact.Contact_ID]) {
//            
//            isTaskforCurrentDoctor=YES;
//        }
//        else
//        {
//            isTaskforCurrentDoctor=NO;
//        }
//    }
    
    
    UIView *paddingTxtfieldView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];// what ever you want
    taskTitleTxtFld.leftView = paddingTxtfieldView;
    taskTitleTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    if (self.isMovingToParentViewController) {
       
    if ([currentVisit.Location_Type isEqualToString:kDoctorLocation]) {
        
        doctorNameTitleLbl.placeholder=kDoctorName;
        
    }
    else if([currentVisit.Location_Type isEqualToString:kPharmacyLocation])
    {
        doctorNameTitleLbl.placeholder=kContactName;

    }
    
    }
    
    //  NSLog(@"visit details in task is %@", self.visitDetailsDict);
    
    NSLog(@"saved task dict is %@", savedTaskDictionary);
    
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    CGSize size = CGSizeMake(376,558); // size of view in popover
    self.preferredContentSize = size;
    
    
    if (tasksArray.count>0) {
        
        
        // visitTask=[tasksArray objectAtIndex:0];
        
        
        NSLog(@"task array is %@", visitTask.Start_Time);
    }
    
    NSLog(@"saved task dictionary is %@",[savedTaskDictionary description]);
    
    statusArray=[[NSMutableArray alloc]init];
    categoryArray=[[NSMutableArray alloc]init];
    priorityArray=[[NSMutableArray alloc]init];
    
    
    //fetch status category and priority
    
    
    if (isVisitCompleted==YES) {
        
        NSLog(@"Visit Completed");
        
    }
    
    else
    {
        NSLog(@"visit incomplete");
        
    }
    
    statusArray=[MedRepQueries fetchTaskAppCodes:@"Task_Status"];
    categoryArray=[MedRepQueries fetchTaskAppCodes:@"Task_Category"];
    priorityArray=[MedRepQueries fetchTaskAppCodes:@"Task_Priority"];
    
    
    NSLog(@"status array is %@", [statusArray description]);
    NSLog(@"category array is %@", [categoryArray description]);
    NSLog(@"priority array is %@", [priorityArray description]);
    
    pickerDataArray=[[NSMutableArray alloc]initWithObjects:@"Title",@"Due Date",@"Information",@" Status",@"Category", @"Priority", nil];
    
    
    //set set our selected Index to -1 to indicate no cell will be expanded
    selectedIndex = -1;
    
    
    
    UIBarButtonItem* saveBtn;
    
    
    if (self.isUpdatingTask) {
        
        saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Update", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
        
        
        [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal]
        ;
        
        
    }
    else
    {
        
        saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
        
        
        [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal]
        ;
        
    }
    
    
    
    self.navigationItem .rightBarButtonItem=saveBtn;
    
    
    
    self.navigationController.navigationBar.topItem.title=@"";
    
    
    if (tasksArray.count>0 && isUpdatingTask==YES) {
        
        
        
        
        currentTask=[tasksArray objectAtIndex:selectedTaskIndexPath.row];
        
        
        if (self.isViewing==YES && buttonTitleString==nil) {
            
            NSString* taskTitleString=currentTask.Title;
            
            NSLog(@"check start time %@", currentTask.Start_Time);
            
            NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithoutTime scrString:currentTask.Start_Time];
            
            NSLog(@"refined date str will appear %@", refinedDate);
            NSLog(@"task object time in will appear %@", currentTask.Start_Time);
            
            
            NSString* duedate=refinedDate;
            NSString* taskInformation=currentTask.Description;
            NSString* taskPriority=currentTask.Priority;
            NSString* taskCategory=currentTask.Category;
            NSString* taskStatusString=currentTask.Status;
            NSString* doctorName=currentTask.Doctor_Name;
            
            
            if ([NSString isEmpty:doctorName]==NO) {
                
                doctorTextField.text=[MedRepDefaults getDefaultStringForEmptyString:doctorName];
            }
            
            if (taskTitleString.length>0) {
                
                taskTitleTxtFld.text=taskTitleString;
                
            }
            
            if (duedate.length>0) {
                
                // [dueDateButton setTitle:duedate forState:UIControlStateNormal];
                dueDateTxtFld.text=duedate;
                
                NSLog(@"due date format %@", duedate);
                
                
                
//                NSString* refinedDueDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy" destFormat:@"yyyy-MM-dd" scrString:currentTask.Start_Time];
                
                NSLog(@"setting start time %@", currentTask.Start_Time);
                //  currentTask.Start_Time=refinedDueDate;
                
            }
            
            else
            {
                
                
            }
            
            if (taskInformation.length>0) {
                
                // [informationButton setTitle:taskInformation forState:UIControlStateNormal];
                InformationTxtFld.text=taskInformation;
            }
            
            if (taskPriority.length>0) {
                
                //[priorityButton setTitle:taskPriority forState:UIControlStateNormal];
                priorityTxtFld.text=taskPriority;
            }
            
            if (taskCategory.length>0) {
                
                //  [categoryButton setTitle:taskCategory forState:UIControlStateNormal];
                categoryTxtFld.text=taskCategory;
            }
            
            if (taskStatusString.length>0) {
                
                //[statusButton setTitle:taskStatusString forState:UIControlStateNormal];
                statusTxtFld.text=taskStatusString;
            }
            
        }
        
        statusTxtFld.isReadOnly=NO;
        
    }
    
    else
        
    {
        statusTxtFld.text=@"New";
        statusTxtFld.isReadOnly=YES;
        // [taskContentDictionary setValue:@"New" forKey:@"Status"];
        
        currentTask.Status=@"New";
        
        //adding default due date
        
        NSString* currentDate=[MedRepQueries fetchDatabaseDateFormat];
        NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:currentDate];
        
        if (refinedDate.length>0) {
            
            
            if ([currentTask.isUpdated isEqualToString:@"YES"]) {
                
            }
            else
            {
                dueDateTxtFld.text=refinedDate;
            }
            
            
            // currentTask.Start_Time=currentDate;
            
        }
        
        
    }
    if (isTaskClosed == YES || self.isInMultiCalls==YES)
    {
        self.navigationItem.rightBarButtonItem.title=@"";
        self.navigationItem.rightBarButtonItem.enabled = NO;
        
        taskTitleTxtFld.enabled=NO;
        dueDateTxtFld.enabled=NO;
        statusTxtFld.enabled=NO;
        categoryTxtFld.enabled=NO;
        priorityTxtFld.enabled=NO;
        InformationTxtFld.editable=NO;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        
        taskTitleTxtFld.enabled=YES;
        dueDateTxtFld.enabled=YES;
        
        if ([currentTask.Status isEqualToString:@"New"] && isUpdatingTask==NO) {
            statusTxtFld.enabled=NO;
        }
        else
        {
            statusTxtFld.enabled=YES;
            
        }
        
        categoryTxtFld.enabled=YES;
        priorityTxtFld.enabled=YES;
        InformationTxtFld.editable=YES;
    }
    
    if (self.isViewing==YES||self.isUpdatingTask==YES) {
        
        doctorTextField.isReadOnly=YES;
        doctorTextField.text=[MedRepDefaults getDefaultStringForEmptyString:[MedRepQueries fetchDoctorwithID:currentTask.Doctor_ID]];
        
    }
    else
    {
        doctorTextField.isReadOnly=NO;

    }
    
    if (self.isMovingToParentViewController) {
        
   
    
    if (self.isInMultiCalls==YES) {
        
        [taskTitleTxtFld becomeFirstResponder];
    }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    
    if (textField==taskTitleTxtFld) {
        
        [taskTitleTxtFld resignFirstResponder];
        [InformationTxtFld becomeFirstResponder];

        
        return NO;
    }
    
    else
    {
        return YES;

    }

   
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
    // [textField release] // uncomment if not using ARC
}


-(void)saveTapped
{

    
    //Save data to PHX_TBL_Tasks
    
    self.navigationItem.rightBarButtonItem.enabled=NO;
    
    if (isUpdatingTask==YES) {
        
        
    }
    
    else
    {
        currentTask.Task_ID=[NSString createGuid];
    }
   
    currentTask.Title=taskTitleTxtFld.text;
    currentTask.Description=InformationTxtFld.text;
    
    if ([taskTitleTxtFld isFirstResponder]) {
        
        [taskTitleTxtFld resignFirstResponder];
    }
    
    if ([InformationTxtFld isFirstResponder]) {
        
        [InformationTxtFld resignFirstResponder];
    }
    
    
    
    if (self.isViewing==YES) {
        
        
    }
    
    
    //information not mandatory
    
    
    NSLog(@"task status while saving new task %@", currentTask.Status);
    
    if ( taskTitleTxtFld.text.length>0 &&(dueDateTxtFld.text.length>0&&statusTxtFld.text.length>0&&categoryTxtFld.text.length>0&&priorityTxtFld.text.length>0)) {
        
        
        
        
        
    NSLog(@"save button tapped %@", [taskContentDictionary description]);
    
    
    if ([tasksArray count]>0) {
        
        //this may be previous task update date accordingly
        
        
        //change date to database format
        
        
        
        
      
    }
        

        NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:dueDateTxtFld.text];
        
        NSLog(@"refined date str while saving %@", refinedDate);
        
        currentTask.Start_Time=refinedDate;

        Doctors *doctor = (Doctors *)_visitDetailsDict;
        if([currentVisit.Location_Type isEqualToString:kDoctorLocation])
        {
            
            
            if(currentVisit!=nil)
                currentTask.Doctor_ID=currentVisit.selectedDoctor.Doctor_ID;
            
            if(currentTask.Doctor_ID==nil)
                @try {
                    currentTask.Doctor_ID = [_visitDetailsDict valueForKey:@"Doctor_ID"];
                }
            @catch (NSException *exception) {
                currentTask.Doctor_ID = doctor.Doctor_ID;
            }
        }
        else
        {
            if(currentVisit!=nil)
                currentTask.Contact_ID=currentVisit.selectedContact.Contact_ID;
            
            if(currentTask.Contact_ID==nil)
                @try {
                    currentTask.Contact_ID=[_visitDetailsDict valueForKey:@"Contact_ID"];
                }
            @catch (NSException *exception) {
            }
        }
        if(currentVisit!=nil)
            currentTask.Location_ID=currentVisit.Location_ID;
        if(currentTask.Location_ID==nil)
            @try {
                currentTask.Location_ID=[_visitDetailsDict valueForKey:@"Location_ID"];
            }
        @catch (NSException *exception) {
            currentTask.Location_ID = doctor.locations.Location_ID;
        }
        
        if ([currentTask.Status isEqualToString:@"Closed"]) {
            
            
            NSDateFormatter *formatterTime = [NSDateFormatter new] ;
            NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [formatterTime setLocale:usLocaleq];
            NSString *taskCloseTime =  [formatterTime stringFromDate:[NSDate date]];
            
            currentTask.End_Time=taskCloseTime;
        }
        
        
       
        NSLog(@"task content before inserting %@", taskContentDictionary);
        
        
        //with out inserting just update the array
        
        NSLog(@"***UPDATED TASK DETAILS before saving ARE %@  %@ %@ %@ %@ %@ %@ ***",currentTask.Title,currentTask.Description,currentTask.Status,currentTask.Priority,currentTask.Category,currentTask.Start_Time,currentTask.isUpdated );
        

        
        
        if ([self.updatesTasksDelegate respondsToSelector:@selector(updatedTasks:)]) {
            
            
            if (tasksArray==nil) {
                
                tasksArray=[[NSMutableArray alloc]init];
                
                NSLog(@"task being added after initializing %@", currentTask.Title);

                
                [tasksArray addObject:currentTask];
            }
            
            else if(isUpdatingTask==YES && [tasksArray objectAtIndex:selectedTaskIndexPath.row]!=nil)
            {
                
                
                [tasksArray replaceObjectAtIndex:selectedTaskIndexPath.row withObject:currentTask];
               
            }
            
            
            else
            {
                
                [tasksArray addObject:currentTask];
            }
            
            
            
            [self.updatesTasksDelegate updatedTasks:tasksArray];

        }
        
       
        
        
        NSString* statusMessage;
        
        if (self.isViewing==YES) {
            
            statusMessage=@"Task updated successfully";
        }
        
        else
        {
            statusMessage=@"Task created successfully";
        }
        
        

        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            UIAlertView* successAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Success", nil) message:[NSString stringWithFormat:NSLocalizedString(statusMessage, nil)] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
            successAlert.tag=87760;
            [successAlert show];
            
        });
        
        
        
//    BOOL status=[MedRepQueries insertTask:taskContentDictionary];
//    
//      
//    
//        if (status==YES) {
//            NSLog(@"Task Created successfully");
//            
//            
//            [self dismissKeyboard];
//
//            
//            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                
//                UIAlertView* successAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:[NSString stringWithFormat:@"Task %@ successfully", statusMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                successAlert.tag=87760;
//                [successAlert show];
//                
//            });
//            
//           
//        }
    }
    
    else
    {
        
        //content hasnt been changed dont do anything
        
        if (self.isViewing==YES && savedTaskDictionary.count>0) {
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        
        else
        {
            self.navigationItem.rightBarButtonItem.enabled=YES;
        
            UIAlertView* successAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"InComplete Data", nil) message:NSLocalizedString(@"Please enter task details", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
            successAlert.tag=87761;
            [successAlert show];
        }
        
    }
    
}

#pragma mark UITableView Methods


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //If this is the selected index we need to return the height of the cell
    //in relation to the label height otherwise we just return the minimum label height with padding
    if(selectedIndex == indexPath.row)
    {
        
        
        return 225.0f;
        
    }
    else {
        return 50.0f;
    }
    
    
    //return 50.0f;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return pickerDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    
    MedRepEDetailTaskDetailViewTableViewCell *cell = (MedRepEDetailTaskDetailViewTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepEDetailTaskDetailViewTableViewCell" owner:nil options:nil] firstObject];
        
    }
    
    UIView *paddingTxtfieldView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 42)];// what ever you want
    cell.taskDescTxtFld.leftView = paddingTxtfieldView;
    cell.taskDescTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    
    if (indexPath.row==0) {
        
        cell.taskDescTxtFld.layer.borderWidth=1.0;
        cell.taskDescTxtFld.layer.borderColor = UITextViewBorderColor.CGColor;

    }
    
   
    
    
    cell.taskDescTxtFld.tag=indexPath.row;
    cell.taskDescTxtFld.delegate=self;
    


    
    if (indexPath.row==2) {
        cell.taskDescTxtFld.userInteractionEnabled=NO;

    }
    
    if (selectedIndex>0) {
        
    }
    
    else
    {
    cell.cellDatePicker.hidden=YES;
    }
    
    
    //check for selected date
    
    if (selectedDate.length>0) {
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * previousSelection=[dateFormat dateFromString:selectedDate];
        
        [cell.cellDatePicker setDate:previousSelection];
        
        
        
    }
    
    
    else
    {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar ];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setYear:1980];
        [components setMonth:1];
        [components setDay:1];
        NSDate *defualtDate = [calendar dateFromComponents:components];
        cell.cellDatePicker.date = defualtDate;
    }
    
    
    
    
    if (indexPath.row==1) {
        
        cell.cellDatePicker.tag=indexPath.row;
        cell.cellDatePicker.minimumDate =[NSDate date];
        cell.cellDatePicker.datePickerMode = UIDatePickerModeDate;
        [cell.cellDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        cell.taskDescTxtFld.userInteractionEnabled=NO;

    }
    
    
    else if (indexPath.row==0)
    {
        cell.taskDescTxtFld.userInteractionEnabled=YES;
       
        if (taskTitle.length>0) {
            
            cell.taskDescTxtFld.text=[NSString stringWithFormat:@"%@", taskTitle];
        }
        
    }
    
    else if (indexPath.row==2)
    {
        if (taskDesc.length>0) {
            
            cell.taskDescTxtFld.text=[NSString stringWithFormat:@"%@", taskDesc];

        }
    }
    
    
    else
    {
        cell.taskDescTxtFld.userInteractionEnabled=NO;

    }
    
    cell.taskLbl.text=[NSString stringWithFormat:@"%@", [pickerDataArray objectAtIndex:indexPath.row]];
    
    
    
    if (selectedDate.length>0 && indexPath.row==selectedRow) {
        cell.taskDescTxtFld.text=selectedDate;
        
    }
    
    else
    {
    
    //cell.taskLbl.text=[NSString stringWithFormat:@"TASK %d", indexPath.row+1];
    }
    
    if (hideDatePicker==YES) {
        
        
        cell.cellDatePicker.hidden=YES;
        
        if (indexPath.row==2 && selectedIndex==indexPath.row) {
            UITextView * taskInformationTxtView=[[UITextView alloc]initWithFrame:CGRectMake(8, 54, 484,162)];
            taskInformationTxtView.backgroundColor=UITextViewBackgroundColor;
            taskInformationTxtView.tag=indexPath.row;
            taskInformationTxtView.delegate=self;
            taskInformationTxtView.font=MedRepFont;
            taskInformationTxtView.textColor=MedRepTitleDescriptionColor;
            

            
            taskInformationTxtView.layer.borderColor = UITextViewBorderColor.CGColor;
            taskInformationTxtView.layer.borderWidth = 1.0;
            taskInformationTxtView.layer.cornerRadius = 3;
            
            [cell addSubview:taskInformationTxtView];
           
            

        }
        
       /*
        else
        {
            for (UIView * currentView in cell.subviews) {
                
                
                if ([currentView isKindOfClass:[UITextView class]]) {
                    
                    [currentView removeFromSuperview];
                }
                
            }

        }*/
        
        
        
    }
    
    else
    {

        for (UIView * currentView in cell.subviews) {
            
            
            if ([currentView isKindOfClass:[UITextView class]]) {
                
                [currentView removeFromSuperview];
            }
            
        }
       
        
    }
    
    
    if (indexPath.row==0) {
        
        if (savedTaskDictionary.count>0) {
            
            cell.taskDescTxtFld.text=[savedTaskDictionary valueForKey:@"Title"];
            [taskContentDictionary setValue:cell.taskDescTxtFld.text forKey:@"Title"];
        }
        
    }
    
    
    else if (indexPath.row==1)
    {
        if (savedTaskDictionary.count>0) {
            
            
            
            NSString* dateString=[savedTaskDictionary valueForKey:@"Created_At"];
            
            
            NSString* displayDateStr=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"yyyy-MM-dd" scrString:dateString];
            
            
            NSDateFormatter *datePickerDateFormat = [NSDateFormatter new] ;
            NSLocale *usLocaleCurrent = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [datePickerDateFormat setDateFormat:@"yyyy-MM-dd"];
            [datePickerDateFormat setLocale:usLocaleCurrent];
            
            NSDate* displayDate=[datePickerDateFormat dateFromString:displayDateStr];
            
            
            
            cell.taskDescTxtFld.text=displayDateStr;
            
            
            
            NSLog(@"saved date is %@", displayDate);
            
            
            cell.cellDatePicker.minimumDate =displayDate;
            
            [taskContentDictionary setValue:displayDateStr forKey:@"Date"];


            [cell.cellDatePicker setDate:displayDate];

        }
    }
    
    else if (indexPath.row==2)
    {
        if (savedTaskDictionary.count>0) {
            cell.taskDescTxtFld.text=[savedTaskDictionary valueForKey:@"Description"];
            
            
            [taskContentDictionary setValue:cell.taskDescTxtFld.text forKey:@"Description"];
            
            
           // NSLog(@"subvies in cell are %@", [cell.subviews description]);
            //now show the same content in textview
            
            
            for (UIView* currentView in cell.subviews) {
                
                if ([currentView isKindOfClass:[UITextView class]]) {
                    
                    
                    UITextView* cellTxtView= (UITextView*)currentView;
                    cellTxtView.text=[savedTaskDictionary valueForKey:@"Description"];
                    
                    [taskContentDictionary setValue:cellTxtView.text forKey:@"Description"];
                    
                    
                }
            }
            
        }
    }
    
   else if (indexPath.row==3) {
        
        if (statusString.length>0) {
            
            cell.taskDescTxtFld.text=statusString;
            
            [taskContentDictionary setValue:statusString forKey:@"Status"];
        }
       
       else if (savedTaskDictionary.count>0)
       {
           cell.taskDescTxtFld.text=[savedTaskDictionary valueForKey:@"Status"];
           [taskContentDictionary setValue:[savedTaskDictionary valueForKey:@"Status"] forKey:@"Status"];
       }
    }
    
    
    else if (indexPath.row==4)

    {
        if (categoryString.length>0) {
            cell.taskDescTxtFld.text=categoryString;
            [taskContentDictionary setValue:categoryString forKey:@"Category"];
        }
        
        else if (savedTaskDictionary.count>0)
        {
            cell.taskDescTxtFld.text=[savedTaskDictionary valueForKey:@"Category"];
            [taskContentDictionary setValue:[savedTaskDictionary valueForKey:@"Category"] forKey:@"Category"];

        }
        
    }
    
    
    else if (indexPath.row==5)
    {
        if (priorityString.length>0) {
            cell.taskDescTxtFld.text=priorityString;
            [taskContentDictionary setValue:priorityString forKey:@"Priority"];

        }
        
        else if (savedTaskDictionary.count>0)
        {
            cell.taskDescTxtFld.text=[savedTaskDictionary valueForKey:@"Priority"];
            [taskContentDictionary setValue:[savedTaskDictionary valueForKey:@"Priority"] forKey:@"Priority"];

        }
    }
    
   
    
    return cell;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"PICKER VIEW DID SELECT CALLED");
}


-(void)dateChanged:(id)sender
{
    
    currentTask.isUpdated=@"YES";

    
    UIButton * senderButton=(UIButton*)sender;
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];

    selectedRow=indexPath.row;
    
     MedRepEDetailTaskDetailViewTableViewCell *cell = (MedRepEDetailTaskDetailViewTableViewCell*)[taskDetailTblView cellForRowAtIndexPath:indexPath];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    selectedDate = [dateFormat stringFromDate:[cell.cellDatePicker date]];
    
   
    [taskContentDictionary setValue:selectedDate forKey:@"Date"];
    
    currentTask.End_Time=selectedDate;
    

    
    NSIndexPath *currentIndexPath = [taskDetailTblView indexPathForCell: cell];
    
    
    if(selectedIndex == currentIndexPath.row)
    {
        
        
        selectedIndex = -1;
        [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        return;
    }

    
//    [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    
     [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    
    if (isVisitCompleted==YES) {
        
   
    }
    
    else
    {
    
    MedRepEDetailTaskDetailViewTableViewCell *cell = (MedRepEDetailTaskDetailViewTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    NSIndexPath *currentIndexPath = [taskDetailTblView indexPathForCell: cell];
    
    MedRepEdetailTaskStatusViewController * taskStatus=[[MedRepEdetailTaskStatusViewController alloc]init];
    taskStatus.delegate=self;
    taskStatus.selectedIndex=indexPath.row;
    
   
   
    if (indexPath.row==1 || indexPath.row==2 ) {
        
    
        if (indexPath.row==2) {
            
            cell.taskDescTxtFld.userInteractionEnabled=NO;
        }
       

    
    if (indexPath.row==1) {
        
        hideDatePicker=NO;
    }
    
    else
    {
        cell.cellDatePicker.hidden=YES;
        hideDatePicker=YES;
    }
    //The user is selecting the cell which is currently expanded
    //we want to minimize it back
    if(selectedIndex == currentIndexPath.row)
    {
        
        
        selectedIndex = -1;
        [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        return;
    }
    
    //First we check if a cell is already expanded.
    //If it is we want to minimize make sure it is reloaded to minimize it back
    if(selectedIndex >= 0)
    {
        NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        selectedIndex = currentIndexPath.row;
        [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    //Finally set the selected index to the new selection and reload it to expand
    selectedIndex = currentIndexPath.row;
    [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    
   
        
       

        
    }
    
    else if (indexPath.row==3)
    {
        
        
        if (savedTaskDictionary.count>0) {
            
            taskStatus.savedTaskString=[savedTaskDictionary valueForKey:@"Status"];
            
        }
            taskStatus.contentArray=statusArray;
        
        NSLog(@"task status is %@",[savedTaskDictionary valueForKey:@"Status"]);
        
        
            [self.navigationController pushViewController:taskStatus animated:YES];
        
        
    }
    
    else if (indexPath.row==4)
    {
        
        if (savedTaskDictionary.count>0) {
            
            taskStatus.savedTaskString=[savedTaskDictionary valueForKey:@"Category"];
            
        }

        
                taskStatus.contentArray=categoryArray;
        [self.navigationController pushViewController:taskStatus animated:YES];
    }
    else if (indexPath.row==5)
    {
        
        if (savedTaskDictionary.count>0) {
            
            taskStatus.savedTaskString=[savedTaskDictionary valueForKey:@"Priority"];
            
        }

                taskStatus.contentArray=priorityArray;
        [self.navigationController pushViewController:taskStatus animated:YES];
    }
    
    }
    
}



#pragma mark Task Status Delegate

-(void)selectedStatus:(NSMutableDictionary*)statusDict
{
    NSLog(@"selected status and index %@ %@", [statusDict valueForKey:@"Selected_Value"],[statusDict valueForKey:@"Selected_Index"] );
    
    selectedDelegateIndex=[[statusDict valueForKey:@"Selected_Index"] integerValue];
    
    selectedStatus=[statusDict valueForKey:@"Selected_Value"];
    
    
    if (selectedDelegateIndex==3) {
        statusString=selectedStatus;
    }
    else if (selectedDelegateIndex==4)
    {
        categoryString=selectedStatus;
    }
    
    else if (selectedDelegateIndex==5)
    {
        priorityString=selectedStatus;
    }
    
    
    
    
    
}


#pragma mark UITextFieldDelegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField==taskTitleTxtFld) {
        
        taskTitle=textField.text;
        
        [taskContentDictionary setValue:taskTitle forKey:@"Title"];
        
        currentTask.Title=taskTitle;
        
        currentTask.isUpdated=@"YES";
        
        visitTask.Title=taskTitle;
        
        
        
    }
    
    
    else
    {
        
    }
    /*
    if (textField.tag==0) {
        //title
        
        taskTitle=textField.text;
        
        [taskContentDictionary setValue:taskTitle forKey:@"Title"];
        
        
    }
    */
    
    }


- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    taskDesc=textView.text;
    
    [taskContentDictionary setValue:taskDesc forKey:@"Description"];
    [savedTaskDictionary setValue:taskDesc forKey:@"Description"];
    
    currentTask.Description=taskDesc;
    
    currentTask.isUpdated=@"YES";

    
    visitTask.Description=taskDesc;
    
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[textView tag] inSection:0];
//    
//    [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
//    
//    
//    MedRepEDetailTaskDetailViewTableViewCell *cell = (MedRepEDetailTaskDetailViewTableViewCell*)[taskDetailTblView cellForRowAtIndexPath:indexPath];
//    
//    NSIndexPath *currentIndexPath = [taskDetailTblView indexPathForCell: cell];
//    
//    
//    if(selectedIndex == currentIndexPath.row)
//    {
//        
//        
//        selectedIndex = -1;
//        [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//        
//        return;
//    }
//    
//    //First we check if a cell is already expanded.
//    //If it is we want to minimize make sure it is reloaded to minimize it back
//    if(selectedIndex >= 0)
//    {
//        NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
//        selectedIndex = currentIndexPath.row;
//        [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
//    }
//    
//    //Finally set the selected index to the new selection and reload it to expand
//    selectedIndex = currentIndexPath.row;
//    [taskDetailTblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//    

    
   // [taskDetailTblView reloadData];
    
}


#pragma mark UIAlertViewDelegate

-(void)popViewPopTimer

{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==87760) {
        

        
        [self performSelector:@selector(popViewPopTimer) withObject:self afterDelay:1];

        
       
        
    }
}

#pragma mark Button Action Methods


//- (IBAction)dueDateTapped:(id)sender {
  -(void)dueDateDropDownTapped
{
    buttonTitleString=@"Date";
    MedRepVisitsDateFilterViewController * visitDateFilterVC=[[MedRepVisitsDateFilterViewController alloc]init];
    visitDateFilterVC.titleString=@"Task Date";
    visitDateFilterVC.visitDateDelegate=self;
    
   
    
    if (dueDateTxtFld.text.length>0) {
         NSString* refinedStr=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy" destFormat:@"yyyy-MM-dd" scrString:dueDateTxtFld.text];
        visitDateFilterVC.previouslySelectedDate=refinedStr;

    }
 
    [self.navigationController pushViewController:visitDateFilterVC animated:YES];

}

//- (IBAction)priorityTapped:(id)sender {
  -(void)priorityDropDownTapped
{
    
    buttonTitleString=@"Priority";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=buttonTitleString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=priorityArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    

}

//- (IBAction)informationTapped:(id)sender {
  -(void)informationdropdownTapped
{
  //  [informationButton setTitle:@"" forState:UIControlStateNormal];
    InformationTxtFld.text=@"";
    
    MedRepEdetailingTaskInformationViewController * infoVC=[[MedRepEdetailingTaskInformationViewController alloc]init];
    infoVC.taskInformationDelegate=self;
    infoVC.isVisitCompleted=isVisitCompleted;
    [self.navigationController pushViewController:infoVC animated:YES];
    
    
}

-(void)taskDetailsInformation:(NSString*)taskInfo
{
    NSLog(@"task info is %@", taskInfo);
    
    [savedTaskDictionary setValue:taskInfo forKey:@"Description"];
    
    [taskContentDictionary setValue:taskInfo forKey:@"Description"];
    
    currentTask.Description=taskInfo;
    
    
    NSLog(@"task info dict %@", [taskContentDictionary description]);
    

   // [informationButton setTitle:[NSString stringWithFormat:@"%@",[taskContentDictionary valueForKey:@"Description"]] forState:UIControlStateNormal];
    InformationTxtFld.text=[taskContentDictionary valueForKey:@"Description"];

}


//- (IBAction)statusTapped:(id)sender {
    
  -(void)statusDropDownTapped
{
    buttonTitleString=@"Status";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=buttonTitleString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=statusArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    

}

//- (IBAction)categoryTapped:(id)sender {
  -(void)categoryDropDownTapped
{
    buttonTitleString=@"Category";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=buttonTitleString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=categoryArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];

}

#pragma Description Delegate Methods

-(void)selectedValueforCreateVisit:(NSIndexPath*)indexPath
{
    
    currentTask.isUpdated=@"YES";
    
    
    if ([buttonTitleString isEqualToString:@"Priority"]) {
        
       // [priorityButton setTitle:[priorityArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        priorityTxtFld.text=[priorityArray objectAtIndex:indexPath.row];
        [taskContentDictionary setValue:[priorityArray objectAtIndex:indexPath.row] forKey:@"Priority"];
        currentTask.Priority=[priorityArray objectAtIndex:indexPath.row];
        
    }
    
    
    
    else if ([buttonTitleString isEqualToString:@"Category"]) {
        
        //[categoryButton setTitle:[categoryArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        categoryTxtFld.text=[categoryArray objectAtIndex:indexPath.row];
        [taskContentDictionary setValue:[categoryArray objectAtIndex:indexPath.row] forKey:@"Category"];
        currentTask.Category=[categoryArray objectAtIndex:indexPath.row];
        
        
    }
    
    
    else  if ([buttonTitleString isEqualToString:@"Status"]) {
        
       // [statusButton setTitle:[statusArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        statusTxtFld.text=[statusArray objectAtIndex:indexPath.row];
        [taskContentDictionary setValue:[statusArray objectAtIndex:indexPath.row] forKey:@"Status"];
        currentTask.Status=[statusArray objectAtIndex:indexPath.row];
        
        
    }
    else if ([buttonTitleString isEqualToString:kDoctorName])
    {
        
        NSString* selectedDoctorName=[MedRepDefaults getDefaultStringForEmptyString:[[currentVisit.doctorsforLocationArray objectAtIndex:indexPath.row]valueForKey:@"Doctor_Name"] ];
        NSString* selectedDoctorID=[MedRepDefaults getDefaultStringForEmptyString:[[currentVisit.doctorsforLocationArray objectAtIndex:indexPath.row]valueForKey:@"Doctor_ID"] ];
        
        currentTask.Doctor_ID=selectedDoctorID;
        currentTask.Doctor_Name=selectedDoctorName;
        doctorTextField.text=selectedDoctorName;
        [taskContentDictionary setValue:selectedDoctorName forKey:@"Doctor_Name"];
        [taskContentDictionary setValue:selectedDoctorID forKey:@"Doctor_ID"];
        

        
    }
    
    else if ([buttonTitleString isEqualToString:kContactName])
    {
        
        NSString* selectedContactName=[MedRepDefaults getDefaultStringForEmptyString:[[currentVisit.locationContactsArray objectAtIndex:indexPath.row]valueForKey:@"Contact_Name"] ];
        NSString* selectedContactID=[MedRepDefaults getDefaultStringForEmptyString:[[currentVisit.locationContactsArray objectAtIndex:indexPath.row]valueForKey:@"Contact_ID"] ];
        
        currentTask.Contact_ID=selectedContactID;
        currentTask.Contact_Name=selectedContactName;
        doctorTextField.text=selectedContactName;
        [taskContentDictionary setValue:selectedContactName forKey:@"Contact_Name"];
        [taskContentDictionary setValue:selectedContactID forKey:@"Contact_ID"];
        

        
        
    }
    
}
-(void)selectedVisitDateDelegate:(NSString*)selectedDate
{
    
    NSLog(@"selected date in date filter %@", selectedDate);
    
    NSString* selectedDateValue=selectedDate;
    
    currentTask.isUpdated=@"YES";

    NSString* refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:mm:ss" destFormat:kDateFormatWithoutTime scrString:selectedDateValue];
    
    NSLog(@"refined date in date picker %@", refinedDate);
    
    
    if ([buttonTitleString isEqualToString:@"Date"]) {
        
        NSLog(@"refined date is %@", refinedDate);

        
       // [dueDateButton setTitle:refinedDate forState:UIControlStateNormal];
        
        dueDateTxtFld.text=refinedDate;
        
        NSString* refinedDate=[MedRepDefaults refineDateFormat:@"dd/MM/yyyy" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:dueDateTxtFld.text];
        
        NSLog(@"refined date str while saving %@", refinedDate);
        
        currentTask.Start_Time=refinedDate;
        
        [taskContentDictionary setValue:refinedDate forKey:@"Date"];
        
    }
}

-(void)showdoctorList
{
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=buttonTitleString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    
    if ([currentVisit.Location_Type isEqualToString:kDoctorLocation]) {
        filterDescVC.filterDescArray=[currentVisit.doctorsforLocationArray valueForKey:@"Doctor_Name"];

    }
    else if ([currentVisit.Location_Type isEqualToString:kPharmacyLocation])
    {
        filterDescVC.filterDescArray=currentVisit.locationContactsArray ;

    }
    
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    

}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [self.view endEditing:YES];

    if(textField==dueDateTxtFld)
    {
        [self dueDateDropDownTapped];
        return NO;
    }
//    else if(textField==InformationTxtFld)
//    {
//        [self informationdropdownTapped];
//        return NO;
//    }
    else if(textField==statusTxtFld)
    {
        [self statusDropDownTapped];
        return NO;

    }
    else if(textField==categoryTxtFld)
    {
        [self categoryDropDownTapped];
        return NO;
        
    }
    else if(textField==priorityTxtFld)
    {
        [self priorityDropDownTapped];
        return NO;
        
    }
    else if (textField==doctorTextField)
    {
        NSLog(@"doctors array is %@", currentVisit.doctorsforLocationArray);
        if ([currentVisit.Location_Type isEqualToString:kDoctorLocation]) {
            buttonTitleString=kDoctorName;

        }
        else if ([currentVisit.Location_Type isEqualToString:kPharmacyLocation])
        {
            buttonTitleString=kContactName;

        }
        [self showdoctorList];
        return NO;
    }

    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if([newString length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    if([newString isEqualToString:@" "])
    {
        return NO;

    }
    
   if(textField==taskTitleTxtFld)
    {
//        NSString *expression = @"^[a-zA-Z ]+$";
//        NSError *error = nil;
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
//        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return ( newString.length<TaskTitleTextFieldLimit);
    }
    
    
    
    return YES;
    
    
    
    
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= TaskInformationTextFieldLimit;
}


@end
