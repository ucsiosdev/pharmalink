//
//  SamplesTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 10/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SamplesTableViewCell.h"
#import "MedRepDefaults.h"

@implementation SamplesTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.titleLbl.font=kSWX_FONT_SEMI_BOLD(14);
    self.titleLbl.textColor=MedRepElementDescriptionLabelColor;

    self.lotNumberLbl.font=kSWX_FONT_SEMI_BOLD(14);
    self.lotNumberLbl.textColor=MedRepElementDescriptionLabelColor;

    self.expiryLbl.font=kSWX_FONT_SEMI_BOLD(14);
    self.expiryLbl.textColor=MedRepElementDescriptionLabelColor;

    self.qtyLbl.font=kSWX_FONT_SEMI_BOLD(14);
    self.qtyLbl.textColor=MedRepElementDescriptionLabelColor;

    self.itemTypeLbl.font=kSWX_FONT_SEMI_BOLD(14);
    self.itemTypeLbl.textColor=MedRepElementDescriptionLabelColor;
    
    
    

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
