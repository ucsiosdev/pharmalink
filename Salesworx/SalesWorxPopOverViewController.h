//
//  SalesWorxPopOverViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepCustomClass.h"


@protocol SalesWorxPopoverControllerDelegate <NSObject>


-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath;
-(void)didDeleteItematIndex:(NSIndexPath*)deletedIndexPath;
-(void)didDismissPopOverController;

@optional
-(void)didSelectPopOverControllerForEdetailingAccompaniedByMultiSelection:(NSMutableArray *)selectedIndexPathArray;

@end

@interface SalesWorxPopOverViewController : UIViewController<UISearchControllerDelegate,UISearchResultsUpdating>

{
    NSMutableArray* filteredPopOverContentArray;
    
    id delegate;
}

@property(nonatomic) id salesWorxPopOverControllerDelegate;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *popoverTableViewTopConstraint;
@property(strong,nonatomic) NSMutableArray* popOverContentArray;
@property(nonatomic) BOOL isDoctorFilter;
@property(strong,nonatomic) NSString* titleKey;

@property(strong,nonatomic) UIPopoverController * popOverController;
@property (strong, nonatomic) IBOutlet UITableView *popOverTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *popOverSearchBar;
@property(nonatomic) BOOL enableSwipeToDelete;
@property(strong,nonatomic) NSString* popoverType;
@property(strong,nonatomic) NSString*headerTitle;
@property (strong, nonatomic) UISearchController *searchController;

@property(nonatomic) BOOL disableSearch;
@property(nonatomic) BOOL enableArabicTranslation;
@property(nonatomic) BOOL isEdetailingAccompaniedBy;
@property(nonatomic) CGSize popOverSize;

@end
