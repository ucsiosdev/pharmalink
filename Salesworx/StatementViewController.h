////
////  StatementViewController.h
////  SWPlatform
////
////  Created by Irfan Bashir on 5/21/12.
////  Copyright (c) 2012 UCS Solutions. All rights reserved.
////
//
//#import "SWPlatform.h"
//
//@interface StatementViewController : SWGridViewController < UIAlertViewDelegate> {
//    NSDictionary *customer;
//    NSMutableArray *invoices;
//
//    NSDictionary *collectionInfo;
//    UILabel *balanceLabel;
//    NSMutableArray *paidInvoices;
//    
//    NSString *paidValue;
//    NSString *paidKey;
//
//    double _total;
//    double _balance;
//    double _payAmount;
//    BOOL isStatment ;
//    SEL action;
//    //       id target;
//
//
//    
//    
//    
//}
//
//
//
//@property (nonatomic, unsafe_unretained) id target;
//@property (nonatomic, assign) SEL action;
//
//- (id)initWithCustomer:(NSDictionary *)customer;
//- (id)initWithCustomer:(NSDictionary *)customer andCollectionInfo:(NSDictionary *)collectionInfo;
//@end




//
//  StatementViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@interface StatementViewController : SWGridViewController < UIAlertViewDelegate> {
    NSDictionary *customer;
    NSMutableArray *invoices;
    
    NSDictionary *collectionInfo;
    UILabel *balanceLabel;
    NSMutableArray *paidInvoices;
    
    NSString *paidValue;
    NSString *paidKey;
    
    double _total;
    double _balance;
    double _payAmount;
    BOOL isStatment ;
    SEL action;
    
    double remainingBalance;
    UIBarButtonItem *balanceButton;
    
    NSMutableArray * selectedIndexArray;
    //       id target;
    
    BOOL saveButton;
    UIBarButtonItem *lastSyncButton;
    
    
    
}


@property(strong,nonatomic) UIPopoverController * popOverController;

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@property(assign,nonatomic) double totalAmtPaid;


- (id)initWithCustomer:(NSDictionary *)customer;
- (id)initWithCustomer:(NSDictionary *)customer andCollectionInfo:(NSDictionary *)collectionInfo;
@end