//
//  SalesWorxToDoPopUpViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 10/16/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxVisitOptionsToDoFilterViewController.h"

@interface SalesWorxToDoPopUpViewController : UIViewController<FilteredToDoDelegate>
{
    IBOutlet  UITableView *toDoListTableView;
    NSMutableArray *ToDoArray;
    NSMutableArray * filteredToDoArray;
    IBOutlet UIView *TopContentView;
    UIImageView * blurredBgImage;
    NSMutableDictionary *previousFilterParameters;
    IBOutlet UIButton *filterButton;
    IBOutlet UISearchBar *toDoSearchBar;
    BOOL isSearching;
}
@end
