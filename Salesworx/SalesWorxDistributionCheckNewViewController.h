//
//  SalesWorxDistributionCheckNewViewController.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 12/22/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepHeader.h"
#import "MedRepProductCodeLabel.h"
#import "AppControl.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesWorxTableView.h"
#import "MedRepView.h"
#import "MedRepTextField.h"
#import "MedRepButton.h"
#import "PNCircleChart.h"
#import "SalesWorxDescriptionLabel1.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxDCFilterViewController.h"
#import "SalesWorxDescriptionLabel4_SemiBold.h"


@interface SalesWorxDistributionCheckNewViewController : UIViewController<UIPopoverControllerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    IBOutlet MedRepView *parentViewOfCustomerDetail;
    IBOutlet MedRepHeader *customerNameLabel;
    IBOutlet MedRepProductCodeLabel *customerCodeLabel;
    IBOutlet UILabel *customerAvailableBalanceLabel;
    NSMutableArray *fetchDistriButionCheckLocations;
    NSMutableArray *MSLItemsArray;
    NSMutableArray *MSLMinStockOfItemsArray;
    
    DistriButionCheckLocation *selectedDistributionCheckLocation;
    DistriButionCheckItem *selectedDistributionCheckItem;
    DistriButionCheckItemLot *selectedDistributionCheckItemLot;
    
    AppControl *appControl;
    IBOutlet SalesWorxTableViewHeaderView *headerView;
    
    NSMutableArray *indexPathArray;
    NSIndexPath *editIndexPath;
    
    BOOL isAddButtonPressed;
    
    IBOutlet SalesWorxTableView *DcTableView;
    IBOutlet UISegmentedControl *segAvailable;
    
    IBOutlet MedRepTextField *txtLocation;

    IBOutlet UILabel *lblLastVisitStock;
    IBOutlet UILabel *lblLastVisitOn;
    
    IBOutlet MedRepTextField *txtQuantity;
    IBOutlet MedRepTextField *txtLotNo;
    IBOutlet MedRepTextField *txtExpiryDate;
    IBOutlet MedRepButton *btnAdd;
    IBOutlet UIView *availDetailView;
    
    IBOutlet UIImageView *imgView;
    UITapGestureRecognizer *tap;
    BOOL flagRetake;
    
    IBOutlet PNCircleChart *pieChart;
    BOOL anyUpdated;
    
    IBOutlet SalesWorxDescriptionLabel1 *lblVisitType;
    
    IBOutlet NSLayoutConstraint *xHeightConstraintOfLastVisitON;
    IBOutlet NSLayoutConstraint *xHeightConstraintOfLastVisitStock;
    IBOutlet UISegmentedControl *segAvailabelWithThreeOption;
    IBOutlet UISegmentedControl *segReorder;
    IBOutlet MedRepElementTitleLabel *lblReorder;
    IBOutlet NSLayoutConstraint *xTopConstraintOfQunatityField;
    
    NSMutableArray*productsArray;
    IBOutlet UIButton *filterButton;
    UIImageView *blurredBgImage;
    NSMutableDictionary *previousFilteredParameters;
    
    IBOutlet MedRepElementTitleLabel *minReqStockTitleLabel;
    IBOutlet SalesWorxDescriptionLabel4_SemiBold *minReqStockLabel;
}
@property(strong,nonatomic) SalesWorxVisit * currentVisit;

@end
