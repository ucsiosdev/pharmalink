//
//  ProductStockView.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "ProductStockView.h"
#import "ProductHeaderView.h"
#import "GroupSectionHeaderView.h"
#import "SWPlatform.h"

@implementation ProductStockView


@synthesize delegate = _delegate,items,gridView,editMode;
//@synthesize serProduct;

- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    
    if (self) {
        
       product=[NSDictionary dictionaryWithDictionary:p];

        self.backgroundColor = [UIColor whiteColor];

        items=[[NSArray array] mutableCopy];
        
        
        gridView=[[GridView alloc] initWithFrame:CGRectZero];
        [gridView setFrame:CGRectMake(0, 60, self.bounds.size.width, self.bounds.size.height)];
        [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        [gridView setDataSource:self];
        [gridView setDelegate:self];
        [self addSubview:gridView];
        
        ProductHeaderView *productHeaderView = [[ProductHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60)] ;
        [productHeaderView.headingLabel setText:[product stringForKey:@"Description"]];
        [productHeaderView.detailLabel setText:[product stringForKey:@"Item_Code"]];
        
      //  [gridView.tableView setTableHeaderView:productHeaderView];
        [self addSubview:productHeaderView];
        
        discountLable = [[UILabel alloc] initWithFrame:CGRectMake(800,12, 200, 32)] ;
        [discountLable setTextAlignment:NSTextAlignmentCenter];
        [discountLable setBackgroundColor:[UIColor clearColor]];
        [discountLable setTextColor:[UIColor darkGrayColor]];
        [discountLable setFont:RegularFontOfSize(18.0)];
        
        AppControl *appControl = [AppControl retrieveSingleton];

        isOnOrder = appControl.SHOW_ON_ORDER;
        NSString *onOrderQty = [[SWDatabaseManager retrieveManager] dbGetOnOrderQuantityOfProduct:[product stringForKey:@"ItemID"]];

        if([isOnOrder isEqualToString:@"Y"])
        {
            if(onOrderQty.length !=0)
            {
                discountLable.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"On Order Quantity", nil),onOrderQty ];
            }
            else
            {
                discountLable.text = [NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"On Order Quantity", nil)];
            }
            discountLable.hidden = NO;
        }
        else
        {
            discountLable.hidden = YES;
            discountLable.text = [NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"On Order Quantity", nil)];
        }
        [productHeaderView addSubview:discountLable];
      

    }
    
    return self;
}

-(void)loadStock
{
    //serProduct.delegate=self;
    //[[SWDatabaseManager retrieveManager] dbGetStockInfo:[product stringForKey:@"ItemID"]];
    NSLog(@"product dic description %@", [product description]);
    
    [self getProductServiceDiddbGetStockInfo:[[SWDatabaseManager retrieveManager] dbGetStockInfo:[product stringForKey:@"ItemID"]]];
    
    NSLog(@"item id is %@", [product description]);
    
}



#pragma mark Product Service Delegate
- (void)getProductServiceDiddbGetStockInfo:(NSArray *)info {
    items=[NSMutableArray arrayWithArray:info];
    Singleton *single = [Singleton retrieveSingleton];

    if(![single.manageOrderRefNumber isEqualToString:@"SO"])
    {
        //Saad Lots
        [self getProductServiceDidGetStockManage:[[SWDatabaseManager retrieveManager] dbGetStockInfoForManage:single.manageOrderRefNumber]];
    }
    else
    {
        [gridView reloadData];
        if ([self.delegate respondsToSelector:@selector(productStockViewLoaded)])
        {
            [self.delegate productStockViewLoaded];
        }
    }
    info=nil;
}

-(void)getProductServiceDidGetStockManage:(NSArray *)info
{
    
    for (int i=0; i < [info count]; i++)
    {
        @autoreleasepool{
        NSMutableDictionary *a = [NSMutableDictionary dictionaryWithDictionary:[info objectAtIndex:i]] ;
        for (int j=0; j < [items count]; j++)
        {
            @autoreleasepool{
            NSMutableDictionary *b = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:j]] ;
            if([[a stringForKey:@"Lot_Number"] isEqualToString:[b stringForKey:@"Lot_No"]])
            {
                [b setValue:[a objectForKey:@"Ordered_Quantity"] forKey:@"Allocated"];
                [items removeObjectAtIndex:j];
                [items addObject:b];
            }
            }
        }
        }
    }

    [gridView reloadData];
    info=nil;
    if ([self.delegate respondsToSelector:@selector(productStockViewLoaded)]) {
        [self.delegate productStockViewLoaded];
    }
}
#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return items.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    if (editMode) {
        return 5;
    }
    
    return 4;
}

- (int)indexOfColumnForInput:(GridView *)gridView {
    return 4;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    if (column == 0) {
        title = NSLocalizedString(@"  Lot No", nil);
    } else if (column == 1) {
        title = NSLocalizedString(@"Quantity", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Expiry", nil);
    } else if (column == 3) {
        title = NSLocalizedString(@"Warehouse", nil);
    } else if (column == 4) {
        title = NSLocalizedString(@"Allocated", nil);
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [items objectAtIndex:row];
    if (column == 0) {
        text =[NSString stringWithFormat:@"     %@", [data objectForKey:@"Lot_No"]];
    } else if (column == 2) {
        text = [data dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle];
    } else if (column == 1) {
        text = [data stringForKey:@"Lot_Qty"];
    } else if (column == 3) {
        text = [data stringForKey:@"Org_ID"];
    } else if (column == 4) {
        text = [data stringForKey:@"Allocated"];
    }
    return text;
}

- (NSString *)gridView:(GridView *)gridView keyForRow:(int)rowIndex {
    return [NSString stringWithFormat:@"%d", rowIndex];
}

- (void)gridView:(GridView *)gridViews didFinishInputForKey:(NSString *)key andValue:(id)value {
    
    NSScanner *scanner = [NSScanner scannerWithString:value];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    
    if (isNumeric)
    {
        if ([value integerValue] > 0)
        {
            if ([self.delegate respondsToSelector:@selector(productStockViewDidFinishedEditingForRowIndex:)]) {
                int rowIndex = [key intValue];
                NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:rowIndex]];
                int availableStock = [[data objectForKey:@"Lot_Qty"] intValue];
                if (availableStock >= [value intValue]) {
                    [data setValue:value forKey:@"Allocated"];
                } else {
                    [data setValue:[data objectForKey:@"Lot_Qty"] forKey:@"Allocated"];
                    [gridView reloadRow:rowIndex];
                }
                [items replaceObjectAtIndex:rowIndex withObject:data];
                [self.delegate productStockViewDidFinishedEditingForRowIndex:rowIndex];
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please provide positive value only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]   show];

//            [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                               message:@"Please provide positive value only."
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:nil];
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please provide positive value only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]   show];

//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:@"Please provide 'Numeric' value only."
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
    }
}

@end