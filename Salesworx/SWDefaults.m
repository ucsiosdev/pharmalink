//
//  SWDefaults.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDefaults.h"
#import "SWPlatform.h"
#import "SSKeychain/SSKeychain.h"
#import "MedRepQueries.h"
#import "SalesWorxVisitOptionsViewController.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"

@implementation SWDefaults

+ (void)initializeDefaults {
    NSString *lastCollectionReference = [SWDefaults lastCollectionReference];
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    NSString *lastPerformaOrderReference = [SWDefaults lastPerformaOrderReference];
    NSString *lastReturnOrderReference = [SWDefaults lastReturnOrderReference];
    NSString *lastSyncDateString = [SWDefaults lastSyncDate];
    NSString *lastSyncStatusString = [SWDefaults lastSyncStatus];
    NSString *userName = [SWDefaults username];
    NSString *userNameForAct = [SWDefaults usernameForActivate];
    NSString *userPassword = [SWDefaults password];
    NSString *userPasswordForAct = [SWDefaults passwordForActivate];

    NSString *isActivationDone = [SWDefaults isActivationDone];

        if (!lastCollectionReference || lastCollectionReference.length == 0) {
        [SWDefaults setLastCollectionReference:@"M000C0000000000"];
    }   if (!lastOrderReference || lastOrderReference.length == 0) {
        [SWDefaults setLastOrderReference:@"M000S0000000000"];
    }   if (!lastPerformaOrderReference || lastPerformaOrderReference.length == 0) {
        [SWDefaults setLastPerformaOrderReference:@"M000D0000000000"];
    }   if (!lastReturnOrderReference || lastReturnOrderReference.length == 0) {
        [SWDefaults setLastReturnOrderReference:@"M000R0000000000"];
    }   if (!lastSyncDateString || lastSyncDateString.length == 0) {
        [SWDefaults setLastSyncDate:@"Never"];
    }   if (!lastSyncStatusString || lastSyncStatusString.length == 0) {
        [SWDefaults setLastSyncStatus:@"N/A"];
        [SWDefaults setLastSyncOrderSent:@"N/A"];
        [SWDefaults setLastSyncType:@"N/A"];
    }   if (!isActivationDone || isActivationDone.length == 0) {
        [SWDefaults setIsActivationDone:@"NO"];
    }   if (!userName || userName.length == 0) {
        [SWDefaults setUsername:@"demo"];//actual content
    }   if (!userNameForAct || userNameForAct.length == 0) {
        [SWDefaults setUsernameForActivate:@"demo"];
    }   if (!userPassword || userPassword.length == 0) {
        [SWDefaults setPassword:@"demo123"];
    }   if (!userPasswordForAct || userPasswordForAct.length == 0) {
        [SWDefaults setPasswordForActivate:@"demo123"];
    }
}

+ (NSString *)username {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kUsernameKey];
}

+ (void)setUsername:(NSString *)username {
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:kUsernameKey];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+(void)setStartVisitwithBeacon:(BOOL)visitFlag
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"StartVisitWithBeaconFlag"];
}

+ (NSString *)password {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kPasswordKey];
}


+ (void)setPassword:(NSString *)password {
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:kPasswordKey];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)ClearLoginCredentials
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kPasswordKey];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kUsernameKey];
    
}

+ (NSMutableDictionary *)customer
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentCustomer"] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
    return testServerDict;

}
+ (void)setCustomer:(NSMutableDictionary *)customerDict
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:customerDict error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"CurrentCustomer"];
    //[[NSUserDefaults standardUserDefaults] synchronize];   
}
+ (void)clearCustomer
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentCustomer"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)locationFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kLocationFilterCustomerList];
}

+ (void)setLocationFilterForCustomerList:(NSString *)filter {
    [[NSUserDefaults standardUserDefaults] setObject:filter forKey:kLocationFilterCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)filterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kFilterCustomerList];
}

+ (void)setFilterForCustomerList:(NSString *)cus_filter {
    [[NSUserDefaults standardUserDefaults] setObject:cus_filter forKey:kFilterCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

////////
+ (NSString *)statusFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kStatusFilterForCustomerList];
}

+ (void)setStatusFilterForCustomerList:(NSString *)status_filter {
    [[NSUserDefaults standardUserDefaults] setObject:status_filter forKey:kStatusFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)paymentFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kPaymentFilterForCustomerList];
}

+ (void)setPaymentFilterForCustomerList:(NSString *)payment_filter {
    [[NSUserDefaults standardUserDefaults] setObject:payment_filter forKey:kPaymentFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
///////
+ (NSString *)codeFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kCodeFilterForCustomerList];
}

+ (void)setCodeFilterForCustomerList:(NSString *)code_filter {
    [[NSUserDefaults standardUserDefaults] setObject:code_filter forKey:kCodeFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)barCodeFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kBarCodeFilterForCustomerList];
}

+ (void)setbarCodeFilterForCustomerList:(NSString *)barcode_filter {
    [[NSUserDefaults standardUserDefaults] setObject:barcode_filter forKey:kBarCodeFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)nameFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kNameFilterForCustomerList];
}

+ (void)setNameFilterForCustomerList:(NSString *)name_filter {
    [[NSUserDefaults standardUserDefaults] setObject:name_filter forKey:kNameFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
//////
+ (NSString *)lastCollectionReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kLastCollectionRef];
}

+ (void)setLastCollectionReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:kLastCollectionRef];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)filterForProductList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kFilterCustomerList];
}

+ (void)setFilterForProductList:(NSString *)pro_filter {
    [[NSUserDefaults standardUserDefaults] setObject:pro_filter forKey:kFilterCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)productFilterBrand {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kProductBrandFilter];
}

+ (void)setProductFilterBrand:(NSString *)value {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:kProductBrandFilter];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)productFilterName {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kProductNameFilter];
}

+ (void)setProductFilterName:(NSString *)value {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:kProductNameFilter];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)productFilterProductID {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kProductProductIDFilter];
}

+ (void)setProductFilterProductID:(NSString *)value {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:kProductProductIDFilter];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString*)coachProfile
{
    NSString *coachID=[[NSUserDefaults standardUserDefaults] valueForKey:@"coachProfile"];
    return coachID;
}
+(void)setCoachProfile:(NSString*)coachID
{
    [[NSUserDefaults standardUserDefaults ]setValue:coachID forKey:@"coachProfile"];
}
+(void)clearCoachProfile
{
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"coachProfile"];
}

+ (NSDictionary *)userProfile {

    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:kUserProfile] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
    return testServerDict;
}


+ (void)setUserProfile:(NSDictionary *)array {
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:array error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:kUserProfile];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)productCategory
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"ProductCategory11"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
    return testServerDict;
    
}
+ (void)setProductCategory:(NSDictionary *)category
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:category error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"ProductCategory11"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)clearProductCategory
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentCustomer"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
//[[NSUserDefaults standardUserDefaults] setObject:yourMutableArray forKey:@"Key"];
//NSMutableArray *array = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Key"]];

+ (NSString *)paymentType
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"PaymentType"];

}

+ (void)setPaymentType:(NSString *)value
{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:@"PaymentType"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)currentVisitID;
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"VisitOnHold_ID"];

}

+ (void)clearCurrentVisitID
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"VisitOnHold_ID"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setCurrentVisitID:(NSString *)visit_ID;
{
    [[NSUserDefaults standardUserDefaults] setObject:visit_ID forKey:@"VisitOnHold_ID"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)orderAmount
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"OrderAmount"];

}

+ (NSString *)pdc_due
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"PDC_Due"];
    
}
+ (NSString *)Credit_Period
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"Credit_Period"];
    
}
+ (NSString *)Over_Due
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"Credit_Period"];
    
}
+ (void)setOrderAmount:(NSString *)orderAmount CreditLimit:(NSString *)credit_Limit PDCDUE:(NSString *)pdc_due Credit_Limit:(NSString *)credit_period OverDues:(NSString *)over_due
{
    [[NSUserDefaults standardUserDefaults] setObject:orderAmount forKey:@"OrderAmount"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:credit_Limit forKey:@"Credit_Limit"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:pdc_due forKey:@"PDC_Due"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:credit_period forKey:@"Credit_Period"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:over_due forKey:@"Over_Due"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)availableBalance
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"avl_bal"];

}
+ (void)setAvailableBalance:(NSString *)avl_bal
{
    [[NSUserDefaults standardUserDefaults] setObject:avl_bal forKey:@"avl_bal"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastOrderReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastOrderRef"];
}

+ (void)setLastOrderReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastOrderRef"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastPerformaOrderReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastPerformaOrderRef"];
}

+ (void)setLastPerformaOrderReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastPerformaOrderRef"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastReturnOrderReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"LastReturnsOrder"];
}

+ (void)setLastReturnOrderReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"LastReturnsOrder"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)serverArray
{
    
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"serverArray"];

}
+ (void)setServerArray:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"serverArray"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)selectedServerDictionary
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"serverDictionary"];

}
+ (void)setSelectedServerDictionary:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"serverDictionary"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+(void)setActivatedServerDetailsDictionary:(NSString*)activatedServerDetailsString
{
    [[NSUserDefaults standardUserDefaults] setObject:activatedServerDetailsString forKey:@"activatedServerDetails"];

}
+ (NSString *)activatedServerDictionary
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"activatedServerDetails"];
    
}

+ (NSString *)activationStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"actStats"];
    
}
+ (void)setActivationStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"actStats"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncdate"];

}
+ (void)setLastSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncdate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncstatus"];

}
+ (void)setLastSyncStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncstatus"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncType
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncType"];
}
+ (void)setLastSyncType:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncType"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncOrderSent
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncOrderSent"];
}

+ (void)setLastSyncOrderSent:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncOrderSent"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)lastVTRXBackGroundSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastVTRXBackgroundSyncdate"];
    
}
+ (void)setLastVTRXBackGroundSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastVTRXBackgroundSyncdate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)lastCOMMBackGroundSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastCOMMBackgroundSyncdate"];

}
+ (void)setLastCOMMBackGroundSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastCOMMBackgroundSyncdate"];

}

+ (NSString *)lastRouteRescheduleCancelBackGroundSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastRouteRescheduleCancelBackGroundSyncDate"];
}
+ (void)setLastRouteRescheduleCancelBackGroundSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastRouteRescheduleCancelBackGroundSyncDate"];
}

+ (NSString *)lastRouteRescheduleBackGroundSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastRouteRescheduleBackGroundSyncDate"];
}
+ (void)setLastRouteRescheduleBackGroundSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastRouteRescheduleBackGroundSyncDate"];
}

+ (NSString *)lastProductStockBackGroundSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastProductStockBackgroundSyncdate"];
    
}
+ (void)setLastProductStockBackGroundSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastProductStockBackgroundSyncdate"];
    
}
+ (NSString *)VisitsAndTransactionsSyncSwitchStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"VisitsAndTransactionsSyncSwitch"];
    
}
+ (void)setVisitsAndTransactionsSyncSwitchStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"VisitsAndTransactionsSyncSwitch"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)CommunicationSyncSwitchStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"CommunicationSyncSwitch"];
    
}
+ (void)setCommunicationSyncSwitchStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"CommunicationSyncSwitch"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastFullSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastFullSyncdate"];
    
}
+ (void)setLastFullSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastFullSyncdate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSDictionary *)licenseKey
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"licenseKey"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    return testServerDict;
}
+ (void)setLicenseKey:(NSDictionary *)category
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:category error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"licenseKey"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+(CGSize)fetchSizeofLabelWithText:(NSString*)text andFont:(UIFont*)fontString
{
    CGSize size = [text sizeWithAttributes:
                   @{NSFontAttributeName: fontString}];
    
    // Values are fractional -- you should take the ceilf to get equivalent values
    CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    
    return adjustedSize;
    
}

+(CGSize)fetchSizewithFontConstrainttoSize :(NSString*)text fontName:(UIFont*)fontName labelSize:(CGSize)labelSize

{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping; //set the line break mode
    NSDictionary *attrDict = [NSDictionary dictionaryWithObjectsAndKeys:fontName, NSFontAttributeName, paragraphStyle, NSParagraphStyleAttributeName, nil];
    CGSize size = [text boundingRectWithSize:labelSize
                                     options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attrDict context:nil].size;
    
    return size;
    
}




+ (NSString *)usernameForActivate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"usernameForActivate"];

}
+ (void)setUsernameForActivate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"usernameForActivate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)passwordForActivate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"passwordForActivate"];
    
}
+ (void)setPasswordForActivate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"passwordForActivate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)isActivationDone
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"isactivationdone"];
    
}
+ (void)setIsActivationDone:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"isactivationdone"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (int)collectionCounter
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"collectionScore"];
}
+ (void)setCollectionCounter:(int)ref
{
    [[NSUserDefaults standardUserDefaults] setInteger:ref forKey:@"collectionScore"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)licenseCustomerID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"licenseCustomerID"];
}
+ (void)setLicenseCustomerID:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"licenseCustomerID"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)licenseIDForInfo
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"licenseIDForInfo"];
}
+ (void)setLicenseIDForInfo:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"licenseIDForInfo"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)odoMeterPreviousReading {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"odoMeterReading"];
}
+ (void)setOdoMeterPreviousReading:(NSString *)reading {
    [[NSUserDefaults standardUserDefaults] setObject:reading forKey:@"odoMeterReading"];
}

+(void)setSalesWorxLicenseActivationStatus:(BOOL)status

{
    [[NSUserDefaults standardUserDefaults]setBool:status forKey:kLicenseActivationStatus];
    
}
+(BOOL)fetchSalesWorxLicenseActivationStatus
{
    BOOL status=[[NSUserDefaults standardUserDefaults]boolForKey:kLicenseActivationStatus];
    return status;
    
}

+ (NSArray *)appControl
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"appControll"] dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *testServerDict = [NSArray arrayWithArray:[djsonSerializer deserializeAsArray:dataDict error:&error]  ] ;
    
    return testServerDict;
}
+ (void)setAppControl:(NSArray *)array
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:array error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"appControll"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}


+(NSString*)checkMultiUOM

{
    
    
    NSString *flag;
    NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check multi UOM %@",[appCtrlFlag valueForKey:@"ENABLE_MULTI_UOM"]);
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"ENABLE_MULTI_UOM"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        return  flag=@"N";
        
    }
    else if ([[[appCtrlFlag valueForKey:@"ENABLE_MULTI_UOM"] objectAtIndex:0] isEqualToString:@"Y"] ) {
        
        flag=@"Y";
        
    }
    
    else
    {
        flag=@"N";
    }
    
    return flag;
    
}

+(NSString*)checkTgtField

{
    NSString *flag;
    NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check app control flag for custmer target %@",[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"]);
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        return  flag=@"N";

    }
    
   
    
   else if ([[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0] isEqualToString:@"Y"] ) {

        flag=@"Y";
    
    }
    
    else
    {
        flag=@"N";
    }
    
    return flag;
    
}



+(NSString*)formatStringWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}


+(NSString*)checkTargetSalesDashBoard

{
    //"DASHBOARD_TYPE" = "TARGET-SALES";
    
    
    
    
    NSString *flag;
    NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check app control flag for custmer target %@",[appCtrlFlag valueForKey:@"DASHBOARD_TYPE"]);
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"DASHBOARD_TYPE"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        return  flag=@"N";
        
    }
    
    
    
    else if ([[[appCtrlFlag valueForKey:@"DASHBOARD_TYPE"] objectAtIndex:0] isEqualToString:@"TARGET-SALES"] ) {
        
        flag=@"Y";
        
    }
    
    else
    {
        flag=@"N";
    }
    
    return flag;

    
    
    
    
    

    
}







+ (NSString *)versionNumber
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"appVersionNumber"];
}

+ (void)setVersionNumber:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"appVersionNumber"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)startDayStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"startDayStatus"];
}
+ (void)setStartDayStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"startDayStatus"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSMutableDictionary *)validateAvailableBalance:(NSArray *)orderAmmount
{
    NSString *tempPDC,*tempOD;
    if ([orderAmmount count]!=0)
    {
        NSMutableArray *pdcArray =[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT PDC_DUE,Over_Due  FROM TBL_Customer_Dues  WHERE Customer_ID= '%@' ",[[SWDefaults customer] stringForKey:@"Customer_ID"]]];
        
        if ([pdcArray count]!=0)
        {
            NSMutableDictionary *pdcDict = [pdcArray objectAtIndex:0];
            [SWDefaults setOrderAmount:[[orderAmmount objectAtIndex:0] stringForKey:@"OrderAmount"] CreditLimit:@"" PDCDUE:[pdcDict stringForKey:@"PDC_Due"] Credit_Limit:@"" OverDues:[pdcDict stringForKey:@"Over_Due"]];
            
            tempPDC=[pdcDict stringForKey:@"PDC_Due"] ;
            tempOD=[pdcDict stringForKey:@"Over_Due"] ;
        }
        else
        {
            [SWDefaults setOrderAmount:[[orderAmmount objectAtIndex:0] stringForKey:@"OrderAmount"] CreditLimit:@"" PDCDUE:@"0" Credit_Limit:@"" OverDues:@"0"];
            
            tempPDC=@"N/A" ;
            tempOD=@"N/A" ;
        }
        [SWDefaults setOrderAmount:[[orderAmmount objectAtIndex:0] stringForKey:@"OrderAmount"] CreditLimit:@"" PDCDUE:tempOD Credit_Limit:@"" OverDues:tempOD];
    }
    else
    {
        NSMutableArray *pdcArray =[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT PDC_DUE,Over_Due  FROM TBL_Customer_Dues  WHERE Customer_ID= '%@' ",[[SWDefaults customer] stringForKey:@"Customer_ID"]]];
        
        if ([pdcArray count]!=0)
        {
            NSMutableDictionary *pdcDict = [pdcArray objectAtIndex:0];
            tempPDC=[pdcDict stringForKey:@"PDC_Due"] ;
            tempOD=[pdcDict stringForKey:@"Over_Due"] ;

        }
        else
        {
            tempPDC=@"N/A" ;
            tempOD=@"N/A" ;

        }
        [SWDefaults setOrderAmount:@"0" CreditLimit:@"" PDCDUE:tempOD Credit_Limit:@"" OverDues:tempOD];
    }
    
    float availableBalance = [[[SWDefaults customer] stringForKey:@"Avail_Bal"] floatValue]-[[SWDefaults orderAmount] floatValue];
    [[SWDefaults customer] setValue:[NSString stringWithFormat:@"%.02f",availableBalance] forKey:@"Avail_Bal"];
    [SWDefaults setAvailableBalance:[NSString stringWithFormat:@"%.02f",availableBalance]];
    
    
    NSMutableDictionary *temp = [SWDefaults customer];
    [temp setValue:tempPDC forKey:@"PDC_Due"];
    [temp setValue:tempOD forKey:@"Over_Due"];
    
    [SWDefaults setCustomer:temp];
    return [SWDefaults customer];
}

+ (void)clearEverything
{
    
}

+(NSString*)applicationDocumentsDirectory
{
    NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
    
    return [filePath path];
    
    
    
}



+(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_IN"] ];

    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}



+(NSString*)fetchDashboardType

{
   NSArray* appCtrl=[SWDefaults appControl];
    
    
    NSString* dashboardType;
    
    if (appCtrl) {
        
        if ([[appCtrl valueForKey:@"CUST_DTL_SCREEN"]isEqualToString:@"DEFAULT"]) {
            
            
            dashboardType= @"DEFAULT";
        }
        
        else
        {
            dashboardType= @"DASHBOARD";
        }
    }
    
    return dashboardType;
    

}

+(void)clearSignature

{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signature"];
    
    
}

+(void)setOnsiteVisitStatus:(BOOL)option
{
    [[NSUserDefaults standardUserDefaults]
     setBool:option forKey:@"isOnsiteVisit"];
    
    NSLog(@"onsite status in set defaults %hhd",[[NSUserDefaults standardUserDefaults]boolForKey:@"isOnsiteVisit"] );
    
    
}



+(void)isDCOptionalforOnsiteVisit:(BOOL)status
{
    [[NSUserDefaults standardUserDefaults]setBool:status forKey:@"isDCOptionalandOnsiteVisit"];
    
}

+(BOOL)fetchDCOptionalStatusforOnsiteVisit
{
    BOOL status;
    
    status=[[NSUserDefaults standardUserDefaults]boolForKey:@"isDCOptionalandOnsiteVisit"];
    return status;
    
}



+(BOOL)fetchOnsiteVisitStatus
{
    BOOL status=[[NSUserDefaults standardUserDefaults]boolForKey:@"isOnsiteVisit"];
    
    NSLog(@"onsite status in fetch defaults %hhd",status );
    
    
    return status;
}



+(BOOL)isMedRep
{
    BOOL result=NO;
    
    
    NSArray* appCtrl=[SWDefaults appControl];
    
    
    if (appCtrl.count>0) {
        
        if ([[[appCtrl objectAtIndex:0] valueForKey:@"ENABLE_MEDREP"]isEqualToString:@"Y"]) {
            
            
            result=YES;
        }
        
        else
        {
            result=NO;
        }
    }
    
    return result;
}

+(NSDictionary*)fetchBarAttributes
{
    NSDictionary *barattributes = [NSDictionary dictionaryWithObject:NavigationBarButtonItemFont
                                                              forKey:NSFontAttributeName];
    
    return barattributes;
    
}

+ (NSMutableDictionary *)nullFreeDictionary:(NSMutableDictionary *)dictionary {
    for (NSString *key in [dictionary allKeys]) {
        id nullString = [dictionary objectForKey:key];
        if ([nullString isKindOfClass:[NSDictionary class]]) {
            [self nullFreeDictionary:(NSMutableDictionary*)nullString];
        } else {
            if ((NSString*)nullString == (id)[NSNull null])
                [dictionary setValue:@"" forKey:key];
        }
    }
    return dictionary;
}

+ (BOOL)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
    
    
    BOOL fileExists=[[NSFileManager defaultManager]fileExistsAtPath:path];
    
    if (fileExists==YES) {
        
        NSLog(@"COLLECTION IMAGE SAVED SUCCESSFULLY");
        return YES;
    }
    else
    {
        return NO;
    }
}


+ (NSString*) dbGetImagesDocumentPath
{
    
    //     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}


+(UIView*)createNavigationBarTitleView:(NSString*)title
{
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text =NSLocalizedString(title,nil);
    [label sizeToFit];
    return label;
}

+(void)showAlertView :(NSMutableArray*)alertContentArray
{
    //Step 1: Create a UIAlertController
    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:@"MyTitle"
                                                                               message: @"MyMessage"
                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
    
    //Step 2: Create a UIAlertAction that can be added to the alert
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:KAlertOkButtonTitle
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here, eg dismiss the alertwindow
                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    //Step 3: Add the UIAlertAction ok that we just created to our AlertController
    [myAlertController addAction: ok];
    
    //Step 4: Present the alert to the user
    
    [[[[UIApplication sharedApplication]keyWindow]rootViewController]presentViewController:myAlertController animated:YES completion:^{
        
    }];
    
   // [self presentViewController:myAlertController animated:YES completion:nil];
}

+(NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end andMainString:(NSString *)mainString
{
    NSScanner* scanner = [NSScanner scannerWithString:mainString];
    [scanner setCharactersToBeSkipped:nil];
    [scanner scanUpToString:start intoString:NULL];
    if ([scanner scanString:start intoString:NULL])
    {
        NSString* result = nil;
        if ([scanner scanUpToString:end intoString:&result])
        {
            return result;
        }
    }
    return nil;
}



+(BOOL)validateSalesWorxLicense :(NSString*)licenseLimit

{
    /*
     
     conditions for validation
     
     1.	If license avid does not match the avid packaged in the application, validation process fails
     2.	If license cid does not match the cid stored on the device, validation process fails
     3.	If license sid does not match the device-identifier of the device, validation process fails
     4.	If license-type is EVAL_TIME and current-date > (license-date + license-limit) then license is expired and hence the validation process fails
     
     compare the vales stored in device defaults with the license
     
     
     the first three will any how be same, just compare the date
     
     
     */
    
    if ([licenseLimit isEqualToString:@"0"]) {
        
        //this is permanent license
        
        return YES;
    }
    
    
    else
    {
    
    NSString* defaultsAvid=@"19";
    NSString* defaultsDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString* licenseID=[SWDefaults licenseIDForInfo];
    NSString* licenseCustomerID=[SWDefaults licenseCustomerID];
    
    
    NSDictionary* defaultsLicenseKey=[SWDefaults licenseKey];
    
    
    
    
    
    
    NSLog(@"Defaults vales : \n  Device ID : \n %@  \n License ID : %@ \n License Customer ID :\n %@", defaultsDeviceID,licenseID,licenseCustomerID);
    
    
    NSLog(@"defaults license key %@", defaultsLicenseKey );
    
    
    
    
    

    
    NSData * licenseActivatedDateData=[SSKeychain passwordDataForService:kLicenseActivationDateKey account:kLicenseActivationAccount];
    
    NSString* licenseActivatedDateString=[[NSString alloc]initWithData:licenseActivatedDateData encoding:NSUTF8StringEncoding];
    NSLog(@"license Activated string in validation %@", licenseActivatedDateString);
    
    
    NSDate * currentDate=[NSDate date];
    
    NSDateFormatter * licenseDateFormat=[[NSDateFormatter alloc]init];
    
    licenseDateFormat.dateFormat=@"yyyy-MM-dd";
    
    NSDate * licenseActivatedDate=[licenseDateFormat dateFromString:licenseActivatedDateString];
    
    NSString* currentDateString=[licenseDateFormat stringFromDate:currentDate];
    
    NSDate * currentFormattedDate=[licenseDateFormat dateFromString:currentDateString];
    

                                                                             
    double hoursSinceLicenseActivated=fabs([licenseActivatedDate timeIntervalSinceNow] / 3600.0);
                                            
    double daysSinceLicenseActivated=hoursSinceLicenseActivated/24;
    
    
    NSLog(@"LICENSE ACTIVATED %0.2f days AGO", daysSinceLicenseActivated);

    

    
    double licenseTimeLimit = [licenseLimit doubleValue];
    
    
    
    if (daysSinceLicenseActivated>licenseTimeLimit) {
        
        NSLog(@"YOUR %@ DAYS LICENSE ACTIVATED ON %@ HAS EXPIRED", licenseLimit,[licenseDateFormat stringFromDate:licenseActivatedDate] );
        
        NSString* licenseExpiredString=[NSString stringWithFormat:@"Your %@ days license activated on %@ has been expired, please contact your system administrator", licenseLimit,[licenseDateFormat stringFromDate:licenseActivatedDate]];
        
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
            
            
            
            //Step 1: Create a UIAlertController
            UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:@"License Expired"
                                                                                       message:licenseExpiredString
                                                                                preferredStyle:UIAlertControllerStyleAlert                   ];
            
            //Step 2: Create a UIAlertAction that can be added to the alert
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:KAlertOkButtonTitle
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     //Do some thing here, eg dismiss the alertwindow
                                     [myAlertController dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            
            //Step 3: Add the UIAlertAction ok that we just created to our AlertController
            [myAlertController addAction: ok];
            
[[[[UIApplication sharedApplication]keyWindow]rootViewController]presentViewController:myAlertController animated:YES completion:^{
    
}];
            
            
            
            
        }
        
        else
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"License Expired"
                                                            message:licenseExpiredString
                                                           delegate:self
                                                  cancelButtonTitle:KAlertOkButtonTitle
                                                  otherButtonTitles:nil];
            [alert show];
#pragma clang diagnostic pop
        }

        return NO;
 
        
    }
        
        else
        {
            return YES;
        }
    
    
    //now compare licenseActivatedDate and current formatted date
}

}

+(NSMutableArray *)parseCustomersArray:(NSMutableArray*)customersArray
{
    NSMutableArray *tempCustomersObjectsList=[[NSMutableArray alloc]init];
    if(customersArray.count>0)
    {
        for (NSInteger i=0; i<customersArray.count; i++) {
            CustomerClass *customer=[[CustomerClass alloc]init];
            customer.customerId=[SWDefaults getValidStringValue:[[customersArray objectAtIndex:i] valueForKey:@"Customer_ID"]];

            customer.customerName=[SWDefaults getValidStringValue:[[customersArray objectAtIndex:i] valueForKey:@"Customer_Name"]];
            customer.customerNumber=[SWDefaults getValidStringValue:[[customersArray objectAtIndex:i] valueForKey:@"Customer_No"]];
            if([[SWDefaults getValidStringValue:[[customersArray objectAtIndex:i] valueForKey:@"Cash_Cust"]].uppercaseString isEqualToString:@"Y"])
            {
                customer.isCashCustomer=YES;
            }
            else
            {
                customer.isCashCustomer=NO;
            }
            customer.customerTradeChannel=[SWDefaults getValidStringValue:[[customersArray objectAtIndex:i] valueForKey:@"Trade_Classification"]];
            [tempCustomersObjectsList addObject:customer];
        }
    }
    
    return tempCustomersObjectsList;
}
+(NSString *)getValidStringValue:(id)inputstr
{
    if ([inputstr isEqual:(id)[NSNull null]] || inputstr==nil)
    {
        return [[NSString alloc]init];
    }
    if([inputstr isKindOfClass:[NSString class]] && ([inputstr isEqualToString:@"<null>"] || [inputstr isEqualToString:@"<Null>"]) )
    {
        return [[NSString alloc]init];
    }
    
    return [NSString stringWithFormat:@"%@",inputstr];
}

+(NSString *)getValidStringAndReplaceEmptyStringWithDefaultValue:(NSString *)str
{
    if([[[SWDefaults getValidStringValue:str] trimString] length]==0)
        return KNotApplicable;
    else
        return str;
    
}

+(void)setSignature:(id)signatureData

{
    [[NSUserDefaults standardUserDefaults]setValue:signatureData forKey:@"signature"];
}



+(id)fetchSignature
{
    id signature=[[NSUserDefaults standardUserDefaults]valueForKey:@"signature"];
    return signature;
    
}



+(NSMutableArray*)fetchSyncLocations
{
    
    
    NSString* customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    
    NSString* syncUrlString=[NSString stringWithFormat:@"http://www.ucssolutions.com/licman/get-sync-locations.php?cid=%@&avid=%@",customerID,@"19"];
    
    
    
    
    NSMutableURLRequest * syncUrlRequest=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:syncUrlString]];
    
    
     __block  NSMutableArray* syncLocationsWebserviceArray=[[NSMutableArray alloc]init];
   
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    

    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:syncUrlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            
            

            
            NSError* dataerror;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                      options:kNilOptions
                                                      error:&dataerror];
            NSArray *copiedArray = json.copy;
            
            if (!dataerror) {
            for (NSInteger i=0; i<copiedArray.count; i++) {
                                                  
            NSMutableDictionary * currentDict=[copiedArray objectAtIndex:i];
            SyncLoctions*      syncLocations=[SyncLoctions new];
            syncLocations.name=[currentDict valueForKey:@"name"];
            syncLocations.seq=[currentDict valueForKey:@"seq"];
            syncLocations.url=[currentDict valueForKey:@"url"];
            [syncLocationsWebserviceArray addObject:syncLocations];
            NSLog(@"webservices array is %@", currentDict);
                                                  
                
                
                                                  
//                                                  [SWDefaults setUsernameForActivate:usernameTextField.text];
//                                                  [SWDefaults setPasswordForActivate:passwordTextField.text];
//                                                  
                                                  
//                                                  NSError * error;
//                                                  
//                                                  CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
//                                                  
//                                                  NSData *dataDict = [jsonSerializer serializeObject:currentDict error:&error];
//                                                  NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
//                                                  [SWDefaults setSelectedServerDictionary:stringDataDict];
//                                                  
//                                                  
//                                                  
//                                                  NSLog(@"setting server array %@", copiedArray);
//                                                  
//                                                  
//                                                  
//                                                  NSData *datas = [jsonSerializer serializeObject:copiedArray error:&error];
//                                                  NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
//                                                  [SWDefaults setServerArray:stringData];
                                                  
                                                  
                                                  
                                                  
                                                  
                                                  
                                              }
                if (syncLocationsWebserviceArray.count>0) {
                    dispatch_semaphore_signal(semaphore);

                }
               
                                              
                                          }
                                          
                                          
                                      }];
    
    [dataTask resume];

    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

    
    
    if (syncLocationsWebserviceArray.count>0) {
        return syncLocationsWebserviceArray;
        
    }
    else
    {
        return nil;
    }

    
}


+(NSString*)GetCurrentTimeStamp
{
    NSDate *currentDate = [[NSDate alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    // or specifc Timezone: with name
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:timeZone];
    NSString *DateString = [dateFormatter stringFromDate:currentDate];
    return DateString;
}

//+ (void)redirectLogToDocuments
//{
//    NSString *documentsDirectory = [SWDefaults applicationDocumentsDirectory];
//    
//    NSDate *currentDate = [NSDate date];
//    NSCalendar* calendar = [NSCalendar currentCalendar];
//    
//    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
//    NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)[components month]];
//    
//    NSString *pathForLog = [documentsDirectory stringByAppendingPathComponent:documentPathComponent];
//    
//    freopen([pathForLog cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
//}



+ (void)redirectLogToDocuments
{
    
    NSString *documentPathComponent=[[SWDefaults fetchPreviousDeviceLogsFileNames]objectAtIndex:0];
    ;
    
    NSString *logFilePath=[[SWDefaults fetchDeviceLogsFolderPath] stringByAppendingPathComponent:documentPathComponent];
    
    
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}


+ (void)writeLogs:(NSString *)str {
    NSString *timeStampStr=[SWDefaults GetCurrentTimeStamp];
    
    NSMutableString *documentsDirectory=[[SWDefaults applicationDocumentsDirectory] mutableCopy];
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
    NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)[components month]];
    NSString *fileSavePath=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    NSString *dataStr=[[NSString alloc]initWithFormat:@"\n[%@ ] - %@",timeStampStr,str];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileSavePath])
    {
        [dataStr writeToFile:fileSavePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
    }
    else
    {
        NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:fileSavePath];
        [myHandle seekToEndOfFile];
        [myHandle writeData:[dataStr dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

+(NSString*)fetchDeviceLogsFolderPath
{
    NSMutableString *documentsDirectory=[[SWDefaults applicationDocumentsDirectory] mutableCopy];
    
    NSString* deviceLogsPath=[documentsDirectory stringByAppendingPathComponent:@"Device Logs"];
    
    BOOL isDir=YES;
    
    if ([[NSFileManager defaultManager]fileExistsAtPath:deviceLogsPath isDirectory:&isDir]==NO) {
        
        [[NSFileManager defaultManager]createDirectoryAtPath:deviceLogsPath withIntermediateDirectories:NO attributes:nil error:nil];
        deviceLogsPath=[documentsDirectory stringByAppendingPathComponent:@"Device Logs"];
    }
    return deviceLogsPath;
    
}


+(void)deletePreviousMonthlyLog
{
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
    
    long i = components.month;
    NSMutableString *documentsDirectory=[[SWDefaults applicationDocumentsDirectory] mutableCopy];
    NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)i];
    NSString *fileSavePath=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    if ([[NSFileManager defaultManager]fileExistsAtPath:fileSavePath]==YES) {
        //delete file
        NSError *error;
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:fileSavePath error:&error];
        if (success==YES) {
            NSLog(@"Previous monthly log file deleted on %@", [MedRepQueries fetchCurrentDateTimeinDatabaseFormat]);
        }
    }
}

+(void)deleteOldDeviceLogFiles
{
    
    //delete monthly log file
    [self deletePreviousMonthlyLog];
    
    
    //deleting previous all logs except for previous 3 days
    NSMutableArray* previousThreeFilesArray=[SWDefaults fetchPreviousDeviceLogsFileNames];
    
    NSError * error;
    NSMutableArray * totalFilesArray =  [[[NSFileManager defaultManager]
                                          contentsOfDirectoryAtPath:[SWDefaults fetchDeviceLogsFolderPath] error:&error] mutableCopy];
    
    for (NSInteger i=0; i<previousThreeFilesArray.count; i++) {
        if ([totalFilesArray containsObject:[previousThreeFilesArray objectAtIndex:i]]) {
            [totalFilesArray  removeObject:[previousThreeFilesArray objectAtIndex:i]];
        }
        else{
        }
    }
    
    //deleting files
    for (NSInteger i=0; i<totalFilesArray.count; i++) {
        [[NSFileManager defaultManager]removeItemAtPath:[[SWDefaults fetchDeviceLogsFolderPath] stringByAppendingPathComponent:[totalFilesArray objectAtIndex:i]] error:nil];
    }
}

+(NSMutableArray*)fetchPreviousDeviceLogsFileNames
{
    NSMutableArray* fileNamesArray=[[NSMutableArray alloc]init];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear
                                    | NSCalendarUnitMonth | NSCalendarUnitDay
                                               fromDate:[NSDate date]];
    for (int i = 0; i < 7; ++i) {
        NSDate *date = [calendar dateFromComponents:components];
        // NSLog(@"%d days before today = %@", i, date);
        --components.day;
        
        NSString* dateString=   [[SWDefaults fetchLogsDateFormat] stringFromDate:date];
        NSString *DeviceLogsFileName=[NSString stringWithFormat:@"DeviceLogs-%@.txt",dateString];
        
        [fileNamesArray addObject:DeviceLogsFileName];
        
    }
    
    return fileNamesArray;
    
}

+(NSDateFormatter*)fetchLogsDateFormat
{
    
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    
    return dateFormatter;
    
    
}
+(void)checkandDeletePreviousLogs
{
    
    
    NSCalendar *weekcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierISO8601];;
    NSDateComponents *dateComponent = [weekcalendar components:(NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[NSDate date]];
    NSLog(@"week number is %ld",(long)dateComponent.weekOfYear);
    
   
    
//    NSCalendar *testcalendar = [NSCalendar currentCalendar];
//    int aIntWeekFlag = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) ? NSCalendarUnitWeekOfYear : NSWeekCalendarUnit;
//    NSDateComponents *dateComponents = [testcalendar components:aIntWeekFlag fromDate:startDate  toDate:endDate options:0];
//    int aIntNumberOfweeks = [dateComponents week];
//    
    
  
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
    NSInteger previousMonth=[components weekOfYear]-1;
    
    //check if there exists any log file of previous month
    
    
    for (NSInteger i=0; i<=12; i++) {
       
        if (i==[components month]) {
            //dont delete current log
            
        }
        
        else
        {
        NSMutableString *documentsDirectory=[[SWDefaults applicationDocumentsDirectory] mutableCopy];
        NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)i];
        NSString *fileSavePath=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        
        if ([[NSFileManager defaultManager]fileExistsAtPath:fileSavePath]==YES) {
            
            //delete file
            NSError *error;
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:fileSavePath error:&error];
            if (success==YES) {
                
                NSLog(@"Previous log file deleted on %@", [MedRepQueries fetchCurrentDateTimeinDatabaseFormat]);
                
                [SWDefaults redirectLogToDocuments];
                
            }
            
        }
        
 
    }
    }
    
    
}

+(NSMutableDictionary*)fetchCurrentCustDict:(Customer*)selectedCustomer
{
    NSMutableDictionary * currentCustDict=[[NSMutableDictionary alloc]init];
    
    [currentCustDict setValue:selectedCustomer.Address forKey:@"Address"];
    [currentCustDict setValue:selectedCustomer.Allow_FOC forKey:@"Allow_FOC"];
    [currentCustDict setValue:selectedCustomer.Avail_Bal forKey:@"Avail_Bal"];
    [currentCustDict setValue:selectedCustomer.Customer_Name forKey:@"Customer_Name"];
    [currentCustDict setValue:selectedCustomer.Bill_Credit_Period forKey:@"Bill_Credit_Period"];
    [currentCustDict setValue:selectedCustomer.Cash_Cust forKey:@"Cash_Cust"];
    [currentCustDict setValue:selectedCustomer.Chain_Customer_Code forKey:@"Chain_Customer_Code"];
    [currentCustDict setValue:selectedCustomer.City forKey:@"City"];
    [currentCustDict setValue:selectedCustomer.Creation_Date forKey:@"Creation_Date"];
    [currentCustDict setValue:selectedCustomer.Credit_Hold forKey:@"Credit_Hold"];
    [currentCustDict setValue:selectedCustomer.Credit_Limit forKey:@"Credit_Limit"];
    [currentCustDict setValue:selectedCustomer.Cust_Lat forKey:@"Cust_Lat"];
    [currentCustDict setValue:selectedCustomer.Cust_Long forKey:@"Cust_Long"];
    [currentCustDict setValue:selectedCustomer.Cust_Status forKey:@"Cust_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Barcode forKey:@"Customer_Barcode"];
    [currentCustDict setValue:selectedCustomer.Customer_Class forKey:@"Customer_Class"];
    [currentCustDict setValue:selectedCustomer.Customer_ID forKey:@"Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_No forKey:@"Customer_No"];
    [currentCustDict setValue:selectedCustomer.Customer_OD_Status forKey:@"Customer_OD_Status"];
    [currentCustDict setValue:selectedCustomer.Customer_Segment_ID forKey:@"Customer_Segment_ID"];
    [currentCustDict setValue:selectedCustomer.Customer_Type forKey:@"Customer_Type"];
    [currentCustDict setValue:selectedCustomer.Dept forKey:@"Dept"];
    [currentCustDict setValue:selectedCustomer.Location forKey:@"Location"];
    [currentCustDict setValue:selectedCustomer.Phone forKey:@"Phone"];
    [currentCustDict setValue:selectedCustomer.Postal_Code forKey:@"Postal_Code"];
    [currentCustDict setValue:selectedCustomer.Price_List_ID forKey:@"Price_List_ID"];
    [currentCustDict setValue:selectedCustomer.SalesRep_ID forKey:@"SalesRep_ID"];
    [currentCustDict setValue:selectedCustomer.Sales_District_ID forKey:@"Sales_District_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Customer_ID forKey:@"Ship_Customer_ID"];
    [currentCustDict setValue:selectedCustomer.Ship_Site_Use_ID forKey:@"Ship_Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Site_Use_ID forKey:@"Site_Use_ID"];
    [currentCustDict setValue:selectedCustomer.Trade_Classification forKey:@"Trade_Classification"];
    [currentCustDict setValue:selectedCustomer.Trade_License_Expiry forKey:@"Trade_License_Expiry"];

    if (selectedCustomer.cashCustomerDictionary.count>0 && [selectedCustomer.Cash_Cust isEqualToString:@"Y"]) {
        
        //this will be called only for cash customers
        NSMutableDictionary * cashCustomerDict=selectedCustomer.cashCustomerDictionary;
        [currentCustDict setValue:[cashCustomerDict valueForKey:@"cash_Name"] forKey:@"cash_Name"];
        [currentCustDict setValue:[cashCustomerDict valueForKey:@"cash_Phone"] forKey:@"cash_Phone"];
        [currentCustDict setValue:[cashCustomerDict valueForKey:@"cash_Contact"] forKey:@"cash_Contact"];
        [currentCustDict setValue:[cashCustomerDict valueForKey:@"cash_Address"] forKey:@"cash_Address"];
        [currentCustDict setValue:[cashCustomerDict valueForKey:@"cash_Fax"] forKey:@"cash_Fax"];

        
    }
    

    
    
    return currentCustDict;
    

}

+(Customer*)fetchCurrentCustObject:(NSMutableDictionary*)currentCustomer
{
    Customer * currentCust=[[Customer alloc]init];
    currentCust.Address=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Address"]];
    currentCust.Avail_Bal=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Avail_Bal"]];
    currentCust.Allow_FOC=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Allow_FOC"]];
    currentCust.Customer_Name=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Name"]];
    currentCust.Bill_Credit_Period=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Bill_Credit_Period"]];
    currentCust.Cash_Cust=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cash_Cust"]];
    currentCust.Chain_Customer_Code=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Chain_Customer_Code"]];
    currentCust.City=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"City"]];
    currentCust.Creation_Date=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Creation_Date"]];
    currentCust.Credit_Hold=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Credit_Hold"]];
    currentCust.Credit_Limit=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Credit_Limit"]];
    currentCust.Cust_Lat=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cust_Lat"]];
    currentCust.Cust_Long=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cust_Long"]];
    currentCust.Cust_Status=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Cust_Status"]];
    currentCust.Customer_Barcode=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Barcode"]];
    currentCust.Customer_Class=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Class"]];
    currentCust.Customer_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_ID"]];
    currentCust.Customer_No=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_No"]];
    currentCust.Customer_OD_Status=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_OD_Status"]];
    currentCust.Customer_Segment_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Segment_ID"]];
    currentCust.Customer_Type=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Type"]];
    currentCust.Dept=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Dept"]];
    currentCust.Location=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Location"]];
    currentCust.Phone=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Phone"]];
    currentCust.Postal_Code=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Postal_Code"]];
    currentCust.Price_List_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Price_List_ID"]];
    currentCust.SalesRep_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"SalesRep_ID"]];
    currentCust.Sales_District_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Sales_District_ID"]];
    currentCust.Ship_Customer_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Ship_Customer_ID"]];
    currentCust.Ship_Site_Use_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Ship_Site_Use_ID"]];
    currentCust.Site_Use_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Site_Use_ID"]];
    currentCust.Trade_Classification=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Trade_Classification"]];
    currentCust.Over_Due=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Over_Due"]];
    if ([currentCustomer valueForKey:@"Planned_Visit_ID"]) {
        currentCust.Planned_visit_ID=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Planned_Visit_ID"]];
    }
    else{
        
    }
    
    if ([currentCustomer valueForKey:@"Visit_Type"]) {
        
        currentCust.Visit_Type=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Visit_Type"]];
    }
    
    if ([[currentCustomer valueForKey:@"Cash_Cust"] isEqualToString:@"Y"]) {
        
        currentCust.cash_Name=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Name"]];
        currentCust.cash_Phone=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Phone"]];
        currentCust.cash_Address=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Address"]];
        currentCust.cash_Contact=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Contact"]];
        currentCust.cash_Fax=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"cash_Fax"]];
    }
    return currentCust;
    
}

+(UITableView*)fetchUITableViewAttributes:(UITableView*)currentTableView
{
    CALayer *layer = currentTableView.layer;
    layer.borderWidth = 2;
    layer.borderColor = [[UIColor redColor] CGColor];
    layer.cornerRadius = 10;
    layer.masksToBounds = YES;
    return currentTableView;
}


+ (NSString*) dbGetDatabasePath
{
    NSString *fileName = databaseName;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentDBFolderPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:documentDBFolderPath])
    {
        return @"Database does not exist";
        
    }
    else
    {
        return [documentsDirectory stringByAppendingPathComponent:databaseName] ;
        
    }
}


+(void) createDirectory : (NSString *) dirName {
    
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    

    
   NSString* dataPath = (NSMutableString *)[documentsDirectory stringByAppendingPathComponent:dirName];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]) {
        NSLog(@"Couldn't create directory error: %@", error);
    }
    else {
        NSLog(@"directory created!");
    }
    
    NSLog(@"dataPath : %@ ",dataPath); // Path of folder created
    
}

+ (NSString*) dbGetDatabaseZipFilePath
{
    NSString *fileName = databaseName;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentDBFolderPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:documentDBFolderPath])
    {
        return @"Database does not exist";
        
    }
    else
    {
        return [documentsDirectory stringByAppendingPathComponent:kDatabaseZipFileName] ;
        
    }
}

+(NSPredicate*)fetchMultipartSearchPredicate:(NSString*)searchString withKey:(NSString*)keyString
{
    NSMutableArray *componentsPredicateArray=[[NSMutableArray alloc]init];
    NSPredicate *searchStringComponentsPredicate;
    NSString* filter = @"%K CONTAINS[cd] %@";

    NSArray *searchTextComponentsArray=[searchString componentsSeparatedByString:@"*"];
    NSLog(@"search text components are %@", searchTextComponentsArray);
    
    
    for (NSInteger i=0; i<searchTextComponentsArray.count; i++) {
        
        if([[[searchTextComponentsArray objectAtIndex:i]trimString]isEqualToString:@""]){
            
        }
        else{
            if([keyString isEqualToString:@""]){
                
                NSPredicate *componentesPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS [cd] %@", [searchTextComponentsArray objectAtIndex:i]];
                [componentsPredicateArray addObject:componentesPredicate];
                
            }else{
                NSPredicate *componentesPredicate = [NSPredicate predicateWithFormat:filter, keyString, [searchTextComponentsArray objectAtIndex:i]];
                [componentsPredicateArray addObject:componentesPredicate];
            }

        }
        
        NSLog(@"components predicate array %@", componentsPredicateArray);
        
        
    }
    
    searchStringComponentsPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:componentsPredicateArray];
    
    return searchStringComponentsPredicate;
}
-(NSString *)getAppVersionWithBuild
{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    NSLog(@"updating client version %@", [NSString stringWithFormat:@"%@ (%@)", version,build]);
    
    NSString * AppVersion =[NSString stringWithFormat:@"%@(%@)", version,build];
    
    return AppVersion;
}

#pragma mark UIAlertController Methods
+(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController{
    [UIView animateWithDuration:0 animations: ^{
        [viewController.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
    
}
+(void) ShowConfirmationAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController{
    [UIView animateWithDuration:0 animations: ^{
        [viewController.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage,nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        for (NSInteger i=0; i<actionsArray.count; i++) {
            [alert addAction:[actionsArray objectAtIndex:i]];
        }
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
    
}
+(UIAlertAction*)GetDefaultCancelAlertActionWithTitle:(NSString*)title
{
    UIAlertAction* CancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(title,nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                   }];
    return CancelAction;
}

#pragma mark LPOImages Methods
+(void)showCameraWithMode:(UIImagePickerControllerCameraCaptureMode)mode withDelegate:(UIViewController *)parentViewController
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [MedRepDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:@"Device has no camera" withController:parentViewController];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = parentViewController;
        picker.allowsEditing = YES;
        if(mode==UIImagePickerControllerCameraCaptureModeVideo)
        {
            picker.mediaTypes =
            [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
            picker.videoMaximumDuration=30;
            picker.videoQuality=UIImagePickerControllerQualityTypeMedium;
            
        }
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.cameraCaptureMode=mode;
        [parentViewController presentViewController:picker animated:YES completion:NULL];
    }
    
}
+(NSString *)getDCImagesFolderPath
{
    // get the image path
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",KDCImagesFolderName]];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return documentsDirectory;
    
}
+(NSString *)getSignatureImagesFolderPath
{
    // get the image path
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",KSignatureImagesFolderName]];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return documentsDirectory;
    
}
+(NSString *)getLPOImagesFolderPath
{
    // get the image path
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",KLPOImagesFolderName]];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return documentsDirectory;
    
}
+(void)saveLPOImage:(UIImage *)image  withName:(NSString *)ImageName
{
    NSString *documentsDirectory=[SWDefaults getLPOImagesFolderPath];
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    
    NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",ImageName];
    NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
        if(NO == [fileManager removeItemAtPath:filename error:NULL]) {
        }
    }
    NSData *data=UIImageJPEGRepresentation(image, 1.0);
    NSLog(@"%lu",(unsigned long)data.length);
    
    float compressionQuality=0.3;
    // Now, save the image to a file.
    if(NO == [UIImageJPEGRepresentation(image,compressionQuality) writeToFile:filename atomically:YES]) {
        [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", filename];
    }
    else
    {
        NSLog(@"Unable to save Image");
    }
}
+(void)deleteLPOTemporaryImages
{
    NSString *LPOImagesDirectory=[SWDefaults getLPOImagesFolderPath];
    LPOImagesDirectory= [LPOImagesDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:LPOImagesDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    for (NSString *file in [fm contentsOfDirectoryAtPath:LPOImagesDirectory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", LPOImagesDirectory, file] error:&error];
        if (!success || error) {
            // it failed.
        }
        else
        {
            NSLog(@"deleted file %@",file);
        }
    }

}
+(void)copyFilesFromLPOImagesTempFolder:(NSString*)path withPrefixName:(NSString *)orderId
{
    
    NSString *LPOImagesDirectory=[SWDefaults getLPOImagesFolderPath];
    NSString *LPOImagesTempDirectory= [LPOImagesDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:LPOImagesTempDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    NSInteger Imagecount=0;
    for (NSString *file in [fm contentsOfDirectoryAtPath:LPOImagesTempDirectory error:&error]) {
        Imagecount++;
        [fm moveItemAtPath:[NSString stringWithFormat:@"%@/%@", LPOImagesTempDirectory, file]
                    toPath:[NSString stringWithFormat:@"%@/%@", LPOImagesDirectory, [NSString stringWithFormat:@"%@_%ld.jpg",orderId,(long)Imagecount]]
                     error:&error];
        
    }
    
}
#pragma mark LPOImages Methods Completed

+(NSMutableArray *)getSyncImagesPathsInaFolder:(NSString*)folderPath
{
    NSMutableArray *imagesPathsArray=[[NSMutableArray alloc]init];
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    NSString * imagesdirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",folderPath]];
    [[NSFileManager defaultManager] createDirectoryAtPath:imagesdirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    
    for (NSString *file in [fm contentsOfDirectoryAtPath:imagesdirectory error:&error]) {
        
        
        if([[[file pathExtension] lowercaseString]isEqualToString:@"jpg"] ||[[[file pathExtension] lowercaseString]isEqualToString:@"jpeg"])
        {
            [imagesPathsArray addObject:[NSString stringWithFormat:@"%@/%@", imagesdirectory, file]];
        }
    }
    return imagesPathsArray;
}


//sync images objects

+(NSMutableArray *)getSyncImagesObjects
{
//    NSMutableArray *DCImagesPaths=[SWDefaults getSyncImagesPathsInaFolder:KSignatureImagesFolderName];
//    NSMutableArray *SignatureImagesPaths=[SWDefaults getSyncImagesPathsInaFolder:KDCImagesFolderName];
   
    NSMutableArray *DCImagesPaths=[SWDefaults getSyncImagesPathsInaFolder:KDCImagesFolderName];
    NSMutableArray *SignatureImagesPaths=[SWDefaults getSyncImagesPathsInaFolder:KSignatureImagesFolderName];

    NSMutableArray *LPOImagePaths=[SWDefaults getSyncImagesPathsInaFolder:KLPOImagesFolderName];
    NSMutableArray *collectionImagePaths=[SWDefaults getSyncImagesPathsInaFolder:KCollectionImagesFolderName];

    NSMutableArray *collectionReceiptImagePaths=[SWDefaults getSyncImagesPathsInaFolder:kCollectionReceiptFolderName];
    
    //competitor info pictures
    NSMutableArray* merchandisingImagePaths=[SWDefaults  getSyncImagesPathsInaFolder:kMerchandisingCompetitorInfoImages];
    
    
    //planogram pictures
    NSMutableArray* planogramImagePaths=[SWDefaults  getSyncImagesPathsInaFolder:kMerchandisingPlanogramImages];

    
    //survey pictures
    NSMutableArray* surveyImagePaths=[SWDefaults  getSyncImagesPathsInaFolder:KMerchandisingSurveyPicturesFolderName];

    //coach survey pictures
    NSMutableArray* coachSurveyImagePaths=[SWDefaults  getSyncImagesPathsInaFolder:KCoachSurveySignatureFolderName];
    

    NSMutableArray *imagesObjectsArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<DCImagesPaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[DCImagesPaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KDistributionImagesFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
        
    }
    for (NSInteger i=0; i<SignatureImagesPaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[SignatureImagesPaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KSignatureSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
        
    }
    for (NSInteger i=0; i<LPOImagePaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[LPOImagePaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KLPOImageSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
        
    }
    for (NSInteger i=0; i<collectionImagePaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[collectionImagePaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KCollectionImageSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
        
    }
    
    for (NSInteger i=0; i<collectionReceiptImagePaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[collectionReceiptImagePaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KCollectionReceiptImageSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
        
    }
    for (NSInteger i=0; i<merchandisingImagePaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[merchandisingImagePaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KMerchandisingSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
    }
    
    for (NSInteger i=0; i<planogramImagePaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[planogramImagePaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KMerchandisingSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
    }
    
    for (NSInteger i=0; i<surveyImagePaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[surveyImagePaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KMerchandisingSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
    }
    
    
    for (NSInteger i=0; i<coachSurveyImagePaths.count; i++) {
        SyncImage *syncImageObj=[[SyncImage alloc]init];
        syncImageObj.imagePath=[coachSurveyImagePaths objectAtIndex:i];
        syncImageObj.imageMIMEType=KJPEGMIMEFormat;
        syncImageObj.imageSyncType=KSignatureSyncFileType;
        syncImageObj.isUploaded=NO;
        [imagesObjectsArray addObject:syncImageObj];
    }
    
    
    
    
    
    return imagesObjectsArray;
    
}
+(NSMutableArray *)deleteSyncImagesInaFolder:(NSString*)folderPath
{
    NSMutableArray *imagesPathsArray=[[NSMutableArray alloc]init];
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    NSString * imagesdirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",folderPath]];
    [[NSFileManager defaultManager] createDirectoryAtPath:imagesdirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    
    for (NSString *file in [fm contentsOfDirectoryAtPath:imagesdirectory error:&error]) {
        
        
        if([[[file pathExtension] lowercaseString]isEqualToString:@"jpg"] ||[[[file pathExtension] lowercaseString]isEqualToString:@"jpeg"])
        {
            [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", imagesdirectory, file] error:&error];
        }
    }
    return imagesPathsArray;
}


#pragma mark SYNC Methods

+(BOOL)isDailyFullSyncRequired
{
    AppControl *appcontrol=[AppControl retrieveSingleton];

    if([appcontrol.ENABLE_DAILY_FULL_SYNC isEqualToString:KAppControlsYESCode] && (![[SWDefaults lastFullSyncDate] containsString:[MedRepQueries fetchDatabaseDateFormat]]))
        return YES;
    else
        return NO;
    
}
+(BOOL)isVersionUpdateFullSyncRequired
{
    return ![[SWDefaults versionNumber] isEqualToString:[[[SWDefaults alloc] init] getAppVersionWithBuild]];
}
+(BOOL)isVTRXBackgroundSyncRequired
{
    AppControl *appcontrol=[AppControl retrieveSingleton];        if([appcontrol.USER_ENABLED_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode] ||[[SWDefaults VisitsAndTransactionsSyncSwitchStatus] isEqualToString:KVisitsAndTrxsSettingsSwitchEnableFlag])
        {
            // Get conversion to months, days, hours, minutes
            NSCalendar *sysCalendar = [NSCalendar currentCalendar];
            NSCalendarUnit unitFlags = NSCalendarUnitMinute;
            NSDateComponents *lastFullSyncTimeInfo = [sysCalendar components:unitFlags fromDate:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]]  toDate:[MedRepQueries convertNSStringtoNSDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]  options:0];
            
            
            BOOL isBackgroundSyncTimeIntervelExceeded=YES;
            
            
#if DEBUG
            NSLog(@"I'm running in a DEBUG mode");
            [AppControl retrieveSingleton].V_TRX_SYNC_INTERVAL = @"1";
#else
#endif
            
            if([SWDefaults lastVTRXBackGroundSyncDate]!=nil)
            {
                NSDateComponents *BS_TimeInfo = [sysCalendar components:unitFlags fromDate:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastVTRXBackGroundSyncDate]]  toDate:[MedRepQueries convertNSStringtoNSDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]  options:0];
                if([BS_TimeInfo minute]<[[[AppControl retrieveSingleton] V_TRX_SYNC_INTERVAL] integerValue])
                    isBackgroundSyncTimeIntervelExceeded=NO;
                    
            }
            
            
            if([lastFullSyncTimeInfo minute] >= [[[AppControl retrieveSingleton] V_TRX_SYNC_INTERVAL] integerValue] && isBackgroundSyncTimeIntervelExceeded)
            {
                return YES;
                
            }
            else{
                NSLog(@"last V_Trx sync time less than the V_TRX_SYNC_INTERVAL");

            }
        }
        else
        {
            NSLog(@"switch not enabled");
        }

        return NO;
}
+(BOOL)isCommunicationBackgroundSyncRequired
{
    AppControl *appcontrol=[AppControl retrieveSingleton];
    
    if( [appcontrol.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode])
    {
 
        if([[SWDefaults CommunicationSyncSwitchStatus] isEqualToString:KCommunicationsSettingsSwitchEnableFlag])
        {    // Get conversion to months, days, hours, minutes
            NSCalendar *sysCalendar = [NSCalendar currentCalendar];
            NSCalendarUnit unitFlags = NSCalendarUnitMinute;
            NSDateComponents *lastFullSyncTimeInfo = [sysCalendar components:unitFlags fromDate:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]]  toDate:[MedRepQueries convertNSStringtoNSDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]  options:0];
            
            
            BOOL isCOMMBackgroundSyncTimeIntervelExceeded=YES;

            if([SWDefaults lastCOMMBackGroundSyncDate]!=nil)
            {
                NSDateComponents *BS_TimeInfo = [sysCalendar components:unitFlags fromDate:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastCOMMBackGroundSyncDate]]  toDate:[MedRepQueries convertNSStringtoNSDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]  options:0];
                if([BS_TimeInfo minute]<[[[AppControl retrieveSingleton] COMM_SYNC_INTERVAL] integerValue])
                    isCOMMBackgroundSyncTimeIntervelExceeded=NO;
                
            }
            
            
            if([lastFullSyncTimeInfo minute] >= [[[AppControl retrieveSingleton] COMM_SYNC_INTERVAL] integerValue] && isCOMMBackgroundSyncTimeIntervelExceeded)
            {
                return YES;
                
            }
            else{
                NSLog(@"last comm sync time less than the COMM_SYNC_INTERVAL");
                
            }
        }
        else
        {
            NSLog(@"comm switch not enabled");
        }
    }
    
    
    return NO;
}

+(BOOL)isRTL
{
    BOOL status=NO;
    
    if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
        status=YES;
    }
    return status;
    
}

#pragma Timer for Visit Methods
NSTimer *visitTimer;
+ (void)startVisitTimerWithController:(UIViewController *)viewController
{
    NSLog(@"visit timer started");
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:@"FS_Visit_StartTime"];
    visitTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(visitStartTime) userInfo:nil repeats:YES];
}
+(UIViewController *)currentTopViewController {
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    if ([topVC isKindOfClass:[SWSplitViewController class]]) {
        SWSplitViewController * currentTopVC=(SWSplitViewController*)topVC;
        return currentTopVC.frontViewController;

    }
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

+(void)invalidateCurrentFieldSalesVisitTimer
{
    if ([visitTimer isValid]) {
        
        NSLog(@"current timer was valid and invalidated");
        [visitTimer invalidate];
        visitTimer=nil;
    }
}

+(void)clearFSVisitTimer
{
    NSLog(@"clearing fs visit timer");
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FS_Visit_StartTime"];
    
}
+ (void)visitStartTime
{
    if ([visitTimer isValid]) {
        
    
        
    AppControl *appControl = [AppControl retrieveSingleton];
    
    NSDate * currentTimer=[[NSUserDefaults standardUserDefaults]objectForKey:@"FS_Visit_StartTime"];
    NSDate *now = [NSDate date];
    NSTimeInterval elapsedTimeDisplayingPoints = [now timeIntervalSinceDate:currentTimer];
    
    NSString *timeLimit =  [appControl FS_VISIT_TIMER_LIMIT];
    
    
    NSTimeInterval endTimeLimit = [timeLimit doubleValue]* 60;
    
    
    NSDate *currentDateTime = [NSDate dateWithTimeIntervalSinceReferenceDate:elapsedTimeDisplayingPoints];
    NSDate *endDateTime = [NSDate dateWithTimeIntervalSinceReferenceDate:endTimeLimit];
    if ([currentDateTime compare:endDateTime] == NSOrderedDescending)
    {
        
        NSLog(@"time up, closing the visit");
        [visitTimer invalidate];
        visitTimer=nil;
        [SWDefaults clearFSVisitTimer];

        
        SWAppDelegate *app = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
        UIViewController *vc = app.window.rootViewController;
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KConfirmationAlertYESButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       SalesWorxVisitOptionsViewController *obj = [[SalesWorxVisitOptionsViewController alloc]init];
                                       //[obj closeVisitTapped];
                                       
                                       [obj closeVisitWithEndDate];
                                       
                                       NSLog(@"Top view controller is %@",[SWDefaults currentTopViewController] );
                                       UINavigationController* currentTopVC;
                                       currentTopVC=(UINavigationController*) [SWDefaults currentTopViewController];

                                       
                                       if (currentTopVC.presentedViewController) {
                                           
                                           [currentTopVC.presentedViewController dismissViewControllerAnimated:YES completion:^{
                                               [currentTopVC popToRootViewControllerAnimated:YES];
  
                                           }];
                                       }
                                       else{
                                           [currentTopVC popToRootViewControllerAnimated:YES];

                                       }
                                       //[UIWindow visible]
                                       NSLog(@"OK action tapped");
                                   }];
        UIAlertAction* CancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KConfirmationAlertIgnoreButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           
                                       }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
        NSString *msg = [NSString stringWithFormat:@"The current visit has been open for more than %@ minutes. Would you like to close the visit.",timeLimit];
        UINavigationController* currentTopVC;
        currentTopVC=(UINavigationController*) [SWDefaults currentTopViewController];

        if (currentTopVC.presentedViewController) {
            
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Alert" andMessage:msg andActions:actionsArray withController:[currentTopVC visibleViewController]];

        }
        else{
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Alert" andMessage:msg andActions:actionsArray withController:[currentTopVC visibleViewController]];

        }
    }
    }
}


+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

+(void)UpdateGoogleAnalyticsforScreenName:(NSString*)screenName
{

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[[GAIDictionaryBuilder createScreenView] set:screenName
                                                         forKey:kGAIScreenName] build]];

}

+(void)updateDeviceValidationFailureDetailstoGoogleAnalytics:(NSMutableDictionary*)deviceDetailsDict
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Device Validation"];
//
//    [tracker set:[GAIFields customDimensionForIndex:1] value:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Customer_ID"]]];
//    [tracker set:[GAIFields customDimensionForIndex:2] value:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Device_ID"]]];
//    [tracker set:[GAIFields customDimensionForIndex:3] value:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Server_Name"]]];
//    [tracker set:[GAIFields customDimensionForIndex:4] value:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Server_Url"]]];
    
//    [tracker send:[[[GAIDictionaryBuilder createScreenView] set:@"Device Validation"
//                                                         forKey:kGAIScreenName] build]];
//    
//    [tracker send:[[[GAIDictionaryBuilder createScreenView] set:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Customer_ID"]]forKey:[GAIFields customDimensionForIndex:1]] build]];
//
//    [tracker send:[[[GAIDictionaryBuilder createScreenView] set:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Device_ID"]]forKey:[GAIFields customDimensionForIndex:2]] build]];
//
//    [tracker send:[[[GAIDictionaryBuilder createScreenView] set:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Server_Name"]]forKey:[GAIFields customDimensionForIndex:3]] build]];
//
//    [tracker send:[[[GAIDictionaryBuilder createScreenView] set:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Server_Url"]]forKey:[GAIFields customDimensionForIndex:4]] build]];

    
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Device_Validation_Failed"     // Event category (required)
                                                          action:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Server_Name"]]  // Event action
                                                           label:[SWDefaults getValidStringValue:[deviceDetailsDict valueForKey:@"Server_Url"]]          // Event label
                                                           value:nil] build]];    // Event value


}

+(void)updateGoogleAnalyticsEvent:(NSString*)eventName
{
    NSLog(@"updating event name %@",eventName);
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[SWDefaults getValidStringValue:eventName] action:@" " label:@"" value:nil]build]];

}

+(void)updateGoogleAnalyticsCustomEvents:(SalesWorxGoogleAnalytics*)swxGoogleAnalytics
{
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Barren Fields"
//                                                          action:@"Visited"
//                                                           label:@"Magic Tree"
//                                                           value:@1] build]];
   
    //[[GAI sharedInstance] trackerWithTrackingId:@"UA-52407698-2"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
   // [tracker send:[[GAIDictionaryBuilder createEventWithCategory:swxGoogleAnalytics.categoryName action:swxGoogleAnalytics.Action label:swxGoogleAnalytics.label value:[NSNumber numberWithInteger:swxGoogleAnalytics.value]] build]];
    
    
    [tracker set:kGAIUserId
           value:swxGoogleAnalytics.userID];
    
    // This hit will be sent with the User ID value and be visible in User-ID-enabled views (profiles).
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX"            // Event category (required)
                                                          action:@"User Sign In"  // Event action (required)
                                                           label:nil              // Event label
                                                           value:nil] build]];
}

+(NSString *)getCollectionImagesFolderPath
{
    // get the image path
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",KCollectionImagesFolderName]];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return documentsDirectory;
    
}
+(void)saveCollectionImage:(UIImage *)image  withName:(NSString *)ImageName
{
    NSString *documentsDirectory=[SWDefaults getCollectionImagesFolderPath];
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    
    NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",ImageName];
    NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
        if(NO == [fileManager removeItemAtPath:filename error:NULL]) {
        }
    }
    NSData *data=UIImageJPEGRepresentation(image, 1.0);
    NSLog(@"%lu",(unsigned long)data.length);
    
    float compressionQuality=0.3;
    // Now, save the image to a file.
    if(NO == [UIImageJPEGRepresentation(image,compressionQuality) writeToFile:filename atomically:YES]) {
        [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", filename];
    }
    else
    {
        NSLog(@"Unable to save Image");
    }
}
+(void)deleteCollectionTemporaryImages
{
    NSString *LPOImagesDirectory=[SWDefaults getCollectionImagesFolderPath];
    LPOImagesDirectory= [LPOImagesDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:LPOImagesDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    for (NSString *file in [fm contentsOfDirectoryAtPath:LPOImagesDirectory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", LPOImagesDirectory, file] error:&error];
        if (!success || error) {
            // it failed.
        }
        else
        {
            NSLog(@"deleted file %@",file);
        }
    }
}
+(void)copyFilesFromCollectionImagesTempFolder:(NSString*)path withPrefixName:(NSString *)orderId
{
    NSString *LPOImagesDirectory=[SWDefaults getCollectionImagesFolderPath];
    NSString *LPOImagesTempDirectory= [LPOImagesDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:LPOImagesTempDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    NSInteger Imagecount=0;
    for (NSString *file in [fm contentsOfDirectoryAtPath:LPOImagesTempDirectory error:&error]) {
        Imagecount++;
        [fm moveItemAtPath:[NSString stringWithFormat:@"%@/%@", LPOImagesTempDirectory, file]
                    toPath:[NSString stringWithFormat:@"%@/%@", LPOImagesDirectory, [NSString stringWithFormat:@"%@.jpg",orderId]]
                     error:&error];
        
    }
    
}



+(NSMutableArray*)fetchDatesofCurrentMonth
{
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"dd"]; // you can set this to whatever you like
    NSDate *today = [NSDate date]; // get todays date
    NSCalendar *cal = [NSCalendar currentCalendar]; // needed to work with components
    NSDateComponents *components = [cal components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:today];
    NSUInteger day = [components day];
    for (NSUInteger i=day; i>0; i--) {
        // loop through all days till down to 1
        [components setDay:i]; // update the day in the components
        NSDate *date = [cal dateFromComponents:components];
        [dates addObject:[fmt stringFromDate:date]]; // add the new date
    }
    if (dates.count>0) {
        
        [dates sortUsingSelector:@selector(compare:)];
        
    }
    return dates;
}



+(NSDate*)getCommunicationV2DefaultExpiryDate
{
    AppControl *appcontrol=[AppControl retrieveSingleton];

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.day = [appcontrol.MESSAGE_DEFAULT_EXPIRY_DAYS integerValue];
    NSDate *seventhDay = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    return seventhDay;
}
+(NSString *)convertDateToDeteWithZeroTimeFormat:(NSDate *)date
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    //NSDate* currentDate=inputDate;
    NSString * formattedDate=[dateFormatter stringFromDate:date];
    formattedDate=[formattedDate stringByAppendingString:@" 00:00:00"];
    
    return formattedDate;
}


#pragma mark productStockSyncRequoted
+(BOOL)isproductStockSyncRequired
{
    AppControl *appcontrol=[AppControl retrieveSingleton];
    
    if( [appcontrol.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode])
    {
        
            NSCalendar *sysCalendar = [NSCalendar currentCalendar];
            NSCalendarUnit unitFlags = NSCalendarUnitMinute;
            NSDateComponents *lastFullSyncTimeInfo = [sysCalendar components:unitFlags fromDate:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]]  toDate:[MedRepQueries convertNSStringtoNSDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]  options:0];
            
            
            BOOL isProductStockBackgroundSyncTimeIntervelExceeded=YES;
            
            if([SWDefaults lastProductStockBackGroundSyncDate]!=nil)
            {
                NSDateComponents *BS_TimeInfo = [sysCalendar components:unitFlags fromDate:[MedRepQueries convertNSStringtoNSDate:[SWDefaults lastProductStockBackGroundSyncDate]]  toDate:[MedRepQueries convertNSStringtoNSDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]  options:0];
                if([BS_TimeInfo minute]<[[[AppControl retrieveSingleton] FS_STOCK_SYNC_TIMER] integerValue])
                    isProductStockBackgroundSyncTimeIntervelExceeded=NO;
                
            }
            
            
            if([lastFullSyncTimeInfo minute] >= [[[AppControl retrieveSingleton] FS_STOCK_SYNC_TIMER] integerValue] && isProductStockBackgroundSyncTimeIntervelExceeded)
            {
                return YES;
                
            }
            else{
                NSLog(@"last prouct sync time less than the FS_STOCK_SYNC_TIMER");
                
            }
        
        }
    
    
    return NO;
}



+(BOOL)isCustomerLevelVisitOptionsFlagEnabled
{
    BOOL status=NO;
    NSString* custLevelFlag=[SWDefaults getValidStringValue:[[AppControl retrieveSingleton] valueForKey:@"ENABLE_CUST_LEVEL_TRX_RESTRICTION"]];
    if ([custLevelFlag isEqualToString:KAppControlsYESCode])
    {
        status=YES;
    }
    else{
        status=NO;
    }
    return status;
}

+(BOOL)isAgeingReportDisplayFormatMonthly
{
    NSString* ageingReportFlag=[[AppControl retrieveSingleton] valueForKey:@"AGEING_REP_DISP_FORMAT"];
    BOOL ageingReportDisplayMonthlyFormat=NO;
    if ([NSString isEmpty:ageingReportFlag]==NO) {
        
        if ([ageingReportFlag isEqualToString:@"MONTH_NAME"]) {
            ageingReportDisplayMonthlyFormat=YES;
        }
        else{
            ageingReportDisplayMonthlyFormat=NO;
        }
    }
    
    return ageingReportDisplayMonthlyFormat;
}

+(NSMutableArray*)fetchPreviousMonthsforAgeingReport
{
    NSMutableArray * ageingDataArray=[[NSMutableArray alloc]init];
    
    for (int i=0; i< 6; i++) {
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *comps = [NSDateComponents new];
        comps.month = - i;
        NSDate *date = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
        
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"MMMM - yyyy"];
        NSString *dateString = [df stringFromDate:date];
        [ageingDataArray addObject:[SWDefaults getValidStringValue:dateString]];
    }
    return ageingDataArray;
}

+(double )getConversionRateWithRespectToPrimaryUOMCodeForProduct:(Products *)pro
{
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
    NSPredicate *selecetdUOMPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.selectedUOM];
    
    NSMutableArray *UOMarray=[pro.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    NSMutableArray *selecetdUOMFilterArray=[[UOMarray filteredArrayUsingPredicate:selecetdUOMPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    ProductUOM *selectedUOMObj=[selecetdUOMFilterArray objectAtIndex:0];
    
    return [selectedUOMObj.Conversion doubleValue]/[primaryUOMObj.Conversion doubleValue];
}
+(double )getConversionRateWithRespectToSelectedUOMCodeForProduct:(Products *)pro
{
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
    NSPredicate *selecetdUOMPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.selectedUOM];
    
    NSMutableArray *UOMarray=[pro.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    NSMutableArray *selecetdUOMFilterArray=[[UOMarray filteredArrayUsingPredicate:selecetdUOMPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    ProductUOM *selectedUOMObj=[selecetdUOMFilterArray objectAtIndex:0];
    
    return  [primaryUOMObj.Conversion doubleValue]/[selectedUOMObj.Conversion doubleValue];
}
+(NSDecimalNumber *)getQuantityWithRespectToSelectedUOMCodeForProducrt:(Products *)pro PrimaryUOMQuanity:(NSDecimalNumber *)qty
{
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
    NSPredicate *selecetdUOMPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.selectedUOM];
    
    NSMutableArray *UOMarray=[pro.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    NSMutableArray *selecetdUOMFilterArray=[[UOMarray filteredArrayUsingPredicate:selecetdUOMPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    ProductUOM *selectedUOMObj=[selecetdUOMFilterArray objectAtIndex:0];
    
    NSDecimalNumber *OrderProductQtyInPrimaryUOM= [[qty decimalNumberByMultiplyingBy:[NSDecimalNumber ValidDecimalNumberWithString:primaryUOMObj.Conversion]]decimalNumberByDividingBy:[NSDecimalNumber ValidDecimalNumberWithString:selectedUOMObj.Conversion]];
    ;
    
    return OrderProductQtyInPrimaryUOM;
}
+(NSDecimalNumber *)getQuantityWithRespectToSelectedUOMCodeForProducrt:(Products *)pro SelectedUOMQuanity:(NSDecimalNumber *)qty
{
    NSPredicate *primaryUomPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.primaryUOM];
    NSPredicate *selecetdUOMPredicate=[NSPredicate predicateWithFormat:@"SELF.Item_UOM= [cd] %@",pro.selectedUOM];
    
    NSMutableArray *UOMarray=[pro.ProductUOMArray mutableCopy];
    NSMutableArray *primaryUOMfiltredArray=[[UOMarray filteredArrayUsingPredicate:primaryUomPredicate] mutableCopy];
    NSMutableArray *selecetdUOMFilterArray=[[UOMarray filteredArrayUsingPredicate:selecetdUOMPredicate] mutableCopy];
    
    ProductUOM *primaryUOMObj=[primaryUOMfiltredArray objectAtIndex:0];
    ProductUOM *selectedUOMObj=[selecetdUOMFilterArray objectAtIndex:0];
    
    NSDecimalNumber *OrderProductQtyInPrimaryUOM= [[qty decimalNumberByMultiplyingBy:[NSDecimalNumber ValidDecimalNumberWithString:selectedUOMObj.Conversion]]decimalNumberByDividingBy:[NSDecimalNumber ValidDecimalNumberWithString:primaryUOMObj.Conversion]];
    ;
    
    return OrderProductQtyInPrimaryUOM;
}

-(UIBarButtonItem *)CreateBarButtonItemWithTitle:(NSString *)title
{
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc] init];
    [barButtonItem setTitle:NSLocalizedString(title, nil)];
    barButtonItem.tintColor=[UIColor whiteColor];
    [barButtonItem setTitleTextAttributes:[MedRepDefaults fetchBarAttributes]
                                    forState:UIControlStateNormal];
    
    return barButtonItem;
}
-(void)AddBackButtonToViewcontroller:(UIViewController *)viewController
{
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSLog(@"language code %@",language);
    
    if([language isEqualToString:@"ar"])
    {
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:KBackButtonArrowImageName] style:UIBarButtonItemStylePlain target:viewController action:@selector(backButtonTapped:)];
        viewController.navigationItem.leftBarButtonItem = barButtonItem;
    }
    else
    {
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:KBackButtonArrowImageName] style:UIBarButtonItemStylePlain target:viewController action:@selector(backButtonTapped:)];
            viewController.navigationItem.leftBarButtonItem = barButtonItem;
    }

}


#pragma mark Device time validation methods

+(void)showDeviceTimeInvalidAlertinViewController:(UIViewController*)currentVC
{
    
    
    NSString *serverResponseDate=[SWDefaults fetchServerDate];
    
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * serverDate = [dateFormatter dateFromString:serverResponseDate];
    NSLog(@"temp date formatter date is %@", serverDate);
    
    
    
    NSString * serverDateString = [[NSString alloc]init];
    NSDateFormatter * dateStringFormatter = [[NSDateFormatter alloc] init];
    dateStringFormatter.dateFormat=@"yyyy-MM-dd HH:mm:ss";
    dateStringFormatter.timeZone=[NSTimeZone systemTimeZone];
    serverDateString=[dateStringFormatter stringFromDate:serverDate];
    
    
    NSDate* datetime = [NSDate date];
    NSString * deviceDateString = [dateStringFormatter stringFromDate:datetime];
    
    
    NSString * refinedServerDateString = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:serverDateString];
    
    NSString * refinedDeviceDateString = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:deviceDateString];
    
    NSLog(@"refined server date string is %@", refinedServerDateString);
    
    
    
    NSString * formattedString = [NSString stringWithFormat:@"Device date is : %@ \n Server date is : %@ \n please reset device date to continue",refinedDeviceDateString,refinedServerDateString ];
    
    
    
    [self showAlertAfterHidingKeyBoard:@"Invalid device time" andMessage:formattedString withController:currentVC];
    
}

+(NSString*)fetchServerDate
{
    [MBProgressHUD showHUDAddedTo:[SWDefaults currentTopViewController].view animated:YES];
    
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:kServerTimeURL]
                                                cachePolicy:NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:60.0];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:&error];
    if (error == nil)
    {
        NSString *serverResponseDate = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [MBProgressHUD hideAllHUDsForView:[SWDefaults currentTopViewController].view animated:YES];
        return serverResponseDate;
        
    }
    
    else{
        [MBProgressHUD hideAllHUDsForView:[SWDefaults currentTopViewController].view animated:YES];
        return  nil;
    }
}

+(BOOL)isDeviceTimeValid
{
    //this to validate whether user has changed device time.
    
    NSString * validateDeviceTimeFlag = [[AppControl retrieveSingleton]valueForKey:@"VALIDATE_DEVICE_TIME"];
    
    if ([validateDeviceTimeFlag isEqualToString:KAppControlsNOCode]) {
        
        return YES;
    }
    
    else
    {
        
        BOOL status = YES;
        
        //fetch server time
        
        NSString *serverResponseDate=[SWDefaults fetchServerDate];
        
        if ([NSString isEmpty:serverResponseDate]==NO)
        {
            // Parse data here
            NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate * serverDate = [dateFormatter dateFromString:serverResponseDate];
            NSLog(@"temp date formatter date is %@", serverDate);
            
            
            
            NSString * serverDateString = [[NSString alloc]init];
            NSDateFormatter * dateStringFormatter = [[NSDateFormatter alloc] init];
            dateStringFormatter.dateFormat=@"yyyy-MM-dd HH:mm:ss";
            dateStringFormatter.timeZone=[NSTimeZone systemTimeZone];
            serverDateString=[dateStringFormatter stringFromDate:serverDate];
            NSLog(@"server date string is %@", serverDateString);
            
            
            NSDate* datetime = [NSDate date];
            NSString* deviceTimeinUTC = [dateFormatter stringFromDate:datetime];
            NSDate * deviceDate =[dateFormatter dateFromString:deviceTimeinUTC];
            NSLog(@"device date is %@", deviceDate);
            NSString * deviceDateString = [dateStringFormatter stringFromDate:datetime];
            NSLog(@"device date string is %@", deviceDateString);
            
            
            
            NSTimeInterval differenceBetweenDatesinseconds = [deviceDate timeIntervalSinceDate:serverDate];
            NSLog(@"difference between dates in seconds %ld",(long)differenceBetweenDatesinseconds);
            
            
            NSInteger differenceBetweenDatesinMinutes = differenceBetweenDatesinseconds/60;
            NSLog(@"difference between dates in minutes %ld",(long)differenceBetweenDatesinMinutes);
            
            
            NSInteger allowedTimeRange = 0;
            
            NSLog(@"device validation range is %@",[[AppControl retrieveSingleton] valueForKey:@"DEVICE_TIME_VALIDATION_RANGE"]);
            
            
            allowedTimeRange = [[[AppControl retrieveSingleton] valueForKey:@"DEVICE_TIME_VALIDATION_RANGE"] integerValue];
            
            
            
            
            if (differenceBetweenDatesinMinutes > allowedTimeRange || differenceBetweenDatesinMinutes < allowedTimeRange*-1  ) {
                // [self showAlertAfterHidingKeyBoard:@"InValid Date" andMessage:@"Please reset device time to current time" withController:self];
                NSLog(@"*****DEVICE TIME IS INVALID*****");
                status = NO;
                
            }
            
        }
        else{
            NSLog(@"error while fetching server time during validating device time");
            status = YES;
            
        }
        
        
        return status;
    }
}




+(NSDictionary*)fetchHmSegmentControlSegmentTitleTextAttributes
{
    NSDictionary * attributesDict =@{NSForegroundColorAttributeName : MedRepMenuTitleUnSelectedCellTextColor ,NSFontAttributeName :kSWX_FONT_SEMI_BOLD(18)};
    return attributesDict;

    
    
}

+(NSDictionary*)fetchHmSegmentControlSelectedTitleTextAttributes
{
    NSDictionary * attributesDict = @{NSForegroundColorAttributeName : MedRepMenuTitleSelectedCellTextColor,NSFontAttributeName :kSWX_FONT_SEMI_BOLD(18)};
    return attributesDict;

}

+(NSDictionary*)fetchTitleTextAttributesforSegmentControl{
    
    NSDictionary * attributesDict = @{NSForegroundColorAttributeName : MedRepMenuTitleUnSelectedCellTextColor ,NSFontAttributeName :kSWX_FONT_SEMI_BOLD(14)};
    
    return attributesDict;
}

+(NSDictionary*)fetchSelectedTitleTextAttributesforSegmentControl
{
    NSDictionary * attributesDict = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName :kSWX_FONT_SEMI_BOLD(14)};
    return attributesDict;
}
+(BOOL)isDrApprovalAvailable
{
    BOOL status = NO;
    //if app control flag FM_ENABLE_DR_APPROVAL is Y then is_Approved is N
    NSString* doctorApproval= [NSString getValidStringValue:[AppControl retrieveSingleton].FM_ENABLE_DR_APPROVAL];
    if ([doctorApproval isEqualToString:KAppControlsYESCode])
    {
        status = YES;
    }
    return status;
}




+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    UIGraphicsBeginImageContext(newSize);
    //UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(BOOL)compressAndMoveImagestoThumbnailDirectory:(NSMutableArray*)imageNamesArray
{
    BOOL status=NO;
    
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        NSArray * filePathsArray = imageNamesArray;
        
        
        NSLog(@"compressing and moving images to thumbnail %@", filePathsArray);
        NSMutableArray * unCompressedImagesArray=[[NSMutableArray alloc]init];
        
        if (filePathsArray.count>0) {
            
            NSError * thumbnailError;
            NSString * thumbnailImageDirectoryPath =[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:thumbnailImageDirectoryPath]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:thumbnailImageDirectoryPath withIntermediateDirectories:NO attributes:nil error:&thumbnailError];
                NSLog(@"Thumbnail images directory created");
            }
            
            for (NSInteger i=0; i<filePathsArray.count; i++) {
                
                NSString* currentFileName = [filePathsArray objectAtIndex:i];
                
                NSString* currentFilePath =[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentFileName];
                
                
                
                UIImage* savedImage = [UIImage imageWithContentsOfFile:currentFilePath];
                
                //145X109
                UIImage * resizedImage=[SWDefaults imageResize:savedImage andResizeTo:CGSizeMake(400, 300)];
                
                NSData *imageData = UIImageJPEGRepresentation(resizedImage, 0.7);
                
                NSLog(@"writing thumbnail file to path %@",[thumbnailImageDirectoryPath stringByAppendingPathComponent:currentFileName]);
                
                
                bool thumbanailSaved= [imageData writeToFile:[thumbnailImageDirectoryPath stringByAppendingPathComponent:currentFileName] atomically:NO];
                
                NSLog(@"thumbnail saved status %hhd",thumbanailSaved);
                
                
            }
        }
        
        NSLog(@"uncompressed images path %@", unCompressedImagesArray);
        
        
        
    }];
    
    return status;
}
-(NSDictionary *)getDummyMedrepVisitLocationFromPhxTblLocationTable
{
    AppControl *appcontrol=[AppControl retrieveSingleton];

    if([appcontrol.ENABLE_MEDREP_NO_VISIT_OPTION isEqualToString:KAppControlsYESCode]){
        NSMutableArray *dbResponseArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from PHX_TBL_Locations where Location_ID='%@'",appcontrol.MEDREP_NO_VISIT_LOCATION]];
        NSLog(@"Dummy medrep location %@",dbResponseArray);

        if(dbResponseArray.count>0)
            return [dbResponseArray objectAtIndex:0];
    }
    NSLog(@"Dummy medrep location not available");
    return [[NSDictionary alloc] init];
}
+(NSString *)getCustomerLastVisitedDateIfDateNotExistReturnDefaultEmptyKey:(Customer *)selectedCustomer{

 NSString *lastVisitedString=[[NSString alloc] init];


    @try {
        NSMutableArray *arrlastVisitedOn = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select Visit_End_Date from TBL_FSR_Actual_Visits where Customer_ID = '%@' and Site_Use_ID = '%@' ORDER BY Visit_End_Date DESC LIMIT 1",selectedCustomer.Ship_Customer_ID, selectedCustomer.Ship_Site_Use_ID]];
        if ([arrlastVisitedOn count] == 0) {
            NSMutableArray *lastVisitDate = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_1,'N/A') AS Visit_Date from TBL_Customer_Addl_Info where Attrib_Name ='EXT' and Customer_ID='%@' and Site_Use_ID = '%@' AND Custom_Attribute_1 IS NOT NULL",selectedCustomer.Ship_Customer_ID, selectedCustomer.Ship_Site_Use_ID]];
            if ([lastVisitDate count] == 0) {
                lastVisitedString = KNotApplicable;
            }
            else
            {
                lastVisitedString = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:[[lastVisitDate objectAtIndex:0] valueForKey:@"Visit_Date"]];
            }
        }
        else
        {
            if ([[[arrlastVisitedOn objectAtIndex:0] valueForKey:@"Visit_End_Date"] isEqualToString:@""]) {
                
                NSMutableArray *lastVisitDate = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_1,'N/A') AS Visit_Date from TBL_Customer_Addl_Info where Attrib_Name ='EXT' and Customer_ID='%@' and Site_Use_ID = '%@'",selectedCustomer.Ship_Customer_ID, selectedCustomer.Ship_Site_Use_ID]];
                if ([lastVisitDate count] == 0) {
                    lastVisitedString = KNotApplicable;
                }
                else
                {
                    lastVisitedString = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:[[lastVisitDate objectAtIndex:0] valueForKey:@"Visit_Date"]];
                }
            }
            else{
                lastVisitedString = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:[[arrlastVisitedOn objectAtIndex:0] valueForKey:@"Visit_End_Date"]];
            }
        }
    } @catch (NSException *exception) {
        lastVisitedString = KNotApplicable;
    }
    
    return lastVisitedString;
}
+(NSDate *)beginningOfDay:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    return [calendar dateFromComponents:components];
}


#pragma Mark Collection Receipt Image Methods

+(NSString *)getCollectionReceiptImagesFolderPath
{
    // get the image path
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kCollectionReceiptFolderName]];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return documentsDirectory;
    
}
+(void)saveCollectionReceiptImage:(UIImage *)image  withName:(NSString *)ImageName
{
    NSString *documentsDirectory=[SWDefaults getCollectionReceiptImagesFolderPath];
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    
    NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",ImageName];
    NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
        if(NO == [fileManager removeItemAtPath:filename error:NULL]) {
        }
    }
    NSData *data=UIImageJPEGRepresentation(image, 1.0);
    NSLog(@"%lu",(unsigned long)data.length);
    
    float compressionQuality=0.3;
    // Now, save the image to a file.
    if(NO == [UIImageJPEGRepresentation(image,compressionQuality) writeToFile:filename atomically:YES]) {
        [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", filename];
    }
    else
    {
        NSLog(@"Unable to save Image");
    }
}

+(void)deleteCollectionReceiptTemporaryImages
{
    NSString *LPOImagesDirectory=[SWDefaults getCollectionReceiptImagesFolderPath];
    LPOImagesDirectory= [LPOImagesDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:LPOImagesDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    for (NSString *file in [fm contentsOfDirectoryAtPath:LPOImagesDirectory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", LPOImagesDirectory, file] error:&error];
        if (!success || error) {
            // it failed.
        }
        else
        {
            NSLog(@"deleted file %@",file);
        }
    }
}



+(void)copyFilesFromCollectionReceiptImagesTempFolder:(NSString*)path withPrefixName:(NSString *)orderId
{
    NSString *LPOImagesDirectory=[SWDefaults getCollectionReceiptImagesFolderPath];
    NSString *LPOImagesTempDirectory= [LPOImagesDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:LPOImagesTempDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    NSInteger Imagecount=0;
    for (NSString *file in [fm contentsOfDirectoryAtPath:LPOImagesTempDirectory error:&error]) {
        Imagecount++;
        [fm moveItemAtPath:[NSString stringWithFormat:@"%@/%@", LPOImagesTempDirectory, file]
                    toPath:[NSString stringWithFormat:@"%@/%@", LPOImagesDirectory, [NSString stringWithFormat:@"%@.jpg",orderId]]
                     error:&error];
        
    }
    
}


-(UINavigationController *)getPresenationControllerWithNavigationController:(UIViewController *)delegeteController WithRootViewController:(UIViewController *)RootViewController WithSourceView:(UIView *)sourceView WithSourceRect:(CGRect)sourcerect WithBarButtonItem:(UIBarButtonItem *)barButtonItem WithPopOverContentSize:(CGSize )PopOverContentSize{
    
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:RootViewController];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    //  popover.containerView=PopOverContentSize;
    popover.delegate   = delegeteController;
    if(barButtonItem==nil){
        popover.sourceView = sourceView;
        popover.sourceRect = sourcerect;
    }else{
        popover.barButtonItem=barButtonItem;
    }
    
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    return nav;
}
+(void)ShowToastMessage:(NSString *)message
{
    
    [KSToastView ks_showToast:message duration:2.0f completion: ^{
    }];
}

//merchandising survey pictures

+(NSString *)getMerchandisingSurveyMarkingPicturesFolderPath
{
    // get the image path
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",KMerchandisingSurveyPicturesFolderName]];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return documentsDirectory;
    
}

+(void)saveMerchandisingSurveyMarkingPictures:(UIImage *)image  withName:(NSString *)ImageName
{
    NSString *documentsDirectory = [SWDefaults getMerchandisingSurveyMarkingPicturesFolderPath];
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    
    NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",ImageName];
    NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
        if(NO == [fileManager removeItemAtPath:filename error:NULL]) {
        }
    }
    NSData *data=UIImageJPEGRepresentation(image, 1.0);
    NSLog(@"%lu",(unsigned long)data.length);
    
    float compressionQuality=0.3;
    // Now, save the image to a file.
    if(NO == [UIImageJPEGRepresentation(image,compressionQuality) writeToFile:filename atomically:YES]) {
        [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", filename];
    }
    else
    {
        NSLog(@"Image saved");
    }
}


//coach survey signature images

+(NSString *)getCoachSurveySignatureFolderPath
{
    // get the image path
    NSString *documentsDirectory=[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]];
    
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",KCoachSurveySignatureFolderName]];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return documentsDirectory;
    
}

+(void)saveCoachSurveySignatureImages:(UIImage *)image  withName:(NSString *)ImageName
{
    NSString *documentsDirectory = [SWDefaults getCoachSurveySignatureFolderPath];
    documentsDirectory= [documentsDirectory stringByAppendingPathComponent:@"/temp"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    
    NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",ImageName];
    NSString *filename=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
        if(NO == [fileManager removeItemAtPath:filename error:NULL]) {
        }
    }
    NSData *data=UIImageJPEGRepresentation(image, 1.0);
    NSLog(@"%lu",(unsigned long)data.length);
    
    float compressionQuality=0.3;
    // Now, save the image to a file.
    if(NO == [UIImageJPEGRepresentation(image,compressionQuality) writeToFile:filename atomically:YES]) {
        [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", filename];
    }
    else
    {
        NSLog(@"Image saved");
    }
}

#pragma mark Merchandising Competitor info directory
+(void)saveMerchandisingCompetitorImages:(UIImage*)capturedImage withImageName:(NSString*)imageName
{
    NSString* directoryPath=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingCompetitorInfoImages] stringByAppendingPathComponent:@"temp"];
    NSError *error;
    
    if (![[NSFileManager defaultManager]fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",imageName];
    NSString *filename=[directoryPath stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
        if(NO == [fileManager removeItemAtPath:filename error:NULL]) {
        }
    }
    NSData *data=UIImageJPEGRepresentation(capturedImage, 1.0);
    NSLog(@"%lu",(unsigned long)data.length);
    
    float compressionQuality=0.3;
//    NSData *imageData = UIImageJPEGRepresentation(capturedImage, compressionQuality);
//    
//    [imageData writeToFile:filename atomically:YES];

    // Now, save the image to a file.
    if(NO == [UIImageJPEGRepresentation(capturedImage,compressionQuality) writeToFile:filename atomically:YES]) {
        [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", filename];
    }
    else
    {
        NSLog(@"Image saved");
    }

}

#pragma mark Planogram  info directory
+(void)savePlanogramImages:(UIImage*)capturedImage withImageName:(NSString*)imageName
{
    NSString* directoryPath=[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kMerchandisingPlanogramImages] stringByAppendingPathComponent:@"temp"];
    NSError *error;
    
    if (![[NSFileManager defaultManager]fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",imageName];
    NSString *filename=[directoryPath stringByAppendingPathComponent:documentPathComponent];
    
    
    // make sure the file is removed if it exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:filename]) {
        if(NO == [fileManager removeItemAtPath:filename error:NULL]) {
        }
    }
    NSData *data=UIImageJPEGRepresentation(capturedImage, 1.0);
    NSLog(@"%lu",(unsigned long)data.length);
    
    float compressionQuality=0.3;
    
    // Now, save the image to a file.
    if(NO == [UIImageJPEGRepresentation(capturedImage,compressionQuality) writeToFile:filename atomically:YES]) {
        [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", filename];
    }
    else
    {
        NSLog(@"Planogram Image saved");
    }
    
}
+(UINavigationController *)CreatePoPoverPresentationNavigationControllerWithViewController:(UIViewController *)Controller AndSourceView:(UIView *)sourceView AndSourceRect:(CGRect )sourcerect AndPerimittedArrowDirections:(UIPopoverArrowDirection)Arrowdirection AndDelegate:(UIViewController<UIPopoverPresentationControllerDelegate>*)delegateController{
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:Controller];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popover.delegate = delegateController;
    popover.sourceView = sourceView;
    popover.sourceRect = sourcerect;
    popover.permittedArrowDirections = Arrowdirection;
    return nav;
}

+(UIColor *)averageColorOfImage:(UIImage*)image{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), image.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}
+(UIColor *)getContrastColorForImage:(UIImage*)image{
   UIColor * averageColor =   [SWDefaults averageColorOfImage:image];
    const CGFloat * colorComponents = CGColorGetComponents(averageColor.CGColor);
    return [UIColor colorWithRed:(1-colorComponents[0])
                           green:colorComponents[1]
                            blue:(1-colorComponents[2])
                           alpha:colorComponents[3]];
}
@end





@implementation CustomerPaymentClass

@synthesize customerId,customerName,amountGoingToPaid,invoicesArray,settledAmount,totalAmount,currencyType,paymentMethod,bankBranchName,bankName,chequeDate,chequeNumber,currencyRate,customerPaidAmountAfterConversion,customerPaidAmount,customerDueAmount;

@end


@implementation CustomerPaymentInvoiceClass

@synthesize previousSettledAmount,invoiceNumber,dueDate,InvoiceDate,dueNetAmount,settledAmountForInvoice;

@end


@implementation CustomerClass

    @synthesize customerName,customerNumber,isCashCustomer,customerTradeChannel,customerId;
@end





