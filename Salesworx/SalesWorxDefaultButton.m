//
//  SalesWorxButton.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDefaultButton.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@implementation SalesWorxDefaultButton
@synthesize EnableImageRTLSupport,EnableTitleRTLSupport;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{

//if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) {
//    self.imageView.image = [self.imageView.image imageFlippedForRightToLeftLayoutDirection];
//}
    self.titleLabel.font=kSWX_FONT_SEMI_BOLD(14);
    if(EnableTitleRTLSupport)
    {
        [super setTitle:NSLocalizedString(self.titleLabel.text, nil) forState:UIControlStateNormal];
    }
    if(EnableImageRTLSupport)
    {
        if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) {
            [super setImage:super.imageView.image.imageFlippedForRightToLeftLayoutDirection forState:UIControlStateNormal];
        }
    }
}


- (void)setTitle:(nullable NSString *)title forState:(UIControlState)state                     // default is nil. title is assumed to be single line
{
    if(EnableTitleRTLSupport)
    {
        [super setTitle:NSLocalizedString(title, nil) forState:state];
    }
    else
    {
        [super setTitle:title forState:state];
    }
}
- (void)setImage:(UIImage *)image forState:(UIControlState)state                     // default is nil. title is assumed to be single line
{
    if(EnableImageRTLSupport)
    {
        if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) {
        [super setImage:image.imageFlippedForRightToLeftLayoutDirection forState:UIControlStateNormal];
        }
    }
    else
    {
        [super setImage:image forState:state];
    }

}


@end
