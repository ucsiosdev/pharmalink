//
//  SalesWorxDefaultTextView.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 8/3/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDefaultTextView.h"

@implementation SalesWorxDefaultTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    
    if(self.EnableRTLTranslation)
    {
        if(_isTextEndingWithExtraSpecialCharacter && super.text.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[super.text characterAtIndex:super.text.length-1]];
            NSString *nString=[super.text stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(super.text, nil);
            
        }
    }
    
    if(self.EnableRTLTextAlignment && [SWDefaults isRTL])
    {
        
        super.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        super.textAlignment=NSTextAlignmentLeft;
    }
}

-(NSString*)text{
    return super.text;
}


-(void)setText:(NSString*)newText {
    
    if(self.EnableRTLTranslation)
    {
        if(_isTextEndingWithExtraSpecialCharacter && newText.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[newText characterAtIndex:newText.length-1]];
            NSString *nString=[newText stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(newText, nil);
            
        }
    }
    else
        super.text = newText;
    
    
}

@end
