//
//  SalesWorxReportsViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/7/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesSummaryViewController.h"
#import "BlockedCustomerViewController.h"
#import "StatementReportViewController.h"

@interface SalesWorxReportsViewController : UIViewController
{
    NSMutableArray * reportsArray;
}
@property (strong, nonatomic) IBOutlet UICollectionView *reportsCollectionView;
@property(strong,nonatomic) SalesWorxVisit* currentVisit;

@end
