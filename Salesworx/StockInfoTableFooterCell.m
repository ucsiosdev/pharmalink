//
//  StockInfoTableFooterCell.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/4/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import "StockInfoTableFooterCell.h"

@implementation StockInfoTableFooterCell
@synthesize avblStockLbl,orderQtyLbl,allocatedQtyLbl,avblStockStatic,orderQtyStatic,allocatedQtyStatic;
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
