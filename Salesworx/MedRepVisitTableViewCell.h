//
//  MedRepVisitTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 2/28/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepView.h"
#import "SalesWorxImageView.h"
#import "MedRepUpdatedDesignCell.h"

@interface MedRepVisitTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *cellDividerImg;
@property (strong, nonatomic) IBOutlet SalesWorxImageView *accessoryImageView;
@property (nonatomic) BOOL isCellSelected;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet MedRepView *statusView;
@property  StausViewType cellStatusViewType;

-(void)setSelectedCellStatus;
-(void)setDeSelectedCellStatus;


@end
