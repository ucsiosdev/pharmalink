//
//  SWCustomerInvoicePopOverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/14/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWCustomerInvoicePopOverViewController.h"
#import "MedRepDefaults.h"
@interface SWCustomerInvoicePopOverViewController ()

@end

@implementation SWCustomerInvoicePopOverViewController

@synthesize orderDateLbl,orderNumberLbl,orderNumber,orderDate;

- (void)viewDidLoad {
    [super viewDidLoad];
    orderNumberLbl.text=orderNumber;
    
    orderDateLbl.text=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd hh:mm:ss" destFormat:kDateFormatWithoutTime scrString:orderDate];
    
    
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)closeTapped:(id)sender
{
    [self.invoicePopOver dismissPopoverAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
