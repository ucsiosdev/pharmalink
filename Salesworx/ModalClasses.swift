//
//  ModalClasses.swift
//  MedRep
//
//  Created by Unique Computer Systems on 10/6/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import Foundation


class Visit_History {
    
    var Contact_ID : String = ""
    var DoctorName : String = ""
    var Doctor_ID : String = ""
    var EDetail : Int = 0
    var EmpNotes : String = ""
    var Location : String = ""
    var LocationType : String = ""
    var Location_ID : String  = ""
    var NextObjective : String = ""
    var Samples : Int = 0
    var Tasks : String = ""
    var TimeSpent : Int = 0
    var VisitDate : String  = ""
    var VisitNote : String = ""
    var Visit_ID : String = ""
    
    init(contactID:String,doctorName:String,doctorID:String,eDetail:Int,empNotes:String,location:String,locationType:String,locationID:String,nextVisitObjective:String,samples:Int,tasks:String,timeSpent:Int,visitDate:String,visitNote:String,visitID:String){
        
        self.Contact_ID = contactID
        self.DoctorName = doctorName
        self.Doctor_ID = doctorID
        self.EDetail = eDetail
        self.EmpNotes = empNotes
        self.Location = location
        self.LocationType = locationType
        self.Location_ID = locationID
        self.NextObjective = nextVisitObjective
        self.Samples = samples
        self.Tasks = tasks
        self.TimeSpent = timeSpent
        self.VisitDate = visitDate
        self.VisitNote = visitNote
        self.Visit_ID = visitID
    }
    
}
