//
//  MedRepVisitsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "MedRepCustomClass.h"
#import "MedRepCreateLocationViewController.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepVisitsDateFilterViewController.h"
#import "MedRepTextView.h"
#import "MedRepCreatePharmacyContactViewController.h"
#import "MedRepVisitsStartMultipleCallsViewController.h"
#import "SalesWorxDefaultSegmentedControl.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "MedRepCalanderwithTimeViewController.h"


@protocol StartVisitDelegate <NSObject>
-(void)startVisit:(BOOL)Value;
-(void)newVisitCreated:(BOOL)value StartVisit:(BOOL)isVisitWithNewParameters AndPlannedVisitId:(NSString *)Planned_Visit_ID;
-(void)VisitUpdated:(BOOL)value;
-(void)createVisitDidCancel;
@end


@interface MedRepVisitsViewController : SWViewController<UITableViewDataSource,UITableViewDelegate,StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,UITextViewDelegate,addLocationProtocol,UIAlertViewDelegate,SelectedFilterDelegate,VisitDatePickerDelegate,CreateContactDelegate,MedRepCalenderDelegate>

{
    AppControl *appCntrl;
    StringPopOverViewController_ReturnRow* locationPopOver;
    IBOutlet UIButton *doctorButton;
    BOOL demoPlanSpecializationMatched;
    IBOutlet UIButton *demoPlanButton;
    IBOutlet UIButton *visitTypeButton;
    IBOutlet UIButton *dateButton;
    IBOutlet UIButton *locationButton;
    NSString* plannedVisitIDfromCreateVisit;
    
    
    IBOutlet UISwitch *pendingVisitSwitch;
    IBOutlet MedRepElementTitleLabel *pendingVisitLabel;
    BOOL existingDoctorChanged;
    
    UIPopoverController *valuePopOverController,*doctorpopOverController,*locationpopoverController,*datePickerpopOverController,*demonstrationPlanpopOverController,*visitTypepopOverController;
    
    NSString* popUpString;
    
    NSMutableArray* locationsarray,*doctorsArray,*refinedDoctorsArray,*demoPlanArray,*plannedVisitsArray,*visitTypeArray;
    
    UIDatePicker* datepicker;
    
    BOOL isNewLocation;
    
    id dismissDelegate;
    
    
    IBOutlet UIView *objectiveTxtBgView;
    NSString* locationIDforExistingLocationNewDoc, * locationIDforNewLocExistingDoc, *doctorIDforNewLocationExistingDoctor;
    
    
    NSMutableDictionary* newLocationDataDict,*newDrDataDict;
    
    
    NSMutableArray* pharmacyContactDetailsArray,*pharmacyContactNamesArray;
    
    NSString* locationType;
    
    
    CGRect oldFrame;
    
    BOOL isFutureVisit;
    
    NSMutableDictionary* locationDetailsDictionary;
    
    BOOL isNewDoctor;
    
    BOOL isNewContact;
    
    BOOL isPendingVisitEnabled;
    IBOutlet MedRepElementTitleLabel *doctorContactTitle;
    NSIndexPath *locationLastselectedIndexPath;
    
    NSString* cretedDoctorID;

    NSString* selectedLocationID;

    
    
    IBOutlet MedRepTextField *locationTextField;
    IBOutlet MedRepTextField *doctorTextField;
    IBOutlet MedRepTextField *dateTextField;
    IBOutlet MedRepTextField *visitTypeTextField;
    IBOutlet MedRepTextField *demoPlanTextField;

    
    NSMutableArray* doctorDetailsArray;
    NSMutableArray*pharmacyDetailsArray;
    NSMutableArray* tempContactsArray;
    
    NSMutableArray* arrOfDoctorsWhenPendingVisitsEnabled;
    NSMutableArray* arrOfPharmacyWhenPendingVisitsEnabled;
    NSMutableArray* tempArrayWhenPendingVisitsEnabled;
    
    BOOL noContactAvailableforLoction;
    
    
    /** below bools for showing the incosistance data messages on editing visit*/
    BOOL showVisitTypeNotAvailableAlertMessage;
    IBOutlet UIButton *AddLocationButton;
    IBOutlet SalesWorxDefaultSegmentedControl *NovisitSegmgmentedControl;
    IBOutlet MedRepElementTitleLabel *objectiveTextViewHeadingLabel;
    IBOutlet MedRepElementTitleLabel *visitTypeTextfieldHeadingLabel;
    
    NSMutableArray *MedrepNoVisitsReasonDBArray;
    
    NSString* objectiveWordLimit;
}
- (IBAction)saveVisitTapped:(id)sender;
- (IBAction)nextButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *visitTypeLbl;

@property(strong,nonatomic)     UIPopoverController* popOverController;

- (IBAction)cancelVisitTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *locationLbl;

@property(nonatomic) id dismissDelegate;

@property (strong, nonatomic) IBOutlet UILabel *doctorLbl;

@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@property (strong, nonatomic) IBOutlet UILabel *demonstrationPlan;

@property (strong, nonatomic) IBOutlet MedRepTextView *objectiveTextView;

- (IBAction)locationBtnTapped:(id)sender;

- (IBAction)doctorBtnTapped:(id)sender;

- (IBAction)dateandTimeBtnTapped:(id)sender;

- (IBAction)demonstrationPlanBtnTapped:(id)sender;

- (IBAction)addLocationTapped:(id)sender;

- (IBAction)addDoctorTapped:(id)sender;

@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *textViewScrollView;

@property (strong, nonatomic) IBOutlet UIButton *doctorPickerBtn;

@property (strong, nonatomic) IBOutlet UIButton *datePickerBtn;

@property (strong, nonatomic) IBOutlet UIButton *addDoctorBtn;

@property(strong,nonatomic)NSMutableDictionary * visitDetailsDict;

@property(strong,nonatomic) UINavigationController * createVisitNavigationController;
@property (strong, nonatomic) IBOutlet UIButton *dateFieldButton;
@property BOOL isUpdatingVisit;
@property MedRepVisitEditType  currentVisitEditType;

@property (strong, nonatomic) NSDictionary * selectedEditVisitDetails;


-(void)selectedValueforCreateVisit:(NSIndexPath *)indexPath;
- (IBAction)NoVisitSegmentedControlValueChanged:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xPendingVisitsLabelTopMarginConstraint;

@end
