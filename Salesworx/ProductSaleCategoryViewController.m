//
//  ProductSaleCategoryViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/12/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductSaleCategoryViewController.h"
#import "SalesOrderViewController.h"
#import "SWDuesView.h"
#import "ReturnViewController.h"
#import "TemplateViewController.h"
#import "SalesOrderNewViewController.h"
#import "AlSeerSalesOrderViewController.h"
#import "AlSeerSalesOrderViewController.h"

@interface ProductSaleCategoryViewController ()

@end

@implementation ProductSaleCategoryViewController
//@synthesize serProduct;
@synthesize target;
@synthesize action;



- (id)initWithCustomer:(NSDictionary *)c {
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) {
        
        customer= [NSDictionary dictionaryWithDictionary:c];
        [self setTitle:NSLocalizedString(@"Product Category & Warehouse", nil)];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)] ];
    }
    
    return self;
}
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor whiteColor];
    [Flurry logEvent:@"Product Category and Warehouse  View"];
    //[Crittercism leaveBreadcrumb:@"<Product Category and Wharehouse View>"];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    isFromVisit = YES;
    
    //iOS 9 UITableView cell frame getting reset issue
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        
        //in ios 9 tableview cell frame getting changed this will fix it
        self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
        
    }

  
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [SWDefaults clearProductCategory];
    //serProduct.delegate=self;
   
    
    
    
    
    
    
    [self getProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
    
   
    
    
    
    
//    OrderAdditionalInfoViewController * orderAddlInfoVC=[[OrderAdditionalInfoViewController alloc]init];
//    
//  
//    orderAddlInfoVC.warehouseDataArray=[items mutableCopy];
//    
//    NSLog(@"array after pushing %@", [orderAddlInfoVC.warehouseDataArray description]);
//    
//    
//    NSLog(@"items desc in did appear %@", [items description]);
    
    
}
- (void)cancel:(id)sender{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
}
#pragma mark UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
    AppControl *appControl = [AppControl retrieveSingleton];
    if ([appControl.ENABLE_TEMPLATES isEqualToString:@"Y"]) {
        return 2;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section{
    if(section==0)
    {
        return items.count;
    }
    else
    {
        return totalOrderArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    @autoreleasepool
//    {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            
        }
    if(indexPath.section==0)
        {
            [cell.textLabel setText:[[items objectAtIndex:indexPath.row] stringForKey:@"Item_No"]];
            [cell.detailTextLabel setText:[[items objectAtIndex:indexPath.row] stringForKey:@"Description"]];
            
            
            
        }
        else
        {
            [cell.textLabel setText:[[totalOrderArray objectAtIndex:indexPath.row] stringForKey:@"Template_Name"]];
            [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ - %@",[[totalOrderArray objectAtIndex:indexPath.row]  stringForKey:@"category_Name"],[[totalOrderArray objectAtIndex:indexPath.row]  stringForKey:@"Custom_Attribute_2"] ]];
        }
    
    
    //getting ware house data
//    NSMutableString * labelText;
//    NSMutableString * detailedLabelText;
//    for (int i=0;i<[items count]; i++) {
//        labelText=[[items objectAtIndex:i]valueForKey:@"Item_No"];
//        detailedLabelText=[[items objectAtIndex:i]valueForKey:@"Description"];
//        
//    
//    }
   
    
    
        return cell;
//    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.font = BoldSemiFontOfSize(14);
    cell.detailTextLabel.font = BoldSemiFontOfSize(12);
    cell.textLabel.textColor = UIColorFromRGB(0x4A5866);
    cell.detailTextLabel.textColor = UIColorFromRGB(0x4687281);
    
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section{
    if(section==0)
    {
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:NSLocalizedString(@"Please select the appropriate category and warehouse combination for which you wish to take an order", nil)] ;
        return sectionHeader;
    }
    else
    {
        if(totalOrderArray.count!=0)
        {
          GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:@"Select Order Templates"] ;
            return sectionHeader;
        }
        else
        {
            return nil;
        }
    }

}

- (CGFloat)tableView:(UITableView *)tv heightForHeaderInSection:(NSInteger)section {
    return 48.0f;
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NSUserDefaults standardUserDefaults]setInteger:indexPath.row forKey:@"SelectedIndex"];
    Singleton *single = [Singleton retrieveSingleton];

    NSString* warehouseString=[[items objectAtIndex:indexPath.row] valueForKey:@"Org_ID"];
    
    single.selectedWarehouse=warehouseString;
    
    
    single.orderStartDate = [NSDate date];
    if(indexPath.section==0)
    {
       Singleton *single = [Singleton retrieveSingleton];
        
        NSString* warehouseString=[[items objectAtIndex:indexPath.row] valueForKey:@"Org_ID"];
        
        single.selectedWarehouse=warehouseString;
        
        if ([single.visitParentView isEqualToString:@"SO"])
        {
            [SWDefaults setProductCategory:[items objectAtIndex:indexPath.row]];
            
            
            NSLog(@"IS MULTI UOM IN CELL FOR ROW %@", [SWDefaults checkMultiUOM]);
            
//            if ([[SWDefaults checkTgtField]isEqualToString:@"Y"]) {
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {

//            
//                AlSeerSalesOrderViewController *newSalesOrder =[[AlSeerSalesOrderViewController alloc] initWithNibName:@"AlSeerSalesOrderViewController" bundle:nil];
//                
//                
//                newSalesOrder.wareHouseDataSalesOrderArray=[items mutableCopy];
//                
//                newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//                newSalesOrder.parentViewString = @"SO";
//                newSalesOrder.preOrderedItems=[NSMutableArray new];
//                newSalesOrder.preOrdered = [NSMutableDictionary new];
//                newSalesOrder.isOrderConfirmed=@"";
//                [self.navigationController pushViewController:newSalesOrder animated:YES];
//                
//                //salesOrderViewController = [[SalesOrderViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
//                //[self.navigationController pushViewController:salesOrderViewController animated:YES];
//                //salesOrderViewController = nil;
//                /***/
//                newSalesOrder = nil;
                
                
                SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
                
                
                newSalesOrder.wareHouseDataSalesOrderArray=[items mutableCopy];
                
                newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
                newSalesOrder.parentViewString = @"SO";
                newSalesOrder.preOrderedItems=[NSMutableArray new];
                newSalesOrder.preOrdered = [NSMutableDictionary new];
                newSalesOrder.isOrderConfirmed=@"";
                [self.navigationController pushViewController:newSalesOrder animated:YES];
                
                //salesOrderViewController = [[SalesOrderViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
                //[self.navigationController pushViewController:salesOrderViewController animated:YES];
                //salesOrderViewController = nil;
                /***/
                newSalesOrder = nil;
                
            }
            else
            {
                
                
                SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
                
                
                newSalesOrder.wareHouseDataSalesOrderArray=[items mutableCopy];
                
                newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
                newSalesOrder.parentViewString = @"SO";
                newSalesOrder.preOrderedItems=[NSMutableArray new];
                newSalesOrder.preOrdered = [NSMutableDictionary new];
                newSalesOrder.isOrderConfirmed=@"";
                [self.navigationController pushViewController:newSalesOrder animated:YES];
                
                //salesOrderViewController = [[SalesOrderViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
                //[self.navigationController pushViewController:salesOrderViewController animated:YES];
                //salesOrderViewController = nil;
                /***/
                newSalesOrder = nil;
                
            }
            
    
            
            
        }
        else if ([single.visitParentView isEqualToString:@"MO"])
        {
            [SWDefaults setProductCategory:[items objectAtIndex:indexPath.row]];
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:[items objectAtIndex:indexPath.row]];
            #pragma clang diagnostic pop
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
        }
        else if ([single.visitParentView isEqualToString:@"RO"])
        {
            [SWDefaults setProductCategory:[items objectAtIndex:indexPath.row]];
            ReturnViewController *returnView = [[ReturnViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
            [self.navigationController pushViewController:returnView animated:YES];
            returnView = nil;
        }
    }
    else
    {
        indexPathTemplate = indexPath.row;
        
        //salesSer.delegate=self;
        NSLog(@"order in old template %@",totalOrderArray);
        
        [self getSalesOrderServiceDidGetTemplateItem:[[SWDatabaseManager retrieveManager] dbGetTemplateOrderItem:[[totalOrderArray objectAtIndex:indexPath.row] stringForKey:@"Order_Template_ID"]]];
    }

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [tableView beginUpdates];

        [[SWDatabaseManager retrieveManager] deleteTemplateOrderWithRef:[[totalOrderArray objectAtIndex:indexPath.row] stringForKey:@"Order_Template_ID"]];
        [totalOrderArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationLeft];
        [tableView endUpdates];
    }
    [tableView reloadData];
}
#pragma mark Product Service Delegate
- (void)getProductServiceDidGetCategories:(NSArray *)categories {
    items= [NSMutableArray arrayWithArray:categories];

    [self getSalesOrderServiceDidGetTemplate:[[SWDatabaseManager retrieveManager] dbGetTemplateOrder]];
    categories=nil;

}
- (void)getSalesOrderServiceDidGetTemplate:(NSArray *)templates{
   Singleton *single = [Singleton retrieveSingleton];
    totalOrderArray = [NSMutableArray arrayWithArray:templates];
    //single.savedOrderTemplateArray = [NSMutableArray array];;
    NSArray *categoryArray=nil;
    if (templates.count!=0)
    {
       categoryArray =  [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager]dbGetAllCategory]];
    }
    for(int i = 0; i < templates.count; i++)
    {
        [single.savedOrderTemplateArray addObject:[[templates objectAtIndex:i] stringForKey:@"Template_Name"]];
        for(int j = 0; j < categoryArray.count; j++)
        {     
            if([[[templates objectAtIndex:i] stringForKey:@"Custom_Attribute_1"] isEqualToString:[[categoryArray objectAtIndex:j] stringForKey:@"category_Code"]])
            {
                [[totalOrderArray objectAtIndex:i] setValue:[[categoryArray objectAtIndex:j] stringForKey:@"category_Name"] forKey:@"category_Name"];
            }
        }
    }
    [self.tableView reloadData];

    if (items.count==1 && totalOrderArray.count==0 && isFromVisit)
    {
        isFromVisit=NO;
       Singleton *single = [Singleton retrieveSingleton];
        single.orderStartDate = [NSDate date];
        
        NSString* warehouseString=[[items objectAtIndex:0] valueForKey:@"Org_ID"];
        
        single.selectedWarehouse=warehouseString;
        
        if ([single.visitParentView isEqualToString:@"SO"])
            {
                [SWDefaults setProductCategory:[items objectAtIndex:0]];
                
                
                //replacing Sale order screen for Al Seer
                
//                if ([[SWDefaults checkTgtField]isEqualToString:@"Y"]) {
                if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {

                    
                    
                    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
                    
                    
                    //pushing to sales order new
                    
                    newSalesOrder.wareHouseDataSalesOrderArray=[items mutableCopy];
                    
                    
                    NSLog(@"items being pushed to new sales order %@", [items description]);
                    
                    // NSLog(@"items being pushed to sales order %@", [salesOrderViewController.wareHouseDataSalesOrder description]);
                    
                    
                    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
                    newSalesOrder.parentViewString = @"SO";
                    newSalesOrder.preOrderedItems=[NSMutableArray new];
                    newSalesOrder.preOrdered = [NSMutableDictionary new];
                    newSalesOrder.isOrderConfirmed=@"";
                    [self.navigationController pushViewController:newSalesOrder animated:YES];
                    
                    //salesOrderViewController = [[SalesOrderViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
                    //[self.navigationController pushViewController:salesOrderViewController animated:YES];
                    //salesOrderViewController = nil;
                    /**/
                    newSalesOrder = nil;

                    
//                    AlSeerSalesOrderViewController *newSalesOrder =[[AlSeerSalesOrderViewController alloc] initWithNibName:@"AlSeerSalesOrderViewController" bundle:nil];
//                    
//                    
//                    newSalesOrder.wareHouseDataSalesOrderArray=[items mutableCopy];
//                    
//                    
//                    NSLog(@"items being pushed to new sales order %@", [items description]);
//                    
//                    // NSLog(@"items being pushed to sales order %@", [salesOrderViewController.wareHouseDataSalesOrder description]);
//                    
//                    
//                    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//                    newSalesOrder.parentViewString = @"SO";
//                    newSalesOrder.preOrderedItems=[NSMutableArray new];
//                    newSalesOrder.preOrdered = [NSMutableDictionary new];
//                    newSalesOrder.isOrderConfirmed=@"";
//                    [self.navigationController pushViewController:newSalesOrder animated:YES];
//                    
//                    //salesOrderViewController = [[SalesOrderViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
//                    //[self.navigationController pushViewController:salesOrderViewController animated:YES];
//                    //salesOrderViewController = nil;
//                    /**/
//                    newSalesOrder = nil;

                    
                    
                    
                    
//                    
//                    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
//                    
//                    
//                    //pushing to sales order new
//                    
//                    newSalesOrder.wareHouseDataSalesOrderArray=[items mutableCopy];
//                    
//                    
//                    NSLog(@"items being pushed to new sales order %@", [items description]);
//                    
//                    // NSLog(@"items being pushed to sales order %@", [salesOrderViewController.wareHouseDataSalesOrder description]);
//                    
//                    
//                    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//                    newSalesOrder.parentViewString = @"SO";
//                    newSalesOrder.preOrderedItems=[NSMutableArray new];
//                    newSalesOrder.preOrdered = [NSMutableDictionary new];
//                    newSalesOrder.isOrderConfirmed=@"";
//                    [self.navigationController pushViewController:newSalesOrder animated:YES];
//                    
//                    //salesOrderViewController = [[SalesOrderViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
//                    //[self.navigationController pushViewController:salesOrderViewController animated:YES];
//                    //salesOrderViewController = nil;
//                    /**/
//                    newSalesOrder = nil;

                    
                }
                
                else
                {
                    
                    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
                    
                    
                    //pushing to sales order new
                    
                    newSalesOrder.wareHouseDataSalesOrderArray=[items mutableCopy];
                    
                    
                    NSLog(@"items being pushed to new sales order %@", [items description]);
                    
                    // NSLog(@"items being pushed to sales order %@", [salesOrderViewController.wareHouseDataSalesOrder description]);
                    
                    
                    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
                    newSalesOrder.parentViewString = @"SO";
                    newSalesOrder.preOrderedItems=[NSMutableArray new];
                    newSalesOrder.preOrdered = [NSMutableDictionary new];
                    newSalesOrder.isOrderConfirmed=@"";
                    [self.navigationController pushViewController:newSalesOrder animated:YES];
                    
                    //salesOrderViewController = [[SalesOrderViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:indexPath.row]] ;
                    //[self.navigationController pushViewController:salesOrderViewController animated:YES];
                    //salesOrderViewController = nil;
                    /**/
                    newSalesOrder = nil;
                    
                }
                
                
                
              
            }
            else if ([single.visitParentView isEqualToString:@"MO"])
            {
                [SWDefaults setProductCategory:[items objectAtIndex:0]];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action withObject:[items objectAtIndex:0]];
#pragma clang diagnostic pop
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
            }
            else if ([single.visitParentView isEqualToString:@"RO"])
            {
                [SWDefaults setProductCategory:[items objectAtIndex:0]];
                ReturnViewController *returnView = [[ReturnViewController alloc] initWithCustomer:customer andCategory:[items objectAtIndex:0]] ;
                [self.navigationController pushViewController:returnView animated:YES];
                returnView = nil;
            }
        }
    else
    {
//        [self.tableView reloadData];
//        templates=nil;
        
    }

}
- (void)getSalesOrderServiceDidGetTemplateItem:(NSArray *)templateItems{
    //NSMutableArray *bonusArray = [NSMutableArray array];
    
    NSLog(@"old template items are %@", templateItems);
    
    NSMutableArray *templateItemsArray = [NSMutableArray array];

    for(int i = 0; i < templateItems.count; i++)
    {
        NSMutableArray *bonusArray = [NSMutableArray arrayWithArray:[[[SWDatabaseManager retrieveManager] dbGetBonusInfoOfProduct:[[templateItems objectAtIndex:i] stringForKey:@"Item_Code"]] mutableCopy]];
        int quantity = 0;
        quantity = [[[templateItems objectAtIndex:i] objectForKey:@"Quantity"] intValue];
        int bonus = 0;
        if([bonusArray count]!=0)
        {
            for(int j=0 ; j<[bonusArray count] ; j++ )
            {
                NSDictionary *row = [bonusArray objectAtIndex:j];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    NSMutableDictionary *bonusProduct;
                    if(j == [bonusArray count]-1)
                    {
                        if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                        {
                            int dividedValue = quantity / rangeStart ;
                            bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                        
                            NSString *childGUID = [NSString createGuid];
                            
                            bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            [bonusProduct setValue:[[templateItems objectAtIndex:i] stringForKey:@"Template_Line_ID"] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [[templateItems objectAtIndex:i] setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [[templateItems objectAtIndex:i] setValue:@"N"   forKey:@"priceFlag"];
                            [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Template_Line_ID"] forKey:@"Guid"];
                            [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Quantity"] forKey:@"Qty"];
                            int qty = [[[templateItems objectAtIndex:i] stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[[templateItems objectAtIndex:i] objectForKey:@"List_Price"] doubleValue]);
                            [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [[templateItems objectAtIndex:i] setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:[templateItems objectAtIndex:i]];
                            [templateItemsArray addObject:bonusProduct];
                        }
                    
                        else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                        {
                            int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                            bonus = floor(dividedValue) ;
                           
                            NSString *childGUID = [NSString createGuid];

                            bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            [bonusProduct setValue:[[templateItems objectAtIndex:i] stringForKey:@"Template_Line_ID"] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [[templateItems objectAtIndex:i] setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [[templateItems objectAtIndex:i] setValue:@"N"   forKey:@"priceFlag"];
                            [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Template_Line_ID"] forKey:@"Guid"];
                            [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Quantity"] forKey:@"Qty"];
                            int qty = [[[templateItems objectAtIndex:i] stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[[templateItems objectAtIndex:i] objectForKey:@"List_Price"] doubleValue]);
                            [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [[templateItems objectAtIndex:i] setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:[templateItems objectAtIndex:i]];
                            [templateItemsArray addObject:bonusProduct];
                        }
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue];
                        NSString *childGUID = [NSString createGuid];
                        
                        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                        [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                        [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                        [bonusProduct setValue:childGUID forKey:@"Guid"];
                        [bonusProduct setValue:[[templateItems objectAtIndex:i] stringForKey:@"Template_Line_ID"] forKey:@"Guid"];
                        [bonusProduct setValue:@"0" forKey:@"Price"];
                        [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                        
                        [[templateItems objectAtIndex:i] setValue:bonusProduct   forKey:@"Bonus_Product"];
                        [[templateItems objectAtIndex:i] setValue:@"N"   forKey:@"priceFlag"];
                        [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Template_Line_ID"] forKey:@"Guid"];
                        [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Quantity"] forKey:@"Qty"];
                        int qty = [[[templateItems objectAtIndex:i] stringForKey:@"Qty"] intValue];
                        double totalPrice = (double)qty * ([[[templateItems objectAtIndex:i] objectForKey:@"List_Price"] doubleValue]);
                        [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                          [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",[[[templateItems objectAtIndex:i] objectForKey:@"List_Price"] doubleValue]] forKey:@"Net_Price"];
                        [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                        [[templateItems objectAtIndex:i] setValue:childGUID forKey:@"ChildGuid"];
                        [templateItemsArray addObject:[templateItems objectAtIndex:i]];
                        [templateItemsArray addObject:bonusProduct];
                    }
                }
            }
        }
        else
        {
            [[templateItems objectAtIndex:i] setValue:@"N"   forKey:@"priceFlag"];
            [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Template_Line_ID"] forKey:@"Guid"];
            [[templateItems objectAtIndex:i] setValue:[[templateItems objectAtIndex:i] stringForKey:@"Quantity"] forKey:@"Qty"];
            int qty = [[[templateItems objectAtIndex:i] stringForKey:@"Qty"] intValue];
            double totalPrice = (double)qty * ([[[templateItems objectAtIndex:i] objectForKey:@"List_Price"] doubleValue]);
            [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",[[[templateItems objectAtIndex:i] objectForKey:@"List_Price"] doubleValue]] forKey:@"Net_Price"];
            [[templateItems objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
            [templateItemsArray addObject:[templateItems objectAtIndex:i]];
        }
    }
    
    NSString *savedCategoryID = [[totalOrderArray objectAtIndex:indexPathTemplate] stringForKey:@"Custom_Attribute_1"];
    [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer] andCategoryName:savedCategoryID andOrderDictionary:[totalOrderArray objectAtIndex:indexPathTemplate] andOrderArray:templateItemsArray];

}
- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict andOrderArray:(NSArray *)array
{
    //categoryArray=categories;
    
    NSLog(@"categories from db are %@", categories);
    
    
    NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:categories];
    NSMutableDictionary *category= [NSMutableDictionary new];
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID]) {
            category=[row mutableCopy];
            
            break;
        }
    }
    [SWDefaults setProductCategory:category];
    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    newSalesOrder.parentViewString = @"TO";
    newSalesOrder.preOrderedItems=[NSMutableArray arrayWithArray:array];
    newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
    newSalesOrder.isOrderConfirmed=@"";
    [self.navigationController pushViewController:newSalesOrder animated:YES];
    newSalesOrder = nil;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
