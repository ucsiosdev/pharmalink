//
//  SWPopoverController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 1/24/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SWPopoverController.h"
#import "SWDefaults.h"
@interface SWPopoverController ()

@end

@implementation SWPopoverController
@synthesize headerTitle,productDescriptionTextView,longDescription;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(80.0/255.0) green:(142.0/255.0) blue:(202.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    UIBarButtonItem * closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    closeButton.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=closeButton;
    
    self.title=headerTitle;
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil]];

    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
  
    [productDescriptionTextView setTextContainerInset:UIEdgeInsetsMake(10, 10, 10, 10)];

    
    if (longDescription.length>0) {
        
        
        productDescriptionTextView.text=longDescription;
        
    }
    
}

-(void)closeTapped
{

    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"close button tapped");
}

-(void)viewDidAppear:(BOOL)animated
{
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
