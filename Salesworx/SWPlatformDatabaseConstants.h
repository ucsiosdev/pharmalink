//
//  SWPlatformDatabaseConstants.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#ifndef SWPlatform_SWPlatformDatabaseConstants_h
#define SWPlatform_SWPlatformDatabaseConstants_h

#define kSQLLogin   @"SELECT * FROM TBL_User WHERE Username = '%@' and Password = '%@'"

#define kSQLCustomerListAll   @"SELECT * FROM TBL_Customer ORDER BY Customer_Name Where "
#define kSQLCashCustomerListAll @"SELECT * FROM TBL_Customer Where Cash_Cust = 'Y' ORDER BY Customer_Name " 
//@"SELECT *  FROM TBL_Product_MSL M  INNER JOIN TBL_Product A ON M.Inventory_Item_ID=A.Inventory_Item_ID AND M.Organization_ID=A.Organization_ID ORDER BY Item_Code ASC"
//TBL_Customer_Ship_Address

#define kSQLCustomerListNonAlpha @"SELECT A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, B.Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM %@ AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  %@ ORDER BY A.Customer_Name"

//#define kSQLCustomerListAlphabet @"SELECT A.*,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, B.Phone, B.Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, B.Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM %@ AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  WHERE A.Customer_Name LIKE '%@%%' %@ ORDER BY A.Customer_Name"

#define kSQLRoute @"select C.Customer_ID,C.Site_Use_ID,C.Customer_No,IFNULL(C.Customer_Name,'N') AS Customer_Name,IFNULL(C.Contact,'N') AS Contact,IFNULL(C.Address,'N/A') AS Address,IFNULL(C.City,'N/A') AS City,IFNULL(C.Phone,'N/A') AS Phone,IFNULL(C.Credit_Limit,'N/A') AS Credit_Limit ,IFNULL(C.Credit_Hold,'N') AS Credit_Hold  ,IFNULL(C.Location,'') AS Location,IFNULL(C.Cust_Status,'N') AS Cust_Status,IFNULL(C.Customer_Barcode,'') AS Customer_Barcode,IFNULL(C.Customer_OD_Status,'N') AS Customer_OD_Status,IFNULL(C.Cash_Cust,'N') AS Cash_Cust,IFNULL(C.Price_List_ID,'') AS Price_List_ID,IFNULL(C.Avail_Bal,'') AS Avail_Bal,IFNULL(C.Bill_Credit_Period,'') AS Bill_Credit_Period,IFNULL(C.Allow_FOC,'N') AS Allow_FOC,IFNULL(C.Customer_Type,'') AS Customer_Type,IFNULL(C.Customer_Class,'') AS Customer_Class,IFNULL(C.Trade_Classification,'') AS Trade_Classification,IFNULL(C.Chain_Customer_Code,'') AS Chain_Customer_Code,C.Creation_Date,A.FSR_Plan_Detail_ID,A.Planned_Visit_ID , A.Visit_Date, A.Start_Time AS Visit_Start_Time, A.End_Time AS Visit_End_Time,A.Visit_Status, B.Customer_Name,B.Customer_No, B.Cust_Lat, B.Cust_Long,B.Customer_ID AS [Ship_Customer_ID] , B.Site_Use_ID AS [Ship_Site_Use_ID] FROM  TBL_FSR_Planned_Visits AS A INNER JOIN TBL_Customer_Ship_Address  AS B ON A.Customer_ID = B.Customer_ID  AND A.Site_Use_ID=B.Site_Use_ID INNER JOIN TBL_Customer AS C ON C.Customer_ID = B.Customer_ID WHERE A.Visit_Date LIKE  '%%%@%%' ORDER BY A.Start_Time ASC , C.Customer_Name ASC"


//#define kSQLRoute @" SELECT TBL_Customer_Ship_Address.Cust_Lat,TBL_Customer_Ship_Address.Cust_Long,TBL_Customer_Ship_Address.Cust_Status,TBL_Customer_Ship_Address.Customer_Name,TBL_Customer.city,TBL_Customer.Cash_Cust,TBL_FSR_Planned_Visits.Customer_ID,TBL_FSR_Planned_Visits.FSR_Plan_Detail_ID,TBL_FSR_Planned_Visits.Start_Time,TBL_FSR_Planned_Visits.Visit_Status,TBL_FSR_Planned_Visits.Customer_IDfromTBL_Customer_Ship_Address,TBL_Customer,TBL_FSR_Planned_Visits where TBL_Customer_Ship_Address.Customer_ID=TBL_Customer.Customer_ID and TBL_Customer.Customer_ID=TBL_FSR_Planned_Visits.Customer_ID  WHERE TBL_FSR_Planned_Visits.Visit_Date LIKE "


#define kSQLProductDetailwithRanking @"SELECT A.Inventory_Item_ID,A.Category, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code As [UOM],A.Discount, p.SPECIAL_DISCOUNT,A.Net_Price As Unit_Selling_Price, A.List_Price As Unit_List_Price, C.Lot_Qty, C.Expiry_Date, A.Control_1,P.Attrib_Value As [Long_Description], P.Custom_Attribute_4 As [Ranking] FROM TBL_Product As A LEFT JOIN (SELECT Item_ID, SUM(Lot_Qty) As [Lot_Qty], MIN(Expiry_Date) As [Expiry_Date] FROM TBL_Product_Stock GROUP BY Item_ID) As C ON A.Inventory_Item_ID=C.Item_ID LEFT JOIN TBL_Product_Addl_Info As P ON A.Inventory_Item_ID=P.Inventory_Item_ID AND A.Organization_ID=P.Organization_ID AND P.Attrib_Name='LD+RANK' WHERE A.Inventory_Item_ID='%@' AND A.Organization_ID='%@'"





//#define kSQLProductListfromCategorywithRanking @"SELECT IFNULL(REMOVE_IN_FOC_LIST,'N') AS REMOVE_IN_FOC_LIST, A.Promo_Item,IFNULL(RESTRICTED,'N') AS RESTRICTED, IFNULL(C.bonus,'0') AS Bonus,A.Item_Code,A.Brand_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price,B.Lot_Qty, B.Expiry_Date, A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode,A.Category FROM TBL_Product As A LEFT JOIN (Select Attrib_Value as RESTRICTED,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RESTRICTED') AS I on A.Inventory_item_ID= I.Inventory_item_ID  and A.Organization_ID=I.Organization_ID  LEFT JOIN (Select Attrib_Value as REMOVE_IN_FOC_LIST,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='REMOVE_IN_FOC_LIST') AS J on A.Inventory_item_ID= J.Inventory_item_ID  and A.Organization_ID=J.Organization_ID    LEFT JOIN (SELECT Item_ID, SUM(Lot_Qty) As [Lot_Qty], MIN(Expiry_Date) As [Expiry_Date] FROM TBL_Product_Stock WHERE Org_ID='%@' GROUP BY Item_ID) As B ON A.Inventory_Item_ID=B.Item_ID LEFT JOIN (SELECT IFNULL(SUM(Get_Qty),'N/A')AS bonus,Item_Code FROM TBL_BNS_Promotion GROUP BY Item_Code) As C ON C.Item_Code=A.Item_Code LEFT JOIN TBL_Product_Addl_Info As P ON A.Inventory_Item_ID=P.Inventory_Item_ID AND A.Organization_ID=P.Organization_ID AND P.Attrib_Name='LD+RANK' WHERE A.Category='%@'  ORDER BY P.Custom_Attribute_4, A.Description"

#define kSQLProductListfromCategorywithRanking @"SELECT IFNULL(RESTRICTED_FOR_RETURN,'N')AS RESTRICTED_FOR_RETURN,IFNULL(REMOVE_IN_FOC_LIST,'N') AS REMOVE_IN_FOC_LIST, A.Promo_Item,IFNULL(RESTRICTED,'N') AS RESTRICTED, IFNULL(C.bonus,'0') AS Bonus,A.Item_Code,A.Brand_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price,B.Lot_Qty, B.Expiry_Date, A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode,A.Category FROM TBL_Product As A LEFT JOIN (Select Attrib_Value as RESTRICTED,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RESTRICTED') AS I on A.Inventory_item_ID= I.Inventory_item_ID  and A.Organization_ID=I.Organization_ID  LEFT JOIN (Select Attrib_Value as REMOVE_IN_FOC_LIST,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='REMOVE_IN_FOC_LIST') AS J on A.Inventory_item_ID= J.Inventory_item_ID  and A.Organization_ID=J.Organization_ID  LEFT JOIN(Select Attrib_Value AS RESTRICTED_FOR_RETURN,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RET_MODE') AS K on A.Inventory_item_ID= K.Inventory_item_ID  and A.Organization_ID=K.Organization_ID   LEFT JOIN (SELECT Item_ID, SUM(Lot_Qty) As [Lot_Qty], MIN(Expiry_Date) As [Expiry_Date] FROM TBL_Product_Stock WHERE Org_ID='%@' GROUP BY Item_ID) As B ON A.Inventory_Item_ID=B.Item_ID LEFT JOIN (SELECT IFNULL(SUM(Get_Qty),'N/A')AS bonus,Item_Code FROM TBL_BNS_Promotion GROUP BY Item_Code) As C ON C.Item_Code=A.Item_Code LEFT JOIN TBL_Product_Addl_Info As P ON A.Inventory_Item_ID=P.Inventory_Item_ID AND A.Organization_ID=P.Organization_ID AND P.Attrib_Name='LD+RANK' WHERE A.Category='%@'  ORDER BY P.Custom_Attribute_4, A.Description"


//#define kSQLProductListfromCategorywithRankingAndStockWithAllwareHouses @"SELECT  IFNULL(REMOVE_IN_FOC_LIST,'N') AS REMOVE_IN_FOC_LIST, A.Promo_Item,IFNULL(RESTRICTED,'N') AS RESTRICTED ,IFNULL(C.bonus,'0') AS Bonus,A.Item_Code,A.Brand_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price,B.Lot_Qty, B.Expiry_Date, A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode,A.Category FROM TBL_Product As A LEFT JOIN (Select Attrib_Value as RESTRICTED,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RESTRICTED') AS I on A.Inventory_item_ID= I.Inventory_item_ID  and A.Organization_ID=I.Organization_ID LEFT JOIN (Select Attrib_Value as REMOVE_IN_FOC_LIST,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='REMOVE_IN_FOC_LIST') AS J on A.Inventory_item_ID= J.Inventory_item_ID  and A.Organization_ID=J.Organization_ID LEFT JOIN (SELECT Item_ID, SUM(Lot_Qty) As [Lot_Qty], MIN(Expiry_Date) As [Expiry_Date] FROM TBL_Product_Stock GROUP BY Item_ID) As B ON A.Inventory_Item_ID=B.Item_ID LEFT JOIN (SELECT IFNULL(SUM(Get_Qty),'N/A')AS bonus,Item_Code FROM TBL_BNS_Promotion GROUP BY Item_Code) As C ON C.Item_Code=A.Item_Code LEFT JOIN TBL_Product_Addl_Info As P ON A.Inventory_Item_ID=P.Inventory_Item_ID AND A.Organization_ID=P.Organization_ID AND P.Attrib_Name='LD+RANK' WHERE A.Category='%@'  ORDER BY P.Custom_Attribute_4, A.Description"

#define kSQLProductListfromCategorywithRankingAndStockWithAllwareHouses @"SELECT  IFNULL(RESTRICTED_FOR_RETURN,'N')AS RESTRICTED_FOR_RETURN,IFNULL(REMOVE_IN_FOC_LIST,'N') AS REMOVE_IN_FOC_LIST, A.Promo_Item,IFNULL(RESTRICTED,'N') AS RESTRICTED ,IFNULL(C.bonus,'0') AS Bonus,A.Item_Code,A.Brand_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price,B.Lot_Qty, B.Expiry_Date, A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode,A.Category FROM TBL_Product As A LEFT JOIN (Select Attrib_Value as RESTRICTED,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RESTRICTED') AS I on A.Inventory_item_ID= I.Inventory_item_ID  and A.Organization_ID=I.Organization_ID LEFT JOIN (Select Attrib_Value as REMOVE_IN_FOC_LIST,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='REMOVE_IN_FOC_LIST') AS J on A.Inventory_item_ID= J.Inventory_item_ID  and A.Organization_ID=J.Organization_ID LEFT JOIN(Select Attrib_Value AS RESTRICTED_FOR_RETURN,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RET_MODE') AS K on A.Inventory_item_ID= K.Inventory_item_ID  and A.Organization_ID=K.Organization_ID LEFT JOIN (SELECT Item_ID, SUM(Lot_Qty) As [Lot_Qty], MIN(Expiry_Date) As [Expiry_Date] FROM TBL_Product_Stock GROUP BY Item_ID) As B ON A.Inventory_Item_ID=B.Item_ID LEFT JOIN (SELECT IFNULL(SUM(Get_Qty),'N/A')AS bonus,Item_Code FROM TBL_BNS_Promotion GROUP BY Item_Code) As C ON C.Item_Code=A.Item_Code LEFT JOIN TBL_Product_Addl_Info As P ON A.Inventory_Item_ID=P.Inventory_Item_ID AND A.Organization_ID=P.Organization_ID AND P.Attrib_Name='LD+RANK' WHERE A.Category='%@'  ORDER BY P.Custom_Attribute_4, A.Description"



//#define kSQLCUstomerLocationFilter @"SELECT City FROM TBL_Customer GROUP BY City"

#define kSQLCUstomerLocationFilter @"SELECT IFNULL(City,'N/A') AS City FROM TBL_Customer GROUP BY City"

#define kSQLCustomerTargetGeneral @"SELECT DISTINCT B.Item_No As [Category], A.Target_Value,  A.Custom_Value_1 As [FSR_Target],   A.Custom_Value_2 As [Sales_Value],    0.00 As [Achievement]    FROM TBL_Sales_Target_Items As A INNER JOIN TBL_Product As B ON      A.Classification_1=B.Agency WHERE A.Classification_2='%@' AND A.Month=%d"


#define kSQLCustomerTargetAlseer @"SELECT DISTINCT  Classification_1 As [Category], Target_Value,  Custom_Value_1 As [FSR_Target],   Sales_Value As [Sales_Value],    0.00 As [Achievement]    FROM TBL_Sales_Target_Items WHERE Classification_2='%@' AND Month=%d"


#define kSQLCustomerOrderHistory @"SELECT A.Orig_Sys_Document_Ref,IFNULL(A.Shipping_Instructions,'N/A') AS Shipping_Instructions, A.Creation_Date,A.End_Time, A.Transaction_Amt, CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status], B.Customer_Name ,(SELECT C.Username FROM TBL_User AS C WHERE C.SalesRep_ID=A.Created_By) As [FSR] FROM TBL_Order_History As A INNER JOIN TBL_Customer_Ship_Address As B ON B.Customer_ID=A.Ship_To_Customer_ID AND B.Site_Use_ID=A.Ship_To_Site_ID WHERE A.Doc_Type='I' AND B.Customer_No = '%@' ORDER BY A.Orig_Sys_Document_Ref DESC"
/////////////////

#define kSQLCustomerPriceList @"SELECT C.Description , B.Item_UOM , B.Unit_Selling_Price,B.Unit_List_Price,B.Is_Generic, C.Item_Code FROM TBL_Customer AS A INNER JOIN TBL_Price_List AS B ON A.Price_List_ID = B.Price_List_ID AND A.Site_Use_ID = B.Organization_ID INNER JOIN TBL_Product AS C ON B.Inventory_Item_ID = C.Inventory_Item_ID WHERE  A.Customer_No = '%@' %@"


//#define  kSQLCustomerPriceListGeneric @"SELECT C.Description , B.Item_UOM , C.Brand_Code,B.Unit_Selling_Price,B.Unit_List_Price,B.Is_Generic, C.Item_Code FROM TBL_Customer AS A  , TBL_Price_List AS B INNER JOIN TBL_Product AS C ON B.Inventory_Item_ID = C.Inventory_Item_ID WHERE  A.Customer_No = '%@' AND B.Is_Generic = 'Y' %@"

#define  kSQLCustomerPriceListGeneric @"select A.inventory_item_id as inventory_item_id, A.Item_UOM as Item_UOM, A.Unit_Selling_Price as Unit_Selling_Price, A.Unit_List_Price as Unit_List_Price, A.Is_Generic as Is_Generic, B.Brand_Code as Brand_Code, B.Description as Description, B.Item_Code as Item_Code from TBL_Price_List As A inner join TBL_Product AS B on A.Inventory_Item_ID = B.Inventory_Item_ID where A.is_generic = 'Y' and A.inventory_item_id is not (select  A.inventory_item_id from TBL_Price_List As A inner join TBL_Product AS B on A.Inventory_Item_ID = B.Inventory_Item_ID where price_list_id  = (select price_list_id from tbl_customer where Customer_No ='%@') ) UNION select A.inventory_item_id as inventory_item_id, A.Item_UOM as Item_UOM, A.Unit_Selling_Price as Unit_Selling_Price, A.Unit_List_Price as Unit_List_Price, A.Is_Generic as Is_Generic, B.Brand_Code as Brand_Code, B.Description as Description, B.Item_Code as Item_Code from TBL_Price_List As A inner join TBL_Product AS B on A.Inventory_Item_ID = B.Inventory_Item_ID where price_list_id  = (select price_list_id from tbl_customer where Customer_No ='%@') and B.Category In (select Product_Category from TBL_Customer_Stock_Map where Customer_ID='%@' AND Site_Use_ID='%@')"


//added control_1 to this list. OLA!!
#define kSQLProductDetail @"SELECT A.Inventory_Item_ID,A.Category, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code As [UOM], A.Discount, A.Net_Price As Unit_Selling_Price, A.List_Price As Unit_List_Price, SUM(C.Lot_Qty) As Lot_Qty, MIN(C.Expiry_Date) As Expiry_Date , A.Control_1 FROM TBL_Product As A LEFT JOIN TBL_Product_Stock As C ON A.Inventory_Item_ID=C.Item_ID WHERE A.Inventory_Item_ID=%@ AND A.Organization_ID=%@ GROUP BY A.Inventory_Item_ID, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code, A.Discount, A.Net_Price, A.List_Price"



#define kSQLProductDetailwithUOM @"SELECT A.Inventory_Item_ID,A.Category, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code As [UOM], A.Discount, A.Net_Price As Unit_Selling_Price, A.List_Price As Unit_List_Price, SUM(C.Lot_Qty) As Lot_Qty, MIN(C.Expiry_Date) As Expiry_Date , A.Control_1 FROM TBL_Product As A LEFT JOIN TBL_Product_Stock As C ON A.Inventory_Item_ID=C.Item_ID WHERE A.Inventory_Item_ID=%@ AND A.Organization_ID=%@ GROUP BY A.Inventory_Item_ID, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code, A.Discount, A.Net_Price, A.List_Price"

////////////////



//#define kSQLCollectionInvoices @"SELECT CAST('0' AS Bit) AS Selected, C.Invoice_Ref_No, C.Pending_Amount AS NetAmount,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate, C.Due_Date as DueDate,C.Customer_ID  FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID   LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No   WHERE (C.Customer_ID='$CUSTOMERID') AND (C.Site_Use_ID='$SITEID')   UNION ALL  SELECT CAST('0' AS Bit) AS Selected, B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount,   CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate,  B.Due_Date as DueDate,B.Ship_To_Customer_ID AS Customer_ID  FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = B.Ship_To_Customer_ID AND  B.Ship_To_Site_ID = N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No   WHERE (B.ERP_Status = 'X') AND (B.Ship_To_Customer_ID='$CUSTOMERID') AND (B.Ship_To_Site_ID='$SITEID') AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID='$CUSTOMERID' AND Site_Use_ID='$SITEID') ORDER BY InvDate ASC"


//changing this from inner to left
#define kSQLOrderInvoiceItems @"SELECT A.Item_Value,A.Inventory_Item_ID, B.Description, A.Order_Quantity_UOM,  A.Ordered_Quantity, A.Calc_Price_Flag, A.Unit_Selling_Price, B.Item_Code FROM TBL_Order_History_Line_Items As A LEFT JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

//changing this from inner to left in some cases tbl product might not have an entry matching to order history line items

#define kSQLOrderInvoice @"SELECT A.ERP_Ref_No,A.Inventory_Item_ID, B.Description,A.Expiry_Date, A.Order_Quantity_UOM,  A.Ordered_Quantity, A.Calc_Price_Flag, A.Unit_Selling_Price,A.Item_Value, B.Item_Code FROM TBL_Sales_History_Line_Items As A LEFT JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

#define kSQLOrderAllInvoices @"SELECT CAST('0' AS Bit) AS Selected, C.Invoice_Ref_No AS Invoice_Ref_No, C.Pending_Amount AS NetAmount,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate, C.Due_Date as DueDate,C.Customer_ID AS Customer_ID FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID   LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No   WHERE (C.Customer_ID={0})  UNION ALL  SELECT CAST('0' AS Bit) AS Selected, B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount,   CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate,  B.Due_Date as DueDate,B.Inv_To_Customer_ID AS Customer_ID  FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = B.Inv_To_Customer_ID AND  B.Inv_To_Site_ID = N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No   WHERE (B.ERP_Status = 'X') AND (B.Inv_To_Customer_ID={0}) AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID={0}) ORDER BY InvDate ASC"

#define kSQLCollectionInsertInfo @"INSERT INTO TBL_Collection(Collection_Id,Collection_Ref_No, Collected_On,Collected_By,Collection_Type,Customer_Id,Site_Use_Id,Amount,Cheque_No,Cheque_Date,Bank_Name,Bank_Branch,Emp_Code,Status,Print_Status,Sync_TimeStamp,Remarks)VALUES ('{0}', '{1}', '{2}', {3}, '{4}', {5}, {6}, {7}, '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}')"

#define kSQLCollectionInsertInvoice @"INSERT INTO TBL_Collection_Invoices(Collection_ID, Invoice_No, Invoice_Date, Amount, ERP_Status, Sync_TimeStamp, Collection_Inv_ID) VALUES ('{0}', '{1}', '{2}', {3}, '{4}','{5}', '{6}')"

//#define kSQLProductList @"SELECT  A.Item_Code,A.Category, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID  ORDER BY A.Item_Code"

#define kSQLProductListNonAlphabet @"SELECT  A.Item_Code,A.Category,A.Brand_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID %@ ORDER BY A.Description"

//#define kSQLProductListAlphabet @"SELECT  A.Item_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID" \
//" WHERE (A.Description LIKE '%@%%') %@" \
//" ORDER BY A.Description"

//#define kSQLProductListAlphabet @"SELECT  A.Item_Code,A.Category, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID" \
" WHERE (A.Description LIKE '%@%%') %@" \
" ORDER BY A.Description"





#define kSQLProductStock @"SELECT Org_ID, Lot_Qty, Lot_No, Expiry_Date, Stock_ID FROM TBL_Product_Stock WHERE Item_ID='%@' ORDER BY Org_ID ASC, Expiry_Date ASC"








#define KSWLProductTarget @"select Item_No As [Category], Target_Value, Custom_value_1 As [FSR_Target],Custom_value_2 [Sales_Value],0.00 As [Achievement],Target_Type from TBL_Product,TBL_Sales_target_items,TBL_Sales_Target where TBL_Sales_Target_Items.Sales_Target_ID= TBL_Sales_Target.Sales_Target_ID and TBL_Sales_Target_Items.Classification_1=TBL_Product.Category and TBL_Sales_Target_Items.Classification_1='%@' and TBL_Sales_Target_Items.Month='%d' and TBL_Sales_Target.Target_Type='C' "






#define kSQLProductFilterBrand @"SELECT {0}, Count({0}) as Count FROM TBL_Product GROUP BY {0}"

#define kSQLProductCategories @"SELECT DISTINCT A.Category, C.Description As [Description], B.Org_ID, A.Item_No FROM TBL_Product As A INNER JOIN TBL_Customer_Stock_Map As B ON A.Category=B.Product_Category AND B.Customer_ID=%@ AND B.Site_Use_ID=%@ INNER JOIN TBL_Org_Info As C ON B.Org_ID=C.Org_ID ORDER BY B.Org_ID, A.Item_No ASC"

//#define kSQLProductListFromCategoriesA @"SELECT A.Item_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price, SUM(B.Lot_Qty) As Lot_Qty, MIN(B.Expiry_Date) As Expiry_Date FROM TBL_Product As A LEFT  JOIN TBL_Product_Stock As B ON A.Inventory_Item_ID=B.Item_ID AND B.Org_ID='%@' WHERE A.Category='%@' AND Description LIKE '%@%%' GROUP BY A.Item_Code, A.Description, A.Inventory_Item_ID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price ORDER BY A.Description"



#define kSQLProductListFromCategoriesNA @"SELECT IFNULL(REMOVE_IN_FOC_LIST,'N') AS REMOVE_IN_FOC_LIST,A.Promo_Item,IFNULL(RESTRICTED,'N' ) AS RESTRICTED , IFNULL(C.bonus,'0') AS Bonus,A.Item_Code,A.Brand_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price, IFNULL(SUM(B.Lot_Qty),'0') As Lot_Qty, MIN(B.Expiry_Date) As Expiry_Date ,A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode,A.Category FROM TBL_Product As A LEFT JOIN (Select Attrib_Value as RESTRICTED,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RESTRICTED') AS I on A.Inventory_item_ID= I.Inventory_item_ID  and A.Organization_ID=I.Organization_ID LEFT JOIN (Select Attrib_Value as REMOVE_IN_FOC_LIST,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='REMOVE_IN_FOC_LIST') AS J on A.Inventory_item_ID= J.Inventory_item_ID  and A.Organization_ID=J.Organization_ID  LEFT JOIN (SELECT IFNULL(SUM(Get_Qty),'N/A')AS bonus,Item_Code FROM TBL_BNS_Promotion GROUP BY Item_Code) As C ON C.Item_Code=A.Item_Code LEFT  JOIN TBL_Product_Stock As B ON A.Inventory_Item_ID=B.Item_ID AND B.Org_ID='%@' WHERE A.Category='%@'   GROUP BY A.Item_Code, A.Description, A.Inventory_Item_ID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price ORDER BY A.Description"




#define kSQLProductListFromCategoriesAllWareHouses @"SELECT IFNULL(REMOVE_IN_FOC_LIST,'N') AS REMOVE_IN_FOC_LIST,   A.Promo_Item,IFNULL(RESTRICTED,'N' ) AS RESTRICTED,   IFNULL(C.bonus,'0') AS Bonus,A.Item_Code,A.Brand_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price, IFNULL(SUM(B.Lot_Qty),'0') As Lot_Qty, MIN(B.Expiry_Date) As Expiry_Date ,A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode,A.Category FROM TBL_Product As A LEFT JOIN (Select Attrib_Value as RESTRICTED,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='RESTRICTED') AS I on A.Inventory_item_ID= I.Inventory_item_ID  and A.Organization_ID=I.Organization_ID  LEFT JOIN (Select Attrib_Value as REMOVE_IN_FOC_LIST,Inventory_item_ID,Organization_ID  from  TBL_Product_Addl_Info where Attrib_Name ='REMOVE_IN_FOC_LIST') AS J on A.Inventory_item_ID= J.Inventory_item_ID  and A.Organization_ID=J.Organization_ID  LEFT JOIN (SELECT IFNULL(SUM(Get_Qty),'N/A')AS bonus,Item_Code FROM TBL_BNS_Promotion GROUP BY Item_Code) As C ON C.Item_Code=A.Item_Code LEFT  JOIN TBL_Product_Stock As B ON A.Inventory_Item_ID=B.Item_ID WHERE A.Category='%@' GROUP BY A.Item_Code, A.Description, A.Inventory_Item_ID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price ORDER BY A.Description"

#define kSQLOrderHistoryForCustomer @"SELECT IFNULL(A.ERP_Ref_No,'') AS ERP_Ref_No,A.Orig_Sys_Document_Ref,IFNULL(A.Shipping_Instructions,'N/A') AS Shipping_Instructions, A.Creation_Date,A.End_Time, IFNULL(A.Transaction_Amt,'0') AS Transaction_Amt, IFNULL(A.Custom_Attribute_5,'N/A') AS LPO_NO, CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status], B.Customer_Name ,(SELECT C.Username FROM TBL_User AS C WHERE C.SalesRep_ID=A.Created_By) As [FSR] FROM TBL_Order_History As A INNER JOIN TBL_Customer_Ship_Address As B ON B.Customer_ID=A.Ship_To_Customer_ID AND B.Site_Use_ID=A.Ship_To_Site_ID WHERE A.Doc_Type='I' AND B.Customer_No = '%@' AND A.Ship_To_Site_ID='%@' ORDER BY A.Orig_Sys_Document_Ref DESC"



#define kSQLDistributionCollection @"SELECT A.Description, A.Inventory_Item_ID, A.Primary_UOM_Code ,Item_Code,A.Organization_ID  FROM TBL_Product_MSL AS M  INNER JOIN TBL_Product A ON M.Inventory_Item_ID=A.Inventory_Item_ID AND M.Organization_ID=A.Organization_ID ORDER BY Item_Code ASC"




#pragma mark BackgroundSyncQueries

#define K_BS_SQLdbGetIncommingMessages @"select * from TBL_Incoming_Messages where Message_Date > '%@' AND Message_Date < '%@'"

#define K_BS_SQLdbGetFSRRepliedMessages @"select * from TBL_FSR_Messages where Reply_Date>'%@' AND Reply_Date < '%@'"


#define K_BS_SQLdbGetCancelRescheduleRoutes @"select * from TBL_Visit_Change_Log where Cancelled_At >= '%@' AND Cancelled_At <= '%@' AND Change_Type = 'R' AND Proc_Status = 'Y' AND Approval_Status = 'N'"
#define K_BS_SQLdbGetRescheduleRoutes @"select * from TBL_Visit_Change_Log where Requested_At >= '%@' AND Requested_At <= '%@' AND Proc_Status = 'N'"


/** Visits*/
#define K_BS_SQLdbGetVisits @"select * from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'"
#define K_BS_SQLdbSyncActualVisits @"select * from TBL_FSR_Actual_Visits"
/** Orders*/

#define K_BS_SQLdbGetOrders @"select * from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetOrderLineItems @"select * from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"

#define K_BS_SQLdbGetOrderLots @"select * from TBL_Order_Lots where Orig_Sys_Document_Ref in (select Orig_Sys_Document_Ref from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')))"
  /* order delete methods*/
#define K_BS_SQLdbDeleteOrders @"Delete from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetDeleteOrderLineItems @"Delete from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"

#define K_BS_SQLdbGetDeleteOrderLots @"Delete from TBL_Order_Lots where Orig_Sys_Document_Ref in (select Orig_Sys_Document_Ref from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')))"


/** Returns*/
#define K_BS_SQLdbGetReturns @"select * from tbl_rma where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetReturnLiteItems @"select * from TBL_RMA_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_rma where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"
#define K_BS_SQLdbGetReturnLots @"select * from TBL_RMA_Lots where Orig_Sys_Document_Ref in (select Orig_Sys_Document_Ref from TBL_RMA_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_rma where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')))"
/** Returns delete methods*/
#define K_BS_SQLdbDeleteReturns @"Delete from tbl_rma where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbDeleteReturnLiteItems @"Delete from TBL_RMA_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_rma where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"
#define K_BS_SQLdbDeleteReturnLots @"Delete from TBL_RMA_Lots where Orig_Sys_Document_Ref in (select Orig_Sys_Document_Ref from TBL_RMA_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_rma where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')))"

/** Order History*/
#define K_BS_SQLdbGetOrderHistory @"select * from TBL_Order_History where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetOrderHistoryLineItems @"select * from TBL_Order_History_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from TBL_Order_History where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"
/** Collections*/

#define K_BS_SQLdbGetCollections @"select * from TBL_Collection where Collected_On > '%@' AND Collected_On < '%@'"
#define K_BS_SQLdbGetCollection_Invoices @"select * from TBL_Collection_Invoices where Collection_ID in (select Collection_ID from TBL_Collection where Collected_On > '%@' AND Collected_On < '%@')"

/** DOC INFO*/
#define K_BS_SQLdbGetCollectionsDOCINFO @"select * from TBL_Doc_Addl_Info where Doc_No in ( select Collection_Ref_No from TBL_Collection where Collected_On > '%@' AND Collected_On < '%@')"
#define K_BS_SQLdbGetOrdersDOCINFO @"select * from TBL_Doc_Addl_Info where Doc_No in (select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"

/** Distribution check**/
#define K_BS_SQLdbGetDistributionCheck @"select * from TBL_Distribution_Check where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"

#define K_BS_SQLdbGetDistributionCheckItems @"select * from TBL_Distribution_Check_Items where DistributionCheck_ID in( select DistributionCheck_ID from TBL_Distribution_Check where Visit_ID in(select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"

/** Doc Info*/
/** Planned Visit **/
#define K_BS_SQLdbGetPlannedVisits @"select * from PHX_TBL_Planned_Visits where Visit_Date > '%@'  AND Visit_Date < '%@'"
#define K_BS_SQLdbGetActualVisits @"select * from PHX_TBL_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'"
#define K_BS_SQLdbGetDoctorList @"select * from PHX_TBL_Contacts where Created_At > '%@'  AND Created_At < '%@'"
#define K_BS_SQLdbGetEmpNotes @"select * from PHX_TBL_Emp_Notes where Visit_ID in( select Actual_Visit_ID from PHX_TBL_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetEDetailing @"select * from PHX_TBL_EDetailing where Visit_ID in( select Actual_Visit_ID from PHX_TBL_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"

#define K_BS_SQLdbGetIssueNotes @"select * from PHX_TBL_Issue_Note where Visit_ID in( select Actual_Visit_ID from PHX_TBL_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetIssueNotesLineItems @"select * from PHX_TBL_Issue_Note_Line_Items where Doc_No in ( select Doc_No from PHX_TBL_Issue_Note where Visit_ID in( select Actual_Visit_ID from PHX_TBL_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"
#define K_BS_SQLdbGetVisitAddlInfo @"select * from TBL_Visit_Addl_Info where Visit_ID in( select Actual_Visit_ID from PHX_TBL_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetEmpStock @"select * from PHX_TBL_Emp_Stock where Last_Updated_At > '%@'  AND Last_Updated_At < '%@'"
#define K_BS_SQLdbGetTaskList @"select * from PHX_TBL_Tasks"

#define K_BS_SQLdbGetVisitDoctorList @"select * from PHX_TBL_Doctors where Created_At > '%@'  AND Created_At < '%@'"
#define K_BS_SQLdbGetDoctorLocationList @"select * from PHX_TBL_Doctor_Location_Map where Doctor_ID in( select Doctor_ID from PHX_TBL_Doctors where Created_At > '%@'  AND Created_At < '%@')"
#define K_BS_SQLdbGetLocationList @"select * from PHX_TBL_Locations where Created_At > '%@'  AND Created_At < '%@'"
#define K_BS_SQLdbGetLocationConatctMapList @"select * from PHX_TBL_Location_Contact_Map where Location_ID in( select Location_ID from PHX_TBL_Locations where Created_At > '%@'  AND Created_At < '%@')"
#endif


/////Sales Order


//SELECT COUNT(A.*) AS Orders,COUNT(B.*)AS Returns,COUNT(C.*)AS Collections   FROM TBL_Order AS A , TBL_Collection AS C , TBL_RMA AS B
