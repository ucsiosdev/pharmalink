//
//  LocationFilterViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "LocationFilterViewController.h"
#import "SWDefaults.h"

@interface LocationFilterViewController ()

@end

@implementation LocationFilterViewController

@synthesize filters;
@synthesize target;
@synthesize action;

- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
        [self setTitle:NSLocalizedString(@"Filter", nil)];
        
        //customerSer.delegate = self;
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundView = nil;
     self.tableView.backgroundColor = [UIColor whiteColor];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(selectionDone:)] ];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter:)] ];
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}

- (void)viewDidAppear:(BOOL)animated {
    //customerSer.delegate = self;

    
    [self getCustomerServiceDidGetFilters:[[SWDatabaseManager retrieveManager] dbGetFilterByColumnCustomer:@"City"]];
    Singleton *single = [Singleton retrieveSingleton];
    single.isBarCode = NO;
}

- (void)selectionDone:(id)sender {
    
    Singleton *single = [Singleton retrieveSingleton];
    if(single.isBarCode)
    {  
        [SWDefaults setFilterForCustomerList:@"BarCode"];
        NSString *filterTitle = single.valueBarCode;
        [SWDefaults setbarCodeFilterForCustomerList:filterTitle];
         #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;
    }
    else
    {
    
        if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)] || [[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)] )
        {
            [((SWTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]]) resignResponder];
        }
         #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;
    }
}

- (void)clearFilter:(id)sender {
    [SWDefaults setLocationFilterForCustomerList:nil];
    [SWDefaults setPaymentFilterForCustomerList:nil];
    [SWDefaults setStatusFilterForCustomerList:nil];
    [SWDefaults setNameFilterForCustomerList:nil];
    [SWDefaults setCodeFilterForCustomerList:nil];
    [SWDefaults setbarCodeFilterForCustomerList:nil];
    if ([[SWDefaults filterForCustomerList]isEqualToString:@""] || [SWDefaults filterForCustomerList].length == 0 || [SWDefaults filterForCustomerList] == nil) {
         #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;

    }
    else {
        [SWDefaults setFilterForCustomerList:nil];
         #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;

    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
    {
        return 5;
    }
    else {
        if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Location", nil)])
        {
            return [self.filters count];
        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)])
        {
            return 1;
        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)])
        {
            return 1;
        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Payment", nil)])
        {
            return 2;
        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Status", nil)])
        {
            return 2;
        }
        else 
        {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];

    if (indexPath.section == 0)
    {
         if (indexPath.row == 0)
         {
             [cell.textLabel setText:NSLocalizedString(@"Location", nil)];
         }
         else if (indexPath.row == 1)
         {
             [cell.textLabel setText:NSLocalizedString(@"Code", nil)];
         }
         else if (indexPath.row == 2)
         {
             [cell.textLabel setText:NSLocalizedString(@"Name", nil)];
         }
         else if (indexPath.row == 3)
         {
             [cell.textLabel setText:NSLocalizedString(@"Payment", nil)];
         }
         else if (indexPath.row == 4)
         {
             [cell.textLabel setText:NSLocalizedString(@"Status", nil)];
         }
        
        
        if ([cell.textLabel.text isEqualToString:[SWDefaults filterForCustomerList]])
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        } 
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        }
    }
    else
    {
        if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Location", nil)])
        {
            NSDictionary *row = [self.filters objectAtIndex:indexPath.row];
            NSString *filterTitle = [row objectForKey:@"City"];
            
            
            if ([filterTitle isEqualToString:@""]) {
                [cell.textLabel setText:@"N/A"];
            }
            else
            {
                [cell.textLabel setText:filterTitle];
            }

            
            
            if ([filterTitle isEqualToString:[SWDefaults locationFilterForCustomerList]])
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            } 
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            }
        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)])
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Code", nil)];
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Code", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"code"];
            [((SWTextFieldCell *)cell).textField setText:[SWDefaults codeFilterForCustomerList]];
        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)])
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Name", nil)];
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeDefault];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Name", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Name"];
            [((SWTextFieldCell *)cell).textField setText:[SWDefaults nameFilterForCustomerList]];

        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Payment", nil)])
        {
            paymentOption = [NSMutableArray array];
            [paymentOption addObject:@"Cash"];          
            [paymentOption addObject:@"Credit"];
            [cell.textLabel setText:[paymentOption objectAtIndex:indexPath.row]];
            if ([cell.textLabel.text isEqualToString:[SWDefaults paymentFilterForCustomerList]])
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            } 
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            }
        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Status", nil)])
        {
           
            customerStatus = [NSMutableArray array];
            [customerStatus addObject:@"Open"]; 
            [customerStatus addObject:@"Blocked"];
            [cell.textLabel setText:[customerStatus objectAtIndex:indexPath.row]]; 
            if ([cell.textLabel.text isEqualToString:[SWDefaults statusFilterForCustomerList]])
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            } 
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            }
        }

        
    }
    return cell;
}
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0)
    {
        UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];

        NSString *filterTitle = cell.textLabel.text;
        [SWDefaults setFilterForCustomerList:filterTitle];
        if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)])
        {
            [SWDefaults setPaymentFilterForCustomerList:nil];
            [SWDefaults setLocationFilterForCustomerList:nil];
            [SWDefaults setStatusFilterForCustomerList:nil];
            [SWDefaults setCodeFilterForCustomerList:nil];
            [SWDefaults setbarCodeFilterForCustomerList:nil];

        }
       else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)])
        {
            [SWDefaults setPaymentFilterForCustomerList:nil];
            [SWDefaults setLocationFilterForCustomerList:nil];
            [SWDefaults setStatusFilterForCustomerList:nil]; 
            [SWDefaults setNameFilterForCustomerList:nil];
        }
       else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Location", nil)])
        {
            [SWDefaults setPaymentFilterForCustomerList:nil];
            [SWDefaults setStatusFilterForCustomerList:nil];
            [SWDefaults setCodeFilterForCustomerList:nil];
            [SWDefaults setNameFilterForCustomerList:nil];
            [SWDefaults setbarCodeFilterForCustomerList:nil];

        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Status", nil)])
        {
            [SWDefaults setLocationFilterForCustomerList:nil];
            [SWDefaults setPaymentFilterForCustomerList:nil];
            [SWDefaults setCodeFilterForCustomerList:nil];
            [SWDefaults setNameFilterForCustomerList:nil];
            [SWDefaults setbarCodeFilterForCustomerList:nil];

        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Payment", nil)])
        {
            [SWDefaults setLocationFilterForCustomerList:nil];
            [SWDefaults setStatusFilterForCustomerList:nil];
            [SWDefaults setCodeFilterForCustomerList:nil];
            [SWDefaults setNameFilterForCustomerList:nil];
            [SWDefaults setbarCodeFilterForCustomerList:nil];

        }
        else if ([[SWDefaults filterForCustomerList]isEqualToString:@"BarCode"])
        {
            [SWDefaults setLocationFilterForCustomerList:nil];
            [SWDefaults setStatusFilterForCustomerList:nil];
            [SWDefaults setCodeFilterForCustomerList:nil];
            [SWDefaults setNameFilterForCustomerList:nil];
            [SWDefaults setPaymentFilterForCustomerList:nil];
        }
    }
    else 
    {
         if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Location", nil)])
         {
             NSDictionary *row = [self.filters objectAtIndex:indexPath.row];
             NSString *filterTitle = [row objectForKey:@"City"];
             [SWDefaults setLocationFilterForCustomerList:filterTitle];
   
         }
         else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Status", nil)])
         {
             UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
            NSString *filterTitle = cell.textLabel.text;         
             [SWDefaults setStatusFilterForCustomerList:filterTitle];
     
         }
         else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Payment", nil)])
         {
             UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
             
             NSString *filterTitle = cell.textLabel.text;
             [SWDefaults setPaymentFilterForCustomerList:filterTitle];
             
         }
        

        
    }
    [self.tableView reloadData];
}
- (void)scrollCell:(EditableCell *)cell {
    [self.tableView scrollRectToVisible:cell.frame animated:YES];
  

}
#pragma mark EditCell Delegate


- (void)editableCellDidStartUpdate:(EditableCell *)cell {
  [self performSelector:@selector(scrollCell:) withObject:cell afterDelay:0.4f];

}

- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key {
    //[self.distributionItem setValue:value forKey:key];
    if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)])
    {
        NSString *filterTitle = value; 
        [SWDefaults setNameFilterForCustomerList:filterTitle];
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)])
    {
        NSString *filterTitle = value;
        [SWDefaults setCodeFilterForCustomerList:filterTitle];
        
    }
}
#pragma mark Service Delegate
- (void)getCustomerServiceDidGetFilters:(NSArray *)f {
    [self setFilters:f];
    [self.tableView reloadData];
    f=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
