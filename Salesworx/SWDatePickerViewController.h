//
//  SWDatePickerViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/17/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWViewController.h"


@interface SWDatePickerViewController : SWViewController {
    UIDatePicker *picker;
//       id target;
    SEL action;
    NSDate *selectedDate;
    UIToolbar *toolbar;
    BOOL isRoute;
    BOOL forReports;
}

@property (nonatomic, strong) UIDatePicker *picker;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, assign) BOOL isRoute;
@property (nonatomic, assign) BOOL forReports;
@property(nonatomic) BOOL isToDate;
- (id)initWithTarget:(id)t action:(SEL)a;

@end