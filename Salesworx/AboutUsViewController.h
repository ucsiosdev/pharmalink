//
//  AboutUsViewController.h
//  Salesworx
//
//  Created by msaad on 6/12/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "FTPBaseViewController.h"
@interface AboutUsViewController : FTPBaseViewController <MFMailComposeViewControllerDelegate>
{
    IBOutlet UILabel *customerLabel;
    IBOutlet UILabel *userLabel;
    IBOutlet UILabel *deviceLabel;
    IBOutlet UILabel *versionLabel;
    IBOutlet UIImageView *mainImage;
}
- (IBAction)actionEmailComposer;

@end
