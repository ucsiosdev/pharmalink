//
//  SalesWorxVisitOptionsManager.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/27/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxVisitOptionsManager.h"
#import "SalesWorxCustomClass.h"
#import "AppControl.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"

@implementation SalesWorxVisitOptionsManager


static SalesWorxVisitOptionsManager *sharedSingleton = nil;

+ (SalesWorxVisitOptionsManager*) retrieveSingleton {
    @synchronized(self)
    {
        if (sharedSingleton == nil) {
            sharedSingleton = [[SalesWorxVisitOptionsManager alloc] init];
        }
    }
    return sharedSingleton;
}

+ (void) destroyMySingleton
{
    sharedSingleton=nil;
}

+ (id) allocWithZone:(NSZone *) zone
{
    //TODO: Replace with newer version of alloc. Memory zones are now not used.
    
    @synchronized(self) {
        if (sharedSingleton == nil) {
            sharedSingleton = [super allocWithZone:zone];
            return sharedSingleton;
        }
    }
    return nil;
}



-(NSMutableArray*)fetchVisitOptionsforCustomer:(SalesWorxVisit*)currentvisit
{
    NSMutableArray * visitOptionsArray=[[NSMutableArray alloc]init];
     appControl=[AppControl retrieveSingleton];
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        NSString *PDARIGHTS=[SWDefaults getValidStringValue:[MedRepQueries getPDARIGHTSForSalesRep]];
        PDAComponents=[PDARIGHTS componentsSeparatedByString:@","];
    }
    
     customerLevelVisitOptionsFlagEnabled=[SWDefaults isCustomerLevelVisitOptionsFlagEnabled];

    if (customerLevelVisitOptionsFlagEnabled ==YES) {
      customerLevelEnabledVisitOptionsArray=[[SWDatabaseManager retrieveManager] fetchVisitOptionsFlagsforCustomer:currentvisit.visitOptions.customer.Ship_Customer_ID];
    }
    else{
        customerLevelEnabledVisitOptionsArray=[[NSMutableArray alloc]init];
    }
    
    if (customerLevelEnabledVisitOptionsArray.count>0) {
        
    }
    else{
        customerLevelEnabledVisitOptionsArray=[[NSMutableArray alloc]init];
    }
   
    
    NSLog(@"visit tyoe in visit options manager %@",currentvisit.visitOptions.customer.Visit_Type);
    /***Distribution Check Visit Option***/
    if ([self isDistributionCheckAvailable] && [currentvisit.visitOptions.customer.Visit_Type.uppercaseString isEqualToString:@"ONSITE"]) {
        [visitOptionsArray addObject:KDistributionCheckDisplayLevelCode];
    }
   
//    /***Telephonic visit distribution check validation***/
//    if ([currentvisit.Visit_Type isEqualToString:kTelephonicVisitTitle] && [visitOptionsArray containsObject:@"D"]) {
//        [visitOptionsArray removeObject:@"D"];
//    }
    
    /***Sales Order Visit Options***/
    if ([self isSalesOrderAvailable]) {
        [visitOptionsArray addObject:KSalesOrderDisplayLevelCode];
    }
    
    /***Manage Order Visit Options***/
    if ([self isManageOrdersAvailable]) {
        [visitOptionsArray addObject:kManageOrderDisplayLevelCode];
    }
    
    /***Order History***/
    if ([self isOrderHistoryAvailable]) {
        [visitOptionsArray addObject:kOrderHistoryDisplayLevelCode];
    }
    
    /*** Returns ***/
    if ([self isReturnsAvailable]) {
        [visitOptionsArray addObject:KReturnsDisplayLevelCode];
    }

    /*** Collections ***/
    if ([self isCollectionAvailable] && currentvisit.visitOptions.customer.Visit_Type) {
        [visitOptionsArray addObject:KPaymentCollectionDisplayLevelCode];
    }
    
    /*** Survey ***/
    if ([self isSurveyAvailable] && currentvisit.visitOptions.customer.Visit_Type) {
        [visitOptionsArray addObject:KSurveyDisplayLevelCode];
    }

    /*** Survey ***/
    if ([self isTodoAvailable]) {
        [visitOptionsArray addObject:kTODODisplayLevelCode];
    }
    
    /***Merchandising**/
    if ([self isMerchandisingAvailable] && currentvisit.visitOptions.customer.Visit_Type) {
        [visitOptionsArray addObject:kMerchandisingDisplayLevelCode];
    }
    /*REPORTS*/
    if ([self isReportsAvailable]) {
        [visitOptionsArray addObject:kReportsDisplayLevelCode];
    }


    return visitOptionsArray;
}

#pragma mark Merchandising

-(BOOL)isMerchandisingAvailable
{
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;
    
    isAvailableForCustomer=YES;

    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:kMerchandisingPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    if([appControl.ENABLE_MERCHANDISING isEqualToString:KAppControlsYESCode])
    {
        isAppControlFlagEnabled=YES;
        
    }
    else
    {
        isAppControlFlagEnabled=NO;
        
    }
    
    if(isAppControlFlagEnabled==YES&&isPDARightEnabled==YES&&isAvailableForCustomer==YES) {
        return YES;
    }
    else{
        return NO;
    }
}

#pragma mark Distribution Check

-(BOOL)isDistributionCheckAvailable
{
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;


    //no app control flag for distribution check
    isAppControlFlagEnabled=YES;
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:KDistributionCheckPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"]) {
        
    if ([customerLevelEnabledVisitOptionsArray containsObject:KDistributionCheckCustomerLevelCode]) {
        isAvailableForCustomer=YES;
        }
    else{
        isAvailableForCustomer=NO;
    }
    }
    else{
        isAvailableForCustomer=YES;
    }
    
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }
    
    return NO;
}

# pragma mark Reports
-(BOOL)isReportsAvailable{
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;
    
    
    isAppControlFlagEnabled=YES;
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:KReportsPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"]) {
        
        if ([customerLevelEnabledVisitOptionsArray containsObject:kReportsDisplayLevelCode]) {
            isAvailableForCustomer=YES;
        }
        else{
            isAvailableForCustomer=NO;
        }
    }
    else{
        isAvailableForCustomer=YES;
    }
    
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }
    
    return NO;
}

#pragma mark Sales Order

-(BOOL)isSalesOrderAvailable
{
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;

    isAppControlFlagEnabled=YES;

    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:KSalesOrderPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"]) {

    if ([customerLevelEnabledVisitOptionsArray containsObject:KSalesOrderCustomerLevelCode]) {
        isAvailableForCustomer=YES;
    }
    else{
        isAvailableForCustomer=NO;
    }
    }
    else{
        isAvailableForCustomer=YES;
    }
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }

    return NO;
}

#pragma mark Manage Order

-(BOOL)isManageOrdersAvailable
{
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;
    
    isAppControlFlagEnabled=YES;
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:KSalesOrderPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"]) {

    if ([customerLevelEnabledVisitOptionsArray containsObject:KSalesOrderCustomerLevelCode]) {
        isAvailableForCustomer=YES;
    }
    else{
        isAvailableForCustomer=NO;
    }
    }
    else{
        isAvailableForCustomer=YES;
 
    }
    
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }
    
    return NO;
}

#pragma mark Order History

-(BOOL)isOrderHistoryAvailable
{
    //for now there is only app control flag validation for order history
    if ([appControl.ENABLE_ORDER_HISTORY isEqualToString:KAppControlsYESCode]) {
        return YES;
    }
    
    return NO;
}

#pragma mark Returns

-(BOOL)isReturnsAvailable
{
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;
    
    isAppControlFlagEnabled=YES;
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:KReturnsPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"])  {

    if ([customerLevelEnabledVisitOptionsArray containsObject:KReturnsCustomerLevelCode]) {
        isAvailableForCustomer=YES;
    }
    else{
        isAvailableForCustomer=NO;
    }
    }
    else{
        isAvailableForCustomer=YES;
    }
    
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }
    
    return NO;
}

#pragma mark Collection

-(BOOL)isCollectionAvailable
{
    
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;
    
    isAppControlFlagEnabled=YES;
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:KPaymentCollectionPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"]) {

    if ([customerLevelEnabledVisitOptionsArray containsObject:KPaymentCollectionCustomerLevelCode]) {
        isAvailableForCustomer=YES;
    }
    else{
        isAvailableForCustomer=NO;
    }
    }
    else{
        isAvailableForCustomer=YES;
    }
    
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }
    
    return NO;
}

#pragma mark Survey
-(BOOL)isSurveyAvailable
{
    
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;
    
    isAppControlFlagEnabled=YES;
    
//    if ([appControl.SURVEY_MANDATORY isEqualToString:KAppControlsYESCode]) {
//        isAppControlFlagEnabled=YES;
//    }
//    else{
//        isAppControlFlagEnabled=NO;
//    }
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:KSurveyPDA_RightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"]) {

    if ([customerLevelEnabledVisitOptionsArray containsObject:KSurveyCustomerLevelCode]) {
        isAvailableForCustomer=YES;
    }
    else{
        isAvailableForCustomer=NO;
    }
    
    }
    else{
        isAvailableForCustomer=YES;
 
    }
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }
    
    return NO;
}

#pragma mark ToDo

-(BOOL)isTodoAvailable
{
    
    BOOL isAppControlFlagEnabled;
    BOOL isPDARightEnabled;
    BOOL isAvailableForCustomer;
    
    
    if([appControl.ENABLE_TODO isEqualToString:KAppControlsYESCode])
    {
        isAppControlFlagEnabled=YES;

    }
    else
    {
        isAppControlFlagEnabled=NO;

    }
    
    if([appControl.ENABLE_PDA_RIGHTS isEqualToString:KAppControlsYESCode])
    {
        if([PDAComponents containsObject:kTODORightsCode])
        {
            isPDARightEnabled=YES;
        }
        else{
            isPDARightEnabled=NO;
        }
    }
    else
    {
        isPDARightEnabled=YES;
    }
    
    
    if (customerLevelVisitOptionsFlagEnabled ==YES && customerLevelEnabledVisitOptionsArray.count>0 && ![customerLevelEnabledVisitOptionsArray containsObject:@"N/A"]) {

    if ([customerLevelEnabledVisitOptionsArray containsObject:kTODOCustomerLevelCode]) {
        isAvailableForCustomer=YES;
    }
    else{
        isAvailableForCustomer=NO;
    }
    }
    else{
        isAvailableForCustomer=YES;
    }
    
    if (isAppControlFlagEnabled==YES && isPDARightEnabled==YES && isAvailableForCustomer ==YES) {
        return YES;
    }
    
    return NO;
}






@end
