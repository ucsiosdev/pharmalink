//
//  MedRepEDetailTaskDetailViewTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepEDetailTaskDetailViewTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIDatePicker *cellDatePicker;

@property (strong, nonatomic) IBOutlet UILabel *taskLbl;
@property (strong, nonatomic) IBOutlet UITextView *cellTextView;

@property (strong, nonatomic) IBOutlet UITextField *taskDescTxtFld;
@property (strong, nonatomic) IBOutlet UILabel *taskDescLbl;
@end
