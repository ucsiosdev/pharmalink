//
//  SalesWorxCustomerStatementViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "XYPieChart/XYPieChart.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "SalesWorxReportsDateSelectorViewController.h"
#import "MedRepDateRangeTextfield.h"
#import "HMSegmentedControl.h"
#import "SalesWorxCustomerDuesTableViewCell.h"
#import "SalesWorxDropShadowView.h"
#import "SalesWorxTableView.h"

//ravinder code
#import "SalesWorxCustomerStatementPDFTemplateView.h"
#import "SalesWorxCustomerAgeingReportPDFTemplateView.h"
#import <MessageUI/MessageUI.h>


@interface SalesWorxCustomerStatementViewController : UIViewController<StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,XYPieChartDataSource,XYPieChartDelegate, SalesWorxDatePickerDelegate, UIPopoverPresentationControllerDelegate,SalesWorxReportsDateSelectorViewControllerDelegate,MFMailComposeViewControllerDelegate>
{
    NSString *datePopOverTitle;
    NSMutableArray *unfilteredCustomersStatementArray, *arrCustomerName, *arrCustomersDetails;


    IBOutlet HMSegmentedControl *customerStatementSegmentControl;
    IBOutlet UIView *viewFooter;
    IBOutlet UITableView *tblCustomerStatement;
    IBOutlet MedRepTextField *txtCustomer;
    IBOutlet MedRepDateRangeTextfield *DateRangeTextField;
    IBOutlet XYPieChart *customerPieChart;
    IBOutlet UILabel *lblTotalNetAmount;
    IBOutlet UILabel *lblTotalPaidAmount;
    IBOutlet UIView *parentViewOfPieChart;
    IBOutlet UIImageView *pieChartPlaceholderImage;
    NSMutableArray* customerDuesArray;
    IBOutlet SalesWorxDropShadowView *ageingReportView;
    IBOutlet SalesWorxTableView *ageingReportTableView;
    //NSMutableArray *invoices;
    //NSDictionary *customer;
   // double _total;
    
}


@property(strong,nonatomic) NSMutableArray *arrCustomersStatement;
@property(strong,nonatomic) Customer *selectedCustomer;
@property(nonatomic, strong) NSMutableArray *slicesForCustomerPotential;
@property(nonatomic, strong) NSArray        *sliceColorsForCustomerPotential;


@end
