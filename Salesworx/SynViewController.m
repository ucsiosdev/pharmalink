
//
//  ViewController.m
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//            [self.request setShouldContinueWhenAppEntersBackground:YES];
#import "AppConstantsList.h"
//#import "RequestResponseManager.h"
#import "SynViewController.h"
#import "DataSyncManager.h"
#import "JSON.h"
#import "Status.h"
#import "SWDefaults.h"
#import "CJSONDeserializer.h"
#import "SBJsonParser.h"
#import "ZipManager.h"
#import "ConfirmOrderViewController.h"
#import "NewActivationViewController.h"
#import "DataType.h"
#import "FMDBHelper.h"
#import "AFJSONRequestOperation.h"
#import "AFImageRequestOperation.h"
#import "UIViewController+MJPopupViewController.h"
#import "MedRepDefaults.h"
#import "SWAppDelegate.h"

@interface SynViewController ()
@end
@implementation SynViewController
@synthesize target,action,request;
@synthesize delegate= _delegate;

static NSString *ClientVersion = @"1";
static NSString *synchReference = @"";

- (id)init
{
    self = [super init];
    if (self){
        self.target = nil;
        self.action = nil;
        self.delegate = nil;
        
    }
    return self;
}
//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[Singleton getFreeMemory];
    
    
    //notification for printing log
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postSycStatus:) name:@"SyncStatusNotification"  object:nil];
   
    NSLog(@"selected server name in view did load is %@", [SWDefaults selectedServerDictionary]);
    

    
    mediaFilesToDeleteArray=[[NSMutableArray alloc]init];
    allMediaFilesArray=[[NSMutableArray alloc]init];

    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    

    
    NSLog(@"updating client version %@", [NSString stringWithFormat:@"%@(%@)", version,build]);
    
    ClientVersion =[NSString stringWithFormat:@"%@(%@)", version,build];
    
    //NSLog(@"Client Version %@",ClientVersion);
    
    
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    
    
    NSInteger collectionCount=[[NSUserDefaults standardUserDefaults]integerForKey:@"collectionCount"];
    
    if (collectionCount) {
        collectionCountString=[NSString stringWithFormat:@"%d",collectionCount];
        
    }
    
    else
    {
        collectionCountString=@"0";
        
    }
    
    // collectionCountString = [[SWDatabaseManager retrieveManager] dbdbGetCollectionCount];
    
    
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Data Synchronization", nil)];
    
    
    
   // [self setTitle:NSLocalizedString(@"Data Synchronization", nil)];
    
    
    //adding a button for settings
    

    
   // self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayActionBarButton, nil];
    
    
    

    
    
    self.view.backgroundColor = UIColorForPlainTable() ;
    appData=[DataSyncManager sharedManager];
    
    NSLog(@"selected server dict in server api %@", [SWDefaults selectedServerDictionary]);
    
    
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    //NSLog(@"SERVER NAME : %@",serverAPI);
    
    
    [[NSUserDefaults standardUserDefaults]setValue:serverAPI forKey:@"ServerAPILink"];
    
    uploadFileCount=0;
    procParamArray=[NSMutableArray arrayWithObjects:@"inpu1",@"input2", nil];
    procValueArray=[NSMutableArray arrayWithObjects:@"1234",@"5678", nil];
    strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    status_ =[[Status alloc] init];
    
    startSynchOrder = [[UIButton alloc] init]  ;
    startSynchOrder=[UIButton buttonWithType:UIButtonTypeCustom];
    //[startSynchOrder setImage:[UIImage imageNamed:@"sendgraybutton.png" cache:NO] forState:UIControlStateHighlighted];
    //[startSynchOrder setTitle:@"Start Synchronize" forState:UIControlStateNormal];
    [startSynchOrder setTintColor:[UIColor clearColor]];
    //[startSynchOrder setBackgroundColor:[UIColor blueColor]];
    [startSynchOrder.titleLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [startSynchOrder addTarget:self action:@selector(startSyncOrder) forControlEvents:UIControlEventTouchUpInside];
    [startSynchOrder.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    startSynchOrder.frame = CGRectMake(45,80,123,122);
    
    
    startSynch = [[UIButton alloc] init]  ;
    startSynch=[UIButton buttonWithType:UIButtonTypeCustom];
    //[startSynch setImage:[UIImage imageNamed:@"sync1Button.png" cache:NO] forState:UIControlStateNormal];
    //[startSynch setTitle:@"Start Synchronize" forState:UIControlStateNormal];
    [startSynch setTintColor:[UIColor clearColor]];
    //[startSynch setBackgroundColor:[UIColor blueColor]];
    [startSynch.titleLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [startSynch addTarget:self action:@selector(startSync) forControlEvents:UIControlEventTouchUpInside];
    [startSynch.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    startSynch.frame = CGRectMake(45,280,123,122);
    
    selectServer = [[UIButton alloc] init]  ;
    selectServer=[UIButton buttonWithType:UIButtonTypeCustom];
    [selectServer setBackgroundImage:[UIImage imageNamed:@"Old_green_button"] forState:UIControlStateNormal];
    
    
    NSData* dataDicts = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *testServerDicts = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDicts error:&error]];
    [selectServer setTitle:[testServerDicts stringForKey:@"name"]forState:UIControlStateNormal];
    [selectServer setTintColor:[UIColor clearColor]];
    [selectServer.titleLabel setTextColor:[UIColor whiteColor]];
    [selectServer.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [selectServer addTarget:self action:@selector(selectServerButton) forControlEvents:UIControlEventTouchUpInside];
    //[self.selectServer.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    selectServer.frame = CGRectMake(620,44,150 ,50);
    
    progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    progressBar.frame = CGRectMake((self.view.frame.size.height/2)-200, 640, 400, 35);
    //progressBar.progress = 1;
    
    logTextView = [[UITextView alloc] initWithFrame:CGRectMake(260, 100, 500, 430)];
    logTextView.scrollEnabled=YES;
    logTextView.editable=NO;
    logTextView.backgroundColor = [UIColor clearColor];
    //logTextView.textColor = [UIColor blueColor ];
    
    
    UIImage *statusImage = [UIImage imageNamed:@"Old_Syncbutton-1"];
    activityImageView = [[UIImageView alloc]   initWithImage:statusImage];
    activityImageView.frame = CGRectMake(45,280,123,122)
    ;
    
    UIImage *statusImage2 = [UIImage imageNamed:@"Send_orders_icon"];
    activityImageView2 = [[UIImageView alloc]   initWithImage:statusImage2];
    activityImageView2.frame = CGRectMake(45,80,123,122);
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {
        UIImageView *mainImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_SyncBG_AR"]];
        mainImage.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [mainImage setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:mainImage];
        
    }
    else
    {
        UIImageView *mainImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_SyncBG"]];
        mainImage.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [mainImage setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:mainImage];
        
    }
    
    
    lastSyncLable=[[UILabel alloc] initWithFrame:CGRectMake(430, 585, 180, 25)];
    [lastSyncLable setText:[SWDefaults lastSyncDate]];
    [lastSyncLable setTextAlignment:NSTextAlignmentLeft];
    [lastSyncLable setBackgroundColor:[UIColor clearColor]];
    [lastSyncLable setTextColor:UIColorFromRGB(0x4790D2)];
    [lastSyncLable setFont:RegularFontOfSize(22.0)];
    
    lastSyncStatusLable=[[UILabel alloc] initWithFrame:CGRectMake(680, 585, 180, 25)];
    [lastSyncStatusLable setText:[SWDefaults lastSyncStatus]];
    [lastSyncStatusLable setTextAlignment:NSTextAlignmentLeft];
    [lastSyncStatusLable setBackgroundColor:[UIColor clearColor]];
    [lastSyncStatusLable setTextColor:UIColorFromRGB(0x4790D2)];
    [lastSyncStatusLable setFont:RegularFontOfSize(22.0)];
    
    lastSyncType=[[UILabel alloc] initWithFrame:CGRectMake(680, 632, 180, 25)];
    [lastSyncType setText:[SWDefaults lastSyncType]];
    [lastSyncType setTextAlignment:NSTextAlignmentLeft];
    [lastSyncType setBackgroundColor:[UIColor clearColor]];
    [lastSyncType setTextColor:UIColorFromRGB(0x4790D2)];
    [lastSyncType setFont:RegularFontOfSize(22.0)];
    
    lastSyncOrderSent=[[UILabel alloc] initWithFrame:CGRectMake(450, 632,180, 25)]  ;
    [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
    [lastSyncOrderSent setTextAlignment:NSTextAlignmentLeft];
    [lastSyncOrderSent setBackgroundColor:[UIColor clearColor]];
    [lastSyncOrderSent setTextColor:UIColorFromRGB(0x4790D2)];
    [lastSyncOrderSent setFont:RegularFontOfSize(22.0)];
    
    pendingOrder=[[UILabel alloc] initWithFrame:CGRectMake(810, 47, 170, 100)]  ;
    [pendingOrder setText:orderCountString];
    [pendingOrder setTextAlignment:NSTextAlignmentCenter];
    [pendingOrder setBackgroundColor:[UIColor clearColor]];
    [pendingOrder setTextColor:[UIColor whiteColor]];
    [pendingOrder setFont:BoldFontOfSize(50.0)];
    
    pendingCollection=[[UILabel alloc] initWithFrame:CGRectMake(810, 210, 170, 100)];
    [pendingCollection setText:collectionCountString];
    [pendingCollection setTextAlignment:NSTextAlignmentCenter];
    [pendingCollection setBackgroundColor:[UIColor clearColor]];
    [pendingCollection setTextColor:[UIColor whiteColor]];
    [pendingCollection setFont:BoldFontOfSize(50.0)];
    
    pendingReturn=[[UILabel alloc] initWithFrame:CGRectMake(810, 372, 170, 100)]  ;
    [pendingReturn setText:returnCountString];
    [pendingReturn setTextAlignment:NSTextAlignmentCenter];
    [pendingReturn setBackgroundColor:[UIColor clearColor]];
    [pendingReturn setTextColor:[UIColor whiteColor]];
    [pendingReturn setFont:BoldFontOfSize(50.0)];
    
    [self.view addSubview:startSynch];
    [self.view addSubview:startSynchOrder];
    //[self.view addSubview:progressBar];
    [self.view addSubview:logTextView];
    [self.view addSubview:lastSyncLable];
    [self.view addSubview:lastSyncStatusLable];
    [self.view addSubview:activityImageView];
    [self.view addSubview:activityImageView2];
    [self.view addSubview:selectServer];
    [self.view addSubview:lastSyncType];
    [self.view addSubview:lastSyncOrderSent];
    [self.view addSubview:pendingReturn];
    [self.view addSubview:pendingOrder];
    [self.view addSubview:pendingCollection];
    
    isSuccessfull = NO;
    
}






-(void)viewWillAppear:(BOOL)animated
{
    

    [self.navigationController setToolbarHidden:YES animated:animated];
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    [pendingOrder setText:orderCountString];
    
}

- (void)selectServerButton
{
    
    
    
    //get the latest server list
    
    
    SeverEditLocation *serverSelectionViewController = [[SeverEditLocation alloc] init] ;
    [serverSelectionViewController setTarget:self];
    [serverSelectionViewController setSelectionChanged :@selector(serverChanged:)];
    [serverSelectionViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:serverSelectionViewController] ;
    popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    [popoverController setPopoverContentSize:CGSizeMake(200, 520)];
    
    
    [popoverController setDelegate:self];
    [popoverController presentPopoverFromRect:selectServer.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
}
- (void)serverChanged:(NSMutableDictionary *)d
{
    
    serverDict=nil;
    serverDict=[NSMutableDictionary dictionaryWithDictionary:d];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:serverDict  ] ;
    serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"url"]];
    //NSLog(@"SERVER NAME : %@",serverAPI);
    [popoverController dismissPopoverAnimated:YES];
    [selectServer setTitle:[serverDict stringForKey:@"name"] forState:UIControlStateNormal];
}


//this is send order action

- (void)startSyncOrder
{
    NSInteger count = [[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT COUNT(*) FROM TBL_Proforma_Order"]objectAtIndex:0] stringForKey:@"COUNT(*)"] integerValue];
    if (count>0)
    {
        ConfirmOrderViewController *orderConfirm = [[ConfirmOrderViewController alloc]init];
        [orderConfirm setTarget:self];
        [orderConfirm setAction:@selector(orderConfirmedSendOrder)];
        [self.navigationController pushViewController:orderConfirm animated:YES];
    }
    else
    {
        [self orderConfirmedSendOrder];
    }
    
    
    
}
-(void)orderConfirmedSendOrder
{
    
    
    //diable left bar button item make it a sync process, user should not do anything
    
    
    //    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    //    [[UIApplication sharedApplication]cancelAllLocalNotifications];
    [self setTarget:nil];
    [self setAction:nil];
    self.target = nil;
    self.action = nil;
    
    SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
    if([syncObject licenseVerification])
    {
        
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
        
        Singleton *single = [Singleton retrieveSingleton];
        single.isFromSync = 2;
        single.isActivated = YES;
        isCompleted = NO;
        logTextView.text = @"";
        [SWDefaults setLastSyncType:@"Send Orders"];
        [lastSyncType setText:[SWDefaults lastSyncType]];
        [self finalLogPrint:@"Starting order upload"];
        orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
        [self finalLogPrint: [NSString stringWithFormat:@"Orders to be uploaded : %@",orderCountString]];
        [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
        [self StartOrderSyncAnimation];
        //sendOrderSer.delegate=self;
        [[SWDatabaseManager retrieveManager] dbGetConfirmOrder];
        [[SWDatabaseManager retrieveManager] setTarget:self];
        [[SWDatabaseManager retrieveManager] setAction:@selector(xmlDone:)];
    }
    else
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

        [[[UIAlertView alloc] initWithTitle:@"License validation error" message:@"You license has been expired or contact your administrator for further assistance " delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil]  show];
    }
    
}
-(void)xmlDone:(NSString *)st
{
    
    if([st isEqualToString:@"done-01"])
    {
        Singleton *single = [Singleton retrieveSingleton];
        NSString  *XMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)single.xmlParsedString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
        //NSString *applicationDocumentsDir =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        //NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:@"sample6.xml"];
        //[XMLPassingString writeToFile:storePath atomically:YES encoding:NSUTF8StringEncoding error:NULL];
        [self sendRequestForExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendOrders" andProcParam:@"OrderList" andProcValue: XMLPassingString];
    }
    else
    {
        
        //hide local notification
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        
        
        [[DataSyncManager sharedManager]hideCustomIndicator];
        [self stopAnimationofSendOrder];
        [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
    }
}
- (void)startSync
{
    int count = [[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT COUNT(*) FROM TBL_Proforma_Order"]objectAtIndex:0] stringForKey:@"COUNT(*)"] intValue];
    if (count>0)
    {
        ConfirmOrderViewController *orderConfirm = [[ConfirmOrderViewController alloc]init];
        [orderConfirm setTarget:self];
        [orderConfirm setAction:@selector(orderConfirmed)];
        [self.navigationController pushViewController:orderConfirm animated:YES];
    }
    else
    {
        NSLog(@"order confirmed called from start sync");
        
        [self orderConfirmed];
    }
    
}
-(void)orderConfirmed
{
    //[locationFilterPopOver dismissPopoverAnimated:YES];
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];


    [self setTarget:nil];
    [self setAction:nil];
    self.target = nil;
    self.action = nil;
    Singleton *single = [Singleton retrieveSingleton];
    single.isActivated = YES;
    isCompleted = NO;
    appData=[DataSyncManager sharedManager];
    [[SWDatabaseManager retrieveManager] checkUnCompleteVisits];
    [self StartSyncAnimation];
    statusSynString = @"100";
    progressBar.progress = 0.0;
    logTextView.text = @"";
    SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
    if([syncObject licenseVerification])
    {
        NSLog(@"send request for sync upload called from synViewcontroller");
        
        [self setRequestForSyncUpload];
    }
    else
    {
        //        [ILAlertView showWithTitle:@"License validation error"
        //                           message:@"You license has been expired or contact your administrator for further assistance "
        //                  closeButtonTitle:NSLocalizedString(@"Ok", nil)
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:^(NSInteger buttonIndex) {
        //                   [[NSNotificationCenter defaultCenter] postNotificationName:@"ActivationError" object:nil];
        //
        //               }];
        [[[UIAlertView alloc] initWithTitle:@"License validation error" message:@"You license has been expired or contact your administrator for further assistance " delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil]  show];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"ActivationError" object:nil];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    
    if (alertView.tag==1000) {
        
        alertView=nil;
        
    }
    if (buttonIndex == [alertView cancelButtonIndex]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ActivationError" object:nil];
        
    }
    
    
    
    // else do your stuff for the rest of the buttons (firstOtherButtonIndex, secondOtherButtonIndex, etc)
}
- (void)setRequestForSyncActivateWithUserName:(NSString *)username andPassword:(NSString *)password
{
    

    NewActivationViewController * newActivation=[[NewActivationViewController alloc]init];
    newActivation.alertDisplayed=NO;
    
    
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isActivated = NO;
    isCompleted=NO;
    if(appData.statusDict)
    {
        [appData.statusDict removeAllObjects];
        appData.statusDict=nil;
    }
    if (self.request)
    {
        self.request = nil;
    }
    
    self.request = [[HttpClient sharedManager] initWithBaseURL:[NSURL URLWithString:@"http://google.com"]];
    [self.request setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         if (status == AFNetworkReachabilityStatusNotReachable)
         {
             [self finalLogPrint:@"Connection Failed"];
             NSLog(@"connection failed");
             
             [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
             return;
         }
         else
         {
             if (!isSuccessfull)
             {
                 isSuccessfull=YES;
                 [self finalLogPrint:@"Connection successfully"];
             }
             
         }
     }];
    //    [[DataSyncManager sharedManager] reachabilityNotificationMethod];
    //    if(appData.errorCode==jNetworkError)
    //    {
    //        [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
    //        return;
    //    }
    passwordActivate =[[DataSyncManager sharedManager] sha1:password];
    usernameActivate = username;
    isByteSent = NO;
    NSString *strDatabaseInfo= [[DataSyncManager sharedManager] dbGetDatabasePath];
    if([strDatabaseInfo isEqualToString:@"Database does not exist"]){}
    else{[[DataSyncManager sharedManager] deleteDatabaseFromApp];}
    isFromSync=1;
    single.isFromSync = 1;
    
    
    [self finalLogPrint:@"Connecting..."];
    
    [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
    [self sendRequestForActivate];
}
- (void)setRequestForSyncUpload
{
    
    
    NSLog(@"send request for sync upload called");

    [ZipManager convertSqliteToNSDATA];
    [SWDefaults setLastSyncType:@"Full Sync"];
    [lastSyncType setText:[SWDefaults lastSyncType]];
    if(appData.statusDict){
        [appData.statusDict removeAllObjects];
        appData.statusDict=nil;
    }
    Singleton *single = [Singleton retrieveSingleton];
    isByteSent = NO;
    isForSyncUpload=1;
    isFromSync=2;
    single.isFromSync = 2;
    single.isForSyncUpload=1;
    
    NSLog(@"connecting from full sync");

    [self finalLogPrint:@"Connecting..."];
    
    usernameActivate = [[SWDefaults userProfile] stringForKey:@"Username"];
    passwordActivate = [[SWDefaults userProfile] stringForKey:@"Password"];
    // [[DataSyncManager sharedManager] reachabilityNotificationMethod];
    
    
    
    
    
    //this is causing delay change and move to utilities
    
    
    
    
    if (self.request)
    {
        self.request = nil;
    }
    self.request = [[HttpClient sharedManager] initWithBaseURL:[NSURL URLWithString:@"http://google.com"]];
    [self.request setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         if (status == AFNetworkReachabilityStatusNotReachable)
         {
             [self finalLogPrint:@"Connection Failed"];
             [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
             return;
         }
         else
         {
             if (!isSuccessfull)
             {
                 isSuccessfull=YES;
                 [self finalLogPrint:@"Connection successfully"];
             }
             
         }
     }];
    /*
     if( [Singleton connectedToInternet])
     {
     [self finalLogPrint:@"Connection successfully"];
     }
     else
     {
     [self finalLogPrint:@"Connection Failed"];
     [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
     return;
     }
     */
    NSString *strDatabaseInfo= [[DataSyncManager sharedManager] dbGetZipDatabasePath];
    
    if([strDatabaseInfo isEqualToString:@"Database does not exist"])
    {
        [[DataSyncManager sharedManager]UserAlert:@"Please activate the process first"];
    }
    else
    {
        [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
        [self sendRequestForUploadData];
    }
}
- (void)sendRequestForActivate
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString *strurl =[serverAPI stringByAppendingString:@"Activate"];
    
    //
    
    NSURL *url = [NSURL URLWithString:strurl];
    if (self.request)
    {
        self.request = nil;
    }
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    //check the reachability of activation url
    
    BOOL isReachable=[self urlIsReachable:[testServerDict stringForKey:@"serverName"]];
    if (isReachable==YES) {
        NSLog(@"server url is reachable");
    }
    
    else
    {
        NSLog(@"server url unreachable");
        
    }
    
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            @"Activation", @"SyncType",
                            [testServerDict stringForKey:@"serverName"], @"SyncLocation",
                            nil];
    
    
    
    NSLog(@"chek request %@", self.request);
    NSLog(@"check parameters %@", [params description]);
    
    
    [self.request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         //NSLog(@"Request Successful, response '%@'", [operationQ responseString]);
         
         
         NSLog(@"check response %@", [responseObject description]);
         
         NSString *responseString = [operationQ responseString];
         appData.statusDict = [responseString JSONValue];
         
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             NSString *strSyncNO=[appData.statusDict objectForKey:@"SyncReferenceNo"];
             synchReference = strSyncNO;
             if(![strSyncNO length]==0)
             {
                 [self sendRequestForInitiate];
             }
             else
             {
                 //NSLog(@"hideCustomIndicator 3");
                 [self stopAnimation];
                 //NSLog(@"Upload response %@", responseString);
                 
                 [[DataSyncManager sharedManager] UserAlert:[NSString stringWithFormat:@"Error:%@",responseString]];
                 //[self writeActivationLogToTextFile:[NSString stringWithFormat:@"Error:%@",responseString]];
                 if(!single.isActivated)
                 {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                     [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                 }
             }
         }
         else
         {
             NSLog(@"hide animation here ");
             
         }
         
     } failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         
         
         
         
         
         [self stopAnimation];
         
         
         
         NSLog(@"[HTTPClient Error]: %@", error.debugDescription);
         
         NewActivationViewController* newActivationVC=[[NewActivationViewController alloc]init];
         newActivationVC.logTextView.text=@"";
         logTextView.text=@"";
         newActivationVC.alertDisplayed=NO;
         
         
         //if user crendentials are invalid then there is no point send it to activation, so commenting it out and clear the log txtview
         
         
         
         
         // [self sendRequestCompleteWithError:[operationQ responseString]];
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             
         }
         else
         {
             
         }
     }];
    
    
}


-(BOOL) urlIsReachable:(NSString*)urlStr {
    
    NSString *url = urlStr;
    
    NSURLRequest* reachableRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:5.0];
    NSHTTPURLResponse* response = nil;
    NSError* error = nil;
    [NSURLConnection sendSynchronousRequest:reachableRequest returningResponse:&response error:&error];
    NSLog(@"statusCode = %d", [response statusCode]);
    
    if ([response statusCode] == 404)
        return NO;
    else
        return YES;
}


- (void)sendRequestForUploadData
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    [self finalLogPrint:@"Uploading database..."];
    //Saadi Progress
    
    [DataSyncManager sharedManager].progressHUD.labelText=@"Uploading database...";
    [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeDeterminate;
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadDataGZ"];
    NSString *path = [[DataSyncManager sharedManager] dbGetZipDatabasePath];
    //NSLog(@"File Path %@",path);
    
    
    NSLog(@"uploading database for url is %@", strUrl);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            @"Full Sync", @"SyncType",
                            [testServerDict stringForKey:@"serverName"], @"SyncLocation",
                            nil];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    if (self.request)
    {
        self.request = nil;
    }
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [self.request multipartFormRequestWithMethod:@"POST"
                                                                             path:nil
                                                                       parameters:params
                                                        constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:@"swx"
                                                                  fileName:@"swx"
                                                                  mimeType:@"application/x-gzip"];
                                      }
                                      ];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
             [DataSyncManager sharedManager].progressHUD.progress = 0.0;
         }
         else
         {
             progressBarFloatValue = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
             [DataSyncManager sharedManager].progressHUD.progress = progressBarFloatValue;
             progressDetail = [NSString stringWithFormat:@"%.0f",progressBarFloatValue*100] ;
             progressDetail =[progressDetail stringByAppendingString:@"%"];
             [DataSyncManager sharedManager].progressHUD.detailsLabelText = progressDetail;
             if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
             {
                 [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
             }
             [DataSyncManager sharedManager].progressHUD.progress = totalBytesWritten * 1.0 / totalBytesExpectedToWrite;
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         //NSLog(@"success with response string %@", operationQ.responseString);
         appData.statusDict = [ operationQ.responseString JSONValue];
         // //NSLog(@"Upload response %@", appData.statusDict);
         
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             
         }
         else if(single.isFromSync==2)
         {
             [DataSyncManager sharedManager].progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_37x-Checkmark"]];
             [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeCustomView;
             [DataSyncManager sharedManager].progressHUD.labelText = @"Uploaded";
             //Saadi Progress
             if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
             {
                 [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
             }
             
             NSString *strSyncNO=[appData.statusDict objectForKey:@"SyncReferenceNo"];
             //NSLog(@"Status dict %@",appData.statusDict);
             synchReference = strSyncNO;
             
             if(![strSyncNO length]==0)
             {
                 [self sendRequestForInitiate];
             }
             else
             {
                 //NSLog(@"hideCustomIndicator 7");
                 [self stopAnimation];
                 [[DataSyncManager sharedManager] UserAlert:@"Please start the process again require Syncrefrenceno."];
                 if(!single.isActivated)
                 {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                     [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                 }
             }
         }
         
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"completion request with error called in upload data %@ %@", operation.responseString,error.debugDescription);
         
         
         [self sendRequestCompleteWithError:[operationQ responseString]];
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             
         }
         else
         {
             
         }
         //NSLog(@"error: %@", error.localizedDescription);
     }];
    
    [operation start];
}
- (void)sendRequestForStatus
{
    
    if(isCompleted)
    {
        [self ProcessCompleted];
        [DataSyncManager sharedManager].progressHUD.progress = [[appData.statusDict objectForKey:@"CurrentProgress"] floatValue];
    }
    else
    {
        NSString *strUrl =[serverAPI stringByAppendingFormat:@"status/%@",[appData.statusDict objectForKey:@"SyncReferenceNo"]];
        
        
        
        NSURL *url = [NSURL URLWithString:strUrl];
        if (self.request)
        {
            self.request = nil;
        }
        //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
        self.request = [[HttpClient sharedManager] initWithBaseURL:url];
        
        
        
        [self.request getPath:nil parameters:nil success:^(AFHTTPRequestOperation *operationQ, id responseObject)
         {
             NSString *responseString = operationQ.responseString;
             appData.statusDict = [responseString JSONValue];
             // //NSLog(@"Upload response %@", appData.statusDict);
             
             Singleton *single = [Singleton retrieveSingleton];
             
             NSLog(@"isfrom sync for download data %d", isFromSync);
             
             if(single.isFromSync==1)
             {
                 if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"D"])
                 {
                     [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
                     
                     NSLog(@"send request for download called 981");
                     
                     NSLog(@"isfrom sync %d", isFromSync);
                     
                     [self sendRequestForDownload];
                 }
                 else if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"F"])
                 {
                     if (!single.isActivated)
                     {
                         //[self writeActivationLogToTextFile:[appData.statusDict stringForKey:@"ProcessResponse"]];
                         UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:[appData.statusDict stringForKey:@"ProcessResponse"] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
                         [ErrorAlert show];
                         //                         [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
                         //                                            message:[appData.statusDict stringForKey:@"ProcessResponse"]
                         //                                   closeButtonTitle:NSLocalizedString(@"Ok", nil)
                         //                                  secondButtonTitle:nil
                         //                                tappedButtonAtIndex:nil];
                         
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                         [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                     }
                 }
                 else
                 {
                     
                     [self performSelector:@selector(sendRequestForStatus) withObject:nil afterDelay:1.0];
                     // [self sendRequestForStatus];
                 }
                 
             }
             else
             {
                 if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"D"])
                 {
                     [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
                     
                     NSLog(@"send request for download called 1020");

                     
                     [self sendRequestForDownload];
                 }
                 else if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"F"])
                 {
                     [self finalLogPrint:[appData.statusDict stringForKey:@"ProcessResponse"] ];
                     [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
                     [[DataSyncManager sharedManager] UserAlert:@"Sync process failed. Please try again or contact your administrator for assistance "];
                 }
                 else
                 {
                     [self performSelector:@selector(sendRequestForStatus) withObject:nil afterDelay:1.0];
                     //[self sendRequestForStatus];
                 }
             }
             
         } failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
             //NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
             
             NSLog(@"completion request with error called in send request for status %@ %@", operation.responseString,error.debugDescription);

             [self sendRequestCompleteWithError:[operationQ responseString]];
             Singleton *single = [Singleton retrieveSingleton];
             if(single.isFromSync==1)
             {
                 
             }
             else
             {
                 
             }
         }];
        
        Singleton *single = [Singleton retrieveSingleton];
        
        if(![statusSynString isEqualToString:[appData.statusDict stringForKey:@"SyncStatus"]])
        {
            statusSynString = [appData.statusDict stringForKey:@"SyncStatus"];
            if([statusSynString isEqualToString:@"N"])
            {
                [self finalLogPrint:@"Sync Status : Pending"];
            }
            if([statusSynString isEqualToString:@"C"])
            {
                if(single.isFromSync==2)
                {
                    [self finalLogPrint:@"Database has been uploaded successfully"];
                }
                [self finalLogPrint:[NSString stringWithFormat:@"Sync Reference # %@",[appData.statusDict stringForKey:@"SyncReferenceNo"]]];
                [self finalLogPrint:[NSString stringWithFormat:@"Byte Sent : %@ bytes",[appData.statusDict stringForKey:@"BytesReceived"]]];
                [self finalLogPrint:@"Sync Status : Copying"];
                [DataSyncManager sharedManager].progressHUD.detailsLabelText = [NSString stringWithFormat:@"Byte Sent : %@ bytes",[appData.statusDict stringForKey:@"BytesReceived"]];
                [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeDeterminate;
                [DataSyncManager sharedManager].progressHUD.labelText = @"Copying";
            }
            if([statusSynString isEqualToString:@"S"])
            {
                [self finalLogPrint:@"Sync Status : Synchronizing"];
                [DataSyncManager sharedManager].progressHUD.labelText = @"Synchronizing";
            }
            if([statusSynString isEqualToString:@"D"])
            {
                [self finalLogPrint:@"Sync Status : Downloaded"];
                [DataSyncManager sharedManager].progressHUD.labelText = @"Please Wait...";
                [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
                [DataSyncManager sharedManager].progressHUD.detailsLabelText = [NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]];
            }
            if([statusSynString isEqualToString:@"F"])
            {
                [self stopAnimation];
                //NSLog(@"hideCustomIndicator 5");
                [self finalLogPrint:[appData.statusDict stringForKey:@"ProcessResponse"]];
                [self finalLogPrint:@"Sync Status : Failed"];
                //NSLog(@"Sync status with fail %@",appData.statusDict);
                
                Singleton *single = [Singleton retrieveSingleton];
                if (!single.isActivated)
                {
                    UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:[appData.statusDict stringForKey:@"ProcessResponse"] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
                    [ErrorAlert show];
                    //                    [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
                    //                                       message:[appData.statusDict stringForKey:@"ProcessResponse"]
                    //                              closeButtonTitle:NSLocalizedString(@"Ok", nil)
                    //                             secondButtonTitle:nil
                    //                           tappedButtonAtIndex:nil];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                    [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                }
            }
            if([statusSynString isEqualToString:@"P"])
            {
                
                [self finalLogPrint:@"Sync Status : Preparing output"];
                
            }
            if([[appData.statusDict objectForKey:@"CurrentProgress"] isEqual:@"1"])
            {
                [DataSyncManager sharedManager].progressHUD.detailsLabelText = @"Linking with application";
            }
        }
        if([[appData.statusDict objectForKey:@"CurrentProgress"] isEqual:@"1"])
        {
            [DataSyncManager sharedManager].progressHUD.labelText = @"PLease wait...";
            [DataSyncManager sharedManager].progressHUD.detailsLabelText = @"Linking with application";
        }
        progressBarFloatValue = [[appData.statusDict objectForKey:@"CurrentProgress"] floatValue];
        progressBar.progress=progressBarFloatValue;
        progressBarFloatValue = progressBarFloatValue * 100.0;
        progressDetail = [NSString stringWithFormat:@"%.0f",progressBarFloatValue] ;
        progressDetail =[progressDetail stringByAppendingString:@"%"];
        [DataSyncManager sharedManager].progressHUD.progress = [[appData.statusDict objectForKey:@"CurrentProgress"] floatValue];
        [DataSyncManager sharedManager].progressHUD.detailsLabelText = progressDetail;
    }
    
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
}
- (void)sendRequestForInitiate
{
    NSString *strurl =[serverAPI stringByAppendingString:@"Initiate"];
    
    
    NSURL *url = [NSURL URLWithString:strurl];
    if (self.request)
    {
        self.request = nil;
    }
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [appData.statusDict objectForKey:@"SyncReferenceNo"], @"SyncReferenceNo",
                            nil];
    
    [self.request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         appData.statusDict = [responseString JSONValue];
         ////NSLog(@"Upload response %@", appData.statusDict);
         
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             NSString *strSyncNO=[appData.statusDict objectForKey:@"SyncReferenceNo"];
             if(![strSyncNO length]==0)
             {
                 NSLog(@"send request for complete called from sync refno send request for initiate");

                 [self sendRequestForStatus];
             }
             else
             {
                 //NSLog(@"hideCustomIndicator 4");
                 [self stopAnimation];
                 
                 [[DataSyncManager sharedManager] UserAlert:@"Please start the process again require Syncrefrenceno."];
                 if (!single.isActivated)
                 {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                     [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                 }
             }
         }
         else if(single.isFromSync==2)
         {
             
             NSLog(@"send request for complete called from issync 2 send request for initiate");

             [self sendRequestForStatus];
         }
         
     }
                   failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                       //NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
                       
                       NSLog(@"completion request with error called in send request for initiate %@ %@", operation.responseString,error.debugDescription);

                       [self sendRequestCompleteWithError:[operationQ responseString]];
                       Singleton *single = [Singleton retrieveSingleton];
                       if(single.isFromSync==1)
                       {
                           
                       }
                       else
                       {
                           
                       }
                   }];
    
}
- (void)sendRequestForDownload
{
    [DataSyncManager sharedManager].progressHUD.labelText = @"Downloading";
    [self finalLogPrint:@"Sync Status : Downloading database..."];
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    
    NSString *strUrl =[serverAPI stringByAppendingFormat:@"DownloadGZ/%@",[appData.statusDict objectForKey:@"SyncReferenceNo"]];
    
    NSString *resourceDocPath;
    
    
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        resourceDocPath=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    }
    
    
    
    
    //NSString *pdfName =databaseName;
    NSString *pdfName = @"swx.sqlite.gz";
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
    
    
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:request123] ;
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation setDownloadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
             [DataSyncManager sharedManager].progressHUD.progress = 0.0;
         }
         else
         {
             progressBarFloatValue = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
             [DataSyncManager sharedManager].progressHUD.progress = progressBarFloatValue;
             progressDetail = [NSString stringWithFormat:@"%.0f",progressBarFloatValue*100] ;
             progressDetail =[progressDetail stringByAppendingString:@"%"];
             [DataSyncManager sharedManager].progressHUD.detailsLabelText = progressDetail;
             if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
             {
                 [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
             }
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject) {
        [self finalLogPrint:@"Sync Status : Database Downloaded"];
        
        NSString *responseString = [operationQ responseString];
        appData.statusDict = [responseString JSONValue];
        
        // //NSLog(@"Upload response %@", appData.statusDict);
        [DataSyncManager sharedManager].progressHUD.progress = 1.0;
        single = [Singleton retrieveSingleton];
        [ZipManager convertNSDataToSQLite];
        
        
        
        
        if(single.isFromSync==1)
        {
            BOOL success;
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            
            NSString *resourceDocPath;
            
            
            //iOS 8 support
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                resourceDocPath=[SWDefaults applicationDocumentsDirectory];
            }
            
            else
            {
                resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
            }
            
            
           
            
            
            //            NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
            NSString *pdfName = databaseName;
            NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
            success = [fileManager fileExistsAtPath:filePath];
            if (success)
            {
                //NSLog(@"SendRequestComplete 1");
                
                
                //now start downloading images
                
                NSLog(@"DOWNLOADING MEDIA IN FULL SYNC JAN 27");
                
                
                
                SWAppDelegate *app=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
                
                app.isNewSync=YES;
                

                
                
                [self downloadMedia];
                
                [self sendRequestForComplete];
                
                
               
                
                
                
                
            }
            else
            {
                [[DataSyncManager sharedManager] UserAlert:@"Databse can not be downloaded. Please try again"];
            }
        }
        else
        {
            
            

            
            
            NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
            if([signatureArray count]>=1)
            {
                

                [self sendRequestForUploadFile];
            }
            else
            {
                //check if condition fails to be done, without this media was downloaded only during new activation
                
                 [self downloadMedia];
                
                [self sendRequestForComplete];
                
               
            }
        }
        
        //NSLog(@"Successfully downloaded file to %@", filePath);
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                                         //NSLog(@"Error: %@", error);
                                         
                                         NSLog(@"completion request with error called in send request for download %@ %@", operation.responseString,error.debugDescription);

                                         [self sendRequestCompleteWithError:[operationQ responseString]];
                                         Singleton *single = [Singleton retrieveSingleton];
                                         if(single.isFromSync==1)
                                         {
                                             
                                         }
                                         else
                                         {
                                             
                                         }
                                     }];
    
    [operation start];
    
    
}
#pragma mark Media files downloading methods

-(NSMutableArray*)fetchDownloadedMediaFiles

{
    [mediaFilesToDeleteArray removeAllObjects];
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files  where Is_Deleted ='N'  "]];
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [mediaFilesToDeleteArray addObject:customer];
    }
    return mediaFilesToDeleteArray;
}

-(NSMutableArray *) fetchMediaFilesToDelete {
    
    [mediaFilesToDeleteArray removeAllObjects];
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files WHERE Is_Deleted='Y'"]];
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [mediaFilesToDeleteArray addObject:customer];
    }
    return mediaFilesToDeleteArray;
}


-(NSMutableArray *) fetchMediaFilesFromDocuments {
    
    [allMediaFilesArray removeAllObjects];
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files WHERE Download_Flag='Y' AND Is_Deleted='N'"]];
    
    //ppt file got downloaded
    
   // NSLog(@"downloaded media files %@", [array description]);
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        customer.Download_Media_Type=kDefaultDownloadMediaType;

        [allMediaFilesArray addObject:customer];
        
        
        //POSM Images
        NSMutableArray * posmArray = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"select IFNULL(Custom_Attribute_1,'')AS Custom_Attribute_1 from TBL_App_Codes where code_type = 'POSM_TITLE'"]];
        if (posmArray.count>0) {
            for (NSMutableDictionary* currentDict in posmArray) {
                MediaFile *posmMediaFile = [[MediaFile alloc]init];
                posmMediaFile.Media_File_ID=[NSString getValidStringValue:[currentDict valueForKey:@"Custom_Attribute_1"]];
                posmMediaFile.Download_Media_Type=kMerchandisingPOSMDownloadMediaType;
                [allMediaFilesArray addObject:posmMediaFile];
            }
        }
        
        //org logo images
        NSMutableArray * orgLogosArray = [[NSMutableArray alloc] init];
        orgLogosArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Value_1 AS FileName from TBL_Custom_Info where Value_2 IS NOT NULL AND Info_Type = 'ORG_LOGO'"];
        
        for (NSMutableDictionary* orgDict in orgLogosArray) {
            MediaFile *posmMediaFile = [[MediaFile alloc]init];
            posmMediaFile.Media_File_ID=[NSString getValidStringValue:[orgDict valueForKey:@"Value_1"]];
            posmMediaFile.Download_Media_Type=kOrgLogoDownloadMediaType;
            [allMediaFilesArray addObject:posmMediaFile];
        }

    }
    return allMediaFilesArray;
}


-(void)downloadMedia {
    
    NSLog(@"download media called");
    
    [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeIndeterminate;
    [DataSyncManager sharedManager].progressHUD.detailsLabelText =@"";
    
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    
    //Delete Media Files for is_delted= Y
    NSMutableArray * arrayToDelete = [self fetchMediaFilesToDelete];
    for (MediaFile * mToDelete in arrayToDelete) {
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        
        // Get dir
        NSString *resourceDocPath;
        
        
        
        //iOS 8 support
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            
            
            
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        
        NSString *fileName =mToDelete.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:fileName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            NSError *error = nil;
            if(![fileManager removeItemAtPath: filePath error:&error]) {
                NSLog(@"Delete failed:%@", error);
            } else {
                NSLog(@"image removed: %@", filePath);
                //Delete from Table
                NSString *updateMediaFile =[NSString stringWithFormat:@"DELETE FROM TBL_Media_Files WHERE Media_File_ID='%@'",mToDelete.Media_File_ID];
                BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
                if (status) {
                }
            }
        }
        else
        {
        }
    }
    
    
    
    //update the table for files which already exists, in case if media download process is going on and user tapped on full sync again.
    
    
    NSMutableArray * arrayOfMediaFiles=[[NSMutableArray alloc]init];
    
    NSMutableArray* arrayofDownloadedMediaFiles= [self fetchDownloadedMediaFiles];
    

    
    
    for (MediaFile * mToDownload in arrayofDownloadedMediaFiles) {
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        
        // Get dir
        NSString *resourceDocPath;
        
        
        
        //iOS 8 support
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            
            
            
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        
        NSString *fileName =mToDownload.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:fileName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            
            //just update the db
            
            NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mToDownload.Media_File_ID];
            BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
            if (status) {
                
                NSLog(@"db updated for media file  without downloading %@", mToDownload.Media_File_ID);
                
            }
        }
        else
        {

        }
    }

    
    
    SWAppDelegate *app=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"is Background process running %hhd, is now sync %hhd",app.isBackgroundProcessRunning,app.isNewSync );
    
    NSLog(@"media files in syn %d",[[self fetchMediaFilesFromDocuments] count]);

    [app startMediaDownloadProcess];
    
    
    
//    
//     arrayOfMediaFiles = [self fetchMediaFilesFromDocuments];
//    
//    
//    NSLog(@"array of media files %@", [arrayOfMediaFiles description]);
//    
//    
//    if (arrayOfMediaFiles.count > 0) {
//        [DataSyncManager sharedManager].progressHUD.labelText = @"Downloading";
//        [self finalLogPrint:@"Sync Status : Downloading Media Files..."];
//    }
//    
//    NSMutableArray * mediaFilestoDownload=[[NSMutableArray alloc]init];
//    
//    
//    
//    
//    SWAppDelegate *app=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];
//    app.currentDownloadingDocumentsArray=arrayOfMediaFiles;
//    
//    
//    
//    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%lu", (unsigned long)arrayOfMediaFiles.count] forKey:@"getTotalCount"];
//    
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:@"StartDownLoadNotification"
//         object:self];
//    }];
//    
//
    
    
    
    /*
    for (MediaFile * mfile in arrayOfMediaFiles) {
        
        NSLog(@"downloading media with url : %@", mfile.Media_File_ID);

        
        
        
        NSString *strUrlForImage =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mfile.Media_File_ID,@"M"]];

        [mediaFilestoDownload addObject:strUrlForImage];
        
        NSLog(@"url's to download %@", strUrlForImage);
        
        
        if ([mfile.Media_Type isEqualToString:@"Image"]) {
            
            
            
            [self downloadProductImageWithName:mfile];
            
        }else if ([mfile.Media_Type isEqualToString:@"Video"]) {
            [self downloadVideoOrPdf:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Brochure"]) {
            [self downloadVideoOrPdf:mfile];
        }else if([mfile.Media_Type isEqualToString:@"Powerpoint"]){
            NSLog(@"got ppt ");
            [self downloadPowerpointFile:mfile];
            
        }else{
            
        }
    }*/
    
    
   // [self sendRequestForComplete];
    
}


-(void)clearThumbnailsDirectory
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:kThumbnailsFolderName];
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error) {
            // it failed.
        }
    }
}

-(void)downloadProductImageWithName:(MediaFile *)mfile
{
    
    //NSString  *serverAPIForImage=@"http://swx-db.cloudapp.net:14008/MCRM/Sync/";
   
    NSString *strUrlForImage =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mfile.Media_File_ID,@"M"]];
    
    NSLog(@"image download url is %@", strUrlForImage);
    
    NSLog(@"downloading media with  file id %@", mfile.Media_File_ID);
    
    
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrlForImage] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:1200];
    
    
    AFImageRequestOperation *operationImag = [AFImageRequestOperation imageRequestOperationWithRequest:request123 imageProcessingBlock:nil
success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                                                                   // Get dir
                                    NSString *documentsDirectory = nil;
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                                   //iOS 8 support
                                                                                                   
                                                                                                   
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                    documentsDirectory=[SWDefaults applicationDocumentsDirectory];
                                                                                                   }
                                                                                                   else
                                                                                                   {
                                                                                                       
                                                                                                       
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                                                                                       documentsDirectory = [paths objectAtIndex:0];
                                                                                                   }
                                                                                                   
    NSString *pathString = [NSString stringWithFormat:@"%@/%@",documentsDirectory, mfile.Filename];
                                                                                                   // Save Image
                            NSData *imageData = UIImageJPEGRepresentation(image, 90);
                                [imageData writeToFile:pathString atomically:YES];
    
    
  
    
                                                                                                   //Update Table
                                                                                                   NSLog(@"DownloadComplete Image");
                                                                                                   NSLog(@"Test ...... DownloadComplete %@",request.URL);
                                                                                                   NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mfile.Media_File_ID];
                                                                                                   BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
                                                                                                   if (status) {
                                                                                                   }
                                                                                                   
                                                                                               }
                                                                                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                                                                   NSLog(@"Test ...... %@,request %@", [error localizedDescription],request.URL);
                                                                                               }];
    
    [operationImag start];
    
}


-(void)downloadVideoOrPdf:(MediaFile *)mfile
{
    //NSString  *serverAPIForImage=@"http://swx-db.cloudapp.net:14008/MCRM/Sync/";
    NSString *strUrlForImage =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mfile.Media_File_ID,@"M"]];
    
    NSLog(@"url for downloading video %@", strUrlForImage);
    

    
    NSLog(@"media file ID for video is %@", mfile.Media_File_ID);
    
    // Get dir
    NSString *resourceDocPath;
    
    
    
    //iOS 8 support
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        resourceDocPath=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        
        
        
        resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    }
    
    
    
    
    
    
    
    
    
    //NSString *pdfName =databaseName;
    NSString *pdfName = mfile.Filename;
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
    
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrlForImage] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:1000];
    
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:request123] ;
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject) {
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        
        
        
        NSString *resourceDocPath;
        
        
        
        //iOS 8 support
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            
            
            
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        
        
        
        
        
        
        
        
        //            NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        NSString *pdfName =mfile.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            NSLog(@"DownloadComplete Video Or PDF");
            //Update Table
            NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mfile.Media_File_ID];
            BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
            if (status) {
            }
        }
        else
        {
        }
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                                         NSLog(@"Error: %@", error);
                                         NSLog(@"completion request with error called %@ %@", operationQ.responseString,error.debugDescription);

                                         //[self sendRequestCompleteWithError:[[operationQ error] localizedDescription]];
                                         single = [Singleton retrieveSingleton];
                                         
                                     }];
    
    [operation start];
}


-(void)downloadPowerpointFile:(MediaFile*)mfile
{
    
    NSLog(@"server api for ppt is %@", serverAPI);
    
    NSString *strUrlForPpt =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mfile.Media_File_ID,@"M"]];
    
    NSLog(@"media file ID is %@", mfile.Media_File_ID);
    
    NSLog(@"string url is %@", strUrlForPpt);
    
    NSString *resourceDocPath;
    
    
    
    //iOS 8 support
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        resourceDocPath=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        
        
        
        resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    }
    
    
    
    
    
    
    
    
    
    //    NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    //NSString *pdfName =databaseName;
    NSString *pdfName = mfile.Filename;
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
    
    NSURLRequest *requestPPT = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrlForPpt] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:1000];
    
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:requestPPT] ;
    
    NSLog(@"operation url is %@", operation.request.URL);
    
    NSLog(@"file path is %@", filePath);
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject) {
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        NSString *resourceDocPath;
        
        
        
        //iOS 8 support
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            
            
            
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        
        
        
        
        
        
        //
        //        NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        NSString *pdfName =mfile.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            NSLog(@"DownloadComplete ppt");
            //Update Table
            NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mfile.Media_File_ID];
            BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
            if (status) {
            }
        }
        else
        {
        }
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                                         NSLog(@"Error: %@", error);
                                         //[self sendRequestCompleteWithError:[[operationQ error] localizedDescription]];
                                         single = [Singleton retrieveSingleton];
                                         
                                     }];
    
    [operation start];
    
}


#pragma mark media file download complete

- (void)sendRequestForComplete
{
    NSString *strurl =[serverAPI stringByAppendingString:@"Complete"];
    
    NSLog(@"send request for complete url %@", strurl);
    
    
    NSURL *url = [NSURL URLWithString:strurl];
    
    if (self.request)
    {
        self.request = nil;
    }
    // self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    

    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            synchReference, @"SyncReferenceNo",
                            nil];
    
    [self.request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         appData.statusDict = [responseString JSONValue];
         
         NSLog(@"Upload response %@", appData.statusDict);
         
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             isCompleted=YES;
             //NSLog(@"hideCustomIndicator 6");
             [self stopAnimation];
             
             NSLog(@"send request for complete called from issync 1");
             
             
             [self sendRequestForStatus];
         }
         else if(single.isFromSync==2)
         {
             
             NSLog(@"send request for complete called from issync 2");

             isCompleted=YES;
             isForSyncUpload =0;
             single.isForSyncUpload=0;
             [self sendRequestForStatus];
         }
         
     }
                   failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
                       //NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
                       
                       NSLog(@"completion request with error called  in send request for complete %@ %@", operation.responseString,error.debugDescription);
                       
                       NSLog(@" completion request with error called with status code %d", [operationQ.response statusCode]);
                       
                       
                       if ([operationQ.response statusCode]==500) {
                           
                       }
                     
                       else
                       {
                           [self sendRequestCompleteWithError:[operationQ responseString]];

                       }


                       Singleton *single = [Singleton retrieveSingleton];
                       if(single.isFromSync==1)
                       {
                           
                       }
                       else
                       {
                           
                       }
                   }];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    
    NSString *directory;
    
    
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Signature/"];
    }
    
    else
    {
        directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature/"];
    }
    
    
    
    
    //    NSString *directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature/"];
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
    {@autoreleasepool {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error)
        {
            //NSLog(NSLocalizedString(@"Error", nil));
        }
    }
    }
}
- (void)sendRequestForUploadFile
{
    [self finalLogPrint:@"Uploading Files"];
    
    
    
    //uploading signature here
    
    NSArray *signatureImageArray= [[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSString *path = [signatureImageArray objectAtIndex:uploadFileCount];
    
    
    //NSLog(@"File Path %@",path);
    
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            FileType, @"FileType",
                            nil];
    
    
    NSLog(@"upload file parameters %@", [params description]);
    
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    if (self.request)
    {
        self.request = nil;
    }
    //self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [self.request multipartFormRequestWithMethod:@"POST"
                                                                             path:nil
                                                                       parameters:params
                                                        constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:[path lastPathComponent]
                                                                  fileName:[path lastPathComponent]
                                                                  mimeType:@"image/jpeg"];
                                      }
                                      ];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
         }
         else
         {
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         appData.statusDict = [responseString JSONValue];
         ////NSLog(@"Upload response %@", appData.statusDict);
         
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             
         }
         else
         {
             NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
             NSString *strImageName =[signatureArray objectAtIndex:uploadFileCount];
             [self finalLogPrint:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]]];
             if(uploadFileCount != [signatureArray count]-1)
             {
                 uploadFileCount++;
                 [self sendRequestForUploadFile];
             }
             else
             {
                 [self sendRequestForComplete];
             }
         }
         
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"completion request with error called in upload file %@ %@", operation.responseString,error.debugDescription);

         
         [self sendRequestCompleteWithError:[operationQ responseString]];
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             
         }
         else
         {
             
         }
         //NSLog(@"error: %@", error.localizedDescription);
     }];
    
    [operation start];
    
    
}
- (void)sendRequestCompleteWithError:(NSString *)error
{
    //NSLog(@"Error : %@",error);
    
    
    NSLog(@"send complete request with error called %@", error.debugDescription);
    
    if(timerObj)
    {
        [timerObj invalidate];
        timerObj=nil;
    }
    Singleton *single = [Singleton retrieveSingleton];
    if(single.isFromSync==1)
    {
        //[self writeActivationLogToTextFile:[NSString stringWithFormat:@"Error : %@.Check your connection or sever link",error]];
        
        if (!ErrorAlert) {
            
            
            
            
            
            
            ErrorAlert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:@"Please check server settings or Internet connection and try again."] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            ErrorAlert.tag=1000;
            
            //to be done
            [ErrorAlert show];
           
            
            
            
        }
        
        //        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
        //                           message:[NSString stringWithFormat:@"%@.Please check the server settings entered and the Internet connection and try again.",error]
        //                  closeButtonTitle:NSLocalizedString(@"Ok", nil)
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:nil];
    }
    else
    {
        NSLog(@"failed here at 2100");
        
        [self finalLogPrint:[NSString stringWithFormat:@"Error.Please check server settings or Internet connection and try again."]];
    }
    [self stopAnimation];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue
{
    
    NSString *syncType = @"Send_Orders";
    if([procName isEqualToString:@"sync_MC_GetLastDocumentReferenceAll"])
    {
        syncType = @"Get_LastDocumentReferenceAll";
        [self finalLogPrint:@"Retrieving orders references number"];
    }
    
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    if (self.request)
    {
        self.request = nil;
    }
    // self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strValues=[[NSString alloc] init];
    NSString *strName=procName;
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",procPram]; //
    strValues=[strValues stringByAppendingFormat:@"&ProcValues=%@",procValue];
    NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]];
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,syncType,[testServerDict stringForKey:@"name"],strName,strProcedureParameter];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [self.request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         // //NSLog(@"Result Array %@",resultArray);
         
         if([procName isEqualToString:@"sync_MC_GetLastDocumentReferenceAll"])
         {
             
             
             resultArray = [responseText JSONValue];
             //NSLog(@"Result Array %@",resultArray);
             [self finalLogPrint:@"Order reference has been retrieved"];
           //  Singleton *single = [Singleton retrieveSingleton];
             single.isFullSyncDone = YES;
             single.isFullSyncCollection = YES;
             single.isFullSyncCustomer = YES;
             single.isFullSyncSurvey = YES;
             single.isFullSyncProduct = YES;
             single.isFullSyncDashboard = YES;
             single.isFullSyncMessage = YES;
             for (int i =0 ; i <[resultArray count]; i++)
             {
                 @autoreleasepool {
                     NSDictionary *data = [resultArray objectAtIndex:i];
                     
                     
                     NSLog(@"SYNC RESPONSE DATA, CHECK FOR DOC NUMBER FOR (I) FOR ADDING SAMPLES GIVEN IN MEDREP %@", [data description]);
                     
                     
                     if([[data stringForKey:@"Doc_Type"] isEqualToString:@"ISSUE_NOTE"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             
                             [MedRepDefaults setIssueNoteReference:[data stringForKey:@"Doc_Reference_No"]];
                             
                         }
                         else
                         {
                             [MedRepDefaults setIssueNoteReference:@"M000I0000000000"];

                         }
                     }
                     
                     
                     
                     if([[data stringForKey:@"Doc_Type"] isEqualToString:@"COLLECTION"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastCollectionReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastCollectionReference:@"M000C0000000000"];
                         }
                     }
                     
                     else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"ORDER"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastOrderReference:@"M000S0000000000"];
                         }
                     }
                     else  if([[data stringForKey:@"Doc_Type"] isEqualToString:@"PROFORMA_ORDER"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastPerformaOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastPerformaOrderReference:@"M000D0000000000"];
                         }
                     }
                     else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"RMA"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastReturnOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastReturnOrderReference:@"M000R0000000000"];
                         }
                     }
                     
                 }
             }
             
             //             NSLog(@"lastCollectionReference %@",[SWDefaults lastCollectionReference]);
             //             NSLog(@"lastOrderReference %@",[SWDefaults lastOrderReference]);
             //             NSLog(@"lastPerformaOrderReference %@",[SWDefaults lastPerformaOrderReference]);
             //             NSLog(@"lastReturnOrderReference %@",[SWDefaults lastReturnOrderReference]);
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastCollectionReference %@",[SWDefaults lastCollectionReference]]];
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastOrderReference %@",[SWDefaults lastOrderReference]]];
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastPerformaOrderReference %@",[SWDefaults lastPerformaOrderReference]]];
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastReturnOrderReference %@",[SWDefaults lastReturnOrderReference]]];
             
             
             
             //Singleton *single = [Singleton retrieveSingleton];
             if (!single.isActivated)
             {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                 [self.target performSelector:self.action withObject:nil];
#pragma clang diagnostic pop
             }
         }
         if([procName isEqualToString:@"sync_MC_ExecSendOrders"])
         {
             resultArray = [responseText JSONValue];
             ////NSLog(@"Result Array %@",resultArray);
             NSString *ProcResponse = [[resultArray objectAtIndex:0] stringForKey:@"ProcResponse"];
             if([ProcResponse isEqualToString:@"Order imported successfully"])
             {
                 [self finalLogPrint:@"Order sent successfully"];
                 [[SWDatabaseManager retrieveManager] deleteSalesOrder];
                 NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
                 if([signatureArray count]!=0)
                 {
                     [self uploadFile];
                 }
                 else
                 {
                     NSDateFormatter *formatter = [NSDateFormatter new];
                     [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                     NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                     [formatter setLocale:usLocale];
                     NSString *dateString =  [formatter stringFromDate:[NSDate date]];
                     
                     [SWDefaults setLastSyncOrderSent:orderCountString];
                     [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
                     
                     orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
                     [pendingOrder setText:orderCountString];
                     
                     [SWDefaults setLastSyncDate:dateString];
                     lastSyncLable.text = [SWDefaults lastSyncDate];
                     
                     [SWDefaults setLastSyncStatus:NSLocalizedString(@"Successful", nil)];
                     lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
                     
                     [self finalLogPrint:@"Order upload successfully completed"];
                     
                     
                     //add a notification here to check the status in visit options, send order button action
                     
                     
                     NSMutableDictionary* sendOrderStatusDict=[[NSMutableDictionary alloc]init];
                     
                     
                     [sendOrderStatusDict setObject:orderCountString forKey:@"OrderCount"];
                     
                     [sendOrderStatusDict setObject:lastSyncLable.text forKey:@"Time"];
                     
                     [sendOrderStatusDict setObject:lastSyncStatusLable.text forKey:@"Status"];
                     
                     
                     
                     
                     NSDictionary *sendOrder = [NSDictionary dictionaryWithObject:sendOrderStatusDict forKey:@"sendOrderStatus"];
                     
                     
                   //  SWVisitOptionsViewController * swVisitOptionsVC=[[SWVisitOptionsViewController alloc]init];
                     
                     
                    

                     NSLog(@"notification added");

                     
                     
                   //  [[NSNotificationCenter defaultCenter] addObserver:swVisitOptionsVC selector:@selector(notificationReceived:) name:@"ordersSentSuccessfully" object:lastSyncStatusLable.text];
                     
                     
                     
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"ordersSentSuccessfully" object:lastSyncStatusLable.text];
                     
                     


                     
                     
                     [[DataSyncManager sharedManager] hideCustomIndicator];
                     formatter=nil;
                     usLocale=nil;
                     [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
                     
                 }
             }
             else
             {
                 [self finalLogPrint:@"Order uplaod failed"];
                 [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
                 [[DataSyncManager sharedManager] hideCustomIndicator];
                 [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
                 UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:ProcResponse delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
                 [ErrorAlert show];
                 //                 [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
                 //                                    message:ProcResponse
                 //                           closeButtonTitle:NSLocalizedString(@"Ok", nil)
                 //                          secondButtonTitle:nil
                 //                        tappedButtonAtIndex:nil];
                 
             }
         }
         
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         //NSLog(@"error: %@", error.localizedDescription);
         
         [self finalLogPrint: [NSString stringWithFormat:@"Process failed.%@",error.localizedDescription]];
         [[DataSyncManager sharedManager] hideCustomIndicator];
         [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
         [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
         lastSyncStatusLable.text=[SWDefaults lastSyncStatus];
         UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:@"Server unreachable at the moment" delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
         [ErrorAlert show];
         //         [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
         //                            message:@"Server unreachable at the moment"
         //                   closeButtonTitle:NSLocalizedString(@"Ok", nil)
         //                  secondButtonTitle:nil
         //                tappedButtonAtIndex:nil];
         
     }];
    
    //call start on your request operation
    [operation start];
    
    
}
-(void)uploadFile
{
    NSArray *signatureImageArray= [[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSURL *url = [NSURL URLWithString: strUrl];
    
    NSString *path = [signatureImageArray objectAtIndex:uploadFileCount];
    [self finalLogPrint:[NSString stringWithFormat:@"Files to be uploaded : %d",[signatureImageArray count]-uploadFileCount ]];
    [self finalLogPrint:@"Uploading Files"];
    
    usernameActivate = [[SWDefaults userProfile] stringForKey:@"Username"];
    passwordActivate = [[SWDefaults userProfile] stringForKey:@"Password"];
    
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            FileType, @"FileType",
                            nil];
    
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    if (self.request)
    {
        self.request = nil;
    }
    // self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [self.request multipartFormRequestWithMethod:@"POST"
                                                                             path:nil
                                                                       parameters:params
                                                        constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:[path lastPathComponent]
                                                                  fileName:[path lastPathComponent]
                                                                  mimeType:@"image/jpeg"];
                                      }
                                      ];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
         }
         else
         {
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         //NSString *responseString =[self.request responseString];
         // //NSLog(@"Upload response %@", responseString);
         NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
         NSString *strImageName =[signatureArray objectAtIndex:uploadFileCount];
         [self finalLogPrint:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]]];
         
         if(uploadFileCount != [signatureArray count]-1)
         {
             uploadFileCount++;
             [self uploadFile];
         }
         else
         {
             [self finalLogPrint:@"Order upload successfully completed"];
             NSFileManager *fm = [NSFileManager defaultManager];
             
             
             
             
             NSString *directory;
             
             
             //iOS 8 support
             
             if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                 directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Signature/"];
             }
             
             else
             {
                 directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature/"];
             }
             
             
             
             
             
             
             NSError *error = nil;
             
             for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
             {
                 @autoreleasepool {
                     
                     BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
                     if (!success || error)
                     {
                         // it failed.
                         //NSLog(NSLocalizedString(@"Error", nil));
                     }
                 }
             }
             [[DataSyncManager sharedManager] hideCustomIndicator];
             
             NSDateFormatter *formatter = [NSDateFormatter new];
             [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
             NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
             [formatter setLocale:usLocale];
             NSString *dateString =  [formatter stringFromDate:[NSDate date]];
             
             [SWDefaults setLastSyncOrderSent:orderCountString];
             [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
             
             orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
             [pendingOrder setText:orderCountString];
             
             [SWDefaults setLastSyncDate:dateString];
             lastSyncLable.text = [SWDefaults lastSyncDate];
             
             [SWDefaults setLastSyncStatus:NSLocalizedString(@"Successful", nil)];
             lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
             
             
             
             
             formatter=nil;
             usLocale=nil;
             [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
         }
         
         
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
         [pendingOrder setText:orderCountString];
         
         NSDateFormatter *formatter = [NSDateFormatter new];
         [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
         NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
         [formatter setLocale:usLocale];
         NSString *dateString =  [formatter stringFromDate:[NSDate date]];
         [SWDefaults setLastSyncDate:dateString];
         lastSyncLable.text = [SWDefaults lastSyncDate];
         
         [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
         lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
         
         [self finalLogPrint:[[operationQ error] localizedDescription]];
         [[DataSyncManager sharedManager] hideCustomIndicator];
         formatter=nil;
         usLocale=nil;
         [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
         //NSLog(@" Error - Statistics file upload failed: \"%@\"",[[operation error] localizedDescription]);
     }];
    
    [operation start];
}
- (void)backtoActivation
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    
    [[DataSyncManager sharedManager] hideCustomIndicator];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:@"done"];
#pragma clang diagnostic pop
}
- (void)ProcessCompleted
{
    Singleton *single = [Singleton retrieveSingleton];
    single.isFullSyncDone = YES;
    single.isFullSyncCollection = YES;
    single.isFullSyncCustomer = YES;
    single.isFullSyncSurvey = YES;
    single.isFullSyncProduct = YES;
    single.isFullSyncDashboard = YES;
    single.isFullSyncMessage = YES;
    
    if(!single.isActivated)
    {
        [SWDatabaseManager destroyMySingleton];
    }
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    if(timerObj)
    {
        [timerObj invalidate];
        timerObj=nil;
    }
    [activityImageView stopAnimating];
    
    [SWDefaults setLastSyncOrderSent:orderCountString];
    [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
    [self finalLogPrint:@"Sync Status : Completed"];
    
    
    
    self.navigationItem.leftBarButtonItem.enabled=YES;

    [DataSyncManager sharedManager].progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Old_37x-Checkmark"]];
    [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeCustomView;
    [DataSyncManager sharedManager].progressHUD.labelText = @"Completed";
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    [[DataSyncManager sharedManager].progressHUD hide:YES afterDelay:0.0];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    [SWDefaults setLastSyncDate:dateString];
    [SWDefaults setLastSyncStatus:NSLocalizedString(@"Successful", nil)];
    lastSyncLable.text = [SWDefaults lastSyncDate];
    lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
    progressBar.progress=1.0;
    
    [SWDefaults setCollectionCounter:0];
    
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    // collectionCountString = [[SWDatabaseManager retrieveManager] dbdbGetCollectionCount];
    collectionCountString=nil;
    collectionCountString = @"0";
    [pendingOrder setText:orderCountString];
    [pendingReturn setText:returnCountString];
    [pendingCollection setText:collectionCountString];
    [pendingCollection setText:@"0"];
    
    
    //update the nsuser defauls for collection count as well because tbl collection will have data, even after full sync, it will show all the collections
    
    [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"collectionCount"];
    
    
    [[DataSyncManager sharedManager]hideCustomIndicator];
    //NSLog(@"hideCustomIndicator 9");
    //Singleton *single = [Singleton retrieveSingleton];
    if(!single.isActivated)
    {
        
        [self performSelector:@selector(backtoActivation) withObject:nil afterDelay:0.0];
    }
    else
    {
        
    }
    [SWDefaults setAppControl:[[SWDatabaseManager retrieveManager] dbGetAppControl]];
    [AppControl destroyMySingleton];
    //[Singleton destroyMySingleton];
    
    // NSLog(@"App Controll %@",[SWDefaults appControl]);
    formatter=nil;
    usLocale=nil;
}



- (void)postSycStatus:(NSNotification *)notification
{
    
    NSLog(@"*******SYC STATUS NOTIFICATION ********** %@", [notification object]);
    
    
}

- (void)finalLogPrint:(NSString *)text
{
    
    //print syc status here
    

    [[NSNotificationCenter defaultCenter]postNotificationName:@"SyncStatusNotification" object:text];
    
    
    Singleton *single = [Singleton retrieveSingleton];
    
    
    logTextView.font = RegularFontOfSize(14);
    // //NSLog(@"addLogToTextView Calls with : %@",text);
    NSString *slashN = @"\n";
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ : %@",dateString , text]];
    
    logTextView.text = [logTextView.text stringByAppendingString:slashN];
    CGPoint p = [logTextView contentOffset];
    [logTextView setContentOffset:p animated:YES];
    [logTextView scrollRangeToVisible:NSMakeRange([logTextView.text length], 0)];
    
    if(single.isFromSync==1)
    {
        [self writeActivationLogToTextFile:text];
        
        //to be done check activation text delegate
        
//        if([self.delegate respondsToSelector:@selector(updateActivationTextDelegate:)])
//        {
//            [self.delegate updateActivationTextDelegate:slashN];
//        }
    }
    else
    {
        [self writeSyncLogToTextFile:text];
//        if([self.delegate respondsToSelector:@selector(updateActivationTextDelegate:)])
//        {
//            [self.delegate updateActivationTextDelegate:slashN];
//        }
    }
    formatter=nil;
    usLocale=nil;
}
-(void) writeActivationLogToTextFile:(NSString *)textString
{
    //get the documents directory:
    
    NSString *slashN = @"\n";
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ : %@",dateString , textString]];
    
    
    
    
    NSString *documentsDirectory;
    
    
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/Activation_Log.txt",documentsDirectory];
    
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [fileHandler seekToEndOfFile];
    [fileHandler writeData:[slashN dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandler closeFile];
    formatter=nil;
    usLocale=nil;
    
}
-(void) writeSyncLogToTextFile:(NSString *)textString
{
    //get the documents directory:
    
    NSString *slashN = @"\n";
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ : %@",dateString , textString]];
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    
    //	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //	NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/Sync_Log.txt",documentsDirectory];
    
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [fileHandler seekToEndOfFile];
    [fileHandler writeData:[slashN dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandler closeFile];
    formatter=nil;
    usLocale=nil;
    
}
- (void)stopAnimation
{
    
    
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    
    if(timerObj)
    {
        [timerObj invalidate];
        timerObj=nil;
    }
    [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
    progressBar.progress = 0.0;
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    [SWDefaults setLastSyncDate:dateString];
    
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    
    
    //data from tbl collection will not be cleared even after full sync so do it with a flag set it to 0 when full sync is done
    
    
    
    
    //    collectionCountString = [[SWDatabaseManager retrieveManager] dbdbGetCollectionCount];
    
    
    
    NSInteger collectionCount=[[NSUserDefaults standardUserDefaults]integerForKey:@"collectionCount"];
    
    if (collectionCount) {
        collectionCountString=[NSString stringWithFormat:@"%d",collectionCount];
        
    }
    
    
    [pendingOrder setText:orderCountString];
    [pendingReturn setText:returnCountString];
    [pendingCollection setText:collectionCountString];
    
    [activityImageView stopAnimating];
    [[DataSyncManager sharedManager]hideCustomIndicator];
    
    formatter=nil;
    usLocale=nil;
    Singleton *single = [Singleton retrieveSingleton];
    
    if(!single.isActivated)
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
    }
}
- (void)stopAnimationofSendOrder
{
    
    
    //add a notification here to check whether order has been send to check status from visit options screen
    
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

    
    
    
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    // [Singleton destroyMySingleton];
    //    [DashboardService destroyMySingleton];
    //    [SWSessionService destroyMySingleton];
    //    [SWCustomerService destroyMySingleton];
    //    [ProductService destroyMySingleton];
    //    [DistributionService destroyMySingleton];
    //    [SalesOrderService destroyMySingleton];
    //    [SendOrderService destroyMySingleton];
    //    [SWCustomerService destroyMySingleton];
    //    [SWRouteService destroyMySingleton];
    
    [activityImageView2 stopAnimating];
}
- (NSData *)dataFromHexString:(NSString *)string
{
    string = [string lowercaseString];
    NSMutableData *data= [NSMutableData new];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i = 0;
    int length = string.length;
    while (i < length-1) {
        char c = [string characterAtIndex:i++];
        if (c < '0' || (c > '9' && c < 'a') || c > 'f')
            continue;
        byte_chars[0] = c;
        byte_chars[1] = [string characterAtIndex:i++];
        whole_byte = strtol(byte_chars, NULL, 16);
        [data appendBytes:&whole_byte length:1];
        
    }
    return data;
}
- (NSString*)hexRepresentationWithSpaces_AS:(BOOL)spaces :(NSData *)data
{
    const unsigned char* bytes = (const unsigned char*)[data bytes];
    NSUInteger nbBytes = [data length];
    //If spaces is true, insert a space every this many input bytes (twice this many output characters).
    static const NSUInteger spaceEveryThisManyBytes = 4UL;
    //If spaces is true, insert a line-break instead of a space every this many spaces.
    static const NSUInteger lineBreakEveryThisManySpaces = 4UL;
    const NSUInteger lineBreakEveryThisManyBytes = spaceEveryThisManyBytes * lineBreakEveryThisManySpaces;
    NSUInteger strLen = 2*nbBytes + (spaces ? nbBytes/spaceEveryThisManyBytes : 0);
    
    NSMutableString* hex = [[NSMutableString alloc] initWithCapacity:strLen];
    for(NSUInteger i=0; i<nbBytes; ) {
        [hex appendFormat:@"%02X", bytes[i]];
        //We need to increment here so that the every-n-bytes computations are right.
        ++i;
        
        if (spaces) {
            if (i % lineBreakEveryThisManyBytes == 0) [hex appendString:@"\n"];
            else if (i % spaceEveryThisManyBytes == 0) [hex appendString:@" "];
        }
    }
    return hex;
}
- (void)StartOrderSyncAnimation
{
    //    activityImageView2.animationImages=nil;
    //    activityImageView2.animationImages = [NSArray arrayWithObjects:
    //                                          [UIImage imageNamed:@"icon-000001.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000002.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000003.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000004.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000005.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000006.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000007.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000008.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000009.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000010.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000011.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000012.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000013.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000014.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000015.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000016.png" cache:NO],
    //                                          [UIImage imageNamed:@"icon-000017.png" cache:NO]
    //                                          ,nil];
    //    //activityImageView.animationDuration = 0.8;
    //    [activityImageView2 setAnimationDuration:1.1f];
    //
    //    [activityImageView2 startAnimating];
}
- (void)StartSyncAnimation
{
    
    
    self.navigationItem.leftBarButtonItem.enabled=NO;

    
    activityImageView.animationImages=nil;
    activityImageView.animationImages = [NSArray arrayWithObjects:
                                         [UIImage imageNamed:@"Old_Syncbutton-1"],
                                         [UIImage imageNamed:@"Old_Syncbutton-2"],
                                         [UIImage imageNamed:@"Old_Syncbutton-3"],
                                         [UIImage imageNamed:@"Old_Syncbutton-4"],
                                         [UIImage imageNamed:@"Old_Syncbutton-5"],
                                         [UIImage imageNamed:@"Old_Syncbutton-6"],
                                         [UIImage imageNamed:@"Old_Syncbutton-7"],
                                         [UIImage imageNamed:@"Old_Syncbutton-8"],
                                         [UIImage imageNamed:@"Old_Syncbutton-9"],
                                         [UIImage imageNamed:@"Old_Syncbutton-10"],
                                         [UIImage imageNamed:@"Old_Syncbutton-11"]
                                         ,nil];
    [activityImageView setAnimationDuration:1.1f];
    [activityImageView startAnimating];
}
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    
    if (alertView.tag==1000) {
        alertView=nil;
    }
    
    //do whatever with the result
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    //[self.tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
}
- (void)RequestFailureAlert:(NSString *)Message
{
    if(Message.length!=0)
    {
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:@"Start",nil];
        alertType=requestFailureAlert;
        [ErrorAlert show];
        
    }
}
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//   Singleton *single = [Singleton retrieveSingleton];
//    switch (alertType)
//    {
//        case requestFailureAlert:
//        {
//            if(buttonIndex==1)
//            {
//                if(isForSyncUpload==1 || single.isForSyncUpload==1)
//                {
//                    [self setRequestForSyncUpload];
//                }
//                else if(isForSyncUpload==0 || single.isForSyncUpload==0)
//                {
//                    [self setRequestForSyncActivateWithUserName:usernameActivate andPassword:passwordActivate];
//
//                }
//            }
//        }
//            break;
//
//        case databaseExistAlert:
//        {
//            if(buttonIndex==1)
//            {
//
//
//            }
//            else
//            {
//                [[DataSyncManager sharedManager] deleteDatabaseFromApp];
//                [self setRequestForSyncActivateWithUserName:usernameActivate andPassword:passwordActivate];
//            }
//        }
//            break;
//
//        default:
//            break;
//    }
//}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    self.request=nil;
    operation= nil;
    logTextView.text=nil;
    
    
}
-(void)cancelHTTPRequest
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    self.request=nil;
    operation=nil;
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
