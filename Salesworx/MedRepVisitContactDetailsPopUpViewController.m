//
//  MedRepVisitContactDetailsPopUpViewController.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 6/26/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedRepVisitContactDetailsPopUpViewController.h"

@interface MedRepVisitContactDetailsPopUpViewController ()

@end

@implementation MedRepVisitContactDetailsPopUpViewController
@synthesize VisitDetailsDictionary,selectedLocation;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    if(self.isMovingToParentViewController)
    {
        if (self.isObjectData==YES) {
            
            addressLabel.text=[MedRepDefaults getDefaultStringForEmptyString:selectedLocation.Address];
            cityLabel.text=[MedRepDefaults getDefaultStringForEmptyString:selectedLocation.City];
            conatcatLabel.text=[MedRepDefaults getDefaultStringForEmptyString:selectedLocation.Phone];
            
        }
        else
        {
        addressLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[VisitDetailsDictionary valueForKey:@"Address"]];
        cityLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[VisitDetailsDictionary valueForKey:@"City"]];
        conatcatLabel.text=[MedRepDefaults getDefaultStringForEmptyString:[VisitDetailsDictionary valueForKey:@"Phone"]];
        }
    }
}

-(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)view{
    [UIView animateWithDuration:0 animations: ^{
        [self.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:alertTitle
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:KAlertOkButtonTitle
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

#pragma mark buttopnActions
- (IBAction)doneButtontapped:(id)sender {
    
    [self dimissViewControllerWithSlideDownAnimation:YES];
}

@end
