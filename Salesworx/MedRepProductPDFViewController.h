//
//  MedRepProductPDFViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/15/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SWDatabaseManager.h"


@protocol ViewedPDFDelegate <NSObject>

-(void)PDFViewedinFullScreenDemo:(NSMutableArray*)imagesArray;

-(void)fullScreenPDFDidCancel;

@optional
-(void)setBorderOnLastViewedImage:(NSInteger)index;

@end


@interface MedRepProductPDFViewController : UIViewController<MFMailComposeViewControllerDelegate,UIWebViewDelegate,UIScrollViewDelegate>
{
    NSString *filePath;
    IBOutlet UILabel *productHeaderTitle;
    NSTimer* demoTimer;
    NSDate* viewedTime;
    NSInteger demoTimeInterval;

    
    IBOutlet UIButton *emailButton;
    
    UIView * zoomableView
    ;
    NSDate * timerDate;
    
    BOOL sendEmail;
    
    NSString *buttonTitle;
    
    BOOL isEmailTapped;
    
    UIImageView *fullScreenImageView;
   
    id pdfViewedDelegate;
    
    NSMutableArray* viewedImages;
    
    NSIndexPath * selectedProductIndexPath;
    
    NSInteger selectedCellIndex;
    
    NSMutableArray* selectedIndexPathArray;
    
    NSIndexPath * selectedCellIndexPath;

    IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
    
    IBOutlet UIButton *cancelButton;
    NSString *emailIDForAutoInsert;
}
@property(strong,nonatomic) SalesWorxVisit* currentVisit;
@property(strong,nonatomic)NSMutableArray* productDataArray;
@property(nonatomic)    NSInteger currentImageIndex;

@property(strong,nonatomic) id pdfViewedDelegate;
@property (strong, nonatomic) IBOutlet UICollectionView *productPDFCollectionView;
@property(nonatomic) BOOL isViewing;
@property(nonatomic) BOOL isFromProductsScreen;


@property (strong, nonatomic) IBOutlet UIWebView *pdfWebView;

@property(strong,nonatomic)NSMutableArray* productImagesArray;
@property(strong,nonatomic)NSIndexPath* selectedIndexPath;
- (IBAction)closeButtontapped:(id)sender;
@property(nonatomic) BOOL isCurrentUserDoctor;
@property(nonatomic) BOOL isCurrentUserPharmacy;
@property(strong,nonatomic) NSString *doctor_ID ;
@property(strong,nonatomic) NSString *contact_ID ;

@end
