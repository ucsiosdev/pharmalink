//
//  SalesWorxSalesSummaryViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "XYPieChart/XYPieChart.h"
#import "SalesWorxReportsDateSelectorViewController.h"
#import "MedRepDateRangeTextfield.h"
@interface SalesWorxSalesSummaryViewController : UIViewController<StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,XYPieChartDataSource,XYPieChartDelegate, UIPopoverPresentationControllerDelegate,SalesWorxReportsDateSelectorViewControllerDelegate>
{
    NSString * popString, *datePopOverTitle;
    NSMutableArray *unfilteredCustomersSalesSummaryArray, *arrCustomerName, *arrCustomersDetails, *arrCustomerType, *arrDocumentType;
        
    IBOutlet MedRepTextField *txtCustomer;
    IBOutlet MedRepTextField *txtCustomerType;
    IBOutlet MedRepTextField *txtDocumentType;
    IBOutlet MedRepDateRangeTextfield *DateRangeTextField;
    IBOutlet XYPieChart *customerPieChart;
    IBOutlet MedRepElementDescriptionLabel *lblTotalInvoices;
    IBOutlet MedRepElementDescriptionLabel *lblTotalCreditNotes;
    IBOutlet UITableView *tblSalesSummary;
    IBOutlet UIView *parentViewOfPieChart;
    IBOutlet UIImageView *pieChartPlaceholderImage;
    IBOutlet UILabel *lblTotalSalesAmount;
    IBOutlet UIView *viewFooter;
    

}
@property(strong,nonatomic) SalesWorxVisit* currentVisit;


@property(strong,nonatomic) NSMutableArray *arrCustomersSalesSummary;
@property(nonatomic, strong) NSMutableArray *slicesForCustomerPotential;
@property(nonatomic, strong) NSArray        *sliceColorsForCustomerPotential;

@end
