//
//  SalesWorxFieldMarketingDashboardViewController.h
//  MedRep
//
//  Created by USHYAKU-IOS on 12/11/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWAppDelegate.h"
#import "SalesWorxImageBarButtonItem.h"
#import "TaskPopoverViewController.h"
#import "GKLineGraph.h"
#import "MedRepElementDescriptionLabel.h"
#import "PNChart.h"

@interface SalesWorxFieldMarketingDashboardViewController : UIViewController<TaskPopoverDelegate, GKLineGraphDataSource>
{
    SWAppDelegate *appDel;
    SalesWorxImageBarButtonItem *btnTask;
    NSString *monthString;
    
    NSMutableArray *arrVisitFrequency;
    BOOL isEmptyGraph;
    IBOutlet MedRepElementDescriptionLabel *monthLabel;

    
    IBOutlet UIView *viewUpcomingAppointment;
    IBOutlet UILabel *lblDoctorName;
    IBOutlet UILabel *lblLocation;
    IBOutlet UILabel *lblTelephoneNo;
    IBOutlet UILabel *lblAppointmentTime;
    IBOutlet SalesWorxDefaultButton *btnRescheduleVisit;
    NSArray *sortedPlannedVisitArray;
    
    
    IBOutlet UIView *faceTimeView;
    IBOutlet UIView *waitTimeView;
    IBOutlet UILabel *lblFaceTime;
    IBOutlet UILabel *lblWaitTime;
    
    
    IBOutlet UILabel *lblDoctorsVisitsRequired;
    IBOutlet UILabel *lblDoctorsVisitsCompleted;
    
    IBOutlet UILabel *lblPharmaciesVisitsRequired;
    IBOutlet UILabel *lblPharmaciesVisitsCompleted;
    
    IBOutlet UILabel *lblProductsDemonstrated;
    IBOutlet UILabel *lblSamplesGiven;
    
    IBOutlet UILabel *lblTasksOpen;
    IBOutlet UILabel *lblTasksCompleted;
}

@property (strong, nonatomic) IBOutlet GKLineGraph *gkLineGraph;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *labels;

@property (weak, nonatomic) IBOutlet UIView *pieChartParentView;
@property (weak, nonatomic) IBOutlet UIView *viewDemonstratedProduct;
@property (weak, nonatomic) IBOutlet MedRepElementTitleDescriptionLabel *lblNoProductDemonstratedData;

@end
