//
//  SeverEditLocation.m
//  SWPlatform
//
//  Created by msaad on 3/13/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "SeverEditLocation.h"
#import "SWPlatform.h"
//#import "CJSONSerializer.h"
//#import "CJSONDeserializer.h"

@interface SeverEditLocation ()

@end

@implementation SeverEditLocation

@synthesize target;
@synthesize selectionChanged,serverArray;

- (id)init {
    self = [super init];
    
    if (self) {
        [self setTitle:@"Select a Server"];
        CJSONDeserializer *jsonSerializer = [CJSONDeserializer deserializer];
        NSError *error;
        NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
        
        
        self.serverArray = [NSMutableArray arrayWithArray:[jsonSerializer deserializeAsArray:data error:&error]  ];
        
        NSLog(@"selected server array in edit %@", self.serverArray);

    }
    
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.serverArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell)
    {
        cell=nil;
    }
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    [cell.textLabel setText:[[self.serverArray objectAtIndex:indexPath.row]stringForKey:@"name"]];
    [cell.detailTextLabel setText:[[self.serverArray objectAtIndex:indexPath.row]stringForKey:@"url"]];

    return cell;
}
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.selectionChanged withObject:[self.serverArray objectAtIndex:indexPath.row]];
#pragma clang diagnostic pop

}

@end
