//
//  GridCellView.h
//  SWFoundation
//
//  Created by Irfan Bashir on 6/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    GridCellBorderTypeAll = 1,
    GridCellBorderTypeRight = 2,
    GridCellBorderTypeBottom = 4,
    GridCellBorderTypeLeft = 8,
    GridCellBorderTypeTop = 16
}; 
typedef NSUInteger GridCellBorderType;

@interface GridCellView : UIView {
    GridCellBorderType borderType;
    UIColor *borderColor;
    UILabel *titleLabel;
}

@property (nonatomic, assign) GridCellBorderType borderType;
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, strong) UILabel *titleLabel;

@end