//
//  AppControl.m
//  SWPlatform
//
//  Created by msaad on 5/5/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "AppControl.h"
#import "SWDefaults.h"
@implementation AppControl

@synthesize ENABLE_ORDER_HISTORY;
@synthesize ENABLE_COLLECTION;
@synthesize ENABLE_DISTRIB_CHECK;
@synthesize ENABLE_LINE_ITEM_NOTES;
@synthesize ENABLE_MANUAL_FOC;
@synthesize ENABLE_TEMPLATES;
@synthesize ENABLE_WHOLESALE_ORDER;
@synthesize ALLOW_BONUS_CHANGE;
@synthesize ALLOW_DISCOUNT;
@synthesize ALLOW_DUAL_FOC;
@synthesize ALLOW_LOT_SELECTION;
@synthesize ALLOW_MANUAL_FOC_ITEM_CHANGE;
@synthesize ALLOW_SHIP_DATE_CHANGE;
@synthesize ALLOW_SKIP_CONSOLIDATION;
@synthesize IS_SIGNATURE_OPTIONAL;
@synthesize SHOW_ON_ORDER;
@synthesize SHOW_PRODUCT_TARGET;
@synthesize ROUTE_SYNC_DAYS;
@synthesize ENABLE_ORDER_CONSOLIDATION;
@synthesize SD_BUCKET_SIZE;
@synthesize SD_DURATION_MON;
@synthesize SHOW_SALES_HISTORY;
@synthesize ODOMETER_READING;
@synthesize IS_START_DAY,ALLOW_MULTI_CURRENCY;
@synthesize IS_LOT_OPTIONAL;
@synthesize BONUS_MODE;
@synthesize ENABLE_BONUS_GROUPS,ENABLE_PRODUCT_IMAGE,IS_SURVEY_OPTIONAL,CUST_DTL_SCREEN,ENABLE_VISIT_TYPE,ENABLE_ITEM_DISCOUNT;
@synthesize ENABLE_DIST_CHECK_MULTI_LOCATION;
@synthesize DUPLICATE_ITEM_ENTRY_CHECK;
@synthesize ENABLE_PROD_RANK,SHOW_UNCONFIRMED_ORDER_SCREEN_ONSYNC,NEW_DOCTOR_CLASS,ALLOW_RESTRICTED_FOC,FILTER_FOC_LIST,SHOW_SURVEY_ALERT,     SHOW_ON_ORDER_QTY,ENABLE_PDA_RIGHTS,ENABLE_COLLECTIONS,ENABLE_RECOMMENDED_ORDER,ENABLE_DIST_CHECK_MULTI_LOTS,ENABLE_LPO_IMAGES,V_TRX_SYNC_INTERVAL,COMM_SYNC_INTERVAL,VALIDATE_CUST_LOC, ENABLE_FS_VISIT_TIMER, FS_VISIT_TIMER_LIMIT,SHOW_COLLECTION_BANK_DROPDOWN,ALLOW_SPECIAL_DISCOUNT,DISTRIBUTIONCHECK_MANDATORY,DEFAULT_GEO_LOCATION_RANGE,MANDATORY_QTY_IN_DC,MANDATORY_EXP_DT_IN_DC,SHOW_ALERT_FOR_TRD_LIC_EXP,ENABLE_ALERT_NO_BONUS, DISABLE_PAST_DATE_FOR_CHECK,CURR_CHQ_MIN_DAYS,DEFAULT_AVAILABILITY_IN_DC;
@synthesize ENABLE_REORDER_OPTION;
@synthesize RO_DESCRIPTION;
@synthesize MSG_MODULE_VERSION;
@synthesize MESSAGE_DEFAULT_EXPIRY_DAYS;
@synthesize IS_COLLECTIONS_BRANCH_NAME_OPTIONAL;
@synthesize ENABLE_CUST_LEVEL_TRX_RESTRICTION;
@synthesize HIDE_DISCOUNT_FIELDS_AND_LABELS;
@synthesize ENABLE_FS_ACCOMPANIED_BY;
@synthesize SHOW_PRODUCT_NAME_SEARCH_POPOVER;
@synthesize ENABLE_MULTI_UOM;
@synthesize DISABLE_FOC_FOR_RESTRICTED;
@synthesize IS_COLLECTIONS_SIGNATURE_OPTIONAL,DEVICE_TIME_VALIDATION_RANGE;
@synthesize ENABLE_PRODUCT_BARCODE_SCAN;
@synthesize ENABLE_FSR_VISIT_NOTES;
@synthesize ENABLE_ASSORTMENT_BONUS;
@synthesize USE_GEO_BRANCH_LOC;
@synthesize ENABLE_BACKGROUND_SYNC;
@synthesize USER_ENABLED_BACKGROUND_SYNC;
@synthesize ENABLE_MEDREP_NO_VISIT_OPTION;
@synthesize MEDREP_NO_VISIT_LOCATION;
@synthesize ENABLE_MEDREP_INCOMPLETE_VISIT_COMMENTS;
@synthesize OH_DUPLICATE_ITEM_CHECK_PERIOD;
@synthesize START_VISIT_ON_ACTIVITY;
@synthesize FSR_TARGET_PRIMARY;
@synthesize FSR_VALUE_TYPE;
@synthesize FM_ENABLE_HOLIDAY;
@synthesize DISABLE_BLOCKED_CUSTOMER_SALES_ORDER_ENTRY;
@synthesize DISABLE_FM_VISIT_DELETE;
@synthesize VALIDATE_MEDREP_CUST_LOC;
@synthesize ALLOW_EXCESS_CASH_COLLECTION;
@synthesize ENABLE_VAT;
@synthesize VAT_RULE_LEVEL;
@synthesize VAT_LEVEL;
@synthesize MERCH_MSL_CLASSIFICATION,ENABLE_MERCHANDISING;
@synthesize SHOW_TWO_LEVEL_DOCTOR_CLASSIFICATION;
@synthesize ENABLE_SALESORDER_SHIPTO_SELECTION;
@synthesize ENABLE_TGT_ACVMT_DASHBOARD;
@synthesize ENABLE_VISIT_LOC_UPDATE_ON_CLOSE;
@synthesize CLOSE_VISIT_LOC_UPDATE_LIMIT;
@synthesize ENABLE_COLLECTION_IMAGE_CAPTURE;
@synthesize FM_VISIT_CONCLUSION_WORD_LIMIT,FM_ENABLE_WORD_LIMIT,FM_SHOW_NEXT_VISIT_OBJECTIVE,FM_ENABLE_PRESENTATION_MODULE,ENABLE_CUSTOMER_STATEMENT_PDF_EMAIL,SHOW_FS_VISIT_CUSTOMER_CONTACT, FM_ENABLE_DISTRIBUTION_CHECK,FM_ENABLE_GIFT_ITEM_TYPE;
@synthesize ENABLE_COLLECTION_RECEIPT_PDF_EMAIL;
@synthesize ENABLE_ITEM_TYPE,VISIT_CLOSURE_INTERVAL;
@synthesize
FM_DISPLAY_CALLS_VISIT_FREQUENCY,FM_EDETAILING_IMAGE_DEFAULT_DEMO_TIME,FM_EDETAILING_VIDEO_DEFAULT_DEMO_TIME,FM_EDETAILING_PDF_DEFAULT_DEMO_TIME,FM_EDETAILING_PPT_DEFAULT_DEMO_TIME;
@synthesize COLL_INV_SETTLEMENT_MANDATORY, UNALLOC_COLL_MODE;

- (void)initializeAppControl
{
    if([[SWDefaults appControl] count] !=0)
    {
        
        
        
        appControlDict = [NSMutableDictionary dictionaryWithDictionary:[[SWDefaults appControl] objectAtIndex:0]];
        
        NSLog(@"app control in app control class %@", appControlDict);
        
        self.ENABLE_ORDER_HISTORY               = [appControlDict valueForKey:@"ENABLE_ORDER_HISTORY"];
        self.ENABLE_TEMPLATES                   = [appControlDict valueForKey:@"ENABLE_TEMPLATES"];
        self.ENABLE_COLLECTION                  = [appControlDict valueForKey:@"ENABLE_COLLECTIONS"];
        self.ENABLE_DISTRIB_CHECK               = [appControlDict valueForKey:@"ENABLE_DISTRIB_CHECK"];
        self.ALLOW_LOT_SELECTION                = [appControlDict valueForKey:@"ALLOW_LOT_SELECTION"];
        self.ENABLE_WHOLESALE_ORDER             = [appControlDict valueForKey:@"ENABLE_WHOLESALE_ORDER"];
        self.ENABLE_ORDER_CONSOLIDATION         = [appControlDict valueForKey:@"ENABLE_ORDER_CONSOLIDATION"];
        self.ALLOW_SKIP_CONSOLIDATION           = [appControlDict valueForKey:@"ALLOW_SKIP_CONSOLIDATION"];
        self.ALLOW_SHIP_DATE_CHANGE             = [appControlDict valueForKey:@"ALLOW_SHIP_DATE_CHANGE"];
        self.ALLOW_BONUS_CHANGE                 = [appControlDict valueForKey:@"ALLOW_BONUS_CHANGE"];
        self.ENABLE_MANUAL_FOC                  = [appControlDict valueForKey:@"ENABLE_MANUAL_FOC"];
        self.ALLOW_MANUAL_FOC_ITEM_CHANGE       = [appControlDict valueForKey:@"ALLOW_MANUAL_FOC_ITEM_CHANGE"];
        self.ALLOW_DISCOUNT                     = [appControlDict valueForKey:@"ALLOW_DISCOUNT"];
        self.ALLOW_DUAL_FOC                     = [appControlDict valueForKey:@"ALLOW_DUAL_FOC"];
        self.IS_SIGNATURE_OPTIONAL              = [appControlDict valueForKey:@"IS_SIGNATURE_OPTIONAL"];
        self.ENABLE_LINE_ITEM_NOTES             = [appControlDict valueForKey:@"ENABLE_LINE_ITEM_NOTES"];
        self.SHOW_ON_ORDER                      = [appControlDict valueForKey:@"SHOW_ON_ORDER_QTY"];
        self.SHOW_PRODUCT_TARGET                = [appControlDict valueForKey:@"SHOW_PRODUCT_TARGET"];
        self.ROUTE_SYNC_DAYS                    = [appControlDict valueForKey:@"ROUTE_SYNC_DAYS"];
        self.SD_BUCKET_SIZE                     = [appControlDict valueForKey:@"SD_BUCKET_SIZE"];
        self.SD_DURATION_MON                    = [appControlDict valueForKey:@"SD_DURATION_MON"];
        self.SHOW_SALES_HISTORY                 = [appControlDict valueForKey:@"SHOW_SALES_DATA"];
        self.ODOMETER_READING                   = [appControlDict valueForKey:@"ODOMETER_READING"];
        self.IS_START_DAY                       = [appControlDict valueForKey:@"IS_START_DAY"];
        self.ALLOW_MULTI_CURRENCY               = [appControlDict valueForKey:@"ALLOW_MULTI_CURRENCY"];
        self.IS_LOT_OPTIONAL                    = [appControlDict valueForKey:@"IS_LOT_OPTIONAL"];
        self.BONUS_MODE                         = [appControlDict valueForKey:@"BONUS_MODE"];
        self.ENABLE_BONUS_GROUPS                = [appControlDict valueForKey:@"ENABLE_BONUS_GROUP"];
        self.ENABLE_PRODUCT_IMAGE               = [appControlDict valueForKey:@"ENABLE_PRODUCT_IMAGE"];
        self.CUST_DTL_SCREEN                    = [appControlDict valueForKey:@"CUST_DTL_SCREEN"];
        self.DASHBOARD_TYPE                     = [appControlDict valueForKey:@"DASHBOARD_TYPE"];
        self.ENABLE_VISIT_TYPE                  = [appControlDict valueForKey:@"ENABLE_VISIT_TYPE"];
        self.ALLOW_SPECIAL_DISCOUNT             = [appControlDict valueForKey:@"ALLOW_SPECIAL_DISCOUNT"];
        self.IS_SURVEY_OPTIONAL                 = [appControlDict valueForKey:@"IS_SURVEY_OPTIONAL"];
        self.ENABLE_ITEM_DISCOUNT               = [appControlDict valueForKey:@"ENABLE_ITEM_DISCOUNT"];
        self.ENABLE_DIST_CHECK_MULTI_LOCATION   = [appControlDict valueForKey:@"ENABLE_DIST_CHECK_MULTI_LOCATION"];
        self.DUPLICATE_ITEM_ENTRY_CHECK         = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DUPLICATE_ITEM_ENTRY_CHECK"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DUPLICATE_ITEM_ENTRY_CHECK"]];
        self.OVER_STOCK=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"OVER_STOCK"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"OVER_STOCK"]];
        self.OVER_LIMIT=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"OVER_LIMIT"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"OVER_LIMIT"]];

        self.SHOW_ORG_STOCK_ONLY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_ORG_STOCK_ONLY"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_ORG_STOCK_ONLY"]];
        self.ENABLE_PDA_RIGHTS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PDA_RIGHTS"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PDA_RIGHTS"]];
        self.ENABLE_COLLECTIONS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTIONS"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTIONS"]];

         self.NEW_DOCTOR_CLASS=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"NEW_DOCTOR_CLASS"]];
        
        self.ALLOW_RESTRICTED_FOC=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ALLOW_RESTRICTED_FOC"]];
        
        
        self.FILTER_FOC_LIST=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FILTER_FOC_LIST"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FILTER_FOC_LIST"]];

        
        
        self.SHOW_ON_ORDER_QTY=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_ON_ORDER_QTY"]];
        
        self.ENABLE_MULTI_CAT_TRX=[appControlDict valueForKey:@"ENABLE_MULTI_CAT_TRX"];
        self.ENABLE_FOC_ORDER=[appControlDict valueForKey:@"ENABLE_FOC_ORDER"];
        self.FOC_ORDER_MODE=[appControlDict valueForKey:@"FOC_ORDER_MODE"];
        self.ENABLE_PROD_RANK=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PROD_RANK"]];
        self.SHOW_UNCONFIRMED_ORDER_SCREEN_ONSYNC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_UNCONFIRMED_ORDER_SCREEN_ONSYNC"]]isEqualToString:KAppControlsYESCode]?KAppControlsYESCode:KAppControlsNOCode;
        self.SHOW_SURVEY_ALERT=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_SURVEY_ALERT"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_SURVEY_ALERT"]];
        
        
        self.ENABLE_PROMO_ITEM_VALIDITY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PROMO_ITEM_VALIDITY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PROMO_ITEM_VALIDITY"]];
        
        
        
        /**Visit time validation flags **/
        
        self.VALIDATE_DEVICE_TIME=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_DEVICE_TIME"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_DEVICE_TIME"]];
        
        self.DEVICE_TIME_VALIDATION_RANGE=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEVICE_TIME_VALIDATION_RANGE"]] isEqualToString:@""]?@"5":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEVICE_TIME_VALIDATION_RANGE"]];
        
        
        /** IDS flags*/
        self.ENABLE_RECOMMENDED_ORDER=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_RECOMMENDED_ORDER"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_RECOMMENDED_ORDER"]];
        self.SHOW_AVERAGE_SALES_HISTORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_AVERAGE_SALES_HISTORY"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_AVERAGE_SALES_HISTORY"]];
        self.DISABLE_COLL_INV_SETTLEMENT=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_COLL_INV_SETTLEMENT"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_COLL_INV_SETTLEMENT"]];
        self.COLL_INV_SETTLEMENT_MANDATORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"COLL_INV_SETTLEMENT_MANDATORY"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"COLL_INV_SETTLEMENT_MANDATORY"]];
        
        self.UNALLOC_COLL_MODE=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"UNALLOC_COLL_MODE"]] isEqualToString:@""]?@"R":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"UNALLOC_COLL_MODE"]];


        self.ENABLE_DIST_CHECK_MULTI_LOTS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_DIST_CHECK_MULTI_LOTS"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_DIST_CHECK_MULTI_LOTS"]];
        self.ENABLE_LPO_IMAGES=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_LPO_IMAGES"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_LPO_IMAGES"]];
        self.ENABLE_DAILY_FULL_SYNC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_DAILY_FULL_SYNC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_DAILY_FULL_SYNC"]];
        self.ENABLE_BACKGROUND_SYNC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_BACKGROUND_SYNC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_BACKGROUND_SYNC"]];
        self.USER_ENABLED_BACKGROUND_SYNC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"USER_ENABLED_BACKGROUND_SYNC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"USER_ENABLED_BACKGROUND_SYNC"]];
        self.ENABLE_TODO=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_TODO"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_TODO"]];
        self.SURVEY_MANDATORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SURVEY_MANDATORY"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SURVEY_MANDATORY"]];
        self.DISTRIBUTIONCHECK_MANDATORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISTRIBUTIONCHECK_MANDATORY"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISTRIBUTIONCHECK_MANDATORY"]];
        self.V_TRX_SYNC_INTERVAL=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"V_TRX_SYNC_INTERVAL"]] isEqualToString:@""]?@"60":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"V_TRX_SYNC_INTERVAL"]];
        self.COMM_SYNC_INTERVAL=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"COMM_SYNC_INTERVAL"]] isEqualToString:@""]?@"10":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"COMM_SYNC_INTERVAL"]];
        self.MANDATORY_QTY_IN_DC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_QTY_IN_DC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_QTY_IN_DC"]];
        self.MANDATORY_EXP_DT_IN_DC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_EXP_DT_IN_DC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_EXP_DT_IN_DC"]];
        self.DEFAULT_AVAILABILITY_IN_DC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEFAULT_AVAILABILITY_IN_DC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEFAULT_AVAILABILITY_IN_DC"]];
        self.SHOW_ALERT_FOR_TRD_LIC_EXP=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_ALERT_FOR_TRD_LIC_EXP"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_ALERT_FOR_TRD_LIC_EXP"]];
        self.SHOW_COLLECTION_BANK_DROPDOWN=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_COLLECTION_BANK_DROPDOWN"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_COLLECTION_BANK_DROPDOWN"]];
        self.ENABLE_ALERT_NO_BONUS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_ALERT_NO_BONUS"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_ALERT_NO_BONUS"]];
        self.ENABLE_FS_STOCK_SYNC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FS_STOCK_SYNC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FS_STOCK_SYNC"]];

        
       self. FS_STOCK_SYNC_TIMER=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_STOCK_SYNC_TIMER"]] isEqualToString:@""]?@"60":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_STOCK_SYNC_TIMER"]];

        self.SKIP_CONSOLIDATION_MANDATORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SKIP_CONSOLIDATION_MANDATORY"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SKIP_CONSOLIDATION_MANDATORY"]];

        
        self.ENABLE_CUST_LEVEL_TRX_RESTRICTION=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_CUST_LEVEL_TRX_RESTRICTION"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_CUST_LEVEL_TRX_RESTRICTION"]];

        
            self.ENABLE_FS_ACCOMPANIED_BY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FS_ACCOMPANIED_BY"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FS_ACCOMPANIED_BY"]];
        

        self.AGEING_REP_DISP_FORMAT=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"AGEING_REP_DISP_FORMAT"]] isEqualToString:@""]?@"DAYS":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"AGEING_REP_DISP_FORMAT"]];
        
        self.FS_ENABLE_RETURN_RESTRICTION=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_ENABLE_RETURN_RESTRICTION"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_ENABLE_RETURN_RESTRICTION"]];
       
        self.VISIT_CLOSURE_INTERVAL=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VISIT_CLOSURE_INTERVAL"]] isEqualToString:@""]?@"120":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VISIT_CLOSURE_INTERVAL"]];

        
        
        
        NSLog(@"app control flag for ageing report %@", self.AGEING_REP_DISP_FORMAT);
        
        
        
        if([self.COMM_SYNC_INTERVAL doubleValue]<4)
        {
            self.COMM_SYNC_INTERVAL=@"4";

        }
        if([self.V_TRX_SYNC_INTERVAL doubleValue]<10)
        {
            self.V_TRX_SYNC_INTERVAL=@"1";

        }
        
        self.VALIDATE_CUST_LOC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_CUST_LOC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_CUST_LOC"]];
        
        
        self.ENABLE_REORDER_OPTION=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_REORDER_OPTION"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_REORDER_OPTION"]];
        
        self.RO_DESCRIPTION=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"RO_DESCRIPTION"]] ;

        self.MSG_MODULE_VERSION=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MSG_MODULE_VERSION"]] isEqualToString:@""]?@"1":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MSG_MODULE_VERSION"]];

        
        self.MESSAGE_DEFAULT_EXPIRY_DAYS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MESSAGE_DEFAULT_EXPIRY_DAYS"]] isEqualToString:@""]?@"7":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MESSAGE_DEFAULT_EXPIRY_DAYS"]];

        self.IS_COLLECTIONS_BRANCH_NAME_OPTIONAL=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_COLLECTIONS_BRANCH_NAME_OPTIONAL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_COLLECTIONS_BRANCH_NAME_OPTIONAL"]];

        
        self.SHOW_PRODUCT_NAME_SEARCH_POPOVER=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_PRODUCT_NAME_SEARCH_POPOVER"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_PRODUCT_NAME_SEARCH_POPOVER"]];
        
        self.ENABLE_MULTI_UOM=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_MULTI_UOM"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_MULTI_UOM"]];
        
        self.DISABLE_FOC_FOR_RESTRICTED=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FOC_FOR_RESTRICTED"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FOC_FOR_RESTRICTED"]];

        
        self.IS_COLLECTIONS_SIGNATURE_OPTIONAL=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_COLLECTIONS_SIGNATURE_OPTIONAL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_COLLECTIONS_SIGNATURE_OPTIONAL"]];

        
        self.VALIDATE_CUST_IBEACON=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_CUST_IBEACON"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_CUST_IBEACON"]];
        
        self.IS_FM_OBJECTIVE_MANDATORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_FM_OBJECTIVE_MANDATORY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_FM_OBJECTIVE_MANDATORY"]];
        
        self.IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_FM_VISIT_CONSLUSION_NOTES_MANDATORY"]];
        
        self.DISABLE_FM_CREATE_NEW_DOCTOR=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_CREATE_NEW_DOCTOR"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_CREATE_NEW_DOCTOR"]];

        
        
        self.FM_SHOW_WOCOACH=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SHOW_WOCOACH"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SHOW_WOCOACH"]];

        self.DISABLE_FM_CREATE_NEW_LOCATION=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_CREATE_NEW_LOCATION"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_CREATE_NEW_LOCATION"]];
        
        
        FM_VISIT_CONCLUSION_WORD_LIMIT=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_VISIT_CONCLUSION_WORD_LIMIT"]] isEqualToString:@""]?@"20":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_VISIT_CONCLUSION_WORD_LIMIT"]];
        
        self.FM_NEXT_VISIT_OBJECTIVE_WORD_LIMIT=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_NEXT_VISIT_OBJECTIVE_WORD_LIMIT"]] isEqualToString:@""]?@"20":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_NEXT_VISIT_OBJECTIVE_WORD_LIMIT"]];
        
        FM_ENABLE_WORD_LIMIT=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_WORD_LIMIT"]] isEqualToString:@""]?@"":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_WORD_LIMIT"]];
        
       
        
        self.FSR_TARGET_TYPE=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_TARGET_TYPE"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_TARGET_TYPE"]];
        
        
        FM_SHOW_NEXT_VISIT_OBJECTIVE=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SHOW_NEXT_VISIT_OBJECTIVE"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SHOW_NEXT_VISIT_OBJECTIVE"]];

        FM_ENABLE_PRESENTATION_MODULE=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_PRESENTATION_MODULE"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_PRESENTATION_MODULE"]];

        
        FM_ENABLE_DISTRIBUTION_CHECK = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DISTRIBUTION_CHECK"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DISTRIBUTION_CHECK"]];
        
        self.FM_ENABLE_STOCK_CHECK_MULTI_LOTS = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_STOCK_CHECK_MULTI_LOTS"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_STOCK_CHECK_MULTI_LOTS"]];
        
        self.FM_SHOW_STOCK_CHECK_FILTER = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SHOW_STOCK_CHECK_FILTER"]] isEqualToString:@""]?KAppControlsYESCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SHOW_STOCK_CHECK_FILTER"]];
        
        self.IS_FM_VISIT_RATING_MANDATORY = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_FM_VISIT_RATING_MANDATORY"]] isEqualToString:@""]?KAppControlsYESCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"IS_FM_VISIT_RATING_MANDATORY"]];

        
        
        self.ENABLE_FSR_VISIT_NOTES=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FSR_VISIT_NOTES"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FSR_VISIT_NOTES"]];
        
        
        self.ENABLE_PRODUCT_BARCODE_SCAN=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PRODUCT_BARCODE_SCAN"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PRODUCT_BARCODE_SCAN"]];

        self.FS_IS_RETURN_DATE_MANDATORY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_IS_RETURN_DATE_MANDATORY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_IS_RETURN_DATE_MANDATORY"]];
        self.ENABLE_ASSORTMENT_BONUS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_ASSORTMENT_BONUS"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_ASSORTMENT_BONUS"]];

        self.USE_GEO_BRANCH_LOC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"USE_GEO_BRANCH_LOC"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"USE_GEO_BRANCH_LOC"]];

        
        self.ENABLE_MEDREP_NO_VISIT_OPTION=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_MEDREP_NO_VISIT_OPTION"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_MEDREP_NO_VISIT_OPTION"]];
        
        NSLog(@"ENABLE_MEDREP_NO_VISIT_OPTION from database %@",ENABLE_MEDREP_NO_VISIT_OPTION);
        
        self.MEDREP_NO_VISIT_LOCATION=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MEDREP_NO_VISIT_LOCATION"]] isEqualToString:@""]?@"":[SWDefaults getValidStringValue:[[appControlDict valueForKey:@"MEDREP_NO_VISIT_LOCATION"] uppercaseString]];
        
        NSLog(@"MEDREP_NO_VISIT_LOCATION from database %@",MEDREP_NO_VISIT_LOCATION);
        
        self.ENABLE_MEDREP_INCOMPLETE_VISIT_COMMENTS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_MEDREP_INCOMPLETE_VISIT_COMMENTS"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[[appControlDict valueForKey:@"ENABLE_MEDREP_INCOMPLETE_VISIT_COMMENTS"] uppercaseString]];
        
        OH_DUPLICATE_ITEM_CHECK_PERIOD=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"OH_DUPLICATE_ITEM_CHECK_PERIOD"]] isEqualToString:@""]?@"0":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"OH_DUPLICATE_ITEM_CHECK_PERIOD"]];

        
        START_VISIT_ON_ACTIVITY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"START_VISIT_ON_ACTIVITY"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[[appControlDict valueForKey:@"START_VISIT_ON_ACTIVITY"] uppercaseString]];
        
        FSR_TARGET_PRIMARY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_TARGET_PRIMARY"]] isEqualToString:@""]?@"":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_TARGET_PRIMARY"] ];
        FSR_VALUE_TYPE=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_VALUE_TYPE"]] isEqualToString:@""]?@"":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_VALUE_TYPE"]];

        
        FM_ENABLE_HOLIDAY = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_HOLIDAY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_HOLIDAY"]];
        
        if(MEDREP_NO_VISIT_LOCATION.length==0){ /*** Disabling the novisit option if novisit location is not configured in appcontrol table.*/
            ENABLE_MEDREP_NO_VISIT_OPTION=@"N";
        }
        NSLog(@"ENABLE_MEDREP_NO_VISIT_OPTION after checking MEDREP_NO_VISIT_LOCATION %@",ENABLE_MEDREP_NO_VISIT_OPTION);
        
        DISABLE_BLOCKED_CUSTOMER_SALES_ORDER_ENTRY=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_BLOCKED_CUSTOMER_SALES_ORDER_ENTRY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_BLOCKED_CUSTOMER_SALES_ORDER_ENTRY"]];
        
        DISABLE_FM_VISIT_DELETE=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_VISIT_DELETE"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_VISIT_DELETE"]];
        
        self.SHOW_COLLECTION_TARGET = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_COLLECTION_TARGET"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_COLLECTION_TARGET"]];

        self.VALIDATE_MEDREP_CUST_LOC = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_MEDREP_CUST_LOC"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VALIDATE_MEDREP_CUST_LOC"]];

        
        self.FS_SHOW_DC_FILTER = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_FILTER"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_FILTER"]];
        
        self.FS_SHOW_DC_AVBL_SEG = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_AVBL_SEG"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_AVBL_SEG"]];
        
        self.FS_SHOW_DC_RO = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_RO"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_RO"]];
        
        self.FS_SHOW_DC_MIN_STOCK = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_MIN_STOCK"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_MIN_STOCK"]];
        
        
        ALLOW_EXCESS_CASH_COLLECTION = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ALLOW_EXCESS_CASH_COLLECTION"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ALLOW_EXCESS_CASH_COLLECTION"]];

        ENABLE_VAT = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VAT"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VAT"]];
        
        VAT_RULE_LEVEL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VAT_RULE_LEVEL"]] isEqualToString:@""]?@"P":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VAT_RULE_LEVEL"]];/** Default Product Level*/
        
        VAT_LEVEL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VAT_LEVEL"]] isEqualToString:@""]?@"NET":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VAT_LEVEL"]];/** Default NET Amount Level*/

        
        
        MERCH_MSL_CLASSIFICATION=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MERCH_MSL_CLASSIFICATION"]];

        ENABLE_MERCHANDISING=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_MERCHANDISING"]];

        SHOW_TWO_LEVEL_DOCTOR_CLASSIFICATION = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_TWO_LEVEL_DOCTOR_CLASSIFICATION"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_TWO_LEVEL_DOCTOR_CLASSIFICATION"]];
        
        ENABLE_FS_VISIT_TIMER = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FS_VISIT_TIMER"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_FS_VISIT_TIMER"]];
        
        FS_VISIT_TIMER_LIMIT = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_VISIT_TIMER_LIMIT"]] isEqualToString:@""]?@"100":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_VISIT_TIMER_LIMIT"]];
        
        ENABLE_TGT_ACVMT_DASHBOARD = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_TGT_ACVMT_DASHBOARD"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_TGT_ACVMT_DASHBOARD"]];
        
        ENABLE_VISIT_LOC_UPDATE_ON_CLOSE = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VISIT_LOC_UPDATE_ON_CLOSE"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VISIT_LOC_UPDATE_ON_CLOSE"]];
        
        CLOSE_VISIT_LOC_UPDATE_LIMIT = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"CLOSE_VISIT_LOC_UPDATE_LIMIT"]] isEqualToString:@""]?@"50":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"CLOSE_VISIT_LOC_UPDATE_LIMIT"]];
        
        ENABLE_COLLECTION_IMAGE_CAPTURE = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTION_IMAGE_CAPTURE"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTION_IMAGE_CAPTURE"]];
        
        ENABLE_SALESORDER_SHIPTO_SELECTION = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_SALESORDER_SHIPTO_SELECTION"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_SALESORDER_SHIPTO_SELECTION"]];

        ENABLE_CUSTOMER_STATEMENT_PDF_EMAIL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_CUSTOMER_STATEMENT_PDF_EMAIL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_CUSTOMER_STATEMENT_PDF_EMAIL"]];

        SHOW_FS_VISIT_CUSTOMER_CONTACT = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_FS_VISIT_CUSTOMER_CONTACT"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SHOW_FS_VISIT_CUSTOMER_CONTACT"]];

        
        ENABLE_COLLECTION_RECEIPT_PDF_EMAIL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTION_RECEIPT_PDF_EMAIL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTION_RECEIPT_PDF_EMAIL"]];

        DEFAULT_GEO_LOCATION_RANGE = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEFAULT_GEO_LOCATION_RANGE"]] isEqualToString:@""]?@"":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEFAULT_GEO_LOCATION_RANGE"]];

        
        self.FM_ENABLE_DR_APPROVAL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DR_APPROVAL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DR_APPROVAL"]];
        self.FM_ENABLE_LOCATION_APPROVAL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_LOCATION_APPROVAL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_LOCATION_APPROVAL"]];

        
        
        
        self.FM_ENABLE_MULTISEL_ACCOMP_BY = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_MULTISEL_ACCOMP_BY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_MULTISEL_ACCOMP_BY"]];
        
        
        //INMA
        FM_DISPLAY_CALLS_VISIT_FREQUENCY = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_DISPLAY_CALLS_VISIT_FREQUENCY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_DISPLAY_CALLS_VISIT_FREQUENCY"]];

        FM_EDETAILING_IMAGE_DEFAULT_DEMO_TIME = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_IMAGE_DEFAULT_DEMO_TIME"]] isEqualToString:@""]?keDetailingDefaultDemoTime:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_IMAGE_DEFAULT_DEMO_TIME"]];

        FM_EDETAILING_VIDEO_DEFAULT_DEMO_TIME = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_VIDEO_DEFAULT_DEMO_TIME"]] isEqualToString:@""]?keDetailingDefaultDemoTime:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_VIDEO_DEFAULT_DEMO_TIME"]];

        FM_EDETAILING_PDF_DEFAULT_DEMO_TIME = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_PDF_DEFAULT_DEMO_TIME"]] isEqualToString:@""]?keDetailingDefaultDemoTime:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_PDF_DEFAULT_DEMO_TIME"]];

        FM_EDETAILING_PPT_DEFAULT_DEMO_TIME = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_PPT_DEFAULT_DEMO_TIME"]] isEqualToString:@""]?keDetailingDefaultDemoTime:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_EDETAILING_PPT_DEFAULT_DEMO_TIME"]];

        self.FM_ENABLE_DEMO_PLAN_SPECIALIZATION = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DEMO_PLAN_SPECIALIZATION"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DEMO_PLAN_SPECIALIZATION"]];

        self.FM_ENABLE_DR_APPROVAL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DR_APPROVAL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_DR_APPROVAL"]];
        self.FM_ENABLE_LOCATION_APPROVAL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_LOCATION_APPROVAL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_LOCATION_APPROVAL"]];
        self.FM_SURVEY_VERSION = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SURVEY_VERSION"]] isEqualToString:@""]?@"2":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_SURVEY_VERSION"]];

        self.FM_ENABLE_VISIT_HISTORY_REPORT = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_VISIT_HISTORY_REPORT"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_ENABLE_VISIT_HISTORY_REPORT"]];
        
        self.FM_DISABLE_CREATE_DR_TRADE_CHANNEL_VALIDATION = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_DISABLE_CREATE_DR_TRADE_CHANNEL_VALIDATION"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_DISABLE_CREATE_DR_TRADE_CHANNEL_VALIDATION"]];

        self.MENU_SHOW_BO_URL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MENU_SHOW_BO_URL"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MENU_SHOW_BO_URL"]];

        self.ENABLE_COLLECTION_REMARKS = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTION_REMARKS"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_COLLECTION_REMARKS"]];
        
        self.FS_COLLECTION_REMARKS_MANDATORY = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_COLLECTION_REMARKS_MANDATORY"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_COLLECTION_REMARKS_MANDATORY"]];

        
        self.FM_DEFAULT_VISIT_RATING = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_DEFAULT_VISIT_RATING"]] isEqualToString:@""]?@"5":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_DEFAULT_VISIT_RATING"]];
        
        self.SET_DEFAULT_EMAIL_FOR_LOG = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SET_DEFAULT_EMAIL_FOR_LOG"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"SET_DEFAULT_EMAIL_FOR_LOG"]];
        
        self.DEFAULT_EMAILID_FOR_LOG = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEFAULT_EMAILID_FOR_LOG"]] isEqualToString:@""]?@"":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEFAULT_EMAILID_FOR_LOG"]];
        
        self.ENABLE_PDARIGHTS_REPORTS = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PDARIGHTS_REPORTS"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_PDARIGHTS_REPORTS"]];
        self.ENABLE_VISIT_REQUIRED_PER_MONTH_FIELD = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VISIT_REQUIRED_PER_MONTH_FIELD"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VISIT_REQUIRED_PER_MONTH_FIELD"]];
        self.INCLUDE_UNAPPR_DR_LOC = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"INCLUDE_UNAPPR_DR_LOC"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"INCLUDE_UNAPPR_DR_LOC"]];

        //Ravinder code, 16 May 2019
        self.POPULATE_CUSTOMER_EMAIL = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"POPULATE_CUSTOMER_EMAIL"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"POPULATE_CUSTOMER_EMAIL"]];
        
        self.FS_ENABLE_ROUTE_RESCHEDULING = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_ENABLE_ROUTE_RESCHEDULING"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_ENABLE_ROUTE_RESCHEDULING"]];
        
        self.FM_VISIT_WITH_PLANNED_DOCTOR_ONLY = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_VISIT_WITH_PLANNED_DOCTOR_ONLY"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FM_VISIT_WITH_PLANNED_DOCTOR_ONLY"]];
        
        self.ENABLE_LOT_MANDATORY_RETURN = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_LOT_MANDATORY_RETURN"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_LOT_MANDATORY_RETURN"]];
        
        self.ALLOW_FM_VISIT_WITHOUT_CONT_DR = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ALLOW_FM_VISIT_WITHOUT_CONT_DR"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ALLOW_FM_VISIT_WITHOUT_CONT_DR"]];
        
        self.DISABLE_FM_CREATE_NEW_CONTACT = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_CREATE_NEW_CONTACT"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_FM_CREATE_NEW_CONTACT"]];
        
         self.NEARBY_CUSTOMERS_DISTANCE_THRESHOLD = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"NEARBY_CUSTOMERS_DISTANCE_THRESHOLD"]] isEqualToString:@""]?@"1000":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"NEARBY_CUSTOMERS_DISTANCE_THRESHOLD"]];
        
        self.FS_ENABLE_PRODUCT_MEDIA = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_ENABLE_PRODUCT_MEDIA"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_ENABLE_PRODUCT_MEDIA"]];

        self.ENABLE_INVOICE_ATTACHMENT_IN_RETURNS = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_INVOICE_ATTACHMENT_IN_RETURNS"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_INVOICE_ATTACHMENT_IN_RETURNS"]];
        
        self.DISABLE_EMAIL_PRODUCT_MEDIA = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_EMAIL_PRODUCT_MEDIA"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_EMAIL_PRODUCT_MEDIA"]];

        self.DISABLE_PAST_DATE_FOR_CHECK = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_PAST_DATE_FOR_CHECK"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DISABLE_PAST_DATE_FOR_CHECK"]];
        self.CURR_CHQ_MIN_DAYS = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"CURR_CHQ_MIN_DAYS"]] isEqualToString:@""]?@"0":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"CURR_CHQ_MIN_DAYS"]];

        self.ENABLE_VISIT_NOTE_ON_END_VISIT = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VISIT_NOTE_ON_END_VISIT"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_VISIT_NOTE_ON_END_VISIT"]];
        
        self.USE_EMPNAME_IN_USERLIST = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"USE_EMPNAME_IN_USERLIST"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"USE_EMPNAME_IN_USERLIST"]];
        self.MANDATORY_CITY_DOCTOR_CREATION = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_CITY_DOCTOR_CREATION"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_CITY_DOCTOR_CREATION"]];
        

        /**  flags related to current development tasks are disabled.  below functionalities are not required for app store release*/
       // ENABLE_MERCHANDISING = @"N";
        ENABLE_VAT = @"N";
        ENABLE_ASSORTMENT_BONUS=@"N";
        ENABLE_ITEM_TYPE =  [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_ITEM_TYPE"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_ITEM_TYPE"]];
        
        self.FS_PRODUCT_GROUP_BY_BRAND = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_PRODUCT_GROUP_BY_BRAND"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_PRODUCT_GROUP_BY_BRAND"]];
        
        
        self.ENABLE_BONUS_ALERTS = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_BONUS_ALERTS"]] isEqualToString:@""]?@"N":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_BONUS_ALERTS"]];
        self.BONUS_ALERT_THRESHOLD_TYPE = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"BONUS_ALERT_THRESHOLD_TYPE"]] isEqualToString:@""]?@"P":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"BONUS_ALERT_THRESHOLD_TYPE"]];
        self.BONUS_ALERT_THRESHOLD = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"BONUS_ALERT_THRESHOLD"]] isEqualToString:@""]?@"0":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"BONUS_ALERT_THRESHOLD"]];
        
        
        if (self.ENABLE_PROD_RANK.length==0) {
            
            self.ENABLE_PROD_RANK = @"N";
        }

        if (self.ENABLE_DIST_CHECK_MULTI_LOCATION.length==0) {
            
            self.ENABLE_DIST_CHECK_MULTI_LOCATION = @"N";
        }
        if (self.ENABLE_ITEM_DISCOUNT.length==0) {
            
            self.ENABLE_ITEM_DISCOUNT=@"N";
        }
        
        if (self.ALLOW_SPECIAL_DISCOUNT.length==0) {
            
            self.ALLOW_SPECIAL_DISCOUNT =@"N";
        }
        
        if (self.ENABLE_VISIT_TYPE.length==0)
        {
            self.ENABLE_VISIT_TYPE                = @"N";
        }
        
        if (self.ENABLE_ORDER_HISTORY.length==0)
        {
            self.ENABLE_ORDER_HISTORY                = @"Y";
        }
        if (self.ENABLE_TEMPLATES.length==0)
        {
            self.ENABLE_TEMPLATES                = @"N";
        }
        if (self.ENABLE_COLLECTION.length==0)
        {
            self.ENABLE_COLLECTION                = @"Y";
        }
        if (self.ENABLE_DISTRIB_CHECK.length==0)
        {
            self.ENABLE_DISTRIB_CHECK                = @"Y";
        }
        if (self.ALLOW_LOT_SELECTION.length==0)
        {
            self.ALLOW_LOT_SELECTION                = @"Y";
        }
        if (self.ENABLE_WHOLESALE_ORDER.length==0)
        {
            self.ENABLE_WHOLESALE_ORDER                = @"N";
        }
        if (self.ENABLE_ORDER_CONSOLIDATION.length==0)
        {
            self.ENABLE_ORDER_CONSOLIDATION                = @"N";
        }
        if (self.ALLOW_SKIP_CONSOLIDATION.length==0)
        {
            self.ALLOW_SKIP_CONSOLIDATION                = @"N";
        }
        if (self.ALLOW_SHIP_DATE_CHANGE.length==0)
        {
            self.ALLOW_SHIP_DATE_CHANGE                = @"Y";
        }
        if (self.ALLOW_BONUS_CHANGE.length==0)
        {
            self.ALLOW_BONUS_CHANGE                = @"Y";
        }
        if (self.ENABLE_MANUAL_FOC.length==0)
        {
            self.ENABLE_MANUAL_FOC                = @"Y";
        }
        if (self.ALLOW_MANUAL_FOC_ITEM_CHANGE.length==0)
        {
            self.ALLOW_MANUAL_FOC_ITEM_CHANGE                = @"Y";
        }
        if (self.ALLOW_DISCOUNT.length==0)
        {
            self.ALLOW_DISCOUNT                = @"Y";
        }
        if (self.ALLOW_DUAL_FOC.length==0)
        {
            self.ALLOW_DUAL_FOC                = @"N";
        }
        if (self.IS_SIGNATURE_OPTIONAL.length==0)
        {
            self.IS_SIGNATURE_OPTIONAL                = @"Y";
        }
        if (self.ENABLE_LINE_ITEM_NOTES.length==0)
        {
            self.ENABLE_LINE_ITEM_NOTES                = @"N";
        }
        if (self.SHOW_ON_ORDER.length==0)
        {
            self.SHOW_ON_ORDER                = @"N";
        }
        if (self.SHOW_PRODUCT_TARGET.length==0)
        {
            self.SHOW_PRODUCT_TARGET                = @"Y";
        }
        if (self.ROUTE_SYNC_DAYS.length==0)
        {
            self.ROUTE_SYNC_DAYS                = @"1";
        }
        if (self.SD_BUCKET_SIZE.length==0)
        {
            self.SD_BUCKET_SIZE                = @"1";
        }
        if (self.SD_DURATION_MON.length==0)
        {
            self.SD_DURATION_MON                = @"6";
        }
        if (self.SHOW_SALES_HISTORY.length==0)
        {
            self.SHOW_SALES_HISTORY                = @"N";
        }
        if (self.ODOMETER_READING.length==0)
        {
            self.ODOMETER_READING                = @"N";
        }
        if (self.IS_START_DAY.length==0)
        {
            self.IS_START_DAY                = @"N";
        }
        if (self.ALLOW_MULTI_CURRENCY.length==0)
        {
            self.ALLOW_MULTI_CURRENCY                = @"N";
        }
        if (self.IS_LOT_OPTIONAL.length==0)
        {
            self.IS_LOT_OPTIONAL                = @"Y";
        }
        if (self.BONUS_MODE.length==0)
        {
            self.BONUS_MODE                = @"DEFAULT";
        }
        if (self.ENABLE_BONUS_GROUPS.length==0)
        {
            self.ENABLE_BONUS_GROUPS                = @"N";
        }
        if (self.ENABLE_PRODUCT_IMAGE.length==0)
        {
            self.ENABLE_PRODUCT_IMAGE                = @"N";
        }
        if (self.IS_SURVEY_OPTIONAL.length==0)
        {
            self.IS_SURVEY_OPTIONAL                = @"Y";
        }
        


    }
    
    else
    {
        self.ALLOW_SPECIAL_DISCOUNT             = @"N";
        self.ENABLE_ORDER_HISTORY               = @"Y";
        self.ENABLE_TEMPLATES                   = @"N";
        self.ENABLE_COLLECTION                  = @"Y";
        self.ENABLE_DISTRIB_CHECK               = @"Y";
        self.ALLOW_LOT_SELECTION                = @"Y";
        self.ENABLE_WHOLESALE_ORDER             = @"N";
        self.ENABLE_ORDER_CONSOLIDATION         = @"N";
        self.ALLOW_SKIP_CONSOLIDATION           = @"Y";
        self.ALLOW_SHIP_DATE_CHANGE             = @"Y";
        self.ALLOW_BONUS_CHANGE                 = @"Y";
        self.ENABLE_MANUAL_FOC                  = @"Y";
        self.ALLOW_MANUAL_FOC_ITEM_CHANGE       = @"Y";
        self.ALLOW_DISCOUNT                     = @"Y";
        self.ALLOW_DUAL_FOC                     = @"N";
        self.IS_SIGNATURE_OPTIONAL              = @"Y";
        self.ENABLE_LINE_ITEM_NOTES             = @"N";
        self.SHOW_ON_ORDER                      = @"N";
        self.SHOW_PRODUCT_TARGET                = @"Y";
        self.ROUTE_SYNC_DAYS                    = @"1";
        self.SD_BUCKET_SIZE                     = @"1";
        self.SD_DURATION_MON                    = @"6";
        self.SHOW_SALES_HISTORY                 = @"N";
        self.ODOMETER_READING                   = @"N";
        self.IS_START_DAY                       = @"N";
        self.ALLOW_MULTI_CURRENCY               = @"N";
        self.IS_LOT_OPTIONAL                    = @"Y";
        self.BONUS_MODE                         = @"DEFAULT";
        self.ENABLE_BONUS_GROUPS                = @"N";
        self.ENABLE_PRODUCT_IMAGE               = @"N";
        self.IS_SURVEY_OPTIONAL                 = @"Y";
        self.ENABLE_VISIT_TYPE                  = @"N";
        self.ENABLE_DIST_CHECK_MULTI_LOCATION   = @"N";
    }
    
    
    
    /** iPad side Flags*/
    /** Do not assign ipad side flags before assigning the databse flags*/
    if(![ALLOW_DISCOUNT isEqualToString:KAppControlsYESCode]&&
        ![ENABLE_ITEM_DISCOUNT isEqualToString:KAppControlsYESCode]&&
         ![ALLOW_SPECIAL_DISCOUNT isEqualToString:KAppControlsYESCode]){
        HIDE_DISCOUNT_FIELDS_AND_LABELS=@"Y";
    }else{
        HIDE_DISCOUNT_FIELDS_AND_LABELS=@"N";
    }
    
    
}



static AppControl *sharedSingleton = nil;

+ (AppControl*) retrieveSingleton {
	@synchronized(self)
    {
		if (sharedSingleton == nil) {
			sharedSingleton = [[AppControl alloc] init];
            [sharedSingleton initializeAppControl];
		}
	}
	return sharedSingleton;
}

+ (void) destroyMySingleton
{
    sharedSingleton=nil;
}

+ (id) allocWithZone:(NSZone *) zone
{
    //TODO: Replace with newer version of alloc. Memory zones are now not used.
    
	@synchronized(self) {
		if (sharedSingleton == nil) {
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;
		}
	}
	return nil;
}




@end
