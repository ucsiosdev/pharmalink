//
//  OrderInvoiceViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 6/25/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWGridViewController.h"

#import "SWNoResultsView.h"
#import "SWFoundation.h"
#import "Singleton.h"


@interface OrderInvoiceViewController : SWGridViewController <GridViewDataSource > {
    NSString *documentReference;
    SWNoResultsView *noResultsView;
    float invoiceTempValue;
    //
    NSString *invoiceValueOrder;
    
    
    
}


- (id)initWithDocumentReference:(NSString *)ref;

@end