//
//  PresentationPlaceHolderCollectionViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 10/10/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentationPlaceHolderCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *saperatorLbl;
@property (strong, nonatomic) IBOutlet UIView *placeHolderView;

@end
