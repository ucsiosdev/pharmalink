//
//  SalesWorxPresentationHomeViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/22/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPresentationHomeViewController.h"
#import "AppControl.h"

@interface SalesWorxPresentationHomeViewController ()

@end

@implementation SalesWorxPresentationHomeViewController

@synthesize mediaArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSMutableArray* tempArray=[[NSMutableArray alloc]init];
    //NSLog(@"media array in home is %@",[mediaArray valueForKey:@"Media_Type"]);
    
    
    //[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(demoedMediaFromChild:) name:@"DEMOED_MEDIA_CHILD" object:nil];
    
    demoedMediaArray=[[NSMutableArray alloc]init];
    
    for (ProductMediaFile *currentFile in mediaArray) {
        if ([currentFile.Media_Type isEqualToString:@"Powerpoint"]) {
            [tempArray addObject:currentFile];
        }
        
    }
    // mediaArray=tempArray;
    viewControllerIndexArray = [[NSMutableArray alloc]init];
    UIBarButtonItem* doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonTapped)];
    [doneButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=doneButton;
    
    self.navigationItem.hidesBackButton=YES;
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Presentation", nil)];

}

-(void)demoedMediaFromChild:(NSNotification*)notification
{
    


    [demoedMediaArray addObject:notification.object];

    NSLog(@"demoed media from child notification %@",demoedMediaArray);
}

-(void)doneButtonTapped
{
    NSLog(@"done button tapped");
    
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"DEMOED_MEDIA" object:demoedMediaArray];

    self.navigationItem.rightBarButtonItem.enabled=NO;
    
    if (self.isFromEditor) {
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }}




-(void)viewDidAppear:(BOOL)animated
{
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];

    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    
    self.pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+40);

//    [[self.pageController view] setFrame:self.view.bounds];
    
    SalesWorxPresentationChildViewController *initialViewController = [self viewControllerAtIndex:0];
    
    selectedIndex = 0;
    if (demoTimer) {
        demoTimer=nil;
    }
    
   // [mediaArray objectAtIndex:index]
    //NSLog(@"media array is %@", [mediaArray valueForKey:@"Media_Type"]);
    
    if (mediaArray.count>0)
    {
        
    ProductMediaFile* currentMediFile = [mediaArray objectAtIndex:0];
    if ([currentMediFile.Media_Type.uppercaseString isEqualToString:kMediaTypeImage])
    {
        demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_IMAGE_DEFAULT_DEMO_TIME.integerValue;
    }
    else if ([currentMediFile.Media_Type.uppercaseString isEqualToString:kMediaTypeVideo])
    {
        demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_VIDEO_DEFAULT_DEMO_TIME.integerValue;
    }
    else if ([currentMediFile.Media_Type.uppercaseString isEqualToString:kMediaTypeBrochure])
    {
        demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_PDF_DEFAULT_DEMO_TIME.integerValue;
    }
    else if ([currentMediFile.Media_Type.uppercaseString isEqualToString:kMediaTypePPT])
    {
        demoTimeInterval = [AppControl retrieveSingleton].FM_EDETAILING_PPT_DEFAULT_DEMO_TIME.integerValue;
    }
    }
    //start the timer to check if images for displayed for more than 5 secs
    demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startDemoTimer) userInfo:nil repeats:NO];

    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    NSLog(@"initial vc is %@",viewControllers);

    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark page control methods


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(SalesWorxPresentationChildViewController *)viewController index];
    
    NSLog(@"viewControllerBeforeViewController with index %ld", index);
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(SalesWorxPresentationChildViewController *)viewController index];
    
    NSLog(@"viewControllerAfterViewController with index %ld", index);

    
    index++;
    
    if (index == mediaArray.count) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

-(void)startDemoTimer
{
    //[demoedMediaArray addObject:selectedMediaFile.Media_File_ID];
    [demoTimer invalidate];
    demoTimer=nil;
    
    //[[NSNotificationCenter defaultCenter]postNotificationName:@"DEMOED_MEDIA_CHILD" object:[mediaArray objectAtIndex:selectedIndex]];
    
    [demoedMediaArray addObject:[mediaArray objectAtIndex:selectedIndex]];
    NSLog(@"demoed media is %@",[[mediaArray objectAtIndex:selectedIndex]valueForKey:@"Media_File_ID"]);
    
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    NSLog(@"willTransitionToViewControllers called");
    
    if([pendingViewControllers count]>0)
    {
        selectedIndex =[(SalesWorxPresentationChildViewController*)[pendingViewControllers objectAtIndex:0] index];
        if (demoTimer) {
            demoTimer=nil;
        }
        //start the timer to check if images for displayed for more than 5 secs
        demoTimer=[NSTimer scheduledTimerWithTimeInterval:demoTimeInterval target:self selector:@selector(startDemoTimer) userInfo:nil repeats:NO];

    }
}

- (SalesWorxPresentationChildViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    //[viewControllerIndexArray addObject:[NSNumber numberWithInteger:index]];
    SalesWorxPresentationChildViewController *childViewController = [[SalesWorxPresentationChildViewController alloc] initWithNibName:@"SalesWorxPresentationChildViewController" bundle:nil];
    childViewController.index = index;
    childViewController.isFromEditor=self.isFromEditor;
    childViewController.presentationDelegate=self;
    //[childViewController createrReaderVC:[mediaArray objectAtIndex:index]];
    
    [childViewController updateMediaViewControllerwithFileType:[mediaArray objectAtIndex:index]];
    return childViewController;
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return mediaArray.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

@end
