//
//  UCSWebViewController.h
//  UCSPresentation
//
//  Created by Unique Computer Systems on 9/13/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"
#import "SalesWorxNavigationHeaderLabel.h"

@interface UCSWebViewController : UIViewController

{
    
    IBOutlet NSLayoutConstraint *headerViewHeightConstrint;
}
@property (strong, nonatomic) IBOutlet UIWebView *ucsWebView;

@property(strong,nonatomic) NSString* requestURLString;
@property(strong,nonatomic) ProductMediaFile * currentMediaFile;


@property (strong, nonatomic) IBOutlet SalesWorxNavigationHeaderLabel *titleLbl;
- (IBAction)closeButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property(nonatomic) BOOL showStatusBar;
-(void)updateWebView:(ProductMediaFile*)selectedMediaFile;
-(void)resetWebView;


@end
