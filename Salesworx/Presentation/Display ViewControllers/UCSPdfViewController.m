//
//  UCSPdfViewController.m
//  UCSPresentation
//
//  Created by Pavan Kumar Singamsetti on 2/8/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "UCSPdfViewController.h"
@interface UCSPdfViewController ()

@end

@implementation UCSPdfViewController
@synthesize currentMediaFile,showStatusBar,headerView,titleLbl,defaultView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!showStatusBar) {
        headerView.hidden=YES;
        headerViewHeightConstrint.constant=0.0f;
    }
    else
    {
        titleLbl.text=currentMediaFile.Product_Name;
        headerViewHeightConstrint.constant=64.0f;
    }


    // Do any additional setup after loading the view from its nib.
}
- (IBAction)closeButtonTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewDidAppear:(BOOL)animated
{
    if (defaultView) {
        [self addPdfToTheView];
    }
}


-(void)removePDFfile
{
    [readerViewController removeFromParentViewController];
    [readerViewController.view removeFromSuperview];
}

-(void)updatePDFwithFile:(ProductMediaFile*)matchedFile
{
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    CGRect  viewFrame;
    if (!showStatusBar) {
        viewFrame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    else{
        viewFrame=CGRectMake(0, headerViewHeightConstrint.constant, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
    
    NSString* filePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:matchedFile.File_Name];
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
    // NSLog(@"bookmark data %@", document.bookmarks);
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.hideCloseButton=YES;
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        readerViewController.view.frame=viewFrame;
        [self addChildViewController:readerViewController];
        [self.view addSubview:readerViewController.view];
    }
}

-(void)addPdfToTheView{
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:_resourceName ofType:nil];
    
    CGRect  viewFrame;
    
    if (!showStatusBar) {
        viewFrame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    else{
        viewFrame=CGRectMake(0, headerViewHeightConstrint.constant, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
    
    NSString* filePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentMediaFile.File_Name];

    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
   // NSLog(@"bookmark data %@", document.bookmarks);
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.hideCloseButton=YES;
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        readerViewController.view.frame=viewFrame;
        [self addChildViewController:readerViewController];
        [self.view addSubview:readerViewController.view];
    }
}

@end
