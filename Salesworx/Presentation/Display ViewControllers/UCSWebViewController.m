//
//  UCSWebViewController.m
//  UCSPresentation
//
//  Created by Unique Computer Systems on 9/13/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "UCSWebViewController.h"

@interface UCSWebViewController ()

@end

@implementation UCSWebViewController

@synthesize ucsWebView,requestURLString,currentMediaFile,showStatusBar,headerView,titleLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!showStatusBar) {
        headerView.hidden=YES;
        headerViewHeightConstrint.constant=0.0f;

    }
    else
    {
        titleLbl.text=currentMediaFile.Product_Name;
        headerViewHeightConstrint.constant=64.0f;

    }
    

    
    
    NSString* filePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentMediaFile.File_Name];
    [ucsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:filePath]]];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)updateWebView:(ProductMediaFile*)selectedMediaFile
{
    NSString* filePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:selectedMediaFile.File_Name];
    [ucsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:filePath]]];
}

-(void)resetWebView
{
    [ucsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
}

- (IBAction)closeButtonTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
