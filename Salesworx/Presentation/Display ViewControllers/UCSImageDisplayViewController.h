//
//  UCSImageDisplayViewController.h
//  UCSPresentation
//
//  Created by Pavan Kumar Singamsetti on 2/8/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"
#import "SalesWorxNavigationHeaderLabel.h"

@interface UCSImageDisplayViewController : UIViewController
{
    
    IBOutlet NSLayoutConstraint *headerViewHeightConstrint;
    NSTimer *demoTimer;
}
@property (strong, nonatomic) IBOutlet UIImageView *presentationImageView;
@property (strong,nonatomic) NSString *resourceName;
@property(strong,nonatomic) ProductMediaFile * currentMediaFile;
@property (strong, nonatomic) IBOutlet SalesWorxNavigationHeaderLabel *titleLbl;
- (IBAction)closeButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property(nonatomic) BOOL showStatusBar;
-(void)updateImage:(ProductMediaFile*)currentImage;
-(void)removeImage;

@end
