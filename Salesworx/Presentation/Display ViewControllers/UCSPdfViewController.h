//
//  UCSPdfViewController.h
//  UCSPresentation
//
//  Created by Pavan Kumar Singamsetti on 2/8/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"
#import "SalesWorxNavigationHeaderLabel.h"

@interface UCSPdfViewController : UIViewController<ReaderViewControllerDelegate>
{
    
    IBOutlet NSLayoutConstraint *headerViewHeightConstrint;
    
    ReaderViewController *readerViewController;
}

@property (strong,nonatomic) NSString *resourceName;
@property(nonatomic) id delegate;
-(void)addPdfToTheView;
@property(strong,nonatomic) ProductMediaFile * currentMediaFile;

@property (strong, nonatomic) IBOutlet SalesWorxNavigationHeaderLabel *titleLbl;
- (IBAction)closeButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property(nonatomic) BOOL showStatusBar;
-(void)updatePDFwithFile:(ProductMediaFile*)matchedFile;
-(void)removePDFfile;
@property(nonatomic) BOOL defaultView;

@end
