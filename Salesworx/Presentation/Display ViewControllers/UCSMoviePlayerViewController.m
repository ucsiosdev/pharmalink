//
//  UCSMoviePlayerViewController.m
//  UCSPresentation
//
//  Created by Pavan Kumar Singamsetti on 2/8/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "UCSMoviePlayerViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface UCSMoviePlayerViewController ()

@property (nonatomic, retain) AVPlayerViewController *avPlayerViewcontroller;

@end

@implementation UCSMoviePlayerViewController
@synthesize currentMediaFile,showStatusBar,headerView,titleLbl,videoContainerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!showStatusBar) {
        headerView.hidden=YES;
        videoContainerView.hidden=YES;
        headerViewHeightConstrint.constant=0.0f;

    }
    else
    {
        titleLbl.text=currentMediaFile.Product_Name;
        headerViewHeightConstrint.constant=64.0f;

    }
    


}

-(void)updateMovieFile:(ProductMediaFile*)selectedMovieFile
{
   /* UIView *view;
    if (!showStatusBar) {
        view= self.view;
    }
    else{
        view=videoContainerView;
    }
    NSString* movieFilePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentMediaFile.File_Name];
    NSAssert(movieFilePath, @"movieFilePath is nil");
    NSURL *fileURL = [NSURL fileURLWithPath:movieFilePath];
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    playerViewController.player = [AVPlayer playerWithURL:fileURL];
    self.avPlayerViewcontroller = playerViewController;
    self.avPlayerViewcontroller.view.frame = view.bounds;
    [view addSubview:playerViewController.view];
    view.autoresizesSubviews = TRUE;*/

}

-(void)viewWillAppear:(BOOL)animated
{
    UIView *view;
    
    if (!showStatusBar) {
        view= self.view;
    }
    else{
        view=videoContainerView;
    }
    NSString* movieFilePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentMediaFile.File_Name];
    NSAssert(movieFilePath, @"movieFilePath is nil");
    
    NSURL *fileURL = [NSURL fileURLWithPath:movieFilePath];
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    
    playerViewController.player = [AVPlayer playerWithURL:fileURL];
    
    self.avPlayerViewcontroller = playerViewController;
    
    self.avPlayerViewcontroller.view.frame = self.view.frame;
    
    
    [view addSubview:playerViewController.view];
    
    view.autoresizesSubviews = TRUE;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    
    
 
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) resizePlayerToViewSize
{
    CGRect frame ;

    if (!showStatusBar) {
        frame= self.view.bounds;
    }
    else{
        frame=videoContainerView.bounds;
    }

    
    //NSLog(@"frame size %d, %d", (int)frame.size.width, (int)frame.size.height);
    
    self.avPlayerViewcontroller.view.frame = frame;
}
-(void)StopVideo
{
    [_avPlayerViewcontroller.player seekToTime:CMTimeMake(0, 1)];
    [_avPlayerViewcontroller.player pause];
}

- (IBAction)closeButtonTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

