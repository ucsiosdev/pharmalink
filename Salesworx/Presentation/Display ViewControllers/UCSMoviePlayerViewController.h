//
//  UCSMoviePlayerViewController.h
//  UCSPresentation
//
//  Created by Pavan Kumar Singamsetti on 2/8/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "SalesWorxCustomClass.h"
#import "SWDefaults.h"
#import "SalesWorxNavigationHeaderLabel.h"

@interface UCSMoviePlayerViewController : UIViewController
{
    
    IBOutlet NSLayoutConstraint *headerViewHeightConstrint;
}
@property (nonatomic) CGRect defaultFrame;
@property (strong,nonatomic) NSString *resourceName;
@property(strong,nonatomic) ProductMediaFile * currentMediaFile;

@property (strong, nonatomic) IBOutlet SalesWorxNavigationHeaderLabel *titleLbl;
- (IBAction)closeButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property(nonatomic) BOOL showStatusBar;
@property (strong, nonatomic) IBOutlet UIView *videoContainerView;
-(void)updateMovieFile:(ProductMediaFile*)selectedMovieFile;

@end
