//
//  UCSPresentationViewerViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/18/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "UCSPdfViewController.h"
#import "UCSMoviePlayerViewController.h"
#import "UCSPdfViewController.h"
#import "UCSWebViewController.h"
#import "UCSImageDisplayViewController.h"
#import "MBProgressHUD.h"


@protocol UCSPresentationDelegate <NSObject>

-(void)demoedMedia:(NSMutableArray*)demoedMedia;

@end

@interface UCSPresentationViewerViewController : UIViewController
{
    IBOutlet UIScrollView *mediaPresentationScrollView;
    BOOL isfirstAppeareanceExecutionDone;
    CGRect scrollViewRect;
    NSTimer * demoTimer;
    NSMutableArray* demoedMediaArray;
    
    id demoedMediaDelegate;
    
    NSInteger loadedPageIndex;
    NSInteger loadedIndex;
    NSMutableArray* matchedIndexArray;

}
@property(nonatomic) id demoedMediaDelegate;
@property(strong,nonatomic) NSMutableArray *MediaFiles;
@property(nonatomic) BOOL isFromEditor;
@end
