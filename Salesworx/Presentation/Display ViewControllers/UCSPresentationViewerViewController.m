//
//  UCSPresentationViewerViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/18/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "UCSPresentationViewerViewController.h"

@interface UCSPresentationViewerViewController ()

@end

@implementation UCSPresentationViewerViewController
@synthesize MediaFiles;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Presentation"];
    demoedMediaArray=[[NSMutableArray alloc]init];
    
    if (MediaFiles.count>0) {
    ProductMediaFile* firstMediaFile=[MediaFiles objectAtIndex:0];
    [demoedMediaArray addObject:firstMediaFile.Media_File_ID];
    }
    
    loadedPageIndex=3;
    loadedIndex=1;
    matchedIndexArray=[[NSMutableArray alloc]init];
    
    //show only videos
    
    NSMutableArray* tempArray=[[NSMutableArray alloc]init];
    
    
    for (ProductMediaFile *currentFile in MediaFiles) {
        
        if ([currentFile.Media_Type isEqualToString:@"Video"]) {
            
            [tempArray addObject:currentFile];
        }
        
    }
   // MediaFiles=tempArray;
    NSLog(@"media files are %@", [MediaFiles valueForKey:@"Media_Type"]);
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    UIBarButtonItem* doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonTapped)];
    [doneButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=doneButton;

    self.navigationItem.rightBarButtonItem.enabled=NO;
    
    self.navigationItem.hidesBackButton=YES;

    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)closePresentation
{
    if (self.isFromEditor) {
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)doneButtonTapped
{
    NSLog(@"done button tapped");
    
    self.navigationItem.rightBarButtonItem.enabled=NO;
    if (self.demoedMediaDelegate!=nil) {
        
    if ([self.demoedMediaDelegate respondsToSelector:@selector(demoedMedia:)]) {
        
        [self.demoedMediaDelegate demoedMedia:demoedMediaArray];
    }
    }
    
    NSLog(@"view controller stack is %@", self.navigationController.viewControllers);
    
    if (self.isFromEditor) {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}



-(void)createPresentationViewer
{
    CGFloat pageWidth=scrollViewRect.size.width;
    CGFloat pageHeight=scrollViewRect.size.height;
    mediaPresentationScrollView.contentSize = CGSizeMake(pageWidth*MediaFiles.count, pageHeight);
    mediaPresentationScrollView.pagingEnabled = YES;
    
    NSMutableArray* tempArray=[[MediaFiles subarrayWithRange:NSMakeRange(0, 3)] mutableCopy];
    
    
    for (NSInteger i=0; i<tempArray.count; i++) {
        ProductMediaFile* currentMediaFile=[tempArray objectAtIndex:i];
        NSString* resourceName=currentMediaFile.File_Name;
        
        NSString* mediaTypeString= [[SWDefaults getValidStringValue:currentMediaFile.Media_Type] uppercaseString];
        
        if ([mediaTypeString isEqualToString:@"IMAGE"]) {
            UCSImageDisplayViewController *imageViewController = [[UCSImageDisplayViewController alloc]initWithNibName:@"UCSImageDisplayViewController" bundle:[NSBundle mainBundle]];
            CGRect frame = scrollViewRect;
            frame.origin.x = pageWidth*i;
            imageViewController.view.frame = frame;
            imageViewController.currentMediaFile=currentMediaFile;
            [self addChildViewController:imageViewController];
            [mediaPresentationScrollView addSubview:imageViewController.view];
            [imageViewController didMoveToParentViewController:self];
        }
        else if ([mediaTypeString isEqualToString:@"VIDEO"])
        {
            UCSMoviePlayerViewController *vodeoViewController = [[UCSMoviePlayerViewController alloc]initWithNibName:@"UCSMoviePlayerViewController" bundle:[NSBundle mainBundle]];
            CGRect frame = scrollViewRect;
            frame.origin.x = pageWidth*i;
            vodeoViewController.view.frame = frame;
            vodeoViewController.currentMediaFile=currentMediaFile;
            [self addChildViewController:vodeoViewController];
            [mediaPresentationScrollView addSubview:vodeoViewController.view];
            [vodeoViewController didMoveToParentViewController:self];
        }
        else if ([mediaTypeString isEqualToString:@"BROCHURE"])
        {
            UCSPdfViewController *pdfViewController = [[UCSPdfViewController alloc]initWithNibName:@"UCSPdfViewController" bundle:[NSBundle mainBundle]];
            CGRect frame = scrollViewRect;
            frame.origin.x = pageWidth*i;
            pdfViewController.view.frame = frame;
            pdfViewController.showStatusBar=NO;
            pdfViewController.currentMediaFile=currentMediaFile;
            [self addChildViewController:pdfViewController];
            [mediaPresentationScrollView addSubview:pdfViewController.view];
            //[pdfViewController addPdfToTheView];
            [pdfViewController didMoveToParentViewController:self];
            [mediaPresentationScrollView updateConstraints];
        }
        else
        {
            UCSWebViewController *webVC = [[UCSWebViewController alloc]initWithNibName:@"UCSWebViewController" bundle:[NSBundle mainBundle]];
            webVC.requestURLString=resourceName;
            webVC.currentMediaFile=currentMediaFile;
            CGRect frame = scrollViewRect;
            frame.origin.x = pageWidth*i;
            webVC.view.frame = frame;
            [self addChildViewController:webVC];
            [mediaPresentationScrollView addSubview:webVC.view];
            [webVC didMoveToParentViewController:self];
        }
    }
    
    [mediaPresentationScrollView setScrollEnabled:YES];
    
    mediaPresentationScrollView.contentSize = CGSizeMake(pageWidth*MediaFiles.count, pageHeight);
    mediaPresentationScrollView.pagingEnabled = YES;
    
    [mediaPresentationScrollView layoutIfNeeded];
    [mediaPresentationScrollView layoutSubviews];
    [mediaPresentationScrollView updateConstraints];
    
    [mediaPresentationScrollView setContentOffset:CGPointZero animated:YES];
    
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    self.navigationItem.rightBarButtonItem.enabled=YES;
    
    //    CGRect frame = mediaPresentationScrollView.frame;
    //    frame.origin.y += mediaPresentationScrollView.contentInset.top;
    //    frame.size.height -= mediaPresentationScrollView.contentInset.top;
    //    NSLog(@"temp frame is %@", NSStringFromCGRect(frame));
    
}
// From the container view controller
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if(isfirstAppeareanceExecutionDone == NO) {
        // Do your stuff
        scrollViewRect=mediaPresentationScrollView.bounds;
        
        isfirstAppeareanceExecutionDone=YES;
        [self createPresentationViewer];
    }
    
    [self updateMediaContentforPage:0];
   // [self updateLazyLoadingforPage:0];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"did receive memory warning in ucs presentation VC");
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger pageIndex =mediaPresentationScrollView.contentOffset.x / mediaPresentationScrollView.bounds.size.width;
    
    [self updateMediaContentforPage:pageIndex];
    //[self updateLazyLoadingforPage:pageIndex];

}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float bottomEdge = scrollView.contentOffset.x + scrollView.frame.size.width;
    if (bottomEdge >= scrollView.contentSize.width)
    {
        // we are at the end
        NSLog(@"you have reached the end of scroll");
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (demoTimer) {
        demoTimer=nil;
    }
    //start the timer to check if images for displayed for more than 5 secs
    demoTimer=[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(startDemoTimer) userInfo:nil repeats:NO];
    
   }


-(void)updateContentOffSet
{
    
    [mediaPresentationScrollView setScrollEnabled:YES];
    
    mediaPresentationScrollView.contentSize = CGSizeMake(scrollViewRect.size.width*MediaFiles.count, scrollViewRect.size.height);
    mediaPresentationScrollView.pagingEnabled = YES;
    
    [mediaPresentationScrollView layoutIfNeeded];
    [mediaPresentationScrollView layoutSubviews];
    [mediaPresentationScrollView updateConstraints];
    
    [mediaPresentationScrollView setContentOffset:CGPointZero animated:YES];
}

-(void)showImageForPageAtIndex:(NSInteger)idx
{
    
    CGFloat pageWidth=scrollViewRect.size.width;
    UCSImageDisplayViewController *imageViewController = [[UCSImageDisplayViewController alloc]initWithNibName:@"UCSImageDisplayViewController" bundle:[NSBundle mainBundle]];
    CGRect frame = scrollViewRect;
    frame.origin.x = pageWidth*idx;
    imageViewController.view.frame = frame;
    imageViewController.currentMediaFile=[MediaFiles objectAtIndex:idx];
    [imageViewController updateImage:[MediaFiles objectAtIndex:idx]];

    
    
    [self addChildViewController:imageViewController];
    [mediaPresentationScrollView addSubview:imageViewController.view];
    [imageViewController didMoveToParentViewController:self];

    
}

-(void)hideImageForPageAtIndex:(NSInteger)idx
{
    UCSImageDisplayViewController * currentImageVC= [self.childViewControllers objectAtIndex:idx];
    [currentImageVC removeImage];
  
    [currentImageVC removeFromParentViewController];
    [currentImageVC.view removeFromSuperview];
}

-(void)showVideoForPageAtIndex:(NSInteger)idx
{
    CGFloat pageWidth=scrollViewRect.size.width;

    UCSMoviePlayerViewController *videoViewController = [[UCSMoviePlayerViewController alloc]initWithNibName:@"UCSMoviePlayerViewController" bundle:[NSBundle mainBundle]];
    CGRect frame = scrollViewRect;
    frame.origin.x = pageWidth*idx;
    videoViewController.view.frame = frame;
    videoViewController.currentMediaFile=[MediaFiles objectAtIndex:idx];
    [self addChildViewController:videoViewController];
    [mediaPresentationScrollView addSubview:videoViewController.view];
    [videoViewController didMoveToParentViewController:self];


}

-(void)hideVideoForPageAtIndex:(NSInteger)idx

{
    UCSMoviePlayerViewController * currentMovieVC= [self.childViewControllers objectAtIndex:idx];
    
    [currentMovieVC removeFromParentViewController];
    [currentMovieVC.view removeFromSuperview];

}


-(void)showPDFForPageAtIndex:(NSInteger)idx
{
    CGFloat pageWidth=scrollViewRect.size.width;

    UCSPdfViewController *pdfViewController = [[UCSPdfViewController alloc]initWithNibName:@"UCSPdfViewController" bundle:[NSBundle mainBundle]];
    CGRect frame = scrollViewRect;
    frame.origin.x = pageWidth*idx;
    pdfViewController.view.frame = frame;
    pdfViewController.showStatusBar=NO;
    pdfViewController.currentMediaFile=[MediaFiles objectAtIndex:idx];
    [self addChildViewController:pdfViewController];
    [mediaPresentationScrollView addSubview:pdfViewController.view];
    [pdfViewController addPdfToTheView];
    [pdfViewController didMoveToParentViewController:self];
    [mediaPresentationScrollView updateConstraints];

}



-(void)hidePDFForPageAtIndex:(NSInteger)idx
{
    UCSPdfViewController * currentPDFVC= [self.childViewControllers objectAtIndex:idx];
    [currentPDFVC removePDFfile];
    
    [currentPDFVC removeFromParentViewController];
    [currentPDFVC.view removeFromSuperview];

}
-(void)showPPTForPageAtIndex:(NSInteger)idx
{
    CGFloat pageWidth=scrollViewRect.size.width;

    ProductMediaFile* currentMediaFile=[MediaFiles objectAtIndex:idx];
    NSString* resourceName=currentMediaFile.File_Name;

    UCSWebViewController *webVC = [[UCSWebViewController alloc]initWithNibName:@"UCSWebViewController" bundle:[NSBundle mainBundle]];
    [webVC updateWebView:currentMediaFile];
    webVC.requestURLString=resourceName;
    webVC.currentMediaFile=currentMediaFile;
    CGRect frame = scrollViewRect;
    frame.origin.x = pageWidth*idx;
    webVC.view.frame = frame;
    [self addChildViewController:webVC];
    [mediaPresentationScrollView addSubview:webVC.view];
    [webVC didMoveToParentViewController:self];

    
}
-(void)hidePPTForPageAtIndex:(NSInteger)idx
{
    UCSWebViewController * currentWebVC= [self.childViewControllers objectAtIndex:idx];
    [currentWebVC resetWebView];
    
    [currentWebVC removeFromParentViewController];
    [currentWebVC.view removeFromSuperview];

}



-(void)updateMediaContentforPage:(NSInteger)pageIdx
{
    for (NSInteger i=0; i<self.childViewControllers.count; i++) {
        if (i==pageIdx||i==pageIdx-1||i==pageIdx+1) {
            
            UIViewController* currentVC=(UIViewController*)[self.childViewControllers objectAtIndex:i];
            
            if ([currentVC isKindOfClass:[UCSImageDisplayViewController class]]) {
                [self showImageForPageAtIndex:i];
            }
            else if ([currentVC isKindOfClass:[UCSMoviePlayerViewController class]])
            {
                [self showVideoForPageAtIndex:i];
            }
            else if ([currentVC isKindOfClass:[UCSPdfViewController class]])
            {
                [self showPDFForPageAtIndex:i];
            }
            else if ([currentVC isKindOfClass:[UCSWebViewController class]])
            {
                [self showPPTForPageAtIndex:i];
            }
        }
        else
        {
            UIViewController* currentVC=(UIViewController*)[self.childViewControllers objectAtIndex:i];
            
            if ([currentVC isKindOfClass:[UCSImageDisplayViewController class]]) {
                [self hideImageForPageAtIndex:i];
            }
            else if ([currentVC isKindOfClass:[UCSMoviePlayerViewController class]])
            {
                [self hideVideoForPageAtIndex:i];
            }
            else if ([currentVC isKindOfClass:[UCSPdfViewController class]])
            {
                [self hidePDFForPageAtIndex:i];
            }
            else if ([currentVC isKindOfClass:[UCSWebViewController class]])
            {
                [self hidePPTForPageAtIndex:i];
            }
        }
        
        
    }
    
    
    /*
    NSInteger pagenumber = pageIdx;

    
    if (pagenumber>loadedPageIndex)
    {
    loadedPageIndex=pagenumber+3;
    
    NSArray* vcArray=[self.childViewControllers subarrayWithRange:NSMakeRange(pagenumber,loadedPageIndex)];

    for (NSInteger currentVCIndex=0; currentVCIndex<vcArray.count; currentVCIndex++) {
        
        UIViewController* currentVC=(UIViewController*)[self.childViewControllers objectAtIndex:currentVCIndex];
        
        if ([currentVC isKindOfClass:[UCSImageDisplayViewController class]]) {
            [self showImageForPageAtIndex:pagenumber];
        }
         else if ([currentVC isKindOfClass:[UCSMoviePlayerViewController class]])
         {
             [self showVideoForPageAtIndex:pagenumber];
         }
         else if ([currentVC isKindOfClass:[UCSPdfViewController class]])
         {
             [self showPDFForPageAtIndex:pagenumber];
         }
         else if ([currentVC isKindOfClass:[UCSWebViewController class]])
         {
             [self showPPTForPageAtIndex:pagenumber];
         }
            
        
    
    }
    }
    */
    
}


-(void)startDemoTimer
{
    NSInteger pagenumber = mediaPresentationScrollView.contentOffset.x / mediaPresentationScrollView.bounds.size.width;
    if (MediaFiles.count>pagenumber) {
        ProductMediaFile* currentMediaFile=[MediaFiles objectAtIndex:pagenumber];
        [demoedMediaArray addObject: currentMediaFile.Media_File_ID];
    }
    [demoTimer invalidate];
    demoTimer=nil;
}


@end
