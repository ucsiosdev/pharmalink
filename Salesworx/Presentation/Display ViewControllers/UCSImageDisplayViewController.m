//
//  UCSImageDisplayViewController.m
//  UCSPresentation
//
//  Created by Pavan Kumar Singamsetti on 2/8/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import "UCSImageDisplayViewController.h"

@interface UCSImageDisplayViewController ()

@end

@implementation UCSImageDisplayViewController
@synthesize resourceName,presentationImageView,currentMediaFile,showStatusBar,headerView,titleLbl;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (!showStatusBar) {
        headerView.hidden=YES;
        headerViewHeightConstrint.constant=0.0f;
    }
    else
    {
        titleLbl.text=currentMediaFile.Product_Name;
        headerViewHeightConstrint.constant=64.0f;

    }

    
    
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    presentationImageView.image=[UIImage imageWithContentsOfFile:[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentMediaFile.File_Name]];

}


-(void)viewDidAppear:(BOOL)animated
{


}


-(void)updateImage:(ProductMediaFile*)currentImage
{
    presentationImageView.image=[UIImage imageWithContentsOfFile:[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:currentImage.File_Name]];
}
-(void)removeImage
{
    presentationImageView.image=nil;
}


- (IBAction)closeButtonTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
