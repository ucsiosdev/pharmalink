//
//  PresentationDestinationCollectionViewCell.h
//  MedRep
//
//  Created by Unique Computer Systems on 10/9/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentationDestinationCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *selectedImgView;
@property (strong, nonatomic) IBOutlet UILabel *captionLbl;
@property (strong, nonatomic) IBOutlet UIImageView *placeHolderImageView;
@property (strong, nonatomic) IBOutlet UIImageView *captionBgView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productImgViewTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productImageViewTrailingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productimageViewLeadingConstraint;
@property (strong, nonatomic) IBOutlet UILabel *saperatorLabel;
@property (strong, nonatomic) IBOutlet UILabel *countLbl;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@end
