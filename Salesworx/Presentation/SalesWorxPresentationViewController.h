//
//  SalesWorxPresentationViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/14/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepProductsCollectionViewCell.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"
#import "MyModel.h"
#import "CardCell.h"
#import "CardView.h"

#import "UCSPresentationViewerViewController.h"
#import "NSString+Additions.h"
#import "SalesWorxPresentationHomeViewController.h"
#import "SalesWorxPresentationHelpViewController.h"

@protocol SalesWorxPresentationDelegate <NSObject>

-(void)createdPresentations:(NSMutableArray*)presentationsArray;
-(void)updatedVisitWithPresentations:(VisitDetails*)updatedDetails;
@end

@interface SalesWorxPresentationViewController : UIViewController<UIGestureRecognizerDelegate,UITextFieldDelegate>

{
    IBOutlet UIButton *productButton;
    IBOutlet UICollectionView * productMediaCollectionView;
    IBOutlet SalesWorxDropShadowView *destinationPlaceHolderView;
    UIPanGestureRecognizer *panGesture;
    IBOutlet UICollectionView *presentationCollectionView;
    
    CGRect currentFrame;
    
    NSMutableArray* productMediaArray;
    NSMutableArray* productMediaDBArray;
    
    IBOutlet UIView *headerView;
    NSMutableArray* productImagesArray;
    NSMutableArray* productVideosArray;
    NSMutableArray* productPdfArray;
    NSMutableArray* productPresentationsArray;
    
    NSMutableArray* destinationCollectionViewMediaArray;
    IBOutlet UILabel *dragnDropPlaceHolderLabel;
    
    IBOutlet UIImageView *footerImageView;
    //MyModel *_selectedModel;
   // MyModel *_model;
//    ProductMediaFile *_model;
    ProductMediaFile *_selectedModel;
    
    //CardView *_draggedCard;
    NSMutableArray *_models;

    NSMutableArray* sourceMediaArray;
    NSMutableArray* destinationMediaArray;

    IBOutlet MedRepElementTitleDescriptionLabel *titleLbl;
    
    
    CGRect scrollViewRect;

    id presentationDelegate;
    
    NSMutableArray* presentationMedia;
    
    BOOL presentationViewed;
    
    CAShapeLayer *placeHolderBorder;
    
}

@property(nonatomic) id presentationDelegate;
@property(strong,nonatomic) NSString* selectedPresentationID;
@property(strong,nonatomic) NSString* selectedDemoPlanID;

- (IBAction)doneButtonTapped:(id)sender;

@property(nonatomic) BOOL isCreatePresentation;
@property(nonatomic) BOOL isEditPresentation;

@property (strong, nonatomic) IBOutlet UIButton *imagesButton;
@property (strong, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *pdfButton;
@property (strong, nonatomic) IBOutlet UIButton *pptButton;
@property(strong,nonatomic) VisitDetails* visitDetails;
- (IBAction)closeButtonTapped:(id)sender;
- (void)setSelectedModel:(ProductMediaFile *)model atPoint:(CGPoint)point;
- (void)updateCardViewDragState:(BOOL)validDropPoint;
- (BOOL)isValidDragPoint:(CGPoint)point;
-(void)hideCardView;


@property(strong,nonatomic)UINavigationController* presentationNavController;
@end
