//
//  SalesWorxPresentationViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 9/14/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPresentationViewController.h"
#import "MBProgressHUD.h"
#import "SourceViewController.h"
#import "DestinationViewController.h"

@interface SalesWorxPresentationViewController ()
{
    SourceViewController *_sourceViewController;
    DestinationViewController *_destinationViewController;
    
    CardView *_draggedCard;
//    MyModel *_model;
    ProductMediaFile *_model;

    
}
@end

@implementation SalesWorxPresentationViewController
@synthesize imagesButton,videoButton,pdfButton,pptButton,visitDetails,selectedPresentationID,isEditPresentation;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    currentFrame= CGRectMake(8, 8, 1008, 502);
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hidePlaceHolder) name:@"ClearPresentationPlaceHolder" object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showPlaceHolder) name:@"ShowDragAndDropPlaceHolder" object:nil];

    
    
    UIBarButtonItem* cancelButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped:)];
    [cancelButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=cancelButton;
    
    
    UIBarButtonItem* doneButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonTapped:)];
    [doneButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    
    UIBarButtonItem* helpButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Help", nil) style:UIBarButtonItemStylePlain target:self action:@selector(helpButtonButtonTapped)];
    [helpButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];

    
    UIBarButtonItem* deleteButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Delete", nil) style:UIBarButtonItemStylePlain target:self action:@selector(deleteButtonTapped)];
    [deleteButton setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItems=@[doneButton,helpButton];
    
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Create Presentation"];

    
    productVideosArray =[[NSMutableArray alloc]init];
    productPdfArray =[[NSMutableArray alloc]init];
    productPresentationsArray=[[NSMutableArray alloc]init];
    productMediaDBArray=[[NSMutableArray alloc]init];
    sourceMediaArray=[[NSMutableArray alloc]init];
    destinationMediaArray=[[NSMutableArray alloc]init];
    
   
    
    
   // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    
        [self fetchMediaFilesData];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (productMediaDBArray.count>0) {
                productImagesArray=[[productMediaDBArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Image'"]] mutableCopy];
                productVideosArray= [[productMediaDBArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Video'"]] mutableCopy];
                productPdfArray=[[productMediaDBArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Brochure'"]] mutableCopy];
                productPresentationsArray=[[productMediaDBArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Media_Type == 'Powerpoint'"]] mutableCopy];
                
                productMediaArray=[[NSMutableArray alloc]init];
                //                collectionViewDataArray=productImagesArray;
                //                [demoPlanProductsCollectionView reloadData];
            
                
                
                _sourceViewController = [[SourceViewController alloc] initWithCollectionView:productMediaCollectionView andParentViewController:self withMediaArray:productImagesArray];
                
                
                UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
                flow.itemSize = CGSizeMake(130,130);
                flow.scrollDirection = UICollectionViewScrollDirectionVertical;
                flow.minimumInteritemSpacing = 0;
                flow.minimumLineSpacing = 0;
                presentationCollectionView.collectionViewLayout = flow;
                _destinationViewController = [[DestinationViewController alloc] initWithCollectionView:presentationCollectionView ];
                
                [self initDraggedCardView];
                
                panGesture =
                [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
                panGesture.delegate = self;
                [self.view addGestureRecognizer:panGesture];
                

                [self mediaActionButtonTapped:imagesButton];
                /*
                [self imagesButtonTapped:nil];
                
                [self setUpSourceViewForDragandDrop];
                [self setUpDestinationViewForDragandDrop];
                
                [self initDraggedCardView];
                
                UIPanGestureRecognizer *panGesture =
                [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
                panGesture.delegate = self;
                [self.view addGestureRecognizer:panGesture];*/
                
                
                
                
                if (visitDetails.fieldMarketingPresentationsArray.count>0 && self.isCreatePresentation==NO) {
                    for (NSInteger i=0; i<visitDetails.fieldMarketingPresentationsArray.count; i++) {
                        
                        if (![NSString isEmpty:selectedPresentationID]) {
                            NSPredicate* presentationPredicate=[NSPredicate predicateWithFormat:@"SELF.presentationID == %@",selectedPresentationID];
                            NSMutableArray* selectedPresentation=[[visitDetails.fieldMarketingPresentationsArray filteredArrayUsingPredicate:presentationPredicate] mutableCopy];
                            if (selectedPresentation.count>0) {
                                destinationMediaArray=[[selectedPresentation objectAtIndex:0] valueForKey:@"presentationContentArray"] ;
                            }
                        }
                        else{
                            //for defualt presentation
                            destinationMediaArray=[[visitDetails.fieldMarketingPresentationsArray valueForKey:@"presentationContentArray"] objectAtIndex:0];
                        }
                        
                        /*
                         FieldMarketingPresentation * currentPresentation=[visitDetails.fieldMarketingPresentationsArray objectAtIndex:i];

                        if ([currentPresentation.presentationID isEqualToString:[SWDefaults getValidStringValue:selectedPresentationID]]) {
                            //for selected presentation
                            destinationMediaArray=[[visitDetails.fieldMarketingPresentationsArray valueForKey:@"presentationContentArray"] objectAtIndex:i];
                        }
                        else{
                            //for defualt presentation
                            destinationMediaArray=[[visitDetails.fieldMarketingPresentationsArray valueForKey:@"presentationContentArray"] objectAtIndex:0];
                        }*/
                        
                    }
                    
                    [_destinationViewController displayExistingModels:destinationMediaArray];

                }
            }
            else
            {
                
               
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
        });
    });
    
   // [productMediaCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    // Do any additional setup after loading the view from its nib.
    
    
    if (visitDetails.fieldMarketingPresentationsArray.count>0) {
        if (visitDetails.fieldMarketingPresentationsArray.count==1 && self.isCreatePresentation==NO) {
            [self hidePlaceHolder];
        }
        else{
           // [self showPlaceHolder];
        }
    }
    else{
        //[self showPlaceHolder];
    }
    
    if (isEditPresentation) {
        [self hidePlaceHolder];
    }

}

-(void)hidePlaceHolder
{
    if (!dragnDropPlaceHolderLabel.hidden) {
        dragnDropPlaceHolderLabel.hidden=YES;
        [placeHolderBorder removeFromSuperlayer];
    }
}
-(void)showPlaceHolder
{
    placeHolderBorder = [CAShapeLayer layer];
    placeHolderBorder.strokeColor = [UIColor lightGrayColor].CGColor;
    placeHolderBorder.fillColor = nil;
    placeHolderBorder.lineWidth=3.0;
    placeHolderBorder.lineDashPattern = @[@20, @4];
    placeHolderBorder.frame = destinationPlaceHolderView.bounds;
    placeHolderBorder.path = [UIBezierPath bezierPathWithRect:destinationPlaceHolderView.bounds].CGPath;
    [destinationPlaceHolderView.layer addSublayer:placeHolderBorder];
    dragnDropPlaceHolderLabel.hidden=NO;

}

-(void)viewWillAppear:(BOOL)animated{
   
   

    
}
-(void)deleteButtonTapped
{
    [_destinationViewController deleteIconTapped];
}
-(void)helpButtonButtonTapped
{
    SalesWorxPresentationHelpViewController * helpVC=[[SalesWorxPresentationHelpViewController alloc]init];
    [self presentViewController:helpVC animated:YES completion:nil];

}
-(void)viewDidAppear:(BOOL)animated{
    
    
    if (self.isMovingToParentViewController) {
        

        if (visitDetails.fieldMarketingPresentationsArray.count>0) {
            if (visitDetails.fieldMarketingPresentationsArray.count==1 && self.isCreatePresentation==NO) {
                [self hidePlaceHolder];
            }
            else{
                [self showPlaceHolder];
            }
        }
        else{
            [self showPlaceHolder];
        }
        
        if (isEditPresentation) {
            [self hidePlaceHolder];
        }

    }
    
}
#pragma mark - Pan Gesture Recognizers/delegate

- (void)handlePan:(UIPanGestureRecognizer *)gesture {
    
    NSLog(@"handle pan called in presentation vc");
    
    CGPoint touchPoint = [gesture locationInView:self.view];
    
    NSLog(@"coordinate point is %f, %f", touchPoint.x,touchPoint.y);
    
    
    
    BOOL validDropPoint = [self isValidDragPoint:touchPoint];
    if(gesture.state == UIGestureRecognizerStateEnded && validDropPoint )
    {
        [_destinationViewController addModelWithTouchPoint:_model withTouchPoint:touchPoint];
    }
   // NSLog(@"touch point is %f, %f", touchPoint.x,touchPoint.y);
    if (gesture.state == UIGestureRecognizerStateChanged
        && !_draggedCard.hidden) {
        // card is dragged
        //[_destinationViewController activateDestinationGesture:NO];
        
        _draggedCard.center = touchPoint;
        [self updateCardViewDragState:[self isValidDragPoint:touchPoint]];
    } else if (gesture.state == UIGestureRecognizerStateRecognized
               && _model != nil) {
        _draggedCard.hidden = YES;
        [_sourceViewController cellDragCompleteWithModel:_model withValidDropPoint:validDropPoint];
        if (validDropPoint) {
            //[_destinationViewController addModel:_model];
            // [_destinationViewController addModelWithTouchPoint:_model withTouchPoint:touchPoint];
            //[sampleDestination addModel:[NSString stringWithFormat:@"%d",_model.value]];
            
        }
        _model = nil;
    }
}

- (void)cellDragCompleteWithModel:(ProductMediaFile *)model withValidDropPoint:(BOOL)validDropPoint {
    if (model != nil) {
        // get indexPath for the model
        NSUInteger index = [_models indexOfObject:model];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        
        if (validDropPoint && indexPath != nil) {
            // [_models removeObjectAtIndex:index];
            //[_collectionView deleteItemsAtIndexPaths:@[indexPath]];
            
            [productMediaCollectionView reloadData];
        } else {
            
            UICollectionViewCell *cell = [productMediaCollectionView cellForItemAtIndexPath:indexPath];
            cell.alpha = 1.0f;
        }
    }
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
//    
//    if ([gestureRecognizer isEqual:panGesture] && [otherGestureRecognizer isEqual:presentationCollectionView.gestureRecognizers]){
//        return YES;
//    }
//    else if ([otherGestureRecognizer isEqual:productMediaCollectionView.gestureRecognizers] && [gestureRecognizer isEqual:panGesture])
//    {
//        return YES;
//    }
    
    return YES;
}


#pragma mark Source View Methods

-(void)setUpSourceViewForDragandDrop
{

    sourceMediaArray = [NSMutableArray array];
    sourceMediaArray=productImagesArray;
    
//    for (int i=0; i<10; i++) {
//        [sourceMediaArray addObject:[[MyModel alloc] initWithValue:i]];
//    }

    
    [productMediaCollectionView registerClass:[MedRepProductsCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                   action:@selector(handlePress:)];
    longPressGesture.numberOfTouchesRequired = 1;
    [productMediaCollectionView addGestureRecognizer:longPressGesture];
    
    [productMediaCollectionView reloadData];

}
- (void)handlePress:(UILongPressGestureRecognizer *)gesture {
    CGPoint point = [gesture locationInView:productMediaCollectionView];
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        NSIndexPath *indexPath = [productMediaCollectionView indexPathForItemAtPoint:point];
        if (indexPath != nil) {
            _selectedModel = [sourceMediaArray objectAtIndex:indexPath.item];
            
            // calculate point in parent view
            point = [gesture locationInView:self.view];
            
            [self setSelectedModel:_selectedModel atPoint:point];
            
            // hide the cell
            [productMediaCollectionView cellForItemAtIndexPath:indexPath].alpha = 0.0f;
        }
    } else if (gesture.state==UIGestureRecognizerStateEnded)
    {
        NSLog(@"gesture state has ended in presentation vc");
    }
    
}



- (void)setSelectedModel:(ProductMediaFile *)model atPoint:(CGPoint)point {
    _model = model;
    
    if (_model != nil) {
        
        _draggedCard.label.text=[NSString getValidStringValue:model.Caption];
        
        NSLog(@"dragged label frame is %@", _draggedCard.label);
        
        if ([model.Media_Type isEqualToString:@"Image"]) {
            NSString* thumbnailImageName = [SWDefaults getValidStringValue:model.File_Name];
            _draggedCard.placeHolderImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
            
        }
        else if ([model.Media_Type isEqualToString:@"Video"])
        {
            _draggedCard.placeHolderImageView.image=[UIImage imageNamed:@"PresentationVideoPlaceHolder"];
        }
        else if ([model.Media_Type isEqualToString:@"Brochure"])
        {
            _draggedCard.placeHolderImageView.image=[UIImage imageNamed:@"PresentationPdfPlaceHolder"];

        }
        else if ([model.Media_Type isEqualToString:@"Powerpoint"])
        {
            _draggedCard.placeHolderImageView.image=[UIImage imageNamed:@"PresentationPptPlaceHolder"];
        }
        
       // _draggedCard.label.text = [NSString stringWithFormat:@"%@", model.Caption];
        _draggedCard.center = point;
        _draggedCard.hidden = NO;
        
        [self updateCardViewDragState:[self isValidDragPoint:point]];
    } else {
        _draggedCard.hidden = YES;
    }
}

#pragma mark - Validation helper methods on drag and drop
- (BOOL)isValidDragPoint:(CGPoint)point {
    return !CGRectContainsPoint(productMediaCollectionView.frame, point);
}

- (void)updateCardViewDragState:(BOOL)validDropPoint {
    if (validDropPoint) {
        _draggedCard.alpha = 1.0f;
    } else {
        _draggedCard.alpha = 0.2f;
    }
}
-(void)hideCardView
{
    _draggedCard.hidden=YES;
}

#pragma mark - initialization code
- (void)initDraggedCardView {
    
//    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[productMediaCollectionView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if(cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UnconfirmedHeaderTableViewCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
//    
//    cell.custNameLbl.text= @"Customer Name";
//    cell.orderAmountLbl.text=@"Order Amount";
//    cell.docRefNumLbl.text=@"Doc Ref Number";
//    cell.dateLbl.text=@"Date";
//    return cell;
    
    _draggedCard = [[CardView alloc] initWithFrame:CGRectMake(0, 0, 160, 162)];
    _draggedCard.hidden = YES;
    [_draggedCard setHighlightSelection:YES];
    
    [self.view addSubview:_draggedCard];
}

#pragma mark DestinationView

-(void)setUpDestinationViewForDragandDrop
{
    [presentationCollectionView registerClass:[PresentationDestinationCollectionViewCell class] forCellWithReuseIdentifier:@"destinationCell"];
    
    UILongPressGestureRecognizer * longPressGesture=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
    [presentationCollectionView addGestureRecognizer:longPressGesture];
    

}
- (void)handleLongPress:(UILongPressGestureRecognizer *)gr{
    
    switch(gr.state){
        case UIGestureRecognizerStateBegan:
        {
            NSIndexPath *selectedIndexPath = [presentationCollectionView indexPathForItemAtPoint:[gr locationInView:presentationCollectionView]];
            if(selectedIndexPath == nil)
                break;
            [presentationCollectionView beginInteractiveMovementForItemAtIndexPath:selectedIndexPath];
            //[self activateDestinationGesture:YES];
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            [presentationCollectionView updateInteractiveMovementTargetPosition:[gr locationInView:gr.view]];
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            [presentationCollectionView endInteractiveMovement];
            break;
        }
        default:
        {
            [presentationCollectionView cancelInteractiveMovement];
            break;
        }
    }
}

//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"did select in destination called");
//}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    //add your data source manipulation logic here
    //specifically, change the order of entries in the data source to match the new visual order of the cells.
    //even without anything inside this function, the cells will move visually if you build and run
    [collectionView reloadData];

}

- (void)addModel:(ProductMediaFile *)model {
    [_models addObject:model];
    [presentationCollectionView reloadData];
}

-(NSMutableArray*)fetchModels
{
    return _models;
}
//-(void)addModelWithTouchPoint:(ProductMediaFile*)model withTouchPoint:(CGPoint)touchPoint
//{
//    NSIndexPath* indexPathToReplace= [presentationCollectionView indexPathForItemAtPoint:touchPoint];
//    if (_models.count>0 && indexPathToReplace!=nil && model!=nil) {
//        
////        [_models insertObject:model atIndex:indexPathToReplace.row];
//        [destinationMediaArray insertObject:model atIndex:indexPathToReplace.row];
//        [presentationCollectionView reloadData];
//        
//        
//      
//        
//    }
//    else if (model!=nil)
//    {
//        [destinationMediaArray addObject:model];
//        [presentationCollectionView reloadData];
//        
//    }
//    else
//    {
//        
//    }
//    
//    [productMediaCollectionView reloadData];
//}



-(void)fetchMediaFilesData
{
    NSString* demoPlanID=self.selectedDemoPlanID;
    NSLog(@"fetching media content for demo plan id %@", demoPlanID);
    
    if (demoPlanID!=nil)
    {
        productMediaDBArray=[MedRepQueries fetchMediaFileswithDemoPlanID:demoPlanID];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark CollectionView Media Button Actions
- (IBAction)imagesButtonTapped:(id)sender {
    [imagesButton setSelected:YES];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [pdfButton setSelected:NO];
    
   
    
    
    footerImageView.image=[UIImage imageNamed:@"PresentationImageFileSelected"];
    
    
    
    
    productMediaArray=[[NSMutableArray alloc]init];
    productMediaArray=productImagesArray;
    //[productMediaCollectionView reloadData];
    [_sourceViewController updateSourceMediaContent:productImagesArray];

}

- (IBAction)videoButtonTapped:(id)sender {
    
    footerImageView.image=[UIImage imageNamed:@"PresentationVideoFileSelected"];

    
    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:YES];
    [pdfButton setSelected:NO];
    pdfButton.layer.borderWidth=0.0;
    if ([videoButton isSelected]) {
        videoButton .layer.borderWidth=2.0;
        videoButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    productMediaArray=[[NSMutableArray alloc]init];
    productMediaArray=productVideosArray;
   // [productMediaCollectionView reloadData];
    
    [_sourceViewController updateSourceMediaContent:productVideosArray];

}

- (IBAction)pdfButtonTapped:(id)sender {
    
    footerImageView.image=[UIImage imageNamed:@"PresentationPDFFileSelected"];

    [imagesButton setSelected:NO];
    [pdfButton setSelected:YES];
    [videoButton setSelected:NO];
    [pptButton setSelected:NO];
    pdfButton.layer.borderWidth=0.0;
    if ([pdfButton isSelected]) {
        pdfButton .layer.borderWidth=2.0;
        pdfButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    productMediaArray=[[NSMutableArray alloc]init];
    productMediaArray=productPdfArray;
    //[productMediaCollectionView reloadData];
    
    [_sourceViewController updateSourceMediaContent:productPdfArray];

}
- (IBAction)presentationButtonTapped:(id)sender {
    
    footerImageView.image=[UIImage imageNamed:@"PresentationPPTFileSelected"];

    [imagesButton setSelected:NO];
    [pptButton setSelected:NO];
    [videoButton setSelected:NO];
    [pdfButton setSelected:NO];
    [pptButton setSelected:YES];
    
    if ([pptButton isSelected]) {
        pptButton .layer.borderWidth=2.0;
        pptButton.layer.borderColor=[[UIColor colorWithRed:(117.0/255.0) green:(199.0/255.0) blue:(226.0/255.0) alpha:1]CGColor];
    }
    imagesButton.layer.borderWidth=0.0;
    videoButton.layer.borderWidth=0.0;
    pptButton.layer.borderWidth=0.0;
    productMediaArray=[[NSMutableArray alloc]init];
    productMediaArray=productPresentationsArray;
    //[productMediaCollectionView reloadData];
    
    [_sourceViewController updateSourceMediaContent:productPresentationsArray];

}

-(IBAction)mediaActionButtonTapped:(id)sender
{
    UIButton* selectedButton= sender;
    productMediaArray=[[NSMutableArray alloc]init];

    if (selectedButton.tag==1000) {
        productMediaArray=productImagesArray;


        [imagesButton setImage:[UIImage imageNamed:@"MedRep_ImageBtnActive"] forState:UIControlStateNormal];
        [videoButton setImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
        [pdfButton setImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
        [pptButton setImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
        footerImageView.image=[UIImage imageNamed:@"PresentationImageFileSelected"];

    }
    else if (selectedButton.tag==1001)
    {
        productMediaArray=productVideosArray;

        [imagesButton setImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
        [videoButton setImage:[UIImage imageNamed:@"MedRep_VideoBtnActive"] forState:UIControlStateNormal];
        [pdfButton setImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
        [pptButton setImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
        footerImageView.image=[UIImage imageNamed:@"PresentationVideoFileSelected"];

    }
    else if (selectedButton.tag==1002)
    {
        productMediaArray=productPdfArray;

        [imagesButton setImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
        [videoButton setImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
        [pdfButton setImage:[UIImage imageNamed:@"MedRep_PDFBtnActive"] forState:UIControlStateNormal];
        [pptButton setImage:[UIImage imageNamed:@"MedRep_PresentationBtnInActive"] forState:UIControlStateNormal];
        footerImageView.image=[UIImage imageNamed:@"PresentationPDFFileSelected"];

    }
    else if (selectedButton.tag==1003)
    {
        productMediaArray=productPresentationsArray;

        [imagesButton setImage:[UIImage imageNamed:@"MedRep_ImageBtnInActive"] forState:UIControlStateNormal];
        [videoButton setImage:[UIImage imageNamed:@"MedRep_VideoBtnInActive"] forState:UIControlStateNormal];
        [pdfButton setImage:[UIImage imageNamed:@"MedRep_PDFBtnInActive"] forState:UIControlStateNormal];
        [pptButton setImage:[UIImage imageNamed:@"MedRep_PresentationBtnActive"] forState:UIControlStateNormal];
        footerImageView.image=[UIImage imageNamed:@"PresentationPPTFileSelected"];

    }
    
    [_sourceViewController updateSourceMediaContent:productMediaArray];

}

#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==presentationCollectionView) {
        return destinationMediaArray.count;
    }
    else{
    return  sourceMediaArray.count;
    }
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
//           viewForSupplementaryElementOfKind:(NSString *)kind
//                                 atIndexPath:(NSIndexPath *)indexPath
//{
//    
//    YourOwnSubClass *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
//                                   UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
//    [self updateSectionHeader:headerView forIndexPath:indexPath];
//    
//    return headerView;
//}

#pragma mark - UICollectionViewDelegateFlowLayout
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return CGSizeMake(100, 120);
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
     if (collectionView==productMediaCollectionView)
     {
    return UIEdgeInsetsMake(10, 20, 0, 20);
     }
     else{
         return UIEdgeInsetsMake(0, 0, 0, 0);

     }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(162, 160);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==presentationCollectionView) {
        
            //        CardCell *cell = [productMediaCollectionView dequeueReusableCellWithReuseIdentifier:CELL_REUSE_ID forIndexPath:indexPath];
            //        MyModel *model = [sourceMediaArray objectAtIndex:indexPath.item];
            //        [cell setModel:model];
            //
            //        return cell;
            
            //    }
            //
            //    else{
            
            static NSString *cellIdentifier = @"productCell";
            NSString* mediaType = [[destinationMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
            MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
            cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
            cell.selectedImgView.hidden=YES;
            NSString*documentsDirectory;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                documentsDirectory=[SWDefaults applicationDocumentsDirectory];
            }
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains
                (NSDocumentDirectory, NSUserDomainMask, YES);
                documentsDirectory = [paths objectAtIndex:0];
            }
            if ([mediaType isEqualToString:@"Image"]) {
                NSString* thumbnailImageName = [[productMediaArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
                cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
                cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
                cell.placeHolderImageView.hidden=NO;
                cell.productImgViewTopConstraint.constant=2.0;
                cell.productimageViewLeadingConstraint.constant=2.0;
                cell.productImageViewTrailingConstraint.constant=2.0;
            }
            else if ([mediaType isEqualToString:@"Video"])
            {
                cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
                cell.captionLbl.text=[[productVideosArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
                cell.productImgViewTopConstraint.constant=0.0;
                cell.productimageViewLeadingConstraint.constant=0.0;
                cell.productImageViewTrailingConstraint.constant=0.0;
            }
            else if ([mediaType isEqualToString:@"Brochure"])
            {
                cell.productImgViewTopConstraint.constant=0.0;
                cell.productimageViewLeadingConstraint.constant=0.0;
                cell.productImageViewTrailingConstraint.constant=0.0;
                cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
                cell.captionLbl.text=[[productPdfArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
            }
            else if ([mediaType isEqualToString:@"Powerpoint"])
            {
                cell.productImgViewTopConstraint.constant=0.0;
                cell.productimageViewLeadingConstraint.constant=0.0;
                cell.productImageViewTrailingConstraint.constant=0.0;
                cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
                cell.captionLbl.text=[[productPresentationsArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
            }
            cell.productImageView.backgroundColor=[UIColor clearColor];
            return cell;
        
//        CardCell *cell = (CardCell *)[presentationCollectionView dequeueReusableCellWithReuseIdentifier:CELL_REUSE_ID forIndexPath:indexPath];
//        MyModel *model = [destinationMediaArray objectAtIndex:indexPath.item];
//        cell.model = model;
//        
//        return cell;
    }
    else if (collectionView==productMediaCollectionView)
    {
//        CardCell *cell = [productMediaCollectionView dequeueReusableCellWithReuseIdentifier:CELL_REUSE_ID forIndexPath:indexPath];
//        MyModel *model = [sourceMediaArray objectAtIndex:indexPath.item];
//        [cell setModel:model];
//        
//        return cell;

//    }
//    
//    else{
        
        
    NSLog(@"destination collection called");
        
    /*
    static NSString *cellIdentifier = @"productCell";
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.selectedImgView.hidden=YES;*/
        
    static NSString *cellIdentifier = @"destinationCell";
    PresentationDestinationCollectionViewCell *cell = (PresentationDestinationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    // cell.userInteractionEnabled=YES;
    NSString* mediaType = [[sourceMediaArray valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];

    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    if ([mediaType isEqualToString:@"Image"]) {
        NSString* thumbnailImageName = [[productMediaArray valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
        cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
        cell.captionLbl.text=[[productImagesArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PlayVideo"];
        cell.captionLbl.text=[[productVideosArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PDF_Thumbnail"];
        cell.captionLbl.text=[[productPdfArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"MedRep_PPT_Thumbnail"];
        cell.captionLbl.text=[[productPresentationsArray objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    cell.productImageView.backgroundColor=[UIColor clearColor];
    return cell;
    }
    else
    {
        return nil;
    }
}


#pragma mark Header Button Actions

-(void)updateExistingPresentation
{
    for (NSInteger i=0; i<visitDetails.fieldMarketingPresentationsArray.count; i++) {
        FieldMarketingPresentation * currentPresentation=[[FieldMarketingPresentation alloc]init];
        currentPresentation=[visitDetails.fieldMarketingPresentationsArray objectAtIndex:i];
        if ([currentPresentation.presentationID isEqualToString:selectedPresentationID]) {
            currentPresentation.presentationID=selectedPresentationID;
            currentPresentation.presentationContentArray=presentationMedia;
            [visitDetails.fieldMarketingPresentationsArray replaceObjectAtIndex:i withObject:currentPresentation];
            
            
            UIAlertAction* yesAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            /*
                                            UCSPresentationViewerViewController * viewerVC=[[UCSPresentationViewerViewController alloc]init];
                                            viewerVC.isFromEditor=YES;
                                            viewerVC.MediaFiles=presentationMedia;
                                            [self.navigationController pushViewController:viewerVC animated:YES];*/
                                            
                                            
                                            SalesWorxPresentationHomeViewController * tempVC=[[SalesWorxPresentationHomeViewController alloc]init];
                                            tempVC.mediaArray=presentationMedia;
                                            tempVC.isFromEditor=YES;
                                            [self.navigationController pushViewController:tempVC animated:YES];

                                            
                                        }];
            
            UIAlertAction * noAction=[UIAlertAction actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                if (self.presentationDelegate!=nil) {
                    if ([self.presentationDelegate respondsToSelector:@selector(updatedVisitWithPresentations:)]) {
                        [self.presentationDelegate updatedVisitWithPresentations:visitDetails];
                    }
                }
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction ,nil];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"View Presentation?" andMessage:@"Would you like to view this presentation?" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
        }
    }

   
    
}

- (IBAction)doneButtonTapped:(id)sender {
    
    
    if (presentationViewed) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
    
    presentationMedia=[_destinationViewController fetchUpdatedPresentationData];
    if (visitDetails.fieldMarketingPresentationsArray.count>0) {
        
    }
    else{
        visitDetails.fieldMarketingPresentationsArray=[[NSMutableArray alloc]init];
    }
    
    
    if (presentationMedia.count>0) {
    if (![NSString isEmpty:selectedPresentationID]) {
        
        [self updateExistingPresentation];
        
    }
    else{
        
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Save"
                                                                              message: @"Please enter a name for this presentation"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Name";
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.font=kSWX_FONT_REGULAR(14);
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleNone;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        NSLog(@"%@",namefield.text);
        
  
        if (![NSString isEmpty:namefield.text]) {
        FieldMarketingPresentation * currentPresentation=[[FieldMarketingPresentation alloc]init];
        currentPresentation.isDefaultPresentation=NO;
        currentPresentation.presentationName=[SWDefaults getValidStringValue:namefield.text];
        currentPresentation.presentationID=[NSString createGuid];
        currentPresentation.demoPlanID=self.selectedDemoPlanID;
//        //in case if user saves and views presentation, we are setting selected presentation id as current id, to avoid displaying name alert and saving presentation again
//        selectedPresentationID=currentPresentation.presentationID;
        presentationViewed=YES;
        
        currentPresentation.presentationContentArray=presentationMedia;
        [visitDetails.fieldMarketingPresentationsArray addObject:currentPresentation];

        if (self.presentationDelegate!=nil) {
            if ([self.presentationDelegate respondsToSelector:@selector(updatedVisitWithPresentations:)]) {
                [self.presentationDelegate updatedVisitWithPresentations:visitDetails];
            }
        }

        
        UIAlertAction* yesAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
//                                       UCSPresentationViewerViewController * viewerVC=[[UCSPresentationViewerViewController alloc]init];
//                                       viewerVC.isFromEditor=YES;
//                                       viewerVC.MediaFiles=presentationMedia;
//                                       [self.navigationController pushViewController:viewerVC animated:YES];
                                       
                                       
                                       SalesWorxPresentationHomeViewController * tempVC=[[SalesWorxPresentationHomeViewController alloc]init];
                                       tempVC.isFromEditor=YES;

                                       tempVC.mediaArray=presentationMedia;
                                       [self.navigationController pushViewController:tempVC animated:YES];


                                   }];
        
        UIAlertAction * noAction=[UIAlertAction actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
            [self.navigationController popViewControllerAnimated:YES];
            
        }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction ,nil];
        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"View Presentation?" andMessage:@"Would you like to view this presentation?" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
        }
        else{

        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    }
    
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please drag and drop files from media panel to create presentation" withController:self];
    }
    }
    
//    [self presentViewController:viewerVC animated:YES completion:nil];
     
    
}

- (IBAction)closeButtonTapped:(id)sender {
    
    
    UIAlertAction* yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    if (self.presentationDelegate!=nil) {
                                        if ([self.presentationDelegate respondsToSelector:@selector(updatedVisitWithPresentations:)]) {
                                            [self.presentationDelegate updatedVisitWithPresentations:visitDetails];
                                        }
                                    }
                                    
                                    [self.navigationController popViewControllerAnimated:YES];
                                    
                                }];
    
    UIAlertAction * noAction=[UIAlertAction actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        

        
    }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction ,nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Close Presentation?" andMessage:@"Would you like to close this presentation?" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
    
    
}

#pragma mark Reload Animation

/*     public class func reload(collectionView:UICollectionView,animationDirection:String) {
 collectionView.reloadData()
 collectionView.layoutIfNeeded()
 let cells = collectionView.visibleCells
 var index = 0
 let tableHeight: CGFloat = collectionView.bounds.size.height
 for i in cells {
 let cell: UICollectionViewCell = i as UICollectionViewCell
 switch animationDirection {
 case "up":
 cell.transform = CGAffineTransform(translationX: 0, y: -tableHeight)
 break
 case "down":
 cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
 break
 case "left":
 cell.transform = CGAffineTransform(translationX: tableHeight, y: 0)
 break
 case "right":
 cell.transform = CGAffineTransform(translationX: -tableHeight, y: 0)
 break
 default:
 cell.transform = CGAffineTransform(translationX: tableHeight, y: 0)
 break
 }
 UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
 cell.transform = CGAffineTransform(translationX: 0, y: 0);
 }, completion: nil)
 index += 1
 }
 }*/
-(void)reloadCollectionViewWithAnimation
{
    [productMediaCollectionView reloadData];
    [productMediaCollectionView layoutIfNeeded];
    NSArray* cellsArray= productMediaCollectionView.visibleCells;
    NSInteger index = 0;
    CGFloat collectionViewHeight = productMediaCollectionView.bounds.size.height;
    for (PresentationDestinationCollectionViewCell *currentCell in cellsArray) {
    currentCell.transform = CGAffineTransformMakeTranslation(-collectionViewHeight, 0);
    [UIView animateWithDuration:1.5 delay:0.05 * index usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        currentCell.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:nil];
        index+=1;
    }
}



@end
