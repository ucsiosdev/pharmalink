//
//  SalesWorxPresentationHelpViewController.m
//  MedRep
//
//  Created by Unique Computer Systems on 12/11/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPresentationHelpViewController.h"

@interface SalesWorxPresentationHelpViewController ()

@end

@implementation SalesWorxPresentationHelpViewController
@synthesize imagesScrollView,imagesPageCoontrol;
- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self setupScrollView];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)doneButtonTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) setupScrollView {
    //add the scrollview to the view
    //setup internal views
    for (int i = 0; i < 5; i++) {
        CGFloat xOrigin = i * self.view.bounds.size.width;
        UIImageView *image = [[UIImageView alloc] initWithFrame:
                              CGRectMake(xOrigin, 0,
                                         self.view.bounds.size.width,
                                         self.view.bounds.size.height)];
        image.image = [UIImage imageNamed:[NSString stringWithFormat:@"PresentationIntro%ld",(long)i+1]];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [imagesScrollView addSubview:image];
    }
    //set the scroll view content size
 imagesScrollView.contentSize = CGSizeMake(self.view.bounds.size.width *
                                             5,
                                             self.view.bounds.size.height);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger indexOfPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
    [imagesPageCoontrol setCurrentPage:indexOfPage];
    
}

- (IBAction)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
