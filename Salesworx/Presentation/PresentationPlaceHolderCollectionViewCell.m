//
//  PresentationPlaceHolderCollectionViewCell.m
//  MedRep
//
//  Created by Unique Computer Systems on 10/10/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "PresentationPlaceHolderCollectionViewCell.h"

@implementation PresentationPlaceHolderCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
   
    
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"PresentationPlaceHolderCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
}

@end
