//
//  PresentationDestinationCollectionViewCell.m
//  MedRep
//
//  Created by Unique Computer Systems on 10/9/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "PresentationDestinationCollectionViewCell.h"

@implementation PresentationDestinationCollectionViewCell

@synthesize productImageView,countLbl,containerView;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    CALayer *layer = self.captionBgView.layer;
    layer.shadowOffset = CGSizeMake(0, -1);
    
    layer.shadowColor = [[UIColor blackColor] CGColor];
    
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 0.1f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
    
    
//    productImageView.layer.borderWidth=1.0f;
//    productImageView.layer.masksToBounds=YES;
//    productImageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    productImageView.layer.cornerRadius = 10.0f;
    
    NSLog(@"label frame in awake from nib %@", NSStringFromCGRect(countLbl.frame));
    
    
    
    countLbl.layer.cornerRadius = CGRectGetWidth(countLbl.frame)/2;
    countLbl.layer.masksToBounds = YES;
    

    
    UIBezierPath *maskPathImageView = [UIBezierPath bezierPathWithRoundedRect:productImageView.bounds byRoundingCorners:( UIRectCornerTopLeft|UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayerImageView = [[CAShapeLayer alloc] init];
    maskLayerImageView.frame = self.bounds;
    maskLayerImageView.path  = maskPathImageView.CGPath;
    productImageView.layer.mask = maskLayerImageView;
    
    
    
    containerView.layer.borderWidth=1.0f;
    containerView.layer.masksToBounds=YES;
    containerView.layer.borderColor=[UIColor colorWithRed:(227.0/255.0) green:(231.0/255.0) blue:(234.0/255.0) alpha:1].CGColor;
    containerView.layer.cornerRadius = 10.0f;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"PresentationDestinationCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
}


@end
