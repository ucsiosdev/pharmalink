//
//  DestinationViewController.h
//  DragAndDropDemo
//
//  Created by Son Ngo on 2/13/14.
//  Copyright (c) 2014 Son Ngo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyModel.h"
#import "SWDefaults.h"
#import "MedRepProductsCollectionViewCell.h"
#import "UCSImageDisplayViewController.h"
#import "UCSMoviePlayerViewController.h"
#import "UCSPdfViewController.h"
#import "UCSWebViewController.h"
#import "PresentationDestinationCollectionViewCell.h"
#import "PresentationPlaceHolderCollectionViewCell.h"
#import "NSString+Additions.h"

@interface DestinationViewController : NSObject
{
    BOOL destinationGesture;
    NSInteger totalRowCount;
    UITapGestureRecognizer *doubleTapGesture;
    
    CGSize  cellSize;
    BOOL showDeleteButton;
    
    NSIndexPath* selectedCellIndexPath;
}
//- (instancetype)initWithCollectionView:(UICollectionViewController *)collectionVC ;
- (void)addModel:(MyModel *)model;
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView ;
-(void)addModelWithTouchPoint:(ProductMediaFile*)model withTouchPoint:(CGPoint)touchPoint;
-(void)activateDestinationGesture:(BOOL)status;
-(BOOL)fetchDestinationGestureStatus;
-(NSMutableArray*)fetchModels;
-(NSMutableArray*)fetchUpdatedPresentationData;
-(void)displayExistingModels:(NSMutableArray*)contentArray;
-(void)deleteIconTapped;

@end
