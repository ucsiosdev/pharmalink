//
//  SourceViewController.m
//  DragAndDropDemo
//
//  Created by Son Ngo on 2/10/14.
//  Copyright (c) 2014 Son Ngo. All rights reserved.
//

#import "SourceViewController.h"
#import "CardCell.h"

@interface SourceViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
  UICollectionView *_collectionView;
  SalesWorxPresentationViewController *_parentController;
  NSMutableArray *_models;
 // MyModel *_selectedModel;

 ProductMediaFile *_selectedModel;

   
}
@end

#pragma mark -
@implementation SourceViewController

- (instancetype)initWithCollectionView:(UICollectionView *)view andParentViewController:(SalesWorxPresentationViewController *)parent withMediaArray:(NSMutableArray*)contentArray {
  if (self = [super init]) {
     
      _models=[[NSMutableArray alloc]init];
      _models=contentArray;
      
    //[self setUpModels];
    [self initCollectionView:view];
    [self setUpGestures];
    
    _parentController = parent;
  }
  return self;
}

- (void)cellDragCompleteWithModel:(MyModel *)model withValidDropPoint:(BOOL)validDropPoint {
  if (model != nil) {
    // get indexPath for the model
    NSUInteger index = [_models indexOfObject:model];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    if (validDropPoint && indexPath != nil) {
     // [_models removeObjectAtIndex:index];
      //[_collectionView deleteItemsAtIndexPaths:@[indexPath]];
      
      [_collectionView reloadData];
     //   [self reloadCollectionViewWithAnimation];
    } else {
      
      UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath:indexPath];
      cell.alpha = 1.0f;
    }
  }
}

#pragma mark - Setup methods
- (void)setUpModels {
  _models = [NSMutableArray array];
  
  for (int i=0; i<10; i++) {
    [_models addObject:[[MyModel alloc] initWithValue:i]];
      
  }
}

-(void)updateSourceMediaContent:(NSMutableArray*)sourceArray
{
    _models=[[NSMutableArray alloc]init];
    _models=sourceArray;
    _collectionView.backgroundColor=[UIColor whiteColor];
   // [_collectionView reloadData];
   // _collectionView.backgroundColor= [UIColor lightGrayColor];
    [self reloadCollectionViewWithAnimation];
}

-(void)reloadCollectionViewWithAnimation
{
    [_collectionView reloadData];
    [_collectionView layoutIfNeeded];
    NSArray* cellsArray= _collectionView.visibleCells;
    NSInteger index = 0;
    CGFloat collectionViewHeight = _collectionView.bounds.size.height;
    for (PresentationDestinationCollectionViewCell *currentCell in cellsArray) {
        CGAffineTransform pushTransformAffine = CGAffineTransformMakeTranslation(collectionViewHeight, 0);
        CATransform3D pushTransform = CATransform3DMakeAffineTransform(pushTransformAffine);

        CATransform3D t = CATransform3DIdentity;
        //Add the perspective!!!
        t.m34 = 1.0/ -500;
        t = CATransform3DRotate(t, 45.0f * M_PI / 180.0f, 0, 1, 0);
        currentCell.layer.transform = CATransform3DConcat(pushTransform, t);
        
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            currentCell.transform = CGAffineTransformMakeTranslation(0, 0);
            
        } completion:nil];
        index+=1;
    }
}
- (void)initCollectionView:(UICollectionView *)view {
  _collectionView            = view;
  _collectionView.delegate   = self;
  _collectionView.dataSource = self;
  
  //[_collectionView registerClass:[CardCell class] forCellWithReuseIdentifier:CELL_REUSE_ID];
    
    [_collectionView registerClass:[PresentationDestinationCollectionViewCell class] forCellWithReuseIdentifier:@"destinationCell"];

}

- (void)setUpGestures {
  
  UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                 action:@selector(handlePress:)];
  longPressGesture.numberOfTouchesRequired = 1;
  [_collectionView addGestureRecognizer:longPressGesture];
}

#pragma mark - Gesture Recognizer
- (void)handlePress:(UILongPressGestureRecognizer *)gesture {
  CGPoint point = [gesture locationInView:_collectionView];
 
  if (gesture.state == UIGestureRecognizerStateBegan) {
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:point];
    if (indexPath != nil) {
      _selectedModel = [_models objectAtIndex:indexPath.item];
      
      // calculate point in parent view
      point = [gesture locationInView:_parentController.view];
      
      [_parentController setSelectedModel:_selectedModel atPoint:point];
      
      // hide the cell
      //[_collectionView cellForItemAtIndexPath:indexPath].alpha = 0.0f;
    }
  }
  else if (gesture.state==UIGestureRecognizerStateEnded)
  {
      NSLog(@"gesture state has ended in source view");
      [_parentController hideCardView];
  }
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return _models.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
 
    /*static NSString *cellIdentifier = @"productCell";
    NSString* mediaType = [[_models valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    MedRepProductsCollectionViewCell *cell = (MedRepProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.productImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.selectedImgView.hidden=YES;*/
    
    static NSString *cellIdentifier = @"destinationCell";
    PresentationDestinationCollectionViewCell *cell = (PresentationDestinationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
   // cell.contentView.backgroundColor=[UIColor redColor];
    // cell.userInteractionEnabled=YES;
    cell.deleteButton.hidden=YES;
    NSString* mediaType = [[_models valueForKey:@"Media_Type"] objectAtIndex:indexPath.row ];
    cell.countLbl.hidden=YES;
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    if ([mediaType isEqualToString:@"Image"]) {
        NSString* thumbnailImageName = [[_models valueForKey:@"File_Name"] objectAtIndex:indexPath.row];
        cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
        cell.captionLbl.text=[[_models objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
        cell.productImgViewTopConstraint.constant=2.0;
        cell.productimageViewLeadingConstraint.constant=2.0;
        cell.productImageViewTrailingConstraint.constant=2.0;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"PresentationVideoPlaceHolder"];
        cell.captionLbl.text=[[_models objectAtIndex:indexPath.row]valueForKey:@"Caption"];
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"PresentationPdfPlaceHolder"];
        cell.captionLbl.text=[[_models objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImgViewTopConstraint.constant=0.0;
        cell.productimageViewLeadingConstraint.constant=0.0;
        cell.productImageViewTrailingConstraint.constant=0.0;
        cell.productImageView.image=[UIImage imageNamed:@"PresentationPptPlaceHolder"];
        cell.captionLbl.text=[[_models objectAtIndex:indexPath.row]valueForKey:@"Caption"];
    }
    cell.productImageView.backgroundColor=[UIColor clearColor];
    return cell;


/*
 CardCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:CELL_REUSE_ID forIndexPath:indexPath];
  MyModel *model = [_models objectAtIndex:indexPath.item];
  [cell setModel:model];
  
  return cell;*/
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
 // return CGSizeMake(162, 160);
    
    CGSize tempSize = CGSizeMake(collectionView.frame.size.width/6, collectionView.frame.size.width/6);
   // NSLog(@"source view size is %@", NSStringFromCGSize(tempSize));
    
    
    return CGSizeMake(collectionView.frame.size.width/6, collectionView.frame.size.width/6);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 16, 0, 8); // top, left, bottom, right
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductMediaFile* currentMediaFile=[_models objectAtIndex:indexPath.row];
    NSString* resourceName=currentMediaFile.File_Name;
    if ([currentMediaFile.Media_Type isEqualToString:@"Image"]) {
        UCSImageDisplayViewController *imageViewController = [[UCSImageDisplayViewController alloc]initWithNibName:@"UCSImageDisplayViewController" bundle:[NSBundle mainBundle]];
        imageViewController.currentMediaFile=currentMediaFile;
        imageViewController.showStatusBar=YES;
        [[SWDefaults currentTopViewController] presentViewController:imageViewController animated:YES completion:nil];
    }
    else if ([currentMediaFile.Media_Type isEqualToString:@"Video"])
    {
        UCSMoviePlayerViewController *videoViewController = [[UCSMoviePlayerViewController alloc]initWithNibName:@"UCSMoviePlayerViewController" bundle:[NSBundle mainBundle]];
        videoViewController.currentMediaFile=currentMediaFile;
        videoViewController.showStatusBar=YES;
        [[SWDefaults currentTopViewController] presentViewController:videoViewController animated:YES completion:nil];
    }
    else if ([currentMediaFile.Media_Type isEqualToString:@"Brochure"])
    {
        UCSPdfViewController *pdfViewController = [[UCSPdfViewController alloc]initWithNibName:@"UCSPdfViewController" bundle:[NSBundle mainBundle]];
        pdfViewController.defaultView=YES;
        pdfViewController.currentMediaFile=currentMediaFile;
        pdfViewController.showStatusBar=YES;
        
        [[SWDefaults currentTopViewController] presentViewController:pdfViewController animated:YES completion:nil];
    }
    else //if ([currentMediaFile.Media_Type isEqualToString:@"Powerpoint"])
    {
        UCSWebViewController *webVC = [[UCSWebViewController alloc]initWithNibName:@"UCSWebViewController" bundle:[NSBundle mainBundle]];
        webVC.requestURLString=resourceName;
        webVC.currentMediaFile=currentMediaFile;
        webVC.showStatusBar=YES;
        [[SWDefaults currentTopViewController] presentViewController:webVC animated:YES completion:nil];
    }
}

@end
