//
//  SourceViewController.h
//  DragAndDropDemo
//
//  Created by Son Ngo on 2/10/14.
//  Copyright (c) 2014 Son Ngo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SalesWorxPresentationViewController.h"
#import "MyModel.h"
#import "PresentationDestinationCollectionViewCell.h"

@interface SourceViewController : NSObject

- (instancetype)initWithCollectionView:(UICollectionView *)view andParentViewController:(SalesWorxPresentationViewController *)parent withMediaArray:(NSMutableArray*)contentArray;

- (void)cellDragCompleteWithModel:(MyModel *)model
               withValidDropPoint:(BOOL)validDropPoint;
-(void)updateSourceMediaContent:(NSMutableArray*)sourceArray;


@end
