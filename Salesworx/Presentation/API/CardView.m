//
//  CardView.m
//  DragAndDropDemo
//
//  Created by Son Ngo on 2/9/14.
//  Copyright (c) 2014 Son Ngo. All rights reserved.
//

#import "CardView.h"
#import "SWDefaults.h"
#import "MedRepElementDescriptionLabel.h"
#pragma mark -
@implementation CardView
@synthesize placeHolderImageView;
- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
      
    
      
      
   
      
    self.label               = [[MedRepElementDescriptionLabel alloc] init];
    self.label.font          = kSWX_FONT_SEMI_BOLD(14);
      self.label.frame=CGRectMake(4, 120, 152, 38);
      
    [self addSubview:self.label];
      
      NSLog(@"label frame is %@", NSStringFromCGRect(self.label.frame));

      
      
      
     placeHolderImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 160, 120)];

      
      UIBezierPath *maskPathImageView = [UIBezierPath bezierPathWithRoundedRect:placeHolderImageView.bounds byRoundingCorners:( UIRectCornerTopLeft|UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
      
      CAShapeLayer *maskLayerImageView = [[CAShapeLayer alloc] init];
      maskLayerImageView.frame = self.bounds;
      maskLayerImageView.path  = maskPathImageView.CGPath;
      placeHolderImageView.layer.mask = maskLayerImageView;
      

      
      [self addSubview:placeHolderImageView];
    
      NSLog(@"place holder view frame is %@", NSStringFromCGRect(placeHolderImageView.frame));
      
      
      NSLog(@"view frame is %@", NSStringFromCGRect(self.label.frame));

    self.backgroundColor    = [UIColor colorWithRed:(227.0/255.0) green:(231.0/255.0) blue:(234.0/255.0) alpha:1];
    self.layer.cornerRadius = 10.0f;
      self.layer.borderColor=[UIColor colorWithRed:(227.0/255.0) green:(231.0/255.0) blue:(234.0/255.0) alpha:1].CGColor;
      
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  
  //self.label.frame = self.bounds;
    placeHolderImageView.frame=CGRectMake(0, 0, 160, 120);

    self.label.frame=CGRectMake(4, 120, 152, 38);
}

#pragma mark - Public methods
- (void)setHighlightSelection:(BOOL)highlight {
  if (highlight) {
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 1.0f;
  } else {
    self.layer.borderWidth = 0.0f;
  }
}

@end
