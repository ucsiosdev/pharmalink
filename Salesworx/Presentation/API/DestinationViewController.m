//
//  DestinationViewController.m
//  DragAndDropDemo
//
//  Created by Son Ngo on 2/13/14.
//  Copyright (c) 2014 Son Ngo. All rights reserved.
//
/*E3E7EA  previous HEXX*/

#import "DestinationViewController.h"
#import "CardCell.h"

@interface DestinationViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
  NSMutableArray *_models;
  UICollectionView *_collectionView;
}

@end

#pragma mark -
@implementation DestinationViewController


-(instancetype)initWithCollectionViewController:(UICollectionViewController *)collectionVC
{
    if (self = [super init]) {
        
    }
    return self;
    

}

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView {
  if (self = [super init]) {
    _models = [NSMutableArray array];
      
      
    //add dummy content
      
//      for (NSInteger i=0; i<32; i++) {
//          ProductMediaFile * dummyMediaforContentSize=[[ProductMediaFile alloc]init];
//          [_models addObject:dummyMediaforContentSize];
//      }
    
    _collectionView = collectionView;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
      
      _collectionView.pagingEnabled=NO;
      
      //_collectionView.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"MedRep_PDF_Thumbnail"]];
      
      
      
      UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
      flow.itemSize = CGSizeMake(160,162);
      flow.scrollDirection = UICollectionViewScrollDirectionVertical;
      //flow.minimumInteritemSpacing = 30;
      //flow.minimumLineSpacing = 40;
      

      
      
      //[_collectionView reloadData];
      _collectionView.collectionViewLayout = flow;
      
    [_collectionView registerClass:[PresentationDestinationCollectionViewCell class] forCellWithReuseIdentifier:@"destinationCell"];

      [_collectionView registerClass:[PresentationPlaceHolderCollectionViewCell class] forCellWithReuseIdentifier:@"placeHolderCell"];

      
      
    //[_collectionView registerClass:[CardCell class] forCellWithReuseIdentifier:CELL_REUSE_ID];
      
      UILongPressGestureRecognizer * longPressGesture=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
      [_collectionView addGestureRecognizer:longPressGesture];
      
      
      
      doubleTapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleDoubleTap:)];
      doubleTapGesture.numberOfTouchesRequired=1;
      doubleTapGesture.numberOfTapsRequired=2;
      
      showDeleteButton=NO;
      
      //[_collectionView addGestureRecognizer:doubleTapGesture];
      

      //[self activateDestinationGesture:NO];

//      // If you are using Storyboards/Nibs, make sure you "registerNib:" instead.
//      [_collectionView registerClass:[HTKSampleCollectionViewCell class] forCellWithReuseIdentifier:HTKDraggableCollectionViewCellIdentifier];
//      
//      // Setup item size
//      HTKDragAndDropCollectionViewLayout *flowLayout = (HTKDragAndDropCollectionViewLayout *)_collectionView.collectionViewLayout;
//      CGFloat itemWidth = CGRectGetWidth(_collectionView.bounds) / 2 - 40;
//      flowLayout.itemSize = CGSizeMake(itemWidth, itemWidth);
//      flowLayout.minimumInteritemSpacing = 20;
//      //flowLayout.lineSpacing = 20;
//      flowLayout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20);
  }
  return self;
}
- (void)handleDoubleTap:(UITapGestureRecognizer *)tapGesture{
    
    // to avoid empty space taps in collectionview we are comparing selectedCellIndexPath and selectedIndexPath
    __block NSIndexPath *selectedIndexPath = [_collectionView indexPathForItemAtPoint:[tapGesture locationInView:_collectionView]];

    if (tapGesture.state == UIGestureRecognizerStateEnded && selectedCellIndexPath.row==selectedIndexPath.row)
    {
        
        NSLog(@"touched point is %@",NSStringFromCGPoint([tapGesture locationInView:_collectionView]));
        
        
        
        
        /*
        if (selectedIndexPath.row==0 || selectedIndexPath.row==totalRowCount-1) {
            //these are start and end nodes
            
        }
        else if (selectedIndexPath)
        {*/
        
            NSLog(@"media file was double tapped");
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [_models removeObjectAtIndex:selectedIndexPath.row];
                                           [_collectionView reloadData];
                                           selectedIndexPath=nil;
                                           
                                           if (_models.count==0) {
                                               
                                               [[NSNotificationCenter defaultCenter]postNotificationName:@"ShowDragAndDropPlaceHolder" object:nil];
                                               
                                               
                                           }
                                           
                                       }];
            
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Remove File?" andMessage:@"Would you like to remove this file from presentation?" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
        }
        else
        {
            
        }
    //}
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gr{
    
   // NSLog(@"destination gesture before flag %hhd", destinationGesture);
    //[self activateDestinationGesture:YES];
   // NSLog(@"destination gesture after flag %hhd", destinationGesture);
    NSIndexPath *selectedIndexPath = [_collectionView indexPathForItemAtPoint:[gr locationInView:_collectionView]];
    
    
        /*
        if (selectedIndexPath.row==totalRowCount-1) {
            NSLog(@"returning from loop");
            return;
        } else {
            
        }
        NSLog(@"gesture state is %ld", (long)gr.state);
         */
        
    switch(gr.state){
        
        case UIGestureRecognizerStateBegan:
        {
            if(selectedIndexPath == nil)
                break;
            [_collectionView beginInteractiveMovementForItemAtIndexPath:selectedIndexPath];
            //[self activateDestinationGesture:YES];
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            [_collectionView updateInteractiveMovementTargetPosition:[gr locationInView:gr.view]];
            break;
        }
        case UIGestureRecognizerStateEnded:
        {

            [_collectionView endInteractiveMovement];
            break;
        }
        default:
        {
            [_collectionView cancelInteractiveMovement];
            break;
        }
    }
    
    
}
-(void)activateDestinationGesture:(BOOL)status
{
    destinationGesture=status;
}

/*
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductMediaFile* currentMediaFile=[_models objectAtIndex:indexPath.row];
    NSString* resourceName=currentMediaFile.File_Name;
    if ([currentMediaFile.Media_Type isEqualToString:@"Image"]) {
        UCSImageDisplayViewController *imageViewController = [[UCSImageDisplayViewController alloc]initWithNibName:@"UCSImageDisplayViewController" bundle:[NSBundle mainBundle]];
        imageViewController.currentMediaFile=currentMediaFile;
        imageViewController.showStatusBar=YES;
        [[SWDefaults currentTopViewController] presentViewController:imageViewController animated:YES completion:nil];
    }
    else if ([currentMediaFile.Media_Type isEqualToString:@"Video"])
    {
        UCSMoviePlayerViewController *videoViewController = [[UCSMoviePlayerViewController alloc]initWithNibName:@"UCSMoviePlayerViewController" bundle:[NSBundle mainBundle]];
        videoViewController.currentMediaFile=currentMediaFile;
        videoViewController.showStatusBar=YES;
        [[SWDefaults currentTopViewController] presentViewController:videoViewController animated:YES completion:nil];
    }
    else if ([currentMediaFile.Media_Type isEqualToString:@"Brochure"])
    {
        UCSPdfViewController *pdfViewController = [[UCSPdfViewController alloc]initWithNibName:@"UCSPdfViewController" bundle:[NSBundle mainBundle]];
        
        pdfViewController.currentMediaFile=currentMediaFile;
        pdfViewController.showStatusBar=YES;

        [[SWDefaults currentTopViewController] presentViewController:pdfViewController animated:YES completion:nil];
    }
    else //if ([currentMediaFile.Media_Type isEqualToString:@"Powerpoint"])
    {
        UCSWebViewController *webVC = [[UCSWebViewController alloc]initWithNibName:@"UCSWebViewController" bundle:[NSBundle mainBundle]];
        webVC.requestURLString=resourceName;
        webVC.currentMediaFile=currentMediaFile;
        webVC.showStatusBar=YES;
        [[SWDefaults currentTopViewController] presentViewController:webVC animated:YES completion:nil];
    }
}*/


-(NSMutableArray*)fetchUpdatedPresentationData
{
    return _models;
}




- (void)addModel:(MyModel *)model {
  [_models addObject:model];
  
  [_collectionView reloadData];
}

-(BOOL)fetchDestinationGestureStatus
{
    return destinationGesture;
}
-(NSMutableArray*)fetchModels
{
    return _models;
}

-(BOOL)isModelExisting:(ProductMediaFile*)currentMedia
{
    BOOL existingMedia=NO;
    for (ProductMediaFile *mediaFile in _models) {
        if ([[SWDefaults getValidStringValue:mediaFile.Media_File_ID] isEqualToString:[SWDefaults getValidStringValue:currentMedia.Media_File_ID]]) {
            existingMedia=YES;
        }
    }
    return existingMedia;
}

-(void)displayExistingModels:(NSMutableArray*)contentArray
{
    [_models addObjectsFromArray:contentArray];
   // [_collectionView reloadData];
    [self reloadCollectionViewWithAnimation];

}

-(void)addModelWithTouchPoint:(ProductMediaFile*)model withTouchPoint:(CGPoint)touchPoint
{
    NSLog(@"destination gesture in method %hhd /n %@", destinationGesture,model);
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ClearPresentationPlaceHolder" object:nil];
    
    
    NSIndexPath *indexPathToReplace = [_collectionView indexPathForItemAtPoint:[[_collectionView superview] convertPoint:touchPoint toView:_collectionView]];

    
//    NSIndexPath* indexPathToReplace= [_collectionView indexPathForItemAtPoint:touchPoint];
//    
//    NSLog(@"indexpath to replace is %ld",(long)indexPathToReplace.row);
//    
//    NSLog(@"current indexpath is %ld \n new index is %ld\n",(long)indexPathToReplace.row,(long)path.row);
    

    if (_models.count>0 && indexPathToReplace!=nil && model!=nil) {
     
        if (![self isModelExisting:model]) {
            NSInteger updatedIndex=indexPathToReplace.row;
          
            /*NSInteger updatedIndex=indexPathToReplace.row-1;

            if (updatedIndex<=0) {
                //to avoid start node
                updatedIndex=0;
            } else {
                
            }*/
            
            [_models insertObject:model atIndex:updatedIndex];
            [_collectionView reloadData];
        }
        else{
            [SWDefaults showAlertAfterHidingKeyBoard:@"File Exists" andMessage:@"This file has already been added to presentation" withController:[SWDefaults currentTopViewController]];
        }
    }
    else if (model!=nil)
    {
    if (![self isModelExisting:model]) {

    if (_models.count!=0) {
       
         [_models insertObject:model atIndex:_models.count];
    }
    else
    {
        [_models addObject:model];

        /*
        ProductMediaFile * startNode=[[ProductMediaFile alloc]init];
        startNode.isStartNode=YES;
        [_models addObject:startNode];

        [_models addObject:model];

        ProductMediaFile * endNode=[[ProductMediaFile alloc]init];
        endNode.isEndNode=YES;
        [_models addObject:endNode];*/
        
    }

    [_collectionView reloadData];
    }
    else{
    [SWDefaults showAlertAfterHidingKeyBoard:@"File Exists" andMessage:@"This file has already been added to presentation" withController:[SWDefaults currentTopViewController]];
    }
    }
    else
    {
        NSLog(@"animation stuck");
        [_collectionView endInteractiveMovement];
        [_collectionView reloadData];

    }
    
    
    
    if (_models.count>0 && model!=nil) {
        
    NSInteger section = 0;
    NSInteger item = [self collectionView:_collectionView numberOfItemsInSection:section] - 1;
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:item inSection:section];
    [_collectionView scrollToItemAtIndexPath:lastIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_models.count>0) {
        totalRowCount=_models.count;
        return _models.count;
    }
    else
    {
        return 0;
    }
}

/*
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 150;
}*/

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCellIndexPath=indexPath;
    NSLog(@"selected cell indexpath is %@",selectedCellIndexPath);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  

    /*
    if (indexPath.row==0 || indexPath.row==totalRowCount-1) {
        static NSString *cellIdentifier = @"placeHolderCell";
        PresentationPlaceHolderCollectionViewCell *cell = (PresentationPlaceHolderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        //cell.userInteractionEnabled=NO;
        cell.placeHolderView.layer.borderWidth=1.0f;
        cell.placeHolderView.layer.borderColor=[UIColor blueColor].CGColor;
        if (indexPath.row==0) {
            cell.titleLbl.text=@"Start";
            cell.saperatorLbl.hidden=NO;
        } else {
            cell.titleLbl.text=@"End";
            cell.saperatorLbl.hidden=YES;
        }
        return cell;
    }
    else{*/
        
    NSInteger currentIndex=indexPath.row;
    static NSString *cellIdentifier = @"destinationCell";
    NSString* mediaType = [SWDefaults getValidStringValue:[[_models valueForKey:@"Media_Type"] objectAtIndex:currentIndex ]];
    PresentationDestinationCollectionViewCell *cell = (PresentationDestinationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.countLbl.text=[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1]];
    
    cell.deleteButton.tag=indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    if (!showDeleteButton) {
//        cell.deleteButton.hidden=YES;
//    }
//    else{
//        cell.deleteButton.hidden=NO;
//    }
    cell.deleteButton.hidden=NO;
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    if ([mediaType isEqualToString:@"Image"]) {
        NSString* thumbnailImageName = [[_models valueForKey:@"File_Name"] objectAtIndex:currentIndex];
        cell.productImageView.image=[UIImage imageWithContentsOfFile:[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName] stringByAppendingPathComponent:thumbnailImageName]];
        cell.captionLbl.text=[[_models objectAtIndex:currentIndex]valueForKey:@"Caption"];
        cell.placeHolderImageView.hidden=NO;
    }
    else if ([mediaType isEqualToString:@"Video"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"PresentationVideoPlaceHolder"];
        cell.captionLbl.text=[[_models objectAtIndex:currentIndex]valueForKey:@"Caption"];
    }
    else if ([mediaType isEqualToString:@"Brochure"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"PresentationPdfPlaceHolder"];
        cell.captionLbl.text=[[_models objectAtIndex:currentIndex]valueForKey:@"Caption"];
    }
    else if ([mediaType isEqualToString:@"Powerpoint"])
    {
        cell.productImageView.image=[UIImage imageNamed:@"PresentationPptPlaceHolder"];
        cell.captionLbl.text=[[_models objectAtIndex:currentIndex]valueForKey:@"Caption"];
    }
    cell.productImageView.backgroundColor=[UIColor clearColor];
    
    [cell updateConstraints];
    return cell;
    /*
    CardCell *cell = (CardCell *)[_collectionView dequeueReusableCellWithReuseIdentifier:CELL_REUSE_ID forIndexPath:indexPath];
  
  MyModel *model = [_models objectAtIndex:indexPath.item];
  cell.model = model;
  
  return cell;*/

   // }
}
-(void)deleteButtonTapped:(id)Sender
{
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   UIButton* selectedButton = Sender;
                                   [_models removeObjectAtIndex:selectedButton.tag];
                                   [_collectionView reloadData];
                                   
                                   if (_models.count==0) {
                                       
                                       [[NSNotificationCenter defaultCenter]postNotificationName:@"ShowDragAndDropPlaceHolder" object:nil];
                                       
                                   }
                                   
                               }];
    
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,[SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)] ,nil];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Remove File?" andMessage:@"Would you like to remove this file from presentation?" andActions:actionsArray withController:[SWDefaults currentTopViewController]];
    


}

#pragma mark - UICollectionViewDelegateFlowLayout

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    NSLog(@"collection view move item called");
    
    NSInteger sourceIndex= sourceIndexPath.row;
    NSInteger destinationIndex = destinationIndexPath.row;

    MyModel * sourceObject=[_models objectAtIndex:sourceIndex];

    [_models removeObjectAtIndex:sourceIndex];
    
    [_models insertObject:sourceObject atIndex:destinationIndex];
    
    
    
  //  [_models exchangeObjectAtIndex:sourceIndex withObjectAtIndex:destinationIndex];
    
    
    [self performSelector:@selector(reloadCollectionView) withObject:nil afterDelay:1.0];
    
    
    //add your data source manipulation logic here
    //specifically, change the order of entries in the data source to match the new visual order of the cells.
    //even without anything inside this function, the cells will move visually if you build and run
    
}

-(void)reloadCollectionView
{
    [_collectionView reloadData];
 
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    cellSize = CGSizeMake(collectionView.frame.size.width/6, collectionView.frame.size.width/6);
    return cellSize;
    
    //return CGSizeMake(135, 142);
}


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(8, 8, 8, 8); // top, left, bottom, right
}

-(void)deleteIconTapped
{
    if (_models.count>0) {
        showDeleteButton=YES;
        [_collectionView reloadData];
    }
}

-(void)reloadCollectionViewWithAnimation
{
    [_collectionView reloadData];
    [_collectionView layoutIfNeeded];
    NSArray* cellsArray= _collectionView.visibleCells;
    NSInteger index = 0;
    CGFloat collectionViewHeight = _collectionView.bounds.size.height;
    for (PresentationDestinationCollectionViewCell *currentCell in cellsArray) {
       // currentCell.transform = CGAffineTransformMakeTranslation(collectionViewHeight, 0);
        
        CGAffineTransform pushTransformAffine = CGAffineTransformMakeTranslation(collectionViewHeight, 0);
        CATransform3D pushTransform = CATransform3DMakeAffineTransform(pushTransformAffine);
        
        CATransform3D t = CATransform3DIdentity;
        //Add the perspective!!!
        t.m34 = 1.0/ -500;
        t = CATransform3DRotate(t, 30.0f * M_PI / 180.0f, 0, 1, 0);
        currentCell.layer.transform = CATransform3DConcat(pushTransform, t);
        
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            currentCell.transform = CGAffineTransformMakeTranslation(0, 0);
        } completion:nil];
        index+=1;
    }
}
/*

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 8.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 180.0;
}*/


@end
