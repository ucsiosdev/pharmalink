//
//  SalesWorxPresentationHelpViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 12/11/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EAIntroView/EAIntroView.h>
#import "SWAppDelegate.h"

@interface SalesWorxPresentationHelpViewController : UIViewController<EAIntroDelegate>
@property (strong, nonatomic) IBOutlet UIPageControl *imagesPageCoontrol;
@property (strong, nonatomic) IBOutlet UIScrollView *imagesScrollView;
- (IBAction)doneButtonTapped:(id)sender;

@end
