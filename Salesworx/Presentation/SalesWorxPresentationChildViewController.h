//
//  SalesWorxPresentationChildViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/22/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
#import "SWDefaults.h"
#import "UCSImageDisplayViewController.h"
#import "UCSMoviePlayerViewController.h"
#import "UCSPdfViewController.h"
#import "UCSWebViewController.h"

@protocol SalesWorxChildPresentationDelegate <NSObject>

-(void)demoedMedia:(NSMutableArray*)demoedMedia;


@end

@interface SalesWorxPresentationChildViewController : UIViewController<ReaderViewControllerDelegate>
{
    ReaderViewController *readerViewController;
    
    NSTimer *demoTimer;
    
    ProductMediaFile * selectedMediaFile;
    NSMutableArray* demoedMediaArray;
    
    id presentationDelegate;
}
@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UILabel *screenNumber;
-(void)createrReaderVC:(ProductMediaFile*)matchedFile;
-(void)updateMediaViewControllerwithFileType:(ProductMediaFile*)mediaFile;
@property(nonatomic) BOOL isFromEditor;


@property(nonatomic) id presentationDelegate;

@end
