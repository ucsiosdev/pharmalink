//
//  SalesWorxPresentationChildViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/22/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxPresentationChildViewController.h"

@interface SalesWorxPresentationChildViewController ()

@end

@implementation SalesWorxPresentationChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.screenNumber.text = [NSString stringWithFormat:@"Screen #%d", self.index];
    demoedMediaArray=[[NSMutableArray alloc]init];
    
    
   

    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)startDemoTimer
//{
//    [demoedMediaArray addObject:selectedMediaFile.Media_File_ID];
//    [demoTimer invalidate];
//    demoTimer=nil;
//    
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"DEMOED_MEDIA_CHILD" object:selectedMediaFile];
//    
//}

-(void)updateMediaViewControllerwithFileType:(ProductMediaFile*)mediaFile
{
    selectedMediaFile=mediaFile;
    if (demoTimer) {
        demoTimer=nil;
    }
    //start the timer to check if images for displayed for more than 5 secs
   // demoTimer=[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(startDemoTimer) userInfo:nil repeats:NO];

    NSString* mediaTypeString=[SWDefaults getValidStringValue:mediaFile.Media_Type];
    NSString* resourceName=mediaFile.File_Name;

    CGRect  viewFrame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    NSLog(@"view frame is %@", NSStringFromCGRect(self.view.bounds));
    
    if ([mediaTypeString isEqualToString:@"Image"]) {
        UCSImageDisplayViewController *imageViewController = [[UCSImageDisplayViewController alloc]initWithNibName:@"UCSImageDisplayViewController" bundle:[NSBundle mainBundle]];
        imageViewController.view.frame = viewFrame;
        imageViewController.currentMediaFile=mediaFile;
        [self addChildViewController:imageViewController];
        [self.view addSubview:imageViewController.view];

    }
    else if ([mediaTypeString isEqualToString:@"Video"])
    {
        UCSMoviePlayerViewController *videoViewController = [[UCSMoviePlayerViewController alloc]initWithNibName:@"UCSMoviePlayerViewController" bundle:[NSBundle mainBundle]];
        videoViewController.view.frame = viewFrame;
        videoViewController.currentMediaFile=mediaFile;
        [self addChildViewController:videoViewController];
        [self.view addSubview:videoViewController.view];

    }
    else if ([mediaTypeString isEqualToString:@"Brochure"])
    {
        UCSPdfViewController *pdfViewController = [[UCSPdfViewController alloc]initWithNibName:@"UCSPdfViewController" bundle:[NSBundle mainBundle]];
        pdfViewController.view.frame = viewFrame;
        pdfViewController.showStatusBar=NO;
        pdfViewController.currentMediaFile=mediaFile;
        [self addChildViewController:pdfViewController];
        [pdfViewController addPdfToTheView];
        [pdfViewController didMoveToParentViewController:self];
        [self.view addSubview:pdfViewController.view];

    }
    else if ([mediaTypeString isEqualToString:@"Powerpoint"])
    {
        UCSWebViewController *webVC = [[UCSWebViewController alloc]initWithNibName:@"UCSWebViewController" bundle:[NSBundle mainBundle]];
        webVC.requestURLString=resourceName;
        webVC.currentMediaFile=mediaFile;
        webVC.view.frame = viewFrame;
        [self addChildViewController:webVC];
        [self.view addSubview:webVC.view];
    }

    
    
    
}

-(void)createrReaderVC:(ProductMediaFile*)matchedFile
{
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    CGRect          viewFrame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    NSString* filePath = [[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:matchedFile.File_Name];
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
    // NSLog(@"bookmark data %@", document.bookmarks);
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.hideCloseButton=YES;
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        readerViewController.view.frame=viewFrame;
        [self addChildViewController:readerViewController];
        [self.view addSubview:readerViewController.view];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
