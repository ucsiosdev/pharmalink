//
//  SalesWorxPresentationHomeViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/22/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxPresentationChildViewController.h"



@interface SalesWorxPresentationHomeViewController : UIViewController<UIPageViewControllerDataSource>
{
    NSMutableArray* demoedMediaArray;
    NSMutableArray* viewControllerIndexArray;
    NSTimer *demoTimer;
    NSInteger selectedIndex;
    NSInteger demoTimeInterval;


}
@property (strong, nonatomic) UIPageViewController *pageController;
@property(strong,nonatomic) NSMutableArray* mediaArray;
@property(nonatomic) BOOL isFromEditor;

@end
