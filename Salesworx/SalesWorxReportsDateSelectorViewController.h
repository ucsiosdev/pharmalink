//
//  SalesWorxReportsDateSelectorViewController.h
//  CustomReportDateSelector
//
//  Created by Pavan Kumar Singamsetti on 11/1/17.
//  Copyright © 2017 Pavan Kumar Singamsetti. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SalesWorxReportsDateSelectorViewControllerDelegate
-(void)DidUserSelectDateRange:(NSDate *)StartDate EndDate:(NSDate *)EndDate AndCustomRangePeriodText:(NSString *)customPeriodText;
@end
@interface SalesWorxReportsDateSelectorViewController : UIViewController{
    NSMutableArray * DatePickerRangesArray;
    IBOutlet UICollectionView * dateRangeCollectionView;
    IBOutlet UIDatePicker *FromDatePicker;
    
    IBOutlet UILabel *FromDateValueLabel;
    IBOutlet UILabel *ToDateValueLabel;
    IBOutlet UIDatePicker *ToDatePicker;
}
@property (nonatomic,strong) id <SalesWorxReportsDateSelectorViewControllerDelegate> delegate;
-(IBAction)DatePickerValueChanged:(UIDatePicker *)sender;
@end
