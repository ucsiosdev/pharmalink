//
//  SalesWorxNotificationManager.m
//  MedRep
//
//  Created by Unique Computer Systems on 9/24/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxNotificationManager.h"
@import UserNotifications;


@implementation SalesWorxNotificationManager

static SalesWorxNotificationManager *retrieveManager = nil;

+ (SalesWorxNotificationManager*) retrieveManager {
    @synchronized(self) {
        if (retrieveManager == nil) {
            retrieveManager = [[SalesWorxNotificationManager alloc] init];
        }
    }
    return retrieveManager;
}
+(void)registerForLocalNotifications
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound;
    [center requestAuthorizationWithOptions:options
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!granted) {
                                  NSLog(@"Something went wrong");
                              }
                              else{
                                  NSLog(@"local notifications access granted");
                              }
                          }];
}
+(void)createNotificationRequest:(NSDictionary*)userInfoDictionary
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.userInfo = userInfoDictionary;
    content.sound = [UNNotificationSound defaultSound];
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:10
                                                                                                    repeats:NO];
    
    NSString *identifier = @"UYLLocalNotification";
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
                                                                          content:content trigger:trigger];
    
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Something went wrong: %@",error);
        }else{
            NSLog(@"triggering notification with request %@",userInfoDictionary);
        }
    }];

}

@end
