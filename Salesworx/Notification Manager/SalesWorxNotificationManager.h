//
//  SalesWorxNotificationManager.h
//  MedRep
//
//  Created by Unique Computer Systems on 9/24/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesWorxNotificationManager : NSObject
+ (SalesWorxNotificationManager*) retrieveManager;
+(void)registerForLocalNotifications;
+(void)createNotificationRequest:(NSDictionary*)userInfoDictionary;
@end
