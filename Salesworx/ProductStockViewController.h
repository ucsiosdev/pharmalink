//
//  ProductStockViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@protocol ProductStockViewControllerDelegate <NSObject>

- (void)didTapSaveButton;

@end

@interface ProductStockViewController : SWViewController < ProductStockViewDelegate > {
    ProductStockView *stockView;
    NSDictionary *product;
    int totalGoods;
    int orderQuantity;
    UILabel *countLabel;
    UILabel *orderedLabel;
    UILabel *allocatedLabel;
//       id target;
    SEL action;
    //
    int orderedQuantity;
    NSString *isLotSelection;
    
    GroupSectionFooterView *footerView;
    UIButton *filterButton;

}

-(void)SaveDataDB;
@property (nonatomic, assign) int orderQuantity;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@property (nonatomic, strong) id<ProductStockViewControllerDelegate> delegate;

@property(strong,nonatomic)NSMutableArray *stockAllocation;
@property(strong,nonatomic)NSMutableDictionary *row;
@property(retain,nonatomic) NSMutableArray * savedArray;
@property(strong,nonatomic)ProductStockViewController * productStockVC;
@property(strong,nonatomic)  NSArray *StockInfoTableArray;

- (id)initWithProduct:(NSDictionary *)product;

@end
