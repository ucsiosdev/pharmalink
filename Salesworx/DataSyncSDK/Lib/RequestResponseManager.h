//
//  DataSyncManager.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppConstantsList.h"



@protocol RequestManagerDelegate
@optional
-(void)requestCompleted:(id)response;


@end

@interface RequestResponseManager : NSObject {
    
    // Objects required for forming request or parsing response
	NSMutableArray *extraInfoObjectArray;
    
	// RequestManagerDelegate objects
	NSMutableArray *delegateArray;
    
    NSString *receivedString;
   
    HTTPRequest requestType;
}
@property(assign) HTTPRequest requestType;
@property(nonatomic,strong) NSString *receivedString;

+(RequestResponseManager*)sharedInstance;
- (void)setRequestPropery:(id<RequestManagerDelegate>)delegate ExtraInfo:(NSObject *)infoObject ;
-(NSMutableArray *)requestForEXEC:(NSString*)receivedstring;
@end
