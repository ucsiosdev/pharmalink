
//
//  DataSyncManager.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//
#import "ParseManager.h"
#import "DataSyncManager.h"
#import "JSON.h"
#import "SBJsonParser.h"
#import "Status.h"

@implementation ParseManager


+(bool)parseActivate:(NSString *)jsonContent {
    
    
    DataSyncManager *appDelegate=[DataSyncManager sharedManager];
    appDelegate.statusDict=[jsonContent JSONValue];
    
    if(appDelegate.statusDict)
    {
        appDelegate.errorCode=jSuccess;
        NSUserDefaults *userDefault =[[NSUserDefaults alloc] init];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:appDelegate.statusDict];
        [userDefault setObject:data forKey:@"statusDictionary"];
        [userDefault synchronize];
    }
    else
    {
        appDelegate.errorCode=jApplicationResponseError;
        appDelegate.errMessage=jsonContent;

    }
    
    return YES;
}

+(bool)parseActivateComplete:(NSString *)jsonContent {
    
    DataSyncManager *appDelegate=[DataSyncManager sharedManager];
    
    appDelegate.statusDict=[jsonContent JSONValue];
     appDelegate.errorCode=jSuccess; 
    
    return YES;
}

+(bool)parseDownload:(NSString *)jsonContent {
    
    DataSyncManager *appDelegate=[DataSyncManager sharedManager];
    if(jsonContent)
    {
        [[DataSyncManager sharedManager] UserAlert:jsonContent];

    }
    else
    {
        appDelegate.errorCode=jSuccess;
    }
    return YES;
}
+(bool)parseUploadFile:(NSString *)jsonContent {
    
    DataSyncManager *appDelegate=[DataSyncManager sharedManager];
    
    if(jsonContent.length!=0)
    {
        [[DataSyncManager sharedManager] UserAlert:jsonContent];
        appDelegate.errorCode=jApplicationResponseError;
        appDelegate.errMessage=jsonContent;
        
    }
    else
    {
        appDelegate.errorCode=jSuccess;
    }
    return YES;
}
+(bool)parseUploadData:(NSString *)jsonContent {
    
    
    DataSyncManager *appDelegate=[DataSyncManager sharedManager];
    
    NSMutableDictionary *jsonResponse=[jsonContent JSONValue];
    if(jsonResponse)
    {
        appDelegate.errorCode=jSuccess;
        appDelegate.statusDict=jsonResponse;
    }
    else
    {
        appDelegate.errorCode=jApplicationResponseError;
        appDelegate.errMessage=jsonContent;

    }
    return YES;
}
+(NSMutableArray *)parseExec:(NSString *)jsonContent{
    //DataSyncManager *appDelegate=[DataSyncManager sharedManager];
    //SBJsonParser *parser = [[SBJsonParser alloc] init];
	//NSError *theError = NULL;
   // NSMutableArray *responseArray=[NSMutableArray arraywi];
	//responseArray= [parser objectWithString:jsonContent error:&theError];
    //appDelegate.execArray=[parser objectWithString:jsonContent error:&theError];
    
    //if([responseArray count]==0)
        //[[DataSyncManager sharedManager] UserAlert:jsonContent];
    
      // return appDelegate.execArray;
    return nil;
}

@end
