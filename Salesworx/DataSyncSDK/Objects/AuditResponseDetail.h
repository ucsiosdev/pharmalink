//
//  AuditResponseDetail.h
//  DataSyncApp
//
//  Created by sapna on 1/22/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuditResponseDetail : NSObject
{
    int _Survey_ID;
    int _Question_ID;
    int _Customer_ID;
    NSString * _Audit_Survey_ID;
    int _SalesRep_ID;
    int _Surveyed_By;
    NSString *_Response;
    NSString *_Emp_Code;
    NSString *_Survey_Timestamp;
   
}
@property(nonatomic,assign)int Survey_ID;
@property(nonatomic,assign)int Question_ID;
@property(nonatomic,assign)int Customer_ID;
@property(nonatomic,copy)NSString *Audit_Survey_ID;
@property(nonatomic,assign)int SalesRep_ID;
@property(nonatomic,assign)int Surveyed_By;
@property(nonatomic,copy)NSString * Response;
@property(nonatomic,copy)NSString * Emp_Code;
@property(nonatomic,copy)NSString * Survey_Timestamp;

-(id)initWithAudit_Survey_ID:(NSString*)Audit_Survey_ID Survey_ID:(int)Survey_ID Question_ID:(int)Question_ID Response:(NSString*)Response Customer_ID:(int)Customer_ID _Surveyed_By:(int)Surveyed_By SalesRep_ID:(int)SalesRep_ID Emp_Code:(NSString*)Emp_Code Survey_Timestamp:(NSString*)Survey_Timestamp;

@end
