//
//  CustomerResponseDetails.m
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "CustomerResponseDetails.h"

@implementation CustomerResponseDetails
@synthesize Survey_ID =_Survey_ID;
@synthesize Question_ID =_Question_ID;
@synthesize Customer_ID =_Customer_ID;
@synthesize Site_Use_ID =_Site_Use_ID;
@synthesize SalesRep_ID =_SalesRep_ID;
@synthesize Response =_Response;
@synthesize Emp_Code =_Emp_Code;
@synthesize Survey_Timestamp =_Survey_Timestamp;
@synthesize Customer_Survey_ID =_Customer_Survey_ID;

-(id)initWithCustomer_Survey_ID:(NSString*)Customer_Survey_ID Survey_ID:(int)Survey_ID Question_ID:(int)Question_ID Response:(NSString*)Response Customer_ID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID SalesRep_ID:(int)SalesRep_ID Emp_Code:(NSString*)Emp_Code Survey_Timestamp:(NSString*)Survey_Timestamp
{
    
    if ((self = [super init])) {
      

        self.Survey_ID=Survey_ID;
        
        self.Customer_ID=Customer_ID;
        self.Site_Use_ID=Site_Use_ID;
        self.SalesRep_ID=SalesRep_ID;
        self.Question_ID=Question_ID;
        self.Response=Response;
        self.Emp_Code=Emp_Code;
        self.Survey_Timestamp=Survey_Timestamp;
        self.Customer_Survey_ID =Customer_Survey_ID;
    }
    return self;

}

@end
