//
//  SurveyResponseDetails.m
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "SurveyResponseDetails.h"

@implementation SurveyResponseDetails
@synthesize  Question_ID =_Question_ID;
@synthesize Response_ID=_Response_ID;
@synthesize Response_Type_ID=_Response_Type_ID;
@synthesize Response_Text=_Response_Text;

-(id)initWithUniqueQuestionId :(int)Question_ID Response_ID:(int)Response_ID Response_Type_ID:(int)Response_Type_ID Response_Text:(NSString*) Response_Text{
    if ((self = [super init])) {
        self.Question_ID=Question_ID;
        self.Response_Text=Response_Text;
        self.Response_ID=Response_ID;
        self.Response_Type_ID=Response_Type_ID;
    }
    return self;

}

@end
