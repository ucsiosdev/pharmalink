//
//  SurveyQuestionDetails.m
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "SurveyQuestionDetails.h"

@implementation SurveyQuestionDetails
@synthesize  Survey_ID =_Survey_ID;
@synthesize Question_ID=_Question_ID;
@synthesize Default_Response_ID=_Default_Response_ID;
@synthesize Question_Text=_Question_Text,Response_Type_ID;




-(id)initWithUniqueQuestionId :(int)Question_ID Survey_ID:(int)Survey_ID Default_Response_ID:(int)Default_Response_ID Question_Text:(NSString*) Question_Text
{
     if ((self = [super init])) {
         self.Question_ID=Question_ID;
         self.Survey_ID=Survey_ID;
         self.Default_Response_ID=Default_Response_ID;
         self.Question_Text=Question_Text;
     }
    return self;
}

@end
