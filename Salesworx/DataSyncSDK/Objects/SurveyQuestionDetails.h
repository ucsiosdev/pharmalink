//
//  SurveyQuestionDetails.h
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SurveyQuestionDetails : NSObject
{
    int  _Question_ID;
    int  _Survey_ID;
    int _Default_Response_ID;
    NSString *_Question_Text;
   
    
}
@property(nonatomic,assign)int Question_ID;
@property(nonatomic,assign)int Survey_ID;
@property(nonatomic,assign)int Default_Response_ID;
@property(nonatomic,copy)NSString * Question_Text;
@property(strong,nonatomic) NSString * Response_Type_ID;

-(id)initWithUniqueQuestionId :(int)Question_ID Survey_ID:(int)Survey_ID Default_Response_ID:(int)Default_Response_ID Question_Text:(NSString*) Question_Text;
@end
