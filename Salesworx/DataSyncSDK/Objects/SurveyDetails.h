//
//  SurveyDetails.h
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SurveyDetails : NSObject
{
    int _Survey_ID;
    int _Customer_ID;
    int _Site_Use_ID;
    int _SalesRep_ID;
    
    NSString *_Survey_Title;
    NSString *_Start_Time;
    NSString *_End_Time;
    NSString *_Survey_Type_Code;
}
@property(nonatomic,assign)int Survey_ID;
@property(nonatomic,assign)int Customer_ID;
@property(nonatomic,assign)int Site_Use_ID;
@property(nonatomic,assign)int SalesRep_ID;
@property(nonatomic,copy)NSString * Survey_Title;
@property(nonatomic,copy)NSString * Start_Time;
@property(nonatomic,copy)NSString * End_Time;
@property(nonatomic,copy)NSString * Survey_Type_Code;

@property(strong,nonatomic) NSString * isSelected;
@property(strong,nonatomic) NSString * isCompleted;


-(id)initWithSurveyId :(int)Survey_ID Customer_ID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID SalesRep_ID:(int)SalesRep_ID Survey_Title:(NSString*)Survey_Title Start_Time:(NSString*)Start_Time End_Time:(NSString*)End_Time Survey_Type_Code:(NSString*)Survey_Type_Code;
@end
