//
//  Status.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/4/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Status : NSObject
{
    NSString *Status1;
    NSString *ProcessResponse;
    NSString *CurrentTimestamp;
    NSString *SyncReferenceNo;
    NSString *SyncStatus;
    NSString *BytesReceived;
    NSString *BytesSent;
    NSString *CurrentProgress;
  
}
@property(nonatomic,strong)NSString *Status1;
@property(nonatomic,strong)NSString *ProcessResponse;
@property(nonatomic,strong)NSString *CurrentTimestamp;
@property(nonatomic,strong)NSString *SyncReferenceNo;
@property(nonatomic,strong)NSString *SyncStatus;
@property(nonatomic,strong)NSString *BytesReceived;
@property(nonatomic,strong)NSString *BytesSent;
@property(nonatomic,strong)NSString *CurrentProgress;
@end
